<s>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
akademickém	akademický	k2eAgNnSc6d1
(	(	kIx(
<g/>
resp.	resp.	kA
vědeckém	vědecký	k2eAgInSc6d1
<g/>
)	)	kIx)
titulu	titul	k1gInSc6
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
doktorským	doktorský	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
PhDr.	PhDr.	kA
–	–	k?
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
filozofie	filozofie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
akademický	akademický	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
rigorózní	rigorózní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
z	z	k7c2
lat.	lat.	k?
philosophiæ	philosophiæ	k?
doctor	doctor	k1gInSc1
<g/>
)	)	kIx)
akademického	akademický	k2eAgInSc2d1
titulu	titul	k1gInSc2
doktor	doktor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
lze	lze	k6eAd1
získat	získat	k5eAaPmF
vysokoškolským	vysokoškolský	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
v	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejvyšší	vysoký	k2eAgInSc1d3
titul	titul	k1gInSc1
<g/>
,	,	kIx,
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
dosáhnout	dosáhnout	k5eAaPmF
studiem	studio	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
tedy	tedy	k9
označuje	označovat	k5eAaImIp3nS
nositele	nositel	k1gMnSc4
doktorátu	doktorát	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
alternativu	alternativa	k1gFnSc4
doctor	doctor	k1gInSc1
philosophiae	philosophiae	k1gNnSc2
(	(	kIx(
<g/>
Doctor	Doctor	k1gInSc1
of	of	k?
Philosophy	Philosopha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někde	někde	k6eAd1
také	také	k9
bývá	bývat	k5eAaImIp3nS
zkracováno	zkracovat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
DPhil	DPhil	k1gInSc1
<g/>
,	,	kIx,
či	či	k8xC
Dr	dr	kA
<g/>
.	.	kIx.
phil	phil	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
často	často	k6eAd1
také	také	k9
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
PhD	PhD	k1gFnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
např.	např.	kA
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
také	také	k9
často	často	k6eAd1
jako	jako	k8xS,k8xC
PhD	PhD	k1gFnSc1
<g/>
;	;	kIx,
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
obecně	obecně	k6eAd1
mezinárodně	mezinárodně	k6eAd1
uznávaný	uznávaný	k2eAgInSc4d1
akademicko-vědecký	akademicko-vědecký	k2eAgInSc4d1
titul	titul	k1gInSc4
získaný	získaný	k2eAgInSc4d1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
třetím	třetí	k4xOgInSc6
cyklu	cyklus	k1gInSc6
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
uváděný	uváděný	k2eAgInSc4d1
za	za	k7c7
jménem	jméno	k1gNnSc7
(	(	kIx(
<g/>
oddělen	oddělit	k5eAaPmNgMnS
čárkou	čárka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
představuje	představovat	k5eAaImIp3nS
vědeckou	vědecký	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
doktora	doktor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
před	před	k7c7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
zkracuje	zkracovat	k5eAaImIp3nS
se	se	k3xPyFc4
často	často	k6eAd1
jako	jako	k9
Dr	dr	kA
<g/>
.	.	kIx.
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
–	–	k?
doktor	doktor	k1gMnSc1
<g/>
)	)	kIx)
uděluje	udělovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
pro	pro	k7c4
určité	určitý	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
vyčleněný	vyčleněný	k2eAgInSc4d1
jiný	jiný	k2eAgInSc4d1
ekvivalent	ekvivalent	k1gInSc4
–	–	k?
typicky	typicky	k6eAd1
pro	pro	k7c4
umělecké	umělecký	k2eAgInPc4d1
obory	obor	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
ArtD	ArtD	k1gFnSc6
<g/>
.	.	kIx.
–	–	k?
doktor	doktor	k1gMnSc1
umění	umění	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
případně	případně	k6eAd1
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
pro	pro	k7c4
náboženské	náboženský	k2eAgFnPc4d1
obory	obora	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
–	–	k?
doktor	doktor	k1gMnSc1
teologie	teologie	k1gFnSc2
<g/>
)	)	kIx)
atp.	atp.	kA
</s>
<s>
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
tři	tři	k4xCgInPc4
samostatné	samostatný	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
<g/>
,	,	kIx,
úrovně	úroveň	k1gFnPc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc1
(	(	kIx(
<g/>
studijní	studijní	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
bakalářské	bakalářský	k2eAgFnPc1d1
(	(	kIx(
<g/>
bachelor	bachelor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gNnSc7
<g/>
)	)	kIx)
–	–	k?
magisterské	magisterský	k2eAgFnPc1d1
(	(	kIx(
<g/>
master	master	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gNnSc7
<g/>
)	)	kIx)
–	–	k?
doktorské	doktorský	k2eAgFnPc4d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
doctor	doctor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gFnSc7
<g/>
,	,	kIx,
doctoral	doctorat	k5eAaImAgMnS,k5eAaPmAgMnS
degree	degreat	k5eAaPmIp3nS
<g/>
,	,	kIx,
doctorate	doctorat	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
doktorát	doktorát	k1gInSc4
zde	zde	k6eAd1
tedy	tedy	k9
představuje	představovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
možný	možný	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
dosažený	dosažený	k2eAgInSc4d1
studiem	studio	k1gNnSc7
(	(	kIx(
<g/>
8	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
požadavkem	požadavek	k1gInSc7
pro	pro	k7c4
práci	práce	k1gFnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
jako	jako	k9
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
pracovník	pracovník	k1gMnSc1
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
<g/>
,	,	kIx,
či	či	k8xC
vědec	vědec	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
oborech	obor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
uvedené	uvedený	k2eAgNnSc4d1
členění	členění	k1gNnSc4
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sjednotil	sjednotit	k5eAaPmAgInS
tzv.	tzv.	kA
Boloňský	boloňský	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
proces	proces	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
označovala	označovat	k5eAaImAgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
harmonizace	harmonizace	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
–	–	k?
příčinou	příčina	k1gFnSc7
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zavedení	zavedení	k1gNnSc2
byla	být	k5eAaImAgFnS
porovnatelnost	porovnatelnost	k1gFnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
určitá	určitý	k2eAgFnSc1d1
standardizace	standardizace	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
resp.	resp.	kA
také	také	k9
usnadnění	usnadnění	k1gNnSc4
studia	studio	k1gNnSc2
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
–	–	k?
důsledkem	důsledek	k1gInSc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
mj.	mj.	kA
zavedení	zavedení	k1gNnSc4
těchto	tento	k3xDgInPc2
stupňů	stupeň	k1gInPc2
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
tří	tři	k4xCgNnPc2
cyklů	cyklus	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
přechod	přechod	k1gInSc1
na	na	k7c4
tzv.	tzv.	kA
kreditový	kreditový	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
European	European	k1gMnSc1
Credit	Credit	k1gFnSc2
Transfer	transfer	k1gInSc1
and	and	k?
Accumulation	Accumulation	k1gInSc1
System	Systo	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
ECTS	ECTS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Orientačně	orientačně	k6eAd1
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
bakalářské	bakalářský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
trvá	trvat	k5eAaImIp3nS
většinou	většinou	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případné	případný	k2eAgNnSc4d1
navazující	navazující	k2eAgNnSc4d1
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
právě	právě	k9
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
pak	pak	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
zpravidla	zpravidla	k6eAd1
další	další	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
roky	rok	k1gInPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
nicméně	nicméně	k8xC
studium	studium	k1gNnSc1
nezáleží	záležet	k5eNaImIp3nS
primárně	primárně	k6eAd1
na	na	k7c6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
právě	právě	k9
na	na	k7c6
získaných	získaný	k2eAgInPc6d1
kreditech	kredit	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
ČR	ČR	kA
pro	pro	k7c4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
většinou	většinou	k6eAd1
získat	získat	k5eAaPmF
240	#num#	k4
kreditů	kredit	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
60	#num#	k4
ECTS	ECTS	kA
za	za	k7c4
akademický	akademický	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Požadavky	požadavek	k1gInPc1
pro	pro	k7c4
získání	získání	k1gNnSc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
jsou	být	k5eAaImIp3nP
odlišné	odlišný	k2eAgInPc1d1
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
instituci	instituce	k1gFnSc6
a	a	k8xC
časovém	časový	k2eAgNnSc6d1
období	období	k1gNnSc6
udělování	udělování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
nabytí	nabytí	k1gNnSc3
této	tento	k3xDgFnSc2
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
takovýto	takovýto	k3xDgMnSc1
student	student	k1gMnSc1
často	často	k6eAd1
označován	označován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
doktorand	doktorand	k1gMnSc1
(	(	kIx(
<g/>
doctoral	doctorat	k5eAaPmAgMnS,k5eAaImAgMnS
student	student	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
PhD	PhD	k1gMnSc1
student	student	k1gMnSc1
(	(	kIx(
<g/>
event.	event.	k?
též	též	k9
doctoral	doctorat	k5eAaImAgMnS,k5eAaPmAgMnS
candidate	candidat	k1gInSc5
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
candidate	candidat	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uchazeč	uchazeč	k1gMnSc1
o	o	k7c4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
musí	muset	k5eAaImIp3nS
předložit	předložit	k5eAaPmF
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
–	–	k?
disertaci	disertace	k1gFnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
obsahující	obsahující	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
originálního	originální	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
principu	princip	k1gInSc6
natolik	natolik	k6eAd1
hodnotný	hodnotný	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
uveřejněn	uveřejněn	k2eAgInSc1d1
v	v	k7c6
recenzovaném	recenzovaný	k2eAgInSc6d1
časopise	časopis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c2
zemích	zem	k1gFnPc6
takovýto	takovýto	k3xDgInSc4
kandidát	kandidát	k1gMnSc1
musí	muset	k5eAaImIp3nS
obhájit	obhájit	k5eAaPmF
tuto	tento	k3xDgFnSc4
práci	práce	k1gFnSc4
před	před	k7c7
komisí	komise	k1gFnSc7
zkoušejících	zkoušející	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
jmenovaných	jmenovaný	k2eAgFnPc2d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
kontextu	kontext	k1gInSc6
akademického	akademický	k2eAgInSc2d1
titulu	titul	k1gInSc2
–	–	k?
termín	termín	k1gInSc1
„	„	k?
<g/>
filozofie	filozofie	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
philosophiae	philosophia	k1gMnSc2
<g/>
)	)	kIx)
zde	zde	k6eAd1
neodkazuje	odkazovat	k5eNaImIp3nS
výhradně	výhradně	k6eAd1
na	na	k7c4
oblast	oblast	k1gFnSc4
akademické	akademický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
používán	používat	k5eAaImNgInS
v	v	k7c6
širším	široký	k2eAgInSc6d2
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc2
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
původním	původní	k2eAgInSc7d1
řeckým	řecký	k2eAgInSc7d1
významem	význam	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
láska	láska	k1gFnSc1
k	k	k7c3
moudrosti	moudrost	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
Evropy	Evropa	k1gFnSc2
všechny	všechen	k3xTgFnPc4
oblasti	oblast	k1gFnPc4
(	(	kIx(
<g/>
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
humanitní	humanitní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
matematika	matematika	k1gFnSc1
a	a	k8xC
přírodní	přírodní	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
<g/>
)	)	kIx)
jiné	jiný	k2eAgFnPc1d1
než	než	k8xS
teologie	teologie	k1gFnPc1
<g/>
,	,	kIx,
právo	právo	k1gNnSc1
a	a	k8xC
medicína	medicína	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
profesní	profesní	k2eAgFnPc1d1
<g/>
,	,	kIx,
odborné	odborný	k2eAgFnPc1d1
nebo	nebo	k8xC
technické	technický	k2eAgFnPc1d1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
tradičně	tradičně	k6eAd1
známé	známý	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
filosofie	filosofie	k1gFnSc1
a	a	k8xC
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
jinde	jinde	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
základní	základní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
byla	být	k5eAaImAgFnS
známa	znám	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
filosofická	filosofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Terminologie	terminologie	k1gFnSc1
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
<g/>
,	,	kIx,
čtení	čtení	k1gNnSc1
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1
„	„	k?
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
“	“	k?
lze	lze	k6eAd1
přečíst	přečíst	k5eAaPmF
<g/>
,	,	kIx,
resp.	resp.	kA
vyslovovat	vyslovovat	k5eAaImF
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
zkratku	zkratka	k1gFnSc4
anglicky	anglicky	k6eAd1
[	[	kIx(
<g/>
pʰ	pʰ	k?
eɪ	eɪ	k1gInSc1
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
diː	diː	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
vyslovováno	vyslovován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
[	[	kIx(
<g/>
pí	pí	k1gNnSc1
ejč	ejč	k?
dý	dý	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popř.	popř.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
česky	česky	k6eAd1
[	[	kIx(
<g/>
pé	pé	k?
há	há	k?
dé	dé	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
nikoliv	nikoliv	k9
jako	jako	k9
zkratku	zkratka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
plné	plný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
jako	jako	k9
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
však	však	k9
doporučuje	doporučovat	k5eAaImIp3nS
zkratky	zkratka	k1gFnPc4
nepoužívat	používat	k5eNaImF
při	při	k7c6
představování	představování	k1gNnSc6
konkrétních	konkrétní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
;	;	kIx,
tam	tam	k6eAd1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
třeba	třeba	k6eAd1
vyslovit	vyslovit	k5eAaPmF
nezkrácené	zkrácený	k2eNgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Uvádění	uvádění	k1gNnSc1
<g/>
,	,	kIx,
zápis	zápis	k1gInSc1
</s>
<s>
Titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jako	jako	k9
ty	ten	k3xDgMnPc4
ostatní	ostatní	k2eAgMnPc4d1
postgraduální	postgraduální	k2eAgMnPc4d1
této	tento	k3xDgFnSc2
úrovně	úroveň	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
CSc.	CSc.	kA
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
event.	event.	k?
i	i	k8xC
Dr	dr	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dále	daleko	k6eAd2
DSc	DSc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nebo	nebo	k8xC
ArtD	ArtD	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
či	či	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
správně	správně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
za	za	k7c7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
odděleno	oddělen	k2eAgNnSc1d1
od	od	k7c2
něj	on	k3xPp3gInSc2
čárkou	čárka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
USA	USA	kA
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
u	u	k7c2
jména	jméno	k1gNnSc2
uvádí	uvádět	k5eAaImIp3nS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
tento	tento	k3xDgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
právě	právě	k9
až	až	k9
od	od	k7c2
této	tento	k3xDgFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
úrovně	úroveň	k1gFnSc2
–	–	k?
doktorské	doktorský	k2eAgNnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
nižší	nízký	k2eAgInPc1d2
akademické	akademický	k2eAgInPc1d1
tituly	titul	k1gInPc1
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
bakalářské	bakalářský	k2eAgFnSc2d1
<g/>
,	,	kIx,
magisterské	magisterský	k2eAgFnSc2d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
běžně	běžně	k6eAd1
neužívají	užívat	k5eNaImIp3nP
<g/>
,	,	kIx,
výjimkou	výjimka	k1gFnSc7
však	však	k9
často	často	k6eAd1
bývají	bývat	k5eAaImIp3nP
některé	některý	k3yIgInPc4
profesní	profesní	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
tituly	titul	k1gInPc1
spjaté	spjatý	k2eAgInPc1d1
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
profesí	profes	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
M.	M.	kA
<g/>
D.	D.	kA
apod.	apod.	kA
Pokud	pokud	k8xS
se	s	k7c7
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
užívá	užívat	k5eAaImIp3nS
před	před	k7c7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
běžně	běžně	k6eAd1
uváděno	uvádět	k5eAaImNgNnS
jako	jako	k8xS,k8xC
Dr	dr	kA
<g/>
.	.	kIx.
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
např.	např.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Novák	Novák	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
resp.	resp.	kA
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
emailové	emailový	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
v	v	k7c6
cizím	cizí	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
tak	tak	k9
běžně	běžně	k6eAd1
užívá	užívat	k5eAaImIp3nS
jen	jen	k9
jeden	jeden	k4xCgInSc1
tento	tento	k3xDgInSc4
vědecký	vědecký	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
za	za	k7c7
jménem	jméno	k1gNnSc7
(	(	kIx(
<g/>
odděleno	oddělit	k5eAaPmNgNnS
čárkou	čárka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
Dr	dr	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
před	před	k7c7
jménem	jméno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
pouze	pouze	k6eAd1
jiný	jiný	k2eAgInSc4d1
nejvyšší	vysoký	k2eAgInSc4d3
získaný	získaný	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
/	/	kIx~
<g/>
hodnost	hodnost	k1gFnSc4
<g/>
/	/	kIx~
<g/>
funkce	funkce	k1gFnSc2
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
prof.	prof.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
nicméně	nicméně	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
obvyklé	obvyklý	k2eAgNnSc4d1
uvádění	uvádění	k1gNnSc4
i	i	k9
nižších	nízký	k2eAgMnPc2d2
titulů	titul	k1gInPc2
současně	současně	k6eAd1
–	–	k?
typicky	typicky	k6eAd1
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
SR	SR	kA
(	(	kIx(
<g/>
tyto	tento	k3xDgFnPc1
země	zem	k1gFnPc1
spolu	spolu	k6eAd1
mají	mít	k5eAaImIp3nP
uzavřenu	uzavřen	k2eAgFnSc4d1
mezinárodní	mezinárodní	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
uznávání	uznávání	k1gNnSc6
a	a	k8xC
rovnocennosti	rovnocennost	k1gFnSc6
dosaženého	dosažený	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
praxi	praxe	k1gFnSc6
často	často	k6eAd1
současně	současně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
navíc	navíc	k6eAd1
i	i	k9
nižší	nízký	k2eAgMnSc1d2
magisterský	magisterský	k2eAgMnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
titul	titul	k1gInSc1
(	(	kIx(
<g/>
před	před	k7c7
jménem	jméno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kupř	kupř	kA
<g/>
.	.	kIx.
technický	technický	k2eAgInSc4d1
<g/>
,	,	kIx,
humanitní	humanitní	k2eAgInSc4d1
<g/>
,	,	kIx,
lékařský	lékařský	k2eAgInSc4d1
<g/>
,	,	kIx,
veterinární	veterinární	k2eAgInSc4d1
apod.	apod.	kA
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
Mgr.	Mgr.	kA
Jana	Jana	k1gFnSc1
Nováková	Nováková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
MUDr.	MUDr.	kA
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
MVDr.	MVDr.	kA
Jana	Jana	k1gFnSc1
Nováková	Nováková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
JUDr.	JUDr.	kA
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
prof.	prof.	kA
MUDr.	MUDr.	kA
Jana	Jana	k1gFnSc1
Nováková	Nováková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
doc.	doc.	kA
ThDr.	ThDr.	k1gMnSc1
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
PhDr.	PhDr.	kA
Jana	Jana	k1gFnSc1
Nováková	Nováková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
et	et	k?
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
;	;	kIx,
doc.	doc.	kA
RNDr.	RNDr.	kA
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
atp.	atp.	kA
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
typické	typický	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
v	v	k7c6
tomto	tento	k3xDgInSc6
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
či	či	k8xC
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
v	v	k7c6
praxi	praxe	k1gFnSc6
samostatně	samostatně	k6eAd1
uvádějí	uvádět	k5eAaImIp3nP
i	i	k9
zmíněné	zmíněný	k2eAgInPc4d1
nižší	nízký	k2eAgInPc4d2
tituly	titul	k1gInPc4
(	(	kIx(
<g/>
magisterské	magisterský	k2eAgFnPc4d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
dokonce	dokonce	k9
i	i	k9
ty	ten	k3xDgFnPc1
bakalářské	bakalářský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
se	se	k3xPyFc4
tedy	tedy	k9
také	také	k9
odděluje	oddělovat	k5eAaImIp3nS
čárkou	čárka	k1gFnSc7
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
<g/>
-li	-li	k?
jméno	jméno	k1gNnSc1
ve	v	k7c6
větě	věta	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
přístavek	přístavek	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Host	host	k1gMnSc1
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
připomenul	připomenout	k5eAaPmAgMnS
důležitost	důležitost	k1gFnSc4
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
–	–	k?
rozepsáno	rozepsán	k2eAgNnSc1d1
<g/>
:	:	kIx,
„	„	k?
<g/>
Host	host	k1gMnSc1
inženýr	inženýr	k1gMnSc1
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
doktor	doktor	k1gMnSc1
<g/>
,	,	kIx,
připomenul	připomenout	k5eAaPmAgMnS
důležitost	důležitost	k1gFnSc4
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Ve	v	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
či	či	k8xC
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
,	,	kIx,
by	by	kYmCp3nP
se	se	k3xPyFc4
pak	pak	k6eAd1
dotyčný	dotyčný	k2eAgMnSc1d1
nejčastěji	často	k6eAd3
uvedl	uvést	k5eAaPmAgMnS
pouze	pouze	k6eAd1
jako	jako	k9
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Novak	Novak	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
jako	jako	k8xC,k8xS
Jan	Jan	k1gMnSc1
Novak	Novak	k1gMnSc1
<g/>
,	,	kIx,
PhD	PhD	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
jako	jako	k9
Dr	dr	kA
<g/>
.	.	kIx.
Jan	Jan	k1gMnSc1
Novak	Novak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
psané	psaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
v	v	k7c6
méně	málo	k6eAd2
oficiálních	oficiální	k2eAgInPc6d1
<g/>
,	,	kIx,
neúředních	úřední	k2eNgInPc6d1
dokumentech	dokument	k1gInPc6
též	též	k6eAd1
možné	možný	k2eAgNnSc1d1
pro	pro	k7c4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
doktoráty	doktorát	k1gInPc4
<g/>
,	,	kIx,
užití	užití	k1gNnSc4
obecné	obecný	k2eAgFnSc2d1
zkratky	zkratka	k1gFnSc2
od	od	k7c2
slova	slovo	k1gNnSc2
doktor	doktor	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k9
prostého	prostý	k2eAgNnSc2d1
dr	dr	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
zkratkou	zkratka	k1gFnSc7
akademického	akademický	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
z	z	k7c2
ní	on	k3xPp3gFnSc2
nevyplývá	vyplývat	k5eNaImIp3nS
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
oblasti	oblast	k1gFnSc6
byl	být	k5eAaImAgInS
doktorát	doktorát	k1gInSc4
získán	získán	k2eAgMnSc1d1
<g/>
,	,	kIx,
resp.	resp.	kA
jestli	jestli	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
velký	velký	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
malý	malý	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dle	dle	k7c2
vysokoškolského	vysokoškolský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
zákona	zákon	k1gInSc2
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
v	v	k7c6
Česku	Česko	k1gNnSc6
kodifikována	kodifikován	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
„	„	k?
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
nikoliv	nikoliv	k9
„	„	k?
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
D.	D.	kA
<g/>
“	“	k?
–	–	k?
nebo	nebo	k8xC
„	„	k?
<g/>
PhD	PhD	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
,	,	kIx,
byť	byť	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
dle	dle	k7c2
obecných	obecný	k2eAgFnPc2d1
typografických	typografický	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
správný	správný	k2eAgInSc4d1
zápis	zápis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Užívání	užívání	k1gNnSc1
<g/>
,	,	kIx,
formální	formální	k2eAgNnSc1d1
oslovování	oslovování	k1gNnSc1
</s>
<s>
Při	při	k7c6
formálním	formální	k2eAgNnSc6d1
oslovování	oslovování	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
vždy	vždy	k6eAd1
nejvyšší	vysoký	k2eAgInSc1d3
dosažený	dosažený	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
/	/	kIx~
<g/>
hodnost	hodnost	k1gFnSc1
<g/>
,	,	kIx,
vhodné	vhodný	k2eAgNnSc1d1
bývá	bývat	k5eAaImIp3nS
upřednostnit	upřednostnit	k5eAaPmF
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nositele	nositel	k1gMnPc4
doktorátu	doktorát	k1gInSc2
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
titulu	titul	k1gInSc2
doktorské	doktorský	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
(	(	kIx(
<g/>
8	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
včetně	včetně	k7c2
ČR	ČR	kA
<g/>
)	)	kIx)
oslovujeme	oslovovat	k5eAaImIp1nP
„	„	k?
<g/>
pane	pan	k1gMnSc5
doktore	doktor	k1gMnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
paní	paní	k1gFnSc2
doktorko	doktorka	k1gFnSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
<g />
.	.	kIx.
</s>
<s hack="1">
nižším	nízký	k2eAgInSc7d2
titulem	titul	k1gInSc7
magisterské	magisterský	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
pane	pan	k1gMnSc5
inženýre	inženýr	k1gMnSc5
<g/>
/	/	kIx~
<g/>
magistře	magistra	k1gFnSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
nebo	nebo	k8xC
snad	snad	k9
ještě	ještě	k6eAd1
nižším	nízký	k2eAgInSc7d2
titulem	titul	k1gInSc7
bakalářské	bakalářský	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
(	(	kIx(
<g/>
6	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
pane	pan	k1gMnSc5
bakaláři	bakalář	k1gMnSc5
<g/>
“	“	k?
apod.	apod.	kA
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Některá	některý	k3yIgNnPc1
dřívější	dřívější	k2eAgNnPc1d1
doporučení	doporučení	k1gNnPc1
pro	pro	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
používat	používat	k5eAaImF
k	k	k7c3
formálnímu	formální	k2eAgNnSc3d1
oslovování	oslovování	k1gNnSc3
jen	jen	k9
titul	titul	k1gInSc4
před	před	k7c7
jménem	jméno	k1gNnSc7
již	již	k9
nejsou	být	k5eNaImIp3nP
aktuální	aktuální	k2eAgInPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
původně	původně	k6eAd1
se	se	k3xPyFc4
totiž	totiž	k9
do	do	k7c2
Boloňského	boloňský	k2eAgInSc2d1
procesu	proces	k1gInSc2
uděloval	udělovat	k5eAaImAgMnS
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
především	především	k6eAd1
bývalého	bývalý	k2eAgMnSc2d1
tzv.	tzv.	kA
Východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
<g/>
)	)	kIx)
místo	místo	k7c2
doktora	doktor	k1gMnSc2
(	(	kIx(
<g/>
Dr	dr	kA
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
kandidát	kandidát	k1gMnSc1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
CSc.	CSc.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
oslovení	oslovení	k1gNnSc1
„	„	k?
<g/>
pane	pan	k1gMnSc5
kandidáte	kandidát	k1gMnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
paní	paní	k1gFnSc1
kandidátko	kandidátka	k1gFnSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
neužívalo	užívat	k5eNaImAgNnS
<g/>
,	,	kIx,
resp.	resp.	kA
neužívá	užívat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Starověk	starověk	k1gInSc1
<g/>
,	,	kIx,
středověk	středověk	k1gInSc1
a	a	k8xC
novověk	novověk	k1gInSc1
</s>
<s>
Latinsky	latinsky	k6eAd1
Philosophiæ	Philosophiæ	k1gMnSc1
doctor	doctor	k1gMnSc1
v	v	k7c6
doslovném	doslovný	k2eAgInSc6d1
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
doktor	doktor	k1gMnSc1
filozofie	filozofie	k1gFnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
však	však	k9
toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
starořeckého	starořecký	k2eAgInSc2d1
Δ	Δ	k?
Φ	Φ	k?
<g/>
,	,	kIx,
tj.	tj.	kA
učitel	učitel	k1gMnSc1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
doctor	doctor	k1gInSc4
lze	lze	k6eAd1
také	také	k9
volně	volně	k6eAd1
přeložit	přeložit	k5eAaPmF
jako	jako	k9
učený	učený	k2eAgMnSc1d1
<g/>
,	,	kIx,
resp.	resp.	kA
učitel	učitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
titul	titul	k1gInSc4
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
udělován	udělovat	k5eAaImNgInS
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
svého	svůj	k3xOyFgNnSc2
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
dokončily	dokončit	k5eAaPmAgFnP
dostatečně	dostatečně	k6eAd1
komplexní	komplexní	k2eAgFnSc4d1
výzkumnou	výzkumný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
o	o	k7c6
předmětu	předmět	k1gInSc6
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
a	a	k8xC
případně	případně	k6eAd1
završily	završit	k5eAaPmAgFnP
některé	některý	k3yIgFnPc1
další	další	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
přesněji	přesně	k6eAd2
definované	definovaný	k2eAgInPc1d1
studijními	studijní	k2eAgFnPc7d1
zvyklostmi	zvyklost	k1gFnPc7
dané	daný	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
už	už	k9
za	za	k7c2
starého	starý	k2eAgInSc2d1
Říma	Řím	k1gInSc2
však	však	k9
byli	být	k5eAaImAgMnP
jako	jako	k9
doktoři	doktor	k1gMnPc1
označováni	označován	k2eAgMnPc1d1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
konali	konat	k5eAaImAgMnP
veřejné	veřejný	k2eAgFnPc4d1
přednášky	přednáška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Asi	asi	k9
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
doktor	doktor	k1gMnSc1
stalo	stát	k5eAaPmAgNnS
čestné	čestný	k2eAgNnSc4d1
označení	označení	k1gNnSc4
učenců	učenec	k1gMnPc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
připojován	připojován	k2eAgInSc4d1
výstižný	výstižný	k2eAgInSc4d1
přívlastek	přívlastek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Tomáš	Tomáš	k1gMnSc1
Akvinský	Akvinský	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
doctor	doctor	k1gInSc4
angelicus	angelicus	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Duns	Dunsa	k1gFnPc2
Scotus	Scotus	k1gMnSc1
doctor	doctor	k1gMnSc1
subtilis	subtilis	k1gFnSc1
nebo	nebo	k8xC
Roger	Roger	k1gMnSc1
Bacon	Bacon	k1gMnSc1
doctor	doctor	k1gMnSc1
mirabilis	mirabilis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církevní	církevní	k2eAgMnPc1d1
otcové	otec	k1gMnPc1
byli	být	k5eAaImAgMnP
nazývání	nazývání	k1gNnSc4
doctores	doctoresa	k1gFnPc2
ecclesiae	ecclesia	k1gInSc2
a	a	k8xC
jako	jako	k8xC,k8xS
doctores	doctores	k1gMnSc1
thalmudiaci	thalmudiace	k1gFnSc4
vystupovali	vystupovat	k5eAaImAgMnP
židovští	židovský	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
<g/>
,	,	kIx,
znalci	znalec	k1gMnPc1
talmudu	talmud	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
středověké	středověký	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
mohl	moct	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
student	student	k1gMnSc1
získat	získat	k5eAaPmF
akademický	akademický	k2eAgInSc4d1
gradus	gradus	k1gInSc4
bakalářský	bakalářský	k2eAgInSc4d1
–	–	k?
Baccalaureatus	Baccalaureatus	k1gInSc1
(	(	kIx(
<g/>
vavřínem	vavřín	k1gInSc7
ověnčený	ověnčený	k2eAgMnSc1d1
<g/>
)	)	kIx)
po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
absolvování	absolvování	k1gNnSc6
trivia	trivium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
tvořila	tvořit	k5eAaImAgFnS
gramatika	gramatika	k1gFnSc1
<g/>
,	,	kIx,
dialektika	dialektika	k1gFnSc1
a	a	k8xC
rétorika	rétorika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
absolvent	absolvent	k1gMnSc1
vypomáhal	vypomáhat	k5eAaImAgMnS
s	s	k7c7
učením	učení	k1gNnSc7
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
kvadriviu	kvadrivium	k1gNnSc3
(	(	kIx(
<g/>
aritmetika	aritmetika	k1gFnSc1
<g/>
,	,	kIx,
geometrie	geometrie	k1gFnSc1
<g/>
,	,	kIx,
astronomie	astronomie	k1gFnSc1
a	a	k8xC
hudba	hudba	k1gFnSc1
<g/>
;	;	kIx,
dohromady	dohromady	k6eAd1
tzv.	tzv.	kA
sedm	sedm	k4xCc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
bakalářský	bakalářský	k2eAgInSc1d1
gradus	gradus	k1gInSc1
získával	získávat	k5eAaImAgInS
po	po	k7c6
absolvování	absolvování	k1gNnSc6
artistické	artistický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
umožňoval	umožňovat	k5eAaImAgInS
vstup	vstup	k1gInSc4
na	na	k7c4
ostatní	ostatní	k1gNnSc4
fakulty	fakulta	k1gFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalář	bakalář	k1gMnSc1
následně	následně	k6eAd1
po	po	k7c6
dalším	další	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
a	a	k8xC
obhajobě	obhajoba	k1gFnSc3
samostatné	samostatný	k2eAgFnSc2d1
odborné	odborný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
these	these	k1gFnPc1
<g/>
)	)	kIx)
mohl	moct	k5eAaImAgInS
získat	získat	k5eAaPmF
gradus	gradus	k1gInSc1
Magister	magistra	k1gFnPc2
(	(	kIx(
<g/>
mistra	mistr	k1gMnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
učitel	učitel	k1gMnSc1
či	či	k8xC
představený	představený	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
artistické	artistický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
(	(	kIx(
<g/>
fakultě	fakulta	k1gFnSc6
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
celý	celý	k2eAgInSc4d1
název	název	k1gInSc4
magistr	magistr	k1gMnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magistr	magistr	k1gMnSc1
značil	značit	k5eAaImAgMnS
na	na	k7c6
středověké	středověký	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
původně	původně	k6eAd1
učitele	učitel	k1gMnPc4
<g/>
,	,	kIx,
už	už	k6eAd1
ve	v	k7c6
středověku	středověk	k1gInSc6
se	se	k3xPyFc4
však	však	k9
stal	stát	k5eAaPmAgInS
akademickým	akademický	k2eAgInSc7d1
titulem	titul	k1gInSc7
a	a	k8xC
byl	být	k5eAaImAgInS
nezbytným	nezbytný	k2eAgInSc7d1,k2eNgInSc7d1
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
získání	získání	k1gNnSc4
učitelského	učitelský	k2eAgNnSc2d1
oprávnění	oprávnění	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
licentia	licentia	k1gFnSc1
docendi	docend	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprávněným	oprávněný	k2eAgNnSc7d1
<g/>
,	,	kIx,
tj.	tj.	kA
zpravidla	zpravidla	k6eAd1
jmenovaným	jmenovaný	k2eAgMnPc3d1
učitelům	učitel	k1gMnPc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
někdy	někdy	k6eAd1
říkalo	říkat	k5eAaImAgNnS
též	též	k9
magister	magister	k1gMnSc1
regens	regens	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
boloňské	boloňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
nicméně	nicméně	k8xC
postupně	postupně	k6eAd1
v	v	k7c6
okruhu	okruh	k1gInSc6
učitelů	učitel	k1gMnPc2
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
doctores	doctores	k1gInSc1
legum	legum	k1gNnSc1
<g/>
)	)	kIx)
z	z	k7c2
čestného	čestný	k2eAgInSc2d1
titulu	titul	k1gInSc2
stala	stát	k5eAaPmAgFnS
zvláštní	zvláštní	k2eAgFnSc1d1
akademická	akademický	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učitelé	učitel	k1gMnPc1
kanonického	kanonický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
byli	být	k5eAaImAgMnP
zváni	zvát	k5eAaImNgMnP
doctores	doctores	k1gMnSc1
canonum	canonum	k1gNnSc1
et	et	k?
decretalium	decretalium	k1gNnSc1
a	a	k8xC
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
sloučilo	sloučit	k5eAaPmAgNnS
studium	studium	k1gNnSc1
světského	světský	k2eAgNnSc2d1
a	a	k8xC
církevního	církevní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
dodnes	dodnes	k6eAd1
užívaný	užívaný	k2eAgInSc1d1
titul	titul	k1gInSc1
iuris	iuris	k1gFnSc2
utriusque	utriusquat	k5eAaPmIp3nS
doctor	doctor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
něj	on	k3xPp3gInSc2
pak	pak	k6eAd1
na	na	k7c6
pařížské	pařížský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
vznikl	vzniknout	k5eAaPmAgInS
theologiae	theologiae	k6eAd1
doctor	doctor	k1gInSc1
a	a	k8xC
pak	pak	k6eAd1
také	také	k9
doktorát	doktorát	k1gInSc4
lékařství	lékařství	k1gNnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
gramatiky	gramatika	k1gFnSc2
<g/>
,	,	kIx,
logiky	logika	k1gFnSc2
apod.	apod.	kA
Povinným	povinný	k2eAgInSc7d1
předstupněm	předstupeň	k1gInSc7
k	k	k7c3
získání	získání	k1gNnSc3
doktorátu	doktorát	k1gInSc2
bylo	být	k5eAaImAgNnS
ale	ale	k9
nutně	nutně	k6eAd1
bakalářské	bakalářský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
na	na	k7c6
artistické	artistický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
,	,	kIx,
následované	následovaný	k2eAgFnSc6d1
ziskem	zisk	k1gInSc7
licenciátu	licenciát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
titulu	titul	k1gInSc2
doktora	doktor	k1gMnSc4
bylo	být	k5eAaImAgNnS
též	též	k9
dříve	dříve	k6eAd2
používáno	používán	k2eAgNnSc4d1
označení	označení	k1gNnSc4
mistr	mistr	k1gMnSc1
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
získávali	získávat	k5eAaImAgMnP
doktorát	doktorát	k1gInSc4
právníci	právník	k1gMnPc1
a	a	k8xC
lékaři	lékař	k1gMnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
bohoslovecké	bohoslovecký	k2eAgFnSc6d1
a	a	k8xC
artistické	artistický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
byl	být	k5eAaImAgInS
nejvyšším	vysoký	k2eAgInSc7d3
titulem	titul	k1gInSc7
Magister	magister	k1gMnSc1
(	(	kIx(
<g/>
mistr	mistr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
odborných	odborný	k2eAgFnPc6d1
fakultách	fakulta	k1gFnPc6
(	(	kIx(
<g/>
ve	v	k7c6
středověku	středověk	k1gInSc6
teologické	teologický	k2eAgFnSc2d1
<g/>
,	,	kIx,
právnické	právnický	k2eAgFnSc2d1
a	a	k8xC
lékařské	lékařský	k2eAgFnSc2d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
uděloval	udělovat	k5eAaImAgMnS
titul	titul	k1gInSc4
doktor	doktor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
až	až	k6eAd1
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
titul	titul	k1gInSc1
magistra	magister	k1gMnSc4
nahradil	nahradit	k5eAaPmAgInS
a	a	k8xC
ten	ten	k3xDgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
pouze	pouze	k6eAd1
titulem	titul	k1gInSc7
farmaceutů	farmaceut	k1gMnPc2
(	(	kIx(
<g/>
PhMr	PhMr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
18	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
tedy	tedy	k8xC
magisterský	magisterský	k2eAgInSc1d1
titul	titul	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
z	z	k7c2
užívání	užívání	k1gNnSc2
a	a	k8xC
až	až	k9
do	do	k7c2
moderní	moderní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
zůstaly	zůstat	k5eAaPmAgFnP
již	již	k6eAd1
jen	jen	k6eAd1
různé	různý	k2eAgFnPc4d1
obdoby	obdoba	k1gFnPc4
doktorátů	doktorát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Vídni	Vídeň	k1gFnSc6
tvořili	tvořit	k5eAaImAgMnP
absolventi	absolvent	k1gMnPc1
univerzit	univerzita	k1gFnPc2
s	s	k7c7
doktorátem	doktorát	k1gInSc7
tzv.	tzv.	kA
doktorské	doktorský	k2eAgInPc1d1
sbory	sbor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1873	#num#	k4
mj.	mj.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
podílely	podílet	k5eAaImAgInP
na	na	k7c6
správě	správa	k1gFnSc6
univerzity	univerzita	k1gFnSc2
a	a	k8xC
příslušných	příslušný	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
a	a	k8xC
na	na	k7c6
doktorských	doktorský	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Magisterský	magisterský	k2eAgInSc1d1
titul	titul	k1gInSc1
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
znovu	znovu	k6eAd1
zaváděn	zavádět	k5eAaImNgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
Boloňského	boloňský	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
tzv.	tzv.	kA
malé	malý	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
a	a	k8xC
aby	aby	kYmCp3nP
též	též	k9
vysokoškolský	vysokoškolský	k2eAgInSc4d1
systém	systém	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
rovněž	rovněž	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
titulů	titul	k1gInPc2
<g/>
,	,	kIx,
lépe	dobře	k6eAd2
odpovídal	odpovídat	k5eAaImAgMnS
titulům	titul	k1gInPc3
anglosaského	anglosaský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc1
pro	pro	k7c4
získání	získání	k1gNnSc4
doktorátu	doktorát	k1gInSc2
obtížnější	obtížný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Reformy	reforma	k1gFnPc1
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
V	v	k7c6
Německu	Německo	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
odehrávaly	odehrávat	k5eAaImAgFnP
významné	významný	k2eAgFnPc1d1
změny	změna	k1gFnPc1
ve	v	k7c6
vysokoškolském	vysokoškolský	k2eAgNnSc6d1
vzdělávání	vzdělávání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artistická	artistický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
označena	označen	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
pro	pro	k7c4
získání	získání	k1gNnSc4
finální	finální	k2eAgFnSc2d1
<g/>
,	,	kIx,
závěrečné	závěrečný	k2eAgFnSc2d1
<g/>
,	,	kIx,
kvalifikace	kvalifikace	k1gFnSc2
požadovat	požadovat	k5eAaImF
též	též	k9
určité	určitý	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
osvědčené	osvědčený	k2eAgNnSc1d1
disertací	disertace	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
označeno	označit	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
doktorát	doktorát	k1gInSc1
filozofie	filozofie	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
veškeré	veškerý	k3xTgInPc1
s	s	k7c7
tímto	tento	k3xDgNnSc7
spjaté	spjatý	k2eAgNnSc1d1
financování	financování	k1gNnSc1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
zajišťováno	zajišťovat	k5eAaImNgNnS
centrální	centrální	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reformy	reforma	k1gFnPc1
zde	zde	k6eAd1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
mimořádně	mimořádně	k6eAd1
úspěšné	úspěšný	k2eAgFnPc1d1
a	a	k8xC
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
začaly	začít	k5eAaPmAgFnP
německé	německý	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
přitahovat	přitahovat	k5eAaImF
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
praxe	praxe	k1gFnSc1
měla	mít	k5eAaImAgFnS
natolik	natolik	k6eAd1
značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
dovezena	dovézt	k5eAaPmNgFnS
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
začala	začít	k5eAaPmAgFnS
Yale	Yale	k1gFnSc1
University	universita	k1gFnSc2
udělovat	udělovat	k5eAaImF
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
těm	ten	k3xDgMnPc3
studentům	student	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
již	již	k6eAd1
měli	mít	k5eAaImAgMnP
ukončený	ukončený	k2eAgInSc4d1
bakalářský	bakalářský	k2eAgInSc4d1
program	program	k1gInSc4
na	na	k7c6
americké	americký	k2eAgFnSc6d1
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
úspěšně	úspěšně	k6eAd1
ukončili	ukončit	k5eAaPmAgMnP
předepsaný	předepsaný	k2eAgInSc4d1
kurz	kurz	k1gInSc4
a	a	k8xC
úspěšně	úspěšně	k6eAd1
obhájili	obhájit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
resp.	resp.	kA
disertaci	disertace	k1gFnSc4
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInSc4d1
původní	původní	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
v	v	k7c6
přírodních	přírodní	k2eAgFnPc6d1
či	či	k8xC
humanitních	humanitní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kvalifikace	kvalifikace	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc6
takovou	takový	k3xDgFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
udělena	udělit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
v	v	k7c6
Německu	Německo	k1gNnSc6
začaly	začít	k5eAaPmAgFnP
filozofické	filozofický	k2eAgFnPc1d1
fakulty	fakulta	k1gFnPc1
rozdělovat	rozdělovat	k5eAaImF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
název	název	k1gInSc4
doktorátu	doktorát	k1gInSc2
upravován	upravován	k2eAgInSc4d1
–	–	k?
např.	např.	kA
Dr	dr	kA
<g/>
.	.	kIx.
rer	rer	k?
<g/>
.	.	kIx.
nat	nat	k?
<g/>
.	.	kIx.
pro	pro	k7c4
doktoráty	doktorát	k1gInPc4
na	na	k7c6
fakultách	fakulta	k1gFnPc6
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
–	–	k?
nicméně	nicméně	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
anglicky	anglicky	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byl	být	k5eAaImAgInS
ponechán	ponechat	k5eAaPmNgInS
doktorát	doktorát	k1gInSc1
filosofie	filosofie	k1gFnSc2
jako	jako	k8xC,k8xS
doktorát	doktorát	k1gInSc4
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
disciplínách	disciplína	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Vysokoškolské	vysokoškolský	k2eAgFnPc1d1
kvalifikace	kvalifikace	k1gFnPc1
určené	určený	k2eAgFnPc1d1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
objevily	objevit	k5eAaPmAgInP
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
doktora	doktor	k1gMnSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
Doctor	Doctor	k1gInSc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
DSc	DSc	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
ScD	ScD	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
ostatních	ostatní	k2eAgInPc2d1
vyšších	vysoký	k2eAgInPc2d2
doktorátů	doktorát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
představila	představit	k5eAaPmAgFnS
DSc	DSc	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jako	jako	k9
pokročilý	pokročilý	k2eAgInSc4d1
studijní	studijní	k2eAgInSc4d1
kurz	kurz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
přímo	přímo	k6eAd1
následoval	následovat	k5eAaImAgInS
na	na	k7c4
předchozí	předchozí	k2eAgNnSc4d1
bakalářské	bakalářský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
BSc	BSc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spíše	spíše	k9
než	než	k8xS
jako	jako	k9
kvalifikaci	kvalifikace	k1gFnSc4
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
tzv.	tzv.	kA
vyšší	vysoký	k2eAgInSc4d2
doktorát	doktorát	k1gInSc4
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
byl	být	k5eAaImAgInS
DSc	DSc	k1gMnSc4
z	z	k7c2
Durham	Durham	k1gInSc4
University	universita	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
představen	představen	k2eAgInSc1d1
roku	rok	k1gInSc2
1882	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedené	uvedený	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
brzy	brzy	k6eAd1
následováno	následovat	k5eAaImNgNnS
dalšími	další	k2eAgFnPc7d1
univerzitami	univerzita	k1gFnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zřizuje	zřizovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
ScD	ScD	k1gFnSc4
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
a	a	k8xC
University	universita	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
transformuje	transformovat	k5eAaBmIp3nS
svůj	svůj	k3xOyFgMnSc1
DSc	DSc	k1gMnSc1
do	do	k7c2
programu	program	k1gInSc2
určeného	určený	k2eAgInSc2d1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
roku	rok	k1gInSc2
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgFnPc4d1
kvalifikace	kvalifikace	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k9
spíše	spíše	k9
než	než	k8xS
programy	program	k1gInPc1
určené	určený	k2eAgInPc1d1
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
na	na	k7c6
úrovni	úroveň	k1gFnSc6
PhD	PhD	k1gFnSc2
<g/>
,	,	kIx,
uvádí	uvádět	k5eAaImIp3nS
Harold	Harold	k1gMnSc1
Jeffreys	Jeffreys	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
získání	získání	k1gNnSc1
doktorátu	doktorát	k1gInSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
ScD	ScD	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
bylo	být	k5eAaImAgNnS
„	„	k?
<g/>
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
být	být	k5eAaImF
navržen	navrhnout	k5eAaPmNgInS
pro	pro	k7c4
Royal	Royal	k1gInSc4
Society	societa	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
akademii	akademie	k1gFnSc3
věd	věda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konečně	konečně	k6eAd1
roku	rok	k1gInSc2
1917	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
představen	představit	k5eAaPmNgInS
i	i	k9
program	program	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
výcvik	výcvik	k1gInSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
současný	současný	k2eAgMnSc1d1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
program	program	k1gInSc1
(	(	kIx(
<g/>
Doctor	Doctor	k1gInSc1
of	of	k?
Philosophy	Philosopha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
vzoru	vzor	k1gInSc6
amerického	americký	k2eAgInSc2d1
a	a	k8xC
německého	německý	k2eAgInSc2d1
modelu	model	k1gInSc2
–	–	k?
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rychle	rychle	k6eAd1
oblíbeným	oblíbený	k2eAgNnSc7d1
mezi	mezi	k7c7
britskými	britský	k2eAgMnPc7d1
i	i	k8xC
zahraničními	zahraniční	k2eAgMnPc7d1
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírně	mírně	k6eAd1
starší	starý	k2eAgFnSc1d2
DSc	DSc	k1gFnSc1
programy	program	k1gInPc1
(	(	kIx(
<g/>
Doctor	Doctor	k1gInSc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
)	)	kIx)
nicméně	nicméně	k8xC
stále	stále	k6eAd1
na	na	k7c6
britských	britský	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
existují	existovat	k5eAaImIp3nP
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
mnoha	mnoho	k4c7
staršími	starší	k1gMnPc7
tzv.	tzv.	kA
vyššími	vysoký	k2eAgInPc7d2
doktoráty	doktorát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nebyly	být	k5eNaImAgInP
uvedené	uvedený	k2eAgInPc1d1
pokročilé	pokročilý	k2eAgInPc1d1
programy	program	k1gInPc1
kritériem	kritérion	k1gNnSc7
pro	pro	k7c4
profesuru	profesura	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
většině	většina	k1gFnSc6
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
postupně	postupně	k6eAd1
měnit	měnit	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
ambicióznější	ambiciózní	k2eAgMnPc1d2
badatelé	badatel	k1gMnPc1
na	na	k7c6
velkých	velký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
odjížděli	odjíždět	k5eAaImAgMnP
do	do	k7c2
Německa	Německo	k1gNnSc2
na	na	k7c4
1	#num#	k4
až	až	k9
3	#num#	k4
roky	rok	k1gInPc4
získat	získat	k5eAaPmF
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
přírodních	přírodní	k2eAgFnPc2d1
či	či	k8xC
humanitních	humanitní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
tak	tak	k9
i	i	k9
v	v	k7c6
USA	USA	kA
začaly	začít	k5eAaPmAgFnP
objevovat	objevovat	k5eAaImF
školy	škola	k1gFnPc4
nabízející	nabízející	k2eAgFnSc2d1
vyšší	vysoký	k2eAgFnSc2d2
kvalifikace	kvalifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1861	#num#	k4
Yale	Yale	k1gFnSc1
University	universita	k1gFnSc2
udělovala	udělovat	k5eAaImAgFnS
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
doktoráty	doktorát	k1gInPc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
je	on	k3xPp3gFnPc4
Eugene	Eugen	k1gInSc5
Schuyler	Schuyler	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Williams	Williamsa	k1gFnPc2
Wright	Wright	k1gMnSc1
a	a	k8xC
James	James	k1gMnSc1
Morris	Morris	k1gFnSc2
Whiton	Whiton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
též	též	k9
další	další	k2eAgFnPc1d1
univerzity	univerzita	k1gFnPc1
v	v	k7c6
USA	USA	kA
začaly	začít	k5eAaPmAgFnP
nabízet	nabízet	k5eAaImF
doktorský	doktorský	k2eAgInSc4d1
program	program	k1gInSc4
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
Harvard	Harvard	k1gInSc1
University	universita	k1gFnPc1
či	či	k8xC
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
zaměřené	zaměřený	k2eAgFnPc4d1
na	na	k7c4
tento	tento	k3xDgInSc4
program	program	k1gInSc4
následovaly	následovat	k5eAaImAgFnP
<g/>
,	,	kIx,
např.	např.	kA
Johns	Johns	k1gInSc1
Hopkins	Hopkins	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
USA	USA	kA
udíleno	udílet	k5eAaImNgNnS
zhruba	zhruba	k6eAd1
300	#num#	k4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
šesti	šest	k4xCc7
univerzitami	univerzita	k1gFnPc7
(	(	kIx(
<g/>
Yale	Yal	k1gFnPc1
University	universita	k1gFnPc1
<g/>
,	,	kIx,
Johns	Johns	k1gInSc1
Hopkins	Hopkinsa	k1gFnPc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc4
University	universita	k1gFnSc2
a	a	k8xC
dalšími	další	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebylo	být	k5eNaImAgNnS
tak	tak	k6eAd1
již	již	k6eAd1
nutné	nutný	k2eAgNnSc1d1
studovat	studovat	k5eAaImF
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Německu	Německo	k1gNnSc6
národní	národní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
financovala	financovat	k5eAaBmAgFnS
univerzity	univerzita	k1gFnPc4
a	a	k8xC
výzkumné	výzkumný	k2eAgInPc4d1
programy	program	k1gInPc4
předních	přední	k2eAgMnPc2d1
profesorů	profesor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nemožné	možný	k2eNgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
profesoři	profesor	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nebyli	být	k5eNaImAgMnP
schváleni	schválit	k5eAaPmNgMnP
Berlínem	Berlín	k1gInSc7
<g/>
,	,	kIx,
vzdělávali	vzdělávat	k5eAaImAgMnP
studenty	student	k1gMnPc7
vyšších	vysoký	k2eAgFnPc2d2
kvalifikací	kvalifikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
soukromé	soukromý	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
obdobně	obdobně	k6eAd1
jako	jako	k9
státní	státní	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
nezávislé	závislý	k2eNgInPc1d1
na	na	k7c6
federální	federální	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezávislost	nezávislost	k1gFnSc1
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
prostředky	prostředek	k1gInPc1
pro	pro	k7c4
financování	financování	k1gNnSc4
byly	být	k5eAaImAgFnP
nízké	nízký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průlom	průlom	k1gInSc1
přišel	přijít	k5eAaPmAgInS
ze	z	k7c2
strany	strana	k1gFnSc2
soukromých	soukromý	k2eAgFnPc2d1
nadací	nadace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
začaly	začít	k5eAaPmAgFnP
pravidelně	pravidelně	k6eAd1
podporovat	podporovat	k5eAaImF
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
a	a	k8xC
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
velké	velký	k2eAgFnPc4d1
korporace	korporace	k1gFnPc4
pak	pak	k6eAd1
někdy	někdy	k6eAd1
podporovaly	podporovat	k5eAaImAgInP
inženýrské	inženýrský	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1919	#num#	k4
pak	pak	k6eAd1
Rockefeller	Rockefeller	k1gInSc4
Foundation	Foundation	k1gInSc4
založila	založit	k5eAaPmAgFnS
pro	pro	k7c4
tzv.	tzv.	kA
postdoktorandy	postdoktoranda	k1gFnSc2
postdoctoral	postdoctorat	k5eAaPmAgInS,k5eAaImAgInS
fellowship	fellowship	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
přední	přední	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
učenými	učený	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
zřídily	zřídit	k5eAaPmAgFnP
síť	síť	k1gFnSc4
vědeckých	vědecký	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
pokrok	pokrok	k1gInSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
základem	základ	k1gInSc7
známá	známý	k2eAgFnSc1d1
formule	formule	k1gFnSc1
„	„	k?
<g/>
Publish	Publish	k1gInSc1
or	or	k?
perish	perish	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
přibližně	přibližně	k6eAd1
<g/>
:	:	kIx,
Publikuj	publikovat	k5eAaBmRp2nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
zahyň	zahynout	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
státní	státní	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
značně	značně	k6eAd1
expandovaly	expandovat	k5eAaImAgFnP
ve	v	k7c6
vysokoškolském	vysokoškolský	k2eAgNnSc6d1
pregraduálním	pregraduální	k2eAgNnSc6d1
vzdělávání	vzdělávání	k1gNnSc6
(	(	kIx(
<g/>
bakalářské	bakalářský	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
též	též	k9
značně	značně	k6eAd1
přidaly	přidat	k5eAaPmAgInP
programy	program	k1gInPc1
magisterské	magisterský	k2eAgInPc1d1
<g/>
,	,	kIx,
či	či	k8xC
doktorské	doktorský	k2eAgInPc4d1
<g/>
,	,	kIx,
orientované	orientovaný	k2eAgInPc4d1
na	na	k7c4
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
graduální	graduální	k2eAgFnSc1d1
<g/>
,	,	kIx,
či	či	k8xC
postgraduální	postgraduální	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
uvedených	uvedený	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
musí	muset	k5eAaImIp3nP
mít	mít	k5eAaImF
vhodný	vhodný	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
publikační	publikační	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
a	a	k8xC
výzkumných	výzkumný	k2eAgInPc6d1
grantech	grant	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
zásada	zásada	k1gFnSc1
„	„	k?
<g/>
Publish	Publish	k1gInSc1
or	or	k?
perish	perish	k1gInSc1
<g/>
“	“	k?
stala	stát	k5eAaPmAgFnS
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
důležitější	důležitý	k2eAgFnSc1d2
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
menších	malý	k2eAgFnPc6d2
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Ovšem	ovšem	k9
platy	plat	k1gInPc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
pracovníků	pracovník	k1gMnPc2
pak	pak	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
jistých	jistý	k2eAgInPc6d1
oborech	obor	k1gInPc6
menší	malý	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Požadavky	požadavek	k1gInPc1
</s>
<s>
Podrobné	podrobný	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
udělování	udělování	k1gNnSc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
školami	škola	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
pro	pro	k7c4
studenta	student	k1gMnSc4
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
absolvovat	absolvovat	k5eAaPmF
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
program	program	k1gInSc1
(	(	kIx(
<g/>
doktorský	doktorský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgMnS
kvalifikaci	kvalifikace	k1gFnSc4
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
té	ten	k3xDgFnSc3
magisterské	magisterský	k2eAgFnSc3d1
<g/>
,	,	kIx,
tedy	tedy	k9
kvalifikaci	kvalifikace	k1gFnSc4
úrovně	úroveň	k1gFnSc2
Master	master	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gFnSc7
(	(	kIx(
<g/>
magisterského	magisterský	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
7	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
případně	případně	k6eAd1
Honours	Honours	k1gInSc1
degree	degree	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
Master	master	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gFnSc7
lze	lze	k6eAd1
zpravidla	zpravidla	k6eAd1
získat	získat	k5eAaPmF
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
magisterském	magisterský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Honours	Honours	k1gInSc1
degree	degree	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
honorovaný	honorovaný	k2eAgMnSc1d1
bakalář	bakalář	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
lze	lze	k6eAd1
zpravidla	zpravidla	k6eAd1
získat	získat	k5eAaPmF
po	po	k7c6
ukončení	ukončení	k1gNnSc6
klasického	klasický	k2eAgInSc2d1
bakalářského	bakalářský	k2eAgInSc2d1
studijního	studijní	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
Bachelor	Bachelor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
tříletého	tříletý	k2eAgNnSc2d1
<g/>
,	,	kIx,
následovaného	následovaný	k2eAgNnSc2d1
roční	roční	k2eAgInSc1d1
nástavbou	nástavba	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
součástí	součást	k1gFnSc7
tohoto	tento	k3xDgInSc2
ročního	roční	k2eAgInSc2d1
„	„	k?
<g/>
honorovaného	honorovaný	k2eAgMnSc4d1
<g/>
“	“	k?
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
výzkum	výzkum	k1gInSc4
v	v	k7c6
konkrétním	konkrétní	k2eAgInSc6d1
oboru	obor	k1gInSc6
a	a	k8xC
závěrečná	závěrečný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mívá	mívat	k5eAaImIp3nS
většinou	většinou	k6eAd1
požadován	požadován	k2eAgInSc1d1
větší	veliký	k2eAgInSc1d2
rozsah	rozsah	k1gInSc1
než	než	k8xS
bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšnému	úspěšný	k2eAgInSc3d1
přijetí	přijetí	k1gNnSc4
k	k	k7c3
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
studiu	studio	k1gNnSc6
může	moct	k5eAaImIp3nS
často	často	k6eAd1
předcházet	předcházet	k5eAaImF
předložení	předložení	k1gNnSc4
kopií	kopie	k1gFnPc2
důležitých	důležitý	k2eAgFnPc2d1
akademických	akademický	k2eAgFnPc2d1
listin	listina	k1gFnPc2
<g/>
,	,	kIx,
doporučujících	doporučující	k2eAgInPc2d1
dopisů	dopis	k1gInPc2
<g/>
,	,	kIx,
často	často	k6eAd1
jsou	být	k5eAaImIp3nP
také	také	k9
požadovány	požadován	k2eAgFnPc1d1
zformulovaný	zformulovaný	k2eAgInSc1d1
záměr	záměr	k1gInSc1
výzkumu	výzkum	k1gInSc2
a	a	k8xC
osobní	osobní	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
univerzit	univerzita	k1gFnPc2
také	také	k9
před	před	k7c7
přijetím	přijetí	k1gNnSc7
do	do	k7c2
tohoto	tento	k3xDgInSc2
programu	program	k1gInSc2
zve	zvát	k5eAaImIp3nS
uchazeče	uchazeč	k1gMnPc4
na	na	k7c4
zvláštní	zvláštní	k2eAgInSc4d1
pohovor	pohovor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kandidát	kandidát	k1gMnSc1
musí	muset	k5eAaImIp3nS
předložit	předložit	k5eAaPmF
určitý	určitý	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
resp.	resp.	kA
disertaci	disertace	k1gFnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
skládající	skládající	k2eAgMnSc1d1
z	z	k7c2
původního	původní	k2eAgInSc2d1
akademického	akademický	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zásadě	zásada	k1gFnSc6
hoden	hoden	k2eAgInSc1d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
publikován	publikovat	k5eAaBmNgInS
v	v	k7c6
recenzovaném	recenzovaný	k2eAgInSc6d1
časopise	časopis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
musí	muset	k5eAaImIp3nP
kandidát	kandidát	k1gMnSc1
obhájit	obhájit	k5eAaPmF
tuto	tento	k3xDgFnSc4
práci	práce	k1gFnSc4
před	před	k7c7
komisí	komise	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
složena	složen	k2eAgFnSc1d1
z	z	k7c2
odborníků	odborník	k1gMnPc2
jmenovaných	jmenovaný	k2eAgFnPc2d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
disertace	disertace	k1gFnSc1
zkoumána	zkoumán	k2eAgFnSc1d1
komisí	komise	k1gFnSc7
odborníků	odborník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
stanovují	stanovovat	k5eAaImIp3nP
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
disertace	disertace	k1gFnSc1
v	v	k7c6
principu	princip	k1gInSc6
na	na	k7c6
odpovídající	odpovídající	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
a	a	k8xC
oznamují	oznamovat	k5eAaImIp3nP
případné	případný	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyřešit	vyřešit	k5eAaPmF
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
disertaci	disertace	k1gFnSc4
nechat	nechat	k5eAaPmF
projít	projít	k5eAaPmF
<g/>
,	,	kIx,
resp.	resp.	kA
ji	on	k3xPp3gFnSc4
schválit	schválit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
univerzity	univerzita	k1gFnPc1
v	v	k7c6
anglicky	anglicky	k6eAd1
nemluvících	mluvící	k2eNgFnPc6d1
zemích	zem	k1gFnPc6
začaly	začít	k5eAaPmAgFnP
přijímat	přijímat	k5eAaImF
podobné	podobný	k2eAgInPc4d1
standardy	standard	k1gInPc4
anglofonním	anglofonní	k2eAgInSc7d1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
programům	program	k1gInPc3
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
doktoráty	doktorát	k1gInPc4
orientované	orientovaný	k2eAgInPc4d1
na	na	k7c4
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
blíže	blízce	k6eAd2
Boloňský	boloňský	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
student	student	k1gMnSc1
či	či	k8xC
kandidát	kandidát	k1gMnSc1
obvykle	obvykle	k6eAd1
studuje	studovat	k5eAaImIp3nS
na	na	k7c6
akademické	akademický	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
pod	pod	k7c7
úzkým	úzký	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
<g/>
,	,	kIx,
supervizí	supervize	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
popularitou	popularita	k1gFnSc7
distančního	distanční	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
a	a	k8xC
technologií	technologie	k1gFnSc7
e-learningu	e-learning	k1gInSc2
některé	některý	k3yIgFnPc1
univerzity	univerzita	k1gFnPc1
nyní	nyní	k6eAd1
akceptují	akceptovat	k5eAaBmIp3nP
studenty	student	k1gMnPc4
zapsané	zapsaný	k2eAgNnSc1d1
do	do	k7c2
distančního	distanční	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
kombinovaného	kombinovaný	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vhodně	vhodně	k6eAd1
kombinuje	kombinovat	k5eAaImIp3nS
částečně	částečně	k6eAd1
prezenční	prezenční	k2eAgNnSc4d1
studium	studium	k1gNnSc4
a	a	k8xC
částečně	částečně	k6eAd1
distanční	distanční	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
částečný	částečný	k2eAgInSc4d1
úvazek	úvazek	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
programy	program	k1gInPc4
označované	označovaný	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
sendvičové	sendvičový	k2eAgNnSc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
sandwich	sandwich	k1gMnSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
značí	značit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
studenti	student	k1gMnPc1
netráví	trávit	k5eNaImIp3nP
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
studia	studio	k1gNnSc2
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k6eAd1
toto	tento	k3xDgNnSc1
stráví	strávit	k5eAaPmIp3nS
první	první	k4xOgFnSc1
a	a	k8xC
poslední	poslední	k2eAgNnPc1d1
období	období	k1gNnPc1
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
na	na	k7c6
svých	svůj	k3xOyFgFnPc6
domovských	domovský	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
a	a	k8xC
mezi	mezi	k7c7
tím	ten	k3xDgNnSc7
provádí	provádět	k5eAaImIp3nS
výzkum	výzkum	k1gInSc4
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Potvrzení	potvrzený	k2eAgMnPc1d1
PhD	PhD	k1gMnPc1
</s>
<s>
Potvrzení	potvrzený	k2eAgMnPc1d1
PhD	PhD	k1gMnPc1
(	(	kIx(
<g/>
pojednání	pojednání	k1gNnSc1
k	k	k7c3
disertační	disertační	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
,	,	kIx,
angl.	angl.	k?
PhD	PhD	k1gFnSc1
confirmation	confirmation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
předběžná	předběžný	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
či	či	k8xC
přednáška	přednáška	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
kandidát	kandidát	k1gMnSc1
na	na	k7c6
PhD	PhD	k1gFnSc6
prezentuje	prezentovat	k5eAaBmIp3nS
fakultě	fakulta	k1gFnSc3
<g/>
,	,	kIx,
respektive	respektive	k9
dalším	další	k2eAgMnPc3d1
zainteresovaným	zainteresovaný	k2eAgMnPc3d1
členům	člen	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednáška	přednáška	k1gFnSc1
následuje	následovat	k5eAaImIp3nS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
vhodné	vhodný	k2eAgNnSc1d1
téma	téma	k1gNnSc1
a	a	k8xC
může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
takové	takový	k3xDgFnPc4
náležitosti	náležitost	k1gFnPc4
jako	jako	k8xS,k8xC
účel	účel	k1gInSc4
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
použitou	použitý	k2eAgFnSc4d1
metodologii	metodologie	k1gFnSc4
<g/>
,	,	kIx,
předběžné	předběžný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
a	a	k8xC
očekávání	očekávání	k1gNnPc4
<g/>
,	,	kIx,
plánované	plánovaný	k2eAgFnPc4d1
nebo	nebo	k8xC
hotové	hotový	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
atp.	atp.	kA
</s>
<s>
Potvrzení	potvrzení	k1gNnSc1
přednášky	přednáška	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
shledáno	shledat	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
předběžná	předběžný	k2eAgFnSc1d1
zkušební	zkušební	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
obhajoba	obhajoba	k1gFnSc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
členové	člen	k1gMnPc1
fakulty	fakulta	k1gFnSc2
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
ještě	ještě	k6eAd1
stále	stále	k6eAd1
mohou	moct	k5eAaImIp3nP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
ovlivnit	ovlivnit	k5eAaPmF
směr	směr	k1gInSc4
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
přednášky	přednáška	k1gFnSc2
PhD	PhD	k1gMnSc1
kandidát	kandidát	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
shledán	shledat	k5eAaPmNgMnS
jako	jako	k9
potvrzený	potvrzený	k2eAgMnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
členové	člen	k1gMnPc1
fakulty	fakulta	k1gFnSc2
uvedené	uvedený	k2eAgFnSc2d1
schvalují	schvalovat	k5eAaImIp3nP
a	a	k8xC
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
studie	studie	k1gFnSc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
řízena	řízen	k2eAgFnSc1d1
a	a	k8xC
bude	být	k5eAaImBp3nS
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
končit	končit	k5eAaImF
u	u	k7c2
kandidáta	kandidát	k1gMnSc2
úspěchem	úspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
a	a	k8xC
kritika	kritika	k1gFnSc1
</s>
<s>
Práce	práce	k1gFnPc1
v	v	k7c6
univerzitním	univerzitní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
na	na	k7c6
akademické	akademický	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
obvykle	obvykle	k6eAd1
vyžaduje	vyžadovat	k5eAaImIp3nS
doktorát	doktorát	k1gInSc1
(	(	kIx(
<g/>
PhD	PhD	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
relativně	relativně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
i	i	k9
bez	bez	k7c2
doktorátu	doktorát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Severní	severní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
lze	lze	k6eAd1
obecně	obecně	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
profesoři	profesor	k1gMnPc1
doktorát	doktorát	k1gInSc4
(	(	kIx(
<g/>
PhD	PhD	k1gFnSc4
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
více	hodně	k6eAd2
povinni	povinen	k2eAgMnPc1d1
jej	on	k3xPp3gMnSc4
mít	mít	k5eAaImF
–	–	k?
uvedené	uvedený	k2eAgMnPc4d1
je	být	k5eAaImIp3nS
užíváno	užíván	k2eAgNnSc4d1
jakožto	jakožto	k8xS
určité	určitý	k2eAgNnSc4d1
měřítko	měřítko	k1gNnSc4
pro	pro	k7c4
hodnocení	hodnocení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Motivace	motivace	k1gFnSc1
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
PhD	PhD	k1gFnSc2
může	moct	k5eAaImIp3nS
též	též	k9
zahrnovat	zahrnovat	k5eAaImF
budoucí	budoucí	k2eAgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
toto	tento	k3xDgNnSc1
není	být	k5eNaImIp3nS
výsledkem	výsledek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernard	Bernard	k1gMnSc1
H.	H.	kA
Casey	Casea	k1gFnPc1
z	z	k7c2
University	universita	k1gFnSc2
of	of	k?
Warwick	Warwick	k1gInSc1
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
výzkumem	výzkum	k1gInSc7
naznačuje	naznačovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
napříč	napříč	k7c7
všemi	všecek	k3xTgInPc7
předměty	předmět	k1gInPc7
<g/>
,	,	kIx,
PhD	PhD	k1gFnPc7
poskytuje	poskytovat	k5eAaImIp3nS
navýšení	navýšení	k1gNnSc1
příjmu	příjem	k1gInSc2
o	o	k7c4
26	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
napříč	napříč	k7c7
všemi	všecek	k3xTgInPc7
obory	obor	k1gInPc7
<g/>
)	)	kIx)
–	–	k?
doktorské	doktorský	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
(	(	kIx(
<g/>
PhD	PhD	k1gFnSc1
<g/>
)	)	kIx)
tedy	tedy	k9
poskytuje	poskytovat	k5eAaImIp3nS
uvedené	uvedený	k2eAgFnPc4d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
přičemž	přičemž	k6eAd1
z	z	k7c2
tohoto	tento	k3xDgNnSc2
magisterské	magisterský	k2eAgFnSc2d1
poskytuje	poskytovat	k5eAaImIp3nS
23	#num#	k4
%	%	kIx~
a	a	k8xC
bakalářské	bakalářský	k2eAgInPc1d1
14	#num#	k4
%	%	kIx~
<g/>
;	;	kIx,
dále	daleko	k6eAd2
též	též	k9
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgFnPc1d1
benefity	benefita	k1gFnPc1
pro	pro	k7c4
společnost	společnost	k1gFnSc4
z	z	k7c2
tohoto	tento	k3xDgNnSc2
dalšího	další	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
či	či	k8xC
výcviku	výcvik	k1gInSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
z	z	k7c2
PhD	PhD	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ačkoli	ačkoli	k8xS
některé	některý	k3yIgInPc1
výzkumy	výzkum	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
překvalifikovaní	překvalifikovaný	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
méně	málo	k6eAd2
spokojení	spokojení	k1gNnSc2
a	a	k8xC
méně	málo	k6eAd2
produktivní	produktivní	k2eAgFnSc1d1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
obtíže	obtíž	k1gFnPc1
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
více	hodně	k6eAd2
pociťovány	pociťován	k2eAgInPc1d1
při	při	k7c6
hledání	hledání	k1gNnSc6
zaměstnání	zaměstnání	k1gNnSc2
absolventy	absolvent	k1gMnPc4
kupř	kupř	kA
<g/>
.	.	kIx.
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
studenti	student	k1gMnPc1
se	se	k3xPyFc4
často	často	k6eAd1
musejí	muset	k5eAaImIp3nP
zadlužit	zadlužit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
uvedené	uvedený	k2eAgInPc4d1
zdárně	zdárně	k6eAd1
dokončit	dokončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Řádný	řádný	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
v	v	k7c6
oboru	obor	k1gInSc6
(	(	kIx(
<g/>
PhD	PhD	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
také	také	k9
vyžadován	vyžadován	k2eAgInSc1d1
na	na	k7c6
některých	některý	k3yIgFnPc6
pozicích	pozice	k1gFnPc6
mimo	mimo	k7c4
akademickou	akademický	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
(	(	kIx(
<g/>
př	př	kA
<g/>
.	.	kIx.
farmacie	farmacie	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
nadnárodních	nadnárodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
atp.	atp.	kA
</s>
<s>
Systém	systém	k1gInSc1
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
USA	USA	kA
často	často	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
malou	malý	k2eAgFnSc4d1
pobídku	pobídka	k1gFnSc4
k	k	k7c3
přesunu	přesun	k1gInSc3
studentů	student	k1gMnPc2
v	v	k7c6
PhD	PhD	k1gFnPc6
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
ekvivalentního	ekvivalentní	k2eAgInSc2d1
Doctor	Doctor	k1gInSc4
of	of	k?
Arts	Artsa	k1gFnPc2
bylo	být	k5eAaImAgNnS
zkrátit	zkrátit	k5eAaPmF
čas	čas	k1gInSc4
potřebný	potřebný	k2eAgInSc4d1
ke	k	k7c3
zdárnému	zdárný	k2eAgNnSc3d1
dokončení	dokončení	k1gNnSc3
programu	program	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zaměřením	zaměření	k1gNnSc7
se	se	k3xPyFc4
na	na	k7c4
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
na	na	k7c4
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Doctor	Doctor	k1gInSc1
of	of	k?
Arts	Arts	k1gInSc4
programy	program	k1gInPc1
stále	stále	k6eAd1
též	též	k9
obsahují	obsahovat	k5eAaImIp3nP
určitou	určitý	k2eAgFnSc4d1
signifikantní	signifikantní	k2eAgFnSc4d1
výzkumovou	výzkumový	k2eAgFnSc4d1
složku	složka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Klást	klást	k5eAaImF
si	se	k3xPyFc3
ale	ale	k8xC
otázku	otázka	k1gFnSc4
<g/>
,	,	kIx,
„	„	k?
<g/>
zda	zda	k8xS
má	mít	k5eAaImIp3nS
titul	titul	k1gInSc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
<g/>
“	“	k?
je	být	k5eAaImIp3nS
podle	podle	k7c2
některých	některý	k3yIgMnPc2
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
srovnatelné	srovnatelný	k2eAgInPc4d1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kdybyste	kdyby	kYmCp2nP
se	se	k3xPyFc4
ptali	ptat	k5eAaImAgMnP
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
na	na	k7c6
světě	svět	k1gInSc6
příliš	příliš	k6eAd1
mnoho	mnoho	k4c4
umění	umění	k1gNnPc2
nebo	nebo	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademičtí	akademický	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
znalosti	znalost	k1gFnPc1
se	se	k3xPyFc4
z	z	k7c2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
šíří	šířit	k5eAaImIp3nP
do	do	k7c2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
stává	stávat	k5eAaImIp3nS
produktivnější	produktivní	k2eAgMnSc1d2
a	a	k8xC
zdravější	zdravý	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
bývá	bývat	k5eAaImIp3nS
u	u	k7c2
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
často	často	k6eAd1
kritizován	kritizovat	k5eAaImNgMnS
a	a	k8xC
uváděn	uvádět	k5eAaImNgInS
jako	jako	k9
problém	problém	k1gInSc1
též	též	k9
tzv.	tzv.	kA
inbreeding	inbreeding	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
lze	lze	k6eAd1
charakterizovat	charakterizovat	k5eAaBmF
například	například	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
student	student	k1gMnSc1
něco	něco	k3yInSc4
vystuduje	vystudovat	k5eAaPmIp3nS
<g/>
,	,	kIx,
udělá	udělat	k5eAaPmIp3nS
si	se	k3xPyFc3
pak	pak	k6eAd1
i	i	k9
doktorát	doktorát	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
v	v	k7c6
oboru	obor	k1gInSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
už	už	k6eAd1
na	na	k7c6
té	ten	k3xDgFnSc6
samé	samý	k3xTgFnSc6
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
popř.	popř.	kA
v	v	k7c6
tom	ten	k3xDgInSc6
samém	samý	k3xTgInSc6
oboru	obor	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
kupříkladu	kupříkladu	k6eAd1
i	i	k8xC
časem	časem	k6eAd1
nahradí	nahradit	k5eAaPmIp3nS
tohoto	tento	k3xDgMnSc2
svého	svůj	k1gMnSc2
profesora	profesor	k1gMnSc2
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
doktorát	doktorát	k1gInSc1
dělal	dělat	k5eAaImAgInS
<g/>
,	,	kIx,
rovněž	rovněž	k9
v	v	k7c6
tom	ten	k3xDgInSc6
samém	samý	k3xTgInSc6
oboru	obor	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
„	„	k?
<g/>
vytvářejí	vytvářet	k5eAaImIp3nP
uzavřené	uzavřený	k2eAgFnPc4d1
skupinky	skupinka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
si	se	k3xPyFc3
o	o	k7c4
sobě	se	k3xPyFc3
velmi	velmi	k6eAd1
často	často	k6eAd1
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
na	na	k7c6
světové	světový	k2eAgFnSc6d1
špici	špice	k1gFnSc6
<g/>
,	,	kIx,
byť	byť	k8xS
nejsou	být	k5eNaImIp3nP
ani	ani	k8xC
na	na	k7c6
té	ten	k3xDgFnSc6
regionální	regionální	k2eAgFnSc6d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
blíže	blízce	k6eAd2
<g/>
:	:	kIx,
Akademický	akademický	k2eAgInSc1d1
inbreeding	inbreeding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
</s>
<s>
V	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
v	v	k7c6
německy	německy	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
či	či	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
nástupnických	nástupnický	k2eAgInPc6d1
státech	stát	k1gInPc6
bývalého	bývalý	k2eAgInSc2d1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
většině	většina	k1gFnSc6
částí	část	k1gFnPc2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
v	v	k7c6
mnoha	mnoho	k4c6
španělsky	španělsky	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odpovídající	odpovídající	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
(	(	kIx(
<g/>
Doctor	Doctor	k1gInSc1
of	of	k?
Philosophy	Philosopha	k1gMnSc2
<g/>
)	)	kIx)
nazýván	nazývat	k5eAaImNgInS
zjednodušeně	zjednodušeně	k6eAd1
jen	jen	k6eAd1
„	„	k?
<g/>
Doctor	Doctor	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
doktor	doktor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
ISCED	ISCED	kA
<g/>
.	.	kIx.
</s>
<s>
Doktorát	doktorát	k1gInSc1
<g/>
,	,	kIx,
titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
označení	označení	k1gNnSc1
pro	pro	k7c4
doktora	doktor	k1gMnSc4
(	(	kIx(
<g/>
Dr	dr	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
dnes	dnes	k6eAd1
užívá	užívat	k5eAaImIp3nS
de	de	k?
facto	facto	k1gNnSc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
v	v	k7c6
různých	různý	k2eAgFnPc6d1
obdobách	obdoba	k1gFnPc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
charakter	charakter	k1gInSc1
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
též	též	k9
díky	díky	k7c3
Boloňskému	boloňský	k2eAgInSc3d1
procesu	proces	k1gInSc3
<g/>
,	,	kIx,
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
podobný	podobný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
vedoucí	vedoucí	k1gFnSc1
k	k	k7c3
doktorátu	doktorát	k1gInSc3
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
bývá	bývat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
orientováno	orientovat	k5eAaBmNgNnS
akademicky	akademicky	k6eAd1
a	a	k8xC
velmi	velmi	k6eAd1
individuálně	individuálně	k6eAd1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
například	například	k6eAd1
od	od	k7c2
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c4
doktorát	doktorát	k1gInSc4
se	se	k3xPyFc4
často	často	k6eAd1
chodí	chodit	k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
za	za	k7c7
osobností	osobnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Evropa	Evropa	k1gFnSc1
a	a	k8xC
svět	svět	k1gInSc1
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
lze	lze	k6eAd1
získat	získat	k5eAaPmF
nadnárodní	nadnárodní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
Evropský	evropský	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
(	(	kIx(
<g/>
Doctor	Doctor	k1gMnSc1
Europaeus	Europaeus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
nutností	nutnost	k1gFnSc7
pro	pro	k7c4
udělení	udělení	k1gNnSc4
tohoto	tento	k3xDgInSc2
titulu	titul	k1gInSc2
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
(	(	kIx(
<g/>
hovorově	hovorově	k6eAd1
také	také	k9
často	často	k6eAd1
PhD	PhD	k1gFnSc2
studium	studium	k1gNnSc1
nebo	nebo	k8xC
doktorandské	doktorandský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
aspirantské	aspirantský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
<g/>
,	,	kIx,
vědecká	vědecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
umělecká	umělecký	k2eAgFnSc1d1
<g/>
)	)	kIx)
aspirantura	aspirantura	k1gFnSc1
<g/>
,	,	kIx,
vědecká	vědecký	k2eAgFnSc1d1
výchova	výchova	k1gFnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
postgraduál	postgraduál	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
nebo	nebo	k8xC
dnes	dnes	k6eAd1
velký	velký	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
<g/>
)	)	kIx)
v	v	k7c6
trvání	trvání	k1gNnSc6
3-4	3-4	k4
let	léto	k1gNnPc2
podle	podle	k7c2
individuálního	individuální	k2eAgInSc2d1
studijního	studijní	k2eAgInSc2d1
plánu	plán	k1gInSc2
(	(	kIx(
<g/>
ISP	ISP	kA
<g/>
)	)	kIx)
pod	pod	k7c7
vedením	vedení	k1gNnSc7
školitele	školitel	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
dříve	dříve	k6eAd2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
nazývané	nazývaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1
studium	studium	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
zaměřeno	zaměřit	k5eAaPmNgNnS
na	na	k7c4
vědecké	vědecký	k2eAgNnSc4d1
bádání	bádání	k1gNnSc4
a	a	k8xC
samostatnou	samostatný	k2eAgFnSc4d1
tvůrčí	tvůrčí	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
výzkumu	výzkum	k1gInSc2
nebo	nebo	k8xC
vývoje	vývoj	k1gInSc2
nebo	nebo	k8xC
na	na	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
teoretickou	teoretický	k2eAgFnSc4d1
a	a	k8xC
tvůrčí	tvůrčí	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Tomuto	tento	k3xDgNnSc3
studiu	studio	k1gNnSc3
předchází	předcházet	k5eAaImIp3nS
vždy	vždy	k6eAd1
studium	studium	k1gNnSc4
magisterské	magisterský	k2eAgNnSc4d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
bývá	bývat	k5eAaImIp3nS
většinou	většinou	k6eAd1
pětileté	pětiletý	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
např.	např.	kA
pro	pro	k7c4
některé	některý	k3yIgInPc4
lékařské	lékařský	k2eAgInPc4d1
obory	obor	k1gInPc4
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
souvislé	souvislý	k2eAgNnSc4d1
šestileté	šestiletý	k2eAgNnSc4d1
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
MUDr.	MUDr.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
např.	např.	kA
uměleckých	umělecký	k2eAgInPc6d1
oborech	obor	k1gInPc6
jde	jít	k5eAaImIp3nS
zase	zase	k9
o	o	k7c4
souvislé	souvislý	k2eAgNnSc4d1
čtyřleté	čtyřletý	k2eAgNnSc4d1
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
MgA.	MgA.	k1gMnSc1
<g/>
)	)	kIx)
atd.	atd.	kA
PhD	PhD	k1gMnSc1
případně	případně	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
i	i	k9
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
ve	v	k7c6
světě	svět	k1gInSc6
a	a	k8xC
v	v	k7c6
Česku	Česko	k1gNnSc6
jej	on	k3xPp3gMnSc4
pak	pak	k6eAd1
i	i	k9
uznat	uznat	k5eAaPmF
<g/>
,	,	kIx,
tzv.	tzv.	kA
nostrifikace	nostrifikace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Absolvovaný	absolvovaný	k2eAgInSc1d1
poslední	poslední	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
požadován	požadovat	k5eAaImNgInS
pro	pro	k7c4
činnost	činnost	k1gFnSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
nadnárodních	nadnárodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
vědce	vědec	k1gMnSc4
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
též	též	k9
o	o	k7c4
nezbytný	nezbytný	k2eAgInSc4d1,k2eNgInSc4d1
požadavek	požadavek	k1gInSc4
pro	pro	k7c4
případné	případný	k2eAgNnSc4d1
pozdější	pozdní	k2eAgNnSc4d2
získaní	získaný	k2eAgMnPc1d1
doktorátu	doktorát	k1gInSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
bývá	bývat	k5eAaImIp3nS
klíčovým	klíčový	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
vysokoškolského	vysokoškolský	k2eAgMnSc4d1
učitele	učitel	k1gMnSc4
<g/>
,	,	kIx,
tedy	tedy	k9
pro	pro	k7c4
působení	působení	k1gNnSc4
jakožto	jakožto	k8xS
odborný	odborný	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
<g/>
,	,	kIx,
či	či	k8xC
následně	následně	k6eAd1
jako	jako	k9
docent	docent	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
habilitačního	habilitační	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
pozice	pozice	k1gFnSc2
associate	associat	k1gInSc5
professor	professora	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
později	pozdě	k6eAd2
jako	jako	k8xS,k8xC
profesor	profesor	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
řízení	řízení	k1gNnSc2
ke	k	k7c3
jmenování	jmenování	k1gNnSc3
profesorem	profesor	k1gMnSc7
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
pozice	pozice	k1gFnSc2
professor	professora	k1gFnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
pro	pro	k7c4
působení	působení	k1gNnSc4
v	v	k7c6
pozici	pozice	k1gFnSc6
lektora	lektor	k1gMnSc4
nebo	nebo	k8xC
asistenta	asistent	k1gMnSc4
není	být	k5eNaImIp3nS
potřeba	potřeba	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
odůvodněných	odůvodněný	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
výjimečně	výjimečně	k6eAd1
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
pro	pro	k7c4
pozdější	pozdní	k2eAgNnSc4d2
jmenování	jmenování	k1gNnSc4
docentem	docent	k1gMnSc7
<g/>
,	,	kIx,
či	či	k8xC
později	pozdě	k6eAd2
profesorem	profesor	k1gMnSc7
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
vyžadováno	vyžadován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
umělecké	umělecký	k2eAgInPc4d1
obory	obor	k1gInPc4
pak	pak	k6eAd1
zákon	zákon	k1gInSc1
připouští	připouštět	k5eAaImIp3nS
výjimku	výjimka	k1gFnSc4
a	a	k8xC
pro	pro	k7c4
jmenování	jmenování	k1gNnSc4
docentem	docent	k1gMnSc7
<g/>
,	,	kIx,
či	či	k8xC
později	pozdě	k6eAd2
profesorem	profesor	k1gMnSc7
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
vůbec	vůbec	k9
absolvování	absolvování	k1gNnSc2
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
nezbytně	zbytně	k6eNd1,k6eAd1
nutné	nutný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Docent	docent	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
vědecko-pedagogické	vědecko-pedagogický	k2eAgFnSc2d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
či	či	k8xC
umělecko-pedagogické	umělecko-pedagogický	k2eAgFnSc2d1
<g/>
)	)	kIx)
hodnosti	hodnost	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
splnit	splnit	k5eAaPmF
určité	určitý	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
publikační	publikační	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
činnost	činnost	k1gFnSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
uměleckou	umělecký	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
atp.	atp.	kA
(	(	kIx(
<g/>
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
další	další	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
/	/	kIx~
<g/>
vzdělání	vzdělání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Požadavky	požadavek	k1gInPc1
</s>
<s>
Náplní	náplň	k1gFnSc7
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
především	především	k9
výzkum	výzkum	k1gInSc1
předem	předem	k6eAd1
stanoveného	stanovený	k2eAgNnSc2d1
vědeckého	vědecký	k2eAgNnSc2d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
uměleckého	umělecký	k2eAgInSc2d1
<g/>
)	)	kIx)
úkolu	úkol	k1gInSc2
pod	pod	k7c7
individuálním	individuální	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
školitele	školitel	k1gMnSc2
a	a	k8xC
pod	pod	k7c7
dozorem	dozor	k1gInSc7
oborové	oborový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studovat	studovat	k5eAaImF
lze	lze	k6eAd1
dnes	dnes	k6eAd1
zpravidla	zpravidla	k6eAd1
v	v	k7c6
prezenční	prezenční	k2eAgFnSc6d1
či	či	k8xC
kombinované	kombinovaný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
formy	forma	k1gFnPc4
studia	studio	k1gNnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
Boloňského	boloňský	k2eAgInSc2d1
procesu	proces	k1gInSc2
rovnocenné	rovnocenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
dosažené	dosažený	k2eAgInPc1d1
tituly	titul	k1gInPc1
jsou	být	k5eAaImIp3nP
stejné	stejný	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
řádně	řádně	k6eAd1
zakončeno	zakončen	k2eAgNnSc1d1
složením	složení	k1gNnSc7
státní	státní	k2eAgFnSc2d1
doktorské	doktorský	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
a	a	k8xC
obhájením	obhájení	k1gNnSc7
disertační	disertační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
student	student	k1gMnSc1
doktorského	doktorský	k2eAgInSc2d1
programu	program	k1gInSc2
obhajuje	obhajovat	k5eAaImIp3nS
kvalitu	kvalita	k1gFnSc4
svého	svůj	k3xOyFgInSc2
výzkumu	výzkum	k1gInSc2
nebo	nebo	k8xC
umělecké	umělecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
;	;	kIx,
obhajobou	obhajoba	k1gFnSc7
se	se	k3xPyFc4
také	také	k9
prokazuje	prokazovat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
a	a	k8xC
připravenost	připravenost	k1gFnSc4
k	k	k7c3
samostatné	samostatný	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
výzkumu	výzkum	k1gInSc2
nebo	nebo	k8xC
vývoje	vývoj	k1gInSc2
nebo	nebo	k8xC
k	k	k7c3
samostatné	samostatný	k2eAgFnSc3d1
teoretické	teoretický	k2eAgFnPc4d1
a	a	k8xC
tvůrčí	tvůrčí	k2eAgFnPc4d1
umělecké	umělecký	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disertační	disertační	k2eAgFnSc1d1
práce	práce	k1gFnSc1
musí	muset	k5eAaImIp3nS
dle	dle	k7c2
českého	český	k2eAgInSc2d1
vysokoškolského	vysokoškolský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
obsahovat	obsahovat	k5eAaImF
„	„	k?
<g/>
původní	původní	k2eAgInSc4d1
a	a	k8xC
uveřejněné	uveřejněný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
nebo	nebo	k8xC
výsledky	výsledek	k1gInPc4
přijaté	přijatý	k2eAgFnSc2d1
k	k	k7c3
uveřejnění	uveřejnění	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Školitel	školitel	k1gMnSc1
není	být	k5eNaImIp3nS
pouhým	pouhý	k2eAgMnSc7d1
vedoucím	vedoucí	k1gMnSc7
závěrečné	závěrečný	k2eAgFnSc2d1
kvalifikační	kvalifikační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
doktoranda	doktorand	k1gMnSc2
(	(	kIx(
<g/>
disertační	disertační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
dozoruje	dozorovat	k5eAaImIp3nS
a	a	k8xC
vede	vést	k5eAaImIp3nS
celé	celý	k2eAgNnSc1d1
jeho	jeho	k3xOp3gNnSc4
studium	studium	k1gNnSc4
<g/>
,	,	kIx,
odbornou	odborný	k2eAgFnSc4d1
a	a	k8xC
vědeckou	vědecký	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
doktoranda	doktorand	k1gMnSc2
<g/>
,	,	kIx,
sestavuje	sestavovat	k5eAaImIp3nS
se	s	k7c7
studentem	student	k1gMnSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
ISP	ISP	kA
<g/>
,	,	kIx,
předkládá	předkládat	k5eAaImIp3nS
předsedovi	předseda	k1gMnSc3
oborové	oborový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roční	roční	k2eAgNnSc4d1
hodnocení	hodnocení	k1gNnSc4
doktoranda	doktorand	k1gMnSc2
<g/>
,	,	kIx,
navrhuje	navrhovat	k5eAaImIp3nS
děkanovi	děkan	k1gMnSc3
např.	např.	kA
zahraniční	zahraniční	k2eAgFnSc2d1
stáže	stáž	k1gFnSc2
doktoranda	doktorand	k1gMnSc2
atd.	atd.	kA
Školitelem	školitel	k1gMnSc7
bývá	bývat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
docent	docent	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
to	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
též	též	k9
uznávaný	uznávaný	k2eAgMnSc1d1
odborník	odborník	k1gMnSc1
<g/>
,	,	kIx,
například	například	k6eAd1
doktor	doktor	k1gMnSc1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
zde	zde	k6eAd1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
jako	jako	k8xC,k8xS
základ	základ	k1gInSc4
vyučována	vyučován	k2eAgFnSc1d1
metodologie	metodologie	k1gFnSc1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
zásady	zásada	k1gFnPc4
vědecké	vědecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
odborné	odborný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
dle	dle	k7c2
specializace	specializace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kredity	kredit	k1gInPc7
se	se	k3xPyFc4
dále	daleko	k6eAd2
udílejí	udílet	k5eAaImIp3nP
za	za	k7c4
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
absolvované	absolvovaný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
výstupy	výstup	k1gInPc4
na	na	k7c6
vědeckých	vědecký	k2eAgFnPc6d1
konferencích	konference	k1gFnPc6
<g/>
,	,	kIx,
absolvované	absolvovaný	k2eAgFnPc1d1
zahraniční	zahraniční	k2eAgFnPc4d1
stáže	stáž	k1gFnPc4
<g/>
,	,	kIx,
publikované	publikovaný	k2eAgInPc4d1
články	článek	k1gInPc4
atd.	atd.	kA
Doktorand	doktorand	k1gMnSc1
má	mít	k5eAaImIp3nS
často	často	k6eAd1
předepsán	předepsat	k5eAaPmNgInS
určitý	určitý	k2eAgInSc1d1
počet	počet	k1gInSc1
hodin	hodina	k1gFnPc2
pro	pro	k7c4
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
(	(	kIx(
<g/>
přímou	přímý	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
<g/>
,	,	kIx,
semináře	seminář	k1gInPc1
<g/>
,	,	kIx,
resp.	resp.	kA
cvičení	cvičení	k1gNnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
konzultace	konzultace	k1gFnPc4
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
programu	program	k1gInSc6
sleduje	sledovat	k5eAaImIp3nS
a	a	k8xC
hodnotí	hodnotit	k5eAaImIp3nS
oborová	oborový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ustavená	ustavený	k2eAgFnSc1d1
podle	podle	k7c2
vnitřního	vnitřní	k2eAgInSc2d1
předpisu	předpis	k1gInSc2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
nebo	nebo	k8xC
její	její	k3xOp3gFnSc2
součásti	součást	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
akreditovaný	akreditovaný	k2eAgInSc4d1
příslušný	příslušný	k2eAgInSc4d1
doktorský	doktorský	k2eAgInSc4d1
studijní	studijní	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
studia	studio	k1gNnSc2
mohou	moct	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnPc1
součásti	součást	k1gFnPc1
na	na	k7c6
základě	základ	k1gInSc6
dohody	dohoda	k1gFnSc2
vytvořit	vytvořit	k5eAaPmF
společnou	společný	k2eAgFnSc4d1
oborovou	oborový	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedu	předseda	k1gMnSc4
oborové	oborový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
volí	volit	k5eAaImIp3nS
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
středu	středa	k1gFnSc4
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
předsedou	předseda	k1gMnSc7
oborové	oborový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
je	být	k5eAaImIp3nS
garant	garant	k1gMnSc1
doktorského	doktorský	k2eAgInSc2d1
studijního	studijní	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doktorand	doktorand	k1gMnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
svého	svůj	k3xOyFgMnSc2
školitele	školitel	k1gMnSc2
veřejně	veřejně	k6eAd1
publikovat	publikovat	k5eAaBmF
své	svůj	k3xOyFgInPc4
odborné	odborný	k2eAgInPc4d1
články	článek	k1gInPc4
<g/>
,	,	kIx,
studie	studie	k1gFnPc4
<g/>
,	,	kIx,
navštěvovat	navštěvovat	k5eAaImF
vědecké	vědecký	k2eAgFnPc4d1
konference	konference	k1gFnPc4
<g/>
,	,	kIx,
vést	vést	k5eAaImF
či	či	k8xC
oponovat	oponovat	k5eAaImF
bakalářské	bakalářský	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
vyšších	vysoký	k2eAgInPc6d2
ročnících	ročník	k1gInPc6
případně	případně	k6eAd1
někdy	někdy	k6eAd1
též	též	k9
magisterské	magisterský	k2eAgFnSc2d1
(	(	kIx(
<g/>
diplomové	diplomový	k2eAgFnSc2d1
<g/>
)	)	kIx)
práce	práce	k1gFnSc2
apod.	apod.	kA
Po	po	k7c6
absolutoriu	absolutorium	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
akademickém	akademický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
často	často	k6eAd1
hovorově	hovorův	k2eAgNnSc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
tzv.	tzv.	kA
postdoktorand	postdoktorand	k1gInSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
postdoc	postdoc	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
dotyčný	dotyčný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
způsobilý	způsobilý	k2eAgInSc1d1
pro	pro	k7c4
vykonávání	vykonávání	k1gNnSc4
činnosti	činnost	k1gFnSc2
odborného	odborný	k2eAgMnSc2d1
asistenta	asistent	k1gMnSc2
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
(	(	kIx(
<g/>
pracovně	pracovně	k6eAd1
někdy	někdy	k6eAd1
zkracováno	zkracován	k2eAgNnSc4d1
odb	odb	k?
<g/>
.	.	kIx.
as	as	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
pozice	pozice	k1gFnSc2
assistant	assistant	k1gMnSc1
professor	professor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
vědeckého	vědecký	k2eAgMnSc4d1
pracovníka	pracovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Předchozí	předchozí	k2eAgFnPc1d1
obdoby	obdoba	k1gFnPc1
vědeckých	vědecký	k2eAgInPc2d1
titulů	titul	k1gInPc2
</s>
<s>
Titul	titul	k1gInSc1
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
pro	pro	k7c4
absolventy	absolvent	k1gMnPc4
doktorských	doktorský	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
ČR	ČR	kA
zaveden	zaveden	k2eAgInSc1d1
roku	rok	k1gInSc2
1998	#num#	k4
zákonem	zákon	k1gInSc7
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
po	po	k7c6
vzoru	vzor	k1gInSc6
anglosaského	anglosaský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předchozím	předchozí	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1990	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
obdobou	obdoba	k1gFnSc7
stejný	stejný	k2eAgInSc1d1
doktorský	doktorský	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
Dr	dr	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
dle	dle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c4
standardně	standardně	k6eAd1
tříleté	tříletý	k2eAgNnSc4d1
„	„	k?
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1
studium	studium	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
o	o	k7c4
standardně	standardně	k6eAd1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
„	„	k?
<g/>
doktorský	doktorský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předcházejícím	předcházející	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1953	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
jim	on	k3xPp3gMnPc3
po	po	k7c6
vzoru	vzor	k1gInSc6
SSSR	SSSR	kA
předcházel	předcházet	k5eAaImAgInS
titul	titul	k1gInSc4
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
vědecká	vědecký	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
kandidát	kandidát	k1gMnSc1
věd	věda	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
CSc.	CSc.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
de	de	k?
facto	facto	k1gNnSc1
o	o	k7c4
ekvivalenty	ekvivalent	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CSc.	CSc.	kA
byla	být	k5eAaImAgFnS
přesněji	přesně	k6eAd2
dříve	dříve	k6eAd2
„	„	k?
<g/>
nižší	nízký	k2eAgFnSc1d2
vědecká	vědecký	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
udělovala	udělovat	k5eAaImAgFnS
se	se	k3xPyFc4
ještě	ještě	k9
též	též	k9
„	„	k?
<g/>
vyšší	vysoký	k2eAgFnSc1d2
vědecká	vědecký	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
<g/>
“	“	k?
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
věd	věda	k1gFnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
variantu	varianta	k1gFnSc4
dnes	dnes	k6eAd1
právní	právní	k2eAgInSc1d1
řád	řád	k1gInSc1
ČR	ČR	kA
(	(	kIx(
<g/>
vysokoškolský	vysokoškolský	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
nezná	neznat	k5eAaImIp3nS,k5eNaImIp3nS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
dnes	dnes	k6eAd1
uděluje	udělovat	k5eAaImIp3nS
DSc	DSc	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
věd	věda	k1gFnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
jeho	jeho	k3xOp3gFnSc4
určitou	určitý	k2eAgFnSc4d1
obdobu	obdoba	k1gFnSc4
na	na	k7c6
základě	základ	k1gInSc6
vlastních	vlastní	k2eAgFnPc2d1
stanov	stanova	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Doktorský	doktorský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
akreditována	akreditovat	k5eAaBmNgFnS
pouze	pouze	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
dnes	dnes	k6eAd1
mohou	moct	k5eAaImIp3nP
udělovat	udělovat	k5eAaImF
pouze	pouze	k6eAd1
tyto	tento	k3xDgFnPc4
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
instituce	instituce	k1gFnPc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
univerzitního	univerzitní	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
ČR	ČR	kA
typicky	typicky	k6eAd1
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Titul	titul	k1gInSc1
„	„	k?
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
“	“	k?
a	a	k8xC
ostatní	ostatní	k2eAgNnSc4d1
„	„	k?
<g/>
doktorské	doktorský	k2eAgFnSc2d1
<g/>
“	“	k?
tituly	titul	k1gInPc1
</s>
<s>
Titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
v	v	k7c6
ČR	ČR	kA
hovorově	hovorově	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k9
tzv.	tzv.	kA
velký	velký	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nS
se	se	k3xPyFc4
odlišil	odlišit	k5eAaPmAgInS
od	od	k7c2
tzv.	tzv.	kA
malého	malý	k2eAgInSc2d1
doktorátu	doktorát	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
uděluje	udělovat	k5eAaImIp3nS
po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
absolvování	absolvování	k1gNnSc6
rigorózní	rigorózní	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
(	(	kIx(
<g/>
PhDr.	PhDr.	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
ThDr.	ThDr.	k1gFnSc1
<g/>
,	,	kIx,
JUDr.	JUDr.	kA
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Doktorský	doktorský	k2eAgInSc1d1
titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
8	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
není	být	k5eNaImIp3nS
ekvivalentem	ekvivalent	k1gInSc7
titulu	titul	k1gInSc2
PhDr.	PhDr.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
titul	titul	k1gInSc4
magisterského	magisterský	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
(	(	kIx(
<g/>
7	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
filozofie	filozofie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
značí	značit	k5eAaImIp3nS
de	de	k?
facto	facto	k1gNnSc1
stejnou	stejný	k2eAgFnSc4d1
kvalifikaci	kvalifikace	k1gFnSc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
magistr	magistr	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Mgr.	Mgr.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Malé	Malé	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
rigorózum	rigorózum	k1gNnSc1
<g/>
)	)	kIx)
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
v	v	k7c6
současnosti	současnost	k1gFnSc6
označovány	označovat	k5eAaImNgFnP
jako	jako	k9
historická	historický	k2eAgFnSc1d1
záležitost	záležitost	k1gFnSc1
či	či	k8xC
specifikum	specifikum	k1gNnSc1
Česka	Česko	k1gNnSc2
či	či	k8xC
Slovenska	Slovensko	k1gNnSc2
z	z	k7c2
minulosti	minulost	k1gFnSc2
a	a	k8xC
zpravidla	zpravidla	k6eAd1
nemají	mít	k5eNaImIp3nP
ve	v	k7c6
světě	svět	k1gInSc6
obdobu	obdoba	k1gFnSc4
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gNnSc2
udělování	udělování	k1gNnSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
jejich	jejich	k3xOp3gFnSc1
existence	existence	k1gFnSc1
<g/>
,	,	kIx,
kupř	kupř	kA
<g/>
.	.	kIx.
srov.	srov.	kA
J.D.	J.D.	k1gFnSc2
a	a	k8xC
JUDr.	JUDr.	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
39	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
totiž	totiž	k9
v	v	k7c6
tomto	tento	k3xDgNnSc6
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
žádný	žádný	k3yNgInSc4
další	další	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
získaného	získaný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
–	–	k?
Boloňský	boloňský	k2eAgInSc1d1
proces	proces	k1gInSc1
chápe	chápat	k5eAaImIp3nS
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc4
klasické	klasický	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
:	:	kIx,
bakalářské	bakalářský	k2eAgFnPc1d1
–	–	k?
magisterské	magisterský	k2eAgFnPc1d1
–	–	k?
doktorské	doktorský	k2eAgFnPc1d1
(	(	kIx(
<g/>
blíže	blízce	k6eAd2
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Např.	např.	kA
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
kupříkladu	kupříkladu	k6eAd1
soudce	soudce	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
titulem	titul	k1gInSc7
JUDr.	JUDr.	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
od	od	k7c2
právníka	právník	k1gMnSc2
s	s	k7c7
titulem	titul	k1gInSc7
Mgr.	Mgr.	kA
de	de	k?
facto	facto	k1gNnSc4
nijak	nijak	k6eAd1
neliší	lišit	k5eNaImIp3nP
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
mají	mít	k5eAaImIp3nP
stejné	stejný	k2eAgNnSc4d1
pětileté	pětiletý	k2eAgNnSc4d1
magisterské	magisterský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
právník	právník	k1gMnSc1
s	s	k7c7
titulem	titul	k1gInSc7
„	„	k?
<g/>
magistr	magistr	k1gMnSc1
<g/>
“	“	k?
ukončil	ukončit	k5eAaPmAgMnS
studium	studium	k1gNnSc4
státní	státní	k2eAgFnSc7d1
závěrečnou	závěrečný	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
hovorově	hovorově	k6eAd1
označovanou	označovaný	k2eAgFnSc4d1
jako	jako	k9
státnice	státnice	k1gFnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
„	„	k?
<g/>
doktor	doktor	k1gMnSc1
práv	právo	k1gNnPc2
<g/>
“	“	k?
se	se	k3xPyFc4
po	po	k7c6
získání	získání	k1gNnSc6
Mgr.	Mgr.	kA
rozhodl	rozhodnout	k5eAaPmAgInS
následně	následně	k6eAd1
složit	složit	k5eAaPmF
ještě	ještě	k9
jednu	jeden	k4xCgFnSc4
zkoušku	zkouška	k1gFnSc4
navíc	navíc	k6eAd1
<g/>
,	,	kIx,
tzv.	tzv.	kA
rigorózní	rigorózní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
označovanou	označovaný	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
rigo	rigo	k6eAd1
<g/>
,	,	kIx,
součástí	součást	k1gFnSc7
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bývá	bývat	k5eAaImIp3nS
i	i	k9
obhajoba	obhajoba	k1gFnSc1
rigorózní	rigorózní	k2eAgFnSc2d1
práce	práce	k1gFnSc2
–	–	k?
může	moct	k5eAaImIp3nS
však	však	k9
být	být	k5eAaImF
uznána	uznán	k2eAgFnSc1d1
i	i	k8xC
stejná	stejný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
jako	jako	k8xS,k8xC
magisterská	magisterský	k2eAgFnSc1d1
(	(	kIx(
<g/>
diplomová	diplomový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
rozšířená	rozšířený	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
to	ten	k3xDgNnSc1
připouští	připouštět	k5eAaImIp3nS
vnitřní	vnitřní	k2eAgInSc4d1
předpis	předpis	k1gInSc4
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgMnSc1
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
soudce	soudce	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
však	však	k9
stále	stále	k6eAd1
zcela	zcela	k6eAd1
stejné	stejný	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
resp.	resp.	kA
kompetence	kompetence	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rigorózum	rigorózum	k1gNnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
fakultativní	fakultativní	k2eAgFnSc1d1
<g/>
)	)	kIx)
státní	státní	k2eAgFnSc1d1
rigorózní	rigorózní	k2eAgFnSc1d1
zkouška	zkouška	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
v	v	k7c6
ČR	ČR	kA
zpoplatněnou	zpoplatněný	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
–	–	k?
poplatky	poplatek	k1gInPc7
s	s	k7c7
tímto	tento	k3xDgNnSc7
spojené	spojený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
příjmem	příjem	k1gInSc7
dané	daný	k2eAgFnSc2d1
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
–	–	k?
mimo	mimo	k6eAd1
lékařských	lékařský	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
MUDr.	MUDr.	kA
<g/>
,	,	kIx,
MDDr	MDDr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
MVDr.	MVDr.	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
standardní	standardní	k2eAgNnSc4d1
zakončení	zakončení	k1gNnSc4
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1
doktorand	doktorand	k1gMnSc1
je	být	k5eAaImIp3nS
studentem	student	k1gMnSc7
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
,	,	kIx,
primárně	primárně	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
dané	daný	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
studenta	student	k1gMnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dle	dle	k7c2
formy	forma	k1gFnSc2
studia	studio	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
vnitřně	vnitřně	k6eAd1
(	(	kIx(
<g/>
pracovně	pracovně	k6eAd1
<g/>
)	)	kIx)
rozdělují	rozdělovat	k5eAaImIp3nP
na	na	k7c4
interní	interní	k2eAgMnPc4d1
a	a	k8xC
externí	externí	k2eAgMnPc4d1
doktorandy	doktorand	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
doktorandů	doktorand	k1gMnPc2
je	být	k5eAaImIp3nS
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
podobné	podobný	k2eAgFnSc2d1
asistentům	asistent	k1gMnPc3
(	(	kIx(
<g/>
pracovně	pracovně	k6eAd1
někdy	někdy	k6eAd1
zkracováno	zkracován	k2eAgNnSc1d1
as	as	k1gNnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
zaměstnanci	zaměstnanec	k1gMnPc1
a	a	k8xC
pobírají	pobírat	k5eAaImIp3nP
plat	plat	k1gInSc4
(	(	kIx(
<g/>
resp.	resp.	kA
mzdu	mzda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktorand	doktorand	k1gMnSc1
v	v	k7c6
prezenční	prezenční	k2eAgFnSc6d1
formě	forma	k1gFnSc6
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
interní	interní	k2eAgMnSc1d1
doktorand	doktorand	k1gMnSc1
<g/>
)	)	kIx)
většinou	většinou	k6eAd1
pobírá	pobírat	k5eAaImIp3nS
stipendium	stipendium	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
většinou	většina	k1gFnSc7
o	o	k7c4
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
šest	šest	k4xCc1
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktorandi	doktorand	k1gMnPc1
také	také	k9
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
pracovní	pracovní	k2eAgInSc4d1
úvazek	úvazek	k1gInSc4
na	na	k7c4
pozici	pozice	k1gFnSc4
asistenta	asistent	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
k	k	k7c3
tomuto	tento	k3xDgInSc3
financováni	financován	k2eAgMnPc1d1
z	z	k7c2
příslušného	příslušný	k2eAgInSc2d1
grantu	grant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interní	interní	k2eAgInSc1d1
doktorandy	doktorand	k1gMnPc4
pak	pak	k6eAd1
stát	stát	k5eAaImF,k5eAaPmF
také	také	k9
většinou	většina	k1gFnSc7
různě	různě	k6eAd1
zvýhodňuje	zvýhodňovat	k5eAaImIp3nS
či	či	k8xC
podporuje	podporovat	k5eAaImIp3nS
–	–	k?
v	v	k7c6
ČR	ČR	kA
stát	stát	k1gInSc1
považuje	považovat	k5eAaImIp3nS
doktoranda	doktorand	k1gMnSc4
za	za	k7c4
studenta	student	k1gMnSc4
nikoliv	nikoliv	k9
max	max	kA
<g/>
.	.	kIx.
pouze	pouze	k6eAd1
do	do	k7c2
26	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
standardně	standardně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
některé	některý	k3yIgMnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
daňové	daňový	k2eAgNnSc4d1
<g/>
,	,	kIx,
účely	účel	k1gInPc4
až	až	k9
do	do	k7c2
dovršení	dovršení	k1gNnSc2
28	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
struktura	struktura	k1gFnSc1
studentů	student	k1gMnPc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
následující	následující	k2eAgFnSc1d1
<g/>
:	:	kIx,
kolem	kolem	k7c2
60	#num#	k4
%	%	kIx~
studentů	student	k1gMnPc2
studuje	studovat	k5eAaImIp3nS
první	první	k4xOgInSc4
cyklus	cyklus	k1gInSc4
vysokoškolského	vysokoškolský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
bakalářský	bakalářský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
třetina	třetina	k1gFnSc1
studentů	student	k1gMnPc2
studuje	studovat	k5eAaImIp3nS
ve	v	k7c6
druhém	druhý	k4xOgInSc6
cyklu	cyklus	k1gInSc6
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
magisterský	magisterský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
právě	právě	k6eAd1
třetí	třetí	k4xOgInSc1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
doktorský	doktorský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
)	)	kIx)
zde	zde	k6eAd1
studuje	studovat	k5eAaImIp3nS
stabilně	stabilně	k6eAd1
kolem	kolem	k7c2
6-8	6-8	k4
%	%	kIx~
všech	všecek	k3xTgMnPc2
studentů	student	k1gMnPc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tento	tento	k3xDgInSc4
cyklus	cyklus	k1gInSc4
v	v	k7c6
ČR	ČR	kA
nedokončí	dokončit	k5eNaPmIp3nS
více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
zapsaných	zapsaný	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
ještě	ještě	k9
vyšší	vysoký	k2eAgFnSc4d2
neúspěšnost	neúspěšnost	k1gFnSc4
než	než	k8xS
u	u	k7c2
studentů	student	k1gMnPc2
cyklu	cyklus	k1gInSc2
prvního	první	k4xOgNnSc2
<g/>
;	;	kIx,
uvedené	uvedený	k2eAgNnSc1d1
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
souviset	souviset	k5eAaImF
též	též	k9
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
ČR	ČR	kA
mají	mít	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
studenti	student	k1gMnPc1
třetího	třetí	k4xOgInSc2
cyklu	cyklus	k1gInSc2
od	od	k7c2
svého	svůj	k3xOyFgInSc2
zápisu	zápis	k1gInSc2
ke	k	k7c3
studiu	studio	k1gNnSc3
nárok	nárok	k1gInSc4
na	na	k7c4
měsíční	měsíční	k2eAgNnSc4d1
doktorské	doktorský	k2eAgNnSc4d1
stipendium	stipendium	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
studenty	student	k1gMnPc4
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
tak	tak	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
výhodné	výhodný	k2eAgFnPc1d1
se	se	k3xPyFc4
přihlásit	přihlásit	k5eAaPmF
ke	k	k7c3
studiu	studio	k1gNnSc3
doktorskému	doktorský	k2eAgNnSc3d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ať	ať	k8xS,k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
už	už	k6eAd1
jej	on	k3xPp3gMnSc4
skutečně	skutečně	k6eAd1
studovat	studovat	k5eAaImF
hodlají	hodlat	k5eAaImIp3nP
<g/>
,	,	kIx,
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Druhou	druhý	k4xOgFnSc7
příčinou	příčina	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ekonomická	ekonomický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
těchto	tento	k3xDgMnPc2
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
stipendium	stipendium	k1gNnSc4
do	do	k7c2
10	#num#	k4
000	#num#	k4
Kč	Kč	kA
nepokryje	pokrýt	k5eNaPmIp3nS
životními	životní	k2eAgInPc7d1
náklady	náklad	k1gInPc7
již	již	k6eAd1
dospělých	dospělý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mívají	mívat	k5eAaImIp3nP
často	často	k6eAd1
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
či	či	k8xC
vlastní	vlastní	k2eAgNnSc4d1
bydlení	bydlení	k1gNnSc4
apod.	apod.	kA
<g/>
.	.	kIx.
</s>
<s>
Modely	model	k1gInPc1
supervize	supervize	k1gFnSc2
</s>
<s>
Na	na	k7c6
některých	některý	k3yIgFnPc6
univerzitách	univerzita	k1gFnPc6
může	moct	k5eAaImIp3nS
existovat	existovat	k5eAaImF
školení	školení	k1gNnPc1
pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
chtějí	chtít	k5eAaImIp3nP
dohlížet	dohlížet	k5eAaImF
na	na	k7c4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
literatury	literatura	k1gFnSc2
pro	pro	k7c4
akademiky	akademik	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
toto	tento	k3xDgNnSc4
chtějí	chtít	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinham	Dinham	k1gInSc4
a	a	k8xC
Scott	Scott	k1gInSc4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
argumentovali	argumentovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
celosvětový	celosvětový	k2eAgInSc1d1
růst	růst	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
studentů	student	k1gMnPc2
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
byl	být	k5eAaImAgInS
doprovázen	doprovázet	k5eAaImNgInS
zvýšením	zvýšení	k1gNnSc7
řady	řada	k1gFnSc2
návodů	návod	k1gInPc2
<g/>
/	/	kIx~
<g/>
manuálů	manuál	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
pro	pro	k7c4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
školitele	školitel	k1gMnPc4
<g/>
,	,	kIx,
citují	citovat	k5eAaBmIp3nP
přitom	přitom	k6eAd1
příklady	příklad	k1gInPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Pugh	Pugh	k1gInSc1
a	a	k8xC
Phillips	Phillips	k1gInSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
autoři	autor	k1gMnPc1
publikovali	publikovat	k5eAaBmAgMnP
empirická	empirický	k2eAgNnPc4d1
data	datum	k1gNnPc4
o	o	k7c6
přínosech	přínos	k1gInPc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
kandidát	kandidát	k1gMnSc1
může	moct	k5eAaImIp3nS
získat	získat	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
publikuje	publikovat	k5eAaBmIp3nS
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
poznamenávají	poznamenávat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
studenti	student	k1gMnPc1
jsou	být	k5eAaImIp3nP
tomu	ten	k3xDgNnSc3
spíše	spíše	k9
nakloněni	naklonit	k5eAaPmNgMnP
s	s	k7c7
adekvátní	adekvátní	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
školitelů	školitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Wisker	Wisker	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
výzkum	výzkum	k1gInSc1
v	v	k7c6
uvedených	uvedený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
rozlišuje	rozlišovat	k5eAaImIp3nS
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
modely	model	k1gInPc7
dohledu	dohled	k1gInSc2
(	(	kIx(
<g/>
supervize	supervize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
první	první	k4xOgInSc1
model	model	k1gInSc1
supervize	supervize	k1gFnSc2
(	(	kIx(
<g/>
technical-rationality	technical-rationalita	k1gFnSc2
model	model	k1gInSc1
<g/>
)	)	kIx)
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
model	model	k1gInSc4
(	(	kIx(
<g/>
negotiated	negotiated	k1gInSc1
order	order	k1gMnSc1
model	model	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
mechanistický	mechanistický	k2eAgMnSc1d1
a	a	k8xC
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
plynulou	plynulý	k2eAgFnSc4d1
a	a	k8xC
dynamickou	dynamický	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
literatury	literatura	k1gFnSc2
ohledně	ohledně	k7c2
očekávání	očekávání	k1gNnSc2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
školitelé	školitel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
své	svůj	k3xOyFgMnPc4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
několik	několik	k4yIc1
doktorandů	doktorand	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
studenti	student	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
své	svůj	k3xOyFgMnPc4
školitele	školitel	k1gMnPc4
<g/>
,	,	kIx,
tedy	tedy	k9
nikoli	nikoli	k9
jednoho	jeden	k4xCgMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
školitele	školitel	k1gMnSc4
pro	pro	k7c4
každý	každý	k3xTgInSc4
kurs	kurs	k1gInSc4
zvlášť	zvlášť	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Některé	některý	k3yIgFnPc4
zajímavosti	zajímavost	k1gFnPc4
pouze	pouze	k6eAd1
z	z	k7c2
ČR	ČR	kA
uvádí	uvádět	k5eAaImIp3nS
například	například	k6eAd1
pramen	pramen	k1gInSc4
Učitelské	učitelský	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stipendium	stipendium	k1gNnSc1
(	(	kIx(
<g/>
doktorské	doktorský	k2eAgNnSc1d1
stipendium	stipendium	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
průměru	průměr	k1gInSc6
nepřekračuje	překračovat	k5eNaImIp3nS
sedm	sedm	k4xCc1
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
doktorandů	doktorand	k1gMnPc2
je	být	k5eAaImIp3nS
26	#num#	k4
429	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgInSc2
cca	cca	kA
2	#num#	k4
200	#num#	k4
na	na	k7c4
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
6,4	6,4	k4
%	%	kIx~
podíl	podíl	k1gInSc1
na	na	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
EU	EU	kA
má	mít	k5eAaImIp3nS
tento	tento	k3xDgInSc4
podíl	podíl	k1gInSc4
na	na	k7c6
čísle	číslo	k1gNnSc6
3,1	3,1	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Polovina	polovina	k1gFnSc1
doktorandů	doktorand	k1gMnPc2
studuje	studovat	k5eAaImIp3nS
v	v	k7c6
prezenční	prezenční	k2eAgFnSc6d1
formě	forma	k1gFnSc6
studia	studio	k1gNnSc2
a	a	k8xC
99,3	99,3	k4
%	%	kIx~
všech	všecek	k3xTgInPc2
studuje	studovat	k5eAaImIp3nS
na	na	k7c6
veřejných	veřejný	k2eAgFnPc6d1
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
oproti	oproti	k7c3
tomu	ten	k3xDgMnSc3
v	v	k7c6
EU	EU	kA
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
79	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
má	mít	k5eAaImIp3nS
celkově	celkově	k6eAd1
podíl	podíl	k1gInSc1
30	#num#	k4
%	%	kIx~
na	na	k7c6
všech	všecek	k3xTgMnPc6
doktorandech	doktorand	k1gMnPc6
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
13	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
ČVUT	ČVUT	kA
pak	pak	k6eAd1
8	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
jenom	jenom	k9
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
v	v	k7c6
ČR	ČR	kA
získalo	získat	k5eAaPmAgNnS
2	#num#	k4
629	#num#	k4
doktorandů	doktorand	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Podíl	podíl	k1gInSc1
doktorů	doktor	k1gMnPc2
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
na	na	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
absolventů	absolvent	k1gMnPc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
je	být	k5eAaImIp3nS
2,7	2,7	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
%	%	kIx~
doktorandů	doktorand	k1gMnPc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
technické	technický	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
20	#num#	k4
%	%	kIx~
na	na	k7c4
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
a	a	k8xC
14	#num#	k4
%	%	kIx~
na	na	k7c4
společenské	společenský	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
kupř	kupř	kA
<g/>
.	.	kIx.
na	na	k7c4
FF	ff	kA
UK	UK	kA
studovalo	studovat	k5eAaImAgNnS
v	v	k7c6
bakalářském	bakalářský	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
celkem	celkem	k6eAd1
3	#num#	k4
133	#num#	k4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
magisterském	magisterský	k2eAgMnSc6d1
1	#num#	k4
796	#num#	k4
a	a	k8xC
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
1	#num#	k4
158	#num#	k4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
kupř	kupř	kA
<g/>
.	.	kIx.
na	na	k7c4
FF	ff	kA
MU	MU	kA
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
4	#num#	k4
278	#num#	k4
–	–	k?
2	#num#	k4
271	#num#	k4
–	–	k?
740	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Například	například	k6eAd1
na	na	k7c6
PřF	PřF	k1gFnSc6
UK	UK	kA
bylo	být	k5eAaImAgNnS
složení	složení	k1gNnSc1
studentů	student	k1gMnPc2
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
cyklech	cyklus	k1gInPc6
2	#num#	k4
000	#num#	k4
–	–	k?
1	#num#	k4
136	#num#	k4
–	–	k?
1	#num#	k4
369	#num#	k4
<g/>
,	,	kIx,
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
na	na	k7c6
PřF	PřF	k1gFnSc6
MU	MU	kA
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
1	#num#	k4
873	#num#	k4
–	–	k?
895	#num#	k4
–	–	k?
911	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Některé	některý	k3yIgFnPc4
zajímavosti	zajímavost	k1gFnPc4
ze	z	k7c2
světa	svět	k1gInSc2
uvádí	uvádět	k5eAaImIp3nS
například	například	k6eAd1
pramen	pramen	k1gInSc4
The	The	k1gFnSc2
Economist	Economist	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1998	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
počet	počet	k1gInSc1
doktorátů	doktorát	k1gInPc2
udělených	udělený	k2eAgInPc2d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
zemích	zem	k1gFnPc6
OECD	OECD	kA
o	o	k7c6
40	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Doktorát	doktorát	k1gInSc1
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
nemusí	muset	k5eNaImIp3nS
přinést	přinést	k5eAaPmF
oproti	oproti	k7c3
magisterskému	magisterský	k2eAgNnSc3d1
vzdělání	vzdělání	k1gNnSc3
žádné	žádný	k3yNgFnSc6
zvýšení	zvýšení	k1gNnSc4
příjmů	příjem	k1gInPc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
i	i	k9
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
poklesu	pokles	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Průzkum	průzkum	k1gInSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
univerzitě	univerzita	k1gFnSc6
z	z	k7c2
USA	USA	kA
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
studia	studio	k1gNnSc2
dokončí	dokončit	k5eAaPmIp3nP
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
o	o	k7c4
nic	nic	k3yNnSc4
chytřejší	chytrý	k2eAgMnSc1d2
než	než	k8xS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
doktorát	doktorát	k1gInSc4
nezískají	získat	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
postdoktorandi	postdoktorand	k1gMnPc1
neuplatní	uplatnit	k5eNaPmIp3nP
v	v	k7c6
akademické	akademický	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
,	,	kIx,
obtížně	obtížně	k6eAd1
hledají	hledat	k5eAaImIp3nP
práci	práce	k1gFnSc4
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
lety	let	k1gInPc7
2005	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
USA	USA	kA
uděleno	udělit	k5eAaPmNgNnS
odhadem	odhad	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
titulů	titul	k1gInPc2
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
V	v	k7c6
Kanadě	Kanada	k1gFnSc6
vydělává	vydělávat	k5eAaImIp3nS
80	#num#	k4
%	%	kIx~
čerstvých	čerstvý	k2eAgMnPc2d1
absolventů	absolvent	k1gMnPc2
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
ročně	ročně	k6eAd1
před	před	k7c7
zdaněním	zdanění	k1gNnSc7
38	#num#	k4
600	#num#	k4
dolarů	dolar	k1gInPc2
a	a	k8xC
méně	málo	k6eAd2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
průměrný	průměrný	k2eAgInSc1d1
plat	plat	k1gInSc1
stavebního	stavební	k2eAgMnSc2d1
dělníka	dělník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Zhruba	zhruba	k6eAd1
třetina	třetina	k1gFnSc1
absolventů	absolvent	k1gMnPc2
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
si	se	k3xPyFc3
najde	najít	k5eAaPmIp3nS
zaměstnání	zaměstnání	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
nesouvisí	souviset	k5eNaImIp3nS
se	s	k7c7
získanou	získaný	k2eAgFnSc7d1
kvalifikací	kvalifikace	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
SRN	SRN	kA
skončí	skončit	k5eAaPmIp3nS
13	#num#	k4
%	%	kIx~
všech	všecek	k3xTgMnPc2
absolventů	absolvent	k1gMnPc2
doktorského	doktorský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
v	v	k7c6
podřadném	podřadný	k2eAgNnSc6d1
zaměstnání	zaměstnání	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
tento	tento	k3xDgInSc1
podíl	podíl	k1gInSc1
činí	činit	k5eAaImIp3nS
21	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
oborech	obor	k1gInPc6
se	se	k3xPyFc4
výhoda	výhoda	k1gFnSc1
držitelů	držitel	k1gMnPc2
titulů	titul	k1gInPc2
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
zcela	zcela	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
–	–	k?
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
doktorát	doktorát	k1gInSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
a	a	k8xC
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
nebo	nebo	k8xC
jazyků	jazyk	k1gInPc2
nevydělávají	vydělávat	k5eNaImIp3nP
o	o	k7c4
nic	nic	k3yNnSc4
víc	hodně	k6eAd2
než	než	k8xS
magistři	magistr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
článku	článek	k1gInSc2
Economist	Economist	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
vyplatí	vyplatit	k5eAaPmIp3nS
pouze	pouze	k6eAd1
lékařům	lékař	k1gMnPc3
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
vědních	vědní	k2eAgInPc6d1
oborech	obor	k1gInPc6
a	a	k8xC
u	u	k7c2
obchodního	obchodní	k2eAgNnSc2d1
a	a	k8xC
finančního	finanční	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
průměru	průměr	k1gInSc6
vztaženém	vztažený	k2eAgInSc6d1
na	na	k7c4
všechny	všechen	k3xTgInPc4
obory	obor	k1gInPc4
se	se	k3xPyFc4
držitelé	držitel	k1gMnPc1
titulu	titul	k1gInSc2
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
mohou	moct	k5eAaImIp3nP
těšit	těšit	k5eAaImF
pouze	pouze	k6eAd1
3	#num#	k4
<g/>
%	%	kIx~
rozdílu	rozdíl	k1gInSc2
ohodnocení	ohodnocení	k1gNnSc1
oproti	oproti	k7c3
držitelům	držitel	k1gMnPc3
magisterské	magisterský	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
studie	studie	k1gFnSc1
absolventů	absolvent	k1gMnPc2
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
v	v	k7c6
UK	UK	kA
přibližně	přibližně	k6eAd1
třetina	třetina	k1gFnSc1
z	z	k7c2
dotázaných	dotázaný	k2eAgMnPc2d1
přiznala	přiznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gNnSc2
zapsali	zapsat	k5eAaPmAgMnP
částečně	částečně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
zůstat	zůstat	k5eAaPmF
studenty	student	k1gMnPc4
nebo	nebo	k8xC
aby	aby	kYmCp3nP
oddálili	oddálit	k5eAaPmAgMnP
hledání	hledání	k1gNnSc3
zaměstnání	zaměstnání	k1gNnSc2
<g/>
,	,	kIx,
připustila	připustit	k5eAaPmAgFnS
to	ten	k3xDgNnSc1
téměř	téměř	k6eAd1
polovina	polovina	k1gFnSc1
studentů	student	k1gMnPc2
technických	technický	k2eAgInPc2d1
oborů	obor	k1gInPc2
–	–	k?
pro	pro	k7c4
vědce	vědec	k1gMnPc4
je	být	k5eAaImIp3nS
snadné	snadný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
stipendium	stipendium	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
táhne	táhnout	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
doktorskému	doktorský	k2eAgNnSc3d1
studiu	studio	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Honours	Honours	k1gInSc1
degree	degree	k1gInSc1
–	–	k?
bakalářský	bakalářský	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
se	s	k7c7
specializací	specializace	k1gFnSc7
(	(	kIx(
<g/>
např.	např.	kA
BS	BS	kA
(	(	kIx(
<g/>
Hons	Hons	k1gMnSc1
<g/>
)	)	kIx)
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Hons	Hons	k1gMnSc1
<g/>
)	)	kIx)
tedy	tedy	k9
znamená	znamenat	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
with	witha	k1gFnPc2
Honours	Honoursa	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
jiný	jiný	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
kvalifikace	kvalifikace	k1gFnSc2
–	–	k?
Honours	Honours	k1gInSc1
degree	degreat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoznačuje	označovat	k5eNaImIp3nS
to	ten	k3xDgNnSc4
studium	studium	k1gNnSc4
s	s	k7c7
vyznamenáním	vyznamenání	k1gNnSc7
(	(	kIx(
<g/>
Graduated	Graduated	k1gMnSc1
<g/>
/	/	kIx~
<g/>
passed	passed	k1gMnSc1
with	with	k1gMnSc1
honors	honors	k6eAd1
či	či	k8xC
with	with	k1gMnSc1
distinction	distinction	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ani	ani	k8xC
čestný	čestný	k2eAgInSc1d1
titul	titul	k1gInSc1
(	(	kIx(
<g/>
honorary	honorar	k1gInPc1
degree	degre	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
právě	právě	k9
onu	onen	k3xDgFnSc4
specializaci	specializace	k1gFnSc4
v	v	k7c6
oboru	obor	k1gInSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
od	od	k7c2
klasického	klasický	k2eAgInSc2d1
bakalariátu	bakalariát	k1gInSc2
odlišuje	odlišovat	k5eAaImIp3nS
<g/>
;	;	kIx,
formální	formální	k2eAgFnSc1d1
kvalifikace	kvalifikace	k1gFnSc1
Honours	Honoursa	k1gFnPc2
degree	degree	k6eAd1
je	být	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
jako	jako	k9
u	u	k7c2
Bachelor	Bachelora	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
degree	degree	k1gFnSc7
–	–	k?
bakalářského	bakalářský	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
6	#num#	k4
v	v	k7c6
ISCED	ISCED	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Doctor	Doctor	k1gMnSc1
of	of	k?
Philosophy	Philosopha	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Z.	Z.	kA
z.	z.	k?
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
zmene	zmenout	k5eAaPmIp3nS
a	a	k8xC
doplnení	doplnení	k1gNnSc4
niektorých	niektorý	k2eAgNnPc2d1
zákonov	zákonovo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
RYCHLÍK	Rychlík	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osvědčil	osvědčit	k5eAaPmAgMnS
se	se	k3xPyFc4
boloňský	boloňský	k2eAgInSc4d1
recept	recept	k1gInSc4
na	na	k7c4
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
,	,	kIx,
2013-02-19	2013-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Zkratky	zkratka	k1gFnSc2
titulů	titul	k1gInPc2
a	a	k8xC
hodností	hodnost	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
47	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOVORKOVÁ	Hovorková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznat	vyznat	k5eAaPmF,k5eAaBmF
se	se	k3xPyFc4
v	v	k7c6
titulech	titul	k1gInPc6
je	být	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
složité	složitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italové	Ital	k1gMnPc1
si	se	k3xPyFc3
s	s	k7c7
nimi	on	k3xPp3gInPc7
hlavu	hlava	k1gFnSc4
nelámou	lámat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-05-27	2010-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sdělení	sdělení	k1gNnSc1
Ministerstva	ministerstvo	k1gNnSc2
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
č.	č.	k?
33	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
m.	m.	k?
s.	s.	k?
<g/>
,	,	kIx,
o	o	k7c6
sjednání	sjednání	k1gNnSc6
Dohody	dohoda	k1gFnSc2
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
vládou	vláda	k1gFnSc7
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
o	o	k7c6
vzájemném	vzájemný	k2eAgNnSc6d1
uznávání	uznávání	k1gNnSc6
rovnocennosti	rovnocennost	k1gFnSc2
dokladů	doklad	k1gInPc2
o	o	k7c4
vzdělání	vzdělání	k1gNnSc4
vydávaných	vydávaný	k2eAgFnPc2d1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slovenské	slovenský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
..	..	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jazyková	jazykový	k2eAgFnSc1d1
poradna	poradna	k1gFnSc1
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
<g/>
:	:	kIx,
Přehled	přehled	k1gInSc1
vysokoškolských	vysokoškolský	k2eAgMnPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
akademických	akademický	k2eAgFnPc2d1
hodností	hodnost	k1gFnPc2
<g/>
↑	↑	k?
Národohospodářská	národohospodářský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Bílá	bílý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Národohospodářské	národohospodářský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
2009	#num#	k4
<g/>
,	,	kIx,
Prvákův	prvákův	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
školou	škola	k1gFnSc7
a	a	k8xC
studiem	studio	k1gNnSc7
<g/>
:	:	kIx,
Jak	jak	k8xC,k8xS
mám	mít	k5eAaImIp1nS
koho	kdo	k3yQnSc4,k3yRnSc4,k3yInSc4
oslovovat	oslovovat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
:	:	kIx,
Příručka	příručka	k1gFnSc1
pro	pro	k7c4
nerozkoukaného	rozkoukaný	k2eNgMnSc4d1
plzeňského	plzeňský	k2eAgMnSc4d1
antropologa	antropolog	k1gMnSc4
Archivováno	archivován	k2eAgNnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
6	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc2
<g/>
:	:	kIx,
Jak	jak	k8xS,k8xC
na	na	k7c4
...	...	k?
<g/>
oslovení	oslovení	k1gNnSc1
učitelů	učitel	k1gMnPc2
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
a	a	k8xC
managementu	management	k1gInSc2
Univerzity	univerzita	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Studium	studium	k1gNnSc1
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
na	na	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
univerzitě	univerzita	k1gFnSc3
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
:	:	kIx,
Základní	základní	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
<g/>
:	:	kIx,
Učitel	učitel	k1gMnSc1
<g/>
↑	↑	k?
Vědeckotechnický	vědeckotechnický	k2eAgInSc1d1
park	park	k1gInSc1
Univerzity	univerzita	k1gFnSc2
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
:	:	kIx,
Slovníček	slovníček	k1gInSc1
pojmů	pojem	k1gInPc2
<g/>
:	:	kIx,
Akademické	akademický	k2eAgFnPc4d1
(	(	kIx(
<g/>
vědecko-pedagogické	vědecko-pedagogický	k2eAgFnPc4d1
<g/>
)	)	kIx)
hodnosti	hodnost	k1gFnPc4
a	a	k8xC
tituly	titul	k1gInPc4
Archivováno	archivován	k2eAgNnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Silvie	Silvie	k1gFnSc1
Trebatická	Trebatický	k2eAgFnSc1d1
<g/>
:	:	kIx,
Pane	Pan	k1gMnSc5
doktore	doktor	k1gMnSc5
nebo	nebo	k8xC
profesore	profesor	k1gMnSc5
aneb	aneb	k?
-	-	kIx~
jak	jak	k8xS,k8xC
správně	správně	k6eAd1
oslovovat	oslovovat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
studentpoint	studentpoint	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20111	#num#	k4
2	#num#	k4
DURDÍK	DURDÍK	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktor	doktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmý	sedmý	k4xOgInSc4
díl	díl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
773	#num#	k4
<g/>
–	–	k?
<g/>
774	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmý	sedmý	k4xOgInSc4
díl	díl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Doktor	doktor	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
774	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
23	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
63	#num#	k4
<g/>
/	/	kIx~
<g/>
1873	#num#	k4
ř.	ř.	k?
z.	z.	k?
<g/>
,	,	kIx,
o	o	k7c4
organisaci	organisace	k1gFnSc4
úřadů	úřad	k1gInPc2
universitních	universitní	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://phys.org/news/2017-01-postdoctoral-science-positions.html	http://phys.org/news/2017-01-postdoctoral-science-positions.html	k1gInSc1
-	-	kIx~
Study	stud	k1gInPc1
recommends	recommendsa	k1gFnPc2
changes	changes	k1gInSc1
to	ten	k3xDgNnSc1
postdoctoral	postdoctorat	k5eAaImAgInS,k5eAaPmAgInS
science	scienka	k1gFnSc3
positions	positions	k6eAd1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jen	jen	k8xS
ztráta	ztráta	k1gFnSc1
času	čas	k1gInSc2
<g/>
...	...	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
týdeník	týdeník	k1gInSc1
Ekonom	ekonom	k1gMnSc1
-	-	kIx~
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
2011-06-01	2011-06-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIESNEROVÁ	Wiesnerová	k1gFnSc1
<g/>
,	,	kIx,
Ema	Ema	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc7
české	český	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Inbreeding	Inbreeding	k1gInSc4
<g/>
,	,	kIx,
administrativní	administrativní	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
a	a	k8xC
špatně	špatně	k6eAd1
nastavené	nastavený	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Universitas	Universitas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-12-18	2017-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
Technologické	technologický	k2eAgFnSc6d1
agentuře	agentura	k1gFnSc6
s	s	k7c7
Martinem	Martin	k1gMnSc7
Bunčekem	Bunček	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VedaVyzkum	VedaVyzkum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://czs.muni.cz/cs/outgoing-mobility/outgoing-student/outgoing-student-studium/outgoing-student-partners/198-international-cooperation/studies-cooperation/levels/index.php	http://czs.muni.cz/cs/outgoing-mobility/outgoing-student/outgoing-student-studium/outgoing-student-partners/198-international-cooperation/studies-cooperation/levels/index.php	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
5	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Individuální	individuální	k2eAgInPc4d1
kontrakty	kontrakt	k1gInPc4
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
47	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
172	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
22	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
47	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
72	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
6	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
74	#num#	k4
odst	odstum	k1gNnPc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
47	#num#	k4
odst	odst	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
137	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
některé	některý	k3yIgInPc1
další	další	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://abicko.avcr.cz/2015/11/11/	http://abicko.avcr.cz/2015/11/11/	k4
<g/>
↑	↑	k?
www.avcr.cz	www.avcr.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
–	–	k?
Pořadí	pořadí	k1gNnSc2
titulů	titul	k1gInPc2
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
<g/>
↑	↑	k?
Postup	postup	k1gInSc1
podle	podle	k7c2
ekvivalenčních	ekvivalenční	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
-	-	kIx~
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
a	a	k8xC
Slovinsko	Slovinsko	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
46	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
a	a	k8xC
§	§	k?
47	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marek	Marek	k1gMnSc1
Skovajsa	Skovajs	k1gMnSc2
<g/>
:	:	kIx,
Velkovýroba	velkovýroba	k1gFnSc1
malodoktorů	malodoktor	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
46	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KVAČKOVÁ	KVAČKOVÁ	kA
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RNDr.	RNDr.	kA
<g/>
,	,	kIx,
PhDr.	PhDr.	kA
<g/>
,	,	kIx,
MVDr.	MVDr.	kA
<g/>
..	..	k?
Česko	Česko	k1gNnSc1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
doktor	doktor	k1gMnSc1
jako	jako	k8xC,k8xS
doktor	doktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JUDr.	JUDr.	kA
-	-	kIx~
titul	titul	k1gInSc1
za	za	k7c4
všechny	všechen	k3xTgInPc4
prachy	prach	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCTOPUS	OCTOPUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
46	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
46	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ZEMEK	zemek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proč	proč	k6eAd1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
jít	jít	k5eAaImF
na	na	k7c4
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PetrZemek	PetrZemek	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
2011-06-04	2011-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
586	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
daních	daň	k1gFnPc6
z	z	k7c2
příjmů	příjem	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
35	#num#	k4
<g/>
ba	ba	k9
Slevy	sleva	k1gFnPc1
na	na	k7c6
dani	daň	k1gFnSc6
pro	pro	k7c4
poplatníky	poplatník	k1gMnPc4
daně	daň	k1gFnSc2
z	z	k7c2
příjmů	příjem	k1gInPc2
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
f	f	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RYCHLÍK	Rychlík	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštnosti	zvláštnost	k1gFnSc3
českých	český	k2eAgInPc2d1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-05-12	2012-05-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Má	mít	k5eAaImIp3nS
být	být	k5eAaImF
doktorské	doktorský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
masové	masový	k2eAgNnSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učitelské	učitelský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
-	-	kIx~
UcitelskeNoviny	UcitelskeNovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-02-01	2013-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Oddělení	oddělení	k1gNnPc2
statistických	statistický	k2eAgInPc2d1
výstupů	výstup	k1gInPc2
a	a	k8xC
analýz	analýza	k1gFnPc2
–	–	k?
studenti	student	k1gMnPc1
podle	podle	k7c2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
/	/	kIx~
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
data	datum	k1gNnPc4
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
stav	stav	k1gInSc1
k	k	k7c3
20	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
dostupné	dostupný	k2eAgNnSc1d1
na	na	k7c6
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hirschův	Hirschův	k2eAgInSc1d1
index	index	k1gInSc1
</s>
<s>
Impakt	Impakt	k1gInSc1
faktor	faktor	k1gInSc1
</s>
<s>
RIV	RIV	kA
</s>
<s>
Web	web	k1gInSc1
of	of	k?
Science	Science	k1gFnSc1
</s>
<s>
Scopus	Scopus	k1gInSc1
(	(	kIx(
<g/>
databáze	databáze	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Grantová	grantový	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Technologická	technologický	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Akreditované	akreditovaný	k2eAgInPc1d1
studijní	studijní	k2eAgInPc1d1
programy	program	k1gInPc1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
–	–	k?
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Charakter	charakter	k1gInSc1
doktorátu	doktorát	k1gInSc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
–	–	k?
Vysoké	vysoká	k1gFnSc2
školství	školství	k1gNnSc2
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zkratky	zkratka	k1gFnSc2
českých	český	k2eAgInPc2d1
akademických	akademický	k2eAgInPc2d1
titulů	titul	k1gInPc2
Akademické	akademický	k2eAgInPc4d1
tituly	titul	k1gInPc4
</s>
<s>
Bc.	Bc.	k?
</s>
<s>
BcA.	BcA.	k?
</s>
<s>
Ing.	ing.	kA
</s>
<s>
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
JUDr.	JUDr.	kA
</s>
<s>
MDDr	MDDr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
MgA.	MgA.	k?
</s>
<s>
Mgr.	Mgr.	kA
</s>
<s>
MUDr.	MUDr.	kA
</s>
<s>
MVDr.	MVDr.	kA
</s>
<s>
PaedDr	PaedDr	kA
<g/>
.	.	kIx.
</s>
<s>
PharmDr.	PharmDr.	k?
</s>
<s>
PhDr.	PhDr.	kA
</s>
<s>
PhMr	PhMr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
RCDr	RCDr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
RNDr.	RNDr.	kA
</s>
<s>
RSDr.	RSDr.	kA
</s>
<s>
RTDr	RTDr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
ThDr.	ThDr.	k?
</s>
<s>
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademicko-vědecké	akademicko-vědecký	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
CSc.	CSc.	kA
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
</s>
<s>
DrSc	DrSc	kA
<g/>
.	.	kIx.
</s>
<s>
DSc	DSc	k?
<g/>
.	.	kIx.
</s>
<s>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Th	Th	k?
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Vědecko-pedagogické	vědecko-pedagogický	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
doc.	doc.	kA
</s>
<s>
prof.	prof.	kA
Čestné	čestný	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
dr	dr	kA
<g/>
.	.	kIx.
h.	h.	k?
c.	c.	k?
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
akad	akad	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
DiS.	DiS.	k?
</s>
<s>
Zkratky	zkratka	k1gFnPc1
anglosaských	anglosaský	k2eAgMnPc2d1
akademických	akademický	k2eAgMnPc2d1
titulů	titul	k1gInPc2
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
Akademické	akademický	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
B.A.	B.A.	k?
•	•	k?
B.B.A.	B.B.A.	k1gFnSc2
•	•	k?
B.	B.	kA
<g/>
S.	S.	kA
•	•	k?
LL	LL	kA
<g/>
.	.	kIx.
<g/>
M.	M.	kA
•	•	k?
M.	M.	kA
<g/>
A.	A.	kA
•	•	k?
M.	M.	kA
<g/>
B.A.	B.A.	k1gMnSc3
•	•	k?
M.	M.	kA
<g/>
D.	D.	kA
•	•	k?
M.	M.	kA
<g/>
P.	P.	kA
<g/>
A.	A.	kA
•	•	k?
M.	M.	kA
<g/>
S.	S.	kA
Akademicko-vědecké	akademicko-vědecký	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
Art	Art	k?
<g/>
.	.	kIx.
<g/>
D.	D.	kA
•	•	k?
D.B.A.	D.B.A.	k1gFnSc2
•	•	k?
D.	D.	kA
<g/>
Sc	Sc	k1gFnSc2
<g/>
.	.	kIx.
•	•	k?
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
•	•	k?
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
