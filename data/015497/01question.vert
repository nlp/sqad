<s>
Jaká	jaký	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
akademického	akademický	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
lze	lze	k6eAd1
získat	získat	k5eAaPmF
vysokoškolským	vysokoškolský	k2eAgNnSc7d1
studiem	studium	k1gNnSc7
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
?	?	kIx.
</s>