<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g />
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
