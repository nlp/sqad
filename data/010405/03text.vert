<p>
<s>
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděný	uváděný	k2eAgInSc1d1	uváděný
i	i	k9	i
jako	jako	k9	jako
Apollón	Apollón	k1gMnSc1	Apollón
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
,	,	kIx,	,
Potrestání	potrestání	k1gNnSc1	potrestání
Marsya	Marsya	k1gMnSc1	Marsya
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
trestá	trestat	k5eAaImIp3nS	trestat
Marsya	Marsyum	k1gNnSc2	Marsyum
nebo	nebo	k8xC	nebo
Stahování	stahování	k1gNnSc2	stahování
Marsya	Marsy	k1gInSc2	Marsy
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc4	obraz
italského	italský	k2eAgInSc2d1	italský
pozdně	pozdně	k6eAd1	pozdně
renesančního	renesanční	k2eAgMnSc2d1	renesanční
umělce	umělec	k1gMnSc2	umělec
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejcennější	cenný	k2eAgNnSc4d3	nejcennější
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
obrazárně	obrazárna	k1gFnSc6	obrazárna
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
Arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
olomouckému	olomoucký	k2eAgMnSc3d1	olomoucký
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
okolnosti	okolnost	k1gFnPc1	okolnost
vzniku	vznik	k1gInSc2	vznik
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
však	však	k9	však
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dílo	dílo	k1gNnSc4	dílo
zralého	zralý	k2eAgMnSc2d1	zralý
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
Tizianových	tizianový	k2eAgFnPc2d1	tizianový
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jako	jako	k9	jako
osmdesátník	osmdesátník	k1gMnSc1	osmdesátník
mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1570	[number]	k4	1570
a	a	k8xC	a
smrtí	smrtit	k5eAaImIp3nS	smrtit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
na	na	k7c6	na
kameni	kámen	k1gInSc6	kámen
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
je	být	k5eAaImIp3nS	být
částečný	částečný	k2eAgInSc1d1	částečný
podpis	podpis	k1gInSc1	podpis
<g/>
.	.	kIx.	.
<g/>
Obraz	obraz	k1gInSc1	obraz
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
,	,	kIx,	,
satyr	satyr	k1gMnSc1	satyr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
drze	drze	k6eAd1	drze
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
boha	bůh	k1gMnSc4	bůh
Apolla	Apollo	k1gNnSc2	Apollo
na	na	k7c4	na
hudební	hudební	k2eAgFnSc4d1	hudební
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
zabíjen	zabíjen	k2eAgMnSc1d1	zabíjen
stahováním	stahování	k1gNnSc7	stahování
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
pláten	plátno	k1gNnPc2	plátno
s	s	k7c7	s
mytologickými	mytologický	k2eAgInPc7d1	mytologický
náměty	námět	k1gInPc7	námět
podle	podle	k7c2	podle
Ovidia	Ovidius	k1gMnSc2	Ovidius
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
Tizian	Tizian	k1gMnSc1	Tizian
tvořil	tvořit	k5eAaImAgMnS	tvořit
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
pozdních	pozdní	k2eAgNnPc6d1	pozdní
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Španělského	španělský	k2eAgMnSc4d1	španělský
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
kroměřížský	kroměřížský	k2eAgInSc1d1	kroměřížský
obraz	obraz	k1gInSc1	obraz
zřejmě	zřejmě	k6eAd1	zřejmě
nebyl	být	k5eNaImAgInS	být
součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
pozapomenut	pozapomenut	k2eAgInSc1d1	pozapomenut
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sbírek	sbírka	k1gFnPc2	sbírka
benátské	benátský	k2eAgFnSc2d1	Benátská
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
"	"	kIx"	"
<g/>
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
do	do	k7c2	do
kritické	kritický	k2eAgFnSc2d1	kritická
literatury	literatura	k1gFnSc2	literatura
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
jej	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
historici	historik	k1gMnPc1	historik
umění	umění	k1gNnSc1	umění
"	"	kIx"	"
<g/>
přijali	přijmout	k5eAaPmAgMnP	přijmout
jako	jako	k9	jako
důležitou	důležitý	k2eAgFnSc4d1	důležitá
pozdní	pozdní	k2eAgFnSc4d1	pozdní
práci	práce	k1gFnSc4	práce
<g/>
"	"	kIx"	"
Tiziana	Tiziana	k1gFnSc1	Tiziana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
málo	málo	k6eAd1	málo
znám	znám	k2eAgInSc1d1	znám
širší	široký	k2eAgFnSc3d2	širší
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
moderní	moderní	k2eAgFnSc6d1	moderní
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
uvítán	uvítán	k2eAgInSc4d1	uvítán
s	s	k7c7	s
užaslým	užaslý	k2eAgInSc7d1	užaslý
obdivem	obdiv	k1gInSc7	obdiv
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
"	"	kIx"	"
významné	významný	k2eAgFnPc1d1	významná
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
Královské	královský	k2eAgFnSc6d1	královská
akademii	akademie	k1gFnSc6	akademie
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
diváků	divák	k1gMnPc2	divák
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgMnSc1d1	nový
a	a	k8xC	a
John	John	k1gMnSc1	John
Russell	Russell	k1gMnSc1	Russell
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejúžasnější	úžasný	k2eAgInSc1d3	nejúžasnější
obraz	obraz	k1gInSc1	obraz
výstavy	výstava	k1gFnSc2	výstava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
své	svůj	k3xOyFgFnSc2	svůj
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
analýzy	analýza	k1gFnSc2	analýza
sir	sir	k1gMnSc1	sir
Lawrence	Lawrence	k1gFnSc2	Lawrence
Gowing	Gowing	k1gInSc1	Gowing
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
celé	celá	k1gFnSc2	celá
měsíce	měsíc	k1gInSc2	měsíc
–	–	k?	–
nepřeháním	přehánět	k5eNaImIp1nS	přehánět
–	–	k?	–
je	být	k5eAaImIp3nS	být
Londýn	Londýn	k1gInSc1	Londýn
uchvácen	uchvácet	k5eAaImNgInS	uchvácet
kouzlem	kouzlo	k1gNnSc7	kouzlo
tohoto	tento	k3xDgNnSc2	tento
mistrovského	mistrovský	k2eAgNnSc2d1	mistrovské
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
tragický	tragický	k2eAgInSc4d1	tragický
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Tizianovu	Tizianův	k2eAgFnSc4d1	Tizianova
poezii	poezie	k1gFnSc4	poezie
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
sedmdesátky	sedmdesátka	k1gFnSc2	sedmdesátka
<g/>
,	,	kIx,	,
dospěl	dochvít	k5eAaPmAgMnS	dochvít
ke	k	k7c3	k
krutému	krutý	k2eAgNnSc3d1	kruté
a	a	k8xC	a
vážnému	vážný	k2eAgNnSc3d1	vážné
vyvrcholení	vyvrcholení	k1gNnSc3	vyvrcholení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
hodin	hodina	k1gFnPc2	hodina
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
shluk	shluk	k1gInSc4	shluk
ohromených	ohromený	k2eAgMnPc2d1	ohromený
a	a	k8xC	a
zmatených	zmatený	k2eAgMnPc2d1	zmatený
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
...	...	k?	...
Na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
stále	stále	k6eAd1	stále
ptají	ptat	k5eAaImIp3nP	ptat
a	a	k8xC	a
v	v	k7c6	v
rádiu	rádio	k1gNnSc6	rádio
seriózní	seriózní	k2eAgMnPc1d1	seriózní
kritici	kritik	k1gMnPc1	kritik
debatují	debatovat	k5eAaImIp3nP	debatovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
krása	krása	k1gFnSc1	krása
nebo	nebo	k8xC	nebo
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
skrze	skrze	k?	skrze
tak	tak	k9	tak
děsivě	děsivě	k6eAd1	děsivě
bolestné	bolestný	k2eAgNnSc1d1	bolestné
téma	téma	k1gNnSc1	téma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
a	a	k8xC	a
téma	téma	k1gNnSc4	téma
==	==	k?	==
</s>
</p>
<p>
<s>
Volba	volba	k1gFnSc1	volba
násilné	násilný	k2eAgFnSc2d1	násilná
scény	scéna	k1gFnSc2	scéna
možná	možná	k9	možná
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
smrtí	smrt	k1gFnSc7	smrt
Marcantonia	Marcantonium	k1gNnSc2	Marcantonium
Bragadina	Bragadin	k2eAgNnSc2d1	Bragadin
<g/>
,	,	kIx,	,
benátského	benátský	k2eAgMnSc2d1	benátský
velitele	velitel	k1gMnSc2	velitel
Famagusty	Famagusta	k1gMnSc2	Famagusta
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
města	město	k1gNnSc2	město
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1571	[number]	k4	1571
Osmany	Osman	k1gMnPc4	Osman
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nesmírné	smírný	k2eNgNnSc1d1	nesmírné
pobouření	pobouření	k1gNnSc1	pobouření
<g/>
.	.	kIx.	.
</s>
<s>
Tizianova	Tizianův	k2eAgFnSc1d1	Tizianova
kompozice	kompozice	k1gFnSc1	kompozice
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
Giulia	Giulium	k1gNnSc2	Giulium
Romana	Roman	k1gMnSc2	Roman
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
o	o	k7c4	o
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
kapitolu	kapitola	k1gFnSc4	kapitola
Vizuální	vizuální	k2eAgInPc1d1	vizuální
zdroje	zdroj	k1gInPc1	zdroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Oba	dva	k4xCgMnPc1	dva
umělci	umělec	k1gMnPc1	umělec
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
vyprávění	vyprávění	k1gNnSc4	vyprávění
z	z	k7c2	z
Ovidiových	Ovidiův	k2eAgFnPc2d1	Ovidiova
Proměn	proměna	k1gFnPc2	proměna
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
6	[number]	k4	6
<g/>
,	,	kIx,	,
verše	verš	k1gInSc2	verš
382	[number]	k4	382
až	až	k9	až
400	[number]	k4	400
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
hudební	hudební	k2eAgFnSc1d1	hudební
soutěž	soutěž	k1gFnSc1	soutěž
popisuje	popisovat	k5eAaImIp3nS	popisovat
jen	jen	k9	jen
stručně	stručně	k6eAd1	stručně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
scéně	scéna	k1gFnSc6	scéna
stahování	stahování	k1gNnSc2	stahování
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
podrobněji	podrobně	k6eAd2	podrobně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
uvádí	uvádět	k5eAaImIp3nS	uvádět
jen	jen	k9	jen
málo	málo	k4c1	málo
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
pomohly	pomoct	k5eAaPmAgInP	pomoct
vizualizovat	vizualizovat	k5eAaImF	vizualizovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jen	jen	k9	jen
Marsyův	Marsyův	k2eAgInSc1d1	Marsyův
výkřik	výkřik	k1gInSc1	výkřik
"	"	kIx"	"
<g/>
Quid	Quid	k1gInSc1	Quid
me	me	k?	me
mihi	mih	k1gFnSc2	mih
detrahis	detrahis	k1gFnSc2	detrahis
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
mě	já	k3xPp1nSc4	já
vytrháváš	vytrhávat	k5eAaImIp2nS	vytrhávat
ze	z	k7c2	z
mne	já	k3xPp1nSc2	já
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
ve	v	k7c6	v
verši	verš	k1gInSc6	verš
VI	VI	kA	VI
<g/>
,385	,385	k4	,385
<g/>
.	.	kIx.	.
<g/>
Marsyas	Marsyas	k1gMnSc1	Marsyas
byl	být	k5eAaImAgMnS	být
zručný	zručný	k2eAgMnSc1d1	zručný
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klasický	klasický	k2eAgInSc4d1	klasický
aulos	aulos	k1gInSc4	aulos
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Tizianových	tizianový	k2eAgFnPc6d1	tizianový
dobách	doba	k1gFnPc6	doba
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
Panovou	Panův	k2eAgFnSc7d1	Panova
flétnou	flétna	k1gFnSc7	flétna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zde	zde	k6eAd1	zde
visí	viset	k5eAaImIp3nS	viset
Marsyovi	Marsya	k1gMnSc3	Marsya
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gMnSc1	Apollo
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
lyru	lyra	k1gFnSc4	lyra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zde	zde	k6eAd1	zde
představuje	představovat	k5eAaImIp3nS	představovat
moderní	moderní	k2eAgFnSc1d1	moderní
lira	lira	k1gFnSc1	lira
da	da	k?	da
braccio	braccio	k1gMnSc1	braccio
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
houslí	houslit	k5eAaImIp3nS	houslit
s	s	k7c7	s
až	až	k6eAd1	až
sedmi	sedm	k4xCc7	sedm
strunami	struna	k1gFnPc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
hraje	hrát	k5eAaImIp3nS	hrát
postava	postava	k1gFnSc1	postava
nejisté	jistý	k2eNgFnSc2d1	nejistá
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
někteří	některý	k3yIgMnPc1	některý
učenci	učenec	k1gMnPc1	učenec
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c2	za
samotného	samotný	k2eAgNnSc2d1	samotné
Apolla	Apollo	k1gNnSc2	Apollo
<g/>
,	,	kIx,	,
možná	možný	k2eAgFnSc1d1	možná
se	se	k3xPyFc4	se
objevujícího	objevující	k2eAgNnSc2d1	objevující
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Apollo	Apollo	k1gMnSc1	Apollo
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
i	i	k9	i
postava	postava	k1gFnSc1	postava
s	s	k7c7	s
vavřínovým	vavřínový	k2eAgInSc7d1	vavřínový
věncem	věnec	k1gInSc7	věnec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
kleká	klekat	k5eAaImIp3nS	klekat
a	a	k8xC	a
nožem	nůž	k1gInSc7	nůž
stahuje	stahovat	k5eAaImIp3nS	stahovat
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
Marsyovu	Marsyův	k2eAgFnSc4d1	Marsyův
hruď	hruď	k1gFnSc4	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudebník	hudebník	k1gMnSc1	hudebník
je	být	k5eAaImIp3nS	být
Orfeus	Orfeus	k1gMnSc1	Orfeus
neboli	neboli	k8xC	neboli
Olympus	Olympus	k1gMnSc1	Olympus
<g/>
,	,	kIx,	,
oddaný	oddaný	k2eAgMnSc1d1	oddaný
žák	žák	k1gMnSc1	žák
Marsyův	Marsyův	k2eAgMnSc1d1	Marsyův
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Apollo	Apollo	k1gMnSc1	Apollo
později	pozdě	k6eAd2	pozdě
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
hráče	hráč	k1gMnSc4	hráč
lyry	lyra	k1gFnSc2	lyra
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
.	.	kIx.	.
</s>
<s>
Mýtický	mýtický	k2eAgMnSc1d1	mýtický
král	král	k1gMnSc1	král
Midas	Midas	k1gMnSc1	Midas
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgMnSc1d1	sedící
starý	starý	k2eAgMnSc1d1	starý
muž	muž	k1gMnSc1	muž
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autoportrét	autoportrét	k1gInSc4	autoportrét
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Sestupná	sestupný	k2eAgFnSc1d1	sestupná
linie	linie	k1gFnSc1	linie
jeho	jeho	k3xOp3gInSc2	jeho
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
Marsya	Marsyus	k1gMnSc4	Marsyus
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
vzestupnou	vzestupný	k2eAgFnSc7d1	vzestupná
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yRgFnPc4	který
hudebník	hudebník	k1gMnSc1	hudebník
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
obrazu	obraz	k1gInSc2	obraz
hledí	hledět	k5eAaImIp3nS	hledět
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovidius	Ovidius	k1gMnSc1	Ovidius
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
otázce	otázka	k1gFnSc3	otázka
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
byli	být	k5eAaImAgMnP	být
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
řeckých	řecký	k2eAgNnPc2d1	řecké
vyprávění	vyprávění	k1gNnPc2	vyprávění
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
role	role	k1gFnSc2	role
ujaly	ujmout	k5eAaPmAgFnP	ujmout
tři	tři	k4xCgFnPc4	tři
múzy	múza	k1gFnPc4	múza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
popletl	poplést	k5eAaPmAgMnS	poplést
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
bájí	báj	k1gFnSc7	báj
<g/>
,	,	kIx,	,
s	s	k7c7	s
Midasovým	Midasův	k2eAgInSc7d1	Midasův
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Midasův	Midasův	k2eAgInSc4d1	Midasův
soud	soud	k1gInSc4	soud
byla	být	k5eAaImAgFnS	být
další	další	k2eAgFnSc4d1	další
hudební	hudební	k2eAgFnSc4d1	hudební
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
zase	zase	k9	zase
lyry	lyra	k1gFnPc1	lyra
proti	proti	k7c3	proti
flétně	flétna	k1gFnSc3	flétna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
Panem	Pan	k1gMnSc7	Pan
hrajícím	hrající	k2eAgMnSc7d1	hrající
na	na	k7c6	na
flétnu	flétna	k1gFnSc4	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
Apollo	Apollo	k1gMnSc1	Apollo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
král	král	k1gMnSc1	král
Midas	Midas	k1gMnSc1	Midas
upřednostnil	upřednostnit	k5eAaPmAgMnS	upřednostnit
flétnu	flétna	k1gFnSc4	flétna
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
oslíma	oslí	k2eAgNnPc7d1	oslí
ušima	ucho	k1gNnPc7	ucho
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Pan	Pan	k1gMnSc1	Pan
byl	být	k5eAaImAgMnS	být
jen	jen	k9	jen
ponížen	ponížen	k2eAgMnSc1d1	ponížen
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgFnSc1d1	sedící
postava	postava	k1gFnSc1	postava
vpravo	vpravo	k6eAd1	vpravo
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Midas	Midas	k1gMnSc1	Midas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnPc1	jeho
uši	ucho	k1gNnPc1	ucho
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
nedotčeny	dotknout	k5eNaPmNgFnP	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Midasův	Midasův	k2eAgInSc1d1	Midasův
soud	soud	k1gInSc1	soud
býval	bývat	k5eAaImAgInS	bývat
také	také	k9	také
malován	malován	k2eAgInSc1d1	malován
<g/>
.	.	kIx.	.
</s>
<s>
Midas	Midas	k1gMnSc1	Midas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
různě	různě	k6eAd1	různě
spojovaný	spojovaný	k2eAgInSc1d1	spojovaný
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
ve	v	k7c6	v
Frýgii	Frýgie	k1gFnSc6	Frýgie
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
příběhy	příběh	k1gInPc1	příběh
umístěny	umístěn	k2eAgInPc1d1	umístěn
<g/>
.	.	kIx.	.
</s>
<s>
Frýgové	Frýg	k1gMnPc1	Frýg
nebyli	být	k5eNaImAgMnP	být
Řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nehelénizovali	helénizovat	k5eNaBmAgMnP	helénizovat
po	po	k7c6	po
výbojích	výboj	k1gInPc6	výboj
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
řeckého	řecký	k2eAgInSc2d1	řecký
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
frygickou	frygický	k2eAgFnSc4d1	frygická
čapku	čapka	k1gFnSc4	čapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apollovi	Apoll	k1gMnSc3	Apoll
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zlověstná	zlověstný	k2eAgFnSc1d1	zlověstná
"	"	kIx"	"
<g/>
skýtská	skýtský	k2eAgFnSc1d1	skýtský
<g/>
"	"	kIx"	"
postava	postava	k1gFnSc1	postava
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnPc1d1	pracující
na	na	k7c6	na
Marsyově	Marsyův	k2eAgFnSc6d1	Marsyův
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
satyr	satyr	k1gMnSc1	satyr
s	s	k7c7	s
kbelíkem	kbelík	k1gInSc7	kbelík
za	za	k7c7	za
Midasem	Midas	k1gMnSc7	Midas
<g/>
,	,	kIx,	,
možná	možná	k9	možná
chtějící	chtějící	k2eAgNnSc1d1	chtějící
zachytit	zachytit	k5eAaPmF	zachytit
krev	krev	k1gFnSc4	krev
nebo	nebo	k8xC	nebo
podržet	podržet	k5eAaPmF	podržet
odstraněnou	odstraněný	k2eAgFnSc4d1	odstraněná
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
příběhu	příběh	k1gInSc2	příběh
Apollo	Apollo	k1gMnSc1	Apollo
později	pozdě	k6eAd2	pozdě
vyvěsí	vyvěsit	k5eAaPmIp3nS	vyvěsit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
chlapec	chlapec	k1gMnSc1	chlapec
nebo	nebo	k8xC	nebo
mladý	mladý	k2eAgMnSc1d1	mladý
satyr	satyr	k1gMnSc1	satyr
krotí	krotit	k5eAaImIp3nS	krotit
velkého	velký	k2eAgMnSc4d1	velký
psa	pes	k1gMnSc4	pes
napravo	napravo	k6eAd1	napravo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgMnSc1d2	menší
pes	pes	k1gMnSc1	pes
chlemtá	chlemtat	k5eAaImIp3nS	chlemtat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Tizianových	tizianový	k2eAgNnPc6d1	tizianový
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
satyr	satyr	k1gMnSc1	satyr
je	být	k5eAaImIp3nS	být
zobrazen	zobrazen	k2eAgInSc1d1	zobrazen
s	s	k7c7	s
kozíma	kozí	k2eAgFnPc7d1	kozí
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Obrácená	obrácený	k2eAgFnSc1d1	obrácená
pozice	pozice	k1gFnSc1	pozice
kozí	kozí	k2eAgFnSc2d1	kozí
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
jeho	jeho	k3xOp3gFnSc4	jeho
animalitu	animalita	k1gFnSc4	animalita
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marsyas	Marsyas	k1gInSc1	Marsyas
připomíná	připomínat	k5eAaImIp3nS	připomínat
středně	středně	k6eAd1	středně
velká	velký	k2eAgNnPc4d1	velké
zvířata	zvíře	k1gNnPc4	zvíře
vyvrhovaná	vyvrhovaný	k2eAgNnPc4d1	vyvrhované
nebo	nebo	k8xC	nebo
stahovaná	stahovaný	k2eAgNnPc4d1	stahované
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
v	v	k7c6	v
řeznictví	řeznictví	k1gNnSc6	řeznictví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
ještě	ještě	k6eAd1	ještě
nestažená	stažený	k2eNgFnSc1d1	nestažená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Apollo	Apollo	k1gMnSc1	Apollo
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
kus	kus	k1gInSc4	kus
oddělené	oddělený	k2eAgFnSc2d1	oddělená
kůže	kůže	k1gFnSc2	kůže
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nedrží	držet	k5eNaImIp3nS	držet
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompozici	kompozice	k1gFnSc4	kompozice
Marsyových	Marsyův	k2eAgFnPc2d1	Marsyův
nohou	noha	k1gFnPc2	noha
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
V	V	kA	V
odrážejí	odrážet	k5eAaImIp3nP	odrážet
další	další	k2eAgInPc1d1	další
tvary	tvar	k1gInPc1	tvar
V	V	kA	V
tvořené	tvořený	k2eAgInPc1d1	tvořený
ohnutými	ohnutý	k2eAgFnPc7d1	ohnutá
pažemi	paže	k1gFnPc7	paže
čtyř	čtyři	k4xCgFnPc2	čtyři
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
dívají	dívat	k5eAaImIp3nP	dívat
na	na	k7c4	na
satyra	satyr	k1gMnSc4	satyr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yQgFnSc6	jaký
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Marsyův	Marsyův	k2eAgInSc4d1	Marsyův
pupek	pupek	k1gInSc4	pupek
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
uprostřed	uprostřed	k7c2	uprostřed
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
průzkum	průzkum	k1gInSc1	průzkum
obrazu	obraz	k1gInSc2	obraz
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
rozdíly	rozdíl	k1gInPc1	rozdíl
vůči	vůči	k7c3	vůči
kompozici	kompozice	k1gFnSc3	kompozice
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Giulia	Giulius	k1gMnSc2	Giulius
Romana	Roman	k1gMnSc2	Roman
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
až	až	k9	až
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Tizian	Tizian	k1gMnSc1	Tizian
začal	začít	k5eAaPmAgMnS	začít
malovat	malovat	k5eAaImF	malovat
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
hudebník	hudebník	k1gMnSc1	hudebník
svůj	svůj	k3xOyFgInSc4	svůj
nástroj	nástroj	k1gInSc4	nástroj
pouze	pouze	k6eAd1	pouze
držel	držet	k5eAaImAgMnS	držet
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Romanově	Romanův	k2eAgInSc6d1	Romanův
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
chlapec	chlapec	k1gMnSc1	chlapec
či	či	k8xC	či
mladý	mladý	k2eAgMnSc1d1	mladý
satyr	satyr	k1gMnSc1	satyr
ani	ani	k8xC	ani
velký	velký	k2eAgMnSc1d1	velký
pes	pes	k1gMnSc1	pes
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
nebyli	být	k5eNaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výklad	výklad	k1gInSc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
autorů	autor	k1gMnPc2	autor
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
zachytit	zachytit	k5eAaPmF	zachytit
význam	význam	k1gInSc4	význam
"	"	kIx"	"
<g/>
proslaveně	proslaveně	k6eAd1	proslaveně
divošské	divošský	k2eAgFnSc2d1	divošská
<g/>
"	"	kIx"	"
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
navzdory	navzdory	k7c3	navzdory
"	"	kIx"	"
<g/>
brutalitě	brutalita	k1gFnSc3	brutalita
trestu	trest	k1gInSc2	trest
<g/>
"	"	kIx"	"
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
mocně	mocně	k6eAd1	mocně
fascinující	fascinující	k2eAgMnSc1d1	fascinující
a	a	k8xC	a
popsána	popsán	k2eAgFnSc1d1	popsána
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nejvíce	hodně	k6eAd3	hodně
diskutovaný	diskutovaný	k2eAgInSc1d1	diskutovaný
<g/>
,	,	kIx,	,
ctěný	ctěný	k2eAgInSc1d1	ctěný
a	a	k8xC	a
nenáviděný	nenáviděný	k2eAgInSc1d1	nenáviděný
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
Tizianových	tizianový	k2eAgInPc2d1	tizianový
obrazů	obraz	k1gInPc2	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
běžným	běžný	k2eAgInSc7d1	běžný
návrhem	návrh	k1gInSc7	návrh
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
odráží	odrážet	k5eAaImIp3nS	odrážet
myšlenky	myšlenka	k1gFnSc2	myšlenka
renesančního	renesanční	k2eAgInSc2d1	renesanční
novoplatonismu	novoplatonismus	k1gInSc2	novoplatonismus
o	o	k7c4	o
"	"	kIx"	"
<g/>
osvobození	osvobození	k1gNnSc4	osvobození
ducha	duch	k1gMnSc2	duch
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
hlubšího	hluboký	k2eAgInSc2d2	hlubší
vhledu	vhled	k1gInSc2	vhled
či	či	k8xC	či
větší	veliký	k2eAgFnSc2d2	veliký
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
myšlenky	myšlenka	k1gFnPc1	myšlenka
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
spojovány	spojovat	k5eAaImNgFnP	spojovat
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
slavným	slavný	k2eAgNnSc7d1	slavné
zobrazením	zobrazení	k1gNnSc7	zobrazení
stahování	stahování	k1gNnPc2	stahování
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
:	:	kIx,	:
stažená	stažený	k2eAgFnSc1d1	stažená
kůže	kůže	k1gFnSc1	kůže
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
autoportrétní	autoportrétní	k2eAgFnSc4d1	autoportrétní
tvář	tvář	k1gFnSc4	tvář
Michelangela	Michelangel	k1gMnSc2	Michelangel
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
drží	držet	k5eAaImIp3nS	držet
svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
atribut	atribut	k1gInSc4	atribut
na	na	k7c6	na
fresce	freska	k1gFnSc6	freska
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Sixtinské	sixtinský	k2eAgFnSc6d1	Sixtinská
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
Michelangelových	Michelangelův	k2eAgFnPc2d1	Michelangelova
básní	báseň	k1gFnPc2	báseň
využívá	využívat	k5eAaImIp3nS	využívat
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odhazuje	odhazovat	k5eAaImIp3nS	odhazovat
starou	starý	k2eAgFnSc4d1	stará
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
metaforu	metafora	k1gFnSc4	metafora
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
začal	začít	k5eAaPmAgMnS	začít
Dante	Dante	k1gMnSc1	Dante
svůj	svůj	k3xOyFgInSc4	svůj
Ráj	ráj	k1gInSc4	ráj
modlitbou	modlitba	k1gFnSc7	modlitba
adresovanou	adresovaný	k2eAgFnSc4d1	adresovaná
Apollovi	Apoll	k1gMnSc3	Apoll
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
prosí	prosit	k5eAaImIp3nS	prosit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	V	kA	V
má	mít	k5eAaImIp3nS	mít
ňadra	ňadro	k1gNnSc2	ňadro
sestup	sestup	k1gInSc4	sestup
<g/>
,	,	kIx,	,
dýchej	dýchat	k5eAaImRp2nS	dýchat
<g />
.	.	kIx.	.
</s>
<s>
zas	zas	k6eAd1	zas
v	v	k7c6	v
jich	on	k3xPp3gMnPc2	on
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
/	/	kIx~	/
jak	jak	k8xC	jak
učinils	učinils	k1gInSc1	učinils
<g/>
,	,	kIx,	,
Marsya	Marsya	k1gFnSc1	Marsya
ve	v	k7c6	v
zápasu	zápas	k1gInSc6	zápas
/	/	kIx~	/
když	když	k8xS	když
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
jsi	být	k5eAaImIp2nS	být
z	z	k7c2	z
pochvy	pochva	k1gFnSc2	pochva
jeho	jeho	k3xOp3gMnPc2	jeho
údů	úd	k1gMnPc2	úd
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
jiných	jiný	k2eAgNnPc6d1	jiné
renesančních	renesanční	k2eAgNnPc6d1	renesanční
zobrazeních	zobrazení	k1gNnPc6	zobrazení
se	se	k3xPyFc4	se
Marsyova	Marsyův	k2eAgFnSc1d1	Marsyův
srstnatá	srstnatý	k2eAgFnSc1d1	srstnatá
noha	noha	k1gFnSc1	noha
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
stahována	stahován	k2eAgFnSc1d1	stahována
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
satyr	satyr	k1gMnSc1	satyr
"	"	kIx"	"
<g/>
vykoupen	vykoupit	k5eAaPmNgMnS	vykoupit
ze	z	k7c2	z
zvířeckosti	zvířeckost	k1gFnSc2	zvířeckost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
strašnému	strašný	k2eAgInSc3d1	strašný
osudu	osud	k1gInSc3	osud
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Filozofka	filozofka	k1gFnSc1	filozofka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Iris	iris	k1gFnSc1	iris
Murdochová	Murdochová	k1gFnSc1	Murdochová
byla	být	k5eAaImAgFnS	být
obzvláště	obzvláště	k6eAd1	obzvláště
fascinována	fascinován	k2eAgFnSc1d1	fascinována
tímto	tento	k3xDgInSc7	tento
obrazem	obraz	k1gInSc7	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
popsala	popsat	k5eAaPmAgFnS	popsat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc4d3	veliký
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
kánonu	kánon	k1gInSc6	kánon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zmínila	zmínit	k5eAaPmAgFnS	zmínit
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
svých	svůj	k3xOyFgInPc6	svůj
románech	román	k1gInPc6	román
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
její	její	k3xOp3gFnPc1	její
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
portrétu	portrét	k1gInSc6	portrét
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
Národní	národní	k2eAgFnSc4d1	národní
portrétní	portrétní	k2eAgFnSc4d1	portrétní
galerii	galerie	k1gFnSc4	galerie
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Tom	Tom	k1gMnSc1	Tom
Phillips	Phillipsa	k1gFnPc2	Phillipsa
<g/>
,	,	kIx,	,
zabírá	zabírat	k5eAaImIp3nS	zabírat
reprodukce	reprodukce	k1gFnSc1	reprodukce
Apolla	Apollo	k1gNnSc2	Apollo
a	a	k8xC	a
Marsya	Marsya	k1gMnSc1	Marsya
většinu	většina	k1gFnSc4	většina
zdi	zeď	k1gFnSc2	zeď
za	za	k7c7	za
její	její	k3xOp3gFnSc7	její
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
stýká	stýkat	k5eAaImIp3nS	stýkat
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
životem	život	k1gInSc7	život
a	a	k8xC	a
všemi	všecek	k3xTgInPc7	všecek
jeho	jeho	k3xOp3gFnPc7	jeho
nejednoznačnostmi	nejednoznačnost	k1gFnPc7	nejednoznačnost
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
jeho	jeho	k3xOp3gFnPc7	jeho
hrůzami	hrůza	k1gFnPc7	hrůza
a	a	k8xC	a
děsy	děs	k1gInPc7	děs
a	a	k8xC	a
bídou	bída	k1gFnSc7	bída
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
něco	něco	k3yInSc1	něco
krásného	krásný	k2eAgMnSc2d1	krásný
<g/>
;	;	kIx,	;
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
vstupem	vstup	k1gInSc7	vstup
duchovna	duchovno	k1gNnSc2	duchovno
do	do	k7c2	do
lidské	lidský	k2eAgFnSc2d1	lidská
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
s	s	k7c7	s
blízkostí	blízkost	k1gFnSc7	blízkost
bohů	bůh	k1gMnPc2	bůh
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
Marsyův	Marsyův	k2eAgInSc1d1	Marsyův
příběh	příběh	k1gInSc1	příběh
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
ukázka	ukázka	k1gFnSc1	ukázka
nevyhnutelné	vyhnutelný	k2eNgFnSc2d1	nevyhnutelná
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
následovala	následovat	k5eAaImAgFnS	následovat
pýchu	pýcha	k1gFnSc4	pýcha
(	(	kIx(	(
<g/>
hybris	hybris	k1gFnSc4	hybris
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
výzvy	výzva	k1gFnSc2	výzva
určené	určený	k2eAgFnSc2d1	určená
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
soutěže	soutěž	k1gFnSc2	soutěž
vynucující	vynucující	k2eAgFnSc1d1	vynucující
celkovou	celkový	k2eAgFnSc7d1	celková
morální	morální	k2eAgFnSc7d1	morální
a	a	k8xC	a
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
dvorní	dvorní	k2eAgFnSc2d1	dvorní
lyry	lyra	k1gFnSc2	lyra
nebo	nebo	k8xC	nebo
moderních	moderní	k2eAgInPc2d1	moderní
strunných	strunný	k2eAgInPc2d1	strunný
nástrojů	nástroj	k1gInPc2	nástroj
nad	nad	k7c7	nad
rustikální	rustikální	k2eAgFnSc7d1	rustikální
a	a	k8xC	a
frivolní	frivolní	k2eAgFnSc7d1	frivolní
rodinou	rodina	k1gFnSc7	rodina
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
starověkých	starověký	k2eAgInPc6d1	starověký
pramenech	pramen	k1gInPc6	pramen
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
si	se	k3xPyFc3	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
význam	význam	k1gInSc4	význam
i	i	k9	i
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Gowing	Gowing	k1gInSc1	Gowing
komentuje	komentovat	k5eAaBmIp3nS	komentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
zákona	zákon	k1gInSc2	zákon
harmonických	harmonický	k2eAgInPc2d1	harmonický
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zní	znět	k5eAaImIp3nS	znět
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
že	že	k8xS	že
Apollo	Apollo	k1gMnSc1	Apollo
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
chaotickým	chaotický	k2eAgInSc7d1	chaotický
a	a	k8xC	a
impulzivním	impulzivní	k2eAgInSc7d1	impulzivní
zvukem	zvuk	k1gInSc7	zvuk
trub	trouba	k1gFnPc2	trouba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
Edgara	Edgar	k1gMnSc4	Edgar
Winda	Winda	k1gFnSc1	Winda
soutěž	soutěž	k1gFnSc4	soutěž
ukázala	ukázat	k5eAaPmAgFnS	ukázat
"	"	kIx"	"
<g/>
poměr	poměr	k1gInSc4	poměr
sil	síla	k1gFnPc2	síla
dionýsovské	dionýsovský	k2eAgFnSc2d1	dionýsovský
temnoty	temnota	k1gFnSc2	temnota
a	a	k8xC	a
apollonské	apollonský	k2eAgFnSc2d1	apollonský
<g />
.	.	kIx.	.
</s>
<s>
jasnosti	jasnost	k1gFnPc1	jasnost
<g/>
;	;	kIx,	;
a	a	k8xC	a
jestliže	jestliže	k8xS	jestliže
soutěž	soutěž	k1gFnSc1	soutěž
skončila	skončit	k5eAaPmAgFnS	skončit
stažením	stažení	k1gNnSc7	stažení
Marsya	Marsyus	k1gMnSc2	Marsyus
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
stahování	stahování	k1gNnSc4	stahování
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
bylo	být	k5eAaImAgNnS	být
samo	sám	k3xTgNnSc1	sám
dionýsovským	dionýsovský	k2eAgInSc7d1	dionýsovský
rituálem	rituál	k1gInSc7	rituál
<g/>
,	,	kIx,	,
tragickým	tragický	k2eAgInSc7d1	tragický
ritem	rit	k1gInSc7	rit
očisty	očista	k1gFnSc2	očista
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
odhazuje	odhazovat	k5eAaImIp3nS	odhazovat
ošklivost	ošklivost	k1gFnSc1	ošklivost
vnějšího	vnější	k2eAgMnSc2d1	vnější
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
krása	krása	k1gFnSc1	krása
jeho	jeho	k3xOp3gInSc2	jeho
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xC	jako
alternativní	alternativní	k2eAgInSc4d1	alternativní
výklad	výklad	k1gInSc4	výklad
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
zvažováno	zvažován	k2eAgNnSc1d1	zvažováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
má	mít	k5eAaImIp3nS	mít
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
obecný	obecný	k2eAgInSc1d1	obecný
nebo	nebo	k8xC	nebo
specifický	specifický	k2eAgInSc1d1	specifický
<g/>
,	,	kIx,	,
a	a	k8xC	a
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
"	"	kIx"	"
<g/>
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
trest	trest	k1gInSc1	trest
<g/>
"	"	kIx"	"
pyšných	pyšný	k2eAgMnPc2d1	pyšný
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
"	"	kIx"	"
<g/>
tři	tři	k4xCgInPc1	tři
věky	věk	k1gInPc1	věk
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
smíme	smět	k5eAaImIp1nP	smět
počítat	počítat	k5eAaImF	počítat
satyry	satyr	k1gMnPc4	satyr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
jsou	být	k5eAaImIp3nP	být
vpravo	vpravo	k6eAd1	vpravo
vyrovnány	vyrovnat	k5eAaBmNgInP	vyrovnat
po	po	k7c6	po
diagonále	diagonála	k1gFnSc6	diagonála
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
nebo	nebo	k8xC	nebo
mladý	mladý	k2eAgMnSc1d1	mladý
satyr	satyr	k1gMnSc1	satyr
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
prázdným	prázdný	k2eAgInSc7d1	prázdný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
diváka	divák	k1gMnSc4	divák
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
nejlepších	dobrý	k2eAgNnPc6d3	nejlepší
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nP	soustředit
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
úkoly	úkol	k1gInPc4	úkol
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
starý	starý	k2eAgMnSc1d1	starý
Midas	Midas	k1gMnSc1	Midas
scénu	scéna	k1gFnSc4	scéna
očividně	očividně	k6eAd1	očividně
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
s	s	k7c7	s
melancholickou	melancholický	k2eAgFnSc7d1	melancholická
rezignací	rezignace	k1gFnSc7	rezignace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepokouší	pokoušet	k5eNaImIp3nS	pokoušet
se	se	k3xPyFc4	se
už	už	k9	už
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
malba	malba	k1gFnSc1	malba
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
příjemce	příjemce	k1gMnSc4	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
klientem	klient	k1gMnSc7	klient
Tiziana	Tizian	k1gMnSc2	Tizian
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
dochované	dochovaný	k2eAgFnSc6d1	dochovaná
korespondenci	korespondence	k1gFnSc6	korespondence
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zůstaly	zůstat	k5eAaPmAgInP	zůstat
v	v	k7c6	v
Tizianově	tizianově	k6eAd1	tizianově
ateliéru	ateliér	k1gInSc2	ateliér
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgFnPc2	takový
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Smrt	smrt	k1gFnSc1	smrt
Actaeonova	Actaeonův	k2eAgFnSc1d1	Actaeonův
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
jistě	jistě	k6eAd1	jistě
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
Filipa	Filip	k1gMnSc4	Filip
<g/>
)	)	kIx)	)
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
obecně	obecně	k6eAd1	obecně
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
dokončené	dokončený	k2eAgNnSc4d1	dokončené
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
mohou	moct	k5eAaImIp3nP	moct
podpis	podpis	k1gInSc4	podpis
a	a	k8xC	a
detailně	detailně	k6eAd1	detailně
dokončené	dokončený	k2eAgFnPc4d1	dokončená
části	část	k1gFnPc4	část
obrazu	obraz	k1gInSc2	obraz
naznačovat	naznačovat	k5eAaImF	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Hans	Hans	k1gMnSc1	Hans
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zakázkou	zakázka	k1gFnSc7	zakázka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Tizianovi	Tizian	k1gMnSc3	Tizian
zadala	zadat	k5eAaPmAgFnS	zadat
Marie	Marie	k1gFnSc1	Marie
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Binche	Binche	k1gFnSc6	Binche
u	u	k7c2	u
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
by	by	kYmCp3nS	by
plynula	plynout	k5eAaImAgFnS	plynout
datace	datace	k1gFnSc1	datace
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1550	[number]	k4	1550
<g/>
–	–	k?	–
<g/>
1576	[number]	k4	1576
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
objednala	objednat	k5eAaPmAgFnS	objednat
rovněž	rovněž	k9	rovněž
obrazy	obraz	k1gInPc4	obraz
na	na	k7c4	na
ovidiovská	ovidiovský	k2eAgNnPc4d1	ovidiovský
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tizian	Tizian	k1gInSc4	Tizian
Marsyův	Marsyův	k2eAgInSc1d1	Marsyův
příběh	příběh	k1gInSc1	příběh
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
dvakrát	dvakrát	k6eAd1	dvakrát
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc4d1	budoucí
kroměřížskou	kroměřížský	k2eAgFnSc4d1	Kroměřížská
verzi	verze	k1gFnSc4	verze
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
kopie	kopie	k1gFnSc1	kopie
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
z	z	k7c2	z
Binche	Binch	k1gFnSc2	Binch
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
<g/>
Nic	nic	k3yNnSc1	nic
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
obrazu	obraz	k1gInSc2	obraz
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
inventáři	inventář	k1gInSc6	inventář
arundelovské	arundelovský	k2eAgFnSc2d1	arundelovský
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
amsterodamském	amsterodamský	k2eAgInSc6d1	amsterodamský
exilu	exil	k1gInSc6	exil
následkem	následkem	k7c2	následkem
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
inventáři	inventář	k1gInSc6	inventář
uveden	uvést	k5eAaPmNgMnS	uvést
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
356	[number]	k4	356
jako	jako	k8xC	jako
Titiano	Titiana	k1gFnSc5	Titiana
–	–	k?	–
Marsias	Marsias	k1gInSc4	Marsias
Scortigato	Scortigat	k2eAgNnSc1d1	Scortigat
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tizian	Tizian	k1gInSc1	Tizian
–	–	k?	–
Marsyas	Marsyas	k1gInSc1	Marsyas
stahovaný	stahovaný	k2eAgInSc1d1	stahovaný
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Thomas	Thomas	k1gMnSc1	Thomas
Howard	Howard	k1gMnSc1	Howard
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Arundelu	Arundel	k1gInSc2	Arundel
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Alethea	Alethe	k2eAgFnSc1d1	Alethe
Howardová	Howardová	k1gFnSc1	Howardová
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
rozptýlena	rozptýlit	k5eAaPmNgFnS	rozptýlit
jejich	jejich	k3xOp3gMnSc7	jejich
synem	syn	k1gMnSc7	syn
po	po	k7c6	po
Aletheině	Alethein	k2eAgFnSc6d1	Alethein
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
sbírky	sbírka	k1gFnSc2	sbírka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Howard	Howard	k1gMnSc1	Howard
trávil	trávit	k5eAaImAgMnS	trávit
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
čas	čas	k1gInSc4	čas
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
koupen	koupit	k5eAaPmNgInS	koupit
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
tradice	tradice	k1gFnSc2	tradice
ho	on	k3xPp3gNnSc4	on
hraběnka	hraběnka	k1gFnSc1	hraběnka
Arundelová	Arundelová	k1gFnSc1	Arundelová
koupila	koupit	k5eAaPmAgFnS	koupit
od	od	k7c2	od
umělcova	umělcův	k2eAgMnSc2d1	umělcův
synovce	synovec	k1gMnSc2	synovec
Tizianella	Tizianell	k1gMnSc2	Tizianell
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
<g/>
.	.	kIx.	.
<g/>
Obraz	obraz	k1gInSc1	obraz
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1655	[number]	k4	1655
koupil	koupit	k5eAaPmAgMnS	koupit
Franz	Franz	k1gMnSc1	Franz
Imstenraed	Imstenraed	k1gMnSc1	Imstenraed
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synovcem	synovec	k1gMnSc7	synovec
Everharda	Everhard	k1gMnSc2	Everhard
Jabacha	Jabach	k1gMnSc2	Jabach
<g/>
,	,	kIx,	,
bankéře	bankéř	k1gMnSc2	bankéř
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
soukromých	soukromý	k2eAgMnPc2d1	soukromý
sběratelů	sběratel	k1gMnPc2	sběratel
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k9	také
agentem	agent	k1gMnSc7	agent
kardinála	kardinál	k1gMnSc2	kardinál
Mazarina	Mazarin	k1gMnSc2	Mazarin
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
rozptýlena	rozptýlen	k2eAgFnSc1d1	rozptýlena
vynikající	vynikající	k2eAgFnSc1d1	vynikající
sbírka	sbírka	k1gFnSc1	sbírka
Arundelova	Arundelův	k2eAgMnSc2d1	Arundelův
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
a	a	k8xC	a
Jabach	Jabach	k1gMnSc1	Jabach
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
,	,	kIx,	,
jednající	jednající	k2eAgMnSc1d1	jednající
jménem	jméno	k1gNnSc7	jméno
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
kupců	kupec	k1gMnPc2	kupec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1673	[number]	k4	1673
obraz	obraz	k1gInSc1	obraz
získal	získat	k5eAaPmAgInS	získat
jako	jako	k9	jako
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
loterii	loterie	k1gFnSc6	loterie
hrabě	hrabě	k1gMnSc1	hrabě
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Lichtenštejna-Kastelkornu	Lichtenštejna-Kastelkorn	k1gInSc2	Lichtenštejna-Kastelkorn
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
.	.	kIx.	.
</s>
<s>
Koupil	koupit	k5eAaPmAgInS	koupit
také	také	k9	také
většinu	většina	k1gFnSc4	většina
sbírky	sbírka	k1gFnSc2	sbírka
bratří	bratřit	k5eAaImIp3nS	bratřit
Bernharda	Bernharda	k1gFnSc1	Bernharda
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
Imstenraedových	Imstenraedův	k2eAgMnPc2d1	Imstenraedův
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vcelku	vcelku	k6eAd1	vcelku
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
,	,	kIx,	,
bývalém	bývalý	k2eAgNnSc6d1	bývalé
arcibiskupském	arcibiskupský	k2eAgNnSc6d1	Arcibiskupské
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
<g/>
Technické	technický	k2eAgInPc1d1	technický
průzkumy	průzkum	k1gInPc1	průzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
malbou	malba	k1gFnSc7	malba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nešetrně	šetrně	k6eNd1	šetrně
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
překládána	překládán	k2eAgNnPc1d1	překládáno
<g/>
,	,	kIx,	,
přemalovávána	přemalováván	k2eAgNnPc1d1	přemalováván
a	a	k8xC	a
drasticky	drasticky	k6eAd1	drasticky
čištěna	čistit	k5eAaImNgFnS	čistit
žíravinou	žíravina	k1gFnSc7	žíravina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
místy	místy	k6eAd1	místy
prodřelo	prodřít	k5eAaPmAgNnS	prodřít
až	až	k9	až
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
obraz	obraz	k1gInSc1	obraz
měřil	měřit	k5eAaImAgInS	měřit
asi	asi	k9	asi
225	[number]	k4	225
x	x	k?	x
199	[number]	k4	199
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
oříznut	oříznout	k5eAaPmNgMnS	oříznout
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
a	a	k8xC	a
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
a	a	k8xC	a
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
přišitím	přišití	k1gNnSc7	přišití
pruhů	pruh	k1gInPc2	pruh
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
nejméně	málo	k6eAd3	málo
šestkrát	šestkrát	k6eAd1	šestkrát
opravován	opravován	k2eAgInSc1d1	opravován
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
tři	tři	k4xCgInPc1	tři
restaurátorské	restaurátorský	k2eAgInPc1d1	restaurátorský
zásahy	zásah	k1gInPc1	zásah
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
práce	práce	k1gFnSc2	práce
provedl	provést	k5eAaPmAgMnS	provést
Victor	Victor	k1gMnSc1	Victor
Jasper	Jasper	k1gMnSc1	Jasper
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
František	František	k1gMnSc1	František
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
František	František	k1gMnSc1	František
Sysel	Sysel	k1gMnSc1	Sysel
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Sysel	Sysel	k1gMnSc1	Sysel
také	také	k9	také
vykonal	vykonat	k5eAaPmAgMnS	vykonat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oprav	oprava	k1gFnPc2	oprava
důkladný	důkladný	k2eAgInSc1d1	důkladný
průzkum	průzkum	k1gInSc1	průzkum
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
odhalil	odhalit	k5eAaPmAgMnS	odhalit
dosud	dosud	k6eAd1	dosud
neznámou	známý	k2eNgFnSc4d1	neznámá
signaturu	signatura	k1gFnSc4	signatura
<g/>
.	.	kIx.	.
</s>
<s>
Syslova	Syslův	k2eAgFnSc1d1	Syslova
analýza	analýza	k1gFnSc1	analýza
i	i	k8xC	i
pozdější	pozdní	k2eAgInSc1d2	pozdější
průzkum	průzkum	k1gInSc1	průzkum
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
Tomáše	Tomáš	k1gMnSc2	Tomáš
a	a	k8xC	a
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Bergerových	Bergerových	k2eAgMnSc2d1	Bergerových
provedený	provedený	k2eAgInSc4d1	provedený
technikou	technika	k1gFnSc7	technika
infračervené	infračervený	k2eAgFnPc1d1	infračervená
reflektografie	reflektografie	k1gFnPc1	reflektografie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
obrazu	obraz	k1gInSc2	obraz
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přemalovával	přemalovávat	k5eAaImAgMnS	přemalovávat
a	a	k8xC	a
lze	lze	k6eAd1	lze
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
zjistit	zjistit	k5eAaPmF	zjistit
i	i	k9	i
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
barevných	barevný	k2eAgFnPc2d1	barevná
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
<g/>
Prvním	první	k4xOgMnSc7	první
historikem	historik	k1gMnSc7	historik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obrazem	obraz	k1gInSc7	obraz
podrobněji	podrobně	k6eAd2	podrobně
zabýval	zabývat	k5eAaImAgMnS	zabývat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Dostál	Dostál	k1gMnSc1	Dostál
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
symfonii	symfonie	k1gFnSc4	symfonie
barevných	barevný	k2eAgFnPc2d1	barevná
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připsal	připsat	k5eAaPmAgMnS	připsat
Tizianovi	Tizian	k1gMnSc3	Tizian
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
jako	jako	k9	jako
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1	pravděpodobná
dobu	doba	k1gFnSc4	doba
vzniku	vznik	k1gInSc6	vznik
roky	rok	k1gInPc4	rok
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
obrazu	obraz	k1gInSc3	obraz
věnovala	věnovat	k5eAaPmAgFnS	věnovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
interpretů	interpret	k1gMnPc2	interpret
<g/>
;	;	kIx,	;
významný	významný	k2eAgMnSc1d1	významný
byl	být	k5eAaImAgMnS	být
přínos	přínos	k1gInSc4	přínos
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
vlivnou	vlivný	k2eAgFnSc4d1	vlivná
obsahovou	obsahový	k2eAgFnSc4d1	obsahová
interpretaci	interpretace	k1gFnSc4	interpretace
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
promyšlené	promyšlený	k2eAgFnSc2d1	promyšlená
novoplatónsko-křesťanské	novoplatónskořesťanský	k2eAgFnSc2d1	novoplatónsko-křesťanský
alegorie	alegorie	k1gFnSc2	alegorie
<g/>
,	,	kIx,	,
ztotožňující	ztotožňující	k2eAgNnPc1d1	ztotožňující
Apolla	Apollo	k1gNnPc1	Apollo
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
a	a	k8xC	a
Midase	Midas	k1gMnSc2	Midas
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
vysvobozovaným	vysvobozovaný	k2eAgMnSc7d1	vysvobozovaný
z	z	k7c2	z
pozemské	pozemský	k2eAgFnSc2d1	pozemská
schrány	schrána	k1gFnSc2	schrána
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
vyššímu	vysoký	k2eAgNnSc3d2	vyšší
poznání	poznání	k1gNnSc3	poznání
<g/>
,	,	kIx,	,
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
také	také	k9	také
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
postava	postava	k1gFnSc1	postava
Midase	Midas	k1gMnSc2	Midas
je	být	k5eAaImIp3nS	být
Tizianovým	tizianový	k2eAgInSc7d1	tizianový
autoportrétem	autoportrét	k1gInSc7	autoportrét
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
pak	pak	k6eAd1	pak
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xS	jako
exponát	exponát	k1gInSc4	exponát
výstavy	výstava	k1gFnSc2	výstava
nedokončených	dokončený	k2eNgNnPc2d1	nedokončené
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
Unfinished	Unfinished	k1gInSc1	Unfinished
<g/>
:	:	kIx,	:
Thoughts	Thoughts	k1gInSc1	Thoughts
Left	Left	k1gMnSc1	Left
Visible	Visible	k1gMnSc1	Visible
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Met	Mety	k1gFnPc2	Mety
Breuer	Breura	k1gFnPc2	Breura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vizuální	vizuální	k2eAgInPc1d1	vizuální
zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Postava	postava	k1gFnSc1	postava
Marsya	Marsya	k1gMnSc1	Marsya
byla	být	k5eAaImAgFnS	být
známým	známý	k2eAgNnSc7d1	známé
tématem	téma	k1gNnSc7	téma
římského	římský	k2eAgMnSc2d1	římský
a	a	k8xC	a
helénistického	helénistický	k2eAgNnSc2d1	helénistické
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zobrazení	zobrazení	k1gNnSc1	zobrazení
svázaného	svázaný	k2eAgMnSc2d1	svázaný
satyra	satyr	k1gMnSc2	satyr
visícího	visící	k2eAgMnSc2d1	visící
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
helénistickém	helénistický	k2eAgInSc6d1	helénistický
Pergamonu	pergamon	k1gInSc6	pergamon
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
využíváno	využívat	k5eAaPmNgNnS	využívat
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
renesanci	renesance	k1gFnSc6	renesance
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
ho	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgInS	použít
Raffael	Raffael	k1gInSc1	Raffael
na	na	k7c6	na
stropě	strop	k1gInSc6	strop
sálu	sál	k1gInSc2	sál
Stanza	Stanza	k1gFnSc1	Stanza
della	della	k1gFnSc1	della
Segnatura	Segnatura	k1gFnSc1	Segnatura
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
Tizianovu	Tizianův	k2eAgFnSc4d1	Tizianova
kompozici	kompozice	k1gFnSc4	kompozice
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
<g/>
Mnohem	mnohem	k6eAd1	mnohem
blíž	blízce	k6eAd2	blízce
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
ošklivý	ošklivý	k2eAgInSc1d1	ošklivý
náčrt	náčrt	k1gInSc1	náčrt
pro	pro	k7c4	pro
nyní	nyní	k6eAd1	nyní
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
fresku	freska	k1gFnSc4	freska
<g/>
"	"	kIx"	"
od	od	k7c2	od
Giulia	Giulius	k1gMnSc2	Giulius
Romana	Roman	k1gMnSc2	Roman
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Te	Te	k1gFnSc2	Te
v	v	k7c6	v
Mantově	Mantova	k1gFnSc6	Mantova
(	(	kIx(	(
<g/>
1524	[number]	k4	1524
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgNnSc2	jenž
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
kresba	kresba	k1gFnSc1	kresba
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
kompozice	kompozice	k1gFnSc2	kompozice
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Skica	skica	k1gFnSc1	skica
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
chlapce	chlapec	k1gMnSc2	chlapec
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
pozicích	pozice	k1gFnPc6	pozice
–	–	k?	–
včetně	včetně	k7c2	včetně
Marsya	Marsyum	k1gNnSc2	Marsyum
uvázaného	uvázaný	k2eAgNnSc2d1	uvázané
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Stojící	stojící	k2eAgFnSc1d1	stojící
postava	postava	k1gFnSc1	postava
vlevo	vlevo	k6eAd1	vlevo
je	být	k5eAaImIp3nS	být
sluha	sluha	k1gMnSc1	sluha
držící	držící	k2eAgFnSc4d1	držící
lyru	lyra	k1gFnSc4	lyra
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
Tizianův	Tizianův	k2eAgMnSc1d1	Tizianův
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
liru	lira	k1gFnSc4	lira
da	da	k?	da
bracchio	bracchio	k1gMnSc1	bracchio
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgFnSc1d1	sedící
postava	postava	k1gFnSc1	postava
Midase	Midas	k1gMnSc2	Midas
má	mít	k5eAaImIp3nS	mít
oslí	oslí	k2eAgNnPc4d1	oslí
uši	ucho	k1gNnPc4	ucho
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zřetelněji	zřetelně	k6eAd2	zřetelně
otřesena	otřesen	k2eAgFnSc1d1	otřesena
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nP	držet
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neklečí	klečet	k5eNaImIp3nS	klečet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sklání	sklánět	k5eAaImIp3nS	sklánět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
neřeže	řezat	k5eNaImIp3nS	řezat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stahuje	stahovat	k5eAaImIp3nS	stahovat
kůži	kůže	k1gFnSc4	kůže
jako	jako	k8xS	jako
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
<g/>
Midasův	Midasův	k2eAgInSc1d1	Midasův
soud	soud	k1gInSc1	soud
Andrey	Andrea	k1gFnSc2	Andrea
Schiavoneho	Schiavone	k1gMnSc2	Schiavone
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
královské	královský	k2eAgFnSc6d1	královská
sbírce	sbírka	k1gFnSc6	sbírka
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Kensingtonském	Kensingtonský	k2eAgInSc6d1	Kensingtonský
paláci	palác	k1gInSc6	palác
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
starší	starý	k2eAgInSc1d2	starší
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kolem	kolem	k7c2	kolem
let	léto	k1gNnPc2	léto
1548	[number]	k4	1548
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
Tizianem	Tizian	k1gInSc7	Tizian
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
několika	několik	k4yIc7	několik
detaily	detail	k1gInPc7	detail
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Tizian	Tizian	k1gInSc1	Tizian
"	"	kIx"	"
<g/>
si	se	k3xPyFc3	se
také	také	k9	také
musel	muset	k5eAaImAgMnS	muset
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
na	na	k7c4	na
prvky	prvek	k1gInPc4	prvek
tohoto	tento	k3xDgInSc2	tento
obrazu	obraz	k1gInSc2	obraz
<g/>
:	:	kIx,	:
zamyšlený	zamyšlený	k2eAgInSc1d1	zamyšlený
a	a	k8xC	a
odtažitý	odtažitý	k2eAgInSc1d1	odtažitý
postoj	postoj	k1gInSc1	postoj
Apolla	Apollo	k1gNnSc2	Apollo
a	a	k8xC	a
Midase	Midas	k1gMnSc2	Midas
i	i	k9	i
radikálně	radikálně	k6eAd1	radikálně
osvobozený	osvobozený	k2eAgInSc4d1	osvobozený
impresionistický	impresionistický	k2eAgInSc4d1	impresionistický
štětec	štětec	k1gInSc4	štětec
<g/>
,	,	kIx,	,
použitý	použitý	k2eAgInSc4d1	použitý
ke	k	k7c3	k
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
dramatické	dramatický	k2eAgFnSc2d1	dramatická
nálady	nálada	k1gFnSc2	nálada
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Apollo	Apollo	k1gMnSc1	Apollo
tu	tu	k6eAd1	tu
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
liru	lira	k1gFnSc4	lira
da	da	k?	da
bracchio	bracchio	k1gMnSc1	bracchio
a	a	k8xC	a
Midas	Midas	k1gMnSc1	Midas
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
normálníma	normální	k2eAgNnPc7d1	normální
ušima	ucho	k1gNnPc7	ucho
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
Pana	Pan	k1gMnSc4	Pan
<g/>
.	.	kIx.	.
<g/>
Midas	Midas	k1gMnSc1	Midas
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Midas	Midas	k1gMnSc1	Midas
/	/	kIx~	/
Tizian	Tizian	k1gMnSc1	Tizian
přebírá	přebírat	k5eAaImIp3nS	přebírat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
melancholickou	melancholický	k2eAgFnSc4d1	melancholická
pózu	póza	k1gFnSc4	póza
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
kterou	který	k3yQgFnSc4	který
Tizian	Tizian	k1gInSc1	Tizian
znal	znát	k5eAaImAgInS	znát
z	z	k7c2	z
Rafaelova	Rafaelův	k2eAgInSc2d1	Rafaelův
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
portrétu	portrét	k1gInSc2	portrét
Michelangela	Michelangela	k1gFnSc1	Michelangela
jako	jako	k8xS	jako
Hérakleita	Hérakleita	k1gFnSc1	Hérakleita
v	v	k7c6	v
Aténské	aténský	k2eAgFnSc6d1	aténská
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
Melancholie	melancholie	k1gFnSc2	melancholie
I	i	k8xC	i
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürer	k1gMnSc2	Dürer
<g/>
,	,	kIx,	,
nemluvě	nemluva	k1gFnSc3	nemluva
o	o	k7c4	o
Schiavonem	Schiavon	k1gMnSc7	Schiavon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technika	technika	k1gFnSc1	technika
a	a	k8xC	a
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Technika	technika	k1gFnSc1	technika
malby	malba	k1gFnSc2	malba
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
Tizianův	Tizianův	k2eAgInSc4d1	Tizianův
pozdní	pozdní	k2eAgInSc4d1	pozdní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Penny	penny	k1gInSc2	penny
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přítomná	přítomný	k2eAgFnSc1d1	přítomná
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
zeleň	zeleň	k1gFnSc1	zeleň
a	a	k8xC	a
modř	modř	k1gFnSc1	modř
a	a	k8xC	a
červeň	červeň	k1gFnSc1	červeň
(	(	kIx(	(
<g/>
rumělka	rumělka	k1gFnSc1	rumělka
i	i	k8xC	i
karmín	karmín	k1gInSc1	karmín
<g/>
)	)	kIx)	)
...	...	k?	...
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
Tizian	Tizian	k1gMnSc1	Tizian
začal	začít	k5eAaPmAgMnS	začít
tyto	tento	k3xDgFnPc4	tento
barvy	barva	k1gFnPc4	barva
aplikovat	aplikovat	k5eAaBmF	aplikovat
jako	jako	k8xC	jako
lazury	lazur	k1gInPc4	lazur
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pomocí	pomocí	k7c2	pomocí
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jen	jen	k9	jen
zpola	zpola	k6eAd1	zpola
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
formě	forma	k1gFnSc3	forma
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
plavou	plavat	k5eAaImIp3nP	plavat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
malby	malba	k1gFnSc2	malba
<g/>
:	:	kIx,	:
zřejmě	zřejmě	k6eAd1	zřejmě
je	být	k5eAaImIp3nS	být
přidával	přidávat	k5eAaImAgMnS	přidávat
nakonec	nakonec	k6eAd1	nakonec
...	...	k?	...
stále	stále	k6eAd1	stále
vidíme	vidět	k5eAaImIp1nP	vidět
úseky	úsek	k1gInPc1	úsek
kouřové	kouřový	k2eAgFnSc2d1	kouřová
předběžné	předběžný	k2eAgFnSc2d1	předběžná
malby	malba	k1gFnSc2	malba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
přízračné	přízračný	k2eAgNnSc4d1	přízračné
Apollovo	Apollův	k2eAgNnSc4d1	Apollův
tělo	tělo	k1gNnSc4	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
jazyky	jazyk	k1gInPc1	jazyk
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
stuhy	stuha	k1gFnSc2	stuha
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
jsou	být	k5eAaImIp3nP	být
zářivě	zářivě	k6eAd1	zářivě
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
popisuje	popisovat	k5eAaImIp3nS	popisovat
skupinu	skupina	k1gFnSc4	skupina
pozdních	pozdní	k2eAgFnPc2d1	pozdní
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
dokončené	dokončený	k2eAgNnSc4d1	dokončené
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tohoto	tento	k3xDgInSc2	tento
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Aktaiónovou	Aktaiónový	k2eAgFnSc7d1	Aktaiónový
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
<g/>
,	,	kIx,	,
Penny	penny	k1gInSc1	penny
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
efekt	efekt	k1gInSc1	efekt
rozostřených	rozostřený	k2eAgFnPc2d1	rozostřená
částí	část	k1gFnPc2	část
malby	malba	k1gFnSc2	malba
(	(	kIx(	(
<g/>
...	...	k?	...
neproniknutelné	proniknutelný	k2eNgFnSc2d1	neproniknutelná
dálky	dálka	k1gFnSc2	dálka
<g/>
,	,	kIx,	,
prosvětlené	prosvětlený	k2eAgFnSc2d1	prosvětlená
<g />
.	.	kIx.	.
</s>
<s>
drobné	drobná	k1gFnSc2	drobná
olistění	olistěný	k2eAgMnPc1d1	olistěný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvýrazněn	zvýraznit	k5eAaPmNgInS	zvýraznit
kontrastem	kontrast	k1gInSc7	kontrast
s	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
detailněji	detailně	k6eAd2	detailně
promodelovány	promodelován	k2eAgInPc1d1	promodelován
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
zčásti	zčásti	k6eAd1	zčásti
malovány	malován	k2eAgMnPc4d1	malován
nahrubo	nahrubo	k6eAd1	nahrubo
<g/>
,	,	kIx,	,
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
malovány	malovat	k5eAaImNgFnP	malovat
detailně	detailně	k6eAd1	detailně
(	(	kIx(	(
<g/>
Midasova	Midasův	k2eAgFnSc1d1	Midasův
koruna	koruna	k1gFnSc1	koruna
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
zde	zde	k6eAd1	zde
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
důrazy	důraz	k1gInPc4	důraz
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
na	na	k7c6	na
čepelích	čepel	k1gFnPc6	čepel
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
na	na	k7c6	na
vědru	vědro	k1gNnSc6	vědro
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Tarquinus	Tarquinus	k1gInSc1	Tarquinus
a	a	k8xC	a
Lukrécie	Lukrécie	k1gFnSc1	Lukrécie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Tizian	Tizian	k1gInSc1	Tizian
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
čistě	čistě	k6eAd1	čistě
technických	technický	k2eAgInPc2d1	technický
prostředků	prostředek	k1gInPc2	prostředek
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
kritici	kritik	k1gMnPc1	kritik
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
dopad	dopad	k1gInSc4	dopad
Tizianovy	Tizianovy	k?	Tizianovy
obrazové	obrazový	k2eAgFnSc2d1	obrazová
řeči	řeč	k1gFnSc2	řeč
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc6d1	ostatní
pozdních	pozdní	k2eAgInPc6d1	pozdní
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sidneyho	Sidney	k1gMnSc2	Sidney
Freedburga	Freedburg	k1gMnSc2	Freedburg
"	"	kIx"	"
<g/>
povrchy	povrch	k1gInPc4	povrch
těl	tělo	k1gNnPc2	tělo
stříbřitě	stříbřitě	k6eAd1	stříbřitě
září	září	k1gNnSc2	září
a	a	k8xC	a
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
hustá	hustý	k2eAgFnSc1d1	hustá
k	k	k7c3	k
nedýchání	nedýchání	k1gNnSc3	nedýchání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
doutnající	doutnající	k2eAgInSc1d1	doutnající
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
...	...	k?	...
Jízlivá	jízlivý	k2eAgFnSc1d1	jízlivá
groteska	groteska	k1gFnSc1	groteska
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
uprostřed	uprostřed	k7c2	uprostřed
krutosti	krutost	k1gFnSc2	krutost
<g/>
,	,	kIx,	,
ošklivosti	ošklivost	k1gFnSc2	ošklivost
a	a	k8xC	a
podivnosti	podivnost	k1gFnSc2	podivnost
obklopené	obklopený	k2eAgInPc1d1	obklopený
úžasnou	úžasný	k2eAgFnSc7d1	úžasná
krásou	krása	k1gFnSc7	krása
<g/>
;	;	kIx,	;
hrůza	hrůza	k1gFnSc1	hrůza
tu	tu	k6eAd1	tu
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
pocit	pocit	k1gInSc4	pocit
vznešenosti	vznešenost	k1gFnSc2	vznešenost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Steera	Steera	k1gFnSc1	Steera
"	"	kIx"	"
<g/>
nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tu	tu	k6eAd1	tu
hovoří	hovořit	k5eAaImIp3nP	hovořit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všudypřítomné	všudypřítomný	k2eAgNnSc1d1	všudypřítomné
vyzařování	vyzařování	k1gNnSc1	vyzařování
přerývaných	přerývaný	k2eAgInPc2d1	přerývaný
dotyků	dotyk	k1gInPc2	dotyk
štětce	štětec	k1gInSc2	štětec
<g/>
,	,	kIx,	,
evokující	evokující	k2eAgFnSc1d1	evokující
zelenkavé	zelenkavý	k2eAgNnSc4d1	zelenkavé
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
potřísněno	potřísnit	k5eAaPmNgNnS	potřísnit
skvrnami	skvrna	k1gFnPc7	skvrna
červeně	červeň	k1gFnSc2	červeň
jako	jako	k8xS	jako
stříkanci	stříkanec	k1gInSc3	stříkanec
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Vize	vize	k1gFnSc1	vize
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc1	vyjádření
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sjednotily	sjednotit	k5eAaPmAgFnP	sjednotit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
diskuse	diskuse	k1gFnSc2	diskuse
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
...	...	k?	...
Tizianův	Tizianův	k2eAgInSc1d1	Tizianův
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
boj	boj	k1gInSc1	boj
o	o	k7c4	o
malířské	malířský	k2eAgNnSc4d1	malířské
zachycení	zachycení	k1gNnSc4	zachycení
fyzické	fyzický	k2eAgFnSc2d1	fyzická
reality	realita	k1gFnSc2	realita
scény	scéna	k1gFnSc2	scéna
pomocí	pomocí	k7c2	pomocí
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
je	být	k5eAaImIp3nS	být
fundamentální	fundamentální	k2eAgFnSc7d1	fundamentální
součástí	součást	k1gFnSc7	součást
smyslu	smysl	k1gInSc2	smysl
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
(	(	kIx(	(
<g/>
Titian	Titian	k1gInSc1	Titian
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Bull	bulla	k1gFnPc2	bulla
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Mirror	Mirror	k1gMnSc1	Mirror
of	of	k?	of
the	the	k?	the
Gods	Gods	k1gInSc1	Gods
<g/>
,	,	kIx,	,
How	How	k1gFnSc1	How
Renaissance	Renaissance	k1gFnSc2	Renaissance
Artists	Artistsa	k1gFnPc2	Artistsa
Rediscovered	Rediscovered	k1gMnSc1	Rediscovered
the	the	k?	the
Pagan	Pagan	k1gInSc1	Pagan
Gods	Gods	k1gInSc1	Gods
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
UP	UP	kA	UP
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0195219236	[number]	k4	0195219236
</s>
</p>
<p>
<s>
Brotton	Brotton	k1gInSc1	Brotton
<g/>
,	,	kIx,	,
Jerry	Jerr	k1gInPc1	Jerr
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
Sale	Sale	k1gNnSc1	Sale
of	of	k?	of
the	the	k?	the
Late	lat	k1gInSc5	lat
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Goods	Goodsa	k1gFnPc2	Goodsa
<g/>
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
I	i	k8xC	i
and	and	k?	and
His	his	k1gNnSc2	his
Art	Art	k1gFnSc2	Art
Collection	Collection	k1gInSc1	Collection
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Macmillan	Macmillan	k1gMnSc1	Macmillan
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
9780330427098	[number]	k4	9780330427098
</s>
</p>
<p>
<s>
Freedburg	Freedburg	k1gInSc1	Freedburg
<g/>
,	,	kIx,	,
Sidney	Sidney	k1gInPc1	Sidney
J.	J.	kA	J.
<g/>
.	.	kIx.	.
</s>
<s>
Painting	Painting	k1gInSc1	Painting
in	in	k?	in
Italy	Ital	k1gMnPc7	Ital
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
rd	rd	k?	rd
edn	edn	k?	edn
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Yale	Yale	k1gFnSc1	Yale
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0300055870	[number]	k4	0300055870
</s>
</p>
<p>
<s>
Glover	Glover	k1gMnSc1	Glover
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Great	Great	k1gInSc1	Great
Works	Works	kA	Works
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
(	(	kIx(	(
<g/>
c.	c.	k?	c.
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Titian	Titian	k1gMnSc1	Titian
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
<g/>
,	,	kIx,	,
28	[number]	k4	28
October	October	k1gInSc1	October
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Gowing	Gowing	k1gInSc1	Gowing
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
"	"	kIx"	"
<g/>
Human	Human	k1gMnSc1	Human
Stuff	Stuff	k1gMnSc1	Stuff
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Review	Review	k1gFnSc2	Review
of	of	k?	of
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
6	[number]	k4	6
No	no	k9	no
<g/>
.	.	kIx.	.
2	[number]	k4	2
·	·	k?	·
2	[number]	k4	2
February	Februara	k1gFnSc2	Februara
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
pages	pages	k1gInSc1	pages
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
Hale	hala	k1gFnSc3	hala
<g/>
,	,	kIx,	,
Sheila	Sheila	k1gFnSc1	Sheila
<g/>
,	,	kIx,	,
Titian	Titian	k1gInSc1	Titian
<g/>
,	,	kIx,	,
His	his	k1gNnSc1	his
Life	Life	k1gFnPc2	Life
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Harper	Harper	k1gInSc1	Harper
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-00717582-6	[number]	k4	978-0-00717582-6
</s>
</p>
<p>
<s>
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Hall	Hall	k1gMnSc1	Hall
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dictionary	Dictionar	k1gInPc7	Dictionar
of	of	k?	of
Subjects	Subjects	k1gInSc1	Subjects
and	and	k?	and
Symbols	Symbols	k1gInSc1	Symbols
in	in	k?	in
Art	Art	k1gFnSc2	Art
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
nd	nd	k?	nd
edn	edn	k?	edn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Murray	Murraa	k1gFnSc2	Murraa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0719541476	[number]	k4	0719541476
</s>
</p>
<p>
<s>
Held	Held	k1gMnSc1	Held
<g/>
,	,	kIx,	,
Jutta	Jutta	k1gMnSc1	Jutta
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Titian	Titian	k1gInSc1	Titian
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Analysis	Analysis	k1gFnSc2	Analysis
of	of	k?	of
the	the	k?	the
Analyses	Analyses	k1gInSc1	Analyses
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
Art	Art	k1gFnSc2	Art
Journal	Journal	k1gFnSc2	Journal
31	[number]	k4	31
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
181	[number]	k4	181
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
JSTOR	JSTOR	kA	JSTOR
</s>
</p>
<p>
<s>
Jaffé	Jaffé	k6eAd1	Jaffé
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Titian	Titian	k1gInSc1	Titian
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
National	National	k1gFnSc2	National
Gallery	Galler	k1gInPc1	Galler
Company	Compana	k1gFnSc2	Compana
<g/>
/	/	kIx~	/
<g/>
Yale	Yale	k1gInSc1	Yale
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
857099036	[number]	k4	857099036
</s>
</p>
<p>
<s>
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tiziano	Tiziana	k1gFnSc5	Tiziana
Vecellio	Vecellio	k1gMnSc1	Vecellio
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Tizian	Tizian	k1gMnSc1	Tizian
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Togner	Togner	k1gMnSc1	Togner
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
:	:	kIx,	:
Kroměřížská	kroměřížský	k2eAgFnSc1d1	Kroměřížská
obrazárna	obrazárna	k1gFnSc1	obrazárna
<g/>
:	:	kIx,	:
Katalog	katalog	k1gInSc1	katalog
sbírky	sbírka	k1gFnSc2	sbírka
obrazů	obraz	k1gInPc2	obraz
arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
410	[number]	k4	410
<g/>
-	-	kIx~	-
<g/>
421	[number]	k4	421
<g/>
.	.	kIx.	.
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Matthews	Matthews	k1gInSc1	Matthews
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Titian	Titian	k1gInSc1	Titian
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
'	'	kIx"	'
<g/>
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
'	'	kIx"	'
as	as	k1gInSc1	as
a	a	k8xC	a
Metaphor	Metaphor	k1gInSc1	Metaphor
of	of	k?	of
Transformation	Transformation	k1gInSc1	Transformation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
originally	originalla	k1gFnSc2	originalla
published	published	k1gMnSc1	published
in	in	k?	in
The	The	k1gMnSc1	The
Salisbury	Salisbura	k1gFnSc2	Salisbura
Review	Review	k1gMnSc1	Review
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paris	Paris	k1gMnSc1	Paris
Review	Review	k1gMnSc1	Review
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Iris	iris	k1gFnSc1	iris
Murdoch	Murdoch	k1gMnSc1	Murdoch
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Favorite	favorit	k1gInSc5	favorit
Painting	Painting	k1gInSc1	Painting
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Dan	Dan	k1gMnSc1	Dan
Piepenbring	Piepenbring	k1gInSc1	Piepenbring
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Paris	Paris	k1gMnSc1	Paris
Review	Review	k1gFnSc2	Review
<g/>
,	,	kIx,	,
July	Jula	k1gFnSc2	Jula
15	[number]	k4	15
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
Penny	penny	k1gFnSc1	penny
<g/>
,	,	kIx,	,
Nicholas	Nicholas	k1gMnSc1	Nicholas
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Gallery	Galler	k1gInPc4	Galler
Catalogues	Catalogues	k1gInSc1	Catalogues
(	(	kIx(	(
<g/>
new	new	k?	new
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Sixteenth	Sixteenth	k1gMnSc1	Sixteenth
Century	Centura	k1gFnSc2	Centura
Italian	Italian	k1gMnSc1	Italian
Paintings	Paintings	k1gInSc1	Paintings
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
II	II	kA	II
<g/>
,	,	kIx,	,
Venice	Venice	k1gFnSc1	Venice
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
National	National	k1gFnSc2	National
Gallery	Galler	k1gInPc1	Galler
Publications	Publicationsa	k1gFnPc2	Publicationsa
Ltd	ltd	kA	ltd
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1857099133	[number]	k4	1857099133
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Titian	Titian	k1gMnSc1	Titian
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
Force	force	k1gFnSc2	force
the	the	k?	the
'	'	kIx"	'
<g/>
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
'	'	kIx"	'
at	at	k?	at
the	the	k?	the
NGA	NGA	kA	NGA
"	"	kIx"	"
<g/>
,	,	kIx,	,
26	[number]	k4	26
January	Januara	k1gFnSc2	Januara
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Washington	Washington	k1gInSc1	Washington
Post	post	k1gInSc1	post
</s>
</p>
<p>
<s>
Robertson	Robertson	k1gMnSc1	Robertson
<g/>
,	,	kIx,	,
Giles	Giles	k1gMnSc1	Giles
<g/>
,	,	kIx,	,
in	in	k?	in
Jane	Jan	k1gMnSc5	Jan
Martineau	Martineaum	k1gNnSc3	Martineaum
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Genius	genius	k1gMnSc1	genius
of	of	k?	of
Venice	Venice	k1gFnSc2	Venice
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
cat	cat	k?	cat
#	#	kIx~	#
<g/>
132	[number]	k4	132
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rosand	Rosand	k1gInSc1	Rosand
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
""	""	k?	""
<g/>
Most	most	k1gInSc1	most
Musical	musical	k1gInSc1	musical
of	of	k?	of
Mourners	Mourners	k1gInSc1	Mourners
<g/>
,	,	kIx,	,
Weep	Weep	k1gMnSc1	Weep
Again	Again	k1gMnSc1	Again
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Titian	Titian	k1gInSc1	Titian
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Triumph	Triumph	k1gInSc1	Triumph
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Arion	Arion	k1gMnSc1	Arion
<g/>
:	:	kIx,	:
A	A	kA	A
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Humanities	Humanities	k1gMnSc1	Humanities
and	and	k?	and
the	the	k?	the
Classics	Classicsa	k1gFnPc2	Classicsa
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
JSTOR	JSTOR	kA	JSTOR
</s>
</p>
<p>
<s>
Rosand	Rosand	k1gInSc1	Rosand
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Exhibition	Exhibition	k1gInSc1	Exhibition
Review	Review	k1gFnSc2	Review
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Genius	genius	k1gMnSc1	genius
of	of	k?	of
Venice	Venice	k1gFnSc2	Venice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Renaissance	Renaissance	k1gFnSc1	Renaissance
Quarterly	Quarterla	k1gFnSc2	Quarterla
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
38	[number]	k4	38
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
290	[number]	k4	290
<g/>
–	–	k?	–
<g/>
304	[number]	k4	304
<g/>
,	,	kIx,	,
JSTOR	JSTOR	kA	JSTOR
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Royal	Royal	k1gInSc4	Royal
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Lucy	Luca	k1gFnSc2	Luca
Whitaker	Whitaker	k1gMnSc1	Whitaker
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Clayton	Clayton	k1gInSc1	Clayton
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Art	Art	k1gFnSc2	Art
of	of	k?	of
Italy	Ital	k1gMnPc4	Ital
in	in	k?	in
the	the	k?	the
Royal	Royal	k1gInSc1	Royal
Collection	Collection	k1gInSc1	Collection
<g/>
;	;	kIx,	;
Renaissance	Renaissance	k1gFnSc1	Renaissance
and	and	k?	and
Baroque	Baroque	k1gInSc1	Baroque
<g/>
,	,	kIx,	,
Royal	Royal	k1gInSc1	Royal
Collection	Collection	k1gInSc1	Collection
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
1	[number]	k4	1
902163	[number]	k4	902163
291	[number]	k4	291
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sistine	Sistin	k1gInSc5	Sistin
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Pietrangeli	Pietrangel	k1gInPc7	Pietrangel
<g/>
,	,	kIx,	,
Carlo	Carla	k1gMnSc5	Carla
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Sistine	Sistin	k1gMnSc5	Sistin
Chapel	Chapel	k1gMnSc1	Chapel
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
the	the	k?	the
History	Histor	k1gInPc1	Histor
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Restoration	Restoration	k1gInSc1	Restoration
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Harmony	Harmona	k1gFnPc1	Harmona
Books	Books	k1gInSc1	Books
<g/>
/	/	kIx~	/
<g/>
Nippon	Nippon	k1gInSc1	Nippon
Television	Television	k1gInSc1	Television
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-517-56274-X	[number]	k4	0-517-56274-X
</s>
</p>
<p>
<s>
Sohm	Sohm	k1gMnSc1	Sohm
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Artist	Artist	k1gMnSc1	Artist
Grows	Growsa	k1gFnPc2	Growsa
Old	Olda	k1gFnPc2	Olda
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Aging	Aging	k1gMnSc1	Aging
of	of	k?	of
Art	Art	k1gMnSc1	Art
and	and	k?	and
Artists	Artists	k1gInSc1	Artists
in	in	k?	in
Italy	Ital	k1gMnPc7	Ital
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
300121237	[number]	k4	300121237
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
9780300121230	[number]	k4	9780300121230
</s>
</p>
<p>
<s>
Wind	Wind	k1gMnSc1	Wind
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
<g/>
,	,	kIx,	,
Pagan	Pagan	k1gMnSc1	Pagan
Mysteries	Mysteries	k1gMnSc1	Mysteries
in	in	k?	in
the	the	k?	the
Renaissance	Renaissance	k1gFnSc2	Renaissance
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
edn	edn	k?	edn
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Peregrine	Peregrin	k1gInSc5	Peregrin
Books	Books	k1gInSc1	Books
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
,	,	kIx,	,
Titian	Titian	k1gMnSc1	Titian
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Flaying	Flaying	k1gInSc1	Flaying
of	of	k?	of
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Spring	Spring	k1gInSc1	Spring
art	art	k?	art
books	books	k1gInSc1	books
(	(	kIx(	(
<g/>
30	[number]	k4	30
pages	pagesa	k1gFnPc2	pagesa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Panofsky	Panofsky	k6eAd1	Panofsky
<g/>
,	,	kIx,	,
Erwin	Erwin	k1gMnSc1	Erwin
<g/>
,	,	kIx,	,
Problems	Problems	k1gInSc1	Problems
in	in	k?	in
Titian	Titiana	k1gFnPc2	Titiana
<g/>
,	,	kIx,	,
mostly	mostnout	k5eAaPmAgFnP	mostnout
Iconographic	Iconographic	k1gMnSc1	Iconographic
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
WANKOVÁ	WANKOVÁ	kA	WANKOVÁ
<g/>
,	,	kIx,	,
Veronika	Veronika	k1gFnSc1	Veronika
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
versus	versus	k7c1	versus
Pieta	pieta	k1gFnSc1	pieta
Rondanini	Rondanin	k2eAgMnPc5d1	Rondanin
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
pozdní	pozdní	k2eAgFnSc2d1	pozdní
tvorby	tvorba	k1gFnSc2	tvorba
Tiziana	Tizian	k1gMnSc2	Tizian
a	a	k8xC	a
Michelangela	Michelangel	k1gMnSc2	Michelangel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Martin	Martin	k1gMnSc1	Martin
Zlatohlávek	zlatohlávek	k1gMnSc1	zlatohlávek
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gInSc1	Marsyas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
