<s>
Beogradska	Beogradska	k1gFnSc1
zadruga	zadruga	k1gFnSc1
</s>
<s>
Beogradska	Beogradska	k1gFnSc1
zadruga	zadruga	k1gFnSc1
Pohled	pohled	k1gInSc1
na	na	k7c6
průčelí	průčelí	k1gNnSc6
budovy	budova	k1gFnSc2
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1882	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Beogradska	Beogradska	k1gFnSc1
zadruga	zadruga	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
srbské	srbský	k2eAgFnSc6d1
cyrilici	cyrilice	k1gFnSc6
Б	Б	k?
з	з	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
historická	historický	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
části	část	k1gFnSc6
Bělehradu	Bělehrad	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
ulici	ulice	k1gFnSc6
Karađorđeva	Karađorđeva	k1gFnSc1
(	(	kIx(
<g/>
poblíž	poblíž	k7c2
Sávy	Sáva	k1gFnSc2
a	a	k8xC
Brankova	Brankův	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Monumentální	monumentální	k2eAgFnSc1d1
rohová	rohový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
finančního	finanční	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
(	(	kIx(
<g/>
srbský	srbský	k2eAgInSc1d1
termín	termín	k1gInSc1
zadruga	zadruga	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
družstvo	družstvo	k1gNnSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
zbudována	zbudovat	k5eAaPmNgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
stavba	stavba	k1gFnSc1
zahájena	zahájen	k2eAgFnSc1d1
1905	#num#	k4
<g/>
,	,	kIx,
dokončena	dokončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
ve	v	k7c6
slohu	sloh	k1gInSc6
<g/>
,	,	kIx,
známém	známý	k2eAgInSc6d1
jako	jako	k8xS,k8xC
akademismus	akademismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drží	držet	k5eAaImIp3nS
bělehradský	bělehradský	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
primát	primát	k1gInSc4
první	první	k4xOgFnSc2
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
při	při	k7c6
výstavbě	výstavba	k1gFnSc6
uplatněn	uplatnit	k5eAaPmNgInS
železobeton	železobeton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
celkem	celkem	k6eAd1
tři	tři	k4xCgNnPc4
křídla	křídlo	k1gNnPc4
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc4
po	po	k7c6
stranách	strana	k1gFnPc6
a	a	k8xC
jedno	jeden	k4xCgNnSc1
zabíhající	zabíhající	k2eAgNnSc1d1
do	do	k7c2
bloku	blok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průčelí	průčelí	k1gNnSc6
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rohu	roh	k1gInSc6
ulic	ulice	k1gFnPc2
Karađorđeva	Karađorđeva	k1gFnSc1
a	a	k8xC
Hercegovačka	Hercegovačka	k1gFnSc1
<g/>
;	;	kIx,
dominuje	dominovat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
prosklená	prosklený	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
osvětluje	osvětlovat	k5eAaImIp3nS
společenský	společenský	k2eAgInSc4d1
sál	sál	k1gInSc4
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
za	za	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
budovy	budova	k1gFnSc2
</s>
<s>
Architektonicky	architektonicky	k6eAd1
unikátní	unikátní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
společné	společný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
architektů	architekt	k1gMnPc2
Nikoly	Nikola	k1gFnSc2
Nestoroviće	Nestorović	k1gFnSc2
a	a	k8xC
Andre	Andr	k1gMnSc5
Stevanoviće	Stevanovićus	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbu	stavba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
sloužila	sloužit	k5eAaImAgFnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
bankovního	bankovní	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc1
první	první	k4xOgFnSc1
bělehradská	bělehradský	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
projektovali	projektovat	k5eAaBmAgMnP
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
ovlivněni	ovlivnit	k5eAaPmNgMnP
pařížskou	pařížský	k2eAgFnSc7d1
Světovou	světový	k2eAgFnSc7d1
výstavou	výstava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekorativní	dekorativní	k2eAgInPc1d1
prvky	prvek	k1gInPc1
nesou	nést	k5eAaImIp3nP
jasné	jasný	k2eAgInPc1d1
znaky	znak	k1gInPc1
tehdejší	tehdejší	k2eAgFnSc2d1
pařížské	pařížský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikola	Nikola	k1gMnSc1
Nestorović	Nestorović	k1gMnSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
základům	základ	k1gInPc3
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
Andra	Andra	k1gMnSc1
Stevanović	Stevanović	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
nosném	nosný	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stevanović	Stevanović	k1gMnSc1
také	také	k9
projektoval	projektovat	k5eAaBmAgMnS
rámcové	rámcový	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
fasáda	fasáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
detailů	detail	k1gInPc2
fasády	fasáda	k1gFnSc2
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Franja	Franja	k1gMnSc1
Valdman	Valdman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Interiér	interiér	k1gInSc1
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
dílem	díl	k1gInSc7
také	také	k9
právě	právě	k9
Nestoroviće	Nestoroviće	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
banky	banka	k1gFnSc2
až	až	k9
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ustanovení	ustanovení	k1gNnSc6
komunistické	komunistický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
ji	on	k3xPp3gFnSc4
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
institucí	instituce	k1gFnPc2
a	a	k8xC
ústavů	ústav	k1gInPc2
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
jugoslávský	jugoslávský	k2eAgInSc1d1
geologický	geologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
Geozavod	Geozavod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
Institut	institut	k1gInSc1
pro	pro	k7c4
hydrologický	hydrologický	k2eAgInSc4d1
a	a	k8xC
geologický	geologický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
není	být	k5eNaImIp3nS
pro	pro	k7c4
budovu	budova	k1gFnSc4
žádné	žádný	k3yNgNnSc1
využití	využití	k1gNnSc1
a	a	k8xC
chátrá	chátrat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byla	být	k5eAaImAgFnS
fasáda	fasáda	k1gFnSc1
budovy	budova	k1gFnSc2
rekonstruována	rekonstruován	k2eAgFnSc1d1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
oblasti	oblast	k1gFnSc2
Beograd	Beograd	k1gInSc1
na	na	k7c4
vodi	vode	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
byla	být	k5eAaImAgFnS
fasáda	fasáda	k1gFnSc1
budovy	budova	k1gFnSc2
po	po	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
nasvěcována	nasvěcován	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Katalog	katalog	k1gInSc1
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
Bělehradu	Bělehrad	k1gInSc2
(	(	kIx(
<g/>
srbsky	srbsky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Článek	článek	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
novin	novina	k1gFnPc2
danas	danas	k1gInSc1
<g/>
.	.	kIx.
<g/>
rs	rs	k?
(	(	kIx(
<g/>
srbsky	srbsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
rs	rs	k?
(	(	kIx(
<g/>
srbsky	srbsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Srbsko	Srbsko	k1gNnSc1
</s>
