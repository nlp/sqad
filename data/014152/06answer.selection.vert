<s>
Monumentální	monumentální	k2eAgFnSc1d1
rohová	rohový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
finančního	finanční	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
(	(	kIx(
<g/>
srbský	srbský	k2eAgInSc1d1
termín	termín	k1gInSc1
zadruga	zadruga	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
družstvo	družstvo	k1gNnSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
zbudována	zbudovat	k5eAaPmNgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
stavba	stavba	k1gFnSc1
zahájena	zahájen	k2eAgFnSc1d1
1905	#num#	k4
<g/>
,	,	kIx,
dokončena	dokončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
ve	v	k7c6
slohu	sloh	k1gInSc6
<g/>
,	,	kIx,
známém	známý	k2eAgInSc6d1
jako	jako	k8xS,k8xC
akademismus	akademismus	k1gInSc4
<g/>
.	.	kIx.
</s>