<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
první	první	k4xOgFnSc2	první
zasedání	zasedání	k1gNnPc2	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
přijalo	přijmout	k5eAaPmAgNnS	přijmout
resoluci	resoluce	k1gFnSc4	resoluce
96	[number]	k4	96
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
genocida	genocida	k1gFnSc1	genocida
je	být	k5eAaImIp3nS	být
zločinem	zločin	k1gInSc7	zločin
podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezajistilo	zajistit	k5eNaPmAgNnS	zajistit
právní	právní	k2eAgFnSc4d1	právní
definici	definice	k1gFnSc4	definice
zločinu	zločin	k1gInSc2	zločin
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
