<s>
Vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
byl	být	k5eAaImAgInS	být
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
poprvé	poprvé	k6eAd1	poprvé
překonán	překonat	k5eAaPmNgInS	překonat
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Francouz	Francouz	k1gMnSc1	Francouz
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Blanchard	Blanchard	k1gMnSc1	Blanchard
a	a	k8xC	a
Američan	Američan	k1gMnSc1	Američan
John	John	k1gMnSc1	John
Jeffries	Jeffries	k1gMnSc1	Jeffries
přeletěli	přeletět	k5eAaPmAgMnP	přeletět
v	v	k7c6	v
balonu	balon	k1gInSc6	balon
mezi	mezi	k7c7	mezi
Doverem	Dovero	k1gNnSc7	Dovero
a	a	k8xC	a
Calais	Calais	k1gNnSc7	Calais
<g/>
.	.	kIx.	.
</s>
