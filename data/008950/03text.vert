<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Islandu	Island	k1gInSc2	Island
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
se	s	k7c7	s
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
lemovaným	lemovaný	k2eAgInSc7d1	lemovaný
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Atlantik	Atlantik	k1gInSc1	Atlantik
a	a	k8xC	a
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
a	a	k8xC	a
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
gejzíry	gejzír	k1gInPc4	gejzír
<g/>
,	,	kIx,	,
ledovce	ledovec	k1gInPc1	ledovec
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pak	pak	k6eAd1	pak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
oheň	oheň	k1gInSc1	oheň
islandských	islandský	k2eAgInPc2d1	islandský
vulkánů	vulkán	k1gInPc2	vulkán
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
horkou	horký	k2eAgFnSc4d1	horká
lávu	láva	k1gFnSc4	láva
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
vztah	vztah	k1gInSc4	vztah
Islandu	Island	k1gInSc2	Island
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
skandinávským	skandinávský	k2eAgInPc3d1	skandinávský
státům	stát	k1gInPc3	stát
a	a	k8xC	a
národům	národ	k1gInPc3	národ
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
kříže	kříž	k1gInSc2	kříž
také	také	k9	také
připomíná	připomínat	k5eAaImIp3nS	připomínat
dánskou	dánský	k2eAgFnSc4d1	dánská
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Island	Island	k1gInSc1	Island
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládal	ovládat	k5eAaImAgMnS	ovládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvními	první	k4xOgFnPc7	první
návštěvníky	návštěvník	k1gMnPc4	návštěvník
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
ostrova	ostrov	k1gInSc2	ostrov
na	na	k7c4	na
pomezí	pomezí	k1gNnSc4	pomezí
Atlantského	atlantský	k2eAgInSc2d1	atlantský
a	a	k8xC	a
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
irští	irský	k2eAgMnPc1d1	irský
mnichové	mnich	k1gMnPc1	mnich
na	na	k7c6	na
konci	konec	k1gInSc6	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Ledová	ledový	k2eAgFnSc1d1	ledová
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
dali	dát	k5eAaPmAgMnP	dát
ostrovu	ostrov	k1gInSc3	ostrov
v	v	k7c4	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Vikingové	Viking	k1gMnPc1	Viking
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ostrov	ostrov	k1gInSc4	ostrov
navštívili	navštívit	k5eAaPmAgMnP	navštívit
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
930	[number]	k4	930
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
národní	národní	k2eAgInSc1d1	národní
sněm	sněm	k1gInSc1	sněm
Althing	Althing	k1gInSc1	Althing
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
kolonizován	kolonizovat	k5eAaBmNgInS	kolonizovat
Nory	Nor	k1gMnPc7	Nor
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vyvěšována	vyvěšovat	k5eAaImNgFnS	vyvěšovat
pouze	pouze	k6eAd1	pouze
dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1809	[number]	k4	1809
Jø	Jø	k1gMnSc1	Jø
Jø	Jø	k1gMnSc1	Jø
(	(	kIx(	(
<g/>
dánský	dánský	k2eAgMnSc1d1	dánský
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
korzár	korzár	k1gMnSc1	korzár
a	a	k8xC	a
tlumočník	tlumočník	k1gMnSc1	tlumočník
<g/>
)	)	kIx)	)
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
dánského	dánský	k2eAgMnSc4d1	dánský
guvernéra	guvernér	k1gMnSc4	guvernér
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
jeho	jeho	k3xOp3gMnSc7	jeho
vládcem	vládce	k1gMnSc7	vládce
a	a	k8xC	a
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
popis	popis	k1gInSc4	popis
<g/>
:	:	kIx,	:
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
modrý	modrý	k2eAgInSc4d1	modrý
list	list	k1gInSc4	list
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
bílými	bílý	k2eAgFnPc7d1	bílá
treskami	treska	k1gFnPc7	treska
bez	bez	k7c2	bez
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Treska	treska	k1gFnSc1	treska
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc4d1	starý
islandský	islandský	k2eAgInSc4d1	islandský
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1589	[number]	k4	1589
byla	být	k5eAaImAgFnS	být
bezhlavá	bezhlavý	k2eAgFnSc1d1	bezhlavá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
treska	treska	k1gFnSc1	treska
ve	v	k7c6	v
štítu	štít	k1gInSc6	štít
islandského	islandský	k2eAgInSc2d1	islandský
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1593	[number]	k4	1593
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
pečeti	pečeť	k1gFnSc6	pečeť
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
asi	asi	k9	asi
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
stříbrná	stříbrná	k1gFnSc1	stříbrná
<g/>
,	,	kIx,	,
bezhlavá	bezhlavý	k2eAgFnSc1d1	bezhlavá
treska	treska	k1gFnSc1	treska
oficiálním	oficiální	k2eAgInSc7d1	oficiální
znakem	znak	k1gInSc7	znak
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
dánská	dánský	k2eAgFnSc1d1	dánská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
Jø	Jø	k1gMnSc1	Jø
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
užívání	užívání	k1gNnSc1	užívání
dánské	dánský	k2eAgFnSc2d1	dánská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
islandský	islandský	k2eAgMnSc1d1	islandský
výtvarník	výtvarník	k1gMnSc1	výtvarník
Sigurð	Sigurð	k1gMnSc1	Sigurð
Guð	Guð	k1gMnSc1	Guð
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
s	s	k7c7	s
vyobrazeným	vyobrazený	k2eAgMnSc7d1	vyobrazený
bílým	bílý	k1gMnSc7	bílý
<g/>
,	,	kIx,	,
sedícím	sedící	k2eAgMnSc7d1	sedící
sokolem	sokol	k1gMnSc7	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Þ	Þ	k?	Þ
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
vzniku	vznik	k1gInSc2	vznik
Althingu	Althing	k1gInSc2	Althing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oslav	oslava	k1gFnPc2	oslava
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
osídlení	osídlení	k1gNnSc2	osídlení
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sílícím	sílící	k2eAgNnSc7d1	sílící
národním	národní	k2eAgNnSc7d1	národní
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
vyvěšována	vyvěšován	k2eAgFnSc1d1	vyvěšována
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
islandský	islandský	k2eAgMnSc1d1	islandský
básník	básník	k1gMnSc1	básník
Einar	Einar	k1gMnSc1	Einar
Benediktsson	Benediktsson	k1gMnSc1	Benediktsson
v	v	k7c6	v
novinovém	novinový	k2eAgInSc6d1	novinový
článku	článek	k1gInSc6	článek
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vlajky	vlajka	k1gFnSc2	vlajka
se	s	k7c7	s
sokolem	sokol	k1gMnSc7	sokol
<g/>
)	)	kIx)	)
slučitelná	slučitelný	k2eAgFnSc1d1	slučitelná
s	s	k7c7	s
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
vlajkami	vlajka	k1gFnPc7	vlajka
ostatních	ostatní	k2eAgInPc2d1	ostatní
severských	severský	k2eAgInPc2d1	severský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
tvořil	tvořit	k5eAaImAgInS	tvořit
modrý	modrý	k2eAgInSc1d1	modrý
list	list	k1gInSc1	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
populární	populární	k2eAgFnSc1d1	populární
vlajka	vlajka	k1gFnSc1	vlajka
poprvé	poprvé	k6eAd1	poprvé
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
<g/>
,	,	kIx,	,
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
přijetí	přijetí	k1gNnSc3	přijetí
zabránily	zabránit	k5eAaPmAgInP	zabránit
dánské	dánský	k2eAgInPc1d1	dánský
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
příliš	příliš	k6eAd1	příliš
podobná	podobný	k2eAgNnPc4d1	podobné
švédské	švédský	k2eAgFnSc6d1	švédská
nebo	nebo	k8xC	nebo
řecké	řecký	k2eAgFnSc6d1	řecká
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
shodná	shodný	k2eAgFnSc1d1	shodná
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
odstín	odstín	k1gInSc4	odstín
a	a	k8xC	a
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
vlajkou	vlajka	k1gFnSc7	vlajka
britských	britský	k2eAgFnPc2d1	britská
Shetland	Shetlanda	k1gFnPc2	Shetlanda
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1904	[number]	k4	1904
byla	být	k5eAaImAgFnS	být
Islandu	Island	k1gInSc2	Island
přiznána	přiznán	k2eAgFnSc1d1	přiznána
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
,	,	kIx,	,
dalším	další	k2eAgInSc7d1	další
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
národněosvobozeneckému	národněosvobozenecký	k2eAgNnSc3d1	národněosvobozenecké
hnutí	hnutí	k1gNnSc3	hnutí
bylo	být	k5eAaImAgNnS	být
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Norska	Norsko	k1gNnSc2	Norsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
dánskou	dánský	k2eAgFnSc7d1	dánská
lodí	loď	k1gFnSc7	loď
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
stráže	stráž	k1gFnSc2	stráž
zatčen	zatčen	k2eAgMnSc1d1	zatčen
Einar	Einar	k1gMnSc1	Einar
Petursson	Petursson	k1gMnSc1	Petursson
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
Islanďan	Islanďan	k1gMnSc1	Islanďan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vesloval	veslovat	k5eAaImAgMnS	veslovat
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
člunu	člun	k1gInSc6	člun
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
vlajkou	vlajka	k1gFnSc7	vlajka
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
křížem	kříž	k1gInSc7	kříž
u	u	k7c2	u
Reykjavického	Reykjavický	k2eAgInSc2d1	Reykjavický
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Zabavení	zabavení	k1gNnSc1	zabavení
vlajky	vlajka	k1gFnSc2	vlajka
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pobouření	pobouření	k1gNnSc3	pobouření
<g/>
,	,	kIx,	,
protestním	protestní	k2eAgNnSc6d1	protestní
shromáždění	shromáždění	k1gNnSc6	shromáždění
a	a	k8xC	a
následně	následně	k6eAd1	následně
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
užívání	užívání	k1gNnSc3	užívání
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Výzvy	výzva	k1gFnPc1	výzva
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
parlamentem	parlament	k1gInSc7	parlament
byly	být	k5eAaImAgFnP	být
dánskými	dánský	k2eAgInPc7d1	dánský
úřady	úřad	k1gInPc7	úřad
i	i	k8xC	i
králem	král	k1gMnSc7	král
nadále	nadále	k6eAd1	nadále
odmítány	odmítán	k2eAgFnPc1d1	odmítána
<g/>
.	.	kIx.	.
<g/>
Student	student	k1gMnSc1	student
Matthias	Matthias	k1gMnSc1	Matthias
Thordarson	Thordarson	k1gMnSc1	Thordarson
(	(	kIx(	(
<g/>
Þ	Þ	k?	Þ
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
další	další	k2eAgFnSc4d1	další
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
místo	místo	k1gNnSc4	místo
bílého	bílý	k2eAgInSc2d1	bílý
kříže	kříž	k1gInSc2	kříž
kříž	kříž	k1gInSc1	kříž
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
lemovaný	lemovaný	k2eAgMnSc1d1	lemovaný
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
královským	královský	k2eAgInSc7d1	královský
výnosem	výnos	k1gInSc7	výnos
přijata	přijmout	k5eAaPmNgNnP	přijmout
za	za	k7c4	za
islandskou	islandský	k2eAgFnSc4d1	islandská
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
výnos	výnos	k1gInSc1	výnos
byl	být	k5eAaImAgMnS	být
podepsán	podepsat	k5eAaPmNgMnS	podepsat
dánským	dánský	k2eAgMnSc7d1	dánský
králem	král	k1gMnSc7	král
Kristiánem	Kristián	k1gMnSc7	Kristián
X.	X.	kA	X.
až	až	k6eAd1	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
královského	královský	k2eAgNnSc2d1	královské
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
současnou	současný	k2eAgFnSc7d1	současná
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
světlejší	světlý	k2eAgInSc4d2	světlejší
odstín	odstín	k1gInSc4	odstín
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
Islandské	islandský	k2eAgNnSc1d1	islandské
království	království	k1gNnSc1	království
a	a	k8xC	a
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
Kristián	Kristián	k1gMnSc1	Kristián
X.	X.	kA	X.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
islandským	islandský	k2eAgMnSc7d1	islandský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
vlajkou	vlajka	k1gFnSc7	vlajka
obchodní	obchodní	k2eAgFnSc1d1	obchodní
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
okupaci	okupace	k1gFnSc6	okupace
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
úmluvě	úmluva	k1gFnSc3	úmluva
s	s	k7c7	s
islandskou	islandský	k2eAgFnSc7d1	islandská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
obsazen	obsazen	k2eAgInSc1d1	obsazen
britským	britský	k2eAgNnSc7d1	Britské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1941	[number]	k4	1941
vystřídáno	vystřídat	k5eAaPmNgNnS	vystřídat
armádou	armáda	k1gFnSc7	armáda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Suverenita	suverenita	k1gFnSc1	suverenita
Islandu	Island	k1gInSc2	Island
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
svazku	svazek	k1gInSc2	svazek
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Islandská	islandský	k2eAgFnSc1d1	islandská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
byl	být	k5eAaImAgInS	být
schválen	schválen	k2eAgInSc1d1	schválen
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
34	[number]	k4	34
(	(	kIx(	(
<g/>
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nová	nový	k2eAgFnSc1d1	nová
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
korunovaným	korunovaný	k2eAgMnSc7d1	korunovaný
sokolem	sokol	k1gMnSc7	sokol
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
v	v	k7c6	v
letech	let	k1gInPc6	let
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
jako	jako	k8xS	jako
královská	královský	k2eAgFnSc1d1	královská
standarta	standarta	k1gFnSc1	standarta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Islandu	Island	k1gInSc2	Island
</s>
</p>
<p>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Islandu	Island	k1gInSc2	Island
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Islandská	islandský	k2eAgFnSc1d1	islandská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
