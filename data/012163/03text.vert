<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Schulmeister	Schulmeister	k1gMnSc1	Schulmeister
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tento	tento	k3xDgMnSc1	tento
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
odchovanec	odchovanec	k1gMnSc1	odchovanec
v	v	k7c6	v
mateřské	mateřský	k2eAgFnSc6d1	mateřská
Sigmě	Sigma	k1gFnSc6	Sigma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
výrazně	výrazně	k6eAd1	výrazně
neprosazoval	prosazovat	k5eNaImAgInS	prosazovat
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Zenit	zenit	k1gInSc1	zenit
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
prošel	projít	k5eAaPmAgInS	projít
několika	několik	k4yIc7	několik
dalšími	další	k2eAgInPc7d1	další
kluby	klub	k1gInPc7	klub
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Schulmeister	Schulmeister	k1gInSc1	Schulmeister
měl	mít	k5eAaImAgInS	mít
pověst	pověst	k1gFnSc4	pověst
hráče	hráč	k1gMnSc2	hráč
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
talentem	talent	k1gInSc7	talent
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ale	ale	k8xC	ale
promarňoval	promarňovat	k5eAaImAgInS	promarňovat
svým	svůj	k3xOyFgMnSc7	svůj
bohémským	bohémský	k2eAgInSc7d1	bohémský
stylem	styl	k1gInSc7	styl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
testech	test	k1gInPc6	test
ve	v	k7c6	v
Zbrojovce	zbrojovka	k1gFnSc6	zbrojovka
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
ale	ale	k8xC	ale
nijak	nijak	k6eAd1	nijak
neoslnil	oslnit	k5eNaPmAgMnS	oslnit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
ligovém	ligový	k2eAgNnSc6d1	ligové
kole	kolo	k1gNnSc6	kolo
podílel	podílet	k5eAaImAgInS	podílet
jedním	jeden	k4xCgInSc7	jeden
gólem	gól	k1gInSc7	gól
na	na	k7c6	na
výhře	výhra	k1gFnSc6	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
Vysočinou	vysočina	k1gFnSc7	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
sigmafotbal	sigmafotbal	k1gInSc1	sigmafotbal
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
fotbalportal	fotbalportat	k5eAaPmAgMnS	fotbalportat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
