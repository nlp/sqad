<s>
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Film	film	k1gInSc1	film
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
slyšet	slyšet	k5eAaImF	slyšet
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
filmu	film	k1gInSc2	film
hrají	hrát	k5eAaImIp3nP	hrát
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Plíhal	Plíhal	k1gMnSc1	Plíhal
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
fiktivním	fiktivní	k2eAgInSc7d1	fiktivní
dokumentem	dokument	k1gInSc7	dokument
<g/>
,	,	kIx,	,
odehrávajícím	odehrávající	k2eAgMnSc7d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
úmrtí	úmrtí	k1gNnSc4	úmrtí
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
se	se	k3xPyFc4	se
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
s	s	k7c7	s
Čechomorem	Čechomor	k1gInSc7	Čechomor
spolu	spolu	k6eAd1	spolu
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
zbavil	zbavit	k5eAaPmAgInS	zbavit
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
zoufale	zoufale	k6eAd1	zoufale
osaměl	osamět	k5eAaPmAgMnS	osamět
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
několik	několik	k4yIc4	několik
různých	různý	k2eAgNnPc2d1	různé
českých	český	k2eAgNnPc2d1	české
i	i	k8xC	i
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
filmu	film	k1gInSc2	film
si	se	k3xPyFc3	se
odnesli	odnést	k5eAaPmAgMnP	odnést
7	[number]	k4	7
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
(	(	kIx(	(
<g/>
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc4d1	filmový
plakát	plakát	k1gInSc4	plakát
a	a	k8xC	a
divácky	divácky	k6eAd1	divácky
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
-	-	kIx~	-
Křišťálový	křišťálový	k2eAgInSc1d1	křišťálový
glóbus	glóbus	k1gInSc1	glóbus
<g/>
.	.	kIx.	.
</s>
