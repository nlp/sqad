<s>
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Film	film	k1gInSc1	film
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
slyšet	slyšet	k5eAaImF	slyšet
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
