<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
(	(	kIx(	(
<g/>
Catagonus	Catagonus	k1gInSc1	Catagonus
wagneri	wagner	k1gFnSc2	wagner
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
nepřežvýkavý	přežvýkavý	k2eNgMnSc1d1	nepřežvýkavý
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c7	za
nejprimitivnější	primitivní	k2eAgFnSc7d3	nejprimitivnější
z	z	k7c2	z
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
pekariů	pekari	k1gMnPc2	pekari
<g/>
.	.	kIx.	.
</s>
