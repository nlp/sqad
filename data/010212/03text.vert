<p>
<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
(	(	kIx(	(
<g/>
Catagonus	Catagonus	k1gInSc1	Catagonus
wagneri	wagner	k1gFnSc2	wagner
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
nepřežvýkavý	přežvýkavý	k2eNgMnSc1d1	nepřežvýkavý
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c7	za
nejprimitivnější	primitivní	k2eAgFnSc7d3	nejprimitivnější
z	z	k7c2	z
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
pekariů	pekari	k1gMnPc2	pekari
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
pekariů	pekari	k1gMnPc2	pekari
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvzácnější	vzácný	k2eAgMnSc1d3	nejvzácnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
i	i	k8xC	i
pod	pod	k7c7	pod
domorodým	domorodý	k2eAgInSc7d1	domorodý
názvem	název	k1gInSc7	název
tagua	tagu	k1gInSc2	tagu
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
guaraní	guaraní	k1gNnSc2	guaraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
připomíná	připomínat	k5eAaImIp3nS	připomínat
vzhledem	vzhled	k1gInSc7	vzhled
pekari	pekari	k1gMnSc1	pekari
páskovaného	páskovaný	k2eAgNnSc2d1	páskované
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
30	[number]	k4	30
-	-	kIx~	-
40	[number]	k4	40
kg	kg	kA	kg
<g/>
,	,	kIx,	,
délky	délka	k1gFnPc1	délka
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
110	[number]	k4	110
cm	cm	kA	cm
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
až	až	k9	až
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
šedohnědé	šedohnědý	k2eAgNnSc1d1	šedohnědé
<g/>
,	,	kIx,	,
s	s	k7c7	s
nevýraznou	výrazný	k2eNgFnSc7d1	nevýrazná
kresbou	kresba	k1gFnSc7	kresba
na	na	k7c6	na
plecích	plec	k1gFnPc6	plec
a	a	k8xC	a
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
pásků	pásek	k1gInPc2	pásek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
bradou	brada	k1gFnSc7	brada
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
hrdla	hrdlo	k1gNnSc2	hrdlo
šikmo	šikmo	k6eAd1	šikmo
vzhůru	vzhůru	k6eAd1	vzhůru
až	až	k9	až
ke	k	k7c3	k
kohoutku	kohoutek	k1gInSc3	kohoutek
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
hrubá	hrubý	k2eAgFnSc1d1	hrubá
a	a	k8xC	a
štětinatá	štětinatý	k2eAgFnSc1d1	štětinatá
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
štětiny	štětina	k1gFnPc4	štětina
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
šíji	šíj	k1gFnSc6	šíj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
rozrušení	rozrušení	k1gNnSc6	rozrušení
prudce	prudko	k6eAd1	prudko
naježit	naježit	k5eAaPmF	naježit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgInPc4d1	velký
<g/>
,	,	kIx,	,
střapaté	střapatý	k2eAgInPc4d1	střapatý
boltce	boltec	k1gInPc4	boltec
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnPc4d1	vysoká
končetiny	končetina	k1gFnPc4	končetina
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
rypák	rypák	k1gInSc4	rypák
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
druhy	druh	k1gInPc4	druh
pekariů	pekari	k1gMnPc2	pekari
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
i	i	k9	i
anatomickými	anatomický	k2eAgInPc7d1	anatomický
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
vyvinuté	vyvinutý	k2eAgInPc4d1	vyvinutý
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
pekariové	pekari	k1gMnPc1	pekari
jen	jen	k9	jen
tři	tři	k4xCgMnPc4	tři
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
blíží	blížit	k5eAaImIp3nS	blížit
pravým	pravý	k2eAgNnPc3d1	pravé
prasatům	prase	k1gNnPc3	prase
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
obývá	obývat	k5eAaImIp3nS	obývat
oblast	oblast	k1gFnSc4	oblast
Gran	Grana	k1gFnPc2	Grana
Chaco	Chaco	k6eAd1	Chaco
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Paragyae	Paragya	k1gFnSc2	Paragya
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
křovinaté	křovinatý	k2eAgFnPc4d1	křovinatá
savany	savana	k1gFnPc4	savana
s	s	k7c7	s
porosty	porost	k1gInPc7	porost
palem	palma	k1gFnPc2	palma
a	a	k8xC	a
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
a	a	k8xC	a
polopouštní	polopouštní	k2eAgFnSc2d1	polopouštní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Bažinám	bažina	k1gFnPc3	bažina
a	a	k8xC	a
otevřeným	otevřený	k2eAgInPc3d1	otevřený
krajům	kraj	k1gInPc3	kraj
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
ničením	ničení	k1gNnSc7	ničení
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
život	život	k1gInSc1	život
pekariů	pekari	k1gMnPc2	pekari
Wagnerových	Wagnerových	k2eAgFnPc2d1	Wagnerových
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
rodinných	rodinný	k2eAgFnPc6d1	rodinná
tlupách	tlupa	k1gFnPc6	tlupa
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
4-10	[number]	k4	4-10
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
a	a	k8xC	a
porostech	porost	k1gInPc6	porost
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
vycházejí	vycházet	k5eAaImIp3nP	vycházet
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
mají	mít	k5eAaImIp3nP	mít
denní	denní	k2eAgFnSc4d1	denní
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žijí	žít	k5eAaImIp3nP	žít
skrytě	skrytě	k6eAd1	skrytě
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
plaší	plachý	k2eAgMnPc1d1	plachý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
člověk	člověk	k1gMnSc1	člověk
setká	setkat	k5eAaPmIp3nS	setkat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Pekari	pekari	k1gMnSc1	pekari
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
hlavně	hlavně	k9	hlavně
kaktusy	kaktus	k1gInPc1	kaktus
<g/>
,	,	kIx,	,
kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
oddenky	oddenek	k1gInPc1	oddenek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
sbírá	sbírat	k5eAaImIp3nS	sbírat
také	také	k9	také
spadané	spadaný	k2eAgInPc4d1	spadaný
plody	plod	k1gInPc4	plod
stromu	strom	k1gInSc2	strom
inga	ing	k1gInSc2	ing
(	(	kIx(	(
<g/>
Inga	Ing	k2eAgNnPc1d1	Ing
alba	album	k1gNnPc1	album
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Dužnaté	dužnatý	k2eAgInPc1d1	dužnatý
kaktusy	kaktus	k1gInPc1	kaktus
mu	on	k3xPp3gMnSc3	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
také	také	k9	také
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pije	pít	k5eAaImIp3nS	pít
méně	málo	k6eAd2	málo
než	než	k8xS	než
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
přesnější	přesný	k2eAgInPc1d2	přesnější
údaje	údaj	k1gInPc1	údaj
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
tři	tři	k4xCgNnPc4	tři
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dožít	dožít	k5eAaPmF	dožít
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
je	být	k5eAaImIp3nS	být
puma	puma	k1gFnSc1	puma
a	a	k8xC	a
jaguár	jaguár	k1gMnSc1	jaguár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
popsal	popsat	k5eAaPmAgMnS	popsat
argentinský	argentinský	k2eAgMnSc1d1	argentinský
paleontolog	paleontolog	k1gMnSc1	paleontolog
Florentino	Florentin	k2eAgNnSc4d1	Florentino
Ameghino	Ameghino	k1gNnSc4	Ameghino
z	z	k7c2	z
pliocénních	pliocénní	k2eAgFnPc2d1	pliocénní
vrstev	vrstva	k1gFnPc2	vrstva
fosilní	fosilní	k2eAgInSc1d1	fosilní
rod	rod	k1gInSc1	rod
pekariů	pekari	k1gMnPc2	pekari
Catagonus	Catagonus	k1gInSc4	Catagonus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
materiálu	materiál	k1gInSc2	materiál
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
popsán	popsán	k2eAgInSc1d1	popsán
italských	italský	k2eAgMnPc2d1	italský
zoologem	zoolog	k1gMnSc7	zoolog
Rusconim	Rusconim	k1gMnSc1	Rusconim
druh	druh	k1gMnSc1	druh
Catagonus	Catagonus	k1gMnSc1	Catagonus
wagneri	wagneri	k6eAd1	wagneri
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
vyhynulé	vyhynulý	k2eAgNnSc4d1	vyhynulé
<g/>
,	,	kIx,	,
prehistorické	prehistorický	k2eAgNnSc4d1	prehistorické
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
žijící	žijící	k2eAgFnSc1d1	žijící
populace	populace	k1gFnSc1	populace
na	na	k7c6	na
severu	sever	k1gInSc6	sever
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
provincie	provincie	k1gFnSc2	provincie
Salta	salto	k1gNnSc2	salto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgMnPc1d1	patřící
do	do	k7c2	do
roku	rok	k1gInSc2	rok
Platygonus	Platygonus	k1gInSc4	Platygonus
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
miocénu	miocén	k1gInSc6	miocén
(	(	kIx(	(
<g/>
mladších	mladý	k2eAgFnPc6d2	mladší
třetihorách	třetihory	k1gFnPc6	třetihory
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pliocénu	pliocén	k1gInSc2	pliocén
(	(	kIx(	(
<g/>
nejmladších	mladý	k2eAgFnPc6d3	nejmladší
třetihorách	třetihory	k1gFnPc6	třetihory
<g/>
)	)	kIx)	)
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc2	pleistocén
(	(	kIx(	(
<g/>
starších	starý	k2eAgFnPc6d2	starší
čtvrtohorách	čtvrtohory	k1gFnPc6	čtvrtohory
<g/>
)	)	kIx)	)
široce	široko	k6eAd1	široko
rozšířeni	rozšířit	k5eAaPmNgMnP	rozšířit
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
chován	chovat	k5eAaImNgInS	chovat
jen	jen	k6eAd1	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2018	[number]	k4	2018
chovalo	chovat	k5eAaImAgNnS	chovat
jen	jen	k6eAd1	jen
šest	šest	k4xCc1	šest
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
institucí	instituce	k1gFnPc2	instituce
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
Zoo	zoo	k1gNnSc1	zoo
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
)	)	kIx)	)
se	s	k7c7	s
37	[number]	k4	37
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
pouze	pouze	k6eAd1	pouze
Tierpark	Tierpark	k1gInSc4	Tierpark
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
odchov	odchov	k1gInSc1	odchov
jednoho	jeden	k4xCgMnSc2	jeden
jedince	jedinec	k1gMnSc2	jedinec
podařil	podařit	k5eAaPmAgInS	podařit
také	také	k9	také
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Planckendael	Planckendaela	k1gFnPc2	Planckendaela
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ho	on	k3xPp3gMnSc4	on
chová	chovat	k5eAaImIp3nS	chovat
pouze	pouze	k6eAd1	pouze
Zoo	zoo	k1gFnSc1	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
dva	dva	k4xCgMnPc1	dva
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
samice	samice	k1gFnPc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
narodila	narodit	k5eAaPmAgFnS	narodit
dvě	dva	k4xCgNnPc4	dva
selata	sele	k1gNnPc4	sele
(	(	kIx(	(
<g/>
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
zoo	zoo	k1gFnSc1	zoo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
teprve	teprve	k6eAd1	teprve
třetí	třetí	k4xOgFnSc7	třetí
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odchov	odchov	k1gInSc1	odchov
podařil	podařit	k5eAaPmAgInS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pekari	pekari	k1gMnSc1	pekari
Wagnerův	Wagnerův	k2eAgMnSc1d1	Wagnerův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
