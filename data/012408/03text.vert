<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
je	být	k5eAaImIp3nS	být
tabulkový	tabulkový	k2eAgInSc1d1	tabulkový
procesor	procesor	k1gInSc1	procesor
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Microsoft	Microsoft	kA	Microsoft
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
počítače	počítač	k1gInSc2	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
5	[number]	k4	5
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
má	mít	k5eAaImIp3nS	mít
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
balíku	balík	k1gInSc2	balík
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
konkurentem	konkurent	k1gMnSc7	konkurent
je	být	k5eAaImIp3nS	být
Calc	Calc	k1gInSc4	Calc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
balíku	balík	k1gInSc2	balík
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
aplikací	aplikace	k1gFnPc2	aplikace
LibreOffice	LibreOffice	k1gFnSc2	LibreOffice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
Excel	Excel	kA	Excel
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
<g/>
"	"	kIx"	"
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
přešlo	přejít	k5eAaPmAgNnS	přejít
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
také	také	k9	také
prostého	prostý	k2eAgNnSc2d1	prosté
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
Excel	Excel	kA	Excel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
tabulkových	tabulkový	k2eAgInPc2d1	tabulkový
kalkulátorů	kalkulátor	k1gInPc2	kalkulátor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
s	s	k7c7	s
programem	program	k1gInSc7	program
nazvaným	nazvaný	k2eAgInSc7d1	nazvaný
Multiplan	Multiplan	k1gInSc4	Multiplan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M.	M.	kA	M.
V	v	k7c4	v
prostředí	prostředí	k1gNnSc4	prostředí
počítačů	počítač	k1gMnPc2	počítač
řízených	řízený	k2eAgMnPc2d1	řízený
systémem	systém	k1gInSc7	systém
MS-DOS	MS-DOS	k1gMnSc1	MS-DOS
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Lotusem	Lotus	k1gInSc7	Lotus
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zahájit	zahájit	k5eAaPmF	zahájit
vývoj	vývoj	k1gInSc4	vývoj
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
tabulkového	tabulkový	k2eAgInSc2d1	tabulkový
kalkulátoru	kalkulátor	k1gInSc2	kalkulátor
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
Excel	Excel	kA	Excel
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
řečeno	říct	k5eAaPmNgNnS	říct
slovy	slovo	k1gNnPc7	slovo
Douga	Doug	k1gMnSc2	Doug
Klundera	Klunder	k1gMnSc2	Klunder
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
umět	umět	k5eAaImF	umět
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
umí	umět	k5eAaImIp3nS	umět
Lotus	Lotus	kA	Lotus
1-2-3	[number]	k4	1-2-3
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
umět	umět	k5eAaImF	umět
lépe	dobře	k6eAd2	dobře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
Macintosh	Macintosh	kA	Macintosh
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
Excel	Excel	kA	Excel
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
GUI	GUI	kA	GUI
využívalo	využívat	k5eAaPmAgNnS	využívat
rozbalovacích	rozbalovací	k2eAgNnPc2d1	rozbalovací
menu	menu	k1gNnPc2	menu
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
klikáním	klikání	k1gNnSc7	klikání
myší	myš	k1gFnPc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
pohodlnější	pohodlný	k2eAgFnSc1d2	pohodlnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
kterémkoli	kterýkoli	k3yIgInSc6	kterýkoli
DOSovém	dosový	k2eAgInSc6d1	dosový
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
se	se	k3xPyFc4	se
např.	např.	kA	např.
dalo	dát	k5eAaPmAgNnS	dát
psát	psát	k5eAaImF	psát
ve	v	k7c6	v
256	[number]	k4	256
fontech	font	k1gInPc6	font
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
koupilo	koupit	k5eAaPmAgNnS	koupit
počítač	počítač	k1gInSc1	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
–	–	k?	–
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
MS	MS	kA	MS
Windows	Windows	kA	Windows
–	–	k?	–
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
táhly	táhnout	k5eAaImAgFnP	táhnout
uživatele	uživatel	k1gMnPc4	uživatel
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
nového	nový	k2eAgInSc2d1	nový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
sjednocení	sjednocení	k1gNnSc2	sjednocení
s	s	k7c7	s
verzí	verze	k1gFnSc7	verze
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
označena	označit	k5eAaPmNgNnP	označit
jako	jako	k9	jako
2.0	[number]	k4	2.0
–	–	k?	–
neexistuje	existovat	k5eNaImIp3nS	existovat
tedy	tedy	k9	tedy
žádný	žádný	k1gMnSc1	žádný
Excel	Excel	kA	Excel
1.0	[number]	k4	1.0
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
již	již	k6eAd1	již
překonával	překonávat	k5eAaImAgMnS	překonávat
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
hlavního	hlavní	k2eAgMnSc2d1	hlavní
konkurenta	konkurent	k1gMnSc2	konkurent
Lotus	Lotus	kA	Lotus
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
širokého	široký	k2eAgNnSc2d1	široké
rozšíření	rozšíření	k1gNnSc2	rozšíření
a	a	k8xC	a
přesto	přesto	k8xC	přesto
až	až	k9	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1992	[number]	k4	1992
se	se	k3xPyFc4	se
neobjevil	objevit	k5eNaPmAgInS	objevit
jediný	jediný	k2eAgInSc1d1	jediný
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
tabulkový	tabulkový	k2eAgInSc1d1	tabulkový
procesor	procesor	k1gInSc1	procesor
pro	pro	k7c4	pro
MS	MS	kA	MS
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
Microsoft	Microsoft	kA	Microsoft
přidal	přidat	k5eAaPmAgMnS	přidat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
Visual	Visual	k1gInSc4	Visual
Basic	Basic	kA	Basic
for	forum	k1gNnPc2	forum
Applications	Applications	k1gInSc1	Applications
(	(	kIx(	(
<g/>
VBA	VBA	kA	VBA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožnil	umožnit	k5eAaPmAgInS	umožnit
široké	široký	k2eAgNnSc4d1	široké
využití	využití	k1gNnSc4	využití
maker	makro	k1gNnPc2	makro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
umožnil	umožnit	k5eAaPmAgInS	umožnit
údajné	údajný	k2eAgNnSc4d1	údajné
šíření	šíření	k1gNnSc4	šíření
makrovirů	makrovir	k1gInPc2	makrovir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k6eAd1	také
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
balíku	balík	k1gInSc2	balík
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
aplikací	aplikace	k1gFnPc2	aplikace
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
s	s	k7c7	s
textovým	textový	k2eAgInSc7d1	textový
editorem	editor	k1gInSc7	editor
Microsoft	Microsoft	kA	Microsoft
Wordem	Word	k1gMnSc7	Word
a	a	k8xC	a
prezentačním	prezentační	k2eAgMnSc7d1	prezentační
editorem	editor	k1gMnSc7	editor
Microsoft	Microsoft	kA	Microsoft
PowerPointem	PowerPoint	k1gMnSc7	PowerPoint
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Excelu	Excel	k1gInSc2	Excel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5.0	[number]	k4	5.0
až	až	k9	až
9.0	[number]	k4	9.0
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
různé	různý	k2eAgFnPc1d1	různá
Easter	Easter	k1gInSc4	Easter
eggs	eggsa	k1gFnPc2	eggsa
–	–	k?	–
skryté	skrytý	k2eAgInPc1d1	skrytý
"	"	kIx"	"
<g/>
bonusy	bonus	k1gInPc1	bonus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
7	[number]	k4	7
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
kapacitní	kapacitní	k2eAgFnPc4d1	kapacitní
možnosti	možnost	k1gFnPc4	možnost
tabulky	tabulka	k1gFnSc2	tabulka
z	z	k7c2	z
65	[number]	k4	65
536	[number]	k4	536
na	na	k7c4	na
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
milion	milion	k4xCgInSc1	milion
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
z	z	k7c2	z
256	[number]	k4	256
(	(	kIx(	(
<g/>
IV	IV	kA	IV
<g/>
)	)	kIx)	)
na	na	k7c4	na
16	[number]	k4	16
384	[number]	k4	384
(	(	kIx(	(
<g/>
XFD	XFD	kA	XFD
<g/>
)	)	kIx)	)
sloupců	sloupec	k1gInPc2	sloupec
<g/>
.	.	kIx.	.
<g/>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
Excelu	Excel	k1gInSc2	Excel
pro	pro	k7c4	pro
prostředí	prostředí	k1gNnSc4	prostředí
Windows	Windows	kA	Windows
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
Excel	Excel	kA	Excel
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
Excelu	Excel	k1gInSc2	Excel
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
Mac	Mac	kA	Mac
OS	OS	kA	OS
je	být	k5eAaImIp3nS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xC	jako
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
for	forum	k1gNnPc2	forum
Mac	Mac	kA	Mac
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Syntaxe	syntaxe	k1gFnSc1	syntaxe
výpočtů	výpočet	k1gInPc2	výpočet
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Excelu	Excel	k1gInSc6	Excel
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
výpočtů	výpočet	k1gInPc2	výpočet
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
-	-	kIx~	-
nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
výpočty	výpočet	k1gInPc4	výpočet
<g/>
)	)	kIx)	)
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
kromě	kromě	k7c2	kromě
abeced	abeceda	k1gFnPc2	abeceda
a	a	k8xC	a
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
mezery	mezera	k1gFnPc1	mezera
a	a	k8xC	a
desetinné	desetinný	k2eAgFnPc1d1	desetinná
čárky	čárka	k1gFnPc1	čárka
zejména	zejména	k9	zejména
</s>
</p>
<p>
<s>
organizační	organizační	k2eAgInPc1d1	organizační
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
úvodní	úvodní	k2eAgNnSc1d1	úvodní
rovnítko	rovnítko	k1gNnSc1	rovnítko
vzorců	vzorec	k1gInPc2	vzorec
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgInSc4d1	úvodní
apostrof	apostrof	k1gInSc4	apostrof
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
uvozovky	uvozovka	k1gFnPc1	uvozovka
(	(	kIx(	(
<g/>
jen	jen	k9	jen
horní	horní	k2eAgFnSc6d1	horní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závorky	závorka	k1gFnPc1	závorka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
kulaté	kulatý	k2eAgInPc1d1	kulatý
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgFnSc1d1	sdružující
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
středník	středník	k1gInSc1	středník
(	(	kIx(	(
<g/>
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
anglické	anglický	k2eAgFnSc6d1	anglická
verzi	verze	k1gFnSc6	verze
čárka	čárka	k1gFnSc1	čárka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
(	(	kIx(	(
<g/>
s	s	k7c7	s
významem	význam	k1gInSc7	význam
oblasti	oblast	k1gFnSc2	oblast
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
běžné	běžný	k2eAgFnPc1d1	běžná
aritmetické	aritmetický	k2eAgFnPc1d1	aritmetická
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
operace	operace	k1gFnPc1	operace
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
nejen	nejen	k6eAd1	nejen
běžné	běžný	k2eAgNnSc4d1	běžné
aritmetické	aritmetický	k2eAgNnSc4d1	aritmetické
např.	např.	kA	např.
násobení	násobení	k1gNnSc1	násobení
<g/>
,	,	kIx,	,
sčítání	sčítání	k1gNnSc1	sčítání
<g/>
,	,	kIx,	,
umocňování	umocňování	k1gNnSc1	umocňování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nerovnostní	rovnostní	k2eNgMnPc1d1	rovnostní
s	s	k7c7	s
Booleovským	booleovský	k2eAgInSc7d1	booleovský
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
.	.	kIx.	.
<g/>
zapisované	zapisovaný	k2eAgFnPc4d1	zapisovaná
obvyklými	obvyklý	k2eAgInPc7d1	obvyklý
operátory	operátor	k1gInPc7	operátor
(	(	kIx(	(
<g/>
značkami	značka	k1gFnPc7	značka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
+	+	kIx~	+
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
součet	součet	k1gInSc1	součet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
*	*	kIx~	*
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
^	^	kIx~	^
(	(	kIx(	(
<g/>
umocnění	umocnění	k1gNnSc1	umocnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
&	&	k?	&
(	(	kIx(	(
<g/>
spojení	spojení	k1gNnSc1	spojení
textů	text	k1gInPc2	text
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
=	=	kIx~	=
(	(	kIx(	(
<g/>
rovno	roven	k2eAgNnSc1d1	rovno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
>	>	kIx)	>
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tak	tak	k6eAd1	tak
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
matematické	matematický	k2eAgFnPc4d1	matematická
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
statistické	statistický	k2eAgFnPc1d1	statistická
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
SUMA	suma	k1gFnSc1	suma
(	(	kIx(	(
<g/>
součet	součet	k1gInSc1	součet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MIN	min	kA	min
(	(	kIx(	(
<g/>
minimum	minimum	k1gNnSc4	minimum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MAX	Max	k1gMnSc1	Max
(	(	kIx(	(
<g/>
maximum	maximum	k1gNnSc1	maximum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
POČET	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
číselných	číselný	k2eAgFnPc2d1	číselná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
podmíněnými	podmíněný	k2eAgInPc7d1	podmíněný
zápočty	zápočet	k1gInPc7	zápočet
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
SUMIF	SUMIF	kA	SUMIF
nebo	nebo	k8xC	nebo
COUNTIF	COUNTIF	kA	COUNTIF
<g/>
,	,	kIx,	,
<g/>
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
PRŮMĚR	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
MAX	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
MEDIAN	MEDIAN	kA	MEDIAN
<g/>
,	,	kIx,	,
POČET	počet	k1gInSc1	počet
nebo	nebo	k8xC	nebo
funkce	funkce	k1gFnSc1	funkce
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
komplexními	komplexní	k2eAgNnPc7d1	komplexní
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
logické	logický	k2eAgFnPc1d1	logická
funkce	funkce	k1gFnPc1	funkce
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
především	především	k9	především
logické	logický	k2eAgFnPc1d1	logická
operace	operace	k1gFnPc1	operace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
KDYŽ	když	k8xS	když
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
,	,	kIx,	,
NE	ne	k9	ne
nebo	nebo	k8xC	nebo
NEBO	nebo	k8xC	nebo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vyhledávací	vyhledávací	k2eAgFnPc1d1	vyhledávací
a	a	k8xC	a
odkazovací	odkazovací	k2eAgFnPc1d1	odkazovací
(	(	kIx(	(
<g/>
vytyčovací	vytyčovací	k2eAgFnPc1d1	vytyčovací
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přebírat	přebírat	k5eAaImF	přebírat
výsledky	výsledek	k1gInPc4	výsledek
(	(	kIx(	(
<g/>
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
)	)	kIx)	)
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gNnSc2	jejich
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
např.	např.	kA	např.
INDEX	index	k1gInSc1	index
<g/>
,	,	kIx,	,
POZVYHLEDAT	POZVYHLEDAT	kA	POZVYHLEDAT
<g/>
,	,	kIx,	,
VYHLEDAT	vyhledat	k5eAaPmF	vyhledat
<g/>
,	,	kIx,	,
SVYHLEDAT	SVYHLEDAT	kA	SVYHLEDAT
nebo	nebo	k8xC	nebo
POSUN	posun	k1gInSc1	posun
</s>
</p>
<p>
<s>
textové	textový	k2eAgFnPc4d1	textová
(	(	kIx(	(
<g/>
řetězcové	řetězcový	k2eAgFnPc4d1	řetězcová
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rovněž	rovněž	k6eAd1	rovněž
vyhledávacích	vyhledávací	k2eAgInPc2d1	vyhledávací
a	a	k8xC	a
dosazujících	dosazující	k2eAgInPc2d1	dosazující
<g/>
,	,	kIx,	,
např.	např.	kA	např.
MALÉ	malé	k1gNnSc1	malé
<g/>
,	,	kIx,	,
DOSADIT	dosadit	k5eAaPmF	dosadit
<g/>
,	,	kIx,	,
ZLEVA	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
HLEDAT	hledat	k5eAaImF	hledat
<g/>
,	,	kIx,	,
CONCATENATE	CONCATENATE	kA	CONCATENATE
</s>
</p>
<p>
<s>
převodní	převodní	k2eAgFnSc1d1	převodní
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
NEPŘÍMÝ	přímý	k2eNgInSc1d1	nepřímý
<g/>
.	.	kIx.	.
<g/>
ODKAZPořadí	ODKAZPořadí	k1gNnSc1	ODKAZPořadí
výpočtů	výpočet	k1gInPc2	výpočet
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
běžné	běžný	k2eAgFnPc4d1	běžná
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
uživatel	uživatel	k1gMnSc1	uživatel
změnit	změnit	k5eAaPmF	změnit
nebo	nebo	k8xC	nebo
potvrdit	potvrdit	k5eAaPmF	potvrdit
používáním	používání	k1gNnSc7	používání
kulatých	kulatý	k2eAgFnPc2d1	kulatá
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konkurenční	konkurenční	k2eAgInPc4d1	konkurenční
produkty	produkt	k1gInPc4	produkt
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
profesionální	profesionální	k2eAgNnSc4d1	profesionální
užití	užití	k1gNnSc4	užití
je	být	k5eAaImIp3nS	být
Excel	Excel	kA	Excel
prakticky	prakticky	k6eAd1	prakticky
bezkonkurenční	bezkonkurenční	k2eAgInSc1d1	bezkonkurenční
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
systémově	systémově	k6eAd1	systémově
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgFnPc4d1	soukromá
nebo	nebo	k8xC	nebo
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejen	nejen	k6eAd1	nejen
základní	základní	k2eAgInPc1d1	základní
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc1	užití
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
bezplatné	bezplatný	k2eAgFnPc1d1	bezplatná
verze	verze	k1gFnPc1	verze
kancelářských	kancelářský	k2eAgMnPc2d1	kancelářský
balíků	balík	k1gInPc2	balík
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
zejména	zejména	k9	zejména
</s>
</p>
<p>
<s>
Apache	Apache	k1gFnSc1	Apache
OpenOffice	OpenOffice	k1gFnSc2	OpenOffice
</s>
</p>
<p>
<s>
LibreOffice	LibreOffice	k1gFnSc1	LibreOffice
</s>
</p>
<p>
<s>
NeoOffice	NeoOffice	k1gFnSc1	NeoOffice
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Docs	Docsa	k1gFnPc2	Docsa
-	-	kIx~	-
síťový	síťový	k2eAgInSc4d1	síťový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rovněž	rovněž	k9	rovněž
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
tabulkami	tabulka	k1gFnPc7	tabulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
opravdu	opravdu	k6eAd1	opravdu
základními	základní	k2eAgInPc7d1	základní
</s>
</p>
<p>
<s>
Pages	Pages	k1gInSc1	Pages
(	(	kIx(	(
<g/>
iWork	iWork	k1gInSc1	iWork
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Microsoft	Microsoft	kA	Microsoft
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
portál	portál	k1gInSc1	portál
ProExcel	ProExcel	k1gFnSc1	ProExcel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
Excelu	Excel	k1gInSc2	Excel
a	a	k8xC	a
programování	programování	k1gNnSc2	programování
ve	v	k7c6	v
VBA	VBA	kA	VBA
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Na	na	k7c6	na
Excel	Excel	kA	Excel
-	-	kIx~	-
web	web	k1gInSc1	web
o	o	k7c4	o
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
a	a	k8xC	a
VBA	VBA	kA	VBA
</s>
</p>
