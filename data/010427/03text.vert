<p>
<s>
Cylindr	cylindr	k1gInSc1	cylindr
je	být	k5eAaImIp3nS	být
pánský	pánský	k2eAgInSc1d1	pánský
vysoký	vysoký	k2eAgInSc1d1	vysoký
klobouk	klobouk	k1gInSc1	klobouk
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
vrškem	vršek	k1gInSc7	vršek
a	a	k8xC	a
širokou	široký	k2eAgFnSc7d1	široká
krempou	krempa	k1gFnSc7	krempa
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
víceméně	víceméně	k9	víceméně
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
jako	jako	k9	jako
doplněk	doplněk	k1gInSc4	doplněk
k	k	k7c3	k
fraku	frak	k1gInSc3	frak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
cylindr	cylindr	k1gInSc1	cylindr
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
a	a	k8xC	a
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vyšel	vyjít	k5eAaPmAgMnS	vyjít
anglický	anglický	k2eAgMnSc1d1	anglický
kloboučník	kloboučník	k1gMnSc1	kloboučník
John	John	k1gMnSc1	John
Hetherington	Hetherington	k1gInSc4	Hetherington
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
módní	módní	k2eAgFnSc1d1	módní
novinka	novinka	k1gFnSc1	novinka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
senzaci	senzace	k1gFnSc4	senzace
i	i	k8xC	i
veřejné	veřejný	k2eAgNnSc4d1	veřejné
pohoršení	pohoršení	k1gNnSc4	pohoršení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
obrovské	obrovský	k2eAgNnSc4d1	obrovské
překvapení	překvapení	k1gNnSc4	překvapení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
cylindr	cylindr	k1gInSc1	cylindr
způsobil	způsobit	k5eAaPmAgInS	způsobit
doslova	doslova	k6eAd1	doslova
poprask	poprask	k1gInSc1	poprask
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
londýnských	londýnský	k2eAgFnPc6d1	londýnská
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nositelem	nositel	k1gMnSc7	nositel
byl	být	k5eAaImAgMnS	být
prodejce	prodejce	k1gMnSc1	prodejce
galanterie	galanterie	k1gFnSc2	galanterie
John	John	k1gMnSc1	John
Hetherington	Hetherington	k1gInSc1	Hetherington
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
a	a	k8xC	a
také	také	k6eAd1	také
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vyšel	vyjít	k5eAaPmAgMnS	vyjít
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobového	dobový	k2eAgNnSc2d1	dobové
svědectví	svědectví	k1gNnSc2	svědectví
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
začali	začít	k5eAaPmAgMnP	začít
kolemjdoucí	kolemjdoucí	k2eAgMnPc1d1	kolemjdoucí
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
klobouk	klobouk	k1gInSc4	klobouk
panikařit	panikařit	k5eAaImF	panikařit
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dam	dáma	k1gFnPc2	dáma
omdlelo	omdlet	k5eAaPmAgNnS	omdlet
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
křičely	křičet	k5eAaImAgFnP	křičet
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
štěkali	štěkat	k5eAaImAgMnP	štěkat
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
poslíček	poslíček	k1gMnSc1	poslíček
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
převálcoval	převálcovat	k5eAaPmAgInS	převálcovat
dav	dav	k1gInSc1	dav
<g/>
.	.	kIx.	.
</s>
<s>
Hetherington	Hetherington	k1gInSc1	Hetherington
byl	být	k5eAaImAgInS	být
předveden	předvést	k5eAaPmNgInS	předvést
před	před	k7c4	před
soud	soud	k1gInSc4	soud
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
vysokou	vysoký	k2eAgFnSc4d1	vysoká
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
zářivé	zářivý	k2eAgInPc1d1	zářivý
odlesky	odlesk	k1gInPc1	odlesk
měly	mít	k5eAaImAgInP	mít
vystrašit	vystrašit	k5eAaPmF	vystrašit
plaché	plachý	k2eAgMnPc4d1	plachý
a	a	k8xC	a
skromné	skromný	k2eAgMnPc4d1	skromný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
povyku	povyk	k1gInSc2	povyk
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Klobouk	klobouk	k1gInSc1	klobouk
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
pokrývce	pokrývka	k1gFnSc6	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nosili	nosit	k5eAaImAgMnP	nosit
puritáni	puritán	k1gMnPc1	puritán
a	a	k8xC	a
kvakeři	kvaker	k1gMnPc1	kvaker
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
protestantských	protestantský	k2eAgFnPc2d1	protestantská
sekt	sekta	k1gFnPc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Kvakerský	kvakerský	k2eAgInSc1d1	kvakerský
klobouk	klobouk	k1gInSc1	klobouk
byl	být	k5eAaImAgInS	být
však	však	k9	však
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc4d2	širší
stříšku	stříška	k1gFnSc4	stříška
než	než	k8xS	než
dnešní	dnešní	k2eAgInSc4d1	dnešní
cylindr	cylindr	k1gInSc4	cylindr
<g/>
.	.	kIx.	.
</s>
<s>
Cylindr	cylindr	k1gInSc1	cylindr
jak	jak	k8xS	jak
ho	on	k3xPp3gInSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
z	z	k7c2	z
filcu	filc	k1gInSc2	filc
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
z	z	k7c2	z
bobří	bobří	k2eAgFnSc2d1	bobří
kožešiny	kožešina	k1gFnSc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
prince	princ	k1gMnSc2	princ
Alberta	Albert	k1gMnSc2	Albert
<g/>
,	,	kIx,	,
manžela	manžel	k1gMnSc2	manžel
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
cylindry	cylindr	k1gInPc1	cylindr
i	i	k9	i
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgFnPc2d1	americká
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
modifikace	modifikace	k1gFnSc1	modifikace
cylindru	cylindr	k1gInSc2	cylindr
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
stovepipe	stovepipat	k5eAaPmIp3nS	stovepipat
hat	hat	k0	hat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
klobouky	klobouk	k1gInPc1	klobouk
byly	být	k5eAaImAgInP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
cylindry	cylindr	k1gInPc1	cylindr
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
spodku	spodek	k1gInSc6	spodek
a	a	k8xC	a
vrcholu	vrchol	k1gInSc6	vrchol
nebyly	být	k5eNaImAgFnP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
připomínaly	připomínat	k5eAaImAgInP	připomínat
komín	komín	k1gInSc4	komín
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
pipe	pipe	k6eAd1	pipe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
klobouk	klobouk	k1gInSc1	klobouk
nosil	nosit	k5eAaImAgInS	nosit
např.	např.	kA	např.
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
modifikace	modifikace	k1gFnSc1	modifikace
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zabudovanou	zabudovaný	k2eAgFnSc4d1	zabudovaná
drátěnou	drátěný	k2eAgFnSc4d1	drátěná
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
složit	složit	k5eAaPmF	složit
naplocho	naplocho	k6eAd1	naplocho
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
kloboukům	klobouk	k1gInPc3	klobouk
se	se	k3xPyFc4	se
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
říkalo	říkat	k5eAaImAgNnS	říkat
opera	opera	k1gFnSc1	opera
hats	hats	k1gInSc4	hats
nebo	nebo	k8xC	nebo
Gibbus	Gibbus	k1gInSc4	Gibbus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
cylindry	cylindr	k1gInPc1	cylindr
začaly	začít	k5eAaPmAgInP	začít
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
vrstvy	vrstva	k1gFnPc1	vrstva
začaly	začít	k5eAaPmAgFnP	začít
nosit	nosit	k5eAaImF	nosit
buřinky	buřinka	k1gFnPc4	buřinka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
bowler	bowler	k1gInSc1	bowler
hats	hatsa	k1gFnPc2	hatsa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
měkké	měkký	k2eAgInPc4d1	měkký
filcové	filcový	k2eAgInPc4d1	filcový
klobouky	klobouk	k1gInPc4	klobouk
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
borsalino	borsalin	k2eAgNnSc4d1	borsalino
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
klobouky	klobouk	k1gInPc1	klobouk
byly	být	k5eAaImAgInP	být
vhodnější	vhodný	k2eAgInPc1d2	vhodnější
pro	pro	k7c4	pro
městský	městský	k2eAgInSc4d1	městský
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
také	také	k9	také
byly	být	k5eAaImAgInP	být
vhodnější	vhodný	k2eAgInPc1d2	vhodnější
pro	pro	k7c4	pro
masovou	masový	k2eAgFnSc4d1	masová
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
cylindr	cylindr	k1gInSc1	cylindr
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ručně	ručně	k6eAd1	ručně
a	a	k8xC	a
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
kloboučníkem	kloboučník	k1gMnSc7	kloboučník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cylinder	Cylinder	k1gInSc1	Cylinder
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
vyšších	vysoký	k2eAgFnPc2d2	vyšší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
terčem	terč	k1gInSc7	terč
satiristů	satirista	k1gMnPc2	satirista
a	a	k8xC	a
společenských	společenský	k2eAgMnPc2d1	společenský
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
cylindry	cylindr	k1gInPc7	cylindr
staly	stát	k5eAaPmAgFnP	stát
raritou	rarita	k1gFnSc7	rarita
v	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
však	však	k9	však
nadále	nadále	k6eAd1	nadále
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
společenského	společenský	k2eAgInSc2d1	společenský
oděvu	oděv	k1gInSc2	oděv
-	-	kIx~	-
vycházkového	vycházkový	k2eAgInSc2d1	vycházkový
obleku	oblek	k1gInSc2	oblek
nebo	nebo	k8xC	nebo
fraku	frak	k1gInSc2	frak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
cylindr	cylindr	k1gInSc1	cylindr
vymizel	vymizet	k5eAaPmAgInS	vymizet
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gNnSc4	jeho
využívání	využívání	k1gNnSc4	využívání
v	v	k7c6	v
politických	politický	k2eAgInPc6d1	politický
a	a	k8xC	a
dipomatických	dipomatický	k2eAgInPc6d1	dipomatický
kruzích	kruh	k1gInPc6	kruh
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
diplomaty	diplomat	k1gMnPc7	diplomat
strhla	strhnout	k5eAaPmAgFnS	strhnout
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
měli	mít	k5eAaImAgMnP	mít
podřídit	podřídit	k5eAaPmF	podřídit
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
konvencím	konvence	k1gFnPc3	konvence
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
cylindr	cylindr	k1gInSc4	cylindr
<g/>
.	.	kIx.	.
</s>
<s>
Cylindry	cylindr	k1gInPc1	cylindr
však	však	k9	však
i	i	k9	i
zde	zde	k6eAd1	zde
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cylindr	cylindr	k1gInSc1	cylindr
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
figurek	figurka	k1gFnPc2	figurka
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
hře	hra	k1gFnSc6	hra
Monopoly	monopol	k1gInPc4	monopol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
nositelé	nositel	k1gMnPc1	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
si	se	k3xPyFc3	se
během	během	k7c2	během
mnoha	mnoho	k4c2	mnoho
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
oblíbily	oblíbit	k5eAaPmAgFnP	oblíbit
různé	různý	k2eAgFnPc1d1	různá
známé	známý	k2eAgFnPc1d1	známá
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
dílech	díl	k1gInPc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
nejznámější	známý	k2eAgMnPc4d3	nejznámější
nositele	nositel	k1gMnPc4	nositel
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
pohádkář	pohádkář	k1gMnSc1	pohádkář
</s>
</p>
<p>
<s>
Fred	Fred	k1gMnSc1	Fred
Astaire	Astair	k1gInSc5	Astair
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
Isambard	Isambard	k1gInSc1	Isambard
Kingdom	Kingdom	k1gInSc1	Kingdom
Brunel	Brunel	k1gInSc4	Brunel
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
inženýr	inženýr	k1gMnSc1	inženýr
</s>
</p>
<p>
<s>
Marc	Marc	k6eAd1	Marc
Bolan	Bolan	k1gMnSc1	Bolan
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
anglické	anglický	k2eAgFnSc2d1	anglická
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
T.	T.	kA	T.
<g/>
Rex	Rex	k1gMnSc1	Rex
</s>
</p>
<p>
<s>
Boy	boy	k1gMnSc1	boy
George	Georg	k1gMnSc2	Georg
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Marlene	Marlen	k1gInSc5	Marlen
Dietrichová	Dietrichový	k2eAgFnSc1d1	Dietrichová
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
šestnáctý	šestnáctý	k4xOgMnSc1	šestnáctý
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Ebenezer	Ebenezer	k1gInSc1	Ebenezer
Scrooge	Scroog	k1gInSc2	Scroog
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
novely	novela	k1gFnSc2	novela
Charlese	Charles	k1gMnSc2	Charles
Dickense	Dickens	k1gMnSc2	Dickens
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
koleda	koleda	k1gFnSc1	koleda
</s>
</p>
<p>
<s>
Strýček	strýček	k1gMnSc1	strýček
Skrblík	skrblík	k1gMnSc1	skrblík
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
pohádek	pohádka	k1gFnPc2	pohádka
Walta	Walt	k1gMnSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
</s>
</p>
<p>
<s>
Strýček	strýček	k1gMnSc1	strýček
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
personifikace	personifikace	k1gFnSc1	personifikace
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Willy	Willa	k1gFnPc1	Willa
Wonka	Wonka	k1gFnSc1	Wonka
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Roalda	Roald	k1gMnSc2	Roald
Dahla	Dahl	k1gMnSc2	Dahl
Willy	Willa	k1gMnSc2	Willa
Wonka	Wonka	k1gFnSc1	Wonka
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
</s>
</p>
<p>
<s>
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
'	'	kIx"	'
<g/>
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
Slash	Slash	k1gMnSc1	Slash
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
a	a	k8xC	a
Velvet	Velvet	k1gMnSc1	Velvet
RevolverA	RevolverA	k1gFnSc4	RevolverA
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cylinder	Cylinder	k1gInSc1	Cylinder
(	(	kIx(	(
<g/>
klobúk	klobúk	k1gInSc1	klobúk
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cylindr	cylindr	k1gInSc1	cylindr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
článek	článek	k1gInSc1	článek
o	o	k7c6	o
pánských	pánský	k2eAgInPc6d1	pánský
kloboucích	klobouk	k1gInPc6	klobouk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
