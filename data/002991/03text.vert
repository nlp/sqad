<s>
Pierre	Pierr	k1gMnSc5	Pierr
Culliford	Culliford	k1gMnSc1	Culliford
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Peyo	Peyo	k6eAd1	Peyo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
belgický	belgický	k2eAgMnSc1d1	belgický
kreslíř	kreslíř	k1gMnSc1	kreslíř
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
Šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Peyův	Peyův	k2eAgMnSc1d1	Peyův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Belgičanka	Belgičanka	k1gFnSc1	Belgičanka
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Culliford	Culliford	k1gMnSc1	Culliford
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
anglického	anglický	k2eAgMnSc2d1	anglický
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
investora	investor	k1gMnSc2	investor
a	a	k8xC	a
belgické	belgický	k2eAgFnSc2d1	belgická
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
kreslil	kreslit	k5eAaImAgMnS	kreslit
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
školních	školní	k2eAgInPc2d1	školní
sešitů	sešit	k1gInPc2	sešit
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgInS	milovat
především	především	k9	především
gymnastiku	gymnastika	k1gFnSc4	gymnastika
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
hrál	hrát	k5eAaImAgInS	hrát
při	při	k7c6	při
škole	škola	k1gFnSc6	škola
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgMnS	milovat
četbu	četba	k1gFnSc4	četba
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pravidelných	pravidelný	k2eAgNnPc6d1	pravidelné
rodinných	rodinný	k2eAgNnPc6d1	rodinné
setkáních	setkání	k1gNnPc6	setkání
vždy	vždy	k6eAd1	vždy
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
znal	znát	k5eAaImAgInS	znát
z	z	k7c2	z
hodin	hodina	k1gFnPc2	hodina
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
-	-	kIx~	-
příběhy	příběh	k1gInPc1	příběh
Myšáka	myšák	k1gMnSc2	myšák
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mouse	k1gFnSc2	Mouse
apod.	apod.	kA	apod.
Byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
katolických	katolický	k2eAgMnPc2d1	katolický
skautů	skaut	k1gMnPc2	skaut
-	-	kIx~	-
de	de	k?	de
Fédération	Fédération	k1gInSc1	Fédération
des	des	k1gNnSc1	des
Scouts	Scoutsa	k1gFnPc2	Scoutsa
Catholiques	Catholiquesa	k1gFnPc2	Catholiquesa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
středoškolské	středoškolský	k2eAgNnSc1d1	středoškolské
studium	studium	k1gNnSc1	studium
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jak	jak	k6eAd1	jak
na	na	k7c6	na
houpačce	houpačka	k1gFnSc6	houpačka
<g/>
:	:	kIx,	:
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
skončil	skončit	k5eAaPmAgMnS	skončit
a	a	k8xC	a
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
teoretičtější	teoretický	k2eAgInSc4d2	teoretičtější
směr	směr	k1gInSc4	směr
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
spolužáci	spolužák	k1gMnPc1	spolužák
měli	mít	k5eAaImAgMnP	mít
náskok	náskok	k1gInSc4	náskok
a	a	k8xC	a
Culliford	Culliford	k1gInSc4	Culliford
studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
let	léto	k1gNnPc2	léto
hledat	hledat	k5eAaImF	hledat
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
setrval	setrvat	k5eAaPmAgMnS	setrvat
rok	rok	k1gInSc4	rok
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
mu	on	k3xPp3gMnSc3	on
připadala	připadat	k5eAaImAgFnS	připadat
nudná	nudný	k2eAgFnSc1d1	nudná
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dělal	dělat	k5eAaImAgMnS	dělat
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Setrvával	setrvávat	k5eAaImAgMnS	setrvávat
ve	v	k7c6	v
skautu	skaut	k1gMnSc6	skaut
<g/>
,	,	kIx,	,
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
sedmnácti	sedmnáct	k4xCc2	sedmnáct
již	již	k6eAd1	již
ne	ne	k9	ne
v	v	k7c6	v
katolickém	katolický	k2eAgNnSc6d1	katolické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
do	do	k7c2	do
kreslícího	kreslící	k2eAgNnSc2d1	kreslící
studia	studio	k1gNnSc2	studio
CBA	CBA	kA	CBA
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
retuše	retuš	k1gFnPc4	retuš
s	s	k7c7	s
kvašem	kvaš	k1gInSc7	kvaš
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
kolegou	kolega	k1gMnSc7	kolega
pánů	pan	k1gMnPc2	pan
-	-	kIx~	-
André	André	k1gMnSc1	André
Franquin	Franquin	k1gMnSc1	Franquin
<g/>
,	,	kIx,	,
Eddy	Edda	k1gFnPc1	Edda
Paape	Paap	k1gMnSc5	Paap
<g/>
,	,	kIx,	,
en	en	k?	en
Morris	Morris	k1gInSc1	Morris
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
však	však	k9	však
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
zbankrotovala	zbankrotovat	k5eAaPmAgFnS	zbankrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgMnS	zůstat
však	však	k9	však
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
šli	jít	k5eAaImAgMnP	jít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
znovuzaložené	znovuzaložený	k2eAgFnPc4d1	znovuzaložený
Spirou	Spira	k1gFnSc7	Spira
(	(	kIx(	(
Robbedoes	Robbedoes	k1gInSc1	Robbedoes
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Culliford	Culliford	k6eAd1	Culliford
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
svou	svůj	k3xOyFgFnSc4	svůj
kreslířskou	kreslířský	k2eAgFnSc4d1	kreslířská
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
naučit	naučit	k5eAaPmF	naučit
kreslit	kreslit	k5eAaImF	kreslit
humoristicky	humoristicky	k6eAd1	humoristicky
<g/>
,	,	kIx,	,
ne	ne	k9	ne
akademicky	akademicky	k6eAd1	akademicky
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
reklamního	reklamní	k2eAgInSc2d1	reklamní
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
podepsal	podepsat	k5eAaPmAgMnS	podepsat
poprvé	poprvé	k6eAd1	poprvé
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
komiks	komiks	k1gInSc4	komiks
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Peyo	Peyo	k1gMnSc1	Peyo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jeho	jeho	k3xOp3gMnSc1	jeho
bratranec	bratranec	k1gMnSc1	bratranec
vyslovoval	vyslovovat	k5eAaImAgMnS	vyslovovat
jeho	jeho	k3xOp3gFnSc4	jeho
přezdívku	přezdívka	k1gFnSc4	přezdívka
(	(	kIx(	(
Pierrot	Pierrot	k1gInSc4	Pierrot
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
z	z	k7c2	z
wikipedie	wikipedie	k1gFnSc2	wikipedie
<g/>
.	.	kIx.	.
<g/>
be	be	k?	be
Šmoulové	šmoula	k1gMnPc1	šmoula
</s>
