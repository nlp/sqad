<s>
Pelyněk	pelyněk	k1gInSc1	pelyněk
estragon	estragon	k1gInSc1	estragon
(	(	kIx(	(
<g/>
Artemisia	Artemisia	k1gFnSc1	Artemisia
dracunculus	dracunculus	k1gMnSc1	dracunculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
pelyněk	pelyněk	k1gInSc1	pelyněk
kozalec	kozalec	k1gInSc1	kozalec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
(	(	kIx(	(
<g/>
Asteraceae	Asteracea	k1gFnSc2	Asteracea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
