<s>
Děkanát	děkanát	k1gInSc1
Uherský	uherský	k2eAgInSc4d1
Brod	Brod	k1gInSc4
</s>
<s>
Děkanát	děkanát	k1gInSc1
Uherský	uherský	k2eAgInSc1d1
BrodDiecéze	BrodDiecéza	k1gFnSc3
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Svatopluk	Svatopluk	k1gMnSc1
Pavlica	Pavlica	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
Farář	farář	k1gMnSc1
v	v	k7c6
Uherském	uherský	k2eAgInSc6d1
Brodě	Brod	k1gInSc6
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
prosinci	prosinec	k1gInSc3
2019	#num#	k4
</s>
<s>
Děkanát	děkanát	k1gInSc1
Uherský	uherský	k2eAgInSc4d1
Brod	Brod	k1gInSc4
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc2
19	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Svatopluk	Svatopluk	k1gMnSc1
Pavlica	Pavlica	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Uherském	uherský	k2eAgInSc6d1
Brodě	Brod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místoděkanem	Místoděkan	k1gInSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Hofírek	Hofírek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
děkanátu	děkanát	k1gInSc6
působí	působit	k5eAaImIp3nS
16	#num#	k4
diecézních	diecézní	k2eAgMnPc2d1
a	a	k8xC
4	#num#	k4
řeholních	řeholní	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církevní	církevní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
existoval	existovat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
děkanát	děkanát	k1gInSc4
lucký	lucký	k2eAgInSc4d1
v	v	k7c6
hranicích	hranice	k1gFnPc6
někdejší	někdejší	k2eAgFnSc2d1
Lucké	lucký	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
území	území	k1gNnSc1
definitivně	definitivně	k6eAd1
připojené	připojený	k2eAgFnSc2d1
k	k	k7c3
českým	český	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
teprve	teprve	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherskobrodský	uherskobrodský	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
ve	v	k7c6
shodném	shodný	k2eAgNnSc6d1
území	území	k1gNnSc6
zmíněného	zmíněný	k2eAgInSc2d1
luckého	lucký	k2eAgInSc2d1
děkanátu	děkanát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
Erb	erb	k1gInSc1
děkanátu	děkanát	k1gInSc2
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
V	v	k7c6
černém	černý	k2eAgNnSc6d1
poli	pole	k1gNnSc6
na	na	k7c6
zeleném	zelený	k2eAgInSc6d1
trávníku	trávník	k1gInSc6
průčelí	průčelí	k1gNnSc2
stříbrného	stříbrný	k2eAgInSc2d1
dvouvěžového	dvouvěžový	k2eAgInSc2d1
kostela	kostel	k1gInSc2
se	s	k7c7
zelenými	zelený	k2eAgFnPc7d1
střechami	střecha	k1gFnPc7
<g/>
,	,	kIx,
opatřenými	opatřený	k2eAgInPc7d1
zlatými	zlatý	k2eAgInPc7d1
křížky	křížek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
věžemi	věž	k1gFnPc7
je	být	k5eAaImIp3nS
zlatá	zlatý	k2eAgFnSc1d1
lilie	lilie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
Pitín	Pitín	k1gMnSc1
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
děkanátu	děkanát	k1gInSc2
je	být	k5eAaImIp3nS
rodištěm	rodiště	k1gNnSc7
olomouckého	olomoucký	k2eAgMnSc2d1
arcibiskupa	arcibiskup	k1gMnSc2
Josefa	Josef	k1gMnSc2
Matochy	Matoch	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
děkanství	děkanství	k1gNnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
osobního	osobní	k2eAgInSc2d1
erbu	erb	k1gInSc2
arcibiskupa	arcibiskup	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lilie	lilie	k1gFnSc1
mezi	mezi	k7c7
věžemi	věž	k1gFnPc7
ukazuje	ukazovat	k5eAaImIp3nS
na	na	k7c4
mariánský	mariánský	k2eAgInSc4d1
titul	titul	k1gInSc4
farního	farní	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
v	v	k7c6
Uherském	uherský	k2eAgInSc6d1
Brodě	Brod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
</s>
<s>
FarnostSprávceFarní	FarnostSprávceFarní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Bánov	Bánov	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Putala	Putal	k1gMnSc2
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
</s>
<s>
Bojkovice	Bojkovice	k1gFnPc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Změlík	Změlík	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
Březová	březový	k2eAgFnSc1d1
u	u	k7c2
Uherského	uherský	k2eAgInSc2d1
Brodu	Brod	k1gInSc2
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Vágner	Vágner	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc4
a	a	k8xC
Metoděje	Metoděj	k1gMnSc4
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Němčí	němčit	k5eAaImIp3nS
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Hofírek	Hofírek	k1gInSc1
</s>
<s>
sv.	sv.	kA
Filipa	Filip	k1gMnSc4
a	a	k8xC
Jakuba	Jakub	k1gMnSc4
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Němčí	němčit	k5eAaImIp3nS
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Dolní	dolní	k2eAgFnPc1d1
Němčí	němčit	k5eAaImIp3nP
</s>
<s>
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
</s>
<s>
Hradčovice	Hradčovice	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Vojtěch	Vojtěch	k1gMnSc1
Daněk	Daněk	k1gMnSc1
</s>
<s>
Všichni	všechen	k3xTgMnPc1
svatí	svatý	k2eAgMnPc1d1
</s>
<s>
Komňa	Komňa	k6eAd1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Starý	starý	k2eAgInSc4d1
Hrozenkov	Hrozenkov	k1gInSc4
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
</s>
<s>
Korytná	Korytný	k2eAgFnSc1d1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Nivnice	Nivnice	k1gFnPc5
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Nezdenice	Nezdenice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Bojkovice	Bojkovice	k1gFnPc4
</s>
<s>
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
</s>
<s>
Nivnice	Nivnice	k1gFnPc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Zdeněk	Zdeněk	k1gMnSc1
Gerhard	Gerhard	k1gMnSc1
Klimeš	Klimeš	k1gMnSc1
</s>
<s>
Andělů	Anděl	k1gMnPc2
Strážných	strážný	k1gMnPc2
</s>
<s>
Pitín	Pitín	k1gMnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Zdeněk	Zdeněk	k1gMnSc1
Graas	Graas	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Stanislava	Stanislav	k1gMnSc4
</s>
<s>
Prakšice	Prakšice	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Kapuš	Kapuš	k1gMnSc1
</s>
<s>
Krista	Kristus	k1gMnSc2
Krále	Král	k1gMnSc2
</s>
<s>
Rudice	Rudice	k1gFnPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Šumice	Šumice	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Starý	starý	k2eAgInSc1d1
Hrozenkov	Hrozenkov	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Bohumil	Bohumil	k1gMnSc1
Svitok	Svitok	k1gInSc4
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Strání	stráň	k1gFnSc7
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Stanislav	Stanislav	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
</s>
<s>
Šumice	Šumice	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Lisowski	Lisowsk	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Svatopluk	Svatopluk	k1gMnSc1
Pavlica	Pavlica	k1gMnSc1
</s>
<s>
Neposkvrněného	poskvrněný	k2eNgNnSc2d1
početí	početí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1
Brod-Újezdec	Brod-Újezdec	k1gMnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Rek	rek	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Vlčnov	Vlčnov	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Hubert	Hubert	k1gMnSc1
Wójczik	Wójczik	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KREJSA	Krejsa	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
olomoucké	olomoucký	k2eAgNnSc1d1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Děkanát	děkanát	k1gInSc4
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
492	#num#	k4
<g/>
-	-	kIx~
<g/>
509	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
Web	web	k1gInSc1
mládeže	mládež	k1gFnSc2
děkanátu	děkanát	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Děkanáty	děkanát	k1gInPc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
</s>
<s>
Holešov	Holešov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Konice	Konice	k1gFnSc2
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Šternberk	Šternberk	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
</s>
<s>
Bánov	Bánov	k1gInSc1
•	•	k?
Bojkovice	Bojkovice	k1gFnPc4
•	•	k?
Březová	březový	k2eAgFnSc1d1
u	u	k7c2
Uherského	uherský	k2eAgInSc2d1
Brodu	Brod	k1gInSc2
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Němčí	němčit	k5eAaImIp3nS
•	•	k?
Horní	horní	k2eAgFnSc1d1
Němčí	němčit	k5eAaImIp3nS
•	•	k?
Hradčovice	Hradčovice	k1gFnSc1
•	•	k?
Komňa	Komň	k1gInSc2
•	•	k?
Korytná	Korytný	k2eAgFnSc1d1
•	•	k?
Nezdenice	Nezdenice	k1gFnSc1
•	•	k?
Nivnice	Nivnice	k1gFnPc4
•	•	k?
Pitín	Pitín	k1gMnSc1
•	•	k?
Prakšice	Prakšice	k1gFnSc2
•	•	k?
Rudice	Rudice	k1gFnPc4
•	•	k?
Starý	starý	k2eAgInSc1d1
Hrozenkov	Hrozenkov	k1gInSc1
•	•	k?
Strání	stráň	k1gFnPc2
•	•	k?
Šumice	Šumic	k1gMnSc2
•	•	k?
Uherský	uherský	k2eAgInSc4d1
Brod	Brod	k1gInSc4
•	•	k?
Uherský	uherský	k2eAgMnSc1d1
Brod-Újezdec	Brod-Újezdec	k1gMnSc1
•	•	k?
Vlčnov	Vlčnov	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
