<s>
Santarém	Santarý	k2eAgInSc6d1
(	(	kIx(
<g/>
Portugalsko	Portugalsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Santarém	Santarý	k2eAgInSc6d1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Santarém	Santarý	k2eAgInSc6d1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
562	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
62	#num#	k4
200	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
110,7	110,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
prezident	prezident	k1gMnSc1
</s>
<s>
Ricardo	Ricardo	k1gNnSc1
Gonçalves	Gonçalvesa	k1gFnPc2
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.cm-santarem.pt	www.cm-santarem.pt	k1gMnSc1
PSČ	PSČ	kA
</s>
<s>
2000	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Santarém	Santarý	k2eAgInSc6d1
je	on	k3xPp3gInPc4
město	město	k1gNnSc4
na	na	k7c6
středozápadě	středozápad	k1gInSc6
Portugalska	Portugalsko	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
centrem	centrum	k1gNnSc7
stejnojmenného	stejnojmenný	k2eAgInSc2d1
distriktu	distrikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
asi	asi	k9
60	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Santarém	Santarém	k1gInSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
významné	významný	k2eAgNnSc1d1
dějiště	dějiště	k1gNnSc1
býčích	býčí	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
v	v	k7c6
červnu	červen	k1gInSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
koná	konat	k5eAaImIp3nS
Ribatejský	Ribatejský	k2eAgInSc1d1
veletrh	veletrh	k1gInSc1
–	–	k?
největší	veliký	k2eAgInSc1d3
zemědělský	zemědělský	k2eAgInSc1d1
veletrh	veletrh	k1gInSc1
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
se	se	k3xPyFc4
zde	zde	k6eAd1
koná	konat	k5eAaImIp3nS
největšímu	veliký	k2eAgMnSc3d3
gastronomický	gastronomický	k2eAgInSc4d1
festival	festival	k1gInSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Pedro	Pedro	k6eAd1
Álvares	Álvares	k1gMnSc1
Cabral	Cabral	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1467	#num#	k4
–	–	k?
asi	asi	k9
1520	#num#	k4
<g/>
)	)	kIx)
–	–	k?
portugalský	portugalský	k2eAgMnSc1d1
mořeplavec	mořeplavec	k1gMnSc1
</s>
<s>
Bernardo	Bernardo	k1gNnSc1
de	de	k?
Sá	Sá	k1gMnSc2
Nogueira	Nogueir	k1gMnSc2
de	de	k?
Figueiredo	Figueiredo	k1gNnSc1
(	(	kIx(
<g/>
1795	#num#	k4
<g/>
–	–	k?
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
–	–	k?
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Portugalska	Portugalsko	k1gNnSc2
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
kostel	kostel	k1gInSc1
Igreja	Igrejus	k1gMnSc2
da	da	k?
Graça	Graçus	k1gMnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
barokní	barokní	k2eAgFnSc7d1
rozetou	rozeta	k1gFnSc7
z	z	k7c2
jednoho	jeden	k4xCgInSc2
kamene	kámen	k1gInSc2
</s>
<s>
jezuitský	jezuitský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
Igreja	Igrejum	k1gNnSc2
do	do	k7c2
Seminário	Seminário	k6eAd1
</s>
<s>
most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Tejo	Tejo	k6eAd1
</s>
<s>
tržiště	tržiště	k1gNnSc1
s	s	k7c7
výzdobou	výzdoba	k1gFnSc7
z	z	k7c2
azulejos	azulejosa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Santarém	Santarý	k2eAgNnSc6d1
<g/>
,	,	kIx,
město	město	k1gNnSc4
s	s	k7c7
bohatou	bohatý	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světadíly	světadíl	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Santarém	Santarý	k2eAgInSc6d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
882045	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4118413-0	4118413-0	k4
</s>
