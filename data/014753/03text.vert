<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
ve	v	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
zrušen	zrušen	k2eAgInSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
delikty	delikt	k1gInPc4
</s>
<s>
zrušen	zrušen	k2eAgMnSc1d1
pro	pro	k7c4
zločiny	zločin	k1gInPc4
nespáchané	spáchaný	k2eNgNnSc1d1
za	za	k7c2
speciálních	speciální	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
(	(	kIx(
<g/>
válečný	válečný	k2eAgInSc1d1
stav	stav	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
existuje	existovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
dlouhodobě	dlouhodobě	k6eAd1
není	být	k5eNaImIp3nS
praktikován	praktikován	k2eAgInSc1d1
</s>
<s>
uplatňován	uplatňován	k2eAgInSc1d1
za	za	k7c4
některé	některý	k3yIgInPc4
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
</s>
<s>
Poprava	poprava	k1gFnSc1
Lady	lady	k1gFnSc1
Jane	Jan	k1gMnSc5
Grey	Greum	k1gNnPc7
<g/>
,	,	kIx,
obraz	obraz	k1gInSc4
Paula	Paul	k1gMnSc2
Delaroche	Delaroch	k1gMnSc2
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Battista	Battista	k1gMnSc1
Bugatti	Bugatť	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
působil	působit	k5eAaImAgMnS
jako	jako	k9
kat	kat	k1gMnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
si	se	k3xPyFc3
s	s	k7c7
vězni	vězeň	k1gMnPc7
často	často	k6eAd1
povídal	povídat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
uklidnil	uklidnit	k5eAaPmAgMnS
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
neboli	neboli	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
trest	trest	k1gInSc4
<g/>
,	,	kIx,
či	či	k8xC
také	také	k9
absolutní	absolutní	k2eAgInSc4d1
trest	trest	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
trest	trest	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
předpokládá	předpokládat	k5eAaImIp3nS
usmrcení	usmrcení	k1gNnSc4
(	(	kIx(
<g/>
neboli	neboli	k8xC
popravu	poprava	k1gFnSc4
<g/>
)	)	kIx)
člověka	člověk	k1gMnSc2
odsouzeného	odsouzený	k2eAgMnSc2d1
za	za	k7c4
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
dle	dle	k7c2
platného	platný	k2eAgNnSc2d1
trestního	trestní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
možné	možný	k2eAgNnSc1d1
tento	tento	k3xDgInSc4
trest	trest	k1gInSc4
uložit	uložit	k5eAaPmF
(	(	kIx(
<g/>
v	v	k7c6
dřívějších	dřívější	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
byly	být	k5eAaImAgFnP
takovéto	takovýto	k3xDgFnPc4
trestné	trestný	k2eAgFnPc4d1
činy	čina	k1gFnPc4
nazývány	nazýván	k2eAgFnPc4d1
hrdelními	hrdelní	k2eAgInPc7d1
zločiny	zločin	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
definitivnosti	definitivnost	k1gFnSc3
(	(	kIx(
<g/>
nemožnosti	nemožnost	k1gFnPc1
jakékoli	jakýkoli	k3yIgFnSc2
nápravy	náprava	k1gFnSc2
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
vykonání	vykonání	k1gNnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
trest	trest	k1gInSc4
velmi	velmi	k6eAd1
kontroverzní	kontroverzní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
zastánci	zastánce	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
spravedlivý	spravedlivý	k2eAgInSc4d1
trest	trest	k1gInSc4
za	za	k7c4
vraždu	vražda	k1gFnSc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
závažné	závažný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
<g/>
,	,	kIx,
znemožňuje	znemožňovat	k5eAaImIp3nS
recidivu	recidiva	k1gFnSc4
a	a	k8xC
má	mít	k5eAaImIp3nS
významné	významný	k2eAgInPc4d1
odstrašující	odstrašující	k2eAgInPc4d1
(	(	kIx(
<g/>
preventivní	preventivní	k2eAgInPc4d1
<g/>
)	)	kIx)
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
upozorňují	upozorňovat	k5eAaImIp3nP
<g/>
,	,	kIx,
kromě	kromě	k7c2
morálních	morální	k2eAgInPc2d1
argumentů	argument	k1gInPc2
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
právem	právo	k1gNnSc7
každého	každý	k3xTgMnSc4
na	na	k7c4
život	život	k1gInSc4
<g/>
,	,	kIx,
především	především	k9
na	na	k7c4
nebezpečí	nebezpečí	k1gNnSc4
justičního	justiční	k2eAgInSc2d1
omylu	omyl	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
nenapravitelný	napravitelný	k2eNgInSc1d1
<g/>
,	,	kIx,
zneužití	zneužití	k1gNnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
k	k	k7c3
justiční	justiční	k2eAgFnSc3d1
vraždě	vražda	k1gFnSc3
<g/>
,	,	kIx,
zpochybňují	zpochybňovat	k5eAaImIp3nP
odstrašující	odstrašující	k2eAgInSc4d1
účinek	účinek	k1gInSc4
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
a	a	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
poprava	poprava	k1gFnSc1
je	být	k5eAaImIp3nS
dražší	drahý	k2eAgFnSc1d2
než	než	k8xS
doživotní	doživotní	k2eAgNnPc1d1
vězení	vězení	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
postupně	postupně	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
ve	v	k7c6
většině	většina	k1gFnSc6
evropských	evropský	k2eAgInPc2d1
a	a	k8xC
jihoamerických	jihoamerický	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ho	on	k3xPp3gMnSc4
i	i	k9
nadále	nadále	k6eAd1
praktikují	praktikovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
mj.	mj.	kA
některé	některý	k3yIgInPc1
ze	z	k7c2
států	stát	k1gInPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
a	a	k8xC
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
praktikuje	praktikovat	k5eAaImIp3nS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
pouze	pouze	k6eAd1
Bělorusko	Bělorusko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Cesare	Cesar	k1gMnSc5
Beccaria	Beccarium	k1gNnPc4
<g/>
,	,	kIx,
Dei	Dei	k1gFnPc4
delitti	delitť	k1gFnSc2
e	e	k0
delle	delle	k1gNnSc7
pene	pene	k1gNnSc3
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
nejvyšším	vysoký	k2eAgInSc7d3
trestem	trest	k1gInSc7
po	po	k7c6
tisíciletí	tisíciletí	k1gNnSc6
<g/>
,	,	kIx,
užívaný	užívaný	k2eAgInSc1d1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
podložený	podložený	k2eAgInSc1d1
i	i	k9
dobovými	dobový	k2eAgInPc7d1
kodexy	kodex	k1gInPc7
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
římské	římský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
vždy	vždy	k6eAd1
byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
způsobem	způsob	k1gInSc7
a	a	k8xC
za	za	k7c2
okolností	okolnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
bychom	by	kYmCp1nP
dnes	dnes	k6eAd1
mohli	moct	k5eAaImAgMnP
označit	označit	k5eAaPmF
za	za	k7c4
eticky	eticky	k6eAd1
pochopitelné	pochopitelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
ukřižování	ukřižování	k1gNnSc1
několika	několik	k4yIc2
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
po	po	k7c6
porážce	porážka	k1gFnSc6
Spartakova	Spartakův	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
krátkodobě	krátkodobě	k6eAd1
rušen	rušen	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
znovu	znovu	k6eAd1
zaváděn	zaváděn	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
širším	široký	k2eAgNnSc6d2
měřítku	měřítko	k1gNnSc6
se	se	k3xPyFc4
myšlenka	myšlenka	k1gFnSc1
úplně	úplně	k6eAd1
zrušit	zrušit	k5eAaPmF
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
začala	začít	k5eAaPmAgFnS
prosazovat	prosazovat	k5eAaImF
až	až	k9
koncem	koncem	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
filozof	filozof	k1gMnSc1
Voltaire	Voltair	k1gInSc5
<g/>
,	,	kIx,
právní	právní	k2eAgMnSc1d1
reformátor	reformátor	k1gMnSc1
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
a	a	k8xC
zejména	zejména	k9
právník	právník	k1gMnSc1
Cesare	Cesar	k1gMnSc5
Beccaria	Beccarium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1764	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
převratné	převratný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Dei	Dei	k1gFnSc2
Delitti	Delitť	k1gFnSc2
e	e	k0
Delle	Delle	k1gInSc4
Pene	Pene	k1gNnPc7
(	(	kIx(
<g/>
O	o	k7c6
zločinech	zločin	k1gInPc6
a	a	k8xC
trestech	trest	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
státem	stát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zrušil	zrušit	k5eAaPmAgInS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Toskánsko	Toskánsko	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1786	#num#	k4
za	za	k7c4
panování	panování	k1gNnSc4
pozdějšího	pozdní	k2eAgMnSc2d2
habsburského	habsburský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
krátkodobě	krátkodobě	k6eAd1
de	de	k?
facto	facto	k1gNnSc4
zrušen	zrušit	k5eAaPmNgMnS
za	za	k7c2
panování	panování	k1gNnSc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
ohlasy	ohlas	k1gInPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
až	až	k9
o	o	k7c4
50	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
na	na	k7c6
americkém	americký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1846	#num#	k4
zrušil	zrušit	k5eAaPmAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
americký	americký	k2eAgInSc1d1
stát	stát	k1gInSc1
Michigan	Michigan	k1gInSc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1853	#num#	k4
Venezuela	Venezuela	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
roku	rok	k1gInSc2
1867	#num#	k4
Portugalsko	Portugalsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Strangulace	strangulace	k1gFnSc1
uvedena	uveden	k2eAgFnSc1d1
v	v	k7c6
záznamech	záznam	k1gInPc6
o	o	k7c6
popraveném	popravený	k2eAgMnSc6d1
Václavu	Václav	k1gMnSc6
Švédovi	Švéd	k1gMnSc6
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
Česku	Česko	k1gNnSc6
zrušen	zrušit	k5eAaPmNgInS
v	v	k7c6
rámci	rámec	k1gInSc6
josefinských	josefinský	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1795	#num#	k4
byl	být	k5eAaImAgMnS
pro	pro	k7c4
vybrané	vybraný	k2eAgInPc4d1
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
obnoven	obnoven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Československu	Československo	k1gNnSc6
bylo	být	k5eAaImAgNnS
po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
převzato	převzít	k5eAaPmNgNnS
trestní	trestní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
z	z	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zachován	zachován	k2eAgInSc4d1
a	a	k8xC
uplatňován	uplatňován	k2eAgInSc4d1
za	za	k7c4
poměrně	poměrně	k6eAd1
malý	malý	k2eAgInSc4d1
počet	počet	k1gInSc4
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
(	(	kIx(
<g/>
Zatímní	zatímní	k2eAgInSc1d1
návrh	návrh	k1gInSc1
obecné	obecný	k2eAgFnSc2d1
části	část	k1gFnSc2
trestního	trestní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
možné	možný	k2eAgNnSc1d1
uložit	uložit	k5eAaPmF
pouze	pouze	k6eAd1
při	při	k7c6
stanném	stanný	k2eAgNnSc6d1
právu	právo	k1gNnSc6
nebo	nebo	k8xC
u	u	k7c2
doživotně	doživotně	k6eAd1
odsouzeného	odsouzený	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
opětovně	opětovně	k6eAd1
spáchal	spáchat	k5eAaPmAgInS
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
nějž	jenž	k3xRgMnSc4
byl	být	k5eAaImAgInS
odsouzen	odsouzen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1918	#num#	k4
až	až	k9
1933	#num#	k4
bylo	být	k5eAaImAgNnS
popraveno	popravit	k5eAaPmNgNnS
devět	devět	k4xCc1
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gMnSc2
Masaryk	Masaryk	k1gMnSc1
udělil	udělit	k5eAaPmAgMnS
naprosté	naprostý	k2eAgFnSc3d1
většině	většina	k1gFnSc3
odsouzenců	odsouzenec	k1gMnPc2
milost	milost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
(	(	kIx(
<g/>
vydání	vydání	k1gNnSc6
zákona	zákon	k1gInSc2
č.	č.	k?
91	#num#	k4
<g/>
/	/	kIx~
<g/>
1934	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zachován	zachovat	k5eAaPmNgInS
<g/>
,	,	kIx,
využívaly	využívat	k5eAaPmAgFnP,k5eAaImAgFnP
se	se	k3xPyFc4
ale	ale	k9
spíše	spíše	k9
jiné	jiný	k2eAgInPc4d1
tresty	trest	k1gInPc4
<g/>
:	:	kIx,
těžší	těžký	k2eAgInPc4d2
tresty	trest	k1gInPc4
žaláře	žalář	k1gInSc2
a	a	k8xC
doživotí	doživotí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
let	léto	k1gNnPc2
1939	#num#	k4
až	až	k8xS
1945	#num#	k4
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
často	často	k6eAd1
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
(	(	kIx(
<g/>
jen	jen	k9
na	na	k7c6
pankrácké	pankrácký	k2eAgFnSc6d1
gilotině	gilotina	k1gFnSc6
přes	přes	k7c4
1000	#num#	k4
popravených	popravený	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
1946	#num#	k4
až	až	k8xS
1949	#num#	k4
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
udělován	udělovat	k5eAaImNgInS
hojně	hojně	k6eAd1
<g/>
,	,	kIx,
především	především	k9
podle	podle	k7c2
tzv.	tzv.	kA
retribučních	retribuční	k2eAgInPc2d1
dekretů	dekret	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zneužila	zneužít	k5eAaPmAgFnS
komunistická	komunistický	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
k	k	k7c3
celé	celý	k2eAgFnSc3d1
řadě	řada	k1gFnSc3
justičních	justiční	k2eAgFnPc2d1
vražd	vražda	k1gFnPc2
<g/>
,	,	kIx,
známé	známý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
případy	případ	k1gInPc1
Milady	Milada	k1gFnSc2
Horákové	Horáková	k1gFnSc2
a	a	k8xC
Rudolfa	Rudolf	k1gMnSc2
Slánského	Slánský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
bylo	být	k5eAaImAgNnS
provedení	provedení	k1gNnSc1
popravy	poprava	k1gFnSc2
omezeno	omezen	k2eAgNnSc1d1
jen	jen	k9
na	na	k7c6
oběšení	oběšení	k1gNnSc6
(	(	kIx(
<g/>
při	při	k7c6
výjimečném	výjimečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
zákon	zákon	k1gInSc1
připouštěl	připouštět	k5eAaImAgInS
zastřelení	zastřelení	k1gNnSc4
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
možnost	možnost	k1gFnSc1
ale	ale	k9
v	v	k7c6
praxi	praxe	k1gFnSc6
nebyla	být	k5eNaImAgFnS
nikdy	nikdy	k6eAd1
použita	použít	k5eAaPmNgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
poprava	poprava	k1gFnSc1
oběšením	oběšení	k1gNnPc3
vykonávána	vykonáván	k2eAgFnSc1d1
na	na	k7c6
tzv.	tzv.	kA
popravčím	popravčí	k2eAgNnSc6d1
prkně	prkno	k1gNnSc6
poměrně	poměrně	k6eAd1
bolestivou	bolestivý	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
zděděnou	zděděný	k2eAgFnSc4d1
ještě	ještě	k9
z	z	k7c2
dob	doba	k1gFnPc2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odsouzenec	odsouzenec	k1gMnSc1
byl	být	k5eAaImAgMnS
přiveden	přivést	k5eAaPmNgMnS
k	k	k7c3
vysokému	vysoký	k2eAgNnSc3d1
prknu	prkno	k1gNnSc3
<g/>
,	,	kIx,
na	na	k7c4
krk	krk	k1gInSc4
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
nasazena	nasazen	k2eAgFnSc1d1
oprátka	oprátka	k1gFnSc1
a	a	k8xC
v	v	k7c6
podpaží	podpaží	k1gNnSc6
byl	být	k5eAaImAgInS
připoután	připoutat	k5eAaPmNgInS
řemenem	řemen	k1gInSc7
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
ho	on	k3xPp3gInSc4
kat	kat	k1gMnSc1
vytáhl	vytáhnout	k5eAaPmAgMnS
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
řemen	řemen	k1gInSc4
uvolnil	uvolnit	k5eAaPmAgMnS
a	a	k8xC
odsouzenec	odsouzenec	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
viset	viset	k5eAaImF
pouze	pouze	k6eAd1
na	na	k7c6
oprátce	oprátka	k1gFnSc6
a	a	k8xC
pomalu	pomalu	k6eAd1
se	se	k3xPyFc4
uškrtil	uškrtit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
pomocníci	pomocník	k1gMnPc1
kata	kata	k9
pomocí	pomocí	k7c2
druhého	druhý	k4xOgInSc2
provazu	provaz	k1gInSc2
tahali	tahat	k5eAaImAgMnP
za	za	k7c4
odsouzencovy	odsouzencův	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
urychlili	urychlit	k5eAaPmAgMnP
strangulaci	strangulace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
používána	používán	k2eAgFnSc1d1
o	o	k7c4
něco	něco	k3yInSc4
humánnější	humánní	k2eAgFnSc1d2
tzv.	tzv.	kA
metoda	metoda	k1gFnSc1
short	short	k1gInSc1
drop	drop	k1gMnSc1
(	(	kIx(
<g/>
krátkého	krátký	k2eAgInSc2d1
pádu	pád	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
byla	být	k5eAaImAgFnS
oprátka	oprátka	k1gFnSc1
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
hák	hák	k1gInSc4
visící	visící	k2eAgInSc4d1
ve	v	k7c6
zdi	zeď	k1gFnSc6
<g/>
,	,	kIx,
pod	pod	k7c7
nímž	jenž	k3xRgNnSc7
bylo	být	k5eAaImAgNnS
umístěno	umístit	k5eAaPmNgNnS
propadlo	propadlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katovi	katův	k2eAgMnPc1d1
pomocníci	pomocník	k1gMnPc1
vložili	vložit	k5eAaPmAgMnP
odsouzenci	odsouzenec	k1gMnPc1
oprátku	oprátka	k1gFnSc4
na	na	k7c4
krk	krk	k1gInSc4
a	a	k8xC
kat	kat	k1gMnSc1
propadlo	propadlo	k1gNnSc4
uvolnil	uvolnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propadliště	propadliště	k1gNnSc1
bylo	být	k5eAaImAgNnS
asi	asi	k9
osmdesát	osmdesát	k4xCc1
centimetrů	centimetr	k1gInPc2
hluboké	hluboký	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
metodě	metoda	k1gFnSc6
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
okamžité	okamžitý	k2eAgFnSc3d1
smrti	smrt	k1gFnSc3
zlomením	zlomení	k1gNnSc7
vazu	vaz	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
několikaminutovému	několikaminutový	k2eAgNnSc3d1
škrcení	škrcení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metoda	metoda	k1gFnSc1
long	longa	k1gFnPc2
drop	drop	k1gMnSc1
<g/>
,	,	kIx,
obvyklá	obvyklý	k2eAgFnSc1d1
v	v	k7c6
západních	západní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zaručovala	zaručovat	k5eAaImAgFnS
okamžitou	okamžitý	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
(	(	kIx(
<g/>
propadliště	propadliště	k1gNnSc2
je	být	k5eAaImIp3nS
při	při	k7c6
této	tento	k3xDgFnSc6
metodě	metoda	k1gFnSc6
2-3	2-3	k4
metry	metr	k1gInPc1
hluboké	hluboký	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
u	u	k7c2
nás	my	k3xPp1nPc2
nikdy	nikdy	k6eAd1
nepoužívala	používat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popravčí	popravčí	k1gMnPc1
byli	být	k5eAaImAgMnP
vybírání	vybírání	k1gNnSc4
z	z	k7c2
vězeňských	vězeňský	k2eAgMnPc2d1
dozorců	dozorce	k1gMnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
jména	jméno	k1gNnPc1
jsou	být	k5eAaImIp3nP
tajná	tajný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
nemohl	moct	k5eNaImAgInS
být	být	k5eAaImF
uložen	uložen	k2eAgInSc1d1
těhotné	těhotný	k2eAgFnSc3d1
ženě	žena	k1gFnSc3
a	a	k8xC
člověku	člověk	k1gMnSc3
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
v	v	k7c6
době	doba	k1gFnSc6
spáchání	spáchání	k1gNnSc2
zločinu	zločin	k1gInSc2
nebylo	být	k5eNaImAgNnS
18	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
ukládán	ukládat	k5eAaImNgInS
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nebyla	být	k5eNaImAgFnS
naděje	naděje	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
uvěznění	uvěznění	k1gNnSc4
do	do	k7c2
15	#num#	k4
let	léto	k1gNnPc2
přineslo	přinést	k5eAaPmAgNnS
nápravu	náprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
také	také	k9
ukládán	ukládat	k5eAaImNgInS
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
ochránit	ochránit	k5eAaPmF
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprava	poprava	k1gFnSc1
mohla	moct	k5eAaImAgFnS
proběhnout	proběhnout	k5eAaPmF
jen	jen	k9
po	po	k7c6
prošetření	prošetření	k1gNnSc6
případu	případ	k1gInSc2
Nejvyšším	vysoký	k2eAgInSc7d3
soudem	soud	k1gInSc7
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
schválení	schválení	k1gNnSc6
rozsudku	rozsudek	k1gInSc2
a	a	k8xC
po	po	k7c6
zamítnutí	zamítnutí	k1gNnSc6
všech	všecek	k3xTgFnPc2
žádostí	žádost	k1gFnPc2
o	o	k7c4
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
popravy	poprava	k1gFnSc2
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
přítomen	přítomen	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
trestního	trestní	k2eAgInSc2d1
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
prokurátor	prokurátor	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
věznice	věznice	k1gFnSc2
a	a	k8xC
lékař	lékař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
podepsalo	podepsat	k5eAaPmAgNnS
350	#num#	k4
lidí	člověk	k1gMnPc2
petici	petice	k1gFnSc4
(	(	kIx(
<g/>
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
Charty	charta	k1gFnSc2
77	#num#	k4
<g/>
)	)	kIx)
žádající	žádající	k2eAgNnSc1d1
zrušení	zrušení	k1gNnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejtemnějších	temný	k2eAgFnPc2d3
skvrn	skvrna	k1gFnPc2
na	na	k7c6
tváři	tvář	k1gFnSc6
našeho	náš	k3xOp1gInSc2
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petice	petice	k1gFnSc1
byla	být	k5eAaImAgFnS
zaslána	zaslat	k5eAaPmNgFnS
Federálnímu	federální	k2eAgNnSc3d1
shromáždění	shromáždění	k1gNnSc3
a	a	k8xC
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
mnohými	mnohý	k2eAgFnPc7d1
známými	známý	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
(	(	kIx(
<g/>
např.	např.	kA
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kyncl	Kyncl	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Seifert	Seifert	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
č.	č.	k?
175	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byl	být	k5eAaImAgInS
také	také	k9
výslovně	výslovně	k6eAd1
zakázán	zakázat	k5eAaPmNgInS
ústavně	ústavně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
čl	čl	kA
<g/>
.	.	kIx.
6	#num#	k4
Listiny	listina	k1gFnSc2
základních	základní	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
svobod	svoboda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
poprava	poprava	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
byla	být	k5eAaImAgFnS
vykonána	vykonán	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1989	#num#	k4
a	a	k8xC
na	na	k7c4
území	území	k1gNnSc4
Slovenska	Slovensko	k1gNnSc2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dobu	doba	k1gFnSc4
trvání	trvání	k1gNnSc2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
bez	bez	k7c2
období	období	k1gNnSc2
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
odsouzeno	odsouzet	k5eAaImNgNnS,k5eAaPmNgNnS
1	#num#	k4
217	#num#	k4
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
61	#num#	k4
%	%	kIx~
za	za	k7c4
retribuční	retribuční	k2eAgInPc4d1
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
<g/>
,	,	kIx,
21	#num#	k4
%	%	kIx~
za	za	k7c4
politické	politický	k2eAgInPc4d1
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
a	a	k8xC
18	#num#	k4
%	%	kIx~
za	za	k7c4
kriminální	kriminální	k2eAgInPc4d1
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
ústavněprávního	ústavněprávní	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
se	se	k3xPyFc4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
zavázala	zavázat	k5eAaPmAgFnS
k	k	k7c3
nepoužívání	nepoužívání	k1gNnSc3
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
ratifikací	ratifikace	k1gFnPc2
Protokolu	protokol	k1gInSc2
č.	č.	k?
6	#num#	k4
k	k	k7c3
Úmluvě	úmluva	k1gFnSc3
o	o	k7c6
ochraně	ochrana	k1gFnSc6
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
základních	základní	k2eAgFnPc2d1
svobod	svoboda	k1gFnPc2
Rady	rada	k1gFnSc2
Evropy	Evropa	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Někteří	některý	k3yIgMnPc1
sociologové	sociolog	k1gMnPc1
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
souvislost	souvislost	k1gFnSc4
mezi	mezi	k7c7
pojetím	pojetí	k1gNnSc7
obecného	obecný	k2eAgInSc2d1
významu	význam	k1gInSc2
trestu	trest	k1gInSc2
a	a	k8xC
aplikací	aplikace	k1gFnPc2
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
evropská	evropský	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
vnímá	vnímat	k5eAaImIp3nS
trest	trest	k1gInSc4
jako	jako	k8xS,k8xC
výchovný	výchovný	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
směřující	směřující	k2eAgInSc4d1
k	k	k7c3
napravení	napravení	k1gNnSc3
odsouzeného	odsouzený	k1gMnSc2
<g/>
,	,	kIx,
americké	americký	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
práva	právo	k1gNnSc2
dává	dávat	k5eAaImIp3nS
trestu	trest	k1gInSc3
význam	význam	k1gInSc1
odplaty	odplata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
v	v	k7c6
současnosti	současnost	k1gFnSc6
velmi	velmi	k6eAd1
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
zrušení	zrušení	k1gNnSc1
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
trestu	trest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
statistik	statistika	k1gFnPc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
88	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nemají	mít	k5eNaImIp3nP
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
trestních	trestní	k2eAgInPc6d1
řádech	řád	k1gInPc6
a	a	k8xC
popravy	poprava	k1gFnPc1
nevykonávají	vykonávat	k5eNaImIp3nP
<g/>
,	,	kIx,
11	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
jen	jen	k9
ve	v	k7c6
výjimečných	výjimečný	k2eAgInPc6d1
stavech	stav	k1gInPc6
<g/>
,	,	kIx,
29	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
v	v	k7c6
trestním	trestní	k2eAgInSc6d1
řádu	řád	k1gInSc6
mají	mít	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
10	#num#	k4
let	léto	k1gNnPc2
neprovedly	provést	k5eNaPmAgFnP
žádnou	žádný	k3yNgFnSc4
popravu	poprava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
69	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
států	stát	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
trestní	trestní	k2eAgInSc1d1
řád	řád	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
uděluje	udělovat	k5eAaImIp3nS
tento	tento	k3xDgInSc4
trest	trest	k1gInSc4
pouze	pouze	k6eAd1
za	za	k7c2
vraždy	vražda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
AI	AI	kA
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
ve	v	k7c6
22	#num#	k4
zemích	zem	k1gFnPc6
popraveno	popravit	k5eAaPmNgNnS
2	#num#	k4
148	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
popravili	popravit	k5eAaPmAgMnP
popravčí	popravčí	k1gMnPc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
a	a	k8xC
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pětinový	pětinový	k2eAgInSc4d1
pokles	pokles	k1gInSc4
počtu	počet	k1gInSc2
poprav	poprava	k1gFnPc2
vzhledem	vzhledem	k7c3
k	k	k7c3
roku	rok	k1gInSc3
2004	#num#	k4
(	(	kIx(
<g/>
nicméně	nicméně	k8xC
ne	ne	k9
všechny	všechen	k3xTgFnPc1
statistiky	statistika	k1gFnPc1
států	stát	k1gInPc2
jsou	být	k5eAaImIp3nP
kompletní	kompletní	k2eAgInPc1d1
<g/>
,	,	kIx,
v	v	k7c6
Číně	Čína	k1gFnSc6
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
popravováno	popravovat	k5eAaImNgNnS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
bývá	bývat	k5eAaImIp3nS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zneužíván	zneužíván	k2eAgInSc1d1
totalitní	totalitní	k2eAgInSc1d1
mocí	moc	k1gFnSc7
(	(	kIx(
<g/>
např.	např.	kA
Čína	Čína	k1gFnSc1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
právního	právní	k2eAgInSc2d1
systému	systém	k1gInSc2
vyškrtly	vyškrtnout	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
země	zem	k1gFnPc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
a	a	k8xC
Libérie	Libérie	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
Filipíny	Filipíny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
používání	používání	k1gNnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
různé	různý	k2eAgInPc1d1
stát	stát	k5eAaImF,k5eAaPmF
od	od	k7c2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
vykonává	vykonávat	k5eAaImIp3nS
pouze	pouze	k6eAd1
za	za	k7c4
vraždu	vražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
v	v	k7c6
devatenácti	devatenáct	k4xCc2
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgInS
federálně	federálně	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
1972	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
rozsudku	rozsudek	k1gInSc2
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
Furman	furman	k1gMnSc1
v.	v.	k?
Georgie	Georgie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
byl	být	k5eAaImAgInS
ale	ale	k8xC
obnoven	obnovit	k5eAaPmNgInS
rozsudkem	rozsudek	k1gInSc7
Gregg	Gregg	k1gInSc4
v.	v.	k?
Georgie	Georgie	k1gFnSc2
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
byly	být	k5eAaImAgFnP
zpřísněny	zpřísněn	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
vykonávání	vykonávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
trestů	trest	k1gInPc2
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
vykonáváno	vykonávat	k5eAaImNgNnS
ve	v	k7c6
státě	stát	k1gInSc6
Texas	Texas	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2007	#num#	k4
v	v	k7c6
něm	on	k3xPp3gInSc6
byl	být	k5eAaImAgInS
popraven	popravit	k5eAaPmNgInS
400	#num#	k4
<g/>
.	.	kIx.
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
historii	historie	k1gFnSc6
USA	USA	kA
popraveno	popravit	k5eAaPmNgNnS
15	#num#	k4
269	#num#	k4
lidí	člověk	k1gMnPc2
do	do	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
probíhají	probíhat	k5eAaImIp3nP
popravy	poprava	k1gFnPc1
dokonce	dokonce	k9
i	i	k9
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
náměstích	náměstí	k1gNnPc6
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
stětím	stětit	k5eAaImIp1nS,k5eAaPmIp1nS
hlavy	hlava	k1gFnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
území	území	k1gNnSc6
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
teroristů	terorista	k1gMnPc2
z	z	k7c2
organizace	organizace	k1gFnSc2
Islámský	islámský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrně	průměrně	k6eAd1
každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
je	být	k5eAaImIp3nS
popraven	popraven	k2eAgMnSc1d1
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
jeden	jeden	k4xCgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Saúdskou	saúdský	k2eAgFnSc4d1
Arábii	Arábie	k1gFnSc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
země	zem	k1gFnPc4
s	s	k7c7
největším	veliký	k2eAgNnSc7d3
množstvím	množství	k1gNnSc7
poprav	poprava	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ukamenování	ukamenování	k1gNnSc6
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
používá	používat	k5eAaImIp3nS
např.	např.	kA
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
,	,	kIx,
Súdánu	Súdán	k1gInSc6
a	a	k8xC
Somálsku	Somálsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právní	právní	k2eAgInSc1d1
systém	systém	k1gInSc1
v	v	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
islámském	islámský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
šaría	šaríum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
z	z	k7c2
května	květen	k1gInSc2
2013	#num#	k4
se	s	k7c7
62	#num#	k4
%	%	kIx~
občanů	občan	k1gMnPc2
ČR	ČR	kA
vyslovilo	vyslovit	k5eAaPmAgNnS
pro	pro	k7c4
existenci	existence	k1gFnSc4
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časem	časem	k6eAd1
však	však	k9
podpora	podpora	k1gFnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
českou	český	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
klesá	klesat	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc3
podporovalo	podporovat	k5eAaImAgNnS
50	#num#	k4
%	%	kIx~
respondentů	respondent	k1gMnPc2
<g/>
,	,	kIx,
rozhodnou	rozhodný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
vyjádřilo	vyjádřit	k5eAaPmAgNnS
19	#num#	k4
%	%	kIx~
respondentů	respondent	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spory	spor	k1gInPc1
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
plynová	plynový	k2eAgFnSc1d1
komora	komora	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
současnosti	současnost	k1gFnSc6
k	k	k7c3
výkonu	výkon	k1gInSc3
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
USA	USA	kA
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
vyvolával	vyvolávat	k5eAaImAgInS
již	již	k6eAd1
od	od	k7c2
počátků	počátek	k1gInPc2
humanismu	humanismus	k1gInSc2
polemiky	polemika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
</s>
<s>
Obvyklé	obvyklý	k2eAgInPc1d1
argumenty	argument	k1gInPc1
zastánců	zastánce	k1gMnPc2
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
odstrašení	odstrašení	k1gNnSc6
potenciálních	potenciální	k2eAgMnPc2d1
pachatelů	pachatel	k1gMnPc2
</s>
<s>
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zabrání	zabránit	k5eAaPmIp3nS
opakování	opakování	k1gNnSc1
zločinu	zločin	k1gInSc2
<g/>
,	,	kIx,
recidivě	recidiva	k1gFnSc6
(	(	kIx(
<g/>
popravený	popravený	k2eAgMnSc1d1
zločinec	zločinec	k1gMnSc1
byl	být	k5eAaImAgMnS
zaručeně	zaručeně	k6eAd1
zneškodněn	zneškodněn	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
může	moct	k5eAaImIp3nS
podle	podle	k7c2
některých	některý	k3yIgMnPc2
eugeniků	eugenik	k1gMnPc2
snížit	snížit	k5eAaPmF
kriminalitu	kriminalita	k1gFnSc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
redukce	redukce	k1gFnSc2
genů	gen	k1gInPc2
vedoucích	vedoucí	k1gMnPc2
ke	k	k7c3
kriminálnímu	kriminální	k2eAgNnSc3d1
chování	chování	k1gNnSc3
</s>
<s>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
ekvivalentní	ekvivalentní	k2eAgFnSc4d1
odplatu	odplata	k1gFnSc4
(	(	kIx(
<g/>
smrt	smrt	k1gFnSc4
za	za	k7c4
smrt	smrt	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
ignorovat	ignorovat	k5eAaImF
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bývá	bývat	k5eAaImIp3nS
většinou	většinou	k6eAd1
nakloněno	nakloněn	k2eAgNnSc1d1
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
věznění	věznění	k1gNnSc1
zločinců	zločinec	k1gMnPc2
představuje	představovat	k5eAaImIp3nS
mnohonásobně	mnohonásobně	k6eAd1
vyšší	vysoký	k2eAgInPc4d2
finanční	finanční	k2eAgInPc4d1
náklady	náklad	k1gInPc4
státu	stát	k1gInSc2
než	než	k8xS
poprava	poprava	k1gFnSc1
</s>
<s>
zmenší	zmenšit	k5eAaPmIp3nS
se	se	k3xPyFc4
počet	počet	k1gInSc1
vězňů	vězeň	k1gMnPc2
-	-	kIx~
v	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
světové	světový	k2eAgFnPc1d1
věznice	věznice	k1gFnPc1
přeplněné	přeplněný	k2eAgFnPc1d1
</s>
<s>
praktičnost	praktičnost	k1gFnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
;	;	kIx,
popravený	popravený	k2eAgMnSc1d1
nebezpečný	bezpečný	k2eNgMnSc1d1
vrah	vrah	k1gMnSc1
už	už	k6eAd1
nepředstavuje	představovat	k5eNaImIp3nS
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
společnost	společnost	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
nebezpečný	bezpečný	k2eNgMnSc1d1
vrah	vrah	k1gMnSc1
ve	v	k7c6
věznici	věznice	k1gFnSc6
může	moct	k5eAaImIp3nS
jistou	jistý	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
stále	stále	k6eAd1
představovat	představovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
několika	několik	k4yIc7
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
může	moct	k5eAaImIp3nS
po	po	k7c6
propuštění	propuštění	k1gNnSc6
nebo	nebo	k8xC
útěku	útěk	k1gInSc6
svůj	svůj	k3xOyFgInSc4
čin	čin	k1gInSc4
opakovat	opakovat	k5eAaImF
</s>
<s>
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
hrozbou	hrozba	k1gFnSc7
i	i	k9
pro	pro	k7c4
ostrahu	ostraha	k1gFnSc4
věznice	věznice	k1gFnSc2
</s>
<s>
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
spojence	spojenec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
ho	on	k3xPp3gMnSc4
budou	být	k5eAaImBp3nP
chtít	chtít	k5eAaImF
osvobodit	osvobodit	k5eAaPmF
napadením	napadení	k1gNnSc7
věznice	věznice	k1gFnSc2
nebo	nebo	k8xC
výměnou	výměna	k1gFnSc7
za	za	k7c4
rukojmí	rukojmí	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
zajmou	zajmout	k5eAaPmIp3nP
</s>
<s>
Proti	proti	k7c3
</s>
<s>
Obvyklé	obvyklý	k2eAgInPc1d1
argumenty	argument	k1gInPc1
odpůrců	odpůrce	k1gMnPc2
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
nemorální	morální	k2eNgMnSc1d1
(	(	kIx(
<g/>
stát	stát	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
dopouští	dopouštět	k5eAaImIp3nS
podobného	podobný	k2eAgInSc2d1
deliktu	delikt	k1gInSc2
jako	jako	k8xC,k8xS
odsouzený	odsouzený	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
hrozba	hrozba	k1gFnSc1
justičního	justiční	k2eAgInSc2d1
omylu	omyl	k1gInSc2
(	(	kIx(
<g/>
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
nevratný	vratný	k2eNgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pouhý	pouhý	k2eAgInSc4d1
akt	akt	k1gInSc4
msty	msta	k1gFnSc2
(	(	kIx(
<g/>
dle	dle	k7c2
tohoto	tento	k3xDgInSc2
argumentu	argument	k1gInSc2
by	by	kYmCp3nS
odsouzený	odsouzený	k2eAgMnSc1d1
neměl	mít	k5eNaImAgMnS
být	být	k5eAaImF
fyzicky	fyzicky	k6eAd1
či	či	k8xC
psychicky	psychicky	k6eAd1
trestán	trestat	k5eAaImNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
být	být	k5eAaImF
oddělen	oddělit	k5eAaPmNgInS
od	od	k7c2
společnosti	společnost	k1gFnSc2
a	a	k8xC
případně	případně	k6eAd1
napraven	napraven	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
nemá	mít	k5eNaImIp3nS
odstrašující	odstrašující	k2eAgInSc4d1
účinek	účinek	k1gInSc4
a	a	k8xC
naopak	naopak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
riziko	riziko	k1gNnSc4
pro	pro	k7c4
policisty	policista	k1gMnPc4
zasahující	zasahující	k2eAgFnSc2d1
proti	proti	k7c3
pachateli	pachatel	k1gMnSc3
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ne	ne	k9
všechny	všechen	k3xTgFnPc1
vraždy	vražda	k1gFnPc1
jsou	být	k5eAaImIp3nP
plánované	plánovaný	k2eAgInPc4d1
</s>
<s>
pachatelé	pachatel	k1gMnPc1
nepočítají	počítat	k5eNaImIp3nP
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
dopadeni	dopadnout	k5eAaPmNgMnP
</s>
<s>
představa	představa	k1gFnSc1
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
člověka	člověk	k1gMnSc4
příliš	příliš	k6eAd1
abstraktní	abstraktní	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
proto	proto	k8xC
nižší	nízký	k2eAgInSc4d2
odstrašující	odstrašující	k2eAgInSc4d1
účinek	účinek	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
představa	představa	k1gFnSc1
několikaletého	několikaletý	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
ve	v	k7c6
vězení	vězení	k1gNnSc6
</s>
<s>
pachatelé	pachatel	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
hrozí	hrozit	k5eAaImIp3nS
dopadení	dopadení	k1gNnSc4
a	a	k8xC
následný	následný	k2eAgInSc4d1
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
sahají	sahat	k5eAaImIp3nP
k	k	k7c3
obraně	obrana	k1gFnSc3
(	(	kIx(
<g/>
protiútoku	protiútok	k1gInSc3
<g/>
)	)	kIx)
extrémními	extrémní	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
</s>
<s>
vysoká	vysoký	k2eAgFnSc1d1
finanční	finanční	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
institutu	institut	k1gInSc2
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
průměrná	průměrný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
věznění	věznění	k1gNnSc2
před	před	k7c7
popravou	poprava	k1gFnSc7
16	#num#	k4
až	až	k9
20	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
kvůli	kvůli	k7c3
zvýšené	zvýšený	k2eAgFnSc3d1
izolaci	izolace	k1gFnSc3
odsouzených	odsouzený	k2eAgFnPc2d1
to	ten	k3xDgNnSc1
představuje	představovat	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
vyšší	vysoký	k2eAgInPc4d2
náklady	náklad	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
doživotní	doživotní	k2eAgNnPc4d1
věznění	věznění	k1gNnPc4
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
věznici	věznice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
možná	možný	k2eAgFnSc1d1
diskriminace	diskriminace	k1gFnSc1
(	(	kIx(
<g/>
častější	častý	k2eAgNnSc1d2
uplatňování	uplatňování	k1gNnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
vůči	vůči	k7c3
náboženským	náboženský	k2eAgFnPc3d1
a	a	k8xC
etnickým	etnický	k2eAgFnPc3d1
minoritám	minorita	k1gFnPc3
a	a	k8xC
vůči	vůči	k7c3
chudým	chudý	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
nemají	mít	k5eNaImIp3nP
peníze	peníz	k1gInPc4
na	na	k7c4
dobré	dobrý	k2eAgMnPc4d1
právníky	právník	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Použití	použití	k1gNnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc1
výslovně	výslovně	k6eAd1
přikazuje	přikazovat	k5eAaImIp3nS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
za	za	k7c4
některá	některý	k3yIgNnPc4
provinění	provinění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanství	křesťanství	k1gNnSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
důležitost	důležitost	k1gFnSc4
odpuštění	odpuštění	k1gNnSc2
(	(	kIx(
<g/>
Ježíš	Ježíš	k1gMnSc1
v	v	k7c6
Jan	Jan	k1gMnSc1
8,1	8,1	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
zachraňuje	zachraňovat	k5eAaImIp3nS
před	před	k7c7
ukamenováním	ukamenování	k1gNnSc7
ženu	žena	k1gFnSc4
obviněnou	obviněný	k2eAgFnSc4d1
z	z	k7c2
cizoložství	cizoložství	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
posvátnost	posvátnost	k1gFnSc1
a	a	k8xC
nedotknutelnost	nedotknutelnost	k1gFnSc1
lidského	lidský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starozákonní	starozákonní	k2eAgInPc1d1
příkazy	příkaz	k1gInPc1
o	o	k7c6
popravách	poprava	k1gFnPc6
za	za	k7c4
určitá	určitý	k2eAgNnPc4d1
provinění	provinění	k1gNnPc4
nebyly	být	k5eNaImAgFnP
v	v	k7c6
křesťanském	křesťanský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
v	v	k7c6
plném	plný	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
nikdy	nikdy	k6eAd1
uskutečňovány	uskutečňován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
novozákonní	novozákonní	k2eAgInPc1d1
texty	text	k1gInPc1
nicméně	nicméně	k8xC
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
explicitně	explicitně	k6eAd1
neodmítají	odmítat	k5eNaImIp3nP
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
povinen	povinen	k2eAgMnSc1d1
podřídit	podřídit	k5eAaPmF
se	se	k3xPyFc4
světským	světský	k2eAgFnPc3d1
autoritám	autorita	k1gFnPc3
a	a	k8xC
tyto	tento	k3xDgFnPc1
autority	autorita	k1gFnPc1
mají	mít	k5eAaImIp3nP
právo	právo	k1gNnSc4
trestat	trestat	k5eAaImF
zlo	zlo	k1gNnSc4
„	„	k?
<g/>
mečem	meč	k1gInSc7
<g/>
“	“	k?
<g/>
:	:	kIx,
„	„	k?
<g/>
Vládcové	vládce	k1gMnPc1
nejsou	být	k5eNaImIp3nP
postrachem	postrach	k1gInSc7
dobrému	dobrý	k2eAgNnSc3d1
jednání	jednání	k1gNnSc3
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
zlému	zlý	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chceš	chtít	k5eAaImIp2nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ses	ses	k?
nemusel	muset	k5eNaImAgMnS
bát	bát	k5eAaImF
autority	autorita	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Čiň	činit	k5eAaImRp2nS
dobré	dobrá	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
budeš	být	k5eAaImBp2nS
mít	mít	k5eAaImF
od	od	k7c2
ní	on	k3xPp3gFnSc2
chválu	chvála	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždyť	vždyť	k9
je	být	k5eAaImIp3nS
Božím	boží	k2eAgMnSc7d1
služebníkem	služebník	k1gMnSc7
pro	pro	k7c4
tvé	tvůj	k3xOp2gNnSc4
dobro	dobro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednáš	jednat	k5eAaImIp2nS
<g/>
-li	-li	k?
však	však	k9
zle	zle	k6eAd1
<g/>
,	,	kIx,
boj	boj	k1gInSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
ne	ne	k9
nadarmo	nadarmo	k6eAd1
nosí	nosit	k5eAaImIp3nS
meč	meč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
Božím	boží	k2eAgMnSc7d1
služebníkem	služebník	k1gMnSc7
<g/>
,	,	kIx,
vykonavatelem	vykonavatel	k1gMnSc7
hněvu	hněv	k1gInSc2
nad	nad	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
činí	činit	k5eAaImIp3nS
zlo	zlo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
13,3	13,3	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
)	)	kIx)
Interpretace	interpretace	k1gFnPc1
těchto	tento	k3xDgFnPc2
pasáží	pasáž	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
směrech	směr	k1gInPc6
křesťanství	křesťanství	k1gNnSc2
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
Akvinský	Akvinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
teologické	teologický	k2eAgInPc1d1
názory	názor	k1gInPc1
jsou	být	k5eAaImIp3nP
základem	základ	k1gInSc7
současné	současný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
legitimní	legitimní	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
autorita	autorita	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
řádného	řádný	k2eAgInSc2d1
soudu	soud	k1gInSc2
právo	právo	k1gNnSc4
usmrtit	usmrtit	k5eAaPmF
zločince	zločinec	k1gMnSc4
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
to	ten	k3xDgNnSc4
zcela	zcela	k6eAd1
nezbytné	nezbytný	k2eAgNnSc4d1,k2eNgNnSc4d1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
společnosti	společnost	k1gFnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
encyklice	encyklika	k1gFnSc6
Evangelium	evangelium	k1gNnSc1
vitae	vitae	k6eAd1
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Dnešní	dnešní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
má	mít	k5eAaImIp3nS
přece	přece	k9
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
bránit	bránit	k5eAaImF
účinně	účinně	k6eAd1
kriminalitě	kriminalita	k1gFnSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
provinilce	provinilec	k1gMnSc4
zneškodní	zneškodnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
mu	on	k3xPp3gMnSc3
zcela	zcela	k6eAd1
uzavírala	uzavírat	k5eAaPmAgFnS,k5eAaImAgFnS
cestu	cesta	k1gFnSc4
k	k	k7c3
nápravě	náprava	k1gFnSc3
<g/>
...	...	k?
Musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
pečlivě	pečlivě	k6eAd1
zvažován	zvažován	k2eAgInSc4d1
a	a	k8xC
vybírán	vybírán	k2eAgInSc4d1
druh	druh	k1gInSc4
a	a	k8xC
způsob	způsob	k1gInSc4
trestu	trest	k1gInSc2
a	a	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
odsuzovat	odsuzovat	k5eAaImF
provinilce	provinilec	k1gMnPc4
k	k	k7c3
nejvyššímu	vysoký	k2eAgInSc3d3
trestu	trest	k1gInSc3
<g/>
,	,	kIx,
totiž	totiž	k9
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
o	o	k7c4
případ	případ	k1gInSc4
absolutní	absolutní	k2eAgFnSc2d1
nezbytnosti	nezbytnost	k1gFnSc2
<g/>
,	,	kIx,
totiž	totiž	k9
o	o	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
bránit	bránit	k5eAaImF
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
již	již	k6eAd1
<g/>
,	,	kIx,
díky	díky	k7c3
vhodnějšímu	vhodný	k2eAgNnSc3d2
uspořádání	uspořádání	k1gNnSc3
trestního	trestní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
tyto	tento	k3xDgInPc1
případy	případ	k1gInPc1
vyskytují	vyskytovat	k5eAaImIp3nP
velmi	velmi	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
důrazněji	důrazně	k6eAd2
vystupuje	vystupovat	k5eAaImIp3nS
proti	proti	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
papež	papež	k1gMnSc1
František	František	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
považuje	považovat	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc3d1
společenské	společenský	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
za	za	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
zcela	zcela	k6eAd1
nepřijatelný	přijatelný	k2eNgInSc1d1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
připouští	připouštět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
minulosti	minulost	k1gFnSc6
za	za	k7c4
jiné	jiný	k2eAgFnPc4d1
společenské	společenský	k2eAgFnPc4d1
situace	situace	k1gFnPc4
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
oprávněný	oprávněný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
papežovy	papežův	k2eAgInPc1d1
výroky	výrok	k1gInPc1
se	se	k3xPyFc4
setkaly	setkat	k5eAaPmAgInP
s	s	k7c7
ostrou	ostrý	k2eAgFnSc7d1
kritikou	kritika	k1gFnSc7
ze	z	k7c2
strany	strana	k1gFnSc2
konzervativních	konzervativní	k2eAgMnPc2d1
katolických	katolický	k2eAgMnPc2d1
publicistů	publicista	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
např.	např.	kA
Roman	Roman	k1gMnSc1
Joch	Joch	k?
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kriticky	kriticky	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
také	také	k9
představitelé	představitel	k1gMnPc1
anglikánské	anglikánský	k2eAgFnSc2d1
<g/>
,	,	kIx,
metodistické	metodistický	k2eAgFnSc2d1
a	a	k8xC
luteránské	luteránský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiné	k1gNnSc1
protestantské	protestantský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Jižní	jižní	k2eAgFnSc1d1
baptistická	baptistický	k2eAgFnSc1d1
konvence	konvence	k1gFnSc1
<g/>
,	,	kIx,
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
naopak	naopak	k6eAd1
schvalují	schvalovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
augsburského	augsburský	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
XVI	XVI	kA
<g/>
,1	,1	k4
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
ukládat	ukládat	k5eAaImF
zločincům	zločinec	k1gMnPc3
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
nepodporuje	podporovat	k5eNaImIp3nS
ani	ani	k8xC
neodsuzuje	odsuzovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
šaría	šaría	k6eAd1
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
schvaluje	schvalovat	k5eAaImIp3nS
a	a	k8xC
často	často	k6eAd1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
koránu	korán	k1gInSc6
např.	např.	kA
stojí	stát	k5eAaImIp3nP
:	:	kIx,
</s>
<s>
A	a	k9
odměnou	odměna	k1gFnSc7
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kdož	kdož	k3yRnSc1
vedli	vést	k5eAaImAgMnP
válku	válka	k1gFnSc4
proti	proti	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
Jeho	jeho	k3xOp3gNnPc3
poslu	posel	k1gMnSc3
a	a	k8xC
šířili	šířit	k5eAaImAgMnP
na	na	k7c6
zemi	zem	k1gFnSc6
pohoršení	pohoršení	k1gNnSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
věru	věra	k1gFnSc4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
zabiti	zabít	k5eAaPmNgMnP
anebo	anebo	k8xC
ukřižováni	ukřižován	k2eAgMnPc1d1
či	či	k8xC
budou	být	k5eAaImBp3nP
jim	on	k3xPp3gMnPc3
useknuty	useknout	k5eAaPmNgInP
jejich	jejich	k3xOp3gInPc1
pravé	pravý	k2eAgInPc1d1
ruce	ruka	k1gFnPc4
a	a	k8xC
levé	levý	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
anebo	anebo	k8xC
budou	být	k5eAaImBp3nP
ze	z	k7c2
země	zem	k1gFnSc2
vyhnáni	vyhnat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
těm	ten	k3xDgMnPc3
dostane	dostat	k5eAaPmIp3nS
se	se	k3xPyFc4
hanby	hanba	k1gFnSc2
na	na	k7c6
tomto	tento	k3xDgInSc6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
onom	onen	k3xDgInSc6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
očekává	očekávat	k5eAaImIp3nS
trest	trest	k1gInSc1
nesmírný	smírný	k2eNgInSc1d1
Korán	korán	k1gInSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
</s>
<s>
Judaismus	judaismus	k1gInSc1
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
v	v	k7c6
principu	princip	k1gInSc6
připouští	připouštět	k5eAaImIp3nS
<g/>
,	,	kIx,
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
Talmud	talmud	k1gInSc1
klade	klást	k5eAaImIp3nS
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc1
vynesení	vynesení	k1gNnSc1
spravedlivé	spravedlivý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ho	on	k3xPp3gInSc4
ale	ale	k9
v	v	k7c6
praxi	praxe	k1gFnSc6
činí	činit	k5eAaImIp3nS
téměř	téměř	k6eAd1
nemožným	možný	k2eNgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
citován	citován	k2eAgInSc1d1
výrok	výrok	k1gInSc1
židovského	židovský	k2eAgMnSc2d1
filosofa	filosof	k1gMnSc2
Maimonida	Maimonid	k1gMnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgMnSc1d2
propustit	propustit	k5eAaPmF
tisíc	tisíc	k4xCgInSc1
zločinců	zločinec	k1gMnPc2
než	než	k8xS
odsoudit	odsoudit	k5eAaPmF
k	k	k7c3
smrti	smrt	k1gFnSc3
jednoho	jeden	k4xCgInSc2
nevinného	vinný	k2eNgInSc2d1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
V	v	k7c6
Izraeli	Izrael	k1gInSc6
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
rozsudku	rozsudek	k1gInSc2
civilního	civilní	k2eAgInSc2d1
soudu	soud	k1gInSc2
vykonán	vykonat	k5eAaPmNgInS
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
nacistickém	nacistický	k2eAgMnSc6d1
válečném	válečný	k2eAgMnSc6d1
zločinci	zločinec	k1gMnSc6
Adolfu	Adolf	k1gMnSc6
Eichmannovi	Eichmann	k1gMnSc6
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
něj	on	k3xPp3gMnSc2
byl	být	k5eAaImAgInS
popraven	popraven	k2eAgInSc1d1
Meir	Meir	k1gInSc1
Tubianski	Tubiansk	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
nespravedlivého	spravedlivý	k2eNgInSc2d1
rozsudku	rozsudek	k1gInSc2
nařízeného	nařízený	k2eAgInSc2d1
Iserem	Isero	k1gNnSc7
Beerim	Beeri	k1gNnSc7
<g/>
,	,	kIx,
kontroverzním	kontroverzní	k2eAgMnSc7d1
šéfem	šéf	k1gMnSc7
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
Aman	Amana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
hinduistických	hinduistický	k2eAgNnPc6d1
písmech	písmo	k1gNnPc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Manu-smriti	Manu-smrit	k1gMnPc1
(	(	kIx(
<g/>
zákoník	zákoník	k1gInSc1
pro	pro	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vraha	vrah	k1gMnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
zabít	zabít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vlastně	vlastně	k9
(	(	kIx(
<g/>
v	v	k7c6
představách	představa	k1gFnPc6
hinduismu	hinduismus	k1gInSc2
<g/>
)	)	kIx)
projevuje	projevovat	k5eAaImIp3nS
vrahovi	vrah	k1gMnSc3
milost	milost	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
zabit	zabit	k5eAaPmNgMnS
v	v	k7c6
tomhle	tenhle	k3xDgInSc6
životě	život	k1gInSc6
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
zabit	zabít	k5eAaPmNgMnS
a	a	k8xC
podroben	podrobit	k5eAaPmNgInS
mnohem	mnohem	k6eAd1
většímu	veliký	k2eAgNnSc3d2
utrpení	utrpení	k1gNnSc3
v	v	k7c6
budoucích	budoucí	k2eAgInPc6d1
životech	život	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stanovisko	stanovisko	k1gNnSc1
buddhismu	buddhismus	k1gInSc2
není	být	k5eNaImIp3nS
jednoznačné	jednoznačný	k2eAgNnSc1d1
a	a	k8xC
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
směru	směr	k1gInSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Totéž	týž	k3xTgNnSc1
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
některé	některý	k3yIgInPc4
hinduistické	hinduistický	k2eAgInPc4d1
směry	směr	k1gInPc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Způsoby	způsob	k1gInPc1
popravy	poprava	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Poprava	poprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1
křeslo	křeslo	k1gNnSc1
</s>
<s>
Garota	Garota	k1gFnSc1
</s>
<s>
Smrtící	smrtící	k2eAgFnPc1d1
injekce	injekce	k1gFnPc1
</s>
<s>
Plynová	plynový	k2eAgFnSc1d1
komora	komora	k1gFnSc1
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1
křeslo	křeslo	k1gNnSc1
</s>
<s>
Zastřelení	zastřelený	k2eAgMnPc1d1
popravčí	popravčí	k1gMnPc1
četou	četa	k1gFnSc7
</s>
<s>
Oběšení	oběšení	k1gNnSc1
</s>
<s>
Setnutí	setnutí	k1gNnSc1
hlavy	hlava	k1gFnSc2
(	(	kIx(
<g/>
gilotina	gilotina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
historické	historický	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
<g/>
:	:	kIx,
ukřižování	ukřižování	k1gNnSc1
<g/>
,	,	kIx,
ukamenování	ukamenování	k1gNnSc1
<g/>
,	,	kIx,
naražení	naražení	k1gNnSc1
na	na	k7c4
kůl	kůl	k1gInSc4
<g/>
,	,	kIx,
estrapáda	estrapáda	k1gFnSc1
<g/>
,	,	kIx,
rozdupání	rozdupání	k1gNnSc1
slonem	slon	k1gMnSc7
<g/>
,	,	kIx,
upálení	upálení	k1gNnSc1
<g/>
,	,	kIx,
stětí	stětit	k5eAaImIp3nS,k5eAaPmIp3nS
mečem	meč	k1gInSc7
<g/>
,	,	kIx,
lámání	lámání	k1gNnPc4
kolem	kolem	k6eAd1
<g/>
,	,	kIx,
stažení	stažení	k1gNnSc1
z	z	k7c2
kůže	kůže	k1gFnSc2
a	a	k8xC
mnohé	mnohý	k2eAgFnPc4d1
další	další	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
těchto	tento	k3xDgInPc2
historických	historický	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
stále	stále	k6eAd1
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
uplatňovány	uplatňován	k2eAgInPc4d1
(	(	kIx(
<g/>
např.	např.	kA
ukamenování	ukamenování	k1gNnSc4
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
stětí	stětit	k5eAaImIp3nS,k5eAaPmIp3nS
mečem	meč	k1gInSc7
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
např.	např.	kA
Financial	Financial	k1gInSc1
Facts	Facts	k1gInSc1
about	about	k2eAgInSc1d1
the	the	k?
Death	Death	k1gInSc1
Penalty	penalta	k1gFnSc2
na	na	k7c4
www.deathpenaltyinfo.org	www.deathpenaltyinfo.org	k1gInSc4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
nebo	nebo	k8xC
Saving	Saving	k1gInSc1
lives	lives	k1gMnSc1
and	and	k?
money	monea	k1gFnSc2
v	v	k7c6
časopise	časopis	k1gInSc6
The	The	k1gFnSc1
Economist	Economist	k1gInSc1
12.3	12.3	k4
<g/>
.2009	.2009	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
ale	ale	k8xC
není	být	k5eNaImIp3nS
tento	tento	k3xDgInSc1
nepoměr	nepoměr	k1gInSc1
jednoznačný	jednoznačný	k2eAgInSc1d1
-	-	kIx~
viz	vidět	k5eAaImRp2nS
např.	např.	kA
diskusi	diskuse	k1gFnSc4
na	na	k7c4
deathpenalty	deathpenalt	k1gInPc4
<g/>
.	.	kIx.
<g/>
procon	procon	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc4
Death	Death	k1gMnSc1
Penalty	penalta	k1gFnSc2
Worldwide	Worldwid	k1gInSc5
(	(	kIx(
<g/>
www.infoplease.com	www.infoplease.com	k1gInSc4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.libri.cz/databaze/dejiny/text/t55.html	http://www.libri.cz/databaze/dejiny/text/t55.html	k1gInSc1
↑	↑	k?
http://zpravy.idnes.cz/domaci.aspx?r=domaci&	http://zpravy.idnes.cz/domaci.aspx?r=domaci&	k1gInSc1
<g/>
↑	↑	k?
http://www.moskyt.net/nase-sibenice-16-novodobe-veseni	http://www.moskyt.net/nase-sibenice-16-novodobe-vesen	k2eAgMnPc1d1
<g/>
↑	↑	k?
STICKINGS	STICKINGS	kA
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
'	'	kIx"
<g/>
put	puta	k1gFnPc2
thousands	thousands	k6eAd1
to	ten	k3xDgNnSc4
death	death	k1gMnSc1
<g/>
'	'	kIx"
in	in	k?
2019	#num#	k4
-	-	kIx~
far	fara	k1gFnPc2
more	mor	k1gInSc5
than	thana	k1gFnPc2
other	other	k1gMnSc1
countries	countries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mail	mail	k1gInSc1
Online	Onlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-04-21	2020-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Texas	Texas	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
popravil	popravit	k5eAaPmAgInS
již	již	k6eAd1
400	#num#	k4
odsouzených	odsouzená	k1gFnPc2
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
profil	profil	k1gInSc4
ČTK	ČTK	kA
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
http://www.deathpenaltyinfo.org/executions-us-1608-2002-espy-file	http://www.deathpenaltyinfo.org/executions-us-1608-2002-espy-fila	k1gFnSc6
<g/>
↑	↑	k?
Saudi	Saud	k1gMnPc1
Arabia	Arabium	k1gNnSc2
is	is	k?
beheading	beheading	k1gInSc1
one	one	k?
person	persona	k1gFnPc2
every	evera	k1gFnSc2
two	two	k?
days	days	k1gInSc1
(	(	kIx(
<g/>
and	and	k?
set	set	k1gInSc4
for	forum	k1gNnPc2
a	a	k8xC
new	new	k?
record	record	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Martin	Martin	k1gMnSc1
Ďurďovič	Ďurďovič	k1gMnSc1
<g/>
:	:	kIx,
Postoje	postoj	k1gInSc2
občanů	občan	k1gMnPc2
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
–	–	k?
květen	květen	k1gInSc1
2013	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
CVVM	CVVM	kA
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
20131	#num#	k4
2	#num#	k4
Daniel	Daniel	k1gMnSc1
Kunštát	Kunštát	k1gInSc1
<g/>
:	:	kIx,
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
res	res	k?
publica	publicum	k1gNnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc1
CVVM	CVVM	kA
<g/>
↑	↑	k?
ZLOČIN	zločin	k1gInSc1
A	a	k8xC
TREST	trest	k1gInSc1
<g/>
:	:	kIx,
Exkluzivní	exkluzivní	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
pro	pro	k7c4
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-03-06	2017-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Postoje	postoj	k1gInSc2
občanů	občan	k1gMnPc2
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
–	–	k?
květen	květen	k1gInSc1
2019	#num#	k4
-	-	kIx~
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
<g/>
.	.	kIx.
cvvm	cvvm	k1gMnSc1
<g/>
.	.	kIx.
<g/>
soc	soc	kA
<g/>
.	.	kIx.
<g/>
cas	cas	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Capital	Capital	k1gMnSc1
Punishment	Punishment	k1gMnSc1
and	and	k?
Police	police	k1gFnSc2
Safety	Safeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Death	Death	k1gInSc1
Penalty	penalty	k1gNnSc2
Information	Information	k1gInSc1
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Time	Tim	k1gFnSc2
on	on	k3xPp3gMnSc1
Death	Death	k1gMnSc1
Row	Row	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Death	Death	k1gInSc1
Penalty	penalty	k1gNnSc2
Information	Information	k1gInSc1
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Summa	Summum	k1gNnSc2
teologická	teologický	k2eAgFnSc1d1
II-II	II-II	k1gFnSc1
<g/>
,	,	kIx,
q.	q.	k?
64	#num#	k4
<g/>
-	-	kIx~
<g/>
63	#num#	k4
<g/>
↑	↑	k?
http://www.kebrle.cz/katdocs/EvangeliumVitae.htm	http://www.kebrle.cz/katdocs/EvangeliumVitae.htm	k1gMnSc1
<g/>
↑	↑	k?
https://www.cirkev.cz/cs/aktuality/180803papez-frantisek-trest-smrti-je-nepripustny-za-vsech-okolnosti	https://www.cirkev.cz/cs/aktuality/180803papez-frantisek-trest-smrti-je-nepripustny-za-vsech-okolnost	k1gFnSc2
<g/>
↑	↑	k?
http://ceskapozice.lidovky.cz/papez-frantisek-prekracuje-zakazem-trestu-smrti-sve-pravomoci-ps3-/tema.aspx?c=A180817_001513_pozice-tema_lube	http://ceskapozice.lidovky.cz/papez-frantisek-prekracuje-zakazem-trestu-smrti-sve-pravomoci-ps3-/tema.aspx?c=A180817_001513_pozice-tema_lubat	k5eAaPmIp3nS
<g/>
↑	↑	k?
Samuel	Samuel	k1gMnSc1
M.	M.	kA
Zwemer	Zwemer	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
law	law	k?
of	of	k?
Apostasy	Apostasa	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc1
Muslim	muslim	k1gMnSc1
World	World	k1gMnSc1
Volume	volum	k1gInSc5
14	#num#	k4
<g/>
,	,	kIx,
Issue	Issue	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
373	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
↑	↑	k?
Toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
objasňuje	objasňovat	k5eAaImIp3nS
A.	A.	kA
Č.	Č.	kA
Bhaktivédánta	Bhaktivédánta	k1gMnSc1
Svámí	Svámí	k1gNnSc2
Prabhupáda	Prabhupáda	k1gFnSc1
(	(	kIx(
<g/>
zakladatel	zakladatel	k1gMnSc1
hnutí	hnutí	k1gNnSc4
Hare	Har	k1gMnSc2
Kršna	Kršna	k1gMnSc1
<g/>
)	)	kIx)
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
výkladech	výklad	k1gInPc6
k	k	k7c3
védským	védský	k2eAgNnPc3d1
písmům	písmo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
ŠB	ŠB	kA
<g/>
.	.	kIx.
6.1	6.1	k4
<g/>
.8	.8	k4
<g/>
,	,	kIx,
ŠB	ŠB	kA
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
,	,	kIx,
Florian	Florian	k1gMnSc1
Sivák	Sivák	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
státu	stát	k1gInSc2
a	a	k8xC
práva	právo	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
do	do	k7c2
r.	r.	kA
1918	#num#	k4
<g/>
,	,	kIx,
Jinočany	Jinočan	k1gMnPc7
<g/>
,	,	kIx,
1993	#num#	k4
</s>
<s>
Cornelie	Cornelie	k1gFnSc1
C.	C.	kA
Bestová	Bestová	k1gFnSc1
<g/>
:	:	kIx,
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
v	v	k7c6
německo-českém	německo-český	k2eAgNnSc6d1
porovnání	porovnání	k1gNnSc6
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
právo	právo	k1gNnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Monestier	Monestier	k1gMnSc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Tauchen	Tauchen	k2eAgMnSc1d1
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
trestního	trestní	k2eAgNnSc2d1
soudnictví	soudnictví	k1gNnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
1933	#num#	k4
-	-	kIx~
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
The	The	k1gMnSc1
European	European	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
History	Histor	k1gInPc7
of	of	k?
Law	Law	k1gMnPc7
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
182	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978-80-904522-2-0	978-80-904522-2-0	k4
</s>
<s>
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
<g/>
:	:	kIx,
When	When	k1gMnSc1
The	The	k1gMnSc1
State	status	k1gInSc5
Kills	Kills	k1gInSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
1989	#num#	k4
</s>
<s>
Death	Death	k1gMnSc1
Penalty	penalty	k1gNnSc2
Information	Information	k1gInSc1
Center	centrum	k1gNnPc2
</s>
<s>
Britannica	Britannica	k1gMnSc1
<g/>
,	,	kIx,
1966	#num#	k4
</s>
<s>
Úřad	úřad	k1gInSc1
dokumentace	dokumentace	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
zločinů	zločin	k1gInPc2
komunismu	komunismus	k1gInSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Doživotí	doživotí	k1gNnSc1
</s>
<s>
Poprava	poprava	k1gFnSc1
</s>
<s>
Výjimečný	výjimečný	k2eAgInSc1d1
trest	trest	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
</s>
<s>
www.prodeathpenalty.com	www.prodeathpenalty.com	k1gInSc1
-	-	kIx~
Stránka	stránka	k1gFnSc1
amerických	americký	k2eAgMnPc2d1
příznivců	příznivec	k1gMnPc2
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc3
</s>
<s>
Web	web	k1gInSc1
Amnesty	Amnest	k1gInPc4
International	International	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgInPc4d1
přehledy	přehled	k1gInPc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
</s>
<s>
www.worldcoalition.org	www.worldcoalition.org	k1gInSc1
-	-	kIx~
Světová	světový	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
proti	proti	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
</s>
<s>
www.deathpenaltyinfo.org	www.deathpenaltyinfo.org	k1gInSc1
-	-	kIx~
Americký	americký	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
server	server	k1gInSc1
o	o	k7c6
trestu	trest	k1gInSc6
smrti	smrt	k1gFnSc2
</s>
<s>
O.	O.	kA
Liška	liška	k1gFnSc1
<g/>
:	:	kIx,
Vykonané	vykonaný	k2eAgInPc1d1
tresty	trest	k1gInPc1
smrti	smrt	k1gFnSc2
Československo	Československo	k1gNnSc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
(	(	kIx(
<g/>
doc	doc	kA
<g/>
,	,	kIx,
3	#num#	k4
MB	MB	kA
<g/>
)	)	kIx)
</s>
<s>
BlogTS	BlogTS	k?
-	-	kIx~
český	český	k2eAgInSc1d1
blog	blog	k1gInSc1
věnující	věnující	k2eAgFnSc2d1
se	se	k3xPyFc4
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
</s>
<s>
Poprava	poprava	k1gFnSc1
úkladného	úkladný	k2eAgMnSc2d1
vraha	vrah	k1gMnSc2
Hoffmanna	Hoffmann	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
vykonán	vykonán	k2eAgInSc4d1
–	–	k?
dobové	dobový	k2eAgFnPc4d1
reportáže	reportáž	k1gFnPc4
<g/>
,	,	kIx,
popisující	popisující	k2eAgFnPc4d1
okolnosti	okolnost	k1gFnPc4
a	a	k8xC
průběh	průběh	k1gInSc4
popravy	poprava	k1gFnSc2
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1895	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Smrt	smrt	k1gFnSc1
a	a	k8xC
související	související	k2eAgNnPc4d1
témata	téma	k1gNnPc4
lékařství	lékařství	k1gNnSc2
</s>
<s>
pitva	pitva	k1gFnSc1
•	•	k?
mozková	mozkový	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
•	•	k?
klinická	klinický	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
•	•	k?
eutanazie	eutanazie	k1gFnSc2
•	•	k?
permanentní	permanentní	k2eAgInSc4d1
vegetativní	vegetativní	k2eAgInSc4d1
stav	stav	k1gInSc4
•	•	k?
dystanázie	dystanázie	k1gFnSc2
•	•	k?
potrat	potrat	k1gInSc1
(	(	kIx(
<g/>
interrupce	interrupce	k1gFnPc1
<g/>
)	)	kIx)
fáze	fáze	k1gFnSc1
smrti	smrt	k1gFnSc2
</s>
<s>
pallor	pallor	k1gMnSc1
mortis	mortis	k1gFnSc2
•	•	k?
algor	algor	k1gMnSc1
mortis	mortis	k1gFnSc2
•	•	k?
rigor	rigor	k1gMnSc1
mortis	mortis	k1gFnSc2
•	•	k?
livor	livor	k1gInSc1
mortis	mortis	k1gFnSc1
•	•	k?
hnití	hnití	k1gNnPc2
•	•	k?
dekompozice	dekompozice	k1gFnSc2
•	•	k?
skeletonizace	skeletonizace	k1gFnSc1
mortalita	mortalita	k1gFnSc1
</s>
<s>
úmrtnost	úmrtnost	k1gFnSc1
(	(	kIx(
<g/>
dětská	dětský	k2eAgFnSc1d1
úmrtnost	úmrtnost	k1gFnSc1
•	•	k?
mateřská	mateřský	k2eAgFnSc1d1
úmrtnost	úmrtnost	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
nesmrtelnost	nesmrtelnost	k1gFnSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
</s>
<s>
s	s	k7c7
tělem	tělo	k1gNnSc7
</s>
<s>
pohřeb	pohřeb	k1gInSc1
•	•	k?
kremace	kremace	k1gFnSc2
•	•	k?
kryonika	kryonika	k1gFnSc1
•	•	k?
mumifikace	mumifikace	k1gFnSc2
•	•	k?
dekompozice	dekompozice	k1gFnSc2
•	•	k?
úmrtní	úmrtní	k2eAgInSc1d1
list	list	k1gInSc1
nadpřirozeně	nadpřirozeně	k6eAd1
</s>
<s>
smrtka	smrtka	k1gFnSc1
•	•	k?
vzkříšení	vzkříšení	k1gNnSc1
(	(	kIx(
<g/>
astrální	astrální	k2eAgNnSc1d1
cestování	cestování	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
reinkarnace	reinkarnace	k1gFnSc2
•	•	k?
posmrtný	posmrtný	k2eAgInSc4d1
život	život	k1gInSc4
(	(	kIx(
<g/>
život	život	k1gInSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
•	•	k?
seance	seance	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
sebevražda	sebevražda	k1gFnSc1
•	•	k?
vražda	vražda	k1gFnSc1
•	•	k?
eutanazie	eutanazie	k1gFnSc1
(	(	kIx(
<g/>
asistovaná	asistovaný	k2eAgFnSc1d1
sebevražda	sebevražda	k1gFnSc1
•	•	k?
sebevražedný	sebevražedný	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
•	•	k?
rána	ráno	k1gNnSc2
z	z	k7c2
milosti	milost	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
mučedník	mučedník	k1gMnSc1
•	•	k?
obětování	obětování	k1gNnPc2
(	(	kIx(
<g/>
lidská	lidský	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
•	•	k?
zvířecí	zvířecí	k2eAgFnSc4d1
oběť	oběť	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
válka	válka	k1gFnSc1
•	•	k?
genocida	genocida	k1gFnSc1
•	•	k?
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
126727	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4060306-4	4060306-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8926	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85019949	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85019949	#num#	k4
</s>
