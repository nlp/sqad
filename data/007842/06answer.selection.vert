<s>
Seriál	seriál	k1gInSc1
satiricky	satiricky	k6eAd1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
životě	život	k1gInSc6
americké	americký	k2eAgFnSc2d1
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
představované	představovaný	k2eAgFnSc2d1
rodinkou	rodinka	k1gFnSc7
Simpsonů	Simpson	k1gInPc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc1d1
Homera	Homera	k1gFnSc1
<g/>
,	,	kIx,
Marge	Marge	k1gFnSc1
<g/>
,	,	kIx,
Barta	Barta	k1gFnSc1
<g/>
,	,	kIx,
Lízu	Líza	k1gFnSc4
<g/>
,	,	kIx,
Maggie	Maggie	k1gFnSc1
<g/>
,	,	kIx,
dědu	děda	k1gMnSc4
Simpsona	Simpson	k1gMnSc4
<g/>
,	,	kIx,
psa	pes	k1gMnSc4
Spasitele	spasitel	k1gMnSc4
a	a	k8xC
kočku	kočka	k1gFnSc4
Sněhulku	Sněhulka	k1gFnSc4
<g/>
.	.	kIx.
</s>