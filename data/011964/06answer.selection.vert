<s>
Alfrédova	Alfrédův	k2eAgFnSc1d1	Alfrédova
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
též	též	k9	též
Alfrédka	Alfrédka	k1gFnSc1	Alfrédka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stával	stávat	k5eAaImAgInS	stávat
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Jelenky	Jelenka	k1gFnSc2	Jelenka
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
pramenů	pramen	k1gInPc2	pramen
Stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
.	.	kIx.	.
</s>
