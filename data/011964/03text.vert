<p>
<s>
Alfrédova	Alfrédův	k2eAgFnSc1d1	Alfrédova
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
též	též	k9	též
Alfrédka	Alfrédka	k1gFnSc1	Alfrédka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stával	stávat	k5eAaImAgInS	stávat
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Jelenky	Jelenka	k1gFnSc2	Jelenka
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
pramenů	pramen	k1gInPc2	pramen
Stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hrabě	hrabě	k1gMnSc1	hrabě
Alfréd	Alfréd	k1gMnSc1	Alfréd
Harrach	Harrach	k1gMnSc1	Harrach
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1884-1914	[number]	k4	1884-1914
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
janovické	janovický	k2eAgNnSc4d1	Janovické
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
téhož	týž	k3xTgNnSc2	týž
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
něco	něco	k3yInSc4	něco
níže	nízce	k6eAd2	nízce
(	(	kIx(	(
<g/>
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1078	[number]	k4	1078
m	m	kA	m
<g/>
)	)	kIx)	)
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
lovecká	lovecký	k2eAgFnSc1d1	lovecká
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
oblíbenosti	oblíbenost	k1gFnPc4	oblíbenost
místa	místo	k1gNnSc2	místo
turisty	turista	k1gMnSc2	turista
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
míst	místo	k1gNnPc2	místo
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
hlavního	hlavní	k2eAgInSc2d1	hlavní
hřebene	hřeben	k1gInSc2	hřeben
Jeseníků	Jeseník	k1gInPc2	Jeseník
<g/>
)	)	kIx)	)
svědčí	svědčit	k5eAaImIp3nP	svědčit
jak	jak	k6eAd1	jak
dochované	dochovaný	k2eAgInPc1d1	dochovaný
pohledy	pohled	k1gInPc1	pohled
<g/>
,	,	kIx,	,
tak	tak	k9	tak
především	především	k9	především
žádost	žádost	k1gFnSc4	žádost
Rýmařovské	Rýmařovský	k2eAgFnSc2d1	Rýmařovská
turistické	turistický	k2eAgFnSc2d1	turistická
sekce	sekce	k1gFnSc2	sekce
o	o	k7c6	o
vyznačení	vyznačení	k1gNnSc6	vyznačení
turistické	turistický	k2eAgFnSc2d1	turistická
cesty	cesta	k1gFnSc2	cesta
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
Vsi	ves	k1gFnSc2	ves
na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Jelení	jelení	k2eAgFnSc4d1	jelení
studánku	studánka	k1gFnSc4	studánka
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačení	vyznačení	k1gNnSc4	vyznačení
cesty	cesta	k1gFnSc2	cesta
nechal	nechat	k5eAaPmAgMnS	nechat
hrabě	hrabě	k1gMnSc1	hrabě
Harrach	Harrach	k1gMnSc1	Harrach
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
lovecké	lovecký	k2eAgFnSc2d1	lovecká
chaty	chata	k1gFnSc2	chata
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nevelký	velký	k2eNgInSc4d1	nevelký
srub	srub	k1gInSc4	srub
<g/>
,	,	kIx,	,
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
sloužící	sloužící	k1gFnSc2	sloužící
jak	jak	k8xS	jak
lovcům	lovec	k1gMnPc3	lovec
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
turistům	turist	k1gMnPc3	turist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohli	moct	k5eAaImAgMnP	moct
případně	případně	k6eAd1	případně
ubytovat	ubytovat	k5eAaPmF	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
lyžování	lyžování	k1gNnSc2	lyžování
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
chata	chata	k1gFnSc1	chata
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
otevřena	otevřít	k5eAaPmNgFnS	otevřít
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
stojí	stát	k5eAaImIp3nS	stát
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
chata	chata	k1gFnSc1	chata
přestávala	přestávat	k5eAaImAgFnS	přestávat
postupně	postupně	k6eAd1	postupně
kapacitně	kapacitně	k6eAd1	kapacitně
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
hrabě	hrabě	k1gMnSc1	hrabě
postavit	postavit	k5eAaPmF	postavit
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
o	o	k7c4	o
něco	něco	k3yInSc4	něco
výše	vysoce	k6eAd2	vysoce
mnohem	mnohem	k6eAd1	mnohem
prostornější	prostorný	k2eAgInSc4d2	prostornější
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nazýval	nazývat	k5eAaImAgInS	nazývat
Waidmannsheil	Waidmannsheil	k1gInSc4	Waidmannsheil
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Lovu	lov	k1gInSc2	lov
zdar	zdar	k1gInSc1	zdar
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
název	název	k1gInSc1	název
Alfrédova	Alfrédův	k2eAgFnSc1d1	Alfrédova
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Zámeček	zámeček	k1gInSc4	zámeček
využíval	využívat	k5eAaImAgMnS	využívat
pouze	pouze	k6eAd1	pouze
hrabě	hrabě	k1gMnSc1	hrabě
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
hosté	host	k1gMnPc1	host
a	a	k8xC	a
personál	personál	k1gInSc1	personál
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
lovecká	lovecký	k2eAgFnSc1d1	lovecká
chata	chata	k1gFnSc1	chata
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
k	k	k7c3	k
využívání	využívání	k1gNnSc1	využívání
turistům	turist	k1gMnPc3	turist
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrabě	hrabě	k1gMnSc1	hrabě
Harrach	Harrach	k1gMnSc1	Harrach
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
lesní	lesní	k2eAgFnSc2d1	lesní
reformy	reforma	k1gFnSc2	reforma
o	o	k7c4	o
janovické	janovický	k2eAgNnSc4d1	Janovické
panství	panství	k1gNnSc4	panství
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
byl	být	k5eAaImAgInS	být
zámeček	zámeček	k1gInSc4	zámeček
s	s	k7c7	s
chatou	chata	k1gFnSc7	chata
zkonfiskován	zkonfiskovat	k5eAaPmNgInS	zkonfiskovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
vzápětí	vzápětí	k6eAd1	vzápětí
předal	předat	k5eAaPmAgMnS	předat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Českým	český	k2eAgInPc3d1	český
lesům	les	k1gInPc3	les
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
zámečku	zámeček	k1gInSc2	zámeček
umístily	umístit	k5eAaPmAgFnP	umístit
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
správce	správce	k1gMnSc4	správce
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
hledalo	hledat	k5eAaImAgNnS	hledat
nové	nový	k2eAgNnSc1d1	nové
využití	využití	k1gNnSc1	využití
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
úředníci	úředník	k1gMnPc1	úředník
to	ten	k3xDgNnSc4	ten
odtud	odtud	k6eAd1	odtud
měli	mít	k5eAaImAgMnP	mít
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
využít	využít	k5eAaPmF	využít
vedení	vedení	k1gNnSc1	vedení
Jesenické	jesenický	k2eAgFnSc2d1	Jesenická
župy	župa	k1gFnSc2	župa
Klubu	klub	k1gInSc2	klub
československých	československý	k2eAgMnPc2d1	československý
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
KČST	KČST	kA	KČST
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
marně	marně	k6eAd1	marně
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
hledalo	hledat	k5eAaImAgNnS	hledat
záchytný	záchytný	k2eAgInSc4d1	záchytný
bod	bod	k1gInSc4	bod
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
horské	horský	k2eAgFnSc2d1	horská
chaty	chata	k1gFnSc2	chata
<g/>
,	,	kIx,	,
a	a	k8xC	a
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1933	[number]	k4	1933
získalo	získat	k5eAaPmAgNnS	získat
KČST	KČST	kA	KČST
objekt	objekt	k1gInSc1	objekt
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
do	do	k7c2	do
pronájmu	pronájem	k1gInSc2	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
adaptačních	adaptační	k2eAgFnPc2d1	adaptační
prací	práce	k1gFnPc2	práce
byl	být	k5eAaImAgMnS	být
zámeček	zámeček	k1gInSc4	zámeček
turistům	turist	k1gMnPc3	turist
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1933	[number]	k4	1933
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
sloužila	sloužit	k5eAaImAgFnS	sloužit
jen	jen	k9	jen
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
hájenka	hájenka	k1gFnSc1	hájenka
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
objektech	objekt	k1gInPc6	objekt
tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
ubytováno	ubytovat	k5eAaPmNgNnS	ubytovat
až	až	k9	až
50	[number]	k4	50
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
zámečku	zámeček	k1gInSc2	zámeček
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
zaveden	zavést	k5eAaPmNgInS	zavést
vodovod	vodovod	k1gInSc1	vodovod
a	a	k8xC	a
KČST	KČST	kA	KČST
na	na	k7c4	na
celkové	celkový	k2eAgFnPc4d1	celková
adaptační	adaptační	k2eAgFnPc4d1	adaptační
práce	práce	k1gFnPc4	práce
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
15	[number]	k4	15
000	[number]	k4	000
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Hájenka	hájenka	k1gFnSc1	hájenka
(	(	kIx(	(
<g/>
chata	chata	k1gFnSc1	chata
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
kolem	kolem	k7c2	kolem
150	[number]	k4	150
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
odpor	odpor	k1gInSc4	odpor
u	u	k7c2	u
německých	německý	k2eAgMnPc2d1	německý
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
k	k	k7c3	k
bojkotu	bojkot	k1gInSc3	bojkot
chaty	chata	k1gFnSc2	chata
a	a	k8xC	a
přeznačili	přeznačit	k5eAaPmAgMnP	přeznačit
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Jelení	jelení	k2eAgFnSc4d1	jelení
studánku	studánka	k1gFnSc4	studánka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	k9	aby
obě	dva	k4xCgFnPc4	dva
chaty	chata	k1gFnPc4	chata
míjela	míjet	k5eAaImAgFnS	míjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
–	–	k?	–
1936	[number]	k4	1936
se	se	k3xPyFc4	se
na	na	k7c6	na
Alfrédově	Alfrédův	k2eAgFnSc6d1	Alfrédova
chatě	chata	k1gFnSc6	chata
každoročně	každoročně	k6eAd1	každoročně
konaly	konat	k5eAaImAgInP	konat
kurzy	kurz	k1gInPc1	kurz
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
cvičitelů	cvičitel	k1gMnPc2	cvičitel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1933	[number]	k4	1933
KČST	KČST	kA	KČST
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
chatu	chata	k1gFnSc4	chata
u	u	k7c2	u
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
Jelení	jelení	k2eAgFnSc2d1	jelení
studánky	studánka	k1gFnSc2	studánka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
převzetím	převzetí	k1gNnSc7	převzetí
zapálena	zapálen	k2eAgFnSc1d1	zapálena
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
probíhala	probíhat	k5eAaImAgFnS	probíhat
mohutná	mohutný	k2eAgFnSc1d1	mohutná
propagace	propagace	k1gFnSc1	propagace
Alfrédky	Alfrédka	k1gFnSc2	Alfrédka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
turistickém	turistický	k2eAgInSc6d1	turistický
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
vytíženost	vytíženost	k1gFnSc1	vytíženost
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
turisté	turist	k1gMnPc1	turist
totiž	totiž	k9	totiž
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
Jeseníků	Jeseník	k1gInPc2	Jeseník
často	často	k6eAd1	často
vynechávali	vynechávat	k5eAaImAgMnP	vynechávat
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
pak	pak	k6eAd1	pak
bojkotovali	bojkotovat	k5eAaImAgMnP	bojkotovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
hledalo	hledat	k5eAaImAgNnS	hledat
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
využití	využití	k1gNnSc1	využití
chaty	chata	k1gFnSc2	chata
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pořádání	pořádání	k1gNnSc1	pořádání
školních	školní	k2eAgInPc2d1	školní
výletů	výlet	k1gInPc2	výlet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
vyznačení	vyznačení	k1gNnSc2	vyznačení
dočkala	dočkat	k5eAaPmAgFnS	dočkat
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
cesta	cesta	k1gFnSc1	cesta
Alfrédova	Alfrédův	k2eAgFnSc1d1	Alfrédova
chata	chata	k1gFnSc1	chata
–	–	k?	–
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hole	hole	k1gFnSc1	hole
–	–	k?	–
Praděd	praděd	k1gMnSc1	praděd
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1935	[number]	k4	1935
dostavby	dostavba	k1gFnSc2	dostavba
silnice	silnice	k1gFnSc2	silnice
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
Vsi	ves	k1gFnSc2	ves
až	až	k9	až
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
chatě	chata	k1gFnSc3	chata
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nízká	nízký	k2eAgFnSc1d1	nízká
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
měli	mít	k5eAaImAgMnP	mít
nemalý	malý	k2eNgInSc4d1	nemalý
podíl	podíl	k1gInSc4	podíl
Němci	Němec	k1gMnPc1	Němec
z	z	k7c2	z
Karlova	Karlův	k2eAgInSc2d1	Karlův
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgMnPc4d1	Malé
Morávky	Morávek	k1gMnPc4	Morávek
a	a	k8xC	a
Staré	Starý	k1gMnPc4	Starý
Vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
českým	český	k2eAgMnPc3d1	český
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
vnucovali	vnucovat	k5eAaImAgMnP	vnucovat
ubytování	ubytování	k1gNnSc4	ubytování
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
penzionech	penzion	k1gInPc6	penzion
a	a	k8xC	a
před	před	k7c7	před
Alfrédkou	Alfrédka	k1gFnSc7	Alfrédka
je	on	k3xPp3gMnPc4	on
varovali	varovat	k5eAaImAgMnP	varovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
Alfrédky	Alfrédka	k1gFnSc2	Alfrédka
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
KČST	KČST	kA	KČST
pro	pro	k7c4	pro
přeznačkování	přeznačkování	k1gNnSc4	přeznačkování
zdejších	zdejší	k2eAgFnPc2d1	zdejší
tras	trasa	k1gFnPc2	trasa
a	a	k8xC	a
do	do	k7c2	do
Rýmařova	rýmařův	k2eAgInSc2d1	rýmařův
a	a	k8xC	a
Staré	Staré	k2eAgFnSc6d1	Staré
Vsi	ves	k1gFnSc6	ves
navíc	navíc	k6eAd1	navíc
umístil	umístit	k5eAaPmAgMnS	umístit
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnPc4d1	psaná
orientační	orientační	k2eAgFnPc4d1	orientační
tabule	tabule	k1gFnPc4	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
Jeseníků	Jeseník	k1gInPc2	Jeseník
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
měla	mít	k5eAaImAgFnS	mít
české	český	k2eAgNnSc4d1	české
značkování	značkování	k1gNnSc4	značkování
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc4	zbytek
nadále	nadále	k6eAd1	nadále
využíval	využívat	k5eAaImAgMnS	využívat
německé	německý	k2eAgNnSc4d1	německé
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
nájemce	nájemce	k1gMnSc2	nájemce
<g/>
,	,	kIx,	,
když	když	k8xS	když
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1935	[number]	k4	1935
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
F.	F.	kA	F.
L.	L.	kA	L.
Bukovský	Bukovský	k1gMnSc1	Bukovský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
získal	získat	k5eAaPmAgMnS	získat
zkušenosti	zkušenost	k1gFnSc2	zkušenost
i	i	k8xC	i
několikaletém	několikaletý	k2eAgInSc6d1	několikaletý
působením	působení	k1gNnPc3	působení
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
však	však	k9	však
nadále	nadále	k6eAd1	nadále
využívali	využívat	k5eAaPmAgMnP	využívat
především	především	k9	především
ubytování	ubytování	k1gNnSc4	ubytování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Morávce	Morávka	k1gFnSc6	Morávka
či	či	k8xC	či
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
muselo	muset	k5eAaImAgNnS	muset
KČST	KČST	kA	KČST
přiznat	přiznat	k5eAaPmF	přiznat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chata	chata	k1gFnSc1	chata
nachází	nacházet	k5eAaImIp3nS	nacházet
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hlavních	hlavní	k2eAgNnPc2d1	hlavní
jesenických	jesenický	k2eAgNnPc2d1	Jesenické
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
snížením	snížení	k1gNnSc7	snížení
počtu	počet	k1gInSc2	počet
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
zřízením	zřízení	k1gNnSc7	zřízení
společných	společný	k2eAgFnPc2d1	společná
nocleháren	noclehárna	k1gFnPc2	noclehárna
a	a	k8xC	a
snížením	snížení	k1gNnSc7	snížení
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1936	[number]	k4	1936
měla	mít	k5eAaImAgFnS	mít
chata	chata	k1gFnSc1	chata
nového	nový	k2eAgMnSc2d1	nový
nájemce	nájemce	k1gMnSc2	nájemce
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
jím	on	k3xPp3gNnSc7	on
byla	být	k5eAaImAgFnS	být
Julie	Julie	k1gFnSc1	Julie
Peerová	Peerová	k1gFnSc1	Peerová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
účetní	účetní	k1gMnPc4	účetní
na	na	k7c6	na
Schreiberově	Schreiberův	k2eAgFnSc6d1	Schreiberova
chatě	chata	k1gFnSc6	chata
na	na	k7c6	na
Červenohorském	červenohorský	k2eAgNnSc6d1	Červenohorské
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
zde	zde	k6eAd1	zde
přespalo	přespat	k5eAaPmAgNnS	přespat
na	na	k7c4	na
3000	[number]	k4	3000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
na	na	k7c6	na
pokojích	pokoj	k1gInPc6	pokoj
topili	topit	k5eAaImAgMnP	topit
v	v	k7c6	v
kamínkách	kamínka	k1gNnPc6	kamínka
<g/>
,	,	kIx,	,
popel	popel	k1gInSc1	popel
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
vynášen	vynášet	k5eAaImNgInS	vynášet
za	za	k7c4	za
chatu	chata	k1gFnSc4	chata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
pak	pak	k6eAd1	pak
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
KČST	KČST	kA	KČST
společně	společně	k6eAd1	společně
s	s	k7c7	s
svazem	svaz	k1gInSc7	svaz
Lyžařů	lyžař	k1gMnPc2	lyžař
republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
LRČ	LRČ	kA	LRČ
<g/>
)	)	kIx)	)
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
závod	závod	k1gInSc4	závod
o	o	k7c4	o
Stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
jesenickou	jesenický	k2eAgFnSc4d1	Jesenická
lyži	lyže	k1gFnSc4	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
okolí	okolí	k1gNnSc2	okolí
chaty	chata	k1gFnSc2	chata
<g/>
,	,	kIx,	,
především	především	k9	především
vykácení	vykácení	k1gNnSc1	vykácení
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
navíc	navíc	k6eAd1	navíc
padlo	padnout	k5eAaImAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
opětovném	opětovný	k2eAgNnSc6d1	opětovné
zřízení	zřízení	k1gNnSc6	zřízení
bufetu	bufet	k1gInSc2	bufet
u	u	k7c2	u
Jelení	jelení	k2eAgFnSc2d1	jelení
studánky	studánka	k1gFnSc2	studánka
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgMnSc4	jenž
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
starat	starat	k5eAaImF	starat
nájemkyně	nájemkyně	k1gFnSc1	nájemkyně
Alfrédky	Alfrédka	k1gFnSc2	Alfrédka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichově	Mnichov	k1gInSc6	Mnichov
chata	chata	k1gFnSc1	chata
přešla	přejít	k5eAaPmAgFnS	přejít
opět	opět	k6eAd1	opět
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1944	[number]	k4	1944
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konali	konat	k5eAaImAgMnP	konat
především	především	k9	především
letní	letní	k2eAgInPc4d1	letní
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
chata	chata	k1gFnSc1	chata
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
KČST	KČST	kA	KČST
<g/>
,	,	kIx,	,
nájemcem	nájemce	k1gMnSc7	nájemce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pan	pan	k1gMnSc1	pan
Vavrečka	Vavrečka	k1gFnSc1	Vavrečka
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Chata	chata	k1gFnSc1	chata
i	i	k9	i
nadále	nadále	k6eAd1	nadále
trpěla	trpět	k5eAaImAgFnS	trpět
slabou	slabý	k2eAgFnSc7d1	slabá
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
příchozí	příchozí	k1gMnPc1	příchozí
osídlenci	osídlenec	k1gMnPc1	osídlenec
oblasti	oblast	k1gFnSc2	oblast
totiž	totiž	k9	totiž
měli	mít	k5eAaImAgMnP	mít
docela	docela	k6eAd1	docela
jiné	jiný	k2eAgFnSc3d1	jiná
starosti	starost	k1gFnSc3	starost
a	a	k8xC	a
ke	k	k7c3	k
krajině	krajina	k1gFnSc3	krajina
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
neměli	mít	k5eNaImAgMnP	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
pořádně	pořádně	k6eAd1	pořádně
nefungovala	fungovat	k5eNaImAgFnS	fungovat
ani	ani	k8xC	ani
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
turisté	turist	k1gMnPc1	turist
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
dostávali	dostávat	k5eAaImAgMnP	dostávat
jen	jen	k9	jen
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
však	však	k8xC	však
KČST	KČST	kA	KČST
o	o	k7c4	o
chatu	chata	k1gFnSc4	chata
přišel	přijít	k5eAaPmAgMnS	přijít
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
připadla	připadnout	k5eAaPmAgFnS	připadnout
České	český	k2eAgFnSc3d1	Česká
obci	obec	k1gFnSc3	obec
sokolské	sokolská	k1gFnSc2	sokolská
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zrušení	zrušení	k1gNnSc6	zrušení
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
n.	n.	k?	n.
p.	p.	k?	p.
Turista	turista	k1gMnSc1	turista
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jejím	její	k3xOp3gNnSc7	její
provozováním	provozování	k1gNnSc7	provozování
pověřil	pověřit	k5eAaPmAgMnS	pověřit
n.	n.	k?	n.
p.	p.	k?	p.
Restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
jídelny	jídelna	k1gFnSc2	jídelna
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
byrokratické	byrokratický	k2eAgFnSc3d1	byrokratická
složitosti	složitost	k1gFnSc3	složitost
ubytovávání	ubytovávání	k1gNnSc4	ubytovávání
turistů	turist	k1gMnPc2	turist
přešla	přejít	k5eAaPmAgFnS	přejít
chata	chata	k1gFnSc1	chata
především	především	k9	především
na	na	k7c4	na
restaurační	restaurační	k2eAgInSc4d1	restaurační
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
provoz	provoz	k1gInSc1	provoz
dotován	dotován	k2eAgInSc1d1	dotován
státem	stát	k1gInSc7	stát
vydržela	vydržet	k5eAaPmAgFnS	vydržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vinou	vinou	k7c2	vinou
neopatrnosti	neopatrnost	k1gFnSc2	neopatrnost
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rekreantů	rekreant	k1gMnPc2	rekreant
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
okresní	okresní	k2eAgInSc1d1	okresní
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
Bruntál	Bruntál	k1gInSc1	Bruntál
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
zastřešení	zastřešení	k1gNnSc6	zastřešení
přístřešku	přístřešek	k1gInSc2	přístřešek
na	na	k7c6	na
Jelení	jelení	k2eAgFnSc6d1	jelení
studánce	studánka	k1gFnSc6	studánka
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgMnSc4	který
investoval	investovat	k5eAaBmAgMnS	investovat
70	[number]	k4	70
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Přístřešek	přístřešek	k1gInSc1	přístřešek
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nově	nově	k6eAd1	nově
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgInSc7d1	jediný
ochranným	ochranný	k2eAgInSc7d1	ochranný
objektem	objekt	k1gInSc7	objekt
mezi	mezi	k7c7	mezi
Skřítkem	skřítek	k1gMnSc7	skřítek
a	a	k8xC	a
Barborkou	Barborka	k1gFnSc7	Barborka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
začalo	začít	k5eAaPmAgNnS	začít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
Alfrédky	Alfrédka	k1gFnPc4	Alfrédka
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nedělo	dít	k5eNaBmAgNnS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Chyběly	chybět	k5eAaImAgInP	chybět
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
plány	plán	k1gInPc4	plán
a	a	k8xC	a
především	především	k6eAd1	především
neexistovala	existovat	k5eNaImAgFnS	existovat
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Základů	základ	k1gInPc2	základ
chaty	chata	k1gFnSc2	chata
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ujaly	ujmout	k5eAaPmAgFnP	ujmout
Kovohutě	kovohuť	k1gFnPc1	kovohuť
Břidličná	břidličný	k2eAgFnSc1d1	Břidličná
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
zde	zde	k6eAd1	zde
vystavěly	vystavět	k5eAaPmAgFnP	vystavět
chatu	chata	k1gFnSc4	chata
novou	nový	k2eAgFnSc4d1	nová
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
46	[number]	k4	46
lůžek	lůžko	k1gNnPc2	lůžko
a	a	k8xC	a
jídelna	jídelna	k1gFnSc1	jídelna
sloužící	sloužící	k1gFnSc2	sloužící
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k8xS	jako
společenský	společenský	k2eAgInSc1d1	společenský
sál	sál	k1gInSc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
krytá	krytý	k2eAgFnSc1d1	krytá
veranda	veranda	k1gFnSc1	veranda
a	a	k8xC	a
zastřešeno	zastřešen	k2eAgNnSc1d1	zastřešeno
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vstupní	vstupní	k2eAgNnSc1d1	vstupní
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Chata	chata	k1gFnSc1	chata
posléze	posléze	k6eAd1	posléze
sloužila	sloužit	k5eAaImAgFnS	sloužit
především	především	k6eAd1	především
jako	jako	k8xS	jako
podnikové	podnikový	k2eAgNnSc1d1	podnikové
rekreační	rekreační	k2eAgNnSc1d1	rekreační
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
turisté	turist	k1gMnPc1	turist
zde	zde	k6eAd1	zde
mohli	moct	k5eAaImAgMnP	moct
nalézt	nalézt	k5eAaBmF	nalézt
občerstvení	občerstvení	k1gNnPc4	občerstvení
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
přespat	přespat	k5eAaPmF	přespat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
prošla	projít	k5eAaPmAgFnS	projít
privatizací	privatizace	k1gFnSc7	privatizace
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
pronajímal	pronajímat	k5eAaImAgMnS	pronajímat
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
chata	chata	k1gFnSc1	chata
nadále	nadále	k6eAd1	nadále
fungovala	fungovat	k5eAaImAgFnS	fungovat
jako	jako	k9	jako
turistická	turistický	k2eAgFnSc1d1	turistická
ubytovna	ubytovna	k1gFnSc1	ubytovna
a	a	k8xC	a
nájemce	nájemce	k1gMnSc1	nájemce
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
její	její	k3xOp3gInSc4	její
celoroční	celoroční	k2eAgInSc4d1	celoroční
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
43	[number]	k4	43
lůžek	lůžko	k1gNnPc2	lůžko
ve	v	k7c6	v
3	[number]	k4	3
–	–	k?	–
4	[number]	k4	4
lůžkových	lůžkový	k2eAgFnPc2d1	lůžková
pokojích	pokoj	k1gInPc6	pokoj
se	s	k7c7	s
společnými	společný	k2eAgFnPc7d1	společná
sprchami	sprcha	k1gFnPc7	sprcha
a	a	k8xC	a
sociálním	sociální	k2eAgNnSc7d1	sociální
zařízením	zařízení	k1gNnSc7	zařízení
na	na	k7c6	na
chodbě	chodba	k1gFnSc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Jídelna	jídelna	k1gFnSc1	jídelna
pak	pak	k6eAd1	pak
sloužila	sloužit	k5eAaImAgFnS	sloužit
také	také	k9	také
jako	jako	k9	jako
společenský	společenský	k2eAgInSc1d1	společenský
sál	sál	k1gInSc1	sál
s	s	k7c7	s
televizí	televize	k1gFnSc7	televize
se	s	k7c7	s
satelitním	satelitní	k2eAgInSc7d1	satelitní
příjmem	příjem	k1gInSc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
jídelna	jídelna	k1gFnSc1	jídelna
měla	mít	k5eAaImAgFnS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
55	[number]	k4	55
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
50	[number]	k4	50
návštěvníků	návštěvník	k1gMnPc2	návštěvník
se	se	k3xPyFc4	se
vešlo	vejít	k5eAaPmAgNnS	vejít
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
restaurace	restaurace	k1gFnSc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lyžaře	lyžař	k1gMnSc4	lyžař
nechyběla	chybět	k5eNaImAgFnS	chybět
lyžárna	lyžárna	k1gFnSc1	lyžárna
a	a	k8xC	a
sušárna	sušárna	k1gFnSc1	sušárna
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
cvičná	cvičný	k2eAgFnSc1d1	cvičná
louka	louka	k1gFnSc1	louka
s	s	k7c7	s
250	[number]	k4	250
m	m	kA	m
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vlekem	vlek	k1gInSc7	vlek
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
chaty	chata	k1gFnSc2	chata
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
i	i	k9	i
parkoviště	parkoviště	k1gNnSc1	parkoviště
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
ubytovaným	ubytovaný	k2eAgFnPc3d1	ubytovaná
umožněn	umožněn	k2eAgInSc1d1	umožněn
příjezd	příjezd	k1gInSc4	příjezd
od	od	k7c2	od
Rýmařova	rýmařův	k2eAgNnSc2d1	rýmařův
a	a	k8xC	a
Staré	Staré	k2eAgFnPc1d1	Staré
Vsi	ves	k1gFnPc1	ves
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
chata	chata	k1gFnSc1	chata
znovu	znovu	k6eAd1	znovu
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
přes	přes	k7c4	přes
Jeseníky	Jeseník	k1gInPc4	Jeseník
přehnala	přehnat	k5eAaPmAgFnS	přehnat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
hasiče	hasič	k1gMnPc4	hasič
zbyly	zbýt	k5eAaPmAgInP	zbýt
pouze	pouze	k6eAd1	pouze
ohořelé	ohořelý	k2eAgInPc1d1	ohořelý
základy	základ	k1gInPc1	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obdélnou	obdélný	k2eAgFnSc4d1	obdélná
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
kamenné	kamenný	k2eAgFnSc6d1	kamenná
podezdívce	podezdívka	k1gFnSc6	podezdívka
<g/>
,	,	kIx,	,
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
sedlovou	sedlový	k2eAgFnSc7d1	sedlová
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
průčelní	průčelní	k2eAgFnSc6d1	průčelní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
krytá	krytý	k2eAgFnSc1d1	krytá
terasa	terasa	k1gFnSc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
vnitřním	vnitřní	k2eAgFnPc3d1	vnitřní
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Terasa	terasa	k1gFnSc1	terasa
před	před	k7c7	před
průčelím	průčelí	k1gNnSc7	průčelí
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
podkrovních	podkrovní	k2eAgFnPc2d1	podkrovní
místností	místnost	k1gFnPc2	místnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dostupnost	dostupnost	k1gFnSc4	dostupnost
==	==	k?	==
</s>
</p>
<p>
<s>
Místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lovecký	lovecký	k2eAgInSc1d1	lovecký
zámeček	zámeček	k1gInSc1	zámeček
stával	stávat	k5eAaImAgInS	stávat
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgFnPc4	tři
značené	značený	k2eAgFnPc4d1	značená
turistické	turistický	k2eAgFnPc4d1	turistická
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
značka	značka	k1gFnSc1	značka
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Jelení	jelení	k2eAgFnSc2d1	jelení
studánky	studánka	k1gFnSc2	studánka
na	na	k7c4	na
Žďárský	žďárský	k2eAgInSc4d1	žďárský
Potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
pak	pak	k6eAd1	pak
od	od	k7c2	od
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
pod	pod	k7c7	pod
Pradědem	praděd	k1gMnSc7	praděd
také	také	k9	také
ke	k	k7c3	k
Žďárskému	žďárský	k2eAgInSc3d1	žďárský
Potoku	potok	k1gInSc3	potok
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
turistickou	turistický	k2eAgFnSc7d1	turistická
značkou	značka	k1gFnSc7	značka
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Jelení	jelení	k2eAgFnSc2d1	jelení
studánky	studánka	k1gFnSc2	studánka
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Skřítek	skřítek	k1gMnSc1	skřítek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Alfrédka	Alfrédka	k1gFnSc1	Alfrédka
na	na	k7c4	na
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
článek	článek	k1gInSc1	článek
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Korandů	Korand	k1gInPc2	Korand
na	na	k7c4	na
jesenik	jesenik	k1gInSc4	jesenik
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
