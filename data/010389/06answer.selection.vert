<s>
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
též	též	k9	též
fundamentální	fundamentální	k2eAgFnSc2d1	fundamentální
nebo	nebo	k8xC	nebo
základní	základní	k2eAgFnSc2d1	základní
částice	částice	k1gFnSc2	částice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
částice	částice	k1gFnSc2	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
