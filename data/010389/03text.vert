<p>
<s>
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
též	též	k9	též
fundamentální	fundamentální	k2eAgFnSc2d1	fundamentální
nebo	nebo	k8xC	nebo
základní	základní	k2eAgFnSc2d1	základní
částice	částice	k1gFnSc2	částice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
částice	částice	k1gFnSc2	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
částice	částice	k1gFnSc1	částice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
základní	základní	k2eAgInPc4d1	základní
fermiony	fermion	k1gInPc4	fermion
(	(	kIx(	(
<g/>
kvarky	kvark	k1gInPc4	kvark
<g/>
,	,	kIx,	,
leptony	lepton	k1gInPc4	lepton
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
antičástice	antičástice	k1gFnPc4	antičástice
<g/>
)	)	kIx)	)
a	a	k8xC	a
základní	základní	k2eAgFnPc4d1	základní
bosony	bosona	k1gFnPc4	bosona
(	(	kIx(	(
<g/>
výměnné	výměnný	k2eAgFnPc4d1	výměnná
bosony	bosona	k1gFnPc4	bosona
a	a	k8xC	a
Higgsův	Higgsův	k2eAgInSc4d1	Higgsův
boson	boson	k1gInSc4	boson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
nedělitelných	dělitelný	k2eNgFnPc2d1	nedělitelná
částic	částice	k1gFnPc2	částice
přišel	přijít	k5eAaPmAgInS	přijít
již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
filosof	filosof	k1gMnSc1	filosof
Démokritos	Démokritos	k1gMnSc1	Démokritos
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
atomisté	atomista	k1gMnPc1	atomista
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
"	"	kIx"	"
<g/>
nedělitelný	dělitelný	k2eNgMnSc1d1	nedělitelný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
ožila	ožít	k5eAaPmAgFnS	ožít
zejména	zejména	k9	zejména
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
formovaly	formovat	k5eAaImAgInP	formovat
základy	základ	k1gInPc1	základ
vědecké	vědecký	k2eAgFnSc2d1	vědecká
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
z	z	k7c2	z
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
formovala	formovat	k5eAaImAgFnS	formovat
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
radikálně	radikálně	k6eAd1	radikálně
změnila	změnit	k5eAaPmAgFnS	změnit
naše	náš	k3xOp1gNnSc4	náš
chápání	chápání	k1gNnSc4	chápání
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
umožnila	umožnit	k5eAaPmAgFnS	umožnit
teoreticky	teoreticky	k6eAd1	teoreticky
popsat	popsat	k5eAaPmF	popsat
a	a	k8xC	a
experimentálně	experimentálně	k6eAd1	experimentálně
ověřit	ověřit	k5eAaPmF	ověřit
existenci	existence	k1gFnSc4	existence
nových	nový	k2eAgFnPc2d1	nová
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
matematiky	matematika	k1gFnSc2	matematika
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c2	za
bodové	bodový	k2eAgFnSc2d1	bodová
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgFnPc4	některý
částicové	částicový	k2eAgFnPc4d1	částicová
teorie	teorie	k1gFnPc4	teorie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
teorie	teorie	k1gFnSc1	teorie
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
rozměry	rozměr	k1gInPc1	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Částice	částice	k1gFnPc4	částice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
teorie	teorie	k1gFnSc1	teorie
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
<g/>
,	,	kIx,	,
slabé	slabý	k2eAgFnSc2d1	slabá
a	a	k8xC	a
silné	silný	k2eAgFnSc2d1	silná
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
základních	základní	k2eAgFnPc2d1	základní
částic	částice	k1gFnPc2	částice
hmoty	hmota	k1gFnSc2	hmota
–	–	k?	–
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
leptony	lepton	k1gInPc1	lepton
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
generačních	generační	k2eAgFnPc6d1	generační
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
částici	částice	k1gFnSc3	částice
hmoty	hmota	k1gFnSc2	hmota
přísluší	příslušet	k5eAaImIp3nS	příslušet
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
antičástice	antičástice	k1gFnSc1	antičástice
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgFnPc3d1	základní
interakcím	interakce	k1gFnPc3	interakce
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
foton	foton	k1gInSc1	foton
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
interakci	interakce	k1gFnSc3	interakce
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
objevené	objevený	k2eAgInPc1d1	objevený
a	a	k8xC	a
potvrzené	potvrzená	k1gFnPc4	potvrzená
Higgsovy	Higgsův	k2eAgFnSc2d1	Higgsova
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
částice	částice	k1gFnSc1	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
leptony	lepton	k1gInPc1	lepton
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
fermiony	fermion	k1gInPc1	fermion
<g/>
,	,	kIx,	,
intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgMnPc4	všechen
bosony	boson	k1gMnPc4	boson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavební	stavební	k2eAgFnPc1d1	stavební
částice	částice	k1gFnPc1	částice
hmoty	hmota	k1gFnSc2	hmota
===	===	k?	===
</s>
</p>
<p>
<s>
Leptony	Lepton	k1gInPc1	Lepton
jsou	být	k5eAaImIp3nP	být
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
generačních	generační	k2eAgFnPc6d1	generační
variantách	varianta	k1gFnPc6	varianta
(	(	kIx(	(
<g/>
elektron	elektron	k1gInSc4	elektron
<g/>
,	,	kIx,	,
mion	mion	k1gNnSc4	mion
a	a	k8xC	a
tauon	tauon	k1gNnSc4	tauon
<g/>
;	;	kIx,	;
a	a	k8xC	a
neutrino	neutrino	k1gNnSc1	neutrino
elektronové	elektronový	k2eAgNnSc1d1	elektronové
<g/>
,	,	kIx,	,
mionové	mionový	k2eAgNnSc1d1	mionový
a	a	k8xC	a
tauonové	tauonový	k2eAgNnSc1d1	tauonové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
lepton	lepton	k1gInSc1	lepton
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
antičástici	antičástice	k1gFnSc4	antičástice
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pozitron	pozitron	k1gInSc1	pozitron
<g/>
.	.	kIx.	.
</s>
<s>
Leptony	Lepton	k1gInPc1	Lepton
nemají	mít	k5eNaImIp3nP	mít
podle	podle	k7c2	podle
současného	současný	k2eAgNnSc2d1	současné
poznání	poznání	k1gNnSc2	poznání
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
podléhají	podléhat	k5eAaImIp3nP	podléhat
slabé	slabý	k2eAgFnSc3d1	slabá
interakci	interakce	k1gFnSc3	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
mion	mion	k1gNnSc1	mion
a	a	k8xC	a
tauon	tauon	k1gNnSc1	tauon
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
jednotkový	jednotkový	k2eAgInSc4d1	jednotkový
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
interagují	interagovat	k5eAaPmIp3nP	interagovat
elektromagneticky	elektromagneticky	k6eAd1	elektromagneticky
<g/>
.	.	kIx.	.
<g/>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
stavební	stavební	k2eAgFnPc4d1	stavební
částice	částice	k1gFnPc4	částice
hadronů	hadron	k1gInPc2	hadron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
protonu	proton	k1gInSc2	proton
nebo	nebo	k8xC	nebo
mezonů	mezon	k1gInPc2	mezon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jich	on	k3xPp3gMnPc2	on
šest	šest	k4xCc1	šest
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
antikvarkem	antikvark	k1gInSc7	antikvark
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
leptonů	lepton	k1gInPc2	lepton
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
generacích	generace	k1gFnPc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Podléhají	podléhat	k5eAaImIp3nP	podléhat
všem	všecek	k3xTgFnPc3	všecek
známým	známý	k2eAgFnPc3d1	známá
základním	základní	k2eAgFnPc3d1	základní
interakcím	interakce	k1gFnPc3	interakce
(	(	kIx(	(
<g/>
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
-1	-1	k4	-1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
+2	+2	k4	+2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
náboje	náboj	k1gInSc2	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
se	se	k3xPyFc4	se
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
okolností	okolnost	k1gFnPc2	okolnost
nikdy	nikdy	k6eAd1	nikdy
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
složitějších	složitý	k2eAgInPc6d2	složitější
objektech	objekt	k1gInPc6	objekt
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgInSc2	jeden
kvarku	kvark	k1gInSc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
tvrzení	tvrzení	k1gNnSc1	tvrzení
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
kvarků	kvark	k1gInPc2	kvark
jako	jako	k8xS	jako
částic	částice	k1gFnPc2	částice
dosti	dosti	k6eAd1	dosti
specifický	specifický	k2eAgInSc1d1	specifický
význam	význam	k1gInSc1	význam
(	(	kIx(	(
<g/>
blíže	blíže	k1gFnSc1	blíže
viz	vidět	k5eAaImRp2nS	vidět
asymptotická	asymptotický	k2eAgFnSc1d1	asymptotická
volnost	volnost	k1gFnSc1	volnost
kvarků	kvark	k1gInPc2	kvark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Částice	částice	k1gFnSc1	částice
interakcí	interakce	k1gFnPc2	interakce
===	===	k?	===
</s>
</p>
<p>
<s>
Intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
interakce	interakce	k1gFnPc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetickou	elektromagnetický	k2eAgFnSc4d1	elektromagnetická
sílu	síla	k1gFnSc4	síla
přenáší	přenášet	k5eAaImIp3nS	přenášet
foton	foton	k1gInSc1	foton
<g/>
.	.	kIx.	.
</s>
<s>
Slabou	slabý	k2eAgFnSc4d1	slabá
sílu	síla	k1gFnSc4	síla
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
tři	tři	k4xCgFnPc1	tři
částice	částice	k1gFnPc1	částice
bosonů	boson	k1gInPc2	boson
W	W	kA	W
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
W-	W-	k1gFnSc4	W-
a	a	k8xC	a
Z0	Z0	k1gFnSc4	Z0
a	a	k8xC	a
silnou	silný	k2eAgFnSc4d1	silná
sílu	síla	k1gFnSc4	síla
osm	osm	k4xCc4	osm
typů	typ	k1gInPc2	typ
gluonu	gluon	k1gInSc2	gluon
<g/>
.	.	kIx.	.
<g/>
Higgsův	Higgsův	k2eAgInSc1d1	Higgsův
boson	boson	k1gInSc1	boson
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
Higgsova	Higgsův	k2eAgNnSc2d1	Higgsovo
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
ve	v	k7c6	v
standardním	standardní	k2eAgInSc6d1	standardní
modelu	model	k1gInSc6	model
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nenulovou	nulový	k2eNgFnSc4d1	nenulová
hmotnost	hmotnost	k1gFnSc4	hmotnost
částic	částice	k1gFnPc2	částice
W	W	kA	W
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
W-	W-	k1gFnSc2	W-
a	a	k8xC	a
Z	z	k7c2	z
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teorie	teorie	k1gFnSc1	teorie
mimo	mimo	k7c4	mimo
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
experimentální	experimentální	k2eAgInPc1d1	experimentální
důkazy	důkaz	k1gInPc1	důkaz
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
předpoklady	předpoklad	k1gInPc4	předpoklad
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
fyziků	fyzik	k1gMnPc2	fyzik
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc1	model
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nedostatečný	dostatečný	k2eNgInSc4d1	nedostatečný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
neurčitých	určitý	k2eNgInPc2d1	neurčitý
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
základních	základní	k2eAgFnPc2d1	základní
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
více	hodně	k6eAd2	hodně
teoretické	teoretický	k2eAgFnPc1d1	teoretická
úvahy	úvaha	k1gFnPc1	úvaha
jako	jako	k8xS	jako
hierarchický	hierarchický	k2eAgInSc1d1	hierarchický
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
spekulativních	spekulativní	k2eAgFnPc2d1	spekulativní
teorií	teorie	k1gFnPc2	teorie
mimo	mimo	k7c4	mimo
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
odstranit	odstranit	k5eAaPmF	odstranit
tyto	tento	k3xDgInPc4	tento
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velké	velký	k2eAgNnSc1d1	velké
sjednocení	sjednocení	k1gNnSc1	sjednocení
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rozšířením	rozšíření	k1gNnSc7	rozšíření
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
o	o	k7c6	o
sjednocení	sjednocení	k1gNnSc6	sjednocení
elektroslabé	elektroslabý	k2eAgFnPc1d1	elektroslabá
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
síly	síla	k1gFnPc1	síla
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
velké	velký	k2eAgFnSc2d1	velká
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
teorie	teorie	k1gFnSc2	teorie
(	(	kIx(	(
<g/>
GUT	GUT	kA	GUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
síla	síla	k1gFnSc1	síla
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
spontánně	spontánně	k6eAd1	spontánně
rozpadala	rozpadat	k5eAaImAgFnS	rozpadat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
síly	síla	k1gFnPc4	síla
způsobem	způsob	k1gInSc7	způsob
podobným	podobný	k2eAgNnPc3d1	podobné
Higgsově	Higgsův	k2eAgInSc6d1	Higgsův
mechanismu	mechanismus	k1gInSc6	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
předpokladem	předpoklad	k1gInSc7	předpoklad
velkého	velký	k2eAgNnSc2d1	velké
sjednocení	sjednocení	k1gNnSc2	sjednocení
je	být	k5eAaImIp3nS	být
předpovídaná	předpovídaný	k2eAgFnSc1d1	předpovídaná
existence	existence	k1gFnSc1	existence
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
bosonů	boson	k1gMnPc2	boson
<g/>
,	,	kIx,	,
způsobujících	způsobující	k2eAgMnPc2d1	způsobující
rozpad	rozpad	k1gInSc4	rozpad
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
experimentu	experiment	k1gInSc6	experiment
takový	takový	k3xDgInSc1	takový
protonový	protonový	k2eAgInSc1d1	protonový
rozpad	rozpad	k1gInSc1	rozpad
nebyl	být	k5eNaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
GUT	GUT	kA	GUT
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
SU	SU	k?	SU
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
SO	So	kA	So
<g/>
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Supersymetrie	Supersymetrie	k1gFnSc2	Supersymetrie
===	===	k?	===
</s>
</p>
<p>
<s>
Supersymetrie	Supersymetrie	k1gFnPc1	Supersymetrie
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
přidáním	přidání	k1gNnSc7	přidání
další	další	k2eAgFnSc2d1	další
třídy	třída	k1gFnSc2	třída
symetrií	symetrie	k1gFnPc2	symetrie
k	k	k7c3	k
Langrangeanově	Langrangeanův	k2eAgFnSc3d1	Langrangeanův
funkci	funkce	k1gFnSc3	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
symetrie	symetrie	k1gFnPc1	symetrie
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
fermionové	fermionový	k2eAgFnPc1d1	fermionový
částice	částice	k1gFnPc1	částice
za	za	k7c4	za
bosonové	bosonová	k1gFnPc4	bosonová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
symetrie	symetrie	k1gFnSc1	symetrie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
existenci	existence	k1gFnSc4	existence
supersymetrických	supersymetrický	k2eAgFnPc2d1	supersymetrická
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sparticles	sparticles	k1gInSc1	sparticles
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
sleptony	slepton	k1gInPc4	slepton
<g/>
,	,	kIx,	,
skvarky	skvark	k1gInPc4	skvark
<g/>
,	,	kIx,	,
neutralina	neutralina	k1gFnSc1	neutralina
a	a	k8xC	a
chargina	chargina	k1gFnSc1	chargina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
tzv.	tzv.	kA	tzv.
superpartnera	superpartner	k1gMnSc2	superpartner
–	–	k?	–
tj.	tj.	kA	tj.
částici	částice	k1gFnSc6	částice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
spin	spin	k1gInSc1	spin
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgInS	lišit
o	o	k7c4	o
1⁄	1⁄	k?	1⁄
Díky	díky	k7c3	díky
rozpadu	rozpad	k1gInSc3	rozpad
supersymetrie	supersymetrie	k1gFnSc2	supersymetrie
jsou	být	k5eAaImIp3nP	být
sčástice	sčástika	k1gFnSc3	sčástika
mnohem	mnohem	k6eAd1	mnohem
těžší	těžký	k2eAgInSc4d2	těžší
než	než	k8xS	než
jejich	jejich	k3xOp3gMnPc1	jejich
běžní	běžný	k2eAgMnPc1d1	běžný
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
tak	tak	k6eAd1	tak
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgInPc1d1	současný
urychlovače	urychlovač	k1gInPc1	urychlovač
částic	částice	k1gFnPc2	částice
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
výkonné	výkonný	k2eAgInPc1d1	výkonný
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
je	on	k3xPp3gInPc4	on
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
někteří	některý	k3yIgMnPc1	některý
fyzikové	fyzik	k1gMnPc1	fyzik
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sčástice	sčástika	k1gFnSc3	sčástika
zpozorují	zpozorovat	k5eAaPmIp3nP	zpozorovat
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
hadronovém	hadronový	k2eAgInSc6d1	hadronový
urychlovači	urychlovač	k1gInSc6	urychlovač
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teorie	teorie	k1gFnSc1	teorie
strun	struna	k1gFnPc2	struna
===	===	k?	===
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
strun	struna	k1gFnPc2	struna
představuje	představovat	k5eAaImIp3nS	představovat
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
lze	lze	k6eAd1	lze
všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
tvořící	tvořící	k2eAgFnSc4d1	tvořící
hmotu	hmota	k1gFnSc4	hmota
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xS	jako
struny	struna	k1gFnSc2	struna
(	(	kIx(	(
<g/>
velikosti	velikost	k1gFnSc2	velikost
cca	cca	kA	cca
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
struny	struna	k1gFnPc1	struna
vibrují	vibrovat	k5eAaImIp3nP	vibrovat
různými	různý	k2eAgFnPc7d1	různá
frekvencemi	frekvence	k1gFnPc7	frekvence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
určuje	určovat	k5eAaImIp3nS	určovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc4d1	barevný
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
spin	spin	k1gInSc4	spin
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
otevřené	otevřený	k2eAgFnPc1d1	otevřená
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
úsečka	úsečka	k1gFnSc1	úsečka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
ve	v	k7c6	v
smyčce	smyčka	k1gFnSc6	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
i	i	k9	i
strunný	strunný	k2eAgMnSc1d1	strunný
kandidát	kandidát	k1gMnSc1	kandidát
pro	pro	k7c4	pro
graviton	graviton	k1gInSc4	graviton
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dosud	dosud	k6eAd1	dosud
hypotetické	hypotetický	k2eAgNnSc1d1	hypotetické
kvantum	kvantum	k1gNnSc1	kvantum
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
teorie	teorie	k1gFnSc1	teorie
superstrun	superstruna	k1gFnPc2	superstruna
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označují	označovat	k5eAaImIp3nP	označovat
strunné	strunný	k2eAgFnPc1d1	strunná
teorie	teorie	k1gFnPc1	teorie
obsahující	obsahující	k2eAgFnSc4d1	obsahující
supersymetrii	supersymetrie	k1gFnSc4	supersymetrie
a	a	k8xC	a
předpovídající	předpovídající	k2eAgInPc4d1	předpovídající
supersymetrické	supersymetrický	k2eAgInPc4d1	supersymetrický
partnery	partner	k1gMnPc7	partner
známých	známý	k2eAgFnPc2d1	známá
částic	částice	k1gFnPc2	částice
hmoty	hmota	k1gFnSc2	hmota
i	i	k8xC	i
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
varianty	varianta	k1gFnPc1	varianta
teorie	teorie	k1gFnSc2	teorie
superstrun	superstruna	k1gFnPc2	superstruna
i	i	k8xC	i
teorie	teorie	k1gFnSc2	teorie
supergravitace	supergravitace	k1gFnSc2	supergravitace
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
limitní	limitní	k2eAgInPc4d1	limitní
případy	případ	k1gInPc4	případ
obecnější	obecní	k2eAgFnSc2d2	obecní
M-teorie	Meorie	k1gFnSc2	M-teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
strunnými	strunný	k2eAgInPc7d1	strunný
i	i	k8xC	i
vícerozměrnými	vícerozměrný	k2eAgInPc7d1	vícerozměrný
objekty	objekt	k1gInPc7	objekt
(	(	kIx(	(
<g/>
p-bránami	prána	k1gFnPc7	p-brána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
1	[number]	k4	1
<g/>
-bránou	rána	k1gFnSc7	-brána
je	být	k5eAaImIp3nS	být
struna	struna	k1gFnSc1	struna
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-bránou	rána	k1gFnSc7	-brána
dvourozměrný	dvourozměrný	k2eAgInSc1d1	dvourozměrný
objekt	objekt	k1gInSc1	objekt
–	–	k?	–
membrána	membrána	k1gFnSc1	membrána
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
existujícími	existující	k2eAgMnPc7d1	existující
v	v	k7c6	v
11	[number]	k4	11
<g/>
-rozměrném	ozměrný	k2eAgInSc6d1	-rozměrný
časoprostoru	časoprostor	k1gInSc6	časoprostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Technicolor	Technicolor	k1gInSc4	Technicolor
===	===	k?	===
</s>
</p>
<p>
<s>
Technicolor	Technicolor	k1gInSc1	Technicolor
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
upravit	upravit	k5eAaPmF	upravit
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
přidáním	přidání	k1gNnSc7	přidání
nové	nový	k2eAgFnSc2d1	nová
interakce	interakce	k1gFnSc2	interakce
(	(	kIx(	(
<g/>
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
podobné	podobný	k2eAgFnSc2d1	podobná
QCD	QCD	kA	QCD
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
přidání	přidání	k1gNnSc4	přidání
nové	nový	k2eAgFnSc2d1	nová
teorie	teorie	k1gFnSc2	teorie
tzv.	tzv.	kA	tzv.
technikvarků	technikvarek	k1gMnPc2	technikvarek
interagujících	interagující	k2eAgMnPc2d1	interagující
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
technigluonů	technigluon	k1gInPc2	technigluon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Higgsův	Higgsův	k2eAgInSc1d1	Higgsův
boson	boson	k1gInSc1	boson
není	být	k5eNaImIp3nS	být
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vázaný	vázaný	k2eAgInSc1d1	vázaný
stav	stav	k1gInSc1	stav
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Navrhované	navrhovaný	k2eAgFnPc1d1	navrhovaná
částice	částice	k1gFnPc1	částice
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
intermediální	intermediální	k2eAgInSc1d1	intermediální
boson	boson	k1gInSc1	boson
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
graviton	graviton	k1gInSc1	graviton
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
jsou	být	k5eAaImIp3nP	být
předpovězené	předpovězený	k2eAgFnPc1d1	předpovězená
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
pro	pro	k7c4	pro
graviton	graviton	k1gInSc4	graviton
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
experimentální	experimentální	k2eAgInPc4d1	experimentální
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
ani	ani	k8xC	ani
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitony	graviton	k1gInPc4	graviton
bude	být	k5eAaImBp3nS	být
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
teorie	teorie	k1gFnSc1	teorie
superstrun	superstrun	k1gMnSc1	superstrun
<g/>
/	/	kIx~	/
<g/>
M-teorie	Meorie	k1gFnSc1	M-teorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Supergravitační	Supergravitační	k2eAgFnSc1d1	Supergravitační
unitární	unitární	k2eAgFnSc1d1	unitární
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
částice	částice	k1gFnSc1	částice
s	s	k7c7	s
názvem	název	k1gInSc7	název
gravitino	gravitin	k2eAgNnSc1d1	gravitino
<g/>
.	.	kIx.	.
</s>
<s>
Gravitina	Gravitina	k1gFnSc1	Gravitina
jsou	být	k5eAaImIp3nP	být
kvanta	kvantum	k1gNnPc1	kvantum
kalibračního	kalibrační	k2eAgNnSc2d1	kalibrační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
superpartnerem	superpartner	k1gMnSc7	superpartner
gravitonu	graviton	k1gInSc2	graviton
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
spin	spin	k1gInSc1	spin
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
fotina	fotina	k1gFnSc1	fotina
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
superpartneři	superpartner	k1gMnPc1	superpartner
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
navrhované	navrhovaný	k2eAgFnSc2d1	navrhovaná
teoreticky	teoreticky	k6eAd1	teoreticky
lákavé	lákavý	k2eAgFnSc2d1	lákavá
nové	nový	k2eAgFnSc2d1	nová
symetrie	symetrie	k1gFnSc2	symetrie
–	–	k?	–
supersymetrie	supersymetrie	k1gFnSc2	supersymetrie
–	–	k?	–
plyne	plynout	k5eAaImIp3nS	plynout
existence	existence	k1gFnSc1	existence
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
částic-superpartnerů	částicuperpartner	k1gMnPc2	částic-superpartner
současných	současný	k2eAgFnPc2d1	současná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
podle	podle	k7c2	podle
vzorů	vzor	k1gInPc2	vzor
např.	např.	kA	např.
skvarky	skvarka	k1gFnSc2	skvarka
<g/>
,	,	kIx,	,
sleptony	slepton	k1gInPc1	slepton
<g/>
,	,	kIx,	,
sneutrina	sneutrin	k2eAgFnSc1d1	sneutrin
nebo	nebo	k8xC	nebo
nových	nový	k2eAgFnPc2d1	nová
částic	částice	k1gFnPc2	částice
bez	bez	k7c2	bez
partnera	partner	k1gMnSc2	partner
jako	jako	k8xC	jako
neutralino	utralin	k2eNgNnSc1d1	neutralino
apod.	apod.	kA	apod.
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
experimentálně	experimentálně	k6eAd1	experimentálně
nalezená	nalezený	k2eAgFnSc1d1	nalezená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
urychlovači	urychlovač	k1gInSc6	urychlovač
LHC	LHC	kA	LHC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bosony	Boson	k1gInPc1	Boson
a	a	k8xC	a
fermiony	fermion	k1gInPc1	fermion
==	==	k?	==
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
ve	v	k7c6	v
vícečásticových	vícečásticův	k2eAgInPc6d1	vícečásticův
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
částice	částice	k1gFnSc1	částice
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
bosony	boson	k1gInPc4	boson
a	a	k8xC	a
fermiony	fermion	k1gInPc4	fermion
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
ve	v	k7c6	v
statistickém	statistický	k2eAgNnSc6d1	statistické
chování	chování	k1gNnSc6	chování
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
chování	chování	k1gNnSc1	chování
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
částic	částice	k1gFnPc2	částice
jako	jako	k8xC	jako
souboru	soubor	k1gInSc2	soubor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anti	ant	k1gMnPc5	ant
<g/>
)	)	kIx)	)
<g/>
symetričnost	symetričnost	k1gFnSc1	symetričnost
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
více	hodně	k6eAd2	hodně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
splňují	splňovat	k5eAaImIp3nP	splňovat
Pauliho	Pauli	k1gMnSc4	Pauli
vylučovací	vylučovací	k2eAgInSc1d1	vylučovací
princip	princip	k1gInSc1	princip
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
kreační	kreační	k2eAgInPc1d1	kreační
operátory	operátor	k1gInPc1	operátor
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
(	(	kIx(	(
<g/>
anti	anti	k6eAd1	anti
<g/>
)	)	kIx)	)
<g/>
komutačním	komutační	k2eAgFnPc3d1	komutační
relacím	relace	k1gFnPc3	relace
<g/>
,	,	kIx,	,
a	a	k8xC	a
spinu	spina	k1gFnSc4	spina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
spolu	spolu	k6eAd1	spolu
vzájemně	vzájemně	k6eAd1	vzájemně
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fermiony	fermion	k1gInPc1	fermion
mají	mít	k5eAaImIp3nP	mít
poločíselný	poločíselný	k2eAgInSc4d1	poločíselný
spin	spin	k1gInSc4	spin
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
±	±	k?	±
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
±	±	k?	±
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
atd	atd	kA	atd
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antisymetrickou	antisymetrický	k2eAgFnSc4d1	antisymetrická
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
funkci	funkce	k1gFnSc4	funkce
více	hodně	k6eAd2	hodně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
splňují	splňovat	k5eAaImIp3nP	splňovat
Pauliho	Pauli	k1gMnSc4	Pauli
vylučovací	vylučovací	k2eAgInSc1d1	vylučovací
princip	princip	k1gInSc1	princip
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
kreační	kreační	k2eAgInPc1d1	kreační
operátory	operátor	k1gInPc1	operátor
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
antikomutačním	antikomutační	k2eAgFnPc3d1	antikomutační
relacím	relace	k1gFnPc3	relace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
určuje	určovat	k5eAaImIp3nS	určovat
Fermiho-Diracova	Fermiho-Diracův	k2eAgFnSc1d1	Fermiho-Diracův
statistika	statistika	k1gFnSc1	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
zástupcem	zástupce	k1gMnSc7	zástupce
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
leptony	lepton	k1gInPc1	lepton
<g/>
,	,	kIx,	,
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
baryony	baryon	k1gInPc1	baryon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
proton	proton	k1gInSc1	proton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Bosony	Bosona	k1gFnPc1	Bosona
mají	mít	k5eAaImIp3nP	mít
celočíselný	celočíselný	k2eAgInSc4d1	celočíselný
spin	spin	k1gInSc4	spin
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
±	±	k?	±
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
±	±	k?	±
<g/>
2	[number]	k4	2
atd	atd	kA	atd
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
funkci	funkce	k1gFnSc4	funkce
více	hodně	k6eAd2	hodně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
nesplňují	splňovat	k5eNaImIp3nP	splňovat
Pauliho	Pauli	k1gMnSc4	Pauli
vylučovací	vylučovací	k2eAgInSc1d1	vylučovací
princip	princip	k1gInSc1	princip
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
kreační	kreační	k2eAgInPc1d1	kreační
operátory	operátor	k1gInPc1	operátor
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
komutačním	komutační	k2eAgFnPc3d1	komutační
relacím	relace	k1gFnPc3	relace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
určuje	určovat	k5eAaImIp3nS	určovat
Boseho-Einsteinova	Boseho-Einsteinův	k2eAgFnSc1d1	Boseho-Einsteinova
statistika	statistika	k1gFnSc1	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
zástupcem	zástupce	k1gMnSc7	zástupce
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mezony	mezon	k1gInPc1	mezon
a	a	k8xC	a
intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
má	mít	k5eAaImIp3nS	mít
specifické	specifický	k2eAgInPc4d1	specifický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
elementární	elementární	k2eAgInSc1d1	elementární
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
základní	základní	k2eAgFnSc1d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
spojují	spojovat	k5eAaImIp3nP	spojovat
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
–	–	k?	–
dále	daleko	k6eAd2	daleko
nedělitelný	dělitelný	k2eNgMnSc1d1	nedělitelný
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
poznání	poznání	k1gNnSc2	poznání
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
považované	považovaný	k2eAgFnPc1d1	považovaná
za	za	k7c4	za
základní	základní	k2eAgFnPc4d1	základní
mají	mít	k5eAaImIp3nP	mít
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
ještě	ještě	k6eAd1	ještě
základnějších	základní	k2eAgFnPc2d2	základnější
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
proton	proton	k1gInSc1	proton
nebo	nebo	k8xC	nebo
neutron	neutron	k1gInSc1	neutron
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
považované	považovaný	k2eAgInPc1d1	považovaný
za	za	k7c4	za
nedělitelné	dělitelný	k2eNgNnSc4d1	nedělitelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
doslovném	doslovný	k2eAgInSc6d1	doslovný
výkladu	výklad	k1gInSc6	výklad
tedy	tedy	k8xC	tedy
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgInPc4d1	elementární
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
aktuálním	aktuální	k2eAgInSc6d1	aktuální
stavu	stav	k1gInSc6	stav
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takové	takový	k3xDgNnSc1	takový
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
krajně	krajně	k6eAd1	krajně
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
drží	držet	k5eAaImIp3nS	držet
nejen	nejen	k6eAd1	nejen
současný	současný	k2eAgInSc1d1	současný
obsah	obsah	k1gInSc1	obsah
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
částice	částice	k1gFnSc1	částice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
poněkud	poněkud	k6eAd1	poněkud
historický	historický	k2eAgInSc1d1	historický
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
významu	význam	k1gInSc3	význam
elementární	elementární	k2eAgFnSc4d1	elementární
před	před	k7c7	před
přijetím	přijetí	k1gNnSc7	přijetí
teorie	teorie	k1gFnSc2	teorie
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
použití	použití	k1gNnSc1	použití
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
chápat	chápat	k5eAaImF	chápat
jen	jen	k9	jen
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
tvrzení	tvrzení	k1gNnSc1	tvrzení
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
atom	atom	k1gInSc4	atom
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
nedělitelný	dělitelný	k2eNgMnSc1d1	nedělitelný
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
dále	daleko	k6eAd2	daleko
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
spekulativní	spekulativní	k2eAgMnPc1d1	spekulativní
"	"	kIx"	"
<g/>
budoucí	budoucí	k2eAgFnPc4d1	budoucí
<g/>
"	"	kIx"	"
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
gravitonu	graviton	k1gInSc2	graviton
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
superpartnerů	superpartner	k1gMnPc2	superpartner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úplný	úplný	k2eAgInSc1d1	úplný
přehled	přehled	k1gInSc1	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
přehled	přehled	k1gInSc1	přehled
všech	všecek	k3xTgFnPc2	všecek
objevených	objevený	k2eAgFnPc2d1	objevená
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
včetně	včetně	k7c2	včetně
hadronů	hadron	k1gInPc2	hadron
(	(	kIx(	(
<g/>
i	i	k9	i
rezonancí	rezonance	k1gFnSc7	rezonance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
i	i	k9	i
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
hledání	hledání	k1gNnSc2	hledání
částic	částice	k1gFnPc2	částice
hypotetických	hypotetický	k2eAgFnPc2d1	hypotetická
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
a	a	k8xC	a
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
sdružení	sdružení	k1gNnSc1	sdružení
Particle	Particle	k1gFnSc2	Particle
Data	datum	k1gNnSc2	datum
Group	Group	k1gMnSc1	Group
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
přehledu	přehled	k1gInSc6	přehled
The	The	k1gMnSc1	The
Review	Review	k1gMnSc1	Review
of	of	k?	of
Particle	Particl	k1gMnSc4	Particl
Physics	Physics	k1gInSc1	Physics
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Aldebaran	Aldebaran	k1gInSc1	Aldebaran
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Záznam	záznam	k1gInSc1	záznam
přednášky	přednáška	k1gFnSc2	přednáška
<g/>
:	:	kIx,	:
Doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Petr	Petr	k1gMnSc1	Petr
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
<g/>
,	,	kIx,	,
CSc	CSc	kA	CSc
<g/>
,	,	kIx,	,
FEL	FEL	kA	FEL
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
Astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
03	[number]	k4	03
</s>
</p>
