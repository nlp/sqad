<p>
<s>
Drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
vzácné	vzácný	k2eAgInPc4d1	vzácný
kovové	kovový	k2eAgInPc4d1	kovový
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc1	jejich
slitiny	slitina	k1gFnPc1	slitina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
jako	jako	k8xS	jako
platidla	platidlo	k1gNnPc4	platidlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
počítá	počítat	k5eAaImIp3nS	počítat
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
platina	platina	k1gFnSc1	platina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
platinové	platinový	k2eAgFnSc2d1	platinová
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
palladium	palladium	k1gNnSc1	palladium
<g/>
,	,	kIx,	,
rhodium	rhodium	k1gNnSc1	rhodium
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
ve	v	k7c6	v
zlatnictví	zlatnictví	k1gNnSc6	zlatnictví
jako	jako	k8xC	jako
přísady	přísada	k1gFnSc2	přísada
a	a	k8xC	a
k	k	k7c3	k
pokovování	pokovování	k1gNnSc3	pokovování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
ceny	cena	k1gFnPc1	cena
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
vzácného	vzácný	k2eAgInSc2d1	vzácný
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
se	se	k3xPyFc4	se
drahé	drahý	k2eAgInPc1d1	drahý
kovy	kov	k1gInPc1	kov
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
také	také	k9	také
vysokou	vysoký	k2eAgFnSc7d1	vysoká
specifickou	specifický	k2eAgFnSc7d1	specifická
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc7d1	malá
chemickou	chemický	k2eAgFnSc7d1	chemická
reaktibilitou	reaktibilita	k1gFnSc7	reaktibilita
(	(	kIx(	(
<g/>
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krásným	krásný	k2eAgInSc7d1	krásný
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc7d1	dobrá
zpracovatelností	zpracovatelnost	k1gFnSc7	zpracovatelnost
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
vodivostí	vodivost	k1gFnSc7	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
jako	jako	k9	jako
ozdoby	ozdoba	k1gFnPc1	ozdoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
šperků	šperk	k1gInPc2	šperk
nebo	nebo	k8xC	nebo
posvátných	posvátný	k2eAgInPc2d1	posvátný
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
i	i	k9	i
jako	jako	k9	jako
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
razily	razit	k5eAaImAgFnP	razit
mince	mince	k1gFnPc4	mince
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
vázala	vázat	k5eAaImAgFnS	vázat
hodnota	hodnota	k1gFnSc1	hodnota
některých	některý	k3yIgFnPc2	některý
měn	měna	k1gFnPc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnSc1	mince
z	z	k7c2	z
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
mívaly	mívat	k5eAaImAgInP	mívat
přesně	přesně	k6eAd1	přesně
stanovenu	stanoven	k2eAgFnSc4d1	stanovena
váhu	váha	k1gFnSc4	váha
a	a	k8xC	a
ryzost	ryzost	k1gFnSc4	ryzost
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ingoty	ingot	k1gInPc1	ingot
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
s	s	k7c7	s
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
stoupajících	stoupající	k2eAgFnPc6d1	stoupající
cenách	cena	k1gFnPc6	cena
zejména	zejména	k9	zejména
zlata	zlato	k1gNnSc2	zlato
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
hledají	hledat	k5eAaImIp3nP	hledat
nová	nový	k2eAgNnPc4d1	nové
naleziště	naleziště	k1gNnPc4	naleziště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nové	nový	k2eAgFnPc1d1	nová
technologie	technologie	k1gFnPc1	technologie
včetně	včetně	k7c2	včetně
recyklování	recyklování	k1gNnSc2	recyklování
odpadů	odpad	k1gInPc2	odpad
a	a	k8xC	a
starých	starý	k2eAgInPc2d1	starý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
zlata	zlato	k1gNnSc2	zlato
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
zpětinásobila	zpětinásobit	k5eAaPmAgFnS	zpětinásobit
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2	[number]	k4	2
500	[number]	k4	500
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
stříbra	stříbro	k1gNnSc2	stříbro
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
producenty	producent	k1gMnPc4	producent
zlata	zlato	k1gNnSc2	zlato
patřila	patřit	k5eAaImAgFnS	patřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
producenty	producent	k1gMnPc4	producent
stříbra	stříbro	k1gNnSc2	stříbro
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
výborné	výborná	k1gFnSc3	výborná
elektrické	elektrický	k2eAgFnSc2d1	elektrická
vodivosti	vodivost	k1gFnSc2	vodivost
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc2	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
stříbra	stříbro	k1gNnSc2	stříbro
spotřebovával	spotřebovávat	k5eAaImAgInS	spotřebovávat
fotografický	fotografický	k2eAgInSc1d1	fotografický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zlata	zlato	k1gNnSc2	zlato
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zlaté	zlatý	k2eAgFnPc4d1	zlatá
rezervy	rezerva	k1gFnPc4	rezerva
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
jako	jako	k9	jako
soukromé	soukromý	k2eAgFnPc4d1	soukromá
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Světový	světový	k2eAgInSc1d1	světový
obchod	obchod	k1gInSc1	obchod
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
jako	jako	k9	jako
s	s	k7c7	s
komoditou	komodita	k1gFnSc7	komodita
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
burzách	burza	k1gFnPc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
burza	burza	k1gFnSc1	burza
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
průběžně	průběžně	k6eAd1	průběžně
výsledky	výsledek	k1gInPc4	výsledek
obchodování	obchodování	k1gNnSc2	obchodování
tzv.	tzv.	kA	tzv.
London	London	k1gMnSc1	London
FIX	fix	k1gInSc1	fix
a	a	k8xC	a
London	London	k1gMnSc1	London
SPOT	spot	k1gInSc1	spot
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
London	London	k1gMnSc1	London
FIX	fix	k1gInSc1	fix
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
většinou	většinou	k6eAd1	většinou
dopolední	dopolední	k2eAgNnSc1d1	dopolední
a	a	k8xC	a
odpolední	odpolední	k2eAgNnSc1d1	odpolední
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
London	London	k1gMnSc1	London
FIX	fix	k1gInSc4	fix
AM	AM	kA	AM
a	a	k8xC	a
London	London	k1gMnSc1	London
FIX	fix	k1gInSc4	fix
PM	PM	kA	PM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cena	cena	k1gFnSc1	cena
London	London	k1gMnSc1	London
SPOT	spot	k1gInSc4	spot
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
online	onlinout	k5eAaPmIp3nS	onlinout
cena	cena	k1gFnSc1	cena
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Forex	Forex	k1gInSc4	Forex
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
základně	základně	k6eAd1	základně
udávaná	udávaný	k2eAgFnSc1d1	udávaná
v	v	k7c6	v
dolarech	dolar	k1gInPc6	dolar
za	za	k7c4	za
trojskou	trojský	k2eAgFnSc4d1	Trojská
unci	unce	k1gFnSc4	unce
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
oz	oz	k?	oz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
drahého	drahý	k2eAgInSc2d1	drahý
kovu	kov	k1gInSc2	kov
za	za	k7c4	za
trojskou	trojský	k2eAgFnSc4d1	Trojská
unci	unce	k1gFnSc4	unce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
přepočítat	přepočítat	k5eAaPmF	přepočítat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
gram	gram	k1gInSc4	gram
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
korunách	koruna	k1gFnPc6	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
grafy	graf	k1gInPc1	graf
za	za	k7c4	za
uplynulé	uplynulý	k2eAgInPc4d1	uplynulý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
s	s	k7c7	s
přepočtem	přepočet	k1gInSc7	přepočet
za	za	k7c4	za
gram	gram	k1gInSc4	gram
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
korunách	koruna	k1gFnPc6	koruna
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
pro	pro	k7c4	pro
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
,	,	kIx,	,
platinu	platina	k1gFnSc4	platina
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc4	palladium
<g/>
,	,	kIx,	,
rhodium	rhodium	k1gNnSc4	rhodium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Täubl	Täubl	k1gFnSc1	Täubl
<g/>
,	,	kIx,	,
Zlatnictví	zlatnictví	k1gNnSc1	zlatnictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zlatnictví	zlatnictví	k1gNnSc1	zlatnictví
</s>
</p>
<p>
<s>
Mince	mince	k1gFnSc1	mince
</s>
</p>
<p>
<s>
Zlato	zlato	k1gNnSc1	zlato
</s>
</p>
<p>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
</s>
</p>
<p>
<s>
Platina	platina	k1gFnSc1	platina
</s>
</p>
<p>
<s>
Palladium	palladium	k1gNnSc1	palladium
</s>
</p>
<p>
<s>
Rhodium	rhodium	k1gNnSc1	rhodium
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
grafy	graf	k1gInPc4	graf
www.KITCO.cz	www.KITCO.cza	k1gFnPc2	www.KITCO.cza
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ceny	cena	k1gFnPc1	cena
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
thebulliondesk	thebulliondesk	k1gInSc4	thebulliondesk
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Měření	měření	k1gNnPc1	měření
a	a	k8xC	a
konverze	konverze	k1gFnPc1	konverze
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
International	International	k1gFnSc1	International
Precious	Precious	k1gMnSc1	Precious
Metals	Metals	k1gInSc1	Metals
Institute	institut	k1gInSc5	institut
</s>
</p>
