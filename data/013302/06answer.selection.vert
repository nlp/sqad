<s>
Drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
vzácné	vzácný	k2eAgInPc4d1	vzácný
kovové	kovový	k2eAgInPc4d1	kovový
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc1	jejich
slitiny	slitina	k1gFnPc1	slitina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
jako	jako	k8xS	jako
platidla	platidlo	k1gNnPc4	platidlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
