<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Vietnamu	Vietnam	k1gInSc2	Vietnam
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
uprostřed	uprostřed	k7c2	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
jednotu	jednota	k1gFnSc4	jednota
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
budování	budování	k1gNnSc2	budování
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
cípů	cíp	k1gInPc2	cíp
hvězdy	hvězda	k1gFnSc2	hvězda
představuje	představovat	k5eAaImIp3nS	představovat
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tříd	třída	k1gFnPc2	třída
ve	v	k7c6	v
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
,	,	kIx,	,
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
hvězdy	hvězda	k1gFnSc2	hvězda
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
vlajky	vlajka	k1gFnSc2	vlajka
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zvolená	zvolený	k2eAgFnSc1d1	zvolená
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
barvy	barva	k1gFnPc4	barva
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
listu	list	k1gInSc2	list
znamená	znamenat	k5eAaImIp3nS	znamenat
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Francouzům	Francouz	k1gMnPc3	Francouz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
už	už	k6eAd1	už
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
vepsaná	vepsaný	k2eAgFnSc1d1	vepsaná
do	do	k7c2	do
kružnice	kružnice	k1gFnSc2	kružnice
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
rovném	rovný	k2eAgInSc6d1	rovný
pětině	pětina	k1gFnSc3	pětina
délky	délka	k1gFnSc2	délka
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
plnější	plný	k2eAgInSc1d2	plnější
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Vietnamu	Vietnam	k1gInSc2	Vietnam
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Vietnamu	Vietnam	k1gInSc2	Vietnam
</s>
</p>
<p>
<s>
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Vietnamu	Vietnam	k1gInSc2	Vietnam
</s>
</p>
