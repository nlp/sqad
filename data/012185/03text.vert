<p>
<s>
Pot	pot	k1gInSc1	pot
(	(	kIx(	(
<g/>
odborně	odborně	k6eAd1	odborně
sudor	sudora	k1gFnPc2	sudora
<g/>
,	,	kIx,	,
hidros	hidrosa	k1gFnPc2	hidrosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
výměšek	výměšek	k1gInSc4	výměšek
apokrinních	apokrinní	k2eAgFnPc2d1	apokrinní
a	a	k8xC	a
ekrinních	ekrinní	k2eAgFnPc2d1	ekrinní
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
žláz	žláza	k1gFnPc2	žláza
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
odlišný	odlišný	k2eAgInSc1d1	odlišný
typ	typ	k1gInSc1	typ
potu	pot	k1gInSc2	pot
s	s	k7c7	s
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
funkcí	funkce	k1gFnSc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
potu	pot	k1gInSc2	pot
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pocení	pocení	k1gNnSc1	pocení
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
hidróza	hidróza	k1gFnSc1	hidróza
(	(	kIx(	(
<g/>
hidrosis	hidrosis	k1gInSc1	hidrosis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diaforéza	diaforéza	k1gFnSc1	diaforéza
<g/>
,	,	kIx,	,
sudatio	sudatio	k1gNnSc1	sudatio
<g/>
,	,	kIx,	,
perspiratio	perspiratio	k6eAd1	perspiratio
nebo	nebo	k8xC	nebo
transpiratio	transpiratio	k6eAd1	transpiratio
<g/>
.	.	kIx.	.
</s>
<s>
Ekrinní	Ekrinný	k2eAgMnPc1d1	Ekrinný
žlázy	žláza	k1gFnPc4	žláza
fungují	fungovat	k5eAaImIp3nP	fungovat
už	už	k6eAd1	už
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
sekret	sekret	k1gInSc1	sekret
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
povrch	povrch	k1gInSc4	povrch
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Pot	pot	k1gInSc1	pot
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
vody	voda	k1gFnSc2	voda
také	také	k6eAd1	také
různé	různý	k2eAgNnSc1d1	různé
množství	množství	k1gNnSc1	množství
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
sodík	sodík	k1gInSc1	sodík
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgNnSc1d1	přesné
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnSc2	ztráta
solutů	solut	k1gInPc2	solut
potem	pot	k1gInSc7	pot
znamenají	znamenat	k5eAaImIp3nP	znamenat
nutnost	nutnost	k1gFnSc4	nutnost
doplňování	doplňování	k1gNnSc1	doplňování
nejen	nejen	k6eAd1	nejen
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
silném	silný	k2eAgNnSc6d1	silné
pocení	pocení	k1gNnSc6	pocení
při	při	k7c6	při
těžké	těžký	k2eAgFnSc6d1	těžká
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
sportovních	sportovní	k2eAgInPc6d1	sportovní
výkonech	výkon	k1gInPc6	výkon
nebo	nebo	k8xC	nebo
při	při	k7c6	při
horečce	horečka	k1gFnSc6	horečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apokrinní	Apokrinný	k2eAgMnPc1d1	Apokrinný
žlázy	žláza	k1gFnSc2	žláza
se	se	k3xPyFc4	se
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
nacházejí	nacházet	k5eAaImIp3nP	nacházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
podpaží	podpaží	k1gNnSc6	podpaží
<g/>
,	,	kIx,	,
v	v	k7c6	v
tříslech	tříslo	k1gNnPc6	tříslo
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
bradavek	bradavka	k1gFnPc2	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
až	až	k9	až
během	během	k7c2	během
puberty	puberta	k1gFnSc2	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Pot	pot	k1gInSc1	pot
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
žláz	žláza	k1gFnPc2	žláza
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
a	a	k8xC	a
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
pro	pro	k7c4	pro
bakterie	bakterie	k1gFnPc4	bakterie
žijící	žijící	k2eAgFnPc4d1	žijící
přirozeně	přirozeně	k6eAd1	přirozeně
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Rozkladem	rozklad	k1gInSc7	rozklad
těchto	tento	k3xDgFnPc2	tento
složek	složka	k1gFnPc2	složka
vznikají	vznikat	k5eAaImIp3nP	vznikat
aromatické	aromatický	k2eAgFnPc1d1	aromatická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
tělesného	tělesný	k2eAgInSc2d1	tělesný
pachu	pach	k1gInSc2	pach
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xC	jako
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
zápach	zápach	k1gInSc1	zápach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
potu	pot	k1gInSc2	pot
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pot	pot	k1gInSc4	pot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Potní	potní	k2eAgFnPc1d1	potní
žlázy	žláza	k1gFnPc1	žláza
</s>
</p>
<p>
<s>
Nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
pocení	pocení	k1gNnSc1	pocení
</s>
</p>
<p>
<s>
Antiperspirant	Antiperspirant	k1gMnSc1	Antiperspirant
</s>
</p>
<p>
<s>
Vylučování	vylučování	k1gNnSc1	vylučování
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pot	pot	k1gInSc1	pot
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
