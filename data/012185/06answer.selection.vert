<s>
Ekrinní	Ekrinný	k2eAgMnPc1d1	Ekrinný
žlázy	žláza	k1gFnSc2	žláza
fungují	fungovat	k5eAaImIp3nP	fungovat
už	už	k6eAd1	už
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
sekret	sekret	k1gInSc1	sekret
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
povrch	povrch	k1gInSc4	povrch
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
