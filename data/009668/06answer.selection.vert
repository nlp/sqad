<s>
Počátky	počátek	k1gInPc1	počátek
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
praxe	praxe	k1gFnSc2	praxe
křtít	křtít	k5eAaImF	křtít
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
Ježíše	Ježíš	k1gMnPc4	Ježíš
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
