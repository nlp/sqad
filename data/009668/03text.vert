<p>
<s>
Křest	křest	k1gInSc1	křest
(	(	kIx(	(
<g/>
asi	asi	k9	asi
ze	z	k7c2	z
starohornoněmeckého	starohornoněmecký	k2eAgInSc2d1	starohornoněmecký
Krist	Krista	k1gFnPc2	Krista
–	–	k?	–
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
β	β	k?	β
[	[	kIx(	[
<g/>
báptisma	báptisma	k1gFnSc1	báptisma
<g/>
]	]	kIx)	]
ponoření	ponoření	k1gNnSc1	ponoření
<	<	kIx(	<
β	β	k?	β
[	[	kIx(	[
<g/>
baptízein	baptízein	k1gMnSc1	baptízein
<g/>
]	]	kIx)	]
nořit	nořit	k5eAaImF	nořit
<g/>
,	,	kIx,	,
ponořovat	ponořovat	k5eAaImF	ponořovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obřad	obřad	k1gInSc4	obřad
očištění	očištění	k1gNnSc2	očištění
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
praktikovaný	praktikovaný	k2eAgMnSc1d1	praktikovaný
většinou	většina	k1gFnSc7	většina
skrze	skrze	k?	skrze
ponoření	ponoření	k1gNnSc6	ponoření
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
skrze	skrze	k?	skrze
omytí	omytí	k1gNnPc1	omytí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
východních	východní	k2eAgNnPc6d1	východní
náboženstvích	náboženství	k1gNnPc6	náboženství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sikhismu	sikhismus	k1gInSc2	sikhismus
nebo	nebo	k8xC	nebo
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
historických	historický	k2eAgFnPc6d1	historická
skupinách	skupina	k1gFnPc6	skupina
židovství	židovství	k1gNnSc2	židovství
a	a	k8xC	a
především	především	k9	především
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
marketingový	marketingový	k2eAgInSc4d1	marketingový
akt	akt	k1gInSc4	akt
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
nesouvisející	související	k2eNgNnPc1d1	nesouvisející
<g/>
,	,	kIx,	,
vztahující	vztahující	k2eAgNnPc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
neživým	živý	k2eNgInPc3d1	neživý
předmětům	předmět	k1gInPc3	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
bylo	být	k5eAaImAgNnS	být
ponořování	ponořování	k1gNnSc1	ponořování
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
užíváno	užívat	k5eAaImNgNnS	užívat
při	při	k7c6	při
rituálním	rituální	k2eAgNnSc6d1	rituální
omývání	omývání	k1gNnSc6	omývání
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
proto-křtitelnice	protořtitelnice	k1gFnSc1	proto-křtitelnice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
prostředí	prostředí	k1gNnSc6	prostředí
nazývána	nazývat	k5eAaImNgFnS	nazývat
mikve	mikvat	k5eAaPmIp3nS	mikvat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vidí	vidět	k5eAaImIp3nS	vidět
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
praxe	praxe	k1gFnSc1	praxe
křtů	křest	k1gInPc2	křest
u	u	k7c2	u
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
židovského	židovský	k2eAgMnSc2d1	židovský
kněze	kněz	k1gMnSc2	kněz
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
křest	křest	k1gInSc1	křest
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
svátostí	svátost	k1gFnPc2	svátost
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
očištění	očištění	k1gNnSc1	očištění
od	od	k7c2	od
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc2	sjednocení
věřícího	věřící	k1gMnSc2	věřící
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
praxe	praxe	k1gFnSc2	praxe
křtít	křtít	k5eAaImF	křtít
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podle	podle	k7c2	podle
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
Ježíše	Ježíš	k1gMnPc4	Ježíš
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
křest	křest	k1gInSc1	křest
se	se	k3xPyFc4	se
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
řekne	říct	k5eAaPmIp3nS	říct
β	β	k?	β
[	[	kIx(	[
<g/>
baptisma	baptisma	k1gFnSc1	baptisma
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
ponoření	ponoření	k1gNnSc4	ponoření
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
definuje	definovat	k5eAaBmIp3nS	definovat
křest	křest	k1gInSc4	křest
jako	jako	k8xC	jako
ponoření	ponoření	k1gNnSc4	ponoření
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
církve	církev	k1gFnPc1	církev
křtí	křtít	k5eAaImIp3nP	křtít
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
pokropením	pokropení	k1gNnSc7	pokropení
<g/>
,	,	kIx,	,
politím	polití	k1gNnSc7	polití
nebo	nebo	k8xC	nebo
plným	plný	k2eAgNnSc7d1	plné
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Janův	Janův	k2eAgInSc1d1	Janův
křest	křest	k1gInSc1	křest
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
Janovým	Janův	k2eAgInSc7d1	Janův
křtem	křest	k1gInSc7	křest
a	a	k8xC	a
křtem	křest	k1gInSc7	křest
do	do	k7c2	do
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Janovi	Jan	k1gMnSc6	Jan
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
již	již	k9	již
v	v	k7c6	v
prorockých	prorocký	k2eAgFnPc6d1	prorocká
knihách	kniha	k1gFnPc6	kniha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
nového	nový	k2eAgInSc2d1	nový
zákona	zákon	k1gInSc2	zákon
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
Janově	Janův	k2eAgNnSc6d1	Janovo
evangeliu	evangelium	k1gNnSc6	evangelium
je	být	k5eAaImIp3nS	být
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
křest	křest	k1gInSc1	křest
popsán	popsat	k5eAaPmNgInS	popsat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
evangelií	evangelium	k1gNnPc2	evangelium
tedy	tedy	k9	tedy
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
křtil	křtít	k5eAaImAgMnS	křtít
pouze	pouze	k6eAd1	pouze
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
odpuštění	odpuštění	k1gNnSc4	odpuštění
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
použité	použitý	k2eAgNnSc4d1	Použité
slovo	slovo	k1gNnSc4	slovo
ε	ε	k?	ε
mající	mající	k2eAgInSc4d1	mající
význam	význam	k1gInSc4	význam
"	"	kIx"	"
<g/>
do	do	k7c2	do
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
tedy	tedy	k9	tedy
nelze	lze	k6eNd1	lze
vnímat	vnímat	k5eAaImF	vnímat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
pouhý	pouhý	k2eAgInSc1d1	pouhý
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
samotný	samotný	k2eAgInSc1d1	samotný
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
odpuštění	odpuštění	k1gNnSc1	odpuštění
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
křest	křest	k1gInSc1	křest
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
verš	verš	k1gInSc1	verš
dále	daleko	k6eAd2	daleko
popisuje	popisovat	k5eAaImIp3nS	popisovat
jak	jak	k8xS	jak
konkrétně	konkrétně	k6eAd1	konkrétně
probíhal	probíhat	k5eAaImAgInS	probíhat
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
křest	křest	k1gInSc1	křest
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
Janovým	Janův	k2eAgInSc7d1	Janův
a	a	k8xC	a
Ježíšovým	Ježíšův	k2eAgInSc7d1	Ježíšův
křtem	křest	k1gInSc7	křest
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
viděl	vidět	k5eAaImAgMnS	vidět
sesupovat	sesupovat	k5eAaBmF	sesupovat
Ducha	duch	k1gMnSc4	duch
Svatého	svatý	k1gMnSc4	svatý
na	na	k7c4	na
Ježíše	Ježíš	k1gMnSc4	Ježíš
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
holubice	holubice	k1gFnSc2	holubice
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
bude	být	k5eAaImBp3nS	být
křtít	křtít	k5eAaImF	křtít
Duchem	duch	k1gMnSc7	duch
Svatým	svatý	k1gMnSc7	svatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lukášově	Lukášův	k2eAgNnSc6d1	Lukášovo
evangeliu	evangelium	k1gNnSc6	evangelium
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
Janově	Janův	k2eAgFnSc6d1	Janova
řeči	řeč	k1gFnSc6	řeč
mírně	mírně	k6eAd1	mírně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přičemž	přičemž	k6eAd1	přičemž
křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
zde	zde	k6eAd1	zde
znamená	znamenat	k5eAaImIp3nS	znamenat
Boží	boží	k2eAgInSc1d1	boží
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
hříšníci	hříšník	k1gMnPc1	hříšník
budou	být	k5eAaImBp3nP	být
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Křest	křest	k1gInSc1	křest
v	v	k7c6	v
církevních	církevní	k2eAgInPc6d1	církevní
dopisech	dopis	k1gInPc6	dopis
===	===	k?	===
</s>
</p>
<p>
<s>
Apoštol	apoštol	k1gMnSc1	apoštol
Pavel	Pavel	k1gMnSc1	Pavel
církvi	církev	k1gFnSc3	církev
v	v	k7c6	v
Kolosu	kolos	k1gInSc6	kolos
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
verš	verš	k1gInSc1	verš
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
význam	význam	k1gInSc4	význam
křtu	křest	k1gInSc2	křest
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
věřícího	věřící	k2eAgNnSc2d1	věřící
v	v	k7c6	v
Krista	Krista	k1gFnSc1	Krista
tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
tomuto	tento	k3xDgInSc3	tento
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
duchovně	duchovně	k6eAd1	duchovně
vzkříšen	vzkřísit	k5eAaPmNgInS	vzkřísit
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
skrze	skrze	k?	skrze
víru	vír	k1gInSc2	vír
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
příslib	příslib	k1gInSc4	příslib
Božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apoštol	apoštol	k1gMnSc1	apoštol
Pavel	Pavel	k1gMnSc1	Pavel
církvi	církev	k1gFnSc3	církev
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
byli	být	k5eAaImAgMnP	být
ponořeni	ponořit	k5eAaPmNgMnP	ponořit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
ponořeni	ponořit	k5eAaPmNgMnP	ponořit
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
věřící	věřící	k1gMnSc1	věřící
člověk	člověk	k1gMnSc1	člověk
nejprve	nejprve	k6eAd1	nejprve
musel	muset	k5eAaImAgMnS	muset
činit	činit	k5eAaImF	činit
pokání	pokání	k1gNnSc4	pokání
<g/>
,	,	kIx,	,
odvrátit	odvrátit	k5eAaPmF	odvrátit
se	se	k3xPyFc4	se
od	od	k7c2	od
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
tj.	tj.	kA	tj.
hříšného	hříšný	k2eAgInSc2d1	hříšný
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
ponořen	ponořit	k5eAaPmNgInS	ponořit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tělem	tělo	k1gNnSc7	tělo
je	být	k5eAaImIp3nS	být
myšlena	myšlen	k2eAgFnSc1d1	myšlena
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
uctívají	uctívat	k5eAaImIp3nP	uctívat
Boha	bůh	k1gMnSc4	bůh
v	v	k7c6	v
Duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
v	v	k7c6	v
Pravdě	pravda	k1gFnSc6	pravda
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uctívají	uctívat	k5eAaImIp3nP	uctívat
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokání	pokání	k1gNnSc6	pokání
následuje	následovat	k5eAaImIp3nS	následovat
ponoření	ponoření	k1gNnSc1	ponoření
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
znovuzrodí	znovuzrodit	k5eAaPmIp3nS	znovuzrodit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
narodí	narodit	k5eAaPmIp3nP	narodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apoštol	apoštol	k1gMnSc1	apoštol
Pavel	Pavel	k1gMnSc1	Pavel
napsal	napsat	k5eAaPmAgMnS	napsat
Galatským	Galatský	k2eAgMnSc7d1	Galatský
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
kdo	kdo	k3yInSc1	kdo
uvěřili	uvěřit	k5eAaPmAgMnP	uvěřit
v	v	k7c4	v
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
učinili	učinit	k5eAaPmAgMnP	učinit
pokání	pokání	k1gNnPc4	pokání
<g/>
,	,	kIx,	,
odložili	odložit	k5eAaPmAgMnP	odložit
starého	starý	k1gMnSc4	starý
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
oblékli	obléct	k5eAaPmAgMnP	obléct
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kladeno	kladen	k2eAgNnSc4d1	kladeno
rovnítko	rovnítko	k1gNnSc4	rovnítko
mezi	mezi	k7c4	mezi
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
křest	křest	k1gInSc4	křest
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
křtu	křest	k1gInSc2	křest
není	být	k5eNaImIp3nS	být
víra	víra	k1gFnSc1	víra
a	a	k8xC	a
bez	bez	k7c2	bez
víry	víra	k1gFnSc2	víra
není	být	k5eNaImIp3nS	být
křest	křest	k1gInSc1	křest
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
zaslíbení	zaslíbení	k1gNnSc2	zaslíbení
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
píše	psát	k5eAaImIp3nS	psát
Pavel	Pavel	k1gMnSc1	Pavel
je	být	k5eAaImIp3nS	být
víra	víra	k1gFnSc1	víra
i	i	k8xC	i
křest	křest	k1gInSc1	křest
<g/>
.	.	kIx.	.
</s>
<s>
Zaslíbení	zaslíbení	k1gNnSc1	zaslíbení
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
obrácený	obrácený	k2eAgMnSc1d1	obrácený
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
nazýván	nazývat	k5eAaImNgMnS	nazývat
Božím	boží	k2eAgMnSc7d1	boží
synem	syn	k1gMnSc7	syn
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
dědicem	dědic	k1gMnSc7	dědic
Božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitost	důležitost	k1gFnSc4	důležitost
křtu	křest	k1gInSc2	křest
a	a	k8xC	a
závazek	závazek	k1gInSc4	závazek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
to	ten	k3xDgNnSc4	ten
představuje	představovat	k5eAaImIp3nS	představovat
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
následujícího	následující	k2eAgInSc2d1	následující
dopisu	dopis	k1gInSc2	dopis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
podstoupit	podstoupit	k5eAaPmF	podstoupit
křest	křest	k1gInSc4	křest
vydává	vydávat	k5eAaImIp3nS	vydávat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
16	[number]	k4	16
<g/>
.	.	kIx.	.
verš	verš	k1gInSc1	verš
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yInSc1	kdo
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Krista	Kristus	k1gMnSc4	Kristus
již	již	k6eAd1	již
nehřeší	hřešit	k5eNaImIp3nS	hřešit
(	(	kIx(	(
<g/>
nebude	být	k5eNaImBp3nS	být
dále	daleko	k6eAd2	daleko
hřešit	hřešit	k5eAaImF	hřešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Symbolika	symbolika	k1gFnSc1	symbolika
křtu	křest	k1gInSc2	křest
===	===	k?	===
</s>
</p>
<p>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
křtu	křest	k1gInSc2	křest
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
křest	křest	k1gInSc1	křest
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
symbolickým	symbolický	k2eAgNnSc7d1	symbolické
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
nadpřirozenou	nadpřirozený	k2eAgFnSc7d1	nadpřirozená
proměnou	proměna	k1gFnSc7	proměna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
paralelu	paralela	k1gFnSc4	paralela
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
Noema	Noe	k1gMnSc2	Noe
či	či	k8xC	či
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
a	a	k8xC	a
projití	projití	k1gNnPc1	projití
Izraelitů	izraelita	k1gMnPc2	izraelita
Rudým	rudý	k2eAgNnSc7d1	Rudé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
víry	víra	k1gFnSc2	víra
nejen	nejen	k6eAd1	nejen
očištěním	očištění	k1gNnSc7	očištění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
vzkříšením	vzkříšení	k1gNnSc7	vzkříšení
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
církví	církev	k1gFnPc2	církev
získává	získávat	k5eAaImIp3nS	získávat
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
neofyta	neofyt	k1gMnSc2	neofyt
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
pokřtěný	pokřtěný	k2eAgMnSc1d1	pokřtěný
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svátost	svátost	k1gFnSc4	svátost
===	===	k?	===
</s>
</p>
<p>
<s>
Křest	křest	k1gInSc1	křest
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
iniciační	iniciační	k2eAgFnSc1d1	iniciační
svátost	svátost	k1gFnSc1	svátost
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
křtem	křest	k1gInSc7	křest
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svátostí	svátost	k1gFnPc2	svátost
je	být	k5eAaImIp3nS	být
křest	křest	k1gInSc4	křest
jejich	jejich	k3xOp3gFnSc4	jejich
branou	braný	k2eAgFnSc4d1	braná
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgFnPc4d1	ostatní
svátosti	svátost	k1gFnPc4	svátost
může	moct	k5eAaImIp3nS	moct
přijmout	přijmout	k5eAaPmF	přijmout
jen	jen	k9	jen
člověk	člověk	k1gMnSc1	člověk
pokřtěný	pokřtěný	k2eAgMnSc1d1	pokřtěný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
církve	církev	k1gFnPc1	církev
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
svátostí	svátost	k1gFnPc2	svátost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
křest	křest	k1gInSc1	křest
je	být	k5eAaImIp3nS	být
uznáván	uznávat	k5eAaImNgInS	uznávat
všemi	všecek	k3xTgFnPc7	všecek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
církvích	církev	k1gFnPc6	církev
probíhá	probíhat	k5eAaImIp3nS	probíhat
křest	křest	k1gInSc4	křest
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
až	až	k6eAd1	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
schopnost	schopnost	k1gFnSc1	schopnost
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
význam	význam	k1gInSc4	význam
křtu	křest	k1gInSc2	křest
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgFnPc1d1	možná
obě	dva	k4xCgFnPc1	dva
možnosti	možnost	k1gFnPc1	možnost
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Křty	křest	k1gInPc1	křest
za	za	k7c4	za
mrtvé	mrtvý	k1gMnPc4	mrtvý
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
současné	současný	k2eAgFnPc1d1	současná
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
skupiny	skupina	k1gFnPc1	skupina
a	a	k8xC	a
společenství	společenství	k1gNnPc1	společenství
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
praktikovaly	praktikovat	k5eAaImAgFnP	praktikovat
nebo	nebo	k8xC	nebo
dnes	dnes	k6eAd1	dnes
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
křty	křest	k1gInPc1	křest
za	za	k7c4	za
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
společenství	společenství	k1gNnPc2	společenství
však	však	k9	však
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
pouze	pouze	k6eAd1	pouze
křty	křest	k1gInPc4	křest
pro	pro	k7c4	pro
živé	živý	k2eAgMnPc4d1	živý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
uznávání	uznávání	k1gNnSc1	uznávání
křtů	křest	k1gInPc2	křest
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
křest	křest	k1gInSc4	křest
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
ritu	rit	k1gInSc2	rit
člověk	člověk	k1gMnSc1	člověk
křest	křest	k1gInSc4	křest
podstoupí	podstoupit	k5eAaPmIp3nS	podstoupit
<g/>
,	,	kIx,	,
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
ritu	rito	k1gNnSc3	rito
náleží	náležet	k5eAaImIp3nP	náležet
a	a	k8xC	a
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
zákoník	zákoník	k1gInSc4	zákoník
té	ten	k3xDgFnSc2	ten
dané	daný	k2eAgFnSc2d1	daná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
křtu	křest	k1gInSc2	křest
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
církvemi	církev	k1gFnPc7	církev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
ekumenické	ekumenický	k2eAgFnSc2d1	ekumenická
rady	rada	k1gFnSc2	rada
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
ustálená	ustálený	k2eAgFnSc1d1	ustálená
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
křest	křest	k1gInSc4	křest
navzájem	navzájem	k6eAd1	navzájem
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
Trojjediného	trojjediný	k2eAgMnSc4d1	trojjediný
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
to	ten	k3xDgNnSc1	ten
i	i	k9	i
z	z	k7c2	z
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
křest	křest	k1gInSc1	křest
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
svou	svůj	k3xOyFgFnSc7	svůj
povahou	povaha	k1gFnSc7	povaha
udělen	udělit	k5eAaPmNgMnS	udělit
<g/>
/	/	kIx~	/
<g/>
přijat	přijmout	k5eAaPmNgInS	přijmout
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
platnost	platnost	k1gFnSc1	platnost
je	být	k5eAaImIp3nS	být
trvalá	trvalý	k2eAgFnSc1d1	trvalá
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
nikdy	nikdy	k6eAd1	nikdy
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
–	–	k?	–
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
–	–	k?	–
pokud	pokud	k8xS	pokud
nějaký	nějaký	k3yIgMnSc1	nějaký
pokřtěný	pokřtěný	k2eAgMnSc1d1	pokřtěný
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
církve	církev	k1gFnSc2	církev
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
nebývá	bývat	k5eNaImIp3nS	bývat
"	"	kIx"	"
<g/>
překřtíván	překřtíván	k2eAgMnSc1d1	překřtíván
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
takového	takový	k3xDgMnSc4	takový
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
později	pozdě	k6eAd2	pozdě
církev	církev	k1gFnSc4	církev
či	či	k8xC	či
víru	víra	k1gFnSc4	víra
opustí	opustit	k5eAaPmIp3nP	opustit
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
CJKSPD	CJKSPD	kA	CJKSPD
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výjimky	výjimek	k1gInPc4	výjimek
====	====	k?	====
</s>
</p>
<p>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
křest	křest	k1gInSc1	křest
nebyl	být	k5eNaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
v	v	k7c6	v
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgMnPc2d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
křest	křest	k1gInSc1	křest
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc1	všechen
křty	křest	k1gInPc1	křest
nevykonané	vykonaný	k2eNgInPc1d1	nevykonaný
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
v	v	k7c6	v
Trojici	trojice	k1gFnSc6	trojice
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
ostatními	ostatní	k2eAgFnPc7d1	ostatní
církvemi	církev	k1gFnPc7	církev
uznáván	uznáván	k2eAgInSc4d1	uznáván
za	za	k7c4	za
platný	platný	k2eAgInSc4d1	platný
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
znovu	znovu	k6eAd1	znovu
křtěni	křtít	k5eAaImNgMnP	křtít
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Svědci	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
neuznávají	uznávat	k5eNaImIp3nP	uznávat
jiné	jiný	k2eAgInPc4d1	jiný
křty	křest	k1gInPc4	křest
a	a	k8xC	a
pokud	pokud	k8xS	pokud
přechází	přecházet	k5eAaImIp3nS	přecházet
někdo	někdo	k3yInSc1	někdo
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
také	také	k6eAd1	také
křtěn	křtěn	k2eAgInSc1d1	křtěn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
neuznává	uznávat	k5eNaImIp3nS	uznávat
křest	křest	k1gInSc4	křest
katolický	katolický	k2eAgInSc4d1	katolický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
katolíci	katolík	k1gMnPc1	katolík
uznávají	uznávat	k5eAaImIp3nP	uznávat
křest	křest	k1gInSc1	křest
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
<g/>
-li	i	k?	-li
pochybnost	pochybnost	k1gFnSc4	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
křest	křest	k1gInSc1	křest
udělen	udělit	k5eAaPmNgInS	udělit
či	či	k8xC	či
neudělen	udělit	k5eNaPmNgInS	udělit
právoplatně	právoplatně	k6eAd1	právoplatně
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
se	s	k7c7	s
tzv.	tzv.	kA	tzv.
křest	křest	k1gInSc1	křest
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
(	(	kIx(	(
<g/>
sub	sub	k7c4	sub
conditione	condition	k1gInSc5	condition
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
nejsi	být	k5eNaImIp2nS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tak	tak	k9	tak
tě	ty	k3xPp2nSc4	ty
křtím	křtít	k5eAaImIp1nS	křtít
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Otce	otec	k1gMnSc2	otec
i	i	k8xC	i
Syna	syn	k1gMnSc2	syn
i	i	k8xC	i
ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
právní	právní	k2eAgInSc4d1	právní
důsledek	důsledek	k1gInSc4	důsledek
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
<g/>
-li	i	k?	-li
první	první	k4xOgFnSc6	první
křest	křest	k1gInSc1	křest
platný	platný	k2eAgMnSc1d1	platný
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
takto	takto	k6eAd1	takto
křtěný	křtěný	k2eAgMnSc1d1	křtěný
má	mít	k5eAaImIp3nS	mít
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
členem	člen	k1gMnSc7	člen
církve	církev	k1gFnSc2	církev
–	–	k?	–
Těla	tělo	k1gNnSc2	tělo
Kristova	Kristův	k2eAgFnSc1d1	Kristova
(	(	kIx(	(
<g/>
praxe	praxe	k1gFnSc1	praxe
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
katolicismu	katolicismus	k1gInSc6	katolicismus
===	===	k?	===
</s>
</p>
<p>
<s>
Katolické	katolický	k2eAgNnSc1d1	katolické
pojetí	pojetí	k1gNnSc1	pojetí
křtu	křest	k1gInSc2	křest
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
specifických	specifický	k2eAgInPc2d1	specifický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Katolíci	katolík	k1gMnPc1	katolík
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
křtu	křest	k1gInSc2	křest
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
pro	pro	k7c4	pro
smazání	smazání	k1gNnSc4	smazání
prvotního	prvotní	k2eAgInSc2d1	prvotní
hříchu	hřích	k1gInSc2	hřích
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
praxe	praxe	k1gFnSc1	praxe
křtu	křest	k1gInSc2	křest
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Podmínky	podmínka	k1gFnSc2	podmínka
křtu	křest	k1gInSc2	křest
====	====	k?	====
</s>
</p>
<p>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
křtu	křest	k1gInSc2	křest
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vyznávat	vyznávat	k5eAaImF	vyznávat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
v	v	k7c4	v
co	co	k3yRnSc4	co
věří	věřit	k5eAaImIp3nS	věřit
církev	církev	k1gFnSc1	církev
provádějící	provádějící	k2eAgFnSc2d1	provádějící
křest	křest	k1gInSc4	křest
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
děti	dítě	k1gFnPc1	dítě
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pokřtěny	pokřtít	k5eAaPmNgInP	pokřtít
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
zaručí	zaručit	k5eAaPmIp3nS	zaručit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
budou	být	k5eAaImBp3nP	být
vychovávat	vychovávat	k5eAaImF	vychovávat
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodiče	rodič	k1gMnPc1	rodič
zajistí	zajistit	k5eAaPmIp3nP	zajistit
naplňování	naplňování	k1gNnPc4	naplňování
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ze	z	k7c2	z
křtu	křest	k1gInSc2	křest
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
(	(	kIx(	(
<g/>
dodržování	dodržování	k1gNnSc4	dodržování
jak	jak	k8xC	jak
desatera	desatero	k1gNnPc4	desatero
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
u	u	k7c2	u
katolíků	katolík	k1gMnPc2	katolík
ještě	ještě	k9	ještě
církevních	církevní	k2eAgNnPc2d1	církevní
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
denominace	denominace	k1gFnPc1	denominace
považují	považovat	k5eAaImIp3nP	považovat
tento	tento	k3xDgInSc4	tento
křest	křest	k1gInSc4	křest
za	za	k7c4	za
neplatný	platný	k2eNgInSc4d1	neplatný
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
zvyk	zvyk	k1gInSc1	zvyk
křtít	křtít	k5eAaImF	křtít
děti	dítě	k1gFnPc4	dítě
je	být	k5eAaImIp3nS	být
tradice	tradice	k1gFnSc1	tradice
církve	církev	k1gFnSc2	církev
už	už	k6eAd1	už
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
(	(	kIx(	(
<g/>
výslovně	výslovně	k6eAd1	výslovně
dosvědčena	dosvědčen	k2eAgFnSc1d1	dosvědčena
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
viz	vidět	k5eAaImRp2nS	vidět
Katechismus	katechismus	k1gInSc1	katechismus
<g/>
,	,	kIx,	,
1252	[number]	k4	1252
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšova	Ježíšův	k2eAgNnPc1d1	Ježíšovo
slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechte	nechat	k5eAaPmRp2nP	nechat
děti	dítě	k1gFnPc4	dítě
přicházet	přicházet	k5eAaImF	přicházet
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
nebraňte	bránit	k5eNaImRp2nP	bránit
jim	on	k3xPp3gMnPc3	on
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mk	Mk	k1gFnSc1	Mk
10,14	[number]	k4	10,14
<g/>
)	)	kIx)	)
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
rodičů	rodič	k1gMnPc2	rodič
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc1	jejich
dítě	dítě	k1gNnSc1	dítě
žilo	žít	k5eAaImAgNnS	žít
a	a	k8xC	a
čerpalo	čerpat	k5eAaImAgNnS	čerpat
z	z	k7c2	z
Božích	boží	k2eAgFnPc2d1	boží
milostí	milost	k1gFnPc2	milost
už	už	k6eAd1	už
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
argument	argument	k1gInSc1	argument
pro	pro	k7c4	pro
křest	křest	k1gInSc4	křest
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
(	(	kIx(	(
<g/>
podobnou	podobný	k2eAgFnSc7d1	podobná
logikou	logika	k1gFnSc7	logika
nechávají	nechávat	k5eAaImIp3nP	nechávat
starostliví	starostlivý	k2eAgMnPc1d1	starostlivý
rodičové	rodič	k1gMnPc1	rodič
očkovat	očkovat	k5eAaImF	očkovat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
proti	proti	k7c3	proti
vlivům	vliv	k1gInPc3	vliv
nemocí	nemoc	k1gFnPc2	nemoc
jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křtem	křest	k1gInSc7	křest
se	se	k3xPyFc4	se
u	u	k7c2	u
konvertitů	konvertita	k1gMnPc2	konvertita
podle	podle	k7c2	podle
převažující	převažující	k2eAgFnSc2d1	převažující
víry	víra	k1gFnSc2	víra
smazává	smazávat	k5eAaImIp3nS	smazávat
jak	jak	k6eAd1	jak
dědičný	dědičný	k2eAgInSc4d1	dědičný
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
tak	tak	k9	tak
hříchy	hřích	k1gInPc4	hřích
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
křtí	křtít	k5eAaImIp3nS	křtít
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
nebo	nebo	k8xC	nebo
jáhen	jáhen	k1gMnSc1	jáhen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
výjimečné	výjimečný	k2eAgFnSc6d1	výjimečná
situaci	situace	k1gFnSc6	situace
může	moct	k5eAaImIp3nS	moct
křtít	křtít	k5eAaImF	křtít
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
úmysl	úmysl	k1gInSc1	úmysl
"	"	kIx"	"
<g/>
konat	konat	k5eAaImF	konat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
církev	církev	k1gFnSc1	církev
<g/>
"	"	kIx"	"
a	a	k8xC	a
křest	křest	k1gInSc4	křest
udělit	udělit	k5eAaPmF	udělit
dle	dle	k7c2	dle
platné	platný	k2eAgFnSc2d1	platná
formy	forma	k1gFnSc2	forma
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhovění	vyhovění	k1gNnSc2	vyhovění
žádosti	žádost	k1gFnSc2	žádost
(	(	kIx(	(
<g/>
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
nevěřící	věřící	k2eNgFnSc1d1	nevěřící
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křestní	křestní	k2eAgFnSc1d1	křestní
formule	formule	k1gFnSc1	formule
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Jméno	jméno	k1gNnSc1	jméno
ve	v	k7c6	v
vokativu	vokativ	k1gInSc6	vokativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
tě	ty	k3xPp2nSc4	ty
křtím	křtít	k5eAaImIp1nS	křtít
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Otce	otec	k1gMnSc2	otec
i	i	k8xC	i
Syna	syn	k1gMnSc2	syn
i	i	k8xC	i
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
těchto	tento	k3xDgNnPc6	tento
slovech	slovo	k1gNnPc6	slovo
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
lita	lit	k2eAgFnSc1d1	lita
čistá	čistá	k1gFnSc1	čistá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
porodu	porod	k1gInSc3	porod
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
nebo	nebo	k8xC	nebo
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
mohou	moct	k5eAaImIp3nP	moct
kapitána	kapitán	k1gMnSc4	kapitán
lodi	loď	k1gFnSc2	loď
nebo	nebo	k8xC	nebo
letadla	letadlo	k1gNnSc2	letadlo
požádat	požádat	k5eAaPmF	požádat
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
křtu	křest	k1gInSc2	křest
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
tento	tento	k3xDgInSc4	tento
křest	křest	k1gInSc4	křest
uzná	uznat	k5eAaPmIp3nS	uznat
<g/>
,	,	kIx,	,
požaduje	požadovat	k5eAaImIp3nS	požadovat
jen	jen	k9	jen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
byla	být	k5eAaImAgFnS	být
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
výše	výše	k1gFnSc1	výše
uvedená	uvedený	k2eAgFnSc1d1	uvedená
formulace	formulace	k1gFnSc1	formulace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Pavle	Pavla	k1gFnSc3	Pavla
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
tě	ty	k3xPp2nSc4	ty
křtím	křtít	k5eAaImIp1nS	křtít
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Otce	otec	k1gMnSc2	otec
i	i	k8xC	i
Syna	syn	k1gMnSc2	syn
i	i	k8xC	i
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kritika	kritika	k1gFnSc1	kritika
dětského	dětský	k2eAgInSc2d1	dětský
křtu	křest	k1gInSc2	křest
====	====	k?	====
</s>
</p>
<p>
<s>
Praxe	praxe	k1gFnSc1	praxe
křtění	křtění	k1gNnSc2	křtění
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
křtu	křest	k1gInSc6	křest
nemohou	moct	k5eNaImIp3nP	moct
samostatně	samostatně	k6eAd1	samostatně
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
kritizována	kritizován	k2eAgFnSc1d1	kritizována
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
praktika	praktika	k1gFnSc1	praktika
některými	některý	k3yIgFnPc7	některý
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
křtem	křest	k1gInSc7	křest
stává	stávat	k5eAaImIp3nS	stávat
bez	bez	k7c2	bez
vědomého	vědomý	k2eAgInSc2d1	vědomý
souhlasu	souhlas	k1gInSc2	souhlas
členem	člen	k1gMnSc7	člen
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
křesťané	křesťan	k1gMnPc1	křesťan
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
až	až	k6eAd1	až
ve	v	k7c6	v
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
křtění	křtění	k1gNnSc1	křtění
dětí	dítě	k1gFnPc2	dítě
odmítají	odmítat	k5eAaImIp3nP	odmítat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
denominace	denominace	k1gFnPc1	denominace
jako	jako	k8xC	jako
baptisté	baptista	k1gMnPc1	baptista
<g/>
,	,	kIx,	,
letniční	letniční	k2eAgMnPc1d1	letniční
<g/>
,	,	kIx,	,
charismatici	charismatik	k1gMnPc1	charismatik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mormoni	mormon	k1gMnPc1	mormon
<g/>
,	,	kIx,	,
amenité	amenitý	k2eAgFnPc1d1	amenitý
<g/>
,	,	kIx,	,
mennonité	mennonitý	k2eAgFnPc1d1	mennonitý
<g/>
,	,	kIx,	,
svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
křest	křest	k1gInSc1	křest
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
neplatný	platný	k2eNgInSc4d1	neplatný
a	a	k8xC	a
pokud	pokud	k8xS	pokud
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závažným	závažný	k2eAgInSc7d1	závažný
argumentem	argument	k1gInSc7	argument
proti	proti	k7c3	proti
křtění	křtění	k1gNnSc3	křtění
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
absence	absence	k1gFnSc1	absence
katechumenátu	katechumenát	k1gInSc2	katechumenát
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
bude	být	k5eAaImBp3nS	být
vzděláváno	vzdělávat	k5eAaImNgNnS	vzdělávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dospívání	dospívání	k1gNnSc2	dospívání
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
neděje	dít	k5eNaImIp3nS	dít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
praktikující	praktikující	k2eAgMnPc1d1	praktikující
křesťané	křesťan	k1gMnPc1	křesťan
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
elementární	elementární	k2eAgFnSc2d1	elementární
znalosti	znalost	k1gFnSc2	znalost
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
neokatechumenát	neokatechumenát	k1gInSc1	neokatechumenát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Alternativní	alternativní	k2eAgFnPc4d1	alternativní
formy	forma	k1gFnPc4	forma
křtu	křest	k1gInSc2	křest
====	====	k?	====
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
křest	křest	k1gInSc1	křest
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
udělen	udělen	k2eAgInSc1d1	udělen
–	–	k?	–
například	například	k6eAd1	například
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
podle	podle	k7c2	podle
učení	učení	k1gNnSc2	učení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
křest	křest	k1gInSc4	křest
touhy	touha	k1gFnSc2	touha
a	a	k8xC	a
křest	křest	k1gInSc4	křest
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Umírající	umírající	k1gMnSc1	umírající
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
křest	křest	k1gInSc1	křest
řádně	řádně	k6eAd1	řádně
vykonán	vykonat	k5eAaPmNgInS	vykonat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
touhou	touha	k1gFnSc7	touha
–	–	k?	–
vírou	víra	k1gFnSc7	víra
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
<g/>
.	.	kIx.	.
</s>
<s>
Křtem	křest	k1gInSc7	křest
krve	krev	k1gFnSc2	krev
jsou	být	k5eAaImIp3nP	být
pokřtěni	pokřtěn	k2eAgMnPc1d1	pokřtěn
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
stačili	stačit	k5eAaBmAgMnP	stačit
přijmout	přijmout	k5eAaPmF	přijmout
křest	křest	k1gInSc4	křest
řádný	řádný	k2eAgInSc4d1	řádný
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
pro	pro	k7c4	pro
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
prvomučedníci	prvomučedník	k1gMnPc1	prvomučedník
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
například	například	k6eAd1	například
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
konverzi	konverze	k1gFnSc4	konverze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
konverzi	konverze	k1gFnSc4	konverze
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
také	také	k9	také
provádí	provádět	k5eAaImIp3nS	provádět
tzv.	tzv.	kA	tzv.
zástupné	zástupný	k2eAgInPc1d1	zástupný
křty	křest	k1gInPc1	křest
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
neměli	mít	k5eNaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
křest	křest	k1gInSc4	křest
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
sikhismu	sikhismus	k1gInSc6	sikhismus
==	==	k?	==
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
sikhistického	sikhistický	k2eAgNnSc2d1	sikhistický
náboženství	náboženství	k1gNnSc2	náboženství
je	být	k5eAaImIp3nS	být
obřad	obřad	k1gInSc4	obřad
očištění	očištění	k1gNnSc2	očištění
<g/>
/	/	kIx~	/
<g/>
křtu	křest	k1gInSc2	křest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
rituálním	rituální	k2eAgNnSc7d1	rituální
ponořením	ponoření	k1gNnSc7	ponoření
v	v	k7c6	v
Khalse	Khalsa	k1gFnSc6	Khalsa
v	v	k7c6	v
posvátném	posvátný	k2eAgNnSc6d1	posvátné
jezírku	jezírko	k1gNnSc6	jezírko
u	u	k7c2	u
chrámu	chrám	k1gInSc2	chrám
Harmandir	Harmandir	k1gMnSc1	Harmandir
Sáhib	sáhib	k1gMnSc1	sáhib
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obřad	obřad	k1gInSc1	obřad
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Amrit	Amrit	k2eAgInSc1d1	Amrit
Sanchar	Sanchar	k1gInSc1	Sanchar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křest	křest	k1gInSc1	křest
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
novověku	novověk	k1gInSc2	novověk
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
civilizaci	civilizace	k1gFnSc6	civilizace
používán	používat	k5eAaImNgInS	používat
pojem	pojem	k1gInSc1	pojem
křest	křest	k1gInSc1	křest
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
první	první	k4xOgFnSc2	první
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
iniciačního	iniciační	k2eAgInSc2d1	iniciační
rituálu	rituál	k1gInSc2	rituál
<g/>
,	,	kIx,	,
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc2	zahájení
či	či	k8xC	či
inaugurace	inaugurace	k1gFnSc2	inaugurace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
první	první	k4xOgFnSc4	první
bojovou	bojový	k2eAgFnSc4d1	bojová
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
vážnou	vážný	k2eAgFnSc4d1	vážná
životní	životní	k2eAgFnSc4d1	životní
událost	událost	k1gFnSc4	událost
jako	jako	k8xS	jako
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
osvědčit	osvědčit	k5eAaPmF	osvědčit
své	svůj	k3xOyFgFnPc4	svůj
kvality	kvalita	k1gFnPc4	kvalita
(	(	kIx(	(
<g/>
takové	takový	k3xDgNnSc1	takový
jejich	jejich	k3xOp3gNnSc1	jejich
reálné	reálný	k2eAgNnSc1d1	reálné
a	a	k8xC	a
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
až	až	k8xS	až
nemilosrdné	milosrdný	k2eNgNnSc1d1	nemilosrdné
prověření	prověření	k1gNnSc1	prověření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
začali	začít	k5eAaPmAgMnP	začít
"	"	kIx"	"
<g/>
křtít	křtít	k5eAaImF	křtít
<g/>
"	"	kIx"	"
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
domy	dům	k1gInPc1	dům
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
slavnost	slavnost	k1gFnSc1	slavnost
křtu	křest	k1gInSc2	křest
byla	být	k5eAaImAgFnS	být
většinou	většinou	k6eAd1	většinou
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k8xC	jako
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
přípitkem	přípitek	k1gInSc7	přípitek
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
rozbitím	rozbití	k1gNnSc7	rozbití
lahve	lahev	k1gFnSc2	lahev
sektu	sekt	k1gInSc2	sekt
o	o	k7c4	o
palubu	paluba	k1gFnSc4	paluba
lodi	loď	k1gFnSc2	loď
apod.	apod.	kA	apod.
Původně	původně	k6eAd1	původně
totiž	totiž	k9	totiž
u	u	k7c2	u
oslav	oslava	k1gFnPc2	oslava
<g />
.	.	kIx.	.
</s>
<s>
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
spouštění	spouštění	k1gNnSc2	spouštění
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
zprovoznění	zprovoznění	k1gNnPc2	zprovoznění
budov	budova	k1gFnPc2	budova
<g/>
)	)	kIx)	)
bývali	bývat	k5eAaImAgMnP	bývat
přítomni	přítomen	k2eAgMnPc1d1	přítomen
zástupci	zástupce	k1gMnPc1	zástupce
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
často	často	k6eAd1	často
objekt	objekt	k1gInSc4	objekt
žehnali	žehnat	k5eAaImAgMnP	žehnat
a	a	k8xC	a
také	také	k9	také
prováděli	provádět	k5eAaImAgMnP	provádět
kropení	kropení	k1gNnSc4	kropení
objektu	objekt	k1gInSc2	objekt
svěcenou	svěcený	k2eAgFnSc7d1	svěcená
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
také	také	k9	také
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
oslavy	oslava	k1gFnPc4	oslava
pochází	pocházet	k5eAaImIp3nP	pocházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
ryze	ryze	k6eAd1	ryze
marketingové	marketingový	k2eAgFnPc4d1	marketingová
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
propagace	propagace	k1gFnSc1	propagace
uvedeného	uvedený	k2eAgInSc2d1	uvedený
produktu	produkt	k1gInSc2	produkt
nebo	nebo	k8xC	nebo
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Křest	křest	k1gInSc1	křest
knihy	kniha	k1gFnSc2	kniha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
desky	deska	k1gFnSc2	deska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
mláďat	mládě	k1gNnPc2	mládě
<g/>
"	"	kIx"	"
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
jiného	jiný	k2eAgInSc2d1	jiný
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
buď	buď	k8xC	buď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
přípitku	přípitek	k1gInSc2	přípitek
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
ovšem	ovšem	k9	ovšem
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
zvukový	zvukový	k2eAgInSc1d1	zvukový
nosič	nosič	k1gInSc1	nosič
apod.	apod.	kA	apod.
doslova	doslova	k6eAd1	doslova
polit	polít	k5eAaPmNgInS	polít
malým	malý	k2eAgInSc7d1	malý
objemem	objem	k1gInSc7	objem
sektu	sekt	k1gInSc2	sekt
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiné	jiný	k2eAgFnSc2d1	jiná
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
jistým	jistý	k2eAgInSc7d1	jistý
paradoxem	paradox	k1gInSc7	paradox
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
formu	forma	k1gFnSc4	forma
profánního	profánní	k2eAgInSc2d1	profánní
křtu	křest	k1gInSc2	křest
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
slavností	slavnost	k1gFnPc2	slavnost
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
státními	státní	k2eAgFnPc7d1	státní
organizacemi	organizace	k1gFnPc7	organizace
v	v	k7c6	v
období	období	k1gNnSc6	období
komunistických	komunistický	k2eAgFnPc2d1	komunistická
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
snažily	snažit	k5eAaImAgFnP	snažit
programově	programově	k6eAd1	programově
propagovat	propagovat	k5eAaImF	propagovat
ateismus	ateismus	k1gInSc4	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porevolučním	porevoluční	k2eAgInSc6d1	porevoluční
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
existovaly	existovat	k5eAaImAgInP	existovat
okťabriny	okťabrin	k1gInPc1	okťabrin
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgInSc1d1	občanský
iniciační	iniciační	k2eAgInSc4d1	iniciační
rituál	rituál	k1gInSc4	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc7d2	pozdější
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
církevnímu	církevní	k2eAgInSc3d1	církevní
křtu	křest	k1gInSc3	křest
se	se	k3xPyFc4	se
v	v	k7c6	v
komunistických	komunistický	k2eAgFnPc6d1	komunistická
zemích	zem	k1gFnPc6	zem
stalo	stát	k5eAaPmAgNnS	stát
vítání	vítání	k1gNnSc1	vítání
občánků	občánek	k1gMnPc2	občánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
neobvyklém	obvyklý	k2eNgNnSc6d1	neobvyklé
přirovnání	přirovnání	k1gNnSc6	přirovnání
použila	použít	k5eAaPmAgFnS	použít
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
<g/>
"	"	kIx"	"
americká	americký	k2eAgFnSc1d1	americká
politička	politička	k1gFnSc1	politička
Sarah	Sarah	k1gFnSc1	Sarah
Palinová	Palinová	k1gFnSc1	Palinová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označila	označit	k5eAaPmAgFnS	označit
mučení	mučení	k1gNnSc4	mučení
technikou	technika	k1gFnSc7	technika
waterboardingu	waterboarding	k1gInSc2	waterboarding
za	za	k7c4	za
"	"	kIx"	"
<g/>
křest	křest	k1gInSc4	křest
(	(	kIx(	(
<g/>
muslimských	muslimský	k2eAgMnPc2d1	muslimský
<g/>
)	)	kIx)	)
teroristů	terorista	k1gMnPc2	terorista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
křest	křest	k1gInSc1	křest
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
křest	křest	k1gInSc1	křest
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
biblickém	biblický	k2eAgInSc6d1	biblický
křtu	křest	k1gInSc6	křest
</s>
</p>
<p>
<s>
Křest	křest	k1gInSc1	křest
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
církvi	církev	k1gFnSc6	církev
</s>
</p>
<p>
<s>
Why	Why	k?	Why
the	the	k?	the
Jews	Jewsa	k1gFnPc2	Jewsa
Let	léto	k1gNnPc2	léto
Themselves	Themselves	k1gMnSc1	Themselves
Be	Be	k1gFnSc2	Be
Baptized	Baptized	k1gMnSc1	Baptized
–	–	k?	–
článek	článek	k1gInSc1	článek
o	o	k7c6	o
křtu	křest	k1gInSc6	křest
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
