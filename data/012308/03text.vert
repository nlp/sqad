<p>
<s>
Monitor	monitor	k1gInSc1	monitor
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc4d1	základní
výstupní	výstupní	k2eAgNnSc4d1	výstupní
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zařízení	zařízení	k1gNnSc4	zařízení
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
zobrazování	zobrazování	k1gNnSc3	zobrazování
textových	textový	k2eAgFnPc2d1	textová
a	a	k8xC	a
grafických	grafický	k2eAgFnPc2d1	grafická
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
propojen	propojen	k2eAgInSc1d1	propojen
s	s	k7c7	s
grafickou	grafický	k2eAgFnSc7d1	grafická
kartou	karta	k1gFnSc7	karta
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připojen	připojen	k2eAgInSc1d1	připojen
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
zařízením	zařízení	k1gNnPc3	zařízení
nebo	nebo	k8xC	nebo
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
přímo	přímo	k6eAd1	přímo
integrován	integrován	k2eAgMnSc1d1	integrován
(	(	kIx(	(
<g/>
PDA	PDA	kA	PDA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monitor	monitor	k1gInSc1	monitor
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
videokartě	videokarta	k1gFnSc3	videokarta
zasílající	zasílající	k2eAgFnPc4d1	zasílající
patřičné	patřičný	k2eAgFnPc4d1	patřičná
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
budou	být	k5eAaImBp3nP	být
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc3	jeho
obrazovce	obrazovka	k1gFnSc3	obrazovka
<g/>
)	)	kIx)	)
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Monitor	monitor	k1gInSc1	monitor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
samostatného	samostatný	k2eAgInSc2d1	samostatný
počítačového	počítačový	k2eAgInSc2d1	počítačový
terminálu	terminál	k1gInSc2	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
barevné	barevný	k2eAgFnSc6d1	barevná
CRT	CRT	kA	CRT
obrazovky	obrazovka	k1gFnPc1	obrazovka
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
katod	katoda	k1gFnPc2	katoda
emitovány	emitován	k2eAgInPc1d1	emitován
elektronové	elektronový	k2eAgInPc1d1	elektronový
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
pomocí	pomocí	k7c2	pomocí
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
mřížek	mřížka	k1gFnPc2	mřížka
až	až	k9	až
na	na	k7c4	na
stínítko	stínítko	k1gNnSc4	stínítko
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
stěně	stěna	k1gFnSc6	stěna
stínítka	stínítko	k1gNnSc2	stínítko
obrazovky	obrazovka	k1gFnSc2	obrazovka
jsou	být	k5eAaImIp3nP	být
naneseny	nanesen	k2eAgFnPc1d1	nanesena
vrstvy	vrstva	k1gFnPc1	vrstva
tzv.	tzv.	kA	tzv.
luminoforů	luminofor	k1gInPc2	luminofor
(	(	kIx(	(
<g/>
luminofor	luminofor	k1gInSc1	luminofor
=	=	kIx~	=
látka	látka	k1gFnSc1	látka
přeměňující	přeměňující	k2eAgFnSc4d1	přeměňující
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
světelnou	světelný	k2eAgFnSc4d1	světelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Green	Green	k1gInSc1	Green
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
aditivní	aditivní	k2eAgInSc4d1	aditivní
model	model	k1gInSc4	model
skládání	skládání	k1gNnSc2	skládání
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1	vlastní
elektronové	elektronový	k2eAgInPc1d1	elektronový
svazky	svazek	k1gInPc1	svazek
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
příslušné	příslušný	k2eAgInPc4d1	příslušný
luminofory	luminofor	k1gInPc4	luminofor
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozsvícení	rozsvícení	k1gNnSc3	rozsvícení
bodu	bod	k1gInSc2	bod
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
stínítkem	stínítko	k1gNnSc7	stínítko
obrazovky	obrazovka	k1gFnSc2	obrazovka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
maska	maska	k1gFnSc1	maska
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
mříž	mříž	k1gFnSc1	mříž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
propustit	propustit	k5eAaPmF	propustit
jen	jen	k9	jen
úzký	úzký	k2eAgInSc4d1	úzký
svazek	svazek	k1gInSc4	svazek
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Maska	maska	k1gFnSc1	maska
obrazovky	obrazovka	k1gFnSc2	obrazovka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
co	co	k9	co
nejméně	málo	k6eAd3	málo
podléhá	podléhat	k5eAaImIp3nS	podléhat
tepelné	tepelný	k2eAgFnPc4d1	tepelná
roztažnosti	roztažnost	k1gFnPc4	roztažnost
a	a	k8xC	a
působení	působení	k1gNnSc4	působení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
by	by	kYmCp3nP	by
totiž	totiž	k9	totiž
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektronové	elektronový	k2eAgInPc1d1	elektronový
svazky	svazek	k1gInPc1	svazek
nedopadnou	dopadnout	k5eNaPmIp3nP	dopadnout
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
luminofor	luminofor	k1gInSc4	luminofor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
nečistotou	nečistota	k1gFnSc7	nečistota
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Elektronové	elektronový	k2eAgInPc1d1	elektronový
svazky	svazek	k1gInPc1	svazek
jsou	být	k5eAaImIp3nP	být
vychylovány	vychylovat	k5eAaImNgInP	vychylovat
pomocí	pomocí	k7c2	pomocí
vychylovacích	vychylovací	k2eAgFnPc2d1	vychylovací
cívek	cívka	k1gFnPc2	cívka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
postupně	postupně	k6eAd1	postupně
opisovaly	opisovat	k5eAaImAgFnP	opisovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
a	a	k8xC	a
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
řádky	řádek	k1gInPc4	řádek
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
televizoru	televizor	k1gInSc2	televizor
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vybaven	vybavit	k5eAaPmNgInS	vybavit
vysokofrekvenčním	vysokofrekvenční	k2eAgInSc7d1	vysokofrekvenční
vstupním	vstupní	k2eAgInSc7d1	vstupní
obvodem	obvod	k1gInSc7	obvod
(	(	kIx(	(
<g/>
tunerem	tuner	k1gInSc7	tuner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
nelze	lze	k6eNd1	lze
připojit	připojit	k5eAaPmF	připojit
anténu	anténa	k1gFnSc4	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
monitoru	monitor	k1gInSc2	monitor
přenášen	přenášen	k2eAgMnSc1d1	přenášen
buď	buď	k8xC	buď
analogově	analogově	k6eAd1	analogově
nebo	nebo	k8xC	nebo
digitálně	digitálně	k6eAd1	digitálně
<g/>
.	.	kIx.	.
</s>
<s>
Monitory	monitor	k1gInPc1	monitor
můžeme	moct	k5eAaImIp1nP	moct
podle	podle	k7c2	podle
používaných	používaný	k2eAgFnPc2d1	používaná
technologií	technologie	k1gFnPc2	technologie
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Obrazovka	obrazovka	k1gFnSc1	obrazovka
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
<g/>
,	,	kIx,	,
klasická	klasický	k2eAgFnSc1d1	klasická
vakuová	vakuový	k2eAgFnSc1d1	vakuová
obrazovka	obrazovka	k1gFnSc1	obrazovka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LCD	LCD	kA	LCD
(	(	kIx(	(
<g/>
tekuté	tekutý	k2eAgInPc1d1	tekutý
krystaly	krystal	k1gInPc1	krystal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
plazmová	plazmový	k2eAgFnSc1d1	plazmová
obrazovka	obrazovka	k1gFnSc1	obrazovka
</s>
</p>
<p>
<s>
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
OLED	OLED	kA	OLED
<g/>
,	,	kIx,	,
SED	sed	k1gInSc1	sed
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
parametry	parametr	k1gInPc1	parametr
monitorů	monitor	k1gInPc2	monitor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úhlopříčka	úhlopříčka	k1gFnSc1	úhlopříčka
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
monitoru	monitor	k1gInSc2	monitor
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
protilehlými	protilehlý	k2eAgInPc7d1	protilehlý
rohy	roh	k1gInPc7	roh
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
značení	značení	k1gNnSc1	značení
velikosti	velikost	k1gFnSc2	velikost
obrazovky	obrazovka	k1gFnSc2	obrazovka
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
monitoru	monitor	k1gInSc2	monitor
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
stejné	stejný	k2eAgFnSc2d1	stejná
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
a	a	k8xC	a
jiného	jiný	k2eAgInSc2d1	jiný
poměru	poměr	k1gInSc2	poměr
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
dostaneme	dostat	k5eAaPmIp1nP	dostat
k	k	k7c3	k
odlišné	odlišný	k2eAgFnSc3d1	odlišná
velikosti	velikost	k1gFnSc3	velikost
zobrazované	zobrazovaný	k2eAgFnSc2d1	zobrazovaná
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
"	"	kIx"	"
monitor	monitor	k1gInSc1	monitor
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
~	~	kIx~	~
<g/>
1361	[number]	k4	1361
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
širokoúhlá	širokoúhlý	k2eAgFnSc1d1	širokoúhlá
obrazovka	obrazovka	k1gFnSc1	obrazovka
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
velikostí	velikost	k1gFnSc7	velikost
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
21	[number]	k4	21
<g/>
"	"	kIx"	"
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
~	~	kIx~	~
<g/>
1212	[number]	k4	1212
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
neduhem	neduh	k1gInSc7	neduh
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
velikosti	velikost	k1gFnSc2	velikost
je	být	k5eAaImIp3nS	být
nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
při	při	k7c6	při
značení	značení	k1gNnSc6	značení
skutečné	skutečný	k2eAgFnSc2d1	skutečná
velikosti	velikost	k1gFnSc2	velikost
monitorů	monitor	k1gInPc2	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výrobců	výrobce	k1gMnPc2	výrobce
totiž	totiž	k9	totiž
udává	udávat	k5eAaImIp3nS	udávat
velikost	velikost	k1gFnSc4	velikost
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
včetně	včetně	k7c2	včetně
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
skryje	skrýt	k5eAaPmIp3nS	skrýt
plastový	plastový	k2eAgInSc1d1	plastový
rám	rám	k1gInSc1	rám
monitoru	monitor	k1gInSc2	monitor
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
plocha	plocha	k1gFnSc1	plocha
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
CRT	CRT	kA	CRT
monitorů	monitor	k1gInPc2	monitor
veliká	veliký	k2eAgFnSc1d1	veliká
i	i	k8xC	i
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc2	výrobce
o	o	k7c4	o
skrytou	skrytý	k2eAgFnSc4d1	skrytá
plochu	plocha	k1gFnSc4	plocha
uživatele	uživatel	k1gMnSc2	uživatel
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
okrádá	okrádat	k5eAaImIp3nS	okrádat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
plocha	plocha	k1gFnSc1	plocha
využívá	využívat	k5eAaImIp3nS	využívat
částečně	částečně	k6eAd1	částečně
pro	pro	k7c4	pro
overscan	overscan	k1gInSc4	overscan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozlišení	rozlišení	k1gNnPc4	rozlišení
obrazovky	obrazovka	k1gFnSc2	obrazovka
===	===	k?	===
</s>
</p>
<p>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
neboli	neboli	k8xC	neboli
pixelech	pixel	k1gInPc6	pixel
(	(	kIx(	(
<g/>
px	px	k?	px
<g/>
)	)	kIx)	)
–	–	k?	–
u	u	k7c2	u
LCD	LCD	kA	LCD
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skutečný	skutečný	k2eAgInSc4d1	skutečný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
tohoto	tento	k3xDgNnSc2	tento
rozlišení	rozlišení	k1gNnSc2	rozlišení
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
deformacím	deformace	k1gFnPc3	deformace
obrazu	obraz	k1gInSc2	obraz
<g/>
;	;	kIx,	;
u	u	k7c2	u
CRT	CRT	kA	CRT
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
maximální	maximální	k2eAgInSc4d1	maximální
zobrazitelný	zobrazitelný	k2eAgInSc4d1	zobrazitelný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
maximální	maximální	k2eAgFnSc7d1	maximální
vstupní	vstupní	k2eAgFnSc7d1	vstupní
frekvencí	frekvence	k1gFnSc7	frekvence
(	(	kIx(	(
<g/>
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovovací	obnovovací	k2eAgFnPc4d1	obnovovací
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgFnPc4d1	vertikální
<g/>
)	)	kIx)	)
frekvence	frekvence	k1gFnPc4	frekvence
===	===	k?	===
</s>
</p>
<p>
<s>
Obnovovací	obnovovací	k2eAgFnSc1d1	obnovovací
frekvence	frekvence	k1gFnSc1	frekvence
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
Hertz	hertz	k1gInSc1	hertz
(	(	kIx(	(
<g/>
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
–	–	k?	–
jako	jako	k8xS	jako
rozumné	rozumný	k2eAgNnSc4d1	rozumné
ergonomické	ergonomický	k2eAgNnSc4d1	ergonomické
minimum	minimum	k1gNnSc4	minimum
pro	pro	k7c4	pro
CRT	CRT	kA	CRT
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
===	===	k?	===
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
milisekund	milisekunda	k1gFnPc2	milisekunda
(	(	kIx(	(
<g/>
ms	ms	k?	ms
<g/>
)	)	kIx)	)
–	–	k?	–
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
bod	bod	k1gInSc1	bod
na	na	k7c6	na
LCD	LCD	kA	LCD
monitoru	monitor	k1gInSc2	monitor
rozsvítí	rozsvítit	k5eAaPmIp3nS	rozsvítit
a	a	k8xC	a
zhasne	zhasnout	k5eAaPmIp3nS	zhasnout
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgNnSc4d1	pracovní
využití	využití	k1gNnSc4	využití
je	být	k5eAaImIp3nS	být
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
doba	doba	k1gFnSc1	doba
8	[number]	k4	8
ms	ms	k?	ms
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
výrobci	výrobce	k1gMnPc1	výrobce
udávají	udávat	k5eAaImIp3nP	udávat
parametr	parametr	k1gInSc4	parametr
podobný	podobný	k2eAgInSc4d1	podobný
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šedé	šedá	k1gFnSc2	šedá
do	do	k7c2	do
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
skutečná	skutečný	k2eAgFnSc1d1	skutečná
odezva	odezva	k1gFnSc1	odezva
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgFnSc1d2	horší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vstupy	vstup	k1gInPc4	vstup
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
vstupy	vstup	k1gInPc1	vstup
D-sub	Duba	k1gFnPc2	D-suba
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
pinový	pinový	k2eAgInSc1d1	pinový
<g/>
,	,	kIx,	,
analogový	analogový	k2eAgInSc1d1	analogový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DVI	DVI	kA	DVI
(	(	kIx(	(
<g/>
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
digitální	digitální	k2eAgInSc1d1	digitální
a	a	k8xC	a
analogový	analogový	k2eAgInSc1d1	analogový
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
HDMI	HDMI	kA	HDMI
(	(	kIx(	(
<g/>
digitální	digitální	k2eAgFnSc4d1	digitální
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
videa	video	k1gNnSc2	video
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
,	,	kIx,	,
zpětně	zpětně	k6eAd1	zpětně
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
s	s	k7c7	s
DVI	DVI	kA	DVI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
monitory	monitor	k1gInPc1	monitor
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
ještě	ještě	k9	ještě
oddělené	oddělený	k2eAgInPc1d1	oddělený
RGB	RGB	kA	RGB
(	(	kIx(	(
<g/>
analogové	analogový	k2eAgInPc4d1	analogový
<g/>
)	)	kIx)	)
vstupy	vstup	k1gInPc4	vstup
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
parametry	parametr	k1gInPc1	parametr
===	===	k?	===
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
zajímavými	zajímavý	k2eAgInPc7d1	zajímavý
parametry	parametr	k1gInPc7	parametr
jsou	být	k5eAaImIp3nP	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
spotřeba	spotřeba	k1gFnSc1	spotřeba
udávaná	udávaný	k2eAgFnSc1d1	udávaná
ve	v	k7c6	v
Wattech	watt	k1gInPc6	watt
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
-	-	kIx~	-
u	u	k7c2	u
LCD	LCD	kA	LCD
je	být	k5eAaImIp3nS	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
až	až	k8xS	až
třetinová	třetinový	k2eAgFnSc1d1	třetinová
proti	proti	k7c3	proti
CRT	CRT	kA	CRT
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
úhlopříčce	úhlopříčka	k1gFnSc6	úhlopříčka
<g/>
,	,	kIx,	,
spotřeba	spotřeba	k1gFnSc1	spotřeba
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
rozteč	rozteč	k1gFnSc1	rozteč
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
monitoru	monitor	k1gInSc2	monitor
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
hlubší	hluboký	k2eAgFnSc1d2	hlubší
než	než	k8xS	než
LCD	LCD	kA	LCD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozorovací	pozorovací	k2eAgInPc1d1	pozorovací
úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnSc1	technologie
zobrazení	zobrazení	k1gNnSc2	zobrazení
==	==	k?	==
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
televizoru	televizor	k1gInSc2	televizor
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
technologií	technologie	k1gFnPc2	technologie
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
zobrazování	zobrazování	k1gNnSc3	zobrazování
obrazových	obrazový	k2eAgNnPc2d1	obrazové
dat	datum	k1gNnPc2	datum
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Plochý	plochý	k2eAgInSc1d1	plochý
displej	displej	k1gInSc1	displej
LCD	LCD	kA	LCD
Liquid	Liquid	k1gInSc1	Liquid
crystal	crystat	k5eAaImAgInS	crystat
display	displaa	k1gFnPc4	displaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
obrazovka	obrazovka	k1gFnSc1	obrazovka
–	–	k?	–
CRT	CRT	kA	CRT
<g/>
,	,	kIx,	,
Cathode	Cathod	k1gInSc5	Cathod
ray	ray	k?	ray
tube	tubus	k1gInSc5	tubus
</s>
</p>
<p>
<s>
Plazmová	plazmový	k2eAgFnSc1d1	plazmová
obrazovka	obrazovka	k1gFnSc1	obrazovka
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
projektor	projektor	k1gInSc1	projektor
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
CRT	CRT	kA	CRT
projektor	projektor	k1gInSc1	projektor
<g/>
,	,	kIx,	,
LCD	LCD	kA	LCD
projektor	projektor	k1gInSc1	projektor
<g/>
,	,	kIx,	,
Digital	Digital	kA	Digital
Light	Light	k2eAgInSc1d1	Light
Processing	Processing	k1gInSc1	Processing
<g/>
,	,	kIx,	,
LCoS	LCoS	k1gFnSc1	LCoS
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
technologií	technologie	k1gFnPc2	technologie
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
promítání	promítání	k1gNnSc3	promítání
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
projekční	projekční	k2eAgFnSc4d1	projekční
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SED	sed	k1gInSc1	sed
Surface-conduction	Surfaceonduction	k1gInSc1	Surface-conduction
electron-emitter	electronmitter	k1gInSc4	electron-emitter
display	displaa	k1gFnSc2	displaa
</s>
</p>
<p>
<s>
OLED	OLED	kA	OLED
Organic	Organice	k1gFnPc2	Organice
light-emitting	lightmitting	k1gInSc1	light-emitting
diode	diode	k6eAd1	diode
</s>
</p>
<p>
<s>
Penetron	Penetron	k1gInSc1	Penetron
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
vojenských	vojenský	k2eAgInPc6d1	vojenský
stíhacích	stíhací	k2eAgInPc6d1	stíhací
letounech	letoun	k1gInPc6	letoun
</s>
</p>
<p>
<s>
===	===	k?	===
Porovnání	porovnání	k1gNnSc1	porovnání
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
CRT	CRT	kA	CRT
====	====	k?	====
</s>
</p>
<p>
<s>
Klady	klad	k1gInPc1	klad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc1d1	vysoký
kontrastní	kontrastní	k2eAgInSc1d1	kontrastní
poměr	poměr	k1gInSc1	poměr
(	(	kIx(	(
<g/>
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
může	moct	k5eAaImIp3nS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
LCD	LCD	kA	LCD
a	a	k8xC	a
plazmových	plazmový	k2eAgInPc2d1	plazmový
displejů	displej	k1gInPc2	displej
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perfektní	perfektní	k2eAgNnSc4d1	perfektní
nastavení	nastavení	k1gNnSc4	nastavení
činitele	činitel	k1gMnSc2	činitel
gama	gama	k1gNnSc4	gama
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgMnSc1d1	stejný
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
monitory	monitor	k1gInPc1	monitor
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
zejména	zejména	k9	zejména
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
kladu	klad	k1gInSc3	klad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výborné	výborný	k2eAgNnSc1d1	výborné
zobrazení	zobrazení	k1gNnSc1	zobrazení
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc1d1	široký
rozsah	rozsah	k1gInSc1	rozsah
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
úroveň	úroveň	k1gFnSc1	úroveň
zobrazení	zobrazení	k1gNnSc2	zobrazení
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
zobrazit	zobrazit	k5eAaPmF	zobrazit
nativně	nativně	k6eAd1	nativně
několik	několik	k4yIc4	několik
rozlišení	rozlišení	k1gNnPc2	rozlišení
při	při	k7c6	při
různé	různý	k2eAgFnSc6d1	různá
obnovovací	obnovovací	k2eAgFnSc6d1	obnovovací
frekvenci	frekvence	k1gFnSc6	frekvence
</s>
</p>
<p>
<s>
Skoro	skoro	k6eAd1	skoro
nulová	nulový	k2eAgFnSc1d1	nulová
barevná	barevný	k2eAgFnSc1d1	barevná
<g/>
,	,	kIx,	,
saturační	saturační	k2eAgFnSc1d1	saturační
<g/>
,	,	kIx,	,
kontrastová	kontrastový	k2eAgFnSc1d1	kontrastový
či	či	k8xC	či
jasová	jasový	k2eAgFnSc1d1	jasová
deformace	deformace	k1gFnSc1	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Výborné	výborný	k2eAgInPc1d1	výborný
pozorovací	pozorovací	k2eAgInPc1d1	pozorovací
úhly	úhel	k1gInPc1	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
<g/>
,	,	kIx,	,
osvědčená	osvědčený	k2eAgFnSc1d1	osvědčená
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
<g/>
Zápory	zápor	k1gInPc1	zápor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc1d1	velký
rozměry	rozměr	k1gInPc1	rozměr
a	a	k8xC	a
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
"	"	kIx"	"
displej	displej	k1gInSc1	displej
váží	vážit	k5eAaImIp3nS	vážit
přes	přes	k7c4	přes
10	[number]	k4	10
kg	kg	kA	kg
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geometrické	geometrický	k2eAgNnSc1d1	geometrické
zkreslení	zkreslení	k1gNnSc1	zkreslení
u	u	k7c2	u
neplochých	plochý	k2eNgNnPc2d1	plochý
CRT	CRT	kA	CRT
monitorů	monitor	k1gInPc2	monitor
</s>
</p>
<p>
<s>
Starší	starší	k1gMnSc1	starší
CRT	CRT	kA	CRT
monitory	monitor	k1gInPc1	monitor
jsou	být	k5eAaImIp3nP	být
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
vypalování	vypalování	k1gNnSc3	vypalování
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
spotřeba	spotřeba	k1gFnSc1	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
u	u	k7c2	u
LCD	LCD	kA	LCD
displejů	displej	k1gInPc2	displej
</s>
</p>
<p>
<s>
Náchylné	náchylný	k2eAgNnSc1d1	náchylné
efektu	efekt	k1gInSc3	efekt
moire	moir	k1gInSc5	moir
při	při	k7c6	při
vyšších	vysoký	k2eAgNnPc6d2	vyšší
rozlišeních	rozlišení	k1gNnPc6	rozlišení
</s>
</p>
<p>
<s>
Citlivé	citlivý	k2eAgNnSc1d1	citlivé
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
vlhkost	vlhkost	k1gFnSc4	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
</s>
</p>
<p>
<s>
Značná	značný	k2eAgFnSc1d1	značná
citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
rušení	rušení	k1gNnSc4	rušení
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
monitoru	monitor	k1gInSc2	monitor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
transformátory	transformátor	k1gInPc1	transformátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
i	i	k9	i
místní	místní	k2eAgInSc1d1	místní
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bedny	bedna	k1gFnPc1	bedna
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
monitor	monitor	k1gInSc4	monitor
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc4	zdroj
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
riziko	riziko	k1gNnSc4	riziko
imploze	imploze	k1gFnSc2	imploze
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
vakuu	vakuum	k1gNnSc3	vakuum
<g/>
)	)	kIx)	)
při	při	k7c6	při
rozbití	rozbití	k1gNnSc6	rozbití
skleněného	skleněný	k2eAgInSc2d1	skleněný
obalu	obal	k1gInSc2	obal
obrazovky	obrazovka	k1gFnSc2	obrazovka
</s>
</p>
<p>
<s>
Při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
obnovovací	obnovovací	k2eAgFnSc6d1	obnovovací
frekvenci	frekvence	k1gFnSc6	frekvence
viditelně	viditelně	k6eAd1	viditelně
problikává	problikávat	k5eAaImIp3nS	problikávat
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nastavení	nastavení	k1gNnSc1	nastavení
alespoň	alespoň	k9	alespoň
75	[number]	k4	75
Hz	Hz	kA	Hz
a	a	k8xC	a
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
velikosti	velikost	k1gFnSc2	velikost
monitoru	monitor	k1gInSc2	monitor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
i	i	k9	i
na	na	k7c6	na
daném	daný	k2eAgMnSc6d1	daný
člověku	člověk	k1gMnSc6	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
výrobci	výrobce	k1gMnPc1	výrobce
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
omezovat	omezovat	k5eAaImF	omezovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
LCD	LCD	kA	LCD
====	====	k?	====
</s>
</p>
<p>
<s>
Klady	klad	k1gInPc1	klad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kompaktní	kompaktní	k2eAgInSc1d1	kompaktní
a	a	k8xC	a
lehký	lehký	k2eAgInSc1d1	lehký
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
4	[number]	k4	4
kg	kg	kA	kg
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
displeje	displej	k1gInSc2	displej
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
energetická	energetický	k2eAgFnSc1d1	energetická
spotřeba	spotřeba	k1gFnSc1	spotřeba
</s>
</p>
<p>
<s>
Při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
úhlopříčce	úhlopříčka	k1gFnSc6	úhlopříčka
</s>
</p>
<p>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
geometrické	geometrický	k2eAgNnSc1d1	geometrické
zkreslení	zkreslení	k1gNnSc1	zkreslení
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
nativním	nativní	k2eAgNnSc6d1	nativní
nebo	nebo	k8xC	nebo
při	při	k7c6	při
rozlišení	rozlišení	k1gNnSc6	rozlišení
dělitelným	dělitelný	k2eAgFnPc3d1	dělitelná
celočíselně	celočíselně	k6eAd1	celočíselně
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
typu	typ	k1gInSc6	typ
displeje	displej	k1gInSc2	displej
a	a	k8xC	a
nastavení	nastavení	k1gNnSc2	nastavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stabilní	stabilní	k2eAgFnSc1d1	stabilní
</s>
</p>
<p>
<s>
Malé	malé	k1gNnSc1	malé
nebo	nebo	k8xC	nebo
žádné	žádný	k3yNgNnSc4	žádný
problikávání	problikávání	k1gNnSc4	problikávání
</s>
</p>
<p>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
vyzařování	vyzařování	k1gNnSc1	vyzařování
</s>
</p>
<p>
<s>
Nízké	nízký	k2eAgInPc1d1	nízký
pořizovací	pořizovací	k2eAgInPc1d1	pořizovací
nákladyZápory	nákladyZápor	k1gInPc1	nákladyZápor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Malý	malý	k2eAgInSc1d1	malý
kontrastní	kontrastní	k2eAgInSc1d1	kontrastní
poměr	poměr	k1gInSc1	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Omezené	omezený	k2eAgInPc1d1	omezený
pozorovací	pozorovací	k2eAgInPc1d1	pozorovací
úhly	úhel	k1gInPc1	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
saturace	saturace	k1gFnSc2	saturace
<g/>
,	,	kIx,	,
kontrastu	kontrast	k1gInSc2	kontrast
a	a	k8xC	a
světlosti	světlost	k1gFnSc2	světlost
<g/>
,	,	kIx,	,
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nerovnoměrným	rovnoměrný	k2eNgNnSc7d1	nerovnoměrné
podsvícením	podsvícení	k1gNnSc7	podsvícení
displeje	displej	k1gInSc2	displej
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
zkreslení	zkreslení	k1gNnSc3	zkreslení
světlosti	světlost	k1gFnSc2	světlost
zobrazené	zobrazený	k2eAgFnSc2d1	zobrazená
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katastrofálně	katastrofálně	k6eAd1	katastrofálně
špatné	špatný	k2eAgNnSc1d1	špatné
nastavení	nastavení	k1gNnSc1	nastavení
gama	gama	k1gNnSc1	gama
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
pohledu	pohled	k1gInSc6	pohled
ve	v	k7c6	v
svislém	svislý	k2eAgInSc6d1	svislý
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
výrobců	výrobce	k1gMnPc2	výrobce
raději	rád	k6eAd2	rád
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomalejší	pomalý	k2eAgInPc1d2	pomalejší
časy	čas	k1gInPc1	čas
odezvy	odezva	k1gFnSc2	odezva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
rozmazání	rozmazání	k1gNnSc4	rozmazání
a	a	k8xC	a
duchy	duch	k1gMnPc4	duch
v	v	k7c6	v
obrazu	obraz	k1gInSc6	obraz
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
monitorů	monitor	k1gInPc2	monitor
již	již	k6eAd1	již
tento	tento	k3xDgInSc4	tento
neduh	neduh	k1gInSc4	neduh
překonala	překonat	k5eAaPmAgFnS	překonat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
nativní	nativní	k2eAgNnSc1d1	nativní
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
jiného	jiný	k2eAgNnSc2d1	jiné
rozlišení	rozlišení	k1gNnSc2	rozlišení
musí	muset	k5eAaImIp3nS	muset
obraz	obraz	k1gInSc1	obraz
přepočítat	přepočítat	k5eAaPmF	přepočítat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
nativní	nativní	k2eAgNnSc4d1	nativní
rozlišení	rozlišení	k1gNnSc4	rozlišení
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
kvality	kvalita	k1gFnSc2	kvalita
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
poté	poté	k6eAd1	poté
připadá	připadat	k5eAaPmIp3nS	připadat
daný	daný	k2eAgInSc4d1	daný
počet	počet	k1gInSc4	počet
pixelů	pixel	k1gInPc2	pixel
na	na	k7c4	na
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
barevná	barevný	k2eAgFnSc1d1	barevná
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
levných	levný	k2eAgInPc2d1	levný
monitorů	monitor	k1gInPc2	monitor
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zobrazit	zobrazit	k5eAaPmF	zobrazit
režim	režim	k1gInSc1	režim
truecolor	truecolora	k1gFnPc2	truecolora
<g/>
.	.	kIx.	.
<g/>
Hlavně	hlavně	k9	hlavně
u	u	k7c2	u
displejů	displej	k1gInPc2	displej
lepších	dobrý	k2eAgInPc2d2	lepší
než	než	k8xS	než
TNT	TNT	kA	TNT
(	(	kIx(	(
<g/>
TN	TN	kA	TN
<g/>
)	)	kIx)	)
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
"	"	kIx"	"
<g/>
mrtvé	mrtvý	k2eAgInPc4d1	mrtvý
<g/>
"	"	kIx"	"
pixely	pixel	k1gInPc4	pixel
</s>
</p>
<p>
<s>
==	==	k?	==
Někteří	některý	k3yIgMnPc1	některý
významní	významný	k2eAgMnPc1d1	významný
výrobci	výrobce	k1gMnPc1	výrobce
==	==	k?	==
</s>
</p>
<p>
<s>
Acer	Acer	k1gMnSc1	Acer
</s>
</p>
<p>
<s>
Albatron	Albatron	k1gInSc1	Albatron
</s>
</p>
<p>
<s>
Amtran	Amtran	k1gInSc1	Amtran
</s>
</p>
<p>
<s>
AOC	AOC	kA	AOC
</s>
</p>
<p>
<s>
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ASUS	ASUS	kA	ASUS
</s>
</p>
<p>
<s>
BenQ	BenQ	k?	BenQ
</s>
</p>
<p>
<s>
Compal	Compat	k5eAaImAgMnS	Compat
</s>
</p>
<p>
<s>
Coretronics	Coretronics	k6eAd1	Coretronics
</s>
</p>
<p>
<s>
Dell	Dell	kA	Dell
</s>
</p>
<p>
<s>
Delta	delta	k1gFnSc1	delta
</s>
</p>
<p>
<s>
Eizo	Eizo	k6eAd1	Eizo
</s>
</p>
<p>
<s>
Foxconn	Foxconn	k1gMnSc1	Foxconn
</s>
</p>
<p>
<s>
Funai	Funai	k6eAd1	Funai
</s>
</p>
<p>
<s>
Hewlett-Packard	Hewlett-Packard	k1gMnSc1	Hewlett-Packard
</s>
</p>
<p>
<s>
HannStar	HannStar	k1gMnSc1	HannStar
Display	Displaa	k1gFnSc2	Displaa
Corporation	Corporation	k1gInSc1	Corporation
</s>
</p>
<p>
<s>
Hisense	Hisense	k6eAd1	Hisense
</s>
</p>
<p>
<s>
Hitachi	Hitachi	k6eAd1	Hitachi
</s>
</p>
<p>
<s>
Changhong	Changhong	k1gMnSc1	Changhong
</s>
</p>
<p>
<s>
Chingwa	Chingwa	k6eAd1	Chingwa
Picture	Pictur	k1gMnSc5	Pictur
Tubes	Tubesa	k1gFnPc2	Tubesa
</s>
</p>
<p>
<s>
Iiyama	Iiyama	k1gFnSc1	Iiyama
Corporation	Corporation	k1gInSc1	Corporation
</s>
</p>
<p>
<s>
JVC	JVC	kA	JVC
</s>
</p>
<p>
<s>
LaCie	LaCie	k1gFnSc1	LaCie
</s>
</p>
<p>
<s>
Lenovo	Lenovo	k1gNnSc1	Lenovo
</s>
</p>
<p>
<s>
Lite-On	Lite-On	k1gMnSc1	Lite-On
</s>
</p>
<p>
<s>
LG	LG	kA	LG
Electronics	Electronics	k1gInSc1	Electronics
</s>
</p>
<p>
<s>
NEC	NEC	kA	NEC
Display	Displaa	k1gFnPc1	Displaa
Solutions	Solutions	k1gInSc4	Solutions
</s>
</p>
<p>
<s>
Panasonic	Panasonic	kA	Panasonic
</s>
</p>
<p>
<s>
Philips	Philips	kA	Philips
</s>
</p>
<p>
<s>
Proview	Proview	k?	Proview
</s>
</p>
<p>
<s>
Sampo	Sampa	k1gFnSc5	Sampa
</s>
</p>
<p>
<s>
Samsung	Samsung	kA	Samsung
</s>
</p>
<p>
<s>
Sanyo	Sanyo	k6eAd1	Sanyo
</s>
</p>
<p>
<s>
Sharp	Sharp	kA	Sharp
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
</s>
</p>
<p>
<s>
Syntax	syntax	k1gFnSc1	syntax
</s>
</p>
<p>
<s>
Tatung	Tatung	k1gMnSc1	Tatung
</s>
</p>
<p>
<s>
Teco	Teco	k6eAd1	Teco
</s>
</p>
<p>
<s>
Toshiba	Toshiba	kA	Toshiba
</s>
</p>
<p>
<s>
TTE	TTE	kA	TTE
</s>
</p>
<p>
<s>
ViewSonic	ViewSonice	k1gFnPc2	ViewSonice
</s>
</p>
<p>
<s>
Visionbank	Visionbank	k6eAd1	Visionbank
</s>
</p>
<p>
<s>
Westinghouse	Westinghouse	k6eAd1	Westinghouse
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obrazovka	obrazovka	k1gFnSc1	obrazovka
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Displej	displej	k1gInSc1	displej
z	z	k7c2	z
tekutých	tekutý	k2eAgInPc2d1	tekutý
krystalů	krystal	k1gInPc2	krystal
(	(	kIx(	(
<g/>
LCD	LCD	kA	LCD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barevná	barevný	k2eAgFnSc1d1	barevná
hloubka	hloubka	k1gFnSc1	hloubka
</s>
</p>
<p>
<s>
Barevná	barevný	k2eAgFnSc1d1	barevná
teplota	teplota	k1gFnSc1	teplota
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
monitor	monitor	k1gInSc1	monitor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
monitor	monitor	k1gInSc1	monitor
<g/>
:	:	kIx,	:
úhlopříčka	úhlopříčka	k1gFnSc1	úhlopříčka
vs	vs	k?	vs
rozlišení	rozlišení	k1gNnSc1	rozlišení
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
LCD	LCD	kA	LCD
monitoru	monitor	k1gInSc3	monitor
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
LCD	LCD	kA	LCD
jako	jako	k8xS	jako
LCD	LCD	kA	LCD
<g/>
,	,	kIx,	,
kvalita	kvalita	k1gFnSc1	kvalita
obrazu	obraz	k1gInSc2	obraz
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
použité	použitý	k2eAgFnSc6d1	použitá
technologii	technologie	k1gFnSc6	technologie
(	(	kIx(	(
<g/>
TN	TN	kA	TN
<g/>
,	,	kIx,	,
PVA	PVA	kA	PVA
nebo	nebo	k8xC	nebo
IPS	IPS	kA	IPS
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Visual	Visual	k1gInSc1	Visual
display	displaa	k1gFnSc2	displaa
unit	unita	k1gFnPc2	unita
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
