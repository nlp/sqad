<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Kierling	Kierling	k1gInSc1	Kierling
u	u	k7c2	u
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
;	;	kIx,	;
zřídka	zřídka	k6eAd1	zřídka
též	též	k9	též
František	František	k1gMnSc1	František
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
židovským	židovský	k2eAgNnSc7d1	Židovské
jménem	jméno	k1gNnSc7	jméno
Anschel	Anschela	k1gFnPc2	Anschela
<g/>
,	,	kIx,	,
א	א	k?	א
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
pražský	pražský	k2eAgMnSc1d1	pražský
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
literárně	literárně	k6eAd1	literárně
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Pražského	pražský	k2eAgInSc2d1	pražský
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
tři	tři	k4xCgInPc4	tři
romány	román	k1gInPc4	román
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
povídku	povídka	k1gFnSc4	povídka
Proměna	proměna	k1gFnSc1	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k8xC	jako
úředník	úředník	k1gMnSc1	úředník
u	u	k7c2	u
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nekuřák	nekuřák	k1gMnSc1	nekuřák
<g/>
,	,	kIx,	,
abstinent	abstinent	k1gMnSc1	abstinent
a	a	k8xC	a
vegetarián	vegetarián	k1gMnSc1	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Oskaru	Oskar	k1gMnSc3	Oskar
Pollakovi	Pollak	k1gMnSc3	Pollak
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
nepustí	pustit	k5eNaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Tebe	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
ani	ani	k9	ani
mě	já	k3xPp1nSc2	já
<g/>
.	.	kIx.	.
</s>
<s>
Tahle	tenhle	k3xDgFnSc1	tenhle
matička	matička	k1gFnSc1	matička
má	mít	k5eAaImIp3nS	mít
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
musí	muset	k5eAaImIp3nS	muset
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-	-	kIx~	-
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
bychom	by	kYmCp1nP	by
ji	on	k3xPp3gFnSc4	on
museli	muset	k5eAaImAgMnP	muset
podpálit	podpálit	k5eAaPmF	podpálit
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
a	a	k8xC	a
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
snad	snad	k9	snad
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
židovského	židovský	k2eAgMnSc2d1	židovský
velkoobchodníka	velkoobchodník	k1gMnSc2	velkoobchodník
s	s	k7c7	s
galanterií	galanterie	k1gFnSc7	galanterie
Hermanna	Hermann	k1gMnSc2	Hermann
Kafky	Kafka	k1gMnSc2	Kafka
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
Kafkové	Kafková	k1gFnSc2	Kafková
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Löwyové	Löwyová	k1gFnSc2	Löwyová
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
v	v	k7c6	v
září	září	k1gNnSc6	září
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelův	spisovatelův	k2eAgInSc1d1	spisovatelův
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
zvaný	zvaný	k2eAgInSc1d1	zvaný
U	u	k7c2	u
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
Zum	Zum	k1gMnSc1	Zum
Turm	Turm	k1gMnSc1	Turm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
dnešních	dnešní	k2eAgFnPc2d1	dnešní
ulic	ulice	k1gFnPc2	ulice
Maiselovy	Maiselův	k2eAgFnSc2d1	Maiselova
a	a	k8xC	a
U	u	k7c2	u
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
rohový	rohový	k2eAgInSc1d1	rohový
činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
čp.	čp.	k?	čp.
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Maiselova	Maiselův	k2eAgInSc2d1	Maiselův
čp.	čp.	k?	čp.
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
částečně	částečně	k6eAd1	částečně
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
původní	původní	k2eAgFnSc7d1	původní
podobou	podoba	k1gFnSc7	podoba
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
též	též	k9	též
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
portál	portál	k1gInSc1	portál
vyhořelé	vyhořelý	k2eAgFnSc2d1	vyhořelá
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
upomínku	upomínka	k1gFnSc4	upomínka
připevněna	připevněn	k2eAgFnSc1d1	připevněna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
busta	busta	k1gFnSc1	busta
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Karla	Karel	k1gMnSc2	Karel
Hladíka	Hladík	k1gMnSc2	Hladík
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
Kafky	Kafka	k1gMnSc2	Kafka
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
i	i	k9	i
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
náměstí	náměstí	k1gNnSc2	náměstí
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Hermann	Hermann	k1gMnSc1	Hermann
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
na	na	k7c6	na
reklamě	reklama	k1gFnSc6	reklama
na	na	k7c6	na
vývěsních	vývěsní	k2eAgInPc6d1	vývěsní
štítech	štít	k1gInPc6	štít
jako	jako	k8xS	jako
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
českojazyčného	českojazyčný	k2eAgNnSc2d1	českojazyčné
prostředí	prostředí	k1gNnSc2	prostředí
jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
Oseka	Osek	k1gInSc2	Osek
u	u	k7c2	u
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
musel	muset	k5eAaImAgMnS	muset
pomáhat	pomáhat	k5eAaImF	pomáhat
s	s	k7c7	s
výdělkem	výdělek	k1gInSc7	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vesnicích	vesnice	k1gFnPc6	vesnice
okolo	okolo	k7c2	okolo
rodné	rodný	k2eAgFnSc2d1	rodná
obce	obec	k1gFnSc2	obec
prodával	prodávat	k5eAaImAgMnS	prodávat
sponky	sponka	k1gFnPc4	sponka
<g/>
,	,	kIx,	,
mašličky	mašlička	k1gFnPc4	mašlička
a	a	k8xC	a
spínací	spínací	k2eAgInPc4d1	spínací
špendlíky	špendlík	k1gInPc4	špendlík
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgInS	chodit
bos	bos	k2eAgInSc1d1	bos
a	a	k8xC	a
omrzly	omrznout	k5eAaPmAgInP	omrznout
mu	on	k3xPp3gMnSc3	on
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
se	se	k3xPyFc4	se
z	z	k7c2	z
podomního	podomní	k2eAgMnSc2d1	podomní
obchodníka	obchodník	k1gMnSc2	obchodník
na	na	k7c4	na
majitele	majitel	k1gMnSc4	majitel
textilního	textilní	k2eAgInSc2d1	textilní
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
a	a	k8xC	a
továrničky	továrnička	k1gFnSc2	továrnička
na	na	k7c4	na
azbest	azbest	k1gInSc4	azbest
<g/>
.	.	kIx.	.
</s>
<s>
Velkoobchod	velkoobchod	k1gInSc1	velkoobchod
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1918	[number]	k4	1918
prodal	prodat	k5eAaPmAgMnS	prodat
příbuznému	příbuzný	k1gMnSc3	příbuzný
své	svůj	k3xOyFgMnPc4	svůj
ženy	žena	k1gFnPc1	žena
Friedrichu	Friedrich	k1gMnSc3	Friedrich
Löwymu	Löwym	k1gInSc2	Löwym
<g/>
,	,	kIx,	,
vývěsní	vývěsní	k2eAgInSc4d1	vývěsní
štít	štít	k1gInSc4	štít
se	se	k3xPyFc4	se
však	však	k9	však
ani	ani	k9	ani
poté	poté	k6eAd1	poté
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
utržené	utržený	k2eAgInPc4d1	utržený
peníze	peníz	k1gInPc4	peníz
koupil	koupit	k5eAaPmAgMnS	koupit
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Bílkově	Bílkův	k2eAgFnSc6d1	Bílkova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dům	dům	k1gInSc4	dům
tehdy	tehdy	k6eAd1	tehdy
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
částce	částka	k1gFnSc6	částka
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Hermannovi	Hermannův	k2eAgMnPc1d1	Hermannův
bratři	bratr	k1gMnPc1	bratr
Filip	Filip	k1gMnSc1	Filip
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
a	a	k8xC	a
Heinrich	Heinrich	k1gMnSc1	Heinrich
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
galanterním	galanterní	k2eAgNnSc7d1	galanterní
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Franz	Franz	k1gMnSc1	Franz
nevycházel	vycházet	k5eNaImAgMnS	vycházet
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
nejen	nejen	k6eAd1	nejen
jeho	jeho	k3xOp3gNnSc1	jeho
dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
Dopis	dopis	k1gInSc1	dopis
otci	otec	k1gMnSc6	otec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Julie	Julie	k1gFnSc1	Julie
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
(	(	kIx(	(
<g/>
Jiřího	Jiří	k1gMnSc2	Jiří
náměstí	náměstí	k1gNnSc4	náměstí
čp.	čp.	k?	čp.
17	[number]	k4	17
<g/>
/	/	kIx~	/
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
na	na	k7c6	na
domě	dům	k1gInSc6	dům
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
busta	busta	k1gFnSc1	busta
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
od	od	k7c2	od
poděbradského	poděbradský	k2eAgMnSc2d1	poděbradský
rodáka	rodák	k1gMnSc2	rodák
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Pichla	Pichla	k1gMnSc2	Pichla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc2d1	situovaná
českožidovské	českožidovský	k2eAgFnSc2d1	českožidovská
rodiny	rodina	k1gFnSc2	rodina
asimilované	asimilovaný	k2eAgFnSc2d1	asimilovaná
k	k	k7c3	k
němectví	němectví	k1gNnSc3	němectví
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
měl	mít	k5eAaImAgMnS	mít
pět	pět	k4xCc4	pět
mladších	mladý	k2eAgMnPc2d2	mladší
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
Georg	Georg	k1gMnSc1	Georg
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
a	a	k8xC	a
Heinrich	Heinrich	k1gMnSc1	Heinrich
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
Gabriele	Gabriela	k1gFnSc6	Gabriela
(	(	kIx(	(
<g/>
Elli	Elli	k1gNnSc1	Elli
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c4	za
Karla	Karel	k1gMnSc4	Karel
Hermanna	Hermann	k1gMnSc4	Hermann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Valerie	Valerie	k1gFnSc1	Valerie
(	(	kIx(	(
<g/>
Valli	Vall	k1gMnPc1	Vall
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c4	za
Josefa	Josef	k1gMnSc4	Josef
Pollaka	Pollak	k1gMnSc4	Pollak
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ottilie	Ottilie	k1gFnSc1	Ottilie
(	(	kIx(	(
<g/>
Ottla	Ottla	k1gMnSc1	Ottla
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
českého	český	k2eAgMnSc2d1	český
právníka	právník	k1gMnSc2	právník
nežidovského	židovský	k2eNgInSc2d1	nežidovský
původu	původ	k1gInSc2	původ
Josefa	Josef	k1gMnSc4	Josef
Davida	David	k1gMnSc4	David
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Minuty	minuta	k1gFnSc2	minuta
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
čp.	čp.	k?	čp.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
oběťmi	oběť	k1gFnPc7	oběť
nacistického	nacistický	k2eAgInSc2d1	nacistický
teroru	teror	k1gInSc2	teror
a	a	k8xC	a
zemřely	zemřít	k5eAaPmAgFnP	zemřít
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Nejmilejší	milý	k2eAgFnSc7d3	nejmilejší
sestrou	sestra	k1gFnSc7	sestra
byla	být	k5eAaImAgFnS	být
Ottilie	Ottilie	k1gFnSc1	Ottilie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
na	na	k7c6	na
statku	statek	k1gInSc6	statek
v	v	k7c6	v
Siřemi	Siře	k1gFnPc7	Siře
strávil	strávit	k5eAaPmAgMnS	strávit
osm	osm	k4xCc4	osm
spokojených	spokojený	k2eAgInPc2d1	spokojený
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ottilie	Ottilie	k1gFnSc1	Ottilie
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgMnSc3	svůj
manželovi	manžel	k1gMnSc3	manžel
Josefu	Josef	k1gMnSc3	Josef
Davidovi	David	k1gMnSc3	David
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
naučila	naučit	k5eAaPmAgFnS	naučit
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dcery	dcera	k1gFnPc4	dcera
Věra	Věra	k1gFnSc1	Věra
(	(	kIx(	(
<g/>
Saudková	Saudkový	k2eAgFnSc1d1	Saudková
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
lékařka	lékařka	k1gFnSc1	lékařka
v	v	k7c6	v
Kašperských	kašperský	k2eAgFnPc6d1	Kašperská
Horách	hora	k1gFnPc6	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
(	(	kIx(	(
<g/>
Kostrouchová	Kostrouchová	k1gFnSc1	Kostrouchová
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
nakladatelská	nakladatelský	k2eAgFnSc1d1	nakladatelská
redaktorka	redaktorka	k1gFnSc1	redaktorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Ottla	Ottla	k1gMnSc1	Ottla
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
uchránila	uchránit	k5eAaPmAgFnS	uchránit
<g/>
,	,	kIx,	,
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
norimberských	norimberský	k2eAgInPc2d1	norimberský
zákonů	zákon	k1gInPc2	zákon
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Dcery	dcera	k1gFnPc1	dcera
holokaust	holokaust	k1gInSc4	holokaust
přežily	přežít	k5eAaPmAgFnP	přežít
<g/>
,	,	kIx,	,
Ottla	Ottla	k1gFnSc1	Ottla
takové	takový	k3xDgNnSc4	takový
štěstí	štěstí	k1gNnSc4	štěstí
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tam	tam	k6eAd1	tam
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
byla	být	k5eAaImAgFnS	být
deportována	deportován	k2eAgFnSc1d1	deportována
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
transportem	transport	k1gInSc7	transport
AAw	AAw	k1gFnPc2	AAw
s	s	k7c7	s
registračním	registrační	k2eAgNnSc7d1	registrační
číslem	číslo	k1gNnSc7	číslo
5285	[number]	k4	5285
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
dětská	dětský	k2eAgFnSc1d1	dětská
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1943	[number]	k4	1943
se	se	k3xPyFc4	se
obětavě	obětavě	k6eAd1	obětavě
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
transport	transport	k1gInSc4	transport
1200	[number]	k4	1200
polských	polský	k2eAgFnPc2d1	polská
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
Bialystoku	Bialystok	k1gInSc2	Bialystok
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
opatrovníci	opatrovník	k1gMnPc1	opatrovník
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
zplynováni	zplynován	k2eAgMnPc1d1	zplynován
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
Siegfried	Siegfried	k1gMnSc1	Siegfried
Löwy	Löwa	k1gFnSc2	Löwa
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
-	-	kIx~	-
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Franzovy	Franzův	k2eAgFnSc2d1	Franzova
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
provozoval	provozovat	k5eAaImAgMnS	provozovat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
praxi	praxe	k1gFnSc4	praxe
v	v	k7c6	v
Třešti	třeštit	k5eAaImRp2nS	třeštit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praxi	praxe	k1gFnSc4	praxe
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Třešti	třeštit	k5eAaImRp2nS	třeštit
jako	jako	k8xS	jako
penzista	penzista	k1gMnSc1	penzista
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
jezdil	jezdit	k5eAaImAgInS	jezdit
na	na	k7c4	na
letní	letní	k2eAgFnPc4d1	letní
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Franzovi	Franzův	k2eAgMnPc1d1	Franzův
býval	bývat	k5eAaImAgInS	bývat
dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
a	a	k8xC	a
rádcem	rádce	k1gMnSc7	rádce
v	v	k7c6	v
letech	let	k1gInPc6	let
dospívání	dospívání	k1gNnSc2	dospívání
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
zdravotním	zdravotní	k2eAgMnSc7d1	zdravotní
rádcem	rádce	k1gMnSc7	rádce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
osudného	osudný	k2eAgNnSc2d1	osudné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Stihl	stihnout	k5eAaPmAgMnS	stihnout
jej	on	k3xPp3gInSc4	on
podobný	podobný	k2eAgInSc4d1	podobný
osud	osud	k1gInSc4	osud
jako	jako	k8xC	jako
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
nacisty	nacista	k1gMnSc2	nacista
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nezemřel	zemřít	k5eNaPmAgMnS	zemřít
však	však	k9	však
přímo	přímo	k6eAd1	přímo
jejich	jejich	k3xOp3gFnSc7	jejich
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svého	svůj	k3xOyFgInSc2	svůj
transportu	transport	k1gInSc2	transport
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přibližně	přibližně	k6eAd1	přibližně
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
nejprve	nejprve	k6eAd1	nejprve
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Německou	německý	k2eAgFnSc4d1	německá
chlapeckou	chlapecký	k2eAgFnSc4d1	chlapecká
obecnou	obecný	k2eAgFnSc4d1	obecná
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Masné	masný	k2eAgFnSc6d1	Masná
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
1000	[number]	k4	1000
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Německé	německý	k2eAgNnSc1d1	německé
státní	státní	k2eAgNnSc1d1	státní
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
zadním	zadní	k2eAgInSc6d1	zadní
traktu	trakt	k1gInSc6	trakt
paláce	palác	k1gInSc2	palác
Golz-Kinských	Golz-Kinský	k2eAgInPc2d1	Golz-Kinský
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
čp.	čp.	k?	čp.
606	[number]	k4	606
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
až	až	k9	až
1906	[number]	k4	1906
studoval	studovat	k5eAaImAgInS	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1	Karlo-Ferdinandova
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
také	také	k9	také
přednášky	přednáška	k1gFnPc4	přednáška
germanistiky	germanistika	k1gFnSc2	germanistika
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
Brodem	Brod	k1gInSc7	Brod
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
studentem	student	k1gMnSc7	student
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
přátelství	přátelství	k1gNnSc2	přátelství
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
Kafka	Kafka	k1gMnSc1	Kafka
promoval	promovat	k5eAaBmAgMnS	promovat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
doktorem	doktor	k1gMnSc7	doktor
práv	práv	k2eAgMnSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
roku	rok	k1gInSc2	rok
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
u	u	k7c2	u
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Assicurazioni	Assicurazion	k1gMnPc1	Assicurazion
Generali	Generali	k1gFnPc2	Generali
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
čp.	čp.	k?	čp.
832	[number]	k4	832
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
místu	místo	k1gNnSc3	místo
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
filiálce	filiálka	k1gFnSc6	filiálka
mu	on	k3xPp3gNnSc3	on
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
protekce	protekce	k1gFnSc1	protekce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1908	[number]	k4	1908
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
předčasného	předčasný	k2eAgNnSc2d1	předčasné
penzionování	penzionování	k1gNnSc2	penzionování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Dělnické	dělnický	k2eAgFnSc6d1	Dělnická
úrazové	úrazový	k2eAgFnSc6d1	Úrazová
pojišťovně	pojišťovna	k1gFnSc6	pojišťovna
pro	pro	k7c4	pro
Království	království	k1gNnSc4	království
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
Arbeiter-Unfall-Versicherungs-Anstalt	Arbeiter-Unfall-Versicherungs-Anstalta	k1gFnPc2	Arbeiter-Unfall-Versicherungs-Anstalta
für	für	k?	für
das	das	k?	das
Königreich	Königreich	k1gInSc1	Königreich
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
)	)	kIx)	)
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
čp.	čp.	k?	čp.
1075	[number]	k4	1075
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
koncipient	koncipient	k1gMnSc1	koncipient
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
pozicích	pozice	k1gFnPc6	pozice
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
u	u	k7c2	u
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
i	i	k8xC	i
nadřízených	nadřízený	k1gMnPc2	nadřízený
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
i	i	k9	i
nesporného	sporný	k2eNgInSc2d1	nesporný
kariérního	kariérní	k2eAgInSc2d1	kariérní
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
ambivalentní	ambivalentní	k2eAgFnSc1d1	ambivalentní
vztah	vztah	k1gInSc1	vztah
-	-	kIx~	-
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
moje	můj	k3xOp1gFnSc1	můj
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
nesnesitelné	snesitelný	k2eNgFnPc1d1	nesnesitelná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odporuje	odporovat	k5eAaImIp3nS	odporovat
...	...	k?	...
mému	můj	k3xOp1gNnSc3	můj
jedinému	jediné	k1gNnSc3	jediné
povolání	povolání	k1gNnSc1	povolání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
literatura	literatura	k1gFnSc1	literatura
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
však	však	k9	však
najít	najít	k5eAaPmF	najít
i	i	k9	i
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vysvítá	vysvítat	k5eAaImIp3nS	vysvítat
hrdost	hrdost	k1gFnSc1	hrdost
nad	nad	k7c7	nad
vykonanou	vykonaný	k2eAgFnSc7d1	vykonaná
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
dosaženou	dosažený	k2eAgFnSc7d1	dosažená
pozicí	pozice	k1gFnSc7	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
texty	text	k1gInPc4	text
v	v	k7c6	v
mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
časopise	časopis	k1gInSc6	časopis
Hyperion	Hyperion	k1gInSc1	Hyperion
a	a	k8xC	a
v	v	k7c6	v
Brodově	brodově	k6eAd1	brodově
almanachu	almanach	k1gInSc3	almanach
Arkadia	Arkadium	k1gNnSc2	Arkadium
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
povídky	povídka	k1gFnPc4	povídka
veřejně	veřejně	k6eAd1	veřejně
četl	číst	k5eAaImAgMnS	číst
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
byl	být	k5eAaImAgMnS	být
Kafka	Kafka	k1gMnSc1	Kafka
vždy	vždy	k6eAd1	vždy
velmi	velmi	k6eAd1	velmi
pečlivě	pečlivě	k6eAd1	pečlivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenápadně	nápadně	k6eNd1	nápadně
oblečen	oblečen	k2eAgMnSc1d1	oblečen
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
vulgární	vulgární	k2eAgInPc4d1	vulgární
vtipy	vtip	k1gInPc4	vtip
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
usměvavý	usměvavý	k2eAgInSc1d1	usměvavý
<g/>
,	,	kIx,	,
laskavý	laskavý	k2eAgInSc1d1	laskavý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
snad	snad	k9	snad
jedinou	jediný	k2eAgFnSc7d1	jediná
chybou	chyba	k1gFnSc7	chyba
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podceňoval	podceňovat	k5eAaImAgMnS	podceňovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1917	[number]	k4	1917
začal	začít	k5eAaPmAgMnS	začít
trpět	trpět	k5eAaImF	trpět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Schönbornském	Schönbornský	k2eAgInSc6d1	Schönbornský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Tržiště	tržiště	k1gNnSc1	tržiště
čp.	čp.	k?	čp.
365	[number]	k4	365
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
dostává	dostávat	k5eAaImIp3nS	dostávat
tříměsíční	tříměsíční	k2eAgNnSc4d1	tříměsíční
volno	volno	k1gNnSc4	volno
na	na	k7c4	na
zotavenou	zotavená	k1gFnSc4	zotavená
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Siřem	Siřem	k1gInSc1	Siřem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Ottla	Ottla	k1gFnSc1	Ottla
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
Josefa	Josef	k1gMnSc2	Josef
Davida	David	k1gMnSc2	David
<g/>
,	,	kIx,	,
manžela	manžel	k1gMnSc2	manžel
nežidovského	židovský	k2eNgInSc2d1	nežidovský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
tu	tu	k6eAd1	tu
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
několik	několik	k4yIc1	několik
stavení	stavení	k1gNnPc2	stavení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Siřem	Siřem	k1gInSc1	Siřem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
považují	považovat	k5eAaImIp3nP	považovat
někteří	některý	k3yIgMnPc1	některý
literární	literární	k2eAgMnPc1d1	literární
historici	historik	k1gMnPc1	historik
za	za	k7c4	za
inspiraci	inspirace	k1gFnSc4	inspirace
Kafkova	Kafkův	k2eAgInSc2d1	Kafkův
enigmatického	enigmatický	k2eAgInSc2d1	enigmatický
Zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
jej	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
opustil	opustit	k5eAaPmAgMnS	opustit
Kafka	Kafka	k1gMnSc1	Kafka
své	svůj	k3xOyFgNnSc4	svůj
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
v	v	k7c6	v
pojišťovně	pojišťovna	k1gFnSc6	pojišťovna
definitivně	definitivně	k6eAd1	definitivně
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
životě	život	k1gInSc6	život
hrála	hrát	k5eAaImAgFnS	hrát
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
překladatelka	překladatelka	k1gFnSc1	překladatelka
jeho	jeho	k3xOp3gFnSc2	jeho
prózy	próza	k1gFnSc2	próza
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
udržovali	udržovat	k5eAaImAgMnP	udržovat
spolu	spolu	k6eAd1	spolu
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
korespondenční	korespondenční	k2eAgInSc4d1	korespondenční
styk	styk	k1gInSc4	styk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jesenská	Jesenská	k1gFnSc1	Jesenská
seznámila	seznámit	k5eAaPmAgFnS	seznámit
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládali	překládat	k5eAaImAgMnP	překládat
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Grmela	Grmela	k1gFnSc1	Grmela
<g/>
,	,	kIx,	,
na	na	k7c4	na
vydání	vydání	k1gNnSc2	vydání
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Florian	Florian	k1gMnSc1	Florian
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
25	[number]	k4	25
<g/>
letou	letý	k2eAgFnSc7d1	letá
Dorou	Dora	k1gFnSc7	Dora
Diamantovou	diamantový	k2eAgFnSc7d1	Diamantová
z	z	k7c2	z
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
3	[number]	k4	3
adresách	adresa	k1gFnPc6	adresa
<g/>
:	:	kIx,	:
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
neexistující	existující	k2eNgInSc4d1	neexistující
dům	dům	k1gInSc4	dům
na	na	k7c4	na
Steglitzer	Steglitzer	k1gInSc4	Steglitzer
Miquelstraße	Miquelstraß	k1gInSc2	Miquelstraß
8	[number]	k4	8
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
vila	vít	k5eAaImAgFnS	vít
v	v	k7c6	v
Grunewaldstraße	Grunewaldstraße	k1gFnSc6	Grunewaldstraße
13	[number]	k4	13
v	v	k7c6	v
Berlíně-Steglitz	Berlíně-Steglitza	k1gFnPc2	Berlíně-Steglitza
(	(	kIx(	(
<g/>
2	[number]	k4	2
pokoje	pokoj	k1gInSc2	pokoj
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
dnešní	dnešní	k2eAgInSc1d1	dnešní
Zehlendorfer	Zehlendorfer	k1gInSc1	Zehlendorfer
Busseallee	Busseallee	k1gFnSc1	Busseallee
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
byl	být	k5eAaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
sionista	sionista	k1gMnSc1	sionista
<g/>
.	.	kIx.	.
</s>
<s>
Odebíral	odebírat	k5eAaImAgInS	odebírat
pražský	pražský	k2eAgInSc1d1	pražský
sionistický	sionistický	k2eAgInSc1d1	sionistický
list	list	k1gInSc1	list
Selbstwehr	Selbstwehra	k1gFnPc2	Selbstwehra
a	a	k8xC	a
učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kafkových	Kafkových	k2eAgInPc2d1	Kafkových
deníků	deník	k1gInPc2	deník
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
snil	snít	k5eAaImAgMnS	snít
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
izraelské	izraelský	k2eAgInPc1d1	izraelský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyprávění	vyprávění	k1gNnSc2	vyprávění
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
Ottly	Ottla	k1gFnSc2	Ottla
svůj	svůj	k3xOyFgInSc4	svůj
sen	sen	k1gInSc4	sen
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
odejít	odejít	k5eAaPmF	odejít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
Čecha	Čech	k1gMnSc2	Čech
Josefa	Josef	k1gMnSc2	Josef
Davida	David	k1gMnSc2	David
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
milovala	milovat	k5eAaImAgFnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
byl	být	k5eAaImAgMnS	být
Kafka	Kafka	k1gMnSc1	Kafka
zábavný	zábavný	k2eAgMnSc1d1	zábavný
a	a	k8xC	a
citlivý	citlivý	k2eAgMnSc1d1	citlivý
společník	společník	k1gMnSc1	společník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nekuřák	nekuřák	k1gMnSc1	nekuřák
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vegetarián	vegetarián	k1gMnSc1	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
lékaři	lékař	k1gMnPc1	lékař
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kvůli	kvůli	k7c3	kvůli
pokročilé	pokročilý	k2eAgFnSc3d1	pokročilá
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
začal	začít	k5eAaPmAgInS	začít
jíst	jíst	k5eAaImF	jíst
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
vegetariánství	vegetariánství	k1gNnSc4	vegetariánství
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzala	vzít	k5eAaPmAgNnP	vzít
Ottla	Ottlo	k1gNnPc1	Ottlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
zvycích	zvyk	k1gInPc6	zvyk
napsal	napsat	k5eAaPmAgMnS	napsat
Felicii	Felicie	k1gFnSc4	Felicie
Bauerové	Bauerová	k1gFnSc2	Bauerová
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
nepiju	pít	k5eNaImIp1nS	pít
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
kafe	kafe	k?	kafe
a	a	k8xC	a
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
nejím	jíst	k5eNaImIp1nS	jíst
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nutně	nutně	k6eAd1	nutně
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
románové	románový	k2eAgMnPc4d1	románový
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
K.	K.	kA	K.
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kávu	káva	k1gFnSc4	káva
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobových	dobový	k2eAgNnPc2d1	dobové
svědectví	svědectví	k1gNnSc2	svědectví
se	se	k3xPyFc4	se
již	již	k6eAd1	již
za	za	k7c2	za
Rakouska	Rakousko	k1gNnSc2	Rakousko
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
přispíval	přispívat	k5eAaImAgMnS	přispívat
na	na	k7c4	na
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nejméně	málo	k6eAd3	málo
jednou	jednou	k6eAd1	jednou
předveden	předvést	k5eAaPmNgInS	předvést
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
ráno	ráno	k6eAd1	ráno
přijít	přijít	k5eAaPmF	přijít
včas	včas	k6eAd1	včas
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
kauci	kauce	k1gFnSc4	kauce
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
sanatoriích	sanatorium	k1gNnPc6	sanatorium
a	a	k8xC	a
lázních	lázeň	k1gFnPc6	lázeň
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
osm	osm	k4xCc1	osm
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
Matliarech	Matliar	k1gInPc6	Matliar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
i	i	k8xC	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pobýval	pobývat	k5eAaImAgMnS	pobývat
také	také	k9	také
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
rakouském	rakouský	k2eAgNnSc6d1	rakouské
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
italském	italský	k2eAgInSc6d1	italský
Riva	Riv	k2eAgFnSc1d1	Riva
del	del	k?	del
Garda	garda	k1gFnSc1	garda
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
z	z	k7c2	z
nutnosti	nutnost	k1gFnSc2	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Léčil	léčit	k5eAaImAgInS	léčit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
hrtanu	hrtan	k1gInSc2	hrtan
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
v	v	k7c6	v
Kierlingu	Kierling	k1gInSc6	Kierling
u	u	k7c2	u
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
hrobce	hrobka	k1gFnSc6	hrobka
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Žižkově	Žižkov	k1gInSc6	Žižkov
(	(	kIx(	(
<g/>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
v	v	k7c6	v
nacistických	nacistický	k2eAgInPc6d1	nacistický
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
na	na	k7c6	na
území	území	k1gNnSc6	území
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
mají	mít	k5eAaImIp3nP	mít
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
náhrobek	náhrobek	k1gInSc1	náhrobek
byl	být	k5eAaImAgInS	být
navržený	navržený	k2eAgInSc4d1	navržený
architektem	architekt	k1gMnSc7	architekt
Leopoldem	Leopold	k1gMnSc7	Leopold
Ehrmannem	Ehrmann	k1gMnSc7	Ehrmann
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc1	dva
neodeslané	odeslaný	k2eNgInPc1d1	neodeslaný
dopisy	dopis	k1gInPc1	dopis
Maxu	Maxa	k1gMnSc4	Maxa
Brodovi	Brodův	k2eAgMnPc1d1	Brodův
<g/>
)	)	kIx)	)
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
veškerá	veškerý	k3xTgFnSc1	veškerý
jeho	on	k3xPp3gMnSc4	on
neuveřejněná	uveřejněný	k2eNgNnPc1d1	neuveřejněné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
a	a	k8xC	a
deníky	deník	k1gInPc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
poctivě	poctivě	k6eAd1	poctivě
všechny	všechen	k3xTgInPc4	všechen
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
zničila	zničit	k5eAaPmAgFnS	zničit
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
zničilo	zničit	k5eAaPmAgNnS	zničit
berlínské	berlínský	k2eAgNnSc1d1	berlínské
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
závěť	závěť	k1gFnSc4	závěť
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
dílo	dílo	k1gNnSc4	dílo
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Kafkův	Kafkův	k2eAgInSc1d1	Kafkův
poměr	poměr	k1gInSc1	poměr
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
s	s	k7c7	s
několika	několik	k4yIc7	několik
ženami	žena	k1gFnPc7	žena
navázal	navázat	k5eAaPmAgInS	navázat
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
Kafka	Kafka	k1gMnSc1	Kafka
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
úřednicí	úřednice	k1gFnSc7	úřednice
Felice	Felice	k1gFnSc2	Felice
Bauerovou	Bauerová	k1gFnSc7	Bauerová
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dvakrát	dvakrát	k6eAd1	dvakrát
zasnouben	zasnouben	k2eAgMnSc1d1	zasnouben
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
a	a	k8xC	a
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vztah	vztah	k1gInSc1	vztah
definitivně	definitivně	k6eAd1	definitivně
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
životě	život	k1gInSc6	život
byly	být	k5eAaImAgFnP	být
Julie	Julie	k1gFnPc1	Julie
Wohryzková	Wohryzkový	k2eAgFnSc1d1	Wohryzková
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1920	[number]	k4	1920
prožil	prožít	k5eAaPmAgInS	prožít
krátký	krátký	k2eAgInSc1d1	krátký
vztah	vztah	k1gInSc1	vztah
a	a	k8xC	a
zasnoubil	zasnoubit	k5eAaPmAgInS	zasnoubit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gFnSc1	jeho
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
životě	život	k1gInSc6	život
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
družkou	družka	k1gFnSc7	družka
byla	být	k5eAaImAgFnS	být
Dora	Dora	k1gFnSc1	Dora
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
prožil	prožít	k5eAaPmAgMnS	prožít
ještě	ještě	k9	ještě
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
rakouských	rakouský	k2eAgNnPc6d1	rakouské
sanatoriích	sanatorium	k1gNnPc6	sanatorium
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Felice	Felice	k1gFnSc1	Felice
Bauerová	Bauerová	k1gFnSc1	Bauerová
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
řídila	řídit	k5eAaImAgFnS	řídit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
firmu	firma	k1gFnSc4	firma
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
stenografy	stenograf	k1gMnPc4	stenograf
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
trval	trvat	k5eAaImAgInS	trvat
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
byli	být	k5eAaImAgMnP	být
dvakrát	dvakrát	k6eAd1	dvakrát
zasnoubeni	zasnouben	k2eAgMnPc1d1	zasnouben
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
fyzicky	fyzicky	k6eAd1	fyzicky
setkali	setkat	k5eAaPmAgMnP	setkat
pouze	pouze	k6eAd1	pouze
sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1912	[number]	k4	1912
u	u	k7c2	u
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Mladých	mladý	k2eAgInPc2d1	mladý
Goliášů	goliáš	k1gInPc2	goliáš
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Skořepka	skořepka	k1gFnSc1	skořepka
čp.	čp.	k?	čp.
527	[number]	k4	527
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Julie	Julie	k1gFnSc1	Julie
Wohryzková	Wohryzkový	k2eAgFnSc1d1	Wohryzková
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kafkova	Kafkův	k2eAgFnSc1d1	Kafkova
druhá	druhý	k4xOgFnSc1	druhý
snoubenka	snoubenka	k1gFnSc1	snoubenka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
šámesem	šámes	k1gMnSc7	šámes
Vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
ve	v	k7c6	v
Stüdlově	Stüdlův	k2eAgNnSc6d1	Stüdlův
penzinu	penzino	k1gNnSc6	penzino
v	v	k7c6	v
Želízech	Želízy	k1gInPc6	Želízy
<g/>
.	.	kIx.	.
</s>
<s>
Jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zasnoubení	zasnoubení	k1gNnSc1	zasnoubení
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
už	už	k9	už
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
ředitele	ředitel	k1gMnSc2	ředitel
jedné	jeden	k4xCgFnSc2	jeden
pražské	pražský	k2eAgFnSc2d1	Pražská
bankovní	bankovní	k2eAgFnSc2d1	bankovní
filálky	filálka	k1gFnSc2	filálka
Josefa	Josef	k1gMnSc2	Josef
Wernera	Werner	k1gMnSc2	Werner
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
vyhlazovacím	vyhlazovací	k2eAgInSc6d1	vyhlazovací
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
o	o	k7c4	o
13	[number]	k4	13
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
"	"	kIx"	"
<g/>
vyléčená	vyléčený	k2eAgFnSc1d1	vyléčená
soudružka	soudružka	k1gFnSc1	soudružka
<g/>
"	"	kIx"	"
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Ernsta	Ernst	k1gMnSc2	Ernst
Pollacka	Pollacka	k1gFnSc1	Pollacka
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
několik	několik	k4yIc4	několik
Kafkových	Kafkových	k2eAgNnPc2d1	Kafkových
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
Rozjímání	rozjímání	k1gNnSc1	rozjímání
a	a	k8xC	a
Ortel	ortel	k1gInSc1	ortel
<g/>
)	)	kIx)	)
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
korespondenci	korespondence	k1gFnSc6	korespondence
<g/>
,	,	kIx,	,
intenzívně	intenzívně	k6eAd1	intenzívně
si	se	k3xPyFc3	se
psali	psát	k5eAaImAgMnP	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
Dopisů	dopis	k1gInPc2	dopis
Mileně	mileně	k6eAd1	mileně
<g/>
,	,	kIx,	,
pohlednic	pohlednice	k1gFnPc2	pohlednice
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
telegramů	telegram	k1gInPc2	telegram
je	být	k5eAaImIp3nS	být
134	[number]	k4	134
<g/>
.	.	kIx.	.
</s>
<s>
Setkali	setkat	k5eAaPmAgMnP	setkat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
den	den	k1gInSc4	den
v	v	k7c6	v
pohraničním	pohraniční	k2eAgNnSc6d1	pohraniční
městě	město	k1gNnSc6	město
Gmünd	Gmünda	k1gFnPc2	Gmünda
<g/>
.	.	kIx.	.
</s>
<s>
Maxi	Max	k1gMnSc3	Max
Brodovi	Broda	k1gMnSc3	Broda
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
květnu	květno	k1gNnSc6	květno
1920	[number]	k4	1920
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
Ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
živoucí	živoucí	k2eAgInSc4d1	živoucí
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakým	jaký	k3yQgInSc7	jaký
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Milena	Milena	k1gFnSc1	Milena
byla	být	k5eAaImAgFnS	být
vlastně	vlastně	k9	vlastně
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
14	[number]	k4	14
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
Mých	můj	k3xOp1gNnPc2	můj
38	[number]	k4	38
židovských	židovský	k2eAgNnPc2d1	Židovské
let	léto	k1gNnPc2	léto
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
Vašim	vaši	k1gMnPc3	vaši
24	[number]	k4	24
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Franz	Franz	k1gMnSc1	Franz
umřel	umřít	k5eAaPmAgMnS	umřít
<g/>
,	,	kIx,	,
zvěřejnila	zvěřejnit	k5eAaImAgFnS	zvěřejnit
Milena	Milena	k1gFnSc1	Milena
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1924	[number]	k4	1924
nekrolog	nekrolog	k1gInSc1	nekrolog
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
Byl	být	k5eAaImAgInS	být
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
umělcem	umělec	k1gMnSc7	umělec
tak	tak	k6eAd1	tak
úzkostlivého	úzkostlivý	k2eAgNnSc2d1	úzkostlivé
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
druzí	druhý	k4xOgMnPc1	druhý
<g/>
,	,	kIx,	,
hluší	hlušit	k5eAaImIp3nS	hlušit
<g/>
,	,	kIx,	,
cítili	cítit	k5eAaImAgMnP	cítit
se	se	k3xPyFc4	se
bezpečni	bezpečen	k2eAgMnPc1d1	bezpečen
<g/>
.	.	kIx.	.
</s>
<s>
Milena	Milena	k1gFnSc1	Milena
byla	být	k5eAaImAgFnS	být
zatčena	zatknout	k5eAaPmNgFnS	zatknout
za	za	k7c4	za
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
proti	proti	k7c3	proti
nacistům	nacista	k1gMnPc3	nacista
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byla	být	k5eAaImAgFnS	být
vězněna	věznit	k5eAaImNgFnS	věznit
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
a	a	k8xC	a
potom	potom	k6eAd1	potom
ji	on	k3xPp3gFnSc4	on
poslali	poslat	k5eAaPmAgMnP	poslat
do	do	k7c2	do
ženského	ženský	k2eAgInSc2d1	ženský
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Ravensbrück	Ravensbrück	k1gInSc1	Ravensbrück
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
židovské	židovský	k2eAgFnSc2d1	židovská
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
v	v	k7c6	v
přímořském	přímořský	k2eAgNnSc6d1	přímořské
letovisku	letovisko	k1gNnSc6	letovisko
Graal-Müritz	Graal-Müritza	k1gFnPc2	Graal-Müritza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
u	u	k7c2	u
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Meklenbursku-Předním	Meklenbursku-Přední	k2eAgNnSc6d1	Meklenbursku-Přední
Pomořansku	Pomořansko	k1gNnSc6	Pomořansko
<g/>
.	.	kIx.	.
</s>
<s>
Jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
tam	tam	k6eAd1	tam
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
prázdninového	prázdninový	k2eAgInSc2d1	prázdninový
tábora	tábor	k1gInSc2	tábor
berlínského	berlínský	k2eAgInSc2d1	berlínský
Židovského	židovský	k2eAgInSc2d1	židovský
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
Kafka	Kafka	k1gMnSc1	Kafka
žil	žít	k5eAaImAgInS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Kafkovou	Kafkův	k2eAgFnSc7d1	Kafkova
mateřštinou	mateřština	k1gFnSc7	mateřština
byla	být	k5eAaImAgFnS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hovořil	hovořit	k5eAaImAgInS	hovořit
také	také	k9	také
česky	česky	k6eAd1	česky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Mileně	mileně	k6eAd1	mileně
Jesenské	Jesenské	k2eAgFnSc1d1	Jesenské
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1920	[number]	k4	1920
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
mezi	mezi	k7c7	mezi
německým	německý	k2eAgInSc7d1	německý
národem	národ	k1gInSc7	národ
nežil	žít	k5eNaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
mateřštinou	mateřština	k1gFnSc7	mateřština
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
mému	můj	k3xOp1gNnSc3	můj
srdci	srdce	k1gNnSc3	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
kvalita	kvalita	k1gFnSc1	kvalita
výuky	výuka	k1gFnSc2	výuka
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
poskytovaná	poskytovaný	k2eAgFnSc1d1	poskytovaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
byla	být	k5eAaImAgFnS	být
kolísavá	kolísavý	k2eAgFnSc1d1	kolísavá
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hebrejsky	hebrejsky	k6eAd1	hebrejsky
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
neuměl	neuměl	k1gMnSc1	neuměl
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc4d1	moderní
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
se	se	k3xPyFc4	se
Kafka	Kafka	k1gMnSc1	Kafka
později	pozdě	k6eAd2	pozdě
učil	učit	k5eAaImAgMnS	učit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miriam	Miriam	k1gFnSc7	Miriam
Singerovou	Singerová	k1gFnSc7	Singerová
u	u	k7c2	u
Jiřího	Jiří	k1gMnSc2	Jiří
Mordechaje	Mordechaje	k1gMnSc2	Mordechaje
Langera	Langer	k1gMnSc2	Langer
a	a	k8xC	a
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Langerem	Langer	k1gMnSc7	Langer
mluvil	mluvit	k5eAaImAgMnS	mluvit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
o	o	k7c6	o
prvních	první	k4xOgNnPc6	první
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
vznášela	vznášet	k5eAaImAgFnS	vznášet
nad	nad	k7c7	nad
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
bohatla	bohatnout	k5eAaImAgFnS	bohatnout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
bydliště	bydliště	k1gNnPc1	bydliště
nalézala	nalézat	k5eAaImAgNnP	nalézat
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
Staroměstského	staroměstský	k2eAgNnSc2d1	Staroměstské
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
sotva	sotva	k8xS	sotva
jednoho	jeden	k4xCgInSc2	jeden
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Kafkův	Kafkův	k2eAgMnSc1d1	Kafkův
učitel	učitel	k1gMnSc1	učitel
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
Friedrich	Friedrich	k1gMnSc1	Friedrich
Thieberger	Thieberger	k1gMnSc1	Thieberger
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jednou	jednou	k6eAd1	jednou
hleděli	hledět	k5eAaImAgMnP	hledět
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
bytu	byt	k1gInSc2	byt
Kafkových	Kafkových	k2eAgMnPc2d1	Kafkových
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
Oppeltově	Oppeltův	k2eAgInSc6d1	Oppeltův
domě	dům	k1gInSc6	dům
dolů	dol	k1gInPc2	dol
na	na	k7c4	na
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
budovy	budova	k1gFnPc4	budova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
bylo	být	k5eAaImAgNnS	být
mé	můj	k3xOp1gNnSc1	můj
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
kousek	kousek	k1gInSc4	kousek
nalevo	nalevo	k6eAd1	nalevo
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc1	můj
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
malém	malý	k2eAgInSc6d1	malý
kruhu	kruh	k1gInSc6	kruh
<g/>
"	"	kIx"	"
-	-	kIx~	-
a	a	k8xC	a
opsal	opsat	k5eAaPmAgMnS	opsat
prstem	prst	k1gInSc7	prst
pár	pár	k4xCyI	pár
malých	malý	k2eAgInPc2d1	malý
kroužků	kroužek	k1gInPc2	kroužek
-	-	kIx~	-
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
celý	celý	k2eAgInSc4d1	celý
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pražské	pražský	k2eAgFnPc1d1	Pražská
adresy	adresa	k1gFnPc1	adresa
jsou	být	k5eAaImIp3nP	být
očíslovány	očíslován	k2eAgFnPc1d1	očíslována
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
odraženy	odražen	k2eAgInPc1d1	odražen
<g/>
:	:	kIx,	:
rodina	rodina	k1gFnSc1	rodina
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
jednorožce	jednorožec	k1gMnSc2	jednorožec
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
548	[number]	k4	548
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nárožním	nárožní	k2eAgInSc6d1	nárožní
domě	dům	k1gInSc6	dům
bydlela	bydlet	k5eAaImAgFnS	bydlet
matka	matka	k1gFnSc1	matka
Julie	Julie	k1gFnSc1	Julie
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
románský	románský	k2eAgInSc1d1	románský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
stopu	stopa	k1gFnSc4	stopa
zanechal	zanechat	k5eAaPmAgMnS	zanechat
architekt	architekt	k1gMnSc1	architekt
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
část	část	k1gFnSc1	část
hlavního	hlavní	k2eAgInSc2d1	hlavní
portálu	portál	k1gInSc2	portál
a	a	k8xC	a
průjezd	průjezd	k1gInSc4	průjezd
s	s	k7c7	s
žebrovou	žebrový	k2eAgFnSc7d1	žebrová
síťovou	síťový	k2eAgFnSc7d1	síťová
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
sedlovým	sedlový	k2eAgInSc7d1	sedlový
portálkem	portálek	k1gInSc7	portálek
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgFnSc1d1	barokní
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
zřídil	zřídit	k5eAaPmAgMnS	zřídit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1882	[number]	k4	1882
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
hotel	hotel	k1gInSc1	hotel
Goldhammer	Goldhammer	k1gInSc1	Goldhammer
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
928	[number]	k4	928
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
12	[number]	k4	12
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
<g />
.	.	kIx.	.
</s>
<s>
rodiče	rodič	k1gMnPc1	rodič
Hermann	Hermann	k1gMnSc1	Hermann
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
slavili	slavit	k5eAaImAgMnP	slavit
svatbu	svatba	k1gFnSc4	svatba
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
/	/	kIx~	/
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1885	[number]	k4	1885
<g/>
:	:	kIx,	:
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
Zum	Zum	k1gMnSc1	Zum
Turm	Turm	k1gMnSc1	Turm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
čp.	čp.	k?	čp.
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
čp.	čp.	k?	čp.
27	[number]	k4	27
<g/>
/	/	kIx~	/
<g/>
I	i	k9	i
-	-	kIx~	-
Mikulášská	mikulášský	k2eAgFnSc1d1	Mikulášská
9	[number]	k4	9
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
U	u	k7c2	u
Radnice	radnice	k1gFnSc2	radnice
5	[number]	k4	5
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
narodil	narodit	k5eAaPmAgMnS	narodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1883	[number]	k4	1883
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
židovské	židovský	k2eAgFnSc2d1	židovská
tradice	tradice	k1gFnSc2	tradice
obřezán	obřezat	k5eAaPmNgMnS	obřezat
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
prelatura	prelatura	k1gFnSc1	prelatura
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1717	[number]	k4	1717
<g/>
-	-	kIx~	-
<g/>
1730	[number]	k4	1730
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
poničena	poničit	k5eAaPmNgFnS	poničit
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
stržena	stržen	k2eAgNnPc1d1	strženo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
postaven	postavit	k5eAaPmNgInS	postavit
architektem	architekt	k1gMnSc7	architekt
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Kříženeckým	kříženecký	k2eAgMnSc7d1	kříženecký
nový	nový	k2eAgInSc4d1	nový
o	o	k7c4	o
patro	patro	k1gNnSc4	patro
vyšší	vysoký	k2eAgInSc1d2	vyšší
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
byl	být	k5eAaImAgInS	být
zabudován	zabudován	k2eAgInSc1d1	zabudován
původní	původní	k2eAgInSc1d1	původní
portál	portál	k1gInSc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
a	a	k8xC	a
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
3.7	[number]	k4	3.7
<g/>
.1883	.1883	k4	.1883
narodil	narodit	k5eAaPmAgMnS	narodit
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Karel	Karel	k1gMnSc1	Karel
Hladík	Hladík	k1gMnSc1	Hladík
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Kaplický	Kaplický	k2eAgMnSc1d1	Kaplický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
/	/	kIx~	/
1885	[number]	k4	1885
(	(	kIx(	(
<g/>
květen-prosinec	květenrosinec	k1gInSc1	květen-prosinec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
802	[number]	k4	802
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
bratr	bratr	k1gMnSc1	bratr
Georg	Georg	k1gMnSc1	Georg
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
nezachoval	zachovat	k5eNaPmAgInS	zachovat
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
/	/	kIx~	/
1885	[number]	k4	1885
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
-	-	kIx~	-
1887	[number]	k4	1887
<g/>
:	:	kIx,	:
Dušní	dušný	k2eAgMnPc1d1	dušný
konskripční	konskripční	k2eAgMnSc1d1	konskripční
číslo	číslo	k1gNnSc4	číslo
V	v	k7c6	v
<g/>
/	/	kIx~	/
<g/>
187	[number]	k4	187
-	-	kIx~	-
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
bratr	bratr	k1gMnSc1	bratr
Georg	Georg	k1gMnSc1	Georg
na	na	k7c4	na
spalničky	spalničky	k1gFnPc4	spalničky
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1887	[number]	k4	1887
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
bratr	bratr	k1gMnSc1	bratr
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
/	/	kIx~	/
1887	[number]	k4	1887
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
-	-	kIx~	-
1888	[number]	k4	1888
<g/>
:	:	kIx,	:
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
č.	č.	k?	č.
6	[number]	k4	6
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mikulášská	mikulášský	k2eAgFnSc1d1	Mikulášská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pětiměsíční	pětiměsíční	k2eAgMnSc1d1	pětiměsíční
bratr	bratr	k1gMnSc1	bratr
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
/	/	kIx~	/
1888	[number]	k4	1888
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1889	[number]	k4	1889
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Sixtův	Sixtův	k2eAgInSc1d1	Sixtův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Celetná	Celetný	k2eAgFnSc1d1	Celetná
čp.	čp.	k?	čp.
553	[number]	k4	553
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
6	[number]	k4	6
<g/>
/	/	kIx~	/
1889	[number]	k4	1889
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1896	[number]	k4	1896
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
i	i	k8xC	i
první	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
domě	dům	k1gInSc6	dům
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
školní	školní	k2eAgInSc4d1	školní
<g />
.	.	kIx.	.
</s>
<s>
docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
1889	[number]	k4	1889
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1893	[number]	k4	1893
<g/>
:	:	kIx,	:
Německá	německý	k2eAgFnSc1d1	německá
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Masná	masný	k2eAgFnSc1d1	Masná
čp.	čp.	k?	čp.
1000	[number]	k4	1000
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1890	[number]	k4	1890
<g/>
:	:	kIx,	:
Osek	Osek	k1gInSc1	Osek
u	u	k7c2	u
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
-	-	kIx~	-
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
pochován	pochován	k2eAgMnSc1d1	pochován
dědeček	dědeček	k1gMnSc1	dědeček
Jakob	Jakob	k1gMnSc1	Jakob
<g />
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Franz	Franz	k1gMnSc1	Franz
téměř	téměř	k6eAd1	téměř
nepoznal	poznat	k5eNaPmAgMnS	poznat
<g/>
.	.	kIx.	.
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
1893	[number]	k4	1893
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1901	[number]	k4	1901
<g/>
:	:	kIx,	:
Německé	německý	k2eAgNnSc1d1	německé
státní	státní	k2eAgNnSc1d1	státní
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
Golz-Kinských	Golz-Kinský	k2eAgFnPc2d1	Golz-Kinský
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
čp.	čp.	k?	čp.
606	[number]	k4	606
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
-	-	kIx~	-
zadní	zadní	k2eAgInSc1d1	zadní
trakt	trakt	k1gInSc1	trakt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Oskarem	Oskar	k1gMnSc7	Oskar
Pollakem	Pollak	k1gInSc7	Pollak
<g/>
.	.	kIx.	.
bar	bar	k1gInSc1	bar
micva	micvo	k1gNnSc2	micvo
-	-	kIx~	-
1896	[number]	k4	1896
(	(	kIx(	(
<g/>
13.6	[number]	k4	13.6
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Cikánova	cikánův	k2eAgFnSc1d1	Cikánova
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
Bílkova	Bílkův	k2eAgFnSc1d1	Bílkova
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Kafky	Kafka	k1gMnSc2	Kafka
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
synagogy	synagoga	k1gFnSc2	synagoga
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
čase	čas	k1gInSc6	čas
stráveném	strávený	k2eAgInSc6d1	strávený
v	v	k7c6	v
templu	templ	k1gInSc6	templ
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Prozíval	prozívat	k5eAaPmAgMnS	prozívat
a	a	k8xC	a
proklimbal	proklimbat	k5eAaPmAgMnS	proklimbat
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
tedy	tedy	k9	tedy
celé	celá	k1gFnPc4	celá
ty	ten	k3xDgFnPc4	ten
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
myslím	myslet	k5eAaImIp1nS	myslet
takhle	takhle	k6eAd1	takhle
nudil	nudit	k5eAaImAgMnS	nudit
už	už	k6eAd1	už
jen	jen	k9	jen
v	v	k7c6	v
tanečních	taneční	k1gFnPc6	taneční
<g/>
)	)	kIx)	)
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
bavit	bavit	k5eAaImF	bavit
těmi	ten	k3xDgMnPc7	ten
několika	několik	k4yIc7	několik
drobnými	drobný	k2eAgNnPc7d1	drobné
zpestřeními	zpestření	k1gNnPc7	zpestření
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
tam	tam	k6eAd1	tam
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
když	když	k8xS	když
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
archu	archa	k1gFnSc4	archa
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mi	já	k3xPp1nSc3	já
pokaždé	pokaždé	k6eAd1	pokaždé
připomnělo	připomnět	k5eAaPmAgNnS	připomnět
střelnici	střelnice	k1gFnSc4	střelnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
trefí	trefit	k5eAaPmIp3nP	trefit
do	do	k7c2	do
černého	černé	k1gNnSc2	černé
<g/>
,	,	kIx,	,
otevřou	otevřít	k5eAaPmIp3nP	otevřít
dvířka	dvířka	k1gNnPc1	dvířka
skříňky	skříňka	k1gFnSc2	skříňka
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
něco	něco	k3yInSc1	něco
zajímavého	zajímavý	k2eAgNnSc2d1	zajímavé
a	a	k8xC	a
tady	tady	k6eAd1	tady
pokaždé	pokaždé	k6eAd1	pokaždé
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
staré	starý	k2eAgFnPc1d1	stará
loutky	loutka	k1gFnPc1	loutka
bez	bez	k7c2	bez
hlav	hlava	k1gFnPc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zažil	zažít	k5eAaPmAgMnS	zažít
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
hodně	hodně	k6eAd1	hodně
strachu	strach	k1gInSc2	strach
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
spousty	spousta	k1gFnSc2	spousta
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
tam	tam	k6eAd1	tam
člověk	člověk	k1gMnSc1	člověk
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
samozřejmé	samozřejmý	k2eAgNnSc1d1	samozřejmé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ses	ses	k?	ses
jednou	jednou	k6eAd1	jednou
mimochodem	mimochodem	k9	mimochodem
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zavolán	zavolat	k5eAaPmNgInS	zavolat
k	k	k7c3	k
tóře	tóra	k1gFnSc3	tóra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
třásl	třást	k5eAaImAgInS	třást
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
však	však	k9	však
nic	nic	k3yNnSc1	nic
zvlášť	zvlášť	k6eAd1	zvlášť
nerušilo	rušit	k5eNaImAgNnS	rušit
mou	můj	k3xOp1gFnSc4	můj
nudu	nuda	k1gFnSc4	nuda
<g/>
,	,	kIx,	,
leda	leda	k8xS	leda
bar	bar	k1gInSc1	bar
micva	micvo	k1gNnSc2	micvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
jen	jen	k9	jen
směšné	směšný	k2eAgNnSc4d1	směšné
učení	učení	k1gNnSc4	učení
nazpaměť	nazpaměť	k6eAd1	nazpaměť
a	a	k8xC	a
končila	končit	k5eAaImAgFnS	končit
tedy	tedy	k9	tedy
jen	jen	k9	jen
směšnou	směšný	k2eAgFnSc7d1	směšná
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Tebe	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgMnPc4d1	drobný
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
významné	významný	k2eAgFnPc4d1	významná
příhody	příhoda	k1gFnPc4	příhoda
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
když	když	k8xS	když
jsi	být	k5eAaImIp2nS	být
<g />
.	.	kIx.	.
</s>
<s>
byl	být	k5eAaImAgInS	být
zavolán	zavolat	k5eAaPmNgInS	zavolat
k	k	k7c3	k
tóře	tóra	k1gFnSc3	tóra
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
jsi	být	k5eAaImIp2nS	být
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
podle	podle	k7c2	podle
mne	já	k3xPp1nSc4	já
vysloveně	vysloveně	k6eAd1	vysloveně
společenské	společenský	k2eAgFnSc6d1	společenská
události	událost	k1gFnSc6	událost
obstál	obstát	k5eAaPmAgMnS	obstát
<g/>
...	...	k?	...
Raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
Cikánova	cikánův	k2eAgFnSc1d1	Cikánova
synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
Zigeunersynagoge	Zigeunersynagoge	k1gFnSc1	Zigeunersynagoge
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
asanace	asanace	k1gFnSc1	asanace
stržena	stržen	k2eAgFnSc1d1	stržena
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
/	/	kIx~	/
1896	[number]	k4	1896
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
Celetná	Celetný	k2eAgFnSc1d1	Celetná
čp.	čp.	k?	čp.
602	[number]	k4	602
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
-	-	kIx~	-
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
vlastní	vlastní	k2eAgInSc4d1	vlastní
pokoj	pokoj	k1gInSc4	pokoj
a	a	k8xC	a
tady	tady	k6eAd1	tady
podnikl	podniknout	k5eAaPmAgMnS	podniknout
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
literární	literární	k2eAgInPc4d1	literární
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zářím	září	k1gNnSc7	září
1896	[number]	k4	1896
a	a	k8xC	a
květnem	květno	k1gNnSc7	květno
1906	[number]	k4	1906
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
provozoval	provozovat	k5eAaImAgMnS	provozovat
otec	otec	k1gMnSc1	otec
Hermann	Hermann	k1gMnSc1	Hermann
svůj	svůj	k3xOyFgInSc4	svůj
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
<g/>
pobyt	pobyt	k1gInSc4	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
prázdniny	prázdniny	k1gFnPc4	prázdniny
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
:	:	kIx,	:
Malé	Malé	k2eAgNnSc4d1	Malé
náměstí	náměstí	k1gNnSc4	náměstí
čp.	čp.	k?	čp.
131	[number]	k4	131
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
Třešť	třeštit	k5eAaImRp2nS	třeštit
-	-	kIx~	-
sem	sem	k6eAd1	sem
opakovaně	opakovaně	k6eAd1	opakovaně
jezdil	jezdit	k5eAaImAgMnS	jezdit
Kafka	Kafka	k1gMnSc1	Kafka
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
strýci	strýc	k1gMnSc3	strýc
MUDr.	MUDr.	kA	MUDr.
Siegfriedu	Siegfried	k1gMnSc3	Siegfried
Löwymu	Löwymum	k1gNnSc3	Löwymum
na	na	k7c4	na
letní	letní	k2eAgFnPc4d1	letní
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Profese	profese	k1gFnSc1	profese
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
prostředí	prostředí	k1gNnSc1	prostředí
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Franze	Franze	k1gFnPc4	Franze
inspirací	inspirace	k1gFnPc2	inspirace
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
povídky	povídka	k1gFnSc2	povídka
Venkovský	venkovský	k2eAgMnSc1d1	venkovský
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
svého	svůj	k3xOyFgMnSc2	svůj
synovce	synovec	k1gMnSc2	synovec
lékař	lékař	k1gMnSc1	lékař
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
kočár	kočár	k1gInSc4	kočár
s	s	k7c7	s
koňským	koňský	k2eAgNnSc7d1	koňské
spřežením	spřežení	k1gNnSc7	spřežení
a	a	k8xC	a
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
si	se	k3xPyFc3	se
motorové	motorový	k2eAgNnSc4d1	motorové
kolo	kolo	k1gNnSc4	kolo
(	(	kIx(	(
<g/>
jízdní	jízdní	k2eAgNnSc4d1	jízdní
kolo	kolo	k1gNnSc4	kolo
opatřené	opatřený	k2eAgNnSc1d1	opatřené
benzinovým	benzinový	k2eAgInSc7d1	benzinový
výbušným	výbušný	k2eAgInSc7d1	výbušný
motorem	motor	k1gInSc7	motor
značky	značka	k1gFnSc2	značka
PUCH	puch	k1gInSc1	puch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
průkopníkem	průkopník	k1gMnSc7	průkopník
motorismu	motorismus	k1gInSc2	motorismus
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
nový	nový	k2eAgInSc4d1	nový
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
spoluobčané	spoluobčan	k1gMnPc1	spoluobčan
"	"	kIx"	"
<g/>
smrdutý	smrdutý	k2eAgMnSc1d1	smrdutý
čert	čert	k1gMnSc1	čert
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
pořídil	pořídit	k5eAaPmAgMnS	pořídit
motocykl	motocykl	k1gInSc4	motocykl
značky	značka	k1gFnSc2	značka
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
Klement	Klement	k1gMnSc1	Klement
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domě	dům	k1gInSc6	dům
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
odhalena	odhalen	k2eAgFnSc1d1	odhalena
busta	busta	k1gFnSc1	busta
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
třešťský	třešťský	k2eAgMnSc1d1	třešťský
rodák	rodák	k1gMnSc1	rodák
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
František	František	k1gMnSc1	František
Häckel	Häckel	k1gMnSc1	Häckel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plaketě	plaketa	k1gFnSc6	plaketa
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
pobýval	pobývat	k5eAaImAgMnS	pobývat
u	u	k7c2	u
svého	své	k1gNnSc2	své
strýce	strýc	k1gMnSc2	strýc
MUDr.	MUDr.	kA	MUDr.
Löwyho	Löwy	k1gMnSc2	Löwy
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
:	:	kIx,	:
Německá	německý	k2eAgFnSc1d1	německá
Karlo-Ferdinandova	Karlo-Ferdinandův	k2eAgFnSc1d1	Karlo-Ferdinandova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
a	a	k8xC	a
<g/>
/	/	kIx~	/
Karolinu	Karolinum	k1gNnSc6	Karolinum
<g/>
,	,	kIx,	,
Železná	železný	k2eAgFnSc1d1	železná
<g />
.	.	kIx.	.
</s>
<s>
čp.	čp.	k?	čp.
541	[number]	k4	541
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
univerzita	univerzita	k1gFnSc1	univerzita
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
ze	z	k7c2	z
Železné	železný	k2eAgFnSc2d1	železná
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
z	z	k7c2	z
Ovocného	ovocný	k2eAgInSc2d1	ovocný
trhu	trh	k1gInSc2	trh
560	[number]	k4	560
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
b	b	k?	b
<g/>
/	/	kIx~	/
Klementinum	Klementinum	k1gNnSc1	Klementinum
(	(	kIx(	(
<g/>
přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
c	c	k0	c
<g/>
/	/	kIx~	/
Clam-Gallasův	Clam-Gallasův	k2eAgInSc1d1	Clam-Gallasův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Husova	Husův	k2eAgFnSc1d1	Husova
čp.	čp.	k?	čp.
158	[number]	k4	158
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
(	(	kIx(	(
<g/>
státovědné	státovědný	k2eAgFnSc2d1	státovědný
přednášky	přednáška	k1gFnSc2	přednáška
<g/>
)	)	kIx)	)
-	-	kIx~	-
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
koketoval	koketovat	k5eAaImAgMnS	koketovat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
obory	obor	k1gInPc7	obor
(	(	kIx(	(
<g/>
nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
na	na	k7c4	na
2	[number]	k4	2
týdny	týden	k1gInPc7	týden
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c6	na
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
semestru	semestr	k1gInSc2	semestr
si	se	k3xPyFc3	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
germanistiku	germanistika	k1gFnSc4	germanistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
zkoušku	zkouška	k1gFnSc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
doktor	doktor	k1gMnSc1	doktor
práv	právo	k1gNnPc2	právo
získal	získat	k5eAaPmAgMnS	získat
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Štorchův	Štorchův	k2eAgInSc1d1	Štorchův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
552	[number]	k4	552
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
-	-	kIx~	-
praxe	praxe	k1gFnSc2	praxe
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
koncipient	koncipient	k1gMnSc1	koncipient
<g/>
)	)	kIx)	)
u	u	k7c2	u
pražského	pražský	k2eAgMnSc2d1	pražský
advokáta	advokát	k1gMnSc2	advokát
Richarda	Richard	k1gMnSc2	Richard
Löwyho	Löwy	k1gMnSc2	Löwy
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1912	[number]	k4	1912
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
palác	palác	k1gInSc1	palác
Hrzánů	Hrzán	k1gInPc2	Hrzán
z	z	k7c2	z
Harasova	Harasův	k2eAgInSc2d1	Harasův
<g/>
,	,	kIx,	,
Celetná	Celetný	k2eAgFnSc1d1	Celetná
čp.	čp.	k?	čp.
558	[number]	k4	558
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
-	-	kIx~	-
rodiče	rodič	k1gMnPc1	rodič
zde	zde	k6eAd1	zde
provozovali	provozovat	k5eAaImAgMnP	provozovat
svůj	svůj	k3xOyFgInSc4	svůj
velkoobchod	velkoobchod	k1gInSc4	velkoobchod
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
Kafkovi	Kafkův	k2eAgMnPc1d1	Kafkův
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
provozovali	provozovat	k5eAaImAgMnP	provozovat
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
velkoobchod	velkoobchod	k1gInSc1	velkoobchod
galanterním	galanterní	k2eAgNnSc7d1	galanterní
a	a	k8xC	a
střižním	střižní	k2eAgNnSc7d1	střižní
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1907	[number]	k4	1907
<g/>
:	:	kIx,	:
povinná	povinný	k2eAgFnSc1d1	povinná
(	(	kIx(	(
<g/>
neplacená	placený	k2eNgFnSc1d1	neplacená
<g/>
)	)	kIx)	)
roční	roční	k2eAgFnSc1d1	roční
praxe	praxe	k1gFnSc1	praxe
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
semestr	semestr	k1gInSc1	semestr
-	-	kIx~	-
Zemský	zemský	k2eAgInSc1d1	zemský
civilní	civilní	k2eAgInSc1d1	civilní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Ovocný	ovocný	k2eAgInSc1d1	ovocný
trh	trh	k1gInSc1	trh
čp.	čp.	k?	čp.
587	[number]	k4	587
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
Obvodní	obvodní	k2eAgInSc1d1	obvodní
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
a	a	k8xC	a
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
7	[number]	k4	7
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
povinná	povinný	k2eAgFnSc1d1	povinná
(	(	kIx(	(
<g/>
neplacená	placený	k2eNgFnSc1d1	neplacená
<g/>
)	)	kIx)	)
roční	roční	k2eAgFnSc1d1	roční
praxe	praxe	k1gFnSc1	praxe
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
semestr	semestr	k1gInSc1	semestr
-	-	kIx~	-
Trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
čp.	čp.	k?	čp.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
8	[number]	k4	8
<g/>
/	/	kIx~	/
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Lodi	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
Zum	Zum	k1gMnSc1	Zum
Schiff	Schiff	k1gMnSc1	Schiff
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
čp.	čp.	k?	čp.
883	[number]	k4	883
<g/>
/	/	kIx~	/
36	[number]	k4	36
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mikulášská	mikulášský	k2eAgFnSc1d1	Mikulášská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
-	-	kIx~	-
první	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracoval	pracovat	k5eAaImAgMnS	pracovat
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Assicurazioni	Assicurazioň	k1gFnSc6	Assicurazioň
Generali	Generali	k1gFnSc2	Generali
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Dělnické	dělnický	k2eAgFnSc6d1	Dělnická
úrazové	úrazový	k2eAgFnSc6d1	Úrazová
pojišťovně	pojišťovna	k1gFnSc6	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
odeslal	odeslat	k5eAaPmAgMnS	odeslat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
psaní	psaní	k1gNnSc4	psaní
Felici	Felice	k1gFnSc4	Felice
Bauer	Bauer	k1gMnSc1	Bauer
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgInS	napsat
povídku	povídka	k1gFnSc4	povídka
Ortel	ortel	k1gInSc1	ortel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgInS	napsat
ještě	ještě	k9	ještě
Proměnu	proměna	k1gFnSc4	proměna
(	(	kIx(	(
<g/>
vydána	vydán	k2eAgFnSc1d1	vydána
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
luxusní	luxusní	k2eAgMnSc1d1	luxusní
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
po	po	k7c6	po
asanaci	asanace	k1gFnSc6	asanace
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgInS	mít
výtah	výtah	k1gInSc1	výtah
a	a	k8xC	a
byty	byt	k1gInPc1	byt
měly	mít	k5eAaImAgFnP	mít
samostatné	samostatný	k2eAgFnPc4d1	samostatná
koupelny	koupelna	k1gFnPc4	koupelna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
tam	tam	k6eAd1	tam
jen	jen	k9	jen
14	[number]	k4	14
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
zničen	zničit	k5eAaPmNgInS	zničit
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1	dělostřelecká
palbou	palba	k1gFnSc7	palba
a	a	k8xC	a
ruiny	ruina	k1gFnPc1	ruina
byly	být	k5eAaImAgFnP	být
srovnány	srovnat	k5eAaPmNgFnP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
někdejšího	někdejší	k2eAgInSc2d1	někdejší
domu	dům	k1gInSc2	dům
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postaven	postavit	k5eAaPmNgInS	postavit
hotel	hotel	k1gInSc1	hotel
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
<g/>
.	.	kIx.	.
<g/>
práce	práce	k1gFnSc1	práce
-	-	kIx~	-
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
-	-	kIx~	-
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
Assicurazioni	Assicurazioň	k1gFnSc3	Assicurazioň
Generali	Generali	k1gFnPc3	Generali
(	(	kIx(	(
<g/>
pražská	pražský	k2eAgFnSc1d1	Pražská
filiálka	filiálka	k1gFnSc1	filiálka
<g />
.	.	kIx.	.
</s>
<s>
italské	italský	k2eAgFnPc1d1	italská
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
839	[number]	k4	839
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
-	-	kIx~	-
práce	práce	k1gFnSc1	práce
mu	on	k3xPp3gMnSc3	on
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
musel	muset	k5eAaImAgMnS	muset
pracovat	pracovat	k5eAaImF	pracovat
od	od	k7c2	od
8.00	[number]	k4	8.00
až	až	k9	až
do	do	k7c2	do
19.00	[number]	k4	19.00
<g/>
,	,	kIx,	,
20.00	[number]	k4	20.00
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
20.30	[number]	k4	20.30
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
úrazová	úrazový	k2eAgFnSc1d1	Úrazová
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
pro	pro	k7c4	pro
Království	království	k1gNnSc4	království
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
Arbeiter-Unfall-Versicherungs-Anstalt	Arbeiter-Unfall-Versicherungs-Anstalta	k1gFnPc2	Arbeiter-Unfall-Versicherungs-Anstalta
für	für	k?	für
das	das	k?	das
Königreich	Königreich	k1gInSc1	Königreich
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
čp.	čp.	k?	čp.
1075	[number]	k4	1075
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
firmě	firma	k1gFnSc6	firma
pracoval	pracovat	k5eAaImAgInS	pracovat
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Udělal	udělat	k5eAaPmAgMnS	udělat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
z	z	k7c2	z
pomocného	pomocný	k2eAgMnSc2d1	pomocný
úředníka	úředník	k1gMnSc2	úředník
se	se	k3xPyFc4	se
během	během	k7c2	během
5	[number]	k4	5
let	léto	k1gNnPc2	léto
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
na	na	k7c4	na
vicesekretáře	vicesekretář	k1gMnSc4	vicesekretář
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
předčasně	předčasně	k6eAd1	předčasně
penzionován	penzionován	k2eAgMnSc1d1	penzionován
<g/>
.	.	kIx.	.
</s>
<s>
Úřadoval	úřadovat	k5eAaImAgInS	úřadovat
od	od	k7c2	od
8.00	[number]	k4	8.00
do	do	k7c2	do
14.00	[number]	k4	14.00
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgInS	pracovat
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
od	od	k7c2	od
15.00	[number]	k4	15.00
do	do	k7c2	do
19.30	[number]	k4	19.30
spal	spát	k5eAaImAgMnS	spát
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
procházka	procházka	k1gFnSc1	procházka
a	a	k8xC	a
rodinná	rodinný	k2eAgFnSc1d1	rodinná
večeře	večeře	k1gFnSc1	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
navazovalo	navazovat	k5eAaImAgNnS	navazovat
psaní	psaní	k1gNnSc1	psaní
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
vydělával	vydělávat	k5eAaImAgInS	vydělávat
asi	asi	k9	asi
6500	[number]	k4	6500
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
dělník	dělník	k1gMnSc1	dělník
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
kolem	kolem	k7c2	kolem
1000	[number]	k4	1000
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vydělané	vydělaný	k2eAgInPc4d1	vydělaný
peníze	peníz	k1gInPc4	peníz
kupoval	kupovat	k5eAaImAgMnS	kupovat
válečné	válečný	k2eAgInPc4d1	válečný
dluhopisy	dluhopis	k1gInPc4	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
významnou	významný	k2eAgFnSc4d1	významná
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
oblast	oblast	k1gFnSc4	oblast
severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
především	především	k9	především
továrny	továrna	k1gFnPc1	továrna
na	na	k7c4	na
textil	textil	k1gInSc4	textil
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
Chrastava	Chrastava	k1gFnSc1	Chrastava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
hotel	hotel	k1gInSc1	hotel
Mercure	Mercur	k1gInSc5	Mercur
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
hale	hala	k1gFnSc6	hala
je	být	k5eAaImIp3nS	být
busta	busta	k1gFnSc1	busta
na	na	k7c6	na
kovovém	kovový	k2eAgInSc6d1	kovový
trojhranném	trojhranný	k2eAgInSc6d1	trojhranný
podstavci	podstavec	k1gInSc6	podstavec
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc1	práce
-	-	kIx~	-
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
Bořivojova	Bořivojův	k2eAgFnSc1d1	Bořivojova
918	[number]	k4	918
<g/>
/	/	kIx~	/
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
-	-	kIx~	-
zde	zde	k6eAd1	zde
ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
domu	dům	k1gInSc2	dům
Kafkův	Kafkův	k2eAgMnSc1d1	Kafkův
švagr	švagr	k1gMnSc1	švagr
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Elli	Ell	k1gFnSc2	Ell
Kafkové	Kafková	k1gFnSc2	Kafková
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
malou	malý	k2eAgFnSc4d1	malá
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
azbest	azbest	k1gInSc4	azbest
(	(	kIx(	(
<g/>
Pražská	pražský	k2eAgFnSc1d1	Pražská
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
azbest	azbest	k1gInSc4	azbest
Hermann	Hermann	k1gMnSc1	Hermann
&	&	k?	&
Co	co	k9	co
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chodu	chod	k1gInSc6	chod
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
továrnu	továrna	k1gFnSc4	továrna
však	však	k9	však
nenáviděl	návidět	k5eNaImAgMnS	návidět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
narušila	narušit	k5eAaPmAgFnS	narušit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
vyjmuta	vyjmout	k5eAaPmNgFnS	vyjmout
z	z	k7c2	z
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
1910	[number]	k4	1910
<g/>
:	:	kIx,	:
tehdy	tehdy	k6eAd1	tehdy
hotel	hotel	k1gInSc1	hotel
Geling	Geling	k1gInSc1	Geling
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
hotel	hotel	k1gInSc1	hotel
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
polyfunkční	polyfunkční	k2eAgInSc1d1	polyfunkční
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
Komenského	Komenského	k2eAgInSc1d1	Komenského
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Kafku	Kafka	k1gMnSc4	Kafka
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Jablonce	Jablonec	k1gInSc2	Jablonec
jeho	jeho	k3xOp3gMnSc1	jeho
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hotelu	hotel	k1gInSc6	hotel
na	na	k7c6	na
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vystoupení	vystoupení	k1gNnSc6	vystoupení
tamním	tamní	k2eAgNnSc6d1	tamní
továrníkům	továrník	k1gMnPc3	továrník
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
zásady	zásada	k1gFnPc4	zásada
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
včetně	včetně	k7c2	včetně
plateb	platba	k1gFnPc2	platba
a	a	k8xC	a
kritérií	kritérion	k1gNnPc2	kritérion
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
kategorie	kategorie	k1gFnPc4	kategorie
podle	podle	k7c2	podle
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Geling	Geling	k1gInSc4	Geling
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
pomezek	pomezek	k1gInSc4	pomezek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc4d2	starší
zástavbu	zástavba	k1gFnSc4	zástavba
nechal	nechat	k5eAaPmAgMnS	nechat
strhnout	strhnout	k5eAaPmF	strhnout
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
postavil	postavit	k5eAaPmAgInS	postavit
novorenesanční	novorenesanční	k2eAgInSc1d1	novorenesanční
hotel	hotel	k1gInSc1	hotel
s	s	k7c7	s
restaurací	restaurace	k1gFnSc7	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
(	(	kIx(	(
<g/>
secesní	secesní	k2eAgFnSc1d1	secesní
přístavba	přístavba	k1gFnSc1	přístavba
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byly	být	k5eAaImAgInP	být
interiéry	interiér	k1gInPc1	interiér
upraveny	upraven	k2eAgInPc1d1	upraven
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
módním	módní	k2eAgInSc6d1	módní
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
hotel	hotel	k1gInSc4	hotel
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k9	až
1990	[number]	k4	1990
patřil	patřit	k5eAaImAgInS	patřit
státnímu	státní	k2eAgInSc3d1	státní
podniku	podnik	k1gInSc3	podnik
Restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Krása	krása	k1gFnSc1	krása
bez	bez	k7c2	bez
závoje	závoj	k1gInSc2	závoj
svlékla	svléknout	k5eAaPmAgFnS	svléknout
první	první	k4xOgFnSc1	první
striptérka	striptérka	k1gFnSc1	striptérka
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
hotel	hotel	k1gInSc1	hotel
rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
na	na	k7c4	na
polyfunkční	polyfunkční	k2eAgInSc4d1	polyfunkční
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
-	-	kIx~	-
1911	[number]	k4	1911
<g/>
:	:	kIx,	:
Café	café	k1gNnSc4	café
Savoy	Savoa	k1gFnSc2	Savoa
<g/>
,	,	kIx,	,
Vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
859	[number]	k4	859
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
-	-	kIx~	-
opakovaně	opakovaně	k6eAd1	opakovaně
zde	zde	k6eAd1	zde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
představení	představení	k1gNnSc4	představení
jidiš	jidiš	k1gNnSc2	jidiš
divadla	divadlo	k1gNnSc2	divadlo
z	z	k7c2	z
Lvova	Lvov	k1gInSc2	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blízce	k6eAd2	blízce
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
principálem	principál	k1gInSc7	principál
souboru	soubor	k1gInSc2	soubor
Jicchakem	Jicchak	k1gInSc7	Jicchak
Löwym	Löwym	k1gInSc1	Löwym
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
této	tento	k3xDgFnSc2	tento
divadelní	divadelní	k2eAgFnSc2d1	divadelní
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
restaurace	restaurace	k1gFnSc2	restaurace
Katr	katr	k1gInSc1	katr
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1912	[number]	k4	1912
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
palác	palác	k1gInSc1	palác
Golz-Kinských	Golz-Kinský	k2eAgFnPc2d1	Golz-Kinský
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
čp.	čp.	k?	čp.
606	[number]	k4	606
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
provozoval	provozovat	k5eAaImAgMnS	provozovat
svůj	svůj	k3xOyFgInSc4	svůj
obchod	obchod	k1gInSc4	obchod
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
je	být	k5eAaImIp3nS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
odhalená	odhalený	k2eAgFnSc1d1	odhalená
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvojjazyčným	dvojjazyčný	k2eAgInSc7d1	dvojjazyčný
česko-anglickým	českonglický	k2eAgInSc7d1	česko-anglický
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
býval	bývat	k5eAaImAgInS	bývat
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
a	a	k8xC	a
galanterním	galanterní	k2eAgNnSc7d1	galanterní
zbožím	zboží	k1gNnSc7	zboží
Hermanna	Hermann	k1gMnSc2	Hermann
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
this	this	k6eAd1	this
site	site	k1gNnSc7	site
stood	stooda	k1gFnPc2	stooda
the	the	k?	the
haberdashery	haberdasher	k1gInPc1	haberdasher
shop	shop	k1gInSc1	shop
of	of	k?	of
Hermann	Hermann	k1gMnSc1	Hermann
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
the	the	k?	the
father	fathra	k1gFnPc2	fathra
of	of	k?	of
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nachází	nacházet	k5eAaImIp3nS	nacházet
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
-	-	kIx~	-
1914	[number]	k4	1914
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
léčení	léčení	k1gNnSc6	léčení
<g/>
:	:	kIx,	:
Oppeltův	Oppeltův	k2eAgInSc1d1	Oppeltův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
934	[number]	k4	934
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horní	horní	k2eAgNnSc1d1	horní
patro	patro	k1gNnSc1	patro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
zrestaurován	zrestaurován	k2eAgMnSc1d1	zrestaurován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
horního	horní	k2eAgNnSc2d1	horní
podlaží	podlaží	k1gNnSc2	podlaží
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Kafkův	Kafkův	k2eAgInSc1d1	Kafkův
byt	byt	k1gInSc1	byt
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
výhled	výhled	k1gInSc4	výhled
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Gretě	Greť	k1gFnSc2	Greť
Blochové	Blochová	k1gFnSc2	Blochová
<g/>
:	:	kIx,	:
Přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
mým	můj	k3xOp1gNnSc7	můj
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
.	.	kIx.	.
poschodí	poschodí	k1gNnSc6	poschodí
<g/>
,	,	kIx,	,
mám	mít	k5eAaImIp1nS	mít
velkou	velký	k2eAgFnSc4d1	velká
kopuli	kopule	k1gFnSc4	kopule
ruského	ruský	k2eAgInSc2d1	ruský
kostela	kostel	k1gInSc2	kostel
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
mezi	mezi	k7c7	mezi
kupolí	kupole	k1gFnSc7	kupole
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
činžovním	činžovní	k2eAgInSc7d1	činžovní
domem	dům	k1gInSc7	dům
jen	jen	k9	jen
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
výsek	výsek	k1gInSc4	výsek
Petřína	Petřín	k1gInSc2	Petřín
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
docela	docela	k6eAd1	docela
malým	malý	k2eAgInSc7d1	malý
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
vidím	vidět	k5eAaImIp1nS	vidět
radnici	radnice	k1gFnSc4	radnice
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
její	její	k3xOp3gFnSc2	její
mohutnosti	mohutnost	k1gFnSc2	mohutnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
tyčí	tyčit	k5eAaImIp3nS	tyčit
a	a	k8xC	a
ubíhá	ubíhat	k5eAaImIp3nS	ubíhat
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
perspektivě	perspektiva	k1gFnSc6	perspektiva
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
snad	snad	k9	snad
ještě	ještě	k9	ještě
nikdo	nikdo	k3yNnSc1	nikdo
správně	správně	k6eAd1	správně
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
/	/	kIx~	/
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bílkova	Bílkův	k2eAgFnSc1d1	Bílkova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
čp.	čp.	k?	čp.
868	[number]	k4	868
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
-	-	kIx~	-
u	u	k7c2	u
sestry	sestra	k1gFnSc2	sestra
Valli	Valle	k1gFnSc4	Valle
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
román	román	k1gInSc4	román
Proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
/	/	kIx~	/
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Polská	polský	k2eAgFnSc1d1	polská
ul	ul	kA	ul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Nerudova	Nerudův	k2eAgFnSc1d1	Nerudova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
čp.	čp.	k?	čp.
1532	[number]	k4	1532
<g/>
/	/	kIx~	/
<g/>
48	[number]	k4	48
-	-	kIx~	-
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
u	u	k7c2	u
sestry	sestra	k1gFnSc2	sestra
<g />
.	.	kIx.	.
</s>
<s>
Elli	Elli	k1gNnPc1	Elli
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
čas	čas	k1gInSc4	čas
opustila	opustit	k5eAaPmAgFnS	opustit
Zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgMnS	napsat
další	další	k2eAgFnPc4d1	další
kapitoly	kapitola	k1gFnPc4	kapitola
románu	román	k1gInSc2	román
Proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
/	/	kIx~	/
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
únor	únor	k1gInSc4	únor
<g/>
-	-	kIx~	-
<g/>
březen	březen	k1gInSc4	březen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bílkova	Bílkův	k2eAgFnSc1d1	Bílkova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
čp.	čp.	k?	čp.
868	[number]	k4	868
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
-	-	kIx~	-
stejná	stejný	k2eAgFnSc1d1	stejná
adresa	adresa	k1gFnSc1	adresa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiný	jiný	k2eAgInSc1d1	jiný
byt	byt	k1gInSc1	byt
než	než	k8xS	než
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
/	/	kIx~	/
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
březen	březen	k1gInSc4	březen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
-	-	kIx~	-
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
štiky	štika	k1gFnSc2	štika
<g/>
,	,	kIx,	,
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
ul	ul	kA	ul
<g/>
.	.	kIx.	.
čp.	čp.	k?	čp.
705	[number]	k4	705
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
chodil	chodit	k5eAaImAgMnS	chodit
psát	psát	k5eAaImF	psát
do	do	k7c2	do
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
uličky	ulička	k1gFnSc2	ulička
(	(	kIx(	(
<g/>
adresa	adresa	k1gFnSc1	adresa
č.	č.	k?	č.
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
hotel	hotel	k1gInSc1	hotel
Balmoral-Osborne	Balmoral-Osborn	k1gInSc5	Balmoral-Osborn
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
390	[number]	k4	390
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
a	a	k8xC	a
a	a	k8xC	a
389	[number]	k4	389
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
-	-	kIx~	-
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
společný	společný	k2eAgInSc4d1	společný
pobyt	pobyt	k1gInSc4	pobyt
s	s	k7c7	s
Felice	Felic	k1gMnPc4	Felic
Bauerovou	Bauerová	k1gFnSc4	Bauerová
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
odjezdu	odjezd	k1gInSc6	odjezd
ještě	ještě	k9	ještě
deset	deset	k4xCc4	deset
dnů	den	k1gInPc2	den
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
pokoj	pokoj	k1gInSc4	pokoj
vedle	vedle	k7c2	vedle
Felice	Felice	k1gFnSc2	Felice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gNnSc7	jejich
pokoji	pokoj	k1gInSc6	pokoj
byly	být	k5eAaImAgFnP	být
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
měli	mít	k5eAaImAgMnP	mít
oba	dva	k4xCgMnPc1	dva
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
si	se	k3xPyFc3	se
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
Teplé	Teplá	k1gFnSc2	Teplá
(	(	kIx(	(
<g/>
8.7	[number]	k4	8.7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
Františkových	Františkův	k2eAgInPc2d1	Františkův
Lázních	lázeň	k1gFnPc6	lázeň
(	(	kIx(	(
<g/>
13.7	[number]	k4	13.7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
budovu	budova	k1gFnSc4	budova
využívá	využívat	k5eAaImIp3nS	využívat
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1916	[number]	k4	1916
na	na	k7c6	na
služební	služební	k2eAgFnSc6d1	služební
cestě	cesta	k1gFnSc6	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
/	/	kIx~	/
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
-	-	kIx~	-
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
březen	březen	k1gInSc4	březen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
čp.	čp.	k?	čp.
22	[number]	k4	22
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
pronajat	pronajmout	k5eAaPmNgInS	pronajmout
jeho	jeho	k3xOp3gNnSc1	jeho
sestře	sestra	k1gFnSc3	sestra
Ottle	Ottle	k1gFnSc2	Ottle
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
sem	sem	k6eAd1	sem
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
zde	zde	k6eAd1	zde
většinu	většina	k1gFnSc4	většina
povídek	povídka	k1gFnPc2	povídka
sbírky	sbírka	k1gFnSc2	sbírka
Venkovský	venkovský	k2eAgMnSc1d1	venkovský
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
modrého	modrý	k2eAgInSc2d1	modrý
domku	domek	k1gInSc2	domek
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
úzkého	úzký	k2eAgInSc2d1	úzký
kovového	kovový	k2eAgInSc2d1	kovový
pásu	pás	k1gInSc2	pás
s	s	k7c7	s
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domku	domek	k1gInSc6	domek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
prodávají	prodávat	k5eAaImIp3nP	prodávat
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
pohlednice	pohlednice	k1gFnPc4	pohlednice
<g/>
,	,	kIx,	,
býval	bývat	k5eAaImAgInS	bývat
malý	malý	k2eAgInSc1d1	malý
obývací	obývací	k2eAgInSc1d1	obývací
pokoj	pokoj	k1gInSc1	pokoj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
okno	okno	k1gNnSc1	okno
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Jeleního	jelení	k2eAgInSc2d1	jelení
příkopu	příkop	k1gInSc2	příkop
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
malá	malý	k2eAgFnSc1d1	malá
hala	hala	k1gFnSc1	hala
a	a	k8xC	a
průchod	průchod	k1gInSc1	průchod
do	do	k7c2	do
podkroví	podkroví	k1gNnSc2	podkroví
a	a	k8xC	a
sklepa	sklep	k1gInSc2	sklep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dopisech	dopis	k1gInPc6	dopis
Felici	Felic	k1gMnPc7	Felic
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsem	být	k5eAaImIp1nS	být
šel	jít	k5eAaImAgMnS	jít
s	s	k7c7	s
Ottlou	Ottla	k1gMnSc7	Ottla
hledat	hledat	k5eAaImF	hledat
byt	byt	k1gInSc4	byt
<g/>
,	,	kIx,	,
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
skutečného	skutečný	k2eAgInSc2d1	skutečný
klidu	klid	k1gInSc2	klid
jsem	být	k5eAaImIp1nS	být
už	už	k6eAd1	už
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
přece	přece	k9	přece
však	však	k9	však
jsem	být	k5eAaImIp1nS	být
šel	jít	k5eAaImAgMnS	jít
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Prohlédli	prohlédnout	k5eAaPmAgMnP	prohlédnout
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
něco	něco	k3yInSc4	něco
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
...	...	k?	...
Nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
nenašli	najít	k5eNaPmAgMnP	najít
jsme	být	k5eAaImIp1nP	být
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
hodilo	hodit	k5eAaImAgNnS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
legrace	legrace	k1gFnSc2	legrace
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
zeptali	zeptat	k5eAaPmAgMnP	zeptat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
malé	malý	k2eAgFnSc6d1	malá
uličce	ulička	k1gFnSc6	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
domek	domek	k1gInSc4	domek
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
k	k	k7c3	k
pronajmutí	pronajmutí	k1gNnSc3	pronajmutí
<g/>
.	.	kIx.	.
</s>
<s>
Ottla	Ottla	k1gFnSc1	Ottla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rovněž	rovněž	k9	rovněž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
dům	dům	k1gInSc1	dům
pronajme	pronajmout	k5eAaPmIp3nS	pronajmout
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
slabostí	slabost	k1gFnSc7	slabost
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
ji	on	k3xPp3gFnSc4	on
zrazoval	zrazovat	k5eAaImAgMnS	zrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
bych	by	kYmCp1nS	by
tam	tam	k6eAd1	tam
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
i	i	k9	i
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jsem	být	k5eAaImIp1nS	být
ani	ani	k8xC	ani
nepomyslel	pomyslet	k5eNaPmAgMnS	pomyslet
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
špinavý	špinavý	k2eAgInSc4d1	špinavý
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
neobyvatelný	obyvatelný	k2eNgMnSc1d1	neobyvatelný
<g/>
,	,	kIx,	,
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
možnými	možný	k2eAgInPc7d1	možný
nedostatky	nedostatek	k1gInPc7	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
však	však	k9	však
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
trvala	trvat	k5eAaImAgFnS	trvat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
domek	domek	k1gInSc4	domek
velkou	velký	k2eAgFnSc7d1	velká
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
bydlela	bydlet	k5eAaImAgFnS	bydlet
<g/>
,	,	kIx,	,
vyklizen	vyklidit	k5eAaPmNgInS	vyklidit
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
ho	on	k3xPp3gInSc4	on
vymalovat	vymalovat	k5eAaPmF	vymalovat
<g/>
,	,	kIx,	,
koupila	koupit	k5eAaPmAgFnS	koupit
pár	pár	k4xCyI	pár
kousků	kousek	k1gInPc2	kousek
rákosového	rákosový	k2eAgInSc2d1	rákosový
nábytku	nábytek	k1gInSc2	nábytek
<g/>
...	...	k?	...
chovala	chovat	k5eAaImAgFnS	chovat
to	ten	k3xDgNnSc4	ten
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
před	před	k7c7	před
ostatní	ostatní	k2eAgFnSc7d1	ostatní
rodinou	rodina	k1gFnSc7	rodina
jako	jako	k8xS	jako
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
...	...	k?	...
Tady	tady	k6eAd1	tady
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
<g/>
:	:	kIx,	:
krásná	krásný	k2eAgFnSc1d1	krásná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
sem	sem	k6eAd1	sem
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgNnSc1d1	zdejší
ticho	ticho	k1gNnSc1	ticho
<g/>
,	,	kIx,	,
od	od	k7c2	od
souseda	soused	k1gMnSc2	soused
mě	já	k3xPp1nSc2	já
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
pouze	pouze	k6eAd1	pouze
tenká	tenký	k2eAgFnSc1d1	tenká
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soused	soused	k1gMnSc1	soused
je	být	k5eAaImIp3nS	být
docela	docela	k6eAd1	docela
potichu	potichu	k6eAd1	potichu
<g/>
;	;	kIx,	;
nosím	nosit	k5eAaImIp1nS	nosit
si	se	k3xPyFc3	se
sem	sem	k6eAd1	sem
večeři	večeře	k1gFnSc4	večeře
a	a	k8xC	a
zůstávám	zůstávat	k5eAaImIp1nS	zůstávat
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
výhoda	výhoda	k1gFnSc1	výhoda
cesty	cesta	k1gFnSc2	cesta
domů	dům	k1gInPc2	dům
<g/>
:	:	kIx,	:
když	když	k8xS	když
se	se	k3xPyFc4	se
zastavím	zastavit	k5eAaPmIp1nS	zastavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jdu	jít	k5eAaImIp1nS	jít
zase	zase	k9	zase
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
procházka	procházka	k1gFnSc1	procházka
mi	já	k3xPp1nSc3	já
zklidní	zklidnit	k5eAaPmIp3nS	zklidnit
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
život	život	k1gInSc4	život
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
něco	něco	k3yInSc1	něco
jedinečného	jedinečný	k2eAgNnSc2d1	jedinečné
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
zamknout	zamknout	k5eAaPmF	zamknout
dveře	dveře	k1gFnPc4	dveře
před	před	k7c7	před
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jen	jen	k9	jen
dveře	dveře	k1gFnPc4	dveře
pokoje	pokoj	k1gInSc2	pokoj
nebo	nebo	k8xC	nebo
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dveře	dveře	k1gFnPc1	dveře
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
vyjít	vyjít	k5eAaPmF	vyjít
ze	z	k7c2	z
dveří	dveře	k1gFnPc2	dveře
domova	domov	k1gInSc2	domov
do	do	k7c2	do
sněhu	sníh	k1gInSc2	sníh
na	na	k7c6	na
tiché	tichý	k2eAgFnSc6d1	tichá
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
za	za	k7c2	za
20	[number]	k4	20
korun	koruna	k1gFnPc2	koruna
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
,	,	kIx,	,
zařízený	zařízený	k2eAgMnSc1d1	zařízený
mou	můj	k3xOp1gFnSc7	můj
sestrou	sestra	k1gFnSc7	sestra
vším	všecek	k3xTgNnSc7	všecek
potřebným	potřebný	k2eAgNnSc7d1	potřebné
<g/>
,	,	kIx,	,
opečovávaný	opečovávaný	k2eAgMnSc1d1	opečovávaný
tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc7d1	malá
květinářkou	květinářka	k1gFnSc7	květinářka
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
a	a	k8xC	a
hezké	hezký	k2eAgNnSc1d1	hezké
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
15	[number]	k4	15
<g/>
/	/	kIx~	/
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
březen-září	březenáří	k1gNnSc2	březen-září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Schönbornský	Schönbornský	k2eAgInSc1d1	Schönbornský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Tržiště	tržiště	k1gNnSc1	tržiště
čp.	čp.	k?	čp.
365	[number]	k4	365
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
-	-	kIx~	-
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
si	se	k3xPyFc3	se
dvoupokojový	dvoupokojový	k2eAgInSc4d1	dvoupokojový
byt	byt	k1gInSc4	byt
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
snoubenku	snoubenka	k1gFnSc4	snoubenka
Felice	Felice	k1gFnSc2	Felice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1917	[number]	k4	1917
zde	zde	k6eAd1	zde
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
první	první	k4xOgNnSc4	první
vážné	vážný	k2eAgNnSc4d1	vážné
plicní	plicní	k2eAgNnSc4d1	plicní
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
plicní	plicní	k2eAgFnSc1d1	plicní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
ambasáda	ambasáda	k1gFnSc1	ambasáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
dvojjazyčným	dvojjazyčný	k2eAgInSc7d1	dvojjazyčný
textem	text	k1gInSc7	text
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
spisovatel	spisovatel	k1gMnSc1	spisovatel
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
writer	writer	k1gMnSc1	writer
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
lived	lived	k1gMnSc1	lived
and	and	k?	and
worked	worked	k1gMnSc1	worked
in	in	k?	in
this	this	k1gInSc1	this
building	building	k1gInSc1	building
in	in	k?	in
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gInSc1	American
Embassy	Embassa	k1gFnSc2	Embassa
Prague	Pragu	k1gFnSc2	Pragu
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
znak	znak	k1gInSc1	znak
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
až	až	k9	až
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
-	-	kIx~	-
obec	obec	k1gFnSc1	obec
Siřem	Siřem	k1gInSc4	Siřem
u	u	k7c2	u
Podbořan	Podbořany	k1gInPc2	Podbořany
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
9	[number]	k4	9
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
opět	opět	k6eAd1	opět
Oppeltův	Oppeltův	k2eAgInSc4d1	Oppeltův
dům	dům	k1gInSc4	dům
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
9	[number]	k4	9
<g/>
a	a	k8xC	a
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
a	a	k8xC	a
<g/>
/	/	kIx~	/
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
/	/	kIx~	/
znovu	znovu	k6eAd1	znovu
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
/	/	kIx~	/
potřetí	potřetí	k4xO	potřetí
týdenní	týdenní	k2eAgInSc4d1	týdenní
pobyt	pobyt	k1gInSc4	pobyt
začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
/	/	kIx~	/
naposled	naposled	k6eAd1	naposled
1923	[number]	k4	1923
(	(	kIx(	(
<g/>
srpen-září	srpenáří	k1gNnSc2	srpen-září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stüdlův	Stüdlův	k2eAgInSc1d1	Stüdlův
penzion	penzion	k1gInSc1	penzion
<g/>
,	,	kIx,	,
Želízy	Želízy	k1gInPc1	Želízy
76	[number]	k4	76
-	-	kIx~	-
léčil	léčit	k5eAaImAgMnS	léčit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
strávil	strávit	k5eAaPmAgMnS	strávit
145	[number]	k4	145
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
5	[number]	k4	5
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
se	se	k3xPyFc4	se
na	na	k7c6	na
slunné	slunný	k2eAgFnSc6d1	slunná
verandě	veranda	k1gFnSc6	veranda
penzionu	penzion	k1gInSc2	penzion
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
Wohryzkovou	Wohryzkový	k2eAgFnSc7d1	Wohryzková
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
pražského	pražský	k2eAgMnSc2d1	pražský
obuvníka	obuvník	k1gMnSc2	obuvník
a	a	k8xC	a
správce	správce	k1gMnSc2	správce
vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třetího	třetí	k4xOgInSc2	třetí
pobytu	pobyt	k1gInSc2	pobyt
napsal	napsat	k5eAaPmAgMnS	napsat
mnohastránkový	mnohastránkový	k2eAgMnSc1d1	mnohastránkový
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nedoručený	doručený	k2eNgMnSc1d1	nedoručený
<g/>
,	,	kIx,	,
naříkavě	naříkavě	k6eAd1	naříkavě
bilancující	bilancující	k2eAgInSc1d1	bilancující
Dopis	dopis	k1gInSc1	dopis
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
stěně	stěna	k1gFnSc6	stěna
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Dalibor	Dalibor	k1gMnSc1	Dalibor
Mandovec	Mandovec	k1gMnSc1	Mandovec
z	z	k7c2	z
Říčan	Říčany	k1gInPc2	Říčany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
najdeme	najít	k5eAaPmIp1nP	najít
Kafkovu	Kafkův	k2eAgFnSc4d1	Kafkova
tvář	tvář	k1gFnSc4	tvář
zasazenou	zasazený	k2eAgFnSc4d1	zasazená
do	do	k7c2	do
šesticípé	šesticípý	k2eAgFnSc2d1	šesticípá
Davidovy	Davidův	k2eAgFnSc2d1	Davidova
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
text	text	k1gInSc1	text
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
a	a	k8xC	a
1923	[number]	k4	1923
spisovatel	spisovatel	k1gMnSc1	spisovatel
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgInS	napsat
slavný	slavný	k2eAgInSc1d1	slavný
"	"	kIx"	"
<g/>
Dopis	dopis	k1gInSc1	dopis
otci	otec	k1gMnSc3	otec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Planá	Planá	k1gFnSc1	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Příčná	příčný	k2eAgFnSc1d1	příčná
čp.	čp.	k?	čp.
145	[number]	k4	145
u	u	k7c2	u
manželů	manžel	k1gMnPc2	manžel
Hniličkových	hniličkový	k2eAgMnPc2d1	hniličkový
<g/>
.	.	kIx.	.
</s>
<s>
Nejmilejší	milý	k2eAgFnSc1d3	nejmilejší
sestra	sestra	k1gFnSc1	sestra
Ottla	Ottla	k1gFnSc1	Ottla
zde	zde	k6eAd1	zde
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
2	[number]	k4	2
pokoje	pokoj	k1gInSc2	pokoj
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
dcerku	dcerka	k1gFnSc4	dcerka
Věru	Věra	k1gFnSc4	Věra
a	a	k8xC	a
služebnou	služebná	k1gFnSc4	služebná
<g/>
;	;	kIx,	;
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
přijížděl	přijíždět	k5eAaImAgMnS	přijíždět
i	i	k9	i
manžel	manžel	k1gMnSc1	manžel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
v	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
patře	patro	k1gNnSc6	patro
soukromého	soukromý	k2eAgInSc2d1	soukromý
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Franzovi	Franzův	k2eAgMnPc1d1	Franzův
přenechala	přenechat	k5eAaPmAgFnS	přenechat
větší	veliký	k2eAgInSc4d2	veliký
světlejší	světlý	k2eAgInSc4d2	světlejší
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Planá	Planá	k1gFnSc1	Planá
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
nejoblíbenějším	oblíbený	k2eAgNnPc3d3	nejoblíbenější
a	a	k8xC	a
nejmódnějším	módný	k2eAgNnPc3d3	nejmódnější
letoviskům	letovisko	k1gNnPc3	letovisko
kvůli	kvůli	k7c3	kvůli
domnělým	domnělý	k2eAgInPc3d1	domnělý
léčebným	léčebný	k2eAgInPc3d1	léčebný
účinkům	účinek	k1gInPc3	účinek
zdejší	zdejší	k2eAgFnSc2d1	zdejší
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
tříměsíčního	tříměsíční	k2eAgInSc2d1	tříměsíční
letního	letní	k2eAgInSc2d1	letní
pobytu	pobyt	k1gInSc2	pobyt
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
penzionován	penzionován	k2eAgMnSc1d1	penzionován
(	(	kIx(	(
<g/>
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgInS	psát
zde	zde	k6eAd1	zde
román	román	k1gInSc1	román
Zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
právě	právě	k9	právě
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nedokončí	dokončit	k5eNaPmIp3nS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Nenašel	najít	k5eNaPmAgMnS	najít
zde	zde	k6eAd1	zde
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
vadil	vadit	k5eAaImAgInS	vadit
mu	on	k3xPp3gMnSc3	on
hluk	hluk	k1gInSc1	hluk
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
pily	pila	k1gFnSc2	pila
i	i	k9	i
křik	křik	k1gInSc4	křik
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
to	ten	k3xDgNnSc1	ten
dopis	dopis	k1gInSc1	dopis
Maxu	Maxa	k1gMnSc4	Maxa
Brodovi	Brodův	k2eAgMnPc1d1	Brodův
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
:	:	kIx,	:
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
tu	tu	k6eAd1	tu
hezky	hezky	k6eAd1	hezky
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
sice	sice	k8xC	sice
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
dost	dost	k6eAd1	dost
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
chaloupka	chaloupka	k1gFnSc1	chaloupka
na	na	k7c6	na
komponování	komponování	k1gNnSc6	komponování
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Ottla	Ottla	k1gMnSc1	Ottla
o	o	k7c4	o
všechno	všechen	k3xTgNnSc4	všechen
zázračně	zázračně	k6eAd1	zázračně
pečuje	pečovat	k5eAaImIp3nS	pečovat
(	(	kIx(	(
<g/>
posílá	posílat	k5eAaImIp3nS	posílat
Ti	ty	k3xPp2nSc3	ty
pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
například	například	k6eAd1	například
neblahý	blahý	k2eNgInSc4d1	neblahý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
tu	tu	k6eAd1	tu
drvoštěp	drvoštěp	k1gMnSc1	drvoštěp
štípe	štípat	k5eAaImIp3nS	štípat
hospodyni	hospodyně	k1gFnSc4	hospodyně
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
on	on	k3xPp3gMnSc1	on
naprosto	naprosto	k6eAd1	naprosto
nepochopitelně	pochopitelně	k6eNd1	pochopitelně
vydrží	vydržet	k5eAaPmIp3nS	vydržet
rukama	ruka	k1gFnPc7	ruka
a	a	k8xC	a
mozkem	mozek	k1gInSc7	mozek
<g/>
,	,	kIx,	,
nevydržím	vydržet	k5eNaPmIp1nS	vydržet
já	já	k3xPp1nSc1	já
ušima	ucho	k1gNnPc7	ucho
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
s	s	k7c7	s
Ohropaxem	Ohropax	k1gInSc7	Ohropax
ne	ne	k9	ne
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
úplně	úplně	k6eAd1	úplně
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
;	;	kIx,	;
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
strčíš	strčit	k5eAaPmIp2nS	strčit
do	do	k7c2	do
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
slyšíš	slyšet	k5eAaImIp2nS	slyšet
toho	ten	k3xDgMnSc4	ten
sice	sice	k8xC	sice
zrovna	zrovna	k6eAd1	zrovna
tolik	tolik	k6eAd1	tolik
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
přece	přece	k9	přece
jen	jen	k9	jen
docílíš	docílit	k5eAaPmIp2nS	docílit
lehkého	lehký	k2eAgNnSc2d1	lehké
ohlušení	ohlušení	k1gNnSc2	ohlušení
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
chabého	chabý	k2eAgInSc2d1	chabý
pocitu	pocit	k1gInSc2	pocit
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
inu	inu	k9	inu
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mnoho	mnoho	k6eAd1	mnoho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
rámus	rámus	k1gInSc1	rámus
od	od	k7c2	od
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsem	být	k5eAaImIp1nS	být
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
změnit	změnit	k5eAaPmF	změnit
pokoj	pokoj	k1gInSc4	pokoj
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
pokoj	pokoj	k1gInSc4	pokoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
až	až	k6eAd1	až
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc1d1	světlý
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
okny	okno	k1gNnPc7	okno
<g/>
,	,	kIx,	,
s	s	k7c7	s
dalekým	daleký	k2eAgInSc7d1	daleký
výhleden	výhledna	k1gFnPc2	výhledna
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
dokonale	dokonale	k6eAd1	dokonale
chudým	chudý	k2eAgInSc7d1	chudý
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
nehotelovým	hotelový	k2eNgNnSc7d1	hotelový
zařízením	zařízení	k1gNnSc7	zařízení
měl	mít	k5eAaImAgMnS	mít
cosi	cosi	k3yInSc4	cosi
<g/>
,	,	kIx,	,
čemu	co	k3yRnSc3	co
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
svatá	svatý	k2eAgFnSc1d1	svatá
prostoto	prostota	k1gFnSc5	prostota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
takový	takový	k3xDgInSc4	takový
hlučný	hlučný	k2eAgInSc4d1	hlučný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
nastane	nastat	k5eAaPmIp3nS	nastat
mi	já	k3xPp1nSc3	já
jich	on	k3xPp3gFnPc2	on
teď	teď	k6eAd1	teď
několik	několik	k4yIc4	několik
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
určitě	určitě	k6eAd1	určitě
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
připadám	připadat	k5eAaPmIp1nS	připadat
jako	jako	k9	jako
vykázaný	vykázaný	k2eAgInSc4d1	vykázaný
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
na	na	k7c4	na
krok	krok	k1gInSc4	krok
jako	jako	k8xC	jako
jindy	jindy	k6eAd1	jindy
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1923	[number]	k4	1923
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Miquelstraße	Miquelstraße	k1gInSc1	Miquelstraße
8	[number]	k4	8
-	-	kIx~	-
dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1923	[number]	k4	1923
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Berlín-Steglitz	Berlín-Steglitz	k1gInSc1	Berlín-Steglitz
<g/>
,	,	kIx,	,
vila	vila	k1gFnSc1	vila
v	v	k7c6	v
Grunewaldstraße	Grunewaldstraße	k1gFnSc6	Grunewaldstraße
13	[number]	k4	13
-	-	kIx~	-
2	[number]	k4	2
pokoje	pokoj	k1gInSc2	pokoj
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
Der	drát	k5eAaImRp2nS	drát
österreichische	österreichische	k1gNnSc4	österreichische
Dichter	Dichter	k1gInSc1	Dichter
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
geboren	geborno	k1gNnPc2	geborno
am	am	k?	am
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Juli	Juli	k6eAd1	Juli
1883	[number]	k4	1883
in	in	k?	in
Prag	Praga	k1gFnPc2	Praga
<g/>
,	,	kIx,	,
verstorben	verstorbna	k1gFnPc2	verstorbna
am	am	k?	am
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Juni	Juni	k6eAd1	Juni
1924	[number]	k4	1924
in	in	k?	in
Wien	Wien	k1gMnSc1	Wien
<g/>
/	/	kIx~	/
<g/>
Klosterneuburg	Klosterneuburg	k1gMnSc1	Klosterneuburg
<g/>
,	,	kIx,	,
wohnte	wohnt	k1gInSc5	wohnt
in	in	k?	in
diesem	dieso	k1gNnSc7	dieso
Hause	Hause	k1gFnSc2	Hause
vom	vom	k?	vom
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
November	November	k1gInSc1	November
1923	[number]	k4	1923
bis	bis	k?	bis
zum	zum	k?	zum
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Februar	Februar	k1gInSc1	Februar
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Republik	republika	k1gFnPc2	republika
Österreich	Österreich	k1gMnSc1	Österreich
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
-	-	kIx~	-
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Zehlendorfer	Zehlendorfer	k1gInSc1	Zehlendorfer
Busseallee	Busseallee	k1gFnSc1	Busseallee
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Sanatorium	sanatorium	k1gNnSc1	sanatorium
Wienerwald	Wienerwalda	k1gFnPc2	Wienerwalda
<g/>
,	,	kIx,	,
Ortmann	Ortmann	k1gNnSc1	Ortmann
<g/>
/	/	kIx~	/
<g/>
Pernitz	Pernitz	k1gInSc1	Pernitz
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
pobyt	pobyt	k1gInSc4	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
nemocnice	nemocnice	k1gFnSc1	nemocnice
(	(	kIx(	(
<g/>
Allgemeines	Allgemeines	k1gMnSc1	Allgemeines
Krankenhaus	Krankenhaus	k1gMnSc1	Krankenhaus
<g/>
)	)	kIx)	)
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Laryngologická	laryngologický	k2eAgFnSc1d1	laryngologický
klinika	klinika	k1gFnSc1	klinika
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Markuse	Markuse	k1gFnSc1	Markuse
Hajka	hajek	k1gMnSc2	hajek
<g/>
.	.	kIx.	.
</s>
<s>
Vícelůžkový	Vícelůžkový	k2eAgInSc4d1	Vícelůžkový
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblečení	oblečení	k1gNnSc6	oblečení
vážil	vážit	k5eAaImAgInS	vážit
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
kg	kg	kA	kg
<g/>
.	.	kIx.	.
pobyt	pobyt	k1gInSc1	pobyt
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Sanatorium	sanatorium	k1gNnSc1	sanatorium
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
Hauptstraße	Hauptstraße	k1gFnSc1	Hauptstraße
187	[number]	k4	187
<g/>
,	,	kIx,	,
Kierling	Kierling	k1gInSc1	Kierling
u	u	k7c2	u
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
-	-	kIx~	-
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
trpěl	trpět	k5eAaImAgMnS	trpět
silnými	silný	k2eAgFnPc7d1	silná
bolestmi	bolest	k1gFnPc7	bolest
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
nemohl	moct	k5eNaImAgMnS	moct
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
dorozumíval	dorozumívat	k5eAaImAgMnS	dorozumívat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
lístečků	lísteček	k1gInPc2	lísteček
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
doktora	doktor	k1gMnSc4	doktor
Roberta	Robert	k1gMnSc2	Robert
Klopstocka	Klopstocko	k1gNnSc2	Klopstocko
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vyšší	vysoký	k2eAgFnSc4d2	vyšší
dávku	dávka	k1gFnSc4	dávka
morfia	morfium	k1gNnSc2	morfium
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zabijte	zabít	k5eAaPmRp2nP	zabít
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jste	být	k5eAaImIp2nP	být
vrah	vrah	k1gMnSc1	vrah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
se	se	k3xPyFc4	se
také	také	k9	také
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
lékaře	lékař	k1gMnSc4	lékař
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neodcházejte	odcházet	k5eNaImRp2nP	odcházet
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Překvapený	překvapený	k2eAgMnSc1d1	překvapený
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
právě	právě	k6eAd1	právě
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
otočil	otočit	k5eAaPmAgMnS	otočit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
já	já	k3xPp1nSc1	já
přeci	přeci	k?	přeci
neodcházím	odcházet	k5eNaImIp1nS	odcházet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
já	já	k3xPp1nSc1	já
odcházím	odcházet	k5eAaImIp1nS	odcházet
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
spisovatele	spisovatel	k1gMnSc2	spisovatel
otevřena	otevřít	k5eAaPmNgFnS	otevřít
studijní	studijní	k2eAgFnSc1d1	studijní
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc1d1	pamětní
síň	síň	k1gFnSc1	síň
(	(	kIx(	(
<g/>
Studien-	Studien-	k1gFnSc1	Studien-
und	und	k?	und
Gedenkraum	Gedenkraum	k1gInSc1	Gedenkraum
<g/>
)	)	kIx)	)
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
90	[number]	k4	90
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Kafkova	Kafkův	k2eAgNnSc2d1	Kafkovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
výstava	výstava	k1gFnSc1	výstava
přeinstalována	přeinstalován	k2eAgFnSc1d1	přeinstalována
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
a	a	k8xC	a
pohlednice	pohlednice	k1gFnPc1	pohlednice
(	(	kIx(	(
<g/>
kopie	kopie	k1gFnPc1	kopie
<g/>
)	)	kIx)	)
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
epizodu	epizoda	k1gFnSc4	epizoda
života	život	k1gInSc2	život
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
pohřeb	pohřeb	k1gInSc1	pohřeb
-	-	kIx~	-
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c4	v
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
-	-	kIx~	-
hrob	hrob	k1gInSc1	hrob
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
21	[number]	k4	21
-	-	kIx~	-
14	[number]	k4	14
-	-	kIx~	-
21	[number]	k4	21
<g/>
)	)	kIx)	)
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
asi	asi	k9	asi
sto	sto	k4xCgNnSc1	sto
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Náhrobek	náhrobek	k1gInSc1	náhrobek
na	na	k7c6	na
privilegované	privilegovaný	k2eAgFnSc6d1	privilegovaná
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hlavní	hlavní	k2eAgFnSc2d1	hlavní
brány	brána	k1gFnSc2	brána
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
šestibokého	šestiboký	k2eAgInSc2d1	šestiboký
krystalu	krystal	k1gInSc2	krystal
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
pohřbených	pohřbený	k2eAgFnPc2d1	pohřbená
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
stěně	stěna	k1gFnSc6	stěna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
architekta	architekt	k1gMnSc2	architekt
Leopolda	Leopold	k1gMnSc2	Leopold
Ehrmanna	Ehrmann	k1gMnSc2	Ehrmann
<g/>
,	,	kIx,	,
renovátora	renovátor	k1gMnSc2	renovátor
smíchovské	smíchovský	k2eAgFnSc2d1	Smíchovská
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
na	na	k7c4	na
zdi	zeď	k1gFnPc4	zeď
má	mít	k5eAaImIp3nS	mít
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
Kafkův	Kafkův	k2eAgMnSc1d1	Kafkův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Památce	památka	k1gFnSc3	památka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
<g/>
,	,	kIx,	,
pražského	pražský	k2eAgMnSc2d1	pražský
rodáka	rodák	k1gMnSc2	rodák
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
myslitele	myslitel	k1gMnSc2	myslitel
<g/>
,	,	kIx,	,
průkopníka	průkopník	k1gMnSc2	průkopník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
přítele	přítel	k1gMnSc2	přítel
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
a	a	k8xC	a
vykladatele	vykladatele	k?	vykladatele
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
náboženská	náboženský	k2eAgFnSc1d1	náboženská
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
-	-	kIx~	-
odhalen	odhalit	k5eAaPmNgInS	odhalit
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
pomník	pomník	k1gInSc4	pomník
mezi	mezi	k7c7	mezi
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
se	s	k7c7	s
Španělskou	španělský	k2eAgFnSc7d1	španělská
synagogou	synagoga	k1gFnSc7	synagoga
-	-	kIx~	-
postava	postava	k1gFnSc1	postava
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
sedícího	sedící	k2eAgMnSc2d1	sedící
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
bezhlavé	bezhlavý	k2eAgFnSc2d1	bezhlavá
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
plastika	plastika	k1gFnSc1	plastika
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
3,75	[number]	k4	3,75
m	m	kA	m
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
700	[number]	k4	700
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Sochař	sochař	k1gMnSc1	sochař
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Róna	Róna	k1gMnSc1	Róna
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
povídkou	povídka	k1gFnSc7	povídka
Popis	popis	k1gInSc1	popis
jednoho	jeden	k4xCgInSc2	jeden
zápasu	zápas	k1gInSc2	zápas
(	(	kIx(	(
<g/>
...	...	k?	...
A	a	k9	a
již	již	k6eAd1	již
jsem	být	k5eAaImIp1nS	být
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
-	-	kIx~	-
jedním	jeden	k4xCgInSc7	jeden
švihem	švih	k1gInSc7	švih
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
-	-	kIx~	-
svému	svůj	k1gMnSc3	svůj
známému	známý	k2eAgMnSc3d1	známý
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
údery	úder	k1gInPc4	úder
pěstí	pěstit	k5eAaImIp3nP	pěstit
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gInSc4	on
přiměl	přimět	k5eAaPmAgMnS	přimět
k	k	k7c3	k
lehkému	lehký	k2eAgInSc3d1	lehký
klusu	klus	k1gInSc3	klus
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
rozkročena	rozkročen	k2eAgFnSc1d1	rozkročena
a	a	k8xC	a
tak	tak	k6eAd1	tak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
dva	dva	k4xCgInPc4	dva
světy	svět	k1gInPc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Židovském	židovský	k2eAgNnSc6d1	Židovské
městě	město	k1gNnSc6	město
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
tudy	tudy	k6eAd1	tudy
probíhá	probíhat	k5eAaImIp3nS	probíhat
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlažbě	dlažba	k1gFnSc6	dlažba
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
šváb	šváb	k1gMnSc1	šváb
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nám	my	k3xPp1nPc3	my
připomíná	připomínat	k5eAaImIp3nS	připomínat
povídku	povídka	k1gFnSc4	povídka
Proměna	proměna	k1gFnSc1	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Plastika	plastika	k1gFnSc1	plastika
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
posazena	posazen	k2eAgFnSc1d1	posazena
na	na	k7c6	na
krovkách	krovka	k1gFnPc6	krovka
brouka	brouk	k1gMnSc2	brouk
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
proměnil	proměnit	k5eAaPmAgMnS	proměnit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Řehoř	Řehoř	k1gMnSc1	Řehoř
Samsa	Samsa	k1gFnSc1	Samsa
<g/>
.	.	kIx.	.
</s>
<s>
Iniciátorem	iniciátor	k1gMnSc7	iniciátor
vybudování	vybudování	k1gNnSc2	vybudování
pomníku	pomník	k1gInSc6	pomník
byla	být	k5eAaImAgFnS	být
Společnost	společnost	k1gFnSc1	společnost
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
120	[number]	k4	120
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgInPc1d1	možný
výklady	výklad	k1gInPc1	výklad
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
/	/	kIx~	/
Kafka	Kafka	k1gMnSc1	Kafka
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
svého	své	k1gNnSc2	své
druhého	druhý	k4xOgNnSc2	druhý
ega	ego	k1gNnSc2	ego
(	(	kIx(	(
<g/>
zástupného	zástupný	k2eAgMnSc2d1	zástupný
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
)	)	kIx)	)
b	b	k?	b
<g/>
/	/	kIx~	/
Prázdný	prázdný	k2eAgInSc1d1	prázdný
oblek	oblek	k1gInSc1	oblek
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
silného	silný	k2eAgMnSc4d1	silný
a	a	k8xC	a
necitlivého	citlivý	k2eNgMnSc4d1	necitlivý
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
c	c	k0	c
<g/>
/	/	kIx~	/
Jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
velkou	velký	k2eAgFnSc4d1	velká
postavu	postava	k1gFnSc4	postava
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
neuchopitelnému	uchopitelný	k2eNgInSc3d1	neuchopitelný
světu	svět	k1gInSc3	svět
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
osudu	osud	k1gInSc3	osud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nás	my	k3xPp1nPc4	my
unáší	unášet	k5eAaImIp3nS	unášet
<g/>
,	,	kIx,	,
a	a	k8xC	a
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
domníváme	domnívat	k5eAaImIp1nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vedeme	vést	k5eAaImIp1nP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
spád	spád	k1gInSc1	spád
snad	snad	k9	snad
můžeme	moct	k5eAaImIp1nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
hlava	hlava	k1gFnSc1	hlava
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
-	-	kIx~	-
odhalena	odhalen	k2eAgFnSc1d1	odhalena
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
mobilní	mobilní	k2eAgNnSc4d1	mobilní
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
lesku	lesk	k1gInSc2	lesk
na	na	k7c6	na
piazzettě	piazzetta	k1gFnSc6	piazzetta
za	za	k7c7	za
obchodním	obchodní	k2eAgInSc7d1	obchodní
a	a	k8xC	a
kancelářským	kancelářský	k2eAgInSc7d1	kancelářský
komplexem	komplex	k1gInSc7	komplex
Quadrio	Quadrio	k6eAd1	Quadrio
nedaleko	nedaleko	k7c2	nedaleko
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Národní	národní	k2eAgFnSc1d1	národní
třída	třída	k1gFnSc1	třída
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
David	David	k1gMnSc1	David
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
39	[number]	k4	39
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
24	[number]	k4	24
tun	tuna	k1gFnPc2	tuna
nerezového	rezový	k2eNgInSc2d1	nerezový
plechu	plech	k1gInSc2	plech
<g/>
)	)	kIx)	)
vážící	vážící	k2eAgFnSc1d1	vážící
busta	busta	k1gFnSc1	busta
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
42	[number]	k4	42
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
se	se	k3xPyFc4	se
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
patra	patro	k1gNnPc1	patro
sochy	socha	k1gFnSc2	socha
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
otáčet	otáčet	k5eAaImF	otáčet
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
10,6	[number]	k4	10,6
m	m	kA	m
<g/>
)	)	kIx)	)
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
21	[number]	k4	21
motorových	motorový	k2eAgInPc2d1	motorový
modulů	modul	k1gInPc2	modul
<g/>
,	,	kIx,	,
2	[number]	k4	2
staticko-dynamické	statickoynamický	k2eAgInPc1d1	staticko-dynamický
posudky	posudek	k1gInPc1	posudek
<g/>
,	,	kIx,	,
1	[number]	k4	1
km	km	kA	km
kabelů	kabel	k1gInPc2	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Investor	investor	k1gMnSc1	investor
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
30	[number]	k4	30
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
Museum	museum	k1gNnSc4	museum
-	-	kIx~	-
Muzeum	muzeum	k1gNnSc1	muzeum
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
Cihelná	cihelný	k2eAgFnSc1d1	cihelná
2	[number]	k4	2
<g/>
b.	b.	k?	b.
6	[number]	k4	6
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgFnPc4	všechen
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
švagr	švagr	k1gMnSc1	švagr
Josef	Josef	k1gMnSc1	Josef
Pollak	Pollak	k1gMnSc1	Pollak
<g/>
,	,	kIx,	,
neteř	teřet	k5eNaImRp2nS	teřet
Hanne	Hann	k1gMnSc5	Hann
<g/>
,	,	kIx,	,
synovec	synovec	k1gMnSc1	synovec
Felix	Felix	k1gMnSc1	Felix
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1882	[number]	k4	1882
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Löwy	Löwa	k1gFnSc2	Löwa
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FRANZ	FRANZ	kA	FRANZ
KAFKA	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Georg	Georg	k1gMnSc1	Georg
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Gabrielle	Gabrielle	k1gFnSc1	Gabrielle
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Hermann	Hermann	k1gMnSc1	Hermann
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
tyfus	tyfus	k1gInSc1	tyfus
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Gerti	Gerti	k1gNnSc1	Gerti
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hanne	Hannout	k5eAaImIp3nS	Hannout
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Valerie	Valerie	k1gFnSc1	Valerie
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Josef	Josef	k1gMnSc1	Josef
Pollak	Pollak	k1gMnSc1	Pollak
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Marianne	Mariannout	k5eAaImIp3nS	Mariannout
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Lotte	Lotte	k5eAaPmIp2nP	Lotte
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ottilie	Ottilie	k1gFnSc1	Ottilie
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1920	[number]	k4	1920
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josef	Josef	k1gMnSc1	Josef
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
Karel	Karel	k1gMnSc1	Karel
Projsa	Projs	k1gMnSc2	Projs
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
utopil	utopit	k5eAaPmAgMnS	utopit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
znovu	znovu	k6eAd1	znovu
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Kafkův	Kafkův	k2eAgInSc4d1	Kafkův
Dopis	dopis	k1gInSc4	dopis
otci	otec	k1gMnSc3	otec
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
+	+	kIx~	+
5	[number]	k4	5
<g/>
letý	letý	k2eAgInSc4d1	letý
na	na	k7c4	na
leukemii	leukemie	k1gFnSc4	leukemie
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
MUDr.	MUDr.	kA	MUDr.
Helena	Helena	k1gFnSc1	Helena
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
∞	∞	k?	∞
MUDr.	MUDr.	kA	MUDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kostrouch	Kostrouch	k1gMnSc1	Kostrouch
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
?	?	kIx.	?
</s>
<s>
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
?	?	kIx.	?
</s>
<s>
Majuskulou	Majuskula	k1gFnSc7	Majuskula
a	a	k8xC	a
tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
zvýrazněni	zvýrazněn	k2eAgMnPc1d1	zvýrazněn
přímí	přímý	k2eAgMnPc1d1	přímý
předkové	předek	k1gMnPc1	předek
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
-	-	kIx~	-
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
generace	generace	k1gFnSc1	generace
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
C	C	kA	C
-	-	kIx~	-
generace	generace	k1gFnSc1	generace
jeho	jeho	k3xOp3gMnPc2	jeho
prarodičů	prarodič	k1gMnPc2	prarodič
atd.	atd.	kA	atd.
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FRANZ	FRANZ	kA	FRANZ
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
HERMANN	Hermann	k1gMnSc1	Hermann
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
a.	a.	k?	a.
Filip	Filip	k1gMnSc1	Filip
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
nebo	nebo	k8xC	nebo
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Klára	Klára	k1gFnSc1	Klára
Poláček	Poláček	k1gMnSc1	Poláček
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
nebo	nebo	k8xC	nebo
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Erich	Erich	k1gMnSc1	Erich
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
(	(	kIx(	(
<g/>
Beck	Beck	k1gMnSc1	Beck
<g/>
)	)	kIx)	)
Schweitzer	Schweitzer	k1gMnSc1	Schweitzer
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
b.	b.	k?	b.
Anna	Anna	k1gFnSc1	Anna
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Jakob	Jakob	k1gMnSc1	Jakob
Adler	Adler	k1gMnSc1	Adler
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
c.	c.	k?	c.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Karoline	Karolin	k1gInSc5	Karolin
Fleischner	Fleischner	k1gMnSc1	Fleischner
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
d.	d.	k?	d.
Julie	Julie	k1gFnSc1	Julie
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Siegmund	Siegmund	k1gMnSc1	Siegmund
Ehrmann	Ehrmann	k1gMnSc1	Ehrmann
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Martha	Martha	k1gMnSc1	Martha
Weisskopf	Weisskopf	k1gMnSc1	Weisskopf
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Kaufmann	Kaufmanna	k1gFnPc2	Kaufmanna
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Fanni	Faneň	k1gFnSc3	Faneň
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Ida	Ida	k1gFnSc1	Ida
Bergmann	Bergmanna	k1gFnPc2	Bergmanna
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
d	d	k?	d
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
e.	e.	k?	e.
Ludwig	Ludwig	k1gMnSc1	Ludwig
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Laura	Laura	k1gFnSc1	Laura
Weingarten	Weingartno	k1gNnPc2	Weingartno
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
e	e	k0	e
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hedwig	Hedwig	k1gMnSc1	Hedwig
Löw	Löw	k1gMnSc1	Löw
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
1	[number]	k4	1
<g/>
e	e	k0	e
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Irma	Irma	k1gFnSc1	Irma
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
JAKOB	Jakob	k1gMnSc1	Jakob
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
-	-	kIx~	-
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
z	z	k7c2	z
Oseka	Osek	k1gInSc2	Osek
[	[	kIx(	[
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
a.	a.	k?	a.
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
Jakoba	Jakob	k1gMnSc2	Jakob
Kafky	Kafka	k1gMnSc2	Kafka
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Samuel	Samuel	k1gMnSc1	Samuel
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Rosalie	Rosalie	k1gFnSc1	Rosalie
Unger	Unger	k1gMnSc1	Unger
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Angelus	Angelus	k1gInSc1	Angelus
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Therese	Therese	k1gFnSc1	Therese
Rosenauer	Rosenaura	k1gFnPc2	Rosenaura
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Wilhelmine	Wilhelmin	k1gInSc5	Wilhelmin
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Moritz	moritz	k1gInSc1	moritz
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Henriette	Henriett	k1gMnSc5	Henriett
Weiss	Weiss	k1gMnSc1	Weiss
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Johanna	Johann	k1gMnSc4	Johann
Wilhartitz	Wilhartitza	k1gFnPc2	Wilhartitza
BC	BC	kA	BC
<g/>
1	[number]	k4	1
<g/>
a	a	k8xC	a
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
b.	b.	k?	b.
Katl	Katl	k1gMnSc1	Katl
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
c.	c.	k?	c.
Magdalena	Magdalena	k1gFnSc1	Magdalena
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
/	/	kIx~	/
<g/>
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
d.	d.	k?	d.
Salomon	Salomon	k1gMnSc1	Salomon
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
e.	e.	k?	e.
Franzl	Franzl	k1gMnSc1	Franzl
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
f.	f.	k?	f.
Karolina	Karolinum	k1gNnSc2	Karolinum
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
-	-	kIx~	-
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
g.	g.	k?	g.
Jonas	Jonas	k1gMnSc1	Jonas
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
h.	h.	k?	h.
Leopold	Leopold	k1gMnSc1	Leopold
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
∞	∞	k?	∞
FRANZISKA	FRANZISKA	kA	FRANZISKA
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Platowski	Platowsk	k1gFnSc2	Platowsk
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
-	-	kIx~	-
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
↑	↑	k?	↑
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
a.	a.	k?	a.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
b.	b.	k?	b.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
c.	c.	k?	c.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
d.	d.	k?	d.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
e.	e.	k?	e.
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
a.	a.	k?	a.
Leopold	Leopold	k1gMnSc1	Leopold
Platowski	Platowsk	k1gFnSc2	Platowsk
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
b.	b.	k?	b.
Maria	Maria	k1gFnSc1	Maria
Platowski	Platowsk	k1gFnSc2	Platowsk
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
JAKOB	Jakob	k1gMnSc1	Jakob
Platowski	Platowsk	k1gFnSc2	Platowsk
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
-	-	kIx~	-
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
-	-	kIx~	-
pradědeček	pradědeček	k1gMnSc1	pradědeček
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
∞	∞	k?	∞
ANNA	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Ratzek	Ratzek	k1gInSc1	Ratzek
-	-	kIx~	-
prababička	prababička	k1gFnSc1	prababička
(	(	kIx(	(
<g/>
↑	↑	k?	↑
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
a.	a.	k?	a.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
∞	∞	k?	∞
1882	[number]	k4	1882
JULIE	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Löwy	Löwa	k1gFnSc2	Löwa
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
jejích	její	k3xOp3gFnPc2	její
6	[number]	k4	6
dětí	dítě	k1gFnPc2	dítě
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
rozrodu	rozrod	k1gInSc6	rozrod
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
a.	a.	k?	a.
Alfred	Alfred	k1gMnSc1	Alfred
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
strýc	strýc	k1gMnSc1	strýc
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
b.	b.	k?	b.
Richard	Richard	k1gMnSc1	Richard
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Hedwig	Hedwig	k1gMnSc1	Hedwig
Trebitsch	Trebitsch	k1gMnSc1	Trebitsch
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
nebo	nebo	k8xC	nebo
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Martha	Martha	k1gFnSc1	Martha
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
AB	AB	kA	AB
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Gertrud	Gertrud	k1gInSc1	Gertrud
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
c.	c.	k?	c.
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Jeanne	Jeann	k1gMnSc5	Jeann
Frè	Frè	k1gMnSc5	Frè
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
JAKOB	Jakob	k1gMnSc1	Jakob
Löwy	Löwa	k1gFnSc2	Löwa
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
-	-	kIx~	-
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
a.	a.	k?	a.
Samuel	Samuel	k1gMnSc1	Samuel
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
b.	b.	k?	b.
Aaron	Aaron	k1gMnSc1	Aaron
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
c.	c.	k?	c.
Michael	Michael	k1gMnSc1	Michael
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
d.	d.	k?	d.
Leopold	Leopold	k1gMnSc1	Leopold
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
nebo	nebo	k8xC	nebo
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
ISAAK	ISAAK	kA	ISAAK
Löwy	Löwy	k1gInPc1	Löwy
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
-	-	kIx~	-
<g/>
1841	[number]	k4	1841
nebo	nebo	k8xC	nebo
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
-	-	kIx~	-
pradědeček	pradědeček	k1gMnSc1	pradědeček
D	D	kA	D
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
∞	∞	k?	∞
KATHARINA	KATHARINA	kA	KATHARINA
nebo	nebo	k8xC	nebo
KAROLINA	Karolinum	k1gNnSc2	Karolinum
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Wiener	Wiener	k1gInSc1	Wiener
-	-	kIx~	-
prababička	prababička	k1gFnSc1	prababička
(	(	kIx(	(
<g/>
↑	↑	k?	↑
její	její	k3xOp3gFnPc4	její
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
a.	a.	k?	a.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
b.	b.	k?	b.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
c.	c.	k?	c.
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
d.	d.	k?	d.
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
ESTHER	ESTHER	kA	ESTHER
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Porias	Porias	k1gInSc4	Porias
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
-	-	kIx~	-
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
↑	↑	k?	↑
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
a.	a.	k?	a.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
b.	b.	k?	b.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
c.	c.	k?	c.
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
a.	a.	k?	a.
Nathan	Nathan	k1gMnSc1	Nathan
Porias	Porias	k1gMnSc1	Porias
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ADAM	Adam	k1gMnSc1	Adam
Porias	Porias	k1gMnSc1	Porias
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
-	-	kIx~	-
pradědeček	pradědeček	k1gMnSc1	pradědeček
D	D	kA	D
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
∞	∞	k?	∞
SARA	SARA	kA	SARA
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Levit	levit	k5eAaImF	levit
nebo	nebo	k8xC	nebo
Levitner	Levitner	k1gInSc1	Levitner
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
-	-	kIx~	-
prababička	prababička	k1gFnSc1	prababička
(	(	kIx(	(
<g/>
↑	↑	k?	↑
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
a.	a.	k?	a.
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
Julie	Julie	k1gFnSc1	Julie
Heller	Heller	k1gMnSc1	Heller
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
↓	↓	k?	↓
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
d.	d.	k?	d.
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
e.	e.	k?	e.
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
d.	d.	k?	d.
Rudolf	Rudolf	k1gMnSc1	Rudolf
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
e.	e.	k?	e.
Siegfried	Siegfried	k1gMnSc1	Siegfried
Löwy	Löwa	k1gMnSc2	Löwa
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
)	)	kIx)	)
-	-	kIx~	-
strýc	strýc	k1gMnSc1	strýc
z	z	k7c2	z
Třešti	třeštit	k5eAaImRp2nS	třeštit
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Za	za	k7c2	za
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
práce	práce	k1gFnSc2	práce
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
považovány	považován	k2eAgInPc1d1	považován
romány	román	k1gInPc1	román
Proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
Nezvěstný	zvěstný	k2eNgInSc4d1	nezvěstný
(	(	kIx(	(
<g/>
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
povídka	povídka	k1gFnSc1	povídka
Proměna	proměna	k1gFnSc1	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
část	část	k1gFnSc1	část
Kafkova	Kafkův	k2eAgNnSc2d1	Kafkovo
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
téměř	téměř	k6eAd1	téměř
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
autorem	autor	k1gMnSc7	autor
se	se	k3xPyFc4	se
Kafka	Kafka	k1gMnSc1	Kafka
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klasikům	klasik	k1gMnPc3	klasik
literatury	literatura	k1gFnSc2	literatura
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jedněm	jeden	k4xCgFnPc3	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
inovátorů	inovátor	k1gMnPc2	inovátor
románové	románový	k2eAgFnSc2d1	románová
formy	forma	k1gFnSc2	forma
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
motivy	motiv	k1gInPc7	motiv
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
pocity	pocit	k1gInPc1	pocit
vyřazenosti	vyřazenost	k1gFnSc2	vyřazenost
a	a	k8xC	a
izolovanosti	izolovanost	k1gFnSc2	izolovanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
humorné	humorný	k2eAgNnSc1d1	humorné
a	a	k8xC	a
ironické	ironický	k2eAgNnSc1d1	ironické
líčení	líčení	k1gNnSc1	líčení
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
podstatným	podstatný	k2eAgInSc7d1	podstatný
rysem	rys	k1gInSc7	rys
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
předmětem	předmět	k1gInSc7	předmět
ponižování	ponižování	k1gNnSc2	ponižování
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnPc1	ukončení
příběhů	příběh	k1gInPc2	příběh
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
tragická	tragický	k2eAgNnPc1d1	tragické
a	a	k8xC	a
bezvýchodná	bezvýchodný	k2eAgNnPc1d1	bezvýchodné
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Damenbrevier	Damenbrevier	k1gMnSc1	Damenbrevier
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Gespräch	Gespräch	k1gInSc1	Gespräch
mit	mit	k?	mit
dem	dem	k?	dem
Beter	Beter	k1gMnSc1	Beter
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Gespräch	Gespräch	k1gInSc1	Gespräch
mit	mit	k?	mit
dem	dem	k?	dem
Betrunkenen	Betrunkenna	k1gFnPc2	Betrunkenna
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Aeroplane	Aeroplan	k1gMnSc5	Aeroplan
in	in	k?	in
Brescia	Brescius	k1gMnSc2	Brescius
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Großer	Großer	k1gMnSc1	Großer
Lärm	Lärm	k1gMnSc1	Lärm
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
in	in	k?	in
Herder-Blätter	Herder-Blätter	k1gMnSc1	Herder-Blätter
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
S.	S.	kA	S.
44	[number]	k4	44
Betrachtung	Betrachtunga	k1gFnPc2	Betrachtunga
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Rozjímání	rozjímání	k1gNnSc1	rozjímání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Urteil	Urteil	k1gMnSc1	Urteil
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Ortel	ortel	k1gInSc1	ortel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
in	in	k?	in
Arkadia	Arkadium	k1gNnSc2	Arkadium
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Jahrbuch	Jahrbuch	k1gMnSc1	Jahrbuch
für	für	k?	für
Dichtkunst	Dichtkunst	k1gMnSc1	Dichtkunst
<g/>
,	,	kIx,	,
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
připravil	připravit	k5eAaPmAgMnS	připravit
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
:	:	kIx,	:
Kurt	Kurt	k1gMnSc1	Kurt
Wolf	Wolf	k1gMnSc1	Wolf
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
53	[number]	k4	53
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
Der	drát	k5eAaImRp2nS	drát
Heizer	Heizer	k1gMnSc1	Heizer
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Topič	topič	k1gInSc1	topič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kapitola	kapitola	k1gFnSc1	kapitola
románového	románový	k2eAgInSc2d1	románový
fragmentu	fragment	k1gInSc2	fragment
Der	drát	k5eAaImRp2nS	drát
Verschollene	Verschollen	k1gInSc5	Verschollen
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Verwandlung	Verwandlung	k1gMnSc1	Verwandlung
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Proměna	proměna	k1gFnSc1	proměna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
in	in	k?	in
Die	Die	k1gFnSc2	Die
Weissen	Weissna	k1gFnPc2	Weissna
Blätter	Blätter	k1gMnSc1	Blätter
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
č.	č.	k?	č.
10	[number]	k4	10
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
S.	S.	kA	S.
1177	[number]	k4	1177
<g/>
-	-	kIx~	-
<g/>
1230	[number]	k4	1230
Ein	Ein	k1gMnSc1	Ein
Brudermord	Brudermord	k1gMnSc1	Brudermord
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
vydána	vydán	k2eAgFnSc1d1	vydána
též	též	k9	též
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
lehce	lehko	k6eAd1	lehko
odlišné	odlišný	k2eAgFnSc6d1	odlišná
podobě	podoba	k1gFnSc6	podoba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Der	drát	k5eAaImRp2nS	drát
Mord	mord	k1gInSc1	mord
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ein	Ein	k1gMnSc2	Ein
Brudermord	Brudermordo	k1gNnPc2	Brudermordo
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
in	in	k?	in
Marsyas	Marsyas	k1gInSc1	Marsyas
<g/>
,	,	kIx,	,
Semesterband	Semesterband	k1gInSc1	Semesterband
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
<g/>
/	/	kIx~	/
<g/>
srpen	srpen	k1gInSc1	srpen
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
S.	S.	kA	S.
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
In	In	k1gFnPc2	In
der	drát	k5eAaImRp2nS	drát
Strafkolonie	Strafkolonie	k1gFnSc2	Strafkolonie
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
V	v	k7c6	v
kárném	kárný	k2eAgInSc6d1	kárný
táboře	tábor	k1gInSc6	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Landarzt	Landarzt	k1gMnSc1	Landarzt
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Venkovský	venkovský	k2eAgMnSc1d1	venkovský
lékař	lékař	k1gMnSc1	lékař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Kübelreiter	Kübelreiter	k1gMnSc1	Kübelreiter
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jezdec	jezdec	k1gMnSc1	jezdec
na	na	k7c6	na
uhláku	uhlák	k1gInSc6	uhlák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Hungerkünstler	Hungerkünstler	k1gMnSc1	Hungerkünstler
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Umělec	umělec	k1gMnSc1	umělec
v	v	k7c6	v
hladovění	hladovění	k1gNnSc6	hladovění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beschreibung	Beschreibung	k1gMnSc1	Beschreibung
eines	eines	k1gMnSc1	eines
Kampfes	Kampfes	k1gMnSc1	Kampfes
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
až	až	k9	až
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Popis	popis	k1gInSc1	popis
jednoho	jeden	k4xCgInSc2	jeden
zápasu	zápas	k1gInSc2	zápas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hochzeitsvorbereitungen	Hochzeitsvorbereitungen	k1gInSc1	Hochzeitsvorbereitungen
auf	auf	k?	auf
dem	dem	k?	dem
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
až	až	k9	až
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Svatební	svatební	k2eAgFnSc2d1	svatební
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Dorfschullehrer	Dorfschullehrer	k1gMnSc1	Dorfschullehrer
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
až	až	k9	až
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Obří	obří	k2eAgMnSc1d1	obří
krtek	krtek	k1gMnSc1	krtek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brodův	Brodův	k2eAgInSc1d1	Brodův
titul	titul	k1gInSc1	titul
Der	drát	k5eAaImRp2nS	drát
Riesenmaulwurf	Riesenmaulwurf	k1gInSc4	Riesenmaulwurf
<g/>
.	.	kIx.	.
</s>
<s>
Blumfeld	Blumfeld	k1gMnSc1	Blumfeld
<g/>
,	,	kIx,	,
ein	ein	k?	ein
älterer	älterer	k1gInSc1	älterer
Junggeselle	Junggeselle	k1gFnSc1	Junggeselle
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Gruftwächter	Gruftwächter	k1gMnSc1	Gruftwächter
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
až	až	k9	až
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Jäger	Jäger	k1gMnSc1	Jäger
Gracchus	Gracchus	k1gMnSc1	Gracchus
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Myslivec	Myslivec	k1gMnSc1	Myslivec
Gracchus	Gracchus	k1gMnSc1	Gracchus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
vydán	vydat	k5eAaPmNgInS	vydat
Brodem	Brod	k1gInSc7	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Beim	Beim	k6eAd1	Beim
Bau	Bau	k1gFnSc1	Bau
der	drát	k5eAaImRp2nS	drát
Chinesischen	Chinesischna	k1gFnPc2	Chinesischna
Mauer	Mauer	k1gInSc1	Mauer
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k6eAd1	Eine
alltägliche	alltäglichat	k5eAaPmIp3nS	alltäglichat
Verwirrung	Verwirrung	k1gInSc1	Verwirrung
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
vydán	vydat	k5eAaPmNgInS	vydat
Brodem	Brod	k1gInSc7	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Brief	Brief	k1gInSc1	Brief
an	an	k?	an
den	den	k1gInSc4	den
Vater	vatra	k1gFnPc2	vatra
<g/>
,	,	kIx,	,
rukopis	rukopis	k1gInSc1	rukopis
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dopis	dopis	k1gInSc1	dopis
otci	otec	k1gMnSc6	otec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kleine	Klein	k1gMnSc5	Klein
Fabel	Fabel	k1gMnSc1	Fabel
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
vydán	vydat	k5eAaPmNgInS	vydat
Brodem	Brod	k1gInSc7	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Geier	Geier	k1gMnSc1	Geier
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Sup	sup	k1gMnSc1	sup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Forschungen	Forschungen	k1gInSc1	Forschungen
eines	eines	k1gMnSc1	eines
Hundes	Hundes	k1gMnSc1	Hundes
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Výzkumy	výzkum	k1gInPc1	výzkum
jednoho	jeden	k4xCgMnSc4	jeden
psa	pes	k1gMnSc4	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Bau	Bau	k1gMnSc5	Bau
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
až	až	k9	až
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
vydán	vydat	k5eAaPmNgInS	vydat
Brodem	Brod	k1gInSc7	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Prozeß	Prozeß	k1gMnSc5	Prozeß
<g/>
,	,	kIx,	,
rukopis	rukopis	k1gInSc4	rukopis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
až	až	k6eAd1	až
1915	[number]	k4	1915
<g/>
;	;	kIx,	;
vydáno	vydat	k5eAaPmNgNnS	vydat
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Josef	Josef	k1gMnSc1	Josef
K.	K.	kA	K.
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgMnPc2	svůj
30	[number]	k4	30
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
K.	K.	kA	K.
brání	bránit	k5eAaImIp3nP	bránit
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
zjistit	zjistit	k5eAaPmF	zjistit
příčinu	příčina	k1gFnSc4	příčina
svého	svůj	k3xOyFgNnSc2	svůj
zatčení	zatčení	k1gNnSc2	zatčení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
začíná	začínat	k5eAaImIp3nS	začínat
věřit	věřit	k5eAaImF	věřit
ve	v	k7c4	v
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Schloß	Schloß	k1gFnSc1	Schloß
<g/>
,	,	kIx,	,
rukopis	rukopis	k1gInSc1	rukopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
;	;	kIx,	;
vydáno	vydat	k5eAaPmNgNnS	vydat
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Románový	románový	k2eAgInSc4d1	románový
fragment	fragment	k1gInSc4	fragment
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
náčrty	náčrt	k1gInPc4	náčrt
1912	[number]	k4	1912
<g/>
;	;	kIx,	;
vydáno	vydat	k5eAaPmNgNnS	vydat
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Románový	románový	k2eAgInSc4d1	románový
fragment	fragment	k1gInSc4	fragment
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Amerika	Amerika	k1gFnSc1	Amerika
nebo	nebo	k8xC	nebo
Nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brodův	Brodův	k2eAgInSc4d1	Brodův
titul	titul	k1gInSc4	titul
Amerika	Amerika	k1gFnSc1	Amerika
nahradil	nahradit	k5eAaPmAgInS	nahradit
původně	původně	k6eAd1	původně
Kafkou	Kafka	k1gMnSc7	Kafka
zamýšlený	zamýšlený	k2eAgInSc4d1	zamýšlený
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
Verschollene	Verschollen	k1gInSc5	Verschollen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příběh	příběh	k1gInSc4	příběh
mladíka	mladík	k1gMnSc2	mladík
cestujícího	cestující	k1gMnSc2	cestující
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
bydlící	bydlící	k2eAgFnSc6d1	bydlící
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgNnPc1d1	další
Kafkova	Kafkův	k2eAgNnPc1d1	Kafkovo
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
i	i	k8xC	i
tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
epická	epický	k2eAgFnSc1d1	epická
část	část	k1gFnSc1	část
velmi	velmi	k6eAd1	velmi
nevýrazná	výrazný	k2eNgFnSc1d1	nevýrazná
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
vykřičení	vykřičení	k1gNnSc4	vykřičení
pocitu	pocit	k1gInSc2	pocit
nevinně	vinně	k6eNd1	vinně
odsouzeného	odsouzený	k2eAgInSc2d1	odsouzený
<g/>
,	,	kIx,	,
bezvýchodně	bezvýchodně	k6eAd1	bezvýchodně
bloudícího	bloudící	k2eAgNnSc2d1	bloudící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Kafka	Kafka	k1gMnSc1	Kafka
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
třeba	třeba	k9	třeba
i	i	k9	i
úsměvným	úsměvný	k2eAgInPc3d1	úsměvný
momentům	moment	k1gInPc3	moment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nás	my	k3xPp1nPc2	my
z	z	k7c2	z
patologického	patologický	k2eAgInSc2d1	patologický
pocitu	pocit	k1gInSc2	pocit
vytrhávají	vytrhávat	k5eAaImIp3nP	vytrhávat
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
nám	my	k3xPp1nPc3	my
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vlastně	vlastně	k9	vlastně
smířit	smířit	k5eAaPmF	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončenost	nedokončenost	k1gFnSc1	nedokončenost
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vedle	vedle	k7c2	vedle
myšlenky	myšlenka	k1gFnSc2	myšlenka
odsouzení	odsouzení	k1gNnSc2	odsouzení
je	být	k5eAaImIp3nS	být
jakékoli	jakýkoli	k3yIgNnSc1	jakýkoli
přirozené	přirozený	k2eAgNnSc1d1	přirozené
vyústění	vyústění	k1gNnSc1	vyústění
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
jeho	jeho	k3xOp3gFnSc1	jeho
soukromá	soukromý	k2eAgFnSc1d1	soukromá
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nevybočuje	vybočovat	k5eNaImIp3nS	vybočovat
ze	z	k7c2	z
stylu	styl	k1gInSc2	styl
jeho	jeho	k3xOp3gNnSc2	jeho
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kafkovi	Kafka	k1gMnSc3	Kafka
nechyběl	chybět	k5eNaImAgMnS	chybět
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
vtip	vtip	k1gInSc4	vtip
a	a	k8xC	a
lehkou	lehký	k2eAgFnSc4d1	lehká
ironii	ironie	k1gFnSc4	ironie
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
dokument	dokument	k1gInSc1	dokument
Kafkova	Kafkův	k2eAgInSc2d1	Kafkův
života	život	k1gInSc2	život
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
představují	představovat	k5eAaImIp3nP	představovat
dopisy	dopis	k1gInPc4	dopis
(	(	kIx(	(
<g/>
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Mileně	mileně	k6eAd1	mileně
Jesenské	Jesenská	k1gFnSc2	Jesenská
<g/>
,	,	kIx,	,
snoubence	snoubenka	k1gFnSc3	snoubenka
Felici	Felice	k1gFnSc3	Felice
Bauerové	Bauerová	k1gFnSc3	Bauerová
<g/>
,	,	kIx,	,
sestře	sestra	k1gFnSc3	sestra
Ottle	Ottla	k1gFnSc3	Ottla
a	a	k8xC	a
rodině	rodina	k1gFnSc3	rodina
<g/>
)	)	kIx)	)
a	a	k8xC	a
deníky	deník	k1gInPc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
Mileně	mileně	k6eAd1	mileně
uspoř	uspořit	k5eAaPmRp2nS	uspořit
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
stať	stať	k1gFnSc1	stať
a	a	k8xC	a
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
naps	naps	k1gInSc1	naps
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kautman	Kautman	k1gMnSc1	Kautman
<g/>
;	;	kIx,	;
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Žantovská	Žantovský	k2eAgFnSc1d1	Žantovská
<g/>
;	;	kIx,	;
výňatky	výňatek	k1gInPc7	výňatek
z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
M.	M.	kA	M.
Jesenské	Jesenská	k1gFnSc2	Jesenská
M.	M.	kA	M.
Brodovi	Broda	k1gMnSc3	Broda
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Dopisy	dopis	k1gInPc1	dopis
Felici	Felic	k1gMnSc3	Felic
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Viola	Viola	k1gFnSc1	Viola
Fischerová	Fischerová	k1gFnSc1	Fischerová
<g/>
;	;	kIx,	;
doslovem	doslov	k1gInSc7	doslov
opatřil	opatřit	k5eAaPmAgMnS	opatřit
Erich	Erich	k1gMnSc1	Erich
Heller	Heller	k1gMnSc1	Heller
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
966	[number]	k4	966
stran	strana	k1gFnPc2	strana
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
též	též	k9	též
Kafkův	Kafkův	k2eAgInSc1d1	Kafkův
soukromý	soukromý	k2eAgInSc1d1	soukromý
deník	deník	k1gInSc1	deník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1902	[number]	k4	1902
až	až	k6eAd1	až
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
k	k	k7c3	k
precedentnímu	precedentní	k2eAgNnSc3d1	precedentní
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
Kafkovy	Kafkův	k2eAgFnPc4d1	Kafkova
pozůstalosti	pozůstalost	k1gFnPc4	pozůstalost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
izraelský	izraelský	k2eAgInSc1d1	izraelský
soud	soud	k1gInSc1	soud
totiž	totiž	k9	totiž
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
dědička	dědička	k1gFnSc1	dědička
Eva	Eva	k1gFnSc1	Eva
Hoffeová	Hoffeová	k1gFnSc1	Hoffeová
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
-	-	kIx~	-
sekretářka	sekretářka	k1gFnSc1	sekretářka
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
-	-	kIx~	-
odkázala	odkázat	k5eAaPmAgFnS	odkázat
kufr	kufr	k1gInSc4	kufr
Kafkových	Kafkových	k2eAgFnPc2d1	Kafkových
písemností	písemnost	k1gFnPc2	písemnost
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
Brod	Brod	k1gInSc1	Brod
odvezl	odvézt	k5eAaPmAgInS	odvézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
veškerý	veškerý	k3xTgInSc4	veškerý
materiál	materiál	k1gInSc1	materiál
odevzdat	odevzdat	k5eAaPmF	odevzdat
izraelské	izraelský	k2eAgFnSc3d1	izraelská
národní	národní	k2eAgFnSc3d1	národní
knihovně	knihovna	k1gFnSc3	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
Hoffeová	Hoffeová	k1gFnSc1	Hoffeová
Kafkovu	Kafkův	k2eAgFnSc4d1	Kafkova
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zdědila	zdědit	k5eAaPmAgFnS	zdědit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
-	-	kIx~	-
již	jenž	k3xRgFnSc4	jenž
zesnulou	zesnulá	k1gFnSc4	zesnulá
-	-	kIx~	-
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
ji	on	k3xPp3gFnSc4	on
poskytnout	poskytnout	k5eAaPmF	poskytnout
literárním	literární	k2eAgMnPc3d1	literární
vědcům	vědec	k1gMnPc3	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
zdroje	zdroj	k1gInSc2	zdroj
Esther	Esthra	k1gFnPc2	Esthra
Hoffeová	Hoffeový	k2eAgFnSc1d1	Hoffeový
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
za	za	k7c4	za
milión	milión	k4xCgInSc4	milión
liber	libra	k1gFnPc2	libra
prodala	prodat	k5eAaPmAgFnS	prodat
německé	německý	k2eAgFnSc6d1	německá
národní	národní	k2eAgFnSc6d1	národní
knihovně	knihovna	k1gFnSc6	knihovna
celý	celý	k2eAgInSc4d1	celý
původní	původní	k2eAgInSc4d1	původní
rukopis	rukopis	k1gInSc4	rukopis
románu	román	k1gInSc2	román
Proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Písemnosti	písemnost	k1gFnPc1	písemnost
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
v	v	k7c6	v
sejfech	sejf	k1gInPc6	sejf
jedné	jeden	k4xCgFnSc2	jeden
nejmenované	jmenovaný	k2eNgFnSc2d1	nejmenovaná
izraelské	izraelský	k2eAgFnSc2d1	izraelská
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
podle	podle	k7c2	podle
literárních	literární	k2eAgMnPc2d1	literární
vědců	vědec	k1gMnPc2	vědec
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
doposud	doposud	k6eAd1	doposud
nezveřejněné	zveřejněný	k2eNgInPc1d1	nezveřejněný
dopisy	dopis	k1gInPc1	dopis
či	či	k8xC	či
povídkové	povídkový	k2eAgInPc1d1	povídkový
náčrty	náčrt	k1gInPc1	náčrt
a	a	k8xC	a
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
i	i	k9	i
řadu	řada	k1gFnSc4	řada
Kafkových	Kafkových	k2eAgFnPc2d1	Kafkových
kreseb	kresba	k1gFnPc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Max	max	kA	max
Brod	Brod	k1gInSc1	Brod
totiž	totiž	k9	totiž
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
písemností	písemnost	k1gFnPc2	písemnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
pouze	pouze	k6eAd1	pouze
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
soudu	soud	k1gInSc2	soud
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
odvolání	odvolání	k1gNnSc4	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Franzi	Franze	k1gFnSc6	Franze
Kafkovi	Kafka	k1gMnSc3	Kafka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
několik	několik	k4yIc1	několik
vlaků	vlak	k1gInPc2	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
EuroCity	EuroCita	k1gFnSc2	EuroCita
dopravce	dopravce	k1gMnSc1	dopravce
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
jezdící	jezdící	k2eAgFnSc2d1	jezdící
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
-	-	kIx~	-
<g/>
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
-	-	kIx~	-
<g/>
Regensburg	Regensburg	k1gInSc1	Regensburg
<g/>
-	-	kIx~	-
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
smyslu	smysl	k1gInSc2	smysl
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
především	především	k9	především
existencialismus	existencialismus	k1gInSc4	existencialismus
přivedly	přivést	k5eAaPmAgFnP	přivést
na	na	k7c4	na
podobnou	podobný	k2eAgFnSc4d1	podobná
cestu	cesta	k1gFnSc4	cesta
i	i	k8xC	i
další	další	k2eAgMnPc4d1	další
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
české	český	k2eAgMnPc4d1	český
následovníky	následovník	k1gMnPc4	následovník
či	či	k8xC	či
pokračovatele	pokračovatel	k1gMnPc4	pokračovatel
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
etapách	etapa	k1gFnPc6	etapa
své	svůj	k3xOyFgFnSc2	svůj
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
činnosti	činnost	k1gFnSc2	činnost
řazeni	řazen	k2eAgMnPc1d1	řazen
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pecka	Pecka	k1gMnSc1	Pecka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kameníček	kameníček	k1gInSc1	kameníček
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
Moníková	Moníková	k1gFnSc1	Moníková
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
Kafkovy	Kafkův	k2eAgFnPc4d1	Kafkova
knihy	kniha	k1gFnPc4	kniha
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
