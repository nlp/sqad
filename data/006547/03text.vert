<s>
Guanin	guanin	k1gInSc1	guanin
je	být	k5eAaImIp3nS	být
heterocyklická	heterocyklický	k2eAgFnSc1d1	heterocyklická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
purinová	purinový	k2eAgFnSc1d1	purinová
dusíkatá	dusíkatý	k2eAgFnSc1d1	dusíkatá
báze	báze	k1gFnSc1	báze
<g/>
.	.	kIx.	.
</s>
<s>
Nukleotidy	nukleotid	k1gInPc4	nukleotid
obsahující	obsahující	k2eAgInPc4d1	obsahující
guanin	guanin	k1gInSc4	guanin
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
i	i	k8xC	i
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
tvoří	tvořit	k5eAaImIp3nS	tvořit
komplementární	komplementární	k2eAgInSc1d1	komplementární
pár	pár	k1gInSc1	pár
s	s	k7c7	s
cytosinem	cytosin	k1gInSc7	cytosin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
třemi	tři	k4xCgInPc7	tři
vodíkovými	vodíkový	k2eAgInPc7d1	vodíkový
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
guanin	guanin	k1gInSc1	guanin
příčinou	příčina	k1gFnSc7	příčina
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nukleotid	nukleotid	k1gInSc1	nukleotid
<g/>
,	,	kIx,	,
cyklický	cyklický	k2eAgInSc1d1	cyklický
guanosinmonofosfát	guanosinmonofosfát	k1gInSc1	guanosinmonofosfát
(	(	kIx(	(
<g/>
cGMP	cGMP	k?	cGMP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
signálních	signální	k2eAgFnPc2d1	signální
kaskád	kaskáda	k1gFnPc2	kaskáda
v	v	k7c6	v
mezibuněčné	mezibuněčný	k2eAgFnSc6d1	mezibuněčná
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
guanosintrifosfát	guanosintrifosfát	k1gInSc1	guanosintrifosfát
je	být	k5eAaImIp3nS	být
alternativou	alternativa	k1gFnSc7	alternativa
ATP	atp	kA	atp
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
biochemických	biochemický	k2eAgInPc6d1	biochemický
pochodech	pochod	k1gInPc6	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Guanin	guanin	k1gInSc4	guanin
lze	lze	k6eAd1	lze
izolovat	izolovat	k5eAaBmF	izolovat
ze	z	k7c2	z
šupin	šupina	k1gFnPc2	šupina
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
<g/>
;	;	kIx,	;
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
využíván	využívat	k5eAaImNgInS	využívat
např.	např.	kA	např.
v	v	k7c6	v
kosmetickém	kosmetický	k2eAgInSc6d1	kosmetický
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k8xS	jako
přídavná	přídavný	k2eAgFnSc1d1	přídavná
látka	látka	k1gFnSc1	látka
do	do	k7c2	do
šampónů	šampónů	k?	šampónů
<g/>
,	,	kIx,	,
laků	lak	k1gInPc2	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
očních	oční	k2eAgInPc2d1	oční
stínů	stín	k1gInPc2	stín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
výrobků	výrobek	k1gInPc2	výrobek
jimž	jenž	k3xRgMnPc3	jenž
dodává	dodávat	k5eAaImIp3nS	dodávat
barevně	barevně	k6eAd1	barevně
měňavý	měňavý	k2eAgInSc4d1	měňavý
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
