<s>
Guanin	guanin	k1gInSc4	guanin
lze	lze	k6eAd1	lze
izolovat	izolovat	k5eAaBmF	izolovat
ze	z	k7c2	z
šupin	šupina	k1gFnPc2	šupina
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
<g/>
;	;	kIx,	;
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
využíván	využívat	k5eAaPmNgInS	využívat
např.	např.	kA	např.
v	v	k7c6	v
kosmetickém	kosmetický	k2eAgInSc6d1	kosmetický
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k8xS	jako
přídavná	přídavný	k2eAgFnSc1d1	přídavná
látka	látka	k1gFnSc1	látka
do	do	k7c2	do
šampónů	šampónů	k?	šampónů
<g/>
,	,	kIx,	,
laků	lak	k1gInPc2	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
očních	oční	k2eAgInPc2d1	oční
stínů	stín	k1gInPc2	stín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
výrobků	výrobek	k1gInPc2	výrobek
jimž	jenž	k3xRgMnPc3	jenž
dodává	dodávat	k5eAaImIp3nS	dodávat
barevně	barevně	k6eAd1	barevně
měňavý	měňavý	k2eAgInSc4d1	měňavý
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
