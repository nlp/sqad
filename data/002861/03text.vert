<s>
Muammar	Muammar	k1gInSc1	Muammar
Kaddáfí	Kaddáfí	k1gFnSc2	Kaddáfí
či	či	k8xC	či
al-Kaddáfi	al-Kaddáf	k1gFnSc2	al-Kaddáf
/	/	kIx~	/
<g/>
arabsky	arabsky	k6eAd1	arabsky
audio	audio	k2eAgFnSc4d1	audio
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Syrta	Syrt	k1gInSc2	Syrt
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
Syrta	Syrto	k1gNnSc2	Syrto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
voják	voják	k1gMnSc1	voják
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Vůdce	vůdce	k1gMnSc1	vůdce
Velké	velký	k2eAgFnSc2d1	velká
revoluce	revoluce	k1gFnSc2	revoluce
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
arabské	arabský	k2eAgFnSc2d1	arabská
lidové	lidový	k2eAgFnSc2d1	lidová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
džamáhíríje	džamáhíríj	k1gFnSc2	džamáhíríj
<g/>
"	"	kIx"	"
vládcem	vládce	k1gMnSc7	vládce
Libye	Libye	k1gFnSc2	Libye
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
také	také	k9	také
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdéle	dlouho	k6eAd3	dlouho
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
vůdců	vůdce	k1gMnPc2	vůdce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
následné	následný	k2eAgFnSc3d1	následná
vojenské	vojenský	k2eAgFnSc3d1	vojenská
intervenci	intervence	k1gFnSc3	intervence
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
kontrolou	kontrola	k1gFnSc7	kontrola
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
začal	začít	k5eAaPmAgInS	začít
zpět	zpět	k6eAd1	zpět
ztracené	ztracený	k2eAgNnSc4d1	ztracené
území	území	k1gNnSc4	území
dobývat	dobývat	k5eAaImF	dobývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
opět	opět	k6eAd1	opět
obrátila	obrátit	k5eAaPmAgFnS	obrátit
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
povstalci	povstalec	k1gMnPc1	povstalec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
sídlo	sídlo	k1gNnSc1	sídlo
Báb	Báb	k1gMnSc1	Báb
al-Azízíju	al-Azízíju	k5eAaPmIp1nS	al-Azízíju
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
nalezli	naleznout	k5eAaPmAgMnP	naleznout
samotného	samotný	k2eAgMnSc4d1	samotný
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
stíhán	stíhat	k5eAaImNgInS	stíhat
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
trestním	trestní	k2eAgInSc7d1	trestní
soudem	soud	k1gInSc7	soud
kvůli	kvůli	k7c3	kvůli
obviněním	obvinění	k1gNnPc3	obvinění
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
skupinou	skupina	k1gFnSc7	skupina
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
lynčován	lynčován	k2eAgInSc4d1	lynčován
a	a	k8xC	a
poté	poté	k6eAd1	poté
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
kočovného	kočovný	k2eAgMnSc2d1	kočovný
beduínského	beduínský	k2eAgMnSc2d1	beduínský
zemědělce	zemědělec	k1gMnSc2	zemědělec
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
nedaleko	nedaleko	k7c2	nedaleko
přístavního	přístavní	k2eAgNnSc2d1	přístavní
města	město	k1gNnSc2	město
Syrta	Syrt	k1gInSc2	Syrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Misurátě	Misurát	k1gInSc6	Misurát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
studií	studio	k1gNnPc2	studio
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
ideologie	ideologie	k1gFnSc2	ideologie
egyptského	egyptský	k2eAgMnSc4d1	egyptský
státníka	státník	k1gMnSc4	státník
Gamála	Gamál	k1gMnSc2	Gamál
Abdula	Abdul	k1gMnSc2	Abdul
Násira	Násir	k1gMnSc2	Násir
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zapřisáhlým	zapřisáhlý	k2eAgMnSc7d1	zapřisáhlý
odpůrcem	odpůrce	k1gMnSc7	odpůrce
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
za	za	k7c4	za
nepovolené	povolený	k2eNgNnSc4d1	nepovolené
organizování	organizování	k1gNnSc4	organizování
studentů	student	k1gMnPc2	student
a	a	k8xC	a
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
demonstracích	demonstrace	k1gFnPc6	demonstrace
proti	proti	k7c3	proti
vystoupení	vystoupení	k1gNnSc3	vystoupení
Sýrie	Sýrie	k1gFnSc2	Sýrie
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
arabské	arabský	k2eAgFnSc2d1	arabská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zahájil	zahájit	k5eAaPmAgInS	zahájit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Libyjské	libyjský	k2eAgFnSc6d1	Libyjská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Benghází	Bengháze	k1gFnSc7	Bengháze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
Královské	královský	k2eAgFnSc2d1	královská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Sandhurstu	Sandhurst	k1gInSc6	Sandhurst
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
jako	jako	k8xS	jako
důstojník	důstojník	k1gMnSc1	důstojník
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
spojovacího	spojovací	k2eAgNnSc2d1	spojovací
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
začal	začít	k5eAaPmAgInS	začít
plánovat	plánovat	k5eAaImF	plánovat
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
důstojníků	důstojník	k1gMnPc2	důstojník
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc1	zrušení
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
nastolení	nastolení	k1gNnSc2	nastolení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
odložena	odložen	k2eAgFnSc1d1	odložena
<g/>
,	,	kIx,	,
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
převratu	převrat	k1gInSc2	převrat
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
sesazení	sesazení	k1gNnSc1	sesazení
libyjského	libyjský	k2eAgMnSc2d1	libyjský
krále	král	k1gMnSc2	král
Idrise	Idrise	k1gFnSc2	Idrise
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Nekrvavou	krvavý	k2eNgFnSc7d1	nekrvavá
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnPc4	vláda
nad	nad	k7c7	nad
Libyí	Libye	k1gFnSc7	Libye
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
se	se	k3xPyFc4	se
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
jmenovat	jmenovat	k5eAaBmF	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Džamáhíríje	Džamáhíríj	k1gFnSc2	Džamáhíríj
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
nastolil	nastolit	k5eAaPmAgInS	nastolit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ideologicky	ideologicky	k6eAd1	ideologicky
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
"	"	kIx"	"
<g/>
islámském	islámský	k2eAgInSc6d1	islámský
socialismu	socialismus	k1gInSc6	socialismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
směsi	směs	k1gFnPc1	směs
arabského	arabský	k2eAgInSc2d1	arabský
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
shrnul	shrnout	k5eAaPmAgMnS	shrnout
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
Zelené	Zelené	k2eAgFnSc6d1	Zelené
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
dílech	díl	k1gInPc6	díl
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1975	[number]	k4	1975
a	a	k8xC	a
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
k	k	k7c3	k
různým	různý	k2eAgNnPc3d1	různé
tématům	téma	k1gNnPc3	téma
od	od	k7c2	od
politiky	politika	k1gFnSc2	politika
přes	přes	k7c4	přes
společenskou	společenský	k2eAgFnSc4d1	společenská
morálku	morálka	k1gFnSc4	morálka
až	až	k9	až
po	po	k7c4	po
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
ideál	ideál	k1gInSc4	ideál
uspořádání	uspořádání	k1gNnSc2	uspořádání
státu	stát	k1gInSc2	stát
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
soukromou	soukromý	k2eAgFnSc4d1	soukromá
ochranku	ochranka	k1gFnSc4	ochranka
tvořily	tvořit	k5eAaImAgFnP	tvořit
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Amazonská	amazonský	k2eAgFnSc1d1	Amazonská
garda	garda	k1gFnSc1	garda
ho	on	k3xPp3gInSc2	on
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
důležitých	důležitý	k2eAgNnPc6d1	důležité
zahraničních	zahraniční	k2eAgNnPc6d1	zahraniční
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Al-Sádí	Al-Sádí	k1gNnSc2	Al-Sádí
je	být	k5eAaImIp3nS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
fotbalistou	fotbalista	k1gMnSc7	fotbalista
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
zahrál	zahrát	k5eAaPmAgMnS	zahrát
italskou	italský	k2eAgFnSc4d1	italská
Serii	serie	k1gFnSc4	serie
A	a	k9	a
za	za	k7c4	za
Sampdorii	Sampdorie	k1gFnSc4	Sampdorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
utužoval	utužovat	k5eAaImAgMnS	utužovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
arabskými	arabský	k2eAgFnPc7d1	arabská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
i	i	k8xC	i
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vynutit	vynutit	k5eAaPmF	vynutit
zrušení	zrušení	k1gNnSc4	zrušení
americké	americký	k2eAgFnSc2d1	americká
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Tripolisu	Tripolis	k1gInSc2	Tripolis
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
v	v	k7c6	v
Tobruku	Tobruk	k1gInSc6	Tobruk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyhnal	vyhnat	k5eAaPmAgInS	vyhnat
asi	asi	k9	asi
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
italské	italský	k2eAgFnSc2d1	italská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
politiky	politika	k1gFnSc2	politika
bylo	být	k5eAaImAgNnS	být
znárodnění	znárodnění	k1gNnSc1	znárodnění
ropných	ropný	k2eAgFnPc2d1	ropná
rafinérií	rafinérie	k1gFnPc2	rafinérie
<g/>
.	.	kIx.	.
</s>
<s>
Zakázal	zakázat	k5eAaPmAgMnS	zakázat
také	také	k9	také
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
hazardní	hazardní	k2eAgFnPc4d1	hazardní
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přebíral	přebírat	k5eAaImAgMnS	přebírat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
Libye	Libye	k1gFnSc1	Libye
dle	dle	k7c2	dle
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
k	k	k7c3	k
jedněm	jeden	k4xCgFnPc3	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
od	od	k7c2	od
kteréžto	kteréžto	k?	kteréžto
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
zemím	zem	k1gFnPc3	zem
světa	svět	k1gInSc2	svět
radikálně	radikálně	k6eAd1	radikálně
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
gramotnost	gramotnost	k1gFnSc1	gramotnost
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Libye	Libye	k1gFnSc2	Libye
z	z	k7c2	z
10	[number]	k4	10
na	na	k7c4	na
89	[number]	k4	89
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgInSc4d1	poslední
údaj	údaj	k1gInSc4	údaj
Libyi	Libye	k1gFnSc4	Libye
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
třetinu	třetina	k1gFnSc4	třetina
nejvzdělanějších	vzdělaný	k2eAgFnPc2d3	nejvzdělanější
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
ze	z	k7c2	z
44	[number]	k4	44
pozic	pozice	k1gFnPc2	pozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Gramotnost	gramotnost	k1gFnSc4	gramotnost
mládeže	mládež	k1gFnSc2	mládež
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
běžný	běžný	k2eAgInSc4d1	běžný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
negramotnější	gramotný	k2eNgFnPc1d2	gramotný
jsou	být	k5eAaImIp3nP	být
ženy	žena	k1gFnPc1	žena
-	-	kIx~	-
17	[number]	k4	17
%	%	kIx~	%
negramotných	gramotný	k2eNgFnPc2d1	negramotná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
negramotných	gramotný	k2eNgNnPc2d1	negramotné
jen	jen	k9	jen
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
světovým	světový	k2eAgInSc7d1	světový
průměrem	průměr	k1gInSc7	průměr
a	a	k8xC	a
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
kvality	kvalita	k1gFnSc2	kvalita
služeb	služba	k1gFnPc2	služba
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zeměmi	zem	k1gFnPc7	zem
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
87	[number]	k4	87
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc3	příčka
ze	z	k7c2	z
190	[number]	k4	190
zkoumaných	zkoumaný	k2eAgFnPc2d1	zkoumaná
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
za	za	k7c7	za
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
,	,	kIx,	,
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
<g/>
,	,	kIx,	,
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
teoreticky	teoreticky	k6eAd1	teoreticky
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
<g/>
,	,	kIx,	,
Libyjci	Libyjec	k1gMnPc1	Libyjec
využívali	využívat	k5eAaImAgMnP	využívat
soukromých	soukromý	k2eAgNnPc2d1	soukromé
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
či	či	k8xC	či
cestovali	cestovat	k5eAaImAgMnP	cestovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
adekvátní	adekvátní	k2eAgFnSc4d1	adekvátní
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vážných	vážný	k2eAgFnPc2d1	vážná
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
tato	tento	k3xDgFnSc1	tento
péče	péče	k1gFnSc1	péče
byla	být	k5eAaImAgFnS	být
hrazena	hradit	k5eAaImNgFnS	hradit
soukromě	soukromě	k6eAd1	soukromě
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
soukromé	soukromý	k2eAgInPc4d1	soukromý
výdaje	výdaj	k1gInPc4	výdaj
tvořily	tvořit	k5eAaImAgFnP	tvořit
23	[number]	k4	23
%	%	kIx~	%
celkových	celkový	k2eAgInPc2d1	celkový
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c6	na
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
dožití	dožití	k1gNnSc1	dožití
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
dnes	dnes	k6eAd1	dnes
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ženy	žena	k1gFnSc2	žena
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
byl	být	k5eAaImAgInS	být
veřejný	veřejný	k2eAgInSc1d1	veřejný
dluh	dluh	k1gInSc1	dluh
Libye	Libye	k1gFnSc2	Libye
nulový	nulový	k2eAgInSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přínos	přínos	k1gInSc1	přínos
svého	svůj	k3xOyFgNnSc2	svůj
ropného	ropný	k2eAgNnSc2d1	ropné
bohatství	bohatství	k1gNnSc2	bohatství
si	se	k3xPyFc3	se
Libye	Libye	k1gFnSc1	Libye
uchovala	uchovat	k5eAaPmAgFnS	uchovat
i	i	k9	i
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
vlády	vláda	k1gFnSc2	vláda
Libye	Libye	k1gFnSc1	Libye
profitovala	profitovat	k5eAaBmAgFnS	profitovat
z	z	k7c2	z
narůstajících	narůstající	k2eAgFnPc2d1	narůstající
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
po	po	k7c6	po
ropné	ropný	k2eAgFnSc6d1	ropná
krizi	krize	k1gFnSc6	krize
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
nijak	nijak	k6eAd1	nijak
nediverzifikovala	diverzifikovat	k5eNaImAgFnS	diverzifikovat
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
konce	konec	k1gInSc2	konec
tvořil	tvořit	k5eAaImAgInS	tvořit
vývoz	vývoz	k1gInSc1	vývoz
ropy	ropa	k1gFnSc2	ropa
70	[number]	k4	70
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
indexu	index	k1gInSc6	index
vnímání	vnímání	k1gNnSc2	vnímání
korupce	korupce	k1gFnSc2	korupce
je	být	k5eAaImIp3nS	být
Libye	Libye	k1gFnSc1	Libye
na	na	k7c4	na
146	[number]	k4	146
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
178	[number]	k4	178
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
soukromým	soukromý	k2eAgInSc7d1	soukromý
majetkem	majetek	k1gInSc7	majetek
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
či	či	k8xC	či
jeho	jeho	k3xOp3gMnPc2	jeho
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
a	a	k8xC	a
majetkem	majetek	k1gInSc7	majetek
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
těžké	těžký	k2eAgNnSc1d1	těžké
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
stavba	stavba	k1gFnSc1	stavba
Velké	velký	k2eAgFnSc2d1	velká
umělé	umělý	k2eAgFnSc2d1	umělá
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
zavlažovacího	zavlažovací	k2eAgInSc2d1	zavlažovací
systému	systém	k1gInSc2	systém
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
udržoval	udržovat	k5eAaImAgInS	udržovat
poměrně	poměrně	k6eAd1	poměrně
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
socialistického	socialistický	k2eAgInSc2d1	socialistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
výraznou	výrazný	k2eAgFnSc7d1	výrazná
měrou	míra	k1gFnSc7wR	míra
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
libyjského	libyjský	k2eAgInSc2d1	libyjský
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
také	také	k9	také
hlavními	hlavní	k2eAgMnPc7d1	hlavní
dodavateli	dodavatel	k1gMnPc7	dodavatel
zbraní	zbraň	k1gFnPc2	zbraň
pro	pro	k7c4	pro
libyjskou	libyjský	k2eAgFnSc4d1	Libyjská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
letectvo	letectvo	k1gNnSc4	letectvo
během	během	k7c2	během
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgMnPc4	tento
dodavatele	dodavatel	k1gMnPc4	dodavatel
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodala	dodat	k5eAaPmAgFnS	dodat
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
těžkou	těžký	k2eAgFnSc4d1	těžká
výzbroj	výzbroj	k1gFnSc4	výzbroj
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
výbušnina	výbušnina	k1gFnSc1	výbušnina
semtex	semtex	k1gInSc1	semtex
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nakoupená	nakoupený	k2eAgFnSc1d1	nakoupená
pro	pro	k7c4	pro
civilní	civilní	k2eAgInPc4d1	civilní
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgNnP	být
využívána	využívat	k5eAaPmNgNnP	využívat
při	při	k7c6	při
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
převzal	převzít	k5eAaPmAgMnS	převzít
Kaddáfí	Kaddáfí	k1gFnSc4	Kaddáfí
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ČSSR	ČSSR	kA	ČSSR
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
I.	I.	kA	I.
třídy	třída	k1gFnPc4	třída
s	s	k7c7	s
řetězem	řetěz	k1gInSc7	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
postkomunistických	postkomunistický	k2eAgFnPc2d1	postkomunistická
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
největším	veliký	k2eAgMnSc7d3	veliký
příjemcem	příjemce	k1gMnSc7	příjemce
libyjských	libyjský	k2eAgInPc2d1	libyjský
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c6	na
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Zemím	zem	k1gFnPc3	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
se	se	k3xPyFc4	se
však	však	k9	však
nevyhnuly	vyhnout	k5eNaPmAgInP	vyhnout
projevy	projev	k1gInPc1	projev
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
nevypočitatelnosti	nevypočitatelnost	k1gFnSc2	nevypočitatelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
Bengházské	Bengházský	k2eAgFnSc6d1	Bengházský
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tristních	tristní	k2eAgFnPc2d1	tristní
hygienických	hygienický	k2eAgFnPc2d1	hygienická
podmínek	podmínka	k1gFnPc2	podmínka
virus	virus	k1gInSc4	virus
HIV	HIV	kA	HIV
mezi	mezi	k7c7	mezi
426	[number]	k4	426
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
režim	režim	k1gInSc1	režim
obratem	obrat	k1gInSc7	obrat
obvinil	obvinit	k5eAaPmAgInS	obvinit
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
personál	personál	k1gInSc1	personál
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
bulharských	bulharský	k2eAgFnPc2d1	bulharská
sestřiček	sestřička	k1gFnPc2	sestřička
a	a	k8xC	a
palestinský	palestinský	k2eAgMnSc1d1	palestinský
lékař	lékař	k1gMnSc1	lékař
byli	být	k5eAaImAgMnP	být
drženi	držet	k5eAaImNgMnP	držet
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
expertním	expertní	k2eAgInPc3d1	expertní
posudkům	posudek	k1gInPc3	posudek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dokazovaly	dokazovat	k5eAaImAgInP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
nakazily	nakazit	k5eAaPmAgFnP	nakazit
virem	vir	k1gInSc7	vir
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
působením	působení	k1gNnSc7	působení
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
následně	následně	k6eAd1	následně
požadoval	požadovat	k5eAaImAgMnS	požadovat
jako	jako	k8xS	jako
výkupné	výkupné	k1gNnSc4	výkupné
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
svobodu	svoboda	k1gFnSc4	svoboda
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
pro	pro	k7c4	pro
odškodnění	odškodnění	k1gNnSc4	odškodnění
rodin	rodina	k1gFnPc2	rodina
nemocných	nemocný	k2eAgFnPc2d1	nemocná
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
vystaveni	vystaven	k2eAgMnPc1d1	vystaven
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
nelidskému	lidský	k2eNgNnSc3d1	nelidské
zacházení	zacházení	k1gNnSc3	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnický	zdravotnický	k2eAgInSc1d1	zdravotnický
personál	personál	k1gInSc1	personál
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
propuštěn	propustit	k5eAaPmNgInS	propustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
finanční	finanční	k2eAgInPc4d1	finanční
ústupky	ústupek	k1gInPc4	ústupek
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
Libye	Libye	k1gFnSc2	Libye
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
arabskými	arabský	k2eAgFnPc7d1	arabská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
podpořit	podpořit	k5eAaPmF	podpořit
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podpora	podpora	k1gFnSc1	podpora
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
až	až	k6eAd1	až
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
čtyřdenní	čtyřdenní	k2eAgFnSc2d1	čtyřdenní
Egyptsko-libyjské	egyptskoibyjský	k2eAgFnSc2d1	egyptsko-libyjský
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tzv.	tzv.	kA	tzv.
Islámskou	islámský	k2eAgFnSc4d1	islámská
legii	legie	k1gFnSc4	legie
sestavenou	sestavený	k2eAgFnSc4d1	sestavená
především	především	k9	především
z	z	k7c2	z
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ze	z	k7c2	z
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
několika	několik	k4yIc2	několik
konfliktů	konflikt	k1gInPc2	konflikt
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vojensky	vojensky	k6eAd1	vojensky
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Čadu	Čad	k1gInSc6	Čad
a	a	k8xC	a
vznesl	vznést	k5eAaPmAgMnS	vznést
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
Aouzou	Aouza	k1gFnSc7	Aouza
<g/>
,	,	kIx,	,
území	území	k1gNnSc4	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
km2	km2	k4	km2
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
bohaté	bohatý	k2eAgFnSc2d1	bohatá
zejména	zejména	k9	zejména
na	na	k7c4	na
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
libyjských	libyjský	k2eAgNnPc2d1	Libyjské
vojsk	vojsko	k1gNnPc2	vojsko
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vlády	vláda	k1gFnSc2	vláda
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
neústupná	ústupný	k2eNgNnPc1d1	neústupné
jednání	jednání	k1gNnPc1	jednání
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nevraživost	nevraživost	k1gFnSc1	nevraživost
vůči	vůči	k7c3	vůči
Izraeli	Izrael	k1gMnSc3	Izrael
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c4	v
pohrůžky	pohrůžka	k1gFnPc4	pohrůžka
vyhnání	vyhnání	k1gNnSc1	vyhnání
30	[number]	k4	30
000	[number]	k4	000
Palestinců	Palestinec	k1gMnPc2	Palestinec
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
OOP	OOP	kA	OOP
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
o	o	k7c6	o
mírovém	mírový	k2eAgInSc6d1	mírový
řešení	řešení	k1gNnSc2	řešení
jejich	jejich	k3xOp3gInSc2	jejich
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
výhrůžky	výhrůžka	k1gFnPc4	výhrůžka
začal	začít	k5eAaPmAgMnS	začít
záhy	záhy	k6eAd1	záhy
naplňovat	naplňovat	k5eAaImF	naplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
tento	tento	k3xDgInSc4	tento
akt	akt	k1gInSc4	akt
za	za	k7c4	za
službu	služba	k1gFnSc4	služba
palestinskému	palestinský	k2eAgInSc3d1	palestinský
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
sionisté	sionista	k1gMnPc1	sionista
plánovali	plánovat	k5eAaImAgMnP	plánovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
Palestinu	Palestina	k1gFnSc4	Palestina
bez	bez	k7c2	bez
Palestinců	Palestinec	k1gMnPc2	Palestinec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ostatní	ostatní	k2eAgInPc4d1	ostatní
arabské	arabský	k2eAgInPc4d1	arabský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednaly	jednat	k5eAaImAgFnP	jednat
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Vyhánění	vyhánění	k1gNnSc1	vyhánění
Palestinců	Palestinec	k1gMnPc2	Palestinec
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
Palestinců	Palestinec	k1gMnPc2	Palestinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
donuceni	donucen	k2eAgMnPc1d1	donucen
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
nevypočitatelné	vypočitatelný	k2eNgInPc4d1	nevypočitatelný
a	a	k8xC	a
nepředvídatelné	předvídatelný	k2eNgNnSc4d1	nepředvídatelné
chování	chování	k1gNnSc4	chování
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
financovala	financovat	k5eAaBmAgFnS	financovat
mnoho	mnoho	k4c4	mnoho
revolučních	revoluční	k2eAgFnPc2d1	revoluční
či	či	k8xC	či
teroristických	teroristický	k2eAgFnPc2d1	teroristická
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Black	Blacka	k1gFnPc2	Blacka
Panthers	Panthers	k1gInSc1	Panthers
a	a	k8xC	a
Nation	Nation	k1gInSc1	Nation
of	of	k?	of
Islam	Islam	k1gInSc1	Islam
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Irské	irský	k2eAgFnSc2d1	irská
republikánské	republikánský	k2eAgFnSc2d1	republikánská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
států	stát	k1gInPc2	stát
podporujících	podporující	k2eAgInPc2d1	podporující
terorismus	terorismus	k1gInSc4	terorismus
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
americkým	americký	k2eAgNnSc7d1	americké
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
jeho	jeho	k3xOp3gFnSc4	jeho
vláda	vláda	k1gFnSc1	vláda
dle	dle	k7c2	dle
dokumentů	dokument	k1gInPc2	dokument
nalezených	nalezený	k2eAgInPc2d1	nalezený
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Stasi	stase	k1gFnSc4	stase
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
bombový	bombový	k2eAgInSc4d1	bombový
útok	útok	k1gInSc4	útok
diskotéku	diskotéka	k1gFnSc4	diskotéka
La	la	k0	la
Belle	bell	k1gInSc5	bell
v	v	k7c6	v
Západním	západní	k2eAgInSc6d1	západní
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
včetně	včetně	k7c2	včetně
amerických	americký	k2eAgMnPc2d1	americký
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
teroristickém	teroristický	k2eAgInSc6d1	teroristický
útoku	útok	k1gInSc6	útok
došla	dojít	k5eAaPmAgFnS	dojít
USA	USA	kA	USA
trpělivost	trpělivost	k1gFnSc1	trpělivost
s	s	k7c7	s
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
podporou	podpora	k1gFnSc7	podpora
terorismu	terorismus	k1gInSc2	terorismus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
odvetu	odvet	k1gInSc6	odvet
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bombardování	bombardování	k1gNnSc2	bombardování
Tripolisu	Tripolis	k1gInSc2	Tripolis
a	a	k8xC	a
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
dle	dle	k7c2	dle
Kaddáfího	Kaddáfí	k2eAgNnSc2d1	Kaddáfí
tvrzení	tvrzení	k1gNnSc2	tvrzení
zemřít	zemřít	k5eAaPmF	zemřít
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
adoptovaná	adoptovaný	k2eAgFnSc1d1	adoptovaná
dcera	dcera	k1gFnSc1	dcera
Hanna	Hanna	k1gFnSc1	Hanna
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
úmrtí	úmrtí	k1gNnSc2	úmrtí
Hanny	Hanna	k1gFnSc2	Hanna
však	však	k9	však
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
existovaly	existovat	k5eAaImAgInP	existovat
rozpory	rozpor	k1gInPc1	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
vládní	vládní	k2eAgMnPc4d1	vládní
úředníky	úředník	k1gMnPc4	úředník
udávaly	udávat	k5eAaImAgFnP	udávat
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
věk	věk	k1gInSc4	věk
v	v	k7c6	v
době	doba	k1gFnSc6	doba
úmrtí	úmrtí	k1gNnPc2	úmrtí
(	(	kIx(	(
<g/>
12	[number]	k4	12
až	až	k9	až
15	[number]	k4	15
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
dokumentace	dokumentace	k1gFnSc1	dokumentace
jejího	její	k3xOp3gInSc2	její
dalšího	další	k2eAgInSc2d1	další
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Hanna	Hanna	k1gFnSc1	Hanna
byla	být	k5eAaImAgFnS	být
nejen	nejen	k6eAd1	nejen
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c6	na
rodinném	rodinný	k2eAgNnSc6d1	rodinné
videu	video	k1gNnSc6	video
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
bombardování	bombardování	k1gNnSc6	bombardování
Tripolisu	Tripolis	k1gInSc2	Tripolis
či	či	k8xC	či
při	při	k7c6	při
snídani	snídaně	k1gFnSc6	snídaně
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
rezidenci	rezidence	k1gFnSc6	rezidence
objeveny	objevit	k5eAaPmNgInP	objevit
její	její	k3xOp3gInPc1	její
studijní	studijní	k2eAgInPc1d1	studijní
výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
či	či	k8xC	či
certifikát	certifikát	k1gInSc4	certifikát
studia	studio	k1gNnSc2	studio
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Německým	německý	k2eAgFnPc3d1	německá
novinám	novina	k1gFnPc3	novina
Die	Die	k1gFnPc2	Die
Welt	Welta	k1gFnPc2	Welta
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
i	i	k8xC	i
svědectví	svědectví	k1gNnSc4	svědectví
doktora	doktor	k1gMnSc2	doktor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgInPc1d1	britský
úřady	úřad	k1gInPc1	úřad
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyměnily	vyměnit	k5eAaPmAgInP	vyměnit
e-maily	eail	k1gInPc1	e-mail
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgInP	zabývat
vydáním	vydání	k1gNnSc7	vydání
dvouletého	dvouletý	k2eAgNnSc2d1	dvouleté
víza	vízo	k1gNnSc2	vízo
pro	pro	k7c4	pro
Hannu	Hanna	k1gFnSc4	Hanna
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
bombardování	bombardování	k1gNnSc6	bombardování
se	se	k3xPyFc4	se
však	však	k9	však
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vyhnout	vyhnout	k5eAaPmF	vyhnout
dalším	další	k2eAgFnPc3d1	další
aférám	aféra	k1gFnPc3	aféra
spojeným	spojený	k2eAgFnPc3d1	spojená
s	s	k7c7	s
terorismem	terorismus	k1gInSc7	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1988	[number]	k4	1988
se	se	k3xPyFc4	se
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombového	bombový	k2eAgInSc2d1	bombový
útoku	útok	k1gInSc2	útok
nad	nad	k7c7	nad
skotským	skotský	k2eAgNnSc7d1	skotské
městečkem	městečko	k1gNnSc7	městečko
Lockerbie	Lockerbie	k1gFnSc2	Lockerbie
letadlo	letadlo	k1gNnSc4	letadlo
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gMnSc1	Am
103	[number]	k4	103
s	s	k7c7	s
259	[number]	k4	259
cestujícími	cestující	k1gMnPc7	cestující
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
11	[number]	k4	11
lidí	člověk	k1gMnPc2	člověk
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
pod	pod	k7c7	pod
troskami	troska	k1gFnPc7	troska
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
události	událost	k1gFnSc6	událost
odmítala	odmítat	k5eAaImAgFnS	odmítat
<g/>
,	,	kIx,	,
vinu	vinout	k5eAaImIp1nS	vinout
přiznala	přiznat	k5eAaPmAgFnS	přiznat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
sblížení	sblížení	k1gNnSc6	sblížení
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
zavázala	zavázat	k5eAaPmAgFnS	zavázat
vyplatit	vyplatit	k5eAaPmF	vyplatit
pozůstalým	pozůstalý	k1gMnPc3	pozůstalý
celkem	celkem	k6eAd1	celkem
2,7	[number]	k4	2,7
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Libye	Libye	k1gFnSc1	Libye
pod	pod	k7c4	pod
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
vládou	vláda	k1gFnSc7	vláda
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
proti	proti	k7c3	proti
francouzskému	francouzský	k2eAgInSc3d1	francouzský
letu	let	k1gInSc3	let
UTA	UTA	kA	UTA
772	[number]	k4	772
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
170	[number]	k4	170
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Vinu	vina	k1gFnSc4	vina
přiznala	přiznat	k5eAaPmAgFnS	přiznat
Libye	Libye	k1gFnSc1	Libye
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
společně	společně	k6eAd1	společně
s	s	k7c7	s
vinou	vina	k1gFnSc7	vina
na	na	k7c6	na
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
Lockerbie	Lockerbie	k1gFnSc2	Lockerbie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Reagana	Reagan	k1gMnSc2	Reagan
a	a	k8xC	a
Bushe	Bush	k1gMnSc2	Bush
st.	st.	kA	st.
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
libyjsko-americké	libyjskomerický	k2eAgInPc1d1	libyjsko-americký
vztahy	vztah	k1gInPc1	vztah
spíše	spíše	k9	spíše
horšily	horšit	k5eAaImAgInP	horšit
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
věci	věc	k1gFnPc1	věc
hnuly	hnout	k5eAaPmAgFnP	hnout
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
se	se	k3xPyFc4	se
do	do	k7c2	do
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
po	po	k7c6	po
bombardování	bombardování	k1gNnSc6	bombardování
Tripolisu	Tripolis	k1gInSc2	Tripolis
a	a	k8xC	a
Benghází	Bengháze	k1gFnPc2	Bengháze
tlačily	tlačit	k5eAaImAgFnP	tlačit
na	na	k7c4	na
Libyi	Libye	k1gFnSc4	Libye
a	a	k8xC	a
chtěly	chtít	k5eAaImAgInP	chtít
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
kategoricky	kategoricky	k6eAd1	kategoricky
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
do	do	k7c2	do
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
USA	USA	kA	USA
si	se	k3xPyFc3	se
Libyjci	Libyjec	k1gMnPc7	Libyjec
hodně	hodně	k6eAd1	hodně
slibovali	slibovat	k5eAaImAgMnP	slibovat
od	od	k7c2	od
vylepšení	vylepšení	k1gNnPc2	vylepšení
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
USA	USA	kA	USA
a	a	k8xC	a
Libye	Libye	k1gFnSc1	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
spletli	splést	k5eAaPmAgMnP	splést
a	a	k8xC	a
Clinton	Clinton	k1gMnSc1	Clinton
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
proti	proti	k7c3	proti
Libyi	Libye	k1gFnSc3	Libye
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Sblížení	sblížení	k1gNnSc1	sblížení
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
ukázala	ukázat	k5eAaPmAgFnS	ukázat
až	až	k9	až
jeho	jeho	k3xOp3gFnSc1	jeho
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
zářijové	zářijový	k2eAgInPc4d1	zářijový
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
je	být	k5eAaImIp3nS	být
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
a	a	k8xC	a
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Al-Kajdě	Al-Kajda	k1gFnSc3	Al-Kajda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
zadržena	zadržet	k5eAaPmNgFnS	zadržet
námořní	námořní	k2eAgFnSc1d1	námořní
dodávka	dodávka	k1gFnSc1	dodávka
součástek	součástka	k1gFnPc2	součástka
centrifug	centrifuga	k1gFnPc2	centrifuga
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
při	při	k7c6	při
obohacování	obohacování	k1gNnSc6	obohacování
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
utajována	utajován	k2eAgFnSc1d1	utajována
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
probíhat	probíhat	k5eAaImF	probíhat
neveřejná	veřejný	k2eNgNnPc4d1	neveřejné
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
jednání	jednání	k1gNnPc4	jednání
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
libyjského	libyjský	k2eAgInSc2d1	libyjský
programu	program	k1gInSc2	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
zadržení	zadržení	k1gNnSc6	zadržení
jaderného	jaderný	k2eAgInSc2d1	jaderný
materiálu	materiál	k1gInSc2	materiál
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Libye	Libye	k1gFnSc2	Libye
rovněž	rovněž	k9	rovněž
odškodnila	odškodnit	k5eAaPmAgFnS	odškodnit
oběti	oběť	k1gFnPc4	oběť
různých	různý	k2eAgInPc2d1	různý
teroristických	teroristický	k2eAgInPc2d1	teroristický
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pozůstalých	pozůstalý	k2eAgFnPc2d1	pozůstalá
obětí	oběť	k1gFnPc2	oběť
z	z	k7c2	z
Lockerbie	Lockerbie	k1gFnSc2	Lockerbie
či	či	k8xC	či
letu	let	k1gInSc2	let
UTA	UTA	kA	UTA
772	[number]	k4	772
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
pak	pak	k6eAd1	pak
Libye	Libye	k1gFnSc1	Libye
začala	začít	k5eAaPmAgFnS	začít
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
ZHN	ZHN	kA	ZHN
likvidovat	likvidovat	k5eAaBmF	likvidovat
a	a	k8xC	a
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
kompletně	kompletně	k6eAd1	kompletně
zbavila	zbavit	k5eAaPmAgFnS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
však	však	k9	však
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
přesto	přesto	k6eAd1	přesto
zůstala	zůstat	k5eAaPmAgFnS	zůstat
část	část	k1gFnSc1	část
obohaceného	obohacený	k2eAgInSc2d1	obohacený
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
USA	USA	kA	USA
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
odkoupit	odkoupit	k5eAaPmF	odkoupit
<g/>
.	.	kIx.	.
</s>
<s>
Rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
se	se	k3xPyFc4	se
tak	tak	k9	tak
opětovná	opětovný	k2eAgNnPc1d1	opětovné
vyjednávání	vyjednávání	k1gNnPc1	vyjednávání
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
uran	uran	k1gInSc1	uran
uskladněn	uskladněn	k2eAgInSc1d1	uskladněn
za	za	k7c2	za
špatných	špatný	k2eAgFnPc2d1	špatná
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c6	na
tripolském	tripolský	k2eAgNnSc6d1	Tripolské
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
Libye	Libye	k1gFnSc2	Libye
vyvezla	vyvézt	k5eAaPmAgFnS	vyvézt
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
800	[number]	k4	800
000	[number]	k4	000
USD	USD	kA	USD
<g/>
,	,	kIx,	,
vyplacených	vyplacený	k2eAgFnPc2d1	vyplacená
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
najímal	najímat	k5eAaImAgMnS	najímat
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
západní	západní	k2eAgFnSc2d1	západní
konzultační	konzultační	k2eAgFnSc2d1	konzultační
firmy	firma	k1gFnSc2	firma
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
vylepšit	vylepšit	k5eAaPmF	vylepšit
pověst	pověst	k1gFnSc4	pověst
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
za	za	k7c4	za
vylepšení	vylepšení	k1gNnSc4	vylepšení
image	image	k1gFnSc2	image
platila	platit	k5eAaImAgFnS	platit
Libye	Libye	k1gFnSc1	Libye
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
za	za	k7c4	za
konzultace	konzultace	k1gFnPc4	konzultace
<g/>
,	,	kIx,	,
brífinky	brífink	k1gInPc4	brífink
<g/>
,	,	kIx,	,
analýzy	analýza	k1gFnPc4	analýza
a	a	k8xC	a
také	také	k6eAd1	také
cesty	cesta	k1gFnPc4	cesta
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
článků	článek	k1gInPc2	článek
a	a	k8xC	a
statí	stať	k1gFnPc2	stať
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
napsali	napsat	k5eAaBmAgMnP	napsat
hosté	host	k1gMnPc1	host
<g/>
,	,	kIx,	,
vyzněla	vyznít	k5eAaPmAgFnS	vyznít
pro	pro	k7c4	pro
Libyi	Libye	k1gFnSc4	Libye
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgFnPc2	tento
snah	snaha	k1gFnPc2	snaha
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
lobbování	lobbování	k1gNnSc1	lobbování
představitelů	představitel	k1gMnPc2	představitel
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
pracovníků	pracovník	k1gMnPc2	pracovník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
oteplení	oteplení	k1gNnSc2	oteplení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Libyí	Libye	k1gFnSc7	Libye
a	a	k8xC	a
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
byl	být	k5eAaImAgInS	být
obnovený	obnovený	k2eAgInSc1d1	obnovený
export	export	k1gInSc1	export
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
Libye	Libye	k1gFnSc1	Libye
vyškrtnuta	vyškrtnut	k2eAgFnSc1d1	vyškrtnuta
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
států	stát	k1gInPc2	stát
podporujících	podporující	k2eAgInPc2d1	podporující
terorismus	terorismus	k1gInSc4	terorismus
<g/>
,	,	kIx,	,
vedeného	vedený	k2eAgNnSc2d1	vedené
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamenalo	znamenat	k5eAaImAgNnS	znamenat
uvolnění	uvolnění	k1gNnSc4	uvolnění
různých	různý	k2eAgFnPc2d1	různá
sankcí	sankce	k1gFnPc2	sankce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
státy	stát	k1gInPc4	stát
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
snahou	snaha	k1gFnSc7	snaha
je	být	k5eAaImIp3nS	být
postupná	postupný	k2eAgFnSc1d1	postupná
integrace	integrace	k1gFnSc1	integrace
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
afrických	africký	k2eAgInPc2d1	africký
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
po	po	k7c6	po
masakrech	masakr	k1gInPc6	masakr
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
č.	č.	k?	č.
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
požadující	požadující	k2eAgNnSc4d1	požadující
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
opravňující	opravňující	k2eAgInPc1d1	opravňující
státy	stát	k1gInPc1	stát
OSN	OSN	kA	OSN
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
všech	všecek	k3xTgNnPc2	všecek
nutných	nutný	k2eAgNnPc2d1	nutné
opatření	opatření	k1gNnPc2	opatření
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
povstalecké	povstalecký	k2eAgFnPc4d1	povstalecká
pozice	pozice	k1gFnPc4	pozice
nadále	nadále	k6eAd1	nadále
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
započala	započnout	k5eAaPmAgFnS	započnout
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
NATO	nato	k6eAd1	nato
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
ukořistěných	ukořistěný	k2eAgInPc6d1	ukořistěný
vojenských	vojenský	k2eAgInPc6d1	vojenský
rozkazech	rozkaz	k1gInPc6	rozkaz
vydaných	vydaný	k2eAgInPc6d1	vydaný
vysoce	vysoce	k6eAd1	vysoce
postavenými	postavený	k2eAgMnPc7d1	postavený
generály	generál	k1gMnPc7	generál
Kaddáfího	Kaddáfí	k2eAgInSc2d1	Kaddáfí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
údajně	údajně	k6eAd1	údajně
svědčit	svědčit	k5eAaImF	svědčit
o	o	k7c6	o
úmyslném	úmyslný	k2eAgNnSc6d1	úmyslné
páchání	páchání	k1gNnSc6	páchání
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
trestního	trestní	k2eAgInSc2d1	trestní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c4	na
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
Sajfa	Sajf	k1gMnSc4	Sajf
Isláma	Islám	k1gMnSc4	Islám
a	a	k8xC	a
na	na	k7c4	na
šéfa	šéf	k1gMnSc4	šéf
libyjské	libyjský	k2eAgFnSc2d1	Libyjská
rozvědky	rozvědka	k1gFnSc2	rozvědka
Abdalláha	Abdalláh	k1gMnSc2	Abdalláh
Sanúsího	Sanúsí	k1gMnSc2	Sanúsí
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
byl	být	k5eAaImAgMnS	být
stíhán	stíhat	k5eAaImNgMnS	stíhat
za	za	k7c2	za
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
zatýkání	zatýkání	k1gNnSc2	zatýkání
a	a	k8xC	a
věznění	věznění	k1gNnSc2	věznění
stovek	stovka	k1gFnPc2	stovka
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
jeho	jeho	k3xOp3gFnSc1	jeho
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k6eAd1	Kaddáfí
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
po	po	k7c6	po
súdánském	súdánský	k2eAgMnSc6d1	súdánský
prezidentu	prezident	k1gMnSc6	prezident
Bašírovi	Bašír	k1gMnSc6	Bašír
druhou	druhý	k4xOgFnSc7	druhý
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
MTS	MTS	kA	MTS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
stíhána	stíhat	k5eAaImNgFnS	stíhat
za	za	k7c4	za
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
probíhaly	probíhat	k5eAaImAgFnP	probíhat
i	i	k9	i
vládou	vláda	k1gFnSc7	vláda
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
směřovaly	směřovat	k5eAaImAgFnP	směřovat
proti	proti	k7c3	proti
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
několik	několik	k4yIc1	několik
demonstrací	demonstrace	k1gFnPc2	demonstrace
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
Zlitán	Zlitán	k1gMnSc1	Zlitán
<g/>
,	,	kIx,	,
Tarhúna	Tarhúna	k1gFnSc1	Tarhúna
<g/>
,	,	kIx,	,
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
<g/>
,	,	kIx,	,
Al-Khoms	Al-Khoms	k1gInSc1	Al-Khoms
a	a	k8xC	a
v	v	k7c6	v
Syrtě	Syrta	k1gFnSc6	Syrta
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
zdrojů	zdroj	k1gInPc2	zdroj
nakloněných	nakloněný	k2eAgInPc2d1	nakloněný
režimu	režim	k1gInSc3	režim
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
až	až	k9	až
1,7	[number]	k4	1,7
milionů	milion	k4xCgInPc2	milion
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
údajně	údajně	k6eAd1	údajně
95	[number]	k4	95
%	%	kIx~	%
populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
díky	díky	k7c3	díky
letecké	letecký	k2eAgFnSc3d1	letecká
podpoře	podpora	k1gFnSc3	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
postupně	postupně	k6eAd1	postupně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
všechna	všechen	k3xTgNnPc4	všechen
města	město	k1gNnPc4	město
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
padlo	padnout	k5eAaImAgNnS	padnout
Tripolis	Tripolis	k1gInSc4	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
povstalci	povstalec	k1gMnPc7	povstalec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
sídlo	sídlo	k1gNnSc1	sídlo
Báb	Báb	k1gMnSc1	Báb
al-Azízíju	al-Azízíju	k5eAaPmIp1nS	al-Azízíju
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nalezli	nalézt	k5eAaBmAgMnP	nalézt
samotného	samotný	k2eAgMnSc4d1	samotný
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nadále	nadále	k6eAd1	nadále
unikal	unikat	k5eAaImAgInS	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Alžírské	alžírský	k2eAgNnSc1d1	alžírské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
příbuzní	příbuzný	k1gMnPc1	příbuzný
překročili	překročit	k5eAaPmAgMnP	překročit
v	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
SELČ	SELČ	kA	SELČ
<g/>
)	)	kIx)	)
hranici	hranice	k1gFnSc4	hranice
do	do	k7c2	do
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
prý	prý	k9	prý
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
jeho	jeho	k3xOp3gFnSc4	jeho
manželka	manželka	k1gFnSc1	manželka
Safíja	Safíja	k1gFnSc1	Safíja
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Ajša	Ajša	k1gFnSc1	Ajša
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
Hannibal	Hannibal	k1gInSc4	Hannibal
i	i	k8xC	i
Muhammad	Muhammad	k1gInSc4	Muhammad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
tak	tak	k9	tak
nadále	nadále	k6eAd1	nadále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
Kaddáfí	Kaddáfí	k2eAgInSc1d1	Kaddáfí
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
Sajf	Sajf	k1gInSc1	Sajf
Islám	islám	k1gInSc1	islám
a	a	k8xC	a
Mutasím	Mutasí	k1gNnSc7	Mutasí
Kaddáfí	Kaddáfí	k1gFnSc2	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
prohlášení	prohlášení	k1gNnSc2	prohlášení
syrsko-irácké	syrskorácký	k2eAgFnSc2d1	syrsko-irácký
satelitní	satelitní	k2eAgFnSc2d1	satelitní
televize	televize	k1gFnSc2	televize
al-Raí	al-Raum	k1gNnPc2	al-Raum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zveřejňovala	zveřejňovat	k5eAaImAgNnP	zveřejňovat
oficiální	oficiální	k2eAgNnPc1d1	oficiální
stanoviska	stanovisko	k1gNnPc1	stanovisko
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
projevů	projev	k1gInPc2	projev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
zabit	zabít	k5eAaPmNgInS	zabít
u	u	k7c2	u
města	město	k1gNnSc2	město
Tarhúna	Tarhún	k1gMnSc2	Tarhún
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
syn	syn	k1gMnSc1	syn
Chámis	Chámis	k1gFnSc1	Chámis
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Posílené	posílený	k2eAgFnPc1d1	posílená
brigády	brigáda	k1gFnPc1	brigáda
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
bylo	být	k5eAaImAgNnS	být
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Muammar	Muammar	k1gInSc1	Muammar
Kaddáfí	Kaddáfí	k1gFnSc2	Kaddáfí
byl	být	k5eAaImAgInS	být
dopaden	dopadnout	k5eAaPmNgInS	dopadnout
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
poblíž	poblíž	k7c2	poblíž
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
Syrta	Syrt	k1gInSc2	Syrt
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
skrýval	skrývat	k5eAaImAgInS	skrývat
v	v	k7c6	v
odpadové	odpadový	k2eAgFnSc6d1	odpadová
rouře	roura	k1gFnSc6	roura
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
uprchnout	uprchnout	k5eAaPmF	uprchnout
v	v	k7c6	v
konvoji	konvoj	k1gInSc6	konvoj
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
letectvo	letectvo	k1gNnSc1	letectvo
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnSc3	povstalec
byl	být	k5eAaImAgMnS	být
postřelen	postřelit	k5eAaPmNgMnS	postřelit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
smrtelného	smrtelný	k2eAgNnSc2d1	smrtelné
střelného	střelný	k2eAgNnSc2d1	střelné
poranění	poranění	k1gNnSc2	poranění
<g/>
;	;	kIx,	;
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
lynčován	lynčovat	k5eAaBmNgInS	lynčovat
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
okolností	okolnost	k1gFnPc2	okolnost
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
Amnesty	Amnest	k1gInPc7	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zveřejněných	zveřejněný	k2eAgNnPc2d1	zveřejněné
videí	video	k1gNnPc2	video
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgNnSc1d1	případné
zabití	zabití	k1gNnSc1	zabití
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dopadení	dopadení	k1gNnSc6	dopadení
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
představovat	představovat	k5eAaImF	představovat
válečný	válečný	k2eAgInSc4d1	válečný
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
na	na	k7c6	na
neznámém	známý	k2eNgNnSc6d1	neznámé
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
dle	dle	k7c2	dle
islámských	islámský	k2eAgFnPc2d1	islámská
zvyklostí	zvyklost	k1gFnPc2	zvyklost
a	a	k8xC	a
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
několika	několik	k4yIc2	několik
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Oslavování	oslavování	k1gNnSc1	oslavování
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
éry	éra	k1gFnSc2	éra
bylo	být	k5eAaImAgNnS	být
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Národní	národní	k2eAgFnSc2d1	národní
přechodné	přechodný	k2eAgFnSc2d1	přechodná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
následně	následně	k6eAd1	následně
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
úsudku	úsudek	k1gInSc2	úsudek
porušoval	porušovat	k5eAaImAgMnS	porušovat
Ústavní	ústavní	k2eAgFnSc3d1	ústavní
deklaraci	deklarace	k1gFnSc3	deklarace
přechodné	přechodný	k2eAgFnSc2d1	přechodná
rady	rada	k1gFnSc2	rada
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
