<s>
Otto	Otto	k1gMnSc1
von	k1gMnSc1	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Bismarck	Bismarcka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
německém	německý	k2eAgMnSc6d1
kancléři	kancléř	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Bismarck	Bismarck	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
</s>
<s>
Německý	německý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1871	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1890	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
funkce	funkce	k1gFnSc1
založena	založen	k2eAgFnSc1d1
Nástupce	nástupce	k1gMnSc4
</s>
<s>
Leo	Leo	k1gMnSc1
von	von	k1gInSc4
Caprivi	Capriev	k1gFnSc5
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1815	#num#	k4
Schönhausen	Schönhausna	k1gFnPc2
<g/>
,	,	kIx,
Pruské	pruský	k2eAgNnSc1d1
království	království	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1898	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Friedrichsruh	Friedrichsruh	k1gInSc1
<g/>
,	,	kIx,
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Bismarckovo	Bismarckův	k2eAgNnSc1d1
mauzoleum	mauzoleum	k1gNnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Němec	Němec	k1gMnSc1
Choť	choť	k1gMnSc1
</s>
<s>
Johanna	Johanna	k6eAd1
von	von	k1gInSc1
Puttkamer	Puttkamer	k1gInSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Karl	Karl	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
a	a	k8xC
Wilhelmine	Wilhelmin	k1gInSc5
Luise	Luisa	k1gFnSc6
Mencken	Mencken	k2eAgMnSc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Malwine	Malwinout	k5eAaPmIp3nS
von	von	k1gInSc4
Bismarck	Bismarcka	k1gFnPc2
a	a	k8xC
Bernhard	Bernharda	k1gFnPc2
von	von	k1gInSc1
Bismarck	Bismarck	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
Zaměstnání	zaměstnání	k1gNnSc1
</s>
<s>
politik	politik	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
luteránství	luteránství	k1gNnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
černé	černý	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
Vilémův	Vilémův	k2eAgInSc1d1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
württemberské	württemberský	k2eAgFnSc2d1
korunyŘád	korunyŘáda	k1gFnPc2
SerafínůŘád	SerafínůŘáda	k1gFnPc2
zvěstování	zvěstování	k1gNnSc2
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Podpis	podpis	k1gInSc4
</s>
<s>
Commons	Commons	k6eAd1
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Otto	Otto	k1gMnSc1
Eduard	Eduard	k1gMnSc1
Leopold	Leopold	k1gMnSc1
von	von	k1gInSc4
Bismarck-Schönhausen	Bismarck-Schönhausen	k2eAgInSc4d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Lauenburgu	Lauenburg	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1815	#num#	k4
<g/>
,	,	kIx,
Schönhausen	Schönhausen	k1gInSc1
<g/>
,	,	kIx,
Pruské	pruský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1898	#num#	k4
<g/>
,	,	kIx,
Friedrichsruh	Friedrichsruh	k1gInSc1
<g/>
,	,	kIx,
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
politiků	politik	k1gMnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
budovatel	budovatel	k1gMnSc1
sjednoceného	sjednocený	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
ministr	ministr	k1gMnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
premiér	premiéra	k1gFnPc2
<g/>
)	)	kIx)
Pruska	Prusko	k1gNnSc2
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
první	první	k4xOgInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
německých	německý	k2eAgMnPc2d1
kancléřů	kancléř	k1gMnPc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
nekompromisní	kompromisní	k2eNgInSc1d1
přístup	přístup	k1gInSc1
k	k	k7c3
řešení	řešení	k1gNnSc3
politických	politický	k2eAgInPc2d1
problémů	problém	k1gMnPc2
a	a	k8xC
autoritativní	autoritativní	k2eAgNnSc4d1
vystupování	vystupování	k1gNnSc4
vůči	vůči	k7c3
sněmu	sněm	k1gInSc3
mu	on	k3xPp3gInSc3
vyneslo	vynést	k5eAaPmAgNnS
přezdívku	přezdívka	k1gFnSc4
Železný	Železný	k1gMnSc1
kancléř	kancléř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc2
a	a	k8xC
studia	studio	k1gNnSc2
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1815	#num#	k4
v	v	k7c4
Schönhausenu	Schönhausen	k2eAgFnSc4d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
bratru	bratr	k1gMnSc6
Bernhardovi	Bernhard	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1771	#num#	k4
<g/>
–	–	k?
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
málo	málo	k6eAd1
nadaný	nadaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
prostý	prostý	k2eAgMnSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
ctižádosti	ctižádost	k1gFnSc2
<g/>
,	,	kIx,
velký	velký	k2eAgMnSc1d1
pijan	pijan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Menchenová	Menchenová	k1gFnSc1
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1839	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
ctižádostivá	ctižádostivý	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
<g/>
,	,	kIx,
milující	milující	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc1
<g/>
,	,	kIx,
společenský	společenský	k2eAgInSc4d1
lesk	lesk	k1gInSc4
a	a	k8xC
konexe	konexe	k1gFnPc4
u	u	k7c2
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bismarck	Bismarck	k1gMnSc1
matku	matka	k1gFnSc4
nesnášel	snášet	k5eNaImAgMnS
<g/>
,	,	kIx,
vyčítal	vyčítat	k5eAaImAgInS
jí	on	k3xPp3gFnSc7
citový	citový	k2eAgInSc1d1
chlad	chlad	k1gInSc1
<g/>
,	,	kIx,
rozumářství	rozumářství	k1gNnSc1
<g/>
,	,	kIx,
liberalismus	liberalismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otto	Otto	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnSc2
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
a	a	k8xC
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
junkerské	junkerský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
Bismarcků	Bismarck	k1gInPc2
<g/>
,	,	kIx,
nedlouho	dlouho	k6eNd1
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
narození	narození	k1gNnSc6
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
na	na	k7c4
statek	statek	k1gInSc4
v	v	k7c6
západních	západní	k2eAgInPc6d1
Pomořanech	Pomořany	k1gInPc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
vzdělávat	vzdělávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nějakém	nějaký	k3yIgInSc6
čase	čas	k1gInSc6
odešel	odejít	k5eAaPmAgMnS
Bismarck	Bismarck	k1gMnSc1
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
pro	pro	k7c4
finanční	finanční	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
vrátil	vrátit	k5eAaPmAgInS
zpět	zpět	k6eAd1
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1836	#num#	k4
<g/>
–	–	k?
<g/>
1839	#num#	k4
byl	být	k5eAaImAgInS
zaměstnán	zaměstnat	k5eAaPmNgInS
ve	v	k7c6
státní	státní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
1839	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
a	a	k8xC
on	on	k3xPp3gMnSc1
spolu	spolu	k6eAd1
s	s	k7c7
bratry	bratr	k1gMnPc7
odešel	odejít	k5eAaPmAgMnS
na	na	k7c4
pomořanský	pomořanský	k2eAgInSc4d1
statek	statek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1845	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
i	i	k9
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
a	a	k8xC
on	on	k3xPp3gMnSc1
odešel	odejít	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Schönhausenu	Schönhausen	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
za	za	k7c4
manželku	manželka	k1gFnSc4
Johannu	Johanen	k2eAgFnSc4d1
Puttkamerovou	Puttkamerová	k1gFnSc4
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
28	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1847	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1845	#num#	k4
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgInS
zaměstnat	zaměstnat	k5eAaPmF
jako	jako	k9
vládní	vládní	k2eAgMnSc1d1
poříční	poříční	k2eAgMnSc1d1
správce	správce	k1gMnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
dostane	dostat	k5eAaPmIp3nS
nejrychleji	rychle	k6eAd3
do	do	k7c2
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
také	také	k9
roku	rok	k1gInSc2
1847	#num#	k4
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1851	#num#	k4
<g/>
–	–	k?
<g/>
1859	#num#	k4
byl	být	k5eAaImAgMnS
zástupcem	zástupce	k1gMnSc7
Pruska	Prusko	k1gNnSc2
ve	v	k7c6
Spolkovém	spolkový	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
odmítal	odmítat	k5eAaImAgMnS
vůdčí	vůdčí	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
Rakouska	Rakousko	k1gNnSc2
v	v	k7c6
Německém	německý	k2eAgInSc6d1
spolku	spolek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1859	#num#	k4
byl	být	k5eAaImAgInS
vyslán	vyslat	k5eAaPmNgInS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
jako	jako	k8xC,k8xS
pruský	pruský	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
setrval	setrvat	k5eAaPmAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1862	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
z	z	k7c2
popudu	popud	k1gInSc2
královské	královský	k2eAgFnSc2d1
pruské	pruský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
přeložen	přeložit	k5eAaPmNgInS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
rozpoután	rozpoután	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
pruským	pruský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
Vilémem	Vilém	k1gMnSc7
I.	I.	kA
a	a	k8xC
poslaneckou	poslanecký	k2eAgFnSc7d1
sněmovnou	sněmovna	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zamítala	zamítat	k5eAaImAgFnS
reformu	reforma	k1gFnSc4
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
prosazena	prosazen	k2eAgFnSc1d1
díky	díky	k7c3
dosazení	dosazení	k1gNnSc3
Bismarcka	Bismarcko	k1gNnSc2
do	do	k7c2
funkce	funkce	k1gFnSc2
pruského	pruský	k2eAgMnSc2d1
ministerského	ministerský	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
navrhl	navrhnout	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
pruský	pruský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Roone	Roon	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Bismarck	Bismarck	k1gMnSc1
jako	jako	k8xC,k8xS
výhybkář	výhybkář	k1gMnSc1
(	(	kIx(
<g/>
karikatura	karikatura	k1gFnSc1
v	v	k7c6
časopise	časopis	k1gInSc6
Punch	Puncha	k1gFnPc2
<g/>
,	,	kIx,
1878	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
pruském	pruský	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
v	v	k7c6
prusko-rakouské	prusko-rakouský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
rozpustil	rozpustit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1867	#num#	k4
Německý	německý	k2eAgInSc1d1
spolek	spolek	k1gInSc1
a	a	k8xC
založil	založit	k5eAaPmAgInS
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
mělo	mít	k5eAaImAgNnS
vedoucí	vedoucí	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
právě	právě	k9
Prusko	Prusko	k1gNnSc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Bismarckem	Bismarck	k1gInSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
sestaven	sestavit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1870	#num#	k4
se	se	k3xPyFc4
vyostřily	vyostřit	k5eAaPmAgInP
již	již	k6eAd1
tak	tak	k6eAd1
napjaté	napjatý	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
s	s	k7c7
Francií	Francie	k1gFnSc7
kvůli	kvůli	k7c3
emžské	emžský	k2eAgFnSc3d1
depeši	depeše	k1gFnSc3
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
zesměšnil	zesměšnit	k5eAaPmAgMnS
jednání	jednání	k1gNnSc4
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
mobilizována	mobilizován	k2eAgFnSc1d1
pruská	pruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
a	a	k8xC
po	po	k7c6
obklíčení	obklíčení	k1gNnSc6
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
u	u	k7c2
Sedanu	sedan	k1gInSc2
a	a	k8xC
několikaměsíčním	několikaměsíční	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
Paříže	Paříž	k1gFnSc2
byla	být	k5eAaImAgFnS
Francie	Francie	k1gFnSc1
donucena	donutit	k5eAaPmNgFnS
v	v	k7c6
květnu	květen	k1gInSc6
1871	#num#	k4
podepsat	podepsat	k5eAaPmF
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
;	;	kIx,
Německo	Německo	k1gNnSc1
anektovalo	anektovat	k5eAaBmAgNnS
Alsasko	Alsasko	k1gNnSc4
a	a	k8xC
část	část	k1gFnSc4
Lotrinska	Lotrinsko	k1gNnSc2
a	a	k8xC
navíc	navíc	k6eAd1
museli	muset	k5eAaImAgMnP
Francouzi	Francouz	k1gMnPc1
zaplatit	zaplatit	k5eAaPmF
reparace	reparace	k1gFnPc4
5	#num#	k4
miliard	miliarda	k4xCgFnPc2
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
záborem	zábor	k1gInSc7
francouzských	francouzský	k2eAgNnPc2d1
území	území	k1gNnPc2
sice	sice	k8xC
nesouhlasil	souhlasit	k5eNaImAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
umlčen	umlčet	k5eAaPmNgInS
vojenskými	vojenský	k2eAgInPc7d1
kruhy	kruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
<g/>
,	,	kIx,
1873	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
v	v	k7c6
Zrcadlové	zrcadlový	k2eAgFnSc6d1
síni	síň	k1gFnSc6
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
vyhlášeno	vyhlášen	k2eAgNnSc1d1
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
německým	německý	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
a	a	k8xC
Bismarck	Bismarck	k1gInSc4
prvním	první	k4xOgMnSc6
německým	německý	k2eAgMnSc7d1
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
na	na	k7c4
něj	on	k3xPp3gMnSc4
spáchán	spáchán	k2eAgInSc1d1
neúspěšný	úspěšný	k2eNgInSc1d1
atentát	atentát	k1gInSc1
rukou	ruka	k1gFnSc7
mladého	mladý	k2eAgMnSc2d1
katolíka	katolík	k1gMnSc2
a	a	k8xC
tak	tak	k6eAd1
omezil	omezit	k5eAaPmAgInS
moc	moc	k1gFnSc4
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
a	a	k8xC
strany	strana	k1gFnSc2
Centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
začal	začít	k5eAaPmAgInS
Kulturkampf	Kulturkampf	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
vyšlo	vyjít	k5eAaPmAgNnS
Centrum	centrum	k1gNnSc1
posíleno	posílit	k5eAaPmNgNnS
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yQnSc4,k3yRnSc4
reagoval	reagovat	k5eAaBmAgInS
podáním	podání	k1gNnSc7
rezignace	rezignace	k1gFnSc2
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
ale	ale	k9
posloužit	posloužit	k5eAaPmF
jen	jen	k9
pro	pro	k7c4
postrašení	postrašení	k1gNnSc4
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ji	on	k3xPp3gFnSc4
ale	ale	k8xC
navzdory	navzdory	k6eAd1
jeho	jeho	k3xOp3gNnSc7
očekáváním	očekávání	k1gNnSc7
přijal	přijmout	k5eAaPmAgMnS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
stáhnout	stáhnout	k5eAaPmF
a	a	k8xC
odjet	odjet	k5eAaPmF
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dovolenou	dovolená	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
až	až	k9
14.2	14.2	k4
<g/>
.1878	.1878	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
z	z	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
popudu	popud	k1gInSc2
uspořádán	uspořádán	k2eAgInSc4d1
Berlínský	berlínský	k2eAgInSc4d1
kongres	kongres	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
ukázat	ukázat	k5eAaPmF
Prusko	Prusko	k1gNnSc4
jako	jako	k8xC,k8xS
zemi	zem	k1gFnSc4
otevřenou	otevřený	k2eAgFnSc4d1
Evropě	Evropa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1881	#num#	k4
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
sociálního	sociální	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zahrnoval	zahrnovat	k5eAaImAgMnS
všechny	všechen	k3xTgFnPc4
vrstvy	vrstva	k1gFnPc4
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
dokončen	dokončit	k5eAaPmNgMnS
byl	být	k5eAaImAgMnS
až	až	k6eAd1
roku	rok	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
pád	pád	k1gInSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1888	#num#	k4
Vilém	Viléma	k1gFnPc2
I.	I.	kA
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
nedlouho	dlouho	k6eNd1
po	po	k7c6
něm	on	k3xPp3gNnSc6
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
sebou	se	k3xPyFc7
nenechal	nechat	k5eNaPmAgMnS
tolik	tolik	k6eAd1
manipulovat	manipulovat	k5eAaImF
a	a	k8xC
postupně	postupně	k6eAd1
Bismarcka	Bismarcko	k1gNnSc2
donutil	donutit	k5eAaPmAgInS
rezignovat	rezignovat	k5eAaBmF
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
18	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1890	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
Bismarck	Bismarck	k1gMnSc1
uchýlil	uchýlit	k5eAaPmAgMnS
na	na	k7c4
své	svůj	k3xOyFgInPc4
statky	statek	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
psal	psát	k5eAaImAgMnS
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1894	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
Johanna	Johanen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
zemřel	zemřít	k5eAaPmAgMnS
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1898	#num#	k4
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
před	před	k7c7
jedenáctou	jedenáctý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
večer	večer	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarcka	Bismarcko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DELLIS	DELLIS	kA
<g/>
,	,	kIx,
Constantin	Constantin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bismarcks	Bismarcksa	k1gFnPc2
Außenpolitik	Außenpolitika	k1gFnPc2
von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Krieg-in-Sicht	Krieg-in-Sicht	k1gInSc1
Krise	kris	k1gInSc5
1875	#num#	k4
bis	bis	k?
zur	zur	k?
Erneuerung	Erneuerung	k1gInSc4
des	des	k1gNnSc3
Dreikaiservertrags	Dreikaiservertragsa	k1gFnPc2
1881	#num#	k4
unter	untra	k1gFnPc2
besonderer	besonderer	k1gMnSc1
Berücksichtigung	Berücksichtigung	k1gMnSc1
der	drát	k5eAaImRp2nS
deutsch-russischen	deutsch-russischen	k2eAgInSc4d1
Beziehungen	Beziehungen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
-	-	kIx~
<g/>
167	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85899	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RÁKOSNÍK	rákosník	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cukr	cukr	k1gInSc4
a	a	k8xC
bič	bič	k1gInSc4
kancléře	kancléř	k1gMnSc2
Bismarcka	Bismarcko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
130	#num#	k4
<g/>
-	-	kIx~
<g/>
138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Loewenstein	Loewenstein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Portréty	portrét	k1gInPc1
-	-	kIx~
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STELLNER	STELLNER	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bismarck	Bismarck	k1gMnSc1
und	und	k?
Caprivi	Capriev	k1gFnSc3
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Probleme	Probleme	k1gMnSc1
der	drát	k5eAaImRp2nS
postbismarckschen	postbismarckschna	k1gFnPc2
Eliten	Eliten	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
419	#num#	k4
<g/>
-	-	kIx~
<g/>
427	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
208	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SUCHÁNEK	Suchánek	k1gMnSc1
<g/>
,	,	kIx,
Drahomír	Drahomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bismarckův	Bismarckův	k2eAgInSc1d1
kulturní	kulturní	k2eAgInSc1d1
boj	boj	k1gInSc1
-	-	kIx~
kořeny	kořen	k1gInPc1
<g/>
,	,	kIx,
příčiny	příčina	k1gFnPc1
<g/>
,	,	kIx,
souvislosti	souvislost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
248	#num#	k4
<g/>
-	-	kIx~
<g/>
258	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLF	Wolf	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhroucení	zhroucení	k1gNnSc1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
359	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Německý	německý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1
Německa	Německo	k1gNnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Německa	Německo	k1gNnSc2
</s>
<s>
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Kulturkampf	Kulturkampf	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Otto	Otto	k1gMnSc1
von	von	k1gInSc1
Bismarck	Bismarcka	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Říšští	říšský	k2eAgMnPc1d1
kancléři	kancléř	k1gMnPc1
Německého	německý	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1871	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc1
Bismarck	Bismarck	k1gMnSc1
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leo	Leo	k1gMnSc1
von	von	k1gInSc1
Caprivi	Capriev	k1gFnSc3
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
–	–	k?
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chlodwig	Chlodwig	k1gMnSc1
zu	zu	k?
Hohenlohe-Schillingsfürst	Hohenlohe-Schillingsfürst	k1gMnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bernhard	Bernhard	k1gInSc1
von	von	k1gInSc1
Bülow	Bülow	k1gFnSc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Theobald	Theobald	k1gInSc1
von	von	k1gInSc1
Bethmann-Hollweg	Bethmann-Hollweg	k1gInSc1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Georg	Georg	k1gInSc1
Michaelis	Michaelis	k1gFnSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Georg	Georg	k1gInSc1
von	von	k1gInSc1
Hertling	Hertling	k1gInSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000600879	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11851136X	11851136X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2101	#num#	k4
0812	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79018384	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
46772111	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79018384	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
