<s>
Klášter	klášter	k1gInSc1	klášter
Cîteaux	Cîteaux	k1gInSc1	Cîteaux
je	být	k5eAaImIp3nS	být
trapistické	trapistický	k2eAgNnSc4d1	trapistické
opatství	opatství	k1gNnSc4	opatství
poblíž	poblíž	k7c2	poblíž
Dijonu	Dijon	k1gInSc2	Dijon
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
dřívější	dřívější	k2eAgFnPc4d1	dřívější
zakládací	zakládací	k2eAgFnPc4d1	zakládací
a	a	k8xC	a
správní	správní	k2eAgNnSc1d1	správní
centrum	centrum	k1gNnSc1	centrum
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1098	[number]	k4	1098
sv.	sv.	kA	sv.
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
z	z	k7c2	z
Molesme	Molesme	k1gFnSc2	Molesme
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
benediktinskou	benediktinský	k2eAgFnSc4d1	benediktinská
fundaci	fundace	k1gFnSc4	fundace
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
benediktinů	benediktin	k1gMnPc2	benediktin
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
klášterem	klášter	k1gInSc7	klášter
nového	nový	k2eAgInSc2d1	nový
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
sedm	sedm	k4xCc4	sedm
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
řádu	řád	k1gInSc2	řád
<g/>
:	:	kIx,	:
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
konala	konat	k5eAaImAgFnS	konat
generální	generální	k2eAgFnSc1d1	generální
kapitula	kapitula	k1gFnSc1	kapitula
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
opat	opat	k1gMnSc1	opat
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
generálním	generální	k2eAgMnSc7d1	generální
opatem	opat	k1gMnSc7	opat
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
prodáno	prodat	k5eAaPmNgNnS	prodat
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
pozemky	pozemek	k1gInPc1	pozemek
vykoupeny	vykoupen	k2eAgInPc1d1	vykoupen
trapisty	trapist	k1gInPc1	trapist
a	a	k8xC	a
opatství	opatství	k1gNnSc1	opatství
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gInSc2	jejich
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
reformy	reforma	k1gFnSc2	reforma
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
mnichů	mnich	k1gMnPc2	mnich
z	z	k7c2	z
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
Molesme	Molesme	k1gFnSc4	Molesme
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
sv.	sv.	kA	sv.
Roberta	Robert	k1gMnSc4	Robert
vydalo	vydat	k5eAaPmAgNnS	vydat
do	do	k7c2	do
neosídlené	osídlený	k2eNgFnSc2d1	neosídlená
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
20	[number]	k4	20
km	km	kA	km
od	od	k7c2	od
Dijonu	Dijon	k1gInSc2	Dijon
<g/>
,	,	kIx,	,
na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
římské	římský	k2eAgFnSc6d1	římská
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Cîteaux	Cîteaux	k1gInSc1	Cîteaux
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
mniši	mnich	k1gMnPc1	mnich
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1098	[number]	k4	1098
nový	nový	k2eAgInSc1d1	nový
klášter	klášter	k1gInSc1	klášter
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ho	on	k3xPp3gInSc4	on
podle	podle	k7c2	podle
milníku	milník	k1gInSc2	milník
Cistercium	Cistercium	k1gNnSc1	Cistercium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vžil	vžít	k5eAaPmAgInS	vžít
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
mnišský	mnišský	k2eAgInSc4d1	mnišský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
Cîteaux	Cîteaux	k1gInSc1	Cîteaux
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mateřským	mateřský	k2eAgInSc7d1	mateřský
klášterem	klášter	k1gInSc7	klášter
všech	všecek	k3xTgInPc2	všecek
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
klášterů	klášter	k1gInPc2	klášter
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
zaujímaly	zaujímat	k5eAaImAgFnP	zaujímat
podřízené	podřízený	k2eAgNnSc4d1	podřízené
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
řádu	řád	k1gInSc2	řád
byla	být	k5eAaImAgFnS	být
pevně	pevně	k6eAd1	pevně
spojena	spojit	k5eAaPmNgFnS	spojit
filiačními	filiační	k2eAgFnPc7d1	filiační
vazbami	vazba	k1gFnPc7	vazba
a	a	k8xC	a
opat	opat	k1gMnSc1	opat
v	v	k7c4	v
Cîteaux	Cîteaux	k1gInSc4	Cîteaux
byl	být	k5eAaImAgInS	být
nejvýše	vysoce	k6eAd3	vysoce
postavenou	postavený	k2eAgFnSc7d1	postavená
osobou	osoba	k1gFnSc7	osoba
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
i	i	k9	i
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
zasedáním	zasedání	k1gNnSc7	zasedání
generální	generální	k2eAgFnSc2d1	generální
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
sjeli	sjet	k5eAaPmAgMnP	sjet
opati	opat	k1gMnPc1	opat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
dceřiných	dceřin	k2eAgInPc2d1	dceřin
klášterů	klášter	k1gInPc2	klášter
do	do	k7c2	do
Cîteaux	Cîteaux	k1gInSc4	Cîteaux
a	a	k8xC	a
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
potřebách	potřeba	k1gFnPc6	potřeba
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
kláštera	klášter	k1gInSc2	klášter
svědčí	svědčit	k5eAaImIp3nS	svědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
místo	místo	k7c2	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
vybrala	vybrat	k5eAaPmAgFnS	vybrat
řada	řada	k1gFnSc1	řada
burgundských	burgundský	k2eAgMnPc2d1	burgundský
vévodů	vévoda	k1gMnPc2	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
Pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
během	během	k7c2	během
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
srovnáváno	srovnávat	k5eAaImNgNnS	srovnávat
s	s	k7c7	s
nekropolí	nekropole	k1gFnSc7	nekropole
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
<g/>
.	.	kIx.	.
</s>
