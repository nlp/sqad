<s>
Založil	založit	k5eAaPmAgInS	založit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1098	[number]	k4	1098
sv.	sv.	kA	sv.
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
z	z	k7c2	z
Molesme	Molesme	k1gFnSc2	Molesme
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
benediktinskou	benediktinský	k2eAgFnSc4d1	benediktinská
fundaci	fundace	k1gFnSc4	fundace
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
benediktinů	benediktin	k1gMnPc2	benediktin
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
klášterem	klášter	k1gInSc7	klášter
nového	nový	k2eAgInSc2d1	nový
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
