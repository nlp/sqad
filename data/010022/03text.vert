<p>
<s>
Marie	Marie	k1gFnSc1	Marie
II	II	kA	II
<g/>
.	.	kIx.	.
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1662	[number]	k4	1662
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
královna	královna	k1gFnSc1	královna
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Protestantská	protestantský	k2eAgFnSc1d1	protestantská
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
po	po	k7c6	po
slavné	slavný	k2eAgFnSc6d1	slavná
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
svržení	svržení	k1gNnSc4	svržení
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
katolického	katolický	k2eAgMnSc2d1	katolický
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
vládla	vládnout	k5eAaImAgFnS	vládnout
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Vilémem	Vilém	k1gMnSc7	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
spravoval	spravovat	k5eAaImAgMnS	spravovat
státní	státní	k2eAgFnSc2d1	státní
záležitosti	záležitost	k1gFnSc2	záležitost
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
časté	častý	k2eAgFnSc6d1	častá
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
na	na	k7c6	na
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
<g/>
,	,	kIx,	,
vládla	vládnout	k5eAaImAgFnS	vládnout
Marie	Marie	k1gFnSc1	Marie
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
často	často	k6eAd1	často
radila	radit	k5eAaImAgNnP	radit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
zapojovala	zapojovat	k5eAaImAgFnS	zapojovat
do	do	k7c2	do
církevních	církevní	k2eAgFnPc2d1	církevní
záležitostí	záležitost	k1gFnPc2	záležitost
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
Anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
St	St	kA	St
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Palace	Palace	k1gFnSc1	Palace
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1662	[number]	k4	1662
jako	jako	k8xS	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
ženy	žena	k1gFnPc1	žena
Anny	Anna	k1gFnSc2	Anna
Hydeové	Hydeová	k1gFnSc2	Hydeová
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
strýcem	strýc	k1gMnSc7	strýc
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
Anny	Anna	k1gFnSc2	Anna
Hydeové	Hydeová	k1gFnSc2	Hydeová
se	se	k3xPyFc4	se
dožily	dožít	k5eAaPmAgFnP	dožít
dospělosti	dospělost	k1gFnPc1	dospělost
pouze	pouze	k6eAd1	pouze
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
nebo	nebo	k8xC	nebo
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
však	však	k9	však
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
vychovávány	vychováván	k2eAgFnPc4d1	vychovávána
v	v	k7c6	v
protestantském	protestantský	k2eAgMnSc6d1	protestantský
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Mariina	Mariin	k2eAgFnSc1d1	Mariina
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1671	[number]	k4	1671
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Modenskou	modenský	k2eAgFnSc7d1	modenská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
zasnoubena	zasnoubit	k5eAaPmNgFnS	zasnoubit
nizozemskému	nizozemský	k2eAgMnSc3d1	nizozemský
protestantskému	protestantský	k2eAgMnSc3d1	protestantský
místokráli	místokrál	k1gMnSc3	místokrál
Vilémovi	Vilém	k1gMnSc3	Vilém
<g/>
,	,	kIx,	,
princi	princ	k1gMnSc3	princ
Oranžskému	oranžský	k2eAgMnSc3d1	oranžský
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
preferoval	preferovat	k5eAaImAgMnS	preferovat
její	její	k3xOp3gNnSc4	její
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
s	s	k7c7	s
následníkem	následník	k1gMnSc7	následník
francouzského	francouzský	k2eAgInSc2d1	francouzský
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
s	s	k7c7	s
nepřijatelností	nepřijatelnost	k1gFnSc7	nepřijatelnost
politické	politický	k2eAgFnSc2d1	politická
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
zásnubami	zásnuba	k1gFnPc7	zásnuba
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
parlamentu	parlament	k1gInSc2	parlament
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
zasnoubením	zasnoubení	k1gNnSc7	zasnoubení
i	i	k8xC	i
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
popularitu	popularita	k1gFnSc4	popularita
mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
pak	pak	k6eAd1	pak
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
svému	svůj	k3xOyFgMnSc3	svůj
muži	muž	k1gMnSc3	muž
oddaná	oddaný	k2eAgFnSc1d1	oddaná
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
tři	tři	k4xCgNnPc1	tři
těhotenství	těhotenství	k1gNnPc1	těhotenství
skončila	skončit	k5eAaPmAgNnP	skončit
potratem	potrat	k1gInSc7	potrat
nebo	nebo	k8xC	nebo
narozením	narození	k1gNnSc7	narození
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
srdečná	srdečný	k2eAgFnSc1d1	srdečná
povaha	povaha	k1gFnSc1	povaha
jí	on	k3xPp3gFnSc3	on
získala	získat	k5eAaPmAgFnS	získat
přízeň	přízeň	k1gFnSc4	přízeň
mezi	mezi	k7c7	mezi
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
dvorní	dvorní	k2eAgFnSc7d1	dvorní
dámou	dáma	k1gFnSc7	dáma
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Villiersovou	Villiersová	k1gFnSc7	Villiersová
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
více	hodně	k6eAd2	hodně
jako	jako	k8xC	jako
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Orkney	Orknea	k1gFnSc2	Orknea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavná	slavný	k2eAgFnSc1d1	slavná
revoluce	revoluce	k1gFnSc1	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
jeho	jeho	k3xOp3gMnSc7	jeho
bratr	bratr	k1gMnSc1	bratr
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Mariin	Mariin	k2eAgMnSc1d1	Mariin
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Jakub	Jakub	k1gMnSc1	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
církevní	církevní	k2eAgFnSc1d1	církevní
politika	politika	k1gFnSc1	politika
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
pro	pro	k7c4	pro
neanglikánské	anglikánský	k2eNgFnPc4d1	anglikánský
církve	církev	k1gFnPc4	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgInS	chtít
královským	královský	k2eAgInSc7d1	královský
dekretem	dekret	k1gInSc7	dekret
anulovat	anulovat	k5eAaBmF	anulovat
zákony	zákon	k1gInPc4	zákon
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
protestantských	protestantský	k2eAgMnPc2d1	protestantský
aristokratů	aristokrat	k1gMnPc2	aristokrat
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
Mariiným	Mariin	k2eAgMnSc7d1	Mariin
manželem	manžel	k1gMnSc7	manžel
Vilémem	Vilém	k1gMnSc7	Vilém
Oranžským	oranžský	k2eAgMnSc7d1	oranžský
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Jakub	Jakub	k1gMnSc1	Jakub
přinutil	přinutit	k5eAaPmAgMnS	přinutit
anglikánské	anglikánský	k2eAgMnPc4d1	anglikánský
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
předčítali	předčítat	k5eAaImAgMnP	předčítat
deklaraci	deklarace	k1gFnSc4	deklarace
o	o	k7c6	o
shovívavosti	shovívavost	k1gFnSc6	shovívavost
–	–	k?	–
prohlášení	prohlášení	k1gNnSc2	prohlášení
udělující	udělující	k2eAgFnSc4d1	udělující
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
pro	pro	k7c4	pro
jinověrce	jinověrec	k1gMnSc4	jinověrec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
popularita	popularita	k1gFnSc1	popularita
upadla	upadnout	k5eAaPmAgFnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Neklid	neklid	k1gInSc1	neklid
mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
vzrostl	vzrůst	k5eAaPmAgMnS	vzrůst
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
druhé	druhý	k4xOgFnSc3	druhý
manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
horlivé	horlivý	k2eAgFnSc3d1	horlivá
katoličce	katolička	k1gFnSc3	katolička
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Jakub	Jakub	k1gMnSc1	Jakub
Stuart	Stuart	k1gInSc4	Stuart
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vychováván	vychováván	k2eAgMnSc1d1	vychováván
v	v	k7c6	v
katolickém	katolický	k2eAgMnSc6d1	katolický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1688	[number]	k4	1688
sedm	sedm	k4xCc1	sedm
významných	významný	k2eAgMnPc2d1	významný
šlechticů	šlechtic	k1gMnPc2	šlechtic
tajně	tajně	k6eAd1	tajně
požádalo	požádat	k5eAaPmAgNnS	požádat
Viléma	Vilém	k1gMnSc4	Vilém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
záměru	záměr	k1gInSc3	záměr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
jako	jako	k8xC	jako
nástupkyně	nástupkyně	k1gFnSc1	nástupkyně
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
převzít	převzít	k5eAaPmF	převzít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Maria	k1gFnSc2	Maria
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
sdělila	sdělit	k5eAaPmAgNnP	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
politické	politický	k2eAgFnPc4d1	politická
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gInSc3	on
loajální	loajální	k2eAgFnSc1d1	loajální
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
tažením	tažení	k1gNnSc7	tažení
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
mnohé	mnohý	k2eAgFnPc4d1	mnohá
záruky	záruka	k1gFnPc4	záruka
pro	pro	k7c4	pro
Angličany	Angličan	k1gMnPc4	Angličan
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
cílem	cíl	k1gInSc7	cíl
výpravy	výprava	k1gFnSc2	výprava
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
sestavení	sestavení	k1gNnSc4	sestavení
svobodného	svobodný	k2eAgInSc2d1	svobodný
a	a	k8xC	a
právoplatného	právoplatný	k2eAgInSc2d1	právoplatný
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
Jakubova	Jakubův	k2eAgFnSc1d1	Jakubova
armáda	armáda	k1gFnSc1	armáda
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgMnSc4d1	schopen
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
krále	král	k1gMnSc4	král
ochránit	ochránit	k5eAaPmF	ochránit
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
Jakub	Jakub	k1gMnSc1	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uniknout	uniknout	k5eAaPmF	uniknout
a	a	k8xC	a
s	s	k7c7	s
tichým	tichý	k2eAgInSc7d1	tichý
souhlasem	souhlas	k1gInSc7	souhlas
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Jakub	Jakub	k1gMnSc1	Jakub
stal	stát	k5eAaPmAgMnS	stát
katolickým	katolický	k2eAgMnSc7d1	katolický
mučedníkem	mučedník	k1gMnSc7	mučedník
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
byl	být	k5eAaImAgInS	být
Vilémem	Vilém	k1gMnSc7	Vilém
svolán	svolat	k5eAaPmNgInS	svolat
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
probíhat	probíhat	k5eAaImF	probíhat
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
si	se	k3xPyFc3	se
nebyl	být	k5eNaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
svou	svůj	k3xOyFgFnSc7	svůj
pozicí	pozice	k1gFnSc7	pozice
a	a	k8xC	a
přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
vládnout	vládnout	k5eAaImF	vládnout
jako	jako	k9	jako
král	král	k1gMnSc1	král
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k9	jako
manžel	manžel	k1gMnSc1	manžel
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Vymínil	vymínit	k5eAaPmAgInS	vymínit
si	se	k3xPyFc3	se
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
případné	případný	k2eAgFnSc6d1	případná
Mariině	Mariin	k2eAgFnSc6d1	Mariina
smrti	smrt	k1gFnSc6	smrt
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
prominentní	prominentní	k2eAgMnPc1d1	prominentní
politici	politik	k1gMnPc1	politik
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
královnou	královna	k1gFnSc7	královna
stala	stát	k5eAaPmAgFnS	stát
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
zůstala	zůstat	k5eAaPmAgFnS	zůstat
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgMnSc3	svůj
muži	muž	k1gMnSc3	muž
loajální	loajální	k2eAgNnSc1d1	loajální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1689	[number]	k4	1689
vydal	vydat	k5eAaPmAgInS	vydat
anglický	anglický	k2eAgInSc1d1	anglický
parlament	parlament	k1gInSc1	parlament
deklaraci	deklarace	k1gFnSc4	deklarace
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
útěku	útěk	k1gInSc2	útěk
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
trůn	trůn	k1gInSc4	trůn
volný	volný	k2eAgInSc4d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Marie	Maria	k1gFnSc2	Maria
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
stát	stát	k5eAaPmF	stát
společně	společně	k6eAd1	společně
panovníky	panovník	k1gMnPc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1689	[number]	k4	1689
korunováni	korunovat	k5eAaBmNgMnP	korunovat
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
korunovace	korunovace	k1gFnSc2	korunovace
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
také	také	k9	také
skotský	skotský	k2eAgInSc4d1	skotský
parlament	parlament	k1gInSc4	parlament
Jakubovu	Jakubův	k2eAgFnSc4d1	Jakubova
vládu	vláda	k1gFnSc4	vláda
za	za	k7c4	za
ukončenou	ukončená	k1gFnSc4	ukončená
<g/>
.	.	kIx.	.
</s>
<s>
Vilémovi	Vilémův	k2eAgMnPc1d1	Vilémův
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
byla	být	k5eAaImAgFnS	být
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
skotská	skotský	k2eAgFnSc1d1	skotská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přijali	přijmout	k5eAaPmAgMnP	přijmout
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1689	[number]	k4	1689
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1689	[number]	k4	1689
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
historii	historie	k1gFnSc6	historie
–	–	k?	–
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
deklarace	deklarace	k1gFnSc2	deklarace
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
omezoval	omezovat	k5eAaImAgInS	omezovat
výsady	výsada	k1gFnSc2	výsada
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
panovník	panovník	k1gMnSc1	panovník
nesmí	smět	k5eNaImIp3nS	smět
pozastavit	pozastavit	k5eAaPmF	pozastavit
platnost	platnost	k1gFnSc4	platnost
zákona	zákon	k1gInSc2	zákon
schváleného	schválený	k2eAgInSc2d1	schválený
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nové	nový	k2eAgFnPc4d1	nová
daně	daň	k1gFnPc4	daň
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
zakázat	zakázat	k5eAaPmF	zakázat
nošení	nošení	k1gNnSc4	nošení
zbraní	zbraň	k1gFnPc2	zbraň
protestantům	protestant	k1gMnPc3	protestant
<g/>
,	,	kIx,	,
neúměrně	úměrně	k6eNd1	úměrně
odkládat	odkládat	k5eAaImF	odkládat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
trestat	trestat	k5eAaImF	trestat
poslance	poslanec	k1gMnPc4	poslanec
obou	dva	k4xCgNnPc2	dva
komor	komora	k1gFnPc2	komora
za	za	k7c4	za
projevy	projev	k1gInPc4	projev
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
debatách	debata	k1gFnPc6	debata
<g/>
,	,	kIx,	,
požadovat	požadovat	k5eAaImF	požadovat
neúměrnou	úměrný	k2eNgFnSc4d1	neúměrná
kauci	kauce	k1gFnSc4	kauce
nebo	nebo	k8xC	nebo
ukládat	ukládat	k5eAaImF	ukládat
nepřiměřené	přiměřený	k2eNgInPc4d1	nepřiměřený
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
také	také	k9	také
řešil	řešit	k5eAaImAgInS	řešit
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
panovnické	panovnický	k2eAgFnSc2d1	panovnická
dvojice	dvojice	k1gFnSc2	dvojice
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
panovníkem	panovník	k1gMnSc7	panovník
ten	ten	k3xDgMnSc1	ten
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
nástupci	nástupce	k1gMnPc7	nástupce
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
Mariinou	Mariin	k2eAgFnSc7d1	Mariina
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Posledními	poslední	k2eAgMnPc7d1	poslední
následníky	následník	k1gMnPc7	následník
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
případné	případný	k2eAgFnPc4d1	případná
Vilémovy	Vilémův	k2eAgFnPc4d1	Vilémova
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
případného	případný	k2eAgInSc2d1	případný
následného	následný	k2eAgInSc2d1	následný
sňatku	sňatek	k1gInSc2	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
trávil	trávit	k5eAaImAgMnS	trávit
Vilém	Vilém	k1gMnSc1	Vilém
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
mimo	mimo	k7c4	mimo
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zápasil	zápasit	k5eAaImAgMnS	zápasit
s	s	k7c7	s
jakobity	jakobita	k1gMnPc7	jakobita
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
;	;	kIx,	;
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
řídila	řídit	k5eAaImAgFnS	řídit
zemi	zem	k1gFnSc4	zem
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Ukazovala	ukazovat	k5eAaImAgNnP	ukazovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
když	když	k8xS	když
nařídila	nařídit	k5eAaPmAgFnS	nařídit
uvěznění	uvěznění	k1gNnSc4	uvěznění
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Henryho	Henry	k1gMnSc2	Henry
Hyda	Hydus	k1gMnSc2	Hydus
za	za	k7c2	za
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
znovu	znovu	k6eAd1	znovu
dosadit	dosadit	k5eAaPmF	dosadit
Jakuba	Jakub	k1gMnSc4	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
odvolala	odvolat	k5eAaPmAgFnS	odvolat
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
uvěznit	uvěznit	k5eAaPmF	uvěznit
vlivného	vlivný	k2eAgMnSc4d1	vlivný
Johna	John	k1gMnSc4	John
Churchilla	Churchill	k1gMnSc2	Churchill
(	(	kIx(	(
<g/>
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Marlborough	Marlborougha	k1gFnPc2	Marlborougha
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
podobné	podobný	k2eAgNnSc4d1	podobné
obvinění	obvinění	k1gNnSc4	obvinění
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
její	její	k3xOp3gFnSc4	její
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Churchillovy	Churchillův	k2eAgFnSc2d1	Churchillova
manželky	manželka	k1gFnSc2	manželka
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
porazil	porazit	k5eAaPmAgMnS	porazit
irské	irský	k2eAgMnPc4d1	irský
jakobity	jakobita	k1gMnPc4	jakobita
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
tažení	tažení	k1gNnSc6	tažení
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
státních	státní	k2eAgFnPc2d1	státní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obracela	obracet	k5eAaImAgFnS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
Vilémovi	Vilém	k1gMnSc3	Vilém
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
podle	podle	k7c2	podle
požadavku	požadavek	k1gInSc2	požadavek
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
velmi	velmi	k6eAd1	velmi
aktivně	aktivně	k6eAd1	aktivně
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
církevních	církevní	k2eAgFnPc2d1	církevní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1694	[number]	k4	1694
v	v	k7c6	v
Kensingtonském	Kensingtonský	k2eAgInSc6d1	Kensingtonský
paláci	palác	k1gInSc6	palác
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
32	[number]	k4	32
let	léto	k1gNnPc2	léto
na	na	k7c4	na
neštovice	neštovice	k1gFnPc4	neštovice
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
vládl	vládnout	k5eAaImAgMnS	vládnout
Vilém	Vilém	k1gMnSc1	Vilém
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgMnSc1d1	samostatný
panovník	panovník	k1gMnSc1	panovník
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
již	již	k6eAd1	již
žádné	žádný	k3yNgMnPc4	žádný
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
dítě	dítě	k1gNnSc1	dítě
Mariiny	Mariin	k2eAgFnSc2d1	Mariina
sestry	sestra	k1gFnSc2	sestra
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1700	[number]	k4	1700
a	a	k8xC	a
Parlament	parlament	k1gInSc1	parlament
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
,	,	kIx,	,
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
vznesením	vznesení	k1gNnSc7	vznesení
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
katolických	katolický	k2eAgMnPc2d1	katolický
potomků	potomek	k1gMnPc2	potomek
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
následnictví	následnictví	k1gNnSc6	následnictví
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
stanovil	stanovit	k5eAaPmAgMnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
protestantský	protestantský	k2eAgMnSc1d1	protestantský
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
Žofie	Žofie	k1gFnSc1	Žofie
Hannoverská	hannoverský	k2eAgFnSc1d1	hannoverská
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
protestantští	protestantský	k2eAgMnPc1d1	protestantský
potomci	potomek	k1gMnPc1	potomek
<g/>
;	;	kIx,	;
po	po	k7c6	po
Vilémově	Vilémův	k2eAgFnSc6d1	Vilémova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
králem	král	k1gMnSc7	král
stal	stát	k5eAaPmAgMnS	stát
Žofiin	Žofiin	k2eAgMnSc1d1	Žofiin
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
Ludvík	Ludvík	k1gMnSc1	Ludvík
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mary	Mary	k1gFnSc2	Mary
II	II	kA	II
of	of	k?	of
England	England	k1gInSc1	England
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marie	Maria	k1gFnSc2	Maria
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
1911	[number]	k4	1911
Encyclopæ	Encyclopæ	k1gFnPc2	Encyclopæ
Britannica	Britannica	k1gFnSc1	Britannica
<g/>
/	/	kIx~	/
<g/>
Mary	Mary	k1gFnSc1	Mary
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc1	Queen
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
