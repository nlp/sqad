<s>
Jihomoravské	jihomoravský	k2eAgNnSc1d1	Jihomoravské
inovační	inovační	k2eAgNnSc1d1	inovační
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
JIC	JIC	kA	JIC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
inovačního	inovační	k2eAgNnSc2d1	inovační
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
komerčního	komerční	k2eAgNnSc2d1	komerční
využití	využití	k1gNnSc2	využití
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
