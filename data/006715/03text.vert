<s>
Jihomoravské	jihomoravský	k2eAgNnSc1d1	Jihomoravské
inovační	inovační	k2eAgNnSc1d1	inovační
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
JIC	JIC	kA	JIC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
inovačního	inovační	k2eAgNnSc2d1	inovační
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
komerčního	komerční	k2eAgNnSc2d1	komerční
využití	využití	k1gNnSc2	využití
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
propojení	propojení	k1gNnSc4	propojení
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vědecko-výzkumných	vědeckoýzkumný	k2eAgFnPc2d1	vědecko-výzkumná
institucí	instituce	k1gFnPc2	instituce
s	s	k7c7	s
podnikatelskou	podnikatelský	k2eAgFnSc7d1	podnikatelská
sférou	sféra	k1gFnSc7	sféra
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
přínos	přínos	k1gInSc4	přínos
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
na	na	k7c6	na
regionální	regionální	k2eAgFnSc6d1	regionální
a	a	k8xC	a
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
let	léto	k1gNnPc2	léto
JIC	JIC	kA	JIC
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
inovační	inovační	k2eAgNnSc4d1	inovační
podnikání	podnikání	k1gNnSc4	podnikání
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
vytváření	vytváření	k1gNnSc4	vytváření
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podnikatelům	podnikatel	k1gMnPc3	podnikatel
komplexní	komplexní	k2eAgInSc4d1	komplexní
balík	balík	k1gInSc4	balík
služeb	služba	k1gFnPc2	služba
-	-	kIx~	-
od	od	k7c2	od
prvotního	prvotní	k2eAgInSc2d1	prvotní
nápadu	nápad	k1gInSc2	nápad
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
start-up	startp	k1gInSc4	start-up
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
plně	plně	k6eAd1	plně
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Aktivity	aktivita	k1gFnPc1	aktivita
JIC	JIC	kA	JIC
přispívají	přispívat	k5eAaImIp3nP	přispívat
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2	konkurenceschopnost
jihomoravských	jihomoravský	k2eAgFnPc2d1	Jihomoravská
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
následně	následně	k6eAd1	následně
nabízejí	nabízet	k5eAaImIp3nP	nabízet
kvalifikované	kvalifikovaný	k2eAgFnPc1d1	kvalifikovaná
a	a	k8xC	a
stabilní	stabilní	k2eAgFnPc1d1	stabilní
pracovní	pracovní	k2eAgFnPc1d1	pracovní
příležitosti	příležitost	k1gFnPc1	příležitost
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
jako	jako	k8xC	jako
zájmové	zájmový	k2eAgNnSc4d1	zájmové
sdružení	sdružení	k1gNnSc4	sdružení
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
sdružení	sdružení	k1gNnSc2	sdružení
jsou	být	k5eAaImIp3nP	být
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
a	a	k8xC	a
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
provozuje	provozovat	k5eAaImIp3nS	provozovat
dva	dva	k4xCgInPc4	dva
inkubátory	inkubátor	k1gInPc4	inkubátor
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Technologický	technologický	k2eAgInSc1d1	technologický
inkubátor	inkubátor	k1gInSc1	inkubátor
JIC	JIC	kA	JIC
INTECH	INTECH	kA	INTECH
a	a	k8xC	a
biotechnologický	biotechnologický	k2eAgInSc1d1	biotechnologický
inkubátor	inkubátor	k1gInSc1	inkubátor
JIC	JIC	kA	JIC
INBIT	INBIT	kA	INBIT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
JIC	JIC	kA	JIC
otevřel	otevřít	k5eAaPmAgMnS	otevřít
třetí	třetí	k4xOgInSc4	třetí
inkubátor	inkubátor	k1gInSc4	inkubátor
<g/>
,	,	kIx,	,
JIC	JIC	kA	JIC
INMEC	INMEC	kA	INMEC
<g/>
,	,	kIx,	,
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
na	na	k7c4	na
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
podporuje	podporovat	k5eAaImIp3nS	podporovat
inovační	inovační	k2eAgNnSc4d1	inovační
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
konkurenceschopnost	konkurenceschopnost	k1gFnSc4	konkurenceschopnost
jihomoravských	jihomoravský	k2eAgFnPc2d1	Jihomoravská
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
vytváření	vytváření	k1gNnSc4	vytváření
kvalifikovaných	kvalifikovaný	k2eAgNnPc2d1	kvalifikované
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
JIC	JIC	kA	JIC
využívají	využívat	k5eAaImIp3nP	využívat
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
dobrými	dobrý	k2eAgInPc7d1	dobrý
nápady	nápad	k1gInPc7	nápad
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
startupy	startup	k1gInPc4	startup
i	i	k8xC	i
zavedené	zavedený	k2eAgFnPc4d1	zavedená
technologické	technologický	k2eAgFnPc4d1	technologická
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
prošly	projít	k5eAaPmAgFnP	projít
programy	program	k1gInPc4	program
JIC	JIC	kA	JIC
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
400	[number]	k4	400
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
stovky	stovka	k1gFnPc4	stovka
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
navazujících	navazující	k2eAgFnPc6d1	navazující
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
13	[number]	k4	13
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
nastartoval	nastartovat	k5eAaPmAgInS	nastartovat
JIC	JIC	kA	JIC
téměř	téměř	k6eAd1	téměř
360	[number]	k4	360
spoluprací	spolupráce	k1gFnPc2	spolupráce
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
a	a	k8xC	a
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
nabízí	nabízet	k5eAaImIp3nS	nabízet
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
programech	program	k1gInPc6	program
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
ENTER	ENTER	kA	ENTER
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
inovativními	inovativní	k2eAgInPc7d1	inovativní
nápady	nápad	k1gInPc7	nápad
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
začít	začít	k5eAaPmF	začít
podnikat	podnikat	k5eAaImF	podnikat
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
STARCUBE	STARCUBE	kA	STARCUBE
První	první	k4xOgFnSc7	první
a	a	k8xC	a
nejdéle	dlouho	k6eAd3	dlouho
fungující	fungující	k2eAgInSc1d1	fungující
akcelerátor	akcelerátor	k1gInSc1	akcelerátor
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tříměsíční	tříměsíční	k2eAgInSc1d1	tříměsíční
program	program	k1gInSc1	program
plný	plný	k2eAgInSc1d1	plný
workshopů	workshop	k1gInPc2	workshop
a	a	k8xC	a
konzultací	konzultace	k1gFnPc2	konzultace
s	s	k7c7	s
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
podnikateli	podnikatel	k1gMnPc7	podnikatel
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
český	český	k2eAgInSc1d1	český
akcelerátor	akcelerátor	k1gInSc1	akcelerátor
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
<g/>
:	:	kIx,	:
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
72	[number]	k4	72
startupů	startup	k1gInPc2	startup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
přes	přes	k7c4	přes
100	[number]	k4	100
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
investici	investice	k1gFnSc4	investice
v	v	k7c6	v
souhrnné	souhrnný	k2eAgFnSc6d1	souhrnná
hodnotě	hodnota	k1gFnSc6	hodnota
více	hodně	k6eAd2	hodně
než	než	k8xS	než
122	[number]	k4	122
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšným	úspěšný	k2eAgFnPc3d1	úspěšná
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
akcelerátorem	akcelerátor	k1gInSc7	akcelerátor
prošly	projít	k5eAaPmAgFnP	projít
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
GINA	GINA	kA	GINA
<g/>
,	,	kIx,	,
Skypicker	Skypicker	k1gMnSc1	Skypicker
<g/>
,	,	kIx,	,
Reservio	Reservio	k1gMnSc1	Reservio
nebo	nebo	k8xC	nebo
OrganizeTube	OrganizeTub	k1gInSc5	OrganizeTub
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Akcelerátor	akcelerátor	k1gInSc1	akcelerátor
opakovaně	opakovaně	k6eAd1	opakovaně
finančně	finančně	k6eAd1	finančně
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
formou	forma	k1gFnSc7	forma
Startup	Startup	k1gInSc1	Startup
Voucherů	Voucher	k1gInPc2	Voucher
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
MASTER	master	k1gMnSc1	master
Intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
šestiměsíční	šestiměsíční	k2eAgInSc4d1	šestiměsíční
program	program	k1gInSc4	program
pro	pro	k7c4	pro
inovativní	inovativní	k2eAgFnPc4d1	inovativní
firmy	firma	k1gFnPc4	firma
mladší	mladý	k2eAgFnPc4d2	mladší
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
s	s	k7c7	s
ambicemi	ambice	k1gFnPc7	ambice
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
významným	významný	k2eAgMnSc7d1	významný
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
i	i	k8xC	i
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
PLATINN	PLATINN	kA	PLATINN
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
pro	pro	k7c4	pro
zavedené	zavedený	k2eAgFnPc4d1	zavedená
firmy	firma	k1gFnPc4	firma
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
metodiky	metodika	k1gFnSc2	metodika
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
společnostem	společnost	k1gFnPc3	společnost
identifikovat	identifikovat	k5eAaBmF	identifikovat
nové	nový	k2eAgFnPc4d1	nová
inovační	inovační	k2eAgFnPc4d1	inovační
příležitosti	příležitost	k1gFnPc4	příležitost
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
JIC	JIC	kA	JIC
nabízí	nabízet	k5eAaImIp3nS	nabízet
firmám	firma	k1gFnPc3	firma
i	i	k8xC	i
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
služby	služba	k1gFnSc2	služba
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
fázích	fáze	k1gFnPc6	fáze
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
propojování	propojování	k1gNnSc2	propojování
byznysu	byznys	k1gInSc2	byznys
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
grantové	grantový	k2eAgNnSc4d1	grantové
poradenství	poradenství	k1gNnSc4	poradenství
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
moderním	moderní	k2eAgFnPc3d1	moderní
technologiím	technologie	k1gFnPc3	technologie
a	a	k8xC	a
pořádání	pořádání	k1gNnSc4	pořádání
akcí	akce	k1gFnPc2	akce
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
networkingovou	tworkingový	k2eNgFnSc7d1	networkingová
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
pořádá	pořádat	k5eAaImIp3nS	pořádat
řadu	řada	k1gFnSc4	řada
akcí	akce	k1gFnPc2	akce
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
networking	networking	k1gInSc4	networking
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
JIC	JIC	kA	JIC
slouží	sloužit	k5eAaImIp3nS	sloužit
jak	jak	k6eAd1	jak
klientům	klient	k1gMnPc3	klient
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsou	být	k5eAaImIp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
zájemce	zájemce	k1gMnPc4	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
StartupClub	StartupClub	k1gInSc1	StartupClub
je	být	k5eAaImIp3nS	být
platforma	platforma	k1gFnSc1	platforma
pro	pro	k7c4	pro
setkávání	setkávání	k1gNnSc4	setkávání
začínajících	začínající	k2eAgMnPc2d1	začínající
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c6	o
podnikání	podnikání	k1gNnSc6	podnikání
teprve	teprve	k6eAd1	teprve
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
třetí	třetí	k4xOgFnSc4	třetí
středu	středa	k1gFnSc4	středa
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
pořádá	pořádat	k5eAaImIp3nS	pořádat
přednášku	přednáška	k1gFnSc4	přednáška
<g/>
,	,	kIx,	,
seminář	seminář	k1gInSc4	seminář
nebo	nebo	k8xC	nebo
panelovou	panelový	k2eAgFnSc4d1	panelová
diskuzi	diskuze	k1gFnSc4	diskuze
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
z	z	k7c2	z
marketingu	marketing	k1gInSc2	marketing
<g/>
,	,	kIx,	,
managementu	management	k1gInSc2	management
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
financí	finance	k1gFnPc2	finance
nebo	nebo	k8xC	nebo
inspirace	inspirace	k1gFnSc2	inspirace
a	a	k8xC	a
motivace	motivace	k1gFnSc2	motivace
<g/>
.	.	kIx.	.
</s>
<s>
Těšit	těšit	k5eAaImF	těšit
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
zejména	zejména	k9	zejména
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
tipy	tip	k1gInPc4	tip
a	a	k8xC	a
rady	rada	k1gFnPc4	rada
podložené	podložený	k2eAgFnPc4d1	podložená
reálnými	reálný	k2eAgNnPc7d1	reálné
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
případové	případový	k2eAgFnPc1d1	Případová
studie	studie	k1gFnPc1	studie
nebo	nebo	k8xC	nebo
příklady	příklad	k1gInPc1	příklad
z	z	k7c2	z
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
akce	akce	k1gFnPc4	akce
StartupClubu	StartupClub	k1gInSc2	StartupClub
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
setkání	setkání	k1gNnSc6	setkání
následuje	následovat	k5eAaImIp3nS	následovat
diskuze	diskuze	k1gFnSc1	diskuze
a	a	k8xC	a
neformální	formální	k2eNgInSc1d1	neformální
networking	networking	k1gInSc1	networking
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
120	[number]	k4	120
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgNnSc1d1	unikátní
formát	formát	k1gInSc4	formát
efektivního	efektivní	k2eAgInSc2d1	efektivní
networkingu	networking	k1gInSc2	networking
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podpořit	podpořit	k5eAaPmF	podpořit
nové	nový	k2eAgFnPc4d1	nová
spolupráce	spolupráce	k1gFnPc4	spolupráce
mezi	mezi	k7c7	mezi
technologickými	technologický	k2eAgFnPc7d1	technologická
firmami	firma	k1gFnPc7	firma
a	a	k8xC	a
výzkumným	výzkumný	k2eAgNnSc7d1	výzkumné
i	i	k8xC	i
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
potkávají	potkávat	k5eAaImIp3nP	potkávat
u	u	k7c2	u
kulatých	kulatý	k2eAgInPc2d1	kulatý
stolů	stol	k1gInPc2	stol
podle	podle	k7c2	podle
stanoveného	stanovený	k2eAgInSc2d1	stanovený
harmonogramu	harmonogram	k1gInSc2	harmonogram
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
zástupce	zástupce	k1gMnSc1	zástupce
firmy	firma	k1gFnSc2	firma
má	mít	k5eAaImIp3nS	mít
přesně	přesně	k6eAd1	přesně
120	[number]	k4	120
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
představení	představení	k1gNnSc4	představení
své	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
případných	případný	k2eAgFnPc2d1	případná
možností	možnost	k1gFnPc2	možnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
akci	akce	k1gFnSc6	akce
je	být	k5eAaImIp3nS	být
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
a	a	k8xC	a
povolen	povolit	k5eAaPmNgInS	povolit
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
zástupce	zástupce	k1gMnSc1	zástupce
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
Grill	Grill	k1gMnSc1	Grill
nabízí	nabízet	k5eAaImIp3nS	nabízet
rychlou	rychlý	k2eAgFnSc4d1	rychlá
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
a	a	k8xC	a
odhalení	odhalení	k1gNnSc4	odhalení
slabých	slabý	k2eAgFnPc2d1	slabá
a	a	k8xC	a
silných	silný	k2eAgFnPc2d1	silná
stránek	stránka	k1gFnPc2	stránka
začínajících	začínající	k2eAgInPc2d1	začínající
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Experti	expert	k1gMnPc1	expert
následně	následně	k6eAd1	následně
mohou	moct	k5eAaImIp3nP	moct
vhodným	vhodný	k2eAgMnPc3d1	vhodný
adeptům	adept	k1gMnPc3	adept
nabídnout	nabídnout	k5eAaPmF	nabídnout
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
programu	program	k1gInSc2	program
JIC	JIC	kA	JIC
ENTER	ENTER	kA	ENTER
<g/>
.	.	kIx.	.
</s>
<s>
Kreativní	kreativní	k2eAgFnSc7d1	kreativní
voucher	vouchra	k1gFnPc2	vouchra
je	být	k5eAaImIp3nS	být
jednorázová	jednorázový	k2eAgFnSc1d1	jednorázová
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
z	z	k7c2	z
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
produkt	produkt	k1gInSc4	produkt
nebo	nebo	k8xC	nebo
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
sektorech	sektor	k1gInPc6	sektor
Regionální	regionální	k2eAgFnSc2d1	regionální
inovační	inovační	k2eAgFnSc2d1	inovační
strategie	strategie	k1gFnSc2	strategie
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
hodnotového	hodnotový	k2eAgInSc2d1	hodnotový
řetězce	řetězec	k1gInSc2	řetězec
takové	takový	k3xDgFnSc2	takový
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Voucher	Vouchra	k1gFnPc2	Vouchra
mohou	moct	k5eAaImIp3nP	moct
firmy	firma	k1gFnPc1	firma
využít	využít	k5eAaPmF	využít
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
profesionály	profesionál	k1gMnPc7	profesionál
z	z	k7c2	z
kreativní	kreativní	k2eAgFnSc2d1	kreativní
galerie	galerie	k1gFnSc2	galerie
na	na	k7c6	na
webu	web	k1gInSc6	web
jic	jic	k?	jic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Voucher	Vouchra	k1gFnPc2	Vouchra
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
75	[number]	k4	75
%	%	kIx~	%
ceny	cena	k1gFnSc2	cena
zakázky	zakázka	k1gFnSc2	zakázka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
100	[number]	k4	100
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
celou	celý	k2eAgFnSc4d1	celá
zakázku	zakázka	k1gFnSc4	zakázka
a	a	k8xC	a
JIC	JIC	kA	JIC
jim	on	k3xPp3gInPc3	on
pak	pak	k6eAd1	pak
voucher	vouchra	k1gFnPc2	vouchra
zpětně	zpětně	k6eAd1	zpětně
proplatí	proplatit	k5eAaPmIp3nS	proplatit
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
zakázky	zakázka	k1gFnSc2	zakázka
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
000	[number]	k4	000
Kč	Kč	kA	Kč
bez	bez	k7c2	bez
DPH	DPH	kA	DPH
<g/>
.	.	kIx.	.
</s>
<s>
Kreativní	kreativní	k2eAgFnPc1d1	kreativní
vouchery	vouchera	k1gFnPc1	vouchera
Brno	Brno	k1gNnSc1	Brno
firmám	firma	k1gFnPc3	firma
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
jihomoravská	jihomoravský	k2eAgFnSc1d1	Jihomoravská
metropole	metropole	k1gFnSc1	metropole
jako	jako	k8xC	jako
první	první	k4xOgNnSc4	první
město	město	k1gNnSc4	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podnikatelům	podnikatel	k1gMnPc3	podnikatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začínali	začínat	k5eAaImAgMnP	začínat
v	v	k7c6	v
inkubátorech	inkubátor	k1gInPc6	inkubátor
JIC	JIC	kA	JIC
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Václav	Václav	k1gMnSc1	Václav
Muchna	Muchn	k1gInSc2	Muchn
<g/>
,	,	kIx,	,
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
firmy	firma	k1gFnSc2	firma
Y	Y	kA	Y
Soft	Soft	k?	Soft
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc4	titul
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
vyhlašované	vyhlašovaný	k2eAgFnSc6d1	vyhlašovaná
společností	společnost	k1gFnSc7	společnost
Ernst	Ernst	k1gMnSc1	Ernst
&	&	k?	&
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
úspěšnými	úspěšný	k2eAgFnPc7d1	úspěšná
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
JIC	JIC	kA	JIC
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Safetica	Safetica	k1gFnSc1	Safetica
Technologies	Technologies	k1gInSc4	Technologies
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	se	k3xPyFc4	se
oblastí	oblast	k1gFnSc7	oblast
Data	datum	k1gNnSc2	datum
Loss	Loss	k1gInSc4	Loss
Prevention	Prevention	k1gInSc1	Prevention
(	(	kIx(	(
<g/>
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
úniku	únik	k1gInSc3	únik
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Flowmon	Flowmon	k1gInSc1	Flowmon
Networks	Networks	k1gInSc1	Networks
<g/>
,	,	kIx,	,
lídr	lídr	k1gMnSc1	lídr
v	v	k7c6	v
monitorování	monitorování	k1gNnSc6	monitorování
síťového	síťový	k2eAgInSc2d1	síťový
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
analýzy	analýza	k1gFnSc2	analýza
chování	chování	k1gNnSc2	chování
sítě	síť	k1gFnSc2	síť
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
datových	datový	k2eAgInPc2d1	datový
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
Sewio	Sewio	k1gNnSc1	Sewio
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
vyvíjející	vyvíjející	k2eAgInSc4d1	vyvíjející
bezdrátový	bezdrátový	k2eAgInSc4d1	bezdrátový
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
lokalizaci	lokalizace	k1gFnSc4	lokalizace
a	a	k8xC	a
sledování	sledování	k1gNnSc4	sledování
pohybu	pohyb	k1gInSc2	pohyb
uvnitř	uvnitř	k7c2	uvnitř
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
nízkonákladových	nízkonákladový	k2eAgFnPc2d1	nízkonákladová
letenek	letenka	k1gFnPc2	letenka
Kiwi	kiwi	k1gNnSc2	kiwi
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Skypicker	Skypicker	k1gInSc1	Skypicker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
kole	kolo	k1gNnSc6	kolo
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
soutěže	soutěž	k1gFnSc2	soutěž
Evropské	evropský	k2eAgFnSc2d1	Evropská
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
podnikání	podnikání	k1gNnSc4	podnikání
a	a	k8xC	a
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
kole	kolo	k1gNnSc6	kolo
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
podnikatelského	podnikatelský	k2eAgNnSc2d1	podnikatelské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Jihomoravské	jihomoravský	k2eAgNnSc1d1	Jihomoravské
inovační	inovační	k2eAgNnSc1d1	inovační
centrum	centrum	k1gNnSc1	centrum
získalo	získat	k5eAaPmAgNnS	získat
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Incubator	Incubator	k1gMnSc1	Incubator
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mezinárodně	mezinárodně	k6eAd1	mezinárodně
zapojený	zapojený	k2eAgInSc4d1	zapojený
inkubátor	inkubátor	k1gInSc4	inkubátor
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
je	být	k5eAaImIp3nS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sítě	síť	k1gFnSc2	síť
Enterprise	Enterprise	k1gFnSc2	Enterprise
Europe	Europ	k1gInSc5	Europ
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
EEN	EEN	kA	EEN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
české	český	k2eAgFnPc4d1	Česká
BIC	BIC	kA	BIC
v	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
European	Europeana	k1gFnPc2	Europeana
Business	business	k1gInSc1	business
&	&	k?	&
Innovation	Innovation	k1gInSc1	Innovation
Centre	centr	k1gInSc5	centr
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
EBN	EBN	kA	EBN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dceřiné	dceřiný	k2eAgFnPc4d1	dceřiná
společnosti	společnost	k1gFnPc4	společnost
<g/>
:	:	kIx,	:
Intemac	Intemac	k1gInSc1	Intemac
Solution	Solution	k1gInSc1	Solution
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
výzkumné	výzkumný	k2eAgNnSc4d1	výzkumné
centrum	centrum	k1gNnSc4	centrum
INTEMAC	INTEMAC	kA	INTEMAC
<g/>
,	,	kIx,	,
a	a	k8xC	a
JIC	JIC	kA	JIC
VENTURES	VENTURES	kA	VENTURES
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
INTEMAC	INTEMAC	kA	INTEMAC
v	v	k7c6	v
Kuřimi	Kuři	k1gFnPc7	Kuři
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
firmám	firma	k1gFnPc3	firma
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnSc2d1	výrobní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
JIC	JIC	kA	JIC
VENTURES	VENTURES	kA	VENTURES
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
současných	současný	k2eAgMnPc2d1	současný
i	i	k8xC	i
bývalých	bývalý	k2eAgMnPc2d1	bývalý
účastníků	účastník	k1gMnPc2	účastník
programů	program	k1gInPc2	program
JIC	JIC	kA	JIC
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
investicemi	investice	k1gFnPc7	investice
v	v	k7c4	v
řádek	řádek	k1gInSc4	řádek
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
až	až	k6eAd1	až
jednotek	jednotka	k1gFnPc2	jednotka
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
podporuje	podporovat	k5eAaImIp3nS	podporovat
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
rozvoj	rozvoj	k1gInSc4	rozvoj
prověřených	prověřený	k2eAgFnPc2d1	prověřená
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
zhodnocené	zhodnocený	k2eAgInPc4d1	zhodnocený
podíly	podíl	k1gInPc4	podíl
následně	následně	k6eAd1	následně
plánuje	plánovat	k5eAaImIp3nS	plánovat
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
dalších	další	k2eAgMnPc2d1	další
klientů	klient	k1gMnPc2	klient
JIC	JIC	kA	JIC
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
firem	firma	k1gFnPc2	firma
investuje	investovat	k5eAaBmIp3nS	investovat
sama	sám	k3xTgMnSc4	sám
nebo	nebo	k8xC	nebo
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
investory	investor	k1gMnPc7	investor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
investice	investice	k1gFnPc1	investice
JIC	JIC	kA	JIC
VENTURES	VENTURES	kA	VENTURES
putovala	putovat	k5eAaImAgFnS	putovat
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
GINA	GINA	kA	GINA
Software	software	k1gInSc1	software
výměnnou	výměnný	k2eAgFnSc4d1	výměnná
za	za	k7c4	za
3	[number]	k4	3
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
<g/>
.	.	kIx.	.
</s>
<s>
GINA	GINA	kA	GINA
Software	software	k1gInSc1	software
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
mapový	mapový	k2eAgInSc1d1	mapový
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
navigaci	navigace	k1gFnSc4	navigace
v	v	k7c6	v
náročném	náročný	k2eAgInSc6d1	náročný
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
koordinaci	koordinace	k1gFnSc6	koordinace
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
efektivní	efektivní	k2eAgFnSc4d1	efektivní
výměnu	výměna	k1gFnSc4	výměna
geografických	geografický	k2eAgFnPc2d1	geografická
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
