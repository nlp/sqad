<s>
1861	[number]	k4	1861
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
Kinematoskopu	Kinematoskop	k1gInSc2	Kinematoskop
<g/>
,	,	kIx,	,
patentovaným	patentovaný	k2eAgMnSc7d1	patentovaný
Američanem	Američan	k1gMnSc7	Američan
Sellersem	Sellers	k1gMnSc7	Sellers
Colemanem	Coleman	k1gMnSc7	Coleman
<g/>
,	,	kIx,	,
zlepšující	zlepšující	k2eAgInPc1d1	zlepšující
rotující	rotující	k2eAgInPc1d1	rotující
lopatkové	lopatkový	k2eAgInPc1d1	lopatkový
stroje	stroj	k1gInPc1	stroj
zobrazující	zobrazující	k2eAgInPc1d1	zobrazující
(	(	kIx(	(
<g/>
ručně	ručně	k6eAd1	ručně
zalomenou	zalomený	k2eAgFnSc4d1	zalomená
<g/>
)	)	kIx)	)
sérii	série	k1gFnSc4	série
stereoskopických	stereoskopický	k2eAgInPc2d1	stereoskopický
snímků	snímek	k1gInPc2	snímek
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
namontovaný	namontovaný	k2eAgInSc1d1	namontovaný
na	na	k7c4	na
do	do	k7c2	do
bedny	bedna	k1gFnSc2	bedna
<g/>
.	.	kIx.	.
</s>
