<s>
Národní	národní	k2eAgInSc1d1	národní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
National	National	k1gMnSc1	National
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
and	and	k?	and
Space	Spaec	k1gInSc2	Spaec
Administration	Administration	k1gInSc1	Administration
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
vládní	vládní	k2eAgFnSc1d1	vládní
agentura	agentura	k1gFnSc1	agentura
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
americký	americký	k2eAgInSc4d1	americký
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
a	a	k8xC	a
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letectví	letectví	k1gNnSc2	letectví
zaštiťoval	zaštiťovat	k5eAaImAgInS	zaštiťovat
Národní	národní	k2eAgInSc1d1	národní
poradní	poradní	k2eAgInSc1d1	poradní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
NACA	NACA	kA	NACA
<g/>
,	,	kIx,	,
National	National	k1gFnPc7	National
Advisory	Advisor	k1gInPc7	Advisor
Committee	Committee	k1gFnSc3	Committee
for	forum	k1gNnPc2	forum
Aeronautics	Aeronautics	k1gInSc1	Aeronautics
<g/>
)	)	kIx)	)
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
NACA	NACA	kA	NACA
věnovala	věnovat	k5eAaImAgFnS	věnovat
také	také	k9	také
experimentům	experiment	k1gInPc3	experiment
s	s	k7c7	s
raketoplány	raketoplán	k1gInPc7	raketoplán
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
nadzvukový	nadzvukový	k2eAgInSc1d1	nadzvukový
Bell	bell	k1gInSc1	bell
X-	X-	k1gFnSc2	X-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
NACA	NACA	kA	NACA
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
(	(	kIx(	(
<g/>
vojenských	vojenský	k2eAgFnPc2d1	vojenská
<g/>
)	)	kIx)	)
institucí	instituce	k1gFnPc2	instituce
obrátila	obrátit	k5eAaPmAgFnS	obrátit
ke	k	k7c3	k
kosmickému	kosmický	k2eAgInSc3d1	kosmický
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
,	,	kIx,	,
především	především	k9	především
vypuštění	vypuštění	k1gNnSc4	vypuštění
první	první	k4xOgFnSc2	první
umělé	umělý	k2eAgFnSc2d1	umělá
družice	družice	k1gFnSc2	družice
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Vanguard	Vanguarda	k1gFnPc2	Vanguarda
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
se	se	k3xPyFc4	se
však	však	k9	však
zdržel	zdržet	k5eAaPmAgInS	zdržet
a	a	k8xC	a
náhradní	náhradní	k2eAgInSc4d1	náhradní
armádní	armádní	k2eAgInSc4d1	armádní
Explorer	Explorer	k1gInSc4	Explorer
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
předstižen	předstihnout	k5eAaPmNgInS	předstihnout
sovětským	sovětský	k2eAgInSc7d1	sovětský
Sputnikem	sputnik	k1gInSc7	sputnik
<g/>
.	.	kIx.	.
</s>
<s>
Šok	šok	k1gInSc1	šok
ze	z	k7c2	z
zpochybnění	zpochybnění	k1gNnSc2	zpochybnění
americké	americký	k2eAgFnSc2d1	americká
technologické	technologický	k2eAgFnSc2d1	technologická
nadřazenosti	nadřazenost	k1gFnSc2	nadřazenost
vedl	vést	k5eAaImAgInS	vést
americkou	americký	k2eAgFnSc4d1	americká
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
rozhodné	rozhodný	k2eAgFnSc3d1	rozhodná
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
podepsal	podepsat	k5eAaPmAgMnS	podepsat
"	"	kIx"	"
<g/>
National	National	k1gFnSc1	National
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
and	and	k?	and
Space	Space	k1gMnSc1	Space
Act	Act	k1gMnSc1	Act
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Národní	národní	k2eAgInSc4d1	národní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NASA	NASA	kA	NASA
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
veškerý	veškerý	k3xTgInSc4	veškerý
nevojenský	vojenský	k2eNgInSc4d1	nevojenský
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
vojenským	vojenský	k2eAgInPc3d1	vojenský
vesmírných	vesmírný	k2eAgMnPc2d1	vesmírný
prostředků	prostředek	k1gInPc2	prostředek
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
současně	současně	k6eAd1	současně
založená	založený	k2eAgFnSc1d1	založená
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
projekty	projekt	k1gInPc4	projekt
(	(	kIx(	(
<g/>
ARPA	ARPA	kA	ARPA
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
DARPA	DARPA	kA	DARPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
převzala	převzít	k5eAaPmAgFnS	převzít
veškeré	veškerý	k3xTgMnPc4	veškerý
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
objekty	objekt	k1gInPc7	objekt
NACA	NACA	kA	NACA
-	-	kIx~	-
čtyři	čtyři	k4xCgFnPc1	čtyři
laboratoře	laboratoř	k1gFnPc1	laboratoř
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
8000	[number]	k4	8000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
NASA	NASA	kA	NASA
přešel	přejít	k5eAaPmAgInS	přejít
rovněž	rovněž	k9	rovněž
personál	personál	k1gInSc1	personál
a	a	k8xC	a
prostředky	prostředek	k1gInPc1	prostředek
armádní	armádní	k2eAgInPc1d1	armádní
ABMA	ABMA	kA	ABMA
a	a	k8xC	a
části	část	k1gFnSc2	část
Námořní	námořní	k2eAgFnSc2d1	námořní
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
programy	program	k1gInPc1	program
NASA	NASA	kA	NASA
byly	být	k5eAaImAgInP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
letů	let	k1gInPc2	let
člověka	člověk	k1gMnSc2	člověk
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
zahájený	zahájený	k2eAgInSc1d1	zahájený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
hlavně	hlavně	k6eAd1	hlavně
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
přežít	přežít	k5eAaPmF	přežít
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
Alan	Alan	k1gMnSc1	Alan
Shepard	Shepard	k1gMnSc1	Shepard
balistický	balistický	k2eAgInSc4d1	balistický
skok	skok	k1gInSc4	skok
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
Mercury-Redstone	Mercury-Redston	k1gInSc5	Mercury-Redston
3	[number]	k4	3
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Glenn	Glenn	k1gMnSc1	Glenn
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
Američanem	Američan	k1gMnSc7	Američan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obletěl	obletět	k5eAaPmAgMnS	obletět
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
v	v	k7c6	v
kosmické	kosmický	k2eAgFnSc6d1	kosmická
lodi	loď	k1gFnSc6	loď
Mercury-Atlas	Mercury-Atlas	k1gMnSc1	Mercury-Atlas
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
program	program	k1gInSc1	program
Mercury	Mercura	k1gFnSc2	Mercura
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
jsou	být	k5eAaImIp3nP	být
uskutečnitelné	uskutečnitelný	k2eAgInPc1d1	uskutečnitelný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zahájen	zahájen	k2eAgInSc4d1	zahájen
program	program	k1gInSc4	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
původně	původně	k6eAd1	původně
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
dosažení	dosažení	k1gNnSc4	dosažení
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
cíl	cíl	k1gInSc1	cíl
byl	být	k5eAaImAgInS	být
předefinován	předefinovat	k5eAaPmNgInS	předefinovat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
měly	mít	k5eAaImAgInP	mít
dopravit	dopravit	k5eAaPmF	dopravit
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
bezpečně	bezpečně	k6eAd1	bezpečně
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc1	Apollo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
právě	právě	k9	právě
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
program	program	k1gInSc1	program
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
umožnit	umožnit	k5eAaPmF	umožnit
vyzkoušení	vyzkoušení	k1gNnSc4	vyzkoušení
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
nezbytných	zbytný	k2eNgInPc2d1	zbytný
pro	pro	k7c4	pro
měsíční	měsíční	k2eAgFnSc4d1	měsíční
mise	mise	k1gFnPc4	mise
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
řada	řada	k1gFnSc1	řada
zkušebních	zkušební	k2eAgMnPc2d1	zkušební
letů	let	k1gInPc2	let
a	a	k8xC	a
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
první	první	k4xOgMnPc4	první
američtí	americký	k2eAgMnPc1d1	americký
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
(	(	kIx(	(
<g/>
požár	požár	k1gInSc1	požár
Apolla	Apollo	k1gNnSc2	Apollo
1	[number]	k4	1
během	během	k7c2	během
tréninku	trénink	k1gInSc2	trénink
startu	start	k1gInSc2	start
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
cíl	cíl	k1gInSc4	cíl
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc4	Apollo
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
úkolu	úkol	k1gInSc2	úkol
dopravit	dopravit	k5eAaPmF	dopravit
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
ochabla	ochabnout	k5eAaPmAgFnS	ochabnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
předčasným	předčasný	k2eAgNnSc7d1	předčasné
ukončením	ukončení	k1gNnSc7	ukončení
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
misí	mise	k1gFnPc2	mise
Apollo	Apollo	k1gMnSc1	Apollo
-	-	kIx~	-
ačkoli	ačkoli	k8xS	ačkoli
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
ještě	ještě	k9	ještě
tři	tři	k4xCgInPc4	tři
další	další	k2eAgInPc4d1	další
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1972	[number]	k4	1972
letem	let	k1gInSc7	let
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
naposledy	naposledy	k6eAd1	naposledy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
dostal	dostat	k5eAaPmAgMnS	dostat
profesionální	profesionální	k2eAgMnSc1d1	profesionální
vědec	vědec	k1gMnSc1	vědec
-	-	kIx~	-
geolog	geolog	k1gMnSc1	geolog
Harrison	Harrison	k1gMnSc1	Harrison
Schmitt	Schmitt	k1gMnSc1	Schmitt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
předčasného	předčasný	k2eAgNnSc2d1	předčasné
ukončení	ukončení	k1gNnSc2	ukončení
programu	program	k1gInSc2	program
byly	být	k5eAaImAgInP	být
i	i	k9	i
rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
škrty	škrt	k1gInPc1	škrt
vyvolané	vyvolaný	k2eAgInPc1d1	vyvolaný
válkou	válka	k1gFnSc7	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
většina	většina	k1gFnSc1	většina
rozpočtu	rozpočet	k1gInSc2	rozpočet
NASA	NASA	kA	NASA
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
na	na	k7c4	na
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
vyslala	vyslat	k5eAaPmAgFnS	vyslat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
bezpilotních	bezpilotní	k2eAgFnPc2d1	bezpilotní
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
Mariner	Mariner	k1gInSc1	Mariner
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
lidmi	člověk	k1gMnPc7	člověk
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
sonda	sonda	k1gFnSc1	sonda
proletěl	proletět	k5eAaPmAgInS	proletět
kolem	kolem	k6eAd1	kolem
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
-	-	kIx~	-
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
Ranger	Rangra	k1gFnPc2	Rangra
<g/>
,	,	kIx,	,
Surveyor	Surveyora	k1gFnPc2	Surveyora
a	a	k8xC	a
Lunar	Lunara	k1gFnPc2	Lunara
Orbiter	Orbitrum	k1gNnPc2	Orbitrum
byly	být	k5eAaImAgFnP	být
klíčové	klíčový	k2eAgFnPc1d1	klíčová
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
před	před	k7c7	před
přistáním	přistání	k1gNnSc7	přistání
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
Program	program	k1gInSc1	program
Viking	Viking	k1gMnSc1	Viking
přistály	přistát	k5eAaPmAgFnP	přistát
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
a	a	k8xC	a
sondy	sonda	k1gFnPc4	sonda
Pioneer	Pioneer	kA	Pioneer
a	a	k8xC	a
Voyager	Voyagra	k1gFnPc2	Voyagra
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgInP	vydat
k	k	k7c3	k
vnějším	vnější	k2eAgFnPc3d1	vnější
planetám	planeta	k1gFnPc3	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vedle	vedle	k7c2	vedle
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
dat	datum	k1gNnPc2	datum
vyslaly	vyslat	k5eAaPmAgInP	vyslat
i	i	k9	i
barevné	barevný	k2eAgInPc1d1	barevný
snímky	snímek	k1gInPc1	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
NASA	NASA	kA	NASA
vypustila	vypustit	k5eAaPmAgFnS	vypustit
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
družicovou	družicový	k2eAgFnSc4d1	družicová
stanici	stanice	k1gFnSc4	stanice
-	-	kIx~	-
Skylab	Skylab	k1gInSc1	Skylab
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
upravený	upravený	k2eAgInSc1d1	upravený
modul	modul	k1gInSc1	modul
S-IVB	S-IVB	k1gMnSc2	S-IVB
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
normálně	normálně	k6eAd1	normálně
montován	montovat	k5eAaImNgMnS	montovat
buď	buď	k8xC	buď
jako	jako	k9	jako
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
nosiče	nosič	k1gMnSc4	nosič
Saturn	Saturn	k1gInSc4	Saturn
1B	[number]	k4	1B
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
nosiče	nosič	k1gMnSc4	nosič
Saturn	Saturn	k1gInSc4	Saturn
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
připojené	připojený	k2eAgFnSc2d1	připojená
lodi	loď	k1gFnSc2	loď
Apollo	Apollo	k1gNnSc4	Apollo
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
91	[number]	k4	91
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
až	až	k6eAd1	až
6,7	[number]	k4	6,7
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
objem	objem	k1gInSc1	objem
354	[number]	k4	354
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
tři	tři	k4xCgFnPc1	tři
tříčlenné	tříčlenný	k2eAgFnPc1d1	tříčlenná
posádky	posádka	k1gFnPc1	posádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
USA	USA	kA	USA
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
o	o	k7c6	o
dosažení	dosažení	k1gNnSc6	dosažení
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
mocnosti	mocnost	k1gFnPc1	mocnost
začaly	začít	k5eAaPmAgFnP	začít
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
Sojuz-Apollo	Sojuz-Apollo	k1gNnSc1	Sojuz-Apollo
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1975	[number]	k4	1975
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
loď	loď	k1gFnSc1	loď
Apollo	Apollo	k1gMnSc1	Apollo
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
sovětským	sovětský	k2eAgInSc7d1	sovětský
Sojuzem	Sojuz	k1gInSc7	Sojuz
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
znamenal	znamenat	k5eAaImAgInS	znamenat
zahájení	zahájení	k1gNnSc4	zahájení
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc4d1	mnohý
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
existují	existovat	k5eAaImIp3nP	existovat
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgNnSc1d1	hlavní
úsilí	úsilí	k1gNnSc1	úsilí
NASA	NASA	kA	NASA
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
4	[number]	k4	4
raketoplány	raketoplán	k1gInPc1	raketoplán
Space	Space	k1gMnSc1	Space
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
,	,	kIx,	,
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
jako	jako	k8xC	jako
opakovaně	opakovaně	k6eAd1	opakovaně
použitelný	použitelný	k2eAgInSc1d1	použitelný
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
však	však	k8xC	však
byly	být	k5eAaImAgFnP	být
podstatně	podstatně	k6eAd1	podstatně
dražší	drahý	k2eAgFnPc1d2	dražší
než	než	k8xS	než
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
plánovalo	plánovat	k5eAaImAgNnS	plánovat
a	a	k8xC	a
katastrofa	katastrofa	k1gFnSc1	katastrofa
Challengeru	Challenger	k1gInSc2	Challenger
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jejich	jejich	k3xOp3gFnPc4	jejich
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
vady	vada	k1gFnPc4	vada
<g/>
.	.	kIx.	.
</s>
<s>
Raketoplány	raketoplán	k1gInPc1	raketoplán
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaPmNgInP	využívat
i	i	k9	i
k	k	k7c3	k
čistě	čistě	k6eAd1	čistě
vojenským	vojenský	k2eAgInPc3d1	vojenský
letům	let	k1gInPc3	let
(	(	kIx(	(
<g/>
program	program	k1gInSc4	program
některých	některý	k3yIgFnPc2	některý
expedic	expedice	k1gFnPc2	expedice
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
tajný	tajný	k2eAgInSc1d1	tajný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vědecký	vědecký	k2eAgInSc1d1	vědecký
přínos	přínos	k1gInSc1	přínos
byl	být	k5eAaImAgInS	být
značný	značný	k2eAgInSc1d1	značný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vynesl	vynést	k5eAaPmAgInS	vynést
raketoplán	raketoplán	k1gInSc1	raketoplán
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
s	s	k7c7	s
ESA	eso	k1gNnPc1	eso
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
významnější	významný	k2eAgFnSc2d2	významnější
spolupráce	spolupráce	k1gFnSc2	spolupráce
obou	dva	k4xCgFnPc2	dva
organizací	organizace	k1gFnPc2	organizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
posádky	posádka	k1gFnPc1	posádka
raketoplánů	raketoplán	k1gInPc2	raketoplán
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
provedly	provést	k5eAaPmAgFnP	provést
i	i	k9	i
několik	několik	k4yIc4	několik
oprav	oprava	k1gFnPc2	oprava
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc4	vylepšení
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
při	při	k7c6	při
pěti	pět	k4xCc6	pět
servisních	servisní	k2eAgFnPc6d1	servisní
misích	mise	k1gFnPc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
NASA	NASA	kA	NASA
zahájila	zahájit	k5eAaPmAgFnS	zahájit
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
americké	americký	k2eAgFnSc6d1	americká
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
vypuštění	vypuštění	k1gNnSc4	vypuštění
se	se	k3xPyFc4	se
plánovalo	plánovat	k5eAaImAgNnS	plánovat
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
nazvána	nazvat	k5eAaBmNgFnS	nazvat
byla	být	k5eAaImAgFnS	být
Freedom	Freedom	k1gInSc4	Freedom
<g/>
.	.	kIx.	.
</s>
<s>
Průtahy	průtah	k1gInPc1	průtah
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
náklady	náklad	k1gInPc1	náklad
vedly	vést	k5eAaImAgInP	vést
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
k	k	k7c3	k
přizvání	přizvání	k1gNnSc3	přizvání
partnerů	partner	k1gMnPc2	partner
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
i	i	k8xC	i
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
podnik	podnik	k1gInSc1	podnik
ruského	ruský	k2eAgInSc2d1	ruský
Roskosmosu	Roskosmos	k1gInSc2	Roskosmos
<g/>
,	,	kIx,	,
americké	americký	k2eAgInPc1d1	americký
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgNnSc4d1	Evropské
ESA	eso	k1gNnPc4	eso
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgFnPc4d1	japonská
JAXA	JAXA	kA	JAXA
a	a	k8xC	a
kanadské	kanadský	k2eAgFnSc2d1	kanadská
CSA	CSA	kA	CSA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
raketoplán	raketoplán	k1gInSc1	raketoplán
poprvé	poprvé	k6eAd1	poprvé
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
ruské	ruský	k2eAgFnSc3d1	ruská
orbitální	orbitální	k2eAgFnSc3d1	orbitální
stanici	stanice	k1gFnSc3	stanice
Mir	mir	k1gInSc4	mir
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
pak	pak	k6eAd1	pak
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgInPc1	který
americké	americký	k2eAgInPc1d1	americký
raketoplány	raketoplán	k1gInPc1	raketoplán
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
řadu	řada	k1gFnSc4	řada
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
katastrofa	katastrofa	k1gFnSc1	katastrofa
potkala	potkat	k5eAaPmAgFnS	potkat
program	program	k1gInSc4	program
Space	Space	k1gFnSc1	Space
Shuttle	Shuttle	k1gFnSc1	Shuttle
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
flotily	flotila	k1gFnSc2	flotila
raketoplánů	raketoplán	k1gInPc2	raketoplán
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
nad	nad	k7c7	nad
Texasem	Texas	k1gInSc7	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vině	vina	k1gFnSc6	vina
bylo	být	k5eAaImAgNnS	být
poškození	poškození	k1gNnSc1	poškození
tepelné	tepelný	k2eAgFnSc2d1	tepelná
izolace	izolace	k1gFnSc2	izolace
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
izolační	izolační	k2eAgFnSc7d1	izolační
pěnou	pěna	k1gFnSc7	pěna
odpadlou	odpadlý	k2eAgFnSc4d1	odpadlá
při	pře	k1gFnSc4	pře
startu	start	k1gInSc2	start
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
lety	let	k1gInPc4	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
dočasně	dočasně	k6eAd1	dočasně
zastaveny	zastaven	k2eAgFnPc1d1	zastavena
<g/>
.	.	kIx.	.
</s>
<s>
Obnoveny	obnoven	k2eAgFnPc1d1	obnovena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
éra	éra	k1gFnSc1	éra
Shuttlů	Shuttl	k1gMnPc2	Shuttl
skončila	skončit	k5eAaPmAgFnS	skončit
misí	mise	k1gFnSc7	mise
STS-135	STS-135	k1gMnSc2	STS-135
raketoplánu	raketoplán	k1gInSc2	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
NASA	NASA	kA	NASA
čelila	čelit	k5eAaImAgFnS	čelit
ztenčujícímu	ztenčující	k2eAgInSc3d1	ztenčující
se	se	k3xPyFc4	se
rozpočtu	rozpočet	k1gInSc3	rozpočet
(	(	kIx(	(
<g/>
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
asi	asi	k9	asi
16	[number]	k4	16
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Devátý	devátý	k4xOgMnSc1	devátý
ředitel	ředitel	k1gMnSc1	ředitel
NASA	NASA	kA	NASA
Daniel	Daniel	k1gMnSc1	Daniel
S.	S.	kA	S.
Goldin	Goldin	k2eAgMnSc1d1	Goldin
proto	proto	k8xC	proto
prosadil	prosadit	k5eAaPmAgInS	prosadit
přístup	přístup	k1gInSc1	přístup
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
laciněji	lacino	k6eAd2	lacino
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
snížit	snížit	k5eAaPmF	snížit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
a	a	k8xC	a
přehodnocen	přehodnotit	k5eAaPmNgInS	přehodnotit
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
sond	sonda	k1gFnPc2	sonda
Mars	Mars	k1gInSc1	Mars
Climate	Climat	k1gInSc5	Climat
Orbiter	Orbiter	k1gMnSc1	Orbiter
a	a	k8xC	a
Mars	Mars	k1gMnSc1	Mars
Polar	Polar	k1gMnSc1	Polar
Lander	Lander	k1gMnSc1	Lander
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Přistání	přistání	k1gNnSc1	přistání
malého	malý	k2eAgNnSc2d1	malé
vozítka	vozítko	k1gNnSc2	vozítko
Sojourner	Sojourner	k1gMnSc1	Sojourner
mise	mise	k1gFnSc2	mise
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
si	se	k3xPyFc3	se
získalo	získat	k5eAaPmAgNnS	získat
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
zájem	zájem	k1gInSc1	zájem
si	se	k3xPyFc3	se
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
dosud	dosud	k6eAd1	dosud
fungující	fungující	k2eAgFnPc4d1	fungující
družice	družice	k1gFnPc4	družice
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgInS	globat
Surveyor	Surveyor	k1gInSc1	Surveyor
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
rudé	rudý	k2eAgFnSc2d1	rudá
planety	planeta	k1gFnSc2	planeta
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
dělá	dělat	k5eAaImIp3nS	dělat
společnost	společnost	k1gFnSc1	společnost
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
-	-	kIx~	-
Mars	Mars	k1gInSc1	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
stopy	stopa	k1gFnPc4	stopa
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
přistání	přistání	k1gNnSc6	přistání
dalšího	další	k2eAgNnSc2d1	další
vozítka	vozítko	k1gNnSc2	vozítko
-	-	kIx~	-
Spirit	Spirit	k1gInSc1	Spirit
-	-	kIx~	-
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
pronesl	pronést	k5eAaPmAgInS	pronést
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
svoji	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
budoucnosti	budoucnost	k1gFnSc2	budoucnost
americké	americký	k2eAgFnSc2d1	americká
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
po	po	k7c6	po
očekávaném	očekávaný	k2eAgNnSc6d1	očekávané
ukončení	ukončení	k1gNnSc6	ukončení
provozu	provoz	k1gInSc2	provoz
Shuttlů	Shuttl	k1gMnPc2	Shuttl
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
plánovaném	plánovaný	k2eAgInSc6d1	plánovaný
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
let	let	k1gInSc1	let
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
dostavění	dostavění	k1gNnSc1	dostavění
stanice	stanice	k1gFnSc2	stanice
ISS	ISS	kA	ISS
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
pracovat	pracovat	k5eAaImF	pracovat
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
lidé	člověk	k1gMnPc1	člověk
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tam	tam	k6eAd1	tam
stálou	stálý	k2eAgFnSc4d1	stálá
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
pokusná	pokusný	k2eAgFnSc1d1	pokusná
stanice	stanice	k1gFnSc1	stanice
i	i	k9	i
jako	jako	k8xC	jako
potenciální	potenciální	k2eAgFnSc1d1	potenciální
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
výpravy	výprava	k1gFnPc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Výhledově	výhledově	k6eAd1	výhledově
se	se	k3xPyFc4	se
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
let	let	k1gInSc1	let
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodi	loď	k1gFnPc4	loď
a	a	k8xC	a
nosné	nosný	k2eAgFnPc4d1	nosná
rakety	raketa	k1gFnPc4	raketa
potřebné	potřebný	k2eAgFnPc1d1	potřebná
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
vytyčených	vytyčený	k2eAgInPc2d1	vytyčený
cílů	cíl	k1gInPc2	cíl
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
v	v	k7c6	v
programu	program	k1gInSc6	program
Constellation	Constellation	k1gInSc1	Constellation
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Constellation	Constellation	k1gInSc1	Constellation
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
vývoj	vývoj	k1gInSc1	vývoj
nosných	nosný	k2eAgFnPc2d1	nosná
raket	raketa	k1gFnPc2	raketa
Ares	Ares	k1gMnSc1	Ares
I	i	k9	i
a	a	k8xC	a
Ares	Ares	k1gMnSc1	Ares
V	V	kA	V
s	s	k7c7	s
nosností	nosnost	k1gFnSc7	nosnost
cca	cca	kA	cca
25	[number]	k4	25
a	a	k8xC	a
188	[number]	k4	188
tun	tuna	k1gFnPc2	tuna
na	na	k7c4	na
LEO	Leo	k1gMnSc1	Leo
<g/>
;	;	kIx,	;
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Orion	orion	k1gInSc1	orion
určené	určený	k2eAgInPc1d1	určený
jak	jak	k8xC	jak
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
k	k	k7c3	k
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
výhledu	výhled	k1gInSc6	výhled
i	i	k8xC	i
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
<g/>
;	;	kIx,	;
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Altair	Altaira	k1gFnPc2	Altaira
a	a	k8xC	a
tahače	tahač	k1gInSc2	tahač
EDS	EDS	kA	EDS
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
dopravit	dopravit	k5eAaPmF	dopravit
sestavu	sestava	k1gFnSc4	sestava
Orion-Altair	Orion-Altaira	k1gFnPc2	Orion-Altaira
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
využívaly	využívat	k5eAaImAgFnP	využívat
technologie	technologie	k1gFnPc1	technologie
(	(	kIx(	(
<g/>
a	a	k8xC	a
dodavatelé	dodavatel	k1gMnPc1	dodavatel
<g/>
)	)	kIx)	)
programu	program	k1gInSc3	program
Space	Spaec	k1gInSc2	Spaec
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
testu	test	k1gInSc3	test
Aresu	Aresu	k?	Aresu
I	i	k9	i
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
několikaměsíčním	několikaměsíční	k2eAgNnSc6d1	několikaměsíční
rozvažování	rozvažování	k1gNnSc6	rozvažování
<g/>
,	,	kIx,	,
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
přílišné	přílišný	k2eAgInPc1d1	přílišný
finanční	finanční	k2eAgInPc1d1	finanční
náklady	náklad	k1gInPc1	náklad
a	a	k8xC	a
zpožďování	zpožďování	k1gNnSc1	zpožďování
vývoje	vývoj	k1gInSc2	vývoj
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2025	[number]	k4	2025
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
konat	konat	k5eAaImF	konat
první	první	k4xOgInSc4	první
let	let	k1gInSc4	let
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
těžký	těžký	k2eAgInSc4d1	těžký
nosič	nosič	k1gInSc4	nosič
Ares	Ares	k1gMnSc1	Ares
V	v	k7c4	v
zahájen	zahájen	k2eAgInSc4d1	zahájen
vývoj	vývoj	k1gInSc4	vývoj
rakety	raketa	k1gFnSc2	raketa
SLS	SLS	kA	SLS
(	(	kIx(	(
<g/>
Space	Space	k1gMnSc1	Space
Launch	Launch	k1gMnSc1	Launch
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
Shuttlů	Shuttl	k1gInPc2	Shuttl
<g/>
,	,	kIx,	,
s	s	k7c7	s
nosností	nosnost	k1gFnSc7	nosnost
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
129	[number]	k4	129
tun	tuna	k1gFnPc2	tuna
na	na	k7c4	na
LEO	Lea	k1gFnSc5	Lea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
padlo	padnout	k5eAaImAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
transformaci	transformace	k1gFnSc6	transformace
lodě	loď	k1gFnSc2	loď
Orion	orion	k1gInSc1	orion
v	v	k7c6	v
MPCV	MPCV	kA	MPCV
(	(	kIx(	(
<g/>
Multi-Purpose	Multi-Purposa	k1gFnSc3	Multi-Purposa
Crew	Crew	k1gFnSc2	Crew
Vehicle	Vehicle	k1gFnSc2	Vehicle
<g/>
,	,	kIx,	,
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
pilotovaná	pilotovaný	k2eAgFnSc1d1	pilotovaná
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
záchranná	záchranný	k2eAgFnSc1d1	záchranná
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
i	i	k9	i
letům	let	k1gInPc3	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
asteroidům	asteroid	k1gInPc3	asteroid
a	a	k8xC	a
výhledově	výhledově	k6eAd1	výhledově
k	k	k7c3	k
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc4d1	běžná
dopravu	doprava	k1gFnSc4	doprava
astronautů	astronaut	k1gMnPc2	astronaut
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vybavení	vybavení	k1gNnSc4	vybavení
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
NASA	NASA	kA	NASA
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
lodě	loď	k1gFnPc4	loď
vlastněné	vlastněný	k2eAgFnPc4d1	vlastněná
výrobci	výrobce	k1gMnPc7	výrobce
kosmické	kosmický	k2eAgFnSc2d1	kosmická
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vyvíjené	vyvíjený	k2eAgFnSc6d1	vyvíjená
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
podporou	podpora	k1gFnSc7	podpora
NASA	NASA	kA	NASA
v	v	k7c6	v
programu	program	k1gInSc6	program
COTS	COTS	kA	COTS
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Mercury	Mercura	k1gFnSc2	Mercura
Program	program	k1gInSc1	program
Gemini	Gemin	k2eAgMnPc1d1	Gemin
Program	program	k1gInSc1	program
Apollo	Apollo	k1gMnSc1	Apollo
Skylab	Skylab	k1gMnSc1	Skylab
Space	Space	k1gMnSc1	Space
Shuttle	Shuttle	k1gFnSc1	Shuttle
(	(	kIx(	(
<g/>
raketoplány	raketoplán	k1gInPc1	raketoplán
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
Výzkum	výzkum	k1gInSc1	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
Program	program	k1gInSc1	program
Ranger	Ranger	k1gInSc4	Ranger
<g/>
,	,	kIx,	,
všech	všecek	k3xTgFnPc2	všecek
devět	devět	k4xCc4	devět
misí	mise	k1gFnPc2	mise
Program	program	k1gInSc1	program
Surveyor	Surveyora	k1gFnPc2	Surveyora
<g/>
,	,	kIx,	,
všech	všecek	k3xTgFnPc2	všecek
sedm	sedm	k4xCc4	sedm
misí	mise	k1gFnPc2	mise
Program	program	k1gInSc1	program
Lunar	Lunar	k1gMnSc1	Lunar
Orbiter	Orbitra	k1gFnPc2	Orbitra
<g/>
,	,	kIx,	,
všech	všecek	k3xTgFnPc2	všecek
pět	pět	k4xCc4	pět
misí	mise	k1gFnPc2	mise
Sonda	sonda	k1gFnSc1	sonda
<g />
.	.	kIx.	.
</s>
<s>
Clementine	Clementin	k1gMnSc5	Clementin
Sonda	sonda	k1gFnSc1	sonda
Lunar	Lunar	k1gMnSc1	Lunar
Prospector	Prospector	k1gMnSc1	Prospector
Výzkum	výzkum	k1gInSc4	výzkum
Merkuru	Merkur	k1gInSc2	Merkur
Program	program	k1gInSc1	program
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
Sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
Výzkum	výzkum	k1gInSc4	výzkum
Venuše	Venuše	k1gFnSc2	Venuše
Program	program	k1gInSc1	program
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
Pioneer-Venus	Pioneer-Venus	k1gInSc1	Pioneer-Venus
1	[number]	k4	1
a	a	k8xC	a
Pioneer-Venus	Pioneer-Venus	k1gInSc4	Pioneer-Venus
2	[number]	k4	2
Sonda	sonda	k1gFnSc1	sonda
Magellan	Magellana	k1gFnPc2	Magellana
Výzkum	výzkum	k1gInSc1	výzkum
Marsu	Mars	k1gInSc2	Mars
Program	program	k1gInSc1	program
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Viking	Viking	k1gMnSc1	Viking
Mars	Mars	k1gMnSc1	Mars
Observer	Observer	k1gMnSc1	Observer
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
Mars	Mars	k1gMnSc1	Mars
Climate	Climat	k1gInSc5	Climat
Orbiter	Orbitra	k1gFnPc2	Orbitra
Mars	Mars	k1gInSc1	Mars
Polar	Polar	k1gMnSc1	Polar
Lander	Lander	k1gMnSc1	Lander
Mars	Mars	k1gMnSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gInSc4	Surveyor
2001	[number]	k4	2001
Mars	Mars	k1gInSc4	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
Mars	Mars	k1gInSc1	Mars
Exploration	Exploration	k1gInSc1	Exploration
Rover	rover	k1gMnSc1	rover
Mars	Mars	k1gMnSc1	Mars
Reconnaissance	Reconnaissance	k1gFnSc2	Reconnaissance
Orbiter	Orbiter	k1gInSc1	Orbiter
Phoenix	Phoenix	k1gInSc1	Phoenix
Mars	Mars	k1gInSc1	Mars
Science	Science	k1gFnSc1	Science
Laboratory	Laborator	k1gMnPc4	Laborator
Výzkum	výzkum	k1gInSc4	výzkum
Jupiteru	Jupiter	k1gInSc2	Jupiter
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc2	mise
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
a	a	k8xC	a
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
Galileo	Galilea	k1gFnSc5	Galilea
Juno	Juno	k1gFnPc4	Juno
Výzkum	výzkum	k1gInSc4	výzkum
Saturnu	Saturn	k1gInSc2	Saturn
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc2	mise
Pioneer	Pioneer	kA	Pioneer
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
Cassini-Huygens	Cassini-Huygens	k1gInSc1	Cassini-Huygens
Lety	let	k1gInPc4	let
k	k	k7c3	k
více	hodně	k6eAd2	hodně
planetám	planeta	k1gFnPc3	planeta
současně	současně	k6eAd1	současně
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc2	mise
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
-	-	kIx~	-
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gInSc1	Saturn
Program	program	k1gInSc1	program
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
-	-	kIx~	-
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Merkur	Merkur	k1gInSc1	Merkur
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
-	-	kIx~	-
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
Voyager	Voyager	k1gMnSc1	Voyager
2	[number]	k4	2
-	-	kIx~	-
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uran	Uran	k1gMnSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gMnSc1	Neptun
New	New	k1gFnSc2	New
Horizons	Horizons	k1gInSc1	Horizons
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Pluto	Pluto	k1gMnSc1	Pluto
a	a	k8xC	a
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
Výzkum	výzkum	k1gInSc1	výzkum
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
prostoru	prostor	k1gInSc2	prostor
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc1	mise
5-9	[number]	k4	5-9
Program	program	k1gInSc1	program
Explorer	Explorer	k1gInSc4	Explorer
,	,	kIx,	,
sondy	sonda	k1gFnPc1	sonda
35	[number]	k4	35
a	a	k8xC	a
49	[number]	k4	49
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gMnSc1	Space
1	[number]	k4	1
NEAR	NEAR	kA	NEAR
Shoemaker	Shoemaker	k1gMnSc1	Shoemaker
Stardust	Stardust	k1gMnSc1	Stardust
CRAF	CRAF	kA	CRAF
-	-	kIx~	-
zrušená	zrušený	k2eAgFnSc1d1	zrušená
Deep	Deep	k1gInSc1	Deep
Impact	Impact	k2eAgInSc4d1	Impact
OSIRIS-REx	OSIRIS-REx	k1gInSc4	OSIRIS-REx
Výzkum	výzkum	k1gInSc1	výzkum
Slunce	slunce	k1gNnSc2	slunce
SOHO	SOHO	kA	SOHO
Ulysses	Ulysses	k1gInSc1	Ulysses
SDO	SDO	kA	SDO
Vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
observatoře	observatoř	k1gFnSc2	observatoř
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
<g />
.	.	kIx.	.
</s>
<s>
dalekohled	dalekohled	k1gInSc1	dalekohled
Comptonova	Comptonův	k2eAgNnSc2d1	Comptonův
gama	gama	k1gNnSc2	gama
observatoř	observatoř	k1gFnSc1	observatoř
Rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
observatoř	observatoř	k1gFnSc1	observatoř
Chandra	chandra	k1gFnSc1	chandra
-	-	kIx~	-
rentgenový	rentgenový	k2eAgInSc1d1	rentgenový
dalekohled	dalekohled	k1gInSc1	dalekohled
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
-	-	kIx~	-
infračervený	infračervený	k2eAgInSc1d1	infračervený
dalekohled	dalekohled	k1gInSc1	dalekohled
COBE	COBE	kA	COBE
IRAS	IRAS	kA	IRAS
Vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
Jamese	Jamese	k1gFnSc1	Jamese
Webba	Webba	k1gFnSc1	Webba
-	-	kIx~	-
plánovaný	plánovaný	k2eAgMnSc1d1	plánovaný
nástupce	nástupce	k1gMnSc1	nástupce
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
zařízení	zařízení	k1gNnSc1	zařízení
NASA	NASA	kA	NASA
je	být	k5eAaImIp3nS	být
Kennedyho	Kennedy	k1gMnSc4	Kennedy
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
KSC	KSC	kA	KSC
<g/>
)	)	kIx)	)
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
kosmodrom	kosmodrom	k1gInSc1	kosmodrom
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
startují	startovat	k5eAaBmIp3nP	startovat
všechny	všechen	k3xTgFnPc4	všechen
americké	americký	k2eAgFnPc4d1	americká
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2011	[number]	k4	2011
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
přerušeny	přerušit	k5eAaPmNgInP	přerušit
<g/>
,	,	kIx,	,
z	z	k7c2	z
KSC	KSC	kA	KSC
nadále	nadále	k6eAd1	nadále
startují	startovat	k5eAaBmIp3nP	startovat
nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
s	s	k7c7	s
družicemi	družice	k1gFnPc7	družice
a	a	k8xC	a
sondami	sonda	k1gFnPc7	sonda
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
předními	přední	k2eAgInPc7d1	přední
centry	centr	k1gInPc7	centr
NASA	NASA	kA	NASA
jsou	být	k5eAaImIp3nP	být
Marshallovo	Marshallův	k2eAgNnSc1d1	Marshallovo
středisko	středisko	k1gNnSc1	středisko
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
(	(	kIx(	(
<g/>
MSFC	MSFC	kA	MSFC
<g/>
)	)	kIx)	)
v	v	k7c6	v
alabamském	alabamský	k2eAgInSc6d1	alabamský
Huntsville	Huntsvill	k1gInSc6	Huntsvill
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
raketa	raketa	k1gFnSc1	raketa
Saturn	Saturn	k1gInSc1	Saturn
5	[number]	k4	5
a	a	k8xC	a
orbitální	orbitální	k2eAgFnSc1d1	orbitální
stanice	stanice	k1gFnSc1	stanice
Skylab	Skylaba	k1gFnPc2	Skylaba
a	a	k8xC	a
Jet	jet	k2eAgInSc1d1	jet
Propulsion	Propulsion	k1gInSc1	Propulsion
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
JPL	JPL	kA	JPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
inženýři	inženýr	k1gMnPc1	inženýr
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
řadu	řada	k1gFnSc4	řada
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
první	první	k4xOgFnSc7	první
americkou	americký	k2eAgFnSc7d1	americká
družicí	družice	k1gFnSc7	družice
Explorer	Explorer	k1gMnSc1	Explorer
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Střediska	středisko	k1gNnPc1	středisko
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
NASA	NASA	kA	NASA
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
komunikační	komunikační	k2eAgFnSc4d1	komunikační
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
teleskopy	teleskop	k1gInPc1	teleskop
<g/>
;	;	kIx,	;
kosmodromy	kosmodrom	k1gInPc1	kosmodrom
a	a	k8xC	a
konstrukční	konstrukční	k2eAgNnPc1d1	konstrukční
střediska	středisko	k1gNnPc1	středisko
<g/>
;	;	kIx,	;
výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
zkušební	zkušební	k2eAgFnSc2d1	zkušební
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
administrativních	administrativní	k2eAgInPc2d1	administrativní
důvodů	důvod	k1gInPc2	důvod
některá	některý	k3yIgFnSc1	některý
slouží	sloužit	k5eAaImIp3nS	sloužit
více	hodně	k6eAd2	hodně
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Centrála	centrála	k1gFnSc1	centrála
NASA	NASA	kA	NASA
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Středisko	středisko	k1gNnSc1	středisko
sdílených	sdílený	k2eAgFnPc2d1	sdílená
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
Shared	Shared	k1gInSc1	Shared
Services	Services	k1gInSc1	Services
center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
Stennisova	Stennisův	k2eAgNnSc2d1	Stennisův
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
u	u	k7c2	u
zálivu	záliv	k1gInSc2	záliv
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Centry	centr	k1gInPc1	centr
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
NASA	NASA	kA	NASA
jsou	být	k5eAaImIp3nP	být
Amesovo	Amesův	k2eAgNnSc4d1	Amesovo
výzkumné	výzkumný	k2eAgNnSc4d1	výzkumné
středisko	středisko	k1gNnSc4	středisko
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Mountain	Mountain	k1gInSc1	Mountain
View	View	k1gFnPc1	View
<g/>
,	,	kIx,	,
Goddardův	Goddardův	k2eAgInSc1d1	Goddardův
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Goddardovo	Goddardův	k2eAgNnSc1d1	Goddardovo
středisko	středisko	k1gNnSc1	středisko
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
v	v	k7c6	v
marylandském	marylandský	k2eAgInSc6d1	marylandský
Greenbeltu	Greenbelt	k1gInSc6	Greenbelt
<g/>
,	,	kIx,	,
Jet	jet	k2eAgInSc1d1	jet
Propulsion	Propulsion	k1gInSc1	Propulsion
Laboratory	Laborator	k1gInPc1	Laborator
v	v	k7c6	v
kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
,	,	kIx,	,
Glennovo	Glennův	k2eAgNnSc1d1	Glennovo
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
středisko	středisko	k1gNnSc1	středisko
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
a	a	k8xC	a
Langleyho	Langley	k1gMnSc2	Langley
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
středisko	středisko	k1gNnSc1	středisko
v	v	k7c6	v
Hamptonu	Hampton	k1gInSc6	Hampton
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
-	-	kIx~	-
KSC	KSC	kA	KSC
-	-	kIx~	-
NASA	NASA	kA	NASA
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
startům	start	k1gInPc3	start
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
i	i	k8xC	i
Letové	letový	k2eAgNnSc4d1	letové
středisko	středisko	k1gNnSc4	středisko
Wallops	Wallopsa	k1gFnPc2	Wallopsa
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Wallops	Wallopsa	k1gFnPc2	Wallopsa
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčními	konstrukční	k2eAgNnPc7d1	konstrukční
a	a	k8xC	a
testovacími	testovací	k2eAgNnPc7d1	testovací
středisky	středisko	k1gNnPc7	středisko
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
Johnsonovo	Johnsonův	k2eAgNnSc4d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
středisko	středisko	k1gNnSc4	středisko
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
,	,	kIx,	,
Marshallovo	Marshallův	k2eAgNnSc1d1	Marshallovo
středisko	středisko	k1gNnSc1	středisko
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
Montážní	montážní	k2eAgNnSc1d1	montážní
středisko	středisko	k1gNnSc1	středisko
Michoud	Michouda	k1gFnPc2	Michouda
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleansu	Orleans	k1gInSc2	Orleans
<g/>
,	,	kIx,	,
Testovací	testovací	k2eAgNnSc1d1	testovací
středisko	středisko	k1gNnSc1	středisko
White	Whit	k1gInSc5	Whit
Sands	Sands	k1gInSc1	Sands
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Cruces	Crucesa	k1gFnPc2	Crucesa
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Drydenovo	Drydenův	k2eAgNnSc4d1	Drydenovo
letecké	letecký	k2eAgNnSc4d1	letecké
výzkumné	výzkumný	k2eAgNnSc4d1	výzkumné
středisko	středisko	k1gNnSc4	středisko
na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
Amesovo	Amesův	k2eAgNnSc4d1	Amesovo
<g/>
,	,	kIx,	,
Glennovo	Glennův	k2eAgNnSc4d1	Glennovo
a	a	k8xC	a
Langleyho	Langleyha	k1gFnSc5	Langleyha
výzkumná	výzkumný	k2eAgNnPc4d1	výzkumné
střediska	středisko	k1gNnPc4	středisko
<g/>
,	,	kIx,	,
Nezávislé	závislý	k2eNgFnPc4d1	nezávislá
ověřovací	ověřovací	k2eAgNnPc4d1	ověřovací
zařízení	zařízení	k1gNnPc4	zařízení
ve	v	k7c6	v
Fairmontu	Fairmont	k1gInSc6	Fairmont
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Virgínii	Virgínie	k1gFnSc6	Virgínie
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Stennisovo	Stennisův	k2eAgNnSc1d1	Stennisův
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
vesmírnými	vesmírný	k2eAgFnPc7d1	vesmírná
sondami	sonda	k1gFnPc7	sonda
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
komplexy	komplex	k1gInPc1	komplex
teleskopů	teleskop	k1gInPc2	teleskop
(	(	kIx(	(
<g/>
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc1	Space
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
Canberře	Canberra	k1gFnSc6	Canberra
<g/>
,	,	kIx,	,
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Barstow	Barstow	k1gMnPc6	Barstow
a	a	k8xC	a
španělském	španělský	k2eAgInSc6d1	španělský
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
teleskop	teleskop	k1gInSc1	teleskop
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
(	(	kIx(	(
<g/>
Infrared	Infrared	k1gInSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
Facility	Facilit	k2eAgInPc1d1	Facilit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
přes	přes	k7c4	přes
komunikační	komunikační	k2eAgFnPc4d1	komunikační
sítě	síť	k1gFnPc4	síť
Near	Near	k1gMnSc1	Near
Earth	Earth	k1gMnSc1	Earth
Network	network	k1gInSc1	network
a	a	k8xC	a
Space	Space	k1gFnSc1	Space
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
NASA	NASA	kA	NASA
stojí	stát	k5eAaImIp3nS	stát
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
administrator	administrator	k1gMnSc1	administrator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
poradcem	poradce	k1gMnSc7	poradce
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pro	pro	k7c4	pro
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
T.	T.	kA	T.
Keith	Keith	k1gMnSc1	Keith
Glennan	Glennan	k1gMnSc1	Glennan
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
E.	E.	kA	E.
Webb	Webb	k1gMnSc1	Webb
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Thomas	Thomas	k1gMnSc1	Thomas
O.	O.	kA	O.
Paine	Pain	k1gInSc5	Pain
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
C.	C.	kA	C.
Fletcher	Fletchra	k1gFnPc2	Fletchra
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
<g />
.	.	kIx.	.
</s>
<s>
Frosch	Frosch	k1gMnSc1	Frosch
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
James	James	k1gInSc1	James
M.	M.	kA	M.
Beggs	Beggs	k1gInSc1	Beggs
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
C.	C.	kA	C.
Fletcher	Fletchra	k1gFnPc2	Fletchra
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
H.	H.	kA	H.
Truly	trul	k1gInPc1	trul
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
S.	S.	kA	S.
Goldin	Goldina	k1gFnPc2	Goldina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Sean	Sean	k1gNnSc1	Sean
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
D.	D.	kA	D.
Griffin	Griffin	k1gMnSc1	Griffin
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
F.	F.	kA	F.
Bolden	Boldna	k1gFnPc2	Boldna
(	(	kIx(	(
<g/>
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
