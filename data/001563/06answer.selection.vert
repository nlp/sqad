<s>
Chandrasekhara	Chandrasekhar	k1gMnSc4	Chandrasekhar
Venkata	Venkat	k1gMnSc4	Venkat
Raman	Raman	k1gMnSc1	Raman
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1888	[number]	k4	1888
Tiruččiráppalli	Tiruččiráppalle	k1gFnSc4	Tiruččiráppalle
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
indický	indický	k2eAgMnSc1d1	indický
fyzik	fyzik	k1gMnSc1	fyzik
vyznamenaný	vyznamenaný	k2eAgMnSc1d1	vyznamenaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c7	za
výsledky	výsledek	k1gInPc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
rozptylu	rozptyl	k1gInSc2	rozptyl
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
za	za	k7c4	za
objev	objev	k1gInSc4	objev
tzv.	tzv.	kA	tzv.
Ramanova	Ramanův	k2eAgInSc2d1	Ramanův
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
