<s>
Chandrasekhara	Chandrasekhar	k1gMnSc4	Chandrasekhar
Venkata	Venkat	k1gMnSc4	Venkat
Raman	Raman	k1gMnSc1	Raman
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1888	[number]	k4	1888
Tiruččiráppalli	Tiruččiráppalle	k1gFnSc4	Tiruččiráppalle
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
indický	indický	k2eAgMnSc1d1	indický
fyzik	fyzik	k1gMnSc1	fyzik
vyznamenaný	vyznamenaný	k2eAgMnSc1d1	vyznamenaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c7	za
výsledky	výsledek	k1gInPc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
rozptylu	rozptyl	k1gInSc2	rozptyl
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
za	za	k7c4	za
objev	objev	k1gInSc4	objev
tzv.	tzv.	kA	tzv.
Ramanova	Ramanův	k2eAgInSc2d1	Ramanův
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgMnS	podařit
L.	L.	kA	L.
I.	I.	kA	I.
Mandelštamovi	Mandelštamův	k2eAgMnPc1d1	Mandelštamův
a	a	k8xC	a
G.	G.	kA	G.
S.	S.	kA	S.
Lansbergovi	Lansbergův	k2eAgMnPc1d1	Lansbergův
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
však	však	k9	však
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ramanův	Ramanův	k2eAgMnSc1d1	Ramanův
spoluobjevitel	spoluobjevitel	k1gMnSc1	spoluobjevitel
Krišnan	Krišnan	k1gMnSc1	Krišnan
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
nezískali	získat	k5eNaPmAgMnP	získat
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chandrasekhara	Chandrasekhara	k1gFnSc1	Chandrasekhara
Venkata	Venkat	k2eAgFnSc1d1	Venkat
Raman	Raman	k1gInSc4	Raman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
nobelprize	nobelprize	k1gFnSc1	nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
