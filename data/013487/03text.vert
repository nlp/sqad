<s>
Pozitivistická	pozitivistický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Auguste	August	k1gMnSc5
Comte	Comt	k1gMnSc5
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
církve	církev	k1gFnSc2
</s>
<s>
Pozitivistická	pozitivistický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
Náboženství	náboženství	k1gNnSc1
humanity	humanita	k1gFnSc2
(	(	kIx(
<g/>
Religion	religion	k1gInSc1
de	de	k7
l	l	kA
<g/>
'	'	kIx"
<g/>
Humanité	humanité	k2eAgFnSc2d1
<g/>
,	,	kIx,
Religiã	Religiã	k1gMnSc1
da	da	k7
Humanidade	Humanidad	k1gInSc5
<g/>
,	,	kIx,
Religion	religion	k1gInSc1
of	of	k7
Humanity	humanita	k1gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sekulární	sekulární	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc4
založil	založit	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
filozof	filozof	k1gMnSc1
a	a	k8xC
sociolog	sociolog	k1gMnSc1
Auguste	Auguste	k1gMnSc1
Comte	Comte	k1gMnSc1
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
které	který	k3yRgNnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
pozitivistické	pozitivistický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoupenci	stoupenec	k1gMnPc1
tohoto	tento	k3xDgNnSc2
náboženství	náboženství	k1gNnSc2
zřídili	zřídit	k5eAaPmAgMnP
„	„	k?
<g/>
Chrámy	chrám	k1gInPc7
humanity	humanita	k1gFnSc2
<g/>
“	“	k?
ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Auguste	August	k1gMnSc5
Comte	Comt	k1gMnSc5
založil	založit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
posílil	posílit	k5eAaPmAgInS
soudržnost	soudržnost	k1gFnSc4
pozitivistických	pozitivistický	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
jako	jako	k8xS,k8xC
alternativu	alternativa	k1gFnSc4
k	k	k7c3
rituálům	rituál	k1gInPc3
a	a	k8xC
liturgiím	liturgie	k1gFnPc3
klasických	klasický	k2eAgFnPc2d1
církevních	církevní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
pozitivistický	pozitivistický	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
stoupenci	stoupenec	k1gMnPc1
církve	církev	k1gFnSc2
používají	používat	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Církev	církev	k1gFnSc1
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
zůstala	zůstat	k5eAaPmAgFnS
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
vzestup	vzestup	k1gInSc4
její	její	k3xOp3gFnSc2
odnože	odnož	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Congreve	Congreev	k1gFnPc4
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
londýnské	londýnský	k2eAgFnSc2d1
pozitivistické	pozitivistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
spoluzakladatelem	spoluzakladatel	k1gMnSc7
této	tento	k3xDgFnSc2
církve	církev	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
založena	založit	k5eAaPmNgFnS
anglickým	anglický	k2eAgMnSc7d1
přistěhovalcem	přistěhovalec	k1gMnSc7
Henrym	Henry	k1gMnSc7
Edgerem	Edger	k1gInSc7
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
pozitivistické	pozitivistický	k2eAgFnSc3d1
víře	víra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1869	#num#	k4
byl	být	k5eAaImAgMnS
vedoucím	vedoucí	k1gMnSc7
obce	obec	k1gFnSc2
David	David	k1gMnSc1
Goodman	Goodman	k1gMnSc1
Croly	Crola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Croly	Crola	k1gFnSc2
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
vychován	vychovat	k5eAaPmNgMnS
v	v	k7c6
duchu	duch	k1gMnSc6
církve	církev	k1gFnSc2
a	a	k8xC
pokřtěn	pokřtěn	k2eAgMnSc1d1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
pozitivistické	pozitivistický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
odtrhlo	odtrhnout	k5eAaPmAgNnS
od	od	k7c2
anglické	anglický	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
v	v	k7c6
USA	USA	kA
sice	sice	k8xC
pokračovala	pokračovat	k5eAaImAgFnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
původním	původní	k2eAgInSc6d1
ateistickém	ateistický	k2eAgInSc6d1
modelu	model	k1gInSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
uvedla	uvést	k5eAaPmAgFnS
do	do	k7c2
liturgie	liturgie	k1gFnSc2
kázání	kázání	k1gNnSc2
<g/>
,	,	kIx,
čtení	čtení	k1gNnSc2
z	z	k7c2
knihy	kniha	k1gFnSc2
Izajáš	Izajáš	k1gFnSc2
a	a	k8xC
svátosti	svátost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
obec	obec	k1gFnSc1
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
významná	významný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
anglická	anglický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1881	#num#	k4
založili	založit	k5eAaPmAgMnP
v	v	k7c6
Riu	Riu	k1gFnSc6
de	de	k?
Janeiro	Janeiro	k1gNnSc1
Miguel	Miguel	k1gMnSc1
Lemos	Lemos	k1gMnSc1
a	a	k8xC
Raimundo	Raimunda	k1gFnSc5
Teixeira	Teixeira	k1gFnSc1
Mendes	Mendesa	k1gFnPc2
Brazilskou	brazilský	k2eAgFnSc4d1
pozitivistickou	pozitivistický	k2eAgFnSc4d1
církev	církev	k1gFnSc4
(	(	kIx(
<g/>
Iglesia	Iglesia	k1gFnSc1
Positivista	positivista	k1gMnSc1
del	del	k?
Brasil	Brasil	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
pozitivistický	pozitivistický	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
jediný	jediný	k2eAgInSc1d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Chrámy	chrám	k1gInPc1
humanity	humanita	k1gFnSc2
</s>
<s>
Temple	templ	k1gInSc5
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Humanité	Humanitý	k2eAgFnSc2d1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
,	,	kIx,
rue	rue	k?
Payenne	Payenn	k1gMnSc5
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Templo	Tempnout	k5eAaPmAgNnS
da	da	k?
Humanidade	Humanidad	k1gInSc5
<g/>
:	:	kIx,
Rua	Rua	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
Constant	Constant	k1gMnSc1
74	#num#	k4
<g/>
,	,	kIx,
Barrio	Barrio	k6eAd1
de	de	k?
la	la	k1gNnSc1
Gloria	Gloria	k1gFnSc1
<g/>
,	,	kIx,
Rio	Rio	k1gFnSc1
de	de	k?
Janeiro	Janeiro	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Capela	Capela	k1gFnSc1
Positivista	positivista	k1gMnSc1
<g/>
:	:	kIx,
Avenida	Avenida	k1gFnSc1
Joã	Joã	k6eAd1
Pessoa	Pesso	k1gInSc2
1058	#num#	k4
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Capela	Capela	k1gFnSc1
Positivista	positivista	k1gMnSc1
<g/>
:	:	kIx,
Rua	Rua	k1gMnSc1
Riachuelo	Riachuela	k1gFnSc5
90	#num#	k4
<g/>
,	,	kIx,
Curitiba	Curitiba	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
„	„	k?
</s>
<s>
Láska	láska	k1gFnSc1
jako	jako	k8xC,k8xS
princip	princip	k1gInSc1
<g/>
,	,	kIx,
řád	řád	k1gInSc1
jako	jako	k8xC,k8xS
základ	základ	k1gInSc1
a	a	k8xC
pokrok	pokrok	k1gInSc1
jako	jako	k8xS,k8xC
cíl	cíl	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
amour	amour	k1gMnSc1
pour	pour	k1gMnSc1
principe	princip	k1gInSc5
et	et	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
ordre	ordre	k1gInSc1
pour	pour	k1gInSc1
base	basa	k1gFnSc3
<g/>
;	;	kIx,
le	le	k?
progrè	progrè	k1gInSc1
pour	pour	k1gInSc1
but	but	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Auguste	August	k1gMnSc5
Comte	Comt	k1gMnSc5
<g/>
,	,	kIx,
motto	motto	k1gNnSc1
pozitivistické	pozitivistický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Religion	religion	k1gInSc1
of	of	k?
Humanity	humanita	k1gFnPc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Přehled	přehled	k1gInSc1
chrámů	chrám	k1gInPc2
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Životopis	životopis	k1gInSc1
Herberta	Herbert	k1gMnSc2
Crolyho	Croly	k1gMnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Auguste	August	k1gMnSc5
Comte	Comt	k1gMnSc5
</s>
<s>
Pozitivismus	pozitivismus	k1gInSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
sociologie	sociologie	k1gFnSc2
</s>
<s>
Maison	Maison	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Auguste	August	k1gMnSc5
Comte	Comt	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Informace	informace	k1gFnPc1
o	o	k7c6
pozitivistické	pozitivistický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
Stránka	stránka	k1gFnSc1
pozitivistické	pozitivistický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Olaf	Olaf	k1gInSc1
Simons	Simons	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Religion	religion	k1gInSc1
of	of	k?
Humanity	humanita	k1gFnPc1
(	(	kIx(
<g/>
a	a	k8xC
structured	structured	k1gInSc1
collection	collection	k1gInSc1
of	of	k?
transcripts	transcripts	k1gInSc1
from	from	k1gMnSc1
English	English	k1gMnSc1
translations	translations	k6eAd1
of	of	k?
Comte	Comt	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
major	major	k1gMnSc1
publications	publications	k6eAd1
on	on	k3xPp3gMnSc1
the	the	k?
topic	topice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Stránka	stránka	k1gFnSc1
pozitivistické	pozitivistický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
