<s>
Pozitivistická	pozitivistický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
Náboženství	náboženství	k1gNnSc1
humanity	humanita	k1gFnSc2
(	(	kIx(
<g/>
Religion	religion	k1gInSc1
de	de	k7
l	l	kA
<g/>
'	'	kIx"
<g/>
Humanité	humanité	k2eAgFnSc2d1
<g/>
,	,	kIx,
Religiã	Religiã	k1gMnSc1
da	da	k7
Humanidade	Humanidad	k1gInSc5
<g/>
,	,	kIx,
Religion	religion	k1gInSc1
of	of	k7
Humanity	humanita	k1gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sekulární	sekulární	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc4
založil	založit	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
filozof	filozof	k1gMnSc1
a	a	k8xC
sociolog	sociolog	k1gMnSc1
Auguste	Auguste	k1gMnSc1
Comte	Comte	k1gMnSc1
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
které	který	k3yRgNnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
pozitivistické	pozitivistický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>