<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
di	di	k?	di
ser	srát	k5eAaImRp2nS	srát
Piero	Piero	k1gNnSc1	Piero
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1452	[number]	k4	1452
Anchiano	Anchiana	k1gFnSc5	Anchiana
u	u	k7c2	u
Vinci	Vinca	k1gMnSc2	Vinca
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1519	[number]	k4	1519
Le	Le	k1gFnSc2	Le
Clos	Closa	k1gFnPc2	Closa
Lucé	Lucá	k1gFnSc2	Lucá
u	u	k7c2	u
Amboise	Amboise	k1gFnSc2	Amboise
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
čelní	čelní	k2eAgMnSc1d1	čelní
představitel	představitel	k1gMnSc1	představitel
renesanční	renesanční	k2eAgFnSc2d1	renesanční
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
autor	autor	k1gMnSc1	autor
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
obrazu	obraz	k1gInSc2	obraz
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
portrétu	portrét	k1gInSc2	portrét
zvaného	zvaný	k2eAgInSc2d1	zvaný
Mona	Mon	k1gInSc2	Mon
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
