<s>
Síra	síra	k1gFnSc1	síra
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
S	s	k7c7	s
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Sulphur	Sulphura	k1gFnPc2	Sulphura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc1d1	nekovový
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
