<s>
Hangul	Hangul	k1gInSc1
(	(	kIx(
<g/>
korejsky	korejsky	k6eAd1
한	한	k?
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
odborném	odborný	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
hangŭ	hangŭ	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
hangeul	hangeul	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
severokorejské	severokorejský	k2eAgFnSc6d1
terminologii	terminologie	k1gFnSc6
čosongul	čosongula	k1gFnPc2
–	–	k?
조	조	k?
<g/>
,	,	kIx,
čosŏ	čosŏ	k1gInSc1
<g/>
,	,	kIx,
joseongeul	joseongeul	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
korejské	korejský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
.	.	kIx.
</s>