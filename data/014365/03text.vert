<s>
Hangul	Hangul	k1gInSc1
</s>
<s>
Hangul	Hangul	k1gInSc1
Typ	typ	k1gInSc1
</s>
<s>
Abeceda	abeceda	k1gFnSc1
(	(	kIx(
<g/>
vizuálně	vizuálně	k6eAd1
reflektující	reflektující	k2eAgInPc4d1
fonologické	fonologický	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
)	)	kIx)
Mluvené	mluvený	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
</s>
<s>
Korejština	korejština	k1gFnSc1
Vytvořeno	vytvořit	k5eAaPmNgNnS
</s>
<s>
Sedžong	Sedžong	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
Časové	časový	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
1443	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Různé	různý	k2eAgInPc1d1
<g/>
,	,	kIx,
uměle	uměle	k6eAd1
zkonstruovánoHangul	zkonstruovánoHangout	k5eAaPmAgMnS
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
(	(	kIx(
<g/>
Čosongul	Čosongul	k1gInSc1
<g/>
)	)	kIx)
Rozsah	rozsah	k1gInSc1
unicode	unicod	k1gMnSc5
</s>
<s>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
AC	AC	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
D	D	kA
<g/>
7	#num#	k4
<g/>
AF	AF	kA
<g/>
,	,	kIx,
U	u	k7c2
<g/>
+	+	kIx~
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
11	#num#	k4
<g/>
FF	ff	kA
<g/>
,	,	kIx,
U	u	k7c2
<g/>
+	+	kIx~
<g/>
3130	#num#	k4
<g/>
–	–	k?
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
318	#num#	k4
<g/>
F	F	kA
<g/>
,	,	kIx,
U	u	k7c2
<g/>
+	+	kIx~
<g/>
3200	#num#	k4
<g/>
–	–	k?
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
32	#num#	k4
<g/>
FF	ff	kA
<g/>
,	,	kIx,
U	U	kA
<g/>
+	+	kIx~
<g/>
A	A	kA
<g/>
960	#num#	k4
<g/>
–	–	k?
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
A	a	k9
<g/>
97	#num#	k4
<g/>
F	F	kA
<g/>
,	,	kIx,
U	U	kA
<g/>
+	+	kIx~
<g/>
D	D	kA
<g/>
7	#num#	k4
<g/>
B	B	kA
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
D	D	kA
<g/>
7	#num#	k4
<g/>
FF	ff	kA
<g/>
,	,	kIx,
U	U	kA
<g/>
+	+	kIx~
<g/>
FF	ff	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
FFEF	FFEF	kA
ISO	ISO	kA
15924	#num#	k4
</s>
<s>
Hang	Hang	k1gInSc1
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Tato	tento	k3xDgFnSc1
stránka	stránka	k1gFnSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
fonetické	fonetický	k2eAgInPc4d1
symboly	symbol	k1gInPc4
IPA	IPA	kA
v	v	k7c6
kódování	kódování	k1gNnSc6
Unicode	Unicod	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
v	v	k7c6
původním	původní	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
hangul	hangula	k1gFnPc2
<g/>
:	:	kIx,
Sedžong	Sedžong	k1gInSc1
Veliký	veliký	k2eAgInSc1d1
</s>
<s>
Hangul	Hangul	k1gInSc1
(	(	kIx(
<g/>
korejsky	korejsky	k6eAd1
한	한	k?
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
odborném	odborný	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
hangŭ	hangŭ	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
hangeul	hangeul	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
severokorejské	severokorejský	k2eAgFnSc6d1
terminologii	terminologie	k1gFnSc6
čosongul	čosongula	k1gFnPc2
–	–	k?
조	조	k?
<g/>
,	,	kIx,
čosŏ	čosŏ	k1gInSc1
<g/>
,	,	kIx,
joseongeul	joseongeul	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
korejské	korejský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
v	v	k7c4
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
naučit	naučit	k5eAaPmF
za	za	k7c4
jediné	jediný	k2eAgNnSc4d1
dopoledne	dopoledne	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k9
jedním	jeden	k4xCgNnSc7
z	z	k7c2
velmi	velmi	k6eAd1
mála	málo	k4c2
skutečně	skutečně	k6eAd1
užívaných	užívaný	k2eAgNnPc2d1
písem	písmo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nevznikla	vzniknout	k5eNaPmAgFnS
odvozením	odvození	k1gNnSc7
(	(	kIx(
<g/>
nápodobou	nápodoba	k1gFnSc7
a	a	k8xC
úpravou	úprava	k1gFnSc7
<g/>
)	)	kIx)
z	z	k7c2
jejich	jejich	k3xOp3gMnPc2
předchůdců	předchůdce	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
od	od	k7c2
hieroglyfů	hieroglyf	k1gInPc2
po	po	k7c4
současná	současný	k2eAgNnPc4d1
písma	písmo	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
latinka	latinka	k1gFnSc1
<g/>
,	,	kIx,
arabské	arabský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
nebo	nebo	k8xC
dévanágarí	dévanágarí	k1gNnSc1
nebo	nebo	k8xC
čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
korejských	korejský	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
téměř	téměř	k6eAd1
shodně	shodně	k6eAd1
<g/>
,	,	kIx,
jen	jen	k9
s	s	k7c7
malými	malý	k2eAgFnPc7d1
odchylkami	odchylka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
abeceda	abeceda	k1gFnSc1
jamo	jamo	k6eAd1
a	a	k8xC
metoda	metoda	k1gFnSc1
sestavování	sestavování	k1gNnSc2
slabičných	slabičný	k2eAgInPc2d1
znaků	znak	k1gInPc2
z	z	k7c2
těchto	tento	k3xDgNnPc2
písmen	písmeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Před	před	k7c7
vytvořením	vytvoření	k1gNnSc7
hangulu	hangul	k1gInSc2
korejština	korejština	k1gFnSc1
neměla	mít	k5eNaImAgFnS
vlastní	vlastní	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělaní	vzdělaný	k2eAgMnPc1d1
Korejci	Korejec	k1gMnPc1
se	se	k3xPyFc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
jazyce	jazyk	k1gInSc6
vyjadřovali	vyjadřovat	k5eAaImAgMnP
pomocí	pomocí	k7c2
čínských	čínský	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
podmíněno	podmínit	k5eAaPmNgNnS
jejich	jejich	k3xOp3gNnSc7
dlouhým	dlouhý	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tisíciletí	tisíciletí	k1gNnSc2
se	se	k3xPyFc4
uskutečnily	uskutečnit	k5eAaPmAgInP
pokusy	pokus	k1gInPc1
vymyslet	vymyslet	k5eAaPmF
vhodnější	vhodný	k2eAgNnSc4d2
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
všechna	všechen	k3xTgFnSc1
tato	tento	k3xDgNnPc4
písma	písmo	k1gNnPc4
však	však	k9
vycházela	vycházet	k5eAaImAgFnS
z	z	k7c2
čínských	čínský	k2eAgInPc2d1
znaků	znak	k1gInPc2
a	a	k8xC
mohli	moct	k5eAaImAgMnP
je	on	k3xPp3gInPc4
číst	číst	k5eAaImF
a	a	k8xC
psát	psát	k5eAaImF
jimi	on	k3xPp3gMnPc7
pouze	pouze	k6eAd1
vzdělaní	vzdělaný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
si	se	k3xPyFc3
král	král	k1gMnSc1
Sedžong	Sedžong	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
začal	začít	k5eAaPmAgMnS
uvědomovat	uvědomovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnPc1
poddaní	poddaný	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
neumějí	umět	k5eNaImIp3nP
číst	číst	k5eAaImF
ani	ani	k8xC
psát	psát	k5eAaImF
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
pocit	pocit	k1gInSc4
křivdy	křivda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
nemohli	moct	k5eNaImAgMnP
předkládat	předkládat	k5eAaImF
své	svůj	k3xOyFgFnPc4
stížnosti	stížnost	k1gFnPc4
úřadům	úřad	k1gInPc3
jinak	jinak	k6eAd1
než	než	k8xS
ústně	ústně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krále	Král	k1gMnSc2
Sedžonga	Sedžong	k1gMnSc2
<g/>
,	,	kIx,
o	o	k7c6
němž	jenž	k3xRgMnSc6
bylo	být	k5eAaImAgNnS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
vždy	vždy	k6eAd1
ochotně	ochotně	k6eAd1
naslouchá	naslouchat	k5eAaImIp3nS
prostým	prostý	k2eAgInSc7d1
lidem	lid	k1gInSc7
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
znepokojoval	znepokojovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
zasadil	zasadit	k5eAaPmAgInS
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
abecedy	abeceda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
odpovídat	odpovídat	k5eAaImF
mluvené	mluvený	k2eAgFnSc3d1
korejštině	korejština	k1gFnSc3
a	a	k8xC
zároveň	zároveň	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
snadné	snadný	k2eAgNnSc1d1
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
naučit	naučit	k5eAaPmF
a	a	k8xC
používat	používat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokončení	dokončení	k1gNnSc1
tohoto	tento	k3xDgInSc2
projektu	projekt	k1gInSc2
bylo	být	k5eAaImAgNnS
zveřejněno	zveřejnit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1446	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokument	dokument	k1gInSc4
popisující	popisující	k2eAgNnSc4d1
nové	nový	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
byl	být	k5eAaImAgInS
nazván	nazván	k2eAgInSc1d1
hunmindžongum	hunmindžongum	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
systém	systém	k1gInSc1
správných	správný	k2eAgFnPc2d1
hlásek	hláska	k1gFnPc2
pro	pro	k7c4
vzdělání	vzdělání	k1gNnSc4
lidu	lid	k1gInSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úvodu	úvod	k1gInSc6
svého	svůj	k3xOyFgNnSc2
prohlášení	prohlášení	k1gNnSc2
král	král	k1gMnSc1
Sedžong	Sedžong	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Řeč	řeč	k1gFnSc1
naší	náš	k3xOp1gFnSc2
země	zem	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
čínštiny	čínština	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
ji	on	k3xPp3gFnSc4
nelze	lze	k6eNd1
čínsky	čínsky	k6eAd1
dobře	dobře	k6eAd1
zapsat	zapsat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
i	i	k8xC
kdyby	kdyby	kYmCp3nS
si	se	k3xPyFc3
některý	některý	k3yIgMnSc1
z	z	k7c2
našich	náš	k3xOp1gMnPc2
nevzdělaných	vzdělaný	k2eNgMnPc2d1
poddaných	poddaný	k1gMnPc2
přál	přát	k5eAaImAgMnS
vyjádřit	vyjádřit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
myšlenky	myšlenka	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
toho	ten	k3xDgNnSc2
nakonec	nakonec	k6eAd1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
můj	můj	k3xOp1gInSc1
lide	lid	k1gInSc5
<g/>
,	,	kIx,
jsme	být	k5eAaImIp1nP
právě	právě	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
písmo	písmo	k1gNnSc4
s	s	k7c7
dvaceti	dvacet	k4xCc7
osmi	osm	k4xCc7
písmeny	písmeno	k1gNnPc7
<g/>
,	,	kIx,
takže	takže	k8xS
bude	být	k5eAaImBp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
jej	on	k3xPp3gNnSc4
každý	každý	k3xTgMnSc1
lehce	lehko	k6eAd1
osvojil	osvojit	k5eAaPmAgMnS
a	a	k8xC
využil	využít	k5eAaPmAgMnS
ku	k	k7c3
prospěchu	prospěch	k1gInSc3
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Vzdělanci	vzdělanec	k1gMnPc1
však	však	k9
dlouho	dlouho	k6eAd1
nechtěli	chtít	k5eNaImAgMnP
hangul	hangul	k1gInSc4
přijmout	přijmout	k5eAaPmF
právě	právě	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
tak	tak	k9
velice	velice	k6eAd1
snadné	snadný	k2eAgNnSc1d1
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
naučit	naučit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posměšně	posměšně	k6eAd1
mu	on	k3xPp3gMnSc3
říkali	říkat	k5eAaImAgMnP
amgul	amgul	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
ženská	ženský	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opovrhovali	opovrhovat	k5eAaImAgMnP
písmem	písmo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
naučit	naučit	k5eAaPmF
i	i	k9
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
jinak	jinak	k6eAd1
číst	číst	k5eAaImF
ve	v	k7c6
škole	škola	k1gFnSc6
neučily	učit	k5eNaImAgFnP,k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tak	tak	k6eAd1
uplynula	uplynout	k5eAaPmAgFnS
celá	celý	k2eAgNnPc4d1
čtyři	čtyři	k4xCgNnPc4
staletí	staletí	k1gNnPc4
<g/>
,	,	kIx,
než	než	k8xS
korejská	korejský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
veřejně	veřejně	k6eAd1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
hangul	hangul	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
používán	používat	k5eAaImNgInS
v	v	k7c6
úředních	úřední	k2eAgInPc6d1
dokladech	doklad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Slabičné	slabičný	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
Korejská	korejský	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
jsou	být	k5eAaImIp3nP
sestavena	sestavit	k5eAaPmNgNnP
z	z	k7c2
jedné	jeden	k4xCgFnSc2
nebo	nebo	k8xC
více	hodně	k6eAd2
slabik	slabika	k1gFnPc2
a	a	k8xC
po	po	k7c6
slabikách	slabika	k1gFnPc6
se	se	k3xPyFc4
také	také	k9
korejština	korejština	k1gFnSc1
zapisuje	zapisovat	k5eAaImIp3nS
do	do	k7c2
pomyslných	pomyslný	k2eAgInPc2d1
přibližně	přibližně	k6eAd1
stejných	stejný	k2eAgInPc2d1
čtverečků	čtvereček	k1gInPc2
<g/>
,	,	kIx,
obdobně	obdobně	k6eAd1
jako	jako	k8xS,k8xC
čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
korejské	korejský	k2eAgFnPc1d1
slabiky	slabika	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
ze	z	k7c2
dvou	dva	k4xCgInPc2
nebo	nebo	k8xC
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
–	–	k?
písmen	písmeno	k1gNnPc2
abecedy	abeceda	k1gFnSc2
jamo	jamo	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
první	první	k4xOgFnSc1
souhláska	souhláska	k1gFnSc1
<g/>
:	:	kIx,
písmeno	písmeno	k1gNnSc1
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
případně	případně	k6eAd1
vlevo	vlevo	k6eAd1
nahoru	nahoru	k6eAd1
</s>
<s>
prostřední	prostřední	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
nebo	nebo	k8xC
více	hodně	k6eAd2
samohlásek	samohláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
prostřední	prostřední	k2eAgFnSc1d1
samohláska	samohláska	k1gFnSc1
tvar	tvar	k1gInSc1
svislé	svislý	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
<g/>
,	,	kIx,
píše	psát	k5eAaImIp3nS
se	se	k3xPyFc4
vpravo	vpravo	k6eAd1
od	od	k7c2
první	první	k4xOgFnSc2
hlásky	hláska	k1gFnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
samohlásky	samohláska	k1gFnPc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
vodorovných	vodorovný	k2eAgFnPc2d1
čar	čára	k1gFnPc2
se	se	k3xPyFc4
píšou	psát	k5eAaImIp3nP
pod	pod	k7c4
první	první	k4xOgFnSc4
hlásku	hláska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlásková	hláskový	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
jamo	jamo	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k9
pro	pro	k7c4
zdůraznění	zdůraznění	k1gNnSc4
zdvojena	zdvojen	k2eAgFnSc1d1
a	a	k8xC
písmena	písmeno	k1gNnPc1
složených	složený	k2eAgFnPc2d1
samohlásek	samohláska	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
stlačit	stlačit	k5eAaPmF
k	k	k7c3
sobě	se	k3xPyFc3
a	a	k8xC
napsat	napsat	k5eAaBmF,k5eAaPmF
tak	tak	k9
jednu	jeden	k4xCgFnSc4
vedle	vedle	k7c2
druhé	druhý	k4xOgFnSc2
<g/>
.	.	kIx.
</s>
<s>
obvykle	obvykle	k6eAd1
také	také	k9
z	z	k7c2
písmena	písmeno	k1gNnSc2
koncové	koncový	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
<g/>
:	:	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
na	na	k7c6
konci	konec	k1gInSc6
slabiky	slabika	k1gFnSc2
souhláska	souhláska	k1gFnSc1
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
dole	dole	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
je	být	k5eAaImIp3nS
v	v	k7c6
hangulu	hangulum	k1gNnSc6
možné	možný	k2eAgNnSc1d1
zapsat	zapsat	k5eAaPmF
tisíce	tisíc	k4xCgInPc4
různých	různý	k2eAgFnPc2d1
slabik	slabika	k1gFnPc2
a	a	k8xC
při	při	k7c6
čtení	čtení	k1gNnSc6
se	se	k3xPyFc4
písmena	písmeno	k1gNnSc2
jamo	jamo	k6eAd1
ve	v	k7c6
znaku	znak	k1gInSc6
čtou	číst	k5eAaImIp3nP
zleva	zleva	k6eAd1
nahoře	nahoře	k6eAd1
směrem	směr	k1gInSc7
doprava	doprava	k6eAd1
dolů	dol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přepis	přepis	k1gInSc1
korejštiny	korejština	k1gFnSc2
</s>
<s>
Samohlásky	samohláska	k1gFnPc1
</s>
<s>
Korejština	korejština	k1gFnSc1
má	mít	k5eAaImIp3nS
celkem	celkem	k6eAd1
21	#num#	k4
samohlásek	samohláska	k1gFnPc2
–	–	k?
10	#num#	k4
základních	základní	k2eAgFnPc2d1
samohlásek	samohláska	k1gFnPc2
a	a	k8xC
11	#num#	k4
jejich	jejich	k3xOp3gFnPc2
kombinací	kombinace	k1gFnPc2
viz	vidět	k5eAaImRp2nS
tabulka	tabulka	k1gFnSc1
níže	nízce	k6eAd2
dle	dle	k7c2
české	český	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
transkripce	transkripce	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ㅏ	ㅏ	k?
a	a	k8xC
</s>
<s>
ㅑ	ㅑ	k?
ja	ja	k?
</s>
<s>
ㅐ	ㅐ	k?
ä	ä	k?
</s>
<s>
ㅒ	ㅒ	k?
jä	jä	k?
</s>
<s>
ㅘ	ㅘ	k?
wa	wa	k?
</s>
<s>
ㅙ	ㅙ	k?
wä	wä	k?
</s>
<s>
ㅓ	ㅓ	k?
ŏ	ŏ	k?
</s>
<s>
ㅕ	ㅕ	k?
jŏ	jŏ	k?
</s>
<s>
ㅔ	ㅔ	k?
e	e	k0
</s>
<s>
ㅖ	ㅖ	k?
je	být	k5eAaImIp3nS
</s>
<s>
ㅞ	ㅞ	k?
we	we	k?
</s>
<s>
ㅗ	ㅗ	k?
o	o	k7c4
</s>
<s>
ㅛ	ㅛ	k?
jo	jo	k9
</s>
<s>
ㅚ	ㅚ	k?
ö	ö	k?
</s>
<s>
ㅝ	ㅝ	k?
wŏ	wŏ	k?
</s>
<s>
ㅜ	ㅜ	k?
u	u	k7c2
</s>
<s>
ㅠ	ㅠ	k?
ju	ju	k0
</s>
<s>
ㅟ	ㅟ	k?
ü	ü	k?
</s>
<s>
ㅡ	ㅡ	k?
ŭ	ŭ	k?
</s>
<s>
ㅣ	ㅣ	k?
i	i	k9
</s>
<s>
ㅢ	ㅢ	k?
ŭ	ŭ	k6eAd1
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
</s>
<s>
Korejština	korejština	k1gFnSc1
má	mít	k5eAaImIp3nS
14	#num#	k4
souhlásek	souhláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výslovnost	výslovnost	k1gFnSc1
souhlásky	souhláska	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
předchozí	předchozí	k2eAgFnSc6d1
samohlásce	samohláska	k1gFnSc6
nebo	nebo	k8xC
znělé	znělý	k2eAgFnSc6d1
<g/>
/	/	kIx~
<g/>
neznělé	znělý	k2eNgFnSc6d1
počáteční	počáteční	k2eAgFnSc6d1
souhlásce	souhláska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
ㄱ	ㄱ	k?
k	k	k7c3
/	/	kIx~
g	g	kA
</s>
<s>
ㅋ	ㅋ	k?
kch	kch	k0
</s>
<s>
ㄷ	ㄷ	k?
t	t	k?
/	/	kIx~
d	d	k?
</s>
<s>
ㅌ	ㅌ	k?
tch	tch	k0
</s>
<s>
ㅂ	ㅂ	k?
p	p	k?
/	/	kIx~
b	b	k?
</s>
<s>
ㅍ	ㅍ	k?
pch	pch	k0wR
</s>
<s>
ㅈ	ㅈ	k?
č	č	k0
/	/	kIx~
dž	dž	k?
</s>
<s>
ㅊ	ㅊ	k?
čch	čch	k?
</s>
<s>
ㅅ	ㅅ	k?
s	s	k7c7
</s>
<s>
ㅎ	ㅎ	k?
h	h	k?
</s>
<s>
ㄴ	ㄴ	k?
n	n	k0
</s>
<s>
ㅁ	ㅁ	k?
m	m	kA
</s>
<s>
ㅇ	ㅇ	k?
ng	ng	k?
</s>
<s>
ㄹ	ㄹ	k?
l	l	kA
/	/	kIx~
r	r	kA
</s>
<s>
Transkripce	transkripce	k1gFnSc1
a	a	k8xC
transliterace	transliterace	k1gFnSc1
</s>
<s>
Transkripce	transkripce	k1gFnSc1
a	a	k8xC
transliterace	transliterace	k1gFnSc1
doznává	doznávat	k5eAaImIp3nS
jistých	jistý	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
kombinaci	kombinace	k1gFnSc6
ukončovací	ukončovací	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
znaku	znak	k1gInSc2
a	a	k8xC
počáteční	počáteční	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
znaku	znak	k1gInSc2
dalšího	další	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
počáteční	počáteční	k2eAgInSc1d1
→	→	k?
</s>
<s>
ㅇ	ㅇ	k?
</s>
<s>
koncové	koncový	k2eAgInPc1d1
↓	↓	k?
</s>
<s>
gndrmbsjchktph	gndrmbsjchktph	k1gMnSc1
</s>
<s>
ㄱ	ㄱ	k1gMnSc1
<g/>
,	,	kIx,
k	k	k7c3
</s>
<s>
ㄴ	ㄴ	k1gMnSc1
<g/>
,	,	kIx,
nnnmnbnsnjnchnkntnpnh	nnnmnbnsnjnchnkntnpnh	k1gMnSc1
</s>
<s>
ㄷ	ㄷ	k1gMnSc1
<g/>
,	,	kIx,
jtgnntdnnnmtbtstjtchtkt-ttpth	jtgnntdnnnmtbtstjtchtkt-ttpth	k1gMnSc1
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
ch	ch	k0
</s>
<s>
ㄹ	ㄹ	k1gMnSc1
<g/>
,	,	kIx,
nnldlllmlblsljlchlkltlplh	nnldlllmlblsljlchlkltlplh	k1gMnSc1
</s>
<s>
ㅁ	ㅁ	k1gMnSc1
</s>
<s>
ㅂ	ㅂ	k1gMnSc1
<g/>
,	,	kIx,
p	p	k?
</s>
<s>
ㅇ	ㅇ	k1gMnSc1
</s>
<s>
Příklady	příklad	k1gInPc1
skládání	skládání	k1gNnSc4
písmen	písmeno	k1gNnPc2
do	do	k7c2
čtverečku	čtvereček	k1gInSc2
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
a	a	k8xC
samohlásky	samohláska	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
po	po	k7c6
slabikách	slabika	k1gFnPc6
do	do	k7c2
čtverce	čtverec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
slabika	slabika	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
samohláskou	samohláska	k1gFnSc7
<g/>
,	,	kIx,
vkládá	vkládat	k5eAaImIp3nS
se	se	k3xPyFc4
před	před	k7c7
ní	on	k3xPp3gFnSc7
formální	formální	k2eAgInSc1d1
znak	znak	k1gInSc1
ㅇ	ㅇ	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nevyslovuje	vyslovovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vysvětlivky	vysvětlivka	k1gFnPc4
k	k	k7c3
příkladům	příklad	k1gInPc3
<g/>
:	:	kIx,
C	C	kA
–	–	k?
souhláska	souhláska	k1gFnSc1
(	(	kIx(
<g/>
konsonant	konsonant	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
samohláska	samohláska	k1gFnSc1
(	(	kIx(
<g/>
vokál	vokál	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
가	가	k?
ka	ka	k?
</s>
<s>
고	고	k?
go	go	k?
</s>
<s>
강	강	k?
kang	kang	k1gInSc1
</s>
<s>
곳	곳	k?
kos	kos	k1gMnSc1
</s>
<s>
아	아	k?
a	a	k8xC
</s>
<s>
오	오	k?
o	o	k7c4
</s>
<s>
울	울	k?
ul	ul	kA
</s>
<s>
알	알	k?
al	ala	k1gFnPc2
</s>
<s>
몫	몫	k?
moks	moks	k6eAd1
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hangul	Hangula	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
Korejština	korejština	k1gFnSc1
<g/>
,	,	kIx,
Zásady	zásada	k1gFnPc1
transkripce	transkripce	k1gFnSc2
a	a	k8xC
transliterace	transliterace	k1gFnSc2
korejštiny	korejština	k1gFnSc2
podle	podle	k7c2
vybraných	vybraný	k2eAgInPc2d1
systémů	systém	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARTŮŠEK	Bartůšek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
VÁHALA	váhat	k5eAaImAgFnS
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
v	v	k7c6
češtině	čeština	k1gFnSc6
zacházet	zacházet	k5eAaImF
se	s	k7c7
jmény	jméno	k1gNnPc7
z	z	k7c2
východoasijských	východoasijský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Exonymum	Exonymum	k1gInSc1
</s>
<s>
Jazykové	jazykový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Pravopis	pravopis	k1gInSc1
</s>
<s>
SAMPA	SAMPA	kA
</s>
<s>
Seznam	seznam	k1gInSc1
jazyků	jazyk	k1gInPc2
</s>
<s>
Seznam	seznam	k1gInSc1
písem	písmo	k1gNnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
abecedy	abeceda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
písem	písmo	k1gNnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
skupin	skupina	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hangŭ	Hangŭ	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Seznam	seznam	k1gInSc1
písemných	písemný	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Obecně	obecně	k6eAd1
</s>
<s>
Historie	historie	k1gFnSc1
písma	písmo	k1gNnSc2
</s>
<s>
Jednotka	jednotka	k1gFnSc1
písma	písmo	k1gNnSc2
</s>
<s>
Kaligrafie	kaligrafie	k1gFnSc1
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
standardizace	standardizace	k1gFnSc1
</s>
<s>
Typografie	typografie	k1gFnSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
písem	písmo	k1gNnPc2
</s>
<s>
nerozluštěná	rozluštěný	k2eNgFnSc1d1
</s>
<s>
tvůrci	tvůrce	k1gMnPc1
</s>
<s>
Jazyky	jazyk	k1gInPc1
dle	dle	k7c2
písma	písmo	k1gNnSc2
/	/	kIx~
dle	dle	k7c2
prvního	první	k4xOgInSc2
písemného	písemný	k2eAgInSc2d1
dokladu	doklad	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc4
písem	písmo	k1gNnPc2
dle	dle	k7c2
řezu	řez	k1gInSc2
</s>
<s>
Typypísem	Typypís	k1gInSc7
</s>
<s>
Ideogramy	ideogram	k1gInPc1
/	/	kIx~
Piktogramy	piktogram	k1gInPc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
skutečné	skutečný	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Aztécké	aztécký	k2eAgNnSc1d1
</s>
<s>
Bliss	Bliss	k6eAd1
systém	systém	k1gInSc1
</s>
<s>
Dongba	Dongba	k1gFnSc1
</s>
<s>
Kaidā	Kaidā	k?
</s>
<s>
Míkmaq	Míkmaq	k?
</s>
<s>
Nsibidi	Nsibid	k1gMnPc1
Logogramy	Logogram	k1gInPc1
</s>
<s>
Čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
tradiční	tradiční	k2eAgInSc1d1
</s>
<s>
zjednodušené	zjednodušený	k2eAgNnSc4d1
</s>
<s>
Hanča	Hanča	k1gFnSc1
</s>
<s>
Idu	Ida	k1gFnSc4
</s>
<s>
Kandži	Kandzat	k5eAaPmIp1nS
</s>
<s>
Chữ	Chữ	k?
nôm	nôm	k?
Připodobněno	připodobnit	k5eAaPmNgNnS
čínským	čínský	k2eAgInSc7d1
</s>
<s>
Džurčenské	Džurčenský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kitanské	Kitanský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Tangutské	Tangutský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
Klínopis	Klínopis	k1gInSc1
</s>
<s>
Akkadský	Akkadský	k2eAgInSc1d1
</s>
<s>
Asyrský	asyrský	k2eAgInSc1d1
</s>
<s>
Chetitský	chetitský	k2eAgInSc1d1
</s>
<s>
Luvijský	Luvijský	k2eAgInSc1d1
</s>
<s>
Sumerský	sumerský	k2eAgMnSc1d1
Ostatní	ostatní	k2eAgMnSc1d1
logosylabické	logosylabický	k2eAgInPc5d1
</s>
<s>
Anatolské	anatolský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
</s>
<s>
Krétské	krétský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
</s>
<s>
Mayské	mayský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
Logokonsonantické	Logokonsonantický	k2eAgInPc1d1
</s>
<s>
Démotické	démotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Hieratické	hieratický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Egyptské	egyptský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
Číslice	číslice	k1gFnSc2
</s>
<s>
Západoarabské	Západoarabský	k2eAgFnSc3d1
(	(	kIx(
<g/>
Evropské	evropský	k2eAgFnSc3d1
<g/>
)	)	kIx)
</s>
<s>
Východoarabské	Východoarabský	k2eAgInPc4d1
(	(	kIx(
<g/>
Indické	indický	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1
</s>
<s>
Římské	římský	k2eAgInPc1d1
</s>
<s>
Slabičná	slabičný	k2eAgFnSc1d1
</s>
<s>
Bybloské	Bybloský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Čerokézské	Čerokézský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kypersko-mínojské	Kypersko-mínojský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
</s>
<s>
Hiragana	Hiragana	k1gFnSc1
</s>
<s>
Katakana	Katakana	k1gFnSc1
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
</s>
<s>
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
jógana	jógana	k1gFnSc1
</s>
<s>
Nüshu	Nüsha	k1gFnSc4
</s>
<s>
Staroperský	staroperský	k2eAgInSc1d1
klínopis	klínopis	k1gInSc1
Částečně	částečně	k6eAd1
slabičná	slabičný	k2eAgFnSc1d1
</s>
<s>
Plná	plný	k2eAgFnSc1d1
</s>
<s>
Keltiberské	Keltiberský	k2eAgFnPc1d1
S	s	k7c7
redundancí	redundance	k1gFnSc7
</s>
<s>
Kitanské	Kitanský	k2eAgNnSc1d1
malé	malý	k2eAgNnSc1d1
pečetní	pečetní	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Jihozápadní	jihozápadní	k2eAgNnSc1d1
paleohispánské	paleohispánský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Ču-jin	Ču-jin	k1gMnSc1
fu-chao	fu-chao	k1gMnSc1
</s>
<s>
Abugidy	Abugida	k1gFnPc1
<g/>
(	(	kIx(
<g/>
abecedně-slabičná	abecedně-slabičný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Bráhmí	Bráhmit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgFnSc1d1
</s>
<s>
Bengalská	Bengalský	k2eAgFnSc1d1
</s>
<s>
Brā	Brā	k?
</s>
<s>
Devanā	Devanā	k?
</s>
<s>
Gudžarátská	Gudžarátský	k2eAgFnSc1d1
</s>
<s>
Gupta	Gupta	k1gFnSc1
</s>
<s>
Gurmukhī	Gurmukhī	k?
</s>
<s>
Kaithi	Kaithi	k6eAd1
</s>
<s>
Nā	Nā	k?
</s>
<s>
'	'	kIx"
<g/>
Phags-pa	Phags-p	k1gMnSc2
</s>
<s>
Siddhaṃ	Siddhaṃ	k?
</s>
<s>
Sojombo	Sojomba	k1gMnSc5
</s>
<s>
Tibetská	tibetský	k2eAgFnSc1d1
</s>
<s>
Tocharská	tocharský	k2eAgFnSc1d1
Jižní	jižní	k2eAgFnSc1d1
</s>
<s>
Barmská	barmský	k2eAgFnSc1d1
</s>
<s>
Kannada	Kannada	k1gFnSc1
</s>
<s>
Khmerská	khmerský	k2eAgFnSc1d1
</s>
<s>
Malajalamská	Malajalamský	k2eAgFnSc1d1
</s>
<s>
Sinhala	Sinhat	k5eAaBmAgFnS,k5eAaImAgFnS,k5eAaPmAgFnS
</s>
<s>
Tai	Tai	k?
Viet	Viet	k1gInSc1
</s>
<s>
Tamilská	tamilský	k2eAgFnSc1d1
</s>
<s>
Telugská	Telugskat	k5eAaPmIp3nS
</s>
<s>
Thajská	thajský	k2eAgFnSc1d1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Kanadská	kanadský	k2eAgNnPc1d1
slabičná	slabičný	k2eAgNnPc1d1
písma	písmo	k1gNnPc1
(	(	kIx(
<g/>
Inuitské	Inuitský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
Kríjské	Kríjský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Ge	Ge	k?
<g/>
'	'	kIx"
<g/>
ez	ez	k?
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
Braillovo	Braillův	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kharóšthí	Kharóšthí	k6eAd1
</s>
<s>
Merojské	Merojský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Tā	Tā	k6eAd1
</s>
<s>
Souhlásková	souhláskový	k2eAgFnSc1d1
(	(	kIx(
<g/>
abdžady	abdžad	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
číslice	číslice	k1gFnSc1
</s>
<s>
Aramejský	aramejský	k2eAgInSc1d1
</s>
<s>
Arabský	arabský	k2eAgInSc1d1
</s>
<s>
Hebrejský	hebrejský	k2eAgInSc1d1
</s>
<s>
Tifinagh	Tifinagh	k1gMnSc1
</s>
<s>
Manichejský	manichejský	k2eAgInSc1d1
</s>
<s>
Nabatajský	Nabatajský	k2eAgInSc1d1
</s>
<s>
Pahlaví	Pahlavit	k5eAaPmIp3nS
</s>
<s>
Paleohebrejský	Paleohebrejský	k2eAgInSc1d1
</s>
<s>
Pegon	Pegon	k1gMnSc1
</s>
<s>
Fénický	fénický	k2eAgInSc1d1
</s>
<s>
Psalter	Psalter	k1gMnSc1
</s>
<s>
Samaritánský	samaritánský	k2eAgInSc1d1
</s>
<s>
Jihoarabský	Jihoarabský	k2eAgInSc1d1
</s>
<s>
Sogdský	Sogdský	k2eAgInSc1d1
</s>
<s>
Syrský	syrský	k2eAgInSc1d1
</s>
<s>
Ugaritský	Ugaritský	k2eAgMnSc1d1
Abecedy	abeceda	k1gFnSc2
</s>
<s>
Lineární	lineární	k2eAgFnSc1d1
</s>
<s>
Anglosaský	anglosaský	k2eAgInSc1d1
futhork	futhork	k1gInSc1
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
</s>
<s>
Avestánská	Avestánský	k2eAgFnSc1d1
</s>
<s>
Kárská	Kárskat	k5eAaPmIp3nS
</s>
<s>
Koptská	koptský	k2eAgFnSc1d1
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1
</s>
<s>
Starší	starý	k2eAgInSc1d2
futhark	futhark	k1gInSc1
</s>
<s>
Etruská	etruský	k2eAgFnSc1d1
</s>
<s>
Gruzínská	gruzínský	k2eAgFnSc1d1
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1
</s>
<s>
Gótská	gótský	k2eAgFnSc1d1
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Řecko-Iberská	řecko-iberský	k2eAgFnSc1d1
</s>
<s>
Hangul	Hangul	k1gInSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Latinka	latinka	k1gFnSc1
</s>
<s>
Beneventána	Beneventán	k2eAgFnSc1d1
</s>
<s>
Gotické	gotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
</s>
<s>
Fraktura	fraktura	k1gFnSc1
</s>
<s>
Cló	Cló	k?
Gaelach	Gaelach	k1gInSc1
</s>
<s>
Iroskotské	Iroskotský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kurent	kurent	k1gInSc1
</s>
<s>
Merovejské	merovejský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Sütterlin	Sütterlin	k1gInSc1
</s>
<s>
Vizigótské	vizigótský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Lýkská	Lýkskat	k5eAaPmIp3nS
</s>
<s>
Lydská	Lydskat	k5eAaPmIp3nS
</s>
<s>
Mandžuská	mandžuský	k2eAgFnSc1d1
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
</s>
<s>
Neo-Tifinagh	Neo-Tifinagh	k1gMnSc1
</s>
<s>
N	N	kA
<g/>
'	'	kIx"
<g/>
Ko	Ko	k1gMnSc1
</s>
<s>
Ogham	Ogham	k6eAd1
</s>
<s>
Rovas	Rovas	k1gMnSc1
</s>
<s>
Orchonské	Orchonský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Staroujgurská	Staroujgurský	k2eAgFnSc1d1
</s>
<s>
Runy	Runa	k1gFnPc1
</s>
<s>
Tifinagh	Tifinagh	k1gMnSc1
</s>
<s>
Visible	Visible	k6eAd1
Speech	speech	k1gInSc1
</s>
<s>
Mladší	mladý	k2eAgInSc1d2
futhark	futhark	k1gInSc1
Nelineární	lineární	k2eNgFnSc2d1
</s>
<s>
Braillovo	Braillův	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
vlajková	vlajkový	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
Morseova	Morseův	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
Chappeův	Chappeův	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
</s>
<s>
Semafor	Semafor	k1gInSc1
(	(	kIx(
<g/>
abeceda	abeceda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Moon	Moon	k1gMnSc1
type	typ	k1gInSc5
</s>
<s>
Fiktivní	fiktivní	k2eAgNnPc1d1
písma	písmo	k1gNnPc1
</s>
<s>
Aurebesh	Aurebesh	k1gMnSc1
</s>
<s>
Cirth	Cirth	k1gMnSc1
</s>
<s>
pIqaD	pIqaD	k?
</s>
<s>
Tengwar	Tengwar	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Korea	Korea	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
339352	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4252163-4	4252163-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2008122928	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2008122928	#num#	k4
</s>
