<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
strávil	strávit	k5eAaPmAgMnS	strávit
Stendhal	Stendhal	k1gMnSc1	Stendhal
víceméně	víceméně	k9	víceméně
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
a	a	k8xC	a
zde	zde	k6eAd1	zde
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
další	další	k2eAgInPc4d1	další
velké	velký	k2eAgInPc4d1	velký
romány	román	k1gInPc4	román
<g/>
:	:	kIx,	:
Lucien	Lucien	k2eAgInSc4d1	Lucien
Leuwen	Leuwen	k1gInSc4	Leuwen
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobiografické	autobiografický	k2eAgNnSc1d1	autobiografické
dílo	dílo	k1gNnSc1	dílo
Život	život	k1gInSc4	život
Henri	Henr	k1gFnSc2	Henr
Brularda	Brularda	k1gFnSc1	Brularda
(	(	kIx(	(
<g/>
Vie	Vie	k1gFnSc1	Vie
de	de	k?	de
Henri	Henri	k1gNnSc1	Henri
Brulard	Brulard	k1gMnSc1	Brulard
<g/>
,	,	kIx,	,
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
