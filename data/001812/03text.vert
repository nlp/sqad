<s>
Stendhal	Stendhal	k1gMnSc1	Stendhal
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
Standal	Standal	k1gFnSc1	Standal
i	i	k8xC	i
Stendal	Stendal	k1gFnSc1	Stendal
<g/>
)	)	kIx)	)
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Henri	Henr	k1gFnSc2	Henr
Marie	Maria	k1gFnSc2	Maria
Beyle	Beyle	k1gFnSc2	Beyle
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
1783	[number]	k4	1783
Grenoble	Grenoble	k1gInSc1	Grenoble
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
1842	[number]	k4	1842
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
motivem	motiv	k1gInSc7	motiv
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc1	hledání
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
krásné	krásný	k2eAgInPc4d1	krásný
okamžiky	okamžik	k1gInPc4	okamžik
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
osobního	osobní	k2eAgNnSc2d1	osobní
uplatnění	uplatnění	k1gNnSc2	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
hrdinové	hrdina	k1gMnPc1	hrdina
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
brání	bránit	k5eAaImIp3nS	bránit
společenským	společenský	k2eAgFnPc3d1	společenská
normám	norma	k1gFnPc3	norma
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
,	,	kIx,	,
prohrávají	prohrávat	k5eAaImIp3nP	prohrávat
<g/>
,	,	kIx,	,
podléhají	podléhat	k5eAaImIp3nP	podléhat
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
opouští	opouštět	k5eAaImIp3nS	opouštět
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
také	také	k9	také
přetvářet	přetvářet	k5eAaImF	přetvářet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
jich	on	k3xPp3gFnPc2	on
okolí	okolí	k1gNnSc4	okolí
vážilo	vážit	k5eAaImAgNnS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Pseudonym	pseudonym	k1gInSc1	pseudonym
Stendhal	Stendhal	k1gMnSc1	Stendhal
použil	použít	k5eAaPmAgMnS	použít
Henri	Henre	k1gFnSc4	Henre
Beyle	Beyle	k1gNnSc3	Beyle
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
Dějiny	dějiny	k1gFnPc1	dějiny
malířství	malířství	k1gNnSc2	malířství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
názvem	název	k1gInSc7	název
německého	německý	k2eAgNnSc2d1	německé
města	město	k1gNnSc2	město
Stendal	Stendal	k1gFnPc2	Stendal
<g/>
,	,	kIx,	,
rodiště	rodiště	k1gNnSc4	rodiště
historika	historik	k1gMnSc2	historik
umění	umění	k1gNnSc2	umění
Winckelmanna	Winckelmann	k1gMnSc2	Winckelmann
<g/>
.	.	kIx.	.
</s>
<s>
Henri	Henr	k1gMnPc1	Henr
Beyle	Beyle	k1gFnSc2	Beyle
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
zámožné	zámožný	k2eAgFnSc6d1	zámožná
měšťácké	měšťácký	k2eAgFnSc6d1	měšťácká
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
Grenoblu	Grenoble	k1gInSc6	Grenoble
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dobrého	dobrý	k2eAgNnSc2d1	dobré
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
postavou	postava	k1gFnSc7	postava
pařížských	pařížský	k2eAgInPc2d1	pařížský
diskuzních	diskuzní	k2eAgInPc2d1	diskuzní
salonů	salon	k1gInPc2	salon
<g/>
,	,	kIx,	,
s	s	k7c7	s
napoleonskou	napoleonský	k2eAgFnSc7d1	napoleonská
armádou	armáda	k1gFnSc7	armáda
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
ruského	ruský	k2eAgNnSc2d1	ruské
tažení	tažení	k1gNnSc2	tažení
a	a	k8xC	a
zažil	zažít	k5eAaPmAgMnS	zažít
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
porážku	porážka	k1gFnSc4	porážka
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
cestopisů	cestopis	k1gInPc2	cestopis
a	a	k8xC	a
teoretických	teoretický	k2eAgInPc2d1	teoretický
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
psychologického	psychologický	k2eAgInSc2d1	psychologický
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgMnS	být
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
osvícenstvím	osvícenství	k1gNnSc7	osvícenství
a	a	k8xC	a
protikrálovským	protikrálovský	k2eAgNnSc7d1	protikrálovský
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
z	z	k7c2	z
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
nadšení	nadšení	k1gNnSc2	nadšení
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
nadšený	nadšený	k2eAgMnSc1d1	nadšený
stoupenec	stoupenec	k1gMnSc1	stoupenec
revoluce	revoluce	k1gFnSc2	revoluce
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparteho	Bonaparte	k1gMnSc2	Bonaparte
a	a	k8xC	a
jako	jako	k8xC	jako
důstojník	důstojník	k1gMnSc1	důstojník
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
tažení	tažení	k1gNnSc4	tažení
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Restaurace	restaurace	k1gFnSc2	restaurace
prožil	prožít	k5eAaPmAgMnS	prožít
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgInS	stýkat
s	s	k7c7	s
italskými	italský	k2eAgMnPc7d1	italský
bojovníky	bojovník	k1gMnPc7	bojovník
za	za	k7c4	za
národní	národní	k2eAgNnPc4d1	národní
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
rakouské	rakouský	k2eAgFnSc2d1	rakouská
policie	policie	k1gFnSc2	policie
však	však	k9	však
Itálii	Itálie	k1gFnSc4	Itálie
musel	muset	k5eAaImAgInS	muset
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
o	o	k7c4	o
romantismus	romantismus	k1gInSc4	romantismus
a	a	k8xC	a
proti	proti	k7c3	proti
dvorskému	dvorský	k2eAgInSc3d1	dvorský
klasicismu	klasicismus	k1gInSc3	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Stendhal	Stendhal	k1gMnSc1	Stendhal
byl	být	k5eAaImAgMnS	být
především	především	k9	především
znalcem	znalec	k1gMnSc7	znalec
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
mělo	mít	k5eAaImAgNnS	mít
pravdivě	pravdivě	k6eAd1	pravdivě
zachycovat	zachycovat	k5eAaImF	zachycovat
"	"	kIx"	"
<g/>
hnutí	hnutí	k1gNnSc1	hnutí
srdce	srdce	k1gNnSc2	srdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
odvahu	odvaha	k1gFnSc4	odvaha
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
tvořit	tvořit	k5eAaImF	tvořit
podle	podle	k7c2	podle
současného	současný	k2eAgInSc2d1	současný
života	život	k1gInSc2	život
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
podle	podle	k7c2	podle
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
společnosti	společnost	k1gFnPc4	společnost
a	a	k8xC	a
životu	život	k1gInSc2	život
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Stendhala	Stendhal	k1gMnSc2	Stendhal
působit	působit	k5eAaImF	působit
na	na	k7c4	na
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
zaujmout	zaujmout	k5eAaPmF	zaujmout
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
otřást	otřást	k5eAaPmF	otřást
jím	jíst	k5eAaImIp1nS	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
situace	situace	k1gFnSc2	situace
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
dychtivost	dychtivost	k1gFnSc1	dychtivost
po	po	k7c6	po
životě	život	k1gInSc6	život
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
Stendhala	Stendhal	k1gMnSc2	Stendhal
nerozlučně	rozlučně	k6eNd1	rozlučně
spjaty	spjat	k2eAgInPc1d1	spjat
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
a	a	k8xC	a
svůdnost	svůdnost	k1gFnSc1	svůdnost
inteligence	inteligence	k1gFnSc2	inteligence
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
citu	cit	k1gInSc3	cit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románech	román	k1gInPc6	román
a	a	k8xC	a
novelách	novela	k1gFnPc6	novela
líčí	líčit	k5eAaImIp3nS	líčit
cestu	cesta	k1gFnSc4	cesta
ctižádostivých	ctižádostivý	k2eAgMnPc2d1	ctižádostivý
hrdinů	hrdina	k1gMnPc2	hrdina
od	od	k7c2	od
romantické	romantický	k2eAgFnSc2d1	romantická
iluze	iluze	k1gFnSc2	iluze
k	k	k7c3	k
cynismu	cynismus	k1gInSc3	cynismus
a	a	k8xC	a
přetvářce	přetvářka	k1gFnSc3	přetvářka
<g/>
,	,	kIx,	,
či	či	k8xC	či
marné	marný	k2eAgFnSc3d1	marná
vzpouře	vzpoura	k1gFnSc3	vzpoura
a	a	k8xC	a
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
romantiků	romantik	k1gMnPc2	romantik
se	se	k3xPyFc4	se
odlišoval	odlišovat	k5eAaImAgMnS	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
věcné	věcný	k2eAgInPc4d1	věcný
a	a	k8xC	a
neosobní	osobní	k2eNgNnSc4d1	neosobní
podání	podání	k1gNnSc4	podání
románové	románový	k2eAgFnSc2d1	románová
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Stendhalovo	Stendhalův	k2eAgNnSc1d1	Stendhalovo
líčení	líčení	k1gNnSc1	líčení
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
realismus	realismus	k1gInSc4	realismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stendhal	Stendhal	k1gMnSc1	Stendhal
nebyl	být	k5eNaImAgMnS	být
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
příliš	příliš	k6eAd1	příliš
čten	číst	k5eAaImNgInS	číst
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
zůstal	zůstat	k5eAaPmAgMnS	zůstat
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
pochopen	pochopen	k2eAgInSc1d1	pochopen
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
předpovědi	předpověď	k1gFnSc2	předpověď
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
náklonnost	náklonnost	k1gFnSc1	náklonnost
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
kultuře	kultura	k1gFnSc3	kultura
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
Stendhalových	Stendhalův	k2eAgFnPc2d1	Stendhalova
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
spis	spis	k1gInSc4	spis
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
bez	bez	k7c2	bez
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
začíná	začínat	k5eAaImIp3nS	začínat
Stendhal	Stendhal	k1gMnSc1	Stendhal
psát	psát	k5eAaImF	psát
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
prvním	první	k4xOgMnSc7	první
je	být	k5eAaImIp3nS	být
Armance	Armanec	k1gMnSc2	Armanec
aneb	aneb	k?	aneb
několik	několik	k4yIc1	několik
scén	scéna	k1gFnPc2	scéna
z	z	k7c2	z
Pařížského	pařížský	k2eAgInSc2d1	pařížský
salonu	salon	k1gInSc2	salon
(	(	kIx(	(
<g/>
Armance	Armanec	k1gInSc2	Armanec
<g/>
,	,	kIx,	,
ou	ou	k0	ou
quelque	quelque	k1gNnSc2	quelque
scè	scè	k?	scè
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
salon	salon	k1gInSc1	salon
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
patrně	patrně	k6eAd1	patrně
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc4d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Rouge	rouge	k1gFnSc1	rouge
et	et	k?	et
le	le	k?	le
Noir	Noir	k1gInSc1	Noir
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Červencové	červencový	k2eAgFnSc6d1	červencová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
byl	být	k5eAaImAgMnS	být
Stendhal	Stendhal	k1gMnSc1	Stendhal
jmenován	jmenován	k2eAgMnSc1d1	jmenován
konzulem	konzul	k1gMnSc7	konzul
v	v	k7c6	v
Terstu	Terst	k1gInSc6	Terst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
rakouská	rakouský	k2eAgFnSc1d1	rakouská
vláda	vláda	k1gFnSc1	vláda
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
policejně	policejně	k6eAd1	policejně
stíhaného	stíhaný	k2eAgMnSc4d1	stíhaný
cizince	cizinec	k1gMnSc4	cizinec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
Papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
strávil	strávit	k5eAaPmAgMnS	strávit
Stendhal	Stendhal	k1gMnSc1	Stendhal
víceméně	víceméně	k9	víceméně
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
a	a	k8xC	a
zde	zde	k6eAd1	zde
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
další	další	k2eAgInPc4d1	další
velké	velký	k2eAgInPc4d1	velký
romány	román	k1gInPc4	román
<g/>
:	:	kIx,	:
Lucien	Lucien	k2eAgInSc4d1	Lucien
Leuwen	Leuwen	k1gInSc4	Leuwen
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobiografické	autobiografický	k2eAgNnSc1d1	autobiografické
dílo	dílo	k1gNnSc1	dílo
Život	život	k1gInSc4	život
Henri	Henr	k1gFnSc2	Henr
Brularda	Brularda	k1gFnSc1	Brularda
(	(	kIx(	(
<g/>
Vie	Vie	k1gFnSc1	Vie
de	de	k?	de
Henri	Henri	k1gNnSc1	Henri
Brulard	Brulard	k1gMnSc1	Brulard
<g/>
,	,	kIx,	,
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
velkých	velký	k2eAgInPc2d1	velký
románů	román	k1gInPc2	román
byla	být	k5eAaImAgFnS	být
Kartouza	kartouza	k1gFnSc1	kartouza
parmská	parmský	k2eAgFnSc1d1	parmská
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Chartreuse	Chartreuse	k1gFnSc2	Chartreuse
de	de	k?	de
Parme	Parm	k1gMnSc5	Parm
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
Lamiela	Lamiela	k1gFnSc1	Lamiela
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stendhal	Stendhal	k1gMnSc1	Stendhal
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
častých	častý	k2eAgFnPc2d1	častá
návštěv	návštěva	k1gFnPc2	návštěva
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
postihla	postihnout	k5eAaPmAgFnS	postihnout
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Montmartre	Montmartr	k1gMnSc5	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
Armance	Armance	k1gFnSc2	Armance
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Le	Le	k1gFnSc2	Le
Rouge	rouge	k1gFnSc2	rouge
et	et	k?	et
le	le	k?	le
noir	noir	k1gMnSc1	noir
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Stendhal	Stendhal	k1gMnSc1	Stendhal
červenou	červený	k2eAgFnSc4d1	červená
barvou	barvý	k2eAgFnSc4d1	barvá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
tmářství	tmářství	k1gNnSc2	tmářství
a	a	k8xC	a
despotismus	despotismus	k1gInSc1	despotismus
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dvojdílný	dvojdílný	k2eAgInSc4d1	dvojdílný
román	román	k1gInSc4	román
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Kronika	kronika	k1gFnSc1	kronika
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
-	-	kIx~	-
líčí	líčit	k5eAaImIp3nS	líčit
osudy	osud	k1gInPc1	osud
chudého	chudý	k2eAgMnSc2d1	chudý
mladíka	mladík	k1gMnSc2	mladík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
za	za	k7c2	za
restaurace	restaurace	k1gFnSc2	restaurace
Bourbonů	bourbon	k1gInPc2	bourbon
pokusí	pokusit	k5eAaPmIp3nS	pokusit
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
vzoru	vzor	k1gInSc6	vzor
o	o	k7c4	o
společenský	společenský	k2eAgInSc4d1	společenský
vzestup	vzestup	k1gInSc4	vzestup
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
společností	společnost	k1gFnSc7	společnost
rozdrcen	rozdrtit	k5eAaPmNgMnS	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
venkovského	venkovský	k2eAgMnSc2d1	venkovský
dřevaře	dřevař	k1gMnSc2	dřevař
Julien	Julien	k2eAgInSc1d1	Julien
Sorel	Sorel	k1gInSc1	Sorel
chce	chtít	k5eAaImIp3nS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c6	v
životě	život	k1gInSc6	život
vynikajícího	vynikající	k2eAgNnSc2d1	vynikající
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
by	by	kYmCp3nS	by
ve	v	k7c6	v
stávajících	stávající	k2eAgInPc6d1	stávající
politických	politický	k2eAgInPc6d1	politický
poměrech	poměr	k1gInPc6	poměr
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nemohl	moct	k5eNaImAgMnS	moct
získat	získat	k5eAaPmF	získat
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
opovrhuje	opovrhovat	k5eAaImIp3nS	opovrhovat
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
na	na	k7c4	na
kněžskou	kněžský	k2eAgFnSc4d1	kněžská
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
nezbytným	zbytný	k2eNgInSc7d1	zbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
ke	k	k7c3	k
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
touží	toužit	k5eAaImIp3nP	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Sorel	Sorel	k1gMnSc1	Sorel
je	být	k5eAaImIp3nS	být
pokrytec	pokrytec	k1gMnSc1	pokrytec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předstírá	předstírat	k5eAaImIp3nS	předstírat
nebo	nebo	k8xC	nebo
utajuje	utajovat	k5eAaImIp3nS	utajovat
city	cit	k1gInPc4	cit
a	a	k8xC	a
názory	názor	k1gInPc4	názor
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jeho	jeho	k3xOp3gInSc1	jeho
vlastní	vlastní	k2eAgInSc1d1	vlastní
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
ctižádost	ctižádost	k1gFnSc4	ctižádost
vnáší	vnášet	k5eAaImIp3nS	vnášet
i	i	k9	i
do	do	k7c2	do
milostného	milostný	k2eAgInSc2d1	milostný
poměru	poměr	k1gInSc2	poměr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
miluje	milovat	k5eAaImIp3nS	milovat
dvě	dva	k4xCgFnPc4	dva
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
paní	paní	k1gFnSc1	paní
de	de	k?	de
Rénal	Rénal	k1gInSc4	Rénal
<g/>
,	,	kIx,	,
starostovu	starostův	k2eAgFnSc4d1	starostova
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
pronikne	proniknout	k5eAaPmIp3nS	proniknout
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Mathyldu	Mathyld	k1gInSc2	Mathyld
de	de	k?	de
la	la	k1gNnSc1	la
Mole	mol	k1gInSc5	mol
<g/>
.	.	kIx.	.
</s>
<s>
Sorel	Sorel	k1gInSc1	Sorel
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
nespokojený	spokojený	k2eNgInSc1d1	nespokojený
<g/>
,	,	kIx,	,
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
střízlivé	střízlivý	k2eAgFnSc6d1	střízlivá
měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
úspěch	úspěch	k1gInSc1	úspěch
problematický	problematický	k2eAgInSc1d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
postřelí	postřelit	k5eAaPmIp3nS	postřelit
paní	paní	k1gFnSc1	paní
de	de	k?	de
Renal	Renal	k1gInSc1	Renal
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
milost	milost	k1gFnSc4	milost
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nenávidí	návidět	k5eNaImIp3nP	návidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
raději	rád	k6eAd2	rád
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
popravišti	popraviště	k1gNnSc6	popraviště
jakožto	jakožto	k8xS	jakožto
vzbouřený	vzbouřený	k2eAgMnSc1d1	vzbouřený
plebejec	plebejec	k1gMnSc1	plebejec
<g/>
.	.	kIx.	.
</s>
<s>
Lucien	Lucien	k2eAgInSc1d1	Lucien
Leuwen	Leuwen	k1gInSc1	Leuwen
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
Kartouza	kartouza	k1gFnSc1	kartouza
parmská	parmský	k2eAgFnSc1d1	parmská
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Chartreuse	Chartreuse	k1gFnSc2	Chartreuse
de	de	k?	de
Parme	Parm	k1gMnSc5	Parm
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
šlechtic	šlechtic	k1gMnSc1	šlechtic
Fabricio	Fabricio	k1gMnSc1	Fabricio
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
prchá	prchat	k5eAaImIp3nS	prchat
(	(	kIx(	(
<g/>
neromantický	romantický	k2eNgInSc4d1	neromantický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
bitvu	bitva	k1gFnSc4	bitva
<g/>
)	)	kIx)	)
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
kariéru	kariéra	k1gFnSc4	kariéra
coby	coby	k?	coby
církevní	církevní	k2eAgMnSc1d1	církevní
hodnostář	hodnostář	k1gMnSc1	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozčarován	rozčarovat	k5eAaPmNgMnS	rozčarovat
intrikami	intrika	k1gFnPc7	intrika
<g/>
;	;	kIx,	;
při	při	k7c6	při
náhodném	náhodný	k2eAgNnSc6d1	náhodné
milostném	milostný	k2eAgNnSc6d1	milostné
dobrodružství	dobrodružství	k1gNnSc6	dobrodružství
zabije	zabít	k5eAaPmIp3nS	zabít
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
jako	jako	k9	jako
vězeň	vězeň	k1gMnSc1	vězeň
parmské	parmský	k2eAgFnSc2d1	parmská
citadely	citadela	k1gFnSc2	citadela
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
dcery	dcera	k1gFnSc2	dcera
jejího	její	k3xOp3gMnSc2	její
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
<g/>
,	,	kIx,	,
Klélie	Klélie	k1gFnSc2	Klélie
<g/>
.	.	kIx.	.
</s>
<s>
Milovaná	milovaný	k2eAgFnSc1d1	milovaná
teta	teta	k1gFnSc1	teta
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Fabricio	Fabricio	k6eAd1	Fabricio
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
lásce	láska	k1gFnSc3	láska
ke	k	k7c3	k
Klérii	Klérie	k1gFnSc3	Klérie
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jeho	jeho	k3xOp3gNnSc1	jeho
dítě	dítě	k1gNnSc1	dítě
a	a	k8xC	a
milenka	milenka	k1gFnSc1	milenka
zemřou	zemřít	k5eAaPmIp3nP	zemřít
<g/>
,	,	kIx,	,
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
-	-	kIx~	-
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
se	se	k3xPyFc4	se
do	do	k7c2	do
parmského	parmský	k2eAgInSc2d1	parmský
kláštera	klášter	k1gInSc2	klášter
kartuziánů	kartuzián	k1gMnPc2	kartuzián
<g/>
.	.	kIx.	.
</s>
<s>
Lamiel	Lamiel	k1gInSc1	Lamiel
(	(	kIx(	(
<g/>
nedokončeno	dokončen	k2eNgNnSc1d1	nedokončeno
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
Novely	novela	k1gFnPc4	novela
Italské	italský	k2eAgFnSc2d1	italská
kroniky	kronika	k1gFnSc2	kronika
(	(	kIx(	(
<g/>
Italian	Italian	k1gMnSc1	Italian
Chroniques	Chroniques	k1gMnSc1	Chroniques
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
–	–	k?	–
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
novely	novela	k1gFnPc1	novela
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
italskou	italský	k2eAgFnSc7d1	italská
literaturou	literatura	k1gFnSc7	literatura
<g/>
:	:	kIx,	:
Les	les	k1gInSc1	les
Cenci	Cence	k1gFnSc4	Cence
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Duchesse	duchesse	k1gFnSc2	duchesse
de	de	k?	de
Palliano	Palliana	k1gFnSc5	Palliana
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Abbesse	Abbess	k1gMnSc2	Abbess
de	de	k?	de
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
Vanina	Vanina	k1gMnSc1	Vanina
Vanini	Vanin	k1gMnPc1	Vanin
<g/>
.	.	kIx.	.
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Rose	Ros	k1gMnSc2	Ros
et	et	k?	et
le	le	k?	le
Vert	Vert	k?	Vert
<g/>
)	)	kIx)	)
-	-	kIx~	-
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
novela	novela	k1gFnSc1	novela
vydaná	vydaný	k2eAgFnSc1d1	vydaná
posmrtně	posmrtně	k6eAd1	posmrtně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Autobiografie	autobiografie	k1gFnPc1	autobiografie
<g/>
,	,	kIx,	,
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
paměti	paměť	k1gFnPc1	paměť
a	a	k8xC	a
korespondence	korespondence	k1gFnPc1	korespondence
Život	život	k1gInSc1	život
Henriho	Henri	k1gMnSc2	Henri
Brularda	Brulard	k1gMnSc2	Brulard
(	(	kIx(	(
<g/>
Vie	Vie	k1gMnSc1	Vie
de	de	k?	de
Henry	Henry	k1gMnSc1	Henry
Brulard	Brulard	k1gMnSc1	Brulard
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
-	-	kIx~	-
posmrtně	posmrtně	k6eAd1	posmrtně
vydaný	vydaný	k2eAgInSc1d1	vydaný
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Egoistické	egoistický	k2eAgFnPc1d1	egoistická
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
(	(	kIx(	(
<g/>
Souvenirs	Souvenirs	k1gInSc1	Souvenirs
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Égotisme	Égotismus	k1gInSc5	Égotismus
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
-	-	kIx~	-
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
kniha	kniha	k1gFnSc1	kniha
pamětí	paměť	k1gFnPc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Deníky	deník	k1gInPc1	deník
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
I-V	I-V	k1gFnSc1	I-V
<g/>
)	)	kIx)	)
-	-	kIx~	-
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgFnPc4d1	vydaná
deníkové	deníkový	k2eAgFnPc4d1	deníková
zápisky	zápiska	k1gFnPc4	zápiska
<g/>
.	.	kIx.	.
</s>
<s>
Korespondence	korespondence	k1gFnSc1	korespondence
(	(	kIx(	(
<g/>
Correspondance	Correspondance	k1gFnSc1	Correspondance
I-X	I-X	k1gFnSc1	I-X
<g/>
)	)	kIx)	)
-	-	kIx~	-
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgInPc4d1	vydaný
soubory	soubor	k1gInPc4	soubor
Stendhalových	Stendhalův	k2eAgInPc2d1	Stendhalův
dopisů	dopis	k1gInPc2	dopis
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyšel	vyjít	k5eAaPmAgInS	vyjít
výbor	výbor	k1gInSc1	výbor
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dopisy	dopis	k1gInPc4	dopis
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
přátelům	přítel	k1gMnPc3	přítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eseje	esej	k1gInPc4	esej
a	a	k8xC	a
uměnovědné	uměnovědný	k2eAgInPc4d1	uměnovědný
spisy	spis	k1gInPc4	spis
Historie	historie	k1gFnSc2	historie
malířství	malířství	k1gNnSc2	malířství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k0	la
Peinture	Peintur	k1gMnSc5	Peintur
en	en	k?	en
Italie	Italie	k1gFnPc4	Italie
<g/>
,	,	kIx,	,
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Stenhalova	Stenhalův	k2eAgFnSc1d1	Stenhalův
esejistická	esejistický	k2eAgFnSc1d1	esejistická
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Racine	Racinout	k5eAaPmIp3nS	Racinout
et	et	k?	et
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
Římské	římský	k2eAgFnSc2d1	římská
procházky	procházka	k1gFnSc2	procházka
(	(	kIx(	(
<g/>
Promenades	Promenades	k1gInSc1	Promenades
dans	dansit	k5eAaPmRp2nS	dansit
Rome	Rom	k1gMnSc5	Rom
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
Paměti	paměť	k1gFnSc2	paměť
turisty	turista	k1gMnSc2	turista
(	(	kIx(	(
<g/>
Mémoires	Mémoires	k1gInSc1	Mémoires
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
touriste	tourist	k1gMnSc5	tourist
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
Část	část	k1gFnSc1	část
Stendhalova	Stendhalův	k2eAgNnSc2d1	Stendhalovo
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
církví	církev	k1gFnPc2	církev
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
všechny	všechen	k3xTgInPc4	všechen
milostné	milostný	k2eAgInPc4d1	milostný
příběhy	příběh	k1gInPc4	příběh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
omnes	omnes	k1gInSc1	omnes
fabulae	fabula	k1gMnSc2	fabula
amatoriae	amatoria	k1gMnSc2	amatoria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stendhal	Stendhal	k1gMnSc1	Stendhal
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
dějiny	dějiny	k1gFnPc4	dějiny
evropského	evropský	k2eAgInSc2d1	evropský
(	(	kIx(	(
<g/>
a	a	k8xC	a
světového	světový	k2eAgMnSc2d1	světový
<g/>
)	)	kIx)	)
umění	umění	k1gNnSc2	umění
jak	jak	k8xS	jak
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
romány	román	k1gInPc7	román
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
Stendhalova	Stendhalův	k2eAgNnSc2d1	Stendhalovo
díla	dílo	k1gNnSc2	dílo
sice	sice	k8xC	sice
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
romány	román	k1gInPc4	román
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
rozsah	rozsah	k1gInSc1	rozsah
<g/>
:	:	kIx,	:
pět	pět	k4xCc4	pět
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgNnPc1	dva
neukončená	ukončený	k2eNgNnPc1d1	neukončené
a	a	k8xC	a
za	za	k7c2	za
života	život	k1gInSc2	život
autorova	autorův	k2eAgFnSc1d1	autorova
nezveřejněná	zveřejněný	k2eNgFnSc1d1	nezveřejněná
(	(	kIx(	(
<g/>
Lucien	Lucino	k1gNnPc2	Lucino
Leuwen	Leuwna	k1gFnPc2	Leuwna
<g/>
,	,	kIx,	,
Lamiela	Lamiela	k1gFnSc1	Lamiela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
trvale	trvale	k6eAd1	trvale
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
nebyly	být	k5eNaImAgInP	být
Stendhalovy	Stendhalův	k2eAgInPc1d1	Stendhalův
romány	román	k1gInPc1	román
oceněny	ocenit	k5eAaPmNgInP	ocenit
ani	ani	k8xC	ani
kritikou	kritika	k1gFnSc7	kritika
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
i	i	k8xC	i
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Honoré	Honorý	k2eAgFnSc2d1	Honorý
de	de	k?	de
Balzaka	Balzac	k1gMnSc4	Balzac
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
čtenáři	čtenář	k1gMnPc7	čtenář
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejčtenějších	čtený	k2eAgMnPc2d3	nejčtenější
světových	světový	k2eAgMnPc2d1	světový
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
nevelkým	velký	k2eNgInSc7d1	nevelký
románovým	románový	k2eAgInSc7d1	románový
dílem	díl	k1gInSc7	díl
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
jednak	jednak	k8xC	jednak
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
realistický	realistický	k2eAgInSc1d1	realistický
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
umění	umění	k1gNnSc4	umění
románu	román	k1gInSc2	román
měl	mít	k5eAaImAgInS	mít
zásadní	zásadní	k2eAgInSc1d1	zásadní
vliv	vliv	k1gInSc1	vliv
i	i	k8xC	i
Stendhalovův	Stendhalovův	k2eAgInSc1d1	Stendhalovův
způsob	způsob	k1gInSc1	způsob
výstavby	výstavba	k1gFnSc2	výstavba
postavy	postava	k1gFnSc2	postava
<g/>
:	:	kIx,	:
vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
rysy	rys	k1gInPc1	rys
postavy	postava	k1gFnSc2	postava
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
nejsou	být	k5eNaImIp3nP	být
podány	podat	k5eAaPmNgInP	podat
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
není	být	k5eNaImIp3nS	být
definitivně	definitivně	k6eAd1	definitivně
a	a	k8xC	a
beze	beze	k7c2	beze
zbytků	zbytek	k1gInPc2	zbytek
popsána	popsat	k5eAaPmNgFnS	popsat
a	a	k8xC	a
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodních	úvodní	k2eAgFnPc6d1	úvodní
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvykem	zvyk	k1gInSc7	zvyk
<g/>
;	;	kIx,	;
postava	postava	k1gFnSc1	postava
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
Stendhala	Stendhal	k1gMnSc2	Stendhal
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
čtenáře	čtenář	k1gMnSc2	čtenář
a	a	k8xC	a
z	z	k7c2	z
fragmentárních	fragmentární	k2eAgInPc2d1	fragmentární
rysů	rys	k1gInPc2	rys
a	a	k8xC	a
charakteristik	charakteristika	k1gFnPc2	charakteristika
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
autor	autor	k1gMnSc1	autor
postupně	postupně	k6eAd1	postupně
syntetického	syntetický	k2eAgInSc2d1	syntetický
portrétu	portrét	k1gInSc2	portrét
<g/>
.	.	kIx.	.
-	-	kIx~	-
Příčila	příčit	k5eAaImAgFnS	příčit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stylizovanost	stylizovanost	k1gFnSc1	stylizovanost
a	a	k8xC	a
nabubřelost	nabubřelost	k1gFnSc1	nabubřelost
<g/>
,	,	kIx,	,
v	v	k7c6	v
textu	text	k1gInSc6	text
příběhu	příběh	k1gInSc2	příběh
výrazně	výrazně	k6eAd1	výrazně
omezil	omezit	k5eAaPmAgInS	omezit
popisnost	popisnost	k1gFnSc4	popisnost
vnějších	vnější	k2eAgFnPc2d1	vnější
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
však	však	k9	však
věnoval	věnovat	k5eAaPmAgMnS	věnovat
vykreslení	vykreslení	k1gNnSc4	vykreslení
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
života	život	k1gInSc2	život
hrdinů	hrdina	k1gMnPc2	hrdina
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
psychologického	psychologický	k2eAgInSc2d1	psychologický
románu	román	k1gInSc2	román
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
opřít	opřít	k5eAaPmF	opřít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
rysů	rys	k1gInPc2	rys
geniálnosti	geniálnost	k1gFnSc2	geniálnost
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
nevláčet	vláčet	k5eNaImF	vláčet
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
brázdou	brázda	k1gFnSc7	brázda
vyjetou	vyjetý	k2eAgFnSc4d1	vyjetá
obyčejnými	obyčejný	k2eAgMnPc7d1	obyčejný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
že	že	k8xS	že
umění	umění	k1gNnSc1	umění
užívat	užívat	k5eAaImF	užívat
života	život	k1gInSc2	život
chápu	chápat	k5eAaImIp1nS	chápat
teprve	teprve	k6eAd1	teprve
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gInSc1	jeho
konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
blízký	blízký	k2eAgInSc1d1	blízký
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Jepice	jepice	k1gFnPc1	jepice
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
za	za	k7c2	za
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
letních	letní	k2eAgInPc2d1	letní
dnů	den	k1gInPc2	den
v	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
;	;	kIx,	;
jak	jak	k6eAd1	jak
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
noc	noc	k1gFnSc1	noc
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
můj	můj	k3xOp1gInSc4	můj
stav	stav	k1gInSc4	stav
byla	být	k5eAaImAgFnS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
obdivování	obdivování	k1gNnSc1	obdivování
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
potěšení	potěšení	k1gNnPc2	potěšení
z	z	k7c2	z
plodů	plod	k1gInPc2	plod
těchto	tento	k3xDgNnPc2	tento
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
neobratné	obratný	k2eNgNnSc4d1	neobratné
tvoření	tvoření	k1gNnSc4	tvoření
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vybranou	vybraný	k2eAgFnSc7d1	vybraná
citlivostí	citlivost	k1gFnSc7	citlivost
jsem	být	k5eAaImIp1nS	být
hledal	hledat	k5eAaImAgInS	hledat
výhledy	výhled	k1gInPc4	výhled
na	na	k7c4	na
krásnou	krásný	k2eAgFnSc4d1	krásná
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
jsem	být	k5eAaImIp1nS	být
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Krajiny	Krajina	k1gFnPc1	Krajina
rozehrávaly	rozehrávat	k5eAaImAgFnP	rozehrávat
mou	můj	k3xOp1gFnSc4	můj
duši	duše	k1gFnSc4	duše
jako	jako	k8xC	jako
smyčec	smyčec	k1gInSc4	smyčec
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
i	i	k9	i
pohledy	pohled	k1gInPc1	pohled
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgNnPc6	jenž
dosud	dosud	k6eAd1	dosud
nikdo	nikdo	k3yNnSc1	nikdo
nepsal	psát	k5eNaImAgMnS	psát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
Henriho	Henri	k1gMnSc2	Henri
Brularda	Brulard	k1gMnSc2	Brulard
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Vidím	vidět	k5eAaImIp1nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejraději	rád	k6eAd3	rád
snění	snění	k1gNnSc2	snění
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jsem	být	k5eAaImIp1nS	být
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
úspěchem	úspěch	k1gInSc7	úspěch
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
Henriho	Henri	k1gMnSc2	Henri
Brularda	Brulard	k1gMnSc2	Brulard
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Vidím	vidět	k5eAaImIp1nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jsem	být	k5eAaImIp1nS	být
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
povětšinou	povětšinou	k6eAd1	povětšinou
pouhé	pouhý	k2eAgFnPc1d1	pouhá
krtiny	krtina	k1gFnPc1	krtina
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
jsem	být	k5eAaImIp1nS	být
přišel	přijít	k5eAaPmAgMnS	přijít
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
Henriho	Henri	k1gMnSc2	Henri
Brularda	Brulard	k1gMnSc2	Brulard
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
mám	mít	k5eAaImIp1nS	mít
nejaristokratičtější	aristokratický	k2eAgFnPc4d3	aristokratický
záliby	záliba	k1gFnPc4	záliba
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgMnS	udělat
bych	by	kYmCp1nS	by
pro	pro	k7c4	pro
blaho	blaho	k1gNnSc4	blaho
lidu	lid	k1gInSc2	lid
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bych	by	kYmCp1nS	by
raději	rád	k6eAd2	rád
polovinu	polovina	k1gFnSc4	polovina
každého	každý	k3xTgInSc2	každý
měsíce	měsíc	k1gInSc2	měsíc
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
než	než	k8xS	než
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
Henriho	Henri	k1gMnSc2	Henri
Brularda	Brulard	k1gMnSc2	Brulard
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
bez	bez	k7c2	bez
jistého	jistý	k2eAgInSc2d1	jistý
stupně	stupeň	k1gInSc2	stupeň
nestydatosti	nestydatost	k1gFnSc2	nestydatost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Egoistické	egoistický	k2eAgFnPc4d1	egoistická
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
poznat	poznat	k5eAaPmF	poznat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
sebe	sebe	k3xPyFc4	sebe
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Egoistické	egoistický	k2eAgFnPc1d1	egoistická
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
)	)	kIx)	)
</s>
