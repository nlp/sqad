<s>
Aristoxenos	Aristoxenos	k1gInSc1	Aristoxenos
z	z	k7c2	z
Taranta	Tarant	k1gMnSc2	Tarant
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Ἀ	Ἀ	k?	Ἀ
Aristóxenos	Aristóxenos	k1gInSc1	Aristóxenos
<g/>
;	;	kIx,	;
*	*	kIx~	*
kolem	kolem	k7c2	kolem
360	[number]	k4	360
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
Tarantu	Tarant	k1gInSc6	Tarant
–	–	k?	–
asi	asi	k9	asi
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
?	?	kIx.	?
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
.	.	kIx.	.
</s>
