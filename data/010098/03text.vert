<p>
<s>
Aristoxenos	Aristoxenos	k1gInSc1	Aristoxenos
z	z	k7c2	z
Taranta	Tarant	k1gMnSc2	Tarant
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Ἀ	Ἀ	k?	Ἀ
Aristóxenos	Aristóxenos	k1gInSc1	Aristóxenos
<g/>
;	;	kIx,	;
*	*	kIx~	*
kolem	kolem	k7c2	kolem
360	[number]	k4	360
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
Tarantu	Tarant	k1gInSc6	Tarant
–	–	k?	–
asi	asi	k9	asi
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
?	?	kIx.	?
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
hudební	hudební	k2eAgFnSc2d1	hudební
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
svému	své	k1gNnSc3	své
spisu	spis	k1gInSc2	spis
Harmonikon	Harmonikon	k1gInSc1	Harmonikon
stoicheion	stoicheion	k1gInSc1	stoicheion
(	(	kIx(	(
<g/>
Ἁ	Ἁ	k?	Ἁ
σ	σ	k?	σ
<g/>
,	,	kIx,	,
Základy	základ	k1gInPc1	základ
hudební	hudební	k2eAgFnSc2d1	hudební
teorie	teorie	k1gFnSc2	teorie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
fragmenty	fragment	k1gInPc1	fragment
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
filozofických	filozofický	k2eAgInPc2d1	filozofický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
Spintara	Spintar	k1gMnSc2	Spintar
<g/>
,	,	kIx,	,
Sokratova	Sokratův	k2eAgMnSc2d1	Sokratův
žáka	žák	k1gMnSc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
u	u	k7c2	u
pythagorejce	pythagorejec	k1gMnSc2	pythagorejec
Xenofila	Xenofil	k1gMnSc2	Xenofil
z	z	k7c2	z
Chalkidiky	Chalkidika	k1gFnSc2	Chalkidika
a	a	k8xC	a
učitele	učitel	k1gMnSc2	učitel
hudby	hudba	k1gFnSc2	hudba
Lampra	Lampr	k1gMnSc2	Lampr
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
byl	být	k5eAaImAgMnS	být
žákem	žák	k1gMnSc7	žák
Aristotelovým	Aristotelův	k2eAgMnSc7d1	Aristotelův
a	a	k8xC	a
příslušníkem	příslušník	k1gMnSc7	příslušník
peripatetické	peripatetický	k2eAgFnSc2d1	peripatetická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
spisy	spis	k1gInPc4	spis
jsou	být	k5eAaImIp3nP	být
nejstarší	starý	k2eAgNnPc1d3	nejstarší
dochovaná	dochovaný	k2eAgNnPc1d1	dochované
antická	antický	k2eAgNnPc1d1	antické
pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
středověkého	středověký	k2eAgInSc2d1	středověký
slovníku	slovník	k1gInSc2	slovník
Suda	Suda	k1gMnSc1	Suda
se	se	k3xPyFc4	se
po	po	k7c6	po
Aristotelově	Aristotelův	k2eAgFnSc6d1	Aristotelova
smrti	smrt	k1gFnSc6	smrt
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
s	s	k7c7	s
hořkostí	hořkost	k1gFnSc7	hořkost
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
když	když	k9	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
určil	určit	k5eAaPmAgMnS	určit
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
peripatetické	peripatetický	k2eAgFnSc2d1	peripatetická
školy	škola	k1gFnSc2	škola
Theofrasta	Theofrasta	k1gFnSc1	Theofrasta
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
aristotelovský	aristotelovský	k2eAgMnSc1d1	aristotelovský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
Aristoklés	Aristoklés	k1gInSc1	Aristoklés
z	z	k7c2	z
Messeny	Messen	k2eAgInPc1d1	Messen
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dějinách	dějiny	k1gFnPc6	dějiny
filozofie	filozofie	k1gFnSc1	filozofie
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nepotvrzuje	potvrzovat	k5eNaImIp3nS	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Suda	Suda	k1gMnSc1	Suda
Aristoxenovi	Aristoxen	k1gMnSc3	Aristoxen
rovněž	rovněž	k6eAd1	rovněž
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
sotva	sotva	k6eAd1	sotva
uvěřitelný	uvěřitelný	k2eAgInSc1d1	uvěřitelný
počet	počet	k1gInSc1	počet
453	[number]	k4	453
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Aristoxenos	Aristoxenos	k1gInSc1	Aristoxenos
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
definoval	definovat	k5eAaBmAgInS	definovat
na	na	k7c6	na
čistě	čistě	k6eAd1	čistě
hudebním	hudební	k2eAgInSc6d1	hudební
základě	základ	k1gInSc6	základ
následující	následující	k2eAgInPc4d1	následující
výrazy	výraz	k1gInPc4	výraz
<g/>
:	:	kIx,	:
interval	interval	k1gInSc4	interval
<g/>
,	,	kIx,	,
tónový	tónový	k2eAgInSc1d1	tónový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
půltón	půltón	k1gInSc1	půltón
<g/>
,	,	kIx,	,
třetinotón	třetinotón	k1gInSc1	třetinotón
<g/>
,	,	kIx,	,
čtvrttón	čtvrttón	k1gInSc1	čtvrttón
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
diatonický	diatonický	k2eAgInSc1d1	diatonický
<g/>
,	,	kIx,	,
chromatický	chromatický	k2eAgInSc1d1	chromatický
a	a	k8xC	a
enharmonický	enharmonický	k2eAgInSc1d1	enharmonický
tónorod	tónorod	k1gInSc1	tónorod
<g/>
,	,	kIx,	,
trvání	trvání	k1gNnPc1	trvání
<g/>
,	,	kIx,	,
rytmus	rytmus	k1gInSc1	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
pozdější	pozdní	k2eAgFnSc2d2	pozdější
hudební	hudební	k2eAgFnSc2d1	hudební
terminologie	terminologie	k1gFnSc2	terminologie
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
antiky	antika	k1gFnSc2	antika
a	a	k8xC	a
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
terminologie	terminologie	k1gFnSc2	terminologie
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
částečně	částečně	k6eAd1	částečně
s	s	k7c7	s
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Aristoxenos	Aristoxenos	k1gMnSc1	Aristoxenos
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
Britannice	Britannika	k1gFnSc6	Britannika
</s>
</p>
