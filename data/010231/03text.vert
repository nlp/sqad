<p>
<s>
György	Györg	k1gInPc1	Györg
Klapka	klapka	k1gFnSc1	klapka
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Klapka	Klapka	k1gMnSc1	Klapka
György	Györg	k1gInPc4	Györg
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Georg	Georg	k1gInSc1	Georg
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
George	George	k1gFnSc1	George
Klapka	klapka	k1gFnSc1	klapka
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
Temešvár	Temešvár	k1gInSc1	Temešvár
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
uherské	uherský	k2eAgFnSc2d1	uherská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
sedmihradském	sedmihradský	k2eAgNnSc6d1	sedmihradské
městě	město	k1gNnSc6	město
Temešvár	Temešvár	k1gInSc1	Temešvár
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
následně	následně	k6eAd1	následně
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
byl	být	k5eAaImAgMnS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
habsburské	habsburský	k2eAgFnSc2d1	habsburská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
elitní	elitní	k2eAgFnSc2d1	elitní
uherské	uherský	k2eAgFnSc2d1	uherská
gardy	garda	k1gFnSc2	garda
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
uherské	uherský	k2eAgFnSc2d1	uherská
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
uherským	uherský	k2eAgMnPc3d1	uherský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
organizování	organizování	k1gNnSc6	organizování
uherské	uherský	k2eAgFnSc2d1	uherská
národní	národní	k2eAgFnSc2d1	národní
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Pešti	Pešť	k1gFnSc6	Pešť
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
generála	generál	k1gMnSc4	generál
Lázára	Lázár	k1gMnSc4	Lázár
Mészárose	Mészárosa	k1gFnSc6	Mészárosa
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
vojska	vojsko	k1gNnSc2	vojsko
byla	být	k5eAaImAgFnS	být
poraženy	poražen	k2eAgInPc4d1	poražen
u	u	k7c2	u
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
honvédskému	honvédský	k2eAgInSc3d1	honvédský
sboru	sbor	k1gInSc3	sbor
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
18	[number]	k4	18
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
operujícímu	operující	k2eAgMnSc3d1	operující
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
území	území	k1gNnSc6	území
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
1849	[number]	k4	1849
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
nad	nad	k7c7	nad
habsburskými	habsburský	k2eAgMnPc7d1	habsburský
vojsky	vojsky	k6eAd1	vojsky
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Kápolna	Kápolno	k1gNnSc2	Kápolno
<g/>
,	,	kIx,	,
Isaszegu	Isaszeg	k1gInSc2	Isaszeg
<g/>
,	,	kIx,	,
Vacova	Vacovo	k1gNnSc2	Vacovo
<g/>
,	,	kIx,	,
Tekovských	Tekovský	k2eAgInPc2d1	Tekovský
Lužan	Lužany	k1gInPc2	Lužany
a	a	k8xC	a
Komárna	Komárno	k1gNnSc2	Komárno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
když	když	k8xS	když
hlavní	hlavní	k2eAgFnPc1d1	hlavní
síly	síla	k1gFnPc1	síla
uherské	uherský	k2eAgFnSc2d1	uherská
armády	armáda	k1gFnSc2	armáda
odtáhly	odtáhnout	k5eAaPmAgInP	odtáhnout
z	z	k7c2	z
Komárna	Komárno	k1gNnSc2	Komárno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Artura	Artur	k1gMnSc2	Artur
Görgeye	Görgey	k1gMnSc2	Görgey
byl	být	k5eAaImAgInS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
obranou	obrana	k1gFnSc7	obrana
pevnosti	pevnost	k1gFnSc2	pevnost
Komárno	Komárno	k1gNnSc4	Komárno
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Komárna	Komárno	k1gNnSc2	Komárno
habsburským	habsburský	k2eAgNnSc7d1	habsburské
vojskem	vojsko	k1gNnSc7	vojsko
trvalo	trvat	k5eAaImAgNnS	trvat
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
až	až	k6eAd1	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c4	o
kapitulaci	kapitulace	k1gFnSc4	kapitulace
uherských	uherský	k2eAgMnPc2d1	uherský
povstalců	povstalec	k1gMnPc2	povstalec
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Világoše	Világoš	k1gMnSc2	Világoš
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Şiria	Şiria	k1gFnSc1	Şiria
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
výhodných	výhodný	k2eAgFnPc2d1	výhodná
podmínek	podmínka	k1gFnPc2	podmínka
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klapka	Klapka	k1gMnSc1	Klapka
následně	následně	k6eAd1	následně
opustil	opustit	k5eAaPmAgMnS	opustit
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lajosem	Lajos	k1gMnSc7	Lajos
Kossuthem	Kossuth	k1gInSc7	Kossuth
politicky	politicky	k6eAd1	politicky
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
opětovné	opětovný	k2eAgFnSc2d1	opětovná
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prusko-rakouské	pruskoakouský	k2eAgFnSc2d1	prusko-rakouská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Pruska	Prusko	k1gNnSc2	Prusko
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
zformování	zformování	k1gNnSc4	zformování
uherské	uherský	k2eAgFnSc2d1	uherská
legie	legie	k1gFnSc2	legie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pruské	pruský	k2eAgFnSc2d1	pruská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
jednotku	jednotka	k1gFnSc4	jednotka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
3000	[number]	k4	3000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
a	a	k8xC	a
rakouská	rakouský	k2eAgNnPc1d1	rakouské
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
poražena	porazit	k5eAaPmNgNnP	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
umožněn	umožněn	k2eAgInSc4d1	umožněn
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Ference	Ferenc	k1gMnSc2	Ferenc
Deáka	Deáek	k1gMnSc2	Deáek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
reformy	reforma	k1gFnPc4	reforma
v	v	k7c6	v
Turecké	turecký	k2eAgFnSc6d1	turecká
armádě	armáda	k1gFnSc6	armáda
v	v	k7c6	v
období	období	k1gNnSc6	období
hrozící	hrozící	k2eAgFnSc2d1	hrozící
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
psaní	psaní	k1gNnSc1	psaní
memoárů	memoáry	k1gInPc2	memoáry
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
historii	historie	k1gFnSc4	historie
vojenství	vojenství	k1gNnPc4	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Komárně	Komárno	k1gNnSc6	Komárno
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
památník	památník	k1gInSc1	památník
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jerome	Jerom	k1gInSc5	Jerom
Klapka	Klapka	k1gMnSc1	Klapka
Jerome	Jerom	k1gInSc5	Jerom
dostal	dostat	k5eAaPmAgMnS	dostat
prostřední	prostřední	k2eAgNnSc1d1	prostřední
jméno	jméno	k1gNnSc1	jméno
Klapka	klapka	k1gFnSc1	klapka
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
rodinného	rodinný	k2eAgMnSc2d1	rodinný
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
generála	generál	k1gMnSc2	generál
Klapky	Klapka	k1gMnSc2	Klapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Memoiren	Memoirna	k1gFnPc2	Memoirna
<g/>
.	.	kIx.	.
</s>
<s>
April	April	k1gInSc1	April
bis	bis	k?	bis
Oktober	Oktober	k1gInSc1	Oktober
1849	[number]	k4	1849
(	(	kIx(	(
<g/>
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Nationalkrieg	Nationalkrieg	k1gInSc1	Nationalkrieg
in	in	k?	in
Ungarn	Ungarn	k1gInSc1	Ungarn
und	und	k?	und
Siebenbürgen	Siebenbürgen	k1gInSc1	Siebenbürgen
in	in	k?	in
den	den	k1gInSc4	den
Jahren	Jahrna	k1gFnPc2	Jahrna
1848	[number]	k4	1848
und	und	k?	und
1849	[number]	k4	1849
(	(	kIx(	(
<g/>
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krieg	Krieg	k1gInSc1	Krieg
im	im	k?	im
Orient	Orient	k1gInSc1	Orient
in	in	k?	in
den	den	k1gInSc4	den
Jahren	Jahrna	k1gFnPc2	Jahrna
1853	[number]	k4	1853
und	und	k?	und
1854	[number]	k4	1854
bis	bis	k?	bis
Ende	Ende	k1gInSc1	Ende
Juli	Juli	k1gNnSc1	Juli
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k6eAd1	Eine
historisch-kritische	historischritischat	k5eAaPmIp3nS	historisch-kritischat
Skizze	Skizze	k1gFnSc1	Skizze
der	drát	k5eAaImRp2nS	drát
Feldzüge	Feldzüge	k1gInSc1	Feldzüge
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gInSc1	donau
<g/>
,	,	kIx,	,
in	in	k?	in
Asien	Asien	k1gInSc1	Asien
und	und	k?	und
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Krim	Krim	k1gMnSc1	Krim
<g/>
,	,	kIx,	,
mit	mit	k?	mit
einem	einem	k1gInSc1	einem
Blick	Blick	k1gInSc1	Blick
auf	auf	k?	auf
die	die	k?	die
mögliche	möglichat	k5eAaPmIp3nS	möglichat
Wendung	Wendung	k1gInSc1	Wendung
der	drát	k5eAaImRp2nS	drát
künftigen	künftigen	k1gInSc4	künftigen
Kriegsereignisse	Kriegsereigniss	k1gMnSc2	Kriegsereigniss
(	(	kIx(	(
<g/>
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aus	Aus	k?	Aus
meinen	meinen	k1gInSc1	meinen
Erinnerungen	Erinnerungen	k1gInSc1	Erinnerungen
(	(	kIx(	(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
z	z	k7c2	z
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
György	Györg	k1gInPc4	Györg
Klapka	klapka	k1gFnSc1	klapka
na	na	k7c4	na
anglické	anglický	k2eAgFnPc4d1	anglická
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
a	a	k8xC	a
György	Györg	k1gInPc4	Györg
Klapka	klapka	k1gFnSc1	klapka
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Slovenska	Slovensko	k1gNnSc2	Slovensko
III	III	kA	III
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
,	,	kIx,	,
K-	K-	k1gFnSc1	K-
<g/>
M.	M.	kA	M.
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
VEDA	vést	k5eAaImSgInS	vést
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
s.	s.	k?	s.
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
György	Györg	k1gInPc4	Györg
Klapka	klapka	k1gFnSc1	klapka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
