<s>
György	Györg	k1gInPc1	Györg
Klapka	klapka	k1gFnSc1	klapka
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Klapka	Klapka	k1gMnSc1	Klapka
György	Györg	k1gInPc4	Györg
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Georg	Georg	k1gInSc1	Georg
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
George	George	k1gFnSc1	George
Klapka	klapka	k1gFnSc1	klapka
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
Temešvár	Temešvár	k1gInSc1	Temešvár
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
uherské	uherský	k2eAgFnSc2d1	uherská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
