<s>
Zárubeň	zárubeň	k1gFnSc1	zárubeň
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
veřej	veřej	k1gFnSc1	veřej
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
futro	futro	k1gNnSc4	futro
nebo	nebo	k8xC	nebo
futra	futro	k1gNnPc4	futro
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Futter	Futter	k1gMnSc1	Futter
<g/>
,	,	kIx,	,
Türfutter	Türfutter	k1gMnSc1	Türfutter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rám	rám	k1gInSc4	rám
pevně	pevně	k6eAd1	pevně
umístěný	umístěný	k2eAgInSc4d1	umístěný
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
otvor	otvor	k1gInSc4	otvor
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jejich	jejich	k3xOp3gFnSc4	jejich
nosnou	nosný	k2eAgFnSc4d1	nosná
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
