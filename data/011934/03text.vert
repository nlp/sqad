<p>
<s>
Zárubeň	zárubeň	k1gFnSc1	zárubeň
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
veřej	veřej	k1gFnSc1	veřej
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
futro	futro	k1gNnSc4	futro
nebo	nebo	k8xC	nebo
futra	futro	k1gNnPc4	futro
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Futter	Futter	k1gMnSc1	Futter
<g/>
,	,	kIx,	,
Türfutter	Türfutter	k1gMnSc1	Türfutter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rám	rám	k1gInSc4	rám
pevně	pevně	k6eAd1	pevně
umístěný	umístěný	k2eAgInSc4d1	umístěný
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
otvor	otvor	k1gInSc4	otvor
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jejich	jejich	k3xOp3gFnSc4	jejich
nosnou	nosný	k2eAgFnSc4d1	nosná
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
kovový	kovový	k2eAgMnSc1d1	kovový
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgMnS	být
dřevěný	dřevěný	k2eAgMnSc1d1	dřevěný
nebo	nebo	k8xC	nebo
kamenný	kamenný	k2eAgMnSc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zárubni	zárubeň	k1gFnSc6	zárubeň
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
závěsů	závěs	k1gInPc2	závěs
(	(	kIx(	(
<g/>
pantů	pant	k1gInPc2	pant
<g/>
)	)	kIx)	)
zavěšeno	zavěšen	k2eAgNnSc4d1	zavěšeno
křídlo	křídlo	k1gNnSc4	křídlo
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
zárubně	zárubeň	k1gFnSc2	zárubeň
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
závěsy	závěs	k1gInPc1	závěs
<g/>
,	,	kIx,	,
těsnění	těsnění	k1gNnSc1	těsnění
a	a	k8xC	a
protiplech	protiplo	k1gNnPc6	protiplo
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
do	do	k7c2	do
zárubně	zárubeň	k1gFnSc2	zárubeň
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
dveře	dveře	k1gFnPc1	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svislé	svislý	k2eAgFnPc1d1	svislá
části	část	k1gFnPc1	část
zárubně	zárubeň	k1gFnSc2	zárubeň
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
ostění	ostění	k1gNnSc4	ostění
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
podvoj	podvoj	k1gFnSc1	podvoj
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
horní	horní	k2eAgNnSc1d1	horní
nadpraží	nadpraží	k1gNnSc1	nadpraží
<g/>
.	.	kIx.	.
</s>
<s>
Zárubeň	zárubeň	k1gFnSc1	zárubeň
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
pevně	pevně	k6eAd1	pevně
uchycena	uchycen	k2eAgFnSc1d1	uchycena
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
u	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
typů	typ	k1gInPc2	typ
zárubní	zárubeň	k1gFnPc2	zárubeň
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
zhlaví	zhlaví	k1gNnSc4	zhlaví
nebo	nebo	k8xC	nebo
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
okraje	okraj	k1gInPc1	okraj
prahu	práh	k1gInSc2	práh
a	a	k8xC	a
nadpraží	nadpraží	k1gNnSc2	nadpraží
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgFnSc4d1	zasahující
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
tesařské	tesařský	k2eAgFnSc2d1	tesařská
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
)	)	kIx)	)
zárubně	zárubeň	k1gFnPc1	zárubeň
byly	být	k5eAaImAgFnP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
tzv.	tzv.	kA	tzv.
hrubou	hrubý	k2eAgFnSc7d1	hrubá
zárubní	zárubeň	k1gFnSc7	zárubeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obkládala	obkládat	k5eAaImAgFnS	obkládat
obrubou	obruba	k1gFnSc7	obruba
(	(	kIx(	(
<g/>
šambránou	šambrána	k1gFnSc7	šambrána
<g/>
)	)	kIx)	)
z	z	k7c2	z
hladce	hladko	k6eAd1	hladko
hoblovaných	hoblovaný	k2eAgNnPc2d1	hoblované
prken	prkno	k1gNnPc2	prkno
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
různě	různě	k6eAd1	různě
profilovaných	profilovaný	k2eAgFnPc2d1	profilovaná
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
obložení	obložení	k1gNnSc1	obložení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
obíjení	obíjení	k1gNnSc1	obíjení
zárubní	zárubeň	k1gFnPc2	zárubeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
zárubní	zárubeň	k1gFnPc2	zárubeň
==	==	k?	==
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
u	u	k7c2	u
dveří	dveře	k1gFnPc2	dveře
se	se	k3xPyFc4	se
u	u	k7c2	u
zárubně	zárubeň	k1gFnSc2	zárubeň
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pravé	pravý	k2eAgNnSc4d1	pravé
a	a	k8xC	a
levé	levý	k2eAgNnSc4d1	levé
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pravou	pravý	k2eAgFnSc4d1	pravá
či	či	k8xC	či
levou	levý	k2eAgFnSc4d1	levá
zárubeň	zárubeň	k1gFnSc4	zárubeň
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podívat	podívat	k5eAaPmF	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
zárubeň	zárubeň	k1gFnSc4	zárubeň
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
závěsy	závěs	k1gInPc1	závěs
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
budou	být	k5eAaImBp3nP	být
otevírat	otevírat	k5eAaImF	otevírat
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pravá	pravý	k2eAgFnSc1d1	pravá
zárubeň	zárubeň	k1gFnSc1	zárubeň
(	(	kIx(	(
<g/>
závěsy	závěsa	k1gFnPc1	závěsa
jsou	být	k5eAaImIp3nP	být
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
levá	levý	k2eAgFnSc1d1	levá
zárubeň	zárubeň	k1gFnSc1	zárubeň
(	(	kIx(	(
<g/>
závěsy	závěsa	k1gFnPc1	závěsa
jsou	být	k5eAaImIp3nP	být
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
Zárubně	zárubeň	k1gFnPc1	zárubeň
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
vzhledu	vzhled	k1gInSc2	vzhled
po	po	k7c6	po
zabudování	zabudování	k1gNnSc6	zabudování
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
zárubeň	zárubeň	k1gFnSc1	zárubeň
lemuje	lemovat	k5eAaImIp3nS	lemovat
stavební	stavební	k2eAgInSc4d1	stavební
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
před	před	k7c4	před
líc	líc	k1gFnSc4	líc
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
v	v	k7c6	v
zavřeném	zavřený	k2eAgInSc6d1	zavřený
stavu	stav	k1gInSc6	stav
nelícují	lícovat	k5eNaImIp3nP	lícovat
se	s	k7c7	s
zárubní	zárubeň	k1gFnSc7	zárubeň
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zapadají	zapadat	k5eAaImIp3nP	zapadat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
pouze	pouze	k6eAd1	pouze
napůl	napůl	k6eAd1	napůl
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
obvodě	obvod	k1gInSc6	obvod
polodrážku	polodrážka	k1gFnSc4	polodrážka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
odlišných	odlišný	k2eAgNnPc2d1	odlišné
moderních	moderní	k2eAgNnPc2d1	moderní
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zlepšit	zlepšit	k5eAaPmF	zlepšit
vzhled	vzhled	k1gInSc4	vzhled
zárubní	zárubeň	k1gFnPc2	zárubeň
a	a	k8xC	a
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tradiční	tradiční	k2eAgFnSc1d1	tradiční
zárubeň	zárubeň	k1gFnSc1	zárubeň
(	(	kIx(	(
<g/>
nelícuje	lícovat	k5eNaImIp3nS	lícovat
se	se	k3xPyFc4	se
zdí	zeď	k1gFnSc7	zeď
-	-	kIx~	-
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
ze	z	k7c2	z
zárubně	zárubeň	k1gFnSc2	zárubeň
polovinou	polovina	k1gFnSc7	polovina
tloušťky	tloušťka	k1gFnSc2	tloušťka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
polodrážka	polodrážka	k1gFnSc1	polodrážka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zárubeň	zárubeň	k1gFnSc1	zárubeň
pro	pro	k7c4	pro
dveře	dveře	k1gFnPc4	dveře
bez	bez	k7c2	bez
polodrážky	polodrážka	k1gFnSc2	polodrážka
(	(	kIx(	(
<g/>
zárubeň	zárubeň	k1gFnSc1	zárubeň
nelícuje	lícovat	k5eNaImIp3nS	lícovat
se	s	k7c7	s
zdí	zeď	k1gFnSc7	zeď
-	-	kIx~	-
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
zapuštěny	zapustit	k5eAaPmNgInP	zapustit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
lícují	lícovat	k5eAaImIp3nP	lícovat
se	s	k7c7	s
zárubní	zárubeň	k1gFnSc7	zárubeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zárubeň	zárubeň	k1gFnSc1	zárubeň
zapuštěná	zapuštěný	k2eAgFnSc1d1	zapuštěná
(	(	kIx(	(
<g/>
povrch	povrch	k7c2wR	povrch
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
zárubeň	zárubeň	k1gFnSc4	zárubeň
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
přední	přední	k2eAgNnSc4d1	přední
čelo	čelo	k1gNnSc4	čelo
dveří	dveře	k1gFnPc2	dveře
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zárubeň	zárubeň	k1gFnSc1	zárubeň
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
(	(	kIx(	(
<g/>
zárubeň	zárubeň	k1gFnSc1	zárubeň
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
překryta	překrýt	k5eAaPmNgFnS	překrýt
omítkou	omítka	k1gFnSc7	omítka
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
obchodních	obchodní	k2eAgInPc6d1	obchodní
materiálech	materiál	k1gInPc6	materiál
prodejců	prodejce	k1gMnPc2	prodejce
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
různé	různý	k2eAgInPc1d1	různý
nejednotné	jednotný	k2eNgInPc1d1	nejednotný
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
tvarové	tvarový	k2eAgFnPc4d1	tvarová
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zárubně	zárubeň	k1gFnSc2	zárubeň
"	"	kIx"	"
<g/>
skryté	skrytý	k2eAgFnPc1d1	skrytá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bezobložkové	bezobložkový	k2eAgFnSc2d1	bezobložkový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
stěny	stěna	k1gFnSc2	stěna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
zárubní	zárubeň	k1gFnPc2	zárubeň
dle	dle	k7c2	dle
materiálu	materiál	k1gInSc2	materiál
==	==	k?	==
</s>
</p>
<p>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
zárubně	zárubeň	k1gFnPc1	zárubeň
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
zárubně	zárubeň	k1gFnPc1	zárubeň
(	(	kIx(	(
<g/>
rámové	rámový	k2eAgFnPc1d1	rámová
<g/>
,	,	kIx,	,
tesařské	tesařský	k2eAgFnPc1d1	tesařská
<g/>
,	,	kIx,	,
fošnové	fošnový	k2eAgFnPc1d1	fošnová
truhlářské	truhlářský	k2eAgFnPc1d1	truhlářská
<g/>
,	,	kIx,	,
obložkové	obložkový	k2eAgFnPc1d1	obložková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ocelové	ocelový	k2eAgFnPc1d1	ocelová
zárubně	zárubeň	k1gFnPc1	zárubeň
</s>
</p>
<p>
<s>
Plastové	plastový	k2eAgInPc1d1	plastový
či	či	k8xC	či
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
zárubněPodle	zárubněPodle	k6eAd1	zárubněPodle
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
též	též	k9	též
liší	lišit	k5eAaImIp3nS	lišit
i	i	k9	i
osazování	osazování	k1gNnSc1	osazování
zárubní	zárubeň	k1gFnPc2	zárubeň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
návod	návod	k1gInSc1	návod
na	na	k7c6	na
osazování	osazování	k1gNnSc6	osazování
zárubní	zárubeň	k1gFnPc2	zárubeň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souvisejícím	související	k2eAgInSc6d1	související
článku	článek	k1gInSc6	článek
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Příruční	příruční	k2eAgInSc1d1	příruční
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
827	[number]	k4	827
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
zárubeň	zárubeň	k1gFnSc1	zárubeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Technický	technický	k2eAgInSc1d1	technický
naučný	naučný	k2eAgInSc1d1	naučný
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
V.	V.	kA	V.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
–	–	k?	–
<g/>
Ž	Ž	kA	Ž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
technické	technický	k2eAgFnSc2d1	technická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
494	[number]	k4	494
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
zárubeň	zárubeň	k1gFnSc1	zárubeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Technický	technický	k2eAgInSc1d1	technický
naučný	naučný	k2eAgInSc1d1	naučný
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
–	–	k?	–
<g/>
Po	Po	kA	Po
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
technické	technický	k2eAgFnSc2d1	technická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
287	[number]	k4	287
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
obíjení	obíjení	k1gNnSc2	obíjení
zárubní	zárubeň	k1gFnPc2	zárubeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
Mgr.	Mgr.	kA	Mgr.
Václav	Václav	k1gMnSc1	Václav
Podlena	Podlena	k1gFnSc1	Podlena
<g/>
.	.	kIx.	.
</s>
<s>
TECHNOLOGIE	technologie	k1gFnSc1	technologie
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
ročník	ročník	k1gInSc1	ročník
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
<g/>
:	:	kIx,	:
Zednické	zednický	k2eAgFnSc2d1	zednická
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
PARTA	parta	k1gFnSc1	parta
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7320	[number]	k4	7320
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
osazování	osazování	k1gNnSc1	osazování
výrobků	výrobek	k1gInPc2	výrobek
HSV	HSV	kA	HSV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pant	pant	k1gInSc1	pant
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Osazování	osazování	k1gNnSc2	osazování
zárubní	zárubeň	k1gFnPc2	zárubeň
ve	v	k7c6	v
Wikiknihách	Wikiknih	k1gInPc6	Wikiknih
</s>
</p>
