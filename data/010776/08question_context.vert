<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1271	[number]	k4	1271
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
Staré	Stará	k1gFnSc6	Stará
Město	město	k1gNnSc4	město
pražské	pražský	k2eAgNnSc4d1	Pražské
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
šestý	šestý	k4xOgInSc4	šestý
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
první	první	k4xOgMnSc1	první
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
<g/>
.	.	kIx.	.
</s>
