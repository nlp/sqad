<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1271	[number]	k4	1271
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
Staré	Stará	k1gFnSc6	Stará
Město	město	k1gNnSc4	město
pražské	pražský	k2eAgNnSc4d1	Pražské
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
šestý	šestý	k4xOgInSc4	šestý
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
první	první	k4xOgMnSc1	první
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Přemyslově	Přemyslův	k2eAgFnSc6d1	Přemyslova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
se	se	k3xPyFc4	se
poručníkem	poručník	k1gMnSc7	poručník
sedmiletého	sedmiletý	k2eAgMnSc4d1	sedmiletý
Václava	Václav	k1gMnSc4	Václav
stal	stát	k5eAaPmAgMnS	stát
Ota	Ota	k1gMnSc1	Ota
V.	V.	kA	V.
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odvézt	odvézt	k5eAaPmF	odvézt
chlapce	chlapec	k1gMnSc4	chlapec
do	do	k7c2	do
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
zmatků	zmatek	k1gInPc2	zmatek
<g/>
,	,	kIx,	,
hladomoru	hladomor	k1gInSc2	hladomor
a	a	k8xC	a
nejisté	jistý	k2eNgFnSc2d1	nejistá
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
Ota	Ota	k1gMnSc1	Ota
s	s	k7c7	s
Václavovým	Václavův	k2eAgInSc7d1	Václavův
návratem	návrat	k1gInSc7	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
však	však	k9	však
hrál	hrát	k5eAaImAgMnS	hrát
nový	nový	k2eAgMnSc1d1	nový
partner	partner	k1gMnSc1	partner
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
matky	matka	k1gFnSc2	matka
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závišův	Závišův	k2eAgInSc1d1	Závišův
vliv	vliv	k1gInSc1	vliv
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
dočasný	dočasný	k2eAgInSc1d1	dočasný
<g/>
,	,	kIx,	,
dospívající	dospívající	k2eAgMnSc1d1	dospívající
Václav	Václav	k1gMnSc1	Václav
jej	on	k3xPp3gInSc4	on
uvěznil	uvěznit	k5eAaPmAgMnS	uvěznit
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
popravě	poprava	k1gFnSc6	poprava
zahájil	zahájit	k5eAaPmAgMnS	zahájit
expanzi	expanze	k1gFnSc4	expanze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
podmanit	podmanit	k5eAaPmF	podmanit
Malopolsko	Malopolsko	k1gNnSc1	Malopolsko
a	a	k8xC	a
také	také	k9	také
získat	získat	k5eAaPmF	získat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
tchána	tchán	k1gMnSc2	tchán
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
nepodpořil	podpořit	k5eNaPmAgInS	podpořit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
Albrechta	Albrecht	k1gMnSc2	Albrecht
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prosadil	prosadit	k5eAaPmAgMnS	prosadit
volbu	volba	k1gFnSc4	volba
hraběte	hrabě	k1gMnSc2	hrabě
Adolfa	Adolf	k1gMnSc2	Adolf
Nasavského	Nasavský	k2eAgMnSc2d1	Nasavský
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
však	však	k9	však
nesplnil	splnit	k5eNaPmAgMnS	splnit
své	svůj	k3xOyFgInPc4	svůj
předvolební	předvolební	k2eAgInPc4d1	předvolební
sliby	slib	k1gInPc4	slib
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
a	a	k8xC	a
nové	nový	k2eAgFnSc3d1	nová
volbě	volba	k1gFnSc3	volba
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
územní	územní	k2eAgFnSc2d1	územní
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1300	[number]	k4	1300
získal	získat	k5eAaPmAgMnS	získat
polskou	polský	k2eAgFnSc4d1	polská
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
i	i	k9	i
korunu	koruna	k1gFnSc4	koruna
uherskou	uherský	k2eAgFnSc4d1	uherská
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
protičeské	protičeský	k2eAgFnSc2d1	protičeská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
kromě	kromě	k7c2	kromě
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Albrechta	Albrecht	k1gMnSc2	Albrecht
stanul	stanout	k5eAaPmAgInS	stanout
i	i	k9	i
pretendent	pretendent	k1gMnSc1	pretendent
uherského	uherský	k2eAgInSc2d1	uherský
trůnu	trůn	k1gInSc2	trůn
Karel	Karel	k1gMnSc1	Karel
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Anjou	Anja	k1gFnSc7	Anja
a	a	k8xC	a
soupeř	soupeř	k1gMnSc1	soupeř
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1304	[number]	k4	1304
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
nepřátelská	přátelský	k2eNgNnPc1d1	nepřátelské
vojska	vojsko	k1gNnPc4	vojsko
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
Kutnou	kutný	k2eAgFnSc4d1	Kutná
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
Václava	Václav	k1gMnSc4	Václav
strhnout	strhnout	k5eAaPmF	strhnout
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
a	a	k8xC	a
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
dobývání	dobývání	k1gNnSc6	dobývání
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
s	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
zimou	zima	k1gFnSc7	zima
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vítězství	vítězství	k1gNnSc4	vítězství
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
Václav	Václav	k1gMnSc1	Václav
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zhoršující	zhoršující	k2eAgFnSc1d1	zhoršující
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
již	již	k9	již
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
činila	činit	k5eAaImAgFnS	činit
značné	značný	k2eAgFnPc4d1	značná
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jeho	jeho	k3xOp3gFnSc4	jeho
předčasnou	předčasný	k2eAgFnSc4d1	předčasná
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
spíše	spíše	k9	spíše
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
než	než	k8xS	než
válečné	válečný	k2eAgNnSc4d1	válečné
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaPmAgMnS	využívat
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stříbra	stříbro	k1gNnPc1	stříbro
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nacházelo	nacházet	k5eAaImAgNnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Zavedením	zavedení	k1gNnSc7	zavedení
pražského	pražský	k2eAgInSc2d1	pražský
groše	groš	k1gInSc2	groš
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
stimulovat	stimulovat	k5eAaImF	stimulovat
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
vytvořením	vytvoření	k1gNnSc7	vytvoření
nového	nový	k2eAgInSc2d1	nový
horního	horní	k2eAgInSc2d1	horní
zákoníku	zákoník	k1gInSc2	zákoník
Ius	Ius	k1gMnSc2	Ius
regale	regale	k6eAd1	regale
montanorum	montanorum	k1gNnSc4	montanorum
se	se	k3xPyFc4	se
definovala	definovat	k5eAaBmAgFnS	definovat
role	role	k1gFnSc1	role
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
hornické	hornický	k2eAgFnSc3d1	hornická
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
literárně	literárně	k6eAd1	literárně
činný	činný	k2eAgMnSc1d1	činný
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
velkým	velký	k2eAgMnSc7d1	velký
mecenášem	mecenáš	k1gMnSc7	mecenáš
minesengrů	minesengr	k1gMnPc2	minesengr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
jako	jako	k9	jako
mnoho	mnoho	k4c4	mnoho
let	let	k1gInSc4	let
očekávaný	očekávaný	k2eAgMnSc1d1	očekávaný
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Václavova	Václavův	k2eAgFnSc1d1	Václavova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Uherská	uherský	k2eAgFnSc1d1	uherská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Rostislava	Rostislav	k1gMnSc2	Rostislav
Haličského	haličský	k2eAgMnSc2d1	haličský
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
a	a	k8xC	a
vnučka	vnučka	k1gFnSc1	vnučka
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Bély	Béla	k1gMnSc2	Béla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zděděných	zděděný	k2eAgFnPc2d1	zděděná
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
získal	získat	k5eAaPmAgMnS	získat
postupně	postupně	k6eAd1	postupně
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
Furlánsko	Furlánsko	k1gNnSc1	Furlánsko
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1273	[number]	k4	1273
zvolen	zvolit	k5eAaPmNgMnS	zvolit
německým	německý	k2eAgMnSc7d1	německý
králem	král	k1gMnSc7	král
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
velký	velký	k2eAgInSc1d1	velký
revindikační	revindikační	k2eAgInSc1d1	revindikační
program	program	k1gInSc1	program
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vybudovat	vybudovat	k5eAaPmF	vybudovat
si	se	k3xPyFc3	se
novou	nový	k2eAgFnSc4d1	nová
rodovou	rodový	k2eAgFnSc4d1	rodová
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
nepodřídil	podřídit	k5eNaPmAgMnS	podřídit
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
Rudolf	Rudolf	k1gMnSc1	Rudolf
říšskou	říšský	k2eAgFnSc4d1	říšská
klatbu	klatba	k1gFnSc4	klatba
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
poprvé	poprvé	k6eAd1	poprvé
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1276	[number]	k4	1276
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oslabený	oslabený	k2eAgMnSc1d1	oslabený
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
poddal	poddat	k5eAaPmAgMnS	poddat
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
a	a	k8xC	a
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
s	s	k7c7	s
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
smlouva	smlouva	k1gFnSc1	smlouva
znamenala	znamenat	k5eAaImAgFnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
alpských	alpský	k2eAgFnPc2d1	alpská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Chebska	Chebsko	k1gNnSc2	Chebsko
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
části	část	k1gFnSc2	část
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c6	na
levobřežní	levobřežní	k2eAgFnSc6d1	levobřežní
straně	strana	k1gFnSc6	strana
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dědic	dědic	k1gMnSc1	dědic
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
Václav	Václav	k1gMnSc1	Václav
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
zasnouben	zasnouben	k2eAgMnSc1d1	zasnouben
s	s	k7c7	s
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Uklidnění	uklidnění	k1gNnSc1	uklidnění
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
zdánlivé	zdánlivý	k2eAgNnSc1d1	zdánlivé
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
následovalo	následovat	k5eAaImAgNnS	následovat
nové	nový	k2eAgNnSc1d1	nové
kolo	kolo	k1gNnSc1	kolo
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgMnPc7d1	znepřátelený
panovníky	panovník	k1gMnPc7	panovník
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
padl	padnout	k5eAaPmAgInS	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regentství	regentství	k1gNnSc4	regentství
Oty	Ota	k1gMnSc2	Ota
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dobách	doba	k1gFnPc6	doba
po	po	k7c6	po
manželově	manželův	k2eAgInSc6d1	manželův
skonu	skon	k1gInSc6	skon
viděla	vidět	k5eAaImAgFnS	vidět
královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
a	a	k8xC	a
hledala	hledat	k5eAaImAgFnS	hledat
oporu	opora	k1gFnSc4	opora
u	u	k7c2	u
Oty	Ota	k1gMnSc2	Ota
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
příbuzenských	příbuzenský	k2eAgInPc2d1	příbuzenský
vztahů	vztah	k1gInPc2	vztah
zde	zde	k6eAd1	zde
sehrály	sehrát	k5eAaPmAgFnP	sehrát
roli	role	k1gFnSc4	role
i	i	k8xC	i
dřívější	dřívější	k2eAgFnPc4d1	dřívější
osobní	osobní	k2eAgFnPc4d1	osobní
dohody	dohoda	k1gFnPc4	dohoda
mezi	mezi	k7c7	mezi
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Otou	Ota	k1gMnSc7	Ota
V.	V.	kA	V.
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
braniborských	braniborský	k2eAgNnPc2d1	braniborské
markrabat	markrabě	k1gNnPc2	markrabě
zamezit	zamezit	k5eAaPmF	zamezit
další	další	k2eAgFnSc4d1	další
expanzi	expanze	k1gFnSc4	expanze
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
mezitím	mezitím	k6eAd1	mezitím
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
přebírat	přebírat	k5eAaImF	přebírat
moc	moc	k6eAd1	moc
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Habsburskou	habsburský	k2eAgFnSc4d1	habsburská
vládu	vláda	k1gFnSc4	vláda
uznala	uznat	k5eAaPmAgNnP	uznat
mnohá	mnohý	k2eAgNnPc1d1	mnohé
moravská	moravský	k2eAgNnPc1d1	Moravské
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Hlubčice	hlubčice	k1gFnPc1	hlubčice
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
získat	získat	k5eAaPmF	získat
i	i	k9	i
olomouckého	olomoucký	k2eAgMnSc4d1	olomoucký
biskupa	biskup	k1gMnSc4	biskup
Bruna	Bruno	k1gMnSc4	Bruno
<g/>
.	.	kIx.	.
</s>
<s>
Dočasný	dočasný	k2eAgInSc1d1	dočasný
zábor	zábor	k1gInSc1	zábor
Moravy	Morava	k1gFnSc2	Morava
byl	být	k5eAaImAgInS	být
zdůvodněn	zdůvodnit	k5eAaPmNgInS	zdůvodnit
náhradou	náhrada	k1gFnSc7	náhrada
dluhů	dluh	k1gInPc2	dluh
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
40	[number]	k4	40
000	[number]	k4	000
marek	marka	k1gFnPc2	marka
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
musel	muset	k5eAaImAgMnS	muset
Rudolf	Rudolf	k1gMnSc1	Rudolf
obětovat	obětovat	k5eAaBmF	obětovat
na	na	k7c6	na
zajištění	zajištění	k1gNnSc6	zajištění
obrany	obrana	k1gFnSc2	obrana
před	před	k7c7	před
vpádem	vpád	k1gInSc7	vpád
českých	český	k2eAgNnPc2d1	české
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
vůči	vůči	k7c3	vůči
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
osvědčeného	osvědčený	k2eAgNnSc2d1	osvědčené
schématu	schéma	k1gNnSc2	schéma
konsensuální	konsensuální	k2eAgFnSc2d1	konsensuální
politiky	politika	k1gFnSc2	politika
vůči	vůči	k7c3	vůči
biskupům	biskup	k1gMnPc3	biskup
<g/>
,	,	kIx,	,
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
nepůsobil	působit	k5eNaImAgMnS	působit
jako	jako	k9	jako
spojenec	spojenec	k1gMnSc1	spojenec
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
silného	silný	k2eAgInSc2d1	silný
vojenského	vojenský	k2eAgInSc2d1	vojenský
kontingentu	kontingent	k1gInSc2	kontingent
obsadil	obsadit	k5eAaPmAgInS	obsadit
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
navázal	navázat	k5eAaPmAgInS	navázat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
měšťanstvem	měšťanstvo	k1gNnSc7	měšťanstvo
i	i	k8xC	i
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vybudovat	vybudovat	k5eAaPmF	vybudovat
si	se	k3xPyFc3	se
v	v	k7c6	v
království	království	k1gNnSc6	království
vlastní	vlastní	k2eAgFnSc2d1	vlastní
silné	silný	k2eAgFnSc2d1	silná
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
královnu	královna	k1gFnSc4	královna
zaskočilo	zaskočit	k5eAaPmAgNnS	zaskočit
a	a	k8xC	a
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
požádala	požádat	k5eAaPmAgFnS	požádat
dalšího	další	k2eAgMnSc4d1	další
Přemyslova	Přemyslův	k2eAgMnSc4d1	Přemyslův
příbuzného	příbuzný	k1gMnSc4	příbuzný
<g/>
,	,	kIx,	,
vratislavského	vratislavský	k2eAgMnSc4d1	vratislavský
vévodu	vévoda	k1gMnSc4	vévoda
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Probuse	Probuse	k1gFnSc1	Probuse
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
vychováván	vychovávat	k5eAaImNgInS	vychovávat
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
Čáslavsku	Čáslavsko	k1gNnSc6	Čáslavsko
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zřejmě	zřejmě	k6eAd1	zřejmě
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
i	i	k8xC	i
části	část	k1gFnPc4	část
východočeské	východočeský	k2eAgFnSc2d1	Východočeská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zneklidnil	zneklidnit	k5eAaPmAgMnS	zneklidnit
Rudolfa	Rudolf	k1gMnSc4	Rudolf
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
k	k	k7c3	k
vojenskému	vojenský	k2eAgNnSc3d1	vojenské
tažení	tažení	k1gNnSc3	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
své	svůj	k3xOyFgFnSc3	svůj
říšské	říšský	k2eAgFnSc3d1	říšská
spojence	spojenka	k1gFnSc3	spojenka
a	a	k8xC	a
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
česko-moravského	českooravský	k2eAgNnSc2d1	česko-moravské
pomezí	pomezí	k1gNnSc2	pomezí
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
utábořil	utábořit	k5eAaPmAgMnS	utábořit
na	na	k7c6	na
Čáslavsku	Čáslavsko	k1gNnSc6	Čáslavsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
navázala	navázat	k5eAaPmAgFnS	navázat
spojení	spojení	k1gNnSc2	spojení
královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
slíbila	slíbit	k5eAaPmAgFnS	slíbit
přijmout	přijmout	k5eAaPmF	přijmout
ochranu	ochrana	k1gFnSc4	ochrana
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Požadovala	požadovat	k5eAaImAgFnS	požadovat
však	však	k9	však
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
vévoda	vévoda	k1gMnSc1	vévoda
uznán	uznat	k5eAaPmNgMnS	uznat
poručníkem	poručník	k1gMnSc7	poručník
malého	malý	k1gMnSc2	malý
dědice	dědic	k1gMnSc2	dědic
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
se	se	k3xPyFc4	se
Rudolfa	Rudolf	k1gMnSc4	Rudolf
nezalekl	zaleknout	k5eNaPmAgInS	zaleknout
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
českými	český	k2eAgMnPc7d1	český
spojenci	spojenec	k1gMnPc7	spojenec
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
ke	k	k7c3	k
Kolínu	Kolín	k1gInSc3	Kolín
a	a	k8xC	a
zahradil	zahradit	k5eAaPmAgMnS	zahradit
mu	on	k3xPp3gMnSc3	on
cestu	cesta	k1gFnSc4	cesta
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Hrozící	hrozící	k2eAgFnSc4d1	hrozící
srážku	srážka	k1gFnSc4	srážka
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určila	určit	k5eAaPmAgFnS	určit
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Poručnictví	poručnictví	k1gNnSc1	poručnictví
nad	nad	k7c7	nad
Václavem	Václav	k1gMnSc7	Václav
(	(	kIx(	(
<g/>
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	let	k1gInSc4	let
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
<g/>
,	,	kIx,	,
Moravu	Morava	k1gFnSc4	Morava
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
Probus	Probus	k1gMnSc1	Probus
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c4	na
blíže	blízce	k6eAd2	blízce
neurčenou	určený	k2eNgFnSc4d1	neurčená
dobu	doba	k1gFnSc4	doba
Kladsko	Kladsko	k1gNnSc1	Kladsko
<g/>
.	.	kIx.	.
</s>
<s>
Kunhutě	Kunhuta	k1gFnSc3	Kunhuta
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgInP	určit
příjmy	příjem	k1gInPc1	příjem
na	na	k7c6	na
Opavsku	Opavsko	k1gNnSc6	Opavsko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
proti	proti	k7c3	proti
nemanželskému	manželský	k2eNgMnSc3d1	nemanželský
synu	syn	k1gMnSc3	syn
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
krále	král	k1gMnPc4	král
Mikuláši	mikuláš	k1gInPc7	mikuláš
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
Opavsku	Opavsko	k1gNnSc6	Opavsko
zaručené	zaručený	k2eAgInPc1d1	zaručený
příjmy	příjem	k1gInPc1	příjem
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těchto	tento	k3xDgInPc2	tento
peněz	peníze	k1gInPc2	peníze
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
vykoupení	vykoupení	k1gNnSc3	vykoupení
z	z	k7c2	z
uherského	uherský	k2eAgNnSc2d1	Uherské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
dohodu	dohoda	k1gFnSc4	dohoda
měly	mít	k5eAaImAgFnP	mít
také	také	k6eAd1	také
garantovat	garantovat	k5eAaBmF	garantovat
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
manželské	manželský	k2eAgFnSc2d1	manželská
aliance	aliance	k1gFnSc2	aliance
mezi	mezi	k7c7	mezi
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
,	,	kIx,	,
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
Askánci	Askánec	k1gMnPc7	Askánec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
české	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
obnovit	obnovit	k5eAaPmF	obnovit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Probus	Probus	k1gMnSc1	Probus
poté	poté	k6eAd1	poté
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
úmluvami	úmluva	k1gFnPc7	úmluva
zabral	zabrat	k5eAaPmAgMnS	zabrat
i	i	k9	i
Broumovsko	Broumovsko	k1gNnSc4	Broumovsko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1278	[number]	k4	1278
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
či	či	k8xC	či
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
k	k	k7c3	k
dvojitým	dvojitý	k2eAgFnPc3d1	dvojitá
dětským	dětský	k2eAgFnPc3d1	dětská
zásnubám	zásnuba	k1gFnPc3	zásnuba
či	či	k8xC	či
sňatku	sňatek	k1gInSc3	sňatek
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
Gutou	Guta	k1gFnSc7	Guta
Habsburskou	habsburský	k2eAgFnSc7d1	habsburská
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Anežkou	Anežka	k1gFnSc7	Anežka
a	a	k8xC	a
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
sestrou	sestra	k1gFnSc7	sestra
Anežkou	Anežka	k1gFnSc7	Anežka
začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
1279	[number]	k4	1279
převezen	převézt	k5eAaPmNgMnS	převézt
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Bezděz	Bezděz	k1gInSc4	Bezděz
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
královna	královna	k1gFnSc1	královna
svolení	svolení	k1gNnSc4	svolení
k	k	k7c3	k
návštěvě	návštěva	k1gFnSc3	návštěva
hrobu	hrob	k1gInSc2	hrob
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
Přemysla	Přemysl	k1gMnSc4	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
zanechala	zanechat	k5eAaPmAgFnS	zanechat
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
Oty	Ota	k1gMnSc2	Ota
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
královny	královna	k1gFnSc2	královna
konstituoval	konstituovat	k5eAaBmAgInS	konstituovat
malý	malý	k2eAgInSc1d1	malý
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
většina	většina	k1gFnSc1	většina
představitelů	představitel	k1gMnPc2	představitel
lokálních	lokální	k2eAgFnPc2d1	lokální
elit	elita	k1gFnPc2	elita
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Milotou	milota	k1gFnSc7	milota
z	z	k7c2	z
Dědic	Dědice	k1gFnPc2	Dědice
<g/>
,	,	kIx,	,
Vokem	Vokum	k1gNnSc7	Vokum
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Herbordem	Herbord	k1gInSc7	Herbord
z	z	k7c2	z
Fulštejna	Fulštejn	k1gInSc2	Fulštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
sblížila	sblížit	k5eAaPmAgFnS	sblížit
se	s	k7c7	s
Závišem	Záviš	k1gMnSc7	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
a	a	k8xC	a
nejpozději	pozdě	k6eAd3	pozdě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
jej	on	k3xPp3gInSc4	on
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
purkrabím	purkrabí	k1gMnSc7	purkrabí
na	na	k7c6	na
Hradci	Hradec	k1gInSc6	Hradec
u	u	k7c2	u
Opavy	Opava	k1gFnSc2	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Mikuláše	mikuláš	k1gInSc2	mikuláš
Opavského	opavský	k2eAgInSc2d1	opavský
z	z	k7c2	z
uherského	uherský	k2eAgNnSc2d1	Uherské
zajetí	zajetí	k1gNnSc2	zajetí
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
na	na	k7c6	na
Opavsku	Opavsko	k1gNnSc6	Opavsko
ujal	ujmout	k5eAaPmAgInS	ujmout
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
Záviš	Záviš	k1gMnSc1	Záviš
s	s	k7c7	s
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
Opavsko	Opavsko	k1gNnSc4	Opavsko
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1282	[number]	k4	1282
se	se	k3xPyFc4	se
dvojici	dvojice	k1gFnSc3	dvojice
na	na	k7c6	na
neznámém	známý	k2eNgNnSc6d1	neznámé
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
braniborského	braniborský	k2eAgNnSc2d1	braniborské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Bezdězu	Bezděz	k1gInSc2	Bezděz
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1279	[number]	k4	1279
převezen	převézt	k5eAaPmNgMnS	převézt
nejdříve	dříve	k6eAd3	dříve
do	do	k7c2	do
Žitavy	Žitava	k1gFnSc2	Žitava
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
Špandavy	Špandava	k1gFnSc2	Špandava
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
autory	autor	k1gMnPc4	autor
tak	tak	k6eAd1	tak
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
strádání	strádání	k1gNnSc1	strádání
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
poněkud	poněkud	k6eAd1	poněkud
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
Václavova	Václavův	k2eAgNnSc2d1	Václavovo
svatořečení	svatořečení	k1gNnSc2	svatořečení
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
královy	králův	k2eAgInPc4d1	králův
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
Braniborským	braniborský	k2eAgMnSc7d1	braniborský
nevypovídají	vypovídat	k5eNaPmIp3nP	vypovídat
o	o	k7c6	o
týrání	týrání	k1gNnSc6	týrání
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
také	také	k9	také
ještě	ještě	k6eAd1	ještě
žila	žít	k5eAaImAgFnS	žít
Václavova	Václavův	k2eAgFnSc1d1	Václavova
teta	teta	k1gFnSc1	teta
a	a	k8xC	a
Otova	Otův	k2eAgFnSc1d1	Otova
matka	matka	k1gFnSc1	matka
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stížnostech	stížnost	k1gFnPc6	stížnost
královny	královna	k1gFnSc2	královna
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
u	u	k7c2	u
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1280	[number]	k4	1280
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
nová	nový	k2eAgFnSc1d1	nová
výprava	výprava	k1gFnSc1	výprava
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytlačení	vytlačení	k1gNnSc1	vytlačení
braniborských	braniborský	k2eAgNnPc2d1	braniborské
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
obnova	obnova	k1gFnSc1	obnova
legitimní	legitimní	k2eAgFnSc2d1	legitimní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
stále	stále	k6eAd1	stále
nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
část	část	k1gFnSc1	část
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
elit	elita	k1gFnPc2	elita
byla	být	k5eAaImAgFnS	být
zájmově	zájmově	k6eAd1	zájmově
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
Braniborským	braniborský	k2eAgMnSc7d1	braniborský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgInSc7d1	jediný
výsledkem	výsledek	k1gInSc7	výsledek
tažení	tažení	k1gNnSc2	tažení
Otův	Otův	k2eAgInSc4d1	Otův
příslib	příslib	k1gInSc4	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václava	Václav	k1gMnSc4	Václav
přiveze	přivézt	k5eAaPmIp3nS	přivézt
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
však	však	k9	však
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgFnPc4d1	válečná
události	událost	k1gFnPc4	událost
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
nejistota	nejistota	k1gFnSc1	nejistota
způsobily	způsobit	k5eAaPmAgFnP	způsobit
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
nedostatečné	nedostatečná	k1gFnSc2	nedostatečná
obhospodařování	obhospodařování	k1gNnSc2	obhospodařování
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
snižování	snižování	k1gNnSc1	snižování
zásob	zásoba	k1gFnPc2	zásoba
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1282	[number]	k4	1282
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
i	i	k8xC	i
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
k	k	k7c3	k
hladomoru	hladomor	k1gInSc3	hladomor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žalostný	žalostný	k2eAgInSc1d1	žalostný
stav	stav	k1gInSc1	stav
rozvráceného	rozvrácený	k2eAgNnSc2d1	rozvrácené
království	království	k1gNnSc2	království
aktivizoval	aktivizovat	k5eAaImAgMnS	aktivizovat
část	část	k1gFnSc4	část
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vynutila	vynutit	k5eAaPmAgFnS	vynutit
Václavovo	Václavův	k2eAgNnSc4d1	Václavovo
vydání	vydání	k1gNnSc4	vydání
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
smlouval	smlouvat	k5eAaImAgMnS	smlouvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
stále	stále	k6eAd1	stále
zvyšoval	zvyšovat	k5eAaImAgMnS	zvyšovat
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednání	jednání	k1gNnSc2	jednání
uspokojeny	uspokojen	k2eAgFnPc1d1	uspokojena
částí	část	k1gFnSc7	část
severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
Václavův	Václavův	k2eAgMnSc1d1	Václavův
rádce	rádce	k1gMnSc1	rádce
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
nezištný	zištný	k2eNgMnSc1d1	nezištný
příznivec	příznivec	k1gMnSc1	příznivec
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Otovy	Otův	k2eAgInPc4d1	Otův
nároky	nárok	k1gInPc4	nárok
neuznal	uznat	k5eNaPmAgMnS	uznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gInSc4	on
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1283	[number]	k4	1283
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k9	jako
příslib	příslib	k1gInSc1	příslib
lepších	dobrý	k2eAgInPc2d2	lepší
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boj	boj	k1gInSc1	boj
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
stran	strana	k1gFnPc2	strana
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
převzala	převzít	k5eAaPmAgFnS	převzít
vládu	vláda	k1gFnSc4	vláda
šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
vykoupení	vykoupení	k1gNnSc6	vykoupení
z	z	k7c2	z
internace	internace	k1gFnSc2	internace
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Tobiáš	Tobiáš	k1gMnSc1	Tobiáš
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
příbuzní	příbuzný	k1gMnPc1	příbuzný
Čeněk	Čeněk	k1gMnSc1	Čeněk
z	z	k7c2	z
Kamenice	Kamenice	k1gFnSc2	Kamenice
a	a	k8xC	a
Dobeš	Dobeš	k1gMnSc1	Dobeš
z	z	k7c2	z
Bechyně	Bechyně	k1gFnSc2	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Purkart	Purkarta	k1gFnPc2	Purkarta
z	z	k7c2	z
Janovic	Janovice	k1gFnPc2	Janovice
<g/>
,	,	kIx,	,
Zbyslav	Zbyslava	k1gFnPc2	Zbyslava
z	z	k7c2	z
Třebouně	Třebouna	k1gFnSc6	Třebouna
<g/>
,	,	kIx,	,
Zdeslav	Zdeslav	k1gMnSc1	Zdeslav
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
z	z	k7c2	z
Vartemberka	Vartemberka	k1gFnSc1	Vartemberka
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
není	být	k5eNaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
vynuceným	vynucený	k2eAgInSc7d1	vynucený
slibem	slib	k1gInSc7	slib
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
zaplatit	zaplatit	k5eAaPmF	zaplatit
Otovi	Ota	k1gMnSc3	Ota
Braniborskému	braniborský	k2eAgInSc3d1	braniborský
žádné	žádný	k3yNgFnPc4	žádný
odstupné	odstupné	k1gNnSc1	odstupné
za	za	k7c4	za
správu	správa	k1gFnSc4	správa
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
k	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
vrátila	vrátit	k5eAaPmAgFnS	vrátit
královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
<g/>
,	,	kIx,	,
vymohla	vymoct	k5eAaPmAgFnS	vymoct
si	se	k3xPyFc3	se
královu	králův	k2eAgFnSc4d1	králova
přízeň	přízeň	k1gFnSc4	přízeň
i	i	k8xC	i
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
milence	milenec	k1gMnSc4	milenec
Záviše	Záviš	k1gMnSc4	Záviš
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc4	jejich
syna	syn	k1gMnSc4	syn
Ješka	Ješek	k1gMnSc4	Ješek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
královniným	královnin	k2eAgInSc7d1	královnin
návratem	návrat	k1gInSc7	návrat
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
i	i	k9	i
k	k	k7c3	k
pokojnému	pokojný	k2eAgInSc3d1	pokojný
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
významné	významný	k2eAgInPc4d1	významný
úřady	úřad	k1gInPc4	úřad
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Závišovi	Závišův	k2eAgMnPc1d1	Závišův
příbuzní	příbuzný	k1gMnPc1	příbuzný
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
Vítek	Vítek	k1gMnSc1	Vítek
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
,	,	kIx,	,
Ojíř	Ojíř	k1gInSc1	Ojíř
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
<g/>
,	,	kIx,	,
Hroznata	Hroznat	k2eAgFnSc1d1	Hroznata
z	z	k7c2	z
Úžic	Úžic	k1gFnSc1	Úžic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
Lichtenburkové	Lichtenburková	k1gFnPc1	Lichtenburková
<g/>
,	,	kIx,	,
Šternberkové	Šternberkový	k2eAgFnPc1d1	Šternberkový
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnSc1d1	opoziční
šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
pokusila	pokusit	k5eAaPmAgNnP	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
ztracené	ztracený	k2eAgFnPc4d1	ztracená
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
a	a	k8xC	a
slibu	slib	k1gInSc3	slib
zachování	zachování	k1gNnSc2	zachování
dané	daný	k2eAgFnSc2d1	daná
situace	situace	k1gFnSc2	situace
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Soupeřící	soupeřící	k2eAgFnPc1d1	soupeřící
šlechtické	šlechtický	k2eAgFnPc1d1	šlechtická
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
snad	snad	k9	snad
mimo	mimo	k6eAd1	mimo
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
spíše	spíše	k9	spíše
podobu	podoba	k1gFnSc4	podoba
momentálních	momentální	k2eAgInPc2d1	momentální
nahodile	nahodile	k6eAd1	nahodile
utvářených	utvářený	k2eAgFnPc2d1	utvářená
účelových	účelový	k2eAgFnPc2d1	účelová
koalic	koalice	k1gFnPc2	koalice
než	než	k8xS	než
pevných	pevný	k2eAgFnPc2d1	pevná
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
aliancí	aliance	k1gFnPc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
vdovou	vdova	k1gFnSc7	vdova
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1285	[number]	k4	1285
se	se	k3xPyFc4	se
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
konalo	konat	k5eAaImAgNnS	konat
slavné	slavný	k2eAgNnSc1d1	slavné
završení	završení	k1gNnSc1	završení
Václavova	Václavův	k2eAgInSc2d1	Václavův
sňatku	sňatek	k1gInSc2	sňatek
s	s	k7c7	s
Gutou	Guta	k1gFnSc7	Guta
a	a	k8xC	a
následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
sblížení	sblížení	k1gNnSc3	sblížení
mezi	mezi	k7c7	mezi
Habsburky	Habsburk	k1gMnPc7	Habsburk
a	a	k8xC	a
Mikulášem	mikuláš	k1gInSc7	mikuláš
Opavským	opavský	k2eAgInSc7d1	opavský
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Rudolf	Rudolf	k1gMnSc1	Rudolf
zjevně	zjevně	k6eAd1	zjevně
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k9	jako
protiváhu	protiváha	k1gFnSc4	protiváha
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
toto	tento	k3xDgNnSc4	tento
sblížení	sblížení	k1gNnSc4	sblížení
stvrdil	stvrdit	k5eAaPmAgInS	stvrdit
i	i	k9	i
sňatek	sňatek	k1gInSc1	sňatek
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
s	s	k7c7	s
Rudolfovou	Rudolfův	k2eAgFnSc7d1	Rudolfova
vzdálenou	vzdálený	k2eAgFnSc7d1	vzdálená
příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
Adelheidou	Adelheida	k1gFnSc7	Adelheida
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Václav	Václav	k1gMnSc1	Václav
složil	složit	k5eAaPmAgMnS	složit
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
lenní	lenní	k2eAgInSc4d1	lenní
hold	hold	k1gInSc4	hold
<g/>
,	,	kIx,	,
odvedl	odvést	k5eAaPmAgMnS	odvést
si	se	k3xPyFc3	se
Rudolf	Rudolf	k1gMnSc1	Rudolf
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
titulovat	titulovat	k5eAaImF	titulovat
králem	král	k1gMnSc7	král
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgMnSc7d1	moravský
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
září	září	k1gNnSc6	září
1285	[number]	k4	1285
zemřela	zemřít	k5eAaPmAgFnS	zemřít
královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
skonem	skon	k1gInSc7	skon
nedošlo	dojít	k5eNaPmAgNnS	dojít
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
Závišem	Záviš	k1gMnSc7	Záviš
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
otčímovo	otčímův	k2eAgNnSc1d1	otčímův
privilegované	privilegovaný	k2eAgNnSc1d1	privilegované
postavení	postavení	k1gNnSc1	postavení
nebylo	být	k5eNaImAgNnS	být
nijak	nijak	k6eAd1	nijak
narušeno	narušit	k5eAaPmNgNnS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1285	[number]	k4	1285
obdaroval	obdarovat	k5eAaPmAgMnS	obdarovat
král	král	k1gMnSc1	král
Záviše	Záviš	k1gMnSc2	Záviš
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Ješka	Ješek	k1gMnSc2	Ješek
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
územím	území	k1gNnSc7	území
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
zahájili	zahájit	k5eAaPmAgMnP	zahájit
tažení	tažení	k1gNnSc4	tažení
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
obnovit	obnovit	k5eAaPmF	obnovit
královskou	královský	k2eAgFnSc4d1	královská
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
bylo	být	k5eAaImAgNnS	být
pobito	pobít	k5eAaPmNgNnS	pobít
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
lapků	lapka	k1gMnPc2	lapka
sužujících	sužující	k2eAgMnPc2d1	sužující
různé	různý	k2eAgFnPc1d1	různá
části	část	k1gFnSc2	část
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
a	a	k8xC	a
odboj	odboj	k1gInSc4	odboj
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
===	===	k?	===
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1287	[number]	k4	1287
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
konečně	konečně	k6eAd1	konečně
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
Václavova	Václavův	k2eAgFnSc1d1	Václavova
manželka	manželka	k1gFnSc1	manželka
Guta	Gut	k2eAgFnSc1d1	Guta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
dle	dle	k7c2	dle
štýrského	štýrský	k2eAgMnSc2d1	štýrský
kronikáře	kronikář	k1gMnSc2	kronikář
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
choť	choť	k1gFnSc4	choť
Anežku	Anežka	k1gFnSc4	Anežka
přijel	přijet	k5eAaPmAgMnS	přijet
mladý	mladý	k2eAgMnSc1d1	mladý
vévoda	vévoda	k1gMnSc1	vévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
královny	královna	k1gFnSc2	královna
začal	začít	k5eAaPmAgMnS	začít
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
pomalu	pomalu	k6eAd1	pomalu
ztrácet	ztrácet	k5eAaImF	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
situaci	situace	k1gFnSc4	situace
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
posílit	posílit	k5eAaPmF	posílit
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
krále	král	k1gMnSc2	král
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
novým	nový	k2eAgInSc7d1	nový
sňatkem	sňatek	k1gInSc7	sňatek
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klesající	klesající	k2eAgInSc4d1	klesající
Závišův	Závišův	k2eAgInSc4d1	Závišův
vliv	vliv	k1gInSc4	vliv
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
přepadení	přepadení	k1gNnSc4	přepadení
jeho	jeho	k3xOp3gFnSc2	jeho
svatební	svatební	k2eAgFnSc2d1	svatební
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Lichtenburka	Lichtenburka	k1gFnSc1	Lichtenburka
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1287	[number]	k4	1287
<g/>
.	.	kIx.	.
</s>
<s>
Záviš	Záviš	k1gMnSc1	Záviš
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
zachránit	zachránit	k5eAaPmF	zachránit
útěkem	útěk	k1gInSc7	útěk
do	do	k7c2	do
Opatovic	Opatovice	k1gFnPc2	Opatovice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
královou	králův	k2eAgFnSc7d1	králova
pomocí	pomoc	k1gFnSc7	pomoc
připravil	připravit	k5eAaPmAgInS	připravit
novou	nový	k2eAgFnSc4d1	nová
výpravu	výprava	k1gFnSc4	výprava
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navázal	navázat	k5eAaPmAgInS	navázat
i	i	k9	i
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Ladislavem	Ladislav	k1gMnSc7	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
opozice	opozice	k1gFnSc1	opozice
soustředěná	soustředěný	k2eAgFnSc1d1	soustředěná
kolem	kolem	k7c2	kolem
královny	královna	k1gFnSc2	královna
Guty	Guta	k1gFnSc2	Guta
využila	využít	k5eAaPmAgFnS	využít
Závišovy	Závišův	k2eAgFnPc4d1	Závišova
několikaměsíční	několikaměsíční	k2eAgFnPc4d1	několikaměsíční
nepřítomnosti	nepřítomnost	k1gFnPc4	nepřítomnost
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
aktivizaci	aktivizace	k1gFnSc3	aktivizace
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
již	již	k9	již
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
příchodu	příchod	k1gInSc2	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
úspěšně	úspěšně	k6eAd1	úspěšně
snažila	snažit	k5eAaImAgFnS	snažit
vzbudit	vzbudit	k5eAaPmF	vzbudit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
a	a	k8xC	a
Závišem	Záviš	k1gMnSc7	Záviš
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1288	[number]	k4	1288
a	a	k8xC	a
1289	[number]	k4	1289
byl	být	k5eAaImAgMnS	být
Záviš	Záviš	k1gMnSc1	Záviš
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
roku	rok	k1gInSc2	rok
1290	[number]	k4	1290
Václav	Václava	k1gFnPc2	Václava
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
právo	právo	k1gNnSc4	právo
volby	volba	k1gFnSc2	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
a	a	k8xC	a
také	také	k9	také
úřad	úřad	k1gInSc1	úřad
říšského	říšský	k2eAgMnSc2d1	říšský
číšníka	číšník	k1gMnSc2	číšník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
Václavově	Václavův	k2eAgFnSc3d1	Václavova
prosbě	prosba	k1gFnSc3	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
nejen	nejen	k6eAd1	nejen
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Rudolfa	Rudolf	k1gMnSc2	Rudolf
a	a	k8xC	a
bamberského	bamberský	k2eAgMnSc2d1	bamberský
biskupa	biskup	k1gMnSc2	biskup
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
početné	početný	k2eAgNnSc1d1	početné
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
pomoci	pomoct	k5eAaPmF	pomoct
při	při	k7c6	při
pacifikaci	pacifikace	k1gFnSc6	pacifikace
povstalých	povstalý	k2eAgInPc2d1	povstalý
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
sice	sice	k8xC	sice
Rudolf	Rudolf	k1gMnSc1	Rudolf
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
bojovníci	bojovník	k1gMnPc1	bojovník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Potlačení	potlačení	k1gNnSc1	potlačení
povstání	povstání	k1gNnSc2	povstání
napomohly	napomoct	k5eAaPmAgFnP	napomoct
i	i	k9	i
okolnosti	okolnost	k1gFnSc2	okolnost
–	–	k?	–
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
sledu	sled	k1gInSc6	sled
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
jak	jak	k6eAd1	jak
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
Probus	Probus	k1gMnSc1	Probus
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Vítkovská	vítkovský	k2eAgFnSc1d1	Vítkovská
vzpoura	vzpoura	k1gFnSc1	vzpoura
tak	tak	k6eAd1	tak
ztratila	ztratit	k5eAaPmAgFnS	ztratit
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
<g/>
Záviš	Záviš	k1gMnSc1	Záviš
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1290	[number]	k4	1290
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
jihočeských	jihočeský	k2eAgInPc2d1	jihočeský
hradů	hrad	k1gInPc2	hrad
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Opavský	opavský	k2eAgMnSc1d1	opavský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vojsko	vojsko	k1gNnSc4	vojsko
vyslané	vyslaný	k2eAgNnSc4d1	vyslané
proti	proti	k7c3	proti
vzbouřeným	vzbouřený	k2eAgInPc3d1	vzbouřený
Vítkovcům	Vítkovec	k1gInPc3	Vítkovec
vedl	vést	k5eAaImAgMnS	vést
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
pod	pod	k7c7	pod
pohrůžkou	pohrůžka	k1gFnSc7	pohrůžka
Závišovy	Závišův	k2eAgFnSc2d1	Závišova
popravy	poprava	k1gFnSc2	poprava
donutil	donutit	k5eAaPmAgInS	donutit
hejtmany	hejtman	k1gMnPc4	hejtman
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tvrzí	tvrz	k1gFnSc7	tvrz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vydali	vydat	k5eAaPmAgMnP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
vyhrotila	vyhrotit	k5eAaPmAgFnS	vyhrotit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mikulášovo	Mikulášův	k2eAgNnSc1d1	Mikulášovo
vojsko	vojsko	k1gNnSc1	vojsko
stanulo	stanout	k5eAaPmAgNnS	stanout
před	před	k7c7	před
hradem	hrad	k1gInSc7	hrad
Hluboká	Hluboká	k1gFnSc1	Hluboká
hájeným	hájený	k2eAgMnSc7d1	hájený
Závišovým	Závišův	k2eAgMnSc7d1	Závišův
bratrem	bratr	k1gMnSc7	bratr
Vítkem	Vítek	k1gMnSc7	Vítek
<g/>
.	.	kIx.	.
</s>
<s>
Vítek	Vítek	k1gMnSc1	Vítek
nehodlal	hodlat	k5eNaImAgMnS	hodlat
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
držel	držet	k5eAaImAgMnS	držet
jako	jako	k9	jako
rukojmí	rukojmí	k1gMnSc1	rukojmí
Čeňka	Čeněk	k1gMnSc2	Čeněk
z	z	k7c2	z
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc2	bratr
biskupa	biskup	k1gMnSc2	biskup
Tobiáše	Tobiáš	k1gMnSc2	Tobiáš
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
strana	strana	k1gFnSc1	strana
ovšem	ovšem	k9	ovšem
taktéž	taktéž	k?	taktéž
nebyla	být	k5eNaImAgFnS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
k	k	k7c3	k
vyjednávání	vyjednávání	k1gNnSc3	vyjednávání
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
Záviš	Záviš	k1gMnSc1	Záviš
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
poblíž	poblíž	k7c2	poblíž
Hluboké	Hluboká	k1gFnSc2	Hluboká
popraven	popravit	k5eAaPmNgInS	popravit
stětím	stětit	k5eAaImIp1nS	stětit
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
nato	nato	k6eAd1	nato
jej	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
i	i	k9	i
Čeněk	Čeněk	k1gMnSc1	Čeněk
z	z	k7c2	z
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
život	život	k1gInSc4	život
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Vítek	Vítek	k1gMnSc1	Vítek
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Závišovo	Závišův	k2eAgNnSc4d1	Závišovo
tělo	tělo	k1gNnSc4	tělo
poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
uložila	uložit	k5eAaPmAgFnS	uložit
ve	v	k7c6	v
vítkovském	vítkovský	k2eAgInSc6d1	vítkovský
rodovém	rodový	k2eAgInSc6d1	rodový
klášteře	klášter	k1gInSc6	klášter
ve	v	k7c6	v
Vyšším	vysoký	k2eAgInSc6d2	vyšší
Brodě	Brod	k1gInSc6	Brod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
samostatné	samostatný	k2eAgFnSc2d1	samostatná
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Václavův	Václavův	k2eAgInSc1d1	Václavův
dvůr	dvůr	k1gInSc1	dvůr
===	===	k?	===
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
samostatné	samostatný	k2eAgFnSc2d1	samostatná
vlády	vláda	k1gFnSc2	vláda
již	již	k6eAd1	již
po	po	k7c6	po
zajetí	zajetí	k1gNnSc6	zajetí
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
zásahem	zásah	k1gInSc7	zásah
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
výměna	výměna	k1gFnSc1	výměna
Závišových	Závišův	k2eAgMnPc2d1	Závišův
straníků	straník	k1gMnPc2	straník
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
působících	působící	k2eAgMnPc2d1	působící
v	v	k7c6	v
nejvyšších	vysoký	k2eAgInPc6d3	Nejvyšší
úřadech	úřad	k1gInPc6	úřad
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
za	za	k7c4	za
loajálnější	loajální	k2eAgMnPc4d2	loajálnější
velmože	velmož	k1gMnPc4	velmož
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
ledna	leden	k1gInSc2	leden
1289	[number]	k4	1289
opustili	opustit	k5eAaPmAgMnP	opustit
posty	post	k1gInPc4	post
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
Vítek	Vítek	k1gMnSc1	Vítek
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
a	a	k8xC	a
Závišův	Závišův	k2eAgMnSc1d1	Závišův
švagr	švagr	k1gMnSc1	švagr
Hroznata	Hroznat	k1gMnSc2	Hroznat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
místa	místo	k1gNnPc1	místo
a	a	k8xC	a
místa	místo	k1gNnPc1	místo
dalších	další	k2eAgMnPc2d1	další
Závišových	Závišův	k2eAgMnPc2d1	Závišův
lidí	člověk	k1gMnPc2	člověk
nyní	nyní	k6eAd1	nyní
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
šlechtici	šlechtic	k1gMnPc1	šlechtic
prohabsburské	prohabsburský	k2eAgFnSc2d1	prohabsburská
orientace	orientace	k1gFnSc2	orientace
jako	jako	k8xS	jako
Albrecht	Albrecht	k1gMnSc1	Albrecht
ze	z	k7c2	z
Žeberka	Žeberka	k1gFnSc1	Žeberka
nebo	nebo	k8xC	nebo
přeběhlíci	přeběhlík	k1gMnPc1	přeběhlík
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Závišových	Závišův	k2eAgMnPc2d1	Závišův
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prostřídal	prostřídat	k5eAaPmAgMnS	prostřídat
funkce	funkce	k1gFnSc2	funkce
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
komorníka	komorník	k1gMnSc2	komorník
a	a	k8xC	a
purkrabího	purkrabí	k1gMnSc2	purkrabí
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
dvora	dvůr	k1gInSc2	dvůr
nyní	nyní	k6eAd1	nyní
tvořili	tvořit	k5eAaImAgMnP	tvořit
nemanželští	manželský	k2eNgMnPc1d1	nemanželský
potomci	potomek	k1gMnPc1	potomek
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patřili	patřit	k5eAaImAgMnP	patřit
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Opavský	opavský	k2eAgMnSc1d1	opavský
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
zastávající	zastávající	k2eAgInPc1d1	zastávající
úřady	úřad	k1gInPc1	úřad
probošta	probošt	k1gMnSc2	probošt
vyšehradského	vyšehradský	k2eAgMnSc2d1	vyšehradský
a	a	k8xC	a
kancléře	kancléř	k1gMnSc2	kancléř
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
připojili	připojit	k5eAaPmAgMnP	připojit
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
lidé	člověk	k1gMnPc1	člověk
pocházející	pocházející	k2eAgMnPc1d1	pocházející
zejména	zejména	k9	zejména
z	z	k7c2	z
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k9	jistě
ne	ne	k9	ne
náhodou	náhodou	k6eAd1	náhodou
se	se	k3xPyFc4	se
na	na	k7c6	na
králově	králův	k2eAgInSc6d1	králův
dvoře	dvůr	k1gInSc6	dvůr
objevil	objevit	k5eAaPmAgInS	objevit
po	po	k7c6	po
náhlém	náhlý	k2eAgInSc6d1	náhlý
skonu	skon	k1gInSc6	skon
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Proba	Proba	k1gMnSc1	Proba
míšeňský	míšeňský	k2eAgMnSc1d1	míšeňský
probošt	probošt	k1gMnSc1	probošt
Bernard	Bernard	k1gMnSc1	Bernard
z	z	k7c2	z
Kamence	Kamenec	k1gInSc2	Kamenec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Václavovi	Václav	k1gMnSc3	Václav
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
při	při	k7c6	při
ovlivnění	ovlivnění	k1gNnSc6	ovlivnění
volby	volba	k1gFnSc2	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgMnS	být
Arnold	Arnold	k1gMnSc1	Arnold
Bamberský	bamberský	k2eAgMnSc1d1	bamberský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
na	na	k7c4	na
uklidňování	uklidňování	k1gNnSc4	uklidňování
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
po	po	k7c4	po
zatčení	zatčení	k1gNnSc4	zatčení
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1296	[number]	k4	1296
oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgMnPc4	tento
zkušené	zkušený	k2eAgMnPc4d1	zkušený
diplomaty	diplomat	k1gMnPc4	diplomat
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
a	a	k8xC	a
cisterciáčtí	cisterciácký	k2eAgMnPc1d1	cisterciácký
opaté	opata	k1gMnPc1	opata
Konrád	Konrád	k1gMnSc1	Konrád
s	s	k7c7	s
Heidenreichem	Heidenreich	k1gMnSc7	Heidenreich
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
mezi	mezi	k7c7	mezi
královskými	královský	k2eAgMnPc7d1	královský
rádci	rádce	k1gMnPc7	rádce
měl	mít	k5eAaImAgMnS	mít
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Tobiáš	Tobiáš	k1gMnSc1	Tobiáš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
protizávišovské	protizávišovský	k2eAgFnSc2d1	protizávišovský
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
Václav	Václav	k1gMnSc1	Václav
udržoval	udržovat	k5eAaImAgMnS	udržovat
s	s	k7c7	s
biskupským	biskupský	k2eAgInSc7d1	biskupský
stolcem	stolec	k1gInSc7	stolec
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
biskupové	biskup	k1gMnPc1	biskup
takového	takový	k3xDgInSc2	takový
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
Václavova	Václavův	k2eAgNnPc4d1	Václavovo
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
před	před	k7c7	před
Václavem	Václav	k1gMnSc7	Václav
vyvstal	vyvstat	k5eAaPmAgMnS	vyvstat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zklidnění	zklidnění	k1gNnSc4	zklidnění
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
opětovné	opětovný	k2eAgNnSc1d1	opětovné
nastolování	nastolování	k1gNnSc1	nastolování
pořádku	pořádek	k1gInSc2	pořádek
a	a	k8xC	a
trestání	trestání	k1gNnPc2	trestání
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
porušovali	porušovat	k5eAaImAgMnP	porušovat
zemský	zemský	k2eAgInSc4d1	zemský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začal	začít	k5eAaPmAgInS	začít
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
vymáhat	vymáhat	k5eAaImF	vymáhat
zpět	zpět	k6eAd1	zpět
majetek	majetek	k1gInSc4	majetek
nelegálně	legálně	k6eNd1	legálně
získaný	získaný	k2eAgInSc4d1	získaný
za	za	k7c4	za
Závišovy	Závišův	k2eAgFnPc4d1	Závišova
vlády	vláda	k1gFnPc4	vláda
a	a	k8xC	a
předávat	předávat	k5eAaImF	předávat
jej	on	k3xPp3gMnSc4	on
Václavovým	Václavův	k2eAgMnPc3d1	Václavův
příznivcům	příznivec	k1gMnPc3	příznivec
a	a	k8xC	a
loajálním	loajální	k2eAgMnPc3d1	loajální
šlechticům	šlechtic	k1gMnPc3	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
dokonce	dokonce	k9	dokonce
nucen	nucen	k2eAgMnSc1d1	nucen
takto	takto	k6eAd1	takto
znovu	znovu	k6eAd1	znovu
nabyté	nabytý	k2eAgNnSc4d1	nabyté
bohatství	bohatství	k1gNnSc4	bohatství
rozdat	rozdat	k5eAaPmF	rozdat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
zajistil	zajistit	k5eAaPmAgMnS	zajistit
podporu	podpora	k1gFnSc4	podpora
významnějších	významný	k2eAgMnPc2d2	významnější
velmožů	velmož	k1gMnPc2	velmož
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Závišova	Závišův	k2eAgNnSc2d1	Závišovo
panství	panství	k1gNnSc2	panství
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Dobeši	Dobeš	k1gMnSc3	Dobeš
z	z	k7c2	z
Bechyně	Bechyně	k1gFnSc2	Bechyně
<g/>
,	,	kIx,	,
značné	značný	k2eAgFnSc2d1	značná
odměny	odměna	k1gFnSc2	odměna
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
biskup	biskup	k1gMnSc1	biskup
Tobiáš	Tobiáš	k1gMnSc1	Tobiáš
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
nezasáhly	zasáhnout	k5eNaPmAgInP	zasáhnout
významnější	významný	k2eAgFnSc1d2	významnější
represe	represe	k1gFnSc1	represe
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
obratným	obratný	k2eAgNnSc7d1	obratné
vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zařadil	zařadit	k5eAaPmAgInS	zařadit
i	i	k9	i
k	k	k7c3	k
Václavově	Václavův	k2eAgInSc6d1	Václavův
dvoru	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
Závišových	Závišův	k2eAgMnPc2d1	Závišův
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
došli	dojít	k5eAaPmAgMnP	dojít
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Vítkovci	Vítkovec	k1gMnPc1	Vítkovec
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
mocí	moc	k1gFnSc7	moc
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
napjatá	napjatý	k2eAgFnSc1d1	napjatá
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
i	i	k8xC	i
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1290	[number]	k4	1290
postupně	postupně	k6eAd1	postupně
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
Václav	Václav	k1gMnSc1	Václav
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
vystavět	vystavět	k5eAaPmF	vystavět
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
cisterciácký	cisterciácký	k2eAgInSc4d1	cisterciácký
klášter	klášter	k1gInSc4	klášter
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
formálnímu	formální	k2eAgNnSc3d1	formální
založení	založení	k1gNnSc3	založení
kláštera	klášter	k1gInSc2	klášter
došlo	dojít	k5eAaPmAgNnS	dojít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
panovník	panovník	k1gMnSc1	panovník
oddával	oddávat	k5eAaImAgMnS	oddávat
zbožnému	zbožný	k2eAgNnSc3d1	zbožné
rozjímání	rozjímání	k1gNnSc3	rozjímání
<g/>
,	,	kIx,	,
uzavíral	uzavírat	k5eAaPmAgMnS	uzavírat
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
klášterních	klášterní	k2eAgFnPc2d1	klášterní
zdí	zeď	k1gFnPc2	zeď
projednávaly	projednávat	k5eAaImAgInP	projednávat
i	i	k9	i
významné	významný	k2eAgFnPc1d1	významná
státní	státní	k2eAgFnPc1d1	státní
záležitosti	záležitost	k1gFnPc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
klášter	klášter	k1gInSc1	klášter
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
Václavově	Václavův	k2eAgInSc6d1	Václavův
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
kláštera	klášter	k1gInSc2	klášter
jako	jako	k8xC	jako
místa	místo	k1gNnSc2	místo
věčného	věčný	k2eAgInSc2d1	věčný
odpočinku	odpočinek	k1gInSc2	odpočinek
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc4	jeho
příbuzné	příbuzný	k1gMnPc4	příbuzný
–	–	k?	–
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
novým	nový	k2eAgNnSc7d1	nové
přemyslovským	přemyslovský	k2eAgNnSc7d1	přemyslovské
pohřebištěm	pohřebiště	k1gNnSc7	pohřebiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expanze	expanze	k1gFnPc1	expanze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Ukazatelem	ukazatel	k1gInSc7	ukazatel
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
české	český	k2eAgFnSc6d1	Česká
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
byla	být	k5eAaImAgFnS	být
lenní	lenní	k2eAgFnSc1d1	lenní
přísaha	přísaha	k1gFnSc1	přísaha
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Bytomského	Bytomský	k2eAgMnSc2d1	Bytomský
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
knížat	kníže	k1gMnPc2wR	kníže
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1289	[number]	k4	1289
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
expanze	expanze	k1gFnSc1	expanze
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
neměla	mít	k5eNaImAgFnS	mít
směřovat	směřovat	k5eAaImF	směřovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
rozděleného	rozdělený	k2eAgInSc2d1	rozdělený
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgNnPc2d2	menší
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Drobnější	drobný	k2eAgInPc4d2	drobnější
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
polských	polský	k2eAgFnPc2d1	polská
a	a	k8xC	a
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
byly	být	k5eAaImAgInP	být
podniknuty	podniknout	k5eAaPmNgInP	podniknout
již	již	k6eAd1	již
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Václav	Václav	k1gMnSc1	Václav
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navazoval	navazovat	k5eAaImAgInS	navazovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
daleko	daleko	k6eAd1	daleko
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
východní	východní	k2eAgFnSc2d1	východní
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Bernard	Bernard	k1gMnSc1	Bernard
z	z	k7c2	z
Kamence	Kamenec	k1gInSc2	Kamenec
<g/>
,	,	kIx,	,
zkušený	zkušený	k2eAgMnSc1d1	zkušený
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
předchozího	předchozí	k2eAgNnSc2d1	předchozí
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
polské	polský	k2eAgInPc4d1	polský
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
Václavovi	Václav	k1gMnSc3	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
velkou	velký	k2eAgFnSc7d1	velká
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
příchodu	příchod	k1gInSc2	příchod
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
dvůr	dvůr	k1gInSc4	dvůr
se	se	k3xPyFc4	se
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
otevřela	otevřít	k5eAaPmAgFnS	otevřít
možnost	možnost	k1gFnSc1	možnost
důrazněji	důrazně	k6eAd2	důrazně
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
dědictví	dědictví	k1gNnSc6	dědictví
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Proba	Prob	k1gMnSc2	Prob
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
zejména	zejména	k9	zejména
Vratislavsko	Vratislavsko	k1gNnSc1	Vratislavsko
a	a	k8xC	a
Krakovsko	Krakovsko	k1gNnSc1	Krakovsko
<g/>
,	,	kIx,	,
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgInS	hodlat
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Vratislavsko	Vratislavsko	k1gNnSc1	Vratislavsko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
lehnickému	lehnický	k2eAgMnSc3d1	lehnický
vévodovi	vévoda	k1gMnSc3	vévoda
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
Tlustému	tlustý	k2eAgMnSc3d1	tlustý
<g/>
,	,	kIx,	,
Krakovsko	Krakovsko	k1gNnSc4	Krakovsko
původně	původně	k6eAd1	původně
získal	získat	k5eAaPmAgMnS	získat
vévoda	vévoda	k1gMnSc1	vévoda
Přemysl	Přemysl	k1gMnSc1	Přemysl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
trvala	trvat	k5eAaImAgFnS	trvat
jen	jen	k6eAd1	jen
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
závěti	závěť	k1gFnSc2	závěť
dostal	dostat	k5eAaPmAgInS	dostat
nazpět	nazpět	k6eAd1	nazpět
Kladsko	Kladsko	k1gNnSc4	Kladsko
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
purkrabím	purkrabí	k1gMnSc7	purkrabí
hradu	hrad	k1gInSc2	hrad
Skála	skála	k1gFnSc1	skála
na	na	k7c6	na
Krakovsku	Krakovsko	k1gNnSc6	Krakovsko
Jindřichem	Jindřich	k1gMnSc7	Jindřich
ze	z	k7c2	z
Wstowa	Wstow	k1gInSc2	Wstow
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
spojence	spojenec	k1gMnPc4	spojenec
i	i	k8xC	i
vévody	vévoda	k1gMnPc4	vévoda
Boleslava	Boleslav	k1gMnSc4	Boleslav
Opolského	opolský	k2eAgMnSc4d1	opolský
a	a	k8xC	a
Měška	Měšek	k1gMnSc4	Měšek
Těšínského	Těšínský	k1gMnSc4	Těšínský
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pod	pod	k7c4	pod
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
dostal	dostat	k5eAaPmAgMnS	dostat
většinu	většina	k1gFnSc4	většina
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
měl	mít	k5eAaImAgInS	mít
zájmy	zájem	k1gInPc7	zájem
i	i	k8xC	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
polských	polský	k2eAgNnPc6d1	polské
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
se	se	k3xPyFc4	se
střetával	střetávat	k5eAaImAgMnS	střetávat
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
braniborských	braniborský	k2eAgMnPc2d1	braniborský
Askánců	Askánec	k1gMnPc2	Askánec
v	v	k7c6	v
gdaňském	gdaňský	k2eAgNnSc6d1	Gdaňské
Pomoří	Pomoří	k1gNnSc6	Pomoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1290	[number]	k4	1290
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
na	na	k7c6	na
základě	základ	k1gInSc6	základ
lenního	lenní	k2eAgInSc2d1	lenní
slibu	slib	k1gInSc2	slib
pomořanského	pomořanský	k2eAgNnSc2d1	Pomořanské
knížete	kníže	k1gNnSc2wR	kníže
Mstivoje	Mstivoj	k1gInSc2	Mstivoj
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
Přemyslem	Přemysl	k1gMnSc7	Přemysl
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
dostal	dostat	k5eAaPmAgMnS	dostat
Přemysl	Přemysl	k1gMnSc1	Přemysl
finanční	finanční	k2eAgNnSc4d1	finanční
odškodnění	odškodnění	k1gNnSc4	odškodnění
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
dvůr	dvůr	k1gInSc1	dvůr
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
angažoval	angažovat	k5eAaBmAgInS	angažovat
i	i	k9	i
při	při	k7c6	při
uzavření	uzavření	k1gNnSc6	uzavření
sňatku	sňatek	k1gInSc2	sňatek
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
braniborského	braniborský	k2eAgMnSc2d1	braniborský
markraběte	markrabě	k1gMnSc2	markrabě
Markétou	Markéta	k1gFnSc7	Markéta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
územní	územní	k2eAgInPc4d1	územní
zisky	zisk	k1gInPc4	zisk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Pomoří	pomořit	k5eAaPmIp3nS	pomořit
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
braniborští	braniborský	k2eAgMnPc1d1	braniborský
Askánci	Askánek	k1gMnPc1	Askánek
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
plně	plně	k6eAd1	plně
nevzdali	vzdát	k5eNaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
vtáhlo	vtáhnout	k5eAaPmAgNnS	vtáhnout
do	do	k7c2	do
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
české	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
je	on	k3xPp3gMnPc4	on
pod	pod	k7c4	pod
Václavovu	Václavův	k2eAgFnSc4d1	Václavova
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
polská	polský	k2eAgFnSc1d1	polská
nobilita	nobilita	k1gFnSc1	nobilita
zpočátku	zpočátku	k6eAd1	zpočátku
neprojevovala	projevovat	k5eNaImAgFnS	projevovat
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
novému	nový	k2eAgNnSc3d1	nové
vládci	vládce	k1gMnPc7	vládce
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
upínala	upínat	k5eAaImAgFnS	upínat
své	svůj	k3xOyFgFnPc4	svůj
naděje	naděje	k1gFnPc4	naděje
k	k	k7c3	k
sandoměřskému	sandoměřský	k2eAgMnSc3d1	sandoměřský
knížeti	kníže	k1gMnSc3	kníže
Vladislavu	Vladislav	k1gMnSc3	Vladislav
Lokýtkovi	Lokýtek	k1gMnSc3	Lokýtek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Sandoměřsko	Sandoměřsko	k1gNnSc4	Sandoměřsko
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Leška	Lešek	k1gMnSc2	Lešek
Černého	Černý	k1gMnSc2	Černý
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sám	sám	k3xTgInSc1	sám
Lokýtek	lokýtek	k1gInSc1	lokýtek
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
pořádal	pořádat	k5eAaImAgInS	pořádat
proto	proto	k8xC	proto
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
loupeživé	loupeživý	k2eAgInPc4d1	loupeživý
nájezdy	nájezd	k1gInPc4	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
krakovské	krakovský	k2eAgMnPc4d1	krakovský
velmože	velmož	k1gMnPc4	velmož
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
nechtěně	chtěně	k6eNd1	chtěně
dovedl	dovést	k5eAaPmAgMnS	dovést
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
ještě	ještě	k9	ještě
proti	proti	k7c3	proti
Lokýtkovi	Lokýtek	k1gMnSc3	Lokýtek
podporovanému	podporovaný	k2eAgInSc3d1	podporovaný
také	také	k9	také
uherskými	uherský	k2eAgNnPc7d1	Uherské
vojsky	vojsko	k1gNnPc7	vojsko
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
nemohl	moct	k5eNaImAgMnS	moct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozornost	pozornost	k1gFnSc4	pozornost
české	český	k2eAgFnSc2d1	Česká
diplomacie	diplomacie	k1gFnSc2	diplomacie
se	se	k3xPyFc4	se
upírala	upírat	k5eAaImAgFnS	upírat
do	do	k7c2	do
svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
nového	nový	k2eAgMnSc2d1	nový
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
polském	polský	k2eAgNnSc6d1	polské
tažení	tažení	k1gNnSc6	tažení
tedy	tedy	k9	tedy
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
Krakovsko	Krakovsko	k1gNnSc4	Krakovsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Opolskému	opolský	k2eAgMnSc3d1	opolský
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
specifikoval	specifikovat	k5eAaBmAgMnS	specifikovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
listinách	listina	k1gFnPc6	listina
principy	princip	k1gInPc4	princip
přemyslovské	přemyslovský	k2eAgFnPc1d1	Přemyslovská
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
nově	nově	k6eAd1	nově
získaných	získaný	k2eAgNnPc6d1	získané
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
první	první	k4xOgFnSc1	první
polská	polský	k2eAgFnSc1d1	polská
magna	magen	k2eAgFnSc1d1	magna
charta	charta	k1gFnSc1	charta
zaručující	zaručující	k2eAgFnSc4d1	zaručující
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
církevních	církevní	k2eAgFnPc2d1	církevní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
dále	daleko	k6eAd2	daleko
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
radu	rada	k1gFnSc4	rada
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
krakovského	krakovský	k2eAgInSc2d1	krakovský
biskupa	biskup	k1gInSc2	biskup
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgMnPc2d2	vyšší
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
předních	přední	k2eAgMnPc2d1	přední
velmožů	velmož	k1gMnPc2	velmož
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
král	král	k1gMnSc1	král
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevypíše	vypsat	k5eNaPmIp3nS	vypsat
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
pojistil	pojistit	k5eAaPmAgInS	pojistit
sňatkem	sňatek	k1gInSc7	sňatek
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
s	s	k7c7	s
plockým	plocký	k1gMnSc7	plocký
knížetem	kníže	k1gMnSc7	kníže
Boleslavem	Boleslav	k1gMnSc7	Boleslav
<g/>
,	,	kIx,	,
protivníkem	protivník	k1gMnSc7	protivník
Vladislava	Vladislav	k1gMnSc2	Vladislav
Lokýtka	Lokýtek	k1gMnSc2	Lokýtek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volba	volba	k1gFnSc1	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
===	===	k?	===
</s>
</p>
<p>
<s>
Římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
zemřel	zemřít	k5eAaPmAgMnS	zemřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1291	[number]	k4	1291
ve	v	k7c6	v
Špýru	Špýr	k1gInSc6	Špýr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgMnS	převzít
správu	správa	k1gFnSc4	správa
říše	říš	k1gFnSc2	říš
dočasně	dočasně	k6eAd1	dočasně
rýnský	rýnský	k2eAgMnSc1d1	rýnský
falckrabě	falckrabě	k1gMnSc1	falckrabě
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
nastalé	nastalý	k2eAgFnSc2d1	nastalá
situace	situace	k1gFnSc2	situace
využil	využít	k5eAaPmAgInS	využít
a	a	k8xC	a
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
říšským	říšský	k2eAgMnSc7d1	říšský
správcem	správce	k1gMnSc7	správce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
důležitou	důležitý	k2eAgFnSc4d1	důležitá
strategickou	strategický	k2eAgFnSc4d1	strategická
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1291	[number]	k4	1291
Václav	Václav	k1gMnSc1	Václav
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
saského	saský	k2eAgMnSc2d1	saský
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
braniborského	braniborský	k2eAgMnSc2d1	braniborský
markraběte	markrabě	k1gMnSc2	markrabě
Oty	Ota	k1gMnSc2	Ota
Dlouhého	Dlouhý	k1gMnSc2	Dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
proklamovali	proklamovat	k5eAaBmAgMnP	proklamovat
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
následovat	následovat	k5eAaImF	následovat
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
volby	volba	k1gFnSc2	volba
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
českého	český	k2eAgMnSc2d1	český
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Padly	padnout	k5eAaPmAgFnP	padnout
i	i	k9	i
pochybnosti	pochybnost	k1gFnPc1	pochybnost
ohledně	ohledně	k7c2	ohledně
volby	volba	k1gFnSc2	volba
Albrechta	Albrecht	k1gMnSc2	Albrecht
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pošlapávání	pošlapávání	k1gNnSc4	pošlapávání
svobod	svoboda	k1gFnPc2	svoboda
šlechty	šlechta	k1gFnSc2	šlechta
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zemích	zem	k1gFnPc6	zem
netěšil	těšit	k5eNaImAgMnS	těšit
v	v	k7c4	v
říši	říše	k1gFnSc4	říše
velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
také	také	k9	také
podkopávala	podkopávat	k5eAaImAgFnS	podkopávat
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
habsburské	habsburský	k2eAgFnSc3d1	habsburská
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
Rakousích	Rakousy	k1gInPc6	Rakousy
a	a	k8xC	a
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ta	ten	k3xDgFnSc1	ten
dokázal	dokázat	k5eAaPmAgInS	dokázat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
útok	útok	k1gInSc1	útok
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Ondřeje	Ondřej	k1gMnSc2	Ondřej
III	III	kA	III
<g/>
.	.	kIx.	.
skončil	skončit	k5eAaPmAgInS	skončit
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Větších	veliký	k2eAgInPc2d2	veliký
úspěchů	úspěch	k1gInPc2	úspěch
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
až	až	k9	až
tři	tři	k4xCgFnPc1	tři
alpské	alpský	k2eAgFnPc1d1	alpská
oblasti	oblast	k1gFnPc1	oblast
Uri	Uri	k1gFnPc2	Uri
<g/>
,	,	kIx,	,
Schwyz	Schwyza	k1gFnPc2	Schwyza
a	a	k8xC	a
Unterwalden	Unterwaldna	k1gFnPc2	Unterwaldna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	s	k7c7	s
spojenými	spojený	k2eAgFnPc7d1	spojená
silami	síla	k1gFnPc7	síla
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
Habsburkům	Habsburk	k1gMnPc3	Habsburk
ubránit	ubránit	k5eAaPmF	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
posléze	posléze	k6eAd1	posléze
stály	stát	k5eAaImAgFnP	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
taktizoval	taktizovat	k5eAaImAgMnS	taktizovat
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
případnou	případný	k2eAgFnSc4d1	případná
volbu	volba	k1gFnSc4	volba
švagra	švagr	k1gMnSc2	švagr
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
i	i	k9	i
celková	celkový	k2eAgFnSc1d1	celková
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
nasvědčovala	nasvědčovat	k5eAaImAgFnS	nasvědčovat
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Albrecht	Albrecht	k1gMnSc1	Albrecht
skutečně	skutečně	k6eAd1	skutečně
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
zvolen	zvolit	k5eAaPmNgMnS	zvolit
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
volba	volba	k1gFnSc1	volba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1292	[number]	k4	1292
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
Bernard	Bernard	k1gMnSc1	Bernard
z	z	k7c2	z
Kamence	Kamenec	k1gInSc2	Kamenec
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
předními	přední	k2eAgMnPc7d1	přední
velmoži	velmož	k1gMnPc7	velmož
království	království	k1gNnSc1	království
–	–	k?	–
Hynkem	Hynek	k1gMnSc7	Hynek
z	z	k7c2	z
Dubé	Dubé	k1gNnSc2	Dubé
<g/>
,	,	kIx,	,
Dobešem	Dobeš	k1gMnSc7	Dobeš
z	z	k7c2	z
Bechyně	Bechyně	k1gFnSc2	Bechyně
a	a	k8xC	a
Albrechtem	Albrecht	k1gMnSc7	Albrecht
ze	z	k7c2	z
Žeberka	Žeberka	k1gFnSc1	Žeberka
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnPc1	jednání
ovšem	ovšem	k9	ovšem
neprobíhala	probíhat	k5eNaImAgNnP	probíhat
tak	tak	k6eAd1	tak
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prohabsburská	prohabsburský	k2eAgFnSc1d1	prohabsburská
strana	strana	k1gFnSc1	strana
očekávala	očekávat	k5eAaImAgFnS	očekávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
část	část	k1gFnSc1	část
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
českými	český	k2eAgMnPc7d1	český
vyslanci	vyslanec	k1gMnPc7	vyslanec
se	se	k3xPyFc4	se
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
proti	proti	k7c3	proti
Albrechtově	Albrechtův	k2eAgFnSc3d1	Albrechtova
zvolení	zvolení	k1gNnSc3	zvolení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
kurfiřty	kurfiřt	k1gMnPc7	kurfiřt
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
předáním	předání	k1gNnSc7	předání
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
mohučského	mohučský	k2eAgMnSc2d1	mohučský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
prohabsburského	prohabsburský	k2eAgMnSc2d1	prohabsburský
falckraběte	falckrabě	k1gMnSc2	falckrabě
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
očekával	očekávat	k5eAaImAgInS	očekávat
podporu	podpora	k1gFnSc4	podpora
habsburské	habsburský	k2eAgFnSc2d1	habsburská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
hraběte	hrabě	k1gMnSc2	hrabě
Adolfa	Adolf	k1gMnSc2	Adolf
Nasavského	Nasavský	k2eAgMnSc2d1	Nasavský
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zapůsobily	zapůsobit	k5eAaPmAgFnP	zapůsobit
předchozí	předchozí	k2eAgFnPc1d1	předchozí
dohody	dohoda	k1gFnPc1	dohoda
mohučského	mohučský	k2eAgMnSc2d1	mohučský
duchovního	duchovní	k1gMnSc2	duchovní
s	s	k7c7	s
kolínským	kolínský	k2eAgMnSc7d1	kolínský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
Siegfriedem	Siegfried	k1gMnSc7	Siegfried
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
Adolfa	Adolf	k1gMnSc2	Adolf
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dalšího	další	k2eAgMnSc2d1	další
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
<g/>
,	,	kIx,	,
trevírského	trevírský	k2eAgMnSc2d1	trevírský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Bohemunda	Bohemund	k1gMnSc2	Bohemund
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
nevýznamného	významný	k2eNgInSc2d1	nevýznamný
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
opřít	opřít	k5eAaPmF	opřít
o	o	k7c4	o
větší	veliký	k2eAgFnPc4d2	veliký
pozemkové	pozemkový	k2eAgFnPc4d1	pozemková
domény	doména	k1gFnPc4	doména
jako	jako	k8xS	jako
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
kurfiřty	kurfiřt	k1gMnPc4	kurfiřt
přijatelným	přijatelný	k2eAgMnSc7d1	přijatelný
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Průchodnost	průchodnost	k1gFnSc1	průchodnost
volby	volba	k1gFnSc2	volba
tohoto	tento	k3xDgMnSc2	tento
nového	nový	k2eAgMnSc2d1	nový
kandidáta	kandidát	k1gMnSc2	kandidát
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
doprovod	doprovod	k1gInSc4	doprovod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
překvapil	překvapit	k5eAaPmAgInS	překvapit
další	další	k2eAgNnPc4d1	další
říšská	říšský	k2eAgNnPc4d1	říšské
prohabsburská	prohabsburský	k2eAgNnPc4d1	prohabsburský
knížata	kníže	k1gNnPc4	kníže
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
sjela	sjet	k5eAaPmAgNnP	sjet
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
volby	volba	k1gFnSc2	volba
ve	v	k7c6	v
slavnostních	slavnostní	k2eAgInPc6d1	slavnostní
oděvech	oděv	k1gInPc6	oděv
<g/>
.	.	kIx.	.
<g/>
České	český	k2eAgFnSc3d1	Česká
diplomatické	diplomatický	k2eAgFnSc3d1	diplomatická
misi	mise	k1gFnSc3	mise
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc4	chvíle
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
arcibiskupova	arcibiskupův	k2eAgMnSc4d1	arcibiskupův
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
okamžiku	okamžik	k1gInSc6	okamžik
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
docílit	docílit	k5eAaPmF	docílit
významných	významný	k2eAgInPc2d1	významný
zisků	zisk	k1gInPc2	zisk
a	a	k8xC	a
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ujednání	ujednání	k1gNnSc3	ujednání
sňatku	sňatek	k1gInSc2	sňatek
mezi	mezi	k7c7	mezi
Adolfovým	Adolfův	k2eAgMnSc7d1	Adolfův
synem	syn	k1gMnSc7	syn
a	a	k8xC	a
Václavovou	Václavová	k1gFnSc7	Václavová
dcerou	dcera	k1gFnSc7	dcera
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
králi	král	k1gMnSc6	král
vymohl	vymoct	k5eAaPmAgMnS	vymoct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
prázdný	prázdný	k2eAgInSc1d1	prázdný
markraběcí	markraběcí	k2eAgInSc1d1	markraběcí
trůn	trůn	k1gInSc1	trůn
v	v	k7c6	v
Míšeňsku	Míšeňsko	k1gNnSc6	Míšeňsko
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc6	zem
ležící	ležící	k2eAgFnSc2d1	ležící
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
tedy	tedy	k9	tedy
obchodně	obchodně	k6eAd1	obchodně
významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
České	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
by	by	kYmCp3nS	by
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
případně	případně	k6eAd1	případně
neprokázal	prokázat	k5eNaPmAgMnS	prokázat
své	svůj	k3xOyFgInPc4	svůj
dědické	dědický	k2eAgInPc4d1	dědický
nároky	nárok	k1gInPc4	nárok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nové	Nové	k2eAgInPc1d1	Nové
územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
===	===	k?	===
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
politické	politický	k2eAgFnSc6d1	politická
situaci	situace	k1gFnSc6	situace
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vytěžit	vytěžit	k5eAaPmF	vytěžit
z	z	k7c2	z
výsledku	výsledek	k1gInSc2	výsledek
volby	volba	k1gFnSc2	volba
co	co	k3yRnSc4	co
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původním	původní	k2eAgMnPc3d1	původní
požadavkům	požadavek	k1gInPc3	požadavek
byly	být	k5eAaImAgInP	být
nyní	nyní	k6eAd1	nyní
připojeny	připojit	k5eAaPmNgInP	připojit
i	i	k9	i
případné	případný	k2eAgInPc1d1	případný
Václavovy	Václavův	k2eAgInPc1d1	Václavův
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
a	a	k8xC	a
Korutany	Korutany	k1gInPc4	Korutany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
nový	nový	k2eAgMnSc1d1	nový
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
zasadit	zasadit	k5eAaPmF	zasadit
o	o	k7c6	o
narovnání	narovnání	k1gNnSc6	narovnání
poměrů	poměr	k1gInPc2	poměr
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
<g/>
,	,	kIx,	,
Albrechtem	Albrecht	k1gMnSc7	Albrecht
a	a	k8xC	a
Menhardem	Menhard	k1gMnSc7	Menhard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolsko-Gorickým	Tyrolsko-Gorický	k2eAgInSc7d1	Tyrolsko-Gorický
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
svátku	svátek	k1gInSc2	svátek
Tří	tři	k4xCgNnPc2	tři
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
skutečnost	skutečnost	k1gFnSc1	skutečnost
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Hrozící	hrozící	k2eAgInSc1d1	hrozící
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Adolfem	Adolf	k1gMnSc7	Adolf
Nasavským	Nasavský	k2eAgMnSc7d1	Nasavský
a	a	k8xC	a
původním	původní	k2eAgMnSc7d1	původní
pretendentem	pretendent	k1gMnSc7	pretendent
trůnu	trůn	k1gInSc2	trůn
Albrechtem	Albrecht	k1gMnSc7	Albrecht
Habsburským	habsburský	k2eAgFnPc3d1	habsburská
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
níž	nízce	k6eAd2	nízce
se	se	k3xPyFc4	se
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
novému	nový	k2eAgMnSc3d1	nový
králi	král	k1gMnSc3	král
poddal	poddat	k5eAaPmAgMnS	poddat
a	a	k8xC	a
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
posílilo	posílit	k5eAaPmAgNnS	posílit
jak	jak	k6eAd1	jak
Adolfovo	Adolfův	k2eAgNnSc1d1	Adolfovo
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pozice	pozice	k1gFnSc1	pozice
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
vlastních	vlastní	k2eAgFnPc6d1	vlastní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
tento	tento	k3xDgInSc4	tento
vývoj	vývoj	k1gInSc4	vývoj
Václav	Václav	k1gMnSc1	Václav
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
plně	plně	k6eAd1	plně
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
tažením	tažení	k1gNnSc7	tažení
do	do	k7c2	do
Malopolska	Malopolsko	k1gNnSc2	Malopolsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stále	stále	k6eAd1	stále
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sandoměře	Sandoměra	k1gFnSc6	Sandoměra
konkuroval	konkurovat	k5eAaImAgInS	konkurovat
přemyslovské	přemyslovský	k2eAgMnPc4d1	přemyslovský
moci	moc	k1gFnSc2	moc
Vladislav	Vladislava	k1gFnPc2	Vladislava
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přemyslovské	přemyslovský	k2eAgNnSc1d1	přemyslovské
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1292	[number]	k4	1292
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
táboře	tábor	k1gInSc6	tábor
poblíž	poblíž	k7c2	poblíž
Opolí	Opolí	k1gNnSc2	Opolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Václav	Václav	k1gMnSc1	Václav
od	od	k7c2	od
braniborského	braniborský	k2eAgMnSc2d1	braniborský
vládce	vládce	k1gMnSc2	vládce
Oty	Ota	k1gMnSc2	Ota
Dlouhého	Dlouhý	k1gMnSc2	Dlouhý
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
šlechtici	šlechtic	k1gMnPc7	šlechtic
obdržel	obdržet	k5eAaPmAgInS	obdržet
rytířský	rytířský	k2eAgInSc1d1	rytířský
pás	pás	k1gInSc1	pás
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
lenní	lenní	k2eAgInSc4d1	lenní
hold	hold	k1gInSc4	hold
opolských	opolský	k2eAgMnPc2d1	opolský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
tažení	tažení	k1gNnSc1	tažení
netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Sandoměř	Sandoměř	k1gFnSc1	Sandoměř
záhy	záhy	k6eAd1	záhy
padla	padnout	k5eAaImAgFnS	padnout
a	a	k8xC	a
Lokýtek	lokýtek	k1gInSc1	lokýtek
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgMnSc1d1	donucen
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Sieradzu	Sieradz	k1gInSc2	Sieradz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
města	město	k1gNnSc2	město
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
Lokýtek	lokýtek	k1gInSc1	lokýtek
zřekl	zřeknout	k5eAaPmAgInS	zřeknout
svých	svůj	k3xOyFgInPc2	svůj
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Krakovsko	Krakovsko	k1gNnSc4	Krakovsko
a	a	k8xC	a
Sandoměřsko	Sandoměřsko	k1gNnSc4	Sandoměřsko
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
Václavovi	Václav	k1gMnSc3	Václav
slib	slib	k1gInSc4	slib
věrnosti	věrnost	k1gFnPc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správě	správa	k1gFnSc6	správa
nově	nově	k6eAd1	nově
získaných	získaný	k2eAgNnPc2d1	získané
území	území	k1gNnPc2	území
zavedla	zavést	k5eAaPmAgFnS	zavést
česká	český	k2eAgFnSc1d1	Česká
administrativa	administrativa	k1gFnSc1	administrativa
řadu	řad	k1gInSc2	řad
reforem	reforma	k1gFnPc2	reforma
dotýkajících	dotýkající	k2eAgFnPc2d1	dotýkající
se	se	k3xPyFc4	se
soudnictví	soudnictví	k1gNnSc1	soudnictví
<g/>
,	,	kIx,	,
mincovnictví	mincovnictví	k1gNnSc2	mincovnictví
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Královým	Králův	k2eAgMnSc7d1	Králův
zástupcem	zástupce	k1gMnSc7	zástupce
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
stal	stát	k5eAaPmAgInS	stát
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
capitaneus	capitaneus	k1gInSc1	capitaneus
(	(	kIx(	(
<g/>
starosta	starosta	k1gMnSc1	starosta
či	či	k8xC	či
též	též	k9	též
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
)	)	kIx)	)
vybíraný	vybíraný	k2eAgMnSc1d1	vybíraný
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
velmožů	velmož	k1gMnPc2	velmož
Václavova	Václavův	k2eAgNnSc2d1	Václavovo
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
vytvoření	vytvoření	k1gNnSc4	vytvoření
tohoto	tento	k3xDgInSc2	tento
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
stávajících	stávající	k2eAgInPc2d1	stávající
zemských	zemský	k2eAgInPc2d1	zemský
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
naopak	naopak	k6eAd1	naopak
udělovány	udělovat	k5eAaImNgInP	udělovat
příslušníkům	příslušník	k1gMnPc3	příslušník
loajální	loajální	k2eAgFnSc2d1	loajální
polské	polský	k2eAgFnSc2d1	polská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
starosty	starosta	k1gMnSc2	starosta
přežila	přežít	k5eAaPmAgFnS	přežít
i	i	k8xC	i
pozdější	pozdní	k2eAgNnSc4d2	pozdější
zhroucení	zhroucení	k1gNnSc4	zhroucení
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obdobách	obdoba	k1gFnPc6	obdoba
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
zachovala	zachovat	k5eAaPmAgFnS	zachovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
byl	být	k5eAaImAgInS	být
vztah	vztah	k1gInSc1	vztah
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
s	s	k7c7	s
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
zástupci	zástupce	k1gMnPc7	zástupce
malopolské	malopolský	k2eAgFnSc2d1	Malopolská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zejména	zejména	k9	zejména
krakovské	krakovský	k2eAgNnSc1d1	Krakovské
biskupství	biskupství	k1gNnSc1	biskupství
s	s	k7c7	s
nemalým	malý	k2eNgInSc7d1	nemalý
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
biskupa	biskup	k1gMnSc2	biskup
Pavla	Pavel	k1gMnSc2	Pavel
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
domoci	domoct	k5eAaPmF	domoct
náhrad	náhrada	k1gFnPc2	náhrada
za	za	k7c4	za
vojenské	vojenský	k2eAgFnPc4d1	vojenská
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
potvrzení	potvrzení	k1gNnPc4	potvrzení
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
dvůr	dvůr	k1gInSc1	dvůr
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Václav	Václav	k1gMnSc1	Václav
nechal	nechat	k5eAaPmAgMnS	nechat
prosadit	prosadit	k5eAaPmF	prosadit
za	za	k7c4	za
Prokopova	Prokopův	k2eAgMnSc4d1	Prokopův
nástupce	nástupce	k1gMnSc4	nástupce
na	na	k7c6	na
biskupském	biskupský	k2eAgInSc6d1	biskupský
stolci	stolec	k1gInSc6	stolec
vratislavského	vratislavský	k2eAgMnSc2d1	vratislavský
rodáka	rodák	k1gMnSc2	rodák
Jana	Jan	k1gMnSc2	Jan
Muskatu	Muskat	k1gInSc2	Muskat
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pročeská	pročeský	k2eAgFnSc1d1	pročeská
orientace	orientace	k1gFnSc1	orientace
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dále	daleko	k6eAd2	daleko
upevňovat	upevňovat	k5eAaImF	upevňovat
českou	český	k2eAgFnSc4d1	Česká
vládu	vláda	k1gFnSc4	vláda
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
<g/>
Dalších	další	k2eAgInPc2d1	další
úspěchů	úspěch	k1gInPc2	úspěch
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
dočkal	dočkat	k5eAaPmAgMnS	dočkat
v	v	k7c6	v
Míšeňsku	Míšeňsek	k1gInSc6	Míšeňsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
biskupem	biskup	k1gInSc7	biskup
významný	významný	k2eAgInSc1d1	významný
člen	člen	k1gInSc1	člen
jeho	on	k3xPp3gInSc2	on
dvora	dvůr	k1gInSc2	dvůr
Bernard	Bernard	k1gMnSc1	Bernard
z	z	k7c2	z
Kamence	Kamenec	k1gInSc2	Kamenec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
musel	muset	k5eAaImAgMnS	muset
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
omezit	omezit	k5eAaPmF	omezit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc4	jeho
zájmy	zájem	k1gInPc4	zájem
nadále	nadále	k6eAd1	nadále
podporoval	podporovat	k5eAaImAgInS	podporovat
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1294	[number]	k4	1294
mu	on	k3xPp3gMnSc3	on
prodal	prodat	k5eAaPmAgInS	prodat
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
město	město	k1gNnSc1	město
Pirnu	Pirn	k1gInSc2	Pirn
<g/>
,	,	kIx,	,
střežící	střežící	k2eAgFnSc4d1	střežící
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Drážďanům	Drážďany	k1gInPc3	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Drážďanský	drážďanský	k2eAgMnSc1d1	drážďanský
pán	pán	k1gMnSc1	pán
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgMnS	navázat
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
klientské	klientský	k2eAgFnSc2d1	klientská
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
slíbila	slíbit	k5eAaPmAgFnS	slíbit
tamější	tamější	k2eAgFnSc1d1	tamější
šlechta	šlechta	k1gFnSc1	šlechta
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Fridrichovy	Fridrichův	k2eAgFnSc2d1	Fridrichova
smrti	smrt	k1gFnSc2	smrt
předání	předání	k1gNnSc1	předání
zemí	zem	k1gFnPc2	zem
pod	pod	k7c4	pod
českou	český	k2eAgFnSc4d1	Česká
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Nasavským	Nasavský	k2eAgFnPc3d1	Nasavský
a	a	k8xC	a
nastolení	nastolení	k1gNnSc3	nastolení
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
===	===	k?	===
</s>
</p>
<p>
<s>
Místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
změnit	změnit	k5eAaPmF	změnit
politickou	politický	k2eAgFnSc4d1	politická
orientaci	orientace	k1gFnSc4	orientace
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc4d1	římská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Míšeňsko	Míšeňsko	k1gNnSc1	Míšeňsko
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
neměl	mít	k5eNaImAgMnS	mít
větší	veliký	k2eAgInSc4d2	veliký
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
vést	vést	k5eAaImF	vést
politiku	politika	k1gFnSc4	politika
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
říšských	říšský	k2eAgMnPc6d1	říšský
kurfiřtech	kurfiřt	k1gMnPc6	kurfiřt
a	a	k8xC	a
další	další	k2eAgFnSc3d1	další
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc4d1	habsburský
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
vlastní	vlastní	k2eAgFnSc4d1	vlastní
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
stát	stát	k5eAaImF	stát
dvě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
rodu	rod	k1gInSc2	rod
Wettinů	Wettin	k1gInPc2	Wettin
–	–	k?	–
Durynsko	Durynsko	k1gNnSc4	Durynsko
a	a	k8xC	a
Míšeňsko	Míšeňsko	k1gNnSc4	Míšeňsko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zisku	zisk	k1gInSc6	zisk
druhé	druhý	k4xOgFnSc2	druhý
ze	z	k7c2	z
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
oblastí	oblast	k1gFnPc2	oblast
ovšem	ovšem	k9	ovšem
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
musel	muset	k5eAaImAgMnS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
nároky	nárok	k1gInPc1	nárok
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
již	již	k9	již
dříve	dříve	k6eAd2	dříve
míšeňské	míšeňský	k2eAgNnSc4d1	míšeňské
území	území	k1gNnSc4	území
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
<g/>
,	,	kIx,	,
dodá	dodat	k5eAaPmIp3nS	dodat
<g/>
-li	i	k?	-li
Václav	Václav	k1gMnSc1	Václav
právní	právní	k2eAgInPc4d1	právní
podklady	podklad	k1gInPc4	podklad
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
nárokům	nárok	k1gInPc3	nárok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
již	již	k6eAd1	již
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c4	za
změněné	změněný	k2eAgFnPc4d1	změněná
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
Albrechtem	Albrecht	k1gMnSc7	Albrecht
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
,	,	kIx,	,
dřívějším	dřívější	k2eAgMnSc7d1	dřívější
Adolfovým	Adolfův	k2eAgMnSc7d1	Adolfův
protivníkem	protivník	k1gMnSc7	protivník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
Adolfově	Adolfův	k2eAgFnSc6d1	Adolfova
volbě	volba	k1gFnSc6	volba
začaly	začít	k5eAaPmAgFnP	začít
urovnávat	urovnávat	k5eAaImF	urovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adolfovy	Adolfův	k2eAgInPc1d1	Adolfův
plány	plán	k1gInPc1	plán
ohrožovaly	ohrožovat	k5eAaImAgInP	ohrožovat
případný	případný	k2eAgInSc4d1	případný
Václavův	Václavův	k2eAgInSc4d1	Václavův
zisk	zisk	k1gInSc4	zisk
Míšeňska	Míšeňsk	k1gInSc2	Míšeňsk
<g/>
,	,	kIx,	,
panovaly	panovat	k5eAaImAgFnP	panovat
po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
mezi	mezi	k7c7	mezi
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
říšským	říšský	k2eAgMnSc7d1	říšský
králem	král	k1gMnSc7	král
korektní	korektní	k2eAgInSc4d1	korektní
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
usmiřovat	usmiřovat	k5eAaImF	usmiřovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
Albrechtem	Albrecht	k1gMnSc7	Albrecht
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Guty	Guta	k1gFnSc2	Guta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
rakouské	rakouský	k2eAgFnSc2d1	rakouská
šlechty	šlechta	k1gFnSc2	šlechta
proti	proti	k7c3	proti
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
tak	tak	k6eAd1	tak
zachoval	zachovat	k5eAaPmAgMnS	zachovat
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
Rakouskému	rakouský	k2eAgMnSc3d1	rakouský
vévodovi	vévoda	k1gMnSc3	vévoda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
skutečně	skutečně	k6eAd1	skutečně
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
nemoci	nemoc	k1gFnSc2	nemoc
vypuknuvší	vypuknuvší	k2eAgFnSc2d1	vypuknuvší
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1295	[number]	k4	1295
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
odpor	odpor	k1gInSc4	odpor
šlechty	šlechta	k1gFnSc2	šlechta
zdolat	zdolat	k5eAaPmF	zdolat
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
začít	začít	k5eAaPmF	začít
plánovat	plánovat	k5eAaImF	plánovat
sesazení	sesazení	k1gNnSc4	sesazení
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Náznaky	náznak	k1gInPc1	náznak
nového	nový	k2eAgInSc2d1	nový
kursu	kurs	k1gInSc2	kurs
české	český	k2eAgFnSc2d1	Česká
diplomacie	diplomacie	k1gFnSc2	diplomacie
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
během	během	k7c2	během
sňatku	sňatek	k1gInSc2	sňatek
Albrechtovy	Albrechtův	k2eAgFnSc2d1	Albrechtova
dcery	dcera	k1gFnSc2	dcera
Anny	Anna	k1gFnSc2	Anna
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Oty	Ota	k1gMnSc2	Ota
Dlouhého	Dlouhý	k1gMnSc2	Dlouhý
Heřmanem	Heřman	k1gMnSc7	Heřman
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
dojednání	dojednání	k1gNnSc4	dojednání
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
nápomocen	nápomocen	k2eAgMnSc1d1	nápomocen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
urovnání	urovnání	k1gNnSc3	urovnání
sporu	spor	k1gInSc2	spor
došlo	dojít	k5eAaPmAgNnS	dojít
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1293	[number]	k4	1293
až	až	k8xS	až
1296	[number]	k4	1296
při	při	k7c6	při
rituálu	rituál	k1gInSc6	rituál
smíření	smíření	k1gNnSc4	smíření
napodobujícím	napodobující	k2eAgMnPc3d1	napodobující
prvky	prvek	k1gInPc7	prvek
lenní	lenní	k2eAgFnSc2d1	lenní
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Albrecht	Albrecht	k1gMnSc1	Albrecht
poklekl	pokleknout	k5eAaPmAgMnS	pokleknout
před	před	k7c7	před
Václavem	Václav	k1gMnSc7	Václav
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
získal	získat	k5eAaPmAgMnS	získat
svého	svůj	k3xOyFgMnSc4	svůj
švagra	švagr	k1gMnSc4	švagr
jako	jako	k8xC	jako
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
spojence	spojenec	k1gMnSc4	spojenec
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
římskému	římský	k2eAgMnSc3d1	římský
králi	král	k1gMnSc3	král
Adolfovi	Adolf	k1gMnSc3	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
za	za	k7c4	za
Bernarda	Bernard	k1gMnSc4	Bernard
z	z	k7c2	z
Kamence	Kamenec	k1gInSc2	Kamenec
nyní	nyní	k6eAd1	nyní
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
schopný	schopný	k2eAgMnSc1d1	schopný
diplomat	diplomat	k1gMnSc1	diplomat
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
domem	dům	k1gInSc7	dům
Habsburků	Habsburk	k1gMnPc2	Habsburk
dlouholeté	dlouholetý	k2eAgInPc4d1	dlouholetý
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
navázal	navázat	k5eAaPmAgInS	navázat
jako	jako	k9	jako
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
rádce	rádce	k1gMnSc1	rádce
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgNnSc2d1	habsburské
<g/>
.	.	kIx.	.
<g/>
Mezitím	mezitím	k6eAd1	mezitím
Adolf	Adolf	k1gMnSc1	Adolf
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
posiloval	posilovat	k5eAaImAgInS	posilovat
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
durynského	durynský	k2eAgMnSc2d1	durynský
lantkraběte	lantkrabě	k1gMnSc2	lantkrabě
Albrechta	Albrecht	k1gMnSc2	Albrecht
Nezdárného	zdárný	k2eNgMnSc2d1	nezdárný
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
Durynsko	Durynsko	k1gNnSc4	Durynsko
i	i	k8xC	i
Míšeň	Míšeň	k1gFnSc4	Míšeň
a	a	k8xC	a
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnPc3	jeho
synům	syn	k1gMnPc3	syn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
svého	svůj	k1gMnSc2	svůj
otce	otec	k1gMnSc2	otec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
válečnou	válečný	k2eAgFnSc4d1	válečná
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
dohody	dohoda	k1gFnPc1	dohoda
mezi	mezi	k7c7	mezi
českým	český	k2eAgInSc7d1	český
i	i	k8xC	i
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
nicméně	nicméně	k8xC	nicméně
nadále	nadále	k6eAd1	nadále
platily	platit	k5eAaImAgInP	platit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
vojenské	vojenský	k2eAgFnSc6d1	vojenská
výpravě	výprava	k1gFnSc6	výprava
roku	rok	k1gInSc2	rok
1296	[number]	k4	1296
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Adolfovi	Adolf	k1gMnSc3	Adolf
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
Albrechtových	Albrechtových	k2eAgInPc2d1	Albrechtových
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Adolf	Adolf	k1gMnSc1	Adolf
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
sešel	sejít	k5eAaPmAgMnS	sejít
v	v	k7c6	v
Grünhainu	Grünhain	k1gInSc6	Grünhain
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
Václavových	Václavových	k2eAgInPc2d1	Václavových
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Míšeňsko	Míšeňsko	k1gNnSc4	Míšeňsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyslal	vyslat	k5eAaPmAgMnS	vyslat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Ruprechta	Ruprecht	k1gMnSc2	Ruprecht
Nasavského	Nasavský	k2eAgMnSc2d1	Nasavský
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
dcerou	dcera	k1gFnSc7	dcera
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
.	.	kIx.	.
</s>
<s>
Ruprecht	Ruprecht	k1gInSc1	Ruprecht
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
jakožto	jakožto	k8xS	jakožto
rukojmí	rukojmí	k1gMnPc1	rukojmí
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
předání	předání	k1gNnSc3	předání
Míšně	Míšeň	k1gFnSc2	Míšeň
skutečně	skutečně	k6eAd1	skutečně
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
však	však	k9	však
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
Anežka	Anežka	k1gFnSc1	Anežka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc4	Václav
tím	ten	k3xDgNnSc7	ten
nepoutaly	poutat	k5eNaImAgFnP	poutat
k	k	k7c3	k
nasavskému	nasavský	k2eAgInSc3d1	nasavský
rodu	rod	k1gInSc3	rod
žádné	žádný	k3yNgInPc4	žádný
svazky	svazek	k1gInPc4	svazek
a	a	k8xC	a
Ruprecht	Ruprecht	k1gInSc1	Ruprecht
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
si	se	k3xPyFc3	se
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
získával	získávat	k5eAaImAgMnS	získávat
svou	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
v	v	k7c6	v
Míšni	Míšeň	k1gFnSc6	Míšeň
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
nevybíravé	vybíravý	k2eNgFnPc1d1	nevybíravá
metody	metoda	k1gFnPc1	metoda
při	při	k7c6	při
prosazování	prosazování	k1gNnSc6	prosazování
územních	územní	k2eAgInPc2d1	územní
požadavků	požadavek	k1gInPc2	požadavek
i	i	k9	i
ochota	ochota	k1gFnSc1	ochota
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
anglickou	anglický	k2eAgFnSc4d1	anglická
úplatu	úplata	k1gFnSc4	úplata
proti	proti	k7c3	proti
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
Filipu	Filip	k1gMnSc3	Filip
Sličnému	sličný	k2eAgMnSc3d1	sličný
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
terčem	terč	k1gInSc7	terč
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
svrhnutí	svrhnutí	k1gNnSc4	svrhnutí
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
se	se	k3xPyFc4	se
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
během	během	k7c2	během
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
korunovace	korunovace	k1gFnSc2	korunovace
konané	konaný	k2eAgFnSc2d1	konaná
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1297	[number]	k4	1297
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
významnými	významný	k2eAgMnPc7d1	významný
hosty	host	k1gMnPc7	host
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
slavnosti	slavnost	k1gFnSc6	slavnost
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
nechybělo	chybět	k5eNaImAgNnS	chybět
vedle	vedle	k7c2	vedle
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgNnSc2d1	habsburské
množství	množství	k1gNnSc2	množství
významných	významný	k2eAgMnPc2d1	významný
říšských	říšský	k2eAgMnPc2d1	říšský
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
naplánovaly	naplánovat	k5eAaBmAgInP	naplánovat
další	další	k2eAgInPc1d1	další
kroky	krok	k1gInPc1	krok
pro	pro	k7c4	pro
nastolení	nastolení	k1gNnSc4	nastolení
Albrechta	Albrecht	k1gMnSc2	Albrecht
novým	nový	k2eAgMnSc7d1	nový
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
zejména	zejména	k9	zejména
samotné	samotný	k2eAgFnSc2d1	samotná
korunovace	korunovace	k1gFnSc2	korunovace
však	však	k9	však
zkalila	zkalit	k5eAaPmAgFnS	zkalit
smrt	smrt	k1gFnSc4	smrt
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
manželky	manželka	k1gFnSc2	manželka
Guty	Guta	k1gFnSc2	Guta
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnPc4d1	poslední
dohody	dohoda	k1gFnPc4	dohoda
Václav	Václav	k1gMnSc1	Václav
s	s	k7c7	s
Albrechtem	Albrecht	k1gMnSc7	Albrecht
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1298	[number]	k4	1298
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
zásnub	zásnuba	k1gFnPc2	zásnuba
českého	český	k2eAgMnSc2d1	český
kralevice	kralevic	k1gMnSc2	kralevic
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
uherské	uherský	k2eAgFnSc2d1	uherská
princezny	princezna	k1gFnSc2	princezna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
Albrecht	Albrecht	k1gMnSc1	Albrecht
slíbil	slíbit	k5eAaPmAgMnS	slíbit
odevzdat	odevzdat	k5eAaPmF	odevzdat
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
Plíseňsko	Plíseňsko	k1gNnSc4	Plíseňsko
<g/>
,	,	kIx,	,
Chebsko	Chebsko	k1gNnSc4	Chebsko
a	a	k8xC	a
skupinu	skupina	k1gFnSc4	skupina
hradů	hrad	k1gInPc2	hrad
ležících	ležící	k2eAgInPc2d1	ležící
na	na	k7c6	na
bavorsko-českém	bavorsko-český	k2eAgNnSc6d1	bavorsko-český
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
nadále	nadále	k6eAd1	nadále
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
nemusel	muset	k5eNaImAgMnS	muset
plnit	plnit	k5eAaImF	plnit
vůči	vůči	k7c3	vůči
říši	říš	k1gFnSc3	říš
žádné	žádný	k3yNgFnPc4	žádný
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
měl	mít	k5eAaImAgInS	mít
právo	právo	k1gNnSc4	právo
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
významných	významný	k2eAgFnPc2d1	významná
porad	porada	k1gFnPc2	porada
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
říše	říš	k1gFnSc2	říš
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
další	další	k2eAgNnPc4d1	další
privilegia	privilegium	k1gNnPc4	privilegium
udělená	udělený	k2eAgNnPc4d1	udělené
předchozími	předchozí	k2eAgMnPc7d1	předchozí
římskými	římský	k2eAgMnPc7d1	římský
králi	král	k1gMnPc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
podpory	podpora	k1gFnSc2	podpora
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
vyjádřené	vyjádřený	k2eAgInPc1d1	vyjádřený
zejména	zejména	k9	zejména
zasláním	zaslání	k1gNnSc7	zaslání
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
i	i	k9	i
podpory	podpora	k1gFnSc2	podpora
dalších	další	k2eAgMnPc2d1	další
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
se	se	k3xPyFc4	se
Albrecht	Albrecht	k1gMnSc1	Albrecht
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
k	k	k7c3	k
otevřené	otevřený	k2eAgFnSc3d1	otevřená
konfrontaci	konfrontace	k1gFnSc3	konfrontace
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Nasavským	Nasavský	k2eAgMnSc7d1	Nasavský
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1298	[number]	k4	1298
se	se	k3xPyFc4	se
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
sešli	sejít	k5eAaPmAgMnP	sejít
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
a	a	k8xC	a
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
Adolfa	Adolf	k1gMnSc4	Adolf
za	za	k7c4	za
sesazeného	sesazený	k2eAgMnSc4d1	sesazený
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
něj	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
jako	jako	k8xC	jako
nového	nový	k2eAgMnSc4d1	nový
vládce	vládce	k1gMnSc4	vládce
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
Albrechta	Albrecht	k1gMnSc4	Albrecht
Habsburského	habsburský	k2eAgMnSc4d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Göllheimu	Göllheim	k1gInSc2	Göllheim
nasavská	nasavský	k2eAgNnPc1d1	nasavský
vojska	vojsko	k1gNnPc1	vojsko
prohrála	prohrát	k5eAaPmAgNnP	prohrát
a	a	k8xC	a
Adolf	Adolf	k1gMnSc1	Adolf
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
římské	římský	k2eAgFnSc2d1	římská
koruny	koruna	k1gFnSc2	koruna
dočasně	dočasně	k6eAd1	dočasně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
nová	nový	k2eAgFnSc1d1	nová
řádná	řádný	k2eAgFnSc1d1	řádná
volba	volba	k1gFnSc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
zakončení	zakončení	k1gNnSc1	zakončení
jednomyslným	jednomyslný	k2eAgNnSc7d1	jednomyslné
zvolením	zvolení	k1gNnSc7	zvolení
rakouského	rakouský	k2eAgMnSc2d1	rakouský
vévody	vévoda	k1gMnSc2	vévoda
byl	být	k5eAaImAgMnS	být
Albrecht	Albrecht	k1gMnSc1	Albrecht
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1298	[number]	k4	1298
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
korunován	korunovat	k5eAaBmNgInS	korunovat
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Epizoda	epizoda	k1gFnSc1	epizoda
vlády	vláda	k1gFnSc2	vláda
nasavského	nasavský	k2eAgInSc2d1	nasavský
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
tak	tak	k6eAd1	tak
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mincovní	mincovní	k2eAgFnSc1d1	mincovní
reforma	reforma	k1gFnSc1	reforma
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
pražského	pražský	k2eAgInSc2d1	pražský
groše	groš	k1gInSc2	groš
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
k	k	k7c3	k
rozkvětu	rozkvět	k1gInSc3	rozkvět
hornické	hornický	k2eAgFnSc2d1	hornická
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
objevena	objeven	k2eAgNnPc1d1	objeveno
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
ložiska	ložisko	k1gNnPc1	ložisko
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
užívané	užívaný	k2eAgNnSc1d1	užívané
pro	pro	k7c4	pro
dary	dar	k1gInPc4	dar
a	a	k8xC	a
úplatky	úplatek	k1gInPc4	úplatek
<g/>
,	,	kIx,	,
na	na	k7c4	na
platbu	platba	k1gFnSc4	platba
námezdným	námezdný	k2eAgNnPc3d1	námezdné
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc3	vedení
úřednického	úřednický	k2eAgInSc2d1	úřednický
aparátu	aparát	k1gInSc2	aparát
či	či	k8xC	či
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
nákladného	nákladný	k2eAgInSc2d1	nákladný
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
hrálo	hrát	k5eAaImAgNnS	hrát
za	za	k7c2	za
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
hornické	hornický	k2eAgNnSc1d1	Hornické
centrum	centrum	k1gNnSc1	centrum
patřící	patřící	k2eAgNnSc1d1	patřící
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnSc4d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
sjíždět	sjíždět	k5eAaImF	sjíždět
horníci	horník	k1gMnPc1	horník
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
zásoby	zásoba	k1gFnPc4	zásoba
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
rud	ruda	k1gFnPc2	ruda
vyčerpané	vyčerpaný	k2eAgFnPc1d1	vyčerpaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
využil	využít	k5eAaPmAgMnS	využít
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
zdejší	zdejší	k2eAgFnSc2d1	zdejší
půdy	půda	k1gFnSc2	půda
patřila	patřit	k5eAaImAgFnS	patřit
sedleckému	sedlecký	k2eAgInSc3d1	sedlecký
klášteru	klášter	k1gInSc3	klášter
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
patronem	patron	k1gMnSc7	patron
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	s	k7c7	s
přímými	přímý	k2eAgInPc7d1	přímý
zásahy	zásah	k1gInPc7	zásah
vést	vést	k5eAaImF	vést
vývoj	vývoj	k1gInSc1	vývoj
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
hornického	hornický	k2eAgNnSc2d1	Hornické
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
prosadit	prosadit	k5eAaPmF	prosadit
mincovní	mincovní	k2eAgFnSc4d1	mincovní
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
používala	používat	k5eAaImAgFnS	používat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
brakteátová	brakteátový	k2eAgFnSc1d1	brakteátový
mince	mince	k1gFnSc1	mince
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
nízká	nízký	k2eAgFnSc1d1	nízká
kvalita	kvalita	k1gFnSc1	kvalita
i	i	k8xC	i
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
razila	razit	k5eAaImAgFnS	razit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
vyvážely	vyvážet	k5eAaImAgInP	vyvážet
kusy	kus	k1gInPc1	kus
neraženého	ražený	k2eNgNnSc2d1	ražený
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
přineslo	přinést	k5eAaPmAgNnS	přinést
zavedení	zavedení	k1gNnSc1	zavedení
pražského	pražský	k2eAgInSc2d1	pražský
groše	groš	k1gInSc2	groš
<g/>
,	,	kIx,	,
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
sousedství	sousedství	k1gNnSc6	sousedství
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
centralizaci	centralizace	k1gFnSc3	centralizace
výroby	výroba	k1gFnSc2	výroba
mincí	mince	k1gFnPc2	mince
–	–	k?	–
místo	místo	k1gNnSc1	místo
několika	několik	k4yIc2	několik
mincoven	mincovna	k1gFnPc2	mincovna
nyní	nyní	k6eAd1	nyní
existovalo	existovat	k5eAaImAgNnS	existovat
jediné	jediný	k2eAgNnSc1d1	jediné
centrum	centrum	k1gNnSc1	centrum
ražby	ražba	k1gFnSc2	ražba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
Václav	Václav	k1gMnSc1	Václav
postavit	postavit	k5eAaPmF	postavit
Vlašský	vlašský	k2eAgInSc4d1	vlašský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
byla	být	k5eAaImAgFnS	být
všechna	všechen	k3xTgFnSc1	všechen
výroba	výroba	k1gFnSc1	výroba
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Samotnou	samotný	k2eAgFnSc4d1	samotná
realizaci	realizace	k1gFnSc4	realizace
reformy	reforma	k1gFnSc2	reforma
přenechal	přenechat	k5eAaPmAgMnS	přenechat
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
bankéřům	bankéř	k1gMnPc3	bankéř
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
též	též	k9	též
starali	starat	k5eAaImAgMnP	starat
o	o	k7c4	o
ekonomiku	ekonomika	k1gFnSc4	ekonomika
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
získávání	získávání	k1gNnSc2	získávání
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
také	také	k9	také
dalších	další	k2eAgInPc2d1	další
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
měna	měna	k1gFnSc1	měna
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
messinského	messinský	k2eAgNnSc2d1	messinský
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
raženého	ražený	k2eAgNnSc2d1	ražené
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
groš	groš	k1gInSc1	groš
vážil	vážit	k5eAaImAgInS	vážit
zhruba	zhruba	k6eAd1	zhruba
3,9	[number]	k4	3,9
gramu	gram	k1gInSc2	gram
a	a	k8xC	a
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
12	[number]	k4	12
drobnějším	drobný	k2eAgFnPc3d2	drobnější
stříbrným	stříbrný	k2eAgFnPc3d1	stříbrná
mincím	mince	k1gFnPc3	mince
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
nově	nově	k6eAd1	nově
raženým	ražený	k2eAgInSc7d1	ražený
<g/>
,	,	kIx,	,
zvaným	zvaný	k2eAgInSc7d1	zvaný
parvus	parvus	k1gInSc4	parvus
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
groš	groš	k1gInSc1	groš
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
platidlem	platidlo	k1gNnSc7	platidlo
jak	jak	k6eAd1	jak
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
dávána	dáván	k2eAgFnSc1d1	dávána
přednost	přednost	k1gFnSc1	přednost
před	před	k7c7	před
jinými	jiný	k2eAgFnPc7d1	jiná
méně	málo	k6eAd2	málo
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
měnami	měna	k1gFnPc7	měna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Horní	horní	k2eAgInSc1d1	horní
zákoník	zákoník	k1gInSc1	zákoník
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
samotné	samotný	k2eAgFnSc6d1	samotná
hornické	hornický	k2eAgFnSc6d1	hornická
činnosti	činnost	k1gFnSc6	činnost
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
nepodílel	podílet	k5eNaImAgInS	podílet
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
přenechána	přenechat	k5eAaPmNgFnS	přenechat
odborníkům	odborník	k1gMnPc3	odborník
a	a	k8xC	a
podnikavým	podnikavý	k2eAgMnPc3d1	podnikavý
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
prováděli	provádět	k5eAaImAgMnP	provádět
horní	horní	k2eAgMnPc1d1	horní
podnikání	podnikání	k1gNnSc3	podnikání
a	a	k8xC	a
činnosti	činnost	k1gFnSc3	činnost
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojené	spojený	k2eAgInPc1d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
z	z	k7c2	z
dolování	dolování	k1gNnSc2	dolování
vzácných	vzácný	k2eAgInPc2d1	vzácný
kovů	kov	k1gInPc2	kov
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
horních	horní	k2eAgInPc2d1	horní
regálů	regál	k1gInPc2	regál
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
práva	právo	k1gNnSc2	právo
vymáhat	vymáhat	k5eAaImF	vymáhat
část	část	k1gFnSc4	část
výtěžku	výtěžek	k1gInSc2	výtěžek
z	z	k7c2	z
těžby	těžba	k1gFnSc2	těžba
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zachovat	zachovat	k5eAaPmF	zachovat
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
autorita	autorita	k1gFnSc1	autorita
zaštiťující	zaštiťující	k2eAgInPc1d1	zaštiťující
právní	právní	k2eAgInPc1d1	právní
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Podnikání	podnikání	k1gNnSc1	podnikání
bylo	být	k5eAaImAgNnS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
v	v	k7c6	v
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
díle	dílo	k1gNnSc6	dílo
Ius	Ius	k1gFnSc2	Ius
regale	regale	k6eAd1	regale
montanorum	montanorum	k1gInSc4	montanorum
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
Královské	královský	k2eAgNnSc4d1	královské
právo	právo	k1gNnSc4	právo
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
svou	svůj	k3xOyFgFnSc7	svůj
definicí	definice	k1gFnSc7	definice
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
podnikatelem	podnikatel	k1gMnSc7	podnikatel
a	a	k8xC	a
státem	stát	k1gInSc7	stát
nemělo	mít	k5eNaImAgNnS	mít
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Evropě	Evropa	k1gFnSc6	Evropa
obdobu	obdoba	k1gFnSc4	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
patrné	patrný	k2eAgNnSc1d1	patrné
zaměření	zaměření	k1gNnSc1	zaměření
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
norem	norma	k1gFnPc2	norma
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gNnSc2	její
odměňování	odměňování	k1gNnSc2	odměňování
a	a	k8xC	a
etice	etika	k1gFnSc3	etika
úřednictva	úřednictvo	k1gNnSc2	úřednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Ital	Ital	k1gMnSc1	Ital
<g/>
,	,	kIx,	,
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
práva	právo	k1gNnSc2	právo
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
z	z	k7c2	z
práva	právo	k1gNnSc2	právo
římského	římský	k2eAgMnSc2d1	římský
<g/>
.	.	kIx.	.
<g/>
Sbírka	sbírka	k1gFnSc1	sbírka
byla	být	k5eAaImAgFnS	být
dělená	dělený	k2eAgFnSc1d1	dělená
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
propůjček	propůjčka	k1gFnPc2	propůjčka
a	a	k8xC	a
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
městské	městský	k2eAgNnSc4d1	Městské
právo	právo	k1gNnSc4	právo
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jihlavského	jihlavský	k2eAgMnSc2d1	jihlavský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
sbírku	sbírka	k1gFnSc4	sbírka
s	s	k7c7	s
celostátním	celostátní	k2eAgNnSc7d1	celostátní
působením	působení	k1gNnSc7	působení
zaštítěnou	zaštítěný	k2eAgFnSc7d1	zaštítěná
samotnou	samotný	k2eAgFnSc7d1	samotná
osobou	osoba	k1gFnSc7	osoba
vladaře	vladař	k1gMnSc2	vladař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ustanoveních	ustanovení	k1gNnPc6	ustanovení
se	se	k3xPyFc4	se
dbalo	dbát	k5eAaImAgNnS	dbát
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
nároků	nárok	k1gInPc2	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
respektována	respektován	k2eAgFnSc1d1	respektována
svoboda	svoboda	k1gFnSc1	svoboda
kverků	kverek	k1gInPc2	kverek
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
horních	horní	k2eAgMnPc2d1	horní
podnikatelů	podnikatel	k1gMnPc2	podnikatel
vytvářejících	vytvářející	k2eAgInPc2d1	vytvářející
větší	veliký	k2eAgInPc4d2	veliký
celky	celek	k1gInPc4	celek
zvané	zvaný	k2eAgInPc4d1	zvaný
těžařstva	těžařstvo	k1gNnSc2	těžařstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
právo	právo	k1gNnSc1	právo
těžby	těžba	k1gFnSc2	těžba
pronajímat	pronajímat	k5eAaImF	pronajímat
dále	daleko	k6eAd2	daleko
havířům	havíř	k1gMnPc3	havíř
nebo	nebo	k8xC	nebo
druhotným	druhotný	k2eAgMnPc3d1	druhotný
horním	horní	k2eAgMnPc3d1	horní
podnikatelům	podnikatel	k1gMnPc3	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
nařízení	nařízení	k1gNnSc2	nařízení
se	se	k3xPyFc4	se
samotných	samotný	k2eAgMnPc2d1	samotný
horníků	horník	k1gMnPc2	horník
dotklo	dotknout	k5eAaPmAgNnS	dotknout
zejména	zejména	k9	zejména
ustanovení	ustanovení	k1gNnSc1	ustanovení
o	o	k7c6	o
šestihodinové	šestihodinový	k2eAgFnSc6d1	šestihodinová
pracovní	pracovní	k2eAgFnSc6d1	pracovní
směně	směna	k1gFnSc6	směna
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
pracovat	pracovat	k5eAaImF	pracovat
dvě	dva	k4xCgFnPc4	dva
směny	směna	k1gFnPc4	směna
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Směny	směna	k1gFnPc4	směna
cyklicky	cyklicky	k6eAd1	cyklicky
bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
obíhaly	obíhat	k5eAaImAgInP	obíhat
<g/>
,	,	kIx,	,
pracovalo	pracovat	k5eAaImAgNnS	pracovat
se	se	k3xPyFc4	se
nepřetržitě	přetržitě	k6eNd1	přetržitě
kromě	kromě	k7c2	kromě
dnů	den	k1gInPc2	den
odpočinku	odpočinek	k1gInSc2	odpočinek
nařízených	nařízený	k2eAgFnPc2d1	nařízená
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
nedělí	neděle	k1gFnPc2	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
i	i	k9	i
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
–	–	k?	–
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
podle	podle	k7c2	podle
nového	nový	k2eAgNnSc2d1	nové
horního	horní	k2eAgNnSc2d1	horní
práva	právo	k1gNnSc2	právo
měli	mít	k5eAaImAgMnP	mít
zodpovídat	zodpovídat	k5eAaPmF	zodpovídat
tesaři	tesař	k1gMnPc1	tesař
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
pozemku	pozemek	k1gInSc2	pozemek
musel	muset	k5eAaImAgMnS	muset
dolování	dolování	k1gNnSc4	dolování
umožnit	umožnit	k5eAaPmF	umožnit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
měl	mít	k5eAaImAgMnS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
část	část	k1gFnSc4	část
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
krále	král	k1gMnSc4	král
představovala	představovat	k5eAaImAgFnS	představovat
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
urbura	urbura	k1gFnSc1	urbura
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
poplatek	poplatek	k1gInSc1	poplatek
za	za	k7c4	za
těžbu	těžba	k1gFnSc4	těžba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
profesí	profes	k1gFnPc2	profes
pevně	pevně	k6eAd1	pevně
stanoven	stanoven	k2eAgInSc1d1	stanoven
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
zákoník	zákoník	k1gInSc1	zákoník
inovativní	inovativní	k2eAgInSc1d1	inovativní
a	a	k8xC	a
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
hornické	hornický	k2eAgNnSc4d1	Hornické
právo	právo	k1gNnSc4	právo
byl	být	k5eAaImAgMnS	být
převzat	převzat	k2eAgMnSc1d1	převzat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotných	samotný	k2eAgFnPc6d1	samotná
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
dílčích	dílčí	k2eAgFnPc6d1	dílčí
částech	část	k1gFnPc6	část
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyla	být	k5eNaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
platnost	platnost	k1gFnSc4	platnost
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
definitivně	definitivně	k6eAd1	definitivně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Završení	završení	k1gNnSc3	završení
polské	polský	k2eAgFnSc2d1	polská
expanze	expanze	k1gFnSc2	expanze
===	===	k?	===
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
vyvíjející	vyvíjející	k2eAgFnSc4d1	vyvíjející
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
byla	být	k5eAaImAgFnS	být
pozornost	pozornost	k1gFnSc1	pozornost
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
nadále	nadále	k6eAd1	nadále
upřena	upřít	k5eAaPmNgFnS	upřít
i	i	k9	i
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gNnSc2	on
snažili	snažit	k5eAaImAgMnP	snažit
prosadit	prosadit	k5eAaPmF	prosadit
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
kníže	kníže	k1gMnSc1	kníže
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hlohovský	Hlohovský	k2eAgMnSc1d1	Hlohovský
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Přemysl	Přemysl	k1gMnSc1	Přemysl
Velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
a	a	k8xC	a
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
držel	držet	k5eAaImAgInS	držet
Kujavsko	Kujavsko	k1gNnSc4	Kujavsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
byl	být	k5eAaImAgInS	být
dosud	dosud	k6eAd1	dosud
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
rukou	ruka	k1gFnPc6	ruka
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
významné	významný	k2eAgNnSc1d1	významné
město	město	k1gNnSc1	město
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
Hnězdno	Hnězdno	k1gNnSc4	Hnězdno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
Přemysl	Přemysl	k1gMnSc1	Přemysl
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
kurií	kurie	k1gFnSc7	kurie
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
své	svůj	k3xOyFgFnSc2	svůj
korunovace	korunovace	k1gFnSc2	korunovace
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
mu	on	k3xPp3gMnSc3	on
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
až	až	k6eAd1	až
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
–	–	k?	–
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
slavnostně	slavnostně	k6eAd1	slavnostně
korunován	korunován	k2eAgMnSc1d1	korunován
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
postoji	postoj	k1gInSc6	postoj
Václava	Václav	k1gMnSc2	Václav
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
korunovaci	korunovace	k1gFnSc3	korunovace
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
žádná	žádný	k3yNgFnSc1	žádný
hodnověrná	hodnověrný	k2eAgFnSc1d1	hodnověrná
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
záhy	záhy	k6eAd1	záhy
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
vázán	vázat	k5eAaImNgMnS	vázat
politikou	politika	k1gFnSc7	politika
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
šance	šance	k1gFnSc1	šance
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
chopili	chopit	k5eAaPmAgMnP	chopit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hlohovský	Hlohovský	k2eAgMnSc1d1	Hlohovský
a	a	k8xC	a
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
dvojice	dvojice	k1gFnSc2	dvojice
získal	získat	k5eAaPmAgInS	získat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Velkopolska	Velkopolsko	k1gNnSc2	Velkopolsko
a	a	k8xC	a
Pomoří	pomořit	k5eAaPmIp3nS	pomořit
Lokýtek	lokýtek	k1gInSc4	lokýtek
<g/>
,	,	kIx,	,
s	s	k7c7	s
nároky	nárok	k1gInPc7	nárok
svého	své	k1gNnSc2	své
rivala	rival	k1gMnSc2	rival
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
podporovaného	podporovaný	k2eAgMnSc2d1	podporovaný
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
musel	muset	k5eAaImAgMnS	muset
vypořádat	vypořádat	k5eAaPmF	vypořádat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
smlouvu	smlouva	k1gFnSc4	smlouva
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
získané	získaný	k2eAgNnSc1d1	získané
území	území	k1gNnSc1	území
mezi	mezi	k7c4	mezi
obě	dva	k4xCgNnPc4	dva
knížata	kníže	k1gNnPc4	kníže
došlo	dojít	k5eAaPmAgNnS	dojít
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1297	[number]	k4	1297
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
podkopat	podkopat	k5eAaPmF	podkopat
českou	český	k2eAgFnSc4d1	Česká
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Polsku	Polsko	k1gNnSc6	Polsko
získáním	získání	k1gNnSc7	získání
přízně	přízeň	k1gFnSc2	přízeň
Jana	Jana	k1gFnSc1	Jana
Muskaty	Muskata	k1gFnSc2	Muskata
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k8xS	jako
náhradu	náhrada	k1gFnSc4	náhrada
pět	pět	k4xCc1	pět
tisíc	tisíc	k4xCgInSc1	tisíc
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
vedlo	vést	k5eAaImAgNnS	vést
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Hlohovského	Hlohovský	k2eAgMnSc4d1	Hlohovský
k	k	k7c3	k
nezávislejší	závislý	k2eNgFnSc3d2	nezávislejší
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
opuštění	opuštění	k1gNnSc4	opuštění
paktu	pakt	k1gInSc2	pakt
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Jindřich	Jindřich	k1gMnSc1	Jindřich
ovšem	ovšem	k9	ovšem
neměl	mít	k5eNaImAgMnS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
situaci	situace	k1gFnSc6	situace
konkurovat	konkurovat	k5eAaImF	konkurovat
Václavovi	Václav	k1gMnSc3	Václav
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zkáze	zkáza	k1gFnSc6	zkáza
Adolfa	Adolf	k1gMnSc2	Adolf
Nasavského	Nasavský	k2eAgInSc2d1	Nasavský
u	u	k7c2	u
Göllheimu	Göllheim	k1gInSc2	Göllheim
a	a	k8xC	a
volbě	volba	k1gFnSc6	volba
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgInSc2d1	habsburský
novým	nový	k2eAgInSc7d1	nový
panovníkem	panovník	k1gMnSc7	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
se	se	k3xPyFc4	se
otevřely	otevřít	k5eAaPmAgFnP	otevřít
možnosti	možnost	k1gFnPc1	možnost
další	další	k2eAgFnSc2d1	další
české	český	k2eAgFnSc2d1	Česká
expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
předpokladů	předpoklad	k1gInPc2	předpoklad
rozšíření	rozšíření	k1gNnSc2	rozšíření
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byla	být	k5eAaImAgFnS	být
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Lokýtkem	lokýtek	k1gInSc7	lokýtek
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
1299	[number]	k4	1299
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
značný	značný	k2eAgInSc4d1	značný
obnos	obnos	k1gInSc4	obnos
velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
kníže	kníže	k1gMnSc1	kníže
uvolil	uvolit	k5eAaPmAgMnS	uvolit
dostavit	dostavit	k5eAaPmF	dostavit
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
převzít	převzít	k5eAaPmF	převzít
své	svůj	k3xOyFgFnPc4	svůj
země	zem	k1gFnPc4	zem
od	od	k7c2	od
Václava	Václav	k1gMnSc2	Václav
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
neučinil	učinit	k5eNaPmAgInS	učinit
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
všechna	všechen	k3xTgNnPc4	všechen
jeho	jeho	k3xOp3gNnPc4	jeho
území	území	k1gNnPc4	území
připadnout	připadnout	k5eAaPmF	připadnout
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Lokýtek	lokýtek	k1gInSc1	lokýtek
slovo	slovo	k1gNnSc4	slovo
nedodržel	dodržet	k5eNaPmAgInS	dodržet
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zajistil	zajistit	k5eAaPmAgMnS	zajistit
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
právní	právní	k2eAgInSc4d1	právní
podklady	podklad	k1gInPc4	podklad
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
dorazili	dorazit	k5eAaPmAgMnP	dorazit
za	za	k7c7	za
Václavem	Václav	k1gMnSc7	Václav
představitelé	představitel	k1gMnPc1	představitel
velkopolské	velkopolský	k2eAgFnSc2d1	Velkopolská
šlechty	šlechta	k1gFnSc2	šlechta
i	i	k8xC	i
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
si	se	k3xPyFc3	se
velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
pán	pán	k1gMnSc1	pán
svou	svůj	k3xOyFgFnSc7	svůj
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
politikou	politika	k1gFnSc7	politika
znepřátelil	znepřátelit	k5eAaPmAgInS	znepřátelit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijal	přijmout	k5eAaPmAgMnS	přijmout
vládu	vláda	k1gFnSc4	vláda
namísto	namísto	k7c2	namísto
Vladislava	Vladislav	k1gMnSc2	Vladislav
Lokýtka	Lokýtek	k1gMnSc2	Lokýtek
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
mohl	moct	k5eAaImAgMnS	moct
navíc	navíc	k6eAd1	navíc
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
umocnit	umocnit	k5eAaPmF	umocnit
zásnubami	zásnuba	k1gFnPc7	zásnuba
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
Přemysla	Přemysl	k1gMnSc2	Přemysl
Velkopolského	velkopolský	k2eAgInSc2d1	velkopolský
Eliškou	Eliška	k1gFnSc7	Eliška
Rejčkou	Rejčka	k1gMnSc7	Rejčka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
výpravou	výprava	k1gFnSc7	výprava
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Václavova	Václavův	k2eAgFnSc1d1	Václavova
výprava	výprava	k1gFnSc1	výprava
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
většiny	většina	k1gFnSc2	většina
velkopolské	velkopolský	k2eAgFnSc2d1	Velkopolská
nobility	nobilita	k1gFnSc2	nobilita
a	a	k8xC	a
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Lokýtkových	Lokýtkových	k2eAgNnSc2d1	Lokýtkových
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
velkopolský	velkopolský	k2eAgMnSc1d1	velkopolský
kníže	kníže	k1gMnSc1	kníže
se	se	k3xPyFc4	se
s	s	k7c7	s
Václavovým	Václavův	k2eAgNnSc7d1	Václavovo
vojskem	vojsko	k1gNnSc7	vojsko
nestřetl	střetnout	k5eNaPmAgMnS	střetnout
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
utekl	utéct	k5eAaPmAgInS	utéct
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
královské	královský	k2eAgFnSc3d1	královská
korunovaci	korunovace	k1gFnSc3	korunovace
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgFnPc4	dva
královské	královský	k2eAgFnPc4d1	královská
koruny	koruna	k1gFnPc4	koruna
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
polskou	polský	k2eAgFnSc4d1	polská
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
polské	polský	k2eAgFnSc2d1	polská
šlechty	šlechta	k1gFnSc2	šlechta
následovala	následovat	k5eAaImAgFnS	následovat
svého	svůj	k3xOyFgMnSc4	svůj
původního	původní	k2eAgMnSc4d1	původní
pána	pán	k1gMnSc4	pán
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
drobné	drobný	k2eAgFnPc1d1	drobná
vojenské	vojenský	k2eAgFnPc1d1	vojenská
srážky	srážka	k1gFnPc1	srážka
se	s	k7c7	s
zbývajícími	zbývající	k2eAgMnPc7d1	zbývající
Lokýtkovými	Lokýtkův	k2eAgMnPc7d1	Lokýtkův
stoupenci	stoupenec	k1gMnPc7	stoupenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nově	nově	k6eAd1	nově
získaném	získaný	k2eAgNnSc6d1	získané
území	území	k1gNnSc6	území
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
v	v	k7c6	v
Malopolsku	Malopolsko	k1gNnSc6	Malopolsko
zaveden	zaveden	k2eAgInSc1d1	zaveden
úřad	úřad	k1gInSc1	úřad
starosty	starosta	k1gMnSc2	starosta
neboli	neboli	k8xC	neboli
zemského	zemský	k2eAgMnSc2d1	zemský
hejtmana	hejtman	k1gMnSc2	hejtman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zisk	zisk	k1gInSc1	zisk
uherské	uherský	k2eAgFnSc2d1	uherská
koruny	koruna	k1gFnSc2	koruna
===	===	k?	===
</s>
</p>
<p>
<s>
Nečekaného	čekaný	k2eNgInSc2d1	nečekaný
úspěchu	úspěch	k1gInSc2	úspěch
se	se	k3xPyFc4	se
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
dynastie	dynastie	k1gFnSc1	dynastie
dočkala	dočkat	k5eAaPmAgFnS	dočkat
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1301	[number]	k4	1301
totiž	totiž	k9	totiž
bez	bez	k7c2	bez
mužského	mužský	k2eAgMnSc2d1	mužský
dědice	dědic	k1gMnSc2	dědic
zemřel	zemřít	k5eAaPmAgMnS	zemřít
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ondřej	Ondřej	k1gMnSc1	Ondřej
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
řešit	řešit	k5eAaImF	řešit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
odbojnou	odbojný	k2eAgFnSc7d1	odbojná
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
nároky	nárok	k1gInPc1	nárok
dalších	další	k2eAgMnPc2d1	další
evropských	evropský	k2eAgMnPc2d1	evropský
vládců	vládce	k1gMnPc2	vládce
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
ze	z	k7c2	z
soupeřů	soupeř	k1gMnPc2	soupeř
<g/>
,	,	kIx,	,
Albrechtem	Albrecht	k1gMnSc7	Albrecht
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyjednat	vyjednat	k5eAaPmF	vyjednat
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
pretendent	pretendent	k1gMnSc1	pretendent
uherského	uherský	k2eAgInSc2d1	uherský
trůnu	trůn	k1gInSc2	trůn
Karel	Karel	k1gMnSc1	Karel
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc4d1	Anjý
nadále	nadále	k6eAd1	nadále
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
vylodil	vylodit	k5eAaPmAgMnS	vylodit
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
korunovat	korunovat	k5eAaBmF	korunovat
chorvatským	chorvatský	k2eAgMnSc7d1	chorvatský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Uherští	uherský	k2eAgMnPc1d1	uherský
magnáti	magnát	k1gMnPc1	magnát
nechtěli	chtít	k5eNaImAgMnP	chtít
přijmout	přijmout	k5eAaPmF	přijmout
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgMnSc1d1	další
uchazeč	uchazeč	k1gMnSc1	uchazeč
o	o	k7c4	o
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
připadal	připadat	k5eAaImAgMnS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
také	také	k9	také
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dolnobavorský	Dolnobavorský	k2eAgMnSc1d1	Dolnobavorský
<g/>
,	,	kIx,	,
Arpádovec	Arpádovec	k1gMnSc1	Arpádovec
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
Haličské	haličský	k2eAgFnSc2d1	Haličská
a	a	k8xC	a
snoubenec	snoubenec	k1gMnSc1	snoubenec
osiřelé	osiřelý	k2eAgFnSc2d1	osiřelá
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodování	rozhodování	k1gNnSc1	rozhodování
uherské	uherský	k2eAgNnSc1d1	Uherské
šlechtě	šlechta	k1gFnSc3	šlechta
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
bohatství	bohatství	k1gNnSc1	bohatství
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c6	po
počátečním	počáteční	k2eAgNnSc6d1	počáteční
váhání	váhání	k1gNnSc6	váhání
i	i	k9	i
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
začal	začít	k5eAaPmAgMnS	začít
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
uherského	uherský	k2eAgInSc2d1	uherský
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
uherští	uherský	k2eAgMnPc1d1	uherský
páni	pan	k1gMnPc1	pan
byli	být	k5eAaImAgMnP	být
uplaceni	uplatit	k5eAaPmNgMnP	uplatit
kutnohorským	kutnohorský	k2eAgNnSc7d1	kutnohorské
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1301	[number]	k4	1301
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
uherským	uherský	k2eAgInSc7d1	uherský
sněmem	sněm	k1gInSc7	sněm
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
čekatele	čekatel	k1gMnPc4	čekatel
svatoštěpánské	svatoštěpánský	k2eAgFnSc2d1	Svatoštěpánská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
schůzkách	schůzka	k1gFnPc6	schůzka
a	a	k8xC	a
dalším	další	k2eAgNnSc6d1	další
mohutném	mohutný	k2eAgNnSc6d1	mohutné
uplácení	uplácení	k1gNnSc6	uplácení
váhajících	váhající	k2eAgInPc2d1	váhající
odjel	odjet	k5eAaPmAgMnS	odjet
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
kralevic	kralevic	k1gMnSc1	kralevic
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
se	s	k7c7	s
spolehlivým	spolehlivý	k2eAgInSc7d1	spolehlivý
doprovodem	doprovod	k1gInSc7	doprovod
a	a	k8xC	a
s	s	k7c7	s
kanceláří	kancelář	k1gFnSc7	kancelář
vedenou	vedený	k2eAgFnSc4d1	vedená
krakovským	krakovský	k2eAgMnSc7d1	krakovský
biskupem	biskup	k1gMnSc7	biskup
Janem	Jan	k1gMnSc7	Jan
Muskatou	Muskatý	k2eAgFnSc7d1	Muskatý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
s	s	k7c7	s
uherskou	uherský	k2eAgFnSc7d1	uherská
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1301	[number]	k4	1301
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Stoličném	stoličný	k1gMnSc6	stoličný
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
korunován	korunován	k2eAgInSc4d1	korunován
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Ladislav	Ladislava	k1gFnPc2	Ladislava
V.	V.	kA	V.
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
část	část	k1gFnSc1	část
šlechty	šlechta	k1gFnSc2	šlechta
nadále	nadále	k6eAd1	nadále
stála	stát	k5eAaImAgFnS	stát
za	za	k7c7	za
Karlem	Karel	k1gMnSc7	Karel
Robertem	Robert	k1gMnSc7	Robert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soupeření	soupeření	k1gNnSc4	soupeření
s	s	k7c7	s
protičeskou	protičeský	k2eAgFnSc7d1	protičeská
koalicí	koalice	k1gFnSc7	koalice
===	===	k?	===
</s>
</p>
<p>
<s>
Náhlý	náhlý	k2eAgInSc1d1	náhlý
růst	růst	k1gInSc1	růst
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
území	území	k1gNnSc2	území
pod	pod	k7c7	pod
jejich	jejich	k3xOp3gFnSc7	jejich
kontrolou	kontrola	k1gFnSc7	kontrola
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
široké	široký	k2eAgFnSc2d1	široká
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
zvrátit	zvrátit	k5eAaPmF	zvrátit
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
Václavovy	Václavův	k2eAgInPc4d1	Václavův
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
osobnosti	osobnost	k1gFnPc4	osobnost
této	tento	k3xDgFnSc2	tento
koalice	koalice	k1gFnSc2	koalice
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
boji	boj	k1gInSc6	boj
za	za	k7c4	za
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
Albrecht	Albrecht	k1gMnSc1	Albrecht
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Adolfa	Adolf	k1gMnSc2	Adolf
Nasavského	Nasavský	k2eAgMnSc2d1	Nasavský
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Wettinů	Wettin	k1gInPc2	Wettin
získat	získat	k5eAaPmF	získat
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
a	a	k8xC	a
Míšeňsku	Míšeňsko	k1gNnSc6	Míšeňsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetával	střetávat	k5eAaImAgMnS	střetávat
i	i	k9	i
se	s	k7c7	s
zájmy	zájem	k1gInPc1	zájem
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Anjouovců	Anjouovec	k1gInPc2	Anjouovec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
využíval	využívat	k5eAaImAgInS	využívat
Albrechta	Albrecht	k1gMnSc4	Albrecht
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Filipem	Filip	k1gMnSc7	Filip
Sličným	sličný	k2eAgMnSc7d1	sličný
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
pokusil	pokusit	k5eAaPmAgMnS	pokusit
využít	využít	k5eAaPmF	využít
možnosti	možnost	k1gFnPc4	možnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc4	jednání
odehrávajících	odehrávající	k2eAgMnPc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1303	[number]	k4	1303
se	se	k3xPyFc4	se
zhostil	zhostit	k5eAaPmAgMnS	zhostit
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
měl	mít	k5eAaImAgMnS	mít
Václav	Václav	k1gMnSc1	Václav
postavit	postavit	k5eAaPmF	postavit
vojsko	vojsko	k1gNnSc4	vojsko
za	za	k7c4	za
100	[number]	k4	100
000	[number]	k4	000
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
česko-francouzská	českorancouzský	k2eAgFnSc1d1	česko-francouzská
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
nerealizovala	realizovat	k5eNaBmAgFnS	realizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
boji	boj	k1gInSc3	boj
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
pokoření	pokoření	k1gNnSc4	pokoření
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Habsburskou	habsburský	k2eAgFnSc4d1	habsburská
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
zastavit	zastavit	k5eAaPmF	zastavit
jednáním	jednání	k1gNnSc7	jednání
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
opět	opět	k6eAd1	opět
zkušený	zkušený	k2eAgMnSc1d1	zkušený
diplomat	diplomat	k1gMnSc1	diplomat
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
již	již	k9	již
úspěchu	úspěch	k1gInSc3	úspěch
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
původními	původní	k2eAgNnPc7d1	původní
privilegii	privilegium	k1gNnPc7	privilegium
zaručenými	zaručený	k2eAgNnPc7d1	zaručené
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnPc7	král
roku	rok	k1gInSc2	rok
1298	[number]	k4	1298
<g/>
,	,	kIx,	,
požadavek	požadavek	k1gInSc1	požadavek
placení	placení	k1gNnSc2	placení
říšského	říšský	k2eAgInSc2d1	říšský
desátku	desátek	k1gInSc2	desátek
případně	případně	k6eAd1	případně
odevzdání	odevzdání	k1gNnSc4	odevzdání
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Plíseňsku	Plíseňsko	k1gNnSc6	Plíseňsko
<g/>
,	,	kIx,	,
Chebsku	Chebsko	k1gNnSc6	Chebsko
<g/>
,	,	kIx,	,
Míšni	Míšeň	k1gFnSc6	Míšeň
<g/>
,	,	kIx,	,
Uhrách	Uhry	k1gFnPc6	Uhry
i	i	k8xC	i
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
takové	takový	k3xDgInPc4	takový
požadavky	požadavek	k1gInPc4	požadavek
striktně	striktně	k6eAd1	striktně
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Albrecht	Albrecht	k1gMnSc1	Albrecht
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1304	[number]	k4	1304
uvalil	uvalit	k5eAaPmAgInS	uvalit
na	na	k7c4	na
svého	svůj	k1gMnSc4	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
švagra	švagr	k1gMnSc2	švagr
říšskou	říšský	k2eAgFnSc4d1	říšská
klatbu	klatba	k1gFnSc4	klatba
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
významní	významný	k2eAgMnPc1d1	významný
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
,	,	kIx,	,
předpokládající	předpokládající	k2eAgInSc1d1	předpokládající
Albrechtův	Albrechtův	k2eAgInSc4d1	Albrechtův
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
přislíbili	přislíbit	k5eAaPmAgMnP	přislíbit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
plánovaném	plánovaný	k2eAgNnSc6d1	plánované
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jim	on	k3xPp3gMnPc3	on
mělo	mít	k5eAaImAgNnS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
značnou	značný	k2eAgFnSc4d1	značná
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1304	[number]	k4	1304
se	se	k3xPyFc4	se
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
Albrechtem	Albrecht	k1gMnSc7	Albrecht
<g/>
,	,	kIx,	,
sešel	sejít	k5eAaPmAgMnS	sejít
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Anjouovec	Anjouovec	k1gMnSc1	Anjouovec
Karel	Karel	k1gMnSc1	Karel
Robert	Robert	k1gMnSc1	Robert
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
Václavův	Václavův	k2eAgMnSc1d1	Václavův
polský	polský	k2eAgMnSc1d1	polský
rival	rival	k1gMnSc1	rival
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc4	lokýtek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
proti	proti	k7c3	proti
svému	svůj	k3xOyFgNnSc3	svůj
nepříteli	nepřítel	k1gMnSc3	nepřítel
dojednali	dojednat	k5eAaPmAgMnP	dojednat
společné	společný	k2eAgInPc4d1	společný
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
<s>
Podporu	podpora	k1gFnSc4	podpora
měl	mít	k5eAaImAgMnS	mít
přislíbit	přislíbit	k5eAaPmF	přislíbit
i	i	k9	i
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hlohovský	Hlohovský	k2eAgMnSc1d1	Hlohovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
osamocen	osamocen	k2eAgMnSc1d1	osamocen
<g/>
.	.	kIx.	.
</s>
<s>
Spojenectví	spojenectví	k1gNnSc1	spojenectví
mu	on	k3xPp3gNnSc3	on
přislíbili	přislíbit	k5eAaPmAgMnP	přislíbit
jak	jak	k9	jak
Askánci	Askánek	k1gMnPc1	Askánek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgMnSc7	který
udržoval	udržovat	k5eAaImAgMnS	udržovat
český	český	k2eAgMnSc1d1	český
vládce	vládce	k1gMnSc1	vládce
tradičně	tradičně	k6eAd1	tradičně
dobré	dobrý	k2eAgMnPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1304	[number]	k4	1304
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
již	již	k6eAd1	již
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vojska	vojsko	k1gNnSc2	vojsko
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
tísnila	tísnit	k5eAaImAgFnS	tísnit
vojska	vojsko	k1gNnSc2	vojsko
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čechy	Čech	k1gMnPc4	Čech
svěřil	svěřit	k5eAaPmAgMnS	svěřit
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
vojenského	vojenský	k2eAgNnSc2d1	vojenské
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
synovce	synovec	k1gMnSc2	synovec
Heřmana	Heřman	k1gMnSc2	Heřman
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
některé	některý	k3yIgInPc4	některý
vojenské	vojenský	k2eAgInPc4d1	vojenský
úspěchy	úspěch	k1gInPc4	úspěch
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
z	z	k7c2	z
Uherska	Uhersko	k1gNnSc2	Uhersko
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
již	již	k6eAd1	již
začaly	začít	k5eAaPmAgFnP	začít
útočit	útočit	k5eAaImF	útočit
první	první	k4xOgFnSc2	první
jednotky	jednotka	k1gFnSc2	jednotka
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
římská	římský	k2eAgNnPc4d1	římské
vojska	vojsko	k1gNnPc4	vojsko
pochodovala	pochodovat	k5eAaImAgFnS	pochodovat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zachovali	zachovat	k5eAaPmAgMnP	zachovat
členové	člen	k1gMnPc1	člen
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Vítkovci	Vítkovec	k1gMnPc1	Vítkovec
<g/>
,	,	kIx,	,
střežící	střežící	k2eAgInPc1d1	střežící
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
hvozdy	hvozd	k1gInPc1	hvozd
<g/>
,	,	kIx,	,
Albrechtovi	Albrechtův	k2eAgMnPc1d1	Albrechtův
neustoupili	ustoupit	k5eNaPmAgMnP	ustoupit
a	a	k8xC	a
nepřeběhli	přeběhnout	k5eNaPmAgMnP	přeběhnout
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
podobné	podobný	k2eAgFnSc2d1	podobná
situace	situace	k1gFnSc2	situace
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
října	říjen	k1gInSc2	říjen
říšská	říšský	k2eAgNnPc1d1	říšské
vojska	vojsko	k1gNnPc1	vojsko
doplněná	doplněný	k2eAgFnSc1d1	doplněná
uherskými	uherský	k2eAgMnPc7d1	uherský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prošli	projít	k5eAaPmAgMnP	projít
za	za	k7c2	za
soustavného	soustavný	k2eAgInSc2d1	soustavný
plenění	plenění	k1gNnSc1	plenění
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
ke	k	k7c3	k
kutnohorským	kutnohorský	k2eAgInPc3d1	kutnohorský
stříbrným	stříbrný	k2eAgInPc3d1	stříbrný
dolům	dol	k1gInPc3	dol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velké	velký	k2eAgFnSc2d1	velká
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Václav	Václav	k1gMnSc1	Václav
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
do	do	k7c2	do
menších	malý	k2eAgInPc2d2	menší
mobilnějších	mobilní	k2eAgInPc2d2	mobilnější
celků	celek	k1gInPc2	celek
majících	mající	k2eAgInPc2d1	mající
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
oslabování	oslabování	k1gNnSc2	oslabování
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
a	a	k8xC	a
málo	málo	k6eAd1	málo
osídleném	osídlený	k2eAgNnSc6d1	osídlené
prostředí	prostředí	k1gNnSc6	prostředí
trpěl	trpět	k5eAaImAgInS	trpět
stálým	stálý	k2eAgInSc7d1	stálý
nedostatkem	nedostatek	k1gInSc7	nedostatek
proviantu	proviant	k1gInSc2	proviant
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Nymburka	Nymburk	k1gInSc2	Nymburk
vyčkávali	vyčkávat	k5eAaImAgMnP	vyčkávat
na	na	k7c4	na
Václavův	Václavův	k2eAgInSc4d1	Václavův
rozkaz	rozkaz	k1gInSc4	rozkaz
i	i	k9	i
Askánci	Askánec	k1gMnSc3	Askánec
–	–	k?	–
Heřman	Heřman	k1gMnSc1	Heřman
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
i	i	k9	i
se	s	k7c7	s
šedesátiletým	šedesátiletý	k2eAgMnSc7d1	šedesátiletý
strýcem	strýc	k1gMnSc7	strýc
Otou	Ota	k1gMnSc7	Ota
Se	s	k7c7	s
šípem	šíp	k1gInSc7	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Velením	velení	k1gNnSc7	velení
části	část	k1gFnSc2	část
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
žoldu	žold	k1gInSc6	žold
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zeť	zeť	k1gMnSc1	zeť
Ruprecht	Ruprecht	k1gMnSc1	Ruprecht
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
stanul	stanout	k5eAaPmAgMnS	stanout
Albrecht	Albrecht	k1gMnSc1	Albrecht
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
před	před	k7c7	před
Kutnou	kutný	k2eAgFnSc7d1	Kutná
Horou	hora	k1gFnSc7	hora
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
oblehnout	oblehnout	k5eAaPmF	oblehnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
úsilí	úsilí	k1gNnSc1	úsilí
bylo	být	k5eAaImAgNnS	být
zmařeno	zmařit	k5eAaPmNgNnS	zmařit
náhle	náhle	k6eAd1	náhle
vypuknuvší	vypuknuvší	k2eAgFnSc7d1	vypuknuvší
epidemií	epidemie	k1gFnSc7	epidemie
úplavice	úplavice	k1gFnSc2	úplavice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
středověcí	středověký	k2eAgMnPc1d1	středověký
kronikáři	kronikář	k1gMnPc1	kronikář
přičítali	přičítat	k5eAaImAgMnP	přičítat
odpadům	odpad	k1gInPc3	odpad
a	a	k8xC	a
jedům	jed	k1gInPc3	jed
vylitým	vylitý	k2eAgInPc3d1	vylitý
do	do	k7c2	do
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
Vrchlického	Vrchlického	k2eAgInSc2d1	Vrchlického
potoka	potok	k1gInSc2	potok
kutnohorskými	kutnohorský	k2eAgMnPc7d1	kutnohorský
horníky	horník	k1gMnPc7	horník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
Kutné	kutný	k2eAgFnSc3d1	Kutná
Hoře	hora	k1gFnSc3	hora
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
přesunul	přesunout	k5eAaPmAgMnS	přesunout
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
dál	daleko	k6eAd2	daleko
od	od	k7c2	od
města	město	k1gNnSc2	město
a	a	k8xC	a
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
vytoužené	vytoužený	k2eAgFnSc2d1	vytoužená
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Nevida	vidět	k5eNaImSgMnS	vidět
jiného	jiný	k2eAgNnSc2d1	jiné
východiska	východisko	k1gNnSc2	východisko
se	se	k3xPyFc4	se
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
jeho	on	k3xPp3gNnSc2	on
nepřítel	nepřítel	k1gMnSc1	nepřítel
využil	využít	k5eAaPmAgMnS	využít
a	a	k8xC	a
na	na	k7c4	na
rozptýlenou	rozptýlený	k2eAgFnSc4d1	rozptýlená
armádu	armáda	k1gFnSc4	armáda
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
poslal	poslat	k5eAaPmAgMnS	poslat
své	svůj	k3xOyFgInPc4	svůj
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Spořádaný	spořádaný	k2eAgInSc1d1	spořádaný
ústup	ústup	k1gInSc1	ústup
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
útěk	útěk	k1gInSc4	útěk
přes	přes	k7c4	přes
Vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k9	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zasypána	zasypat	k5eAaPmNgFnS	zasypat
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
jenom	jenom	k9	jenom
zbytky	zbytek	k1gInPc7	zbytek
původního	původní	k2eAgNnSc2d1	původní
Albrechtova	Albrechtův	k2eAgNnSc2d1	Albrechtovo
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgInPc4d1	poslední
měsíce	měsíc	k1gInPc4	měsíc
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
se	se	k3xPyFc4	se
po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
své	svůj	k3xOyFgFnSc2	svůj
výpravy	výprava	k1gFnSc2	výprava
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
nezáviděníhodné	nezáviděníhodný	k2eAgFnSc6d1	nezáviděníhodná
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořist	kořist	k1gFnSc1	kořist
z	z	k7c2	z
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
mu	on	k3xPp3gMnSc3	on
vynese	vynést	k5eAaPmIp3nS	vynést
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
i	i	k9	i
četný	četný	k2eAgInSc4d1	četný
říšský	říšský	k2eAgInSc4d1	říšský
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
začali	začít	k5eAaPmAgMnP	začít
odpadávat	odpadávat	k5eAaImF	odpadávat
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
spojenci	spojenec	k1gMnPc1	spojenec
jako	jako	k8xC	jako
bavorští	bavorský	k2eAgMnPc1d1	bavorský
vévodové	vévoda	k1gMnPc1	vévoda
či	či	k8xC	či
Wettinové	Wettin	k1gMnPc1	Wettin
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
diplomacie	diplomacie	k1gFnSc1	diplomacie
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
chvíli	chvíle	k1gFnSc6	chvíle
musela	muset	k5eAaImAgFnS	muset
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c6	na
zastavení	zastavení	k1gNnSc6	zastavení
poklesu	pokles	k1gInSc2	pokles
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
kombinací	kombinace	k1gFnSc7	kombinace
pružných	pružný	k2eAgNnPc2d1	pružné
protiopatření	protiopatření	k1gNnPc2	protiopatření
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
území	území	k1gNnPc2	území
udržet	udržet	k5eAaPmF	udržet
zejména	zejména	k9	zejména
Velkopolsko	Velkopolsko	k1gNnSc4	Velkopolsko
a	a	k8xC	a
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc2	Václav
opouštěly	opouštět	k5eAaImAgFnP	opouštět
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
svolil	svolit	k5eAaPmAgInS	svolit
k	k	k7c3	k
mírovému	mírový	k2eAgNnSc3d1	Mírové
jednání	jednání	k1gNnSc3	jednání
a	a	k8xC	a
ústupkům	ústupek	k1gInPc3	ústupek
vůči	vůči	k7c3	vůči
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
usnadnil	usnadnit	k5eAaPmAgInS	usnadnit
převzetí	převzetí	k1gNnSc4	převzetí
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
syna	syn	k1gMnSc2	syn
zapřisáhl	zapřisáhnout	k5eAaPmAgMnS	zapřisáhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dodržel	dodržet	k5eAaPmAgMnS	dodržet
finanční	finanční	k2eAgInPc4d1	finanční
závazky	závazek	k1gInPc4	závazek
a	a	k8xC	a
splatil	splatit	k5eAaPmAgMnS	splatit
jeho	jeho	k3xOp3gInPc4	jeho
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Stižen	stižen	k2eAgInSc1d1	stižen
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
<g/>
,	,	kIx,	,
vydechl	vydechnout	k5eAaPmAgMnS	vydechnout
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
v	v	k7c6	v
měšťanském	měšťanský	k2eAgInSc6d1	měšťanský
domě	dům	k1gInSc6	dům
zlatníka	zlatník	k1gMnSc2	zlatník
Konráda	Konrád	k1gMnSc2	Konrád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
plánovaném	plánovaný	k2eAgNnSc6d1	plánované
přemyslovském	přemyslovský	k2eAgNnSc6d1	přemyslovské
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
nemocí	nemoc	k1gFnSc7	nemoc
zubožené	zubožený	k2eAgNnSc4d1	zubožené
tělo	tělo	k1gNnSc4	tělo
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
šatu	šat	k1gInSc6	šat
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
sotva	sotva	k6eAd1	sotva
dostavěném	dostavěný	k2eAgInSc6d1	dostavěný
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
tak	tak	k9	tak
vyslyšeno	vyslyšen	k2eAgNnSc1d1	vyslyšeno
jeho	jeho	k3xOp3gNnSc4	jeho
údajné	údajný	k2eAgNnSc4d1	údajné
přání	přání	k1gNnSc4	přání
být	být	k5eAaImF	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatu	šat	k1gInSc6	šat
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
konvrše	konvrše	k?	konvrše
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1420	[number]	k4	1420
vnikly	vniknout	k5eAaPmAgFnP	vniknout
do	do	k7c2	do
klášterního	klášterní	k2eAgNnSc2d1	klášterní
ticha	ticho	k1gNnSc2	ticho
houfy	houf	k1gInPc1	houf
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
Síň	síň	k1gFnSc1	síň
královská	královský	k2eAgFnSc1d1	královská
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
plamenům	plamen	k1gInPc3	plamen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
souboru	soubor	k1gInSc6	soubor
vyneseného	vynesený	k2eAgInSc2d1	vynesený
kosterního	kosterní	k2eAgInSc2d1	kosterní
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgMnSc2	jenž
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
antropologem	antropolog	k1gMnSc7	antropolog
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
identifikovány	identifikován	k2eAgInPc4d1	identifikován
i	i	k8xC	i
Václavovy	Václavův	k2eAgInPc4d1	Václavův
ostatky	ostatek	k1gInPc4	ostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Především	především	k9	především
starší	starý	k2eAgFnSc1d2	starší
historiografie	historiografie	k1gFnSc1	historiografie
Václava	Václav	k1gMnSc2	Václav
často	často	k6eAd1	často
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgMnS	jevit
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
nepříliš	příliš	k6eNd1	příliš
energický	energický	k2eAgMnSc1d1	energický
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
objíždějící	objíždějící	k2eAgFnSc1d1	objíždějící
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
neúčastnící	účastnící	k2eNgNnSc4d1	účastnící
se	se	k3xPyFc4	se
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
se	s	k7c7	s
slabou	slabý	k2eAgFnSc7d1	slabá
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
konstitucí	konstituce	k1gFnSc7	konstituce
a	a	k8xC	a
sklony	sklon	k1gInPc1	sklon
k	k	k7c3	k
psychické	psychický	k2eAgFnSc3d1	psychická
labilitě	labilita	k1gFnSc3	labilita
<g/>
,	,	kIx,	,
projevované	projevovaný	k2eAgNnSc1d1	projevované
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
bouřky	bouřka	k1gFnSc2	bouřka
či	či	k8xC	či
silným	silný	k2eAgInSc7d1	silný
až	až	k8xS	až
panickým	panický	k2eAgInSc7d1	panický
odporem	odpor	k1gInSc7	odpor
vůči	vůči	k7c3	vůči
kočkám	kočka	k1gFnPc3	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Rádci	rádce	k1gMnPc1	rádce
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
drželi	držet	k5eAaImAgMnP	držet
hlavní	hlavní	k2eAgFnSc4d1	hlavní
rozhodovací	rozhodovací	k2eAgFnSc4d1	rozhodovací
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInSc1d2	novější
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Václavovu	Václavův	k2eAgFnSc4d1	Václavova
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pak	pak	k6eAd1	pak
zůstal	zůstat	k5eAaPmAgMnS	zůstat
platným	platný	k2eAgInSc7d1	platný
takřka	takřka	k6eAd1	takřka
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Václavově	Václavův	k2eAgFnSc3d1	Václavova
osobnosti	osobnost	k1gFnSc3	osobnost
zejména	zejména	k9	zejména
Josef	Josef	k1gMnSc1	Josef
Šusta	Šusta	k1gMnSc1	Šusta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
nepůsobí	působit	k5eNaImIp3nP	působit
již	již	k6eAd1	již
role	role	k1gFnPc1	role
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
tak	tak	k9	tak
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nicméně	nicméně	k8xC	nicméně
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
rozporuplnou	rozporuplný	k2eAgFnSc7d1	rozporuplná
osobností	osobnost	k1gFnSc7	osobnost
–	–	k?	–
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
zbožnost	zbožnost	k1gFnSc1	zbožnost
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc1d1	bohatý
milostný	milostný	k2eAgInSc1d1	milostný
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
nádheře	nádhera	k1gFnSc6	nádhera
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
značná	značný	k2eAgFnSc1d1	značná
štědrost	štědrost	k1gFnSc1	štědrost
a	a	k8xC	a
obliba	obliba	k1gFnSc1	obliba
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
současných	současný	k2eAgMnPc2d1	současný
historiků	historik	k1gMnPc2	historik
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
vůči	vůči	k7c3	vůči
Václavovi	Václav	k1gMnSc3	Václav
odlišný	odlišný	k2eAgInSc4d1	odlišný
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInSc4d1	vycházející
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
interpretace	interpretace	k1gFnSc2	interpretace
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Zbraslavské	zbraslavský	k2eAgFnPc1d1	Zbraslavská
kroniky	kronika	k1gFnPc1	kronika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
králi	král	k1gMnSc3	král
přiřadit	přiřadit	k5eAaPmF	přiřadit
atributy	atribut	k1gInPc4	atribut
dokonalého	dokonalý	k2eAgMnSc2d1	dokonalý
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
používají	používat	k5eAaImIp3nP	používat
literární	literární	k2eAgInPc4d1	literární
prostředky	prostředek	k1gInPc4	prostředek
příznačné	příznačný	k2eAgInPc4d1	příznačný
pro	pro	k7c4	pro
hagiografie	hagiografie	k1gFnPc4	hagiografie
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
měly	mít	k5eAaImAgFnP	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
svatořečení	svatořečení	k1gNnSc3	svatořečení
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
vysoce	vysoce	k6eAd1	vysoce
citlivého	citlivý	k2eAgNnSc2d1	citlivé
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nemocného	nemocný	k2eAgMnSc4d1	nemocný
krále	král	k1gMnSc4	král
trpícího	trpící	k2eAgMnSc4d1	trpící
psychickou	psychický	k2eAgFnSc7d1	psychická
poruchou	porucha	k1gFnSc7	porucha
tak	tak	k6eAd1	tak
nemusí	muset	k5eNaImIp3nS	muset
stát	stát	k5eAaImF	stát
na	na	k7c6	na
příliš	příliš	k6eAd1	příliš
pevných	pevný	k2eAgInPc6d1	pevný
základech	základ	k1gInPc6	základ
<g/>
.	.	kIx.	.
<g/>
Přes	přes	k7c4	přes
problematiku	problematika	k1gFnSc4	problematika
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
osobností	osobnost	k1gFnSc7	osobnost
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
usednutí	usednutí	k1gNnSc6	usednutí
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastalo	nastat	k5eAaPmAgNnS	nastat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
období	období	k1gNnSc2	období
relativního	relativní	k2eAgInSc2d1	relativní
míru	mír	k1gInSc2	mír
a	a	k8xC	a
obnovení	obnovení	k1gNnSc2	obnovení
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
vladař	vladař	k1gMnSc1	vladař
sám	sám	k3xTgMnSc1	sám
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
mincovní	mincovní	k2eAgFnSc7d1	mincovní
reformou	reforma	k1gFnSc7	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
úspěchů	úspěch	k1gInPc2	úspěch
českého	český	k2eAgMnSc2d1	český
panovníka	panovník	k1gMnSc2	panovník
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
bylo	být	k5eAaImAgNnS	být
české	český	k2eAgNnSc1d1	české
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
naklonit	naklonit	k5eAaPmF	naklonit
si	se	k3xPyFc3	se
cizí	cizí	k2eAgMnSc1d1	cizí
velmože	velmož	k1gMnSc4	velmož
<g/>
,	,	kIx,	,
vést	vést	k5eAaImF	vést
válečné	válečný	k2eAgMnPc4d1	válečný
výpravy	výprava	k1gMnPc4	výprava
a	a	k8xC	a
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
se	se	k3xPyFc4	se
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
činitelem	činitel	k1gMnSc7	činitel
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podpora	podpora	k1gFnSc1	podpora
byla	být	k5eAaImAgFnS	být
klíčová	klíčový	k2eAgFnSc1d1	klíčová
pro	pro	k7c4	pro
dosazení	dosazení	k1gNnSc4	dosazení
dvou	dva	k4xCgMnPc2	dva
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
Adolfa	Adolf	k1gMnSc2	Adolf
Nasavského	Nasavský	k2eAgMnSc2d1	Nasavský
a	a	k8xC	a
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1301	[number]	k4	1301
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
zásluhám	zásluha	k1gFnPc3	zásluha
stanula	stanout	k5eAaPmAgFnS	stanout
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
dynastie	dynastie	k1gFnSc1	dynastie
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
značné	značný	k2eAgFnSc3d1	značná
části	část	k1gFnSc3	část
Uher	uher	k1gInSc4	uher
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lenní	lenní	k2eAgFnSc2d1	lenní
závislosti	závislost	k1gFnSc2	závislost
získáno	získán	k2eAgNnSc1d1	získáno
pod	pod	k7c4	pod
český	český	k2eAgInSc4d1	český
vliv	vliv	k1gInSc4	vliv
Míšeňsko	Míšeňsko	k1gNnSc1	Míšeňsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
úspěchy	úspěch	k1gInPc4	úspěch
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dokončit	dokončit	k5eAaPmF	dokončit
dva	dva	k4xCgInPc1	dva
Václavovy	Václavův	k2eAgInPc1d1	Václavův
projekty	projekt	k1gInPc1	projekt
–	–	k?	–
zavedení	zavedení	k1gNnSc4	zavedení
psaného	psaný	k2eAgInSc2d1	psaný
zákoníku	zákoník	k1gInSc2	zákoník
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
páni	pan	k1gMnPc1	pan
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInPc3	jeho
plánům	plán	k1gInPc3	plán
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
,	,	kIx,	,
obávajíce	obávat	k5eAaImSgMnP	obávat
se	se	k3xPyFc4	se
poklesu	pokles	k1gInSc3	pokles
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ukotvením	ukotvení	k1gNnSc7	ukotvení
práva	právo	k1gNnSc2	právo
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
ztratili	ztratit	k5eAaPmAgMnP	ztratit
možnost	možnost	k1gFnSc4	možnost
nadále	nadále	k6eAd1	nadále
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
zákonech	zákon	k1gInPc6	zákon
země	zem	k1gFnSc2	zem
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
by	by	k9	by
naopak	naopak	k6eAd1	naopak
mohla	moct	k5eAaImAgFnS	moct
posílit	posílit	k5eAaPmF	posílit
vliv	vliv	k1gInSc4	vliv
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
postavení	postavení	k1gNnSc4	postavení
nobility	nobilita	k1gFnSc2	nobilita
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Kladný	kladný	k2eAgInSc1d1	kladný
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
šlechtou	šlechta	k1gFnSc7	šlechta
se	se	k3xPyFc4	se
Václavovi	Václav	k1gMnSc3	Václav
vyplatil	vyplatit	k5eAaPmAgInS	vyplatit
–	–	k?	–
při	při	k7c6	při
vpádu	vpád	k1gInSc6	vpád
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Albrechta	Albrecht	k1gMnSc2	Albrecht
roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
podobné	podobný	k2eAgFnSc2d1	podobná
situace	situace	k1gFnSc2	situace
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zůstali	zůstat	k5eAaPmAgMnP	zůstat
čeští	český	k2eAgMnPc1d1	český
šlechtici	šlechtic	k1gMnPc1	šlechtic
na	na	k7c6	na
králově	králův	k2eAgFnSc6d1	králova
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
původní	původní	k2eAgInPc1d1	původní
teritoriální	teritoriální	k2eAgInPc1d1	teritoriální
zisky	zisk	k1gInPc1	zisk
nezůstaly	zůstat	k5eNaPmAgInP	zůstat
dlouho	dlouho	k6eAd1	dlouho
v	v	k7c6	v
přemyslovských	přemyslovský	k2eAgFnPc6d1	Přemyslovská
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
vzdal	vzdát	k5eAaPmAgMnS	vzdát
uherské	uherský	k2eAgFnPc4d1	uherská
koruny	koruna	k1gFnPc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vymření	vymření	k1gNnSc2	vymření
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
po	po	k7c6	po
meči	meč	k1gInSc6	meč
jeho	jeho	k3xOp3gNnSc7	jeho
zavražděním	zavraždění	k1gNnSc7	zavraždění
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
se	se	k3xPyFc4	se
od	od	k7c2	od
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
odpoutalo	odpoutat	k5eAaPmAgNnS	odpoutat
také	také	k9	také
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zde	zde	k6eAd1	zde
nepřišlo	přijít	k5eNaPmAgNnS	přijít
úsilí	úsilí	k1gNnSc1	úsilí
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
úplně	úplně	k6eAd1	úplně
nazmar	nazmar	k6eAd1	nazmar
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
nastoupivší	nastoupivší	k2eAgMnPc1d1	nastoupivší
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
dokázali	dokázat	k5eAaPmAgMnP	dokázat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nároků	nárok	k1gInPc2	nárok
zděděných	zděděný	k2eAgInPc2d1	zděděný
po	po	k7c6	po
Přemyslovcích	Přemyslovec	k1gMnPc6	Přemyslovec
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
polských	polský	k2eAgFnPc6d1	polská
piastovských	piastovský	k2eAgFnPc6d1	piastovský
vládcích	vládce	k1gMnPc6	vládce
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
zůstala	zůstat	k5eAaPmAgFnS	zůstat
součástí	součást	k1gFnSc7	součást
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
až	až	k8xS	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc4d1	český
a	a	k8xC	a
polského	polský	k2eAgMnSc4d1	polský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
navázala	navázat	k5eAaPmAgFnS	navázat
dynastie	dynastie	k1gFnSc1	dynastie
lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
její	její	k3xOp3gMnSc1	její
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
představitel	představitel	k1gMnSc1	představitel
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podpora	podpora	k1gFnSc1	podpora
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc1d1	vlastní
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
mecenášem	mecenáš	k1gMnSc7	mecenáš
<g/>
,	,	kIx,	,
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
posluchačem	posluchač	k1gMnSc7	posluchač
a	a	k8xC	a
také	také	k9	také
autorem	autor	k1gMnSc7	autor
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kladný	kladný	k2eAgInSc1d1	kladný
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
rytířské	rytířský	k2eAgFnSc3d1	rytířská
dvorské	dvorský	k2eAgFnSc3d1	dvorská
kultuře	kultura	k1gFnSc3	kultura
získal	získat	k5eAaPmAgInS	získat
během	během	k7c2	během
internace	internace	k1gFnSc2	internace
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
braniborských	braniborský	k2eAgMnPc2d1	braniborský
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
zálibu	záliba	k1gFnSc4	záliba
<g/>
.	.	kIx.	.
</s>
<s>
Neuměl	umět	k5eNaImAgMnS	umět
sice	sice	k8xC	sice
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odposlechem	odposlech	k1gInSc7	odposlech
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
latinsky	latinsky	k6eAd1	latinsky
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
znal	znát	k5eAaImAgInS	znát
i	i	k9	i
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
dvoře	dvůr	k1gInSc6	dvůr
společně	společně	k6eAd1	společně
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
panovník	panovník	k1gMnSc1	panovník
skládal	skládat	k5eAaImAgMnS	skládat
dodnes	dodnes	k6eAd1	dodnes
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
milostné	milostný	k2eAgFnPc4d1	milostná
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
v	v	k7c6	v
Codexu	Codex	k1gInSc6	Codex
Manesse	Manesse	k1gFnSc2	Manesse
i	i	k8xC	i
s	s	k7c7	s
celostránkovou	celostránkový	k2eAgFnSc7d1	celostránková
iluminací	iluminace	k1gFnSc7	iluminace
zpodobňující	zpodobňující	k2eAgFnSc2d1	zpodobňující
samotného	samotný	k2eAgMnSc4d1	samotný
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
se	se	k3xPyFc4	se
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
mnoho	mnoho	k4c1	mnoho
minesengrů	minesengr	k1gMnPc2	minesengr
<g/>
,	,	kIx,	,
těšících	těšící	k2eAgFnPc2d1	těšící
se	se	k3xPyFc4	se
panovníkově	panovníkův	k2eAgFnSc3d1	panovníkova
podpoře	podpora	k1gFnSc3	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Etzenbachu	Etzenbach	k1gInSc2	Etzenbach
zde	zde	k6eAd1	zde
složil	složit	k5eAaPmAgInS	složit
román	román	k1gInSc1	román
Wilhelm	Wilhelm	k1gInSc1	Wilhelm
von	von	k1gInSc1	von
Wenden	Wendna	k1gFnPc2	Wendna
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
postavy	postava	k1gFnSc2	postava
královny	královna	k1gFnSc2	královna
Guty	Guta	k1gFnSc2	Guta
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Clausner	Clausner	k1gMnSc1	Clausner
na	na	k7c4	na
královu	králův	k2eAgFnSc4d1	králova
zakázku	zakázka	k1gFnSc4	zakázka
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc6	Maria
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
Frauenlob	Frauenloba	k1gFnPc2	Frauenloba
pro	pro	k7c4	pro
uměnímilovného	uměnímilovný	k2eAgMnSc4d1	uměnímilovný
panovníka	panovník	k1gMnSc4	panovník
sepsal	sepsat	k5eAaPmAgInS	sepsat
Lejch	lejch	k1gInSc1	lejch
o	o	k7c6	o
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc6	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
oplýval	oplývat	k5eAaImAgMnS	oplývat
i	i	k9	i
dalšími	další	k2eAgInPc7d1	další
zájmy	zájem	k1gInPc7	zájem
–	–	k?	–
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
měl	mít	k5eAaImAgMnS	mít
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
zděděný	zděděný	k2eAgInSc1d1	zděděný
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
globus	globus	k1gInSc4	globus
a	a	k8xC	a
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
éry	éra	k1gFnSc2	éra
začal	začít	k5eAaPmAgInS	začít
vznikat	vznikat	k5eAaImF	vznikat
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
atlas	atlas	k1gInSc1	atlas
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
svaté	svatý	k2eAgInPc4d1	svatý
ostatky	ostatek	k1gInPc4	ostatek
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
ho	on	k3xPp3gMnSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
dcera	dcera	k1gFnSc1	dcera
Eliška	Eliška	k1gFnSc1	Eliška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
krále	král	k1gMnSc2	král
je	být	k5eAaImIp3nS	být
vykreslena	vykreslit	k5eAaPmNgFnS	vykreslit
v	v	k7c6	v
dobové	dobový	k2eAgFnSc6d1	dobová
Zbraslavské	zbraslavský	k2eAgFnSc6d1	Zbraslavská
kronice	kronika	k1gFnSc6	kronika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
krále	král	k1gMnSc4	král
idealizuje	idealizovat	k5eAaBmIp3nS	idealizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
Štýrská	štýrský	k2eAgFnSc1d1	štýrská
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
kronika	kronika	k1gFnSc1	kronika
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Otakara	Otakar	k1gMnSc2	Otakar
Štýrského	štýrský	k2eAgInSc2d1	štýrský
jej	on	k3xPp3gMnSc4	on
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k8xC	jako
poživačného	poživačný	k2eAgMnSc4d1	poživačný
uzurpátora	uzurpátor	k1gMnSc4	uzurpátor
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Dalimil	Dalimil	k1gMnSc1	Dalimil
sice	sice	k8xC	sice
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
králi	král	k1gMnSc3	král
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
církevním	církevní	k2eAgMnPc3d1	církevní
hodnostářům	hodnostář	k1gMnPc3	hodnostář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighieri	k1gNnSc2	Alighieri
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
nazval	nazvat	k5eAaPmAgMnS	nazvat
floutkem	floutek	k1gMnSc7	floutek
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
darmošlapem	darmošlap	k1gMnSc7	darmošlap
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Božské	božský	k2eAgFnSc6d1	božská
komedii	komedie	k1gFnSc6	komedie
do	do	k7c2	do
očistce	očistec	k1gInSc2	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
změkčilosti	změkčilost	k1gFnSc6	změkčilost
a	a	k8xC	a
chtíči	chtíč	k1gInSc6	chtíč
českého	český	k2eAgMnSc2d1	český
vladaře	vladař	k1gMnSc2	vladař
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nezaměnil	zaměnit	k5eNaPmAgMnS	zaměnit
otce	otec	k1gMnSc4	otec
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Václavem	Václav	k1gMnSc7	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
současné	současný	k2eAgFnSc2d1	současná
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
hrdinou	hrdina	k1gMnSc7	hrdina
románů	román	k1gInPc2	román
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Ludmily	Ludmila	k1gFnSc2	Ludmila
Vaňkové	Vaňkové	k2eAgNnPc4d1	Vaňkové
Zlá	zlý	k2eAgNnPc4d1	zlé
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
Dědici	dědic	k1gMnPc1	dědic
zlatého	zlatý	k2eAgMnSc2d1	zlatý
krále	král	k1gMnSc2	král
a	a	k8xC	a
Žebrák	Žebrák	k1gInSc4	Žebrák
se	s	k7c7	s
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
holí	hole	k1gFnSc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Daněk	Daněk	k1gMnSc1	Daněk
jej	on	k3xPp3gMnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
v	v	k7c6	v
Králi	Král	k1gMnSc6	Král
bez	bez	k7c2	bez
přilby	přilba	k1gFnSc2	přilba
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hanibal	Hanibal	k1gMnSc1	Hanibal
v	v	k7c6	v
Údělu	úděl	k1gInSc6	úděl
královském	královský	k2eAgInSc6d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
Václavův	Václavův	k2eAgInSc1d1	Václavův
deník	deník	k1gInSc1	deník
Deník	deník	k1gInSc1	deník
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
tíha	tíha	k1gFnSc1	tíha
království	království	k1gNnSc2	království
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
spočinula	spočinout	k5eAaPmAgFnS	spočinout
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
jen	jen	k9	jen
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Závišovy	Závišův	k2eAgFnSc2d1	Závišova
vedený	vedený	k2eAgInSc1d1	vedený
napsal	napsat	k5eAaPmAgMnS	napsat
Jan	Jan	k1gMnSc1	Jan
Lipšanský	Lipšanský	k2eAgMnSc1d1	Lipšanský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
1278	[number]	k4	1278
Guta	Gut	k2eAgFnSc1d1	Guta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
1271	[number]	k4	1271
<g/>
–	–	k?	–
<g/>
1297	[number]	k4	1297
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1288	[number]	k4	1288
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1289	[number]	k4	1289
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
∞	∞	k?	∞
1305	[number]	k4	1305
Viola	Viola	k1gFnSc1	Viola
Těšínská	Těšínská	k1gFnSc1	Těšínská
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1289	[number]	k4	1289
<g/>
–	–	k?	–
<g/>
1296	[number]	k4	1296
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1296	[number]	k4	1296
Ruprecht	Ruprecht	k2eAgInSc4d1	Ruprecht
Nasavský	Nasavský	k2eAgInSc4d1	Nasavský
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1290	[number]	k4	1290
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1313	[number]	k4	1313
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
∞	∞	k?	∞
1306	[number]	k4	1306
Jindřich	Jindřich	k1gMnSc1	Jindřich
Korutanský	korutanský	k2eAgInSc1d1	korutanský
</s>
</p>
<p>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1292	[number]	k4	1292
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1330	[number]	k4	1330
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
∞	∞	k?	∞
1310	[number]	k4	1310
Jan	Jana	k1gFnPc2	Jana
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
</s>
</p>
<p>
<s>
Guta	Guta	k1gFnSc1	Guta
(	(	kIx(	(
<g/>
1293	[number]	k4	1293
<g/>
–	–	k?	–
<g/>
1294	[number]	k4	1294
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1294	[number]	k4	1294
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1295	[number]	k4	1295
<g/>
–	–	k?	–
<g/>
1296	[number]	k4	1296
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1296	[number]	k4	1296
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1322	[number]	k4	1322
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
a	a	k8xC	a
lehnická	lehnický	k2eAgFnSc1d1	lehnický
kněžna	kněžna	k1gFnSc1	kněžna
∞	∞	k?	∞
1308	[number]	k4	1308
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Lehnický	Lehnický	k2eAgInSc1d1	Lehnický
</s>
</p>
<p>
<s>
Guta	Guta	k1gFnSc1	Guta
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1297	[number]	k4	1297
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
1300	[number]	k4	1300
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
(	(	kIx(	(
<g/>
1286	[number]	k4	1286
<g/>
–	–	k?	–
<g/>
1335	[number]	k4	1335
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
<g/>
–	–	k?	–
<g/>
1336	[number]	k4	1336
<g/>
/	/	kIx~	/
<g/>
1337	[number]	k4	1337
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javorská	javorský	k2eAgFnSc1d1	javorský
kněžna	kněžna	k1gFnSc1	kněžna
∞	∞	k?	∞
1316	[number]	k4	1316
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
JavorskýVáclav	JavorskýVáclav	k1gMnSc1	JavorskýVáclav
II	II	kA	II
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
levobočků	levoboček	k1gMnPc2	levoboček
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediné	k1gNnSc7	jediné
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
legitimně	legitimně	k6eAd1	legitimně
doložen	doložen	k2eAgMnSc1d1	doložen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Volek	Volek	k1gMnSc1	Volek
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1351	[number]	k4	1351
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Pokračovatelé	pokračovatel	k1gMnPc1	pokračovatel
Kosmovi	Kosmův	k2eAgMnPc1d1	Kosmův
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Marie	Marie	k1gFnSc1	Marie
Bláhová	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Hrdina	Hrdina	k1gMnSc1	Hrdina
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
Tomek	tomka	k1gFnPc2	tomka
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Bláhová	bláhový	k2eAgFnSc1d1	bláhová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
251	[number]	k4	251
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Příběhy	příběh	k1gInPc1	příběh
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
;	;	kIx,	;
Zlá	zlý	k2eAgNnPc4d1	zlé
léta	léto	k1gNnPc4	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
:	:	kIx,	:
Dva	dva	k4xCgInPc4	dva
současné	současný	k2eAgInPc4d1	současný
letopisy	letopis	k1gInPc4	letopis
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Rudolf	Rudolf	k1gMnSc1	Rudolf
Holinka	Holinka	k?	Holinka
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Hrdina	Hrdina	k1gMnSc1	Hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
95	[number]	k4	95
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
František	františek	k1gInSc1	františek
Heřmanský	Heřmanský	k2eAgInSc1d1	Heřmanský
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
597	[number]	k4	597
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
:	:	kIx,	:
epos	epos	k1gInSc1	epos
z	z	k7c2	z
konce	konec	k1gInSc2	konec
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
horní	horní	k2eAgFnSc2d1	horní
němčiny	němčina	k1gFnSc2	němčina
Ulrich	Ulrich	k1gMnSc1	Ulrich
von	von	k1gInSc1	von
Etzenbach	Etzenbach	k1gInSc4	Etzenbach
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gInSc4	Wilhelm
von	von	k1gInSc4	von
Wenden	Wendna	k1gFnPc2	Wendna
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dana	Dana	k1gFnSc1	Dana
Dvořáčková-Malá	Dvořáčková-Malý	k2eAgFnSc1d1	Dvořáčková-Malá
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
338	[number]	k4	338
s.	s.	k?	s.
:	:	kIx,	:
ilustrace	ilustrace	k1gFnSc2	ilustrace
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
<g/>
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
1671	[number]	k4	1671
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Hnězdenská	hnězdenský	k2eAgFnSc1d1	Hnězdenská
korunovace	korunovace	k1gFnSc1	korunovace
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
123	[number]	k4	123
<g/>
,	,	kIx,	,
s.	s.	k?	s.
337	[number]	k4	337
<g/>
–	–	k?	–
<g/>
365	[number]	k4	365
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	let	k1gInPc6	let
1283	[number]	k4	1283
<g/>
–	–	k?	–
<g/>
1289	[number]	k4	1289
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
126	[number]	k4	126
<g/>
,	,	kIx,	,
s.	s.	k?	s.
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Malopolsko	Malopolsko	k1gNnSc1	Malopolsko
jako	jako	k8xC	jako
objekt	objekt	k1gInSc1	objekt
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	let	k1gInPc6	let
1289	[number]	k4	1289
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
Historica	Historica	k1gFnSc1	Historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
979	[number]	k4	979
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Lenní	lenní	k2eAgInSc1d1	lenní
institut	institut	k1gInSc1	institut
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
při	při	k7c6	při
smíření	smíření	k1gNnSc6	smíření
v	v	k7c6	v
diplomacii	diplomacie	k1gFnSc6	diplomacie
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
NODL	NODL	kA	NODL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
smíření	smíření	k1gNnSc2	smíření
:	:	kIx,	:
konflikt	konflikt	k1gInSc1	konflikt
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
řešení	řešení	k1gNnSc1	řešení
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
konané	konaný	k2eAgFnSc2d1	konaná
ve	v	k7c6	v
dnech	den	k1gInPc6	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
109	[number]	k4	109
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
piastovském	piastovský	k2eAgNnSc6d1	piastovský
Polsku	Polsko	k1gNnSc6	Polsko
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
DOLEŽALOVÁ	Doležalová	k1gFnSc1	Doležalová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
;	;	kIx,	;
ŠIMŮNEK	Šimůnek	k1gMnSc1	Šimůnek
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
knížat	kníže	k1gMnPc2wR	kníže
ke	k	k7c3	k
králům	král	k1gMnPc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
60	[number]	k4	60
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
Josefa	Josef	k1gMnSc2	Josef
Žemličky	Žemlička	k1gMnSc2	Žemlička
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
896	[number]	k4	896
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
177	[number]	k4	177
<g/>
–	–	k?	–
<g/>
191	[number]	k4	191
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	let	k1gInPc6	let
1283	[number]	k4	1283
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
306	[number]	k4	306
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BAR	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Nieudana	Nieudana	k1gFnSc1	Nieudana
próba	próba	k1gFnSc1	próba
opanowania	opanowanium	k1gNnSc2	opanowanium
księstwa	księstwum	k1gNnSc2	księstwum
wrocławskiego	wrocławskiego	k1gMnSc1	wrocławskiego
przez	przez	k1gMnSc1	przez
Wacława	Wacława	k1gMnSc1	Wacława
II	II	kA	II
w	w	k?	w
latach	latach	k1gInSc1	latach
1289	[number]	k4	1289
<g/>
/	/	kIx~	/
<g/>
1290	[number]	k4	1290
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Józef	Józef	k1gMnSc1	Józef
Dobosz	Dobosz	k1gMnSc1	Dobosz
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Kujawiński	Kujawińsk	k1gFnSc2	Kujawińsk
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
Matla-Kozłowska	Matla-Kozłowska	k1gFnSc1	Matla-Kozłowska
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsze	Pierwsze	k1gFnSc1	Pierwsze
polsko-czeskie	polskozeskie	k1gFnSc2	polsko-czeskie
forum	forum	k1gNnSc1	forum
młodych	młodych	k1gInSc1	młodych
mediewistów	mediewistów	k?	mediewistów
<g/>
.	.	kIx.	.
</s>
<s>
Materiały	Materiały	k1gInPc1	Materiały
z	z	k7c2	z
konferencji	konferenc	k6eAd2	konferenc
naukowej	naukowat	k5eAaImRp2nS	naukowat
Gniezno	Gniezna	k1gFnSc5	Gniezna
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
września	wrześnium	k1gNnSc2	wrześnium
2005	[number]	k4	2005
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Instytut	Instytut	k1gMnSc1	Instytut
Historii	historie	k1gFnSc4	historie
UAM	UAM	kA	UAM
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
89407	[number]	k4	89407
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
201	[number]	k4	201
<g/>
–	–	k?	–
<g/>
210	[number]	k4	210
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BAR	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Probus	Probus	k1gMnSc1	Probus
a	a	k8xC	a
poslední	poslední	k2eAgMnPc1d1	poslední
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
106	[number]	k4	106
<g/>
,	,	kIx,	,
s.	s.	k?	s.
753	[number]	k4	753
<g/>
–	–	k?	–
<g/>
787	[number]	k4	787
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVOŘÁČKOVÁ-MALÁ	DVOŘÁČKOVÁ-MALÁ	k?	DVOŘÁČKOVÁ-MALÁ
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
zajetí	zajetí	k1gNnSc6	zajetí
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česko-braniborské	českoraniborský	k2eAgInPc4d1	česko-braniborský
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
BOBKOVÁ	Bobková	k1gFnSc1	Bobková
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
;	;	kIx,	;
KONVIČNÁ	KONVIČNÁ	kA	KONVIČNÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Korunní	korunní	k2eAgFnPc1d1	korunní
země	zem	k1gFnPc1	zem
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Společné	společný	k2eAgInPc1d1	společný
a	a	k8xC	a
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
v	v	k7c6	v
životě	život	k1gInSc6	život
a	a	k8xC	a
vědomí	vědomí	k1gNnSc1	vědomí
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
přednesených	přednesený	k2eAgInPc2d1	přednesený
na	na	k7c6	na
kolokviu	kolokvium	k1gNnSc6	kolokvium
pořádaném	pořádaný	k2eAgNnSc6d1	pořádané
ve	v	k7c6	v
dnech	den	k1gInPc6	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
v	v	k7c6	v
Clam-Gallasově	Clam-Gallasův	k2eAgInSc6d1	Clam-Gallasův
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
FF	ff	kA	ff
UK	UK	kA	UK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86971	[number]	k4	86971
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
129	[number]	k4	129
<g/>
–	–	k?	–
<g/>
158	[number]	k4	158
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVOŘÁČKOVÁ-MALÁ	DVOŘÁČKOVÁ-MALÁ	k?	DVOŘÁČKOVÁ-MALÁ
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
Anežka	Anežka	k1gFnSc1	Anežka
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
1269	[number]	k4	1269
<g/>
-	-	kIx~	-
<g/>
1296	[number]	k4	1296
<g/>
,	,	kIx,	,
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
Historica	Historica	k1gFnSc1	Historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
/	/	kIx~	/
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
194	[number]	k4	194
s.	s.	k?	s.
<g/>
,	,	kIx,	,
200	[number]	k4	200
s.	s.	k?	s.
<g/>
,	,	kIx,	,
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
979	[number]	k4	979
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
47-76	[number]	k4	47-76
</s>
</p>
<p>
<s>
DVOŘÁČKOVÁ-MALÁ	DVOŘÁČKOVÁ-MALÁ	k?	DVOŘÁČKOVÁ-MALÁ
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
348	[number]	k4	348
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVOŘÁČKOVÁ-MALÁ	DVOŘÁČKOVÁ-MALÁ	k?	DVOŘÁČKOVÁ-MALÁ
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
-	-	kIx~	-
ZELENKA	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
Přemyslovský	přemyslovský	k2eAgInSc1d1	přemyslovský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
králů	král	k1gMnPc2	král
a	a	k8xC	a
rytířů	rytíř	k1gMnPc2	rytíř
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
626	[number]	k4	626
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
276	[number]	k4	276
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
117	[number]	k4	117
<g/>
–	–	k?	–
<g/>
129	[number]	k4	129
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
343	[number]	k4	343
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
841	[number]	k4	841
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
se	s	k7c7	s
Závišem	Záviš	k1gMnSc7	Záviš
a	a	k8xC	a
proměna	proměna	k1gFnSc1	proměna
královské	královský	k2eAgFnSc2d1	královská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
letech	let	k1gInPc6	let
1289	[number]	k4	1289
<g/>
–	–	k?	–
<g/>
1290	[number]	k4	1290
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
103	[number]	k4	103
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
300	[number]	k4	300
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MITÁČEK	MITÁČEK	kA	MITÁČEK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Vládcové	vládce	k1gMnPc1	vládce
Moravy	Morava	k1gFnSc2	Morava
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
statí	stať	k1gFnPc2	stať
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
cyklu	cyklus	k1gInSc2	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7028	[number]	k4	7028
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
na	na	k7c6	na
stříbrném	stříbrný	k2eAgInSc6d1	stříbrný
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
739	[number]	k4	739
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
1544	[number]	k4	1544
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MALÁ	Malá	k1gFnSc1	Malá
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
pražského	pražský	k2eAgInSc2d1	pražský
dvora	dvůr	k1gInSc2	dvůr
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
Historica	Historica	k1gFnSc1	Historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
97	[number]	k4	97
<g/>
–	–	k?	–
<g/>
163	[number]	k4	163
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
979	[number]	k4	979
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
POJSL	POJSL	kA	POJSL
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
;	;	kIx,	;
ŘEHOLKA	ŘEHOLKA	kA	ŘEHOLKA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
SULITKOVÁ	SULITKOVÁ	kA	SULITKOVÁ
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Panovnická	panovnický	k2eAgFnSc1d1	panovnická
kancelář	kancelář	k1gFnSc1	kancelář
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
archivních	archivní	k2eAgFnPc2d1	archivní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
261	[number]	k4	261
<g/>
–	–	k?	–
<g/>
365	[number]	k4	365
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5246	[number]	k4	5246
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROKOPOVÁ	Prokopová	k1gFnSc1	Prokopová
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
založení	založení	k1gNnSc4	založení
Zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
146	[number]	k4	146
<g/>
–	–	k?	–
<g/>
158	[number]	k4	158
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Soumrak	soumrak	k1gInSc1	soumrak
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
803	[number]	k4	803
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Kus	kus	k1gInSc1	kus
středověké	středověký	k2eAgFnSc2d1	středověká
historie	historie	k1gFnSc2	historie
našeho	náš	k3xOp1gInSc2	náš
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
538	[number]	k4	538
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
376	[number]	k4	376
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Skládal	skládat	k5eAaImAgMnS	skládat
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
básně	báseň	k1gFnPc1	báseň
milostné	milostný	k2eAgFnPc1d1	milostná
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
s.	s.	k?	s.
217	[number]	k4	217
<g/>
–	–	k?	–
<g/>
244	[number]	k4	244
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
polská	polský	k2eAgFnSc1d1	polská
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
s.	s.	k?	s.
313	[number]	k4	313
<g/>
–	–	k?	–
<g/>
346	[number]	k4	346
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VALECKÝ	VALECKÝ	kA	VALECKÝ
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Erb	erb	k1gInSc1	erb
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Heraldická	heraldický	k2eAgFnSc1d1	heraldická
ročenka	ročenka	k1gFnSc1	ročenka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Heraldická	heraldický	k2eAgFnSc1d1	heraldická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
904942	[number]	k4	904942
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
173	[number]	k4	173
<g/>
–	–	k?	–
<g/>
187	[number]	k4	187
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
III	III	kA	III
<g/>
.	.	kIx.	.
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
760	[number]	k4	760
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	mikuláš	k1gInSc4	mikuláš
I.	I.	kA	I.
Opavský	opavský	k2eAgInSc4d1	opavský
mezi	mezi	k7c7	mezi
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
99	[number]	k4	99
<g/>
,	,	kIx,	,
s.	s.	k?	s.
209	[number]	k4	209
<g/>
–	–	k?	–
<g/>
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
281	[number]	k4	281
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Václav	Václava	k1gFnPc2	Václava
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Filipem	Filip	k1gMnSc7	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Sličným	sličný	k2eAgFnPc3d1	sličná
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
e-stredovek	etredovka	k1gFnPc2	e-stredovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
mincovnictví	mincovnictví	k1gNnSc4	mincovnictví
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
stribrnak	stribrnak	k1gInSc1	stribrnak
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008-02-01	[number]	k4	2008-02-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
aneb	aneb	k?	aneb
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
začalo	začít	k5eAaPmAgNnS	začít
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
numismatická	numismatický	k2eAgFnSc1d1	numismatická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
pobočka	pobočka	k1gFnSc1	pobočka
Pražské	pražský	k2eAgInPc1d1	pražský
groše	groš	k1gInPc1	groš
<g/>
,	,	kIx,	,
2009-09-15	[number]	k4	2009-09-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wacław	Wacław	k?	Wacław
II	II	kA	II
Czeski	Czesk	k1gFnSc2	Czesk
(	(	kIx(	(
<g/>
Przemyślida	Przemyślida	k1gFnSc1	Przemyślida
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Poczet	Poczet	k5eAaImF	Poczet
władców	władców	k?	władców
polski	polske	k1gFnSc4	polske
i	i	k9	i
rzeczypospolitej	rzeczypospolitat	k5eAaPmRp2nS	rzeczypospolitat
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wenzel	Wenzet	k5eAaPmAgMnS	Wenzet
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
König	König	k1gInSc1	König
von	von	k1gInSc1	von
Böhmen	Böhmen	k1gInSc1	Böhmen
(	(	kIx(	(
<g/>
1278	[number]	k4	1278
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
König	König	k1gInSc1	König
von	von	k1gInSc1	von
Polen	poleno	k1gNnPc2	poleno
(	(	kIx(	(
<g/>
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Genealogie	genealogie	k1gFnSc1	genealogie
Mittelalter	Mittelaltra	k1gFnPc2	Mittelaltra
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.ceskatelevize.cz/porady/10169539755-dvaasedmdesat-jmen-ceske-historie/208572232200004-vaclav-ii/	[url]	k4	http://www.ceskatelevize.cz/porady/10169539755-dvaasedmdesat-jmen-ceske-historie/208572232200004-vaclav-ii/
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
</s>
</p>
