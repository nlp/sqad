<s>
OSN	OSN	kA	OSN
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přijetí	přijetí	k1gNnSc2	přijetí
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
50	[number]	k4	50
státy	stát	k1gInPc7	stát
včetně	včetně	k7c2	včetně
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
významné	významný	k2eAgFnSc2d1	významná
podpory	podpora	k1gFnSc2	podpora
Rockefellerova	Rockefellerův	k2eAgInSc2d1	Rockefellerův
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
