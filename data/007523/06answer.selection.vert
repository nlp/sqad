<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
kopytníky	kopytník	k1gInPc4	kopytník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
další	další	k2eAgNnPc4d1	další
atraktivní	atraktivní	k2eAgNnPc4d1	atraktivní
zvířata	zvíře	k1gNnPc4	zvíře
jako	jako	k8xS	jako
lední	lední	k2eAgMnPc4d1	lední
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
opice	opice	k1gFnPc4	opice
apod.	apod.	kA	apod.
</s>
