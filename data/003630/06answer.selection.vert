<s>
Druhým	druhý	k4xOgInSc7	druhý
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
hradem	hrad	k1gInSc7	hrad
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
Veveří	veveří	k2eAgNnSc1d1	veveří
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tyčící	tyčící	k2eAgMnSc1d1	tyčící
nad	nad	k7c7	nad
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
