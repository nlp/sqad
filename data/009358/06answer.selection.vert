<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
přípustná	přípustný	k2eAgFnSc1d1	přípustná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
posouzení	posouzení	k1gNnSc2	posouzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
i	i	k8xC	i
prognózy	prognóza	k1gFnSc2	prognóza
Odbornou	odborný	k2eAgFnSc7d1	odborná
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
změny	změna	k1gFnSc2	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
transsexuálních	transsexuální	k2eAgMnPc2d1	transsexuální
pacientů	pacient	k1gMnPc2	pacient
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
