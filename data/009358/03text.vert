<p>
<s>
Transsexualita	Transsexualita	k1gFnSc1	Transsexualita
(	(	kIx(	(
<g/>
odborně	odborně	k6eAd1	odborně
též	též	k9	též
transsexualismus	transsexualismus	k1gInSc1	transsexualismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
nesouladu	nesoulad	k1gInSc2	nesoulad
mezi	mezi	k7c7	mezi
psychickým	psychický	k2eAgNnSc7d1	psychické
a	a	k8xC	a
anatomickým	anatomický	k2eAgNnSc7d1	anatomické
pohlavím	pohlaví	k1gNnSc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
neztotožněním	neztotožnění	k1gNnSc7	neztotožnění
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
biologickým	biologický	k2eAgNnSc7d1	biologické
pohlavím	pohlaví	k1gNnSc7	pohlaví
a	a	k8xC	a
touhou	touha	k1gFnSc7	touha
být	být	k5eAaImF	být
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
jeho	jeho	k3xOp3gInPc4	jeho
tělesné	tělesný	k2eAgInPc4d1	tělesný
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
projevy	projev	k1gInPc4	projev
<g/>
.	.	kIx.	.
</s>
<s>
Transsexuál	transsexuál	k1gMnSc1	transsexuál
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
primární	primární	k2eAgInPc4d1	primární
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
znaky	znak	k1gInPc4	znak
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
ženou	žena	k1gFnSc7	žena
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
male	male	k6eAd1	male
to	ten	k3xDgNnSc1	ten
female	female	k6eAd1	female
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
MtF	MtF	k1gFnSc1	MtF
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
z	z	k7c2	z
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
primární	primární	k2eAgInPc4d1	primární
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
znaky	znak	k1gInPc4	znak
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
mužem	muž	k1gMnSc7	muž
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
female	female	k6eAd1	female
to	ten	k3xDgNnSc1	ten
male	male	k6eAd1	male
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
FtM	FtM	k1gFnSc1	FtM
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
z	z	k7c2	z
ženy	žena	k1gFnSc2	žena
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
transsexuála	transsexuál	k1gMnSc4	transsexuál
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
zkratka	zkratka	k1gFnSc1	zkratka
Ts	ts	k0	ts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sexuální	sexuální	k2eAgFnSc1d1	sexuální
identita	identita	k1gFnSc1	identita
(	(	kIx(	(
<g/>
identifikace	identifikace	k1gFnSc1	identifikace
<g/>
)	)	kIx)	)
či	či	k8xC	či
úžeji	úzko	k6eAd2	úzko
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
identita	identita	k1gFnSc1	identita
(	(	kIx(	(
<g/>
identifikace	identifikace	k1gFnSc1	identifikace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
transsexuality	transsexualita	k1gFnSc2	transsexualita
rovněž	rovněž	k9	rovněž
nejsou	být	k5eNaImIp3nP	být
charakteristiky	charakteristika	k1gFnPc1	charakteristika
sexuální	sexuální	k2eAgFnPc1d1	sexuální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
charakteristiky	charakteristika	k1gFnPc1	charakteristika
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
transsexualitou	transsexualita	k1gFnSc7	transsexualita
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
důležitější	důležitý	k2eAgMnSc1d2	důležitější
naplňovat	naplňovat	k5eAaImF	naplňovat
sociální	sociální	k2eAgInPc4d1	sociální
aspekty	aspekt	k1gInPc4	aspekt
vytoužené	vytoužený	k2eAgFnSc2d1	vytoužená
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
role	role	k1gFnSc2	role
než	než	k8xS	než
její	její	k3xOp3gInPc1	její
aspekty	aspekt	k1gInPc1	aspekt
sexuální	sexuální	k2eAgInPc1d1	sexuální
<g/>
.	.	kIx.	.
</s>
<s>
Vybírají	vybírat	k5eAaImIp3nP	vybírat
si	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
profese	profes	k1gFnPc4	profes
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
zájmy	zájem	k1gInPc1	zájem
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
společnostech	společnost	k1gFnPc6	společnost
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lidé	člověk	k1gMnPc1	člověk
transsexualitu	transsexualit	k1gInSc2	transsexualit
prožívají	prožívat	k5eAaImIp3nP	prožívat
i	i	k9	i
jak	jak	k6eAd1	jak
společnost	společnost	k1gFnSc4	společnost
k	k	k7c3	k
transsexuálním	transsexuální	k2eAgMnPc3d1	transsexuální
lidem	člověk	k1gMnPc3	člověk
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
zformovala	zformovat	k5eAaPmAgFnS	zformovat
hnutí	hnutí	k1gNnSc4	hnutí
transsexuálů	transsexuál	k1gMnPc2	transsexuál
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
a	a	k8xC	a
důstojnost	důstojnost	k1gFnSc4	důstojnost
transsexuálních	transsexuální	k2eAgMnPc2d1	transsexuální
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
i	i	k8xC	i
úřední	úřední	k2eAgFnSc4d1	úřední
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
těmto	tento	k3xDgFnPc3	tento
snahám	snaha	k1gFnPc3	snaha
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
odmítavě	odmítavě	k6eAd1	odmítavě
nebo	nebo	k8xC	nebo
rozpačitě	rozpačitě	k6eAd1	rozpačitě
konzervativnější	konzervativní	k2eAgFnSc6d2	konzervativnější
části	část	k1gFnSc6	část
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
diagnostika	diagnostika	k1gFnSc1	diagnostika
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
MKN-10	MKN-10	k1gFnSc2	MKN-10
uvádí	uvádět	k5eAaImIp3nS	uvádět
tzv.	tzv.	kA	tzv.
poruchy	porucha	k1gFnSc2	porucha
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
identity	identita	k1gFnSc2	identita
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
F64	F64	k1gFnSc2	F64
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
64.0	[number]	k4	64.0
<g/>
:	:	kIx,	:
Transsexualizmus	Transsexualizmus	k1gInSc1	Transsexualizmus
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
64.2	[number]	k4	64.2
Porucha	porucha	k1gFnSc1	porucha
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
identity	identita	k1gFnSc2	identita
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transsexualizmus	Transsexualizmus	k1gInSc1	Transsexualizmus
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
přání	přání	k1gNnSc1	přání
žít	žít	k5eAaImF	žít
a	a	k8xC	a
být	být	k5eAaImF	být
akceptován	akceptován	k2eAgMnSc1d1	akceptován
jako	jako	k8xS	jako
příslušník	příslušník	k1gMnSc1	příslušník
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
označuje	označovat	k5eAaImIp3nS	označovat
pocit	pocit	k1gInSc4	pocit
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
anatomickým	anatomický	k2eAgNnSc7d1	anatomické
pohlavím	pohlaví	k1gNnSc7	pohlaví
nebo	nebo	k8xC	nebo
pocit	pocit	k1gInSc1	pocit
jeho	jeho	k3xOp3gFnSc2	jeho
nevhodnosti	nevhodnost	k1gFnSc2	nevhodnost
a	a	k8xC	a
přání	přání	k1gNnSc2	přání
hormonálního	hormonální	k2eAgNnSc2d1	hormonální
léčení	léčení	k1gNnSc2	léčení
a	a	k8xC	a
chirurgického	chirurgický	k2eAgInSc2d1	chirurgický
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tělo	tělo	k1gNnSc1	tělo
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
preferovanému	preferovaný	k2eAgNnSc3d1	preferované
pohlaví	pohlaví	k1gNnSc3	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
této	tento	k3xDgFnSc2	tento
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
přítomnost	přítomnost	k1gFnSc1	přítomnost
transsexuální	transsexuální	k2eAgFnSc2d1	transsexuální
identity	identita	k1gFnSc2	identita
trvale	trvale	k6eAd1	trvale
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
žádné	žádný	k3yNgFnSc2	žádný
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
)	)	kIx)	)
intersexuální	intersexuální	k2eAgFnPc4d1	intersexuální
<g/>
,	,	kIx,	,
genetické	genetický	k2eAgFnPc4d1	genetická
nebo	nebo	k8xC	nebo
chromozomové	chromozomový	k2eAgFnPc4d1	chromozomová
abnormality	abnormalita	k1gFnPc4	abnormalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
diagnostický	diagnostický	k2eAgInSc1d1	diagnostický
a	a	k8xC	a
statistický	statistický	k2eAgInSc1d1	statistický
manuál	manuál	k1gInSc1	manuál
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
"	"	kIx"	"
<g/>
transsexualism	transsexualism	k6eAd1	transsexualism
<g/>
"	"	kIx"	"
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
DSM-III	DSM-III	k1gFnSc2	DSM-III
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
DSM-III-R	DSM-III-R	k1gFnSc2	DSM-III-R
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
DSM-IV	DSM-IV	k1gFnSc2	DSM-IV
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
diagnózou	diagnóza	k1gFnSc7	diagnóza
302.85	[number]	k4	302.85
"	"	kIx"	"
<g/>
gender	gender	k1gInSc1	gender
identity	identita	k1gFnSc2	identita
disorders	disordersa	k1gFnPc2	disordersa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
v	v	k7c6	v
DSM-IV-TR	DSM-IV-TR	k1gFnSc6	DSM-IV-TR
<g/>
.	.	kIx.	.
</s>
<s>
Pojetí	pojetí	k1gNnSc1	pojetí
transsexuality	transsexualita	k1gFnSc2	transsexualita
v	v	k7c6	v
připravovaném	připravovaný	k2eAgNnSc6d1	připravované
vydání	vydání	k1gNnSc6	vydání
DSM-V	DSM-V	k1gFnSc2	DSM-V
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
značného	značný	k2eAgInSc2d1	značný
mediálního	mediální	k2eAgInSc2d1	mediální
a	a	k8xC	a
lobbistického	lobbistický	k2eAgInSc2d1	lobbistický
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
organizace	organizace	k1gFnSc2	organizace
transsexuálů	transsexuál	k1gMnPc2	transsexuál
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
transsexualita	transsexualita	k1gFnSc1	transsexualita
nebyla	být	k5eNaImAgFnS	být
patologizována	patologizován	k2eAgFnSc1d1	patologizován
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
právě	právě	k6eAd1	právě
zařazení	zařazení	k1gNnSc3	zařazení
transsexuality	transsexualita	k1gFnSc2	transsexualita
mezi	mezi	k7c4	mezi
poruchy	porucha	k1gFnPc4	porucha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
nákladech	náklad	k1gInPc6	náklad
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Lékařství	lékařství	k1gNnSc1	lékařství
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
transsexualita	transsexualita	k1gFnSc1	transsexualita
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
psychiatrických	psychiatrický	k2eAgFnPc6d1	psychiatrická
poruchách	porucha	k1gFnPc6	porucha
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
uvedena	uvést	k5eAaPmNgFnS	uvést
ve	v	k7c6	v
vrozených	vrozený	k2eAgFnPc6d1	vrozená
vadách	vada	k1gFnPc6	vada
<g/>
,	,	kIx,	,
deformacích	deformace	k1gFnPc6	deformace
a	a	k8xC	a
chromozomálních	chromozomální	k2eAgFnPc6d1	chromozomální
abnormalitách	abnormalita	k1gFnPc6	abnormalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
neurology	neurolog	k1gMnPc7	neurolog
a	a	k8xC	a
sexuology	sexuolog	k1gMnPc7	sexuolog
postupně	postupně	k6eAd1	postupně
převažuje	převažovat	k5eAaImIp3nS	převažovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychické	psychický	k2eAgNnSc1d1	psychické
pohlaví	pohlaví	k1gNnSc1	pohlaví
je	být	k5eAaImIp3nS	být
determinované	determinovaný	k2eAgNnSc1d1	determinované
speciálním	speciální	k2eAgInSc7d1	speciální
mozkovým	mozkový	k2eAgInSc7d1	mozkový
centrem	centr	k1gInSc7	centr
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
identity	identita	k1gFnSc2	identita
a	a	k8xC	a
že	že	k8xS	že
psychická	psychický	k2eAgFnSc1d1	psychická
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
identita	identita	k1gFnSc1	identita
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
nejpozději	pozdě	k6eAd3	pozdě
během	během	k7c2	během
nitroděložního	nitroděložní	k2eAgInSc2d1	nitroděložní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
výzkumy	výzkum	k1gInPc1	výzkum
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
stoprocentně	stoprocentně	k6eAd1	stoprocentně
exaktní	exaktní	k2eAgFnPc1d1	exaktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístupy	přístup	k1gInPc1	přístup
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reparativní	reparativní	k2eAgFnPc1d1	reparativní
terapie	terapie	k1gFnPc1	terapie
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
psychiatrické	psychiatrický	k2eAgInPc1d1	psychiatrický
a	a	k8xC	a
psychologické	psychologický	k2eAgInPc1d1	psychologický
přístupy	přístup	k1gInPc1	přístup
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
psychiku	psychika	k1gFnSc4	psychika
transsexuála	transsexuál	k1gMnSc2	transsexuál
jeho	jeho	k3xOp3gNnSc3	jeho
anatomickému	anatomický	k2eAgNnSc3d1	anatomické
pohlaví	pohlaví	k1gNnSc3	pohlaví
a	a	k8xC	a
vést	vést	k5eAaImF	vést
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
anatomickým	anatomický	k2eAgNnSc7d1	anatomické
pohlavím	pohlaví	k1gNnSc7	pohlaví
smířil	smířit	k5eAaPmAgMnS	smířit
a	a	k8xC	a
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
mu	on	k3xPp3gInSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
životní	životní	k2eAgFnSc4d1	životní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
překonaný	překonaný	k2eAgInSc4d1	překonaný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
jako	jako	k9	jako
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
alternativ	alternativa	k1gFnPc2	alternativa
zabývají	zabývat	k5eAaImIp3nP	zabývat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Genderový	Genderový	k2eAgInSc1d1	Genderový
nomádismus	nomádismus	k1gInSc1	nomádismus
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
přístupy	přístup	k1gInPc1	přístup
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
zejména	zejména	k9	zejména
z	z	k7c2	z
myšlenkového	myšlenkový	k2eAgNnSc2d1	myšlenkové
zázemí	zázemí	k1gNnSc2	zázemí
feminismu	feminismus	k1gInSc2	feminismus
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
problém	problém	k1gInSc4	problém
především	především	k9	především
ve	v	k7c6	v
stereotypních	stereotypní	k2eAgFnPc6d1	stereotypní
rolích	role	k1gFnPc6	role
a	a	k8xC	a
očekáváních	očekávání	k1gNnPc6	očekávání
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
přísné	přísný	k2eAgFnSc3d1	přísná
dichotomii	dichotomie	k1gFnSc3	dichotomie
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
hájí	hájit	k5eAaImIp3nP	hájit
široký	široký	k2eAgInSc4d1	široký
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
role	role	k1gFnPc4	role
a	a	k8xC	a
stavy	stav	k1gInPc4	stav
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přeměna	přeměna	k1gFnSc1	přeměna
==	==	k?	==
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
dominantní	dominantní	k2eAgInSc1d1	dominantní
přístup	přístup	k1gInSc1	přístup
lékařských	lékařský	k2eAgFnPc2d1	lékařská
institucí	instituce	k1gFnPc2	instituce
i	i	k8xC	i
organizací	organizace	k1gFnPc2	organizace
transsexuálů	transsexuál	k1gMnPc2	transsexuál
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
optimální	optimální	k2eAgNnSc4d1	optimální
řešení	řešení	k1gNnSc4	řešení
hormonální	hormonální	k2eAgFnSc4d1	hormonální
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
chirurgické	chirurgický	k2eAgInPc4d1	chirurgický
zásahy	zásah	k1gInPc4	zásah
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
anatomického	anatomický	k2eAgNnSc2d1	anatomické
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
to	ten	k3xDgNnSc1	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
stanoví	stanovit	k5eAaPmIp3nS	stanovit
i	i	k9	i
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
úřední	úřední	k2eAgFnSc4d1	úřední
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
přípustná	přípustný	k2eAgFnSc1d1	přípustná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
posouzení	posouzení	k1gNnSc2	posouzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
i	i	k8xC	i
prognózy	prognóza	k1gFnSc2	prognóza
Odbornou	odborný	k2eAgFnSc7d1	odborná
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
změny	změna	k1gFnSc2	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
transsexuálních	transsexuální	k2eAgMnPc2d1	transsexuální
pacientů	pacient	k1gMnPc2	pacient
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
osobní	osobní	k2eAgInPc4d1	osobní
poměry	poměr	k1gInPc4	poměr
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Automaticky	automaticky	k6eAd1	automaticky
však	však	k9	však
zaniká	zanikat	k5eAaImIp3nS	zanikat
manželství	manželství	k1gNnSc1	manželství
nebo	nebo	k8xC	nebo
registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
<g/>
.	.	kIx.	.
</s>
<s>
Weiss	Weiss	k1gMnSc1	Weiss
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Klient	klient	k1gMnSc1	klient
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgFnPc7	tři
variantami	varianta	k1gFnPc7	varianta
–	–	k?	–
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
biologické	biologický	k2eAgNnSc4d1	biologické
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
psychické	psychický	k2eAgNnSc4d1	psychické
pohlaví	pohlaví	k1gNnSc4	pohlaví
a	a	k8xC	a
procesem	proces	k1gInSc7	proces
přeměny	přeměna	k1gFnSc2	přeměna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
dobře	dobře	k6eAd1	dobře
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
typ	typ	k1gInSc4	typ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
s	s	k7c7	s
transsexualitou	transsexualita	k1gFnSc7	transsexualita
převažuje	převažovat	k5eAaImIp3nS	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
máme	mít	k5eAaImIp1nP	mít
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
od	od	k7c2	od
klientů	klient	k1gMnPc2	klient
našich	naši	k1gMnPc2	naši
ordinací	ordinace	k1gFnPc2	ordinace
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
existuje	existovat	k5eAaImIp3nS	existovat
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
lékaře	lékař	k1gMnSc4	lékař
...	...	k?	...
nikdy	nikdy	k6eAd1	nikdy
neobrátí	obrátit	k5eNaPmIp3nP	obrátit
a	a	k8xC	a
adaptují	adaptovat	k5eAaBmIp3nP	adaptovat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
asistence	asistence	k1gFnSc2	asistence
pomáhajících	pomáhající	k2eAgFnPc2d1	pomáhající
profesí	profes	k1gFnPc2	profes
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
z	z	k7c2	z
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Operativní	operativní	k2eAgFnSc3d1	operativní
změně	změna	k1gFnSc3	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
předchází	předcházet	k5eAaImIp3nS	předcházet
tzv.	tzv.	kA	tzv.
RLE	RLE	kA	RLE
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
real	real	k1gInSc1	real
life	lif	k1gMnSc2	lif
experience	experienec	k1gMnSc2	experienec
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
člověk	člověk	k1gMnSc1	člověk
činí	činit	k5eAaImIp3nS	činit
novou	nový	k2eAgFnSc4d1	nová
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
)	)	kIx)	)
a	a	k8xC	a
RLT	RLT	kA	RLT
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
real	realit	k5eAaPmRp2nS	realit
life	life	k1gNnSc4	life
test	test	k1gInSc1	test
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
člověk	člověk	k1gMnSc1	člověk
testuje	testovat	k5eAaImIp3nS	testovat
správnost	správnost	k1gFnSc4	správnost
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
nejméně	málo	k6eAd3	málo
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
psychickou	psychický	k2eAgFnSc7d1	psychická
identitou	identita	k1gFnSc7	identita
<g/>
,	,	kIx,	,
a	a	k8xC	a
podávání	podávání	k1gNnSc1	podávání
hormonálních	hormonální	k2eAgInPc2d1	hormonální
přípravků	přípravek	k1gInPc2	přípravek
(	(	kIx(	(
<g/>
u	u	k7c2	u
MtF	MtF	k1gFnSc2	MtF
antiandrogenů	antiandrogen	k1gInPc2	antiandrogen
a	a	k8xC	a
estrogenů	estrogen	k1gInPc2	estrogen
<g/>
,	,	kIx,	,
u	u	k7c2	u
FtM	FtM	k1gFnSc2	FtM
testosteronů	testosteron	k1gInPc2	testosteron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pořadí	pořadí	k1gNnSc3	pořadí
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
fází	fáze	k1gFnPc2	fáze
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
FtM	FtM	k?	FtM
v	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
k	k	k7c3	k
sexuologovi	sexuolog	k1gMnSc3	sexuolog
již	již	k6eAd1	již
většinou	většinou	k6eAd1	většinou
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
roli	role	k1gFnSc6	role
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
MtF	MtF	k1gFnSc2	MtF
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
často	často	k6eAd1	často
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
hormonální	hormonální	k2eAgFnSc3d1	hormonální
terapii	terapie	k1gFnSc3	terapie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
snadnější	snadný	k2eAgInSc4d2	snadnější
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
RLT	RLT	kA	RLT
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
operativní	operativní	k2eAgFnSc1d1	operativní
změna	změna	k1gFnSc1	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
anatomických	anatomický	k2eAgMnPc2d1	anatomický
mužů	muž	k1gMnPc2	muž
spočívá	spočívat	k5eAaImIp3nS	spočívat
chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
změna	změna	k1gFnSc1	změna
v	v	k7c4	v
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
odstranění	odstranění	k1gNnSc6	odstranění
penisu	penis	k1gInSc2	penis
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
vagíny	vagína	k1gFnSc2	vagína
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgFnSc1d1	hormonální
léčba	léčba	k1gFnSc1	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
podávání	podávání	k1gNnSc6	podávání
hormonálního	hormonální	k2eAgInSc2d1	hormonální
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zaoblování	zaoblování	k1gNnSc4	zaoblování
boků	bok	k1gInPc2	bok
<g/>
,	,	kIx,	,
růst	růst	k1gInSc4	růst
prsou	prsa	k1gNnPc2	prsa
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
tvaru	tvar	k1gInSc2	tvar
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
redukci	redukce	k1gFnSc3	redukce
ochlupení	ochlupení	k1gNnSc2	ochlupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
anatomických	anatomický	k2eAgFnPc2d1	anatomická
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
změna	změna	k1gFnSc1	změna
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
úřední	úřední	k2eAgFnSc4d1	úřední
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
odstranění	odstranění	k1gNnSc6	odstranění
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgFnSc1d1	hormonální
léčba	léčba	k1gFnSc1	léčba
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zastavení	zastavení	k1gNnSc4	zastavení
menstruace	menstruace	k1gFnSc2	menstruace
<g/>
,	,	kIx,	,
mohutnění	mohutnění	k1gNnSc2	mohutnění
svalové	svalový	k2eAgFnSc2d1	svalová
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
růst	růst	k1gInSc4	růst
ochlupení	ochlupení	k1gNnPc2	ochlupení
a	a	k8xC	a
vousů	vous	k1gInPc2	vous
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesnáze	nesnáz	k1gFnPc4	nesnáz
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
neopenisu	neopenis	k1gInSc6	neopenis
někteří	některý	k3yIgMnPc1	některý
FtM	FtM	k1gMnPc1	FtM
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
varianty	varianta	k1gFnSc2	varianta
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnutí	hnutí	k1gNnSc1	hnutí
transsexuálů	transsexuál	k1gMnPc2	transsexuál
==	==	k?	==
</s>
</p>
<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
zřetelněji	zřetelně	k6eAd2	zřetelně
formovat	formovat	k5eAaImF	formovat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
počalo	počnout	k5eAaPmAgNnS	počnout
o	o	k7c4	o
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
značné	značný	k2eAgNnSc4d1	značné
splynutí	splynutí	k1gNnSc4	splynutí
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
<g/>
,	,	kIx,	,
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
a	a	k8xC	a
transsexuální	transsexuální	k2eAgFnSc2d1	transsexuální
problematiky	problematika	k1gFnSc2	problematika
pod	pod	k7c4	pod
zkratku	zkratka	k1gFnSc4	zkratka
LGBT	LGBT	kA	LGBT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Odborná	odborný	k2eAgFnSc1d1	odborná
====	====	k?	====
</s>
</p>
<p>
<s>
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sexuologie	sexuologie	k1gFnSc1	sexuologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
724	[number]	k4	724
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2492	[number]	k4	2492
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
FIFKOVÁ	FIFKOVÁ	kA	FIFKOVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	porucha	k1gFnPc1	porucha
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
identity	identita	k1gFnSc2	identita
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
v	v	k7c6	v
dospívání	dospívání	k1gNnSc6	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Pediatrie	pediatrie	k1gFnSc1	pediatrie
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
174	[number]	k4	174
<g/>
–	–	k?	–
<g/>
176	[number]	k4	176
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
www.pediatriepropraxi.cz/pdfs/ped/2010/03/09.pdf	www.pediatriepropraxi.cz/pdfs/ped/2010/03/09.pdf	k1gInSc1	www.pediatriepropraxi.cz/pdfs/ped/2010/03/09.pdf
</s>
</p>
<p>
<s>
FIFKOVÁ	FIFKOVÁ	kA	FIFKOVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Transsexualita	Transsexualita	k1gFnSc1	Transsexualita
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
poruchy	porucha	k1gFnPc1	porucha
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
202	[number]	k4	202
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1696	[number]	k4	1696
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Spoluautoři	spoluautor	k1gMnPc1	spoluautor
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Peggy	Pegga	k1gFnPc1	Pegga
T.	T.	kA	T.
Cohen	Cohen	k2eAgInSc1d1	Cohen
<g/>
–	–	k?	–
<g/>
Kettenis	Kettenis	k1gInSc1	Kettenis
<g/>
,	,	kIx,	,
Friedemann	Friedemann	k1gInSc1	Friedemann
Pfäfflin	Pfäfflin	k1gInSc1	Pfäfflin
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Jarolím	Jarole	k1gFnPc3	Jarole
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
FIFKOVÁ	FIFKOVÁ	kA	FIFKOVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Transsexualita	Transsexualita	k1gFnSc1	Transsexualita
<g/>
:	:	kIx,	:
diagnostika	diagnostika	k1gFnSc1	diagnostika
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
166	[number]	k4	166
s.	s.	k?	s.
Edice	edice	k1gFnSc1	edice
Psyché	psyché	k1gFnSc1	psyché
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
333	[number]	k4	333
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
ZVĚŘINA	Zvěřina	k1gMnSc1	Zvěřina
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgNnSc1d1	sexuální
chování	chování	k1gNnSc1	chování
v	v	k7c6	v
ČR	ČR	kA	ČR
–	–	k?	–
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
trendy	trend	k1gInPc4	trend
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7178	[number]	k4	7178
<g/>
-	-	kIx~	-
<g/>
558	[number]	k4	558
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
FIFKOVÁ	FIFKOVÁ	kA	FIFKOVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
přeměny	přeměna	k1gFnSc2	přeměna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
č.	č.	k?	č.
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
26	[number]	k4	26
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
9607	[number]	k4	9607
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEJBALOVÁ	SEJBALOVÁ	kA	SEJBALOVÁ
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
MÍCHALOVÁ	MÍCHALOVÁ	kA	MÍCHALOVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
v	v	k7c4	v
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
sexuologické	sexuologický	k2eAgMnPc4d1	sexuologický
pacienty	pacient	k1gMnPc4	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
266	[number]	k4	266
<g/>
–	–	k?	–
<g/>
268	[number]	k4	268
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213	[number]	k4	1213
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
508	[number]	k4	508
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kvalifikační	kvalifikační	k2eAgFnSc2d1	kvalifikační
práce	práce	k1gFnSc2	práce
====	====	k?	====
</s>
</p>
<p>
<s>
MICHÁLKOVÁ	Michálková	k1gFnSc1	Michálková
<g/>
,	,	kIx,	,
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
.	.	kIx.	.
</s>
<s>
Transsexuální	Transsexuální	k2eAgMnPc1d1	Transsexuální
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
minoritu	minorita	k1gFnSc4	minorita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
<g/>
,	,	kIx,	,
13	[number]	k4	13
s.	s.	k?	s.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ved	Ved	k?	Ved
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
Mgr.	Mgr.	kA	Mgr.
Monika	Monika	k1gFnSc1	Monika
Nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
oponent	oponent	k1gMnSc1	oponent
MUDr.	MUDr.	kA	MUDr.
Jiřina	Jiřina	k1gFnSc1	Jiřina
Ondrušová	Ondrušová	k1gFnSc1	Ondrušová
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Husitská	husitský	k2eAgFnSc1d1	husitská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
psychosociálních	psychosociální	k2eAgFnPc2d1	psychosociální
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
etiky	etika	k1gFnSc2	etika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Populárně-naučná	Populárněaučný	k2eAgNnPc4d1	Populárně-naučný
====	====	k?	====
</s>
</p>
<p>
<s>
FEINBERG	FEINBERG	kA	FEINBERG
<g/>
,	,	kIx,	,
Leslie	Leslie	k1gFnSc1	Leslie
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgMnPc1d1	pohlavní
štvanci	štvanec	k1gMnPc1	štvanec
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Spencerová	Spencerová	k1gFnSc1	Spencerová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
G	G	kA	G
plus	plus	k1gInSc1	plus
G	G	kA	G
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
Edice	edice	k1gFnSc2	edice
Zde	zde	k6eAd1	zde
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86103	[number]	k4	86103
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
380	[number]	k4	380
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NĚMEC	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Monika	Monika	k1gFnSc1	Monika
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Míla	Míla	k1gFnSc1	Míla
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Němec	Němec	k1gMnSc1	Němec
<g/>
:	:	kIx,	:
biografie	biografie	k1gFnSc1	biografie
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Cassandra	Cassandra	k1gFnSc1	Cassandra
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900854	[number]	k4	900854
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
SPENCEROVÁ	SPENCEROVÁ	kA	SPENCEROVÁ
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
tranďák	tranďák	k1gInSc4	tranďák
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
G	G	kA	G
plus	plus	k1gInSc1	plus
G	G	kA	G
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
124	[number]	k4	124
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86103	[number]	k4	86103
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgNnSc1d1	další
související	související	k2eAgNnSc1d1	související
====	====	k?	====
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Brychtová	Brychtová	k1gFnSc1	Brychtová
–	–	k?	–
Show	show	k1gFnSc2	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Youtube	Youtub	k1gInSc5	Youtub
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Publikováno	publikován	k2eAgNnSc4d1	Publikováno
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
BRYCHTOVÁ	Brychtová	k1gFnSc1	Brychtová
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
a	a	k8xC	a
WILKOVÁ	WILKOVÁ	kA	WILKOVÁ
<g/>
,	,	kIx,	,
Scarlett	Scarlett	k2eAgInSc1d1	Scarlett
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
,	,	kIx,	,
č.	č.	k?	č.
28	[number]	k4	28
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1168	[number]	k4	1168
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČTK	ČTK	kA	ČTK
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Transsexuální	Transsexuální	k2eAgFnSc1d1	Transsexuální
herečka	herečka	k1gFnSc1	herečka
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
jevišti	jeviště	k1gNnSc6	jeviště
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
svůj	svůj	k3xOyFgInSc4	svůj
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Doktorka	doktorka	k1gFnSc1	doktorka
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Žiji	žít	k5eAaImIp1nS	žít
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
transsexuál	transsexuál	k1gMnSc1	transsexuál
<g/>
.	.	kIx.	.
</s>
<s>
ČT	ČT	kA	ČT
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
ve	v	k7c4	v
21.05	[number]	k4	21.05
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
Michal	Michal	k1gMnSc1	Michal
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Dočasný	dočasný	k2eAgInSc1d1	dočasný
přístup	přístup	k1gInSc1	přístup
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
JONÁŠOVÁ	Jonášová	k1gFnSc1	Jonášová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Dialog	dialog	k1gInSc1	dialog
mužy	muža	k1gFnSc2	muža
s	s	k7c7	s
ženem	ženem	k?	ženem
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
jaro	jaro	k1gNnSc1	jaro
[	[	kIx(	[
<g/>
č.	č.	k?	č.
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213	[number]	k4	1213
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KALIVODOVÁ	Kalivodová	k1gFnSc1	Kalivodová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Brychtová	Brychtová	k1gFnSc1	Brychtová
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Čadek	Čadek	k1gMnSc1	Čadek
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Topvip	Topvip	k1gMnSc1	Topvip
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KOTT	KOTT	kA	KOTT
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
a	a	k8xC	a
ŽIVNÁ	živný	k2eAgFnSc1d1	živná
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ze	z	k7c2	z
mne	já	k3xPp1nSc2	já
po	po	k7c4	po
operaci	operace	k1gFnSc4	operace
konečně	konečně	k6eAd1	konečně
stal	stát	k5eAaPmAgMnS	stát
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
:	:	kIx,	:
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
lékařka	lékařka	k1gFnSc1	lékařka
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Kottová	Kottová	k1gFnSc1	Kottová
se	s	k7c7	s
chirurgickým	chirurgický	k2eAgInSc7d1	chirurgický
zásahem	zásah	k1gInSc7	zásah
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c6	v
Otto	Otto	k1gMnSc1	Otto
Kotta	Kotta	k1gMnSc1	Kotta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
žít	žít	k5eAaImF	žít
spokojený	spokojený	k2eAgMnSc1d1	spokojený
a	a	k8xC	a
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
č.	č.	k?	č.
66	[number]	k4	66
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
5139	[number]	k4	5139
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Show	show	k1gFnSc1	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
Prima	prima	k1gFnSc1	prima
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
ve	v	k7c4	v
21.35	[number]	k4	21.35
<g/>
–	–	k?	–
<g/>
22.40	[number]	k4	22.40
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Hosté	host	k1gMnPc1	host
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Brychtová	Brychtová	k1gFnSc1	Brychtová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Čadek	Čadek	k1gMnSc1	Čadek
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
65	[number]	k4	65
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STULÍROVÁ	STULÍROVÁ	kA	STULÍROVÁ
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
prodělala	prodělat	k5eAaPmAgFnS	prodělat
přeměnu	přeměna	k1gFnSc4	přeměna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
hraje	hrát	k5eAaImIp3nS	hrát
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
deník	deník	k1gInSc1	deník
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
č.	č.	k?	č.
15	[number]	k4	15
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
887	[number]	k4	887
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Romány	román	k1gInPc4	román
====	====	k?	====
</s>
</p>
<p>
<s>
EBERSHOFF	EBERSHOFF	kA	EBERSHOFF
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
dívka	dívka	k1gFnSc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Čapková	Čapková	k1gFnSc1	Čapková
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7498	[number]	k4	7498
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERRY	PERRY	kA	PERRY
<g/>
,	,	kIx,	,
Mike	Mike	k1gNnSc2	Mike
<g/>
.	.	kIx.	.
</s>
<s>
Klec	klec	k1gFnSc1	klec
pro	pro	k7c4	pro
majáky	maják	k1gInPc4	maják
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
335	[number]	k4	335
s.	s.	k?	s.
Edice	edice	k1gFnSc2	edice
Fleet	Fleeta	k1gFnPc2	Fleeta
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87497	[number]	k4	87497
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
transsexualita	transsexualita	k1gFnSc1	transsexualita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
:	:	kIx,	:
Ani	ani	k8xC	ani
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
žena	žena	k1gFnSc1	žena
<g/>
?	?	kIx.	?
</s>
<s>
O	o	k7c6	o
Teiresiově	Teiresiův	k2eAgNnSc6d1	Teiresiův
prokletí	prokletí	k1gNnSc6	prokletí
a	a	k8xC	a
záchodcích	záchodek	k1gInPc6	záchodek
</s>
</p>
<p>
<s>
MUDr.	MUDr.	kA	MUDr.
Hana	Hana	k1gFnSc1	Hana
Fifková	Fifková	k1gFnSc1	Fifková
<g/>
,	,	kIx,	,
sexuoložka	sexuoložka	k1gFnSc1	sexuoložka
a	a	k8xC	a
psychoterapeutka	psychoterapeutka	k1gFnSc1	psychoterapeutka
<g/>
.	.	kIx.	.
</s>
</p>
