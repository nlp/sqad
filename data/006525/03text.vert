<s>
Guy	Guy	k?	Guy
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
[	[	kIx(	[
<g/>
gí	gí	k?	gí
d	d	k?	d
mópasán	mópasán	k2eAgMnSc1d1	mópasán
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
Tourville-sur-Arques	Tourvilleur-Arques	k1gInSc1	Tourville-sur-Arques
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Émilem	Émil	k1gMnSc7	Émil
Zolou	Zola	k1gMnSc7	Zola
a	a	k8xC	a
Gustavem	Gustav	k1gMnSc7	Gustav
Flaubertem	Flaubert	k1gMnSc7	Flaubert
představitel	představitel	k1gMnSc1	představitel
literárního	literární	k2eAgInSc2d1	literární
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc2	naturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
šest	šest	k4xCc1	šest
románů	román	k1gInPc2	román
a	a	k8xC	a
několik	několik	k4yIc4	několik
povídkových	povídkový	k2eAgFnPc2d1	povídková
sbírek	sbírka	k1gFnPc2	sbírka
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
pozornost	pozornost	k1gFnSc4	pozornost
díky	díky	k7c3	díky
síle	síla	k1gFnSc3	síla
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
fantaskním	fantaskní	k2eAgInPc3d1	fantaskní
prvkům	prvek	k1gInPc3	prvek
a	a	k8xC	a
pesimismu	pesimismus	k1gInSc3	pesimismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
díky	díky	k7c3	díky
stylistickému	stylistický	k2eAgNnSc3d1	stylistické
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
literární	literární	k2eAgFnSc1d1	literární
dráha	dráha	k1gFnSc1	dráha
Maupassanta	Maupassanta	k1gFnSc1	Maupassanta
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
vymezena	vymezit	k5eAaPmNgFnS	vymezit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
desetiletím	desetiletí	k1gNnSc7	desetiletí
–	–	k?	–
tvořil	tvořit	k5eAaImAgInS	tvořit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1880	[number]	k4	1880
a	a	k8xC	a
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc2	uznání
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xC	jako
velikán	velikán	k1gMnSc1	velikán
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
a	a	k8xC	a
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Miromesnil	Miromesnil	k1gFnSc2	Miromesnil
v	v	k7c6	v
Seine-Inférieure	Seine-Inférieur	k1gMnSc5	Seine-Inférieur
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
de	de	k?	de
Maupassant	Maupassanta	k1gFnPc2	Maupassanta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
partikule	partikule	k1gFnSc1	partikule
"	"	kIx"	"
<g/>
de	de	k?	de
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
rodu	rod	k1gInSc2	rod
přiřknuta	přiřknout	k5eAaPmNgFnS	přiřknout
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Laure	laur	k1gInSc5	laur
le	le	k?	le
Poittevin	Poittevina	k1gFnPc2	Poittevina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc2d1	situovaná
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Alfrédem	Alfréd	k1gMnSc7	Alfréd
se	se	k3xPyFc4	se
přátelila	přátelit	k5eAaImAgFnS	přátelit
s	s	k7c7	s
Gustavem	Gustav	k1gMnSc7	Gustav
Flaubertem	Flaubert	k1gMnSc7	Flaubert
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
později	pozdě	k6eAd2	pozdě
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
literární	literární	k2eAgFnSc4d1	literární
dráhu	dráha	k1gFnSc4	dráha
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
narodil	narodit	k5eAaPmAgMnS	narodit
ještě	ještě	k9	ještě
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Hervé	Hervá	k1gFnSc2	Hervá
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
pak	pak	k6eAd1	pak
přežili	přežít	k5eAaPmAgMnP	přežít
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
byl	být	k5eAaImAgInS	být
Gustave	Gustav	k1gMnSc5	Gustav
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
Stolz	Stolza	k1gFnPc2	Stolza
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Guy	Guy	k1gMnSc1	Guy
tak	tak	k6eAd1	tak
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
císařské	císařský	k2eAgNnSc4d1	císařské
gymnázium	gymnázium	k1gNnSc4	gymnázium
(	(	kIx(	(
<g/>
Lycée	Lycée	k1gInSc1	Lycée
Henri-IV	Henri-IV	k1gFnSc2	Henri-IV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svobodomyslnost	svobodomyslnost	k1gFnSc1	svobodomyslnost
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
přelétavost	přelétavost	k1gFnSc1	přelétavost
otce	otec	k1gMnSc2	otec
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Laure	laur	k1gInSc5	laur
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
ponechala	ponechat	k5eAaPmAgFnS	ponechat
a	a	k8xC	a
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Étretat	Étretat	k1gFnSc2	Étretat
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
atmosféru	atmosféra	k1gFnSc4	atmosféra
si	se	k3xPyFc3	se
Guy	Guy	k1gMnSc1	Guy
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
okolní	okolní	k2eAgFnSc3d1	okolní
krajině	krajina	k1gFnSc3	krajina
a	a	k8xC	a
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
ovládal	ovládat	k5eAaImAgInS	ovládat
také	také	k9	také
místní	místní	k2eAgInSc1d1	místní
dialekt	dialekt	k1gInSc1	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
fixován	fixován	k2eAgInSc1d1	fixován
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
matky	matka	k1gFnSc2	matka
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
seminář	seminář	k1gInSc4	seminář
do	do	k7c2	do
Yvetotu	Yvetot	k1gInSc2	Yvetot
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tady	tady	k6eAd1	tady
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
verši	verš	k1gInPc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
raného	raný	k2eAgNnSc2d1	rané
studia	studio	k1gNnSc2	studio
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
odnesl	odnést	k5eAaPmAgMnS	odnést
pouze	pouze	k6eAd1	pouze
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
nechal	nechat	k5eAaPmAgMnS	nechat
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
vyhodit	vyhodit	k5eAaPmF	vyhodit
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
lyceum	lyceum	k1gNnSc4	lyceum
v	v	k7c4	v
Rouen	Rouen	k1gInSc4	Rouen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jako	jako	k9	jako
výborný	výborný	k2eAgMnSc1d1	výborný
student	student	k1gMnSc1	student
věnující	věnující	k2eAgFnSc3d1	věnující
se	se	k3xPyFc4	se
poezii	poezie	k1gFnSc3	poezie
a	a	k8xC	a
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
pravidelně	pravidelně	k6eAd1	pravidelně
Louise	Louis	k1gMnSc4	Louis
Bouilheta	Bouilhet	k1gMnSc4	Bouilhet
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
Gustava	Gustav	k1gMnSc2	Gustav
Flauberta	Flaubert	k1gMnSc2	Flaubert
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
mnohému	mnohé	k1gNnSc3	mnohé
naučil	naučit	k5eAaPmAgInS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Maupassantovi	Maupassantův	k2eAgMnPc1d1	Maupassantův
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
tonoucího	tonoucí	k2eAgMnSc4d1	tonoucí
Algernona	Algernon	k1gMnSc4	Algernon
Charlese	Charles	k1gMnSc2	Charles
Swinburne	Swinburn	k1gInSc5	Swinburn
<g/>
,	,	kIx,	,
proslulého	proslulý	k2eAgMnSc2d1	proslulý
anglického	anglický	k2eAgMnSc2d1	anglický
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
kuráž	kuráž	k?	kuráž
pozval	pozvat	k5eAaPmAgInS	pozvat
na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
Maupassant	Maupassant	k1gMnSc1	Maupassant
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
lyceu	lyceum	k1gNnSc6	lyceum
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
Flauberta	Flaubert	k1gMnSc4	Flaubert
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
práva	právo	k1gNnPc4	právo
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc4	všechen
nicméně	nicméně	k8xC	nicméně
přerušila	přerušit	k5eAaPmAgFnS	přerušit
prusko-francouzská	pruskorancouzský	k2eAgFnSc1d1	prusko-francouzská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
účastnil	účastnit	k5eAaImAgInS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
Normandii	Normandie	k1gFnSc4	Normandie
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k8xC	jako
úředník	úředník	k1gMnSc1	úředník
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
námořnictví	námořnictví	k1gNnSc2	námořnictví
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vzdělání	vzdělání	k1gNnSc2	vzdělání
(	(	kIx(	(
<g/>
od	od	k7c2	od
1878	[number]	k4	1878
do	do	k7c2	do
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
povídku	povídka	k1gFnSc4	povídka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Almanach	almanach	k1gInSc1	almanach
lorrain	lorrain	k1gInSc1	lorrain
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1875	[number]	k4	1875
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Joseph	Joseph	k1gMnSc1	Joseph
Prunier	Prunier	k1gMnSc1	Prunier
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
povídka	povídka	k1gFnSc1	povídka
En	En	k1gFnSc2	En
canot	canota	k1gFnPc2	canota
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1876	[number]	k4	1876
<g/>
;	;	kIx,	;
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
ni	on	k3xPp3gFnSc4	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jako	jako	k9	jako
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Valmont	Valmont	k1gMnSc1	Valmont
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1876	[number]	k4	1876
Maupassantovi	Maupassant	k1gMnSc3	Maupassant
přišla	přijít	k5eAaPmAgFnS	přijít
pozvánka	pozvánka	k1gFnSc1	pozvánka
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
svobodným	svobodný	k2eAgMnSc7d1	svobodný
zednářem	zednář	k1gMnSc7	zednář
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
reaguje	reagovat	k5eAaBmIp3nS	reagovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
napojen	napojen	k2eAgInSc4d1	napojen
na	na	k7c4	na
žádnou	žádný	k3yNgFnSc4	žádný
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
je	být	k5eAaImIp3nS	být
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
sektu	sekta	k1gFnSc4	sekta
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
školu	škola	k1gFnSc4	škola
<g/>
;	;	kIx,	;
nikdy	nikdy	k6eAd1	nikdy
nevstoupit	vstoupit	k5eNaPmF	vstoupit
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
asociace	asociace	k1gFnSc1	asociace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
doktrínami	doktrína	k1gFnPc7	doktrína
<g/>
,	,	kIx,	,
nesklonit	sklonit	k5eNaPmF	sklonit
se	se	k3xPyFc4	se
před	před	k7c7	před
žádným	žádný	k3yNgNnSc7	žádný
dogmatem	dogma	k1gNnSc7	dogma
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
právo	právo	k1gNnSc4	právo
říct	říct	k5eAaPmF	říct
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
něco	něco	k3yInSc1	něco
špatného	špatný	k2eAgNnSc2d1	špatné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
1877	[number]	k4	1877
se	se	k3xPyFc4	se
Maupassant	Maupassant	k1gMnSc1	Maupassant
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Turgeněvem	Turgeněv	k1gInSc7	Turgeněv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
shledal	shledat	k5eAaPmAgInS	shledat
velmi	velmi	k6eAd1	velmi
sešlým	sešlý	k2eAgNnSc7d1	sešlé
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
byla	být	k5eAaImAgFnS	být
jasná	jasný	k2eAgFnSc1d1	jasná
–	–	k?	–
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
Maupassanta	Maupassanta	k1gFnSc1	Maupassanta
pomalu	pomalu	k6eAd1	pomalu
zabíjela	zabíjet	k5eAaImAgFnS	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
jedinou	jediný	k2eAgFnSc7d1	jediná
potěchou	potěcha	k1gFnSc7	potěcha
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
provozoval	provozovat	k5eAaImAgMnS	provozovat
na	na	k7c6	na
Seině	Seina	k1gFnSc6	Seina
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
společnosti	společnost	k1gFnSc6	společnost
každou	každý	k3xTgFnSc4	každý
neděli	neděle	k1gFnSc4	neděle
a	a	k8xC	a
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Brával	brávat	k5eAaImAgMnS	brávat
na	na	k7c4	na
vyjížďky	vyjížďka	k1gFnPc4	vyjížďka
na	na	k7c6	na
loďce	loďka	k1gFnSc6	loďka
Feuille	Feuilla	k1gFnSc6	Feuilla
de	de	k?	de
rose	rosa	k1gFnSc6	rosa
mladé	mladý	k2eAgFnSc2d1	mladá
slečny	slečna	k1gFnSc2	slečna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
jeho	jeho	k3xOp3gFnSc7	jeho
kratochvílí	kratochvíle	k1gFnSc7	kratochvíle
byl	být	k5eAaImAgInS	být
lov	lov	k1gInSc1	lov
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
promítlo	promítnout	k5eAaPmAgNnS	promítnout
i	i	k9	i
do	do	k7c2	do
obsahu	obsah	k1gInSc2	obsah
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
Flaubert	Flaubert	k1gMnSc1	Flaubert
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
pod	pod	k7c4	pod
svá	svůj	k3xOyFgNnPc4	svůj
ochranná	ochranný	k2eAgNnPc4d1	ochranné
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
literární	literární	k2eAgFnSc2d1	literární
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Nabádal	nabádat	k5eAaImAgMnS	nabádat
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zanechal	zanechat	k5eAaPmAgInS	zanechat
hloupostí	hloupost	k1gFnSc7	hloupost
<g/>
,	,	kIx,	,
lehkých	lehký	k2eAgFnPc2d1	lehká
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
kanoistiky	kanoistika	k1gFnSc2	kanoistika
a	a	k8xC	a
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
veršům	verš	k1gInPc3	verš
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Flauberta	Flaubert	k1gMnSc2	Flaubert
se	se	k3xPyFc4	se
setkával	setkávat	k5eAaImAgMnS	setkávat
s	s	k7c7	s
vrcholnými	vrcholný	k2eAgMnPc7d1	vrcholný
představiteli	představitel	k1gMnPc7	představitel
literárního	literární	k2eAgInSc2d1	literární
života	život	k1gInSc2	život
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
:	:	kIx,	:
Turgeněvem	Turgeněv	k1gInSc7	Turgeněv
<g/>
,	,	kIx,	,
Zolou	Zola	k1gFnSc7	Zola
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
představiteli	představitel	k1gMnPc7	představitel
naturalismu	naturalismus	k1gInSc2	naturalismus
a	a	k8xC	a
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
několika	několik	k4yIc2	několik
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Le	Le	k1gFnSc1	Le
Figaro	Figara	k1gFnSc5	Figara
<g/>
,	,	kIx,	,
Gil	Gil	k1gMnSc1	Gil
Blas	Blas	k1gInSc1	Blas
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Gaulois	Gaulois	k1gFnSc1	Gaulois
nebo	nebo	k8xC	nebo
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Echo	echo	k1gNnSc1	echo
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
podporován	podporovat	k5eAaImNgMnS	podporovat
Flaubertem	Flaubert	k1gMnSc7	Flaubert
vydal	vydat	k5eAaPmAgInS	vydat
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
stu	sto	k4xCgNnSc3	sto
stránkách	stránka	k1gFnPc6	stránka
Histoire	Histoir	k1gInSc5	Histoir
du	du	k?	du
vieux	vieux	k1gInSc4	vieux
temps	tempsa	k1gFnPc2	tempsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zolou	Zola	k1gMnSc7	Zola
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
kolektivní	kolektivní	k2eAgFnSc6d1	kolektivní
sbírce	sbírka	k1gFnSc6	sbírka
naturalistů	naturalista	k1gMnPc2	naturalista
Les	les	k1gInSc4	les
Soirées	Soiréesa	k1gFnPc2	Soiréesa
de	de	k?	de
Médan	Médana	k1gFnPc2	Médana
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přispěl	přispět	k5eAaPmAgInS	přispět
svou	svůj	k3xOyFgFnSc7	svůj
povídkou	povídka	k1gFnSc7	povídka
Kulička	kulička	k1gFnSc1	kulička
(	(	kIx(	(
<g/>
Boule	boule	k1gFnSc1	boule
de	de	k?	de
suif	suif	k1gInSc1	suif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
u	u	k7c2	u
publika	publikum	k1gNnSc2	publikum
i	i	k8xC	i
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
náhlé	náhlý	k2eAgFnSc6d1	náhlá
smrti	smrt	k1gFnSc6	smrt
Flauberta	Flaubert	k1gMnSc2	Flaubert
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vymezené	vymezený	k2eAgFnSc2d1	vymezená
lety	léto	k1gNnPc7	léto
1880	[number]	k4	1880
a	a	k8xC	a
1890	[number]	k4	1890
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc7	jeho
nejplodnějším	plodný	k2eAgNnSc7d3	nejplodnější
obdobím	období	k1gNnSc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
šest	šest	k4xCc4	šest
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
několik	několik	k4yIc4	několik
cestopisů	cestopis	k1gInPc2	cestopis
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
metodicky	metodicky	k6eAd1	metodicky
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
vydává	vydávat	k5eAaImIp3nS	vydávat
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
čtyři	čtyři	k4xCgInPc4	čtyři
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
dobrý	dobrý	k2eAgInSc4d1	dobrý
obchod	obchod	k1gInSc4	obchod
mu	on	k3xPp3gMnSc3	on
přinášel	přinášet	k5eAaImAgMnS	přinášet
bohatství	bohatství	k1gNnPc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1881	[number]	k4	1881
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
sbírku	sbírka	k1gFnSc4	sbírka
povídek	povídka	k1gFnPc2	povídka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
La	la	k1gNnSc2	la
Maison	Maison	k1gNnSc1	Maison
Tellier	Tellier	k1gInSc1	Tellier
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
dočkala	dočkat	k5eAaPmAgFnS	dočkat
šesti	šest	k4xCc2	šest
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1881	[number]	k4	1881
opustil	opustit	k5eAaPmAgMnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
jako	jako	k8xS	jako
redaktor	redaktor	k1gMnSc1	redaktor
časopisu	časopis	k1gInSc2	časopis
Le	Le	k1gFnSc2	Le
Gaulois	Gaulois	k1gFnPc2	Gaulois
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
milence	milenec	k1gMnSc4	milenec
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odjel	odjet	k5eAaPmAgMnS	odjet
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
Saharu	Sahara	k1gFnSc4	Sahara
<g/>
!!!	!!!	k?	!!!
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Nemějte	mít	k5eNaImRp2nP	mít
mi	já	k3xPp1nSc3	já
za	za	k7c2	za
zlé	zlá	k1gFnSc2	zlá
<g/>
,	,	kIx,	,
drahá	drahý	k2eAgFnSc1d1	drahá
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
učinil	učinit	k5eAaPmAgMnS	učinit
tak	tak	k9	tak
rychlé	rychlý	k2eAgNnSc4d1	rychlé
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
tulák	tulák	k1gMnSc1	tulák
a	a	k8xC	a
nemám	mít	k5eNaImIp1nS	mít
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Řekněte	říct	k5eAaPmRp2nP	říct
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mám	mít	k5eAaImIp1nS	mít
adresovat	adresovat	k5eAaBmF	adresovat
své	svůj	k3xOyFgInPc4	svůj
dopisy	dopis	k1gInPc4	dopis
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
své	svůj	k3xOyFgFnPc4	svůj
posílejte	posílat	k5eAaImRp2nP	posílat
do	do	k7c2	do
Alžíru	Alžír	k1gInSc2	Alžír
poste	post	k1gInSc5	post
restante	restant	k1gInSc5	restant
<g/>
.	.	kIx.	.
</s>
<s>
Líbám	líbat	k5eAaImIp1nS	líbat
Vás	vy	k3xPp2nPc4	vy
všude	všude	k6eAd1	všude
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
Korsice	Korsika	k1gFnSc6	Korsika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
dokončil	dokončit	k5eAaPmAgInS	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
již	již	k6eAd1	již
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
jednoho	jeden	k4xCgInSc2	jeden
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Une	Une	k1gFnSc1	Une
vie	vie	k?	vie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
rozprodáno	rozprodat	k5eAaPmNgNnS	rozprodat
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc4	dílo
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
ocenil	ocenit	k5eAaPmAgMnS	ocenit
i	i	k9	i
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
počin	počin	k1gInSc1	počin
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
hned	hned	k6eAd1	hned
po	po	k7c6	po
Bídnících	bídník	k1gMnPc6	bídník
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
mu	on	k3xPp3gMnSc3	on
první	první	k4xOgFnSc2	první
publikace	publikace	k1gFnSc2	publikace
vynesly	vynést	k5eAaPmAgInP	vynést
nějaké	nějaký	k3yIgInPc1	nějaký
příjmy	příjem	k1gInPc1	příjem
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
si	se	k3xPyFc3	se
Maupassant	Maupassant	k1gMnSc1	Maupassant
v	v	k7c6	v
Étretat	Étretat	k1gFnSc6	Étretat
dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
La	la	k0	la
Guillette	Guillett	k1gMnSc5	Guillett
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Guyův	Guyův	k2eAgInSc1d1	Guyův
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
obýval	obývat	k5eAaImAgMnS	obývat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1883	[number]	k4	1883
mu	on	k3xPp3gMnSc3	on
modistka	modistka	k1gFnSc1	modistka
Joséphine	Joséphin	k1gInSc5	Joséphin
Litzelmann	Litzelmanna	k1gFnPc2	Litzelmanna
porodila	porodit	k5eAaPmAgFnS	porodit
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
–	–	k?	–
Luciena	Lucieno	k1gNnSc2	Lucieno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
navázal	navázat	k5eAaPmAgInS	navázat
Maupassant	Maupassant	k1gInSc1	Maupassant
milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
kněžnou	kněžna	k1gFnSc7	kněžna
Emmanuelou	Emmanuela	k1gFnSc7	Emmanuela
Potockou	Potocka	k1gFnSc7	Potocka
<g/>
,	,	kIx,	,
krásnou	krásný	k2eAgFnSc7d1	krásná
a	a	k8xC	a
spirituální	spirituální	k2eAgFnSc7d1	spirituální
bohatou	bohatý	k2eAgFnSc7d1	bohatá
ženou	žena	k1gFnSc7	žena
s	s	k7c7	s
italsko-polskými	italskoolský	k2eAgInPc7d1	italsko-polský
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
dokončil	dokončit	k5eAaPmAgInS	dokončit
psaní	psaní	k1gNnSc4	psaní
Miláčka	miláček	k1gMnSc2	miláček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
románech	román	k1gInPc6	román
Guy	Guy	k1gFnSc2	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
koncentroval	koncentrovat	k5eAaBmAgMnS	koncentrovat
všechna	všechen	k3xTgNnPc4	všechen
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
rozeseta	rozeset	k2eAgNnPc1d1	rozeseto
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Miláček	miláček	k1gMnSc1	miláček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
už	už	k6eAd1	už
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
měsíců	měsíc	k1gInPc2	měsíc
několikrát	několikrát	k6eAd1	několikrát
dotiskován	dotiskován	k2eAgMnSc1d1	dotiskován
<g/>
.	.	kIx.	.
</s>
<s>
Maupassant	Maupassant	k1gInSc1	Maupassant
se	s	k7c7	s
smíchem	smích	k1gInSc7	smích
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Miláček	miláček	k1gMnSc1	miláček
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
věci	věc	k1gFnPc4	věc
kolem	kolem	k7c2	kolem
publikace	publikace	k1gFnSc2	publikace
Miláčka	miláček	k1gMnSc2	miláček
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Maupassantova	Maupassantův	k2eAgFnSc1d1	Maupassantova
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
daná	daný	k2eAgFnSc1d1	daná
averze	averze	k1gFnSc1	averze
vůči	vůči	k7c3	vůči
společnosti	společnost	k1gFnSc3	společnost
–	–	k?	–
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
podlomené	podlomený	k2eAgNnSc1d1	podlomené
zdraví	zdraví	k1gNnSc1	zdraví
–	–	k?	–
ho	on	k3xPp3gMnSc4	on
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
předčasného	předčasný	k2eAgInSc2d1	předčasný
důchodu	důchod	k1gInSc2	důchod
<g/>
,	,	kIx,	,
k	k	k7c3	k
samotě	samota	k1gFnSc3	samota
a	a	k8xC	a
meditaci	meditace	k1gFnSc3	meditace
<g/>
.	.	kIx.	.
</s>
<s>
Odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Bretaň	Bretaň	k1gFnSc4	Bretaň
<g/>
,	,	kIx,	,
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
Auvergne	Auvergn	k1gInSc5	Auvergn
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
nové	nový	k2eAgFnPc4d1	nová
reportáže	reportáž	k1gFnPc4	reportáž
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
a	a	k8xC	a
cestovní	cestovní	k2eAgInPc4d1	cestovní
deníky	deník	k1gInPc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
jachtě	jachta	k1gFnSc6	jachta
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
"	"	kIx"	"
<g/>
Bel-Ami	Bel-A	k1gFnPc7	Bel-A
<g/>
"	"	kIx"	"
–	–	k?	–
Miláček	miláček	k1gMnSc1	miláček
podnikl	podniknout	k5eAaPmAgMnS	podniknout
okružní	okružní	k2eAgFnSc4d1	okružní
jízdu	jízda	k1gFnSc4	jízda
Středomořím	středomoří	k1gNnSc7	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
prostoru	prostor	k1gInSc3	prostor
<g/>
,	,	kIx,	,
cestám	cesta	k1gFnPc3	cesta
a	a	k8xC	a
samotě	samota	k1gFnSc3	samota
mu	on	k3xPp3gNnSc3	on
nezabraňovala	zabraňovat	k5eNaImAgFnS	zabraňovat
navazovat	navazovat	k5eAaImF	navazovat
nová	nový	k2eAgNnPc4d1	nové
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Dumasem	Dumas	k1gMnSc7	Dumas
ml.	ml.	kA	ml.
nebo	nebo	k8xC	nebo
historikem	historik	k1gMnSc7	historik
a	a	k8xC	a
filozofem	filozof	k1gMnSc7	filozof
Hippolytem	Hippolyt	k1gInSc7	Hippolyt
Tainem	Taino	k1gNnSc7	Taino
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
spatřil	spatřit	k5eAaPmAgMnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
román	román	k1gInSc4	román
Mont-Oriol	Mont-Oriola	k1gFnPc2	Mont-Oriola
z	z	k7c2	z
lékařského	lékařský	k2eAgNnSc2d1	lékařské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
Maupassant	Maupassant	k1gMnSc1	Maupassant
věnoval	věnovat	k5eAaImAgMnS	věnovat
novému	nový	k2eAgNnSc3d1	nové
vědnímu	vědní	k2eAgNnSc3d1	vědní
odvětví	odvětví	k1gNnSc3	odvětví
–	–	k?	–
psychologii	psychologie	k1gFnSc6	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Dotýkal	dotýkat	k5eAaImAgInS	dotýkat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
otázky	otázka	k1gFnPc1	otázka
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
v	v	k7c6	v
salonech	salon	k1gInPc6	salon
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
postavy	postava	k1gFnSc2	postava
Williama	William	k1gMnSc2	William
Andermatta	Andermatt	k1gMnSc2	Andermatt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
petici	petice	k1gFnSc4	petice
proti	proti	k7c3	proti
vztyčení	vztyčení	k1gNnSc3	vztyčení
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
syfilidy	syfilida	k1gFnSc2	syfilida
v	v	k7c4	v
její	její	k3xOp3gFnSc4	její
neuroluetickou	uroluetický	k2eNgFnSc4d1	uroluetický
formu	forma	k1gFnSc4	forma
vedl	vést	k5eAaImAgMnS	vést
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
k	k	k7c3	k
Maupassantovým	Maupassantův	k2eAgFnPc3d1	Maupassantova
četným	četný	k2eAgFnPc3d1	četná
migrenózním	migrenózní	k2eAgFnPc3d1	migrenózní
bolestem	bolest	k1gFnPc3	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
přechodným	přechodný	k2eAgFnPc3d1	přechodná
poruchám	porucha	k1gFnPc3	porucha
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
si	se	k3xPyFc3	se
povšimli	povšimnout	k5eAaPmAgMnP	povšimnout
jeho	jeho	k3xOp3gNnSc2	jeho
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
nápadné	nápadný	k2eAgFnSc2d1	nápadná
posedlosti	posedlost	k1gFnSc2	posedlost
sebezáchovou	sebezáchova	k1gFnSc7	sebezáchova
<g/>
,	,	kIx,	,
samotou	samota	k1gFnSc7	samota
a	a	k8xC	a
šílenstvím	šílenství	k1gNnSc7	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
své	svůj	k3xOyFgFnPc4	svůj
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc4	bolest
i	i	k8xC	i
strach	strach	k1gInSc4	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Sužovaly	sužovat	k5eAaImAgFnP	sužovat
jej	on	k3xPp3gInSc4	on
halucinace	halucinace	k1gFnPc1	halucinace
a	a	k8xC	a
také	také	k9	také
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	on	k3xPp3gInSc4	on
mozek	mozek	k1gInSc4	mozek
požírají	požírat	k5eAaImIp3nP	požírat
mouchy	moucha	k1gFnPc4	moucha
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
soustavně	soustavně	k6eAd1	soustavně
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
neznámým	známý	k2eNgNnSc7d1	neznámé
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
se	se	k3xPyFc4	se
Maupassant	Maupassant	k1gMnSc1	Maupassant
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
mu	on	k3xPp3gMnSc3	on
zabránil	zabránit	k5eAaPmAgMnS	zabránit
jeho	jeho	k3xOp3gMnSc1	jeho
sluha	sluha	k1gMnSc1	sluha
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
choromyslné	choromyslný	k2eAgNnSc4d1	choromyslné
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Esprita	Esprita	k1gFnSc1	Esprita
Blanche	Blanch	k1gFnSc2	Blanch
v	v	k7c4	v
Passy	Passa	k1gFnPc4	Passa
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1893	[number]	k4	1893
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
<g/>
.	.	kIx.	.
</s>
<s>
Maupassant	Maupassant	k1gMnSc1	Maupassant
definoval	definovat	k5eAaBmAgMnS	definovat
své	svůj	k3xOyFgFnPc4	svůj
koncepce	koncepce	k1gFnPc4	koncepce
popisu	popis	k1gInSc2	popis
a	a	k8xC	a
vyprávění	vyprávění	k1gNnSc2	vyprávění
v	v	k7c6	v
Předmluvě	předmluva	k1gFnSc6	předmluva
k	k	k7c3	k
románu	román	k1gInSc3	román
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
romanopisec	romanopisec	k1gMnSc1	romanopisec
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
dokáže	dokázat	k5eAaPmIp3nS	dokázat
všechno	všechen	k3xTgNnSc1	všechen
převést	převést	k5eAaPmF	převést
v	v	k7c4	v
dílo	dílo	k1gNnSc4	dílo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reflektoval	reflektovat	k5eAaImAgInS	reflektovat
city	city	k1gFnSc7	city
prosté	prostý	k2eAgFnSc2d1	prostá
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukázal	ukázat	k5eAaPmAgInS	ukázat
současného	současný	k2eAgMnSc4d1	současný
člověka	člověk	k1gMnSc4	člověk
takového	takový	k3xDgNnSc2	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgInS	odmítat
romantický	romantický	k2eAgInSc1d1	romantický
román	román	k1gInSc1	román
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
deformovanou	deformovaný	k2eAgFnSc7d1	deformovaná
<g/>
,	,	kIx,	,
nadlidskou	nadlidský	k2eAgFnSc7d1	nadlidská
a	a	k8xC	a
poetickou	poetický	k2eAgFnSc7d1	poetická
vizí	vize	k1gFnSc7	vize
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
román	román	k1gInSc1	román
symbolistický	symbolistický	k2eAgInSc1d1	symbolistický
s	s	k7c7	s
přemírou	přemíra	k1gFnSc7	přemíra
psychologismů	psychologismus	k1gInPc2	psychologismus
a	a	k8xC	a
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
najít	najít	k5eAaPmF	najít
román	román	k1gInSc4	román
pravdivý	pravdivý	k2eAgInSc4d1	pravdivý
<g/>
,	,	kIx,	,
realistický	realistický	k2eAgInSc4d1	realistický
<g/>
,	,	kIx,	,
objektivní	objektivní	k2eAgInSc4d1	objektivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgInSc4d1	vědom
jistých	jistý	k2eAgInPc2d1	jistý
limitů	limit	k1gInPc2	limit
takových	takový	k3xDgInPc2	takový
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
realismus	realismus	k1gInSc1	realismus
"	"	kIx"	"
<g/>
osobní	osobní	k2eAgInSc1d1	osobní
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
)	)	kIx)	)
hledá	hledat	k5eAaImIp3nS	hledat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
nám	my	k3xPp1nPc3	my
ji	on	k3xPp3gFnSc4	on
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
předělá	předělat	k5eAaPmIp3nS	předělat
v	v	k7c4	v
knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
osobnosti	osobnost	k1gFnPc4	osobnost
autora	autor	k1gMnSc2	autor
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
výběru	výběr	k1gInSc6	výběr
z	z	k7c2	z
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
tak	tak	k9	tak
rovněž	rovněž	k9	rovněž
naturalismus	naturalismus	k1gInSc4	naturalismus
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
těžké	těžký	k2eAgFnSc3d1	těžká
dokumentárnosti	dokumentárnost	k1gFnSc3	dokumentárnost
a	a	k8xC	a
demonstrativní	demonstrativní	k2eAgFnSc3d1	demonstrativní
ambici	ambice	k1gFnSc3	ambice
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
totálního	totální	k2eAgInSc2d1	totální
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Zoly	Zola	k1gFnSc2	Zola
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychologie	psychologie	k1gFnSc1	psychologie
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
pod	pod	k7c7	pod
nánosem	nános	k1gInSc7	nános
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Témata	téma	k1gNnPc1	téma
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
každodenním	každodenní	k2eAgInSc7d1	každodenní
životem	život	k1gInSc7	život
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
se	s	k7c7	s
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
sám	sám	k3xTgMnSc1	sám
zažil	zažít	k5eAaPmAgMnS	zažít
<g/>
.	.	kIx.	.
</s>
<s>
Normandie	Normandie	k1gFnSc1	Normandie
-	-	kIx~	-
rodný	rodný	k2eAgInSc1d1	rodný
kraj	kraj	k1gInSc1	kraj
Maupassanta	Maupassant	k1gMnSc2	Maupassant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
jeho	on	k3xPp3gMnSc2	on
obyvatele	obyvatel	k1gMnSc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
1870	[number]	k4	1870
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
Žena	žena	k1gFnSc1	žena
jako	jako	k8xS	jako
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
přesahem	přesah	k1gInSc7	přesah
do	do	k7c2	do
tématu	téma	k1gNnSc2	téma
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
otcovství	otcovství	k1gNnSc2	otcovství
Pesimismus	pesimismus	k1gInSc4	pesimismus
Šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
a	a	k8xC	a
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
;	;	kIx,	;
smrt	smrt	k1gFnSc1	smrt
jako	jako	k8xC	jako
destrukce	destrukce	k1gFnSc1	destrukce
Realismus	realismus	k1gInSc1	realismus
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
ve	v	k7c6	v
výběru	výběr	k1gInSc6	výběr
detailů	detail	k1gInPc2	detail
a	a	k8xC	a
popisu	popis	k1gInSc2	popis
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
v	v	k7c6	v
odrazu	odraz	k1gInSc6	odraz
pitoreskního	pitoreskní	k2eAgInSc2d1	pitoreskní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
v	v	k7c6	v
Maupassantově	Maupassantův	k2eAgInSc6d1	Maupassantův
díle	díl	k1gInSc6	díl
však	však	k9	však
najít	najít	k5eAaPmF	najít
i	i	k9	i
prvky	prvek	k1gInPc4	prvek
fantaskní	fantaskní	k2eAgInPc4d1	fantaskní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jde	jít	k5eAaImIp3nS	jít
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
šílenstvím	šílenství	k1gNnSc7	šílenství
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
projevy	projev	k1gInPc7	projev
(	(	kIx(	(
<g/>
Horla	Horla	k1gFnSc1	Horla
<g/>
,	,	kIx,	,
Hrobka	hrobka	k1gFnSc1	hrobka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dramatičnost	dramatičnost	k1gFnSc1	dramatičnost
s	s	k7c7	s
sebou	se	k3xPyFc7	se
pak	pak	k6eAd1	pak
přináší	přinášet	k5eAaImIp3nP	přinášet
přítomnost	přítomnost	k1gFnSc4	přítomnost
hrozby	hrozba	k1gFnSc2	hrozba
(	(	kIx(	(
<g/>
úzkost	úzkost	k1gFnSc1	úzkost
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
Miláčkovi	miláček	k1gMnSc6	miláček
<g/>
,	,	kIx,	,
šílenství	šílenství	k1gNnSc3	šílenství
v	v	k7c6	v
Horle	horlit	k5eAaImSgInS	horlit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zmizení	zmizení	k1gNnSc1	zmizení
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pesimistický	pesimistický	k2eAgInSc1d1	pesimistický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
často	často	k6eAd1	často
černě	černě	k6eAd1	černě
viděné	viděný	k2eAgInPc4d1	viděný
sociální	sociální	k2eAgInPc4d1	sociální
a	a	k8xC	a
osobní	osobní	k2eAgInPc4d1	osobní
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
tragickém	tragický	k2eAgInSc6d1	tragický
registru	registr	k1gInSc6	registr
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
komično	komično	k1gNnSc1	komično
zde	zde	k6eAd1	zde
také	také	k9	také
nechybí	chybit	k5eNaPmIp3nS	chybit
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
gest	gesto	k1gNnPc2	gesto
<g/>
,	,	kIx,	,
karikatuře	karikatura	k1gFnSc3	karikatura
charakterů	charakter	k1gInPc2	charakter
<g/>
...	...	k?	...
Lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
prvky	prvek	k1gInPc4	prvek
výsměchu	výsměch	k1gInSc2	výsměch
morálce	morálka	k1gFnSc3	morálka
a	a	k8xC	a
zkažené	zkažený	k2eAgFnSc3d1	zkažená
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
například	například	k6eAd1	například
přehlíží	přehlížet	k5eAaImIp3nS	přehlížet
nevěru	nevěra	k1gFnSc4	nevěra
<g/>
.	.	kIx.	.
</s>
<s>
Maupassantovo	Maupassantův	k2eAgNnSc1d1	Maupassantův
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rovnovážně	rovnovážně	k6eAd1	rovnovážně
rozložené	rozložený	k2eAgNnSc1d1	rozložené
mezi	mezi	k7c4	mezi
vyprávění	vyprávění	k1gNnSc4	vyprávění
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
omezený	omezený	k2eAgInSc4d1	omezený
a	a	k8xC	a
funkční	funkční	k2eAgInSc4d1	funkční
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
a	a	k8xC	a
hru	hra	k1gFnSc4	hra
mezi	mezi	k7c7	mezi
přímou	přímý	k2eAgFnSc7d1	přímá
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
nepřímou	přímý	k2eNgFnSc7d1	nepřímá
řečí	řeč	k1gFnSc7	řeč
a	a	k8xC	a
volným	volný	k2eAgNnSc7d1	volné
vyprávěním	vyprávění	k1gNnSc7	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
typické	typický	k2eAgNnSc4d1	typické
užití	užití	k1gNnSc4	užití
spíše	spíše	k9	spíše
krátkých	krátký	k2eAgFnPc2d1	krátká
vět	věta	k1gFnPc2	věta
s	s	k7c7	s
expresivní	expresivní	k2eAgFnSc7d1	expresivní
interpunkcí	interpunkce	k1gFnSc7	interpunkce
<g/>
.	.	kIx.	.
</s>
<s>
Odstavce	odstavec	k1gInPc1	odstavec
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
kratšího	krátký	k2eAgInSc2d2	kratší
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
spisovný	spisovný	k2eAgMnSc1d1	spisovný
ve	v	k7c4	v
vyprávění	vyprávění	k1gNnPc4	vyprávění
a	a	k8xC	a
dynamický	dynamický	k2eAgInSc4d1	dynamický
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
zkomolený	zkomolený	k2eAgInSc4d1	zkomolený
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
jazyka	jazyk	k1gInSc2	jazyk
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
.	.	kIx.	.
</s>
<s>
Guy	Guy	k?	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
šesti	šest	k4xCc2	šest
románů	román	k1gInPc2	román
a	a	k8xC	a
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
povídky	povídka	k1gFnPc1	povídka
přinesly	přinést	k5eAaPmAgFnP	přinést
Maupassantovi	Maupassant	k1gMnSc3	Maupassant
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
jak	jak	k8xC	jak
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
náměty	námět	k1gInPc4	námět
z	z	k7c2	z
normandského	normandský	k2eAgInSc2d1	normandský
venkova	venkov	k1gInSc2	venkov
či	či	k8xC	či
městského	městský	k2eAgNnSc2d1	Městské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
především	především	k9	především
pro	pro	k7c4	pro
autorův	autorův	k2eAgInSc4d1	autorův
vypravěčský	vypravěčský	k2eAgInSc4d1	vypravěčský
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
románovou	románový	k2eAgFnSc4d1	románová
tvorbu	tvorba	k1gFnSc4	tvorba
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zaměření	zaměření	k1gNnSc1	zaměření
na	na	k7c4	na
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
a	a	k8xC	a
aristokratickou	aristokratický	k2eAgFnSc4d1	aristokratická
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílech	díl	k1gInPc6	díl
Silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
Naše	náš	k3xOp1gNnSc4	náš
srdce	srdce	k1gNnSc4	srdce
jsou	být	k5eAaImIp3nP	být
tentokrát	tentokrát	k6eAd1	tentokrát
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
umělcem	umělec	k1gMnSc7	umělec
a	a	k8xC	a
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
vysokých	vysoký	k2eAgInPc2d1	vysoký
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
významné	významný	k2eAgInPc4d1	významný
okamžiky	okamžik	k1gInPc4	okamžik
autorova	autorův	k2eAgInSc2d1	autorův
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
vtiskují	vtiskovat	k5eAaImIp3nP	vtiskovat
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novele	novela	k1gFnSc6	novela
Dědictví	dědictví	k1gNnPc2	dědictví
vstupujeme	vstupovat	k5eAaImIp1nP	vstupovat
do	do	k7c2	do
úřednického	úřednický	k2eAgNnSc2d1	úřednické
prostředí	prostředí	k1gNnSc2	prostředí
samotného	samotný	k2eAgNnSc2d1	samotné
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Miláčkovi	miláček	k1gMnSc6	miláček
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
stylizoval	stylizovat	k5eAaImAgMnS	stylizovat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
Georgese	Georgese	k1gFnSc2	Georgese
Duroye	Duroy	k1gFnSc2	Duroy
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
časté	častý	k2eAgInPc4d1	častý
pobyty	pobyt	k1gInPc4	pobyt
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
mohl	moct	k5eAaImAgMnS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
v	v	k7c6	v
románu	román	k1gInSc6	román
Mont-Oriol	Mont-Oriol	k1gInSc1	Mont-Oriol
<g/>
.	.	kIx.	.
</s>
<s>
Kulička	kulička	k1gFnSc1	kulička
(	(	kIx(	(
<g/>
Boule	boule	k1gFnSc1	boule
de	de	k?	de
suif	suif	k1gInSc1	suif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
–	–	k?	–
asi	asi	k9	asi
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
Maupassantova	Maupassantův	k2eAgFnSc1d1	Maupassantova
povídka	povídka	k1gFnSc1	povídka
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
prostitutka	prostitutka	k1gFnSc1	prostitutka
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
Rousset	Rousset	k1gMnSc1	Rousset
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Kulička	kulička	k1gFnSc1	kulička
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zachrání	zachránit	k5eAaPmIp3nP	zachránit
občany	občan	k1gMnPc4	občan
městečka	městečko	k1gNnSc2	městečko
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
služby	služba	k1gFnPc4	služba
pruskému	pruský	k2eAgMnSc3d1	pruský
důstojníkovi	důstojník	k1gMnSc3	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
"	"	kIx"	"
<g/>
hrdinský	hrdinský	k2eAgInSc4d1	hrdinský
čin	čin	k1gInSc4	čin
<g/>
"	"	kIx"	"
jí	jíst	k5eAaImIp3nS	jíst
ostatní	ostatní	k2eAgInPc1d1	ostatní
neustále	neustále	k6eAd1	neustále
pohrdají	pohrdat	k5eAaImIp3nP	pohrdat
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
rozpor	rozpor	k1gInSc1	rozpor
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
mravnější	mravní	k2eAgMnSc1d2	mravnější
a	a	k8xC	a
větší	veliký	k2eAgMnSc1d2	veliký
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
:	:	kIx,	:
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obětovala	obětovat	k5eAaBmAgFnS	obětovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
slušní	slušný	k2eAgMnPc1d1	slušný
<g/>
"	"	kIx"	"
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vše	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
přežít	přežít	k5eAaPmF	přežít
<g/>
?	?	kIx.	?
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Slečna	slečna	k1gFnSc1	slečna
Fifi	Fif	k1gFnSc2	Fif
(	(	kIx(	(
<g/>
Mademoiselle	Mademoiselle	k1gInSc1	Mademoiselle
Fifi	Fif	k1gFnSc2	Fif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
–	–	k?	–
opět	opět	k6eAd1	opět
z	z	k7c2	z
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
barbarské	barbarský	k2eAgNnSc1d1	barbarské
chování	chování	k1gNnSc1	chování
pruských	pruský	k2eAgMnPc2d1	pruský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
jednoho	jeden	k4xCgInSc2	jeden
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
Miláček	miláček	k1gMnSc1	miláček
(	(	kIx(	(
<g/>
Bel-Ami	Bel-A	k1gFnPc7	Bel-A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
–	–	k?	–
společensko-kritický	společenskoritický	k2eAgInSc4d1	společensko-kritický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
mladý	mladý	k2eAgMnSc1d1	mladý
novinář	novinář	k1gMnSc1	novinář
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
své	svůj	k3xOyFgFnSc2	svůj
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
buduje	budovat	k5eAaImIp3nS	budovat
kariéru	kariéra	k1gFnSc4	kariéra
využíváním	využívání	k1gNnSc7	využívání
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
cílevědomý	cílevědomý	k2eAgMnSc1d1	cílevědomý
mladík	mladík	k1gMnSc1	mladík
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
do	do	k7c2	do
pařížských	pařížský	k2eAgInPc2d1	pařížský
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Mont-Oriol	Mont-Oriol	k1gInSc1	Mont-Oriol
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
-	-	kIx~	-
román	román	k1gInSc1	román
z	z	k7c2	z
lázeňského	lázeňský	k2eAgNnSc2d1	lázeňské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Christiane	Christian	k1gMnSc5	Christian
Andermattová	Andermattová	k1gFnSc1	Andermattová
se	se	k3xPyFc4	se
přijede	přijet	k5eAaPmIp3nS	přijet
léčit	léčit	k5eAaImF	léčit
do	do	k7c2	do
lázeňského	lázeňský	k2eAgNnSc2d1	lázeňské
města	město	k1gNnSc2	město
kvůli	kvůli	k7c3	kvůli
domnělé	domnělý	k2eAgFnSc3d1	domnělá
neplodnosti	neplodnost	k1gFnSc3	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
Paula	Paul	k1gMnSc2	Paul
Brétignyho	Brétigny	k1gMnSc2	Brétigny
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgNnSc7	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
holčičku	holčička	k1gFnSc4	holčička
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
William	William	k1gInSc1	William
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
poměrně	poměrně	k6eAd1	poměrně
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
rozehrává	rozehrávat	k5eAaImIp3nS	rozehrávat
spekulace	spekulace	k1gFnPc4	spekulace
ohledně	ohledně	k7c2	ohledně
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
výstavbě	výstavba	k1gFnSc3	výstavba
dalšího	další	k2eAgInSc2d1	další
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
kasina	kasino	k1gNnSc2	kasino
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Christiane	Christian	k1gMnSc5	Christian
Gontran	Gontran	k1gInSc4	Gontran
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
v	v	k7c6	v
dluzích	dluh	k1gInPc6	dluh
a	a	k8xC	a
využívající	využívající	k2eAgFnPc4d1	využívající
svého	svůj	k3xOyFgMnSc4	svůj
švagra	švagr	k1gMnSc2	švagr
<g/>
,	,	kIx,	,
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Oriola	Oriola	k1gFnSc1	Oriola
<g/>
,	,	kIx,	,
Louisou	Louisý	k2eAgFnSc4d1	Louisý
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
dvořil	dvořit	k5eAaImAgMnS	dvořit
její	její	k3xOp3gFnSc3	její
sestře	sestra	k1gFnSc3	sestra
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
tak	tak	k6eAd1	tak
výhodné	výhodný	k2eAgNnSc1d1	výhodné
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
spadalo	spadat	k5eAaImAgNnS	spadat
do	do	k7c2	do
Williamových	Williamový	k2eAgMnPc2d1	Williamový
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěnou	opuštěný	k2eAgFnSc4d1	opuštěná
Charlotte	Charlott	k1gInSc5	Charlott
si	se	k3xPyFc3	se
tak	tak	k9	tak
vyhlédne	vyhlédnout	k5eAaPmIp3nS	vyhlédnout
Paul	Paul	k1gMnSc1	Paul
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
ožení	oženit	k5eAaPmIp3nP	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Christiane	Christian	k1gMnSc5	Christian
<g/>
,	,	kIx,	,
zotavující	zotavující	k2eAgFnSc2d1	zotavující
se	se	k3xPyFc4	se
z	z	k7c2	z
porodu	porod	k1gInSc2	porod
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
zavolat	zavolat	k5eAaPmF	zavolat
Paula	Paula	k1gFnSc1	Paula
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
jej	on	k3xPp3gMnSc4	on
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
rozpláče	rozplakat	k5eAaPmIp3nS	rozplakat
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
satirou	satira	k1gFnSc7	satira
na	na	k7c4	na
lázeňské	lázeňský	k2eAgNnSc4d1	lázeňské
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnPc4d1	finanční
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zázračné	zázračný	k2eAgInPc4d1	zázračný
<g/>
"	"	kIx"	"
produkty	produkt	k1gInPc4	produkt
a	a	k8xC	a
reklamu	reklama	k1gFnSc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
-	-	kIx~	-
naturalistický	naturalistický	k2eAgInSc4d1	naturalistický
či	či	k8xC	či
realisticko-psychologický	realistickosychologický	k2eAgInSc4d1	realisticko-psychologický
krátký	krátký	k2eAgInSc4d1	krátký
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
hlavně	hlavně	k9	hlavně
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
Maupassant	Maupassant	k1gMnSc1	Maupassant
popisuje	popisovat	k5eAaImIp3nS	popisovat
svou	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
románu	román	k1gInSc2	román
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
le	le	k?	le
Havru	Havr	k1gInSc6	Havr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
rodina	rodina	k1gFnSc1	rodina
Rolandů	Roland	k1gInPc2	Roland
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
-	-	kIx~	-
studovali	studovat	k5eAaImAgMnP	studovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
staršího	starý	k2eAgMnSc2d2	starší
Petra	Petr	k1gMnSc2	Petr
je	být	k5eAaImIp3nS	být
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
z	z	k7c2	z
mladšího	mladý	k2eAgMnSc2d2	mladší
Jana	Jan	k1gMnSc2	Jan
právník	právník	k1gMnSc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tomu	ten	k3xDgNnSc3	ten
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
klína	klín	k1gInSc2	klín
nečekané	čekaný	k2eNgNnSc4d1	nečekané
dědictví	dědictví	k1gNnSc4	dědictví
po	po	k7c6	po
rodinném	rodinný	k2eAgMnSc6d1	rodinný
příteli	přítel	k1gMnSc6	přítel
Léonu	Léon	k1gMnSc6	Léon
Maréchalovi	Maréchal	k1gMnSc6	Maréchal
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
suma	suma	k1gFnSc1	suma
spustí	spustit	k5eAaPmIp3nS	spustit
vlnu	vlna	k1gFnSc4	vlna
rivality	rivalita	k1gFnSc2	rivalita
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
jako	jako	k8xS	jako
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Petra	Petr	k1gMnSc2	Petr
se	se	k3xPyFc4	se
tak	tak	k9	tak
zrodí	zrodit	k5eAaPmIp3nS	zrodit
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
dědicem	dědic	k1gMnSc7	dědic
jen	jen	k9	jen
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
odlišní	odlišný	k2eAgMnPc1d1	odlišný
<g/>
...	...	k?	...
Co	co	k9	co
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Léonův	Léonův	k2eAgMnSc1d1	Léonův
syn	syn	k1gMnSc1	syn
<g/>
?	?	kIx.	?
</s>
<s>
Co	co	k9	co
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
nevěrná	věrný	k2eNgFnSc1d1	nevěrná
<g/>
?	?	kIx.	?
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
postupně	postupně	k6eAd1	postupně
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
celou	celý	k2eAgFnSc4d1	celá
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
deptá	deptat	k5eAaImIp3nS	deptat
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Petra	Petr	k1gMnSc4	Petr
téměř	téměř	k6eAd1	téměř
nenávidí	nenávidět	k5eAaImIp3nP	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ránou	rána	k1gFnSc7	rána
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadcházející	nadcházející	k2eAgFnSc1d1	nadcházející
svatba	svatba	k1gFnSc1	svatba
Jana	Jana	k1gFnSc1	Jana
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
paní	paní	k1gFnSc2	paní
Rosémilly	Rosémilla	k1gFnSc2	Rosémilla
<g/>
.	.	kIx.	.
</s>
<s>
Kupodivu	kupodivu	k6eAd1	kupodivu
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
legitimní	legitimní	k2eAgMnSc1d1	legitimní
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
najmout	najmout	k5eAaPmF	najmout
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
na	na	k7c4	na
zaoceánskou	zaoceánský	k2eAgFnSc4d1	zaoceánská
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xC	jako
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
Fort	Fort	k?	Fort
comme	commat	k5eAaPmIp3nS	commat
la	la	k1gNnSc1	la
mort	morta	k1gFnPc2	morta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1888-9	[number]	k4	1888-9
-	-	kIx~	-
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
žárlivost	žárlivost	k1gFnSc1	žárlivost
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
jako	jako	k9	jako
náhrobní	náhrobní	k2eAgInSc4d1	náhrobní
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Malíř	malíř	k1gMnSc1	malíř
Olivier	Olivier	k1gMnSc1	Olivier
Bertin	Bertin	k2eAgMnSc1d1	Bertin
hostí	hostit	k5eAaImIp3nS	hostit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
ateliéru	ateliér	k1gInSc6	ateliér
ty	ten	k3xDgFnPc4	ten
nejkrásnější	krásný	k2eAgFnPc4d3	nejkrásnější
ženy	žena	k1gFnPc4	žena
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Anne	Ann	k1gFnSc2	Ann
de	de	k?	de
Guilleroy	Guilleroa	k1gFnSc2	Guilleroa
<g/>
,	,	kIx,	,
sveden	sveden	k2eAgMnSc1d1	sveden
její	její	k3xOp3gFnSc7	její
laskavostí	laskavost	k1gFnSc7	laskavost
a	a	k8xC	a
elegancí	elegance	k1gFnSc7	elegance
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
bohatého	bohatý	k2eAgMnSc2d1	bohatý
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
zbohatlého	zbohatlý	k2eAgMnSc2d1	zbohatlý
poslance	poslanec	k1gMnSc2	poslanec
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
šestileté	šestiletý	k2eAgFnSc2d1	šestiletá
holčičky	holčička	k1gFnSc2	holčička
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
ale	ale	k8xC	ale
nebrání	bránit	k5eNaImIp3nS	bránit
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
malířovou	malířův	k2eAgFnSc7d1	malířova
milenkou	milenka	k1gFnSc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Uplyne	uplynout	k5eAaPmIp3nS	uplynout
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
malíř	malíř	k1gMnSc1	malíř
vášnivě	vášnivě	k6eAd1	vášnivě
milován	milován	k2eAgMnSc1d1	milován
touto	tento	k3xDgFnSc7	tento
vdanou	vdaný	k2eAgFnSc7d1	vdaná
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Obětovala	obětovat	k5eAaBmAgFnS	obětovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udržela	udržet	k5eAaPmAgFnS	udržet
tuto	tento	k3xDgFnSc4	tento
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
koketérii	koketérie	k1gFnSc4	koketérie
a	a	k8xC	a
šarm	šarm	k1gInSc4	šarm
a	a	k8xC	a
uměla	umět	k5eAaImAgFnS	umět
mu	on	k3xPp3gMnSc3	on
lichotit	lichotit	k5eAaImF	lichotit
<g/>
.	.	kIx.	.
</s>
<s>
Bertin	Bertin	k2eAgInSc1d1	Bertin
ji	on	k3xPp3gFnSc4	on
oplátkou	oplátka	k1gFnSc7	oplátka
věnuje	věnovat	k5eAaImIp3nS	věnovat
hluboký	hluboký	k2eAgInSc1d1	hluboký
cit	cit	k1gInSc1	cit
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
zamilované	zamilovaný	k2eAgNnSc1d1	zamilované
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
s	s	k7c7	s
časem	čas	k1gInSc7	čas
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Anne	Ann	k1gFnSc2	Ann
Annette	Annett	k1gInSc5	Annett
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
obraz	obraz	k1gInSc4	obraz
své	svůj	k3xOyFgFnSc2	svůj
milenky	milenka	k1gFnSc2	milenka
s	s	k7c7	s
tváří	tvář	k1gFnSc7	tvář
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Anette	Anett	k1gInSc5	Anett
obraz	obraz	k1gInSc1	obraz
své	svůj	k3xOyFgFnSc2	svůj
dřívější	dřívější	k2eAgFnSc2d1	dřívější
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
rozboří	rozbořit	k5eAaPmIp3nS	rozbořit
stávající	stávající	k2eAgInSc1d1	stávající
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
milenci	milenec	k1gMnPc7	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
dumají	dumat	k5eAaImIp3nP	dumat
nad	nad	k7c7	nad
míjejícími	míjející	k2eAgNnPc7d1	míjející
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
stárnutím	stárnutí	k1gNnSc7	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Bertin	Bertin	k2eAgInSc1d1	Bertin
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
svou	svůj	k3xOyFgFnSc4	svůj
osamělost	osamělost	k1gFnSc4	osamělost
a	a	k8xC	a
potlačovanou	potlačovaný	k2eAgFnSc4d1	potlačovaná
závist	závist	k1gFnSc4	závist
vůči	vůči	k7c3	vůči
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
podváděnému	podváděný	k2eAgMnSc3d1	podváděný
<g/>
.	.	kIx.	.
</s>
<s>
Slepá	slepý	k2eAgFnSc1d1	slepá
láska	láska	k1gFnSc1	láska
-	-	kIx~	-
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
smrt	smrt	k1gFnSc1	smrt
-	-	kIx~	-
k	k	k7c3	k
Anette	Anett	k1gInSc5	Anett
ho	on	k3xPp3gMnSc4	on
dovedete	dovést	k5eAaPmIp2nP	dovést
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
obětí	oběť	k1gFnSc7	oběť
nehody	nehoda	k1gFnPc1	nehoda
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
sil	síla	k1gFnPc2	síla
požádá	požádat	k5eAaPmIp3nS	požádat
Anne	Anne	k1gInSc1	Anne
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zničila	zničit	k5eAaPmAgFnS	zničit
jejich	jejich	k3xOp3gFnSc4	jejich
milostnou	milostný	k2eAgFnSc4d1	milostná
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc4	náš
srdce	srdce	k1gNnSc4	srdce
Maupassantova	Maupassantův	k2eAgFnSc1d1	Maupassantova
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
naplněna	naplnit	k5eAaPmNgFnS	naplnit
kritikou	kritika	k1gFnSc7	kritika
všech	všecek	k3xTgFnPc2	všecek
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
té	ten	k3xDgFnSc3	ten
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
–	–	k?	–
titulované	titulovaný	k2eAgNnSc1d1	titulované
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
právem	právem	k6eAd1	právem
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
kritika	kritika	k1gFnSc1	kritika
mravů	mrav	k1gInPc2	mrav
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Prunier	Prunier	k1gMnSc1	Prunier
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Valmont	Valmont	k1gMnSc1	Valmont
Maufrigneuse	Maufrigneuse	k1gFnSc2	Maufrigneuse
LAGARDÉ	LAGARDÉ	kA	LAGARDÉ
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
<g/>
;	;	kIx,	;
MICHARD	MICHARD	kA	MICHARD
<g/>
,	,	kIx,	,
Laurent	Laurent	k1gInSc1	Laurent
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
579	[number]	k4	579
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7407	[number]	k4	7407
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
MAUPASSANT	MAUPASSANT	kA	MAUPASSANT
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
<g/>
.	.	kIx.	.
</s>
<s>
Kulička	kulička	k1gFnSc1	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Stanislav	Stanislav	k1gMnSc1	Stanislav
Guth-Jarkovský	Guth-Jarkovský	k2eAgMnSc1d1	Guth-Jarkovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jos	Jos	k1gFnSc1	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
220	[number]	k4	220
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guy	Guy	k1gMnSc2	Guy
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
Osoba	osoba	k1gFnSc1	osoba
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Autor	autor	k1gMnSc1	autor
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Guy	Guy	k1gMnSc2	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
Stránky	stránka	k1gFnSc2	stránka
Společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
Guye	Guye	k1gNnSc2	Guye
de	de	k?	de
Maupassanta	Maupassanta	k1gFnSc1	Maupassanta
</s>
