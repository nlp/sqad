<s>
Shine	Shinout	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
,	,	kIx,
Edenbridge	Edenbridge	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ShineInterpretEdenbridgeDruh	ShineInterpretEdenbridgeDruh	k1gInSc1
albaStudiové	albaStudiový	k2eAgFnSc2d1
albumVydáno	albumVydán	k2eAgNnSc1d1
<g/>
2004	#num#	k4
<g/>
ŽánrHeavy	ŽánrHeava	k1gFnSc2
metalDélka	metalDélka	k1gFnSc1
<g/>
59	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
<g/>
VydavatelstvíMassacre	VydavatelstvíMassacr	k1gInSc5
RecordsProducentLanvallEdenbridge	RecordsProducentLanvallEdenbridge	k1gNnSc3
chronologicky	chronologicky	k6eAd1
</s>
<s>
A	a	k9
Livetime	Livetim	k1gInSc5
in	in	k?
Eden	Eden	k1gInSc1
<g/>
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Shine	Shinout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Grand	grand	k1gMnSc1
Design	design	k1gInSc1
<g/>
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Shine	Shinout	k5eAaPmIp3nS,k5eAaImIp3nS
je	on	k3xPp3gNnSc4
čtvrté	čtvrtý	k4xOgNnSc4
album	album	k1gNnSc4
od	od	k7c2
rakouské	rakouský	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Edenbridge	Edenbridg	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Shine	Shin	k1gMnSc5
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
„	„	k?
<g/>
Move	Move	k1gInSc1
along	along	k1gMnSc1
home	home	k1gFnPc2
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
</s>
<s>
„	„	k?
<g/>
Centennial	Centennial	k1gInSc1
Legend	legenda	k1gFnPc2
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
</s>
<s>
„	„	k?
<g/>
Wild	Wild	k1gMnSc1
Chase	chasa	k1gFnSc3
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
</s>
<s>
„	„	k?
<g/>
And	Anda	k1gFnPc2
the	the	k?
road	road	k1gMnSc1
goes	goes	k6eAd1
on	on	k3xPp3gMnSc1
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
</s>
<s>
„	„	k?
<g/>
What	What	k1gInSc1
you	you	k?
leave	leavat	k5eAaPmIp3nS
behind	behind	k1gInSc1
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
</s>
<s>
„	„	k?
<g/>
Elsewhere	Elsewher	k1gMnSc5
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
</s>
<s>
„	„	k?
<g/>
October	October	k1gMnSc1
Sky	Sky	k1gMnSc1
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnPc1
Canterville	Cantervill	k1gMnSc2
Prophecy	Propheca	k1gMnSc2
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnSc1
Canterville	Canterville	k1gFnSc1
Ghost	Ghost	k1gFnSc1
<g/>
“	“	k?
–	–	k?
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
Sacred	Sacred	k1gMnSc1
Ground	Ground	k1gMnSc1
<g/>
“	“	k?
–	–	k?
(	(	kIx(
<g/>
Bonus	bonus	k1gInSc1
Track	Tracka	k1gFnPc2
Europe	Europ	k1gInSc5
<g/>
)	)	kIx)
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
„	„	k?
<g/>
Anthem	anthem	k1gInSc1
<g/>
“	“	k?
–	–	k?
(	(	kIx(
<g/>
Bonus	bonus	k1gInSc1
Track	Track	k1gMnSc1
Japan	japan	k1gInSc1
<g/>
)	)	kIx)
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
