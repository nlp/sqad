<s>
Gotický	gotický	k2eAgInSc1d1	gotický
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
gothic	gothic	k1gMnSc1	gothic
metal	metat	k5eAaImAgMnS	metat
či	či	k8xC	či
goth	goth	k1gMnSc1	goth
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
zkombinováním	zkombinování	k1gNnSc7	zkombinování
death	deatha	k1gFnPc2	deatha
<g/>
/	/	kIx~	/
<g/>
doomu	doomat	k5eAaPmIp1nS	doomat
a	a	k8xC	a
gothic	gothic	k1gMnSc1	gothic
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
gothic	gothic	k1gMnSc1	gothic
metal	metal	k1gInSc4	metal
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
naprosto	naprosto	k6eAd1	naprosto
odlišných	odlišný	k2eAgInPc2d1	odlišný
metalových	metalový	k2eAgInPc2d1	metalový
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
zařazením	zařazení	k1gNnSc7	zařazení
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaké	jaký	k3yRgNnSc1	jaký
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
kláves	klávesa	k1gFnPc2	klávesa
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
hluboko	hluboko	k6eAd1	hluboko
posazený	posazený	k2eAgInSc1d1	posazený
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
growlingem	growling	k1gInSc7	growling
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ženské	ženský	k2eAgInPc1d1	ženský
hlasy	hlas	k1gInPc1	hlas
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
operní	operní	k2eAgFnPc1d1	operní
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
temné	temný	k2eAgFnPc1d1	temná
až	až	k8xS	až
depresivní	depresivní	k2eAgFnPc1d1	depresivní
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
beznadějí	beznaděj	k1gFnSc7	beznaděj
<g/>
,	,	kIx,	,
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
vampýrismem	vampýrismus	k1gInSc7	vampýrismus
<g/>
,	,	kIx,	,
hororovou	hororový	k2eAgFnSc7d1	hororová
fikcí	fikce	k1gFnSc7	fikce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
romantickou	romantický	k2eAgFnSc7d1	romantická
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
gothic	gothice	k1gFnPc2	gothice
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
gothic	gothic	k1gMnSc1	gothic
rock	rock	k1gInSc4	rock
s	s	k7c7	s
metalovými	metalový	k2eAgInPc7d1	metalový
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Glenn	Glenn	k1gInSc1	Glenn
Danzigova	Danzigův	k2eAgFnSc1d1	Danzigův
skupina	skupina	k1gFnSc1	skupina
Samhain	Samhaina	k1gFnPc2	Samhaina
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
také	také	k6eAd1	také
projevila	projevit	k5eAaPmAgFnS	projevit
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
se	se	k3xPyFc4	se
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
kapele	kapela	k1gFnSc3	kapela
Christian	Christian	k1gMnSc1	Christian
Death	Death	k1gMnSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Hluboký	hluboký	k2eAgInSc1d1	hluboký
baryton	baryton	k1gInSc1	baryton
Rozze	Rozze	k?	Rozze
Williamse	Williams	k1gMnSc2	Williams
a	a	k8xC	a
Valora	Valor	k1gMnSc2	Valor
Kanda	Kando	k1gNnSc2	Kando
<g/>
,	,	kIx,	,
sestupné	sestupný	k2eAgFnPc1d1	sestupná
basové	basový	k2eAgFnPc1d1	basová
kytary	kytara	k1gFnPc1	kytara
a	a	k8xC	a
děsivé	děsivý	k2eAgInPc1d1	děsivý
synthy	synth	k1gInPc1	synth
udělaly	udělat	k5eAaPmAgInP	udělat
na	na	k7c4	na
spoustu	spousta	k1gFnSc4	spousta
následujících	následující	k2eAgFnPc2d1	následující
kapel	kapela	k1gFnPc2	kapela
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
u	u	k7c2	u
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
Celtic	Celtice	k1gFnPc2	Celtice
Frost	Frost	k1gFnSc1	Frost
sehrála	sehrát	k5eAaPmAgFnS	sehrát
kapela	kapela	k1gFnSc1	kapela
výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
pozdější	pozdní	k2eAgMnSc1d2	pozdější
"	"	kIx"	"
<g/>
gotický	gotický	k2eAgInSc1d1	gotický
<g/>
"	"	kIx"	"
zvuk	zvuk	k1gInSc1	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
pár	pár	k4xCyI	pár
mladých	mladý	k2eAgFnPc2d1	mladá
kapel	kapela	k1gFnPc2	kapela
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Anglii	Anglie	k1gFnSc6	Anglie
vypůjčilo	vypůjčit	k5eAaPmAgNnS	vypůjčit
zvuk	zvuk	k1gInSc4	zvuk
gothic	gothic	k1gMnSc1	gothic
rocku	rock	k1gInSc2	rock
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
spojily	spojit	k5eAaPmAgFnP	spojit
jej	on	k3xPp3gMnSc4	on
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
vznikajícím	vznikající	k2eAgInSc7d1	vznikající
death	death	k1gInSc4	death
<g/>
/	/	kIx~	/
<g/>
doomem	doom	k1gMnSc7	doom
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
growling	growling	k1gInSc4	growling
a	a	k8xC	a
beatblasty	beatblast	k1gInPc4	beatblast
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
hutností	hutnost	k1gFnSc7	hutnost
a	a	k8xC	a
ponurou	ponurý	k2eAgFnSc7d1	ponurá
atmosférou	atmosféra	k1gFnSc7	atmosféra
doomu	doom	k1gInSc2	doom
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
neoficiálně	neoficiálně	k6eAd1	neoficiálně
nazýván	nazýván	k2eAgMnSc1d1	nazýván
gothic	gothic	k1gMnSc1	gothic
doom	doom	k1gMnSc1	doom
nebo	nebo	k8xC	nebo
také	také	k9	také
gothic	gothic	k1gMnSc1	gothic
death	death	k1gMnSc1	death
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
fázi	fáze	k1gFnSc4	fáze
vývoje	vývoj	k1gInSc2	vývoj
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýraznější	výrazný	k2eAgMnPc4d3	nejvýraznější
průkopníky	průkopník	k1gMnPc4	průkopník
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
i	i	k8xC	i
zakladatele	zakladatel	k1gMnSc2	zakladatel
stylu	styl	k1gInSc2	styl
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
trojlístek	trojlístek	k1gInSc1	trojlístek
severoanglických	severoanglický	k2eAgFnPc2d1	severoanglická
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Peaceville	Peaceville	k1gFnSc1	Peaceville
Three	Three	k1gFnSc1	Three
-	-	kIx~	-
Paradise	Paradise	k1gFnSc1	Paradise
Lost	Losta	k1gFnPc2	Losta
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Dying	Dying	k1gInSc1	Dying
Bride	Brid	k1gInSc5	Brid
a	a	k8xC	a
Anathema	anathema	k1gNnSc4	anathema
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
metalová	metalový	k2eAgFnSc1d1	metalová
scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
black	black	k1gInSc4	black
a	a	k8xC	a
death	death	k1gInSc4	death
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kapel	kapela	k1gFnPc2	kapela
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
experimentovaly	experimentovat	k5eAaImAgFnP	experimentovat
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
gothic	gothice	k1gFnPc2	gothice
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Tiamat	Tiamat	k1gInSc1	Tiamat
<g/>
,	,	kIx,	,
Theatre	Theatr	k1gInSc5	Theatr
of	of	k?	of
Tragedy	Trageda	k1gMnSc2	Trageda
nebo	nebo	k8xC	nebo
Lake	Lak	k1gMnSc2	Lak
of	of	k?	of
Tears	Tears	k1gInSc1	Tears
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
zpěv	zpěv	k1gInSc1	zpěv
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
částečně	částečně	k6eAd1	částečně
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
skupin	skupina	k1gFnPc2	skupina
Paradise	Paradise	k1gFnSc1	Paradise
Lost	Lost	k1gInSc1	Lost
a	a	k8xC	a
Anathema	anathema	k1gNnSc1	anathema
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
až	až	k9	až
norští	norský	k2eAgMnPc1d1	norský
Theatre	Theatr	k1gInSc5	Theatr
of	of	k?	of
Tragedy	Traged	k1gMnPc7	Traged
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
party	party	k1gFnSc7	party
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
stejným	stejný	k2eAgInSc7d1	stejný
dílem	díl	k1gInSc7	díl
a	a	k8xC	a
zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
forma	forma	k1gFnSc1	forma
duetu	duet	k1gInSc2	duet
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
beauty	beaut	k1gMnPc4	beaut
and	and	k?	and
the	the	k?	the
beast	beast	k1gInSc4	beast
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
soprán	soprán	k1gInSc4	soprán
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
mužským	mužský	k2eAgInSc7d1	mužský
growlingem	growling	k1gInSc7	growling
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
kapely	kapela	k1gFnSc2	kapela
stála	stát	k5eAaImAgFnS	stát
Liv	Liv	k1gFnSc1	Liv
Kristine	Kristin	k1gInSc5	Kristin
Espenaes	Espenaes	k1gInSc4	Espenaes
Krull	Krull	k1gInSc4	Krull
-	-	kIx~	-
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
z	z	k7c2	z
gothic	gothice	k1gFnPc2	gothice
rockových	rockový	k2eAgMnPc2d1	rockový
The	The	k1gMnPc2	The
Crest	Crest	k1gMnSc1	Crest
<g/>
,	,	kIx,	,
Nell	Nell	k1gMnSc1	Nell
Sigland	Sigland	k1gInSc1	Sigland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
skupina	skupina	k1gFnSc1	skupina
Type	typ	k1gInSc5	typ
O	o	k7c6	o
Negative	negativ	k1gInSc5	negativ
byla	být	k5eAaImAgFnS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
především	především	k6eAd1	především
s	s	k7c7	s
death	death	k1gInSc1	death
metalem	metal	k1gInSc7	metal
a	a	k8xC	a
thrash	thrash	k1gInSc1	thrash
metalem	metal	k1gInSc7	metal
kvůli	kvůli	k7c3	kvůli
předchozí	předchozí	k2eAgFnSc3d1	předchozí
kapele	kapela	k1gFnSc3	kapela
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
basisty	basista	k1gMnSc2	basista
Petera	Peter	k1gMnSc2	Peter
Steela	Steel	k1gMnSc2	Steel
<g/>
,	,	kIx,	,
Carnivore	Carnivor	k1gInSc5	Carnivor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
však	však	k9	však
hojně	hojně	k6eAd1	hojně
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
i	i	k9	i
vlivy	vliv	k1gInPc1	vliv
doom	dooma	k1gFnPc2	dooma
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
hardcoru	hardcor	k1gInSc2	hardcor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
gothic	gothic	k1gMnSc1	gothic
rocku	rock	k1gInSc2	rock
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
album	album	k1gNnSc1	album
Bloody	Blooda	k1gFnSc2	Blooda
Kisses	Kissesa	k1gFnPc2	Kissesa
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
představitelem	představitel	k1gMnSc7	představitel
gothic	gothice	k1gInPc2	gothice
metalové	metalový	k2eAgFnSc2d1	metalová
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
USA	USA	kA	USA
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
velice	velice	k6eAd1	velice
významným	významný	k2eAgMnSc7d1	významný
zástupcem	zástupce	k1gMnSc7	zástupce
žánru	žánr	k1gInSc2	žánr
jsou	být	k5eAaImIp3nP	být
portugalští	portugalský	k2eAgMnPc1d1	portugalský
Moonspell	Moonspella	k1gFnPc2	Moonspella
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začínali	začínat	k5eAaImAgMnP	začínat
jako	jako	k9	jako
black	black	k6eAd1	black
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
hudbu	hudba	k1gFnSc4	hudba
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
temná	temný	k2eAgFnSc1d1	temná
vampýristická	vampýristický	k2eAgFnSc1d1	vampýristický
poetika	poetika	k1gFnSc1	poetika
<g/>
,	,	kIx,	,
hutné	hutný	k2eAgFnPc1d1	hutná
dusavé	dusavý	k2eAgFnPc4d1	dusavá
bicí	bicí	k2eAgFnPc4d1	bicí
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
hluboký	hluboký	k2eAgInSc1d1	hluboký
baryton	baryton	k1gInSc1	baryton
Fernanda	Fernando	k1gNnSc2	Fernando
Ribeira	Ribeir	k1gInSc2	Ribeir
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Wolfheart	Wolfhearta	k1gFnPc2	Wolfhearta
patří	patřit	k5eAaImIp3nS	patřit
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
ke	k	k7c3	k
klasice	klasika	k1gFnSc3	klasika
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
výraznými	výrazný	k2eAgMnPc7d1	výrazný
představiteli	představitel	k1gMnPc7	představitel
žánru	žánr	k1gInSc2	žánr
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
kapely	kapela	k1gFnPc1	kapela
Draconian	Draconian	k1gMnSc1	Draconian
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Sins	Sinsa	k1gFnPc2	Sinsa
Of	Of	k1gMnSc1	Of
Thy	Thy	k1gMnSc1	Thy
Beloved	Beloved	k1gMnSc1	Beloved
<g/>
,	,	kIx,	,
Theatres	Theatres	k1gMnSc1	Theatres
Des	des	k1gNnSc2	des
Vampires	Vampiresa	k1gFnPc2	Vampiresa
<g/>
,	,	kIx,	,
Evereve	Evereev	k1gFnSc2	Evereev
nebo	nebo	k8xC	nebo
Trail	Trail	k1gInSc1	Trail
Of	Of	k1gFnSc2	Of
Tears	Tearsa	k1gFnPc2	Tearsa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
agresivní	agresivní	k2eAgFnSc1d1	agresivní
verze	verze	k1gFnSc1	verze
gothic	gothice	k1gFnPc2	gothice
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgFnP	zaměřit
na	na	k7c4	na
ženské	ženský	k2eAgInPc4d1	ženský
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vysoko	vysoko	k6eAd1	vysoko
posazené	posazený	k2eAgNnSc1d1	posazené
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
operní	operní	k2eAgFnPc1d1	operní
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
omezily	omezit	k5eAaPmAgFnP	omezit
doom	doom	k6eAd1	doom
metalové	metalový	k2eAgInPc1d1	metalový
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
deathmetalový	deathmetalový	k2eAgInSc1d1	deathmetalový
growling	growling	k1gInSc1	growling
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Within	Within	k2eAgInSc1d1	Within
Temptation	Temptation	k1gInSc1	Temptation
(	(	kIx(	(
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lacuna	Lacuna	k1gFnSc1	Lacuna
Coil	Coil	k1gMnSc1	Coil
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
After	After	k1gMnSc1	After
Forever	Forever	k1gMnSc1	Forever
(	(	kIx(	(
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tristania	Tristanium	k1gNnSc2	Tristanium
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sirenia	Sirenium	k1gNnSc2	Sirenium
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
sem	sem	k6eAd1	sem
bývají	bývat	k5eAaImIp3nP	bývat
řazeni	řadit	k5eAaImNgMnP	řadit
i	i	k9	i
Nightwish	Nightwish	k1gMnSc1	Nightwish
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zde	zde	k6eAd1	zde
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
symphony	symphon	k1gInPc4	symphon
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
také	také	k9	také
přidávají	přidávat	k5eAaImIp3nP	přidávat
vlivy	vliv	k1gInPc1	vliv
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
klasický	klasický	k2eAgInSc4d1	klasický
rock	rock	k1gInSc4	rock
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
bývá	bývat	k5eAaImIp3nS	bývat
lehčí	lehký	k2eAgFnSc1d2	lehčí
a	a	k8xC	a
snesitelnější	snesitelný	k2eAgFnSc1d2	snesitelnější
forma	forma	k1gFnSc1	forma
žánru	žánr	k1gInSc2	žánr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Delain	Delain	k1gInSc1	Delain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
gothic	gothic	k1gMnSc1	gothic
metal	metal	k1gInSc4	metal
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
posouvat	posouvat	k5eAaImF	posouvat
k	k	k7c3	k
mainstreamu	mainstream	k1gInSc3	mainstream
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
těchto	tento	k3xDgFnPc2	tento
kapel	kapela	k1gFnPc2	kapela
vznikalo	vznikat	k5eAaImAgNnS	vznikat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
HIM	HIM	kA	HIM
<g/>
,	,	kIx,	,
Entwine	Entwin	k1gInSc5	Entwin
nebo	nebo	k8xC	nebo
Poisonblack	Poisonblack	k1gInSc1	Poisonblack
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
obrovských	obrovský	k2eAgInPc2d1	obrovský
prodejních	prodejní	k2eAgInPc2d1	prodejní
úspěchů	úspěch	k1gInPc2	úspěch
a	a	k8xC	a
vyšplhaly	vyšplhat	k5eAaPmAgFnP	vyšplhat
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
gothic	gothice	k1gFnPc2	gothice
metalem	metal	k1gInSc7	metal
však	však	k8xC	však
kromě	kromě	k7c2	kromě
názvu	název	k1gInSc2	název
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc4	tento
převážně	převážně	k6eAd1	převážně
poprockové	poprockový	k2eAgFnPc1d1	poprocková
kapely	kapela	k1gFnPc1	kapela
už	už	k9	už
jen	jen	k9	jen
pramálo	pramálo	k6eAd1	pramálo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
sláva	sláva	k1gFnSc1	sláva
skupiny	skupina	k1gFnSc2	skupina
Marilyn	Marilyn	k1gFnSc1	Marilyn
Manson	Manson	k1gMnSc1	Manson
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
novináři	novinář	k1gMnPc1	novinář
ji	on	k3xPp3gFnSc4	on
zařadili	zařadit	k5eAaPmAgMnP	zařadit
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
gothic	gothic	k1gMnSc1	gothic
rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
gothic	gothic	k1gMnSc1	gothic
metal	metat	k5eAaImAgMnS	metat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
lze	lze	k6eAd1	lze
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
hudbě	hudba	k1gFnSc6	hudba
nalézt	nalézt	k5eAaBmF	nalézt
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
znaků	znak	k1gInPc2	znak
těchto	tento	k3xDgInPc2	tento
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
popularita	popularita	k1gFnSc1	popularita
industrial	industrial	k1gInSc1	industrial
rocku	rock	k1gInSc2	rock
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
těchto	tento	k3xDgInPc2	tento
omylů	omyl	k1gInPc2	omyl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
"	"	kIx"	"
<g/>
gotickým	gotický	k2eAgInSc7d1	gotický
<g/>
"	"	kIx"	"
stylem	styl	k1gInSc7	styl
oblékání	oblékání	k1gNnSc2	oblékání
podle	podle	k7c2	podle
různých	různý	k2eAgFnPc2d1	různá
gothic	gothice	k1gFnPc2	gothice
metalových	metalový	k2eAgFnPc2d1	metalová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
gothic	gothic	k1gMnSc1	gothic
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
metalových	metalový	k2eAgInPc2d1	metalový
subžánrů	subžánr	k1gInPc2	subžánr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těžko	těžko	k6eAd1	těžko
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
hlavní	hlavní	k2eAgInPc1d1	hlavní
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
<g/>
:	:	kIx,	:
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
abstraktní	abstraktní	k2eAgNnPc4d1	abstraktní
témata	téma	k1gNnPc4	téma
<g/>
:	:	kIx,	:
víra	víra	k1gFnSc1	víra
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nebe	nebe	k1gNnSc1	nebe
a	a	k8xC	a
peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc1d1	gotický
horor	horor	k1gInSc1	horor
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
blízkých	blízký	k2eAgMnPc2d1	blízký
<g/>
,	,	kIx,	,
prázdnota	prázdnota	k1gFnSc1	prázdnota
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nezřídka	nezřídka	k6eAd1	nezřídka
bývají	bývat	k5eAaImIp3nP	bývat
texty	text	k1gInPc1	text
inspirovány	inspirován	k2eAgInPc1d1	inspirován
gotickou	gotický	k2eAgFnSc7d1	gotická
literaturou	literatura	k1gFnSc7	literatura
nebo	nebo	k8xC	nebo
díly	díl	k1gInPc1	díl
E.	E.	kA	E.
A.	A.	kA	A.
Poa	Poa	k1gMnSc1	Poa
<g/>
.	.	kIx.	.
zpěv	zpěv	k1gInSc1	zpěv
<g/>
:	:	kIx,	:
Mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
hluboko	hluboko	k6eAd1	hluboko
posazený	posazený	k2eAgInSc1d1	posazený
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
kombinován	kombinován	k2eAgInSc1d1	kombinován
s	s	k7c7	s
growlingem	growling	k1gInSc7	growling
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženský	k2eAgInPc1d1	ženský
hlasy	hlas	k1gInPc1	hlas
bývají	bývat	k5eAaImIp3nP	bývat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
a	a	k8xC	a
operní	operní	k2eAgFnPc1d1	operní
<g/>
.	.	kIx.	.
kytary	kytara	k1gFnPc1	kytara
a	a	k8xC	a
basy	basa	k1gFnPc1	basa
hrají	hrát	k5eAaImIp3nP	hrát
prim	prim	k1gInSc4	prim
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
distortion	distortion	k1gInSc4	distortion
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
doom	doom	k6eAd1	doom
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
bicí	bicí	k2eAgFnPc1d1	bicí
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
synthetizéry	synthetizéra	k1gFnPc1	synthetizéra
<g/>
,	,	kIx,	,
pomalá	pomalý	k2eAgFnSc1d1	pomalá
doom	doom	k6eAd1	doom
metalová	metalový	k2eAgNnPc1d1	metalové
tempa	tempo	k1gNnPc1	tempo
často	často	k6eAd1	často
střídají	střídat	k5eAaImIp3nP	střídat
dva	dva	k4xCgInPc1	dva
kopáky	kopák	k1gInPc1	kopák
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
i	i	k9	i
s	s	k7c7	s
beatblasty	beatblast	k1gInPc7	beatblast
Gothic	Gothice	k1gInPc2	Gothice
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgMnSc1d1	zaměňován
se	se	k3xPyFc4	se
symphonic	symphonice	k1gFnPc2	symphonice
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
gothic	gothice	k1gFnPc2	gothice
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
agresivní	agresivní	k2eAgNnSc1d1	agresivní
a	a	k8xC	a
melancholicky	melancholicky	k6eAd1	melancholicky
laděný	laděný	k2eAgMnSc1d1	laděný
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
z	z	k7c2	z
death	deatha	k1gFnPc2	deatha
<g/>
/	/	kIx~	/
<g/>
doomu	dooma	k1gFnSc4	dooma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
symphonic	symphonice	k1gFnPc2	symphonice
metal	metat	k5eAaImAgInS	metat
popovější	popový	k2eAgInSc1d2	popovější
a	a	k8xC	a
líbivější	líbivý	k2eAgInSc1d2	líbivější
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
bombastičtější	bombastický	k2eAgFnSc1d2	bombastičtější
a	a	k8xC	a
epicky	epicky	k6eAd1	epicky
rozmáchlý	rozmáchlý	k2eAgInSc1d1	rozmáchlý
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
<g/>
,	,	kIx,	,
flétny	flétna	k1gFnPc1	flétna
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
motivy	motiv	k1gInPc1	motiv
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
Nightwish	Nightwish	k1gMnSc1	Nightwish
<g/>
,	,	kIx,	,
Xandria	Xandrium	k1gNnSc2	Xandrium
<g/>
,	,	kIx,	,
Epica	Epicum	k1gNnSc2	Epicum
<g/>
,	,	kIx,	,
Edenbridge	Edenbridge	k1gNnSc2	Edenbridge
nebo	nebo	k8xC	nebo
Within	Within	k2eAgInSc4d1	Within
Temptation	Temptation	k1gInSc4	Temptation
<g/>
.	.	kIx.	.
</s>
<s>
Therion	Therion	k1gInSc1	Therion
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
Šédsko	Šédsko	k1gNnSc1	Šédsko
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
Nostra	Nostra	k1gFnSc1	Nostra
Morte	Mort	k1gInSc5	Mort
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
Semblant	Semblant	k1gMnSc1	Semblant
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
Gotické	gotický	k2eAgNnSc1d1	gotické
hnutí	hnutí	k1gNnSc1	hnutí
Gothic	Gothice	k1gFnPc2	Gothice
rock	rock	k1gInSc4	rock
Symphonic	Symphonice	k1gFnPc2	Symphonice
metal	metat	k5eAaImAgMnS	metat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gotický	gotický	k2eAgInSc4d1	gotický
metal	metal	k1gInSc4	metal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Metalové	metalový	k2eAgFnSc2d1	metalová
databáze	databáze	k1gFnSc2	databáze
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
BNR	BNR	kA	BNR
Metal	metat	k5eAaImAgMnS	metat
Pages	Pages	k1gInSc4	Pages
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
Metallum	Metallum	k1gNnSc4	Metallum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Goth	Gotha	k1gFnPc2	Gotha
Metalové	metalový	k2eAgInPc1d1	metalový
články	článek	k1gInPc1	článek
/	/	kIx~	/
portály	portál	k1gInPc1	portál
/	/	kIx~	/
přehledy	přehled	k1gInPc1	přehled
<g/>
:	:	kIx,	:
Goth	Gotha	k1gFnPc2	Gotha
Metal	metal	k1gInSc1	metal
World	World	k1gMnSc1	World
</s>
