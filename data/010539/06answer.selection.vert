<s>
Největší	veliký	k2eAgNnSc1d3	veliký
povodí	povodí	k1gNnSc1	povodí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
:	:	kIx,	:
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
6	[number]	k4	6
915	[number]	k4	915
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
