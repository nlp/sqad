<s>
Illinci	Illinec	k1gMnPc1
</s>
<s>
Illinci	Illinec	k1gMnPc1
І	І	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
214	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
2	#num#	k4
Stát	stát	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
Oblast	oblast	k1gFnSc1
</s>
<s>
Vinnycká	Vinnycký	k2eAgFnSc1d1
Rajón	rajón	k1gInSc1
</s>
<s>
Illinecký	Illinecký	k2eAgInSc1d1
</s>
<s>
Illinci	Illinec	k1gMnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6,0	6,0	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
11	#num#	k4
270	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
894,1	894,1	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Volodymyr	Volodymyr	k1gMnSc1
Jaščuk	Jaščuk	k1gMnSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Soborna	Soborna	k1gFnSc1
1922700	#num#	k4
Illinci	Illinec	k1gInSc6
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
4345	#num#	k4
PSČ	PSČ	kA
</s>
<s>
22700	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Illinci	Illinec	k1gInSc1
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
І	І	k?
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Ilińce	Ilińce	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
na	na	k7c6
střední	střední	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
212	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
Kyjeva	Kyjev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
centrem	centr	k1gInSc7
Illineckého	Illinecký	k2eAgInSc2d1
rajónu	rajón	k1gInSc2
Vinnycké	Vinnycký	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
11	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1391	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
však	však	k9
město	město	k1gNnSc1
jmenovalo	jmenovat	k5eAaBmAgNnS,k5eAaImAgNnS
Linci	Linec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
rodištěm	rodiště	k1gNnSc7
polsko-ukrajinského	polsko-ukrajinský	k2eAgMnSc2d1
básníka	básník	k1gMnSc2
a	a	k8xC
skladatele	skladatel	k1gMnSc2
Tomasze	Tomasze	k1gFnSc2
Padury	Padura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Włoszczowa	Włoszczowa	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Edineț	Edineț	k?
<g/>
,	,	kIx,
Moldavsko	Moldavsko	k1gNnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smižany	Smižan	k1gInPc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Ukrajinská	ukrajinský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
statistická	statistický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyjev	Kyjev	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
966	#num#	k4
<g/>
-	-	kIx~
<g/>
8459	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Illinci	Illinec	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vinnycká	Vinnycký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Rajóny	rajón	k1gInPc4
</s>
<s>
Barský	Barský	k2eAgInSc1d1
•	•	k?
Beršadský	Beršadský	k2eAgInSc1d1
•	•	k?
Čečelnycký	Čečelnycký	k2eAgInSc1d1
•	•	k?
Černivecký	Černivecký	k2eAgInSc1d1
•	•	k?
Hajsynský	Hajsynský	k2eAgInSc1d1
•	•	k?
Chmilnycký	Chmilnycký	k2eAgInSc1d1
•	•	k?
Illinecký	Illinecký	k2eAgInSc1d1
•	•	k?
Jampilský	Jampilský	k2eAgInSc1d1
•	•	k?
Kalynivský	Kalynivský	k2eAgInSc1d1
•	•	k?
Kozjatynský	Kozjatynský	k2eAgInSc1d1
•	•	k?
Kryžopilský	Kryžopilský	k2eAgInSc1d1
•	•	k?
Litynský	Litynský	k2eAgInSc1d1
•	•	k?
Lypovecký	Lypovecký	k2eAgInSc1d1
•	•	k?
Mohyliv-podilský	Mohyliv-podilský	k2eAgInSc1d1
•	•	k?
Murovanokurylovecký	Murovanokurylovecký	k2eAgInSc1d1
•	•	k?
Nemyrivský	Nemyrivský	k2eAgInSc1d1
•	•	k?
Orativský	Orativský	k2eAgInSc1d1
•	•	k?
Piščanský	Piščanský	k2eAgInSc1d1
•	•	k?
Pohrebyščenský	Pohrebyščenský	k2eAgInSc1d1
•	•	k?
Šarhorodský	Šarhorodský	k2eAgInSc1d1
•	•	k?
Teplycký	Teplycký	k2eAgInSc1d1
•	•	k?
Tomašpilský	Tomašpilský	k2eAgInSc1d1
•	•	k?
Trosťanecký	Trosťanecký	k2eAgInSc1d1
•	•	k?
Tulčynský	Tulčynský	k2eAgInSc1d1
•	•	k?
Tyvrivský	Tyvrivský	k2eAgInSc1d1
•	•	k?
Vinnycký	Vinnycký	k2eAgInSc1d1
•	•	k?
Žmerynský	Žmerynský	k2eAgInSc1d1
Města	město	k1gNnSc2
</s>
<s>
oblastního	oblastní	k2eAgInSc2d1
významu	význam	k1gInSc2
</s>
<s>
Chmilnyk	Chmilnyk	k1gMnSc1
•	•	k?
Kozjatyn	Kozjatyn	k1gMnSc1
•	•	k?
Ladyžyn	Ladyžyn	k1gMnSc1
•	•	k?
Mohyliv-Podilskyj	Mohyliv-Podilskyj	k1gMnSc1
•	•	k?
Vinnycja	Vinnycja	k1gMnSc1
•	•	k?
Žmerynka	Žmerynka	k1gFnSc1
rajónního	rajónní	k2eAgInSc2d1
významu	význam	k1gInSc2
</s>
<s>
Bar	bar	k1gInSc1
•	•	k?
Beršaď	Beršaď	k1gFnSc2
•	•	k?
Hajsyn	Hajsyn	k1gMnSc1
•	•	k?
Hnivan	Hnivan	k1gMnSc1
•	•	k?
Illinci	Illinec	k1gMnSc3
•	•	k?
Jampil	Jampil	k1gMnSc1
•	•	k?
Kalynivka	Kalynivka	k1gFnSc1
•	•	k?
Lypovec	Lypovec	k1gInSc1
•	•	k?
Nemyriv	Nemyriva	k1gFnPc2
•	•	k?
Pohrebyšče	Pohrebyšče	k1gNnSc4
•	•	k?
Šarhorod	Šarhorod	k1gInSc1
•	•	k?
Tulčyn	Tulčyn	k1gInSc1
</s>
<s>
Sídla	sídlo	k1gNnPc1
městského	městský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Brajiliv	Brajilit	k5eAaPmDgInS
•	•	k?
Braclav	Braclav	k1gMnSc2
•	•	k?
Brodecke	Brodeck	k1gMnSc2
•	•	k?
Čečelnyk	Čečelnyk	k1gMnSc1
•	•	k?
Černivci	Černivec	k1gMnSc3
•	•	k?
Dašiv	Dašivo	k1gNnPc2
•	•	k?
Desna	Desn	k1gMnSc2
•	•	k?
Hluchivci	Hluchivec	k1gMnSc3
•	•	k?
Kopajhorod	Kopajhorod	k1gInSc1
•	•	k?
Kryžopil	Kryžopil	k1gFnSc2
•	•	k?
Kyrnasivka	Kyrnasivka	k1gFnSc1
•	•	k?
Lityn	Lityna	k1gFnPc2
•	•	k?
Murovani	Murovan	k1gMnPc1
Kurylivci	Kurylivec	k1gMnPc1
•	•	k?
Orativ	Orativ	k1gInSc1
•	•	k?
Piščanka	Piščanka	k1gFnSc1
•	•	k?
Rudnycja	Rudnycja	k1gFnSc1
•	•	k?
Špykiv	Špykiva	k1gFnPc2
•	•	k?
Stryžavka	Stryžavka	k1gFnSc1
•	•	k?
Sutysky	Sutyska	k1gFnSc2
•	•	k?
Sytkivci	Sytkivec	k1gMnSc3
•	•	k?
Teplyk	Teplyk	k1gMnSc1
•	•	k?
Tomašpil	Tomašpil	k1gMnSc1
•	•	k?
Trosťanec	Trosťanec	k1gMnSc1
•	•	k?
Turbiv	Turbiv	k1gMnSc1
•	•	k?
Tyvriv	Tyvrivo	k1gNnPc2
•	•	k?
Vapňarka	Vapňarka	k1gFnSc1
•	•	k?
Vendyčany	Vendyčan	k1gMnPc7
•	•	k?
Voronovycja	Voronovycj	k1gInSc2
•	•	k?
Zaliznyčne	Zaliznyčn	k1gInSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
239210508	#num#	k4
</s>
