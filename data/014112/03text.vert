<s>
Teorie	teorie	k1gFnSc1
konvergence	konvergence	k1gFnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navržen	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
celého	celý	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
článku	článek	k1gInSc2
Konvergence	konvergence	k1gFnSc2
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
do	do	k7c2
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odtamtud	odtamtud	k6eAd1
sem	sem	k6eAd1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
přesměrování	přesměrování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
konvergence	konvergence	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
convergentia	convergentia	k1gFnSc1
=	=	kIx~
směřování	směřování	k1gNnSc1
<g/>
,	,	kIx,
sklon	sklon	k1gInSc1
<g/>
,	,	kIx,
sbíhavost	sbíhavost	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
formulována	formulovat	k5eAaImNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
americkým	americký	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
Clarkem	Clarek	k1gMnSc7
Kerrem	Kerr	k1gMnSc7
z	z	k7c2
kalifornské	kalifornský	k2eAgFnSc2d1
Berkeley	Berkelea	k1gFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc4
myšlenku	myšlenka	k1gFnSc4
pak	pak	k6eAd1
navazovali	navazovat	k5eAaImAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
premisou	premisa	k1gFnSc7
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
důsledku	důsledek	k1gInSc6
industrializace	industrializace	k1gFnSc2
se	se	k3xPyFc4
země	zem	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
podobat	podobat	k5eAaImF
z	z	k7c2
hlediska	hledisko	k1gNnSc2
společenských	společenský	k2eAgFnPc2d1
norem	norma	k1gFnPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Důsledkem	důsledek	k1gInSc7
teorie	teorie	k1gFnSc2
konvergence	konvergence	k1gFnSc2
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
vznik	vznik	k1gInSc4
unifikované	unifikovaný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
málo	málo	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c7
zeměmi	zem	k1gFnPc7
velké	velký	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
a	a	k8xC
náboženské	náboženský	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
také	také	k9
uvádí	uvádět	k5eAaImIp3nS
tzv.	tzv.	kA
“	“	k?
<g/>
catch	catch	k1gInSc1
up	up	k?
<g/>
”	”	k?
efekt	efekt	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
že	že	k8xS
méně	málo	k6eAd2
vyspělé	vyspělý	k2eAgFnPc1d1
země	zem	k1gFnPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
vyvíjet	vyvíjet	k5eAaImF
rychleji	rychle	k6eAd2
v	v	k7c6
důsledku	důsledek	k1gInSc6
přístupu	přístup	k1gInSc2
k	k	k7c3
informacím	informace	k1gFnPc3
a	a	k8xC
technologiím	technologie	k1gFnPc3
vyspělejších	vyspělý	k2eAgFnPc2d2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pojem	pojem	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
,	,	kIx,
politologii	politologie	k1gFnSc6
a	a	k8xC
sociologii	sociologie	k1gFnSc6
začal	začít	k5eAaPmAgInS
vyskytovat	vyskytovat	k5eAaImF
od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
o	o	k7c6
společenské	společenský	k2eAgFnSc6d1
konvergenci	konvergence	k1gFnSc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
tvrdily	tvrdit	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
rozvojové	rozvojový	k2eAgFnPc1d1
země	zem	k1gFnPc1
budou	být	k5eAaImBp3nP
ekonomicky	ekonomicky	k6eAd1
růst	růst	k5eAaImF
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
před	před	k7c7
nimi	on	k3xPp3gInPc7
země	zem	k1gFnPc1
rozvinuté	rozvinutý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definoval	definovat	k5eAaBmAgMnS
ho	on	k3xPp3gMnSc4
však	však	k9
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
právě	právě	k9
Clark	Clark	k1gInSc1
Kerr	Kerra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Náznaky	náznak	k1gInPc1
teorie	teorie	k1gFnSc2
konvergence	konvergence	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
ale	ale	k8xC
sledovat	sledovat	k5eAaImF
již	již	k6eAd1
v	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
hospodářským	hospodářský	k2eAgInSc7d1
dirigismem	dirigismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
faktory	faktor	k1gInPc7
podporujícími	podporující	k2eAgInPc7d1
tuto	tento	k3xDgFnSc4
myšlenku	myšlenka	k1gFnSc4
bylo	být	k5eAaImAgNnS
zbrojení	zbrojení	k1gNnSc1
a	a	k8xC
válečná	válečná	k1gFnSc1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
napomáhaly	napomáhat	k5eAaImAgFnP,k5eAaBmAgFnP
překonání	překonání	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
<g/>
,	,	kIx,
politických	politický	k2eAgFnPc2d1
a	a	k8xC
ideologických	ideologický	k2eAgFnPc2d1
odlišností	odlišnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
země	zem	k1gFnPc1
s	s	k7c7
rozdílným	rozdílný	k2eAgNnSc7d1
společenským	společenský	k2eAgNnSc7d1
uspořádáním	uspořádání	k1gNnSc7
mohou	moct	k5eAaImIp3nP
spolupracovat	spolupracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
významnější	významný	k2eAgFnSc4d2
koncepci	koncepce	k1gFnSc4
blízkou	blízký	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
konvergence	konvergence	k1gFnSc2
však	však	k9
vytvořil	vytvořit	k5eAaPmAgMnS
P.	P.	kA
A.	A.	kA
Sorokin	Sorokin	k1gMnSc1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
USA	USA	kA
and	and	k?
Russia	Russia	k1gFnSc1
pojednával	pojednávat	k5eAaImAgInS
o	o	k7c6
podobnostech	podobnost	k1gFnPc6
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k8xC,k8xS
geografických	geografický	k2eAgInPc2d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
kulturních	kulturní	k2eAgFnPc2d1
a	a	k8xC
velmocenských	velmocenský	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
vzniklé	vzniklý	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
si	se	k3xPyFc3
všímaly	všímat	k5eAaImAgFnP
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
země	zem	k1gFnPc1
po	po	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgInPc4d1
válce	válec	k1gInPc4
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
mnohém	mnohé	k1gNnSc6
sbližovat	sbližovat	k5eAaImF
i	i	k9
přes	přes	k7c4
zachování	zachování	k1gNnSc4
svých	svůj	k3xOyFgInPc2
specifických	specifický	k2eAgInPc2d1
rysů	rys	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
dobách	doba	k1gFnPc6
napadána	napadán	k2eAgFnSc1d1
kvůli	kvůli	k7c3
jejímu	její	k3xOp3gInSc3
determinismu	determinismus	k1gInSc3
<g/>
,	,	kIx,
příklonu	příklon	k1gInSc6
k	k	k7c3
Západnímu	západní	k2eAgNnSc3d1
myšlení	myšlení	k1gNnSc3
či	či	k8xC
zanedbání	zanedbání	k1gNnSc3
role	role	k1gFnSc2
rozvojových	rozvojový	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
ve	v	k7c6
světové	světový	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
však	však	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
posměšky	posměšek	k1gInPc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
vážné	vážný	k2eAgFnPc4d1
vědecké	vědecký	k2eAgFnPc4d1
studie	studie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
teorie	teorie	k1gFnSc1
kritizována	kritizovat	k5eAaImNgFnS
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
status	status	k1gInSc4
rozvinuté	rozvinutý	k2eAgFnSc2d1
země	zem	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
cílem	cíl	k1gInSc7
zemí	zem	k1gFnPc2
rozvojových	rozvojový	k2eAgFnPc2d1
a	a	k8xC
nedbá	dbát	k5eNaImIp3nS,k5eAaImIp3nS
přitom	přitom	k6eAd1
ohledu	ohled	k1gInSc2
na	na	k7c4
negativní	negativní	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
čistě	čistě	k6eAd1
ekonomicky	ekonomicky	k6eAd1
zaměřeného	zaměřený	k2eAgInSc2d1
modelu	model	k1gInSc2
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgInSc1
model	model	k1gInSc1
totiž	totiž	k9
může	moct	k5eAaImIp3nS
dle	dle	k7c2
některých	některý	k3yIgMnPc2
akademiků	akademik	k1gMnPc2
rozevřít	rozevřít	k5eAaPmF
nůžky	nůžky	k1gFnPc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
k	k	k7c3
dosažení	dosažení	k1gNnSc3
cíle	cíl	k1gInSc2
také	také	k9
nepřiměřeně	přiměřeně	k6eNd1
vyčerpává	vyčerpávat	k5eAaImIp3nS
přírodní	přírodní	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
a	a	k8xC
ničí	ničit	k5eAaImIp3nS
přírodní	přírodní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konvergence	konvergence	k1gFnSc1
a	a	k8xC
divergence	divergence	k1gFnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
teorii	teorie	k1gFnSc3
konvergence	konvergence	k1gFnSc2
snažili	snažit	k5eAaImAgMnP
přeformulovat	přeformulovat	k5eAaPmF
Inkeles	Inkeles	k1gInSc4
a	a	k8xC
Sirowy	Sirowa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inkeles	Inkeles	k1gMnSc1
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
dřívějších	dřívější	k2eAgNnPc6d1
pojednáních	pojednání	k1gNnPc6
o	o	k7c4
teorii	teorie	k1gFnSc4
nebyly	být	k5eNaImAgInP
rozlišeny	rozlišen	k2eAgInPc1d1
různé	různý	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každá	každý	k3xTgFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vyvíjet	vyvíjet	k5eAaImF
nejen	nejen	k6eAd1
jinou	jiný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
jiným	jiný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společenský	společenský	k2eAgInSc1d1
systém	systém	k1gInSc1
tedy	tedy	k9
navrhl	navrhnout	k5eAaPmAgInS
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
nejméně	málo	k6eAd3
5	#num#	k4
aspektů	aspekt	k1gInPc2
<g/>
,	,	kIx,
rozlišil	rozlišit	k5eAaPmAgMnS
také	také	k9
formy	forma	k1gFnPc4
konvergence	konvergence	k1gFnSc2
a	a	k8xC
divergence	divergence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konvergence	konvergence	k1gFnSc1
</s>
<s>
jednoduchá	jednoduchý	k2eAgFnSc1d1
konvergence	konvergence	k1gFnSc1
<g/>
,	,	kIx,
přechod	přechod	k1gInSc1
z	z	k7c2
rozdílnosti	rozdílnost	k1gFnSc2
do	do	k7c2
jednoty	jednota	k1gFnSc2
</s>
<s>
konvergence	konvergence	k1gFnSc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
směrů	směr	k1gInPc2
ke	k	k7c3
společnému	společný	k2eAgInSc3d1
cíli	cíl	k1gInSc3
<g/>
,	,	kIx,
při	při	k7c6
této	tento	k3xDgFnSc6
konvergenci	konvergence	k1gFnSc6
se	se	k3xPyFc4
společnosti	společnost	k1gFnPc1
v	v	k7c6
některých	některý	k3yIgFnPc6
věcech	věc	k1gFnPc6
přibližují	přibližovat	k5eAaImIp3nP
a	a	k8xC
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oddalují	oddalovat	k5eAaImIp3nP
</s>
<s>
konvergence	konvergence	k1gFnSc1
pomocí	pomocí	k7c2
překračování	překračování	k1gNnSc2
hranic	hranice	k1gFnPc2
spíše	spíše	k9
než	než	k8xS
změnou	změna	k1gFnSc7
v	v	k7c6
největších	veliký	k2eAgInPc6d3
rozdílech	rozdíl	k1gInPc6
</s>
<s>
divergentní	divergentní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ke	k	k7c3
konvergenci	konvergence	k1gFnSc3
<g/>
,	,	kIx,
rozdílné	rozdílný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
vedou	vést	k5eAaImIp3nP
ke	k	k7c3
stejnému	stejný	k2eAgInSc3d1
výsledku	výsledek	k1gInSc3
a	a	k8xC
poté	poté	k6eAd1
tedy	tedy	k9
ke	k	k7c3
konvergenci	konvergence	k1gFnSc3
</s>
<s>
konvergence	konvergence	k1gFnSc1
ve	v	k7c6
formě	forma	k1gFnSc6
paralelní	paralelní	k2eAgFnSc2d1
změny	změna	k1gFnSc2
</s>
<s>
Divergence	divergence	k1gFnSc1
</s>
<s>
jednoduchá	jednoduchý	k2eAgFnSc1d1
divergence	divergence	k1gFnSc1
<g/>
,	,	kIx,
opak	opak	k1gInSc1
jednoduché	jednoduchý	k2eAgFnSc2d1
konvergence	konvergence	k1gFnSc2
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc2
se	se	k3xPyFc4
ještě	ještě	k9
více	hodně	k6eAd2
vzdalují	vzdalovat	k5eAaImIp3nP
a	a	k8xC
rozdíly	rozdíl	k1gInPc1
se	se	k3xPyFc4
zvětšují	zvětšovat	k5eAaImIp3nP
</s>
<s>
divergence	divergence	k1gFnSc1
se	s	k7c7
společným	společný	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
společnosti	společnost	k1gFnPc1
potkají	potkat	k5eAaPmIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
zase	zase	k9
vzdalují	vzdalovat	k5eAaImIp3nP
</s>
<s>
konvergentní	konvergentní	k2eAgInPc1d1
trendy	trend	k1gInPc1
maskující	maskující	k2eAgFnSc4d1
divergenci	divergence	k1gFnSc4
</s>
<s>
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
Inkeles	Inkeles	k1gMnSc1
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
pečlivě	pečlivě	k6eAd1
zvolit	zvolit	k5eAaPmF
formu	forma	k1gFnSc4
a	a	k8xC
hloubku	hloubka	k1gFnSc4
analýzy	analýza	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
časový	časový	k2eAgInSc4d1
úsek	úsek	k1gInSc4
ke	k	k7c3
zkoumání	zkoumání	k1gNnSc3
konvergence	konvergence	k1gFnSc2
či	či	k8xC
divergence	divergence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Clark	Clark	k1gInSc1
Kerr	Kerr	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
T.	T.	kA
Dunlop	Dunlop	k1gInSc1
<g/>
,	,	kIx,
Frederick	Frederick	k1gMnSc1
Harbison	Harbison	k1gMnSc1
a	a	k8xC
Charles	Charles	k1gMnSc1
A.	A.	kA
Myers	Myers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Industrialism	Industrialism	k1gMnSc1
and	and	k?
industrial	industrial	k1gMnSc1
man	man	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
problems	problems	k6eAd1
of	of	k?
labour	labour	k1gMnSc1
and	and	k?
management	management	k1gInSc1
in	in	k?
economic	economic	k1gMnSc1
growth	growth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portsmouth	Portsmouth	k1gInSc1
<g/>
:	:	kIx,
Heinemann	Heinemann	k1gInSc1
Educational	Educational	k1gFnSc2
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Inkeles	Inkeles	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
&	&	k?
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
H.	H.	kA
Becoming	Becoming	k1gInSc1
modern	modern	k1gInSc1
<g/>
:	:	kIx,
Individual	Individual	k1gInSc1
change	change	k1gFnSc2
in	in	k?
six	six	k?
developing	developing	k1gInSc1
countries	countries	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
MA	MA	kA
<g/>
,	,	kIx,
US	US	kA
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Skinner	Skinner	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
Technological	Technological	k1gMnSc1
Determinism	Determinism	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Critique	Critique	k1gNnPc2
of	of	k?
Convergence	Convergence	k1gFnSc2
Theory	Theora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comparative	Comparativ	k1gInSc5
Studies	Studies	k1gInSc4
in	in	k?
Society	societa	k1gFnSc2
and	and	k?
History	Histor	k1gMnPc7
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sorokin	Sorokin	k1gMnSc1
<g/>
,	,	kIx,
Pitirim	Pitirim	k1gMnSc1
A.	A.	kA
Mutual	Mutual	k1gMnSc1
Convergence	Convergence	k1gFnSc2
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
and	and	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
<g/>
S.	S.	kA
<g/>
R.	R.	kA
to	ten	k3xDgNnSc1
the	the	k?
Mixed	Mixed	k1gInSc1
Sociocultural	Sociocultural	k1gMnSc1
Type	typ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
SAGE	SAGE	kA
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Kerr	Kerra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Convergence	Convergenec	k1gInSc2
Theory	Theora	k1gFnSc2
--	--	k?
What	What	k1gMnSc1
It	It	k1gMnSc1
Is	Is	k1gMnSc1
and	and	k?
How	How	k1gMnSc1
It	It	k1gMnSc1
Affects	Affectsa	k1gFnPc2
Nations	Nationsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ThoughtCo	ThoughtCo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
SOROKIN	SOROKIN	kA
<g/>
,	,	kIx,
Pitirim	Pitirim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mutual	Mutual	k1gInSc1
Convergence	Convergence	k1gFnSc2
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
and	and	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
<g/>
S.	S.	kA
<g/>
R.	R.	kA
to	ten	k3xDgNnSc1
the	the	k?
Mixed	Mixed	k1gInSc1
Sociocultural	Sociocultural	k1gMnSc1
Type	typ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
SAGE	SAGE	kA
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Convergence	Convergenec	k1gInSc2
Theories	Theoriesa	k1gFnPc2
|	|	kIx~
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.encyclopedia.com	www.encyclopedia.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
