<s>
Teorie	teorie	k1gFnSc1
konvergence	konvergence	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
convergentia	convergentia	k1gFnSc1
=	=	kIx~
směřování	směřování	k1gNnSc1
<g/>
,	,	kIx,
sklon	sklon	k1gInSc1
<g/>
,	,	kIx,
sbíhavost	sbíhavost	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
formulována	formulovat	k5eAaImNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
americkým	americký	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
Clarkem	Clarek	k1gMnSc7
Kerrem	Kerr	k1gMnSc7
z	z	k7c2
kalifornské	kalifornský	k2eAgFnSc2d1
Berkeley	Berkelea	k1gFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>