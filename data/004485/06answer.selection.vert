<s>
Bratři	bratr	k1gMnPc1	bratr
Adolf	Adolf	k1gMnSc1	Adolf
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dasslerové	Dasslerová	k1gFnSc2	Dasslerová
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
firmu	firma	k1gFnSc4	firma
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
sportovní	sportovní	k2eAgFnSc2d1	sportovní
obuvi	obuv	k1gFnSc2	obuv
Gebrüder	Gebrüder	k1gMnSc1	Gebrüder
Dassler	Dassler	k1gMnSc1	Dassler
Schuhfabrik	Schuhfabrik	k1gMnSc1	Schuhfabrik
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
trhu	trh	k1gInSc6	trh
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
