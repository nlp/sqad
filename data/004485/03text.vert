<s>
Adidas	Adidas	k1gInSc1	Adidas
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgInSc1d1	německý
koncern	koncern	k1gInSc1	koncern
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
bavorském	bavorský	k2eAgNnSc6d1	bavorské
městě	město	k1gNnSc6	město
Herzogenaurach	Herzogenauracha	k1gFnPc2	Herzogenauracha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přední	přední	k2eAgFnSc1d1	přední
světová	světový	k2eAgFnSc1d1	světová
firma	firma	k1gFnSc1	firma
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
sportovní	sportovní	k2eAgNnSc4d1	sportovní
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
obuv	obuv	k1gFnSc4	obuv
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc4d1	sportovní
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
firmy	firma	k1gFnSc2	firma
Adidas	Adidas	k1gInSc1	Adidas
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
černé	černý	k2eAgFnPc1d1	černá
proužky	proužka	k1gFnPc1	proužka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
případě	případ	k1gInSc6	případ
oblečení	oblečení	k1gNnSc2	oblečení
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
délku	délka	k1gFnSc4	délka
rukávů	rukáv	k1gInPc2	rukáv
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bot	bota	k1gFnPc2	bota
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
vnější	vnější	k2eAgFnSc4d1	vnější
boční	boční	k2eAgFnSc4d1	boční
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
převzala	převzít	k5eAaPmAgFnS	převzít
firma	firma	k1gFnSc1	firma
nové	nový	k2eAgNnSc4d1	nové
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Trefoil	Trefoil	k1gMnSc1	Trefoil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
trojlístek	trojlístek	k1gInSc1	trojlístek
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
Adolfa	Adolf	k1gMnSc2	Adolf
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
Adi	Adi	k1gFnSc1	Adi
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Dasslera	Dassler	k1gMnSc2	Dassler
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zkratek	zkratka	k1gFnPc2	zkratka
Adi	Adi	k1gFnSc4	Adi
Dassler	Dassler	k1gInSc4	Dassler
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
slovo	slovo	k1gNnSc1	slovo
Adidas	Adidas	k1gInSc1	Adidas
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Adolf	Adolf	k1gMnSc1	Adolf
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dasslerové	Dasslerová	k1gFnSc2	Dasslerová
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
firmu	firma	k1gFnSc4	firma
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
sportovní	sportovní	k2eAgFnSc2d1	sportovní
obuvi	obuv	k1gFnSc2	obuv
Gebrüder	Gebrüder	k1gMnSc1	Gebrüder
Dassler	Dassler	k1gMnSc1	Dassler
Schuhfabrik	Schuhfabrik	k1gMnSc1	Schuhfabrik
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
trhu	trh	k1gInSc6	trh
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
rozešli	rozejít	k5eAaPmAgMnP	rozejít
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
založil	založit	k5eAaPmAgMnS	založit
opět	opět	k6eAd1	opět
firmu	firma	k1gFnSc4	firma
vyrábějící	vyrábějící	k2eAgFnSc4d1	vyrábějící
sportovní	sportovní	k2eAgFnSc4d1	sportovní
obuv	obuv	k1gFnSc4	obuv
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
zprvu	zprvu	k6eAd1	zprvu
podle	podle	k7c2	podle
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
písmen	písmeno	k1gNnPc2	písmeno
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
Ruda	ruda	k1gFnSc1	ruda
(	(	kIx(	(
<g/>
RUdolf	Rudolf	k1gMnSc1	Rudolf
DAssler	DAssler	k1gMnSc1	DAssler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
firmu	firma	k1gFnSc4	firma
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Puma	puma	k1gFnSc1	puma
Schuhfabrik	Schuhfabrik	k1gMnSc1	Schuhfabrik
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dassler	Dassler	k1gMnSc1	Dassler
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
postupoval	postupovat	k5eAaImAgMnS	postupovat
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
(	(	kIx(	(
<g/>
Adi	Adi	k1gMnSc1	Adi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Adidas	Adidasa	k1gFnPc2	Adidasa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
výroby	výroba	k1gFnSc2	výroba
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
oděvních	oděvní	k2eAgFnPc2d1	oděvní
součástí	součást	k1gFnPc2	součást
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgFnPc4d1	sportovní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Adidas	Adidasa	k1gFnPc2	Adidasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Adidas	Adidasa	k1gFnPc2	Adidasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
firmy	firma	k1gFnSc2	firma
České	český	k2eAgFnSc2d1	Česká
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
firmy	firma	k1gFnSc2	firma
</s>
