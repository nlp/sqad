<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Maria	Maria	k1gFnSc1	Maria
Salomea	Salomea	k1gFnSc1	Salomea
Skłodowska	Skłodowska	k1gFnSc1	Skłodowska
<g/>
,	,	kIx,	,
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
Maria	Maria	k1gFnSc1	Maria
Skłodowska-Curie	Skłodowska-Curie	k1gFnSc1	Skłodowska-Curie
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1867	[number]	k4	1867
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
Passy	Passa	k1gFnSc2	Passa
<g/>
,	,	kIx,	,
Haute-Savoie	Haute-Savoie	k1gFnSc1	Haute-Savoie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vědkyně	vědkyně	k1gFnSc1	vědkyně
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávila	strávit	k5eAaPmAgFnS	strávit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
výzkumy	výzkum	k1gInPc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
největším	veliký	k2eAgInPc3d3	veliký
úspěchům	úspěch	k1gInPc3	úspěch
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
teorie	teorie	k1gFnSc1	teorie
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
technika	technik	k1gMnSc2	technik
dělení	dělení	k1gNnSc2	dělení
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
izotopů	izotop	k1gInPc2	izotop
objev	objev	k1gInSc4	objev
dvou	dva	k4xCgInPc2	dva
nových	nový	k2eAgInPc2d1	nový
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
polonia	polonium	k1gNnSc2	polonium
<g/>
.	.	kIx.	.
</s>

