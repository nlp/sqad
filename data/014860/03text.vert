<s>
Slonim	Slonim	k?
</s>
<s>
Slonim	Slonim	k?
С	С	k?
Chrám	chrám	k1gInSc1
sv.	sv.	kA
Andreje	Andrej	k1gMnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
156	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Slonim	Slonim	k?
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
46	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
50	#num#	k4
100	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
0	#num#	k4
<g/>
89,1	89,1	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1252	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.slonim.grodno-region.by/en/	www.slonim.grodno-region.by/en/	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
1562	#num#	k4
PSČ	PSČ	kA
</s>
<s>
231800	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Slonim	Slonim	k1gNnSc1
<g/>
,	,	kIx,
bělorusky	bělorusky	k6eAd1
С	С	k?
<g/>
́	́	k?
<g/>
н	н	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Hrodenské	Hrodenský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
soutoku	soutok	k1gInSc6
řek	řeka	k1gFnPc2
Ščara	Ščar	k1gInSc2
a	a	k8xC
Isa	Isa	k1gFnSc2
<g/>
,	,	kIx,
asi	asi	k9
170	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Minsku	Minsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
mělo	mít	k5eAaImAgNnS
49	#num#	k4
739	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
městě	město	k1gNnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1252	#num#	k4
<g/>
,	,	kIx,
středověká	středověký	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
tehdy	tehdy	k6eAd1
zaznamenala	zaznamenat	k5eAaPmAgFnS
název	název	k1gInSc4
Uslonim	Uslonima	k1gFnPc2
a	a	k8xC
hovořila	hovořit	k5eAaImAgFnS
o	o	k7c6
dřevěné	dřevěný	k2eAgFnSc6d1
pevnosti	pevnost	k1gFnSc6
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Ščary	Ščara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
město	město	k1gNnSc1
střídavě	střídavě	k6eAd1
patřilo	patřit	k5eAaImAgNnS
litevsko-polskému	litevsko-polský	k2eAgInSc3d1
státu	stát	k1gInSc3
a	a	k8xC
Kyjevské	kyjevský	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dělení	dělení	k1gNnSc6
Polska	Polsko	k1gNnSc2
připadlo	připadnout	k5eAaPmAgNnS
Rusku	Ruska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
města	město	k1gNnSc2
přičinil	přičinit	k5eAaPmAgMnS
šlechtic	šlechtic	k1gMnSc1
Michał	Michał	k1gMnSc2
Kazimierz	Kazimierz	k1gMnSc1
Ogiński	Ogińsk	k1gFnPc4
<g/>
,	,	kIx,
především	především	k6eAd1
vybudováním	vybudování	k1gNnSc7
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
oblast	oblast	k1gFnSc4
napojil	napojit	k5eAaPmAgMnS
na	na	k7c4
řeku	řeka	k1gFnSc4
Dněpr	Dněpr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
město	město	k1gNnSc4
ztratili	ztratit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gMnSc4
dobyli	dobýt	k5eAaPmAgMnP
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
o	o	k7c4
město	město	k1gNnSc4
bojovali	bojovat	k5eAaImAgMnP
Sověti	Sovět	k1gMnPc1
a	a	k8xC
Poláci	Polák	k1gMnPc1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
dle	dle	k7c2
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
z	z	k7c2
Rigy	Riga	k1gFnSc2
připadlo	připadnout	k5eAaPmAgNnS
Polsku	Polska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1939	#num#	k4
Sověti	Sovět	k1gMnPc1
uzavřeli	uzavřít	k5eAaPmAgMnP
s	s	k7c7
Němci	Němec	k1gMnPc7
smlouvu	smlouva	k1gFnSc4
Molotov	Molotov	k1gInSc4
<g/>
–	–	k?
<g/>
Ribbentrop	Ribbentrop	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
si	se	k3xPyFc3
rozdělili	rozdělit	k5eAaPmAgMnP
poražené	poražený	k2eAgNnSc4d1
Polsko	Polsko	k1gNnSc4
a	a	k8xC
Slonim	Slonim	k?
spadl	spadnout	k5eAaPmAgMnS
do	do	k7c2
sovětské	sovětský	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sověti	Sovět	k1gMnPc1
ho	on	k3xPp3gNnSc4
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
součástí	součást	k1gFnSc7
Běloruské	běloruský	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
roku	rok	k1gInSc2
1941	#num#	k4
Němci	Němec	k1gMnPc7
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
přepadli	přepadnout	k5eAaPmAgMnP
a	a	k8xC
město	město	k1gNnSc4
dobyli	dobýt	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
tragické	tragický	k2eAgNnSc1d1
zejména	zejména	k9
pro	pro	k7c4
velkou	velký	k2eAgFnSc4d1
židovskou	židovský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Židé	Žid	k1gMnPc1
usazovali	usazovat	k5eAaImAgMnP
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1388	#num#	k4
na	na	k7c4
pozvání	pozvání	k1gNnSc4
litevské	litevský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tak	tak	k6eAd1
chtěla	chtít	k5eAaImAgFnS
oživit	oživit	k5eAaPmF
obchod	obchod	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
zde	zde	k6eAd1
v	v	k7c4
jediný	jediný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
povraždili	povraždit	k5eAaPmAgMnP
9000	#num#	k4
Židů	Žid	k1gMnPc2
a	a	k8xC
další	další	k2eAgFnSc1d1
masová	masový	k2eAgFnSc1d1
vražda	vražda	k1gFnSc1
(	(	kIx(
<g/>
8000	#num#	k4
Židů	Žid	k1gMnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sověti	Sovět	k1gMnPc1
město	město	k1gNnSc4
dobyli	dobýt	k5eAaPmAgMnP
zpět	zpět	k6eAd1
roku	rok	k1gInSc2
1944	#num#	k4
a	a	k8xC
již	již	k9
zůstalo	zůstat	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
Běloruska	Bělorusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
Michael	Michael	k1gMnSc1
a	a	k8xC
Efraim	Efraim	k1gMnSc1
Marksovi	Marksův	k2eAgMnPc1d1
<g/>
,	,	kIx,
zakladatelé	zakladatel	k1gMnPc1
britské	britský	k2eAgFnSc2d1
oděvní	oděvní	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Marks	Marksa	k1gFnPc2
&	&	k?
Spencer	Spencer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Slonim	Slonim	k?
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
population	population	k1gInSc1
as	as	k1gInSc1
of	of	k?
January	Januara	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
and	and	k?
the	the	k?
average	average	k6eAd1
annual	annuat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
population	population	k1gInSc1
for	forum	k1gNnPc2
2019	#num#	k4
in	in	k?
the	the	k?
Republic	Republice	k1gFnPc2
of	of	k?
Belarus	Belarus	k1gMnSc1
by	by	kYmCp3nS
regions	regions	k6eAd1
<g/>
,	,	kIx,
districts	districts	k1gInSc1
<g/>
,	,	kIx,
cities	cities	k1gInSc1
and	and	k?
urban-type	urban-typ	k1gInSc5
settlements	settlementsa	k1gFnPc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Slonim	Slonim	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Heslo	heslo	k1gNnSc1
v	v	k7c6
The	The	k1gFnSc6
New	New	k1gFnPc2
International	International	k1gMnSc2
Encyclopæ	Encyclopæ	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bělorusko	Bělorusko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134492	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4333685-1	4333685-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83144326	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
138413854	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83144326	#num#	k4
</s>
