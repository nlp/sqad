<s>
Genitální	genitální	k2eAgFnSc1d1	genitální
bradavice	bradavice	k1gFnSc1	bradavice
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
kondylom	kondylom	k1gInSc1	kondylom
(	(	kIx(	(
<g/>
pl.	pl.	k?	pl.
kondylomy	kondylom	k1gInPc4	kondylom
r.	r.	kA	r.
<g/>
m.	m.	k?	m.
nebo	nebo	k8xC	nebo
kondylomata	kondyloma	k1gNnPc4	kondyloma
r.	r.	kA	r.
<g/>
s.	s.	k?	s.
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
condylomata	condyloma	k1gNnPc1	condyloma
acuminata	acumine	k1gNnPc1	acumine
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
venerické	venerický	k2eAgFnPc1d1	venerická
bradavice	bradavice	k1gFnPc1	bradavice
<g/>
,	,	kIx,	,
anální	anální	k2eAgFnPc1d1	anální
bradavice	bradavice	k1gFnPc1	bradavice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
anogenitální	anogenitální	k2eAgFnSc1d1	anogenitální
bradavice	bradavice	k1gFnSc1	bradavice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
nakažlivou	nakažlivý	k2eAgFnSc4d1	nakažlivá
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
infekcí	infekce	k1gFnSc7	infekce
<g/>
,	,	kIx,	,
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
některým	některý	k3yIgMnSc7	některý
z	z	k7c2	z
podtypů	podtyp	k1gInPc2	podtyp
lidského	lidský	k2eAgInSc2d1	lidský
papilomaviru	papilomavir	k1gInSc2	papilomavir
(	(	kIx(	(
<g/>
HPV	HPV	kA	HPV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
během	během	k7c2	během
orálního	orální	k2eAgInSc2d1	orální
<g/>
,	,	kIx,	,
genitálního	genitální	k2eAgInSc2d1	genitální
či	či	k8xC	či
análního	anální	k2eAgInSc2d1	anální
sexu	sex	k1gInSc2	sex
s	s	k7c7	s
infikovaným	infikovaný	k2eAgMnSc7d1	infikovaný
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnPc1	bradavice
jsou	být	k5eAaImIp3nP	být
nejsnadněji	snadno	k6eAd3	snadno
rozpoznatelným	rozpoznatelný	k2eAgInSc7d1	rozpoznatelný
příznakem	příznak	k1gInSc7	příznak
genitální	genitální	k2eAgFnSc2d1	genitální
HPV	HPV	kA	HPV
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
HPV	HPV	kA	HPV
kmeny	kmen	k1gInPc1	kmen
6	[number]	k4	6
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
,	,	kIx,	,
44	[number]	k4	44
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
,	,	kIx,	,
51	[number]	k4	51
<g/>
,	,	kIx,	,
52	[number]	k4	52
a	a	k8xC	a
54	[number]	k4	54
<g/>
;	;	kIx,	;
a	a	k8xC	a
za	za	k7c4	za
90	[number]	k4	90
%	%	kIx~	%
případů	případ	k1gInPc2	případ
genitálních	genitální	k2eAgFnPc2d1	genitální
bradavic	bradavice	k1gFnPc2	bradavice
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
HPV	HPV	kA	HPV
typy	typ	k1gInPc1	typ
6	[number]	k4	6
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
typy	typ	k1gInPc1	typ
HPV	HPV	kA	HPV
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
karcinom	karcinom	k1gInSc1	karcinom
děložního	děložní	k2eAgNnSc2d1	děložní
hrdla	hrdlo	k1gNnSc2	hrdlo
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
análních	anální	k2eAgInPc2d1	anální
karcinomů	karcinom	k1gInPc2	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Genitální	genitální	k2eAgFnSc1d1	genitální
HPV	HPV	kA	HPV
infekce	infekce	k1gFnPc1	infekce
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
odhadovanou	odhadovaný	k2eAgFnSc4d1	odhadovaná
prevalenci	prevalence	k1gFnSc4	prevalence
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
klinický	klinický	k2eAgInSc4d1	klinický
projev	projev	k1gInSc4	projev
u	u	k7c2	u
1	[number]	k4	1
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
sexuálně	sexuálně	k6eAd1	sexuálně
aktivní	aktivní	k2eAgFnSc2d1	aktivní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
%	%	kIx~	%
nakažených	nakažený	k2eAgMnPc2d1	nakažený
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
léčba	léčba	k1gFnSc1	léčba
může	moct	k5eAaImIp3nS	moct
bradavice	bradavice	k1gFnSc1	bradavice
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
neodstraní	odstranit	k5eNaPmIp3nS	odstranit
HPV	HPV	kA	HPV
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
léčbě	léčba	k1gFnSc6	léčba
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
vzniku	vznik	k1gInSc2	vznik
bradavic	bradavice	k1gFnPc2	bradavice
(	(	kIx(	(
<g/>
v	v	k7c6	v
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
jejich	jejich	k3xOp3gNnSc3	jejich
spontánnímu	spontánní	k2eAgNnSc3d1	spontánní
vymizení	vymizení	k1gNnSc3	vymizení
<g/>
.	.	kIx.	.
</s>
<s>
Genitální	genitální	k2eAgFnPc1d1	genitální
bradavice	bradavice	k1gFnPc1	bradavice
můžeme	moct	k5eAaImIp1nP	moct
obvykle	obvykle	k6eAd1	obvykle
identifikovat	identifikovat	k5eAaBmF	identifikovat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
lepší	lepšit	k5eAaImIp3nP	lepšit
zviditelnění	zviditelněný	k2eAgMnPc1d1	zviditelněný
lékaři	lékař	k1gMnPc1	lékař
při	při	k7c6	při
diagnostice	diagnostika	k1gFnSc6	diagnostika
používají	používat	k5eAaImIp3nP	používat
roztok	roztok	k1gInSc4	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
odlišení	odlišení	k1gNnSc1	odlišení
od	od	k7c2	od
zhoubného	zhoubný	k2eAgNnSc2d1	zhoubné
kožního	kožní	k2eAgNnSc2d1	kožní
bujení	bujení	k1gNnSc2	bujení
(	(	kIx(	(
<g/>
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
při	při	k7c6	při
nejisté	jistý	k2eNgFnSc6d1	nejistá
diagnóze	diagnóza	k1gFnSc6	diagnóza
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
odebrán	odebrat	k5eAaPmNgInS	odebrat
vzorek	vzorek	k1gInSc1	vzorek
tkáně	tkáň	k1gFnSc2	tkáň
k	k	k7c3	k
mikroskopickému	mikroskopický	k2eAgNnSc3d1	mikroskopické
vyšetření	vyšetření	k1gNnSc3	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
s	s	k7c7	s
bradavicemi	bradavice	k1gFnPc7	bradavice
na	na	k7c6	na
děložním	děložní	k2eAgNnSc6d1	děložní
hrdle	hrdlo	k1gNnSc6	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Odebrané	odebraný	k2eAgInPc1d1	odebraný
vzorky	vzorek	k1gInPc1	vzorek
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
odeslány	odeslat	k5eAaPmNgInP	odeslat
na	na	k7c4	na
HPV	HPV	kA	HPV
DNA	DNA	kA	DNA
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
typ	typ	k1gInSc4	typ
viru	vir	k1gInSc2	vir
HPV	HPV	kA	HPV
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
karcinomu	karcinom	k1gInSc2	karcinom
děložního	děložní	k2eAgInSc2d1	děložní
čípku	čípek	k1gInSc2	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
genitálních	genitální	k2eAgFnPc2d1	genitální
bradavic	bradavice	k1gFnPc2	bradavice
patří	patřit	k5eAaImIp3nS	patřit
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
nepoužívejte	používat	k5eNaImRp2nP	používat
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
léčbě	léčba	k1gFnSc3	léčba
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
přípravky	přípravka	k1gFnPc1	přípravka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
koupíte	koupit	k5eAaPmIp2nP	koupit
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
navštivte	navštívit	k5eAaPmRp2nP	navštívit
kožního	kožní	k2eAgMnSc4d1	kožní
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vám	vy	k3xPp2nPc3	vy
předepíše	předepsat	k5eAaPmIp3nS	předepsat
vhodná	vhodný	k2eAgNnPc4d1	vhodné
léčiva	léčivo	k1gNnPc4	léčivo
a	a	k8xC	a
doporučí	doporučit	k5eAaPmIp3nS	doporučit
vhodný	vhodný	k2eAgInSc4d1	vhodný
postup	postup	k1gInSc4	postup
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgInPc4d1	používaný
způsoby	způsob	k1gInPc4	způsob
léčby	léčba	k1gFnSc2	léčba
genitálních	genitální	k2eAgFnPc2d1	genitální
bradavic	bradavice	k1gFnPc2	bradavice
patří	patřit	k5eAaImIp3nP	patřit
přípravky	přípravek	k1gInPc1	přípravek
Aldara	Aldar	k1gMnSc2	Aldar
(	(	kIx(	(
<g/>
Imiquimod	Imiquimod	k1gInSc1	Imiquimod
<g/>
)	)	kIx)	)
a	a	k8xC	a
Condylox	Condylox	k1gInSc1	Condylox
<g/>
,	,	kIx,	,
Wartec	Wartec	k1gInSc1	Wartec
či	či	k8xC	či
Warticon	Warticon	k1gInSc1	Warticon
(	(	kIx(	(
<g/>
Podophylox	Podophylox	k1gInSc1	Podophylox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
krémů	krém	k1gInPc2	krém
se	se	k3xPyFc4	se
genitální	genitální	k2eAgFnPc1d1	genitální
bradavice	bradavice	k1gFnPc1	bradavice
také	také	k9	také
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kryochirurgie	kryochirurgie	k1gFnSc2	kryochirurgie
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
trichloroctové	trichloroctová	k1gFnSc2	trichloroctová
anebo	anebo	k8xC	anebo
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
elektrokauterizace	elektrokauterizace	k1gFnSc2	elektrokauterizace
<g/>
,	,	kIx,	,
laserové	laserový	k2eAgFnPc4d1	laserová
ablace	ablace	k1gFnPc4	ablace
či	či	k8xC	či
chirurgické	chirurgický	k2eAgFnPc4d1	chirurgická
excize	excize	k1gFnPc4	excize
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
interferony	interferon	k1gInPc4	interferon
<g/>
.	.	kIx.	.
</s>
