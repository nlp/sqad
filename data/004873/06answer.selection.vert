<s>
Genitální	genitální	k2eAgFnSc1d1	genitální
bradavice	bradavice	k1gFnSc1	bradavice
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
kondylom	kondylom	k1gInSc1	kondylom
(	(	kIx(	(
<g/>
pl.	pl.	k?	pl.
kondylomy	kondylom	k1gInPc4	kondylom
r.	r.	kA	r.
<g/>
m.	m.	k?	m.
nebo	nebo	k8xC	nebo
kondylomata	kondyloma	k1gNnPc4	kondyloma
r.	r.	kA	r.
<g/>
s.	s.	k?	s.
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
condylomata	condyloma	k1gNnPc1	condyloma
acuminata	acumine	k1gNnPc1	acumine
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
venerické	venerický	k2eAgFnPc1d1	venerická
bradavice	bradavice	k1gFnPc1	bradavice
<g/>
,	,	kIx,	,
anální	anální	k2eAgFnPc1d1	anální
bradavice	bradavice	k1gFnPc1	bradavice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
anogenitální	anogenitální	k2eAgFnSc1d1	anogenitální
bradavice	bradavice	k1gFnSc1	bradavice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
nakažlivou	nakažlivý	k2eAgFnSc4d1	nakažlivá
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
infekcí	infekce	k1gFnSc7	infekce
<g/>
,	,	kIx,	,
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
některým	některý	k3yIgMnSc7	některý
z	z	k7c2	z
podtypů	podtyp	k1gInPc2	podtyp
lidského	lidský	k2eAgInSc2d1	lidský
papilomaviru	papilomavir	k1gInSc2	papilomavir
(	(	kIx(	(
<g/>
HPV	HPV	kA	HPV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
