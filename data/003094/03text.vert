<s>
Kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
sešívačka	sešívačka	k1gFnSc1	sešívačka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgNnPc1d1	zvané
koník	koník	k1gMnSc1	koník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
drobné	drobný	k2eAgNnSc4d1	drobné
technické	technický	k2eAgNnSc4d1	technické
kancelářské	kancelářský	k2eAgNnSc4d1	kancelářské
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
spojování	spojování	k1gNnSc3	spojování
papírových	papírový	k2eAgFnPc2d1	papírová
tiskovin	tiskovina	k1gFnPc2	tiskovina
pomocí	pomocí	k7c2	pomocí
tenkých	tenký	k2eAgInPc2d1	tenký
drátků	drátek	k1gInPc2	drátek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
malou	malý	k2eAgFnSc4d1	malá
speciální	speciální	k2eAgFnSc4d1	speciální
ruční	ruční	k2eAgFnSc4d1	ruční
ohýbačku	ohýbačka	k1gFnSc4	ohýbačka
tenkých	tenký	k2eAgInPc2d1	tenký
ocelových	ocelový	k2eAgInPc2d1	ocelový
drátků	drátek	k1gInPc2	drátek
<g/>
.	.	kIx.	.
</s>
<s>
Sešívačka	sešívačka	k1gFnSc1	sešívačka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
základnu	základna	k1gFnSc4	základna
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
pružinový	pružinový	k2eAgInSc1d1	pružinový
zásobník	zásobník	k1gInSc1	zásobník
drátků	drátek	k1gInPc2	drátek
(	(	kIx(	(
<g/>
sponek	sponka	k1gFnPc2	sponka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
širokého	široký	k2eAgNnSc2d1	široké
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stlačením	stlačení	k1gNnSc7	stlačení
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
sešívačky	sešívačka	k1gFnSc2	sešívačka
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
jednoho	jeden	k4xCgInSc2	jeden
drátku	drátek	k1gInSc2	drátek
(	(	kIx(	(
<g/>
sponky	sponka	k1gFnSc2	sponka
<g/>
)	)	kIx)	)
ze	z	k7c2	z
zásobníku	zásobník	k1gInSc2	zásobník
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
posunu	posun	k1gInSc3	posun
do	do	k7c2	do
vodicí	vodicí	k2eAgFnSc2d1	vodicí
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgInPc1d1	ostrý
konce	konec	k1gInPc1	konec
drátků	drátek	k1gInPc2	drátek
proseknou	proseknout	k5eAaPmIp3nP	proseknout
resp.	resp.	kA	resp.
propíchnou	propíchnout	k5eAaPmIp3nP	propíchnout
spojované	spojovaný	k2eAgInPc1d1	spojovaný
papíry	papír	k1gInPc1	papír
<g/>
,	,	kIx,	,
nárazem	náraz	k1gInSc7	náraz
o	o	k7c4	o
pevnou	pevný	k2eAgFnSc4d1	pevná
spodní	spodní	k2eAgFnSc4d1	spodní
tvarovanou	tvarovaný	k2eAgFnSc4d1	tvarovaná
část	část	k1gFnSc4	část
zařízení	zařízení	k1gNnSc2	zařízení
se	s	k7c7	s
drátky	drátek	k1gInPc7	drátek
ohnou	ohnout	k5eAaPmIp3nP	ohnout
o	o	k7c4	o
90	[number]	k4	90
a	a	k8xC	a
více	hodně	k6eAd2	hodně
stupňů	stupeň	k1gInPc2	stupeň
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
papíry	papír	k1gInPc1	papír
tak	tak	k6eAd1	tak
mechanicky	mechanicky	k6eAd1	mechanicky
spojí	spojit	k5eAaPmIp3nP	spojit
-	-	kIx~	-
sešijí	sešít	k5eAaPmIp3nP	sešít
kovovým	kovový	k2eAgInSc7d1	kovový
drátkem	drátek	k1gInSc7	drátek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rychlou	rychlý	k2eAgFnSc4d1	rychlá
<g/>
,	,	kIx,	,
levnou	levný	k2eAgFnSc4d1	levná
<g/>
,	,	kIx,	,
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
a	a	k8xC	a
materiálově	materiálově	k6eAd1	materiálově
nenáročnou	náročný	k2eNgFnSc4d1	nenáročná
metodu	metoda	k1gFnSc4	metoda
spojováni	spojován	k2eAgMnPc1d1	spojován
tiskovin	tiskovina	k1gFnPc2	tiskovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
při	při	k7c6	při
strojním	strojní	k2eAgNnSc6d1	strojní
sešívání	sešívání	k1gNnSc6	sešívání
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
sešitů	sešit	k1gInPc2	sešit
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
složených	složený	k2eAgInPc2d1	složený
z	z	k7c2	z
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
papírových	papírový	k2eAgInPc2d1	papírový
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
i	i	k9	i
zpětně	zpětně	k6eAd1	zpětně
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
rozebiratelné	rozebiratelný	k2eAgNnSc1d1	rozebiratelné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typů	typ	k1gInPc2	typ
sešívaček	sešívačka	k1gFnPc2	sešívačka
lze	lze	k6eAd1	lze
sešívat	sešívat	k5eAaImF	sešívat
až	až	k9	až
200	[number]	k4	200
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Rozešívačka	Rozešívačka	k1gFnSc1	Rozešívačka
</s>
