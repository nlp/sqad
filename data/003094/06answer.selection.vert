<s>
Kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
sešívačka	sešívačka	k1gFnSc1	sešívačka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgNnPc1d1	zvané
koník	koník	k1gMnSc1	koník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
drobné	drobný	k2eAgNnSc4d1	drobné
technické	technický	k2eAgNnSc4d1	technické
kancelářské	kancelářský	k2eAgNnSc4d1	kancelářské
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
spojování	spojování	k1gNnSc3	spojování
papírových	papírový	k2eAgFnPc2d1	papírová
tiskovin	tiskovina	k1gFnPc2	tiskovina
pomocí	pomocí	k7c2	pomocí
tenkých	tenký	k2eAgInPc2d1	tenký
drátků	drátek	k1gInPc2	drátek
<g/>
.	.	kIx.	.
</s>
