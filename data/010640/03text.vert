<p>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1980	[number]	k4	1980
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
výprava	výprava	k1gFnSc1	výprava
75	[number]	k4	75
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
57	[number]	k4	57
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
18	[number]	k4	18
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
10	[number]	k4	10
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailisté	medailista	k1gMnPc5	medailista
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
na	na	k7c4	na
LOH	LOH	kA	LOH
1980	[number]	k4	1980
</s>
</p>
