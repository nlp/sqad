<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
je	být	k5eAaImIp3nS	být
etapový	etapový	k2eAgInSc1d1	etapový
závod	závod	k1gInSc1	závod
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
cyklistů	cyklista	k1gMnPc2	cyklista
trvající	trvající	k2eAgMnSc1d1	trvající
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
ale	ale	k9	ale
zajíždí	zajíždět	k5eAaImIp3nS	zajíždět
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
pořádání	pořádání	k1gNnSc4	pořádání
přerušila	přerušit	k5eAaPmAgFnS	přerušit
pouze	pouze	k6eAd1	pouze
první	první	k4xOgMnSc1	první
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xS	jako
propagační	propagační	k2eAgInSc1d1	propagační
závod	závod	k1gInSc1	závod
novin	novina	k1gFnPc2	novina
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Auto	auto	k1gNnSc1	auto
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešního	dnešní	k2eAgInSc2d1	dnešní
listu	list	k1gInSc2	list
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Equipe	Equip	k1gInSc5	Equip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
vydavatelem	vydavatel	k1gMnSc7	vydavatel
byl	být	k5eAaImAgMnS	být
Henrim	Henrim	k1gMnSc1	Henrim
Desgrangem	Desgrang	k1gInSc7	Desgrang
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
konkurencí	konkurence	k1gFnSc7	konkurence
pro	pro	k7c4	pro
závod	závod	k1gInSc4	závod
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Brest	Brest	k1gInSc1	Brest
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
sponzorovaný	sponzorovaný	k2eAgInSc1d1	sponzorovaný
listem	list	k1gInSc7	list
Le	Le	k1gFnSc2	Le
Petit	petit	k1gInSc1	petit
Journal	Journal	k1gFnSc4	Journal
a	a	k8xC	a
závod	závod	k1gInSc4	závod
(	(	kIx(	(
<g/>
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
-	-	kIx~	-
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
sponzorovaný	sponzorovaný	k2eAgInSc1d1	sponzorovaný
novinami	novina	k1gFnPc7	novina
Le	Le	k1gMnSc1	Le
Vélo	Vélo	k1gMnSc1	Vélo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
vytrvalostní	vytrvalostní	k2eAgInSc4d1	vytrvalostní
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
spali	spát	k5eAaImAgMnP	spát
kolem	kolem	k7c2	kolem
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
přijímat	přijímat	k5eAaImF	přijímat
cizí	cizí	k2eAgFnSc4d1	cizí
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Tour	Tour	k1gInSc1	Tour
etapový	etapový	k2eAgInSc1d1	etapový
závod	závod	k1gInSc4	závod
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
etap	etapa	k1gFnPc2	etapa
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
trvá	trvat	k5eAaImIp3nS	trvat
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Peloton	peloton	k1gInSc1	peloton
závodu	závod	k1gInSc2	závod
provází	provázet	k5eAaImIp3nS	provázet
několik	několik	k4yIc1	několik
doprovodných	doprovodný	k2eAgNnPc2d1	doprovodné
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
automobily	automobil	k1gInPc1	automobil
a	a	k8xC	a
motocykly	motocykl	k1gInPc1	motocykl
<g/>
)	)	kIx)	)
poskytujících	poskytující	k2eAgMnPc2d1	poskytující
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc1	pití
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
mechanikům	mechanik	k1gMnPc3	mechanik
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
vozidel	vozidlo	k1gNnPc2	vozidlo
patří	patřit	k5eAaImIp3nS	patřit
organizátorům	organizátor	k1gMnPc3	organizátor
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
jezdce	jezdec	k1gInPc4	jezdec
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
týmové	týmový	k2eAgNnSc1d1	týmové
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
etap	etapa	k1gFnPc2	etapa
vede	vést	k5eAaImIp3nS	vést
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
etapy	etapa	k1gFnPc1	etapa
zavítají	zavítat	k5eAaPmIp3nP	zavítat
i	i	k9	i
do	do	k7c2	do
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
nesousedících	sousedící	k2eNgFnPc2d1	sousedící
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
nebo	nebo	k8xC	nebo
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
trvající	trvající	k2eAgInSc1d1	trvající
závod	závod	k1gInSc1	závod
obvykle	obvykle	k6eAd1	obvykle
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
volna	volno	k1gNnSc2	volno
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
přesun	přesun	k1gInSc4	přesun
mezi	mezi	k7c7	mezi
etapami	etapa	k1gFnPc7	etapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
předchází	předcházet	k5eAaImIp3nS	předcházet
první	první	k4xOgFnSc6	první
etapě	etapa	k1gFnSc6	etapa
krátká	krátký	k2eAgFnSc1d1	krátká
časovka	časovka	k1gFnSc1	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
(	(	kIx(	(
<g/>
1	[number]	k4	1
-	-	kIx~	-
15	[number]	k4	15
km	km	kA	km
<g/>
)	)	kIx)	)
zvaná	zvaný	k2eAgFnSc1d1	zvaná
prolog	prolog	k1gInSc1	prolog
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
cíl	cíl	k1gInSc1	cíl
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
bulváru	bulvár	k1gInSc6	bulvár
Avenue	avenue	k1gFnSc4	avenue
des	des	k1gNnSc2	des
Champs-Élysées	Champs-Élyséesa	k1gFnPc2	Champs-Élyséesa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
závod	závod	k1gInSc1	závod
nabízí	nabízet	k5eAaImIp3nS	nabízet
etapy	etapa	k1gFnSc2	etapa
různého	různý	k2eAgInSc2d1	různý
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rovinaté	rovinatý	k2eAgFnPc1d1	rovinatá
etapy	etapa	k1gFnPc1	etapa
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnPc1d1	horská
etapy	etapa	k1gFnPc1	etapa
<g/>
,	,	kIx,	,
časovky	časovka	k1gFnPc1	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
časovky	časovka	k1gFnSc2	časovka
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
celkovým	celkový	k2eAgMnSc7d1	celkový
vítězem	vítěz	k1gMnSc7	vítěz
obvykle	obvykle	k6eAd1	obvykle
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
vrchař	vrchař	k1gMnSc1	vrchař
nebo	nebo	k8xC	nebo
časovkář	časovkář	k1gMnSc1	časovkář
<g/>
.	.	kIx.	.
1903	[number]	k4	1903
až	až	k9	až
1939	[number]	k4	1939
-	-	kIx~	-
Henri	Henri	k1gNnSc6	Henri
Desgrange	Desgrang	k1gInSc2	Desgrang
1947	[number]	k4	1947
až	až	k9	až
1961	[number]	k4	1961
-	-	kIx~	-
Jacques	Jacques	k1gMnSc1	Jacques
Goddet	Goddet	k1gInSc4	Goddet
1962	[number]	k4	1962
až	až	k9	až
1987	[number]	k4	1987
-	-	kIx~	-
Jacques	Jacques	k1gMnSc1	Jacques
Goddet	Goddet	k1gMnSc1	Goddet
a	a	k8xC	a
Felix	Felix	k1gMnSc1	Felix
Lévitan	Lévitan	k1gInSc4	Lévitan
1988	[number]	k4	1988
-	-	kIx~	-
Jacques	Jacques	k1gMnSc1	Jacques
Goddet	Goddet	k1gMnSc1	Goddet
a	a	k8xC	a
Xavier	Xavier	k1gMnSc1	Xavier
Louy	Loua	k1gFnSc2	Loua
1989	[number]	k4	1989
až	až	k9	až
2005	[number]	k4	2005
-	-	kIx~	-
Jean-Marie	Jean-Marie	k1gFnSc1	Jean-Marie
Leblanc	Leblanc	k1gInSc1	Leblanc
2006	[number]	k4	2006
-	-	kIx~	-
Christian	Christian	k1gMnSc1	Christian
Prudhomme	Prudhomme	k1gMnSc1	Prudhomme
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgNnP	být
Tour	Tour	k1gInSc1	Tour
zkažena	zkazit	k5eAaPmNgFnS	zkazit
mnoha	mnoho	k4c7	mnoho
dopingovými	dopingový	k2eAgInPc7d1	dopingový
skandály	skandál	k1gInPc7	skandál
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
výjezdu	výjezd	k1gInSc6	výjezd
na	na	k7c4	na
Mont	Mont	k2eAgInSc4d1	Mont
Ventoux	Ventoux	k1gInSc4	Ventoux
britský	britský	k2eAgMnSc1d1	britský
cyklista	cyklista	k1gMnSc1	cyklista
Tom	Tom	k1gMnSc1	Tom
Simpson	Simpson	k1gMnSc1	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Pitva	pitva	k1gFnSc1	pitva
prokázala	prokázat	k5eAaPmAgFnS	prokázat
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
užívání	užívání	k1gNnSc4	užívání
amfetaminu	amfetamin	k1gInSc2	amfetamin
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
propukl	propuknout	k5eAaPmAgInS	propuknout
velký	velký	k2eAgInSc1d1	velký
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
když	když	k8xS	když
francouzští	francouzský	k2eAgMnPc1d1	francouzský
celníci	celník	k1gMnPc1	celník
zatkli	zatknout	k5eAaPmAgMnP	zatknout
Willyho	Willy	k1gMnSc4	Willy
Woeta	Woet	k1gMnSc4	Woet
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnSc4	lékař
cyklistického	cyklistický	k2eAgInSc2d1	cyklistický
týmu	tým	k1gInSc2	tým
Festina	Festin	k1gMnSc2	Festin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
hlavním	hlavní	k2eAgMnSc7d1	hlavní
jezdcem	jezdec	k1gMnSc7	jezdec
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Virenque	Virenqu	k1gInSc2	Virenqu
<g/>
,	,	kIx,	,
za	za	k7c4	za
držení	držení	k1gNnSc4	držení
nelegálního	legální	k2eNgNnSc2d1	nelegální
množství	množství	k1gNnSc2	množství
léků	lék	k1gInPc2	lék
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
a	a	k8xC	a
narkotik	narkotik	k1gMnSc1	narkotik
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
erytropoetinu	erytropoetin	k2eAgFnSc4d1	erytropoetin
(	(	kIx(	(
<g/>
EPO	EPO	kA	EPO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růstových	růstový	k2eAgInPc2d1	růstový
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
testosteronů	testosteron	k1gInPc2	testosteron
a	a	k8xC	a
amfetaminu	amfetamin	k1gInSc2	amfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Woet	Woet	k1gMnSc1	Woet
později	pozdě	k6eAd2	pozdě
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
mnoho	mnoho	k4c1	mnoho
praktik	praktikum	k1gNnPc2	praktikum
používaných	používaný	k2eAgNnPc2d1	používané
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Massacre	Massacr	k1gInSc5	Massacr
a	a	k8xC	a
la	la	k0	la
Chaîne	Chaîn	k1gInSc5	Chaîn
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Virenque	Virenqu	k1gInSc2	Virenqu
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
dopingu	doping	k1gInSc2	doping
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestliže	jestliže	k8xS	jestliže
dopoval	dopovat	k5eAaImAgMnS	dopovat
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
vědomě	vědomě	k6eAd1	vědomě
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
francouzská	francouzský	k2eAgFnSc1d1	francouzská
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
jednající	jednající	k2eAgMnSc1d1	jednající
dle	dle	k7c2	dle
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
domovní	domovní	k2eAgFnSc3d1	domovní
prohlídce	prohlídka	k1gFnSc3	prohlídka
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
důležité	důležitý	k2eAgNnSc4d1	důležité
množství	množství	k1gNnSc4	množství
dopingových	dopingový	k2eAgInPc2d1	dopingový
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
automobilech	automobil	k1gInPc6	automobil
stáje	stáj	k1gFnSc2	stáj
TVM	TVM	kA	TVM
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
byl	být	k5eAaImAgInS	být
zásadně	zásadně	k6eAd1	zásadně
narušen	narušit	k5eAaPmNgInS	narušit
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
týmy	tým	k1gInPc1	tým
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
odstoupily	odstoupit	k5eAaPmAgFnP	odstoupit
a	a	k8xC	a
jiné	jiná	k1gFnPc1	jiná
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
proti	proti	k7c3	proti
zákrokům	zákrok	k1gInPc3	zákrok
policie	policie	k1gFnSc2	policie
přerušením	přerušení	k1gNnSc7	přerušení
etap	etapa	k1gFnPc2	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byli	být	k5eAaImAgMnP	být
Richard	Richard	k1gMnSc1	Richard
Virenque	Virenqu	k1gFnSc2	Virenqu
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
stáje	stáj	k1gFnSc2	stáj
Festina	Festin	k2eAgFnSc1d1	Festina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
vyslýcháni	vyslýchat	k5eAaImNgMnP	vyslýchat
<g/>
.	.	kIx.	.
</s>
<s>
Lance	lance	k1gNnPc4	lance
Armstrong	Armstrong	k1gMnSc1	Armstrong
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
rekordně	rekordně	k6eAd1	rekordně
sedmkrát	sedmkrát	k6eAd1	sedmkrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
kariéry	kariéra	k1gFnSc2	kariéra
přišel	přijít	k5eAaPmAgInS	přijít
list	list	k1gInSc1	list
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Equipe	Equip	k1gInSc5	Equip
s	s	k7c7	s
obviněními	obvinění	k1gNnPc7	obvinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
nedovolenými	dovolený	k2eNgInPc7d1	nedovolený
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Armstrongovy	Armstrongův	k2eAgFnSc2d1	Armstrongova
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Američan	Američan	k1gMnSc1	Američan
Floyd	Floyd	k1gMnSc1	Floyd
Landis	Landis	k1gFnSc1	Landis
<g/>
.	.	kIx.	.
</s>
<s>
Dopingová	dopingový	k2eAgFnSc1d1	dopingová
kontrola	kontrola	k1gFnSc1	kontrola
ale	ale	k9	ale
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
vzorcích	vzorec	k1gInPc6	vzorec
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ředitelstvím	ředitelství	k1gNnSc7	ředitelství
TdF	TdF	k1gFnSc2	TdF
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nový	nový	k2eAgMnSc1d1	nový
vítěz	vítěz	k1gMnSc1	vítěz
-	-	kIx~	-
Oscar	Oscar	k1gInSc1	Oscar
Pereiro	Pereiro	k1gNnSc1	Pereiro
Sio	Sio	k1gFnPc2	Sio
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
druhý	druhý	k4xOgInSc1	druhý
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
už	už	k6eAd1	už
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
několik	několik	k4yIc1	několik
favoritů	favorit	k1gMnPc2	favorit
zapletených	zapletený	k2eAgMnPc2d1	zapletený
do	do	k7c2	do
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
dopingových	dopingový	k2eAgNnPc2d1	dopingové
obvinění	obvinění	k1gNnPc2	obvinění
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vítěze	vítěz	k1gMnSc2	vítěz
závodu	závod	k1gInSc2	závod
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
Jana	Jan	k1gMnSc2	Jan
Ullricha	Ullrich	k1gMnSc2	Ullrich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
doping	doping	k1gInSc1	doping
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
aspirantů	aspirant	k1gMnPc2	aspirant
na	na	k7c4	na
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vinokurov	Vinokurov	k1gInSc4	Vinokurov
měl	mít	k5eAaImAgMnS	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
výsledek	výsledek	k1gInSc4	výsledek
na	na	k7c4	na
krevní	krevní	k2eAgInSc4d1	krevní
doping	doping	k1gInSc4	doping
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
týmu	tým	k1gInSc2	tým
Astana	Astana	k1gFnSc1	Astana
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
TdF	TdF	k1gMnSc1	TdF
byl	být	k5eAaImAgMnS	být
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
Dán	Dán	k1gMnSc1	Dán
Michael	Michael	k1gMnSc1	Michael
Rasmussen	Rasmussen	k1gInSc4	Rasmussen
ze	z	k7c2	z
stáje	stáj	k1gFnSc2	stáj
Rabobank	Rabobanka	k1gFnPc2	Rabobanka
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
vyhazovu	vyhazov	k1gInSc2	vyhazov
byly	být	k5eAaImAgFnP	být
lživé	lživý	k2eAgFnPc1d1	lživá
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
tréninku	trénink	k1gInSc2	trénink
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
kontrolu	kontrola	k1gFnSc4	kontrola
dopingovým	dopingový	k2eAgMnSc7d1	dopingový
komisařům	komisař	k1gMnPc3	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Rasmussen	Rasmussen	k1gInSc1	Rasmussen
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jel	jet	k5eAaImAgMnS	jet
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
trikotu	trikot	k1gInSc6	trikot
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
1910	[number]	k4	1910
Francouz	Francouz	k1gMnSc1	Francouz
Adolphe	Adolph	k1gInSc2	Adolph
Heliè	Heliè	k1gFnPc2	Heliè
se	se	k3xPyFc4	se
utopil	utopit	k5eAaPmAgInS	utopit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volného	volný	k2eAgInSc2d1	volný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
Španěl	Španěl	k1gMnSc1	Španěl
Francisco	Francisco	k1gMnSc1	Francisco
Cepeda	Cepeda	k1gMnSc1	Cepeda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sjezdu	sjezd	k1gInSc2	sjezd
z	z	k7c2	z
Col	cola	k1gFnPc2	cola
du	du	k?	du
Galibier	Galibira	k1gFnPc2	Galibira
spadl	spadnout	k5eAaPmAgInS	spadnout
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
udeřil	udeřit	k5eAaPmAgMnS	udeřit
se	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
o	o	k7c4	o
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
Angličan	Angličan	k1gMnSc1	Angličan
Tom	Tom	k1gMnSc1	Tom
Simpson	Simpson	k1gMnSc1	Simpson
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
etapa	etapa	k1gFnSc1	etapa
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
selhání	selhání	k1gNnSc2	selhání
srdce	srdce	k1gNnSc4	srdce
při	při	k7c6	při
výšlapu	výšlap	k1gInSc6	výšlap
na	na	k7c4	na
Mont	Mont	k2eAgInSc4d1	Mont
Ventoux	Ventoux	k1gInSc4	Ventoux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
krvi	krev	k1gFnSc6	krev
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
amfetamin	amfetamin	k1gInSc1	amfetamin
i	i	k8xC	i
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
Ital	Ital	k1gMnSc1	Ital
Fabio	Fabio	k1gMnSc1	Fabio
Casartelli	Casartelle	k1gFnSc4	Casartelle
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
etapa	etapa	k1gFnSc1	etapa
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
upadl	upadnout	k5eAaPmAgMnS	upadnout
při	při	k7c6	při
sjezdu	sjezd	k1gInSc6	sjezd
z	z	k7c2	z
Col	cola	k1gFnPc2	cola
de	de	k?	de
Portet	Portet	k1gMnSc1	Portet
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aspet	Aspet	k1gMnSc1	Aspet
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
88	[number]	k4	88
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Casartelli	Casartell	k1gMnSc6	Casartell
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
přilbu	přilba	k1gFnSc4	přilba
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
etapách	etapa	k1gFnPc6	etapa
startují	startovat	k5eAaBmIp3nP	startovat
všichni	všechen	k3xTgMnPc1	všechen
jezdci	jezdec	k1gMnPc1	jezdec
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Cyklistům	cyklista	k1gMnPc3	cyklista
je	být	k5eAaImIp3nS	být
dovolen	dovolen	k2eAgInSc1d1	dovolen
kontakt	kontakt	k1gInSc1	kontakt
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
jet	jet	k5eAaImF	jet
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
(	(	kIx(	(
<g/>
v	v	k7c6	v
háku	hák	k1gInSc6	hák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jízda	jízda	k1gFnSc1	jízda
ve	v	k7c6	v
větrném	větrný	k2eAgInSc6d1	větrný
stínu	stín	k1gInSc6	stín
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pro	pro	k7c4	pro
taktiku	taktika	k1gFnSc4	taktika
<g/>
:	:	kIx,	:
osamocený	osamocený	k2eAgMnSc1d1	osamocený
jezdec	jezdec	k1gMnSc1	jezdec
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
dojetí	dojetí	k1gNnSc4	dojetí
skupiny	skupina	k1gFnSc2	skupina
cyklistů	cyklista	k1gMnPc2	cyklista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
střídat	střídat	k5eAaImF	střídat
v	v	k7c6	v
namáhavé	namáhavý	k2eAgFnSc6d1	namáhavá
pozici	pozice	k1gFnSc6	pozice
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jezdců	jezdec	k1gMnPc2	jezdec
tvoří	tvořit	k5eAaImIp3nS	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
peloton	peloton	k1gInSc4	peloton
<g/>
,	,	kIx,	,
s	s	k7c7	s
útočícími	útočící	k2eAgFnPc7d1	útočící
skupinami	skupina	k1gFnPc7	skupina
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
odpadajícími	odpadající	k2eAgFnPc7d1	odpadající
skupinami	skupina	k1gFnPc7	skupina
na	na	k7c6	na
chvostu	chvost	k1gInSc6	chvost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
etapách	etapa	k1gFnPc6	etapa
bývá	bývat	k5eAaImIp3nS	bývat
peloton	peloton	k1gInSc1	peloton
rozdrobený	rozdrobený	k2eAgInSc1d1	rozdrobený
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovinatých	rovinatý	k2eAgFnPc6d1	rovinatá
etapách	etapa	k1gFnPc6	etapa
jezdí	jezdit	k5eAaImIp3nP	jezdit
pospolu	pospolu	k6eAd1	pospolu
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
dorazí	dorazit	k5eAaPmIp3nP	dorazit
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
více	hodně	k6eAd2	hodně
jezdců	jezdec	k1gMnPc2	jezdec
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
nezrychlují	zrychlovat	k5eNaImIp3nP	zrychlovat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zlepšili	zlepšit	k5eAaPmAgMnP	zlepšit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
o	o	k7c4	o
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
totiž	totiž	k9	totiž
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jeden	jeden	k4xCgMnSc1	jeden
závodník	závodník	k1gMnSc1	závodník
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připsán	připsán	k2eAgInSc1d1	připsán
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jezdci	jezdec	k1gMnPc1	jezdec
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
platí	platit	k5eAaImIp3nS	platit
postupně	postupně	k6eAd1	postupně
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
jezdce	jezdec	k1gInPc4	jezdec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
když	když	k8xS	když
peloton	peloton	k1gInSc1	peloton
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgMnPc3	všecek
jezdcům	jezdec	k1gMnPc3	jezdec
připsán	připsán	k2eAgInSc4d1	připsán
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
jako	jako	k8xS	jako
vítězi	vítěz	k1gMnSc6	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
průjezd	průjezd	k1gInSc4	průjezd
pelotonu	peloton	k1gInSc2	peloton
přes	přes	k7c4	přes
cílovou	cílový	k2eAgFnSc4d1	cílová
pásku	páska	k1gFnSc4	páska
trvá	trvat	k5eAaImIp3nS	trvat
desítky	desítka	k1gFnSc2	desítka
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
probíhají	probíhat	k5eAaImIp3nP	probíhat
sprintérské	sprintérský	k2eAgInPc1d1	sprintérský
souboje	souboj	k1gInPc1	souboj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ne	ne	k9	ne
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
a	a	k8xC	a
pro	pro	k7c4	pro
body	bod	k1gInPc4	bod
pro	pro	k7c4	pro
zisk	zisk	k1gInSc4	zisk
zeleného	zelený	k2eAgInSc2d1	zelený
trikotu	trikot	k1gInSc2	trikot
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sprintera	sprinter	k1gMnSc4	sprinter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
dvacítce	dvacítka	k1gFnSc6	dvacítka
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nerozdělují	rozdělovat	k5eNaImIp3nP	rozdělovat
sprintérské	sprintérský	k2eAgInPc4d1	sprintérský
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jezdci	jezdec	k1gMnPc1	jezdec
nesoutěží	soutěžit	k5eNaImIp3nP	soutěžit
o	o	k7c4	o
konečné	konečný	k2eAgNnSc4d1	konečné
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nepsané	nepsaný	k2eAgNnSc1d1	nepsaný
pravidlo	pravidlo	k1gNnSc1	pravidlo
předchází	předcházet	k5eAaImIp3nS	předcházet
nebezpečným	bezpečný	k2eNgInPc3d1	nebezpečný
hromadným	hromadný	k2eAgInPc3d1	hromadný
spurtům	spurt	k1gInPc3	spurt
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
některých	některý	k3yIgFnPc2	některý
etap	etapa	k1gFnPc2	etapa
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
koncích	konec	k1gInPc6	konec
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2008	[number]	k4	2008
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
časový	časový	k2eAgInSc4d1	časový
bonus	bonus	k1gInSc4	bonus
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
to	ten	k3xDgNnSc1	ten
platilo	platit	k5eAaImAgNnS	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
jezdce	jezdec	k1gInPc4	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
určitého	určitý	k2eAgNnSc2d1	určité
kontrolního	kontrolní	k2eAgNnSc2d1	kontrolní
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
bonus	bonus	k1gInSc1	bonus
býval	bývat	k5eAaImAgInS	bývat
maximálně	maximálně	k6eAd1	maximálně
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jezdec	jezdec	k1gMnSc1	jezdec
upadne	upadnout	k5eAaPmIp3nS	upadnout
méně	málo	k6eAd2	málo
než	než	k8xS	než
kilometr	kilometr	k1gInSc4	kilometr
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
etapy	etapa	k1gFnSc2	etapa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
signalizováno	signalizován	k2eAgNnSc1d1	signalizováno
červeným	červený	k2eAgInSc7d1	červený
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
praporkem	praporek	k1gInSc7	praporek
nad	nad	k7c7	nad
tratí	trať	k1gFnSc7	trať
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
připsán	připsán	k2eAgInSc4d1	připsán
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
dojezdu	dojezd	k1gInSc2	dojezd
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
spurtéři	spurtér	k1gMnPc1	spurtér
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
za	za	k7c4	za
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nereflektuje	reflektovat	k5eNaImIp3nS	reflektovat
jejich	jejich	k3xOp3gInSc4	jejich
skutečný	skutečný	k2eAgInSc4d1	skutečný
celkový	celkový	k2eAgInSc4d1	celkový
výkon	výkon	k1gInSc4	výkon
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
spurtér	spurtér	k1gMnSc1	spurtér
sice	sice	k8xC	sice
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
etapu	etapa	k1gFnSc4	etapa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
penalizován	penalizovat	k5eAaImNgMnS	penalizovat
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Časovku	časovka	k1gFnSc4	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
jezdí	jezdit	k5eAaImIp3nP	jezdit
cyklisté	cyklista	k1gMnPc1	cyklista
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
startují	startovat	k5eAaBmIp3nP	startovat
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
dvou	dva	k4xCgFnPc2	dva
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
obráceném	obrácený	k2eAgNnSc6d1	obrácené
pořadí	pořadí	k1gNnSc6	pořadí
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
klasifikaci	klasifikace	k1gFnSc6	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
postavení	postavení	k1gNnSc4	postavení
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
startující	startující	k2eAgMnPc1d1	startující
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
tří	tři	k4xCgFnPc2	tři
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc1	Tour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
časovku	časovka	k1gFnSc4	časovka
(	(	kIx(	(
<g/>
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
legendární	legendární	k2eAgFnSc1d1	legendární
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Alpe-d	Alpe	k1gMnSc1	Alpe-d
<g/>
'	'	kIx"	'
<g/>
Huez	Huez	k1gMnSc1	Huez
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jezdci	jezdec	k1gMnPc1	jezdec
startovali	startovat	k5eAaBmAgMnP	startovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
jedné	jeden	k4xCgFnSc2	jeden
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
dvou	dva	k4xCgFnPc2	dva
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
časovka	časovka	k1gFnSc1	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
zkouškou	zkouška	k1gFnSc7	zkouška
individuálních	individuální	k2eAgFnPc2d1	individuální
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
jezdci	jezdec	k1gMnPc1	jezdec
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
etapy	etapa	k1gFnSc2	etapa
nemohou	moct	k5eNaImIp3nP	moct
domlouvat	domlouvat	k5eAaImF	domlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jezdec	jezdec	k1gMnSc1	jezdec
dojede	dojet	k5eAaPmIp3nS	dojet
závodníka	závodník	k1gMnSc4	závodník
jedoucího	jedoucí	k2eAgMnSc4d1	jedoucí
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
různým	různý	k2eAgFnPc3d1	různá
schopnostem	schopnost	k1gFnPc3	schopnost
cyklistů	cyklista	k1gMnPc2	cyklista
často	často	k6eAd1	často
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nP	smět
jet	jet	k5eAaImF	jet
tito	tento	k3xDgMnPc1	tento
závodníci	závodník	k1gMnPc1	závodník
jako	jako	k8xC	jako
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
nesmí	smět	k5eNaImIp3nS	smět
jet	jet	k5eAaImF	jet
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
tři	tři	k4xCgFnPc4	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgFnPc4	čtyři
časovky	časovka	k1gFnPc1	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
etapou	etapa	k1gFnSc7	etapa
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
krátká	krátký	k2eAgFnSc1d1	krátká
časovka	časovka	k1gFnSc1	časovka
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
prolog	prolog	k1gInSc1	prolog
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
bude	být	k5eAaImBp3nS	být
vozit	vozit	k5eAaImF	vozit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
etapě	etapa	k1gFnSc6	etapa
žlutý	žlutý	k2eAgInSc4d1	žlutý
trikot	trikot	k1gInSc4	trikot
a	a	k8xC	a
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
jezdci	jezdec	k1gMnPc1	jezdec
vměstnáni	vměstnán	k2eAgMnPc1d1	vměstnán
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
klasifikaci	klasifikace	k1gFnSc6	klasifikace
se	s	k7c7	s
stejnými	stejný	k2eAgInPc7d1	stejný
časy	čas	k1gInPc7	čas
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
mnoho	mnoho	k4c1	mnoho
jezdců	jezdec	k1gMnPc2	jezdec
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
díky	díky	k7c3	díky
pravidlu	pravidlo	k1gNnSc3	pravidlo
zmíněnému	zmíněný	k2eAgInSc3d1	zmíněný
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
časovka	časovka	k1gFnSc1	časovka
<g/>
,	,	kIx,	,
horská	horský	k2eAgFnSc1d1	horská
časovka	časovka	k1gFnSc1	časovka
a	a	k8xC	a
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
závodu	závod	k1gInSc2	závod
další	další	k2eAgFnSc1d1	další
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
časovka	časovka	k1gFnSc1	časovka
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
časovka	časovka	k1gFnSc1	časovka
před	před	k7c7	před
poslední	poslední	k2eAgFnSc7d1	poslední
etapou	etapa	k1gFnSc7	etapa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
startem	start	k1gInSc7	start
poslední	poslední	k2eAgFnSc2d1	poslední
etapy	etapa	k1gFnSc2	etapa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nejede	jet	k5eNaImIp3nS	jet
soutěžně	soutěžně	k6eAd1	soutěžně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
příležitostech	příležitost	k1gFnPc6	příležitost
však	však	k8xC	však
organizátoři	organizátor	k1gMnPc1	organizátor
závodu	závod	k1gInSc2	závod
změnili	změnit	k5eAaPmAgMnP	změnit
poslední	poslední	k2eAgFnSc4d1	poslední
etapu	etapa	k1gFnSc4	etapa
s	s	k7c7	s
dojezdem	dojezd	k1gInSc7	dojezd
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c4	na
časovku	časovka	k1gFnSc4	časovka
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
závodem	závod	k1gInSc7	závod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Tour	Tour	k1gInSc4	Tour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
pověstná	pověstný	k2eAgFnSc1d1	pověstná
nejtěsnějším	těsný	k2eAgNnSc7d3	nejtěsnější
vítězstvím	vítězství	k1gNnSc7	vítězství
Grega	Greg	k1gMnSc2	Greg
LeMonda	LeMond	k1gMnSc2	LeMond
před	před	k7c7	před
Laurentem	Laurent	k1gMnSc7	Laurent
Fignonem	Fignon	k1gMnSc7	Fignon
<g/>
.	.	kIx.	.
</s>
<s>
Fignon	Fignon	k1gMnSc1	Fignon
vjížděl	vjíždět	k5eAaImAgMnS	vjíždět
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
etapy	etapa	k1gFnSc2	etapa
jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
jezdec	jezdec	k1gMnSc1	jezdec
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
50	[number]	k4	50
s	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
o	o	k7c4	o
osm	osm	k4xCc4	osm
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
skvělém	skvělý	k2eAgInSc6d1	skvělý
časovkářském	časovkářský	k2eAgInSc6d1	časovkářský
výkonu	výkon	k1gInSc6	výkon
LeMonda	LeMonda	k1gFnSc1	LeMonda
<g/>
.	.	kIx.	.
</s>
<s>
Časovka	časovka	k1gFnSc1	časovka
týmů	tým	k1gInPc2	tým
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
časovce	časovka	k1gFnSc3	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celý	celý	k2eAgInSc1d1	celý
tým	tým	k1gInSc1	tým
jezdí	jezdit	k5eAaImIp3nS	jezdit
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
jezdí	jezdit	k5eAaImIp3nS	jezdit
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
jezdci	jezdec	k1gMnPc1	jezdec
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
členu	člen	k1gInSc3	člen
týmu	tým	k1gInSc2	tým
je	být	k5eAaImIp3nS	být
připočítán	připočítán	k2eAgInSc1d1	připočítán
konečný	konečný	k2eAgInSc1d1	konečný
čas	čas	k1gInSc1	čas
pátého	pátý	k4xOgInSc2	pátý
jezdce	jezdec	k1gInSc2	jezdec
devítičlenného	devítičlenný	k2eAgInSc2d1	devítičlenný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
odtrhne	odtrhnout	k5eAaPmIp3nS	odtrhnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jezdec	jezdec	k1gInSc1	jezdec
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
dojede	dojet	k5eAaPmIp3nS	dojet
na	na	k7c6	na
šesté	šestý	k4xOgFnSc6	šestý
či	či	k8xC	či
nižší	nízký	k2eAgFnSc4d2	nižší
pozici	pozice	k1gFnSc4	pozice
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připočítán	připočítán	k2eAgInSc1d1	připočítán
horší	zlý	k2eAgInSc4d2	horší
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterého	který	k3yQgMnSc4	který
skutečně	skutečně	k6eAd1	skutečně
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
píchne	píchnout	k5eAaPmIp3nS	píchnout
<g/>
-li	i	k?	-li
nebo	nebo	k8xC	nebo
upadne	upadnout	k5eAaPmIp3nS	upadnout
<g/>
-li	i	k?	-li
vůdce	vůdce	k1gMnPc4	vůdce
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
musí	muset	k5eAaImIp3nS	muset
obětovat	obětovat	k5eAaBmF	obětovat
čas	čas	k1gInSc4	čas
týmu	tým	k1gInSc2	tým
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
času	čas	k1gInSc2	čas
svého	svůj	k3xOyFgMnSc2	svůj
lídra	lídr	k1gMnSc2	lídr
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
časovka	časovka	k1gFnSc1	časovka
týmů	tým	k1gInPc2	tým
jela	jet	k5eAaImAgFnS	jet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
několika	několik	k4yIc7	několik
soutěžemi	soutěž	k1gFnPc7	soutěž
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přidružen	přidružen	k2eAgInSc4d1	přidružen
barevný	barevný	k2eAgInSc4d1	barevný
trikot	trikot	k1gInSc4	trikot
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgMnSc1d1	aktuální
držitel	držitel	k1gMnSc1	držitel
ocenění	ocenění	k1gNnSc4	ocenění
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
nosit	nosit	k5eAaImF	nosit
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
příslušný	příslušný	k2eAgInSc4d1	příslušný
trikot	trikot	k1gInSc4	trikot
<g/>
.	.	kIx.	.
</s>
<s>
Žlutý	žlutý	k2eAgInSc1d1	žlutý
trikot	trikot	k1gInSc1	trikot
(	(	kIx(	(
<g/>
maillot	maillot	k1gInSc1	maillot
jaune	jaunout	k5eAaImIp3nS	jaunout
<g/>
)	)	kIx)	)
nosí	nosit	k5eAaImIp3nS	nosit
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
jezdec	jezdec	k1gMnSc1	jezdec
celého	celý	k2eAgInSc2d1	celý
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ceněn	ceněn	k2eAgMnSc1d1	ceněn
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k1gMnSc1	vedoucí
jezdec	jezdec	k1gMnSc1	jezdec
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
celkovým	celkový	k2eAgMnSc7d1	celkový
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
získává	získávat	k5eAaImIp3nS	získávat
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
součet	součet	k1gInSc4	součet
všech	všecek	k3xTgInPc2	všecek
časů	čas	k1gInPc2	čas
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
etap	etapa	k1gFnPc2	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
trikot	trikot	k1gInSc1	trikot
(	(	kIx(	(
<g/>
maillot	maillot	k1gInSc1	maillot
vert	vert	k?	vert
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
udělován	udělovat	k5eAaImNgInS	udělovat
za	za	k7c4	za
sprintérské	sprintérský	k2eAgInPc4d1	sprintérský
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
etapy	etapa	k1gFnSc2	etapa
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
uděleny	udělit	k5eAaPmNgInP	udělit
jezdcům	jezdec	k1gMnPc3	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
skončí	skončit	k5eAaPmIp3nP	skončit
první	první	k4xOgNnPc4	první
<g/>
,	,	kIx,	,
druzí	druhý	k4xOgMnPc1	druhý
atd.	atd.	kA	atd.
Počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
typu	typ	k1gInSc6	typ
etapy	etapa	k1gFnSc2	etapa
-	-	kIx~	-
mnoho	mnoho	k6eAd1	mnoho
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
rovinaté	rovinatý	k2eAgFnPc4d1	rovinatá
etapy	etapa	k1gFnPc4	etapa
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
již	již	k9	již
za	za	k7c4	za
horské	horský	k2eAgFnPc4d1	horská
etapy	etapa	k1gFnPc4	etapa
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
za	za	k7c2	za
časovky	časovka	k1gFnSc2	časovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
etapy	etapa	k1gFnSc2	etapa
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
body	bod	k1gInPc4	bod
i	i	k9	i
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
průjezd	průjezd	k1gInSc4	průjezd
určitými	určitý	k2eAgFnPc7d1	určitá
kontrolními	kontrolní	k2eAgNnPc7d1	kontrolní
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rychlostními	rychlostní	k2eAgFnPc7d1	rychlostní
prémiemi	prémie	k1gFnPc7	prémie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
bývaly	bývat	k5eAaImAgFnP	bývat
v	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
pouze	pouze	k6eAd1	pouze
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgMnS	změnit
se	se	k3xPyFc4	se
i	i	k9	i
počet	počet	k1gInSc1	počet
přidělovaných	přidělovaný	k2eAgInPc2d1	přidělovaný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zelených	zelený	k2eAgInPc2d1	zelený
trikotů	trikot	k1gInPc2	trikot
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
Erik	Erik	k1gMnSc1	Erik
Zabel	Zabel	k1gMnSc1	Zabel
<g/>
.	.	kIx.	.
</s>
<s>
Puntíkovaný	puntíkovaný	k2eAgInSc1d1	puntíkovaný
trikot	trikot	k1gInSc1	trikot
(	(	kIx(	(
<g/>
maillot	maillot	k1gInSc1	maillot
a	a	k8xC	a
pois	pois	k1gInSc1	pois
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
krále	král	k1gMnPc4	král
hor.	hor.	k?	hor.
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
vybraných	vybraný	k2eAgNnPc2d1	vybrané
stoupání	stoupání	k1gNnPc2	stoupání
jsou	být	k5eAaImIp3nP	být
nejrychlejším	rychlý	k2eAgInPc3d3	nejrychlejší
jezdcům	jezdec	k1gInPc3	jezdec
rozdělovány	rozdělován	k2eAgFnPc1d1	rozdělována
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Výjezdy	výjezd	k1gInPc1	výjezd
jsou	být	k5eAaImIp3nP	být
děleny	dělit	k5eAaImNgInP	dělit
na	na	k7c4	na
kategorie	kategorie	k1gFnPc4	kategorie
od	od	k7c2	od
1	[number]	k4	1
(	(	kIx(	(
<g/>
nejobtížnější	obtížný	k2eAgMnSc1d3	nejobtížnější
<g/>
)	)	kIx)	)
do	do	k7c2	do
4	[number]	k4	4
(	(	kIx(	(
<g/>
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
náročnosti	náročnost	k1gFnSc2	náročnost
(	(	kIx(	(
<g/>
strmosti	strmost	k1gFnSc2	strmost
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
stoupání	stoupání	k1gNnSc1	stoupání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgNnPc1d3	nejtěžší
stoupání	stoupání	k1gNnPc1	stoupání
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xS	jako
Hors	Hors	k1gInSc1	Hors
catégorie	catégorie	k1gFnSc2	catégorie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kategorie	kategorie	k1gFnSc1	kategorie
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
bodové	bodový	k2eAgNnSc4d1	bodové
ohodnocení	ohodnocení	k1gNnSc4	ohodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
vrcholu	vrchol	k1gInSc6	vrchol
etapy	etapa	k1gFnSc2	etapa
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
bodová	bodový	k2eAgNnPc1d1	bodové
ohodnocení	ohodnocení	k1gNnPc1	ohodnocení
dvojnásobná	dvojnásobný	k2eAgNnPc1d1	dvojnásobné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vrcholy	vrchol	k1gInPc4	vrchol
HC	HC	kA	HC
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vrchařské	vrchařský	k2eAgNnSc4d1	vrchařské
vítězství	vítězství	k1gNnSc4	vítězství
se	se	k3xPyFc4	se
bojuje	bojovat	k5eAaImIp3nS	bojovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
puntíkovaný	puntíkovaný	k2eAgInSc1d1	puntíkovaný
trikot	trikot	k1gInSc1	trikot
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
zvoleny	zvolit	k5eAaPmNgFnP	zvolit
sponzorem	sponzor	k1gMnSc7	sponzor
Poulain	Poulain	k2eAgMnSc5d1	Poulain
Chocolate	Chocolat	k1gMnSc5	Chocolat
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
jezdcem	jezdec	k1gMnSc7	jezdec
je	být	k5eAaImIp3nS	být
Richard	Richard	k1gMnSc1	Richard
Virenque	Virenqu	k1gMnSc2	Virenqu
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
sedmkrát	sedmkrát	k6eAd1	sedmkrát
-	-	kIx~	-
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
-	-	kIx~	-
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
trikot	trikot	k1gInSc1	trikot
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
žlutého	žlutý	k2eAgInSc2d1	žlutý
trikotu	trikot	k1gInSc2	trikot
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
jezdcům	jezdec	k1gMnPc3	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
nedovršili	dovršit	k5eNaPmAgMnP	dovršit
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
soutěží	soutěž	k1gFnSc7	soutěž
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
nejbojovnějšího	bojovný	k2eAgMnSc4d3	nejbojovnější
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
sejde	sejít	k5eAaPmIp3nS	sejít
skupina	skupina	k1gFnSc1	skupina
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
uděluje	udělovat	k5eAaImIp3nS	udělovat
body	bod	k1gInPc4	bod
závodníkům	závodník	k1gMnPc3	závodník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
únik	únik	k1gInSc4	únik
či	či	k8xC	či
jiný	jiný	k2eAgInSc4d1	jiný
podobný	podobný	k2eAgInSc4d1	podobný
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
nosil	nosit	k5eAaImAgInS	nosit
bílé	bílý	k2eAgNnSc4d1	bílé
startovní	startovní	k2eAgNnSc4d1	startovní
číslo	číslo	k1gNnSc4	číslo
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
pozadí	pozadí	k1gNnSc6	pozadí
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
černého	černý	k2eAgNnSc2d1	černé
čísla	číslo	k1gNnSc2	číslo
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
soutěží	soutěž	k1gFnSc7	soutěž
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc4	soutěž
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
etapě	etapa	k1gFnSc6	etapa
je	být	k5eAaImIp3nS	být
týmu	tým	k1gInSc3	tým
přidán	přidán	k2eAgInSc1d1	přidán
čas	čas	k1gInSc1	čas
tří	tři	k4xCgMnPc2	tři
nejrychlejších	rychlý	k2eAgMnPc2d3	nejrychlejší
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
22	[number]	k4	22
týmů	tým	k1gInPc2	tým
po	po	k7c4	po
9	[number]	k4	9
závodnících	závodník	k1gMnPc6	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
etapa	etapa	k1gFnSc1	etapa
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
Avenue	avenue	k1gFnSc4	avenue
des	des	k1gNnSc4	des
Champs-Élysées	Champs-Élysées	k1gInSc1	Champs-Élysées
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
pokrytí	pokrytí	k1gNnSc1	pokrytí
dlažebními	dlažební	k2eAgInPc7d1	dlažební
kameny	kámen	k1gInPc7	kámen
pro	pro	k7c4	pro
jezdce	jezdec	k1gMnPc4	jezdec
velmi	velmi	k6eAd1	velmi
nepříjemný	příjemný	k2eNgMnSc1d1	nepříjemný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
etapa	etapa	k1gFnSc1	etapa
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nejezdí	jezdit	k5eNaImIp3nP	jezdit
v	v	k7c6	v
soutěžním	soutěžní	k2eAgMnSc6d1	soutěžní
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zpravidla	zpravidla	k6eAd1	zpravidla
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
soubojem	souboj	k1gInSc7	souboj
sprinterů	sprinter	k1gMnPc2	sprinter
o	o	k7c4	o
prestižní	prestižní	k2eAgNnSc4d1	prestižní
poslední	poslední	k2eAgNnSc4d1	poslední
etapové	etapový	k2eAgNnSc4d1	etapové
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
objevily	objevit	k5eAaPmAgFnP	objevit
výjimky	výjimka	k1gFnPc1	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Stephen	Stephen	k2eAgInSc1d1	Stephen
Roche	Roche	k1gInSc1	Roche
vedl	vést	k5eAaImAgInS	vést
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
časovce	časovka	k1gFnSc6	časovka
o	o	k7c4	o
40	[number]	k4	40
s	s	k7c7	s
před	před	k7c7	před
Pedrem	Pedr	k1gInSc7	Pedr
Delgadem	Delgad	k1gInSc7	Delgad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Delgado	Delgada	k1gFnSc5	Delgada
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
etapě	etapa	k1gFnSc6	etapa
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
od	od	k7c2	od
pelotonu	peloton	k1gInSc2	peloton
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
strhnout	strhnout	k5eAaPmF	strhnout
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Roche	Roche	k1gFnSc1	Roche
Delgada	Delgada	k1gFnSc1	Delgada
dohonil	dohonit	k5eAaPmAgInS	dohonit
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
dojeli	dojet	k5eAaPmAgMnP	dojet
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pelotonu	peloton	k1gInSc6	peloton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
organizátoři	organizátor	k1gMnPc1	organizátor
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
přesunutím	přesunutí	k1gNnSc7	přesunutí
poslední	poslední	k2eAgFnSc2d1	poslední
časovky	časovka	k1gFnSc2	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
etapy	etapa	k1gFnSc2	etapa
místo	místo	k7c2	místo
etapy	etapa	k1gFnSc2	etapa
předposlední	předposlední	k2eAgMnSc1d1	předposlední
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
stala	stát	k5eAaPmAgFnS	stát
Tour	Tour	k1gInSc4	Tour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Greg	Greg	k1gMnSc1	Greg
LeMond	LeMond	k1gMnSc1	LeMond
převzal	převzít	k5eAaPmAgMnS	převzít
žlutý	žlutý	k2eAgInSc4d1	žlutý
trikot	trikot	k1gInSc4	trikot
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
časovkářské	časovkářský	k2eAgFnSc6d1	časovkářská
etapě	etapa	k1gFnSc6	etapa
před	před	k7c7	před
Laurentem	Laurent	k1gMnSc7	Laurent
Fignonem	Fignon	k1gMnSc7	Fignon
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
o	o	k7c4	o
osm	osm	k4xCc4	osm
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Proslulou	proslulý	k2eAgFnSc7d1	proslulá
etapou	etapa	k1gFnSc7	etapa
je	být	k5eAaImIp3nS	být
etapa	etapa	k1gFnSc1	etapa
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
na	na	k7c6	na
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Alpe-d	Alpe	k1gMnSc1	Alpe-d
<g/>
'	'	kIx"	'
<g/>
Huez	Huez	k1gMnSc1	Huez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
organizátoři	organizátor	k1gMnPc1	organizátor
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
experimentu	experiment	k1gInSc3	experiment
a	a	k8xC	a
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
časovku	časovka	k1gFnSc4	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
na	na	k7c6	na
Alpe	Alpe	k1gFnSc6	Alpe
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Huez	Huez	k1gMnSc1	Huez
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
však	však	k9	však
závodníky	závodník	k1gMnPc4	závodník
nebyla	být	k5eNaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
známou	známý	k2eAgFnSc7d1	známá
etapou	etapa	k1gFnSc7	etapa
je	být	k5eAaImIp3nS	být
etapa	etapa	k1gFnSc1	etapa
končíci	končík	k1gMnPc1	končík
v	v	k7c6	v
Provensálských	provensálský	k2eAgFnPc6d1	provensálská
Alpách	Alpy	k1gFnPc6	Alpy
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mont	Mont	k2eAgInSc4d1	Mont
Ventoux	Ventoux	k1gInSc4	Ventoux
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
téměř	téměř	k6eAd1	téměř
22	[number]	k4	22
kilometrový	kilometrový	k2eAgInSc1d1	kilometrový
kopec	kopec	k1gInSc1	kopec
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
sklonem	sklon	k1gInSc7	sklon
7,43	[number]	k4	7,43
%	%	kIx~	%
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
zařazován	zařazovat	k5eAaImNgInS	zařazovat
do	do	k7c2	do
programu	program	k1gInSc2	program
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
jako	jako	k8xC	jako
výšlap	výšlap	k1gInSc1	výšlap
na	na	k7c4	na
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Alpe-d	Alpe	k1gMnSc1	Alpe-d
<g/>
'	'	kIx"	'
<g/>
Huez	Huez	k1gMnSc1	Huez
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgFnPc7d1	mnohá
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejnáročnější	náročný	k2eAgInSc4d3	nejnáročnější
kopec	kopec	k1gInSc4	kopec
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelství	hostitelství	k1gNnSc1	hostitelství
začátku	začátek	k1gInSc2	začátek
nebo	nebo	k8xC	nebo
konce	konec	k1gInSc2	konec
etapy	etapa	k1gFnSc2	etapa
přináší	přinášet	k5eAaImIp3nS	přinášet
dotyčnému	dotyčný	k2eAgNnSc3d1	dotyčné
městu	město	k1gNnSc3	město
prestiž	prestiž	k1gFnSc1	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
začínala	začínat	k5eAaImAgFnS	začínat
každá	každý	k3xTgFnSc1	každý
etapa	etapa	k1gFnSc1	etapa
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předchozí	předchozí	k2eAgFnSc1d1	předchozí
etapa	etapa	k1gFnSc1	etapa
končila	končit	k5eAaImAgFnS	končit
<g/>
,	,	kIx,	,
a	a	k8xC	a
závod	závod	k1gInSc1	závod
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
kontinuální	kontinuální	k2eAgInSc1d1	kontinuální
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
startují	startovat	k5eAaBmIp3nP	startovat
etapy	etapa	k1gFnPc4	etapa
o	o	k7c4	o
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
kousek	kousek	k1gInSc4	kousek
slávy	sláva	k1gFnSc2	sláva
užije	užít	k5eAaPmIp3nS	užít
více	hodně	k6eAd2	hodně
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
prestižní	prestižní	k2eAgNnSc1d1	prestižní
je	být	k5eAaImIp3nS	být
hostit	hostit	k5eAaImF	hostit
prolog	prolog	k1gInSc4	prolog
a	a	k8xC	a
začátek	začátek	k1gInSc4	začátek
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
prolog	prolog	k1gInSc4	prolog
příliš	příliš	k6eAd1	příliš
krátký	krátký	k2eAgInSc4d1	krátký
na	na	k7c4	na
přejezd	přejezd	k1gInSc4	přejezd
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
prolog	prolog	k1gInSc1	prolog
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
bývá	bývat	k5eAaImIp3nS	bývat
střídavě	střídavě	k6eAd1	střídavě
zahájena	zahájit	k5eAaPmNgFnS	zahájit
uvnitř	uvnitř	k7c2	uvnitř
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
i	i	k9	i
několik	několik	k4yIc1	několik
prvních	první	k4xOgInPc2	první
etap	etapa	k1gFnPc2	etapa
jezdí	jezdit	k5eAaImIp3nS	jezdit
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
devět	devět	k4xCc1	devět
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
pět	pět	k4xCc1	pět
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
vyměnil	vyměnit	k5eAaPmAgInS	vyměnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
slovenské	slovenský	k2eAgFnSc2d1	slovenská
občanství	občanství	k1gNnSc4	občanství
za	za	k7c4	za
české	český	k2eAgMnPc4d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějšími	úspěšný	k2eAgMnPc7d3	nejúspěšnější
jezdci	jezdec	k1gMnPc7	jezdec
jsou	být	k5eAaImIp3nP	být
Svorada	Svorada	k1gFnSc1	Svorada
s	s	k7c7	s
osmi	osm	k4xCc7	osm
starty	start	k1gInPc7	start
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
individuálními	individuální	k2eAgNnPc7d1	individuální
etapovými	etapový	k2eAgNnPc7d1	etapové
vítězstvími	vítězství	k1gNnPc7	vítězství
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
s	s	k7c7	s
osmi	osm	k4xCc7	osm
starty	start	k1gInPc7	start
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
vítězstvími	vítězství	k1gNnPc7	vítězství
v	v	k7c6	v
týmové	týmový	k2eAgFnSc6d1	týmová
časovce	časovka	k1gFnSc6	časovka
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
etapovými	etapový	k2eAgNnPc7d1	etapové
vítězstvími	vítězství	k1gNnPc7	vítězství
a	a	k8xC	a
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
zelenými	zelené	k1gNnPc7	zelené
trikoty	trikot	k1gInPc1	trikot
a	a	k8xC	a
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g />
.	.	kIx.	.
</s>
<s>
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
doposud	doposud	k6eAd1	doposud
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
konečného	konečný	k2eAgNnSc2d1	konečné
umístění	umístění	k1gNnSc2	umístění
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
Květoslav	Květoslava	k1gFnPc2	Květoslava
Palov	Palov	k1gInSc1	Palov
(	(	kIx(	(
<g/>
103	[number]	k4	103
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Jurčo	Jurčo	k1gMnSc1	Jurčo
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
Milan	Milan	k1gMnSc1	Milan
Jurčo	Jurčo	k1gMnSc1	Jurčo
(	(	kIx(	(
<g/>
139	[number]	k4	139
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
103	[number]	k4	103
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
etapu	etapa	k1gFnSc4	etapa
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
etapu	etapa	k1gFnSc4	etapa
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
85	[number]	k4	85
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
129	[number]	k4	129
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
etapu	etapa	k1gFnSc4	etapa
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Tomáš	Tomáš	k1gMnSc1	Tomáš
Konečný	Konečný	k1gMnSc1	Konečný
(	(	kIx(	(
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Svorada	Svorada	k1gFnSc1	Svorada
(	(	kIx(	(
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
René	René	k1gMnSc1	René
Anderle	Anderle	k1gMnSc1	Anderle
(	(	kIx(	(
<g/>
83	[number]	k4	83
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
102	[number]	k4	102
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g />
.	.	kIx.	.
</s>
<s>
týmovou	týmový	k2eAgFnSc4d1	týmová
časovku	časovka	k1gFnSc4	časovka
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
týmovou	týmový	k2eAgFnSc4d1	týmová
časovku	časovka	k1gFnSc4	časovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hruška	Hruška	k1gMnSc1	Hruška
(	(	kIx(	(
<g/>
117	[number]	k4	117
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
týmovou	týmový	k2eAgFnSc4d1	týmová
časovku	časovka	k1gFnSc4	časovka
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Pavel	Pavel	k1gMnSc1	Pavel
<g />
.	.	kIx.	.
</s>
<s>
Padrnos	Padrnos	k1gInSc1	Padrnos
(	(	kIx(	(
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g />
.	.	kIx.	.
</s>
<s>
<g/>
místo	místo	k1gNnSc1	místo
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
112	[number]	k4	112
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
3	[number]	k4	3
etapy	etapa	k1gFnPc1	etapa
a	a	k8xC	a
zelený	zelený	k2eAgInSc1d1	zelený
trikot	trikot	k1gInSc1	trikot
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnPc4	vítěz
bodovací	bodovací	k2eAgFnSc2d1	bodovací
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Saxo	Saxo	k1gMnSc1	Saxo
<g />
.	.	kIx.	.
</s>
<s>
Bank-Tinkoff	Bank-Tinkoff	k1gInSc1	Bank-Tinkoff
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
klasifikaci	klasifikace	k1gFnSc4	klasifikace
týmů	tým	k1gInPc2	tým
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
jednu	jeden	k4xCgFnSc4	jeden
etapu	etapa	k1gFnSc4	etapa
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
zelený	zelený	k2eAgInSc4d1	zelený
dres	dres	k1gInSc4	dres
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
Leopold	Leopold	k1gMnSc1	Leopold
König	König	k1gMnSc1	König
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
jel	jet	k5eAaImAgMnS	jet
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
německého	německý	k2eAgInSc2d1	německý
týmu	tým	k1gInSc2	tým
NetApp-Endura	NetApp-Endur	k1gMnSc2	NetApp-Endur
<g/>
,	,	kIx,	,
dojel	dojet	k5eAaPmAgMnS	dojet
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
horské	horský	k2eAgFnSc6d1	horská
etapě	etapa	k1gFnSc6	etapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bárta	Bárta	k1gMnSc1	Bárta
(	(	kIx(	(
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
dojel	dojet	k5eAaPmAgMnS	dojet
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
individuální	individuální	k2eAgFnSc6d1	individuální
časovce	časovka	k1gFnSc6	časovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Velits	Velits	k1gInSc1	Velits
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
po	po	k7c4	po
třetí	třetí	k4xOgFnPc4	třetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
získal	získat	k5eAaPmAgInS	získat
zelený	zelený	k2eAgInSc1d1	zelený
dres	dres	k1gInSc1	dres
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bárta	Bárta	k1gMnSc1	Bárta
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
König	König	k1gMnSc1	König
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
70	[number]	k4	70
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štybar	Štybar	k1gMnSc1	Štybar
(	(	kIx(	(
<g/>
103	[number]	k4	103
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
etapu	etapa	k1gFnSc4	etapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
získal	získat	k5eAaPmAgInS	získat
zelený	zelený	k2eAgInSc1d1	zelený
dres	dres	k1gInSc1	dres
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
(	(	kIx(	(
<g/>
Team	team	k1gInSc1	team
Tinkoff-Saxo	Tinkoff-Saxo	k1gMnSc1	Tinkoff-Saxo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bárta	Bárta	k1gMnSc1	Bárta
(	(	kIx(	(
<g/>
Bora-Argon	Bora-Argon	k1gInSc1	Bora-Argon
18	[number]	k4	18
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
88	[number]	k4	88
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Vakoč	Vakoč	k1gMnSc1	Vakoč
(	(	kIx(	(
<g/>
Etixx-Quick	Etixx-Quick	k1gMnSc1	Etixx-Quick
Step	step	k1gFnSc1	step
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
118	[number]	k4	118
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
Team	team	k1gInSc1	team
Tinkoff-Saxo	Tinkoff-Saxo	k1gNnSc1	Tinkoff-Saxo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
Baal	Baal	k1gMnSc1	Baal
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
:	:	kIx,	:
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
:	:	kIx,	:
sny	sen	k1gInPc1	sen
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
profesionální	profesionální	k2eAgFnSc2d1	profesionální
cyklistiky	cyklistika	k1gFnSc2	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
:	:	kIx,	:
Altimax	Altimax	k1gInSc1	Altimax
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86942-03-1	[number]	k4	80-86942-03-1
Pacina	Pacino	k1gNnSc2	Pacino
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
:	:	kIx,	:
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
profesionální	profesionální	k2eAgFnSc2d1	profesionální
cyklistiky	cyklistika	k1gFnSc2	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc1	galerie
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
Legenda	legenda	k1gFnSc1	legenda
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
1	[number]	k4	1
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
-	-	kIx~	-
video	video	k1gNnSc1	video
Legenda	legenda	k1gFnSc1	legenda
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
2	[number]	k4	2
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
-	-	kIx~	-
video	video	k1gNnSc1	video
</s>
