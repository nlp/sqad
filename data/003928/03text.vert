<s>
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
část	část	k1gFnSc1	část
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
části	část	k1gFnSc6	část
Ameriky	Amerika	k1gFnSc2	Amerika
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
většinu	většina	k1gFnSc4	většina
Karibské	karibský	k2eAgFnSc2d1	karibská
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
státy	stát	k1gInPc1	stát
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Panama	Panama	k1gFnSc1	Panama
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
státy	stát	k1gInPc1	stát
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnPc1	Belize
a	a	k8xC	a
poloostrovem	poloostrov	k1gInSc7	poloostrov
Yucatán	Yucatán	k2eAgMnSc1d1	Yucatán
(	(	kIx(	(
<g/>
patřícím	patřící	k2eAgNnSc6d1	patřící
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
)	)	kIx)	)
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Velkými	velký	k2eAgFnPc7d1	velká
Antilami	Antily	k1gFnPc7	Antily
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ostrovy	ostrov	k1gInPc7	ostrov
Kuba	kuba	k1gNnSc2	kuba
<g/>
,	,	kIx,	,
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
a	a	k8xC	a
Portoriko	Portoriko	k1gNnSc1	Portoriko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Malými	malý	k2eAgFnPc7d1	malá
Antilami	Antily	k1gFnPc7	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
754	[number]	k4	754
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
nejhlubším	hluboký	k2eAgNnSc7d3	nejhlubší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
Kajmanský	Kajmanský	k2eAgInSc1d1	Kajmanský
příkop	příkop	k1gInSc1	příkop
mezi	mezi	k7c7	mezi
Kubou	Kuba	k1gFnSc7	Kuba
a	a	k8xC	a
Jamajkou	Jamajka	k1gFnSc7	Jamajka
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
7686	[number]	k4	7686
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Hondurasu	Honduras	k1gInSc2	Honduras
a	a	k8xC	a
Belize	Belize	k1gFnSc2	Belize
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Honduraský	honduraský	k2eAgInSc1d1	honduraský
záliv	záliv	k1gInSc1	záliv
a	a	k8xC	a
Mezoamerický	Mezoamerický	k2eAgInSc4d1	Mezoamerický
korálový	korálový	k2eAgInSc4d1	korálový
útes	útes	k1gInSc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
sužována	sužovat	k5eAaImNgFnS	sužovat
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
,	,	kIx,	,
sopečnými	sopečný	k2eAgInPc7d1	sopečný
výbuchy	výbuch	k1gInPc7	výbuch
a	a	k8xC	a
sezónními	sezónní	k2eAgInPc7d1	sezónní
hurikány	hurikán	k1gInPc7	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
Antily	Antily	k1gFnPc1	Antily
Grand	grand	k1gMnSc1	grand
Cayman	Cayman	k1gMnSc1	Cayman
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
Kajmanský	Kajmanský	k2eAgInSc1d1	Kajmanský
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
(	(	kIx(	(
<g/>
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
leží	ležet	k5eAaImIp3nS	ležet
Haiti	Haiti	k1gNnSc1	Haiti
a	a	k8xC	a
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Jamajka	Jamajka	k1gFnSc1	Jamajka
(	(	kIx(	(
<g/>
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Kuba	Kuba	k1gFnSc1	Kuba
(	(	kIx(	(
<g/>
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Portoriko	Portoriko	k1gNnSc1	Portoriko
(	(	kIx(	(
<g/>
přidružený	přidružený	k2eAgInSc1d1	přidružený
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
Závětrné	závětrný	k2eAgInPc4d1	závětrný
ostrovy	ostrov	k1gInPc4	ostrov
Anguilla	Anguilla	k1gMnSc1	Anguilla
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Antigua	Antigua	k1gFnSc1	Antigua
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
státu	stát	k1gInSc2	stát
Antigua	Antiguum	k1gNnSc2	Antiguum
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
<g/>
)	)	kIx)	)
Barbuda	Barbuda	k1gFnSc1	Barbuda
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
státu	stát	k1gInSc2	stát
Antigua	Antiguum	k1gNnSc2	Antiguum
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
<g/>
)	)	kIx)	)
Dominika	Dominik	k1gMnSc2	Dominik
(	(	kIx(	(
<g/>
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Guadeloupe	Guadeloupe	k1gFnSc1	Guadeloupe
(	(	kIx(	(
<g/>
zámořský	zámořský	k2eAgInSc1d1	zámořský
region	region	k1gInSc1	region
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
Montserrat	Montserrat	k1gInSc1	Montserrat
(	(	kIx(	(
<g/>
zámořské	zámořský	k2eAgNnSc1d1	zámořské
území	území	k1gNnSc1	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Nevis	viset	k5eNaImRp2nS	viset
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
republiky	republika	k1gFnSc2	republika
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
<g/>
)	)	kIx)	)
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
Britské	britský	k2eAgFnPc4d1	britská
Pan	Pan	k1gMnSc1	Pan
<g/>
.	.	kIx.	.
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Americké	americký	k2eAgFnPc1d1	americká
Pan	Pan	k1gMnSc1	Pan
<g/>
.	.	kIx.	.
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
Saba	Saba	k1gFnSc1	Saba
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Svatý	svatý	k1gMnSc1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
(	(	kIx(	(
<g/>
francouzské	francouzský	k2eAgNnSc1d1	francouzské
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
<g/>
)	)	kIx)	)
Svatý	svatý	k2eAgMnSc1d1	svatý
<g />
.	.	kIx.	.
</s>
<s>
Eustach	Eustach	k1gMnSc1	Eustach
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Svatý	svatý	k1gMnSc1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
(	(	kIx(	(
<g/>
republika	republika	k1gFnSc1	republika
Svatý	svatý	k1gMnSc1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
<g/>
)	)	kIx)	)
Svatý	svatý	k1gMnSc1	svatý
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
mezi	mezi	k7c4	mezi
francouzské	francouzský	k2eAgNnSc4d1	francouzské
zámořské	zámořský	k2eAgNnSc4d1	zámořské
společenství	společenství	k1gNnSc4	společenství
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc4d1	autonomní
zemi	zem	k1gFnSc4	zem
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Návětrné	návětrný	k2eAgInPc1d1	návětrný
ostrovy	ostrov	k1gInPc1	ostrov
Barbados	Barbadosa	k1gFnPc2	Barbadosa
(	(	kIx(	(
<g/>
člen	člen	k1gInSc1	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
)	)	kIx)	)
Grenada	Grenada	k1gFnSc1	Grenada
(	(	kIx(	(
<g/>
člen	člen	k1gInSc1	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Grenadiny	grenadina	k1gFnPc1	grenadina
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
ostrovy	ostrov	k1gInPc4	ostrov
Grenada	Grenada	k1gFnSc1	Grenada
a	a	k8xC	a
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
<g/>
)	)	kIx)	)
Martinik	Martinik	k1gMnSc1	Martinik
(	(	kIx(	(
<g/>
zámořský	zámořský	k2eAgInSc1d1	zámořský
region	region	k1gInSc1	region
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
státu	stát	k1gInSc2	stát
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
<g/>
)	)	kIx)	)
Tobago	Tobago	k1gNnSc1	Tobago
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
republiky	republika	k1gFnSc2	republika
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Trinidad	Trinidad	k1gInSc1	Trinidad
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
republiky	republika	k1gFnSc2	republika
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k6eAd1	Tobago
<g/>
)	)	kIx)	)
Závětrné	závětrný	k2eAgFnPc4d1	závětrná
Antily	Antily	k1gFnPc4	Antily
Aruba	Aruba	k1gFnSc1	Aruba
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Bonaire	Bonair	k1gInSc5	Bonair
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Curaçao	curaçao	k1gNnSc1	curaçao
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
Isla	Isl	k2eAgFnSc1d1	Isla
de	de	k?	de
Margarita	Margarita	k1gFnSc1	Margarita
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
)	)	kIx)	)
ostrovy	ostrov	k1gInPc1	ostrov
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g />
.	.	kIx.	.
</s>
<s>
Islas	Islas	k1gInSc1	Islas
de	de	k?	de
la	la	k1gNnSc1	la
Bahía	Bahía	k1gFnSc1	Bahía
(	(	kIx(	(
<g/>
Honduras	Honduras	k1gInSc1	Honduras
<g/>
)	)	kIx)	)
Corn	Corn	k1gInSc1	Corn
Islands	Islands	k1gInSc1	Islands
<g/>
,	,	kIx,	,
Cayos	Cayos	k1gMnSc1	Cayos
Miskitos	Miskitos	k1gMnSc1	Miskitos
(	(	kIx(	(
<g/>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
)	)	kIx)	)
atolové	atolový	k2eAgInPc1d1	atolový
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Belize	Belize	k1gFnSc2	Belize
San	San	k1gFnPc2	San
Andrés	Andrés	k1gInSc1	Andrés
a	a	k8xC	a
Providencia	Providencia	k1gFnSc1	Providencia
(	(	kIx(	(
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
Cozumel	Cozumel	k1gInSc1	Cozumel
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karibské	karibský	k2eAgFnSc2d1	karibská
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
