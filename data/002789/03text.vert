<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Daniel	Daniel	k1gMnSc1	Daniel
Ďurech	Ďurech	k1gInSc1	Ďurech
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
v	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
v	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
Žižkov	Žižkov	k1gInSc4	Žižkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našel	najít	k5eAaPmAgMnS	najít
svoje	svůj	k3xOyFgNnSc4	svůj
hobby	hobby	k1gNnSc4	hobby
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Hip	hip	k0	hip
Hop	hop	k0	hop
<g/>
.	.	kIx.	.
</s>
<s>
Hip-hopové	Hipopový	k2eAgNnSc1d1	Hip-hopový
uskupení	uskupení	k1gNnSc1	uskupení
s	s	k7c7	s
názvem	název	k1gInSc7	název
K.O.	K.O.	k1gMnPc1	K.O.
Kru	kra	k1gFnSc4	kra
založili	založit	k5eAaPmAgMnP	založit
Hack	Hack	k1gMnSc1	Hack
(	(	kIx(	(
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
)	)	kIx)	)
s	s	k7c7	s
Phatem	Phat	k1gInSc7	Phat
(	(	kIx(	(
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Hugo	Hugo	k1gMnSc1	Hugo
Toxxxem	Toxxx	k1gInSc7	Toxxx
založili	založit	k5eAaPmAgMnP	založit
kultovní	kultovní	k2eAgNnSc4d1	kultovní
Supercrooo	Supercrooo	k1gNnSc4	Supercrooo
<g/>
.	.	kIx.	.
</s>
<s>
Phat	Phat	k1gInSc1	Phat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
začal	začít	k5eAaPmAgMnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
u	u	k7c2	u
P.	P.	kA	P.
<g/>
A.	A.	kA	A.
<g/>
trick	trick	k6eAd1	trick
records	records	k6eAd1	records
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
desky	deska	k1gFnSc2	deska
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Frekvence	frekvence	k1gFnSc2	frekvence
P.	P.	kA	P.
<g/>
H.A.T.	H.A.T.	k1gMnSc2	H.A.T.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgInS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nejstarším	starý	k2eAgInSc7d3	nejstarší
českým	český	k2eAgInSc7d1	český
raperem	raper	k1gInSc7	raper
Orionem	orion	k1gInSc7	orion
CD	CD	kA	CD
Orikoule	Orikoule	k1gFnPc4	Orikoule
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
tvůrcům	tvůrce	k1gMnPc3	tvůrce
snímku	snímka	k1gFnSc4	snímka
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
filozofii	filozofie	k1gFnSc4	filozofie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Sólová	sólový	k2eAgFnSc1d1	sólová
tvorba	tvorba	k1gFnSc1	tvorba
Frekvence	frekvence	k1gFnSc2	frekvence
P.	P.	kA	P.
<g/>
H.A.T.	H.A.T.	k1gMnSc1	H.A.T.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Phat	Phat	k1gMnSc1	Phat
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
je	být	k5eAaImIp3nS	být
Kapitán	kapitán	k1gMnSc1	kapitán
Láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mixtape	Mixtap	k1gMnSc5	Mixtap
Halucinace	halucinace	k1gFnPc1	halucinace
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
patra	patro	k1gNnSc2	patro
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
Moby	Moba	k1gMnSc2	Moba
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
LP	LP	kA	LP
Orfeus	Orfeus	k1gMnSc1	Orfeus
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-LP	-LP	k?	-LP
SuperCrooo	SuperCrooo	k1gMnSc1	SuperCrooo
Toxic	Toxic	k1gMnSc1	Toxic
Funk	funk	k1gInSc1	funk
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
České	český	k2eAgNnSc1d1	české
kuře	kuře	k1gNnSc1	kuře
<g/>
:	:	kIx,	:
Neurofolk	Neurofolk	k1gInSc1	Neurofolk
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
Baby	baba	k1gFnSc2	baba
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
2	[number]	k4	2
Nosáči	nosáč	k1gMnPc1	nosáč
tankujou	tankovat	k5eAaImIp3nP	tankovat
super	super	k2eAgMnSc1d1	super
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
Další	další	k2eAgFnSc7d1	další
<g />
.	.	kIx.	.
</s>
<s>
projekty	projekt	k1gInPc1	projekt
K.O.	K.O.	k1gMnSc2	K.O.
Krů	Krů	k1gMnSc2	Krů
-	-	kIx~	-
Nádech	nádech	k1gInSc1	nádech
<g/>
/	/	kIx~	/
<g/>
Náš	náš	k3xOp1gInSc1	náš
cíl	cíl	k1gInSc1	cíl
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
SP	SP	kA	SP
Dixxx	Dixxx	k1gInSc1	Dixxx
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgNnSc1d1	experimentální
elektro	elektro	k1gNnSc1	elektro
projekt	projekt	k1gInSc1	projekt
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
&	&	k?	&
Orion	orion	k1gInSc1	orion
-	-	kIx~	-
Orikoule	Orikoule	k1gFnSc1	Orikoule
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
&	&	k?	&
DJ	DJ	kA	DJ
Scarface	Scarface	k1gFnSc2	Scarface
-	-	kIx~	-
Jed	jed	k1gInSc1	jed
na	na	k7c4	na
krysy	krysa	k1gFnPc4	krysa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
LP	LP	kA	LP
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
&	&	k?	&
LA4	LA4	k1gFnSc3	LA4
&	&	k?	&
Mike	Mik	k1gInSc2	Mik
Trafik	trafika	k1gFnPc2	trafika
-	-	kIx~	-
Nadzemí	nadzemí	k1gNnSc2	nadzemí
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
EP	EP	kA	EP
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
ers	ers	k?	ers
-	-	kIx~	-
Making	Making	k1gInSc1	Making
Of	Of	k1gFnSc2	Of
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
