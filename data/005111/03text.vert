<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
či	či	k8xC	či
geomagnetické	geomagnetický	k2eAgNnSc1d1	geomagnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
indukované	indukovaný	k2eAgNnSc1d1	indukované
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
prostoru	prostor	k1gInSc6	prostor
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
působí	působit	k5eAaImIp3nS	působit
magnetická	magnetický	k2eAgFnSc1d1	magnetická
síla	síla	k1gFnSc1	síla
generovaná	generovaný	k2eAgFnSc1d1	generovaná
geodynamem	geodynam	k1gInSc7	geodynam
uvnitř	uvnitř	k7c2	uvnitř
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vlivem	vliv	k1gInSc7	vliv
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
zmáčklé	zmáčklý	k2eAgNnSc1d1	zmáčklé
a	a	k8xC	a
na	na	k7c4	na
odvrácené	odvrácený	k2eAgNnSc4d1	odvrácené
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
protáhlé	protáhlý	k2eAgNnSc1d1	protáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
důležitý	důležitý	k2eAgInSc4d1	důležitý
jev	jev	k1gInSc4	jev
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
biosféry	biosféra	k1gFnSc2	biosféra
respektive	respektive	k9	respektive
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgNnSc2	tento
pole	pole	k1gNnSc2	pole
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
25	[number]	k4	25
a	a	k8xC	a
65	[number]	k4	65
mikrotesla	mikrotést	k5eAaPmAgFnS	mikrotést
(	(	kIx(	(
<g/>
od	od	k7c2	od
0,25	[number]	k4	0,25
do	do	k7c2	do
0,65	[number]	k4	0,65
Gaussů	gauss	k1gInPc2	gauss
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Swarm	Swarm	k1gInSc1	Swarm
vykázalo	vykázat	k5eAaPmAgNnS	vykázat
trend	trend	k1gInSc4	trend
slábnutí	slábnutí	k1gNnSc2	slábnutí
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
jasné	jasný	k2eAgNnSc1d1	jasné
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
trvalý	trvalý	k2eAgInSc4d1	trvalý
nebo	nebo	k8xC	nebo
dočasný	dočasný	k2eAgInSc4d1	dočasný
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
4	[number]	k4	4
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
udržování	udržování	k1gNnSc1	udržování
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
i	i	k8xC	i
působení	působení	k1gNnSc4	působení
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
dipólový	dipólový	k2eAgInSc1d1	dipólový
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozložení	rozložení	k1gNnSc1	rozložení
siločar	siločára	k1gFnPc2	siločára
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
siločarám	siločára	k1gFnPc3	siločára
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
tyčového	tyčový	k2eAgInSc2d1	tyčový
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
osa	osa	k1gFnSc1	osa
neprochází	procházet	k5eNaImIp3nS	procházet
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
520	[number]	k4	520
kilometrů	kilometr	k1gInPc2	kilometr
odkloněna	odkloněn	k2eAgFnSc1d1	odkloněna
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
magnetických	magnetický	k2eAgInPc2d1	magnetický
pólů	pól	k1gInPc2	pól
driftuje	driftovat	k5eAaBmIp3nS	driftovat
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
i	i	k9	i
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
km	km	kA	km
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
navigaci	navigace	k1gFnSc6	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
vznikajícím	vznikající	k2eAgNnPc3d1	vznikající
třením	tření	k1gNnPc3	tření
při	při	k7c6	při
rotaci	rotace	k1gFnSc6	rotace
vnějšího	vnější	k2eAgNnSc2d1	vnější
polotekutého	polotekutý	k2eAgNnSc2d1	polotekuté
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
pevným	pevný	k2eAgNnSc7d1	pevné
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
jádrem	jádro	k1gNnSc7	jádro
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
zemským	zemský	k2eAgInSc7d1	zemský
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
obrovské	obrovský	k2eAgFnSc2d1	obrovská
hydrodynamické	hydrodynamický	k2eAgFnSc2d1	hydrodynamická
geodynamo	geodynamo	k6eAd1	geodynamo
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
hrají	hrát	k5eAaImIp3nP	hrát
prvky	prvek	k1gInPc1	prvek
jádra	jádro	k1gNnSc2	jádro
<g/>
:	:	kIx,	:
nikl	nikl	k1gInSc1	nikl
či	či	k8xC	či
železo	železo	k1gNnSc1	železo
<g/>
.	.	kIx.	.
</s>
<s>
Geomagnetické	geomagnetický	k2eAgNnSc1d1	geomagnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
v	v	k7c6	v
čase	čas	k1gInSc6	čas
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
polaritě	polarita	k1gFnSc6	polarita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
často	často	k6eAd1	často
měnila	měnit	k5eAaImAgFnS	měnit
(	(	kIx(	(
<g/>
při	při	k7c6	při
přepólování	přepólování	k1gNnSc6	přepólování
dipólu	dipól	k1gInSc2	dipól
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgInSc4d1	dominantní
slabší	slabý	k2eAgInSc4d2	slabší
kvadrupólový	kvadrupólový	k2eAgInSc4d1	kvadrupólový
moment	moment	k1gInSc4	moment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
polarity	polarita	k1gFnSc2	polarita
dochází	docházet	k5eAaImIp3nS	docházet
zhruba	zhruba	k6eAd1	zhruba
během	během	k7c2	během
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
spontánně	spontánně	k6eAd1	spontánně
(	(	kIx(	(
<g/>
chaoticky	chaoticky	k6eAd1	chaoticky
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
či	či	k8xC	či
impakty	impakt	k1gInPc1	impakt
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobných	podobný	k2eAgFnPc2d1	podobná
příčin	příčina	k1gFnPc2	příčina
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
změnou	změna	k1gFnSc7	změna
rotace	rotace	k1gFnSc2	rotace
(	(	kIx(	(
<g/>
délkou	délka	k1gFnSc7	délka
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
paleomagnetismu	paleomagnetismus	k1gInSc2	paleomagnetismus
hornin	hornina	k1gFnPc2	hornina
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
existence	existence	k1gFnSc1	existence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
už	už	k6eAd1	už
před	před	k7c7	před
3,9	[number]	k4	3,9
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
docházelo	docházet	k5eAaImAgNnS	docházet
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
polarity	polarita	k1gFnSc2	polarita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
umožnil	umožnit	k5eAaPmAgInS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
tzv.	tzv.	kA	tzv.
paleomagnetické	paleomagnetický	k2eAgFnSc2d1	paleomagnetický
časové	časový	k2eAgFnSc2d1	časová
škály	škála	k1gFnSc2	škála
spolehlivě	spolehlivě	k6eAd1	spolehlivě
sahající	sahající	k2eAgFnSc1d1	sahající
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
jury	jura	k1gFnSc2	jura
před	před	k7c7	před
160	[number]	k4	160
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
byly	být	k5eAaImAgFnP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důkazů	důkaz	k1gInPc2	důkaz
pohybu	pohyb	k1gInSc2	pohyb
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vznik	vznik	k1gInSc4	vznik
teorie	teorie	k1gFnSc2	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
změna	změna	k1gFnSc1	změna
polarity	polarita	k1gFnSc2	polarita
pole	pole	k1gNnSc2	pole
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nehrozí	hrozit	k5eNaImIp3nS	hrozit
(	(	kIx(	(
<g/>
dipólová	dipólový	k2eAgFnSc1d1	dipólová
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nadprůměrná	nadprůměrný	k2eAgFnSc1d1	nadprůměrná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
vzniká	vznikat	k5eAaImIp3nS	vznikat
zemská	zemský	k2eAgFnSc1d1	zemská
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
kosmických	kosmický	k2eAgNnPc2d1	kosmické
těles	těleso	k1gNnPc2	těleso
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
pulzary	pulzar	k1gInPc7	pulzar
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemská	zemský	k2eAgFnSc1d1	zemská
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
své	svůj	k3xOyFgFnSc2	svůj
blízkosti	blízkost	k1gFnSc2	blízkost
nejlépe	dobře	k6eAd3	dobře
prozkoumaná	prozkoumaný	k2eAgFnSc1d1	prozkoumaná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
působí	působit	k5eAaImIp3nS	působit
tlakem	tlak	k1gInSc7	tlak
přibližně	přibližně	k6eAd1	přibližně
1,7	[number]	k4	1,7
nPa	nPa	k?	nPa
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
tlačí	tlačit	k5eAaImIp3nS	tlačit
na	na	k7c4	na
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
deseti	deset	k4xCc2	deset
zemských	zemský	k2eAgInPc2d1	zemský
průměrů	průměr	k1gInPc2	průměr
(	(	kIx(	(
<g/>
cca	cca	kA	cca
60	[number]	k4	60
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
chvostu	chvost	k1gInSc2	chvost
sahajícího	sahající	k2eAgInSc2d1	sahající
daleko	daleko	k6eAd1	daleko
za	za	k7c4	za
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
magnetosférou	magnetosféra	k1gFnSc7	magnetosféra
a	a	k8xC	a
meziplanetárním	meziplanetární	k2eAgInSc7d1	meziplanetární
magnetických	magnetický	k2eAgNnPc6d1	magnetické
polem	pole	k1gNnSc7	pole
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
magnetopauza	magnetopauza	k1gFnSc1	magnetopauza
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
Země	země	k1gFnSc1	země
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
většině	většina	k1gFnSc3	většina
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
musí	muset	k5eAaImIp3nP	muset
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
sledovat	sledovat	k5eAaImF	sledovat
siločáry	siločára	k1gFnPc4	siločára
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
tedy	tedy	k9	tedy
plní	plnit	k5eAaImIp3nS	plnit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
které	který	k3yRgFnSc2	který
by	by	kYmCp3nS	by
život	život	k1gInSc1	život
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
planet	planeta	k1gFnPc2	planeta
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
magnetismus	magnetismus	k1gInSc4	magnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
planetou	planeta	k1gFnSc7	planeta
je	být	k5eAaImIp3nS	být
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
kterému	který	k3yQgMnSc3	který
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pozemské	pozemský	k2eAgNnSc1d1	pozemské
pole	pole	k1gNnSc1	pole
100	[number]	k4	100
<g/>
x	x	k?	x
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
obří	obří	k2eAgFnPc1d1	obří
plynné	plynný	k2eAgFnPc1d1	plynná
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
taktéž	taktéž	k?	taktéž
vlastní	vlastní	k2eAgInSc1d1	vlastní
magnetismus	magnetismus	k1gInSc1	magnetismus
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
magnetismus	magnetismus	k1gInSc4	magnetismus
pouze	pouze	k6eAd1	pouze
indukovaný	indukovaný	k2eAgMnSc1d1	indukovaný
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
Jižní	jižní	k2eAgInSc1d1	jižní
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
Magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
planet	planeta	k1gFnPc2	planeta
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Zeme	Zem	k1gFnSc2	Zem
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
