<s>
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Br	br	k0	br
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Bromum	Bromum	k1gInSc1	Bromum
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
halogenů	halogen	k1gInPc2	halogen
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
,	,	kIx,	,
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Brom	brom	k1gInSc1	brom
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ochotně	ochotně	k6eAd1	ochotně
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
prvků	prvek	k1gInPc2	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
Antoinem	Antoin	k1gMnSc7	Antoin
Balardem	Balard	k1gMnSc7	Balard
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
brom	brom	k1gInSc1	brom
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
některých	některý	k3yIgNnPc2	některý
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
sloučeniny	sloučenina	k1gFnPc1	sloučenina
bromu	brom	k1gInSc2	brom
analogické	analogický	k2eAgFnPc1d1	analogická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgNnSc1d1	relativní
zastoupení	zastoupení	k1gNnSc1	zastoupení
bromu	brom	k1gInSc2	brom
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
brom	brom	k1gInSc1	brom
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
většina	většina	k1gFnSc1	většina
bromu	brom	k1gInSc2	brom
přítomného	přítomný	k2eAgInSc2d1	přítomný
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jeho	jeho	k3xOp3gNnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
67	[number]	k4	67
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Vesmíru	vesmír	k1gInSc6	vesmír
na	na	k7c4	na
1	[number]	k4	1
atom	atom	k1gInSc4	atom
bromu	brom	k1gInSc2	brom
připadá	připadat	k5eAaPmIp3nS	připadat
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Brom	brom	k1gInSc1	brom
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
chlorováním	chlorování	k1gNnSc7	chlorování
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
solanky	solanka	k1gFnSc2	solanka
(	(	kIx(	(
<g/>
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
roztoku	roztok	k1gInSc2	roztok
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
<g/>
)	)	kIx)	)
při	při	k7c6	při
pH	ph	kA	ph
kolem	kolem	k7c2	kolem
3,5	[number]	k4	3,5
<g/>
.	.	kIx.	.
</s>
<s>
Vyloučený	vyloučený	k2eAgInSc1d1	vyloučený
elementární	elementární	k2eAgInSc1d1	elementární
brom	brom	k1gInSc1	brom
se	se	k3xPyFc4	se
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
probubláním	probublání	k1gNnSc7	probublání
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
kondenzací	kondenzace	k1gFnSc7	kondenzace
ochlazením	ochlazení	k1gNnSc7	ochlazení
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
brom	brom	k1gInSc1	brom
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc1d1	silné
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
nízkému	nízký	k2eAgInSc3d1	nízký
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
odpařuje	odpařovat	k5eAaImIp3nS	odpařovat
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
páry	pár	k1gInPc4	pár
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
smrt	smrt	k1gFnSc4	smrt
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
koncentracích	koncentrace	k1gFnPc6	koncentrace
však	však	k9	však
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
pokožku	pokožka	k1gFnSc4	pokožka
a	a	k8xC	a
především	především	k9	především
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
brom	brom	k1gInSc1	brom
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Br	br	k0	br
-	-	kIx~	-
<g/>
,	,	kIx,	,
Br	br	k0	br
+	+	kIx~	+
<g/>
,	,	kIx,	,
Br	br	k0	br
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Br	br	k0	br
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
a	a	k8xC	a
Br	br	k0	br
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
z	z	k7c2	z
uvedených	uvedený	k2eAgNnPc2d1	uvedené
mocenství	mocenství	k1gNnPc2	mocenství
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
brom	brom	k1gInSc1	brom
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
.	.	kIx.	.
jedinou	jediný	k2eAgFnSc7d1	jediná
bezkyslíkatou	bezkyslíkatý	k2eAgFnSc7d1	bezkyslíkatá
kyselinou	kyselina	k1gFnSc7	kyselina
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
bromovodíková	bromovodíkový	k2eAgFnSc1d1	bromovodíkový
HBr	HBr	k1gFnSc1	HBr
kyselina	kyselina	k1gFnSc1	kyselina
bromná	bromný	k2eAgFnSc1d1	bromný
HBrO	HBrO	k1gFnSc1	HBrO
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
valenci	valence	k1gFnSc4	valence
Br	br	k0	br
<g/>
+	+	kIx~	+
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Br	br	k0	br
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
kyselina	kyselina	k1gFnSc1	kyselina
bromitá	bromitat	k5eAaPmIp3nS	bromitat
HBrO	HBrO	k1gFnSc4	HBrO
<g/>
2	[number]	k4	2
kyselina	kyselina	k1gFnSc1	kyselina
bromičná	bromičný	k2eAgFnSc1d1	bromičný
HBrO	HBrO	k1gFnSc1	HBrO
<g/>
3	[number]	k4	3
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
valenci	valence	k1gFnSc4	valence
Br	br	k0	br
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
Praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
soli	sůl	k1gFnSc2	sůl
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
bromid	bromid	k1gInSc1	bromid
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
<g/>
,	,	kIx,	,
AgBr	AgBr	k1gInSc1	AgBr
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
využití	využití	k1gNnSc1	využití
ve	v	k7c6	v
fotografickém	fotografický	k2eAgInSc6d1	fotografický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
sloučeniny	sloučenina	k1gFnPc4	sloučenina
bromu	brom	k1gInSc2	brom
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
bromoform	bromoform	k1gInSc1	bromoform
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
některé	některý	k3yIgFnPc1	některý
bromované	bromovaný	k2eAgFnPc1d1	bromovaný
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
zhášeče	zhášeč	k1gInSc2	zhášeč
nebo	nebo	k8xC	nebo
zpomalovače	zpomalovač	k1gInSc2	zpomalovač
hoření	hoření	k1gNnSc2	hoření
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
např.	např.	kA	např.
o	o	k7c4	o
polybromované	polybromovaný	k2eAgInPc4d1	polybromovaný
difenyletery	difenyleter	k1gInPc4	difenyleter
(	(	kIx(	(
<g/>
PBDE	PBDE	kA	PBDE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hexabromcyklododekan	hexabromcyklododekan	k1gMnSc1	hexabromcyklododekan
(	(	kIx(	(
<g/>
HBCD	HBCD	kA	HBCD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polybromované	polybromovaný	k2eAgInPc1d1	polybromovaný
bifenyly	bifenyl	k1gInPc1	bifenyl
(	(	kIx(	(
<g/>
PBB	PBB	kA	PBB
<g/>
)	)	kIx)	)
a	a	k8xC	a
bromované	bromovaný	k2eAgFnPc4d1	bromovaný
bisfenoly	bisfenola	k1gFnPc4	bisfenola
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tetrabrombisfenol	tetrabrombisfenol	k1gInSc1	tetrabrombisfenol
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc1	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
brom	brom	k1gInSc1	brom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
brom	brom	k1gInSc1	brom
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
