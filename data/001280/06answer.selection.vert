<s>
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Br	br	k0	br
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Bromum	Bromum	k1gInSc1	Bromum
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
halogenů	halogen	k1gInPc2	halogen
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
,	,	kIx,	,
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
