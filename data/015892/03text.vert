<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
praotec	praotec	k1gMnSc1
Čech	Čechy	k1gFnPc2
Josef	Josef	k1gMnSc1
Mathauser	Mathauser	k1gMnSc1
-	-	kIx~
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
na	na	k7c6
hoře	hora	k1gFnSc6
Říp	Říp	k1gInSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
kníže	kníže	k1gMnSc1
Lech	Lech	k1gMnSc1
a	a	k8xC
kníže	kníže	k1gMnSc1
Rus	Rus	k1gMnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
je	být	k5eAaImIp3nS
legendární	legendární	k2eAgMnSc1d1
prapředek	prapředek	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
podle	podle	k7c2
českých	český	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
o	o	k7c6
svém	svůj	k3xOyFgInSc6
původu	původ	k1gInSc6
přivedl	přivést	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
lid	lid	k1gInSc4
do	do	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
po	po	k7c6
něm	on	k3xPp3gNnSc6
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
Čechy	Čechy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
kmene	kmen	k1gInSc2
či	či	k8xC
národa	národ	k1gInSc2
podle	podle	k7c2
postavy	postava	k1gFnSc2
řadí	řadit	k5eAaImIp3nS
Čecha	Čech	k1gMnSc4
mezi	mezi	k7c4
takzvané	takzvaný	k2eAgMnPc4d1
eponymní	eponymní	k2eAgMnPc4d1
hrdiny	hrdina	k1gMnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
římský	římský	k2eAgMnSc1d1
Romulus	Romulus	k1gMnSc1
<g/>
,	,	kIx,
řecký	řecký	k2eAgInSc4d1
Hellén	Hellén	k1gInSc4
nebo	nebo	k8xC
francký	francký	k2eAgInSc4d1
Francion	Francion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kosmově	Kosmův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
je	být	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
jménem	jméno	k1gNnSc7
Boemus	Boemus	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
latinským	latinský	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Čech	Čech	k1gMnSc1
„	„	k?
<g/>
Bohemia	bohemia	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
odvozeným	odvozený	k2eAgFnPc3d1
od	od	k7c2
keltského	keltský	k2eAgInSc2d1
kmene	kmen	k1gInSc2
Bójů	Bój	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jméno	jméno	k1gNnSc1
Čech	Čechy	k1gFnPc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
až	až	k9
v	v	k7c6
Dalimilově	Dalimilův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
mu	on	k3xPp3gNnSc3
v	v	k7c6
různých	různý	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
přisuzován	přisuzován	k2eAgInSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kosmově	Kosmův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
pouze	pouze	k6eAd1
jako	jako	k8xC,k8xS
stařešina	stařešina	k1gMnSc1
(	(	kIx(
<g/>
senior	senior	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
pán	pán	k1gMnSc1
<g/>
,	,	kIx,
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
mu	on	k3xPp3gMnSc3
přisuzuje	přisuzovat	k5eAaImIp3nS
nejasný	jasný	k2eNgInSc1d1
titul	titul	k1gInSc1
lech	lecha	k1gFnPc2
a	a	k8xC
Hájkova	Hájkův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
titul	titul	k1gInSc4
knížete	kníže	k1gMnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
českých	český	k2eAgMnPc2d1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
vojvoda	vojvoda	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
dílech	díl	k1gInPc6
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
přisuzován	přisuzován	k2eAgInSc1d1
bratr	bratr	k1gMnSc1
jménem	jméno	k1gNnSc7
Lech	Lech	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
zase	zase	k9
prapředkem	prapředek	k1gMnSc7
Poláků	Polák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
postava	postava	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
nejdříve	dříve	k6eAd3
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
uherských	uherský	k2eAgFnPc6d1
a	a	k8xC
polských	polský	k2eAgFnPc6d1
kronikách	kronika	k1gFnPc6
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
společně	společně	k6eAd1
s	s	k7c7
postavou	postava	k1gFnSc7
jménem	jméno	k1gNnSc7
Rus	Rus	k1gMnSc1
(	(	kIx(
<g/>
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
Lech	Lech	k1gMnSc1
a	a	k8xC
Rus	Rus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českých	český	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
až	až	k9
v	v	k7c6
kronice	kronika	k1gFnSc6
Přibíka	Přibík	k1gMnSc2
Pulkava	Pulkava	k1gFnSc1
z	z	k7c2
Radenína	Radenín	k1gInSc2
sepsané	sepsaný	k2eAgFnPc4d1
na	na	k7c6
konci	konec	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postava	postava	k1gFnSc1
Lecha	lecha	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
objevovala	objevovat	k5eAaImAgFnS
i	i	k9
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
dílech	dílo	k1gNnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
Hájkově	Hájkův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
či	či	k8xC
Starých	Starých	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
českých	český	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kdy	kdy	k6eAd1
legendární	legendární	k2eAgFnSc1d1
postava	postava	k1gFnSc1
měla	mít	k5eAaImAgFnS
žít	žít	k5eAaImF
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
logiky	logika	k1gFnSc2
věci	věc	k1gFnSc2
(	(	kIx(
<g/>
příchod	příchod	k1gInSc1
Slovanů	Slovan	k1gInPc2
do	do	k7c2
české	český	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
)	)	kIx)
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
raný	raný	k2eAgInSc4d1
středověk	středověk	k1gInSc4
okolo	okolo	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hájkově	Hájkův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
je	být	k5eAaImIp3nS
uváděno	uváděn	k2eAgNnSc1d1
datum	datum	k1gNnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
příchodu	příchod	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
644	#num#	k4
a	a	k8xC
smrti	smrt	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
661	#num#	k4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
není	být	k5eNaImIp3nS
nijak	nijak	k6eAd1
doloženo	doložen	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc1
působení	působení	k1gNnSc1
krylo	krýt	k5eAaImAgNnS
s	s	k7c7
existencí	existence	k1gFnSc7
Sámovy	Sámův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
Lech	Lech	k1gMnSc1
a	a	k8xC
Čech	Čech	k1gMnSc1
dřevoryt	dřevoryt	k1gInSc4
z	z	k7c2
Kroniky	kronika	k1gFnSc2
Matěje	Matěj	k1gMnSc2
Miechowského	Miechowský	k1gMnSc2
</s>
<s>
Chronica	Chronica	k6eAd1
Boemorum	Boemorum	k1gNnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
Bratři	bratr	k1gMnPc1
Čech	Čech	k1gMnSc1
a	a	k8xC
Lech	Lech	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
</s>
<s>
V	v	k7c6
latinsky	latinsky	k6eAd1
psané	psaný	k2eAgFnSc6d1
Kosmově	Kosmův	k2eAgFnSc6d1
Kronice	kronika	k1gFnSc6
Čechů	Čech	k1gMnPc2
se	se	k3xPyFc4
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c6
příchodu	příchod	k1gInSc6
bezejmenného	bezejmenný	k2eAgInSc2d1
lidu	lid	k1gInSc2
do	do	k7c2
dnešních	dnešní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
„	„	k?
</s>
<s>
(	(	kIx(
<g/>
…	…	k?
<g/>
)	)	kIx)
tuším	tušit	k5eAaImIp1nS
kolem	kolem	k7c2
hory	hora	k1gFnSc2
Řípu	Říp	k1gInSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
řekami	řeka	k1gFnPc7
<g/>
,	,	kIx,
Ohří	Ohře	k1gFnSc7
a	a	k8xC
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
prvá	prvý	k4xOgNnPc1
zařídil	zařídit	k5eAaPmAgMnS
svá	svůj	k3xOyFgNnPc4
sídla	sídlo	k1gNnPc4
<g/>
,	,	kIx,
prvá	prvý	k4xOgNnPc1
založil	založit	k5eAaPmAgMnS
obydlí	obydlí	k1gNnPc4
a	a	k8xC
radostně	radostně	k6eAd1
na	na	k7c6
zemi	zem	k1gFnSc6
postavil	postavit	k5eAaPmAgMnS
bůžky	bůžek	k1gMnPc4
(	(	kIx(
<g/>
penates	penates	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
sebou	se	k3xPyFc7
na	na	k7c4
ramenou	rameno	k1gNnPc6
přinesl	přinést	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
stařešina	stařešina	k1gMnSc1
(	(	kIx(
<g/>
senior	senior	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
ostatní	ostatní	k1gNnSc4
jako	jako	k9
pána	pán	k1gMnSc4
provázeli	provázet	k5eAaImAgMnP
<g/>
,	,	kIx,
mezi	mezi	k7c7
jinými	jiný	k1gMnPc7
takto	takto	k6eAd1
promluvil	promluvit	k5eAaPmAgMnS
k	k	k7c3
své	svůj	k3xOyFgFnSc3
družině	družina	k1gFnSc3
<g/>
:	:	kIx,
‚	‚	k?
<g/>
Druhové	druh	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
jste	být	k5eAaImIp2nP
nejednou	jednou	k6eNd1
snášeli	snášet	k5eAaImAgMnP
se	s	k7c7
mnou	já	k3xPp1nSc7
těžké	těžký	k2eAgInPc1d1
trudy	trud	k1gInPc1
cesty	cesta	k1gFnSc2
po	po	k7c6
neschůdných	schůdný	k2eNgInPc6d1
lesích	les	k1gInPc6
<g/>
,	,	kIx,
zastavte	zastavit	k5eAaPmRp2nP
se	se	k3xPyFc4
a	a	k8xC
obětujte	obětovat	k5eAaBmRp2nP
svým	svůj	k3xOyFgMnPc3
bůžkům	bůžek	k1gMnPc3
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
zázračnou	zázračný	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
jste	být	k5eAaImIp2nP
konečně	konečně	k6eAd1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
této	tento	k3xDgFnSc2
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
osudem	osud	k1gInSc7
vám	vy	k3xPp2nPc3
předurčené	předurčený	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
'	'	kIx"
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Následuje	následovat	k5eAaImIp3nS
Čechova	Čechův	k2eAgFnSc1d1
oslavná	oslavný	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
na	na	k7c4
zem	zem	k1gFnSc4
do	do	k7c2
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
dorazili	dorazit	k5eAaPmAgMnP
a	a	k8xC
otázka	otázka	k1gFnSc1
jak	jak	k6eAd1
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
pojmenován	pojmenovat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
lid	lid	k1gInSc4
mu	on	k3xPp3gMnSc3
odpověděl	odpovědět	k5eAaPmAgMnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Poněvadž	poněvadž	k8xS
ty	ten	k3xDgFnPc1
otče	otec	k1gMnSc5
<g/>
,	,	kIx,
se	se	k3xPyFc4
jmenuješ	jmenovat	k5eAaBmIp2nS,k5eAaImIp2nS
Čech	Čechy	k1gFnPc2
(	(	kIx(
<g/>
Boemus	Boemus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
najdeme	najít	k5eAaPmIp1nP
lepší	dobrý	k2eAgNnSc4d2
a	a	k8xC
vhodnější	vhodný	k2eAgNnSc4d2
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nS
i	i	k9
země	země	k1gFnSc1
slula	slout	k5eAaImAgFnS
Čechy	Čechy	k1gFnPc4
(	(	kIx(
<g/>
Boemia	Boemia	k1gFnSc1
<g/>
)	)	kIx)
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
Čech	Čech	k1gMnSc1
dojat	dojat	k2eAgMnSc1d1
a	a	k8xC
pronesl	pronést	k5eAaPmAgInS
další	další	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
k	k	k7c3
nové	nový	k2eAgFnSc3d1
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
</s>
<s>
V	v	k7c6
česky	česky	k6eAd1
psané	psaný	k2eAgFnSc6d1
veršované	veršovaný	k2eAgFnSc6d1
Kronice	kronika	k1gFnSc6
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc4d1
Dalimila	Dalimil	k1gMnSc4
z	z	k7c2
počátku	počátek	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
motiv	motiv	k1gInSc4
Charvátské	charvátský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
vraždy	vražda	k1gFnSc2
<g/>
,	,	kIx,
výstup	výstup	k1gInSc4
na	na	k7c4
Říp	Říp	k1gInSc4
a	a	k8xC
titul	titul	k1gInSc4
lech	lech	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Leží	ležet	k5eAaImIp3nS
země	země	k1gFnSc1
v	v	k7c6
srbském	srbský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
Charváty	Charváty	k1gFnPc1
ji	on	k3xPp3gFnSc4
nazývají	nazývat	k5eAaImIp3nP
</s>
<s>
a	a	k8xC
v	v	k7c6
ní	on	k3xPp3gFnSc6
kdysi	kdysi	k6eAd1
vládl	vládnout	k5eAaImAgMnS
lech	lech	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nosil	nosit	k5eAaImAgInS
jméno	jméno	k1gNnSc4
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Neboť	neboť	k8xC
dopustil	dopustit	k5eAaPmAgMnS
se	se	k3xPyFc4
vraždy	vražda	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
zbaven	zbaven	k2eAgMnSc1d1
domova	domov	k1gInSc2
byl	být	k5eAaImAgInS
navždy	navždy	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Kronika	kronika	k1gFnSc1
Přibíka	Přibík	k1gMnSc2
Pulkavy	Pulkava	k1gFnSc2
z	z	k7c2
Radenína	Radenín	k1gInSc2
</s>
<s>
V	v	k7c6
Kronice	kronika	k1gFnSc6
české	český	k2eAgFnSc2d1
Přibíka	Přibík	k1gMnSc4
Pulkavy	Pulkava	k1gFnSc2
z	z	k7c2
Radenína	Radenín	k1gInSc2
z	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
příběh	příběh	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Čech	Čech	k1gMnSc1
je	být	k5eAaImIp3nS
líčen	líčen	k2eAgMnSc1d1
jak	jak	k8xC,k8xS
obyčejný	obyčejný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
stařešina	stařešina	k1gMnSc1
či	či	k8xC
lech	lech	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
však	však	k9
postava	postava	k1gFnSc1
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
bratra	bratr	k1gMnSc2
Lecha	Lech	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
osídlil	osídlit	k5eAaPmAgMnS
Polsko	Polsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hájkova	Hájkův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
</s>
<s>
Kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
Václava	Václav	k1gMnSc4
Hájka	Hájek	k1gMnSc4
z	z	k7c2
Libočan	Libočan	k1gMnSc1
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předchozích	předchozí	k2eAgNnPc2d1
zpracování	zpracování	k1gNnPc2
látky	látka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
přináší	přinášet	k5eAaImIp3nS
řadu	řada	k1gFnSc4
nových	nový	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
považovaných	považovaný	k2eAgFnPc2d1
za	za	k7c4
autorovu	autorův	k2eAgFnSc4d1
literární	literární	k2eAgFnSc4d1
invenci	invence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
právě	právě	k6eAd1
toto	tento	k3xDgNnSc4
dílo	dílo	k1gNnSc4
svojí	svojit	k5eAaImIp3nS
velkou	velký	k2eAgFnSc7d1
čteností	čtenost	k1gFnSc7
zajistilo	zajistit	k5eAaPmAgNnS
popularizaci	popularizace	k1gFnSc4
příběhu	příběh	k1gInSc2
o	o	k7c6
Čechovi	Čech	k1gMnSc6
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
kanonickým	kanonický	k2eAgNnSc7d1
až	až	k8xS
do	do	k7c2
dob	doba	k1gFnPc2
Jiráskova	Jiráskův	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příchod	příchod	k1gInSc1
Čecha	Čech	k1gMnSc2
do	do	k7c2
nové	nový	k2eAgFnSc2d1
vlasti	vlast	k1gFnSc2
je	být	k5eAaImIp3nS
Hájkem	hájek	k1gInSc7
datován	datovat	k5eAaImNgMnS
do	do	k7c2
roku	rok	k1gInSc2
644	#num#	k4
a	a	k8xC
v	v	k7c6
charvátské	charvátský	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
přisouzen	přisoudit	k5eAaPmNgInS
hrad	hrad	k1gInSc1
Psáry	Psára	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnSc1
bratru	bratru	k9
Lechovi	Lech	k1gMnSc3
Krapina	Krapin	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
je	být	k5eAaImIp3nS
jmenován	jmenován	k2eAgMnSc1d1
Čechův	Čechův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Kleň	klenout	k5eAaImRp2nS
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
Klenče	Kleneč	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Hájkově	Hájkův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
je	být	k5eAaImIp3nS
Čechovi	Čech	k1gMnSc3
přisouzen	přisouzen	k2eAgInSc4d1
hrob	hrob	k1gInSc4
v	v	k7c6
obci	obec	k1gFnSc6
Ctiněves	Ctiněvesa	k1gFnPc2
na	na	k7c6
úpatí	úpatí	k1gNnSc6
Řípu	Říp	k1gInSc2
kde	kde	k6eAd1
měl	mít	k5eAaImAgMnS
roku	rok	k1gInSc2
661	#num#	k4
skonat	skonat	k5eAaPmF
a	a	k8xC
být	být	k5eAaImF
pochován	pochován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tomto	tento	k3xDgInSc6
hrobu	hrob	k1gInSc3
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
pověstí	pověst	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literární	literární	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
</s>
<s>
Legenda	legenda	k1gFnSc1
o	o	k7c6
praotci	praotec	k1gMnSc6
Čechu	Čech	k1gMnSc6
</s>
<s>
Čech	Čech	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
díle	dílo	k1gNnSc6
Legenda	legenda	k1gFnSc1
o	o	k7c6
praotci	praotec	k1gMnSc6
Čechu	Čech	k1gMnSc6
Sofie	Sofia	k1gFnSc2
Podlipské	Podlipský	k2eAgFnSc6d1
z	z	k7c2
roku	rok	k1gInSc2
1888	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
představen	představit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
charvátský	charvátský	k2eAgMnSc1d1
válečník	válečník	k1gMnSc1
bojující	bojující	k2eAgMnSc1d1
proti	proti	k7c3
Hunům	Hun	k1gMnPc3
a	a	k8xC
Markomanům	Markoman	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvuje	navštěvovat	k5eAaImIp3nS
také	také	k9
budoucí	budoucí	k2eAgMnPc4d1
Čechy	Čech	k1gMnPc4
kde	kde	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
keltských	keltský	k2eAgInPc2d1
motivů	motiv	k1gInPc2
jako	jako	k8xS,k8xC
korkonti	korkonti	k1gNnSc4
„	„	k?
<g/>
krkonošští	krkonošský	k2eAgMnPc1d1
obři	obr	k1gMnPc1
<g/>
“	“	k?
a	a	k8xC
druidi	druid	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
se	se	k3xPyFc4
přátelsky	přátelsky	k6eAd1
stýká	stýkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
zmíněn	zmíněn	k2eAgInSc1d1
hrad	hrad	k1gInSc1
Zvíkov	Zvíkov	k1gInSc1
<g/>
,	,	kIx,
údajně	údajně	k6eAd1
vystavěný	vystavěný	k2eAgInSc1d1
Římany	Říman	k1gMnPc4
bojujícími	bojující	k2eAgFnPc7d1
proti	proti	k7c3
Markomanům	Markoman	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
české	český	k2eAgFnSc2d1
</s>
<s>
Ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
českých	český	k2eAgFnPc6d1
Aloise	Alois	k1gMnSc2
Jiráska	Jirásek	k1gMnSc2
je	být	k5eAaImIp3nS
příběh	příběh	k1gInSc4
o	o	k7c6
Čechovi	Čech	k1gMnSc6
již	již	k6eAd1
silně	silně	k6eAd1
rozvinut	rozvinut	k2eAgInSc1d1
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
pozdních	pozdní	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
následuje	následovat	k5eAaImIp3nS
počátek	počátek	k1gInSc4
vypravování	vypravování	k1gNnSc2
z	z	k7c2
této	tento	k3xDgFnSc2
knihy	kniha	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Za	za	k7c7
Tatrami	Tatra	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
rovinách	rovina	k1gFnPc6
při	při	k7c6
řece	řeka	k1gFnSc6
Visle	Visla	k1gFnSc6
rozkládala	rozkládat	k5eAaImAgFnS
se	se	k3xPyFc4
od	od	k7c2
nepaměti	nepaměť	k1gFnSc2
charvátská	charvátský	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
prvotní	prvotní	k2eAgFnSc2d1
veliké	veliký	k2eAgFnSc2d1
vlasti	vlast	k1gFnSc2
slovanské	slovanský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
charvátské	charvátský	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
bytovala	bytovat	k5eAaImAgNnP
četná	četný	k2eAgNnPc1d1
plemena	plemeno	k1gNnPc1
<g/>
,	,	kIx,
příbuzná	příbuzný	k2eAgFnSc1d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
mravy	mrav	k1gInPc4
<g/>
,	,	kIx,
způsobem	způsob	k1gInSc7
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
strhly	strhnout	k5eAaPmAgFnP
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
vády	váda	k1gFnPc1
a	a	k8xC
krvavé	krvavý	k2eAgInPc1d1
boje	boj	k1gInPc1
o	o	k7c4
meze	mez	k1gFnPc4
a	a	k8xC
dědiny	dědina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstal	vstát	k5eAaPmAgMnS
rod	rod	k1gInSc4
na	na	k7c4
rod	rod	k1gInSc4
<g/>
,	,	kIx,
příbuzní	příbuzný	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
příbuzným	příbuzný	k1gMnPc3
a	a	k8xC
hubili	hubit	k5eAaImAgMnP
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
ten	ten	k3xDgInSc4
čas	čas	k1gInSc4
dva	dva	k4xCgInPc4
bratři	bratr	k1gMnPc1
mocného	mocný	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
vojvodové	vojvoda	k1gMnPc1
<g/>
,	,	kIx,
Čech	Čech	k1gMnSc1
a	a	k8xC
Lech	Lech	k1gMnSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
se	se	k3xPyFc4
o	o	k7c4
to	ten	k3xDgNnSc4
snesli	snést	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
opustí	opustit	k5eAaPmIp3nS
rodnou	rodný	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
bojem	boj	k1gInSc7
neblahou	blahý	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekli	říct	k5eAaPmAgMnP
si	se	k3xPyFc3
<g/>
:	:	kIx,
„	„	k?
<g/>
Vyhledejme	vyhledat	k5eAaPmRp1nP
sobě	se	k3xPyFc3
nových	nový	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
<g/>
,	,	kIx,
kdež	kdež	k9
by	by	kYmCp3nS
náš	náš	k3xOp1gInSc1
rod	rod	k1gInSc1
žil	žít	k5eAaImAgInS
s	s	k7c7
pokojem	pokoj	k1gInSc7
a	a	k8xC
díla	dílo	k1gNnSc2
si	se	k3xPyFc3
hleděl	hledět	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Na	na	k7c6
českém	český	k2eAgInSc6d1
internetu	internet	k1gInSc6
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
pověst	pověst	k1gFnSc4
o	o	k7c6
dceři	dcera	k1gFnSc6
Čecha	Čech	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zamilovala	zamilovat	k5eAaPmAgFnS
do	do	k7c2
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
její	její	k3xOp3gFnSc6
otec	otec	k1gMnSc1
nesnášel	snášet	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
milým	milý	k1gMnSc7
utekla	utéct	k5eAaPmAgFnS
na	na	k7c4
jih	jih	k1gInSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
po	po	k7c6
letech	léto	k1gNnPc6
Čech	Čechy	k1gFnPc2
potkal	potkat	k5eAaPmAgInS
a	a	k8xC
usmířil	usmířit	k5eAaPmAgInS
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
odmítli	odmítnout	k5eAaPmAgMnP
odstěhovat	odstěhovat	k5eAaPmF
zpět	zpět	k6eAd1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
na	na	k7c6
tom	ten	k3xDgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
pozdějším	pozdní	k2eAgInSc6d2
Zvíkově	Zvíkov	k1gInSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
již	již	k6eAd1
zvykli	zvyknout	k5eAaPmAgMnP
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výklad	výklad	k1gInSc1
</s>
<s>
Většina	většina	k1gFnSc1
historiků	historik	k1gMnPc2
považovala	považovat	k5eAaImAgFnS
příběh	příběh	k1gInSc4
o	o	k7c6
Čechovi	Čech	k1gMnSc6
za	za	k7c7
ohlas	ohlas	k1gInSc1
<g/>
,	,	kIx,
přinejmenším	přinejmenším	k6eAd1
vzdálený	vzdálený	k2eAgInSc1d1
<g/>
,	,	kIx,
„	„	k?
<g/>
pravé	pravá	k1gFnSc6
lidové	lidový	k2eAgFnSc2d1
<g/>
“	“	k?
pověsti	pověst	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
hledala	hledat	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
určitý	určitý	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
základ	základ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
autentické	autentický	k2eAgInPc4d1
byly	být	k5eAaImAgFnP
považovány	považován	k2eAgInPc1d1
již	již	k6eAd1
Pavlem	Pavel	k1gMnSc7
Josefem	Josef	k1gMnSc7
Šafaříkem	Šafařík	k1gMnSc7
a	a	k8xC
Františkem	František	k1gMnSc7
Palackým	Palacký	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
badateli	badatel	k1gMnPc7
pozdějšími	pozdní	k2eAgMnPc7d2
<g/>
,	,	kIx,
motivy	motiv	k1gInPc1
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
až	až	k9
v	v	k7c6
Dalimilově	Dalimilův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
příchod	příchod	k1gInSc1
Čechů	Čech	k1gMnPc2
z	z	k7c2
Charvátska	Charvátsko	k1gNnSc2
(	(	kIx(
<g/>
Charvátská	charvátský	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autenticitu	autenticita	k1gFnSc4
Kosmova	Kosmův	k2eAgNnSc2d1
vyprávění	vyprávění	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
následovníků	následovník	k1gMnPc2
<g/>
,	,	kIx,
odmítl	odmítnout	k5eAaPmAgMnS
v	v	k7c6
duchu	duch	k1gMnSc6
pozitivismu	pozitivismus	k1gInSc2
Lubor	Lubor	k1gMnSc1
Niederle	Niederle	k1gFnSc2
a	a	k8xC
považoval	považovat	k5eAaImAgMnS
jej	on	k3xPp3gMnSc4
za	za	k7c4
literární	literární	k2eAgFnSc4d1
fikci	fikce	k1gFnSc4
<g/>
,	,	kIx,
přinejlepším	přinejlepším	k6eAd1
inspirovaný	inspirovaný	k2eAgMnSc1d1
nějakou	nějaký	k3yIgFnSc7
místní	místní	k2eAgFnSc7d1
pověstí	pověst	k1gFnSc7
o	o	k7c6
Řípu	Říp	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Kosmově	Kosmův	k2eAgNnSc6d1
líčení	líčení	k1gNnSc6
příchodu	příchod	k1gInSc2
do	do	k7c2
Čech	Čechy	k1gFnPc2
je	být	k5eAaImIp3nS
nalézána	nalézán	k2eAgFnSc1d1
řada	řada	k1gFnSc1
shod	shoda	k1gFnPc2
s	s	k7c7
Vergiliovou	Vergiliův	k2eAgFnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
Aeneidou	Aeneida	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
biblický	biblický	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
příchodu	příchod	k1gInSc6
Izraelitů	izraelita	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
Mojžíšem	Mojžíš	k1gMnSc7
do	do	k7c2
Kanaánu	Kanaán	k1gInSc2
<g/>
,	,	kIx,
Dalimilův	Dalimilův	k2eAgInSc4d1
motiv	motiv	k1gInSc4
odchodu	odchod	k1gInSc2
z	z	k7c2
domova	domov	k1gInSc2
z	z	k7c2
důvodu	důvod	k1gInSc2
spáchané	spáchaný	k2eAgFnSc2d1
vraždy	vražda	k1gFnSc2
zase	zase	k9
připomíná	připomínat	k5eAaImIp3nS
Mojžíšův	Mojžíšův	k2eAgInSc4d1
odchod	odchod	k1gInSc4
z	z	k7c2
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
amatérští	amatérský	k2eAgMnPc1d1
badatelé	badatel	k1gMnPc1
spojují	spojovat	k5eAaImIp3nP
postavu	postava	k1gFnSc4
Čech	Čechy	k1gFnPc2
s	s	k7c7
dřívějším	dřívější	k2eAgNnSc7d1
keltským	keltský	k2eAgNnSc7d1
osídlením	osídlení	k1gNnSc7
a	a	k8xC
považují	považovat	k5eAaImIp3nP
tak	tak	k6eAd1
za	za	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
předobraz	předobraz	k1gInSc4
například	například	k6eAd1
Segovese	Segovese	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charvátsko	Charvátsko	k1gNnSc1
</s>
<s>
V	v	k7c6
Dalimilově	Dalimilův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
je	být	k5eAaImIp3nS
uváděna	uváděn	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
vlast	vlast	k1gFnSc1
Čecha	Čech	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc3
lidu	lid	k1gInSc3
Charvátsko	Charvátsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
všeobecně	všeobecně	k6eAd1
přijímaného	přijímaný	k2eAgInSc2d1
názoru	názor	k1gInSc2
o	o	k7c6
příchodu	příchod	k1gInSc6
Čechů	Čech	k1gMnPc2
z	z	k7c2
východu	východ	k1gInSc2
nebyla	být	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
země	země	k1gFnSc1
ztotožněna	ztotožněn	k2eAgFnSc1d1
s	s	k7c7
jihoslovanským	jihoslovanský	k2eAgNnSc7d1
Chorvatskem	Chorvatsko	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
Bílým	bílý	k2eAgNnSc7d1
Charvátskem	Charvátsko	k1gNnSc7
zmiňovaným	zmiňovaný	k2eAgNnSc7d1
Konstatinem	Konstatin	k1gMnSc7
Porfyrogenetem	Porfyrogenet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístění	umístění	k1gNnSc1
české	český	k2eAgFnSc2d1
pravlasti	pravlast	k1gFnSc2
na	na	k7c4
Balkán	Balkán	k1gInSc4
by	by	kYmCp3nS
tak	tak	k9
bylo	být	k5eAaImAgNnS
omylem	omylem	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lubor	Lubor	k1gMnSc1
Niederle	Niederle	k1gFnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Dalimilovým	Dalimilův	k2eAgNnSc7d1
přisouzením	přisouzení	k1gNnSc7
balkánské	balkánský	k2eAgFnSc2d1
pravlasti	pravlast	k1gFnSc2
Čechům	Čech	k1gMnPc3
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
učenou	učený	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
Pověsti	pověst	k1gFnSc6
dávných	dávný	k2eAgNnPc2d1
let	léto	k1gNnPc2
o	o	k7c6
ilyrském	ilyrský	k2eAgInSc6d1
či	či	k8xC
panonském	panonský	k2eAgInSc6d1
původu	původ	k1gInSc6
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tohoto	tento	k3xDgInSc2
konceptu	koncept	k1gInSc2
by	by	kYmCp3nS
po	po	k7c6
zmatení	zmatení	k1gNnSc6
jazyků	jazyk	k1gInPc2
museli	muset	k5eAaImAgMnP
předci	předek	k1gMnPc1
Čechů	Čech	k1gMnPc2
putovat	putovat	k5eAaImF
z	z	k7c2
Šineáru	Šineár	k1gInSc2
do	do	k7c2
Čech	Čechy	k1gFnPc2
přes	přes	k7c4
Balkán	Balkán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
této	tento	k3xDgFnSc2
hypotézy	hypotéza	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
že	že	k8xS
na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
geografické	geografický	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ostatně	ostatně	k6eAd1
nebyla	být	k5eNaImAgFnS
středověkému	středověký	k2eAgInSc3d1
myšlení	myšlení	k1gNnSc3
vlastní	vlastní	k2eAgNnSc4d1
<g/>
,	,	kIx,
by	by	kYmCp3nP
musely	muset	k5eAaImAgInP
hledat	hledat	k5eAaImF
své	svůj	k3xOyFgMnPc4
předky	předek	k1gMnPc4
na	na	k7c6
Balkáně	Balkán	k1gInSc6
všechny	všechen	k3xTgInPc4
evropské	evropský	k2eAgInPc4d1
národy	národ	k1gInPc4
<g/>
,	,	kIx,
snad	snad	k9
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgMnPc2
měla	mít	k5eAaImAgFnS
právě	právě	k9
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
vzniknout	vzniknout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VESELÝ	Veselý	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
-	-	kIx~
Staré	Staré	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
české	český	k2eAgFnSc2d1
aneb	aneb	k?
Causa	causa	k1gFnSc1
praotce	praotec	k1gMnSc2
Čecha	Čech	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GRIČOVÁ	GRIČOVÁ	kA
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Topos	Topos	k1gInSc1
příchodu	příchod	k1gInSc2
praotce	praotec	k1gMnSc2
Čecha	Čech	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Ústav	ústav	k1gInSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
komparatistiky	komparatistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Petr	Petr	k1gMnSc1
A.	A.	kA
Bílek	Bílek	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Gričová	Gričová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GRZESIK	GRZESIK	kA
<g/>
,	,	kIx,
Ryszard	Ryszard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Żywot	Żywot	k1gMnSc1
św	św	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefana	Stefan	k1gMnSc4
króla	krónout	k5eAaImAgFnS,k5eAaPmAgFnS,k5eAaBmAgFnS
Węgier	Węgier	k1gInSc4
<g/>
,	,	kIx,
czyli	czyle	k1gFnSc4
Kronika	kronika	k1gFnSc1
Węgiersko-polska	Węgiersko-polska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warszawa	Warszawa	k1gMnSc1
<g/>
:	:	kIx,
Wydawnictwo	Wydawnictwo	k1gMnSc1
„	„	k?
<g/>
DiG	DiG	k1gMnSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8371812760	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FAŁOWSKI	FAŁOWSKI	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biesiada	Biesiada	k1gFnSc1
słowiańska	słowiańska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraków	Kraków	k1gMnSc1
<g/>
:	:	kIx,
Universitas	Universitas	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
23	#num#	k4
<g/>
.	.	kIx.
schůzka	schůzka	k1gFnSc1
<g/>
:	:	kIx,
Staré	Staré	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
české	český	k2eAgFnSc2d1
aneb	aneb	k?
Causa	causa	k1gFnSc1
praotce	praotec	k1gMnSc4
Čecha	Čech	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojka	dvojka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kosmas	Kosmas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
515	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Kosmas	Kosmas	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kosmas	Kosmas	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
↑	↑	k?
Gričová	Gričová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
↑	↑	k?
Gričová	Gričová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
↑	↑	k?
Gričová	Gričová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
<g/>
,	,	kIx,
35	#num#	k4
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Sever	sever	k1gInSc1
-	-	kIx~
Přes	přes	k7c4
Říp	Říp	k1gInSc4
do	do	k7c2
Ctiněvsi	Ctiněvse	k1gFnSc3
k	k	k7c3
hrobu	hrob	k1gInSc3
praotce	praotec	k1gMnSc2
Čecha	Čech	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Gričová	Gričová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
↑	↑	k?
Staré	Staré	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
české	český	k2eAgFnPc4d1
-	-	kIx~
O	o	k7c6
Čechovi	Čech	k1gMnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Záhadolog	Záhadolog	k1gMnSc1
-	-	kIx~
Zvíkov	Zvíkov	k1gInSc1
<g/>
:	:	kIx,
Tajemná	tajemný	k2eAgFnSc1d1
věž	věž	k1gFnSc1
Markomanka	Markomanka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Projekt	projekt	k1gInSc1
záře	zář	k1gFnSc2
-	-	kIx~
Jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
skutečné	skutečný	k2eAgNnSc1d1
stáří	stáří	k1gNnSc1
hradu	hrad	k1gInSc2
Zvíkov	Zvíkov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýty	mýtus	k1gInPc1
kmene	kmen	k1gInSc2
Čechů	Čech	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
646	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
8	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
203	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
<g/>
↑	↑	k?
Kosmas	Kosmas	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
s.	s.	k?
218	#num#	k4
<g/>
↑	↑	k?
Gričová	Gričová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
27	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
↑	↑	k?
Bratrstvo	bratrstvo	k1gNnSc1
Keltů	Kelt	k1gMnPc2
-	-	kIx~
Vliv	vliv	k1gInSc1
Keltů	Kelt	k1gMnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Segovesus	Segovesus	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
-	-	kIx~
Praotec	praotec	k1gMnSc1
Kelt	Kelt	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Boiohaemum	Boiohaemum	k1gNnSc1
-	-	kIx~
Staré	Staré	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
keltské	keltský	k2eAgFnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
203	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
59	#num#	k4
<g/>
↑	↑	k?
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
203	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Dílo	dílo	k1gNnSc4
Rýmovaná	rýmovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
tak	tak	k8xC,k8xS
řečeného	řečený	k2eAgMnSc4d1
Dalimila	Dalimil	k1gMnSc4
<g/>
/	/	kIx~
<g/>
II	II	kA
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Staré	Staré	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
české	český	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
O	o	k7c6
Čechovi	Čech	k1gMnSc6
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
HÁJEK	hájek	k1gInSc1
Z	z	k7c2
LIBOČAN	LIBOČAN	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnSc2
o	o	k7c6
počátcích	počátek	k1gInPc6
českého	český	k2eAgInSc2d1
národu	národ	k1gInSc2
a	a	k8xC
o	o	k7c6
českých	český	k2eAgNnPc6d1
pohanských	pohanský	k2eAgNnPc6d1
knížatech	kníže	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jan	Jan	k1gMnSc1
Kočí	Kočí	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kočí	Kočí	k1gMnSc1
<g/>
,	,	kIx,
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
-	-	kIx~
kapitola	kapitola	k1gFnSc1
Čech	Čech	k1gMnSc1
a	a	k8xC
Lech	Lech	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
Lech	Lech	k1gMnSc1
a	a	k8xC
Rus	Rus	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
praotec	praotec	k1gMnSc1
Čech	Čechy	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
-	-	kIx~
Příchod	příchod	k1gInSc1
lidu	lid	k1gInSc2
praotce	praotec	k1gMnSc2
Čecha	Čech	k1gMnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
-	-	kIx~
Staré	Staré	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
české	český	k2eAgFnSc2d1
aneb	aneb	k?
Causa	causa	k1gFnSc1
praotce	praotec	k1gMnSc4
Čecha	Čech	k1gMnSc4
</s>
<s>
Vývoj	vývoj	k1gInSc1
příběhu	příběh	k1gInSc2
o	o	k7c6
praotci	praotec	k1gMnSc6
Čechovi	Čech	k1gMnSc6
Archivováno	archivován	k2eAgNnSc4d1
3	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Legendární	legendární	k2eAgMnSc1d1
stařešina	stařešina	k1gMnSc1
<g/>
/	/	kIx~
<g/>
pán	pán	k1gMnSc1
<g/>
/	/	kIx~
<g/>
lech	lech	k1gMnSc1
<g/>
/	/	kIx~
<g/>
kníže	kníže	k1gMnSc1
<g/>
/	/	kIx~
<g/>
vojvoda	vojvoda	k1gMnSc1
Čechů	Čech	k1gMnPc2
Čech	Čech	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Krok	krok	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnPc1
mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Krok	krok	k1gInSc1
•	•	k?
Libuše	Libuše	k1gFnSc1
Mytičtí	mytický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Oráč	oráč	k1gMnSc1
•	•	k?
Nezamysl	Nezamysl	k1gMnSc1
•	•	k?
Mnata	Mnata	k1gFnSc1
•	•	k?
Vojen	vojna	k1gFnPc2
•	•	k?
Vnislav	Vnislav	k1gMnSc1
•	•	k?
Křesomysl	Křesomysl	k1gMnSc1
•	•	k?
Neklan	Neklan	k1gMnSc1
•	•	k?
Hostivít	Hostivít	k1gMnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
872	#num#	k4
<g/>
–	–	k?
<g/>
889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
I.	I.	kA
(	(	kIx(
<g/>
894	#num#	k4
<g/>
–	–	k?
<g/>
915	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
915	#num#	k4
<g/>
–	–	k?
<g/>
921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
svatý	svatý	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
Chrabrý¹	Chrabrý¹	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladivoj	Vladivoj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
II	II	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svatopluk	Svatopluk	k1gMnSc1
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1117	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
I.	I.	kA
(	(	kIx(
<g/>
1120	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1178	#num#	k4
<g/>
–	–	k?
<g/>
1189	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1198	#num#	k4
<g/>
–	–	k?
<g/>
1230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1230	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1253	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1278	#num#	k4
<g/>
–	–	k?
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1307	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1419	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
interregnum	interregnum	k1gNnSc4
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1457	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín²	Korvín²	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
Jagellonci	Jagellonec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Falcký³	Falcký³	k1gMnSc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1620	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský³	Bavorský³	k1gFnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
Piastovec	Piastovec	k1gInSc1
<g/>
,	,	kIx,
²	²	k?
vládce	vládce	k1gMnSc1
vedlejších	vedlejší	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
³	³	k?
vzdorokrál	vzdorokrál	k1gMnSc1
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
a	a	k8xC
folklór	folklór	k1gInSc1
božstva	božstvo	k1gNnSc2
a	a	k8xC
démonivýchodních	démonivýchodní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
<g/>
(	(	kIx(
<g/>
zejména	zejména	k9
Rusů	Rus	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Vladimírův	Vladimírův	k2eAgInSc1d1
panteon	panteon	k1gInSc1
</s>
<s>
Perun	Perun	k1gMnSc1
•	•	k?
Chors-Dažbog	Chors-Dažbog	k1gMnSc1
•	•	k?
Stribog	Stribog	k1gMnSc1
•	•	k?
Simargl	Simargl	k1gMnSc1
•	•	k?
Mokoš	Mokoš	k1gMnSc1
ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Veles	Veles	k1gMnSc1
•	•	k?
Svarog	Svarog	k1gMnSc1
•	•	k?
Svarožic	Svarožic	k1gMnSc1
•	•	k?
Trojan	Trojan	k1gMnSc1
•	•	k?
Zbručský	Zbručský	k2eAgInSc4d1
idol	idol	k1gInSc4
pozdní	pozdní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
afolklór	afolklóra	k1gFnPc2
</s>
<s>
Jarilo	Jarít	k5eAaPmAgNnS,k5eAaImAgNnS,k5eAaBmAgNnS
•	•	k?
Rod	rod	k1gInSc1
•	•	k?
Rožanice	Rožanice	k1gFnSc2
•	•	k?
Kupalo	Kupala	k1gFnSc5
•	•	k?
Kostroma	Kostrom	k1gMnSc2
•	•	k?
Marena	Maren	k1gMnSc2
•	•	k?
Ognyena	Ognyen	k1gMnSc2
(	(	kIx(
<g/>
Ohnivá	ohnivý	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
<g/>
)	)	kIx)
démoni	démon	k1gMnPc1
a	a	k8xC
duchové	duch	k1gMnPc1
</s>
<s>
Baba	baba	k1gFnSc1
Jaga	Jaga	k1gFnSc1
•	•	k?
běs	běs	k1gInSc1
•	•	k?
domovoj	domovoj	k1gInSc1
•	•	k?
Kostěj	Kostěj	k1gFnSc2
•	•	k?
lešij	lešít	k5eAaPmRp2nS
•	•	k?
Polevik	Polevik	k1gInSc1
•	•	k?
rusalka	rusalka	k1gFnSc1
•	•	k?
víla	víla	k1gFnSc1
•	•	k?
vlkodlak	vlkodlak	k1gMnSc1
•	•	k?
zmej	zmej	k1gMnSc1
</s>
<s>
Zbručský	Zbručský	k2eAgInSc1d1
idol	idol	k1gInSc1
božstva	božstvo	k1gNnSc2
a	a	k8xC
démonizápadních	démonizápadní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
</s>
<s>
Obodrité	Obodrita	k1gMnPc1
</s>
<s>
Prove	Proev	k1gFnPc1
•	•	k?
Podaga	Podaga	k1gFnSc1
•	•	k?
Živa	Živa	k1gFnSc1
Lutici	Lutice	k1gFnSc4
</s>
<s>
Černoboh	Černoboh	k1gMnSc1
•	•	k?
Černohlav	Černohlav	k1gMnSc1
•	•	k?
bohové-blíženci	bohové-blíženec	k1gMnSc3
•	•	k?
Jarovít	Jarovít	k1gMnSc1
•	•	k?
Radgost	Radgost	k1gInSc1
•	•	k?
Rujevít	Rujevít	k1gFnSc2
•	•	k?
Porenut	Porenut	k2eAgInSc1d1
(	(	kIx(
<g/>
Turupit	Turupit	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Porevít	Porevít	k1gMnSc1
•	•	k?
Pizamar	Pizamar	k1gMnSc1
•	•	k?
Pripegala	Pripegala	k1gFnSc1
•	•	k?
Svarožic	Svarožic	k1gMnSc1
•	•	k?
Svantovít	Svantovít	k1gMnSc1
•	•	k?
Triglav	Triglav	k1gMnSc1
Pomořané	Pomořan	k1gMnPc1
</s>
<s>
Jarovít	Jarovít	k5eAaPmF
•	•	k?
Triglav	Triglav	k1gMnSc1
•	•	k?
Wolinský	Wolinský	k2eAgInSc1d1
idol	idol	k1gInSc1
•	•	k?
Wolinské	Wolinský	k2eAgInPc1d1
kopí	kopit	k5eAaImIp3nP
pozdní	pozdní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
afolklór	afolklóra	k1gFnPc2
</s>
<s>
Jessa	Jessa	k1gFnSc1
•	•	k?
Kresnik	Kresnik	k1gMnSc1
/	/	kIx~
Zduhač	Zduhač	k1gMnSc1
•	•	k?
Lada	Lada	k1gFnSc1
/	/	kIx~
Lado	lado	k1gNnSc1
•	•	k?
Lel	Lel	k1gMnSc1
a	a	k8xC
Polel	Polel	k1gMnSc1
•	•	k?
Morana	Morana	k1gFnSc1
•	•	k?
Dodola	Dodola	k1gFnSc1
/	/	kIx~
Perperuna	Perperuna	k1gFnSc1
démoni	démon	k1gMnPc1
a	a	k8xC
duchové	duch	k1gMnPc1
</s>
<s>
čert	čert	k1gMnSc1
•	•	k?
ludkové	ludková	k1gFnSc2
•	•	k?
rusalka	rusalka	k1gFnSc1
•	•	k?
sudičky	sudička	k1gFnSc2
•	•	k?
upír	upír	k1gMnSc1
•	•	k?
víla	víla	k1gFnSc1
•	•	k?
vodník	vodník	k1gMnSc1
•	•	k?
vlkodlak	vlkodlak	k1gMnSc1
•	•	k?
zmej	zmej	k1gMnSc1
</s>
<s>
hypotetická	hypotetický	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
</s>
<s>
Bělbog	Bělbog	k1gMnSc1
•	•	k?
German	German	k1gMnSc1
•	•	k?
Pogoda	Pogoda	k1gMnSc1
•	•	k?
Pohvizd	pohvizd	k1gInSc1
•	•	k?
Zora	Zora	k1gFnSc1
epika	epika	k1gFnSc1
</s>
<s>
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
Lech	Lech	k1gMnSc1
a	a	k8xC
Rus	Rus	k1gMnSc1
(	(	kIx(
<g/>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Lech	Lech	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Rus	Rus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
dynastický	dynastický	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
•	•	k?
Ilja	Ilja	k1gMnSc1
Muromec	Muromec	k1gMnSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
dynastický	dynastický	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
•	•	k?
Slovo	slovo	k1gNnSc1
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
kosmologie	kosmologie	k1gFnSc2
</s>
<s>
Bujan	bujan	k1gMnSc1
•	•	k?
Nav	navit	k5eAaImRp2nS
slovanské	slovanský	k2eAgFnPc4d1
svátky	svátek	k1gInPc7
</s>
<s>
Dožínky	dožínky	k1gFnPc1
•	•	k?
Dziady	Dziada	k1gFnSc2
•	•	k?
Hromnice	hromnice	k1gFnSc2
•	•	k?
Kupadelné	Kupadelný	k2eAgInPc4d1
svátky	svátek	k1gInPc4
•	•	k?
Kračun	Kračun	k1gInSc1
•	•	k?
vynášení	vynášení	k1gNnSc1
smrti	smrt	k1gFnSc2
/	/	kIx~
vítání	vítání	k1gNnSc4
jara	jaro	k1gNnSc2
související	související	k2eAgNnSc1d1
</s>
<s>
Prillwitzské	Prillwitzský	k2eAgInPc1d1
idoly	idol	k1gInPc1
•	•	k?
slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
•	•	k?
rodnověří	rodnověřet	k5eAaImIp3nS,k5eAaPmIp3nS
•	•	k?
indoevropské	indoevropský	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
•	•	k?
Velesova	Velesův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
</s>
