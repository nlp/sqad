<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
je	být	k5eAaImIp3nS
legendární	legendární	k2eAgMnSc1d1
prapředek	prapředek	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
podle	podle	k7c2
českých	český	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
o	o	k7c6
svém	svůj	k3xOyFgInSc6
původu	původ	k1gInSc6
přivedl	přivést	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
lid	lid	k1gInSc4
do	do	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
po	po	k7c6
něm	on	k3xPp3gNnSc6
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
Čechy	Čechy	k1gFnPc1
<g/>
.	.	kIx.
</s>