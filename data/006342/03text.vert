<s>
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Adeline	Adelin	k1gInSc5	Adelin
Virginia	Virginium	k1gNnSc2	Virginium
Stephen	Stephen	k1gInSc4	Stephen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
<g/>
,	,	kIx,	,
esejistka	esejistka	k1gFnSc1	esejistka
<g/>
,	,	kIx,	,
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
<g/>
,	,	kIx,	,
filozofka	filozofka	k1gFnSc1	filozofka
a	a	k8xC	a
feministka	feministka	k1gFnSc1	feministka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
avantgardních	avantgardní	k2eAgMnPc2d1	avantgardní
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
výuky	výuka	k1gFnSc2	výuka
o	o	k7c6	o
modernismu	modernismus	k1gInSc6	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
Adeline	Adelin	k1gInSc5	Adelin
Virginia	Virginium	k1gNnPc1	Virginium
Stephen	Stephna	k1gFnPc2	Stephna
(	(	kIx(	(
<g/>
Stephenová	Stephenový	k2eAgFnSc1d1	Stephenový
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vychovaná	vychovaný	k2eAgFnSc1d1	vychovaná
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
vlastními	vlastní	k2eAgMnPc7d1	vlastní
sourozenci	sourozenec	k1gMnPc7	sourozenec
i	i	k8xC	i
sourozenci	sourozenec	k1gMnPc7	sourozenec
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
matčina	matčin	k2eAgNnSc2d1	matčino
manželství	manželství	k1gNnSc2	manželství
v	v	k7c6	v
typické	typický	k2eAgFnSc6d1	typická
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
rodině	rodina	k1gFnSc6	rodina
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
22	[number]	k4	22
Hyde	Hyde	k1gNnSc2	Hyde
Park	park	k1gInSc1	park
Gate	Gate	k1gFnSc4	Gate
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
kmotrem	kmotr	k1gMnSc7	kmotr
byl	být	k5eAaImAgMnS	být
James	James	k1gMnSc1	James
Russell	Russell	k1gMnSc1	Russell
Lowell	Lowell	k1gMnSc1	Lowell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
přítelem	přítel	k1gMnSc7	přítel
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
měla	mít	k5eAaImAgFnS	mít
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
incestnímu	incestní	k2eAgNnSc3d1	incestní
obtěžování	obtěžování	k1gNnSc3	obtěžování
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Geralda	Gerald	k1gMnSc2	Gerald
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
sexuálním	sexuální	k2eAgNnSc6d1	sexuální
tématu	téma	k1gNnSc6	téma
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnSc2	Virginium
mlčela	mlčet	k5eAaImAgFnS	mlčet
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
traumatu	trauma	k1gNnSc6	trauma
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
nenávidět	návidět	k5eNaImF	návidět
svou	svůj	k3xOyFgFnSc4	svůj
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
existenci	existence	k1gFnSc4	existence
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
matčině	matčin	k2eAgFnSc6d1	matčina
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
napjaly	napnout	k5eAaPmAgInP	napnout
především	především	k6eAd1	především
emocionálně	emocionálně	k6eAd1	emocionálně
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
George	George	k1gFnSc1	George
ji	on	k3xPp3gFnSc4	on
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
psychicky	psychicky	k6eAd1	psychicky
tyranizoval	tyranizovat	k5eAaImAgInS	tyranizovat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
i	i	k9	i
sexuálně	sexuálně	k6eAd1	sexuálně
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnSc2	Virginium
navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
mlčenlivosti	mlčenlivost	k1gFnSc3	mlčenlivost
vše	všechen	k3xTgNnSc4	všechen
psala	psát	k5eAaImAgFnS	psát
do	do	k7c2	do
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
si	se	k3xPyFc3	se
vedla	vést	k5eAaImAgFnS	vést
už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
i	i	k9	i
její	její	k3xOp3gFnSc3	její
otec	otec	k1gMnSc1	otec
Sir	sir	k1gMnSc1	sir
Leslie	Leslie	k1gFnSc2	Leslie
Stephen	Stephen	k2eAgMnSc1d1	Stephen
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
National	National	k1gFnSc2	National
Biography	Biographa	k1gFnSc2	Biographa
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
otec	otec	k1gMnSc1	otec
podporoval	podporovat	k5eAaImAgMnS	podporovat
její	její	k3xOp3gNnSc4	její
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
příliš	příliš	k6eAd1	příliš
nerozuměla	rozumět	k5eNaImAgFnS	rozumět
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
věkovému	věkový	k2eAgInSc3d1	věkový
rozdílu	rozdíl	k1gInSc3	rozdíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
rodině	rodina	k1gFnSc6	rodina
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
puritánská	puritánský	k2eAgFnSc1d1	puritánská
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
po	po	k7c6	po
matčině	matčin	k2eAgFnSc6d1	matčina
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
z	z	k7c2	z
otce	otec	k1gMnSc2	otec
stal	stát	k5eAaPmAgMnS	stát
tyran	tyran	k1gMnSc1	tyran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bratr	bratr	k1gMnSc1	bratr
Toby	Toba	k1gFnSc2	Toba
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
psychicky	psychicky	k6eAd1	psychicky
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
také	také	k9	také
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
se	se	k3xPyFc4	se
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
přestěhovaly	přestěhovat	k5eAaPmAgFnP	přestěhovat
do	do	k7c2	do
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
scházet	scházet	k5eAaImF	scházet
skupina	skupina	k1gFnSc1	skupina
intelektuálů	intelektuál	k1gMnPc2	intelektuál
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
přátel	přítel	k1gMnPc2	přítel
patřili	patřit	k5eAaImAgMnP	patřit
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Lytton	Lytton	k1gInSc4	Lytton
Strachey	Strachey	k1gInPc7	Strachey
a	a	k8xC	a
Vita	vit	k2eAgFnSc1d1	Vita
Sackville-Westová	Sackville-Westová	k1gFnSc1	Sackville-Westová
<g/>
,	,	kIx,	,
historikové	historik	k1gMnPc1	historik
umění	umění	k1gNnSc2	umění
Roger	Rogra	k1gFnPc2	Rogra
Fry	Fry	k1gFnSc2	Fry
a	a	k8xC	a
Clive	Cliev	k1gFnSc2	Cliev
Bell	bell	k1gInSc4	bell
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Vanessou	Vanessa	k1gFnSc7	Vanessa
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
Woolfové	Woolf	k1gMnPc1	Woolf
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Duncan	Duncany	k1gInPc2	Duncany
Grant	grant	k1gInSc1	grant
nebo	nebo	k8xC	nebo
ekonom	ekonom	k1gMnSc1	ekonom
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
<g/>
.	.	kIx.	.
</s>
<s>
Hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
tématech	téma	k1gNnPc6	téma
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgFnPc2d1	filozofická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c6	o
pacifismu	pacifismus	k1gInSc6	pacifismus
nebo	nebo	k8xC	nebo
feminismu	feminismus	k1gInSc6	feminismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Group	Group	k1gInSc1	Group
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
názory	názor	k1gInPc1	názor
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
i	i	k9	i
dílo	dílo	k1gNnSc1	dílo
Virginie	Virginie	k1gFnSc2	Virginie
Woolfové	Woolfový	k2eAgFnSc2d1	Woolfová
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
Bloomsbury	Bloomsbur	k1gInPc7	Bloomsbur
a	a	k8xC	a
konkrétně	konkrétně	k6eAd1	konkrétně
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
doktrinářskému	doktrinářský	k2eAgInSc3d1	doktrinářský
racionalismu	racionalismus	k1gInSc3	racionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
nepříjemným	příjemný	k2eNgInPc3d1	nepříjemný
zážitkům	zážitek	k1gInPc3	zážitek
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
odmítala	odmítat	k5eAaImAgFnS	odmítat
muže	muž	k1gMnSc4	muž
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fyzické	fyzický	k2eAgFnSc2d1	fyzická
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
blízká	blízký	k2eAgNnPc4d1	blízké
<g/>
,	,	kIx,	,
až	až	k9	až
intimní	intimní	k2eAgNnSc4d1	intimní
přátelství	přátelství	k1gNnSc4	přátelství
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Leonarda	Leonardo	k1gMnSc2	Leonardo
Woolfa	Woolf	k1gMnSc2	Woolf
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc2	člen
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
kritika	kritik	k1gMnSc2	kritik
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
statí	stať	k1gFnPc2	stať
o	o	k7c6	o
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Leonard	Leonard	k1gMnSc1	Leonard
podporoval	podporovat	k5eAaImAgMnS	podporovat
její	její	k3xOp3gFnSc4	její
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
dráhu	dráha	k1gFnSc4	dráha
i	i	k8xC	i
revoltu	revolta	k1gFnSc4	revolta
vůči	vůči	k7c3	vůči
patriarchálnímu	patriarchální	k2eAgNnSc3d1	patriarchální
pojetí	pojetí	k1gNnSc3	pojetí
rodinných	rodinný	k2eAgInPc2d1	rodinný
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
postojům	postoj	k1gInPc3	postoj
generace	generace	k1gFnSc2	generace
pozitivistů	pozitivista	k1gMnPc2	pozitivista
a	a	k8xC	a
realistů	realista	k1gMnPc2	realista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Woolfovi	Woolfův	k2eAgMnPc1d1	Woolfův
koupili	koupit	k5eAaPmAgMnP	koupit
ruční	ruční	k2eAgInSc4d1	ruční
tiskařský	tiskařský	k2eAgInSc4d1	tiskařský
stroj	stroj	k1gInSc4	stroj
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Hogarth	Hogartha	k1gFnPc2	Hogartha
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
vydalo	vydat	k5eAaPmAgNnS	vydat
nejen	nejen	k6eAd1	nejen
některé	některý	k3yIgFnPc1	některý
knihy	kniha	k1gFnPc1	kniha
Virginie	Virginie	k1gFnSc2	Virginie
Woolfové	Woolfový	k2eAgFnPc1d1	Woolfová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
významná	významný	k2eAgNnPc1d1	významné
modernistická	modernistický	k2eAgNnPc1d1	modernistické
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
básní	báseň	k1gFnPc2	báseň
T.	T.	kA	T.
S.	S.	kA	S.
Eliota	Eliota	k1gFnSc1	Eliota
nebo	nebo	k8xC	nebo
překladu	překlad	k1gInSc3	překlad
kompletního	kompletní	k2eAgNnSc2d1	kompletní
díla	dílo	k1gNnSc2	dílo
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
rozborem	rozbor	k1gInSc7	rozbor
vlastního	vlastní	k2eAgNnSc2d1	vlastní
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
ovlivněného	ovlivněný	k2eAgNnSc2d1	ovlivněné
dětstvím	dětství	k1gNnSc7	dětství
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
byla	být	k5eAaImAgFnS	být
hospitalizována	hospitalizovat	k5eAaBmNgFnS	hospitalizovat
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
trpělivost	trpělivost	k1gFnSc1	trpělivost
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
překonat	překonat	k5eAaPmF	překonat
své	své	k1gNnSc4	své
zhoršující	zhoršující	k2eAgNnSc4d1	zhoršující
se	se	k3xPyFc4	se
depresivní	depresivní	k2eAgInPc1d1	depresivní
stavy	stav	k1gInPc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
přišla	přijít	k5eAaPmAgFnS	přijít
neobyčejně	obyčejně	k6eNd1	obyčejně
silná	silný	k2eAgFnSc1d1	silná
duševní	duševní	k2eAgFnSc1d1	duševní
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
trpěla	trpět	k5eAaImAgFnS	trpět
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
depresemi	deprese	k1gFnPc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k6eAd1	kolem
zuřila	zuřit	k5eAaImAgFnS	zuřit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
(	(	kIx(	(
<g/>
Židem	Žid	k1gMnSc7	Žid
<g/>
)	)	kIx)	)
účastnili	účastnit	k5eAaImAgMnP	účastnit
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
tušila	tušit	k5eAaImAgFnS	tušit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
čekalo	čekat	k5eAaImAgNnS	čekat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
válku	válka	k1gFnSc4	válka
prohrála	prohrát	k5eAaPmAgFnS	prohrát
nebo	nebo	k8xC	nebo
kdyby	kdyby	kYmCp3nP	kdyby
byli	být	k5eAaImAgMnP	být
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neúnosné	únosný	k2eNgFnSc3d1	neúnosná
situaci	situace	k1gFnSc3	situace
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
utopením	utopení	k1gNnSc7	utopení
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Ouse	Ous	k1gInSc2	Ous
blízko	blízko	k7c2	blízko
svého	svůj	k3xOyFgInSc2	svůj
venkovského	venkovský	k2eAgInSc2d1	venkovský
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Rodmellu	Rodmell	k1gInSc6	Rodmell
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
skončila	skončit	k5eAaPmAgFnS	skončit
oficiálně	oficiálně	k6eAd1	oficiálně
již	již	k6eAd1	již
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Virginii	Virginie	k1gFnSc4	Virginie
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
sourozenci	sourozenec	k1gMnSc3	sourozenec
vydávat	vydávat	k5eAaPmF	vydávat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
noviny	novina	k1gFnPc4	novina
Hyde	Hyd	k1gInSc2	Hyd
Park	park	k1gInSc1	park
Gate	Gat	k1gFnSc2	Gat
News	Newsa	k1gFnPc2	Newsa
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
do	do	k7c2	do
jejích	její	k3xOp3gNnPc2	její
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odmala	odmala	k6eAd1	odmala
si	se	k3xPyFc3	se
také	také	k9	také
všichni	všechen	k3xTgMnPc1	všechen
psali	psát	k5eAaImAgMnP	psát
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgNnPc1d1	jediné
Virginia	Virginium	k1gNnPc1	Virginium
u	u	k7c2	u
psaní	psaní	k1gNnSc2	psaní
vydržela	vydržet	k5eAaPmAgFnS	vydržet
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
deníku	deník	k1gInSc2	deník
"	"	kIx"	"
<g/>
křičela	křičet	k5eAaImAgFnS	křičet
<g/>
"	"	kIx"	"
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yInSc6	co
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
musela	muset	k5eAaImAgFnS	muset
mlčet	mlčet	k5eAaImF	mlčet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
určitým	určitý	k2eAgNnSc7d1	určité
osvobozením	osvobození	k1gNnSc7	osvobození
a	a	k8xC	a
také	také	k9	také
sebepoznáním	sebepoznání	k1gNnSc7	sebepoznání
a	a	k8xC	a
utvářením	utváření	k1gNnSc7	utváření
vlastní	vlastní	k2eAgFnSc2d1	vlastní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
si	se	k3xPyFc3	se
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
"	"	kIx"	"
<g/>
spisovatelský	spisovatelský	k2eAgInSc4d1	spisovatelský
deník	deník	k1gInSc4	deník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgNnPc3d3	nejcennější
dílům	dílo	k1gNnPc3	dílo
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
v	v	k7c6	v
obrovské	obrovský	k2eAgFnSc6d1	obrovská
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
první	první	k4xOgInSc1	první
román	román	k1gInSc1	román
The	The	k1gMnSc2	The
Voyage	Voyag	k1gMnSc2	Voyag
Out	Out	k1gMnSc2	Out
(	(	kIx(	(
<g/>
Plavba	plavba	k1gFnSc1	plavba
<g/>
)	)	kIx)	)
vyšel	vyjít	k5eAaPmAgInS	vyjít
po	po	k7c6	po
šestileté	šestiletý	k2eAgFnSc6d1	šestiletá
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Melymbrosia	Melymbrosius	k1gMnSc4	Melymbrosius
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Virignia	Virignium	k1gNnSc2	Virignium
jej	on	k3xPp3gNnSc2	on
změnila	změnit	k5eAaPmAgFnS	změnit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
byl	být	k5eAaImAgInS	být
knize	kniha	k1gFnSc6	kniha
přičítán	přičítán	k2eAgInSc1d1	přičítán
politický	politický	k2eAgInSc1d1	politický
kontext	kontext	k1gInSc1	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
vyšel	vyjít	k5eAaPmAgInS	vyjít
druhý	druhý	k4xOgInSc4	druhý
román	román	k1gInSc4	román
Night	Night	k1gMnSc1	Night
and	and	k?	and
Day	Day	k1gMnSc1	Day
(	(	kIx(	(
<g/>
Noc	noc	k1gFnSc1	noc
a	a	k8xC	a
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgNnSc7	svůj
pojetím	pojetí	k1gNnSc7	pojetí
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgNnSc4d1	předchozí
dílo	dílo	k1gNnSc4	dílo
ještě	ještě	k9	ještě
románem	román	k1gInSc7	román
tradičním	tradiční	k2eAgInSc7d1	tradiční
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
zde	zde	k6eAd1	zde
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
životy	život	k1gInPc1	život
dvou	dva	k4xCgFnPc2	dva
přítelkyň	přítelkyně	k1gFnPc2	přítelkyně
Katherine	Katherin	k1gInSc5	Katherin
a	a	k8xC	a
Mary	Mary	k1gFnSc2	Mary
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ve	v	k7c6	v
Woolfové	Woolfový	k2eAgFnSc6d1	Woolfová
tvorbě	tvorba	k1gFnSc6	tvorba
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
poetičtější	poetický	k2eAgInSc4d2	poetičtější
a	a	k8xC	a
subjektivnější	subjektivní	k2eAgInSc4d2	subjektivnější
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
myšlenky	myšlenka	k1gFnPc4	myšlenka
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
orientovat	orientovat	k5eAaBmF	orientovat
romány	román	k1gInPc4	román
mnohem	mnohem	k6eAd1	mnohem
intimněji	intimně	k6eAd2	intimně
a	a	k8xC	a
introspektivněji	introspektivně	k6eAd2	introspektivně
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Room	Roo	k1gNnSc7	Roo
(	(	kIx(	(
<g/>
Jákobův	Jákobův	k2eAgInSc4d1	Jákobův
pokoj	pokoj	k1gInSc4	pokoj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
života	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
Tobyho	Toby	k1gMnSc2	Toby
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
přelomové	přelomový	k2eAgNnSc4d1	přelomové
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
našla	najít	k5eAaPmAgFnS	najít
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
psala	psát	k5eAaImAgNnP	psát
pozdější	pozdní	k2eAgNnPc1d2	pozdější
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgFnSc2d1	moderní
klasiky	klasika	k1gFnSc2	klasika
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
autory	autor	k1gMnPc7	autor
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
James	James	k1gMnSc1	James
Joyce	Joyce	k1gMnSc1	Joyce
nebo	nebo	k8xC	nebo
Marcel	Marcel	k1gMnSc1	Marcel
Proust	Proust	k1gMnSc1	Proust
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
romány	román	k1gInPc4	román
<g/>
:	:	kIx,	:
Mrs	Mrs	k1gFnPc4	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Dalloway	Dalloway	k1gInPc1	Dalloway
(	(	kIx(	(
<g/>
Paní	paní	k1gFnSc1	paní
Dallowayová	Dallowayová	k1gFnSc1	Dallowayová
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc4	ten
The	The	k1gMnSc1	The
Lighthouse	Lighthouse	k1gFnSc2	Lighthouse
(	(	kIx(	(
<g/>
K	k	k7c3	k
majáku	maják	k1gInSc3	maják
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
The	The	k1gMnSc1	The
Waves	Waves	k1gMnSc1	Waves
(	(	kIx(	(
<g/>
Vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
romány	román	k1gInPc1	román
ji	on	k3xPp3gFnSc4	on
zařadily	zařadit	k5eAaPmAgInP	zařadit
k	k	k7c3	k
nejvýraznějším	výrazný	k2eAgMnPc3d3	nejvýraznější
a	a	k8xC	a
nejuznávanějším	uznávaný	k2eAgMnPc3d3	nejuznávanější
romanopiscům	romanopisec	k1gMnPc3	romanopisec
její	její	k3xOp3gFnSc1	její
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgFnPc2	tento
prací	práce	k1gFnPc2	práce
bylo	být	k5eAaImAgNnS	být
odhalení	odhalení	k1gNnSc4	odhalení
ženského	ženský	k2eAgNnSc2d1	ženské
nitra	nitro	k1gNnSc2	nitro
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
jako	jako	k8xS	jako
alternativy	alternativa	k1gFnSc2	alternativa
k	k	k7c3	k
dominujícímu	dominující	k2eAgInSc3d1	dominující
mužskému	mužský	k2eAgInSc3d1	mužský
pohledu	pohled	k1gInSc3	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozici	pozice	k1gFnSc4	pozice
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
fantaskní	fantaskní	k2eAgInSc1d1	fantaskní
román	román	k1gInSc1	román
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
sledující	sledující	k2eAgInSc1d1	sledující
osud	osud	k1gInSc1	osud
hlavní	hlavní	k2eAgInSc1d1	hlavní
<g/>
(	(	kIx(	(
<g/>
ho	on	k3xPp3gMnSc2	on
<g/>
)	)	kIx)	)
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
/	/	kIx~	/
<g/>
hrdinky	hrdinka	k1gFnPc1	hrdinka
procházející	procházející	k2eAgFnPc1d1	procházející
<g/>
(	(	kIx(	(
<g/>
ho	on	k3xPp3gMnSc4	on
<g/>
)	)	kIx)	)
různými	různý	k2eAgFnPc7d1	různá
dobami	doba	k1gFnPc7	doba
<g/>
,	,	kIx,	,
od	od	k7c2	od
alžbětinského	alžbětinský	k2eAgInSc2d1	alžbětinský
dvora	dvůr	k1gInSc2	dvůr
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc4	román
napsala	napsat	k5eAaPmAgFnS	napsat
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozchodu	rozchod	k1gInSc2	rozchod
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
blízkou	blízký	k2eAgFnSc7d1	blízká
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Vitou	vitý	k2eAgFnSc7d1	vitá
Sackville-Westovou	Sackville-Westová	k1gFnSc7	Sackville-Westová
<g/>
,	,	kIx,	,
psaním	psaní	k1gNnSc7	psaní
se	se	k3xPyFc4	se
léčila	léčit	k5eAaImAgFnS	léčit
a	a	k8xC	a
zaháněla	zahánět	k5eAaImAgFnS	zahánět
depresivní	depresivní	k2eAgInPc4d1	depresivní
stavy	stav	k1gInPc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnPc4	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
plodnou	plodný	k2eAgFnSc7d1	plodná
esejistkou	esejistka	k1gFnSc7	esejistka
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
přes	přes	k7c4	přes
500	[number]	k4	500
esejí	esej	k1gFnPc2	esej
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
v	v	k7c4	v
Times	Times	k1gInSc4	Times
Literary	Literara	k1gFnSc2	Literara
Supplement	Supplement	k1gInSc1	Supplement
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
napsala	napsat	k5eAaBmAgFnS	napsat
několik	několik	k4yIc4	několik
svazků	svazek	k1gInPc2	svazek
kritických	kritický	k2eAgFnPc2d1	kritická
statí	stať	k1gFnPc2	stať
-	-	kIx~	-
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Vlastní	vlastní	k2eAgInSc4d1	vlastní
pokoj	pokoj	k1gInSc4	pokoj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poprvé	poprvé	k6eAd1	poprvé
představila	představit	k5eAaPmAgFnS	představit
při	při	k7c6	při
přednášce	přednáška	k1gFnSc6	přednáška
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
poukázala	poukázat	k5eAaPmAgNnP	poukázat
velmi	velmi	k6eAd1	velmi
důrazně	důrazně	k6eAd1	důrazně
na	na	k7c4	na
význam	význam	k1gInSc4	význam
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
ženské	ženský	k2eAgFnSc2d1	ženská
literární	literární	k2eAgFnSc2d1	literární
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
popis	popis	k1gInSc4	popis
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bránily	bránit	k5eAaImAgFnP	bránit
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
podporovaly	podporovat	k5eAaImAgFnP	podporovat
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
se	se	k3xPyFc4	se
<g/>
/	/	kIx~	/
promluvit	promluvit	k5eAaPmF	promluvit
vlastním	vlastní	k2eAgInSc7d1	vlastní
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
nekopírovat	kopírovat	k5eNaImF	kopírovat
tedy	tedy	k9	tedy
etablované	etablovaný	k2eAgInPc4d1	etablovaný
mužské	mužský	k2eAgInPc4d1	mužský
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
dílo	dílo	k1gNnSc1	dílo
zejména	zejména	k9	zejména
dvou	dva	k4xCgFnPc2	dva
autorek	autorka	k1gFnPc2	autorka
–	–	k?	–
Emily	Emil	k1gMnPc4	Emil
Brontë	Brontë	k1gFnSc2	Brontë
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
Austen	Austno	k1gNnPc2	Austno
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
psát	psát	k5eAaImF	psát
jako	jako	k9	jako
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
podstatný	podstatný	k2eAgInSc4d1	podstatný
prvek	prvek	k1gInSc4	prvek
ženského	ženský	k2eAgNnSc2d1	ženské
psaní	psaní	k1gNnSc2	psaní
považuje	považovat	k5eAaImIp3nS	považovat
mlčení	mlčení	k1gNnSc4	mlčení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
přemýšlení	přemýšlení	k1gNnSc4	přemýšlení
a	a	k8xC	a
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
sdělitelným	sdělitelný	k2eAgInSc7d1	sdělitelný
<g/>
,	,	kIx,	,
vypověditelným	vypověditelný	k2eAgInSc7d1	vypověditelný
a	a	k8xC	a
nevypověditelným	vypověditelný	k2eNgInSc7d1	nevypověditelný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
ale	ale	k9	ale
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
i	i	k9	i
problematiku	problematika	k1gFnSc4	problematika
zobrazování	zobrazování	k1gNnSc2	zobrazování
ženy	žena	k1gFnSc2	žena
jako	jako	k8xS	jako
literárního	literární	k2eAgNnSc2d1	literární
tématu	téma	k1gNnSc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
eseji	esej	k1gFnSc6	esej
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
institucích	instituce	k1gFnPc6	instituce
a	a	k8xC	a
tradicích	tradice	k1gFnPc6	tradice
mužských	mužský	k2eAgFnPc2d1	mužská
škol	škola	k1gFnPc2	škola
tónem	tón	k1gInSc7	tón
výsměšné	výsměšný	k2eAgFnSc2d1	výsměšná
pokory	pokora	k1gFnSc2	pokora
a	a	k8xC	a
uctivosti	uctivost	k1gFnSc2	uctivost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zesměšnila	zesměšnit	k5eAaPmAgFnS	zesměšnit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
spojené	spojený	k2eAgFnPc1d1	spojená
patriarchální	patriarchální	k2eAgFnPc4d1	patriarchální
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
autoritě	autorita	k1gFnSc6	autorita
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc6	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Tři	tři	k4xCgInPc1	tři
guineje	guinej	k1gInPc1	guinej
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
především	především	k9	především
problematikou	problematika	k1gFnSc7	problematika
feminismu	feminismus	k1gInSc2	feminismus
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
ženy-spisovatelky	ženypisovatelka	k1gFnPc1	ženy-spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
známý	známý	k2eAgInSc4d1	známý
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
...	...	k?	...
<g/>
Psaní	psaní	k1gNnSc1	psaní
ženy	žena	k1gFnPc1	žena
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
ženské	ženský	k2eAgNnSc1d1	ženské
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
být	být	k5eAaImF	být
ženské	ženský	k2eAgFnPc4d1	ženská
<g/>
,	,	kIx,	,
a	a	k8xC	a
přinejlepším	přinejlepším	k6eAd1	přinejlepším
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ženské	ženský	k2eAgNnSc1d1	ženské
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
opomíjený	opomíjený	k2eAgInSc4d1	opomíjený
a	a	k8xC	a
potlačovaný	potlačovaný	k2eAgInSc4d1	potlačovaný
ženský	ženský	k2eAgInSc4d1	ženský
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
slavné	slavný	k2eAgFnSc2d1	slavná
formulace	formulace	k1gFnSc2	formulace
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
"	"	kIx"	"
<g/>
pět	pět	k4xCc1	pět
set	sto	k4xCgNnPc2	sto
liber	libra	k1gFnPc2	libra
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
vlastní	vlastní	k2eAgInSc4d1	vlastní
pokoj	pokoj	k1gInSc4	pokoj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jedněm	jeden	k4xCgInPc3	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
esejí	esej	k1gFnPc2	esej
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgNnPc7d1	hlavní
tématy	téma	k1gNnPc7	téma
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
distribuce	distribuce	k1gFnSc1	distribuce
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
nemožnost	nemožnost	k1gFnSc1	nemožnost
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
seberealizaci	seberealizace	k1gFnSc4	seberealizace
žen	žena	k1gFnPc2	žena
mimo	mimo	k7c4	mimo
prostředí	prostředí	k1gNnSc4	prostředí
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pokládá	pokládat	k5eAaImIp3nS	pokládat
základ	základ	k1gInSc1	základ
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
feministky	feministka	k1gFnPc4	feministka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
volaly	volat	k5eAaImAgFnP	volat
po	po	k7c6	po
změnách	změna	k1gFnPc6	změna
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Problematizuje	problematizovat	k5eAaImIp3nS	problematizovat
právě	právě	k9	právě
autoritativní	autoritativní	k2eAgNnSc1d1	autoritativní
situování	situování	k1gNnSc1	situování
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
domácností	domácnost	k1gFnPc2	domácnost
skrze	skrze	k?	skrze
nátlak	nátlak	k1gInSc1	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
ale	ale	k8xC	ale
také	také	k9	také
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
nerovnoměrnou	rovnoměrný	k2eNgFnSc7d1	nerovnoměrná
distribucí	distribuce	k1gFnSc7	distribuce
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
nerovnoměrnou	rovnoměrný	k2eNgFnSc7d1	nerovnoměrná
distribucí	distribuce	k1gFnSc7	distribuce
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
modernistům	modernista	k1gMnPc3	modernista
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
velkého	velký	k2eAgMnSc2d1	velký
inovátora	inovátor	k1gMnSc2	inovátor
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
užití	užití	k1gNnSc2	užití
v	v	k7c6	v
beletristickém	beletristický	k2eAgNnSc6d1	beletristické
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
s	s	k7c7	s
psychologickými	psychologický	k2eAgInPc7d1	psychologický
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
možnostmi	možnost	k1gFnPc7	možnost
přerušovaného	přerušovaný	k2eAgInSc2d1	přerušovaný
vyprávění	vyprávění	k1gNnPc4	vyprávění
a	a	k8xC	a
chronologie	chronologie	k1gFnPc4	chronologie
<g/>
,	,	kIx,	,
soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
emocionální	emocionální	k2eAgInPc4d1	emocionální
stavy	stav	k1gInPc4	stav
svých	svůj	k3xOyFgFnPc2	svůj
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
bývá	bývat	k5eAaImIp3nS	bývat
právem	právem	k6eAd1	právem
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
autorku	autorka	k1gFnSc4	autorka
netradiční	tradiční	k2eNgNnSc1d1	netradiční
<g/>
,	,	kIx,	,
či	či	k8xC	či
protitradiční	protitradiční	k2eAgFnSc4d1	protitradiční
a	a	k8xC	a
feministickou	feministický	k2eAgFnSc4d1	feministická
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
ženskou	ženský	k2eAgFnSc4d1	ženská
každodennost	každodennost	k1gFnSc4	každodennost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
popis	popis	k1gInSc1	popis
"	"	kIx"	"
<g/>
banálních	banální	k2eAgInPc2d1	banální
<g/>
"	"	kIx"	"
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
význam	význam	k1gInSc1	význam
ovšem	ovšem	k9	ovšem
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
společnosti	společnost	k1gFnSc3	společnost
skryt	skryt	k1gInSc4	skryt
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
popis	popis	k1gInSc4	popis
člověka	člověk	k1gMnSc2	člověk
viděného	viděný	k2eAgNnSc2d1	viděné
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
,	,	kIx,	,
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
popisuje	popisovat	k5eAaImIp3nS	popisovat
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
patriarchální	patriarchální	k2eAgInPc4d1	patriarchální
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
genderově	genderově	k6eAd1	genderově
diferencovanou	diferencovaný	k2eAgFnSc4d1	diferencovaná
socializaci	socializace	k1gFnSc4	socializace
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celkovou	celkový	k2eAgFnSc4d1	celková
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
psychickou	psychický	k2eAgFnSc4d1	psychická
náladu	nálada	k1gFnSc4	nálada
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nastoluje	nastolovat	k5eAaImIp3nS	nastolovat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
značně	značně	k6eAd1	značně
provokující	provokující	k2eAgNnSc1d1	provokující
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vyloučení	vyloučení	k1gNnSc1	vyloučení
žen	žena	k1gFnPc2	žena
z	z	k7c2	z
kulturního	kulturní	k2eAgInSc2d1	kulturní
<g/>
,	,	kIx,	,
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přínos	přínos	k1gInSc1	přínos
Virginie	Virginie	k1gFnSc5	Virginie
Woolfové	Woolf	k1gMnPc1	Woolf
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
bezesporu	bezesporu	k9	bezesporu
spatřovat	spatřovat	k5eAaImF	spatřovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nabourávala	nabourávat	k5eAaImAgFnS	nabourávat
"	"	kIx"	"
<g/>
zajeté	zajetý	k2eAgFnPc1d1	zajetá
koleje	kolej	k1gFnPc1	kolej
<g/>
"	"	kIx"	"
a	a	k8xC	a
jak	jak	k6eAd1	jak
pronikala	pronikat	k5eAaImAgFnS	pronikat
genderovými	genderová	k1gFnPc7	genderová
stereotypy	stereotyp	k1gInPc4	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
odvětví	odvětví	k1gNnSc6	odvětví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
málo	málo	k6eAd1	málo
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
ženami	žena	k1gFnPc7	žena
<g/>
;	;	kIx,	;
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navíc	navíc	k6eAd1	navíc
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
autorům	autor	k1gMnPc3	autor
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dodávala	dodávat	k5eAaImAgFnS	dodávat
odvahu	odvaha	k1gFnSc4	odvaha
i	i	k8xC	i
dalším	další	k2eAgFnPc3d1	další
ženám	žena	k1gFnPc3	žena
spisovatelkám	spisovatelka	k1gFnPc3	spisovatelka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepsaly	psát	k5eNaImAgInP	psát
jen	jen	k9	jen
"	"	kIx"	"
<g/>
do	do	k7c2	do
šuflíku	šuflík	k1gInSc2	šuflík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
pohlížely	pohlížet	k5eAaImAgFnP	pohlížet
sebevědomě	sebevědomě	k6eAd1	sebevědomě
a	a	k8xC	a
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
ji	on	k3xPp3gFnSc4	on
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
působení	působení	k1gNnSc1	působení
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
oboru	obor	k1gInSc6	obor
kromě	kromě	k7c2	kromě
starání	starání	k1gNnSc2	starání
se	se	k3xPyFc4	se
o	o	k7c4	o
chod	chod	k1gInSc4	chod
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
postupně	postupně	k6eAd1	postupně
ukazovat	ukazovat	k5eAaImF	ukazovat
světu	svět	k1gInSc3	svět
a	a	k8xC	a
především	především	k9	především
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ženy	žena	k1gFnPc1	žena
mají	mít	k5eAaImIp3nP	mít
co	co	k3yQnSc4	co
nabídnout	nabídnout	k5eAaPmF	nabídnout
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
zaujmout	zaujmout	k5eAaPmF	zaujmout
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
brát	brát	k5eAaImF	brát
je	on	k3xPp3gInPc4	on
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Neopomenutelný	opomenutelný	k2eNgInSc1d1	neopomenutelný
je	být	k5eAaImIp3nS	být
také	také	k9	také
její	její	k3xOp3gInSc1	její
přínos	přínos	k1gInSc1	přínos
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zrovnopravňování	zrovnopravňování	k1gNnSc1	zrovnopravňování
žen	žena	k1gFnPc2	žena
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Virginie	Virginie	k1gFnSc1	Virginie
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
byla	být	k5eAaImAgFnS	být
odvážnou	odvážný	k2eAgFnSc7d1	odvážná
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
upozorňující	upozorňující	k2eAgFnSc7d1	upozorňující
na	na	k7c4	na
"	"	kIx"	"
<g/>
nezdravé	zdravý	k2eNgNnSc4d1	nezdravé
<g/>
"	"	kIx"	"
vytváření	vytváření	k1gNnSc4	vytváření
neopodstatněných	opodstatněný	k2eNgInPc2d1	neopodstatněný
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ženské	ženský	k2eAgFnSc2d1	ženská
pozice	pozice	k1gFnSc2	pozice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
radikální	radikální	k2eAgFnSc4d1	radikální
kritiku	kritika	k1gFnSc4	kritika
patriarchální	patriarchální	k2eAgFnSc2d1	patriarchální
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňovala	upozorňovat	k5eAaImAgFnS	upozorňovat
na	na	k7c4	na
zkostnatělost	zkostnatělost	k1gFnSc4	zkostnatělost
tradičního	tradiční	k2eAgNnSc2d1	tradiční
patriarchálního	patriarchální	k2eAgNnSc2d1	patriarchální
uspořádání	uspořádání	k1gNnSc2	uspořádání
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celé	celý	k2eAgFnSc2d1	celá
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
tak	tak	k6eAd1	tak
posílila	posílit	k5eAaPmAgFnS	posílit
rozvoj	rozvoj	k1gInSc4	rozvoj
feministického	feministický	k2eAgNnSc2d1	feministické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
jeho	jeho	k3xOp3gFnSc4	jeho
argumentaci	argumentace	k1gFnSc4	argumentace
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tváří	tvář	k1gFnSc7	tvář
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
fáze	fáze	k1gFnSc2	fáze
první	první	k4xOgFnSc2	první
vlny	vlna	k1gFnSc2	vlna
feminizmu	feminizmus	k1gInSc2	feminizmus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Woolfovou	Woolfový	k2eAgFnSc4d1	Woolfová
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
útlaku	útlak	k1gInSc2	útlak
žen	žena	k1gFnPc2	žena
globálním	globální	k2eAgInSc7d1	globální
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávala	srovnávat	k5eAaImAgFnS	srovnávat
tyranii	tyranie	k1gFnSc3	tyranie
patriarchálního	patriarchální	k2eAgInSc2d1	patriarchální
státu	stát	k1gInSc2	stát
s	s	k7c7	s
tyranií	tyranie	k1gFnSc7	tyranie
fašistického	fašistický	k2eAgInSc2d1	fašistický
národního	národní	k2eAgInSc2d1	národní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Voyage	Voyage	k1gFnSc1	Voyage
Out	Out	k1gFnSc1	Out
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Noc	noc	k1gFnSc1	noc
a	a	k8xC	a
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
Night	Night	k2eAgInSc1d1	Night
and	and	k?	and
Day	Day	k1gFnSc7	Day
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Jákobův	Jákobův	k2eAgInSc4d1	Jákobův
pokoj	pokoj	k1gInSc4	pokoj
(	(	kIx(	(
<g/>
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Room	Roo	k1gNnSc7	Roo
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Paní	paní	k1gFnSc1	paní
Dallowayová	Dallowayová	k1gFnSc1	Dallowayová
(	(	kIx(	(
<g/>
Mrs	Mrs	k1gFnSc1	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Dalloway	Dalloway	k1gInPc1	Dalloway
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
K	k	k7c3	k
majáku	maják	k1gInSc3	maják
(	(	kIx(	(
<g/>
To	to	k9	to
the	the	k?	the
Lighthouse	Lighthouse	k1gFnSc1	Lighthouse
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Orlando	Orlanda	k1gFnSc5	Orlanda
(	(	kIx(	(
<g/>
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
:	:	kIx,	:
A	a	k9	a
Biography	Biographa	k1gFnSc2	Biographa
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Waves	Waves	k1gInSc1	Waves
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Years	Years	k1gInSc1	Years
1937	[number]	k4	1937
Mezi	mezi	k7c4	mezi
akty	akt	k1gInPc4	akt
(	(	kIx(	(
<g/>
Between	Between	k2eAgInSc4d1	Between
the	the	k?	the
Acts	Acts	k1gInSc4	Acts
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Znamení	znamení	k1gNnSc1	znamení
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
The	The	k1gFnSc3	The
Mark	Mark	k1gMnSc1	Mark
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Wall	Wallum	k1gNnPc2	Wallum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Kew	Kew	k1gFnSc6	Kew
(	(	kIx(	(
<g/>
Kew	Kew	k1gFnSc1	Kew
Gardens	Gardensa	k1gFnPc2	Gardensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Pondělí	pondělí	k1gNnSc1	pondělí
nebo	nebo	k8xC	nebo
úterý	úterý	k1gNnSc1	úterý
(	(	kIx(	(
<g/>
Monday	Monda	k2eAgFnPc1d1	Monda
or	or	k?	or
Tuesday	Tuesdaa	k1gFnPc1	Tuesdaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Pan	Pan	k1gMnSc1	Pan
Bennet	Bennet	k1gMnSc1	Bennet
a	a	k8xC	a
paní	paní	k1gFnSc1	paní
Brownová	Brownová	k1gFnSc1	Brownová
(	(	kIx(	(
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bennett	Bennett	k1gMnSc1	Bennett
and	and	k?	and
Mrs	Mrs	k1gMnSc1	Mrs
Brown	Brown	k1gMnSc1	Brown
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc4	esej
Obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
čtenář	čtenář	k1gMnSc1	čtenář
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Common	Common	k1gMnSc1	Common
Reader	Reader	k1gMnSc1	Reader
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
esejů	esej	k1gInPc2	esej
Vlastní	vlastní	k2eAgInSc4d1	vlastní
pokoj	pokoj	k1gInSc4	pokoj
(	(	kIx(	(
<g/>
A	a	k9	a
Room	Room	k1gMnSc1	Room
of	of	k?	of
One	One	k1gMnSc1	One
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Own	Own	k1gFnSc7	Own
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
)	)	kIx)	)
Obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
čtenář	čtenář	k1gMnSc1	čtenář
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Druhá	druhý	k4xOgFnSc1	druhý
edice	edice	k1gFnSc1	edice
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Common	Common	k1gMnSc1	Common
Reader	Reader	k1gMnSc1	Reader
<g/>
:	:	kIx,	:
Second	Second	k1gMnSc1	Second
Series	Series	k1gMnSc1	Series
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
esejů	esej	k1gInPc2	esej
Tři	tři	k4xCgInPc1	tři
guineje	guinej	k1gInPc1	guinej
(	(	kIx(	(
<g/>
Three	Three	k1gFnSc1	Three
Guineas	Guineas	k1gMnSc1	Guineas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
<g/>
)	)	kIx)	)
Strašidelný	strašidelný	k2eAgInSc1d1	strašidelný
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
A	A	kA	A
Haunted	Haunted	k1gInSc1	Haunted
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Flush	flush	k1gInSc1	flush
<g/>
:	:	kIx,	:
A	a	k9	a
Biography	Biographa	k1gFnSc2	Biographa
1933	[number]	k4	1933
Roger	Roger	k1gMnSc1	Roger
Fry	Fry	k1gMnSc1	Fry
<g/>
:	:	kIx,	:
A	A	kA	A
Biography	Biographa	k1gFnSc2	Biographa
1940	[number]	k4	1940
Deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Odeon	odeon	k1gInSc1	odeon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
a	a	k8xC	a
vydáno	vydán	k2eAgNnSc1d1	vydáno
Leonardem	Leonardo	k1gMnSc7	Leonardo
Woolfem	Woolf	k1gMnSc7	Woolf
JOHNSTONE	JOHNSTONE	kA	JOHNSTONE
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
K.	K.	kA	K.
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Study	stud	k1gInPc1	stud
of	of	k?	of
E.	E.	kA	E.
M.	M.	kA	M.
Forster	Forster	k1gInSc1	Forster
<g/>
,	,	kIx,	,
Lytton	Lytton	k1gInSc1	Lytton
Strachey	Strachea	k1gFnSc2	Strachea
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gInSc1	Woolf
<g/>
,	,	kIx,	,
and	and	k?	and
Their	Their	k1gInSc1	Their
Circle	Circle	k1gInSc1	Circle
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
Secker	Secker	k1gMnSc1	Secker
&	&	k?	&
Warburg	Warburg	k1gMnSc1	Warburg
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
383	[number]	k4	383
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
WOOLF	WOOLF	kA	WOOLF
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
<g/>
.	.	kIx.	.
</s>
<s>
Beginning	Beginning	k1gInSc1	Beginning
Again	Again	k2eAgInSc1d1	Again
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
Hogarth	Hogarth	k1gMnSc1	Hogarth
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
BELL	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
Quentin	Quentin	k1gInSc1	Quentin
<g/>
.	.	kIx.	.
</s>
<s>
Bloomsbury	Bloomsbura	k1gFnPc1	Bloomsbura
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Basic	Basic	kA	Basic
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
126	[number]	k4	126
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
BELL	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
Quentin	Quentin	k1gInSc1	Quentin
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gInSc4	Woolf
<g/>
:	:	kIx,	:
A	a	k9	a
Biography	Biographa	k1gMnSc2	Biographa
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
-	-	kIx~	-
Virginia	Virginium	k1gNnSc2	Virginium
Stephen	Stephno	k1gNnPc2	Stephno
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
Hogarth	Hogarth	k1gInSc1	Hogarth
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7012	[number]	k4	7012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
291	[number]	k4	291
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
BELL	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
Quentin	Quentin	k1gInSc1	Quentin
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gInSc4	Woolf
<g/>
:	:	kIx,	:
A	a	k9	a
Biography	Biographa	k1gMnSc2	Biographa
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
-	-	kIx~	-
Mrs	Mrs	k1gFnSc2	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Woolf	Woolf	k1gInSc1	Woolf
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
Hogarth	Hogarth	k1gInSc1	Hogarth
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
300	[number]	k4	300
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7012	[number]	k4	7012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
371	[number]	k4	371
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
GARNETT	GARNETT	kA	GARNETT
<g/>
,	,	kIx,	,
Angelica	Angelicum	k1gNnSc2	Angelicum
<g/>
.	.	kIx.	.
</s>
<s>
Deceived	Deceived	k1gInSc1	Deceived
with	with	k1gInSc1	with
Kindness	Kindness	k1gInSc4	Kindness
<g/>
:	:	kIx,	:
A	a	k9	a
Bloomsbury	Bloomsbur	k1gInPc4	Bloomsbur
Childhood	Childhooda	k1gFnPc2	Childhooda
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
ROSENBAUM	ROSENBAUM	kA	ROSENBAUM
<g/>
,	,	kIx,	,
Stanford	Stanford	k1gMnSc1	Stanford
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Early	earl	k1gMnPc4	earl
Literary	Literara	k1gFnSc2	Literara
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Basingstoke	Basingstoke	k1gFnSc1	Basingstoke
:	:	kIx,	:
Macmillan	Macmillan	k1gMnSc1	Macmillan
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
3	[number]	k4	3
svazky	svazek	k1gInPc1	svazek
vydané	vydaný	k2eAgInPc1d1	vydaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
MARKER	marker	k1gInSc1	marker
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
W.	W.	kA	W.
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bloomsbury	Bloomsbur	k1gInPc1	Bloomsbur
Group	Group	k1gInSc1	Group
<g/>
:	:	kIx,	:
A	a	k8xC	a
Reference	reference	k1gFnPc1	reference
Guide	Guid	k1gMnSc5	Guid
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
:	:	kIx,	:
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HILSKÝ	HILSKÝ	kA	HILSKÝ
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Modernisté	modernista	k1gMnPc1	modernista
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85639	[number]	k4	85639
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
BELL	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
Quentin	Quentin	k1gInSc1	Quentin
<g/>
.	.	kIx.	.
</s>
<s>
Bloomsbury	Bloomsbura	k1gFnPc1	Bloomsbura
Recalled	Recalled	k1gInSc4	Recalled
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
234	[number]	k4	234
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
10564	[number]	k4	10564
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HILSKÝ	HILSKÝ	kA	HILSKÝ
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
docela	docela	k6eAd1	docela
malý	malý	k2eAgInSc1d1	malý
experiment	experiment	k1gInSc1	experiment
s	s	k7c7	s
hlemýžděm	hlemýžď	k1gMnSc7	hlemýžď
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
HILSKÝ	HILSKÝ	kA	HILSKÝ
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
NAGY	NAGY	kA	NAGY
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
slavíka	slavík	k1gMnSc2	slavík
k	k	k7c3	k
papouškovi	papoušek	k1gMnSc3	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7294	[number]	k4	7294
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
HILSKÝ	HILSKÝ	kA	HILSKÝ
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Květiny	květina	k1gFnPc1	květina
pro	pro	k7c4	pro
paní	paní	k1gFnSc4	paní
Dallowayovou	Dallowayový	k2eAgFnSc4d1	Dallowayový
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
HILSKÝ	HILSKÝ	kA	HILSKÝ
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
NAGY	NAGY	kA	NAGY
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
slavíka	slavík	k1gMnSc2	slavík
k	k	k7c3	k
papouškovi	papoušek	k1gMnSc3	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Doslov	doslov	k1gInSc1	doslov
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Paní	paní	k1gFnSc1	paní
Dallowayová	Dallowayová	k1gFnSc1	Dallowayová
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7294	[number]	k4	7294
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
HARRISOVÁ	HARRISOVÁ	kA	HARRISOVÁ
<g/>
,	,	kIx,	,
Alexandra	Alexandra	k1gFnSc1	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnPc4	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Kateřina	Kateřina	k1gFnSc1	Kateřina
Hilská	Hilská	k1gFnSc1	Hilská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
221	[number]	k4	221
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
1010	[number]	k4	1010
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Přeloženo	přeložit	k5eAaPmNgNnS	přeložit
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
FROULA	FROULA	kA	FROULA
<g/>
,	,	kIx,	,
Christine	Christin	k1gMnSc5	Christin
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gMnSc1	Woolf
and	and	k?	and
the	the	k?	the
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
Avantgarde	Avantgard	k1gMnSc5	Avantgard
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
428	[number]	k4	428
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
13444	[number]	k4	13444
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
Seznam	seznam	k1gInSc4	seznam
anglických	anglický	k2eAgMnPc2d1	anglický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Virginia	Virginium	k1gNnSc2	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
Osoba	osoba	k1gFnSc1	osoba
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gInSc1	Woolf
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Autor	autor	k1gMnSc1	autor
Virginia	Virginium	k1gNnSc2	Virginium
Woolf	Woolf	k1gMnSc1	Woolf
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
</s>
