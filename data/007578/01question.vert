<s>
Které	který	k3yIgNnSc1	který
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
arcibiskupství	arcibiskupství	k1gNnPc2	arcibiskupství
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
metropolity	metropolita	k1gMnSc2	metropolita
moravské	moravský	k2eAgFnSc2d1	Moravská
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
centrem	centrum	k1gNnSc7	centrum
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
?	?	kIx.	?
</s>
