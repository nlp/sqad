<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
fylé	fylá	k1gFnSc2	fylá
=	=	kIx~	=
kmen	kmen	k1gInSc1	kmen
a	a	k8xC	a
genesis	genesis	k1gFnSc1	genesis
=	=	kIx~	=
zrození	zrození	k1gNnSc1	zrození
<g/>
,	,	kIx,	,
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
fylogenetický	fylogenetický	k2eAgInSc1d1	fylogenetický
vývoj	vývoj	k1gInSc1	vývoj
znamená	znamenat	k5eAaImIp3nS	znamenat
vývoj	vývoj	k1gInSc4	vývoj
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
většinou	většinou	k6eAd1	většinou
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Protějškem	protějšek	k1gInSc7	protějšek
fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
je	být	k5eAaImIp3nS	být
ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
(	(	kIx(	(
<g/>
morfogeneze	morfogeneze	k1gFnSc1	morfogeneze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
vývoj	vývoj	k1gInSc1	vývoj
jedince	jedinec	k1gMnSc2	jedinec
k	k	k7c3	k
dospělosti	dospělost	k1gFnSc3	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
fylogenezi	fylogeneze	k1gFnSc4	fylogeneze
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fylogenetika	fylogenetika	k1gFnSc1	fylogenetika
<g/>
.	.	kIx.	.
</s>
<s>
Grafickým	grafický	k2eAgNnSc7d1	grafické
znázorněním	znázornění	k1gNnSc7	znázornění
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
skupinami	skupina	k1gFnPc7	skupina
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
fylogenetické	fylogenetický	k2eAgInPc1d1	fylogenetický
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
systematické	systematický	k2eAgNnSc1d1	systematické
uspořádání	uspořádání	k1gNnSc1	uspořádání
všech	všecek	k3xTgInPc2	všecek
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgInPc2d1	známý
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
švédský	švédský	k2eAgMnSc1d1	švédský
biolog	biolog	k1gMnSc1	biolog
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgMnSc1d1	Linné
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Systema	System	k1gMnSc2	System
naturae	natura	k1gMnSc2	natura
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linnéův	Linnéův	k2eAgInSc1d1	Linnéův
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
podobnostech	podobnost	k1gFnPc6	podobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Linné	Linná	k1gFnPc4	Linná
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
,	,	kIx,	,
a	a	k8xC	a
seřadil	seřadit	k5eAaPmAgMnS	seřadit
všechny	všechen	k3xTgInPc4	všechen
organismy	organismus	k1gInPc4	organismus
do	do	k7c2	do
hierarchie	hierarchie	k1gFnSc2	hierarchie
taxonů	taxon	k1gInPc2	taxon
(	(	kIx(	(
<g/>
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
biologové	biolog	k1gMnPc1	biolog
snažili	snažit	k5eAaImAgMnP	snažit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
tento	tento	k3xDgInSc4	tento
hierarchický	hierarchický	k2eAgInSc4d1	hierarchický
systém	systém	k1gInSc4	systém
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vznik	vznik	k1gInSc1	vznik
taxonů	taxon	k1gInPc2	taxon
dal	dát	k5eAaPmAgInS	dát
evolučně	evolučně	k6eAd1	evolučně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dnes	dnes	k6eAd1	dnes
užívaná	užívaný	k2eAgFnSc1d1	užívaná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
biologická	biologický	k2eAgFnSc1d1	biologická
taxonomie	taxonomie	k1gFnSc1	taxonomie
a	a	k8xC	a
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
(	(	kIx(	(
<g/>
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
taxony	taxon	k1gInPc1	taxon
monofyletické	monofyletický	k2eAgInPc1d1	monofyletický
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
společným	společný	k2eAgInSc7d1	společný
předkem	předek	k1gInSc7	předek
a	a	k8xC	a
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
všechny	všechen	k3xTgMnPc4	všechen
jeho	jeho	k3xOp3gMnPc4	jeho
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
polyfyletické	polyfyletický	k2eAgInPc4d1	polyfyletický
bez	bez	k7c2	bez
blízkého	blízký	k2eAgMnSc2d1	blízký
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
parafyletické	parafyletický	k2eAgInPc1d1	parafyletický
<g/>
,	,	kIx,	,
nezahrnující	zahrnující	k2eNgInPc1d1	nezahrnující
všechny	všechen	k3xTgMnPc4	všechen
potomky	potomek	k1gMnPc4	potomek
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sousedním	sousední	k2eAgInSc6d1	sousední
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
žlutě	žlutě	k6eAd1	žlutě
<g/>
)	)	kIx)	)
znázorněna	znázorněn	k2eAgFnSc1d1	znázorněna
monofyletická	monofyletický	k2eAgFnSc1d1	monofyletická
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgMnPc4d1	zahrnující
plazy	plaz	k1gMnPc4	plaz
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
tyrkysově	tyrkysově	k6eAd1	tyrkysově
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
třída	třída	k1gFnSc1	třída
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
červeně	červeně	k6eAd1	červeně
polyfyletická	polyfyletický	k2eAgFnSc1d1	polyfyletická
skupina	skupina	k1gFnSc1	skupina
teplokrevných	teplokrevný	k2eAgNnPc2d1	teplokrevné
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgNnPc1d1	zahrnující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
i	i	k8xC	i
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kladistika	Kladistika	k1gFnSc1	Kladistika
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
změnila	změnit	k5eAaPmAgFnS	změnit
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
rozluštění	rozluštění	k1gNnSc3	rozluštění
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
diskrétních	diskrétní	k2eAgNnPc2d1	diskrétní
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
algoritmicky	algoritmicky	k6eAd1	algoritmicky
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
porovnávat	porovnávat	k5eAaImF	porovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
dostupnost	dostupnost	k1gFnSc4	dostupnost
i	i	k8xC	i
možnosti	možnost	k1gFnPc4	možnost
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
prosazovat	prosazovat	k5eAaImF	prosazovat
nová	nový	k2eAgFnSc1d1	nová
mechanická	mechanický	k2eAgFnSc1d1	mechanická
metoda	metoda	k1gFnSc1	metoda
uspořádávání	uspořádávání	k1gNnSc2	uspořádávání
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
německý	německý	k2eAgMnSc1d1	německý
entomolog	entomolog	k1gMnSc1	entomolog
Willy	Willa	k1gFnSc2	Willa
Hennig	Hennig	k1gInSc1	Hennig
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
německo-americký	německomerický	k2eAgMnSc1d1	německo-americký
biolog	biolog	k1gMnSc1	biolog
Ernst	Ernst	k1gMnSc1	Ernst
Mayr	Mayr	k1gMnSc1	Mayr
nazval	nazvat	k5eAaPmAgMnS	nazvat
kladistikou	kladistika	k1gFnSc7	kladistika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
klados	klados	k1gInSc1	klados
<g/>
,	,	kIx,	,
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kladistika	Kladistika	k1gFnSc1	Kladistika
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
skupinu	skupina	k1gFnSc4	skupina
druhů	druh	k1gInPc2	druh
shromáždit	shromáždit	k5eAaPmF	shromáždit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
srovnatelných	srovnatelný	k2eAgNnPc2d1	srovnatelné
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
uspořádat	uspořádat	k5eAaPmF	uspořádat
druhy	druh	k1gInPc4	druh
do	do	k7c2	do
kladistického	kladistický	k2eAgInSc2d1	kladistický
stromu	strom	k1gInSc2	strom
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
například	například	k6eAd1	například
minimalizoval	minimalizovat	k5eAaBmAgInS	minimalizovat
počet	počet	k1gInSc1	počet
potřebných	potřebný	k2eAgFnPc2d1	potřebná
mutací	mutace	k1gFnPc2	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Kladistika	Kladistika	k1gFnSc1	Kladistika
ovšem	ovšem	k9	ovšem
pracuje	pracovat	k5eAaImIp3nS	pracovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
druhy	druh	k1gInPc7	druh
a	a	k8xC	a
nenabízí	nabízet	k5eNaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
vytvářet	vytvářet	k5eAaImF	vytvářet
vyšší	vysoký	k2eAgInPc1d2	vyšší
smysluplné	smysluplný	k2eAgInPc1d1	smysluplný
taxony	taxon	k1gInPc1	taxon
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
naopak	naopak	k6eAd1	naopak
pracují	pracovat	k5eAaImIp3nP	pracovat
nejen	nejen	k6eAd1	nejen
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pěstitelé	pěstitel	k1gMnPc1	pěstitel
a	a	k8xC	a
chovatelé	chovatel	k1gMnPc1	chovatel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
řešení	řešení	k1gNnSc6	řešení
problému	problém	k1gInSc2	problém
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
projekt	projekt	k1gInSc1	projekt
PhyloCode	PhyloCod	k1gMnSc5	PhyloCod
<g/>
.	.	kIx.	.
</s>
