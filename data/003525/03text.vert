<s>
Databáze	databáze	k1gFnPc1	databáze
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
datová	datový	k2eAgFnSc1d1	datová
základna	základna	k1gFnSc1	základna
<g/>
,	,	kIx,	,
též	též	k9	též
databanka	databanka	k1gFnSc1	databanka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
souborů	soubor	k1gInPc2	soubor
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
strukturou	struktura	k1gFnSc7	struktura
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
soubory	soubor	k1gInPc1	soubor
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
propojeny	propojit	k5eAaPmNgInP	propojit
pomocí	pomocí	k7c2	pomocí
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
databáze	databáze	k1gFnSc2	databáze
i	i	k8xC	i
softwarové	softwarový	k2eAgInPc1d1	softwarový
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
uloženými	uložený	k2eAgNnPc7d1	uložené
daty	datum	k1gNnPc7	datum
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
software	software	k1gInSc1	software
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
nazývá	nazývat	k5eAaImIp3nS	nazývat
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
báze	báze	k1gFnSc2	báze
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
SŘBD	SŘBD	kA	SŘBD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
označením	označení	k1gNnSc7	označení
databáze	databáze	k1gFnSc2	databáze
–	–	k?	–
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kontextu	kontext	k1gInSc6	kontext
–	–	k?	–
myslí	mysl	k1gFnPc2	mysl
jak	jak	k8xC	jak
uložená	uložený	k2eAgNnPc4d1	uložené
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
SŘBD	SŘBD	kA	SŘBD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
databází	databáze	k1gFnPc2	databáze
byly	být	k5eAaImAgFnP	být
papírové	papírový	k2eAgFnPc1d1	papírová
kartotéky	kartotéka	k1gFnPc1	kartotéka
<g/>
.	.	kIx.	.
</s>
<s>
Umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
uspořádávání	uspořádávání	k1gNnSc4	uspořádávání
dat	datum	k1gNnPc2	datum
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
a	a	k8xC	a
zatřiďování	zatřiďování	k1gNnSc6	zatřiďování
nových	nový	k2eAgFnPc2d1	nová
položek	položka	k1gFnPc2	položka
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
operace	operace	k1gFnPc1	operace
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
prováděl	provádět	k5eAaImAgMnS	provádět
přímo	přímo	k6eAd1	přímo
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
takových	takový	k3xDgFnPc2	takový
kartoték	kartotéka	k1gFnPc2	kartotéka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobná	podobný	k2eAgFnSc1d1	podobná
správě	správa	k1gFnSc3	správa
dnešních	dnešní	k2eAgFnPc2d1	dnešní
databází	databáze	k1gFnPc2	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
převedení	převedení	k1gNnSc1	převedení
zpracování	zpracování	k1gNnSc2	zpracování
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
strojové	strojový	k2eAgNnSc4d1	strojové
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
lze	lze	k6eAd1	lze
asi	asi	k9	asi
považovat	považovat	k5eAaImF	považovat
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Paměťovým	paměťový	k2eAgNnSc7d1	paměťové
médiem	médium	k1gNnSc7	médium
byl	být	k5eAaImAgInS	být
děrný	děrný	k2eAgInSc1d1	děrný
štítek	štítek	k1gInSc1	štítek
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
sebraných	sebraný	k2eAgFnPc2d1	sebraná
informací	informace	k1gFnPc2	informace
probíhalo	probíhat	k5eAaImAgNnS	probíhat
na	na	k7c6	na
elektromechanických	elektromechanický	k2eAgInPc6d1	elektromechanický
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Elektromechanické	elektromechanický	k2eAgInPc1d1	elektromechanický
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaPmAgInP	využívat
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zpracování	zpracování	k1gNnSc2	zpracování
dat	datum	k1gNnPc2	datum
další	další	k2eAgNnSc4d1	další
půlstoletí	půlstoletí	k1gNnSc4	půlstoletí
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
impulsem	impuls	k1gInSc7	impuls
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
databází	databáze	k1gFnPc2	databáze
byl	být	k5eAaImAgInS	být
překotný	překotný	k2eAgInSc1d1	překotný
vývoj	vývoj	k1gInSc1	vývoj
počítačů	počítač	k1gInPc2	počítač
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
univerzální	univerzální	k2eAgNnSc1d1	univerzální
používání	používání	k1gNnSc1	používání
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
procesorů	procesor	k1gInPc2	procesor
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
databázové	databázový	k2eAgFnPc4d1	databázová
úlohy	úloha	k1gFnPc4	úloha
neefektivní	efektivní	k2eNgFnPc4d1	neefektivní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
vyšší	vysoký	k2eAgInSc4d2	vyšší
jazyk	jazyk	k1gInSc4	jazyk
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
konference	konference	k1gFnSc1	konference
zástupců	zástupce	k1gMnPc2	zástupce
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
závěrem	závěr	k1gInSc7	závěr
byl	být	k5eAaImAgInS	být
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
univerzální	univerzální	k2eAgInSc4d1	univerzální
databázový	databázový	k2eAgInSc4d1	databázový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
CODASYL	CODASYL	kA	CODASYL
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
jazyka	jazyk	k1gInSc2	jazyk
COBOL	Cobol	kA	Cobol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
nejrozšířenějším	rozšířený	k2eAgMnSc7d3	nejrozšířenější
jazykem	jazyk	k1gMnSc7	jazyk
pro	pro	k7c4	pro
hromadné	hromadný	k2eAgNnSc4d1	hromadné
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
na	na	k7c4	na
konferenci	konference	k1gFnSc4	konference
CODASYL	CODASYL	kA	CODASYL
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
výbor	výbor	k1gInSc1	výbor
Database	Databasa	k1gFnSc3	Databasa
Task	Task	k1gMnSc1	Task
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
DBTG	DBTG	kA	DBTG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vytvořit	vytvořit	k5eAaPmF	vytvořit
koncepci	koncepce	k1gFnSc4	koncepce
databázových	databázový	k2eAgInPc2d1	databázový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgFnPc4	první
síťové	síťový	k2eAgFnPc4d1	síťová
SŘBD	SŘBD	kA	SŘBD
na	na	k7c6	na
sálových	sálový	k2eAgInPc6d1	sálový
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
průkopníků	průkopník	k1gMnPc2	průkopník
databází	databáze	k1gFnSc7	databáze
byl	být	k5eAaImAgMnS	být
Charles	Charles	k1gMnSc1	Charles
Bachman	Bachman	k1gMnSc1	Bachman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
vydal	vydat	k5eAaPmAgInS	vydat
výbor	výbor	k1gInSc1	výbor
zprávu	zpráva	k1gFnSc4	zpráva
The	The	k1gFnSc2	The
DBTG	DBTG	kA	DBTG
April	April	k1gInSc4	April
1971	[number]	k4	1971
Report	report	k1gInSc4	report
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
pojmy	pojem	k1gInPc1	pojem
jako	jako	k8xC	jako
schéma	schéma	k1gNnSc1	schéma
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc4	jazyk
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
schématu	schéma	k1gNnSc2	schéma
<g/>
,	,	kIx,	,
subschéma	subschémum	k1gNnSc2	subschémum
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
popsána	popsat	k5eAaPmNgFnS	popsat
celá	celý	k2eAgFnSc1d1	celá
architektura	architektura	k1gFnSc1	architektura
síťového	síťový	k2eAgInSc2d1	síťový
databázového	databázový	k2eAgInSc2d1	databázový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
vyvíjeny	vyvíjet	k5eAaImNgFnP	vyvíjet
i	i	k9	i
hierarchické	hierarchický	k2eAgFnPc1d1	hierarchická
databáze	databáze	k1gFnPc1	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
SŘBD	SŘBD	kA	SŘBD
byl	být	k5eAaImAgInS	být
IMS	IMS	kA	IMS
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
firmou	firma	k1gFnSc7	firma
IBM	IBM	kA	IBM
pro	pro	k7c4	pro
program	program	k1gInSc4	program
letu	let	k1gInSc2	let
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
Program	program	k1gInSc1	program
Apollo	Apollo	k1gNnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
IMS	IMS	kA	IMS
patří	patřit	k5eAaImIp3nS	patřit
stále	stále	k6eAd1	stále
k	k	k7c3	k
nejrozšířenějším	rozšířený	k2eAgFnPc3d3	nejrozšířenější
na	na	k7c6	na
sálových	sálový	k2eAgInPc6d1	sálový
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
začínají	začínat	k5eAaImIp3nP	začínat
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
článku	článek	k1gInSc2	článek
E.	E.	kA	E.
F.	F.	kA	F.
Codda	Codda	k1gMnSc1	Codda
první	první	k4xOgFnSc2	první
relační	relační	k2eAgFnSc2d1	relační
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pohlížejí	pohlížet	k5eAaImIp3nP	pohlížet
na	na	k7c4	na
data	datum	k1gNnPc4	datum
jako	jako	k8xC	jako
na	na	k7c4	na
tabulky	tabulka	k1gFnPc4	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
dotazovacího	dotazovací	k2eAgInSc2d1	dotazovací
jazyka	jazyk	k1gInSc2	jazyk
SQL	SQL	kA	SQL
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
po	po	k7c6	po
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
přinesl	přinést	k5eAaPmAgMnS	přinést
výkonově	výkonově	k6eAd1	výkonově
použitelné	použitelný	k2eAgInPc4d1	použitelný
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
se	s	k7c7	s
síťovými	síťový	k2eAgFnPc7d1	síťová
a	a	k8xC	a
hierarchickými	hierarchický	k2eAgFnPc7d1	hierarchická
databázemi	databáze	k1gFnPc7	databáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začínaly	začínat	k5eAaImAgInP	začínat
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInSc1	první
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
filozofie	filozofie	k1gFnSc1	filozofie
byla	být	k5eAaImAgFnS	být
přebírána	přebírat	k5eAaImNgFnS	přebírat
z	z	k7c2	z
objektově	objektově	k6eAd1	objektově
orientovaných	orientovaný	k2eAgInPc2d1	orientovaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
databáze	databáze	k1gFnPc1	databáze
měly	mít	k5eAaImAgFnP	mít
podle	podle	k7c2	podle
předpokladů	předpoklad	k1gInPc2	předpoklad
vytlačit	vytlačit	k5eAaPmF	vytlačit
relační	relační	k2eAgInPc4d1	relační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
předpoklady	předpoklad	k1gInPc1	předpoklad
se	se	k3xPyFc4	se
však	však	k9	však
nenaplnily	naplnit	k5eNaPmAgFnP	naplnit
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kompromisní	kompromisní	k2eAgFnSc1d1	kompromisní
objektově-relační	objektověelační	k2eAgFnSc1d1	objektově-relační
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
způsobu	způsob	k1gInSc3	způsob
ukládání	ukládání	k1gNnSc2	ukládání
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
databáze	databáze	k1gFnPc4	databáze
do	do	k7c2	do
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
Hierarchická	hierarchický	k2eAgFnSc1d1	hierarchická
databáze	databáze	k1gFnSc1	databáze
Síťová	síťový	k2eAgFnSc1d1	síťová
databáze	databáze	k1gFnSc1	databáze
Relační	relační	k2eAgFnSc2d1	relační
databáze	databáze	k1gFnSc2	databáze
Objektová	objektový	k2eAgFnSc1d1	objektová
databáze	databáze	k1gFnSc1	databáze
Objektově	objektově	k6eAd1	objektově
relační	relační	k2eAgFnSc2d1	relační
databáze	databáze	k1gFnSc2	databáze
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
databáze	databáze	k1gFnSc1	databáze
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zjednodušován	zjednodušován	k2eAgMnSc1d1	zjednodušován
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
databázový	databázový	k2eAgInSc1d1	databázový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
databázový	databázový	k2eAgInSc1d1	databázový
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
báze	báze	k1gFnSc2	báze
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
tabulky	tabulka	k1gFnSc2	tabulka
–	–	k?	–
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
tzv.	tzv.	kA	tzv.
databázových	databázový	k2eAgInPc2d1	databázový
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
databázových	databázový	k2eAgFnPc2d1	databázová
entit	entita	k1gFnPc2	entita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilejší	pokročilý	k2eAgInPc1d2	pokročilejší
databázové	databázový	k2eAgInPc1d1	databázový
systémy	systém	k1gInPc1	systém
dále	daleko	k6eAd2	daleko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
:	:	kIx,	:
pohledy	pohled	k1gInPc1	pohled
neboli	neboli	k8xC	neboli
views	views	k1gInSc1	views
–	–	k?	–
SQL	SQL	kA	SQL
příkazy	příkaz	k1gInPc1	příkaz
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
a	a	k8xC	a
uložené	uložený	k2eAgInPc4d1	uložený
v	v	k7c6	v
databázovém	databázový	k2eAgInSc6d1	databázový
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vybírat	vybírat	k5eAaImF	vybírat
(	(	kIx(	(
<g/>
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
příkaz	příkaz	k1gInSc1	příkaz
SELECT	SELECT	kA	SELECT
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
na	na	k7c4	na
ostatní	ostatní	k1gNnSc4	ostatní
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
indexy	index	k1gInPc1	index
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Klíče	klíč	k1gInPc1	klíč
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgInP	definovat
nad	nad	k7c7	nad
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
sloupci	sloupec	k1gInPc7	sloupec
tabulek	tabulka	k1gFnPc2	tabulka
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
klíč	klíč	k1gInSc1	klíč
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
vést	vést	k5eAaImF	vést
si	se	k3xPyFc3	se
v	v	k7c6	v
tabulkách	tabulka	k1gFnPc6	tabulka
rychlé	rychlý	k2eAgFnSc2d1	rychlá
LUT	LUT	kA	LUT
(	(	kIx(	(
<g/>
look-up	lookp	k1gMnSc1	look-up
tables	tables	k1gMnSc1	tables
–	–	k?	–
"	"	kIx"	"
<g/>
pořadníky	pořadník	k1gInPc4	pořadník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c4	na
sloupce	sloupec	k1gInPc4	sloupec
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimiž	jenž	k3xRgMnPc7	jenž
byly	být	k5eAaImAgFnP	být
definovány	definován	k2eAgFnPc1d1	definována
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vyloučit	vyloučit	k5eAaPmF	vyloučit
duplicitu	duplicita	k1gFnSc4	duplicita
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
nebo	nebo	k8xC	nebo
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
fulltextové	fulltextový	k2eAgNnSc4d1	Fulltextové
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
<g/>
.	.	kIx.	.
spouště	spoušť	k1gFnPc4	spoušť
neboli	neboli	k8xC	neboli
Trigger	Trigger	k1gInSc4	Trigger
<g/>
–	–	k?	–
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
nad	nad	k7c7	nad
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
řádkami	řádka	k1gFnPc7	řádka
tabulky	tabulka	k1gFnSc2	tabulka
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
samotnou	samotný	k2eAgFnSc7d1	samotná
tabulkou	tabulka	k1gFnSc7	tabulka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc6	odstranění
nebo	nebo	k8xC	nebo
přidání	přidání	k1gNnSc6	přidání
řádky	řádka	k1gFnSc2	řádka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
smazání	smazání	k1gNnSc1	smazání
tabulky	tabulka	k1gFnSc2	tabulka
a	a	k8xC	a
provede	provést	k5eAaPmIp3nS	provést
předprogramovanou	předprogramovaný	k2eAgFnSc4d1	předprogramovaná
akci	akce	k1gFnSc4	akce
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
například	například	k6eAd1	například
kontrolu	kontrola	k1gFnSc4	kontrola
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
doplění	doplění	k2eAgMnPc1d1	doplění
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
uživatelem	uživatel	k1gMnSc7	uživatel
definované	definovaný	k2eAgFnSc2d1	definovaná
procedury	procedura	k1gFnSc2	procedura
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
–	–	k?	–
některé	některý	k3yIgInPc1	některý
databázové	databázový	k2eAgInPc1d1	databázový
stroje	stroj	k1gInPc1	stroj
podporují	podporovat	k5eAaImIp3nP	podporovat
ukládání	ukládání	k1gNnSc4	ukládání
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
kusů	kus	k1gInPc2	kus
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
provedou	provést	k5eAaPmIp3nP	provést
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
nad	nad	k7c7	nad
danými	daný	k2eAgFnPc7d1	daná
tabulkami	tabulka	k1gFnPc7	tabulka
určitou	určitý	k2eAgFnSc4d1	určitá
sekvenci	sekvence	k1gFnSc4	sekvence
příkazů	příkaz	k1gInPc2	příkaz
(	(	kIx(	(
<g/>
procedury	procedura	k1gFnPc1	procedura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
navíc	navíc	k6eAd1	navíc
vrátí	vrátit	k5eAaPmIp3nS	vrátit
nějaký	nějaký	k3yIgInSc1	nějaký
výsledek	výsledek	k1gInSc1	výsledek
(	(	kIx(	(
<g/>
uživatelské	uživatelský	k2eAgFnSc2d1	Uživatelská
funkce	funkce	k1gFnSc2	funkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
vstupní	vstupní	k2eAgFnSc4d1	vstupní
(	(	kIx(	(
<g/>
IN	IN	kA	IN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výstupní	výstupní	k2eAgMnSc1d1	výstupní
(	(	kIx(	(
<g/>
OUT	OUT	kA	OUT
<g/>
)	)	kIx)	)
a	a	k8xC	a
vstupně-výstupní	vstupněýstupní	k2eAgFnSc7d1	vstupně-výstupní
(	(	kIx(	(
<g/>
INOUT	INOUT	kA	INOUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
též	též	k9	též
(	(	kIx(	(
<g/>
počeštěně	počeštěně	k6eAd1	počeštěně
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
eventy	event	k1gInPc1	event
<g/>
"	"	kIx"	"
–	–	k?	–
de	de	k?	de
facto	facto	k1gNnSc4	facto
procedury	procedura	k1gFnSc2	procedura
<g/>
,	,	kIx,	,
spouštěné	spouštěný	k2eAgFnSc2d1	spouštěná
v	v	k7c4	v
určitý	určitý	k2eAgInSc4d1	určitý
(	(	kIx(	(
<g/>
uživatelem	uživatel	k1gMnSc7	uživatel
definovaný	definovaný	k2eAgInSc4d1	definovaný
<g/>
)	)	kIx)	)
datum	datum	k1gInSc4	datum
a	a	k8xC	a
čas	čas	k1gInSc4	čas
nebo	nebo	k8xC	nebo
opakovaně	opakovaně	k6eAd1	opakovaně
s	s	k7c7	s
definovatelnou	definovatelný	k2eAgFnSc7d1	definovatelná
periodou	perioda	k1gFnSc7	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
údržbě	údržba	k1gFnSc3	údržba
<g/>
,	,	kIx,	,
promazávání	promazávání	k1gNnSc3	promazávání
dočasných	dočasný	k2eAgNnPc2d1	dočasné
dat	datum	k1gNnPc2	datum
či	či	k8xC	či
kontrolování	kontrolování	k1gNnSc2	kontrolování
referenční	referenční	k2eAgFnSc2d1	referenční
integrity	integrita	k1gFnSc2	integrita
<g/>
.	.	kIx.	.
formuláře	formulář	k1gInPc1	formulář
–	–	k?	–
některé	některý	k3yIgInPc4	některý
databázové	databázový	k2eAgInPc4d1	databázový
systémy	systém	k1gInPc4	systém
jako	jako	k8xC	jako
např.	např.	kA	např.
Microsoft	Microsoft	kA	Microsoft
Access	Accessa	k1gFnPc2	Accessa
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
vytvářet	vytvářet	k5eAaImF	vytvářet
vstupní	vstupní	k2eAgInPc4d1	vstupní
formuláře	formulář	k1gInPc4	formulář
pro	pro	k7c4	pro
vizuálně	vizuálně	k6eAd1	vizuálně
přívětivé	přívětivý	k2eAgNnSc4d1	přívětivé
zadávání	zadávání	k1gNnSc4	zadávání
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
např.	např.	kA	např.
nadefinovat	nadefinovat	k5eAaPmF	nadefinovat
rozložení	rozložení	k1gNnSc4	rozložení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vstupních	vstupní	k2eAgFnPc2d1	vstupní
polí	pole	k1gFnPc2	pole
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
popisky	popiska	k1gFnPc4	popiska
atd.	atd.	kA	atd.
sestavy	sestava	k1gFnSc2	sestava
nebo	nebo	k8xC	nebo
též	též	k9	též
reporty	report	k1gInPc1	report
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
formulářů	formulář	k1gInPc2	formulář
sestavy	sestava	k1gFnSc2	sestava
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
definovat	definovat	k5eAaBmF	definovat
layout	layout	k1gInSc4	layout
s	s	k7c7	s
políčky	políček	k1gInPc7	políček
dané	daný	k2eAgFnSc2d1	daná
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
doplní	doplnit	k5eAaPmIp3nP	doplnit
aktuální	aktuální	k2eAgFnPc4d1	aktuální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
prezentaci	prezentace	k1gFnSc4	prezentace
nebo	nebo	k8xC	nebo
pouhé	pouhý	k2eAgNnSc4d1	pouhé
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sestavy	sestava	k1gFnPc1	sestava
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c4	o
filtry	filtr	k1gInPc4	filtr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyfiltrují	vyfiltrovat	k5eAaPmIp3nP	vyfiltrovat
jen	jen	k9	jen
kýžené	kýžený	k2eAgInPc1d1	kýžený
záznamy	záznam	k1gInPc1	záznam
<g/>
.	.	kIx.	.
uživatelská	uživatelský	k2eAgNnPc1d1	Uživatelské
oprávnění	oprávnění	k1gNnSc4	oprávnění
–	–	k?	–
u	u	k7c2	u
lepších	dobrý	k2eAgInPc2d2	lepší
databázových	databázový	k2eAgInPc2d1	databázový
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
nabídnout	nabídnout	k5eAaPmF	nabídnout
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
oddělit	oddělit	k5eAaPmF	oddělit
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
úrovně	úroveň	k1gFnSc2	úroveň
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
objektům	objekt	k1gInPc3	objekt
databáze	databáze	k1gFnSc2	databáze
jejich	jejich	k3xOp3gMnPc3	jejich
uživatelům	uživatel	k1gMnPc3	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Možností	možnost	k1gFnSc7	možnost
bývají	bývat	k5eAaImIp3nP	bývat
desítky	desítka	k1gFnPc1	desítka
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
typy	typ	k1gInPc4	typ
příkazů	příkaz	k1gInPc2	příkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ten	ten	k3xDgInSc4	ten
který	který	k3yIgMnSc1	který
uživatel	uživatel	k1gMnSc1	uživatel
bude	být	k5eAaImBp3nS	být
nebo	nebo	k8xC	nebo
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
spustit	spustit	k5eAaPmF	spustit
<g/>
.	.	kIx.	.
partitioning	partitioning	k1gInSc1	partitioning
–	–	k?	–
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rozdělit	rozdělit	k5eAaPmF	rozdělit
data	datum	k1gNnPc4	datum
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
na	na	k7c4	na
více	hodně	k6eAd2	hodně
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rozložit	rozložit	k5eAaPmF	rozložit
zátěž	zátěž	k1gFnSc4	zátěž
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
kladenou	kladený	k2eAgFnSc4d1	kladená
procesy	proces	k1gInPc1	proces
–	–	k?	–
databázové	databázový	k2eAgInPc1d1	databázový
stroje	stroj	k1gInPc1	stroj
<g />
.	.	kIx.	.
</s>
<s>
umí	umět	k5eAaImIp3nS	umět
podat	podat	k5eAaPmF	podat
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jejich	jejich	k3xOp3gFnPc2	jejich
služeb	služba	k1gFnPc2	služba
aktuálně	aktuálně	k6eAd1	aktuálně
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
.	.	kIx.	.
proměnné	proměnný	k2eAgNnSc1d1	proměnné
nastavení	nastavení	k1gNnSc1	nastavení
–	–	k?	–
typicky	typicky	k6eAd1	typicky
desítky	desítka	k1gFnPc1	desítka
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
přenastavovat	přenastavovat	k5eAaBmF	přenastavovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
výkon	výkon	k1gInSc4	výkon
databázového	databázový	k2eAgInSc2d1	databázový
stroje	stroj	k1gInSc2	stroj
jako	jako	k8xC	jako
takového	takový	k3xDgMnSc4	takový
<g/>
.	.	kIx.	.
collation	collation	k1gInSc1	collation
–	–	k?	–
MySQL	MySQL	k1gFnSc1	MySQL
má	mít	k5eAaImIp3nS	mít
pokročilé	pokročilý	k2eAgFnPc4d1	pokročilá
možnosti	možnost	k1gFnPc4	možnost
pro	pro	k7c4	pro
nastavení	nastavení	k1gNnSc4	nastavení
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
znakových	znakový	k2eAgFnPc2d1	znaková
sad	sada	k1gFnPc2	sada
a	a	k8xC	a
porovnávání	porovnávání	k1gNnSc6	porovnávání
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
collation	collation	k1gInSc4	collation
<g/>
.	.	kIx.	.
</s>
<s>
Nastavení	nastavení	k1gNnSc1	nastavení
collation	collation	k1gInSc1	collation
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
textové	textový	k2eAgInPc4d1	textový
sloupce	sloupec	k1gInPc4	sloupec
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnSc2d1	celá
tabulky	tabulka	k1gFnSc2	tabulka
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
databáze	databáze	k1gFnSc2	databáze
(	(	kIx(	(
<g/>
s	s	k7c7	s
kaskádovitou	kaskádovitý	k2eAgFnSc7d1	kaskádovitá
dědičností	dědičnost	k1gFnSc7	dědičnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Collation	Collation	k1gInSc1	Collation
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
řazení	řazení	k1gNnSc1	řazení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hodnota	hodnota	k1gFnSc1	hodnota
utf	utf	k?	utf
<g/>
8	[number]	k4	8
<g/>
_czech_ci	_czech_ce	k1gMnPc7	_czech_ce
zajistí	zajistit	k5eAaPmIp3nS	zajistit
správné	správný	k2eAgNnSc1d1	správné
řazení	řazení	k1gNnSc1	řazení
podle	podle	k7c2	podle
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
včetně	včetně	k7c2	včetně
diakritiky	diakritika	k1gFnSc2	diakritika
a	a	k8xC	a
včetně	včetně	k7c2	včetně
ch	ch	k0	ch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
vizuální	vizuální	k2eAgFnSc1d1	vizuální
E-R	E-R	k1gFnSc1	E-R
schéma	schéma	k1gNnSc1	schéma
–	–	k?	–
(	(	kIx(	(
<g/>
v	v	k7c6	v
MySQL	MySQL	k1gFnSc6	MySQL
INFORMATION	INFORMATION	kA	INFORMATION
<g/>
.	.	kIx.	.
<g/>
SCHEMA	schema	k1gNnSc1	schema
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1	vizuální
reprezentace	reprezentace	k1gFnSc1	reprezentace
vztahů	vztah	k1gInPc2	vztah
(	(	kIx(	(
<g/>
relací	relace	k1gFnPc2	relace
<g/>
)	)	kIx)	)
na	na	k7c6	na
sobě	se	k3xPyFc3	se
závislých	závislý	k2eAgFnPc2d1	závislá
polí	pole	k1gFnPc2	pole
(	(	kIx(	(
<g/>
cizích	cizí	k2eAgInPc2d1	cizí
klíčů	klíč	k1gInPc2	klíč
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
tabulkam	tabulkam	k1gInSc4	tabulkam
<g/>
.	.	kIx.	.
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Databázová	databázový	k2eAgFnSc1d1	databázová
integrita	integrita	k1gFnSc1	integrita
<g/>
.	.	kIx.	.
</s>
<s>
Databázová	databázový	k2eAgFnSc1d1	databázová
integrita	integrita	k1gFnSc1	integrita
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
databázi	databáze	k1gFnSc6	databáze
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
soustavě	soustava	k1gFnSc6	soustava
určitých	určitý	k2eAgNnPc2d1	určité
definovaných	definovaný	k2eAgNnPc2d1	definované
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
obvykle	obvykle	k6eAd1	obvykle
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
vybraným	vybraný	k2eAgNnPc3d1	vybrané
pravidlům	pravidlo	k1gNnPc3	pravidlo
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
části	část	k1gFnSc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
databáze	databáze	k1gFnSc1	databáze
slouží	sloužit	k5eAaImIp3nS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c4	o
pravidla	pravidlo	k1gNnPc4	pravidlo
stanovující	stanovující	k2eAgInSc4d1	stanovující
rozsah	rozsah	k1gInSc4	rozsah
uložených	uložený	k2eAgFnPc2d1	uložená
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
typ	typ	k1gInSc1	typ
nebo	nebo	k8xC	nebo
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
MySQL	MySQL	k?	MySQL
PostgreSQL	PostgreSQL	k1gMnSc1	PostgreSQL
Oracle	Oracl	k1gMnSc2	Oracl
Microsoft	Microsoft	kA	Microsoft
SQL	SQL	kA	SQL
Server	server	k1gInSc1	server
SQLite	SQLit	k1gInSc5	SQLit
Firebird	Firebird	k1gInSc4	Firebird
Integritní	integritní	k2eAgNnSc4d1	integritní
omezení	omezení	k1gNnSc4	omezení
Normální	normální	k2eAgFnSc1d1	normální
forma	forma	k1gFnSc1	forma
Multimediální	multimediální	k2eAgFnSc2d1	multimediální
databáze	databáze	k1gFnSc2	databáze
Relační	relační	k2eAgNnSc4d1	relační
schéma	schéma	k1gNnSc4	schéma
databáze	databáze	k1gFnSc2	databáze
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
databáze	databáze	k1gFnSc2	databáze
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Databáze	databáze	k1gFnSc1	databáze
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Databáze	databáze	k1gFnSc2	databáze
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
http://www.dbsvet.cz	[url]	k1gInSc1	http://www.dbsvet.cz
–	–	k?	–
Databázový	databázový	k2eAgInSc1d1	databázový
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
o	o	k7c6	o
databázích	databáze	k1gFnPc6	databáze
Seriál	seriál	k1gInSc1	seriál
Embedded	Embedded	k1gInSc4	Embedded
databáze	databáze	k1gFnSc2	databáze
</s>
