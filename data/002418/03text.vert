<s>
Endemický	endemický	k2eAgInSc1d1	endemický
kretenismus	kretenismus	k1gInSc1	kretenismus
je	být	k5eAaImIp3nS	být
vývojová	vývojový	k2eAgFnSc1d1	vývojová
porucha	porucha	k1gFnSc1	porucha
způsobená	způsobený	k2eAgFnSc1d1	způsobená
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
hypothyreózou	hypothyreóza	k1gFnSc7	hypothyreóza
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nedostatek	nedostatek	k1gInSc4	nedostatek
jódu	jód	k1gInSc2	jód
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
matky	matka	k1gFnSc2	matka
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
funkce	funkce	k1gFnSc1	funkce
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
matky	matka	k1gFnSc2	matka
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
slova	slovo	k1gNnSc2	slovo
crétin	crétina	k1gFnPc2	crétina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
slabomyslný	slabomyslný	k2eAgMnSc1d1	slabomyslný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
projevů	projev	k1gInPc2	projev
kretenismu	kretenismus	k1gInSc2	kretenismus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
malého	malý	k2eAgInSc2d1	malý
vzrůstu	vzrůst	k1gInSc2	vzrůst
a	a	k8xC	a
snížené	snížený	k2eAgFnSc2d1	snížená
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
endemický	endemický	k2eAgInSc4d1	endemický
kretenismus	kretenismus	k1gInSc4	kretenismus
typické	typický	k2eAgFnPc1d1	typická
též	též	k9	též
hluchoněmost	hluchoněmost	k1gFnSc1	hluchoněmost
<g/>
,	,	kIx,	,
svalová	svalový	k2eAgFnSc1d1	svalová
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
motorikou	motorika	k1gFnSc7	motorika
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
lehký	lehký	k2eAgInSc1d1	lehký
nedostatek	nedostatek	k1gInSc1	nedostatek
jódu	jód	k1gInSc2	jód
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
snížení	snížení	k1gNnSc4	snížení
inteligenčního	inteligenční	k2eAgInSc2d1	inteligenční
kvocientu	kvocient	k1gInSc2	kvocient
o	o	k7c4	o
5	[number]	k4	5
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoji	rozvoj	k1gInPc7	rozvoj
choroby	choroba	k1gFnSc2	choroba
lze	lze	k6eAd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
včasnou	včasný	k2eAgFnSc7d1	včasná
léčbou	léčba	k1gFnSc7	léčba
spočívající	spočívající	k2eAgFnSc7d1	spočívající
v	v	k7c6	v
podávání	podávání	k1gNnSc6	podávání
jódu	jód	k1gInSc2	jód
nebo	nebo	k8xC	nebo
hormonů	hormon	k1gInPc2	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
obvykle	obvykle	k6eAd1	obvykle
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
běžného	běžný	k2eAgInSc2d1	běžný
fyzického	fyzický	k2eAgInSc2d1	fyzický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
léčbou	léčba	k1gFnSc7	léčba
začne	začít	k5eAaPmIp3nS	začít
až	až	k9	až
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
nevratnou	vratný	k2eNgFnSc7d1	nevratná
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
lidi	člověk	k1gMnPc4	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jód	jód	k1gInSc1	jód
je	být	k5eAaImIp3nS	být
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
prvkem	prvek	k1gInSc7	prvek
stravy	strava	k1gFnSc2	strava
<g/>
,	,	kIx,	,
nutným	nutný	k2eAgNnSc7d1	nutné
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
hormonů	hormon	k1gInPc2	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bývá	bývat	k5eAaImIp3nS	bývat
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
potravinách	potravina	k1gFnPc6	potravina
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nP	muset
ho	on	k3xPp3gInSc4	on
vždy	vždy	k6eAd1	vždy
být	být	k5eAaImF	být
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
potravina	potravina	k1gFnSc1	potravina
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přirozeným	přirozený	k2eAgInSc7d1	přirozený
zdrojem	zdroj	k1gInSc7	zdroj
jódu	jód	k1gInSc2	jód
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
existují	existovat	k5eAaImIp3nP	existovat
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
vnitrozemské	vnitrozemský	k2eAgFnSc2d1	vnitrozemská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
půda	půda	k1gFnSc1	půda
dostatek	dostatek	k1gInSc1	dostatek
jódu	jód	k1gInSc2	jód
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
následně	následně	k6eAd1	následně
ho	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
i	i	k9	i
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
a	a	k8xC	a
mase	maso	k1gNnSc6	maso
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
vystaveny	vystavit	k5eAaPmNgInP	vystavit
riziku	riziko	k1gNnSc6	riziko
chorob	choroba	k1gFnPc2	choroba
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
jódu	jód	k1gInSc2	jód
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
endemického	endemický	k2eAgInSc2d1	endemický
kretenismu	kretenismus	k1gInSc2	kretenismus
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
jódu	jód	k1gInSc2	jód
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
různé	různý	k2eAgFnSc2d1	různá
míry	míra	k1gFnSc2	míra
negativně	negativně	k6eAd1	negativně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
fyzický	fyzický	k2eAgInSc4d1	fyzický
i	i	k8xC	i
mentální	mentální	k2eAgInSc4d1	mentální
vývoj	vývoj	k1gInSc4	vývoj
postižených	postižený	k2eAgMnPc2d1	postižený
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
postupné	postupný	k2eAgNnSc1d1	postupné
zvětšení	zvětšení	k1gNnSc1	zvětšení
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
též	též	k9	též
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
struma	struma	k1gFnSc1	struma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
boje	boj	k1gInSc2	boj
různých	různý	k2eAgFnPc2d1	různá
kampaní	kampaň	k1gFnPc2	kampaň
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
spočívajících	spočívající	k2eAgInPc2d1	spočívající
v	v	k7c6	v
podávání	podávání	k1gNnSc6	podávání
jódu	jód	k1gInSc2	jód
ohroženým	ohrožený	k2eAgMnPc3d1	ohrožený
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
endemický	endemický	k2eAgInSc1d1	endemický
kretenismus	kretenismus	k1gInSc1	kretenismus
vymizel	vymizet	k5eAaPmAgInS	vymizet
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jód	jód	k1gInSc1	jód
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
potravních	potravní	k2eAgInPc2d1	potravní
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
do	do	k7c2	do
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Struma	struma	k1gFnSc1	struma
Štítná	štítný	k2eAgFnSc1d1	štítná
žláza	žláza	k1gFnSc1	žláza
Hormony	hormon	k1gInPc4	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cretinism	Cretinisma	k1gFnPc2	Cretinisma
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Endemický	endemický	k2eAgInSc4d1	endemický
kretenismus	kretenismus	k1gInSc4	kretenismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kretenismus	kretenismus	k1gInSc1	kretenismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
