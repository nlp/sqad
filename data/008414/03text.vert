<p>
<s>
Tilman	Tilman	k1gMnSc1	Tilman
Riemenschneider	Riemenschneider	k1gMnSc1	Riemenschneider
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1460	[number]	k4	1460
<g/>
,	,	kIx,	,
Heilbad	Heilbad	k1gInSc1	Heilbad
Heiligenstadt	Heiligenstadt	k1gInSc1	Heiligenstadt
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1531	[number]	k4	1531
<g/>
,	,	kIx,	,
Würzburg	Würzburg	k1gMnSc1	Würzburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
stylově	stylově	k6eAd1	stylově
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Riemenschneider	Riemenschneider	k1gMnSc1	Riemenschneider
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Osterode	Osterod	k1gMnSc5	Osterod
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
rodina	rodina	k1gFnSc1	rodina
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c6	o
jeho	jeho	k3xOp3gNnPc6	jeho
učňovských	učňovský	k2eAgNnPc6d1	učňovské
letech	léto	k1gNnPc6	léto
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Martin	Martin	k1gMnSc1	Martin
Schongauer	Schongauer	k1gMnSc1	Schongauer
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
se	se	k3xPyFc4	se
jako	jako	k9	jako
tovaryš	tovaryš	k1gMnSc1	tovaryš
usadil	usadit	k5eAaPmAgMnS	usadit
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
malířského	malířský	k2eAgInSc2d1	malířský
cechu	cech	k1gInSc2	cech
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Annu	Anna	k1gFnSc4	Anna
Schmidtovou	Schmidtová	k1gFnSc4	Schmidtová
<g/>
,	,	kIx,	,
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
mistru	mistr	k1gMnSc6	mistr
zlatotepci	zlatotepec	k1gMnSc6	zlatotepec
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Riemenschneiderovi	Riemenschneider	k1gMnSc3	Riemenschneider
přineslo	přinést	k5eAaPmAgNnS	přinést
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
porodila	porodit	k5eAaPmAgFnS	porodit
Riemenschneiderovi	Riemenschneider	k1gMnSc3	Riemenschneider
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1494	[number]	k4	1494
<g/>
;	;	kIx,	;
sochař	sochař	k1gMnSc1	sochař
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
třikrát	třikrát	k6eAd1	třikrát
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
další	další	k2eAgFnPc4d1	další
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
rovněž	rovněž	k9	rovněž
výtvarně	výtvarně	k6eAd1	výtvarně
nadané	nadaný	k2eAgFnPc4d1	nadaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
se	se	k3xPyFc4	se
Tilman	Tilman	k1gMnSc1	Tilman
Riemenschneider	Riemenschneider	k1gMnSc1	Riemenschneider
těšil	těšit	k5eAaImAgMnS	těšit
již	již	k9	již
značné	značný	k2eAgFnSc3d1	značná
umělecké	umělecký	k2eAgFnSc3d1	umělecká
reputaci	reputace	k1gFnSc3	reputace
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
několik	několik	k4yIc4	několik
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vinici	vinice	k1gFnSc4	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1520	[number]	k4	1520
až	až	k9	až
1524	[number]	k4	1524
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
Würzburgu	Würzburg	k1gInSc2	Würzburg
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
selské	selský	k2eAgFnSc2d1	selská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Würzburg	Würzburg	k1gMnSc1	Würzburg
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
.	.	kIx.	.
</s>
<s>
Tilman	Tilman	k1gMnSc1	Tilman
Riemenschneider	Riemenschneider	k1gMnSc1	Riemenschneider
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
zabaven	zabavit	k5eAaPmNgInS	zabavit
majetek	majetek	k1gInSc1	majetek
a	a	k8xC	a
mučením	mučení	k1gNnSc7	mučení
mu	on	k3xPp3gNnSc3	on
prý	prý	k9	prý
byly	být	k5eAaImAgFnP	být
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
ruce	ruka	k1gFnPc1	ruka
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
své	svůj	k3xOyFgNnSc4	svůj
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
v	v	k7c6	v
řezbářské	řezbářský	k2eAgFnSc6d1	řezbářská
živnosti	živnost	k1gFnSc6	živnost
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
syn	syn	k1gMnSc1	syn
Jörg	Jörg	k1gMnSc1	Jörg
(	(	kIx(	(
<g/>
Georg	Georg	k1gMnSc1	Georg
<g/>
)	)	kIx)	)
ze	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
asteroid	asteroid	k1gInSc4	asteroid
(	(	kIx(	(
<g/>
6145	[number]	k4	6145
<g/>
)	)	kIx)	)
Riemenschneider	Riemenschneidra	k1gFnPc2	Riemenschneidra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tilman	Tilman	k1gMnSc1	Tilman
Riemenschneider	Riemenschneidra	k1gFnPc2	Riemenschneidra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
