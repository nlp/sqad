<p>
<s>
Vec	Vec	k?	Vec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
Moravce	Moravka	k1gFnSc6	Moravka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Branislav	Branislav	k1gMnSc1	Branislav
Kováč	Kováč	k1gMnSc1	Kováč
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
DJ	DJ	kA	DJ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
založil	založit	k5eAaPmAgInS	založit
s	s	k7c7	s
Midim	Midim	k1gInSc4	Midim
první	první	k4xOgFnSc4	první
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
rapovou	rapový	k2eAgFnSc4d1	rapová
formaci	formace	k1gFnSc4	formace
Trosky	troska	k1gFnSc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Trosky	troska	k1gFnSc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
mj.	mj.	kA	mj.
politickými	politický	k2eAgNnPc7d1	politické
i	i	k8xC	i
sociálními	sociální	k2eAgNnPc7d1	sociální
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
přispěl	přispět	k5eAaPmAgMnS	přispět
svou	svůj	k3xOyFgFnSc4	svůj
skladbou	skladba	k1gFnSc7	skladba
Calex	Calex	k1gInSc4	Calex
City	city	k1gNnSc1	city
2001	[number]	k4	2001
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
produkci	produkce	k1gFnSc6	produkce
a	a	k8xC	a
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
skreči	skreč	k1gInPc7	skreč
na	na	k7c4	na
album	album	k1gNnSc4	album
From	From	k1gInSc4	From
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
To	to	k9	to
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
přichází	přicházet	k5eAaImIp3nS	přicházet
Vec	Vec	k1gMnSc1	Vec
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
albem	album	k1gNnSc7	album
Dobré	dobrá	k1gFnSc2	dobrá
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
pochází	pocházet	k5eAaImIp3nP	pocházet
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xC	jako
Na	na	k7c6	na
horách	hora	k1gFnPc6	hora
nebo	nebo	k8xC	nebo
Gde	Gde	k1gFnSc2	Gde
ste	ste	k?	ste
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgMnS	vydat
album	album	k1gNnSc4	album
Funkčný	Funkčný	k2eAgMnSc1d1	Funkčný
Veterán	veterán	k1gMnSc1	veterán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sólo	sólo	k2eAgNnPc4d1	sólo
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Dobré	dobrý	k2eAgNnSc1d1	dobré
Ráno	ráno	k1gNnSc1	ráno
(	(	kIx(	(
<g/>
Escape	Escap	k1gMnSc5	Escap
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Funkčný	Funkčný	k2eAgMnSc1d1	Funkčný
Veterán	veterán	k1gMnSc1	veterán
(	(	kIx(	(
<g/>
Escape	Escap	k1gInSc5	Escap
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Funkčné	Funkčný	k2eAgInPc1d1	Funkčný
remixy	remix	k1gInPc1	remix
(	(	kIx(	(
<g/>
Escape	Escap	k1gInSc5	Escap
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Stereo	stereo	k6eAd1	stereo
Farbo	Farba	k1gFnSc5	Farba
Slepo	Slepa	k1gFnSc5	Slepa
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Domáce	Domáec	k1gInSc2	Domáec
potreby	potreba	k1gFnSc2	potreba
(	(	kIx(	(
<g/>
Sam	Sam	k1gMnSc1	Sam
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Videoklipy	videoklip	k1gInPc4	videoklip
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sólo	sólo	k2eAgInPc1d1	sólo
klipy	klip	k1gInPc1	klip
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Na	na	k7c6	na
horách	hora	k1gFnPc6	hora
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Gde	Gde	k1gMnSc1	Gde
ste	ste	k?	ste
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Zverina	Zverina	k1gFnSc1	Zverina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Turista	turista	k1gMnSc1	turista
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
PIO	PIO	kA	PIO
Squad	Squad	k1gInSc1	Squad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Tu	tu	k6eAd1	tu
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Supa	sup	k1gMnSc4	sup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Vianočná	Vianočný	k2eAgFnSc1d1	Vianočná
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Moja	Moja	k?	Moja
Reč	Reč	k1gMnSc1	Reč
<g/>
,	,	kIx,	,
Zverina	Zverina	k1gMnSc1	Zverina
&	&	k?	&
Hafner	Hafner	k1gMnSc1	Hafner
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Branči	Branči	k1gMnSc1	Branči
Kováč	Kováč	k1gMnSc1	Kováč
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Placebo	placebo	k1gNnSc1	placebo
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Kúsky	Kúska	k1gFnSc2	Kúska
ma	ma	k?	ma
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
Slovák	Slovák	k1gMnSc1	Slovák
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc1d1	ostatní
klipy	klip	k1gInPc1	klip
===	===	k?	===
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Tozmemi	Tozme	k1gFnPc7	Tozme
(	(	kIx(	(
<g/>
Trosky	troska	k1gFnPc1	troska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Dávej	dávat	k5eAaImRp2nS	dávat
bacha	bacha	k?	bacha
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Winterová	Winterová	k1gFnSc1	Winterová
<g/>
,	,	kIx,	,
Moja	Moja	k?	Moja
Reč	Reč	k1gMnSc1	Reč
<g/>
,	,	kIx,	,
Vec	Vec	k1gMnSc1	Vec
&	&	k?	&
Supercrooo	Supercrooo	k1gMnSc1	Supercrooo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Kariera	Kariera	k1gFnSc1	Kariera
16	[number]	k4	16
(	(	kIx(	(
<g/>
Supercrooo	Supercrooo	k6eAd1	Supercrooo
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
H16	H16	k4	H16
&	&	k?	&
Vec	Vec	k1gMnSc1	Vec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
Hip	hip	k0	hip
Hop	hop	k0	hop
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Hazard	hazard	k1gMnSc1	hazard
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Mišo	Mišo	k1gNnSc1	Mišo
Biely	Biela	k1gFnSc2	Biela
<g/>
,	,	kIx,	,
Otecko	otecko	k1gMnSc1	otecko
<g/>
,	,	kIx,	,
Opak	opak	k1gInSc1	opak
<g/>
,	,	kIx,	,
Vec	Vec	k1gFnSc1	Vec
<g/>
,	,	kIx,	,
Supa	sup	k1gMnSc4	sup
<g/>
,	,	kIx,	,
Delik	Delik	k1gMnSc1	Delik
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
518	[number]	k4	518
<g/>
,	,	kIx,	,
Majk	Majk	k1gInSc1	Majk
Spirit	Spirit	k1gInSc1	Spirit
<g/>
,	,	kIx,	,
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
Moe	Moe	k1gFnSc1	Moe
<g/>
,	,	kIx,	,
Zverina	Zverina	k1gFnSc1	Zverina
&	&	k?	&
DJ	DJ	kA	DJ
Metys	Metys	k1gInSc1	Metys
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Wack	Wack	k1gMnSc1	Wack
MCs	MCs	k1gMnSc1	MCs
(	(	kIx(	(
<g/>
Oliver	Oliver	k1gMnSc1	Oliver
Lowe	Low	k1gFnSc2	Low
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rest	rest	k6eAd1	rest
<g/>
,	,	kIx,	,
Vec	Vec	k1gFnSc1	Vec
&	&	k?	&
DJ	DJ	kA	DJ
Alyaz	Alyaz	k1gInSc1	Alyaz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Ledové	ledový	k2eAgNnSc4d1	ledové
ostří	ostří	k1gNnSc4	ostří
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Vec	Vec	k?	Vec
<g/>
,	,	kIx,	,
Zverina	Zverin	k2eAgFnSc1d1	Zverin
&	&	k?	&
Indy	Indus	k1gInPc4	Indus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
biografie	biografie	k1gFnSc1	biografie
Vece	veka	k1gFnSc3	veka
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
kapely	kapela	k1gFnSc2	kapela
Trosky	troska	k1gFnSc2	troska
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
StreetFame	StreetFam	k1gInSc5	StreetFam
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
FUNKCNY	FUNKCNY	kA	FUNKCNY
VETERAN	VETERAN	kA	VETERAN
–	–	k?	–
Myspace	Myspace	k1gFnSc1	Myspace
</s>
</p>
<p>
<s>
Funkčný	Funkčný	k2eAgMnSc1d1	Funkčný
veterán	veterán	k1gMnSc1	veterán
Vec	Vec	k1gFnSc2	Vec
sa	sa	k?	sa
nevyhýba	nevyhýba	k1gFnSc1	nevyhýba
žiadnym	žiadnym	k1gInSc1	žiadnym
témam	témam	k1gInSc1	témam
–	–	k?	–
SME	SME	k?	SME
</s>
</p>
