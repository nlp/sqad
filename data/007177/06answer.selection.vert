<s>
Hladítko	hladítko	k1gNnSc1	hladítko
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
zednický	zednický	k2eAgInSc1d1	zednický
hoblík	hoblík	k1gInSc1	hoblík
<g/>
,	,	kIx,	,
hobl	hobl	k1gMnSc1	hobl
<g/>
,	,	kIx,	,
rajbret	rajbret	k1gMnSc1	rajbret
–	–	k?	–
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
originálu	originál	k1gInSc2	originál
"	"	kIx"	"
<g/>
das	das	k?	das
Reibebrett	Reibebrett	k1gInSc1	Reibebrett
<g/>
"	"	kIx"	"
–	–	k?	–
hladítko	hladítko	k1gNnSc4	hladítko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zednický	zednický	k2eAgInSc4d1	zednický
nástroj	nástroj	k1gInSc4	nástroj
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
vyhlazování	vyhlazování	k1gNnSc3	vyhlazování
a	a	k8xC	a
nanášení	nanášení	k1gNnSc3	nanášení
omítek	omítka	k1gFnPc2	omítka
<g/>
,	,	kIx,	,
lepidel	lepidlo	k1gNnPc2	lepidlo
<g/>
,	,	kIx,	,
malty	malta	k1gFnSc2	malta
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
