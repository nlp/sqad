<s>
Hladítko	hladítko	k1gNnSc1	hladítko
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
zednický	zednický	k2eAgInSc1d1	zednický
hoblík	hoblík	k1gInSc1	hoblík
<g/>
,	,	kIx,	,
hobl	hobl	k1gMnSc1	hobl
<g/>
,	,	kIx,	,
rajbret	rajbret	k1gMnSc1	rajbret
–	–	k?	–
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
originálu	originál	k1gInSc2	originál
"	"	kIx"	"
<g/>
das	das	k?	das
Reibebrett	Reibebrett	k1gInSc1	Reibebrett
<g/>
"	"	kIx"	"
–	–	k?	–
hladítko	hladítko	k1gNnSc4	hladítko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zednický	zednický	k2eAgInSc4d1	zednický
nástroj	nástroj	k1gInSc4	nástroj
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
vyhlazování	vyhlazování	k1gNnSc3	vyhlazování
a	a	k8xC	a
nanášení	nanášení	k1gNnSc3	nanášení
omítek	omítka	k1gFnPc2	omítka
<g/>
,	,	kIx,	,
lepidel	lepidlo	k1gNnPc2	lepidlo
<g/>
,	,	kIx,	,
malty	malta	k1gFnSc2	malta
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
z	z	k7c2	z
pokličkového	pokličkový	k2eAgInSc2d1	pokličkový
typu	typ	k1gInSc2	typ
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Plastové	plastový	k2eAgNnSc4d1	plastové
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
a	a	k8xC	a
ocelové	ocelový	k2eAgNnSc4d1	ocelové
hladítko	hladítko	k1gNnSc4	hladítko
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
natahovák	natahovák	k1gInSc4	natahovák
neboli	neboli	k8xC	neboli
natahovací	natahovací	k2eAgNnSc4d1	natahovací
hladítko	hladítko	k1gNnSc4	hladítko
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zednickou	zednický	k2eAgFnSc7d1	zednická
lžící	lžíce	k1gFnSc7	lžíce
jsou	být	k5eAaImIp3nP	být
hladítka	hladítko	k1gNnPc1	hladítko
základním	základní	k2eAgNnSc7d1	základní
vybavením	vybavení	k1gNnSc7	vybavení
každého	každý	k3xTgMnSc2	každý
omítkáře	omítkář	k1gMnSc2	omítkář
<g/>
,	,	kIx,	,
fasádníka	fasádník	k1gMnSc2	fasádník
a	a	k8xC	a
zedníka	zedník	k1gMnSc2	zedník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čerstvě	čerstvě	k6eAd1	čerstvě
nahozenou	nahozený	k2eAgFnSc4d1	nahozená
maltu	malta	k1gFnSc4	malta
se	se	k3xPyFc4	se
přiloží	přiložit	k5eAaPmIp3nS	přiložit
zdola	zdola	k6eAd1	zdola
spodní	spodní	k2eAgFnSc1d1	spodní
hrana	hrana	k1gFnSc1	hrana
plastového	plastový	k2eAgNnSc2d1	plastové
hladítka	hladítko	k1gNnSc2	hladítko
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
hrana	hrana	k1gFnSc1	hrana
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
odkloní	odklonit	k5eAaPmIp3nS	odklonit
<g/>
,	,	kIx,	,
přitlačí	přitlačit	k5eAaPmIp3nS	přitlačit
se	se	k3xPyFc4	se
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
omítka	omítka	k1gFnSc1	omítka
zhutní	zhutnit	k5eAaPmIp3nS	zhutnit
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
vyrovná	vyrovnat	k5eAaPmIp3nS	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
strhne	strhnout	k5eAaPmIp3nS	strhnout
latí	lať	k1gFnPc2	lať
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavadnutí	zavadnutí	k1gNnSc6	zavadnutí
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
přiložíme	přiložit	k5eAaPmIp1nP	přiložit
hladítko	hladítko	k1gNnSc4	hladítko
a	a	k8xC	a
krouživými	krouživý	k2eAgInPc7d1	krouživý
pohyby	pohyb	k1gInPc7	pohyb
povrch	povrch	k6eAd1wR	povrch
zhruba	zhruba	k6eAd1	zhruba
vyhladíme	vyhladit	k5eAaPmIp1nP	vyhladit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
naneseme	nanést	k5eAaBmIp1nP	nanést
hladítkem	hladítko	k1gNnSc7	hladítko
vrstvu	vrstva	k1gFnSc4	vrstva
omítky	omítka	k1gFnSc2	omítka
jemné	jemný	k2eAgFnSc2d1	jemná
<g/>
,	,	kIx,	,
po	po	k7c6	po
zavadnutí	zavadnutí	k1gNnSc6	zavadnutí
ji	on	k3xPp3gFnSc4	on
vyrovnáme	vyrovnat	k5eAaBmIp1nP	vyrovnat
a	a	k8xC	a
vyhladíme	vyhladit	k5eAaPmIp1nP	vyhladit
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
hladítkem	hladítko	k1gNnSc7	hladítko
z	z	k7c2	z
pěnové	pěnový	k2eAgFnSc2d1	pěnová
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Omítka	omítka	k1gFnSc1	omítka
je	být	k5eAaImIp3nS	být
hotová	hotový	k2eAgFnSc1d1	hotová
<g/>
.	.	kIx.	.
</s>
<s>
Plastové	plastový	k2eAgNnSc1d1	plastové
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
novodurák	novodurák	k1gInSc1	novodurák
<g/>
)	)	kIx)	)
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
plastovou	plastový	k2eAgFnSc4d1	plastová
desku	deska	k1gFnSc4	deska
180	[number]	k4	180
<g/>
×	×	k?	×
<g/>
400	[number]	k4	400
mm	mm	kA	mm
<g/>
,	,	kIx,	,
tloušťky	tloušťka	k1gFnSc2	tloušťka
4	[number]	k4	4
mm	mm	kA	mm
<g/>
,	,	kIx,	,
s	s	k7c7	s
rukojetí	rukojeť	k1gFnSc7	rukojeť
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
existovalo	existovat	k5eAaImAgNnS	existovat
jen	jen	k6eAd1	jen
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
<g/>
:	:	kIx,	:
K	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
natahování	natahování	k1gNnSc3	natahování
omítky	omítka	k1gFnSc2	omítka
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
hrubému	hrubý	k2eAgNnSc3d1	hrubé
srovnání	srovnání	k1gNnSc3	srovnání
nahozené	nahozený	k2eAgFnSc2d1	nahozená
omítky	omítka	k1gFnSc2	omítka
před	před	k7c4	před
stržení	stržení	k1gNnSc4	stržení
latí	lať	k1gFnPc2	lať
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
natažení	natažení	k1gNnSc3	natažení
jemné	jemný	k2eAgFnSc2d1	jemná
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
(	(	kIx(	(
<g/>
dřevovohobl	dřevovohobnout	k5eAaPmAgMnS	dřevovohobnout
<g/>
;	;	kIx,	;
rajbret	rajbret	k1gInSc1	rajbret
<g/>
)	)	kIx)	)
–	–	k?	–
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
nenahraditelné	nahraditelný	k2eNgInPc1d1	nenahraditelný
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
tuhého	tuhý	k2eAgNnSc2d1	tuhé
(	(	kIx(	(
<g/>
ne	ne	k9	ne
tekutého	tekutý	k2eAgMnSc2d1	tekutý
<g/>
)	)	kIx)	)
betonu	beton	k1gInSc2	beton
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
velikostech	velikost	k1gFnPc6	velikost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
zedník	zedník	k1gMnSc1	zedník
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
nebo	nebo	k8xC	nebo
nechá	nechat	k5eAaPmIp3nS	nechat
vyrobit	vyrobit	k5eAaPmF	vyrobit
dle	dle	k7c2	dle
svých	svůj	k3xOyFgInPc2	svůj
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Prodávaný	prodávaný	k2eAgMnSc1d1	prodávaný
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
měkkého	měkký	k2eAgNnSc2d1	měkké
smrkového	smrkový	k2eAgNnSc2d1	smrkové
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
z	z	k7c2	z
měkkého	měkký	k2eAgInSc2d1	měkký
a	a	k8xC	a
lehkého	lehký	k2eAgInSc2d1	lehký
topolového	topolový	k2eAgInSc2d1	topolový
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
srazí	srazit	k5eAaPmIp3nP	srazit
hrany	hrana	k1gFnPc1	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
někteří	některý	k3yIgMnPc1	některý
zedníci	zedník	k1gMnPc1	zedník
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc1	přednost
tvrdému	tvrdý	k2eAgNnSc3d1	tvrdé
dřevu	dřevo	k1gNnSc3	dřevo
jasanovému	jasanový	k2eAgNnSc3d1	jasanové
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
hrany	hrana	k1gFnPc1	hrana
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
betonové	betonový	k2eAgFnSc2d1	betonová
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
hladítku	hladítko	k1gNnSc6	hladítko
říkalo	říkat	k5eAaImAgNnS	říkat
kdysi	kdysi	k6eAd1	kdysi
zednický	zednický	k2eAgInSc4d1	zednický
hoblík	hoblík	k1gInSc4	hoblík
<g/>
.	.	kIx.	.
</s>
<s>
Zedník	Zedník	k1gMnSc1	Zedník
např.	např.	kA	např.
změřil	změřit	k5eAaPmAgMnS	změřit
výšku	výška	k1gFnSc4	výška
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jít	jít	k5eAaImF	jít
o	o	k7c4	o
5	[number]	k4	5
mm	mm	kA	mm
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Přiloží	přiložit	k5eAaPmIp3nS	přiložit
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
hladítko	hladítko	k1gNnSc1	hladítko
<g/>
,	,	kIx,	,
přitlačí	přitlačit	k5eAaPmIp3nS	přitlačit
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
dělat	dělat	k5eAaImF	dělat
krouživé	krouživý	k2eAgInPc4d1	krouživý
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Hrany	hrana	k1gFnPc1	hrana
hladítka	hladítko	k1gNnSc2	hladítko
strhují	strhovat	k5eAaImIp3nP	strhovat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
shoblovávají	shoblovávat	k5eAaPmIp3nP	shoblovávat
beton	beton	k1gInSc4	beton
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c4	na
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
shoblovaný	shoblovaný	k2eAgInSc4d1	shoblovaný
beton	beton	k1gInSc4	beton
hladítkem	hladítko	k1gNnSc7	hladítko
odhrne	odhrnout	k5eAaPmIp3nS	odhrnout
a	a	k8xC	a
plochu	plocha	k1gFnSc4	plocha
jemně	jemně	k6eAd1	jemně
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yInSc7	co
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
zručnost	zručnost	k1gFnSc4	zručnost
pracovník	pracovník	k1gMnSc1	pracovník
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Laik	laik	k1gMnSc1	laik
je	být	k5eAaImIp3nS	být
udiven	udivit	k5eAaPmNgMnS	udivit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vše	všechen	k3xTgNnSc1	všechen
tento	tento	k3xDgInSc1	tento
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
nástroj	nástroj	k1gInSc1	nástroj
dokáže	dokázat	k5eAaPmIp3nS	dokázat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
mistra	mistr	k1gMnSc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
betonová	betonový	k2eAgFnSc1d1	betonová
podlaha	podlaha	k1gFnSc1	podlaha
plus-mínus	plusínus	k1gInSc1	plus-mínus
2	[number]	k4	2
mm	mm	kA	mm
pod	pod	k7c7	pod
latí	lať	k1gFnSc7	lať
4	[number]	k4	4
metry	metr	k1gInPc4	metr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Drahá	drahý	k2eAgFnSc1d1	drahá
nivelační	nivelační	k2eAgFnSc1d1	nivelační
malta	malta	k1gFnSc1	malta
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
hladítko	hladítko	k1gNnSc1	hladítko
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
velké	velký	k2eAgNnSc4d1	velké
uplatnění	uplatnění	k1gNnSc4	uplatnění
u	u	k7c2	u
památkářských	památkářský	k2eAgMnPc2d1	památkářský
zedníků	zedník	k1gMnPc2	zedník
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc1d1	staré
několik	několik	k4yIc1	několik
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Plstěné	plstěný	k2eAgFnPc1d1	plstěná
(	(	kIx(	(
<g/>
filcové	filcový	k2eAgFnPc1d1	filcová
<g/>
;	;	kIx,	;
filc	filc	k1gInSc1	filc
<g/>
,	,	kIx,	,
hobl	hobl	k1gInSc1	hobl
<g/>
)	)	kIx)	)
–	–	k?	–
náleží	náležet	k5eAaImIp3nS	náležet
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
také	také	k9	také
mezi	mezi	k7c4	mezi
historickou	historický	k2eAgFnSc4d1	historická
zednickou	zednický	k2eAgFnSc4d1	zednická
klasiku	klasika	k1gFnSc4	klasika
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
ho	on	k3xPp3gMnSc4	on
ještě	ještě	k9	ještě
používají	používat	k5eAaImIp3nP	používat
památkářští	památkářský	k2eAgMnPc1d1	památkářský
zedníci	zedník	k1gMnPc1	zedník
a	a	k8xC	a
štukatéři	štukatér	k1gMnPc1	štukatér
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
už	už	k6eAd1	už
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
na	na	k7c4	na
jemnou	jemný	k2eAgFnSc4d1	jemná
vápennou	vápenný	k2eAgFnSc4d1	vápenná
maltu	malta	k1gFnSc4	malta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
asi	asi	k9	asi
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
míchala	míchat	k5eAaImAgFnS	míchat
postaru	postaru	k6eAd1	postaru
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
zcela	zcela	k6eAd1	zcela
převládla	převládnout	k5eAaPmAgFnS	převládnout
už	už	k6eAd1	už
hotová	hotový	k2eAgFnSc1d1	hotová
jemná	jemný	k2eAgFnSc1d1	jemná
malta	malta	k1gFnSc1	malta
v	v	k7c6	v
pytlích	pytel	k1gInPc6	pytel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pohodlnější	pohodlný	k2eAgMnSc1d2	pohodlnější
a	a	k8xC	a
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
používat	používat	k5eAaImF	používat
hladítka	hladítko	k1gNnPc1	hladítko
z	z	k7c2	z
pěnové	pěnový	k2eAgFnSc2d1	pěnová
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
vápennou	vápenný	k2eAgFnSc4d1	vápenná
maltu	malta	k1gFnSc4	malta
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
filcové	filcový	k2eAgNnSc1d1	filcové
hladítko	hladítko	k1gNnSc1	hladítko
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Filcových	filcový	k2eAgNnPc2d1	filcové
hladítek	hladítko	k1gNnPc2	hladítko
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
šířek	šířka	k1gFnPc2	šířka
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
zedníci	zedník	k1gMnPc1	zedník
si	se	k3xPyFc3	se
zhotovovali	zhotovovat	k5eAaImAgMnP	zhotovovat
i	i	k9	i
svoje	svůj	k3xOyFgInPc4	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
měl	mít	k5eAaImAgMnS	mít
doma	doma	k6eAd1	doma
desku	deska	k1gFnSc4	deska
tvrdé	tvrdá	k1gFnSc2	tvrdá
plsti-filcu	plstiilcus	k1gInSc2	plsti-filcus
<g/>
,	,	kIx,	,
uřízl	uříznout	k5eAaPmAgMnS	uříznout
potřebný	potřebný	k2eAgInSc4d1	potřebný
kus	kus	k1gInSc4	kus
a	a	k8xC	a
nalepil	nalepit	k5eAaPmAgMnS	nalepit
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
chlupy	chlup	k1gInPc1	chlup
nad	nad	k7c7	nad
ohněm	oheň	k1gInSc7	oheň
opálit	opálit	k5eAaPmF	opálit
<g/>
.	.	kIx.	.
</s>
<s>
Koupit	koupit	k5eAaPmF	koupit
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
jen	jen	k9	jen
základní	základní	k2eAgNnSc1d1	základní
provedení	provedení	k1gNnSc1	provedení
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
125	[number]	k4	125
<g/>
×	×	k?	×
<g/>
265	[number]	k4	265
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Hladítko	hladítko	k1gNnSc1	hladítko
z	z	k7c2	z
pěnové	pěnový	k2eAgFnSc2d1	pěnová
gumy	guma	k1gFnSc2	guma
–	–	k?	–
je	být	k5eAaImIp3nS	být
novější	nový	k2eAgInSc4d2	novější
typ	typ	k1gInSc4	typ
hladítka	hladítko	k1gNnSc2	hladítko
a	a	k8xC	a
přišlo	přijít	k5eAaPmAgNnS	přijít
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
jemných	jemný	k2eAgFnPc2d1	jemná
hotových	hotový	k2eAgFnPc2d1	hotová
malt	malta	k1gFnPc2	malta
v	v	k7c6	v
pytlích	pytel	k1gInPc6	pytel
<g/>
,	,	kIx,	,
převládlo	převládnout	k5eAaPmAgNnS	převládnout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
hladítko	hladítko	k1gNnSc1	hladítko
plstěné	plstěný	k2eAgNnSc1d1	plstěné
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc1	rozměr
125	[number]	k4	125
<g/>
×	×	k?	×
<g/>
265	[number]	k4	265
mm	mm	kA	mm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tuhosti	tuhost	k1gFnSc2	tuhost
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
pěnové	pěnový	k2eAgFnSc2d1	pěnová
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Molitanové	molitanový	k2eAgFnPc1d1	molitanová
–	–	k?	–
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
s	s	k7c7	s
dosažitelností	dosažitelnost	k1gFnSc7	dosažitelnost
molitanu	molitan	k1gInSc2	molitan
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Molitan	molitan	k1gInSc1	molitan
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
měkký	měkký	k2eAgInSc1d1	měkký
a	a	k8xC	a
omítku	omítka	k1gFnSc4	omítka
nesrovnával	srovnávat	k5eNaImAgMnS	srovnávat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byla	být	k5eAaImAgFnS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
pohodlná	pohodlný	k2eAgFnSc1d1	pohodlná
<g/>
,	,	kIx,	,
nemuselo	muset	k5eNaImAgNnS	muset
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
tlačit	tlačit	k5eAaImF	tlačit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
povrch	povrch	k1gInSc1	povrch
omítky	omítka	k1gFnSc2	omítka
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
fušeřina	fušeřina	k1gFnSc1	fušeřina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
podřadné	podřadný	k2eAgFnPc4d1	podřadná
omítky	omítka	k1gFnPc4	omítka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
kotelně	kotelna	k1gFnSc6	kotelna
nebo	nebo	k8xC	nebo
garáži	garáž	k1gFnSc6	garáž
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgNnSc1d1	ocelové
hladítko	hladítko	k1gNnSc1	hladítko
(	(	kIx(	(
<g/>
gleťák	gleťák	k1gInSc1	gleťák
–	–	k?	–
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
die	die	k?	die
Glätte	Glätt	k1gInSc5	Glätt
<g/>
"	"	kIx"	"
–	–	k?	–
hladkost	hladkost	k1gFnSc1	hladkost
<g/>
,	,	kIx,	,
uhlazenost	uhlazenost	k1gFnSc1	uhlazenost
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
současně	současně	k6eAd1	současně
s	s	k7c7	s
cementem	cement	k1gInSc7	cement
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
už	už	k6eAd1	už
přes	přes	k7c4	přes
100	[number]	k4	100
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tabulka	tabulka	k1gFnSc1	tabulka
pružného	pružný	k2eAgInSc2d1	pružný
ocelového	ocelový	k2eAgInSc2d1	ocelový
plechu	plech	k1gInSc2	plech
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
0,8	[number]	k4	0,8
až	až	k9	až
1	[number]	k4	1
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
hranaté	hranatý	k2eAgNnSc1d1	hranaté
<g/>
,	,	kIx,	,
pravoúhlé	pravoúhlý	k2eAgNnSc1d1	pravoúhlé
<g/>
,	,	kIx,	,
šíře	šíře	k1gFnSc1	šíře
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
podélně	podélně	k6eAd1	podélně
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
půlkulatého	půlkulatý	k2eAgNnSc2d1	půlkulaté
zakončení	zakončení	k1gNnSc2	zakončení
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
45	[number]	k4	45
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Délku	délka	k1gFnSc4	délka
má	mít	k5eAaImIp3nS	mít
250	[number]	k4	250
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejlevnější	levný	k2eAgInSc4d3	nejlevnější
povrch	povrch	k1gInSc4	povrch
betonové	betonový	k2eAgFnSc2d1	betonová
podlahy	podlaha	k1gFnSc2	podlaha
byl	být	k5eAaImAgInS	být
povrch	povrch	k1gInSc4	povrch
zagletovaný	zagletovaný	k2eAgInSc4d1	zagletovaný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
dlažba	dlažba	k1gFnSc1	dlažba
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zcela	zcela	k6eAd1	zcela
převládal	převládat	k5eAaImAgInS	převládat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
betonové	betonový	k2eAgFnSc2d1	betonová
podlahy	podlaha	k1gFnSc2	podlaha
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
hladítkem	hladítko	k1gNnSc7	hladítko
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
beton	beton	k1gInSc1	beton
poprášil	poprášit	k5eAaPmAgInS	poprášit
cementem	cement	k1gInSc7	cement
<g/>
.	.	kIx.	.
</s>
<s>
Dělalo	dělat	k5eAaImAgNnS	dělat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
kovovým	kovový	k2eAgNnSc7d1	kovové
kuchyňským	kuchyňský	k2eAgNnSc7d1	kuchyňské
sítkem	sítko	k1gNnSc7	sítko
a	a	k8xC	a
připomínalo	připomínat	k5eAaImAgNnS	připomínat
to	ten	k3xDgNnSc1	ten
posypávání	posypávání	k1gNnSc1	posypávání
koláčů	koláč	k1gInPc2	koláč
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Naprášený	naprášený	k2eAgInSc4d1	naprášený
cement	cement	k1gInSc4	cement
zvlhnul	zvlhnout	k5eAaPmAgMnS	zvlhnout
a	a	k8xC	a
zedník	zedník	k1gMnSc1	zedník
hranou	hrana	k1gFnSc7	hrana
ocelového	ocelový	k2eAgNnSc2d1	ocelové
hladítka	hladítko	k1gNnSc2	hladítko
–	–	k?	–
zešikma	zešikma	k6eAd1	zešikma
–	–	k?	–
ho	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgInS	začít
přejíždět	přejíždět	k5eAaImF	přejíždět
a	a	k8xC	a
zatlačovat	zatlačovat	k5eAaImF	zatlačovat
do	do	k7c2	do
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Přejížděl	přejíždět	k5eAaImAgMnS	přejíždět
tak	tak	k9	tak
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sklovitě	sklovitě	k6eAd1	sklovitě
lesklý	lesklý	k2eAgInSc4d1	lesklý
a	a	k8xC	a
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyzrání	vyzrání	k1gNnSc6	vyzrání
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
a	a	k8xC	a
trvanlivý	trvanlivý	k2eAgInSc1d1	trvanlivý
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zagletovat	zagletovat	k5eAaBmF	zagletovat
i	i	k9	i
omítku	omítka	k1gFnSc4	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
<g/>
:	:	kIx,	:
prádelny	prádelna	k1gFnPc1	prádelna
<g/>
,	,	kIx,	,
kotelny	kotelna	k1gFnPc1	kotelna
<g/>
,	,	kIx,	,
dílny	dílna	k1gFnPc1	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
beton	beton	k1gInSc1	beton
byl	být	k5eAaImAgInS	být
nevzhledný	vzhledný	k2eNgMnSc1d1	nevzhledný
<g/>
,	,	kIx,	,
šedočerné	šedočerný	k2eAgFnPc1d1	šedočerná
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
natíral	natírat	k5eAaImAgMnS	natírat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
olejovou	olejový	k2eAgFnSc7d1	olejová
nebo	nebo	k8xC	nebo
syntetickou	syntetický	k2eAgFnSc7d1	syntetická
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
nátěr	nátěr	k1gInSc1	nátěr
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
časem	čas	k1gInSc7	čas
obnovovat	obnovovat	k5eAaImF	obnovovat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hladítko	hladítko	k1gNnSc1	hladítko
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
přišlo	přijít	k5eAaPmAgNnS	přijít
ze	z	k7c2	z
Západu	západ	k1gInSc2	západ
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
nových	nový	k2eAgFnPc2d1	nová
metod	metoda	k1gFnPc2	metoda
při	při	k7c6	při
lepení	lepení	k1gNnSc6	lepení
obkladů	obklad	k1gInPc2	obklad
a	a	k8xC	a
dlažby	dlažba	k1gFnSc2	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
také	také	k9	také
přišla	přijít	k5eAaPmAgFnS	přijít
spousta	spousta	k1gFnSc1	spousta
nových	nový	k2eAgNnPc2d1	nové
lepidel	lepidlo	k1gNnPc2	lepidlo
a	a	k8xC	a
tmelů	tmel	k1gInPc2	tmel
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
metoda	metoda	k1gFnSc1	metoda
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
tak	tak	k6eAd1	tak
rovné	rovný	k2eAgFnPc4d1	rovná
zdi	zeď	k1gFnPc4	zeď
nebo	nebo	k8xC	nebo
podlahy	podlaha	k1gFnPc4	podlaha
<g/>
,	,	kIx,	,
zubové	zubový	k2eAgNnSc4d1	Zubové
hladítko	hladítko	k1gNnSc4	hladítko
natáhlo	natáhnout	k5eAaPmAgNnS	natáhnout
díky	díky	k7c3	díky
zubům	zub	k1gInPc3	zub
pruhy	pruh	k1gInPc7	pruh
lepidla	lepidlo	k1gNnSc2	lepidlo
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
8	[number]	k4	8
mm	mm	kA	mm
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
sklonu	sklon	k1gInSc2	sklon
hladítka	hladítko	k1gNnSc2	hladítko
při	při	k7c6	při
natahování	natahování	k1gNnSc6	natahování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obkládačka	obkládačka	k1gFnSc1	obkládačka
nebo	nebo	k8xC	nebo
dlaždice	dlaždice	k1gFnSc1	dlaždice
je	být	k5eAaImIp3nS	být
nalepena	nalepit	k5eAaPmNgFnS	nalepit
na	na	k7c6	na
proužcích	proužec	k1gInPc6	proužec
lepidla	lepidlo	k1gNnSc2	lepidlo
jen	jen	k8xS	jen
padesáti	padesát	k4xCc7	padesát
procenty	procent	k1gInPc7	procent
své	svůj	k3xOyFgFnSc2	svůj
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Proužky	proužka	k1gFnPc1	proužka
lepidla	lepidlo	k1gNnSc2	lepidlo
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgFnPc1d1	měkká
a	a	k8xC	a
poddajné	poddajný	k2eAgFnPc1d1	poddajná
a	a	k8xC	a
přilepené	přilepený	k2eAgFnPc1d1	přilepená
dlaždice	dlaždice	k1gFnPc1	dlaždice
nebo	nebo	k8xC	nebo
obkládačky	obkládačka	k1gFnPc1	obkládačka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
lehce	lehko	k6eAd1	lehko
srovnají	srovnat	k5eAaPmIp3nP	srovnat
přitlačenou	přitlačený	k2eAgFnSc7d1	přitlačená
latí	lať	k1gFnSc7	lať
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
podlaha	podlaha	k1gFnSc1	podlaha
nebo	nebo	k8xC	nebo
zeď	zeď	k1gFnSc1	zeď
tak	tak	k6eAd1	tak
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
konečné	konečný	k2eAgNnSc1d1	konečné
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
sílou	síla	k1gFnSc7	síla
podmazání	podmazání	k1gNnSc2	podmazání
pod	pod	k7c4	pod
obklad	obklad	k1gInSc4	obklad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neodborné	odborný	k2eNgFnSc6d1	neodborná
práci	práce	k1gFnSc6	práce
nebo	nebo	k8xC	nebo
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
nevhodných	vhodný	k2eNgInPc2d1	nevhodný
tmelů	tmel	k1gInPc2	tmel
není	být	k5eNaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
práce	práce	k1gFnSc2	práce
ideální	ideální	k2eAgFnSc2d1	ideální
(	(	kIx(	(
<g/>
hodně	hodně	k6eAd1	hodně
reklamací	reklamace	k1gFnPc2	reklamace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
odpadání	odpadání	k1gNnSc3	odpadání
obkladaček	obkladačka	k1gFnPc2	obkladačka
nebo	nebo	k8xC	nebo
uvolnění	uvolnění	k1gNnSc4	uvolnění
dlaždic	dlaždice	k1gFnPc2	dlaždice
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odmrznutí	odmrznutí	k1gNnSc4	odmrznutí
na	na	k7c6	na
terasách	terasa	k1gFnPc6	terasa
nebo	nebo	k8xC	nebo
otřes	otřes	k1gInSc4	otřes
nosné	nosný	k2eAgFnSc2d1	nosná
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
průvan	průvan	k1gInSc1	průvan
pořádně	pořádně	k6eAd1	pořádně
práskne	prásknout	k5eAaPmIp3nS	prásknout
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
příčka	příčka	k1gFnSc1	příčka
se	se	k3xPyFc4	se
otřese	otřást	k5eAaPmIp3nS	otřást
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Špičkoví	špičkový	k2eAgMnPc1d1	špičkový
zedníci	zedník	k1gMnPc1	zedník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
hlídají	hlídat	k5eAaImIp3nP	hlídat
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
reklamaci	reklamace	k1gFnSc4	reklamace
<g/>
,	,	kIx,	,
odmítají	odmítat	k5eAaImIp3nP	odmítat
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Zubové	zubový	k2eAgNnSc4d1	Zubové
hladítko	hladítko	k1gNnSc4	hladítko
nepoužívají	používat	k5eNaImIp3nP	používat
a	a	k8xC	a
ani	ani	k8xC	ani
ho	on	k3xPp3gMnSc4	on
nemají	mít	k5eNaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nářadí	nářadí	k1gNnSc6	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
je	on	k3xPp3gNnSc4	on
zákazníci	zákazník	k1gMnPc1	zákazník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
neptají	ptat	k5eNaImIp3nP	ptat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rychle	rychle	k6eAd1	rychle
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
hotové	hotový	k2eAgNnSc1d1	hotové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jaké	jaký	k3yQgFnSc6	jaký
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
sice	sice	k8xC	sice
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dělala	dělat	k5eAaImAgFnS	dělat
omítka	omítka	k1gFnSc1	omítka
nebo	nebo	k8xC	nebo
betonová	betonový	k2eAgFnSc1d1	betonová
podlaha	podlaha	k1gFnSc1	podlaha
rovná	rovnat	k5eAaImIp3nS	rovnat
a	a	k8xC	a
obklady	obklad	k1gInPc7	obklad
nebo	nebo	k8xC	nebo
dlažba	dlažba	k1gFnSc1	dlažba
se	se	k3xPyFc4	se
lepila	lepit	k5eAaImAgFnS	lepit
na	na	k7c4	na
cementovou	cementový	k2eAgFnSc4d1	cementová
kaši	kaše	k1gFnSc4	kaše
celou	celý	k2eAgFnSc7d1	celá
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
pracné	pracný	k2eAgNnSc1d1	pracné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
dokonalém	dokonalý	k2eAgNnSc6d1	dokonalé
provedení	provedení	k1gNnSc6	provedení
byla	být	k5eAaImAgFnS	být
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
desítky	desítka	k1gFnSc2	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ale	ale	k8xC	ale
i	i	k9	i
znalí	znalý	k2eAgMnPc1d1	znalý
tamních	tamní	k2eAgMnPc2d1	tamní
poměrů	poměr	k1gInPc2	poměr
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesní	profesní	k2eAgFnSc1d1	profesní
úroveň	úroveň	k1gFnSc1	úroveň
většiny	většina	k1gFnSc2	většina
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
stavebních	stavební	k2eAgMnPc2d1	stavební
dělníků	dělník	k1gMnPc2	dělník
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
neumí	umět	k5eNaImIp3nS	umět
udělat	udělat	k5eAaPmF	udělat
dokonale	dokonale	k6eAd1	dokonale
rovnou	rovný	k2eAgFnSc4d1	rovná
podlahu	podlaha	k1gFnSc4	podlaha
nebo	nebo	k8xC	nebo
zeď	zeď	k1gFnSc4	zeď
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
zavedena	zavést	k5eAaPmNgFnS	zavést
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zednická	zednický	k2eAgFnSc1d1	zednická
lžíce	lžíce	k1gFnSc1	lžíce
Špachtle	špachtle	k1gFnSc2	špachtle
Zednické	zednický	k2eAgNnSc1d1	zednické
kladivo	kladivo	k1gNnSc1	kladivo
Hans	hansa	k1gFnPc2	hansa
Nestle	Nestle	k1gFnSc2	Nestle
<g/>
;	;	kIx,	;
Moderní	moderní	k2eAgNnSc1d1	moderní
stavitelství	stavitelství	k1gNnSc1	stavitelství
pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
i	i	k8xC	i
praxi	praxe	k1gFnSc4	praxe
<g/>
;	;	kIx,	;
EUROPA	EUROPA	kA	EUROPA
–	–	k?	–
Sobotáles	sobotáles	k1gInSc1	sobotáles
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80-86706-11-7	[number]	k4	80-86706-11-7
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hladítko	hladítko	k1gNnSc4	hladítko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hladítko	hladítko	k1gNnSc4	hladítko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
