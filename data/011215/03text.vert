<p>
<s>
Opátka	Opátka	k1gFnSc1	Opátka
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc4	potok
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
částí	část	k1gFnSc7	část
okresu	okres	k1gInSc2	okres
Košice-okolí	Košicekole	k1gFnPc2	Košice-okole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
Belé	Belá	k1gFnSc2	Belá
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
7,6	[number]	k4	7,6
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tokem	tok	k1gInSc7	tok
VI	VI	kA	VI
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pramen	pramen	k1gInSc1	pramen
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
ve	v	k7c6	v
Volovských	volovský	k2eAgInPc6d1	volovský
vrších	vrch	k1gInPc6	vrch
východně	východně	k6eAd1	východně
od	od	k7c2	od
kóty	kóta	k1gFnSc2	kóta
1	[number]	k4	1
065	[number]	k4	065
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
Okrúhlej	Okrúhlej	k1gFnSc2	Okrúhlej
(	(	kIx(	(
<g/>
1	[number]	k4	1
0	[number]	k4	0
<g/>
88,4	[number]	k4	88,4
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
930	[number]	k4	930
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Směr	směr	k1gInSc1	směr
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pramenné	pramenný	k2eAgFnSc6d1	pramenná
oblasti	oblast	k1gFnSc6	oblast
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
po	po	k7c6	po
přibrání	přibrání	k1gNnSc6	přibrání
přítoku	přítok	k1gInSc2	přítok
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Tisícovky	tisícovka	k1gFnSc2	tisícovka
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Opátka	Opátek	k1gMnSc2	Opátek
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stáčí	stáčet	k5eAaImIp3nS	stáčet
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
celky	celek	k1gInPc1	celek
==	==	k?	==
</s>
</p>
<p>
<s>
Volovské	volovský	k2eAgInPc4d1	volovský
vrchy	vrch	k1gInPc4	vrch
</s>
</p>
<p>
<s>
Podsestava	podsestava	k1gFnSc1	podsestava
Kojšovská	Kojšovská	k1gFnSc1	Kojšovská
hoľa	hoľa	k1gFnSc1	hoľa
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Levostranné	levostranný	k2eAgFnPc4d1	levostranná
<g/>
:	:	kIx,	:
krátký	krátký	k2eAgInSc4d1	krátký
přítok	přítok	k1gInSc4	přítok
z	z	k7c2	z
VJV	VJV	kA	VJV
svahu	svah	k1gInSc2	svah
Okrúhlej	Okrúhlej	k1gFnSc2	Okrúhlej
<g/>
,	,	kIx,	,
Zlámaný	zlámaný	k2eAgInSc1d1	zlámaný
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Viničkový	Viničkový	k2eAgInSc1d1	Viničkový
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Zlatník	zlatník	k1gInSc1	zlatník
</s>
</p>
<p>
<s>
Pravostranné	pravostranný	k2eAgFnPc4d1	pravostranná
<g/>
:	:	kIx,	:
přítok	přítok	k1gInSc4	přítok
pramenící	pramenící	k2eAgInSc4d1	pramenící
východně	východně	k6eAd1	východně
od	od	k7c2	od
kóty	kóta	k1gFnSc2	kóta
1	[number]	k4	1
0	[number]	k4	0
<g/>
33,6	[number]	k4	33,6
m	m	kA	m
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc1	přítok
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Tisícovky	tisícovka	k1gFnSc2	tisícovka
(	(	kIx(	(
<g/>
pramen	pramen	k1gInSc1	pramen
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
kóty	kóta	k1gFnSc2	kóta
1	[number]	k4	1
0	[number]	k4	0
<g/>
53,9	[number]	k4	53,9
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc1	přítok
vznikající	vznikající	k2eAgInSc1d1	vznikající
severně	severně	k6eAd1	severně
od	od	k7c2	od
kóty	kóta	k1gFnSc2	kóta
1	[number]	k4	1
0	[number]	k4	0
<g/>
53,9	[number]	k4	53,9
m	m	kA	m
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc1	přítok
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Borovničiar	Borovničiar	k1gInSc1	Borovničiar
<g/>
,	,	kIx,	,
Košarisko	Košarisko	k1gNnSc1	Košarisko
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc1	přítok
pramenící	pramenící	k2eAgInSc1d1	pramenící
SSZ	SSZ	kA	SSZ
od	od	k7c2	od
kóty	kóta	k1gFnSc2	kóta
812,4	[number]	k4	812,4
m	m	kA	m
</s>
</p>
<p>
<s>
==	==	k?	==
Ústí	ústí	k1gNnSc1	ústí
==	==	k?	==
</s>
</p>
<p>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Ružín	Ružína	k1gFnPc2	Ružína
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Košická	košický	k2eAgNnPc1d1	košické
Belá	Belé	k1gNnPc1	Belé
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
325	[number]	k4	325
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Před	před	k7c7	před
vybudováním	vybudování	k1gNnSc7	vybudování
ruzinska	ruzinsko	k1gNnSc2	ruzinsko
přehrady	přehrada	k1gFnSc2	přehrada
ústil	ústit	k5eAaImAgInS	ústit
potok	potok	k1gInSc1	potok
cca	cca	kA	cca
1	[number]	k4	1
km	km	kA	km
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Belé	Belá	k1gFnSc2	Belá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Opátka	Opátka	k1gFnSc1	Opátka
</s>
</p>
<p>
<s>
Rekreační	rekreační	k2eAgFnSc1d1	rekreační
osada	osada	k1gFnSc1	osada
Košické	košický	k2eAgFnSc2d1	Košická
Hámre	Hámr	k1gMnSc5	Hámr
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Opátka	Opátka	k1gFnSc1	Opátka
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
