<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
potok	potok	k1gInSc1	potok
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
částí	část	k1gFnSc7	část
okresu	okres	k1gInSc2	okres
Košice-okolí	Košicekole	k1gFnPc2	Košice-okole
<g/>
?	?	kIx.	?
</s>
