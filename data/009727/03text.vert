<p>
<s>
Antukový	antukový	k2eAgInSc1d1	antukový
dvorec	dvorec	k1gInSc1	dvorec
(	(	kIx(	(
<g/>
antuka	antuka	k1gFnSc1	antuka
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
en	en	k?	en
tout	tout	k1gInSc1	tout
cas	cas	k?	cas
=	=	kIx~	=
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
do	do	k7c2	do
každého	každý	k3xTgNnSc2	každý
počasí	počasí	k1gNnSc2	počasí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
povrchu	povrch	k1gInSc2	povrch
tenisového	tenisový	k2eAgInSc2d1	tenisový
a	a	k8xC	a
volejbalového	volejbalový	k2eAgInSc2d1	volejbalový
dvorce	dvorec	k1gInSc2	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Antukové	antukový	k2eAgFnPc1d1	antuková
kurty	kurta	k1gFnPc1	kurta
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
z	z	k7c2	z
rozdrcené	rozdrcený	k2eAgFnSc2d1	rozdrcená
břidlice	břidlice	k1gFnSc2	břidlice
<g/>
,	,	kIx,	,
štěrku	štěrk	k1gInSc2	štěrk
nebo	nebo	k8xC	nebo
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Antuka	antuka	k1gFnSc1	antuka
nesmí	smět	k5eNaImIp3nS	smět
obsahovat	obsahovat	k5eAaImF	obsahovat
přírodní	přírodní	k2eAgInSc4d1	přírodní
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ostrohranné	ostrohranný	k2eAgFnPc1d1	ostrohranná
částice	částice	k1gFnPc1	částice
včetně	včetně	k7c2	včetně
prachových	prachový	k2eAgFnPc2d1	prachová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
jejím	její	k3xOp3gNnSc7	její
zhutněním	zhutnění	k1gNnSc7	zhutnění
vytvořit	vytvořit	k5eAaPmF	vytvořit
pevný	pevný	k2eAgInSc4d1	pevný
neklouzavý	klouzavý	k2eNgInSc4d1	neklouzavý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Opotřebovaná	opotřebovaný	k2eAgFnSc1d1	opotřebovaná
antuka	antuka	k1gFnSc1	antuka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
omílání	omílání	k1gNnSc3	omílání
hran	hrana	k1gFnPc2	hrana
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
a	a	k8xC	a
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Antuka	antuka	k1gFnSc1	antuka
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
antuky	antuka	k1gFnSc2	antuka
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
antuka	antuka	k1gFnSc1	antuka
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
rozdrcených	rozdrcený	k2eAgFnPc2d1	rozdrcená
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
zejména	zejména	k9	zejména
střešních	střešní	k2eAgFnPc2d1	střešní
tašek	taška	k1gFnPc2	taška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
a	a	k8xC	a
méně	málo	k6eAd2	málo
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
než	než	k8xS	než
zelená	zelený	k2eAgFnSc1d1	zelená
antuka	antuka	k1gFnSc1	antuka
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Har-Tru	Har-Tra	k1gFnSc4	Har-Tra
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
americká	americký	k2eAgFnSc1d1	americká
<g/>
"	"	kIx"	"
antuka	antuka	k1gFnSc1	antuka
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
drceného	drcený	k2eAgInSc2d1	drcený
metamorfovaného	metamorfovaný	k2eAgInSc2d1	metamorfovaný
čediče	čedič	k1gInSc2	čedič
<g/>
.	.	kIx.	.
</s>
<s>
Vlastností	vlastnost	k1gFnSc7	vlastnost
antuky	antuka	k1gFnSc2	antuka
je	být	k5eAaImIp3nS	být
také	také	k9	také
pórovitost	pórovitost	k1gFnSc1	pórovitost
<g/>
,	,	kIx,	,
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
propustnost	propustnost	k1gFnSc1	propustnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejpomalejší	pomalý	k2eAgInSc4d3	nejpomalejší
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tenisu	tenis	k1gInSc6	tenis
preferována	preferován	k2eAgFnSc1d1	preferována
hra	hra	k1gFnSc1	hra
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
čáry	čára	k1gFnSc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
tenisový	tenisový	k2eAgInSc1d1	tenisový
turnaj	turnaj	k1gInSc1	turnaj
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
antukových	antukový	k2eAgInPc6d1	antukový
kurtech	kurt	k1gInPc6	kurt
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
se	se	k3xPyFc4	se
ale	ale	k9	ale
kromě	kromě	k7c2	kromě
tenisu	tenis	k1gInSc2	tenis
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
volejbal	volejbal	k1gInSc1	volejbal
a	a	k8xC	a
nohejbal	nohejbal	k1gInSc1	nohejbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
viděna	vidět	k5eAaImNgFnS	vidět
modrá	modrý	k2eAgFnSc1d1	modrá
antuka	antuka	k1gFnSc1	antuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tenisový	tenisový	k2eAgInSc4d1	tenisový
kurt	kurt	k1gInSc4	kurt
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
antukové	antukový	k2eAgInPc4d1	antukový
kurty	kurt	k1gInPc4	kurt
jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgMnPc1d2	levnější
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
typy	typ	k1gInPc4	typ
tenisových	tenisový	k2eAgInPc2d1	tenisový
kurtů	kurt	k1gInPc2	kurt
<g/>
,	,	kIx,	,
údržba	údržba	k1gFnSc1	údržba
těchto	tento	k3xDgInPc2	tento
kurtů	kurt	k1gInPc2	kurt
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Antukové	antukový	k2eAgFnPc1d1	antuková
kurty	kurta	k1gFnPc1	kurta
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
válcovány	válcovat	k5eAaImNgInP	válcovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
rovnost	rovnost	k1gFnSc1	rovnost
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
antuce	antuka	k1gFnSc6	antuka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyvážen	vyvážen	k2eAgInSc1d1	vyvážen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antukové	antukový	k2eAgFnPc1d1	antuková
kurty	kurta	k1gFnPc1	kurta
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
než	než	k8xS	než
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kurty	kurta	k1gFnPc1	kurta
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
antukové	antukový	k2eAgFnPc1d1	antuková
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
kurtů	kurt	k1gInPc2	kurt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráči	hráč	k1gMnPc1	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
hráčkou	hráčka	k1gFnSc7	hráčka
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
byla	být	k5eAaImAgFnS	být
Justine	Justin	k1gMnSc5	Justin
Henin	Henin	k1gInSc1	Henin
<g/>
,	,	kIx,	,
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
vítězka	vítězka	k1gFnSc1	vítězka
dvouhry	dvouhra	k1gFnSc2	dvouhra
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
je	být	k5eAaImIp3nS	být
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
posledních	poslední	k2eAgInPc2d1	poslední
čtyř	čtyři	k4xCgMnPc2	čtyři
ročníků	ročník	k1gInPc2	ročník
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
zde	zde	k6eAd1	zde
neprohrál	prohrát	k5eNaPmAgInS	prohrát
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
drží	držet	k5eAaImIp3nS	držet
historický	historický	k2eAgInSc1d1	historický
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vítězných	vítězný	k2eAgInPc2d1	vítězný
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
-	-	kIx~	-
81	[number]	k4	81
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
do	do	k7c2	do
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
je	být	k5eAaImIp3nS	být
držitelkou	držitelka	k1gFnSc7	držitelka
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vítězných	vítězný	k2eAgInPc2d1	vítězný
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
srpnem	srpen	k1gInSc7	srpen
1973	[number]	k4	1973
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
1979	[number]	k4	1979
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
125	[number]	k4	125
utkání	utkání	k1gNnPc2	utkání
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesionální	profesionální	k2eAgInPc4d1	profesionální
turnaje	turnaj	k1gInPc4	turnaj
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
antuka	antuka	k1gFnSc1	antuka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
antuka	antuka	k1gFnSc1	antuka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Movistar	Movistar	k1gMnSc1	Movistar
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Viñ	Viñ	k1gMnSc1	Viñ
del	del	k?	del
Mar	Mar	k1gFnSc2	Mar
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Copa	Copa	k6eAd1	Copa
Telmex	Telmex	k1gInSc1	Telmex
(	(	kIx(	(
<g/>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brasil	Brasit	k5eAaPmAgMnS	Brasit
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Costa	Costa	k1gMnSc1	Costa
do	do	k7c2	do
Sauipe	Sauip	k1gInSc5	Sauip
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Abierto	Abierta	k1gFnSc5	Abierta
Mexicano	Mexicana	k1gFnSc5	Mexicana
Telcel	Telcel	k1gFnSc1	Telcel
(	(	kIx(	(
<g/>
Acapulco	Acapulco	k1gNnSc1	Acapulco
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Open	Open	k1gInSc1	Open
de	de	k?	de
Tenis	tenis	k1gInSc1	tenis
Comunidad	Comunidad	k1gInSc1	Comunidad
Valenciana	Valenciana	k1gFnSc1	Valenciana
(	(	kIx(	(
<g/>
Valencia	Valencia	k1gFnSc1	Valencia
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masters	Masters	k6eAd1	Masters
Series	Series	k1gInSc1	Series
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
(	(	kIx(	(
<g/>
Roquebrune-Cap-Martin	Roquebrune-Cap-Martin	k1gInSc1	Roquebrune-Cap-Martin
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Open	Open	k1gMnSc1	Open
Seat	Seat	k1gMnSc1	Seat
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Hassan	Hassan	k1gInSc1	Hassan
II	II	kA	II
(	(	kIx(	(
<g/>
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Men	Men	k1gMnSc1	Men
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Clay	Cla	k1gMnPc7	Cla
Court	Courta	k1gFnPc2	Courta
Championships	Championships	k1gInSc1	Championships
(	(	kIx(	(
<g/>
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estoril	Estoril	k1gMnSc1	Estoril
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Estoril	Estoril	k1gInSc1	Estoril
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BMW	BMW	kA	BMW
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Internazionali	Internazionat	k5eAaImAgMnP	Internazionat
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masters	Masters	k6eAd1	Masters
Series	Series	k1gInSc1	Series
Hamburg	Hamburg	k1gInSc1	Hamburg
(	(	kIx(	(
<g/>
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hypo	Hypo	k6eAd1	Hypo
Group	Group	k1gMnSc1	Group
Tennis	Tennis	k1gFnSc2	Tennis
International	International	k1gMnSc1	International
(	(	kIx(	(
<g/>
Pörtschach	Pörtschach	k1gInSc1	Pörtschach
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Allianz	Allianz	k1gMnSc1	Allianz
Suisse	Suisse	k1gFnSc2	Suisse
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Gstaad	Gstaad	k1gInSc1	Gstaad
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Swedish	Swedish	k1gMnSc1	Swedish
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Bå	Bå	k1gFnSc1	Bå
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mercedes	mercedes	k1gInSc1	mercedes
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Serbia	Serbia	k1gFnSc1	Serbia
Open	Opena	k1gFnPc2	Opena
(	(	kIx(	(
<g/>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Austrian	Austrian	k1gMnSc1	Austrian
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Kitzbühel	Kitzbühel	k1gInSc1	Kitzbühel
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Croatia	Croatia	k1gFnSc1	Croatia
Open	Opena	k1gFnPc2	Opena
(	(	kIx(	(
<g/>
Umag	Umag	k1gInSc1	Umag
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orange	Orange	k6eAd1	Orange
Prokom	Prokom	k1gInSc1	Prokom
Open	Open	k1gInSc1	Open
(	(	kIx(	(
<g/>
Sopoty	sopot	k1gInPc1	sopot
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Open	Open	k1gInSc1	Open
Romania	Romanium	k1gNnSc2	Romanium
(	(	kIx(	(
<g/>
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Campionati	Campionat	k5eAaImF	Campionat
Internazionali	Internazionali	k1gFnSc4	Internazionali
di	di	k?	di
Sicilia	Sicilia	k1gFnSc1	Sicilia
(	(	kIx(	(
<g/>
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
antuka	antuka	k1gFnSc1	antuka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Bausch	Bausch	k1gInSc1	Bausch
&	&	k?	&
Lomb	Lomb	k1gInSc1	Lomb
Championships	Championships	k1gInSc1	Championships
(	(	kIx(	(
<g/>
Amelia	Amelia	k1gFnSc1	Amelia
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Family	Famil	k1gInPc1	Famil
Circle	Circle	k1gFnSc1	Circle
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Charleston	charleston	k1gInSc1	charleston
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
antuka	antuka	k1gFnSc1	antuka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Copa	Copa	k1gMnSc1	Copa
Colsanitas	Colsanitas	k1gMnSc1	Colsanitas
(	(	kIx(	(
<g/>
Bogotá	Bogotá	k1gMnSc1	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Abierto	Abierta	k1gFnSc5	Abierta
Mexicano	Mexicana	k1gFnSc5	Mexicana
Telcel	Telcel	k1gFnSc1	Telcel
(	(	kIx(	(
<g/>
Acapulco	Acapulco	k1gNnSc1	Acapulco
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estoril	Estoril	k1gMnSc1	Estoril
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Estoril	Estoril	k1gInSc1	Estoril
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J	J	kA	J
<g/>
&	&	k?	&
<g/>
S	s	k7c7	s
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brussels	Brussels	k1gInSc1	Brussels
Open	Open	k1gInSc1	Open
(	(	kIx(	(
<g/>
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Qatar	Qatar	k1gInSc1	Qatar
Telecom	Telecom	k1gInSc1	Telecom
German	German	k1gMnSc1	German
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prague	Prague	k1gInSc1	Prague
Open	Open	k1gInSc1	Open
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Internazionali	Internazionat	k5eAaImAgMnP	Internazionat
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
La	la	k1gNnSc2	la
Princesse	Princesse	k1gFnSc2	Princesse
Lalla	Lalla	k1gMnSc1	Lalla
Meryem	Meryem	k1gInSc1	Meryem
(	(	kIx(	(
<g/>
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Internationaux	Internationaux	k1gInSc1	Internationaux
de	de	k?	de
Strasbourg	Strasbourg	k1gInSc1	Strasbourg
(	(	kIx(	(
<g/>
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Internazionali	Internazionat	k5eAaPmAgMnP	Internazionat
di	di	k?	di
Modena	Modena	k1gFnSc1	Modena
(	(	kIx(	(
<g/>
Modena	Modena	k1gFnSc1	Modena
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Internazionali	Internazionat	k5eAaImAgMnP	Internazionat
Femminili	Femminili	k1gFnPc4	Femminili
di	di	k?	di
Palermo	Palermo	k1gNnSc1	Palermo
(	(	kIx(	(
<g/>
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Budapest	Budapest	k1gFnSc1	Budapest
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
(	(	kIx(	(
<g/>
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Clay	Claa	k1gFnSc2	Claa
court	courta	k1gFnPc2	courta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antukový	antukový	k2eAgInSc4d1	antukový
dvorec	dvorec	k1gInSc4	dvorec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
antuka	antuka	k1gFnSc1	antuka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
