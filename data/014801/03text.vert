<s>
Neprůstřelné	průstřelný	k2eNgNnSc1d1
sklo	sklo	k1gNnSc1
</s>
<s>
Tabule	tabule	k1gFnSc1
neprůstřelného	průstřelný	k2eNgNnSc2d1
skla	sklo	k1gNnSc2
po	po	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c6
rozbití	rozbití	k1gNnSc6
lupičem	lupič	k1gMnSc7
</s>
<s>
Neprůstřelné	neprůstřelný	k2eNgNnSc1d1
sklo	sklo	k1gNnSc1
je	být	k5eAaImIp3nS
sklo	sklo	k1gNnSc4
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
odolností	odolnost	k1gFnSc7
proti	proti	k7c3
nárazu	náraz	k1gInSc3
vystřeleného	vystřelený	k2eAgInSc2d1
projektilu	projektil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
pro	pro	k7c4
okna	okno	k1gNnPc4
významných	významný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
nebo	nebo	k8xC
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
chrání	chránit	k5eAaImIp3nP
před	před	k7c7
ozbrojenými	ozbrojený	k2eAgInPc7d1
útoky	útok	k1gInPc7
a	a	k8xC
vzdoruje	vzdorovat	k5eAaImIp3nS
střelám	střela	k1gFnPc3
i	i	k8xC
výbuchům	výbuch	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
běžným	běžný	k2eAgNnSc7d1
místem	místo	k1gNnSc7
použití	použití	k1gNnSc2
jsou	být	k5eAaImIp3nP
kabiny	kabina	k1gFnPc4
bitevních	bitevní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
a	a	k8xC
vrtulníků	vrtulník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
běžně	běžně	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
u	u	k7c2
přepážek	přepážka	k1gFnPc2
finančních	finanční	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
pošt	pošta	k1gFnPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Neprůstřelná	průstřelný	k2eNgNnPc1d1
skla	sklo	k1gNnPc1
bývají	bývat	k5eAaImIp3nP
vyrobena	vyroben	k2eAgNnPc1d1
buď	buď	k8xC
z	z	k7c2
polykarbonátů	polykarbonát	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
vrstveného	vrstvený	k2eAgNnSc2d1
laminovaného	laminovaný	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
sendvičovém	sendvičový	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
dodávají	dodávat	k5eAaImIp3nP
tabule	tabule	k1gFnPc1
skla	sklo	k1gNnSc2
potřebnou	potřebný	k2eAgFnSc4d1
tvrdost	tvrdost	k1gFnSc4
a	a	k8xC
laminovací	laminovací	k2eAgFnSc2d1
fólie	fólie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
tabule	tabule	k1gFnPc1
spojeny	spojit	k5eAaPmNgFnP
<g/>
,	,	kIx,
zase	zase	k9
pružnost	pružnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tabule	tabule	k1gFnSc1
neprůstřelného	průstřelný	k2eNgNnSc2d1
skla	sklo	k1gNnSc2
má	mít	k5eAaImIp3nS
tloušťku	tloušťka	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
2,5	2,5	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jakékoliv	jakýkoliv	k3yIgNnSc1
takové	takový	k3xDgNnSc4
sklo	sklo	k1gNnSc4
lze	lze	k6eAd1
prostřelit	prostřelit	k5eAaPmF
projektily	projektil	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jejich	jejich	k3xOp3gFnSc4
pevnost	pevnost	k1gFnSc4
překonají	překonat	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasklení	zasklení	k1gNnSc6
kabiny	kabina	k1gFnSc2
bitevního	bitevní	k2eAgInSc2d1
vrtulníku	vrtulník	k1gInSc2
AH-64	AH-64	k1gMnSc2
Apache	Apach	k1gMnSc2
je	být	k5eAaImIp3nS
kupříkladu	kupříkladu	k6eAd1
odolné	odolný	k2eAgNnSc1d1
do	do	k7c2
ráže	ráže	k1gFnSc2
12,7	12,7	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
ale	ale	k9
<g/>
,	,	kIx,
že	že	k8xS
nebude	být	k5eNaImBp3nS
více	hodně	k6eAd2
zásahů	zásah	k1gInPc2
do	do	k7c2
stejného	stejný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
<g/>
,	,	kIx,
zejména	zejména	k9
opakovanými	opakovaný	k2eAgInPc7d1
zásahy	zásah	k1gInPc7
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
neprůstřelné	průstřelný	k2eNgNnSc1d1
sklo	sklo	k1gNnSc1
i	i	k9
dosti	dosti	k6eAd1
neprůhledným	průhledný	k2eNgMnPc3d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ODČENÁŠKOVÁ	ODČENÁŠKOVÁ	kA
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenčí	tenčí	k1gNnSc1
a	a	k8xC
lehčí	lehký	k2eAgNnSc1d2
neprůstřelné	průstřelný	k2eNgNnSc1d1
sklo	sklo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
irozhlas	irozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009-09-14	2009-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
bezpečnostní	bezpečnostní	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
neprůstřelné	průstřelný	k2eNgFnSc2d1
sklo	sklo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
