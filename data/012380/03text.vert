<p>
<s>
Lesák	lesák	k1gMnSc1	lesák
rumělkový	rumělkový	k2eAgMnSc1d1	rumělkový
(	(	kIx(	(
<g/>
Cucujus	Cucujus	k1gMnSc1	Cucujus
cinnaberinus	cinnaberinus	k1gMnSc1	cinnaberinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
brouk	brouk	k1gMnSc1	brouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lesákovitých	lesákovitý	k2eAgMnPc2d1	lesákovitý
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířený	rozšířený	k2eAgMnSc1d1	rozšířený
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
mezi	mezi	k7c4	mezi
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgMnPc4d1	ohrožený
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hnijícím	hnijící	k2eAgNnSc7d1	hnijící
lýkem	lýko	k1gNnSc7	lýko
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
jsou	být	k5eAaImIp3nP	být
příležitostně	příležitostně	k6eAd1	příležitostně
dravé	dravý	k2eAgFnPc1d1	dravá
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
brouka	brouk	k1gMnSc2	brouk
trvá	trvat	k5eAaImIp3nS	trvat
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
či	či	k8xC	či
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Loužek	loužek	k1gInSc1	loužek
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lesák	lesák	k1gMnSc1	lesák
rumělkový	rumělkový	k2eAgMnSc1d1	rumělkový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
lesák	lesák	k1gMnSc1	lesák
rumělkový	rumělkový	k2eAgMnSc1d1	rumělkový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
