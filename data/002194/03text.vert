<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Ostrawa	Ostraw	k2eAgFnSc1d1	Ostraw
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Ostrau	Ostraus	k1gInSc3	Ostraus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
a	a	k8xC	a
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
město	město	k1gNnSc1	město
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Odra	Odra	k1gFnSc1	Odra
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
Ostravice	Ostravice	k1gFnSc1	Ostravice
a	a	k8xC	a
Lučina	lučina	k1gFnSc1	lučina
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
rozlohou	rozloha	k1gFnSc7	rozloha
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Ostravy	Ostrava	k1gFnSc2	Ostrava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čtyři	čtyři	k4xCgFnPc4	čtyři
městské	městský	k2eAgFnPc4d1	městská
památkové	památkový	k2eAgFnPc4d1	památková
zóny	zóna	k1gFnPc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
už	už	k6eAd1	už
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
aglomerací	aglomerace	k1gFnSc7	aglomerace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
jako	jako	k9	jako
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
středisko	středisko	k1gNnSc4	středisko
černouhelné	černouhelný	k2eAgFnSc2d1	černouhelná
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
hornický	hornický	k2eAgInSc4d1	hornický
a	a	k8xC	a
hutnický	hutnický	k2eAgInSc4d1	hutnický
průmysl	průmysl	k1gInSc4	průmysl
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
ocelové	ocelový	k2eAgNnSc1d1	ocelové
srdce	srdce	k1gNnSc1	srdce
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
výraznými	výrazný	k2eAgFnPc7d1	výrazná
změnami	změna	k1gFnPc7	změna
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
restrukturalizace	restrukturalizace	k1gFnSc2	restrukturalizace
průmyslu	průmysl	k1gInSc2	průmysl
byla	být	k5eAaImAgFnS	být
utlumena	utlumen	k2eAgFnSc1d1	utlumena
důlní	důlní	k2eAgFnSc1d1	důlní
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
poslední	poslední	k2eAgNnSc1d1	poslední
uhlí	uhlí	k1gNnSc1	uhlí
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vytěžilo	vytěžit	k5eAaPmAgNnS	vytěžit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Živoucím	živoucí	k2eAgInSc7d1	živoucí
důkazem	důkaz	k1gInSc7	důkaz
hornické	hornický	k2eAgFnSc2d1	hornická
minulosti	minulost	k1gFnSc2	minulost
je	být	k5eAaImIp3nS	být
Dolní	dolní	k2eAgFnSc1d1	dolní
oblast	oblast	k1gFnSc1	oblast
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgInSc4d1	bývalý
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
areál	areál	k1gInSc4	areál
s	s	k7c7	s
unikátním	unikátní	k2eAgInSc7d1	unikátní
souborem	soubor	k1gInSc7	soubor
industriální	industriální	k2eAgFnSc2d1	industriální
architektury	architektura	k1gFnSc2	architektura
aspirující	aspirující	k2eAgFnSc2d1	aspirující
na	na	k7c4	na
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
se	se	k3xPyFc4	se
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
transformuje	transformovat	k5eAaBmIp3nS	transformovat
v	v	k7c4	v
moderní	moderní	k2eAgNnSc4d1	moderní
kulturní	kulturní	k2eAgNnSc4d1	kulturní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pořádají	pořádat	k5eAaImIp3nP	pořádat
různorodé	různorodý	k2eAgFnPc4d1	různorodá
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
festivaly	festival	k1gInPc1	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
Janáčkův	Janáčkův	k2eAgInSc1d1	Janáčkův
máj	máj	k1gInSc1	máj
a	a	k8xC	a
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Shakespearovské	shakespearovský	k2eAgFnPc1d1	shakespearovská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
atletický	atletický	k2eAgInSc1d1	atletický
závod	závod	k1gInSc1	závod
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
tretra	tretra	k1gFnSc1	tretra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Dny	den	k1gInPc1	den
NATO	NATO	kA	NATO
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
-	-	kIx~	-
Mošnově	Mošnov	k1gInSc6	Mošnov
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
školství	školství	k1gNnSc1	školství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
Vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
báňskou	báňský	k2eAgFnSc7d1	báňská
-	-	kIx~	-
Technickou	technický	k2eAgFnSc7d1	technická
univerzitou	univerzita	k1gFnSc7	univerzita
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
všeobecněji	všeobecně	k6eAd2	všeobecně
zaměřenou	zaměřený	k2eAgFnSc7d1	zaměřená
Ostravskou	ostravský	k2eAgFnSc7d1	Ostravská
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
Ostrava	Ostrava	k1gFnSc1	Ostrava
evropským	evropský	k2eAgNnSc7d1	Evropské
městem	město	k1gNnSc7	město
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pořádala	pořádat	k5eAaImAgFnS	pořádat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
IIHF	IIHF	kA	IIHF
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
blasonován	blasonován	k2eAgInSc1d1	blasonován
<g/>
:	:	kIx,	:
V	v	k7c6	v
modrém	modrý	k2eAgInSc6d1	modrý
štítě	štít	k1gInSc6	štít
na	na	k7c6	na
zeleném	zelený	k2eAgInSc6d1	zelený
trávníku	trávník	k1gInSc6	trávník
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
poskoku	poskok	k1gInSc6	poskok
se	s	k7c7	s
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
sedlem	sedlo	k1gNnSc7	sedlo
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
pokrývkou	pokrývka	k1gFnSc7	pokrývka
<g/>
,	,	kIx,	,
provázený	provázený	k2eAgInSc1d1	provázený
vlevo	vlevo	k6eAd1	vlevo
nahoře	nahoře	k6eAd1	nahoře
zlatou	zlatý	k2eAgFnSc7d1	zlatá
růží	růž	k1gFnSc7	růž
se	s	k7c7	s
zelenými	zelený	k2eAgInPc7d1	zelený
kališními	kališní	k2eAgInPc7d1	kališní
lístky	lístek	k1gInPc7	lístek
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
semeníkem	semeník	k1gInSc7	semeník
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
nemá	mít	k5eNaImIp3nS	mít
uzdu	uzda	k1gFnSc4	uzda
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pečeti	pečeť	k1gFnSc6	pečeť
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1426	[number]	k4	1426
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
bývá	bývat	k5eAaImIp3nS	bývat
vykládán	vykládán	k2eAgMnSc1d1	vykládán
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
polohy	poloha	k1gFnSc2	poloha
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
figura	figura	k1gFnSc1	figura
z	z	k7c2	z
erbu	erb	k1gInSc2	erb
prvního	první	k4xOgMnSc2	první
fojta	fojt	k1gMnSc2	fojt
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
růže	růže	k1gFnSc1	růže
zřejmě	zřejmě	k6eAd1	zřejmě
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rodového	rodový	k2eAgInSc2d1	rodový
erbu	erb	k1gInSc2	erb
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Stanislava	Stanislav	k1gMnSc2	Stanislav
Thurza	Thurz	k1gMnSc2	Thurz
(	(	kIx(	(
<g/>
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
verzi	verze	k1gFnSc3	verze
se	se	k3xPyFc4	se
současná	současný	k2eAgFnSc1d1	současná
historická	historický	k2eAgFnSc1d1	historická
literatura	literatura	k1gFnSc1	literatura
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
nejvíce	hodně	k6eAd3	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
udělil	udělit	k5eAaPmAgMnS	udělit
biskup	biskup	k1gMnSc1	biskup
Ostravským	ostravský	k2eAgMnSc7d1	ostravský
koně	kůň	k1gMnSc4	kůň
do	do	k7c2	do
znaku	znak	k1gInSc2	znak
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
Hukvaldským	hukvaldský	k2eAgFnPc3d1	Hukvaldská
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc1	jejich
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřátelé	nepřítel	k1gMnPc1	nepřítel
nestihli	stihnout	k5eNaPmAgMnP	stihnout
svým	svůj	k3xOyFgMnPc3	svůj
koním	kůň	k1gMnPc3	kůň
dát	dát	k5eAaPmF	dát
ani	ani	k8xC	ani
uzdu	uzda	k1gFnSc4	uzda
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
pověstí	pověst	k1gFnSc7	pověst
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
Ostravští	ostravský	k2eAgMnPc1d1	ostravský
vypustili	vypustit	k5eAaPmAgMnP	vypustit
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
města	město	k1gNnSc2	město
koně	kůň	k1gMnSc2	kůň
bez	bez	k7c2	bez
uzdy	uzda	k1gFnSc2	uzda
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
zmátli	zmást	k5eAaPmAgMnP	zmást
obléhající	obléhající	k2eAgMnPc1d1	obléhající
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
představeno	představen	k2eAgNnSc1d1	představeno
nové	nový	k2eAgNnSc1d1	nové
logo	logo	k1gNnSc1	logo
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Studio	studio	k1gNnSc1	studio
Najbrt	Najbrta	k1gFnPc2	Najbrta
<g/>
.	.	kIx.	.
</s>
<s>
OSTRAVA	Ostrava	k1gFnSc1	Ostrava
<g/>
!!!	!!!	k?	!!!
je	být	k5eAaImIp3nS	být
marketingovou	marketingový	k2eAgFnSc7d1	marketingová
značkou	značka	k1gFnSc7	značka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Podobu	podoba	k1gFnSc4	podoba
loga	logo	k1gNnSc2	logo
tvoří	tvořit	k5eAaImIp3nP	tvořit
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vykřičníky	vykřičník	k1gInPc7	vykřičník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Slezské	slezský	k2eAgFnSc6d1	Slezská
Ostravě	Ostrava	k1gFnSc6	Ostrava
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1229	[number]	k4	1229
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
jako	jako	k9	jako
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1267	[number]	k4	1267
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
jako	jako	k8xS	jako
o	o	k7c6	o
městečku	městečko	k1gNnSc6	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
malá	malý	k2eAgFnSc1d1	malá
osada	osada	k1gFnSc1	osada
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Ostravice	Ostravice	k1gFnSc1	Ostravice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jí	on	k3xPp3gFnSc3	on
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
ji	on	k3xPp3gFnSc4	on
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
Moravskou	moravský	k2eAgFnSc4d1	Moravská
Ostravu	Ostrava	k1gFnSc4	Ostrava
a	a	k8xC	a
Slezskou	slezský	k2eAgFnSc4d1	Slezská
Ostravu	Ostrava	k1gFnSc4	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
zemské	zemský	k2eAgFnSc6d1	zemská
hranici	hranice	k1gFnSc6	hranice
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
procházela	procházet	k5eAaImAgFnS	procházet
jantarová	jantarový	k2eAgFnSc1d1	jantarová
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1621-1645	[number]	k4	1621-1645
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
,	,	kIx,	,
však	však	k9	však
význam	význam	k1gInSc4	význam
Ostravy	Ostrava	k1gFnSc2	Ostrava
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
slezské	slezský	k2eAgFnSc6d1	Slezská
části	část	k1gFnSc6	část
Ostravy	Ostrava	k1gFnSc2	Ostrava
objeveno	objevit	k5eAaPmNgNnS	objevit
bohaté	bohatý	k2eAgNnSc1d1	bohaté
ložisko	ložisko	k1gNnSc1	ložisko
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
proměnu	proměna	k1gFnSc4	proměna
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
založil	založit	k5eAaPmAgMnS	založit
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
hutě	huť	k1gFnPc1	huť
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
Rudolfovy	Rudolfův	k2eAgInPc1d1	Rudolfův
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tyto	tento	k3xDgFnPc1	tento
hutě	huť	k1gFnPc1	huť
přešly	přejít	k5eAaPmAgFnP	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
rodiny	rodina	k1gFnSc2	rodina
Rothschildů	Rothschild	k1gMnPc2	Rothschild
a	a	k8xC	a
získaly	získat	k5eAaPmAgInP	získat
název	název	k1gInSc4	název
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
jádrem	jádro	k1gNnSc7	jádro
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
rozmachu	rozmach	k1gInSc2	rozmach
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
odrazem	odraz	k1gInSc7	odraz
byla	být	k5eAaImAgFnS	být
i	i	k9	i
(	(	kIx(	(
<g/>
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
přezdívka	přezdívka	k1gFnSc1	přezdívka
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
ocelové	ocelový	k2eAgNnSc1d1	ocelové
srdce	srdce	k1gNnSc1	srdce
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Ostrava	Ostrava	k1gFnSc1	Ostrava
osvobozena	osvobodit	k5eAaPmNgFnS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
stavebnímu	stavební	k2eAgInSc3d1	stavební
rozmachu	rozmach	k1gInSc3	rozmach
v	v	k7c6	v
obytné	obytný	k2eAgFnSc6d1	obytná
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
menší	malý	k2eAgFnSc1d2	menší
zástavba	zástavba	k1gFnSc1	zástavba
jen	jen	k9	jen
na	na	k7c6	na
území	území	k1gNnSc6	území
Poruby	Poruba	k1gFnSc2	Poruba
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgInPc4d1	chráněný
domy	dům	k1gInPc4	dům
ve	v	k7c6	v
slohu	sloh	k1gInSc6	sloh
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
masívní	masívní	k2eAgFnSc1d1	masívní
urbanizace	urbanizace	k1gFnSc1	urbanizace
méně	málo	k6eAd2	málo
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Porubě	Poruba	k1gFnSc6	Poruba
i	i	k8xC	i
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vylidnění	vylidnění	k1gNnSc2	vylidnění
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
likvidace	likvidace	k1gFnSc2	likvidace
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
útlumu	útlum	k1gInSc6	útlum
hutního	hutní	k2eAgInSc2d1	hutní
a	a	k8xC	a
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
zavíráním	zavírání	k1gNnSc7	zavírání
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1994	[number]	k4	1994
netěží	těžet	k5eNaImIp3nS	těžet
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
investicemi	investice	k1gFnPc7	investice
do	do	k7c2	do
nápravy	náprava	k1gFnSc2	náprava
škod	škoda	k1gFnPc2	škoda
na	na	k7c6	na
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
Ostrava	Ostrava	k1gFnSc1	Ostrava
výrazně	výrazně	k6eAd1	výrazně
pročistila	pročistit	k5eAaPmAgFnS	pročistit
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c6	na
důrazu	důraz	k1gInSc6	důraz
nabírá	nabírat	k5eAaImIp3nS	nabírat
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
aktivita	aktivita	k1gFnSc1	aktivita
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
regiony	region	k1gInPc4	region
Jeseníky	Jeseník	k1gInPc4	Jeseník
a	a	k8xC	a
Beskydy	Beskydy	k1gFnPc4	Beskydy
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
stovky	stovka	k1gFnSc2	stovka
hektarů	hektar	k1gInPc2	hektar
rekultivovaných	rekultivovaný	k2eAgFnPc2d1	rekultivovaná
ploch	plocha	k1gFnPc2	plocha
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
původních	původní	k2eAgFnPc2d1	původní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
jako	jako	k8xS	jako
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
Polanský	Polanský	k1gMnSc1	Polanský
les	les	k1gInSc1	les
a	a	k8xC	a
Polanská	Polanská	k1gFnSc1	Polanská
niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Poodří	Poodří	k1gNnSc2	Poodří
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přírodním	přírodní	k2eAgFnPc3d1	přírodní
raritám	rarita	k1gFnPc3	rarita
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
bludné	bludný	k2eAgInPc4d1	bludný
balvany	balvan	k1gInPc4	balvan
ze	z	k7c2	z
švédské	švédský	k2eAgFnSc2d1	švédská
žuly	žula	k1gFnSc2	žula
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
unikátem	unikát	k1gInSc7	unikát
je	být	k5eAaImIp3nS	být
i	i	k9	i
halda	halda	k1gFnSc1	halda
Ema	Ema	k1gFnSc1	Ema
ve	v	k7c6	v
Slezské	slezský	k2eAgFnSc6d1	Slezská
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
na	na	k7c6	na
haldě	halda	k1gFnSc6	halda
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Černé	Černé	k2eAgFnSc6d1	Černé
louce	louka	k1gFnSc6	louka
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgFnSc1	první
horská	horský	k2eAgFnSc1d1	horská
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
dráhu	dráha	k1gFnSc4	dráha
jménem	jméno	k1gNnSc7	jméno
Tivoli	Tivole	k1gFnSc6	Tivole
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
vzniku	vznik	k1gInSc2	vznik
jasné	jasný	k2eAgNnSc1d1	jasné
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
tam	tam	k6eAd1	tam
stála	stát	k5eAaImAgNnP	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
společně	společně	k6eAd1	společně
s	s	k7c7	s
haldou	halda	k1gFnSc7	halda
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1229	[number]	k4	1229
<g/>
:	:	kIx,	:
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
Slezská	slezský	k2eAgFnSc1d1	Slezská
<g/>
)	)	kIx)	)
Ostrawa	Ostrawa	k1gFnSc1	Ostrawa
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
papeže	papež	k1gMnSc4	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
1267	[number]	k4	1267
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
osada	osada	k1gFnSc1	osada
(	(	kIx(	(
<g/>
Moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
)	)	kIx)	)
Ostrava	Ostrava	k1gFnSc1	Ostrava
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Bruna	Bruno	k1gMnSc2	Bruno
(	(	kIx(	(
<g/>
1205	[number]	k4	1205
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1281	[number]	k4	1281
<g/>
)	)	kIx)	)
před	před	k7c7	před
1279	[number]	k4	1279
<g/>
:	:	kIx,	:
povýšení	povýšení	k1gNnSc6	povýšení
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
písemné	písemný	k2eAgInPc4d1	písemný
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
1297	[number]	k4	1297
<g/>
:	:	kIx,	:
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
písemně	písemně	k6eAd1	písemně
připomíná	připomínat	k5eAaImIp3nS	připomínat
slezskoostravský	slezskoostravský	k2eAgInSc1d1	slezskoostravský
knížecí	knížecí	k2eAgInSc1d1	knížecí
hrad	hrad	k1gInSc1	hrad
1763	[number]	k4	1763
<g/>
:	:	kIx,	:
objev	objev	k1gInSc1	objev
uhlí	uhlí	k1gNnSc2	uhlí
(	(	kIx(	(
<g/>
údolí	údolí	k1gNnSc2	údolí
Bufet	bufet	k1gNnSc2	bufet
<g/>
)	)	kIx)	)
1782	[number]	k4	1782
<g/>
:	:	kIx,	:
zahájena	zahájen	k2eAgFnSc1d1	zahájena
těžba	těžba	k1gFnSc1	těžba
<g />
.	.	kIx.	.
</s>
<s>
uhlí	uhlí	k1gNnSc1	uhlí
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
1828	[number]	k4	1828
<g/>
:	:	kIx,	:
založení	založení	k1gNnSc1	založení
železáren	železárna	k1gFnPc2	železárna
(	(	kIx(	(
<g/>
Rudolfova	Rudolfův	k2eAgFnSc1d1	Rudolfova
huť	huť	k1gFnSc1	huť
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnPc1d2	pozdější
Vítkovické	vítkovický	k2eAgFnPc1d1	Vítkovická
železárny	železárna	k1gFnPc1	železárna
<g/>
)	)	kIx)	)
1835	[number]	k4	1835
<g/>
:	:	kIx,	:
majitelem	majitel	k1gMnSc7	majitel
Rudolfovy	Rudolfův	k2eAgFnSc2d1	Rudolfova
hutě	huť	k1gFnSc2	huť
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
bankéř	bankéř	k1gMnSc1	bankéř
S.	S.	kA	S.
M.	M.	kA	M.
Rothschild	Rothschild	k1gMnSc1	Rothschild
1840	[number]	k4	1840
<g/>
:	:	kIx,	:
postavena	postavit	k5eAaPmNgFnS	postavit
první	první	k4xOgFnSc1	první
koksovací	koksovací	k2eAgFnSc1d1	koksovací
pec	pec	k1gFnSc1	pec
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
1889	[number]	k4	1889
<g/>
:	:	kIx,	:
dokončena	dokončen	k2eAgFnSc1d1	dokončena
stavba	stavba	k1gFnSc1	stavba
kostela	kostel	k1gInSc2	kostel
Božského	božský	k2eAgMnSc4d1	božský
Spasitele	spasitel	k1gMnSc4	spasitel
1894	[number]	k4	1894
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
otevřen	otevřít	k5eAaPmNgInS	otevřít
Český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
1898	[number]	k4	1898
<g/>
:	:	kIx,	:
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
banka	banka	k1gFnSc1	banka
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
čítárna	čítárna	k1gFnSc1	čítárna
1919	[number]	k4	1919
<g/>
:	:	kIx,	:
založeno	založen	k2eAgNnSc1d1	založeno
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
moravskoslezské	moravskoslezský	k2eAgFnSc2d1	Moravskoslezská
1922	[number]	k4	1922
<g/>
:	:	kIx,	:
založen	založit	k5eAaPmNgInS	založit
SK	Sk	kA	Sk
Slezská	slezský	k2eAgFnSc1d1	Slezská
Ostrava	Ostrava	k1gFnSc1	Ostrava
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
vzniká	vznikat	k5eAaImIp3nS	vznikat
Velká	velký	k2eAgFnSc1d1	velká
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
městskému	městský	k2eAgNnSc3d1	Městské
jádru	jádro	k1gNnSc3	jádro
připojeno	připojit	k5eAaPmNgNnS	připojit
7	[number]	k4	7
sousedních	sousední	k2eAgFnPc2d1	sousední
obcí	obec	k1gFnPc2	obec
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
<g/>
:	:	kIx,	:
otevřen	otevřen	k2eAgInSc1d1	otevřen
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc1	umění
1928	[number]	k4	1928
<g/>
:	:	kIx,	:
založen	založit	k5eAaPmNgInS	založit
SSK	SSK	kA	SSK
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
1930	[number]	k4	1930
<g/>
:	:	kIx,	:
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pokračování	pokračování	k1gNnSc1	pokračování
integračního	integrační	k2eAgInSc2d1	integrační
procesu	proces	k1gInSc2	proces
města	město	k1gNnSc2	město
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
připojení	připojení	k1gNnSc4	připojení
8	[number]	k4	8
slezských	slezský	k2eAgInPc2d1	slezský
a	a	k8xC	a
4	[number]	k4	4
moravských	moravský	k2eAgFnPc2d1	Moravská
obcí	obec	k1gFnPc2	obec
k	k	k7c3	k
Moravské	moravský	k2eAgFnSc3d1	Moravská
Ostravě	Ostrava	k1gFnSc3	Ostrava
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
30	[number]	k4	30
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
den	den	k1gInSc1	den
osvobození	osvobození	k1gNnSc6	osvobození
Ostravy	Ostrava	k1gFnSc2	Ostrava
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
Příbrami	Příbram	k1gFnSc2	Příbram
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
přesídlena	přesídlen	k2eAgFnSc1d1	přesídlen
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
báňská	báňský	k2eAgFnSc1d1	báňská
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
výstavby	výstavba	k1gFnSc2	výstavba
Nové	Nové	k2eAgFnSc2d1	Nové
huti	huť	k1gFnSc2	huť
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
se	se	k3xPyFc4	se
výnosem	výnos	k1gInSc7	výnos
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
číslo	číslo	k1gNnSc1	číslo
1522	[number]	k4	1522
<g/>
/	/	kIx~	/
<g/>
1946	[number]	k4	1946
Ú.	Ú.	kA	Ú.
l.	l.	k?	l.
I	i	k9	i
přejmenovává	přejmenovávat	k5eAaImIp3nS	přejmenovávat
na	na	k7c4	na
Ostravu	Ostrava	k1gFnSc4	Ostrava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
vyhláškou	vyhláška	k1gFnSc7	vyhláška
číslo	číslo	k1gNnSc1	číslo
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
1947	[number]	k4	1947
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
1951	[number]	k4	1951
<g/>
:	:	kIx,	:
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Státní	státní	k2eAgFnSc1d1	státní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
zahájena	zahájen	k2eAgFnSc1d1	zahájena
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
1953	[number]	k4	1953
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
založena	založen	k2eAgFnSc1d1	založena
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
hudebně	hudebně	k6eAd1	hudebně
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
název	název	k1gInSc1	název
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc1	vznik
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
filharmonie	filharmonie	k1gFnSc2	filharmonie
Ostrava	Ostrava	k1gFnSc1	Ostrava
z	z	k7c2	z
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
orchestru	orchestr	k1gInSc2	orchestr
1955	[number]	k4	1955
<g/>
:	:	kIx,	:
ostravské	ostravský	k2eAgNnSc1d1	ostravské
studio	studio	k1gNnSc1	studio
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
jako	jako	k8xS	jako
druhé	druhý	k4xOgNnSc1	druhý
televizní	televizní	k2eAgNnSc1d1	televizní
studio	studio	k1gNnSc1	studio
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
i	i	k9	i
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc1	vznik
Pedagogického	pedagogický	k2eAgInSc2d1	pedagogický
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
stává	stávat	k5eAaImIp3nS	stávat
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
;	;	kIx,	;
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
letiště	letiště	k1gNnSc4	letiště
Ostrava-Mošnov	Ostrava-Mošnov	k1gInSc1	Ostrava-Mošnov
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
otevřen	otevřít	k5eAaPmNgInS	otevřít
Dům	dům	k1gInSc1	dům
kultury	kultura	k1gFnSc2	kultura
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
otevřen	otevřít	k5eAaPmNgInS	otevřít
Palác	palác	k1gInSc1	palác
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
sportu	sport	k1gInSc3	sport
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Ostravar	Ostravar	k1gInSc1	Ostravar
Aréna	aréna	k1gFnSc1	aréna
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
89	[number]	k4	89
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
měst	město	k1gNnPc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
devadeátých	devadeátý	k2eAgNnPc6d1	devadeátý
letech	léto	k1gNnPc6	léto
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
Čechy	Čechy	k1gFnPc4	Čechy
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Vznik	vznik	k1gInSc1	vznik
týmu	tým	k1gInSc2	tým
Amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g />
.	.	kIx.	.
</s>
<s>
Steelers	Steelers	k1gInSc1	Steelers
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
poslední	poslední	k2eAgNnSc4d1	poslední
vytěžené	vytěžený	k2eAgNnSc4d1	vytěžené
uhlí	uhlí	k1gNnSc4	uhlí
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
zahájena	zahájen	k2eAgFnSc1d1	zahájena
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
obchodně-podnikatelského	obchodněodnikatelský	k2eAgInSc2d1	obchodně-podnikatelský
areálu	areál	k1gInSc2	areál
Ostrava-Mošnov	Ostrava-Mošnov	k1gInSc1	Ostrava-Mošnov
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc1	vznik
ostravsko-opavské	ostravskopavský	k2eAgFnSc2d1	ostravsko-opavská
diecéze	diecéze	k1gFnSc2	diecéze
bulou	bula	k1gFnSc7	bula
Ad	ad	k7c4	ad
Christifidelium	Christifidelium	k1gNnSc4	Christifidelium
spirituali	spirituat	k5eAaPmAgMnP	spirituat
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
stopadesátiletá	stopadesátiletý	k2eAgFnSc1d1	stopadesátiletá
povodeň	povodeň	k1gFnSc1	povodeň
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
<g/>
,	,	kIx,	,
Opavě	Opava	k1gFnSc6	Opava
a	a	k8xC	a
Ostravici	Ostravice	k1gFnSc6	Ostravice
<g/>
,	,	kIx,	,
zaplavena	zaplaven	k2eAgFnSc1d1	zaplavena
většina	většina	k1gFnSc1	většina
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgFnPc2d1	položená
částí	část	k1gFnPc2	část
<g />
.	.	kIx.	.
</s>
<s>
Ostravy	Ostrava	k1gFnPc1	Ostrava
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Ostrava	Ostrava	k1gFnSc1	Ostrava
s	s	k7c7	s
partnery	partner	k1gMnPc7	partner
založilo	založit	k5eAaPmAgNnS	založit
Vědecko-Technologický	vědeckoechnologický	k2eAgInSc1d1	vědecko-technologický
park	park	k1gInSc1	park
Ostrava	Ostrava	k1gFnSc1	Ostrava
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
ukončení	ukončení	k1gNnSc6	ukončení
provozu	provoz	k1gInSc2	provoz
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pecí	pec	k1gFnPc2	pec
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc4	vznik
Ostravského	ostravský	k2eAgNnSc2d1	ostravské
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
<g/>
)	)	kIx)	)
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
město	město	k1gNnSc1	město
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
<g />
.	.	kIx.	.
</s>
<s>
CTP	CTP	kA	CTP
Invest	Invest	k1gFnSc1	Invest
začíná	začínat	k5eAaImIp3nS	začínat
budovat	budovat	k5eAaImF	budovat
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
zónu	zóna	k1gFnSc4	zóna
Ostrava-Hrabová	Ostrava-Hrabová	k1gFnSc1	Ostrava-Hrabová
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
okres	okres	k1gInSc1	okres
Ostrava-město	Ostravaěsta	k1gMnSc5	Ostrava-města
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
o	o	k7c4	o
některé	některý	k3yIgFnPc4	některý
okolní	okolní	k2eAgFnPc4d1	okolní
obce	obec	k1gFnPc4	obec
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Ostrava	Ostrava	k1gFnSc1	Ostrava
napojena	napojen	k2eAgFnSc1d1	napojena
na	na	k7c4	na
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
dokončení	dokončení	k1gNnSc6	dokončení
I.	I.	kA	I.
fáze	fáze	k1gFnSc1	fáze
revitalizace	revitalizace	k1gFnSc2	revitalizace
Dolní	dolní	k2eAgFnSc2d1	dolní
oblasti	oblast	k1gFnSc2	oblast
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
a	a	k8xC	a
nového	nový	k2eAgNnSc2d1	nové
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
otevření	otevření	k1gNnSc6	otevření
památkového	památkový	k2eAgInSc2d1	památkový
objektu	objekt	k1gInSc2	objekt
Trojhalí	Trojhalý	k2eAgMnPc1d1	Trojhalý
po	po	k7c4	po
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
,	,	kIx,	,
kolejové	kolejový	k2eAgNnSc4d1	kolejové
napojení	napojení	k1gNnSc4	napojení
letiště	letiště	k1gNnSc2	letiště
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
zde	zde	k6eAd1	zde
tvoří	tvořit	k5eAaImIp3nP	tvořit
řeky	řeka	k1gFnPc1	řeka
Odra	Odra	k1gFnSc1	Odra
a	a	k8xC	a
Ostravice	Ostravice	k1gFnSc1	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
řekami	řeka	k1gFnPc7	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
území	území	k1gNnSc4	území
spadající	spadající	k2eAgNnSc4d1	spadající
pod	pod	k7c4	pod
České	český	k2eAgNnSc4d1	české
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
většina	většina	k1gFnSc1	většina
města	město	k1gNnPc4	město
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
důležitých	důležitý	k2eAgFnPc2d1	důležitá
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
včetně	včetně	k7c2	včetně
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
velkých	velký	k2eAgNnPc2d1	velké
sídlišť	sídliště	k1gNnPc2	sídliště
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Poruby	Poruba	k1gFnSc2	Poruba
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
národnostně	národnostně	k6eAd1	národnostně
nevyhraněné	vyhraněný	k2eNgFnPc1d1	nevyhraněná
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
byla	být	k5eAaImAgFnS	být
německy	německy	k6eAd1	německy
hovořící	hovořící	k2eAgFnSc1d1	hovořící
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vlivem	vliv	k1gInSc7	vliv
příchodu	příchod	k1gInSc2	příchod
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
intelektuálů	intelektuál	k1gMnPc2	intelektuál
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
počešťovat	počešťovat	k5eAaImF	počešťovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odsunu	odsun	k1gInSc3	odsun
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
moravské	moravský	k2eAgNnSc1d1	Moravské
národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
neprosadilo	prosadit	k5eNaPmAgNnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
početná	početný	k2eAgFnSc1d1	početná
romská	romský	k2eAgFnSc1d1	romská
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
a	a	k8xC	a
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
druhé	druhý	k4xOgFnSc2	druhý
největší	veliký	k2eAgFnSc2d3	veliký
české	český	k2eAgFnSc2d1	Česká
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
kromě	kromě	k7c2	kromě
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
obcí	obec	k1gFnPc2	obec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
většinu	většina	k1gFnSc4	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
především	především	k9	především
Opavu	Opava	k1gFnSc4	Opava
<g/>
,	,	kIx,	,
Karvinou	Karviná	k1gFnSc4	Karviná
<g/>
,	,	kIx,	,
Havířov	Havířov	k1gInSc4	Havířov
<g/>
,	,	kIx,	,
Frýdek-Místek	Frýdek-Místek	k1gInSc4	Frýdek-Místek
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc4	Třinec
<g/>
,	,	kIx,	,
Kopřivnici	Kopřivnice	k1gFnSc4	Kopřivnice
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
<g/>
,	,	kIx,	,
Orlovou	Orlová	k1gFnSc4	Orlová
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc4	Bohumín
nebo	nebo	k8xC	nebo
Český	český	k2eAgInSc4d1	český
Těšín	Těšín	k1gInSc4	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tvoří	tvořit	k5eAaImIp3nS	tvořit
79	[number]	k4	79
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Lučiny	lučina	k1gFnSc2	lučina
<g/>
,	,	kIx,	,
Odry	Odra	k1gFnSc2	Odra
<g/>
,	,	kIx,	,
Opavy	Opava	k1gFnSc2	Opava
a	a	k8xC	a
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
umístění	umístění	k1gNnSc3	umístění
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
bráně	brána	k1gFnSc6	brána
spadá	spadat	k5eAaImIp3nS	spadat
Ostrava	Ostrava	k1gFnSc1	Ostrava
do	do	k7c2	do
teplé	teplý	k2eAgFnSc2d1	teplá
klimatické	klimatický	k2eAgFnSc2d1	klimatická
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
210	[number]	k4	210
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svědčí	svědčit	k5eAaImIp3nS	svědčit
řadě	řada	k1gFnSc3	řada
druhů	druh	k1gInPc2	druh
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jiným	jiný	k2eAgInPc3d1	jiný
regionům	region	k1gInPc3	region
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
určitými	určitý	k2eAgFnPc7d1	určitá
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
<g/>
,	,	kIx,	,
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
hustou	hustý	k2eAgFnSc7d1	hustá
zástavbou	zástavba	k1gFnSc7	zástavba
a	a	k8xC	a
specifickými	specifický	k2eAgFnPc7d1	specifická
podmínkami	podmínka	k1gFnPc7	podmínka
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zde	zde	k6eAd1	zde
panuje	panovat	k5eAaImIp3nS	panovat
klima	klima	k1gNnSc4	klima
s	s	k7c7	s
horkými	horký	k2eAgInPc7d1	horký
<g/>
,	,	kIx,	,
vlhkými	vlhký	k2eAgNnPc7d1	vlhké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
mírnými	mírný	k2eAgFnPc7d1	mírná
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
10,2	[number]	k4	10,2
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
:	:	kIx,	:
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
<g/>
:	:	kIx,	:
23,5	[number]	k4	23,5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
průměrem	průměr	k1gInSc7	průměr
srážek	srážka	k1gFnPc2	srážka
kolem	kolem	k7c2	kolem
580	[number]	k4	580
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
Antošovice-Nová	Antošovice-Nová	k1gFnSc1	Antošovice-Nová
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
)	)	kIx)	)
měří	měřit	k5eAaImIp3nS	měřit
Ostrava	Ostrava	k1gFnSc1	Ostrava
20,5	[number]	k4	20,5
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
Bartovice-Krásné	Bartovice-Krásný	k2eAgFnPc1d1	Bartovice-Krásný
Pole	pole	k1gFnPc1	pole
<g/>
)	)	kIx)	)
20,1	[number]	k4	20,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
činí	činit	k5eAaImIp3nS	činit
828	[number]	k4	828
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
300	[number]	k4	300
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Správní	správní	k2eAgMnSc1d1	správní
a	a	k8xC	a
územní	územní	k2eAgInSc1d1	územní
vývoj	vývoj	k1gInSc1	vývoj
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
214	[number]	k4	214
km	km	kA	km
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
23	[number]	k4	23
městskými	městský	k2eAgInPc7d1	městský
obvody	obvod	k1gInPc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1990	[number]	k4	1990
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ostrava	Ostrava	k1gFnSc1	Ostrava
bude	být	k5eAaImBp3nS	být
dělena	dělit	k5eAaImNgFnS	dělit
na	na	k7c4	na
22	[number]	k4	22
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
se	se	k3xPyFc4	se
od	od	k7c2	od
Poruby	Poruba	k1gFnSc2	Poruba
oddělil	oddělit	k5eAaPmAgInS	oddělit
nejmladší	mladý	k2eAgInSc1d3	nejmladší
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
-	-	kIx~	-
Plesná	plesný	k2eAgFnSc1d1	Plesná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
obvody	obvod	k1gInPc1	obvod
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
více	hodně	k6eAd2	hodně
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
statutu	statut	k1gInSc2	statut
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
připadla	připadnout	k5eAaPmAgFnS	připadnout
předsedovi	předseda	k1gMnSc3	předseda
příslušného	příslušný	k2eAgInSc2d1	příslušný
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
primátorem	primátor	k1gMnSc7	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starostů	starosta	k1gMnPc2	starosta
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zmínit	zmínit	k5eAaPmF	zmínit
Hermanna	Hermann	k1gMnSc4	Hermann
Zwierzinu	Zwierzina	k1gMnSc4	Zwierzina
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc4	její
prvního	první	k4xOgMnSc4	první
starostu	starosta	k1gMnSc4	starosta
<g/>
,	,	kIx,	,
a	a	k8xC	a
Johanna	Johann	k1gMnSc4	Johann
Ulricha	Ulrich	k1gMnSc4	Ulrich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
starostou	starosta	k1gMnSc7	starosta
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
starostů	starosta	k1gMnPc2	starosta
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
prvním	první	k4xOgMnSc7	první
československým	československý	k2eAgMnSc7d1	československý
starostou	starosta	k1gMnSc7	starosta
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Jan	Jan	k1gMnSc1	Jan
Prokeš	Prokeš	k1gMnSc1	Prokeš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podpořil	podpořit	k5eAaPmAgMnS	podpořit
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
výstavbu	výstavba	k1gFnSc4	výstavba
města	město	k1gNnSc2	město
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
modernizaci	modernizace	k1gFnSc4	modernizace
jeho	jeho	k3xOp3gFnSc2	jeho
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
zásluhy	zásluha	k1gFnPc4	zásluha
připomíná	připomínat	k5eAaImIp3nS	připomínat
Prokešovo	Prokešův	k2eAgNnSc1d1	Prokešovo
náměstí	náměstí	k1gNnSc1	náměstí
před	před	k7c7	před
Novou	nový	k2eAgFnSc7d1	nová
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc4d1	speciální
zásluhu	zásluha	k1gFnSc4	zásluha
má	mít	k5eAaImIp3nS	mít
protektorátní	protektorátní	k2eAgMnSc1d1	protektorátní
starosta	starosta	k1gMnSc1	starosta
Emil	Emil	k1gMnSc1	Emil
Beier	Beier	k1gMnSc1	Beier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prosadil	prosadit	k5eAaPmAgMnS	prosadit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
oč	oč	k6eAd1	oč
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
Jan	Jan	k1gMnSc1	Jan
Prokeš	Prokeš	k1gMnSc1	Prokeš
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
-	-	kIx~	-
spojit	spojit	k5eAaPmF	spojit
Moravskou	moravský	k2eAgFnSc4d1	Moravská
a	a	k8xC	a
Slezskou	slezský	k2eAgFnSc4d1	Slezská
Ostravu	Ostrava	k1gFnSc4	Ostrava
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
poválečných	poválečný	k2eAgMnPc2d1	poválečný
starostů	starosta	k1gMnPc2	starosta
zasluhuje	zasluhovat	k5eAaImIp3nS	zasluhovat
pozornost	pozornost	k1gFnSc4	pozornost
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
primátorem	primátor	k1gMnSc7	primátor
zvolen	zvolit	k5eAaPmNgMnS	zvolit
tři	tři	k4xCgNnPc4	tři
funkční	funkční	k2eAgNnPc4d1	funkční
období	období	k1gNnPc4	období
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
hejtmanem	hejtman	k1gMnSc7	hejtman
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
či	či	k8xC	či
Petr	Petr	k1gMnSc1	Petr
Kajnar	Kajnar	k1gMnSc1	Kajnar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
zón	zóna	k1gFnPc2	zóna
v	v	k7c6	v
Hrabové	hrabový	k2eAgFnSc6d1	Hrabová
a	a	k8xC	a
Mošnově	Mošnův	k2eAgFnSc6d1	Mošnova
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přes	přes	k7c4	přes
9	[number]	k4	9
000	[number]	k4	000
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
primátorem	primátor	k1gMnSc7	primátor
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Macura	Macura	k1gMnSc1	Macura
zvolený	zvolený	k2eAgMnSc1d1	zvolený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
Úřad	úřad	k1gInSc1	úřad
práce	práce	k1gFnSc1	práce
Ostrava	Ostrava	k1gFnSc1	Ostrava
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
úřad	úřad	k1gInSc4	úřad
Ostrava	Ostrava	k1gFnSc1	Ostrava
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
zastupování	zastupování	k1gNnSc4	zastupování
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
majetkových	majetkový	k2eAgInPc2d1	majetkový
územní	územní	k2eAgNnSc4d1	územní
pracoviště	pracoviště	k1gNnSc4	pracoviště
Ostrava	Ostrava	k1gFnSc1	Ostrava
Celní	celní	k2eAgFnSc1d1	celní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj	kraj	k1gInSc4	kraj
Finanční	finanční	k2eAgNnSc1d1	finanční
ředitelství	ředitelství	k1gNnSc1	ředitelství
pro	pro	k7c4	pro
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj	kraj	k1gInSc4	kraj
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
-	-	kIx~	-
Ostrava	Ostrava	k1gFnSc1	Ostrava
Okresní	okresní	k2eAgFnSc1d1	okresní
správa	správa	k1gFnSc1	správa
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
Ostrava	Ostrava	k1gFnSc1	Ostrava
Energetický	energetický	k2eAgInSc1d1	energetický
regulační	regulační	k2eAgInSc1d1	regulační
úřad	úřad	k1gInSc1	úřad
-	-	kIx~	-
Dislokované	dislokovaný	k2eAgNnSc1d1	dislokované
pracoviště	pracoviště	k1gNnSc1	pracoviště
Ostrava	Ostrava	k1gFnSc1	Ostrava
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
Havlíčkově	Havlíčkův	k2eAgNnSc6d1	Havlíčkovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
působnost	působnost	k1gFnSc1	působnost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
původního	původní	k2eAgInSc2d1	původní
Severomoravského	severomoravský	k2eAgInSc2d1	severomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
U	u	k7c2	u
Soudu	soud	k1gInSc2	soud
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
obvodu	obvod	k1gInSc6	obvod
Poruba	Poruba	k1gFnSc1	Poruba
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
nepůsobí	působit	k5eNaImIp3nS	působit
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
okrese	okres	k1gInSc6	okres
Ostrava-město	Ostravaěsta	k1gMnSc5	Ostrava-města
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
působnost	působnost	k1gFnSc1	působnost
je	být	k5eAaImIp3nS	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
jen	jen	k6eAd1	jen
územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
brněnským	brněnský	k2eAgInSc7d1	brněnský
městským	městský	k2eAgInSc7d1	městský
soudem	soud	k1gInSc7	soud
jde	jít	k5eAaImIp3nS	jít
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
soudců	soudce	k1gMnPc2	soudce
o	o	k7c4	o
největší	veliký	k2eAgInPc4d3	veliký
české	český	k2eAgInPc4d1	český
okresní	okresní	k2eAgInPc4d1	okresní
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
soudům	soud	k1gInPc3	soud
přísluší	příslušet	k5eAaImIp3nS	příslušet
krajské	krajský	k2eAgNnSc4d1	krajské
a	a	k8xC	a
okresní	okresní	k2eAgNnSc4d1	okresní
státní	státní	k2eAgNnSc4d1	státní
zastupitelství	zastupitelství	k1gNnSc4	zastupitelství
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ale	ale	k9	ale
i	i	k9	i
pobočka	pobočka	k1gFnSc1	pobočka
Vrchního	vrchní	k2eAgNnSc2d1	vrchní
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Krajská	krajský	k2eAgFnSc1d1	krajská
hygienická	hygienický	k2eAgFnSc1d1	hygienická
stanice	stanice	k1gFnSc1	stanice
Ostrava	Ostrava	k1gFnSc1	Ostrava
Národní	národní	k2eAgFnSc2d1	národní
památkový	památkový	k2eAgInSc4d1	památkový
ústav	ústav	k1gInSc4	ústav
Ostrava	Ostrava	k1gFnSc1	Ostrava
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
a	a	k8xC	a
logistickým	logistický	k2eAgInSc7d1	logistický
uzlem	uzel	k1gInSc7	uzel
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
25	[number]	k4	25
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Ostravu	Ostrava	k1gFnSc4	Ostrava
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
evropských	evropský	k2eAgFnPc2d1	Evropská
metropolí	metropol	k1gFnPc2	metropol
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
kód	kód	k1gInSc4	kód
<g/>
:	:	kIx,	:
OSR	OSR	kA	OSR
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
LKMT	LKMT	kA	LKMT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
dostupné	dostupný	k2eAgFnSc6d1	dostupná
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
létají	létat	k5eAaImIp3nP	létat
několikrát	několikrát	k6eAd1	několikrát
týdně	týdně	k6eAd1	týdně
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
lety	let	k1gInPc4	let
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
Düsseldorfu	Düsseldorf	k1gInSc2	Düsseldorf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
jsou	být	k5eAaImIp3nP	být
destinace	destinace	k1gFnPc1	destinace
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c6	o
převážně	převážně	k6eAd1	převážně
středomořská	středomořský	k2eAgNnPc1d1	středomořské
letoviska	letovisko	k1gNnPc1	letovisko
<g/>
.	.	kIx.	.
</s>
<s>
Páteří	páteř	k1gFnSc7	páteř
silniční	silniční	k2eAgFnSc2d1	silniční
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
je	být	k5eAaImIp3nS	být
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
dopravu	doprava	k1gFnSc4	doprava
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
přes	přes	k7c4	přes
Ostravu	Ostrava	k1gFnSc4	Ostrava
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
Ostrava	Ostrava	k1gFnSc1	Ostrava
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
360	[number]	k4	360
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
170	[number]	k4	170
km	km	kA	km
<g/>
,	,	kIx,	,
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
polských	polský	k2eAgFnPc2d1	polská
Katovic	Katovice	k1gFnPc2	Katovice
a	a	k8xC	a
310	[number]	k4	310
km	km	kA	km
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ostravou	Ostrava	k1gFnSc7	Ostrava
dále	daleko	k6eAd2	daleko
procházejí	procházet	k5eAaImIp3nP	procházet
silnice	silnice	k1gFnPc4	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
11	[number]	k4	11
<g/>
,	,	kIx,	,
56	[number]	k4	56
<g/>
,	,	kIx,	,
58	[number]	k4	58
a	a	k8xC	a
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Ostravy	Ostrava	k1gFnSc2	Ostrava
vede	vést	k5eAaImIp3nS	vést
evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
E75	E75	k1gFnSc1	E75
a	a	k8xC	a
E	E	kA	E
<g/>
462	[number]	k4	462
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgInSc7d1	významný
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
železničním	železniční	k2eAgInSc6d1	železniční
koridoru	koridor	k1gInSc6	koridor
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
tudy	tudy	k6eAd1	tudy
dálková	dálkový	k2eAgFnSc1d1	dálková
osobní	osobní	k2eAgFnSc1d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
ostravskými	ostravský	k2eAgInPc7d1	ostravský
nádražími	nádraží	k1gNnPc7	nádraží
jsou	být	k5eAaImIp3nP	být
Ostrava	Ostrava	k1gFnSc1	Ostrava
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
Ostrava-Svinov	Ostrava-Svinov	k1gInSc4	Ostrava-Svinov
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
s	s	k7c7	s
tramvajemi	tramvaj	k1gFnPc7	tramvaj
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
začaly	začít	k5eAaPmAgFnP	začít
tramvaje	tramvaj	k1gFnPc1	tramvaj
jezdit	jezdit	k5eAaImF	jezdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
rozšiřující	rozšiřující	k2eAgFnSc1d1	rozšiřující
se	se	k3xPyFc4	se
síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
tratě	trať	k1gFnPc1	trať
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
především	především	k9	především
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
východ	východ	k1gInSc4	východ
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nepřekrývaly	překrývat	k5eNaImAgFnP	překrývat
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
úzkorozchodných	úzkorozchodný	k2eAgFnPc2d1	úzkorozchodná
drah	draha	k1gFnPc2	draha
mezi	mezi	k7c7	mezi
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
,	,	kIx,	,
Karvinou	Karviná	k1gFnSc7	Karviná
a	a	k8xC	a
Bohumínem	Bohumín	k1gInSc7	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byla	být	k5eAaImAgFnS	být
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
dráha	dráha	k1gFnSc1	dráha
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
provozovalo	provozovat	k5eAaImAgNnS	provozovat
Vítkovické	vítkovický	k2eAgNnSc1d1	Vítkovické
horní	horní	k2eAgNnSc1d1	horní
a	a	k8xC	a
hutní	hutní	k2eAgNnSc1d1	hutní
těžířstvo	těžířstvo	k1gNnSc1	těžířstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
společnosti	společnost	k1gFnPc1	společnost
provozující	provozující	k2eAgFnSc2d1	provozující
elektrické	elektrický	k2eAgFnSc2d1	elektrická
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
byly	být	k5eAaImAgFnP	být
stavěny	stavěn	k2eAgFnPc1d1	stavěna
tratě	trať	k1gFnPc1	trať
do	do	k7c2	do
sídlišť	sídliště	k1gNnPc2	sídliště
(	(	kIx(	(
<g/>
Poruba	Poruba	k1gFnSc1	Poruba
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
továrnám	továrna	k1gFnPc3	továrna
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
tratí	trať	k1gFnPc2	trať
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
otevřen	otevřen	k2eAgInSc1d1	otevřen
úsek	úsek	k1gInSc1	úsek
podél	podél	k7c2	podél
Místecké	místecký	k2eAgFnSc2d1	Místecká
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
ostravská	ostravský	k2eAgFnSc1d1	Ostravská
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
nejmodernějším	moderní	k2eAgFnPc3d3	nejmodernější
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
trolejbusů	trolejbus	k1gInPc2	trolejbus
začal	začít	k5eAaPmAgInS	začít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgNnPc6d1	jiné
českých	český	k2eAgNnPc6d1	české
městech	město	k1gNnPc6	město
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
okružní	okružní	k2eAgFnSc1d1	okružní
trať	trať	k1gFnSc1	trať
kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
postupně	postupně	k6eAd1	postupně
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
úzkorozchodné	úzkorozchodný	k2eAgFnPc4d1	úzkorozchodná
dráhy	dráha	k1gFnPc4	dráha
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
sídliště	sídliště	k1gNnSc4	sídliště
Fifejdy	Fifejda	k1gFnSc2	Fifejda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Koblova	Koblův	k2eAgNnSc2d1	Koblův
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
nastal	nastat	k5eAaPmAgInS	nastat
útlum	útlum	k1gInSc1	útlum
v	v	k7c6	v
rozšiřovaní	rozšiřovaný	k2eAgMnPc1d1	rozšiřovaný
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Novou	Nová	k1gFnSc4	Nová
Karolinu	Karolinum	k1gNnSc3	Karolinum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
krátká	krátký	k2eAgFnSc1d1	krátká
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Hulváky	Hulvák	k1gMnPc4	Hulvák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
28.2	[number]	k4	28.2
<g/>
.2016	.2016	k4	.2016
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Hranečník	hranečník	k1gInSc4	hranečník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tak	tak	k9	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Přestupní	přestupní	k2eAgInSc1d1	přestupní
uzel	uzel	k1gInSc1	uzel
Hranečník	hranečník	k1gInSc1	hranečník
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
také	také	k9	také
propojení	propojení	k1gNnSc2	propojení
Ostravy	Ostrava	k1gFnSc2	Ostrava
s	s	k7c7	s
oderskou	oderský	k2eAgFnSc7d1	Oderská
vodní	vodní	k2eAgFnSc7d1	vodní
cestou	cesta	k1gFnSc7	cesta
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
výstavby	výstavba	k1gFnSc2	výstavba
průplavu	průplav	k1gInSc2	průplav
Dunaj-Odra-Labe	Dunaj-Odra-Lab	k1gMnSc5	Dunaj-Odra-Lab
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
koncentraci	koncentrace	k1gFnSc3	koncentrace
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
znečištěné	znečištěný	k2eAgNnSc4d1	znečištěné
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ovzduší	ovzduší	k1gNnSc1	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
měření	měření	k1gNnSc2	měření
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
patří	patřit	k5eAaImIp3nS	patřit
znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
benzopyrenem	benzopyren	k1gInSc7	benzopyren
a	a	k8xC	a
prachovými	prachový	k2eAgFnPc7d1	prachová
částicemi	částice	k1gFnPc7	částice
k	k	k7c3	k
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ze	z	k7c2	z
statistik	statistika	k1gFnPc2	statistika
Ostrava	Ostrava	k1gFnSc1	Ostrava
nevychází	vycházet	k5eNaImIp3nS	vycházet
nejlépe	dobře	k6eAd3	dobře
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
také	také	k9	také
díky	díky	k7c3	díky
řadě	řada	k1gFnSc3	řada
ekologických	ekologický	k2eAgNnPc2d1	ekologické
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
hodlá	hodlat	k5eAaImIp3nS	hodlat
realizovat	realizovat	k5eAaBmF	realizovat
významný	významný	k2eAgMnSc1d1	významný
znečišťovatel	znečišťovatel	k1gMnSc1	znečišťovatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
hutnická	hutnický	k2eAgFnSc1d1	Hutnická
společnost	společnost	k1gFnSc1	společnost
ArcelorMittal	ArcelorMittal	k1gMnSc1	ArcelorMittal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
ArcelorMittal	ArcelorMittal	k1gMnSc1	ArcelorMittal
chce	chtít	k5eAaImIp3nS	chtít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
třináct	třináct	k4xCc4	třináct
ekologických	ekologický	k2eAgFnPc2d1	ekologická
investic	investice	k1gFnPc2	investice
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
tří	tři	k4xCgFnPc2	tři
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
zachycení	zachycení	k1gNnSc1	zachycení
61	[number]	k4	61
tun	tuna	k1gFnPc2	tuna
prachu	prach	k1gInSc2	prach
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Stavu	stav	k1gInSc2	stav
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
projektů	projekt	k1gInPc2	projekt
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
jsou	být	k5eAaImIp3nP	být
speciální	speciální	k2eAgFnPc1d1	speciální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
http://dycham.ostrava.cz	[url]	k1gFnPc2	http://dycham.ostrava.cz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
občanům	občan	k1gMnPc3	občan
sledovat	sledovat	k5eAaImF	sledovat
aktuální	aktuální	k2eAgInSc4d1	aktuální
stav	stav	k1gInSc4	stav
ovzduší	ovzduší	k1gNnSc2	ovzduší
nebo	nebo	k8xC	nebo
ozdravné	ozdravný	k2eAgInPc4d1	ozdravný
pobyty	pobyt	k1gInPc4	pobyt
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
ohrožené	ohrožený	k2eAgFnPc4d1	ohrožená
znečištěním	znečištění	k1gNnSc7	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ropné	ropný	k2eAgFnPc4d1	ropná
laguny	laguna	k1gFnPc4	laguna
bývalé	bývalý	k2eAgFnSc2d1	bývalá
chemičky	chemička	k1gFnSc2	chemička
Ostramo	Ostrama	k1gFnSc5	Ostrama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
ekologické	ekologický	k2eAgFnSc2d1	ekologická
zátěže	zátěž	k1gFnSc2	zátěž
<g/>
.	.	kIx.	.
</s>
<s>
Správou	správa	k1gFnSc7	správa
<g/>
,	,	kIx,	,
přípravou	příprava	k1gFnSc7	příprava
a	a	k8xC	a
zajištěním	zajištění	k1gNnSc7	zajištění
odstranění	odstranění	k1gNnSc3	odstranění
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
Diamo	Diama	k1gFnSc5	Diama
<g/>
.	.	kIx.	.
</s>
<s>
Ropným	ropný	k2eAgFnPc3d1	ropná
lagunám	laguna	k1gFnPc3	laguna
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
pozornost	pozornost	k1gFnSc1	pozornost
na	na	k7c6	na
vládní	vládní	k2eAgFnSc6d1	vládní
úrovni	úroveň	k1gFnSc6	úroveň
i	i	k8xC	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
řešením	řešení	k1gNnSc7	řešení
sanace	sanace	k1gFnSc2	sanace
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
zemin	zemina	k1gFnPc2	zemina
v	v	k7c6	v
lagunách	laguna	k1gFnPc6	laguna
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
zabývat	zabývat	k5eAaImF	zabývat
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vypracuje	vypracovat	k5eAaPmIp3nS	vypracovat
koncepční	koncepční	k2eAgInPc4d1	koncepční
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
posléze	posléze	k6eAd1	posléze
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k8xS	jako
zadavatel	zadavatel	k1gMnSc1	zadavatel
sanace	sanace	k1gFnSc2	sanace
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc4	čtyři
stálá	stálý	k2eAgNnPc4d1	stálé
divadla	divadlo	k1gNnPc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
moravskoslezské	moravskoslezský	k2eAgNnSc4d1	moravskoslezské
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
scény	scéna	k1gFnPc4	scéna
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
a	a	k8xC	a
Divadlo	divadlo	k1gNnSc1	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Myrona	Myron	k1gMnSc2	Myron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Divadlo	divadlo	k1gNnSc1	divadlo
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
Komorní	komorní	k2eAgFnSc1d1	komorní
scéna	scéna	k1gFnSc1	scéna
Aréna	aréna	k1gFnSc1	aréna
a	a	k8xC	a
Divadlo	divadlo	k1gNnSc1	divadlo
loutek	loutka	k1gFnPc2	loutka
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
v	v	k7c4	v
liché	lichý	k2eAgInPc4d1	lichý
roky	rok	k1gInPc4	rok
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
loutkářský	loutkářský	k2eAgInSc4d1	loutkářský
festival	festival	k1gInSc4	festival
Spectaculo	Spectacula	k1gFnSc5	Spectacula
Interesse	Interesse	k1gFnPc1	Interesse
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
sudý	sudý	k2eAgInSc4d1	sudý
rok	rok	k1gInSc4	rok
festival	festival	k1gInSc1	festival
Divadlo	divadlo	k1gNnSc1	divadlo
bez	bez	k7c2	bez
bariér	bariéra	k1gFnPc2	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
působí	působit	k5eAaImIp3nS	působit
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
filharmonie	filharmonie	k1gFnSc1	filharmonie
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
zde	zde	k6eAd1	zde
probíhají	probíhat	k5eAaImIp3nP	probíhat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
festivaly	festival	k1gInPc4	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
Janáčkův	Janáčkův	k2eAgInSc1d1	Janáčkův
máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
či	či	k8xC	či
Ostravské	ostravský	k2eAgInPc1d1	ostravský
dny	den	k1gInPc1	den
-	-	kIx~	-
Festival	festival	k1gInSc1	festival
nové	nový	k2eAgFnSc2d1	nová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
koná	konat	k5eAaImIp3nS	konat
multižánrový	multižánrový	k2eAgInSc1d1	multižánrový
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
každoročně	každoročně	k6eAd1	každoročně
přivádí	přivádět	k5eAaImIp3nS	přivádět
řadu	řada	k1gFnSc4	řada
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
-	-	kIx~	-
Plesné	plesný	k2eAgNnSc1d1	Plesné
tradičně	tradičně	k6eAd1	tradičně
probíhá	probíhat	k5eAaImIp3nS	probíhat
folklórní	folklórní	k2eAgInSc1d1	folklórní
festival	festival	k1gInSc1	festival
Májová	májový	k2eAgFnSc1d1	Májová
Plesná	plesný	k2eAgFnSc1d1	Plesná
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
sochařské	sochařský	k2eAgNnSc1d1	sochařské
sympozium	sympozium	k1gNnSc1	sympozium
Plesná	plesný	k2eAgFnSc1d1	Plesná
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
produkty	produkt	k1gInPc1	produkt
dnes	dnes	k6eAd1	dnes
zdobí	zdobit	k5eAaImIp3nP	zdobit
nejen	nejen	k6eAd1	nejen
plesenský	plesenský	k2eAgInSc4d1	plesenský
parčík	parčík	k1gInSc4	parčík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Porubě	Poruba	k1gFnSc3	Poruba
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
Třebovicích	Třebovice	k1gFnPc6	Třebovice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
patří	patřit	k5eAaImIp3nS	patřit
filmové	filmový	k2eAgInPc4d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgInPc4d1	divadelní
festivaly	festival	k1gInPc4	festival
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
Kamera	kamera	k1gFnSc1	kamera
Oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
outdoorových	outdoorův	k2eAgInPc2d1	outdoorův
filmů	film	k1gInPc2	film
nebo	nebo	k8xC	nebo
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
Letní	letní	k2eAgFnPc1d1	letní
shakespearovské	shakespearovský	k2eAgFnPc1d1	shakespearovská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
na	na	k7c6	na
Slezskoostravském	slezskoostravský	k2eAgInSc6d1	slezskoostravský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Zvykům	zvyk	k1gInPc3	zvyk
a	a	k8xC	a
tradicím	tradice	k1gFnPc3	tradice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
věnuje	věnovat	k5eAaImIp3nS	věnovat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
adventních	adventní	k2eAgInPc2d1	adventní
a	a	k8xC	a
vánočních	vánoční	k2eAgInPc2d1	vánoční
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
koled	koleda	k1gFnPc2	koleda
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
Souznění	souznění	k1gNnSc2	souznění
<g/>
,	,	kIx,	,
festival	festival	k1gInSc1	festival
Folklor	folklor	k1gInSc1	folklor
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
nebo	nebo	k8xC	nebo
Irský	irský	k2eAgInSc1d1	irský
kulturní	kulturní	k2eAgInSc1d1	kulturní
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
irskou	irský	k2eAgFnSc4d1	irská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
také	také	k9	také
řadu	řada	k1gFnSc4	řada
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
OSTRAVSKÉ	ostravský	k2eAgNnSc1d1	ostravské
MUZEUM	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Staré	Staré	k2eAgFnSc2d1	Staré
radnice	radnice	k1gFnSc2	radnice
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
stálé	stálý	k2eAgFnPc1d1	stálá
expozice	expozice	k1gFnPc1	expozice
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
historii	historie	k1gFnSc4	historie
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc3	příroda
i	i	k8xC	i
krajině	krajina	k1gFnSc3	krajina
Ostravska	Ostravsko	k1gNnSc2	Ostravsko
<g/>
.	.	kIx.	.
</s>
<s>
MALÝ	malý	k2eAgInSc1d1	malý
SVĚT	svět	k1gInSc1	svět
TECHNIKY	technika	k1gFnSc2	technika
U6	U6	k1gFnSc2	U6
umožní	umožnit	k5eAaPmIp3nS	umožnit
namísto	namísto	k7c2	namísto
složité	složitý	k2eAgFnSc2d1	složitá
teorie	teorie	k1gFnSc2	teorie
z	z	k7c2	z
nudných	nudný	k2eAgFnPc2d1	nudná
učebnic	učebnice	k1gFnPc2	učebnice
testování	testování	k1gNnSc2	testování
vědecko-technické	vědeckoechnický	k2eAgFnSc2d1	vědecko-technická
zákonitosti	zákonitost	k1gFnSc2	zákonitost
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohou	moct	k5eAaImIp3nP	moct
zkusit	zkusit	k5eAaPmF	zkusit
řídit	řídit	k5eAaImF	řídit
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
proletět	proletět	k5eAaPmF	proletět
se	se	k3xPyFc4	se
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
zahrát	zahrát	k5eAaPmF	zahrát
si	se	k3xPyFc3	se
na	na	k7c4	na
taviče	tavič	k1gMnPc4	tavič
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
vesmírného	vesmírný	k2eAgMnSc4d1	vesmírný
kosmonauta	kosmonaut	k1gMnSc4	kosmonaut
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
Nema	Nemus	k1gMnSc4	Nemus
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
složitá	složitý	k2eAgFnSc1d1	složitá
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
hře	hra	k1gFnSc3	hra
stává	stávat	k5eAaImIp3nS	stávat
logickou	logický	k2eAgFnSc7d1	logická
a	a	k8xC	a
jasnou	jasný	k2eAgFnSc7d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
VELKÝ	velký	k2eAgInSc1d1	velký
SVĚT	svět	k1gInSc1	svět
TECHNIKY	technika	k1gFnSc2	technika
nabízí	nabízet	k5eAaImIp3nS	nabízet
stovky	stovka	k1gFnPc4	stovka
zábavných	zábavný	k2eAgFnPc2d1	zábavná
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pobaví	pobavit	k5eAaPmIp3nP	pobavit
a	a	k8xC	a
poučí	poučit	k5eAaPmIp3nP	poučit
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc6d1	přírodní
i	i	k8xC	i
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
14	[number]	k4	14
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
na	na	k7c4	na
vás	vy	k3xPp2nPc4	vy
čekají	čekat	k5eAaImIp3nP	čekat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
"	"	kIx"	"
<g/>
světy	svět	k1gInPc7	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
MUZEUM	muzeum	k1gNnSc1	muzeum
HRAČEK	hračka	k1gFnPc2	hračka
Ke	k	k7c3	k
zhlédnutí	zhlédnutí	k1gNnSc3	zhlédnutí
jsou	být	k5eAaImIp3nP	být
unikáty	unikát	k1gInPc1	unikát
i	i	k8xC	i
rarity	rarita	k1gFnPc1	rarita
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
také	také	k9	také
nejstarší	starý	k2eAgFnPc1d3	nejstarší
hračky	hračka	k1gFnPc1	hračka
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
ŽELEZNIČNÍ	železniční	k2eAgNnSc1d1	železniční
MUZEUM	muzeum	k1gNnSc1	muzeum
Děti	dítě	k1gFnPc1	dítě
potěší	potěšit	k5eAaPmIp3nS	potěšit
modely	model	k1gInPc4	model
vláčků	vláček	k1gInPc2	vláček
<g/>
,	,	kIx,	,
milovníky	milovník	k1gMnPc4	milovník
historie	historie	k1gFnSc1	historie
pak	pak	k6eAd1	pak
originály	originál	k1gInPc1	originál
dokumentů	dokument	k1gInPc2	dokument
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
PIVOVARSKÉ	pivovarský	k2eAgNnSc1d1	Pivovarské
MUZEUM	muzeum	k1gNnSc1	muzeum
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pivo	pivo	k1gNnSc1	pivo
vaří	vařit	k5eAaImIp3nS	vařit
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
daří	dařit	k5eAaImIp3nS	dařit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pivo	pivo	k1gNnSc1	pivo
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídková	prohlídkový	k2eAgFnSc1d1	prohlídková
trasa	trasa	k1gFnSc1	trasa
umožní	umožnit	k5eAaPmIp3nS	umožnit
seznámit	seznámit	k5eAaPmF	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
postupy	postup	k1gInPc7	postup
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc7	jeho
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Příjemnou	příjemný	k2eAgFnSc7d1	příjemná
tečkou	tečka	k1gFnSc7	tečka
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
je	být	k5eAaImIp3nS	být
ochutnávka	ochutnávka	k1gFnSc1	ochutnávka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dá	dát	k5eAaPmIp3nS	dát
pocítit	pocítit	k5eAaPmF	pocítit
pravou	pravý	k2eAgFnSc4d1	pravá
chuť	chuť	k1gFnSc4	chuť
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
HASIČSKÉ	hasičský	k2eAgNnSc1d1	hasičské
MUZEUM	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
secesní	secesní	k2eAgFnSc6d1	secesní
budově	budova	k1gFnSc6	budova
někdejšího	někdejší	k2eAgInSc2d1	někdejší
německého	německý	k2eAgInSc2d1	německý
sboru	sbor	k1gInSc2	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
v	v	k7c6	v
Ostravě-Přívoze	Ostravě-Přívoz	k1gInSc5	Ostravě-Přívoz
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
expozice	expozice	k1gFnSc2	expozice
je	být	k5eAaImIp3nS	být
videoprojekce	videoprojekce	k1gFnSc1	videoprojekce
přibližující	přibližující	k2eAgFnSc4d1	přibližující
práci	práce	k1gFnSc4	práce
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
MUZEUM	muzeum	k1gNnSc1	muzeum
KELTIČKOVA	KELTIČKOVA	kA	KELTIČKOVA
KOVÁRNA	kovárna	k1gFnSc1	kovárna
V	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
muzea	muzeum	k1gNnSc2	muzeum
údajně	údajně	k6eAd1	údajně
žil	žít	k5eAaImAgMnS	žít
kovář	kovář	k1gMnSc1	kovář
Keltička	Keltička	k1gFnSc1	Keltička
-	-	kIx~	-
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
tradované	tradovaný	k2eAgFnSc2d1	tradovaná
pověsti	pověst	k1gFnSc2	pověst
objevitel	objevitel	k1gMnSc1	objevitel
černého	černé	k1gNnSc2	černé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
domem	dům	k1gInSc7	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
památník	památník	k1gInSc1	památník
kováře	kovář	k1gMnSc2	kovář
Keltičky	Keltička	k1gFnSc2	Keltička
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
expozice	expozice	k1gFnSc1	expozice
mapuje	mapovat	k5eAaImIp3nS	mapovat
prvopočátky	prvopočátek	k1gInPc4	prvopočátek
hornictví	hornictví	k1gNnPc2	hornictví
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
zhlédnou	zhlédnout	k5eAaPmIp3nP	zhlédnout
sbírku	sbírka	k1gFnSc4	sbírka
kahanů	kahan	k1gInPc2	kahan
a	a	k8xC	a
svítidel	svítidlo	k1gNnPc2	svítidlo
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
hornické	hornický	k2eAgInPc4d1	hornický
a	a	k8xC	a
kovářské	kovářský	k2eAgInPc4d1	kovářský
exponáty	exponát	k1gInPc4	exponát
<g/>
.	.	kIx.	.
</s>
<s>
GEOLOGICKÝ	geologický	k2eAgInSc1d1	geologický
PAVILON	pavilon	k1gInSc1	pavilon
PROF.	prof.	kA	prof.
FRANTIŠKA	Františka	k1gFnSc1	Františka
POŠEPNÉHO	POŠEPNÉHO	kA	POŠEPNÉHO
Obdivovat	obdivovat	k5eAaImF	obdivovat
lze	lze	k6eAd1	lze
okolo	okolo	k7c2	okolo
15	[number]	k4	15
000	[number]	k4	000
exponátů	exponát	k1gInPc2	exponát
mineralogických	mineralogický	k2eAgInPc2d1	mineralogický
<g/>
,	,	kIx,	,
petrografických	petrografický	k2eAgInPc2d1	petrografický
a	a	k8xC	a
paleontologických	paleontologický	k2eAgFnPc2d1	paleontologická
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolekce	kolekce	k1gFnSc1	kolekce
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
jinde	jinde	k6eAd1	jinde
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
jindy	jindy	k6eAd1	jindy
než	než	k8xS	než
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jste	být	k5eAaImIp2nP	být
již	již	k6eAd1	již
navštívili	navštívit	k5eAaPmAgMnP	navštívit
tolik	tolik	k4yIc1	tolik
industriálních	industriální	k2eAgFnPc2d1	industriální
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
byste	by	kYmCp2nP	by
takou	takou	k?	takou
sbírku	sbírka	k1gFnSc4	sbírka
měli	mít	k5eAaImAgMnP	mít
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
<g/>
.	.	kIx.	.
</s>
<s>
DŮM	dům	k1gInSc1	dům
UMĚNÍ	umění	k1gNnSc2	umění
Patří	patřit	k5eAaImIp3nS	patřit
určitě	určitě	k6eAd1	určitě
k	k	k7c3	k
architektonickým	architektonický	k2eAgFnPc3d1	architektonická
chloubám	chlouba	k1gFnPc3	chlouba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitou	kvalita	k1gFnSc7	kvalita
sbírek	sbírka	k1gFnPc2	sbírka
se	se	k3xPyFc4	se
galerie	galerie	k1gFnPc1	galerie
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
pětici	pětice	k1gFnSc3	pětice
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
sbírkotvorných	sbírkotvorný	k2eAgFnPc2d1	sbírkotvorná
institucí	instituce	k1gFnPc2	instituce
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
PLATO	plato	k1gNnSc1	plato
-	-	kIx~	-
GALERIE	galerie	k1gFnSc1	galerie
MĚSTA	město	k1gNnSc2	město
OSTRAVY	Ostrava	k1gFnSc2	Ostrava
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
instituci	instituce	k1gFnSc4	instituce
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
sbírkového	sbírkový	k2eAgInSc2d1	sbírkový
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
domácí	domácí	k2eAgNnSc4d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
památkové	památkový	k2eAgFnSc6d1	památková
zóně	zóna	k1gFnSc6	zóna
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc1d1	dolní
oblast	oblast	k1gFnSc1	oblast
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
předměstských	předměstský	k2eAgFnPc6d1	předměstská
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
mnoho	mnoho	k4c1	mnoho
pozoruhodných	pozoruhodný	k2eAgNnPc2d1	pozoruhodné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
velice	velice	k6eAd1	velice
známa	znám	k2eAgFnSc1d1	známa
zástavba	zástavba	k1gFnSc1	zástavba
modernistických	modernistický	k2eAgFnPc2d1	modernistická
vil	vila	k1gFnPc2	vila
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Bělé	Bělá	k1gFnSc6	Bělá
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Bělského	Bělský	k2eAgInSc2d1	Bělský
Lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
ojedinělé	ojedinělý	k2eAgFnPc1d1	ojedinělá
stavby	stavba	k1gFnPc1	stavba
sloužící	sloužící	k1gFnSc2	sloužící
především	především	k6eAd1	především
veřejnému	veřejný	k2eAgInSc3d1	veřejný
životu	život	k1gInSc3	život
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
Staré	Staré	k2eAgFnSc2d1	Staré
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
Mariánským	mariánský	k2eAgInSc7d1	mariánský
morovým	morový	k2eAgInSc7d1	morový
sloupem	sloup	k1gInSc7	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
jsou	být	k5eAaImIp3nP	být
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
čtvrti	čtvrt	k1gFnSc2	čtvrt
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
Divadla	divadlo	k1gNnSc2	divadlo
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
a	a	k8xC	a
funkcionalistickým	funkcionalistický	k2eAgInSc7d1	funkcionalistický
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
Knihcentrum	Knihcentrum	k1gNnSc1	Knihcentrum
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
s	s	k7c7	s
palácem	palác	k1gInSc7	palác
Elektra	Elektrum	k1gNnSc2	Elektrum
a	a	k8xC	a
bankovními	bankovní	k2eAgInPc7d1	bankovní
paláci	palác	k1gInPc7	palác
<g/>
,	,	kIx,	,
či	či	k8xC	či
Prokešovo	Prokešův	k2eAgNnSc1d1	Prokešovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
budova	budova	k1gFnSc1	budova
Nové	Nové	k2eAgFnSc2d1	Nové
radnice	radnice	k1gFnSc2	radnice
s	s	k7c7	s
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nP	stát
také	také	k9	také
církevní	církevní	k2eAgFnPc1d1	církevní
stavby	stavba	k1gFnPc1	stavba
<g/>
:	:	kIx,	:
nejstarší	starý	k2eAgInSc1d3	nejstarší
ostravský	ostravský	k2eAgInSc1d1	ostravský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nebo	nebo	k8xC	nebo
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
<g/>
katedrála	katedrála	k1gFnSc1	katedrála
Božského	božský	k2eAgMnSc2d1	božský
Spasitele	spasitel	k1gMnSc2	spasitel
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
slavných	slavný	k2eAgMnPc2d1	slavný
architektů	architekt	k1gMnPc2	architekt
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Kotas	Kotas	k1gMnSc1	Kotas
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Korner	Kornero	k1gNnPc2	Kornero
nebo	nebo	k8xC	nebo
Alexander	Alexandra	k1gFnPc2	Alexandra
Graf	graf	k1gInSc1	graf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
poblíž	poblíž	k7c2	poblíž
centra	centrum	k1gNnSc2	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
postaveno	postaven	k2eAgNnSc4d1	postaveno
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
Nová	Nová	k1gFnSc1	Nová
Karolina	Karolinum	k1gNnSc2	Karolinum
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Moravské	moravský	k2eAgFnSc3d1	Moravská
Ostravě	Ostrava	k1gFnSc3	Ostrava
je	být	k5eAaImIp3nS	být
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
Ostrava-Poruba	Ostrava-Poruba	k1gMnSc1	Ostrava-Poruba
známý	známý	k1gMnSc1	známý
svou	svůj	k3xOyFgFnSc7	svůj
architekturou	architektura	k1gFnSc7	architektura
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sorela	sorela	k1gFnSc1	sorela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
vzhledem	vzhled	k1gInSc7	vzhled
sovětských	sovětský	k2eAgNnPc2d1	sovětské
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
prvky	prvek	k1gInPc4	prvek
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
bytové	bytový	k2eAgFnSc2d1	bytová
zástavby	zástavba	k1gFnSc2	zástavba
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
Oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těžkým	těžký	k2eAgInSc7d1	těžký
průmyslem	průmysl	k1gInSc7	průmysl
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnSc2	Ostrava-Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
narůstajícím	narůstající	k2eAgInSc7d1	narůstající
počtem	počet	k1gInSc7	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Vítkovických	vítkovický	k2eAgFnPc2d1	Vítkovická
železáren	železárna	k1gFnPc2	železárna
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dělnické	dělnický	k2eAgFnPc1d1	Dělnická
kolonie	kolonie	k1gFnPc1	kolonie
společně	společně	k6eAd1	společně
se	s	k7c7	s
sociálním	sociální	k2eAgNnSc7d1	sociální
zázemím	zázemí	k1gNnSc7	zázemí
<g/>
,	,	kIx,	,
radnicí	radnice	k1gFnSc7	radnice
i	i	k8xC	i
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
režné	režný	k2eAgNnSc1d1	režné
neomítané	omítaný	k2eNgNnSc1d1	neomítané
zdivo	zdivo	k1gNnSc1	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
návštěvu	návštěva	k1gFnSc4	návštěva
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
v	v	k7c6	v
Ostravě-Přívoze	Ostravě-Přívoz	k1gInSc5	Ostravě-Přívoz
se	se	k3xPyFc4	se
secesní	secesní	k2eAgFnSc7d1	secesní
zástavbou	zástavba	k1gFnSc7	zástavba
či	či	k8xC	či
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
kolonie	kolonie	k1gFnSc1	kolonie
v	v	k7c6	v
Ostravě-Hrabůvce	Ostravě-Hrabůvka	k1gFnSc6	Ostravě-Hrabůvka
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
bývalých	bývalý	k2eAgInPc2d1	bývalý
dělnických	dělnický	k2eAgInPc2d1	dělnický
bytů	byt	k1gInPc2	byt
vybudovaných	vybudovaný	k2eAgInPc2d1	vybudovaný
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Citerárium	Citerárium	k1gNnSc1	Citerárium
-	-	kIx~	-
muzeum	muzeum	k1gNnSc1	muzeum
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
citer	citera	k1gFnPc2	citera
Hasičské	hasičský	k2eAgNnSc1d1	hasičské
muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
Hornické	hornický	k2eAgNnSc1d1	Hornické
muzeum	muzeum	k1gNnSc1	muzeum
OKD	OKD	kA	OKD
Landek	Landek	k1gInSc1	Landek
Muzeum	muzeum	k1gNnSc1	muzeum
Mlejn	Mlejn	k1gInSc1	Mlejn
-	-	kIx~	-
Mlýny	mlýn	k1gInPc1	mlýn
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
Keltičkova	Keltičkův	k2eAgFnSc1d1	Keltičkova
kovárna	kovárna	k1gFnSc1	kovárna
-	-	kIx~	-
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
kovářských	kovářský	k2eAgInPc2d1	kovářský
a	a	k8xC	a
hornických	hornický	k2eAgInPc2d1	hornický
exponátů	exponát	k1gInPc2	exponát
NKP	NKP	kA	NKP
Důl	důl	k1gInSc1	důl
Michal	Michal	k1gMnSc1	Michal
Ostravské	ostravský	k2eAgNnSc4d1	ostravské
muzeum	muzeum	k1gNnSc4	muzeum
Evangelický	evangelický	k2eAgInSc1d1	evangelický
Kristův	Kristův	k2eAgInSc1d1	Kristův
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
Ostrava-Polanka	Ostrava-Polanka	k1gFnSc1	Ostrava-Polanka
<g />
.	.	kIx.	.
</s>
<s>
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
(	(	kIx(	(
<g/>
Ostrava-Nová	Ostrava-Nový	k2eAgFnSc1d1	Ostrava-Nová
Ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
(	(	kIx(	(
<g/>
Ostrava-Pustkovec	Ostrava-Pustkovec	k1gInSc1	Ostrava-Pustkovec
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
(	(	kIx(	(
<g/>
Ostrava-Zábřeh	Ostrava-Zábřeh	k1gInSc1	Ostrava-Zábřeh
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
Ostrava-Stará	Ostrava-Starý	k2eAgFnSc1d1	Ostrava-Stará
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
Ostrava-Plesná	Ostrava-Plesný	k2eAgFnSc1d1	Ostrava-Plesná
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Slezská	slezský	k2eAgFnSc1d1	Slezská
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
Ostrava-Hrabová	Ostrava-Hrabová	k1gFnSc1	Ostrava-Hrabová
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Krista	Kristus	k1gMnSc2	Kristus
Krále	Král	k1gMnSc2	Král
(	(	kIx(	(
<g/>
Ostrava-Svinov	Ostrava-Svinov	k1gInSc1	Ostrava-Svinov
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Ostrava-Michálkovice	Ostrava-Michálkovice	k1gFnSc1	Ostrava-Michálkovice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Ostrava-Třebovice	Ostrava-Třebovice	k1gFnSc1	Ostrava-Třebovice
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Ostrava-Zábřeh	Ostrava-Zábřeh	k1gInSc1	Ostrava-Zábřeh
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Ostrava-Radvanice	Ostrava-Radvanice	k1gFnSc1	Ostrava-Radvanice
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Královny	královna	k1gFnPc1	královna
Kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
Katedrála	katedrál	k1gMnSc2	katedrál
Božského	božský	k2eAgMnSc2d1	božský
Spasitele	spasitel	k1gMnSc2	spasitel
Kostel	kostel	k1gInSc4	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Přívoze	přívoz	k1gInSc6	přívoz
Kostel	kostel	k1gInSc1	kostel
<g />
.	.	kIx.	.
</s>
<s>
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
(	(	kIx(	(
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
Krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
Nechte	nechat	k5eAaPmRp2nP	nechat
se	se	k3xPyFc4	se
pohltit	pohltit	k5eAaPmF	pohltit
tajemným	tajemný	k2eAgNnSc7d1	tajemné
industriálním	industriální	k2eAgNnSc7d1	industriální
kouzlem	kouzlo	k1gNnSc7	kouzlo
města	město	k1gNnSc2	město
a	a	k8xC	a
zpestřete	zpestřet	k5eAaPmRp2nP	zpestřet
si	se	k3xPyFc3	se
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
fascinující	fascinující	k2eAgFnPc1d1	fascinující
prohlídkou	prohlídka	k1gFnSc7	prohlídka
Dolní	dolní	k2eAgFnPc1d1	dolní
oblasti	oblast	k1gFnPc1	oblast
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
,	,	kIx,	,
autentickým	autentický	k2eAgInSc7d1	autentický
zážitkem	zážitek	k1gInSc7	zážitek
z	z	k7c2	z
Dolu	dol	k1gInSc2	dol
Michal	Michala	k1gFnPc2	Michala
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnPc2	kombinace
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
fárání	fárání	k1gNnSc2	fárání
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
v	v	k7c4	v
Landek	Landek	k1gInSc4	Landek
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
adventura	adventura	k1gFnSc1	adventura
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
Kód	kód	k1gInSc1	kód
Salomon	Salomon	k1gMnSc1	Salomon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
příběhu	příběh	k1gInSc3	příběh
barona	baron	k1gMnSc2	baron
Rothschilda	Rothschild	k1gMnSc2	Rothschild
stanete	stanout	k5eAaPmIp2nP	stanout
dobrodruhem	dobrodruh	k1gMnSc7	dobrodruh
<g/>
!	!	kIx.	!
</s>
<s>
Při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
navštívíte	navštívit	k5eAaPmIp2nP	navštívit
mnoho	mnoho	k6eAd1	mnoho
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
a	a	k8xC	a
jedinečných	jedinečný	k2eAgNnPc2d1	jedinečné
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
zažijete	zažít	k5eAaPmIp2nP	zažít
velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
,	,	kIx,	,
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
zachráníte	zachránit	k5eAaPmIp2nP	zachránit
město	město	k1gNnSc4	město
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
objevíte	objevit	k5eAaPmIp2nP	objevit
magický	magický	k2eAgInSc4d1	magický
amulet	amulet	k1gInSc4	amulet
<g/>
.	.	kIx.	.
</s>
<s>
Nedivte	divit	k5eNaImRp2nP	divit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
toulek	toulka	k1gFnPc2	toulka
industriálními	industriální	k2eAgNnPc7d1	industriální
zákoutími	zákoutí	k1gNnPc7	zákoutí
města	město	k1gNnSc2	město
narazíte	narazit	k5eAaPmIp2nP	narazit
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
štáb	štáb	k1gInSc4	štáb
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
právě	právě	k6eAd1	právě
točí	točit	k5eAaImIp3nS	točit
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc4	seriál
nebo	nebo	k8xC	nebo
videoklip	videoklip	k1gInSc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Ostravu	Ostrava	k1gFnSc4	Ostrava
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
totiž	totiž	k9	totiž
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
obrazovky	obrazovka	k1gFnPc4	obrazovka
a	a	k8xC	a
plátna	plátno	k1gNnPc1	plátno
přenesly	přenést	k5eAaPmAgFnP	přenést
alespoň	alespoň	k9	alespoň
něco	něco	k3yInSc4	něco
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
<g/>
.	.	kIx.	.
</s>
<s>
NOVÁ	nový	k2eAgFnSc1d1	nová
RADNICE	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
věží	věž	k1gFnSc7	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Prokešově	Prokešův	k2eAgNnSc6d1	Prokešovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ochozu	ochoz	k1gInSc2	ochoz
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
73	[number]	k4	73
m	m	kA	m
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
Ostravu	Ostrava	k1gFnSc4	Ostrava
z	z	k7c2	z
ptačí	ptačí	k2eAgFnSc2d1	ptačí
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Výhled	výhled	k1gInSc1	výhled
s	s	k7c7	s
komentářem	komentář	k1gInSc7	komentář
vás	vy	k3xPp2nPc4	vy
seznámí	seznámit	k5eAaPmIp3nS	seznámit
jak	jak	k6eAd1	jak
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
současností	současnost	k1gFnSc7	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídky	prohlídka	k1gFnPc1	prohlídka
jsou	být	k5eAaImIp3nP	být
každých	každý	k3xTgFnPc2	každý
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
KOMENSKÉHO	Komenského	k2eAgInPc1d1	Komenského
SADY	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
narazíte	narazit	k5eAaPmIp2nP	narazit
na	na	k7c4	na
sochu	socha	k1gFnSc4	socha
sovětských	sovětský	k2eAgMnPc2d1	sovětský
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
cvičící	cvičící	k2eAgMnPc4d1	cvičící
jogíny	jogín	k1gMnPc4	jogín
<g/>
,	,	kIx,	,
tančící	tančící	k2eAgFnPc1d1	tančící
maminky	maminka	k1gFnPc1	maminka
i	i	k9	i
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Proběhněte	proběhnout	k5eAaPmRp2nP	proběhnout
se	se	k3xPyFc4	se
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
ostravské	ostravský	k2eAgFnSc3d1	Ostravská
pláži	pláž	k1gFnSc3	pláž
a	a	k8xC	a
naberte	nabrat	k5eAaPmRp2nP	nabrat
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
dalšímu	další	k1gNnSc3	další
výletu	výlet	k1gInSc2	výlet
Ostravou	Ostrava	k1gFnSc7	Ostrava
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
.	.	kIx.	.
</s>
<s>
SVĚT	svět	k1gInSc1	svět
MINIATUR	miniatura	k1gFnPc2	miniatura
MINIUNI	MINIUNI	kA	MINIUNI
<g/>
,	,	kIx,	,
na	na	k7c6	na
Výstavišti	výstaviště	k1gNnSc6	výstaviště
Černá	černý	k2eAgFnSc1d1	černá
louka	louka	k1gFnSc1	louka
najdete	najít	k5eAaPmIp2nP	najít
malý	malý	k2eAgInSc4d1	malý
svět	svět	k1gInSc4	svět
plný	plný	k2eAgInSc4d1	plný
nejvyhlášenějších	vyhlášený	k2eAgFnPc2d3	nejvyhlášenější
světových	světový	k2eAgFnPc2d1	světová
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
odpoledne	odpoledne	k1gNnSc4	odpoledne
tak	tak	k6eAd1	tak
můžete	moct	k5eAaImIp2nP	moct
objet	objet	k2eAgInSc4d1	objet
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
odolal	odolat	k5eAaPmAgMnS	odolat
<g/>
?	?	kIx.	?
</s>
<s>
Vyfotit	vyfotit	k5eAaPmF	vyfotit
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
pod	pod	k7c7	pod
Eiffelovkou	Eiffelovka	k1gFnSc7	Eiffelovka
<g/>
,	,	kIx,	,
u	u	k7c2	u
egyptské	egyptský	k2eAgFnSc2d1	egyptská
pyramidy	pyramida	k1gFnSc2	pyramida
či	či	k8xC	či
Big	Big	k1gFnSc2	Big
Benu	Ben	k1gInSc2	Ben
<g/>
.	.	kIx.	.
</s>
<s>
POHÁDKOVÝ	pohádkový	k2eAgInSc1d1	pohádkový
ORLOJ	orloj	k1gInSc1	orloj
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
od	od	k7c2	od
osmé	osmý	k4xOgFnSc2	osmý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgInSc4d1	ranní
do	do	k7c2	do
osmé	osmý	k4xOgFnSc2	osmý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgInSc4d1	večerní
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
dvouhodinových	dvouhodinový	k2eAgInPc6d1	dvouhodinový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oknech	okno	k1gNnPc6	okno
galerie	galerie	k1gFnSc2	galerie
loutek	loutka	k1gFnPc2	loutka
se	se	k3xPyFc4	se
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
boj	boj	k1gInSc1	boj
Kašpárka	Kašpárek	k1gMnSc2	Kašpárek
se	s	k7c7	s
smrťákem	smrťák	k1gInSc7	smrťák
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
další	další	k2eAgFnPc4d1	další
čtyři	čtyři	k4xCgFnPc4	čtyři
postavy	postava	k1gFnPc4	postava
-	-	kIx~	-
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
a	a	k8xC	a
čert	čert	k1gMnSc1	čert
<g/>
.	.	kIx.	.
</s>
<s>
Nádherné	nádherný	k2eAgFnPc1d1	nádherná
světelné	světelný	k2eAgFnPc1d1	světelná
hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
nejzajímavější	zajímavý	k2eAgInSc4d3	nejzajímavější
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
SLEZSKOOSTRAVSKÝ	slezskoostravský	k2eAgInSc1d1	slezskoostravský
HRAD	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
důlní	důlní	k2eAgFnSc2d1	důlní
činnosti	činnost	k1gFnSc2	činnost
propadl	propadnout	k5eAaPmAgInS	propadnout
o	o	k7c4	o
16	[number]	k4	16
m	m	kA	m
<g/>
,	,	kIx,	,
najdete	najít	k5eAaPmIp2nP	najít
nedaleko	nedaleko	k7c2	nedaleko
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Lučiny	lučina	k1gFnSc2	lučina
a	a	k8xC	a
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
celoročně	celoročně	k6eAd1	celoročně
mnoho	mnoho	k4c1	mnoho
akcí	akce	k1gFnPc2	akce
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
festival	festival	k1gInSc1	festival
Letní	letní	k2eAgFnSc2d1	letní
shakespearovské	shakespearovský	k2eAgFnSc2d1	shakespearovská
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
STODOLNÍ	stodolní	k2eAgFnSc1d1	stodolní
ULICE	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgNnSc1d1	legendární
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
naleznete	nalézt	k5eAaBmIp2nP	nalézt
přes	přes	k7c4	přes
60	[number]	k4	60
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
line	linout	k5eAaImIp3nS	linout
libá	libý	k2eAgFnSc1d1	libá
vůně	vůně	k1gFnSc1	vůně
všelijakých	všelijaký	k3yIgInPc2	všelijaký
pokrmů	pokrm	k1gInPc2	pokrm
se	s	k7c7	s
směsicí	směsice	k1gFnSc7	směsice
hudby	hudba	k1gFnSc2	hudba
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Stodolní	stodolní	k2eAgFnSc1d1	stodolní
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
nikdy	nikdy	k6eAd1	nikdy
nespí	spát	k5eNaImIp3nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
MASARYKOVO	Masarykův	k2eAgNnSc1d1	Masarykovo
NÁMĚSTÍ	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
střeží	střežit	k5eAaImIp3nS	střežit
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Immaculaty	Immacule	k1gNnPc7	Immacule
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
a	a	k8xC	a
poblíž	poblíž	k6eAd1	poblíž
barokní	barokní	k2eAgFnSc1d1	barokní
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
byla	být	k5eAaImAgFnS	být
přemístěna	přemístěn	k2eAgFnSc1d1	přemístěna
busta	busta	k1gFnSc1	busta
prezidenta	prezident	k1gMnSc2	prezident
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
z	z	k7c2	z
foyer	foyer	k1gNnSc2	foyer
Nové	Nové	k2eAgFnSc2d1	Nové
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
procházka	procházka	k1gFnSc1	procházka
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
dlažbě	dlažba	k1gFnSc6	dlažba
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
kousek	kousek	k1gInSc4	kousek
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Laso	laso	k1gNnSc1	laso
<g/>
.	.	kIx.	.
</s>
<s>
Prohlédněte	prohlédnout	k5eAaPmRp2nP	prohlédnout
si	se	k3xPyFc3	se
také	také	k9	také
Schönhofův	Schönhofův	k2eAgInSc4d1	Schönhofův
dům	dům	k1gInSc4	dům
přezdívaný	přezdívaný	k2eAgInSc4d1	přezdívaný
"	"	kIx"	"
<g/>
Dům	dům	k1gInSc4	dům
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
vchody	vchod	k1gInPc7	vchod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Dům	dům	k1gInSc1	dům
dědiců	dědic	k1gMnPc2	dědic
I.	I.	kA	I.
Reisze	Reisze	k1gFnSc1	Reisze
<g/>
,	,	kIx,	,
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
architekta	architekt	k1gMnSc2	architekt
Wunibalda	Wunibalda	k1gFnSc1	Wunibalda
Deiningera	Deiningera	k1gFnSc1	Deiningera
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
pamětihodnosti	pamětihodnost	k1gFnPc1	pamětihodnost
<g/>
.	.	kIx.	.
</s>
<s>
PORUBA	Poruba	k1gFnSc1	Poruba
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
v	v	k7c6	v
Sorele	Sorela	k1gFnSc6	Sorela
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
nahrazení	nahrazení	k1gNnSc2	nahrazení
městského	městský	k2eAgNnSc2d1	Městské
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
poruby	porub	k1gInPc1	porub
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
monumentálních	monumentální	k2eAgInPc2d1	monumentální
velkolepých	velkolepý	k2eAgInPc2d1	velkolepý
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
nerealizovaly	realizovat	k5eNaBmAgFnP	realizovat
celé	celý	k2eAgFnPc1d1	celá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
hlavní	hlavní	k2eAgInPc4d1	hlavní
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Procházka	Procházka	k1gMnSc1	Procházka
po	po	k7c6	po
historické	historický	k2eAgFnSc6d1	historická
Porubě	Poruba	k1gFnSc6	Poruba
v	v	k7c6	v
člověku	člověk	k1gMnSc6	člověk
zajisté	zajisté	k9	zajisté
zanechá	zanechat	k5eAaPmIp3nS	zanechat
hluboký	hluboký	k2eAgInSc1d1	hluboký
prožitek	prožitek	k1gInSc1	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
TROJHALÍ	TROJHALÍ	kA	TROJHALÍ
KAROLINA	Karolinum	k1gNnPc4	Karolinum
<g/>
,	,	kIx,	,
Historické	historický	k2eAgFnPc4d1	historická
budovy	budova	k1gFnPc4	budova
na	na	k7c6	na
území	území	k1gNnSc6	území
po	po	k7c6	po
bývalé	bývalý	k2eAgFnSc6d1	bývalá
koksovně	koksovna	k1gFnSc6	koksovna
Karolina	Karolinum	k1gNnSc2	Karolinum
jsou	být	k5eAaImIp3nP	být
přetvořeny	přetvořen	k2eAgInPc1d1	přetvořen
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Pleskota	Pleskot	k1gMnSc2	Pleskot
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
lidé	člověk	k1gMnPc1	člověk
chodí	chodit	k5eAaImIp3nP	chodit
za	za	k7c7	za
sportem	sport	k1gInSc7	sport
a	a	k8xC	a
za	za	k7c7	za
zábavou	zábava	k1gFnSc7	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svým	svůj	k3xOyFgNnSc7	svůj
moderním	moderní	k2eAgNnSc7d1	moderní
využitím	využití	k1gNnSc7	využití
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
unikátní	unikátní	k2eAgFnSc3d1	unikátní
kombinaci	kombinace	k1gFnSc3	kombinace
ostravského	ostravský	k2eAgNnSc2d1	ostravské
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
multifunkčnosti	multifunkčnost	k1gFnSc2	multifunkčnost
<g/>
,	,	kIx,	,
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Půvdoně	Půvdoň	k1gFnPc4	Půvdoň
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
turbíny	turbína	k1gFnPc4	turbína
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sem	sem	k6eAd1	sem
přichází	přicházet	k5eAaImIp3nP	přicházet
návštěvníci	návštěvník	k1gMnPc1	návštěvník
za	za	k7c7	za
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
příjemně	příjemně	k6eAd1	příjemně
strávit	strávit	k5eAaPmF	strávit
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
Trojhalí	Trojhalý	k2eAgMnPc1d1	Trojhalý
Karolina	Karolinum	k1gNnSc2	Karolinum
jsou	být	k5eAaImIp3nP	být
spojnicí	spojnice	k1gFnSc7	spojnice
mezi	mezi	k7c7	mezi
centrem	centrum	k1gNnSc7	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc7d1	dolní
oblastí	oblast	k1gFnSc7	oblast
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
budou	být	k5eAaImBp3nP	být
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
důležitých	důležitý	k2eAgInPc2d1	důležitý
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostorů	prostor	k1gInPc2	prostor
stát	stát	k5eAaPmF	stát
příjemně	příjemně	k6eAd1	příjemně
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
ZOO	zoo	k1gFnSc1	zoo
OSTRAVA	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
rozlohou	rozloha	k1gFnSc7	rozloha
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
zoo	zoo	k1gFnSc1	zoo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
400	[number]	k4	400
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
pavilony	pavilon	k1gInPc1	pavilon
<g/>
,	,	kIx,	,
výběhy	výběh	k1gInPc1	výběh
<g/>
,	,	kIx,	,
večerní	večerní	k2eAgFnPc4d1	večerní
prohlídky	prohlídka	k1gFnPc4	prohlídka
<g/>
,	,	kIx,	,
komentovaná	komentovaný	k2eAgNnPc4d1	komentované
krmení	krmení	k1gNnPc4	krmení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
zimní	zimní	k2eAgNnSc1d1	zimní
běžkování	běžkování	k1gNnSc1	běžkování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
novou	nový	k2eAgFnSc7d1	nová
součástí	součást	k1gFnSc7	součást
Safari	safari	k1gNnSc2	safari
asijských	asijský	k2eAgInPc2d1	asijský
kopytníků	kopytník	k1gInPc2	kopytník
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
Pavilon	pavilon	k1gInSc4	pavilon
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
DOLNÍ	dolní	k2eAgInPc1d1	dolní
VÍTKOVICE	Vítkovice	k1gInPc1	Vítkovice
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
jsou	být	k5eAaImIp3nP	být
unikátním	unikátní	k2eAgInSc7d1	unikátní
světem	svět	k1gInSc7	svět
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
pec	pec	k1gFnSc4	pec
č.	č.	k?	č.
1	[number]	k4	1
se	se	k3xPyFc4	se
dostanete	dostat	k5eAaPmIp2nP	dostat
proskleným	prosklený	k2eAgInSc7d1	prosklený
výtahem	výtah	k1gInSc7	výtah
a	a	k8xC	a
po	po	k7c6	po
zdolání	zdolání	k1gNnSc6	zdolání
pár	pár	k4xCyI	pár
dalších	další	k2eAgInPc2d1	další
schodů	schod	k1gInPc2	schod
se	se	k3xPyFc4	se
ocitnete	ocitnout	k5eAaPmIp2nP	ocitnout
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Uvidíte	uvidět	k5eAaPmIp2nP	uvidět
nejen	nejen	k6eAd1	nejen
celý	celý	k2eAgInSc4d1	celý
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Ostravu	Ostrava	k1gFnSc4	Ostrava
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
pak	pak	k6eAd1	pak
nahlédnete	nahlédnout	k5eAaPmIp2nP	nahlédnout
do	do	k7c2	do
útrob	útroba	k1gFnPc2	útroba
tavicí	tavicí	k2eAgFnSc2d1	tavicí
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
s	s	k7c7	s
průvodcem	průvodce	k1gMnSc7	průvodce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vás	vy	k3xPp2nPc4	vy
seznámí	seznámit	k5eAaPmIp3nS	seznámit
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
rodiny	rodina	k1gFnSc2	rodina
Rothschildů	Rothschild	k1gMnPc2	Rothschild
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Energetické	energetický	k2eAgFnPc4d1	energetická
ústředny	ústředna	k1gFnPc4	ústředna
<g/>
,	,	kIx,	,
kompresorovny	kompresorovna	k1gFnPc4	kompresorovna
<g/>
,	,	kIx,	,
starými	starý	k2eAgFnPc7d1	stará
hornickými	hornický	k2eAgFnPc7d1	hornická
koupelnami	koupelna	k1gFnPc7	koupelna
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Gongem	gong	k1gInSc7	gong
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgFnSc7d1	unikátní
halou	hala	k1gFnSc7	hala
postavenou	postavený	k2eAgFnSc7d1	postavená
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
plynojemu	plynojem	k1gInSc2	plynojem
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
restaurace	restaurace	k1gFnSc2	restaurace
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
jsou	být	k5eAaImIp3nP	být
nejnovějšími	nový	k2eAgFnPc7d3	nejnovější
možnostmi	možnost	k1gFnPc7	možnost
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
MALÝ	malý	k2eAgInSc4d1	malý
A	a	k8xC	a
VELKÝ	velký	k2eAgInSc4d1	velký
SVĚT	svět	k1gInSc4	svět
TECHNIKY	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
svět	svět	k1gInSc1	svět
techniky	technika	k1gFnSc2	technika
U6	U6	k1gFnSc2	U6
umožní	umožnit	k5eAaPmIp3nS	umožnit
namísto	namísto	k7c2	namísto
složité	složitý	k2eAgFnSc2d1	složitá
teorie	teorie	k1gFnSc2	teorie
z	z	k7c2	z
nudných	nudný	k2eAgFnPc2d1	nudná
učebnic	učebnice	k1gFnPc2	učebnice
testování	testování	k1gNnSc2	testování
vědecko-technické	vědeckoechnický	k2eAgFnSc2d1	vědecko-technická
zákonitosti	zákonitost	k1gFnSc2	zákonitost
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohou	moct	k5eAaImIp3nP	moct
zkusit	zkusit	k5eAaPmF	zkusit
řídit	řídit	k5eAaImF	řídit
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
proletět	proletět	k5eAaPmF	proletět
se	se	k3xPyFc4	se
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
zahrát	zahrát	k5eAaPmF	zahrát
si	se	k3xPyFc3	se
na	na	k7c4	na
taviče	tavič	k1gMnPc4	tavič
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
vesmírného	vesmírný	k2eAgMnSc4d1	vesmírný
kosmonauta	kosmonaut	k1gMnSc4	kosmonaut
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
Nema	Nemus	k1gMnSc4	Nemus
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
složitá	složitý	k2eAgFnSc1d1	složitá
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
hře	hra	k1gFnSc3	hra
stává	stávat	k5eAaImIp3nS	stávat
logickou	logický	k2eAgFnSc7d1	logická
a	a	k8xC	a
jasnou	jasný	k2eAgFnSc7d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
svět	svět	k1gInSc1	svět
techniky	technika	k1gFnSc2	technika
nabízí	nabízet	k5eAaImIp3nS	nabízet
stovky	stovka	k1gFnPc4	stovka
zábavných	zábavný	k2eAgFnPc2d1	zábavná
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pobaví	pobavit	k5eAaPmIp3nP	pobavit
a	a	k8xC	a
poučí	poučit	k5eAaPmIp3nP	poučit
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc6d1	přírodní
i	i	k8xC	i
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
14	[number]	k4	14
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
na	na	k7c4	na
vás	vy	k3xPp2nPc4	vy
čekají	čekat	k5eAaImIp3nP	čekat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
"	"	kIx"	"
<g/>
světy	svět	k1gInPc7	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
DŮL	důl	k1gInSc1	důl
MICHAL	Michala	k1gFnPc2	Michala
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zdař	zdařit	k5eAaPmRp2nS	zdařit
bůh	bůh	k1gMnSc1	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
vás	vy	k3xPp2nPc4	vy
budou	být	k5eAaImBp3nP	být
vítat	vítat	k5eAaImF	vítat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nejlépe	dobře	k6eAd3	dobře
zachovalém	zachovalý	k2eAgInSc6d1	zachovalý
důlním	důlní	k2eAgInSc6d1	důlní
areálu	areál	k1gInSc6	areál
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prohlídce	prohlídka	k1gFnSc6	prohlídka
projdete	projít	k5eAaPmIp2nP	projít
stejnou	stejný	k2eAgFnSc4d1	stejná
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
chodili	chodit	k5eAaImAgMnP	chodit
horníci	horník	k1gMnPc1	horník
každodenně	každodenně	k6eAd1	každodenně
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Uvidíte	uvidět	k5eAaPmIp2nP	uvidět
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
řetízkový	řetízkový	k2eAgInSc1d1	řetízkový
systém	systém	k1gInSc1	systém
šaten	šatna	k1gFnPc2	šatna
<g/>
,	,	kIx,	,
koupelny	koupelna	k1gFnSc2	koupelna
<g/>
,	,	kIx,	,
převlékárny	převlékárna	k1gFnSc2	převlékárna
<g/>
,	,	kIx,	,
dispečink	dispečink	k1gInSc4	dispečink
<g/>
,	,	kIx,	,
strojovnu	strojovna	k1gFnSc4	strojovna
s	s	k7c7	s
dochovanými	dochovaný	k2eAgInPc7d1	dochovaný
těžními	těžní	k2eAgInPc7d1	těžní
stroji	stroj	k1gInPc7	stroj
a	a	k8xC	a
kompresory	kompresor	k1gInPc7	kompresor
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
zde	zde	k6eAd1	zde
nesfáráte	sfárat	k5eNaPmIp2nP	sfárat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musíte	muset	k5eAaImIp2nP	muset
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
Hornické	hornický	k2eAgNnSc4d1	Hornické
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c4	v
Landek	Landek	k1gInSc4	Landek
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
LANDEK	LANDEK	kA	LANDEK
PARK	park	k1gInSc1	park
-	-	kIx~	-
HORNICKÉ	hornický	k2eAgNnSc1d1	Hornické
MUZEUM	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
do	do	k7c2	do
Hornického	hornický	k2eAgNnSc2d1	Hornické
muzea	muzeum	k1gNnSc2	muzeum
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
vydat	vydat	k5eAaPmF	vydat
na	na	k7c4	na
prohlídku	prohlídka	k1gFnSc4	prohlídka
uhelného	uhelný	k2eAgInSc2d1	uhelný
dolu	dol	k1gInSc2	dol
Anselm	Anselm	k1gMnSc1	Anselm
s	s	k7c7	s
průvodcem-horníkem	průvodcemorník	k1gMnSc7	průvodcem-horník
<g/>
.	.	kIx.	.
</s>
<s>
Připraveni	připraven	k2eAgMnPc1d1	připraven
buďte	budit	k5eAaImRp2nP	budit
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
sešup	sešup	k1gInSc4	sešup
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
autentický	autentický	k2eAgInSc1d1	autentický
důlní	důlní	k2eAgInSc1d1	důlní
rámus	rámus	k1gInSc1	rámus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hornickou	hornický	k2eAgFnSc4d1	hornická
mluvu	mluva	k1gFnSc4	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Ochutnat	ochutnat	k5eAaPmF	ochutnat
můžete	moct	k5eAaImIp2nP	moct
místní	místní	k2eAgFnPc4d1	místní
speciality	specialita	k1gFnPc4	specialita
hornickou	hornický	k2eAgFnSc4d1	hornická
svačinku	svačinka	k1gFnSc4	svačinka
nebo	nebo	k8xC	nebo
Hornickou	hornický	k2eAgFnSc4d1	hornická
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ponořte	ponořit	k5eAaPmRp2nP	ponořit
se	se	k3xPyFc4	se
do	do	k7c2	do
bohaté	bohatý	k2eAgFnSc2d1	bohatá
kultury	kultura	k1gFnSc2	kultura
této	tento	k3xDgFnSc2	tento
hrdé	hrdý	k2eAgFnSc2d1	hrdá
profese	profes	k1gFnSc2	profes
<g/>
.	.	kIx.	.
</s>
<s>
PLANETÁRIUM	planetárium	k1gNnSc1	planetárium
OSTRAVA	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
po	po	k7c6	po
zelené	zelený	k2eAgFnSc6d1	zelená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
dojdete	dojít	k5eAaPmIp2nP	dojít
k	k	k7c3	k
budově	budova	k1gFnSc3	budova
planetária	planetárium	k1gNnSc2	planetárium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
vědomosti	vědomost	k1gFnPc4	vědomost
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
nebo	nebo	k8xC	nebo
strávit	strávit	k5eAaPmF	strávit
příjemné	příjemný	k2eAgFnSc2d1	příjemná
chvíle	chvíle	k1gFnSc2	chvíle
poslechem	poslech	k1gInSc7	poslech
hudby	hudba	k1gFnSc2	hudba
pod	pod	k7c7	pod
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
oblohou	obloha	k1gFnSc7	obloha
za	za	k7c2	za
každého	každý	k3xTgNnSc2	každý
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
HALDA	halda	k1gFnSc1	halda
EMA	Ema	k1gFnSc1	Ema
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc2	Ostravice
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
315	[number]	k4	315
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
vládne	vládnout	k5eAaImIp3nS	vládnout
subtropické	subtropický	k2eAgNnSc4d1	subtropické
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
sníh	sníh	k1gInSc1	sníh
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nehledejte	hledat	k5eNaImRp2nP	hledat
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
trávu	tráva	k1gFnSc4	tráva
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Bělostné	bělostný	k2eAgInPc1d1	bělostný
obláčky	obláček	k1gInPc1	obláček
plynu	plyn	k1gInSc2	plyn
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
halda	halda	k1gFnSc1	halda
stále	stále	k6eAd1	stále
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
Ostravu	Ostrava	k1gFnSc4	Ostrava
je	být	k5eAaImIp3nS	být
úchvatný	úchvatný	k2eAgInSc1d1	úchvatný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
žluté	žlutý	k2eAgFnSc6d1	žlutá
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
si	se	k3xPyFc3	se
vyšlápněte	vyšlápnout	k5eAaPmRp2nP	vyšlápnout
na	na	k7c4	na
haldu	halda	k1gFnSc4	halda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
místní	místní	k2eAgMnPc1d1	místní
znají	znát	k5eAaImIp3nP	znát
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Terezie	Terezie	k1gFnSc2	Terezie
Ema	Ema	k1gFnSc1	Ema
nebo	nebo	k8xC	nebo
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
sopka	sopka	k1gFnSc1	sopka
<g/>
.	.	kIx.	.
</s>
<s>
DinoPark	DinoPark	k1gInSc1	DinoPark
Ostrava	Ostrava	k1gFnSc1	Ostrava
Černá	Černá	k1gFnSc1	Černá
louka	louka	k1gFnSc1	louka
(	(	kIx(	(
<g/>
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
)	)	kIx)	)
GALERIE	galerie	k1gFnSc1	galerie
VÝTVARNÉHO	výtvarný	k2eAgNnSc2d1	výtvarné
UMĚNÍ	umění	k1gNnSc2	umění
KATEDRÁLA	katedrála	k1gFnSc1	katedrála
BOŽSKÉHO	božský	k2eAgMnSc2d1	božský
SPASITELE	spasitel	k1gMnSc2	spasitel
KOSTEL	kostel	k1gInSc4	kostel
NEPOSKVRNĚNÉHO	poskvrněný	k2eNgNnSc2d1	neposkvrněné
POČETÍ	početí	k1gNnSc2	početí
PANNY	Panna	k1gFnSc2	Panna
MARIE	Maria	k1gFnSc2	Maria
PŘÍVOZ	přívoz	k1gInSc1	přívoz
DIVADLO	divadlo	k1gNnSc1	divadlo
ANTONÍNA	Antonín	k1gMnSc2	Antonín
DVOŘÁKA	Dvořák	k1gMnSc2	Dvořák
DIVADLO	divadlo	k1gNnSc1	divadlo
JIŘÍHO	Jiří	k1gMnSc2	Jiří
MYRONA	MYRONA	kA	MYRONA
ÚDOLÍ	údolí	k1gNnSc4	údolí
TROJICE	trojice	k1gFnSc2	trojice
1	[number]	k4	1
<g/>
st	st	kA	st
International	International	k1gFnSc1	International
School	School	k1gInSc1	School
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
EDUCAnet	EDUCAnet	k1gInSc1	EDUCAnet
-	-	kIx~	-
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
gymnázium	gymnázium	k1gNnSc1	gymnázium
Ostrava	Ostrava	k1gFnSc1	Ostrava
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Hladnov	Hladnov	k1gInSc4	Hladnov
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
<g />
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Ostrava-Hrabůvka	Ostrava-Hrabůvka	k1gFnSc1	Ostrava-Hrabůvka
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Ostrava-Zábřeh	Ostrava-Zábřeh	k1gInSc1	Ostrava-Zábřeh
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
a	a	k8xC	a
Gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
Jazykové	jazykový	k2eAgNnSc1d1	jazykové
gymnázium	gymnázium	k1gNnSc1	gymnázium
Pavla	Pavel	k1gMnSc2	Pavel
Tigrida	Tigrid	k1gMnSc2	Tigrid
Matiční	matiční	k2eAgNnSc4d1	matiční
gymnázium	gymnázium	k1gNnSc4	gymnázium
Ostrava	Ostrava	k1gFnSc1	Ostrava
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc2d1	sociální
Ostrava-Mariánské	Ostrava-Mariánský	k2eAgFnSc2d1	Ostrava-Mariánský
Hory	hora	k1gFnSc2	hora
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
gymnázium	gymnázium	k1gNnSc1	gymnázium
Dany	Dana	k1gFnSc2	Dana
a	a	k8xC	a
Emila	Emil	k1gMnSc2	Emil
Zátopkových	Zátopkových	k2eAgFnSc1d1	Zátopkových
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
dopravní	dopravní	k2eAgFnSc1d1	dopravní
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnPc1	Ostrava-Vítkovice
Střední	střední	k1gMnSc1	střední
<g />
.	.	kIx.	.
</s>
<s>
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
waldorfská	waldorfský	k2eAgFnSc1d1	Waldorfská
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
chemická	chemický	k2eAgFnSc1d1	chemická
akademika	akademik	k1gMnSc4	akademik
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
a	a	k8xC	a
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnSc2	Ostrava-Vítkovice
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc1d1	stavební
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
Ostrava-Kunčice	Ostrava-Kunčice	k1gFnSc2	Ostrava-Kunčice
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
prof.	prof.	kA	prof.
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Matějčka	Matějček	k1gMnSc2	Matějček
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
podnikání	podnikání	k1gNnSc3	podnikání
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
společného	společný	k2eAgNnSc2d1	společné
stravování	stravování	k1gNnSc2	stravování
<g />
.	.	kIx.	.
</s>
<s>
Ostrava-Hrabůvka	Ostrava-Hrabůvka	k1gFnSc1	Ostrava-Hrabůvka
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc1d1	stavební
a	a	k8xC	a
dřevozpracující	dřevozpracující	k2eAgFnSc1d1	dřevozpracující
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
teleinformatiky	teleinformatika	k1gFnSc2	teleinformatika
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
škola	škola	k1gFnSc1	škola
Ostrava	Ostrava	k1gFnSc1	Ostrava
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
Ostrava	Ostrava	k1gFnSc1	Ostrava
Vítkovická	vítkovický	k2eAgFnSc1d1	Vítkovická
střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc1	gymnázium
Wichterlovo	Wichterlův	k2eAgNnSc1d1	Wichterlovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
AHOL	AHOL	kA	AHOL
-	-	kIx~	-
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc1d1	sociální
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Jana	Jan	k1gMnSc2	Jan
Ámose	Ámos	k1gMnSc2	Ámos
<g />
.	.	kIx.	.
</s>
<s>
Komenského	Komenského	k2eAgFnSc1d1	Komenského
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
Hornoslezská	hornoslezský	k2eAgFnSc1d1	Hornoslezská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
Newport	Newport	k1gInSc4	Newport
International	International	k1gMnSc2	International
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
externí	externí	k2eAgNnSc4d1	externí
pracoviště	pracoviště	k1gNnSc4	pracoviště
Ostrava	Ostrava	k1gFnSc1	Ostrava
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
Panevropská	panevropský	k2eAgFnSc1d1	panevropská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
pracoviště	pracoviště	k1gNnSc1	pracoviště
Ostrava	Ostrava	k1gFnSc1	Ostrava
Vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
báňská	báňský	k2eAgFnSc1d1	báňská
-	-	kIx~	-
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
Ostrava	Ostrava	k1gFnSc1	Ostrava
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
1	[number]	k4	1
<g/>
st	st	kA	st
International	International	k1gFnPc2	International
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
Ostrčilova	Ostrčilův	k2eAgFnSc1d1	Ostrčilova
Bilingual	Bilingual	k1gInSc4	Bilingual
School	Schoola	k1gFnPc2	Schoola
Gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Hello	Hello	k1gNnSc1	Hello
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
ZŠ	ZŠ	kA	ZŠ
a	a	k8xC	a
MŠ	MŠ	kA	MŠ
Monty	Monta	k1gFnPc4	Monta
School	School	k1gInSc4	School
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Hladnov	Hladnovo	k1gNnPc2	Hladnovo
a	a	k8xC	a
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Slezské	slezský	k2eAgFnSc6d1	Slezská
Ostravě	Ostrava	k1gFnSc6	Ostrava
Jazykové	jazykový	k2eAgNnSc4d1	jazykové
gymnázium	gymnázium	k1gNnSc4	gymnázium
Pavla	Pavel	k1gMnSc2	Pavel
Tigrida	Tigrid	k1gMnSc2	Tigrid
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
-	-	kIx~	-
Porubě	Poruba	k1gFnSc6	Poruba
PORG	PORG	kA	PORG
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc1	gymnázium
o.	o.	k?	o.
p.	p.	k?	p.
s.	s.	k?	s.
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
se	s	k7c7	s
sportovní	sportovní	k2eAgFnSc7d1	sportovní
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
kvalitním	kvalitní	k2eAgMnSc7d1	kvalitní
hostitelem	hostitel	k1gMnSc7	hostitel
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
jak	jak	k8xC	jak
republikového	republikový	k2eAgNnSc2d1	republikové
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
evropského	evropský	k2eAgInSc2d1	evropský
či	či	k8xC	či
světového	světový	k2eAgInSc2d1	světový
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
občané	občan	k1gMnPc1	občan
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
sportovní	sportovní	k2eAgNnSc4d1	sportovní
publikum	publikum	k1gNnSc4	publikum
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
nabízí	nabízet	k5eAaImIp3nS	nabízet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
sportovního	sportovní	k2eAgNnSc2d1	sportovní
vyžití	vyžití	k1gNnSc2	vyžití
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
Ostrava	Ostrava	k1gFnSc1	Ostrava
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
Evropské	evropský	k2eAgNnSc1d1	Evropské
město	město	k1gNnSc1	město
sportu	sport	k1gInSc2	sport
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
atletické	atletický	k2eAgInPc4d1	atletický
<g/>
,	,	kIx,	,
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
a	a	k8xC	a
zimní	zimní	k2eAgInPc4d1	zimní
stadiony	stadion	k1gInPc4	stadion
<g/>
,	,	kIx,	,
víceúčelové	víceúčelový	k2eAgFnPc4d1	víceúčelová
sportovní	sportovní	k2eAgFnPc4d1	sportovní
haly	hala	k1gFnPc4	hala
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
<g/>
,	,	kIx,	,
squashové	squashový	k2eAgInPc4d1	squashový
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgInPc4d1	krytý
bazény	bazén	k1gInPc4	bazén
<g/>
,	,	kIx,	,
koupaliště	koupaliště	k1gNnPc4	koupaliště
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
sportovní	sportovní	k2eAgNnPc4d1	sportovní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
areály	areál	k1gInPc4	areál
provozuje	provozovat	k5eAaImIp3nS	provozovat
společnost	společnost	k1gFnSc1	společnost
Sportovní	sportovní	k2eAgFnSc2d1	sportovní
a	a	k8xC	a
rekreační	rekreační	k2eAgNnPc4d1	rekreační
zařízení	zařízení	k1gNnPc4	zařízení
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
SAREZA	SAREZA	kA	SAREZA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
letní	letní	k2eAgNnSc1d1	letní
koupaliště	koupaliště	k1gNnSc1	koupaliště
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnPc4d3	veliký
přírodní	přírodní	k2eAgNnPc4d1	přírodní
koupaliště	koupaliště	k1gNnPc4	koupaliště
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavači	vyznavač	k1gMnPc1	vyznavač
cyklistiky	cyklistika	k1gFnSc2	cyklistika
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
nabídky	nabídka	k1gFnSc2	nabídka
cyklotras	cyklotrasa	k1gFnPc2	cyklotrasa
a	a	k8xC	a
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Ostravu	Ostrava	k1gFnSc4	Ostrava
protínají	protínat	k5eAaImIp3nP	protínat
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgFnPc1d1	bohatá
příležitosti	příležitost	k1gFnPc1	příležitost
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
okolí	okolí	k1gNnSc4	okolí
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohořet	k5eAaPmIp3nP	pohořet
Beskydy	Beskydy	k1gFnPc4	Beskydy
a	a	k8xC	a
Jeseníky	Jeseník	k1gInPc4	Jeseník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
leží	ležet	k5eAaImIp3nS	ležet
cca	cca	kA	cca
30	[number]	k4	30
km	km	kA	km
a	a	k8xC	a
60	[number]	k4	60
km	km	kA	km
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
dobrými	dobrý	k2eAgFnPc7d1	dobrá
podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
lyžování	lyžování	k1gNnSc4	lyžování
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
jara	jaro	k1gNnSc2	jaro
až	až	k8xS	až
podzimu	podzim	k1gInSc2	podzim
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dostatek	dostatek	k1gInSc4	dostatek
příležitostí	příležitost	k1gFnPc2	příležitost
pro	pro	k7c4	pro
pěší	pěší	k2eAgFnSc4d1	pěší
turistiku	turistika	k1gFnSc4	turistika
<g/>
,	,	kIx,	,
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
a	a	k8xC	a
rybaření	rybařený	k2eAgMnPc1d1	rybařený
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
do	do	k7c2	do
Beskyd	Beskydy	k1gFnPc2	Beskydy
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
cyklobus	cyklobus	k1gInSc1	cyklobus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
především	především	k9	především
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cykloturistů	cykloturista	k1gMnPc2	cykloturista
s	s	k7c7	s
jízdními	jízdní	k2eAgNnPc7d1	jízdní
koly	kolo	k1gNnPc7	kolo
a	a	k8xC	a
pěších	pěší	k2eAgMnPc2d1	pěší
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
připraven	připraven	k2eAgInSc4d1	připraven
Skibus	Skibus	k1gInSc4	Skibus
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
golfu	golf	k1gInSc2	golf
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
koníček	koníček	k1gInSc4	koníček
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dokonale	dokonale	k6eAd1	dokonale
upravený	upravený	k2eAgInSc4d1	upravený
a	a	k8xC	a
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
náročnější	náročný	k2eAgInSc4d2	náročnější
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zamířit	zamířit	k5eAaPmF	zamířit
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
zámeckém	zámecký	k2eAgInSc6d1	zámecký
parku	park	k1gInSc6	park
v	v	k7c6	v
Šilheřovicích	Šilheřovice	k1gFnPc6	Šilheřovice
u	u	k7c2	u
Ostravy	Ostrava	k1gFnSc2	Ostrava
či	či	k8xC	či
na	na	k7c4	na
vzdálenější	vzdálený	k2eAgNnSc4d2	vzdálenější
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c4	v
Čeladné	Čeladný	k2eAgMnPc4d1	Čeladný
<g/>
,	,	kIx,	,
Ropici	Ropice	k1gFnSc4	Ropice
a	a	k8xC	a
Ostravici	Ostravice	k1gFnSc4	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
km	km	kA	km
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
v	v	k7c6	v
Kravařích	Kravaře	k1gInPc6	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
mnoha	mnoho	k4c2	mnoho
vrcholových	vrcholový	k2eAgFnPc2d1	vrcholová
sportovních	sportovní	k2eAgFnPc2d1	sportovní
evropských	evropský	k2eAgFnPc2d1	Evropská
i	i	k8xC	i
světových	světový	k2eAgInPc2d1	světový
šampionátů	šampionát	k1gInPc2	šampionát
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádá	pořádat	k5eAaImIp3nS	pořádat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
atletický	atletický	k2eAgInSc1d1	atletický
meeting	meeting	k1gInSc1	meeting
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
tretra	tretra	k1gFnSc1	tretra
Ostrava	Ostrava	k1gFnSc1	Ostrava
obsazovaný	obsazovaný	k2eAgInSc4d1	obsazovaný
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
atlety	atlet	k1gMnPc7	atlet
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
Ostrava	Ostrava	k1gFnSc1	Ostrava
stala	stát	k5eAaPmAgFnS	stát
spolupořadatelem	spolupořadatel	k1gMnSc7	spolupořadatel
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
ČEZ	ČEZ	kA	ČEZ
ARÉNĚ	aréna	k1gFnSc6	aréna
-	-	kIx~	-
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
víceúčelových	víceúčelový	k2eAgFnPc2d1	víceúčelová
hal	hala	k1gFnPc2	hala
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
opakuje	opakovat	k5eAaImIp3nS	opakovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
hostila	hostit	k5eAaImAgFnS	hostit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
MS	MS	kA	MS
v	v	k7c6	v
odbíjené	odbíjená	k1gFnSc6	odbíjená
žen	žena	k1gFnPc2	žena
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
MS	MS	kA	MS
ve	v	k7c6	v
vzpírání	vzpírání	k1gNnSc6	vzpírání
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
MS	MS	kA	MS
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
mužů	muž	k1gMnPc2	muž
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
ME	ME	kA	ME
v	v	k7c6	v
kulturistice	kulturistika	k1gFnSc6	kulturistika
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
MS	MS	kA	MS
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
ME	ME	kA	ME
ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
mužů	muž	k1gMnPc2	muž
2003	[number]	k4	2003
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
ME	ME	kA	ME
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
MS	MS	kA	MS
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
ME	ME	kA	ME
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
MS	MS	kA	MS
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
latinsko-amerických	latinskomerický	k2eAgInPc6d1	latinsko-americký
tancích	tanec	k1gInPc6	tanec
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
ME	ME	kA	ME
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
MS	MS	kA	MS
v	v	k7c6	v
latinsko-amerických	latinskomerický	k2eAgInPc6d1	latinsko-americký
tancích	tanec	k1gInPc6	tanec
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
ME	ME	kA	ME
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
teamgym	teamgym	k1gInSc1	teamgym
MS	MS	kA	MS
v	v	k7c6	v
kulturistice	kulturistika	k1gFnSc6	kulturistika
mužů	muž	k1gMnPc2	muž
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
kolo	kolo	k1gNnSc4	kolo
světové	světový	k2eAgFnSc2d1	světová
<g />
.	.	kIx.	.
</s>
<s>
skupiny	skupina	k1gFnPc1	skupina
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
MS	MS	kA	MS
mládeže	mládež	k1gFnSc2	mládež
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
MS	MS	kA	MS
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
mužů	muž	k1gMnPc2	muž
-	-	kIx~	-
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
MS	MS	kA	MS
ve	v	k7c6	v
freestyle	freestyl	k1gInSc5	freestyl
motokrosu	motokros	k1gInSc3	motokros
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
ME	ME	kA	ME
U20	U20	k1gFnSc2	U20
v	v	k7c6	v
rugby	rugby	k1gNnSc6	rugby
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
MS	MS	kA	MS
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
žen	žena	k1gFnPc2	žena
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
MS	MS	kA	MS
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
žen	žena	k1gFnPc2	žena
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Davis	Davis	k1gFnSc1	Davis
Cup	cup	k1gInSc1	cup
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
atletický	atletický	k2eAgInSc1d1	atletický
mítink	mítink	k1gInSc1	mítink
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
tretra	tretra	k1gFnSc1	tretra
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
sídlí	sídlet	k5eAaImIp3nP	sídlet
týmy	tým	k1gInPc1	tým
FC	FC	kA	FC
Baník	Baník	k1gInSc1	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
MFK	MFK	kA	MFK
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
Steel	Stelo	k1gNnPc2	Stelo
<g/>
,	,	kIx,	,
HC	HC	kA	HC
RT	RT	kA	RT
Torax	torax	k1gInSc1	torax
Poruba	Poruba	k1gFnSc1	Poruba
(	(	kIx(	(
<g/>
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NH	NH	kA	NH
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
WOOW	WOOW	kA	WOOW
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
a	a	k8xC	a
Remedicum	Remedicum	k1gNnSc4	Remedicum
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
florbal	florbal	k1gInSc1	florbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arrows	Arrows	k1gInSc1	Arrows
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
baseball	baseball	k1gInSc1	baseball
a	a	k8xC	a
softball	softball	k1gInSc1	softball
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
VK	VK	kA	VK
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
Steelers	Steelersa	k1gFnPc2	Steelersa
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jehuda	Jehuda	k1gMnSc1	Jehuda
Bacon	Bacon	k1gMnSc1	Bacon
-	-	kIx~	-
izraelský	izraelský	k2eAgMnSc1d1	izraelský
malíř	malíř	k1gMnSc1	malíř
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Jan	Jan	k1gMnSc1	Jan
Balabán	Balabán	k1gMnSc1	Balabán
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
)	)	kIx)	)
Pavla	Pavla	k1gFnSc1	Pavla
Beretová	Beretový	k2eAgFnSc1d1	Beretová
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Beno	Beno	k1gMnSc1	Beno
Blachut	Blachut	k1gMnSc1	Blachut
-	-	kIx~	-
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
Václav	Václav	k1gMnSc1	Václav
Bednář	Bednář	k1gMnSc1	Bednář
-	-	kIx~	-
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Slávka	Slávka	k1gFnSc1	Slávka
Budínová	Budínová	k1gFnSc1	Budínová
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Čejka	Čejka	k1gMnSc1	Čejka
-	-	kIx~	-
mim	mim	k1gMnSc1	mim
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Čelovský	Čelovský	k1gMnSc1	Čelovský
-	-	kIx~	-
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
Tamara	Tamara	k1gFnSc1	Tamara
Černá	Černá	k1gFnSc1	Černá
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
SofiG	SofiG	k1gFnSc2	SofiG
<g/>
)	)	kIx)	)
-	-	kIx~	-
primabalerina	primabalerina	k1gFnSc1	primabalerina
<g/>
,	,	kIx,	,
umělecká	umělecký	k2eAgFnSc1d1	umělecká
fotografka	fotografka	k1gFnSc1	fotografka
Oldřich	Oldřich	k1gMnSc1	Oldřich
Daněk	Daněk	k1gMnSc1	Daněk
-	-	kIx~	-
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Miroslav	Miroslav	k1gMnSc1	Miroslav
Etzler	Etzler	k1gMnSc1	Etzler
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
-	-	kIx~	-
novinář	novinář	k1gMnSc1	novinář
Karel	Karel	k1gMnSc1	Karel
Fiala	Fiala	k1gMnSc1	Fiala
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
herec	herec	k1gMnSc1	herec
Ota	Ota	k1gMnSc1	Ota
Filip	Filip	k1gMnSc1	Filip
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
Emerich	Emerich	k1gMnSc1	Emerich
Gabzdyl	Gabzdyl	k1gInSc1	Gabzdyl
-	-	kIx~	-
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
choreograf	choreograf	k1gMnSc1	choreograf
Anna	Anna	k1gFnSc1	Anna
Friedlová-Kanczuská	Friedlová-Kanczuský	k2eAgFnSc1d1	Friedlová-Kanczuský
-	-	kIx~	-
architektka	architektka	k1gFnSc1	architektka
<g/>
,	,	kIx,	,
urbanistka	urbanistka	k1gFnSc1	urbanistka
Petr	Petr	k1gMnSc1	Petr
Hruška	Hruška	k1gMnSc1	Hruška
-	-	kIx~	-
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
Ilja	Ilja	k1gMnSc1	Ilja
Hurník	Hurník	k1gMnSc1	Hurník
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chlopčík	Chlopčík	k1gMnSc1	Chlopčík
-	-	kIx~	-
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
Igor	Igor	k1gMnSc1	Igor
Chmela	Chmel	k1gMnSc2	Chmel
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
Zábradlí	zábradlí	k1gNnSc4	zábradlí
Martin	Martin	k1gMnSc1	Martin
Chodúr	Chodúr	k1gMnSc1	Chodúr
-	-	kIx~	-
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Česko-Slovenské	českolovenský	k2eAgFnPc1d1	česko-slovenská
Superstar	superstar	k1gFnPc1	superstar
Věra	Věra	k1gFnSc1	Věra
Chytilová	Chytilová	k1gFnSc1	Chytilová
-	-	kIx~	-
režisérka	režisérka	k1gFnSc1	režisérka
Jožka	Jožka	k1gFnSc1	Jožka
Jabůrková	Jabůrková	k1gFnSc1	Jabůrková
-	-	kIx~	-
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Eva	Eva	k1gFnSc1	Eva
Jakoubková	Jakoubková	k1gFnSc1	Jakoubková
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Liana	Liana	k1gFnSc1	Liana
Janáčková	Janáčková	k1gFnSc1	Janáčková
-	-	kIx~	-
architektka	architektka	k1gFnSc1	architektka
<g/>
,	,	kIx,	,
starostka	starostka	k1gFnSc1	starostka
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
býv.	býv.	k?	býv.
senátorka	senátorka	k1gFnSc1	senátorka
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
-	-	kIx~	-
podnikatel	podnikatel	k1gMnSc1	podnikatel
Marek	Marek	k1gMnSc1	Marek
Jankulovski	Jankulovsk	k1gFnSc2	Jankulovsk
-	-	kIx~	-
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
s	s	k7c7	s
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Vladimír	Vladimír	k1gMnSc1	Vladimír
Javorský	Javorský	k1gMnSc1	Javorský
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Valentin	Valentin	k1gMnSc1	Valentin
Bernard	Bernard	k1gMnSc1	Bernard
Jestřábský	Jestřábský	k2eAgMnSc1d1	Jestřábský
-	-	kIx~	-
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
kazatel	kazatel	k1gMnSc1	kazatel
a	a	k8xC	a
barokní	barokní	k2eAgMnSc1d1	barokní
spisovatel	spisovatel	k1gMnSc1	spisovatel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jirotka	Jirotka	k1gFnSc1	Jirotka
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
Eva	Eva	k1gFnSc1	Eva
Jurinová	Jurinová	k1gFnSc1	Jurinová
-	-	kIx~	-
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kajnarová	Kajnarová	k1gFnSc1	Kajnarová
Říčařová	Říčařová	k1gFnSc1	Říčařová
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Nikolas	Nikolas	k1gInSc1	Nikolas
Kara	kara	k1gFnSc1	kara
-	-	kIx~	-
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
<g/>
iluzionista	iluzionista	k1gMnSc1	iluzionista
Naděžda	Naděžda	k1gFnSc1	Naděžda
Kniplová	Kniplová	k1gFnSc1	Kniplová
-	-	kIx~	-
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Martina	Martin	k1gMnSc2	Martin
<g />
.	.	kIx.	.
</s>
<s>
Kociánová	Kociánová	k1gFnSc1	Kociánová
-	-	kIx~	-
televizní	televizní	k2eAgFnSc1d1	televizní
moderátorka	moderátorka	k1gFnSc1	moderátorka
Přemysl	Přemysl	k1gMnSc1	Přemysl
Kokeš	Kokeš	k1gMnSc1	Kokeš
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
Irena	Irena	k1gFnSc1	Irena
Korbelářová	Korbelářová	k1gFnSc1	Korbelářová
-	-	kIx~	-
historička	historička	k1gFnSc1	historička
<g/>
,	,	kIx,	,
archivářka	archivářka	k1gFnSc1	archivářka
<g/>
,	,	kIx,	,
proděkanka	proděkanka	k1gFnSc1	proděkanka
<g/>
,	,	kIx,	,
profesorka	profesorka	k1gFnSc1	profesorka
Richard	Richard	k1gMnSc1	Richard
Krajčo	Krajčo	k1gMnSc1	Krajčo
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
-	-	kIx~	-
tenista	tenista	k1gMnSc1	tenista
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
Davis	Davis	k1gFnSc2	Davis
cupu	cup	k1gInSc2	cup
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
x	x	k?	x
vítěz	vítěz	k1gMnSc1	vítěz
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
x	x	k?	x
vítěz	vítěz	k1gMnSc1	vítěz
<g />
.	.	kIx.	.
</s>
<s>
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
vítěz	vítěz	k1gMnSc1	vítěz
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
Mario	Mario	k1gMnSc1	Mario
Lička	lička	k1gFnSc1	lička
-	-	kIx~	-
fotbalista	fotbalista	k1gMnSc1	fotbalista
Artur	Artur	k1gMnSc1	Artur
London	London	k1gMnSc1	London
-	-	kIx~	-
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Malý	Malý	k1gMnSc1	Malý
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Otmar	Otmar	k1gMnSc1	Otmar
Mácha	Mácha	k1gMnSc1	Mácha
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
dramaturg	dramaturg	k1gMnSc1	dramaturg
Karel	Karel	k1gMnSc1	Karel
Müller	Müller	k1gMnSc1	Müller
-	-	kIx~	-
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
heraldik	heraldik	k1gMnSc1	heraldik
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Zemského	zemský	k2eAgInSc2d1	zemský
archivu	archiv	k1gInSc2	archiv
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
Petr	Petr	k1gMnSc1	Petr
Němec	Němec	k1gMnSc1	Němec
-	-	kIx~	-
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g />
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
-	-	kIx~	-
písničkář	písničkář	k1gMnSc1	písničkář
Elly	Ella	k1gFnSc2	Ella
Oehlerová	Oehlerová	k1gFnSc1	Oehlerová
-	-	kIx~	-
architektka	architektka	k1gFnSc1	architektka
Marta	Marta	k1gFnSc1	Marta
Ondráčková	Ondráčková	k1gFnSc1	Ondráčková
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
Bohdan	Bohdana	k1gFnPc2	Bohdana
Pomahač	pomahač	k1gMnSc1	pomahač
-	-	kIx~	-
plastický	plastický	k2eAgMnSc1d1	plastický
chirurg	chirurg	k1gMnSc1	chirurg
Alexej	Alexej	k1gMnSc1	Alexej
Pyško	Pyško	k1gNnSc4	Pyško
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Artur	Artur	k1gMnSc1	Artur
Radvanský	Radvanský	k2eAgMnSc1d1	Radvanský
-	-	kIx~	-
pamětník	pamětník	k1gMnSc1	pamětník
holocaustu	holocaust	k1gInSc2	holocaust
Karel	Karel	k1gMnSc1	Karel
Reisz	Reisz	k1gMnSc1	Reisz
-	-	kIx~	-
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Marie	Maria	k1gFnSc2	Maria
Rottrová	Rottrová	k1gFnSc1	Rottrová
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Šlomo	Šloma	k1gFnSc5	Šloma
Rozen	rozen	k2eAgMnSc1d1	rozen
-	-	kIx~	-
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
-	-	kIx~	-
býv.	býv.	k?	býv.
předseda	předseda	k1gMnSc1	předseda
<g />
.	.	kIx.	.
</s>
<s>
vlády	vláda	k1gFnPc1	vláda
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
Jan	Jan	k1gMnSc1	Jan
Světlík	Světlík	k1gMnSc1	Světlík
-	-	kIx~	-
podnikatel	podnikatel	k1gMnSc1	podnikatel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šavrda	Šavrda	k1gMnSc1	Šavrda
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
disident	disident	k1gMnSc1	disident
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šindel	šindel	k1gInSc1	šindel
-	-	kIx~	-
hokejista	hokejista	k1gMnSc1	hokejista
-	-	kIx~	-
3	[number]	k4	3
<g/>
x	x	k?	x
Mistr	mistr	k1gMnSc1	mistr
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
Mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
s	s	k7c7	s
reprezentací	reprezentace	k1gFnSc7	reprezentace
ČSSR	ČSSR	kA	ČSSR
1985	[number]	k4	1985
Ladislav	Ladislav	k1gMnSc1	Ladislav
Špaček	Špaček	k1gMnSc1	Špaček
-	-	kIx~	-
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gMnSc1	mluvčí
prezidenta	prezident	k1gMnSc4	prezident
Havla	Havel	k1gMnSc4	Havel
<g/>
,	,	kIx,	,
popularizátor	popularizátor	k1gMnSc1	popularizátor
<g />
.	.	kIx.	.
</s>
<s>
společenské	společenský	k2eAgFnPc1d1	společenská
etikety	etiketa	k1gFnPc1	etiketa
Radim	Radim	k1gMnSc1	Radim
Špaček	Špaček	k1gMnSc1	Špaček
-	-	kIx~	-
režisér	režisér	k1gMnSc1	režisér
Teodor	Teodor	k1gMnSc1	Teodor
Šrubař	Šrubař	k1gMnSc1	Šrubař
-	-	kIx~	-
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
Oldřich	Oldřich	k1gMnSc1	Oldřich
Šuleř	Šuleř	k1gMnSc1	Šuleř
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
-	-	kIx~	-
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
býv.	býv.	k?	býv.
primátor	primátor	k1gMnSc1	primátor
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
hejtman	hejtman	k1gMnSc1	hejtman
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
Radim	Radim	k1gMnSc1	Radim
Uzel	uzel	k1gInSc1	uzel
-	-	kIx~	-
sexuolog	sexuolog	k1gMnSc1	sexuolog
Věra	Věra	k1gFnSc1	Věra
Weislitzová-Lustigová	Weislitzová-Lustigový	k2eAgFnSc1d1	Weislitzová-Lustigový
-	-	kIx~	-
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
býv.	býv.	k?	býv.
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
Vilém	Vilém	k1gMnSc1	Vilém
Závada	závada	k1gFnSc1	závada
-	-	kIx~	-
básník	básník	k1gMnSc1	básník
Rudolf	Rudolf	k1gMnSc1	Rudolf
Zuber	Zuber	k1gMnSc1	Zuber
-	-	kIx~	-
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
knihovník	knihovník	k1gMnSc1	knihovník
</s>
