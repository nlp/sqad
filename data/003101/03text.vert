<s>
Letní	letní	k2eAgInSc1d1	letní
spánek	spánek	k1gInSc1	spánek
neboli	neboli	k8xC	neboli
estivace	estivace	k1gFnSc1	estivace
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
zimnímu	zimní	k2eAgInSc3d1	zimní
spánku	spánek	k1gInSc3	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Živočichové	živočich	k1gMnPc1	živočich
i	i	k8xC	i
rostliny	rostlina	k1gFnPc1	rostlina
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
upadají	upadat	k5eAaPmIp3nP	upadat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
překonali	překonat	k5eAaPmAgMnP	překonat
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
horko	horko	k1gNnSc1	horko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
během	během	k7c2	během
hibernace	hibernace	k1gFnSc2	hibernace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
respirace	respirace	k1gFnSc1	respirace
<g/>
,	,	kIx,	,
šetří	šetřit	k5eAaImIp3nS	šetřit
se	se	k3xPyFc4	se
energetickými	energetický	k2eAgFnPc7d1	energetická
zásobami	zásoba	k1gFnPc7	zásoba
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
dehydrataci	dehydratace	k1gFnSc3	dehydratace
pomocí	pomoc	k1gFnPc2	pomoc
přerušovaného	přerušovaný	k2eAgNnSc2d1	přerušované
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
zvířata	zvíře	k1gNnPc4	zvíře
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
(	(	kIx(	(
<g/>
místy	místo	k1gNnPc7	místo
i	i	k9	i
subtropických	subtropický	k2eAgFnPc6d1	subtropická
<g/>
)	)	kIx)	)
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
hlemýždě	hlemýžď	k1gMnPc4	hlemýžď
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
plazi	plaz	k1gMnPc1	plaz
i	i	k8xC	i
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
pytlouši	pytlouš	k1gMnPc1	pytlouš
rodu	rod	k1gInSc2	rod
Perognathus	Perognathus	k1gInSc1	Perognathus
<g/>
,	,	kIx,	,
tarbíkomyš	tarbíkomyš	k1gInSc1	tarbíkomyš
či	či	k8xC	či
křečkové	křeček	k1gMnPc1	křeček
rodu	rod	k1gInSc2	rod
Baiomys	Baiomys	k1gInSc1	Baiomys
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
bahníci	bahník	k1gMnPc1	bahník
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgFnPc1d1	pouštní
žáby	žába	k1gFnPc1	žába
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
<g/>
,	,	kIx,	,
když	když	k8xS	když
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
voda	voda	k1gFnSc1	voda
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
si	se	k3xPyFc3	se
v	v	k7c6	v
měkkém	měkký	k2eAgNnSc6d1	měkké
bahně	bahno	k1gNnSc6	bahno
slizový	slizový	k2eAgInSc4d1	slizový
kokon	kokon	k1gInSc4	kokon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hluboce	hluboko	k6eAd1	hluboko
spí	spát	k5eAaImIp3nS	spát
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
paprsčitá	paprsčitý	k2eAgFnSc1d1	paprsčitá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
v	v	k7c6	v
období	období	k1gNnSc6	období
horka	horko	k1gNnSc2	horko
a	a	k8xC	a
sucha	sucho	k1gNnSc2	sucho
vyhloubí	vyhloubit	k5eAaPmIp3nS	vyhloubit
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
zaleze	zalézt	k5eAaPmIp3nS	zalézt
a	a	k8xC	a
spí	spát	k5eAaImIp3nS	spát
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
začne	začít	k5eAaPmIp3nS	začít
opět	opět	k6eAd1	opět
pršet	pršet	k5eAaImF	pršet
<g/>
.	.	kIx.	.
</s>
