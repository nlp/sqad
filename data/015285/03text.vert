<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blaník	Blaník	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
BlaníkIUCN	BlaníkIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c4
Velký	velký	k2eAgInSc4d1
BlaníkZákladní	BlaníkZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1981	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
366	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
40,31	40,31	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Louňovice	Louňovice	k1gFnSc1
pod	pod	k7c7
Blaníkem	Blaník	k1gInSc7
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
21,71	21,71	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
43,79	43,79	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blaník	Blaník	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
21	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.blanik.ochranaprirody.cz	www.blanik.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	to	k3xDgNnSc4
nejmenší	malý	k2eAgFnSc4d3
CHKO	CHKO	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
40,31	40,31	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
Velkého	velký	k2eAgInSc2d1
Blaníku	Blaník	k1gInSc2
(	(	kIx(
<g/>
638	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
je	být	k5eAaImIp3nS
tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
Blanice	Blanice	k1gFnSc2
v	v	k7c6
Ostrově	ostrov	k1gInSc6
(	(	kIx(
<g/>
366	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Vlašim	Vlašim	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
obec	obec	k1gFnSc1
v	v	k7c6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
Louňovice	Louňovice	k1gFnPc1
pod	pod	k7c7
Blaníkem	Blaník	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
sídlí	sídlet	k5eAaImIp3nS
správa	správa	k1gFnSc1
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
řízena	řídit	k5eAaImNgFnS
prostřednictvím	prostřednictvím	k7c2
regionálního	regionální	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
Střední	střední	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důvod	důvod	k1gInSc1
zřízení	zřízení	k1gNnSc2
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
</s>
<s>
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
zachovalého	zachovalý	k2eAgNnSc2d1
jak	jak	k8xS,k8xC
přírodního	přírodní	k2eAgNnSc2d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Celé	celá	k1gFnPc1
CHKO	CHKO	kA
dominují	dominovat	k5eAaImIp3nP
vrcholy	vrchol	k1gInPc1
Velkého	velký	k2eAgInSc2d1
Blaníku	Blaník	k1gInSc2
(	(	kIx(
<g/>
638	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Malého	malé	k1gNnSc2
Blaníku	Blaník	k1gInSc2
(	(	kIx(
<g/>
580	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
významnými	významný	k2eAgInPc7d1
vrcholy	vrchol	k1gInPc7
jsou	být	k5eAaImIp3nP
Křížovská	Křížovský	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
(	(	kIx(
<g/>
580	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Býkovická	Býkovický	k2eAgFnSc1d1
hůrka	hůrka	k1gFnSc1
(	(	kIx(
<g/>
562	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
NPR	NPR	kA
Ve	v	k7c6
Studeném	studený	k2eAgInSc6d1
</s>
<s>
NPR	NPR	kA
Voděradské	Voděradský	k2eAgFnSc2d1
bučiny	bučina	k1gFnSc2
</s>
<s>
PP	PP	kA
Částrovické	Částrovický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
(	(	kIx(
<g/>
3,56	3,56	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
Malý	Malý	k1gMnSc1
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
12,71	12,71	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
Podlesí	podlesí	k1gNnPc1
(	(	kIx(
<g/>
9,89	9,89	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
PP	PP	kA
Rybník	rybník	k1gInSc1
Louňov	Louňov	k1gInSc1
(	(	kIx(
<g/>
1,88	1,88	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
Velký	velký	k2eAgInSc1d1
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
84,68	84,68	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
spravuje	spravovat	k5eAaImIp3nS
i	i	k9
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc4,k3yQgNnPc4,k3yRgNnPc4
leží	ležet	k5eAaImIp3nS
mimo	mimo	k7c4
CHKO	CHKO	kA
<g/>
:	:	kIx,
</s>
<s>
NPR	NPR	kA
Drbákov	Drbákov	k1gInSc1
-	-	kIx~
Albertovy	Albertův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
</s>
<s>
NPP	NPP	kA
Hadce	hadec	k1gInPc1
u	u	k7c2
Želivky	Želivka	k1gFnSc2
</s>
<s>
NPP	NPP	kA
Chýnovská	Chýnovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Jankovský	Jankovský	k2eAgInSc4d1
potok	potok	k1gInSc4
</s>
<s>
NPP	NPP	kA
Kaňk	Kaňk	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Luční	luční	k2eAgFnSc7d1
</s>
<s>
NPP	NPP	kA
Medník	medník	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Rybníček	rybníček	k1gInSc1
u	u	k7c2
Hořan	Hořana	k1gFnPc2
</s>
<s>
NPP	NPP	kA
Stročov	Stročov	k1gInSc1
</s>
<s>
NPR	NPR	kA
Ve	v	k7c6
Studeném	studený	k2eAgInSc6d1
</s>
<s>
NPR	NPR	kA
Voděradské	Voděradský	k2eAgFnSc2d1
bučiny	bučina	k1gFnSc2
</s>
<s>
Lesy	les	k1gInPc1
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
říční	říční	k2eAgFnSc2d1
</s>
<s>
Lesy	les	k1gInPc1
pokrývají	pokrývat	k5eAaImIp3nP
asi	asi	k9
31	#num#	k4
<g/>
%	%	kIx~
území	území	k1gNnSc2
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc7d3
dřevinou	dřevina	k1gFnSc7
je	být	k5eAaImIp3nS
smrk	smrk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
57	#num#	k4
<g/>
%	%	kIx~
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
následuje	následovat	k5eAaImIp3nS
borovice	borovice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
buk	buk	k1gInSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
modřín	modřín	k1gInSc1
(	(	kIx(
<g/>
6,5	6,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
například	například	k6eAd1
bříza	bříza	k1gFnSc1
(	(	kIx(
<g/>
2,7	2,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dub	dub	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
habr	habr	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
jedle	jedle	k1gFnPc1
(	(	kIx(
<g/>
0,8	0,8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Živočichové	živočich	k1gMnPc1
</s>
<s>
Ze	z	k7c2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
kterých	který	k3yRgMnPc2,k3yIgMnPc2,k3yQgMnPc2
v	v	k7c6
CHKO	CHKO	kA
žije	žít	k5eAaImIp3nS
asi	asi	k9
čtyřicet	čtyřicet	k4xCc4
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
vzácný	vzácný	k2eAgMnSc1d1
netopýr	netopýr	k1gMnSc1
černý	černý	k1gMnSc1
a	a	k8xC
netopýr	netopýr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptáků	pták	k1gMnPc2
například	například	k6eAd1
křepelka	křepelka	k1gFnSc1
polní	polní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ledňáček	ledňáček	k1gMnSc1
říční	říční	k2eAgMnSc1d1
či	či	k8xC
moták	moták	k1gMnSc1
pochop	pochop	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plazů	plaz	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
například	například	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
zmije	zmije	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
a	a	k8xC
z	z	k7c2
obojživelníků	obojživelník	k1gMnPc2
čolek	čolek	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
kuňka	kuňka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
rosnička	rosnička	k1gFnSc1
zelená	zelená	k1gFnSc1
<g/>
,	,	kIx,
blatnice	blatnice	k1gFnSc1
skvrnitá	skvrnitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
či	či	k8xC
skokan	skokan	k1gMnSc1
ostronohý	ostronohý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
brouků	brouk	k1gMnPc2
například	například	k6eAd1
mandelinka	mandelinka	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
střevlík	střevlík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
vážky	vážka	k1gFnSc2
a	a	k8xC
také	také	k9
asi	asi	k9
200	#num#	k4
druhů	druh	k1gInPc2
pavouků	pavouk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vodních	vodní	k2eAgMnPc2d1
plžů	plž	k1gMnPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
zmínku	zmínka	k1gFnSc4
například	například	k6eAd1
velevrubové	velevrub	k1gMnPc1
<g/>
,	,	kIx,
kružníci	kružník	k1gMnPc1
severní	severní	k2eAgMnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
červené	červený	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
hrachovka	hrachovka	k1gFnSc1
obrácená	obrácený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rostlinstvo	rostlinstvo	k1gNnSc1
</s>
<s>
Lilie	lilie	k1gFnSc1
zlatohlávek	zlatohlávka	k1gFnPc2
</s>
<s>
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
je	být	k5eAaImIp3nS
bohatá	bohatý	k2eAgFnSc1d1
na	na	k7c4
rostliny	rostlina	k1gFnPc4
z	z	k7c2
vodního	vodní	k2eAgNnSc2d1
a	a	k8xC
bažinatého	bažinatý	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
lučního	luční	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
například	například	k6eAd1
ostřice	ostřice	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
ptačinec	ptačinec	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
prstnatec	prstnatec	k1gMnSc1
májový	májový	k2eAgMnSc1d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
také	také	k9
například	například	k6eAd1
lilie	lilie	k1gFnSc1
zlatohlávek	zlatohlávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Na	na	k7c6
vrcholcích	vrcholek	k1gInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
převážně	převážně	k6eAd1
ortoruly	ortorul	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
pararuly	pararula	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
například	například	k6eAd1
aplit	aplit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
na	na	k7c6
seznamu	seznam	k1gInSc6
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
AOPK	AOPK	kA
ČR	ČR	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Benešov	Benešov	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blaník	Blaník	k1gInSc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Profil	profil	k1gInSc1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
projektu	projekt	k1gInSc2
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Beran	Beran	k1gMnSc1
L.	L.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příspěvek	příspěvek	k1gInSc1
k	k	k7c3
poznání	poznání	k1gNnSc3
vodních	vodní	k2eAgMnPc2d1
měkkýšů	měkkýš	k1gMnPc2
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc4
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malacologica	Malacologica	k1gFnSc1
Bohemoslovaca	Bohemoslovaca	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
46	#num#	k4
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Benešov	Benešov	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Džbány-Žebrák	Džbány-Žebrák	k1gInSc1
•	•	k?
Hornopožárský	Hornopožárský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Jistebnická	jistebnický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Střed	střed	k1gInSc1
Čech	Čechy	k1gFnPc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blaník	Blaník	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Ve	v	k7c6
Studeném	Studený	k1gMnSc6
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Hadce	hadec	k1gInPc1
u	u	k7c2
Želivky	Želivka	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čížov	Čížov	k1gInSc1
•	•	k?
Grybla	Grybla	k1gFnSc2
•	•	k?
Malý	malý	k2eAgInSc1d1
Blaník	Blaník	k1gInSc1
•	•	k?
Podhrázský	podhrázský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Podlesí	Podlesí	k1gNnSc2
•	•	k?
Posázavské	posázavský	k2eAgFnSc2d1
bučiny	bučina	k1gFnSc2
•	•	k?
Štěpánovský	Štěpánovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Blaník	Blaník	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Částrovické	Částrovický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Hadce	hadec	k1gInSc2
u	u	k7c2
Hrnčíř	Hrnčíř	k1gMnSc1
•	•	k?
Kališťské	Kališťský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
a	a	k8xC
mokřady	mokřad	k1gInPc4
•	•	k?
Křečovický	Křečovický	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Les	les	k1gInSc1
u	u	k7c2
Libeže	Libež	k1gFnSc2
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Budenína	Budenín	k1gInSc2
•	•	k?
Minartice	Minartice	k1gFnSc2
•	•	k?
Na	na	k7c6
ostrově	ostrov	k1gInSc6
•	•	k?
Na	na	k7c6
Stříbrné	stříbrná	k1gFnSc6
•	•	k?
Roudný	Roudný	k2eAgInSc1d1
•	•	k?
Rybníček	rybníček	k1gInSc1
u	u	k7c2
Studeného	Studený	k1gMnSc2
•	•	k?
Rybník	rybník	k1gInSc1
Louňov	Louňov	k1gInSc1
•	•	k?
Řísnice	Řísnice	k1gFnSc2
•	•	k?
Slavkov	Slavkov	k1gInSc1
•	•	k?
Suchdolský	Suchdolský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Štola	štola	k1gFnSc1
Mořic	Mořic	k1gMnSc1
•	•	k?
Teletínský	Teletínský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Týnecká	týnecký	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
•	•	k?
V	v	k7c6
olších	olše	k1gFnPc6
•	•	k?
Vlčí	vlčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Vlčkovice	Vlčkovice	k1gFnSc2
–	–	k?
Dubský	dubský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
–	–	k?
kostel	kostel	k1gInSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
