<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	to	k3xDgNnSc4
nejmenší	malý	k2eAgFnSc4d3
CHKO	CHKO	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>