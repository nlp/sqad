<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
tekutá	tekutý	k2eAgFnSc1d1	tekutá
složka	složka	k1gFnSc1	složka
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
nejobjemnější	objemný	k2eAgFnSc1d3	nejobjemnější
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
má	mít	k5eAaImIp3nS	mít
jantarově	jantarově	k6eAd1	jantarově
nažloutlou	nažloutlý	k2eAgFnSc4d1	nažloutlá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3,5	[number]	k4	3,5
litrů	litr	k1gInPc2	litr
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
bílkoviny	bílkovina	k1gFnSc2	bílkovina
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
hormony	hormon	k1gInPc1	hormon
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
<g/>
,	,	kIx,	,
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
glukóza	glukóza	k1gFnSc1	glukóza
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
anorganické	anorganický	k2eAgFnPc1d1	anorganická
soli	sůl	k1gFnPc1	sůl
(	(	kIx(	(
<g/>
0,9	[number]	k4	0,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
roztok	roztok	k1gInSc1	roztok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
médium	médium	k1gNnSc1	médium
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
lipidů	lipid	k1gInPc2	lipid
<g/>
,	,	kIx,	,
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
metabolických	metabolický	k2eAgInPc2d1	metabolický
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Schopnost	schopnost	k1gFnSc1	schopnost
plazmy	plazma	k1gFnSc2	plazma
přenášet	přenášet	k5eAaImF	přenášet
kyslík	kyslík	k1gInSc4	kyslík
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Krevní	krevní	k2eAgNnSc1d1	krevní
plazma	plazma	k1gNnSc1	plazma
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
regulátorem	regulátor	k1gInSc7	regulátor
acidobazické	acidobazický	k2eAgFnSc2d1	acidobazická
a	a	k8xC	a
osmotické	osmotický	k2eAgFnSc2d1	osmotická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
látky	látka	k1gFnSc2	látka
podporující	podporující	k2eAgNnSc1d1	podporující
srážení	srážení	k1gNnSc1	srážení
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
proteinů	protein	k1gInPc2	protein
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
osmotického	osmotický	k2eAgInSc2d1	osmotický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
obsahu	obsah	k1gInSc2	obsah
bílkovin	bílkovina	k1gFnPc2	bílkovina
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
těžké	těžký	k2eAgFnSc6d1	těžká
podvýživě	podvýživa	k1gFnSc6	podvýživa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
z	z	k7c2	z
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
dostává	dostávat	k5eAaImIp3nS	dostávat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
tekutiny	tekutina	k1gFnSc2	tekutina
a	a	k8xC	a
důsledkem	důsledek	k1gInSc7	důsledek
jsou	být	k5eAaImIp3nP	být
otoky	otok	k1gInPc1	otok
<g/>
.	.	kIx.	.
</s>
<s>
Odběr	odběr	k1gInSc1	odběr
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmaferéza	plazmaferéza	k1gFnSc1	plazmaferéza
<g/>
.	.	kIx.	.
</s>
<s>
Plazmatické	plazmatický	k2eAgInPc4d1	plazmatický
proteiny	protein	k1gInPc4	protein
Krevní	krevní	k2eAgInPc4d1	krevní
sérum	sérum	k1gNnSc4	sérum
</s>
