<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
je	být	k5eAaImIp3nS	být
západoslovanský	západoslovanský	k2eAgInSc4d1	západoslovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
češtině	čeština	k1gFnSc6	čeština
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
historické	historický	k2eAgFnSc2d1	historická
příbuznosti	příbuznost	k1gFnSc2	příbuznost
i	i	k8xC	i
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
zejména	zejména	k9	zejména
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
společného	společný	k2eAgNnSc2d1	společné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
existoval	existovat	k5eAaImAgInS	existovat
pasivní	pasivní	k2eAgInSc1d1	pasivní
bilingvismus	bilingvismus	k1gInSc1	bilingvismus
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
podobách	podoba	k1gFnPc6	podoba
jednoho	jeden	k4xCgInSc2	jeden
československého	československý	k2eAgInSc2d1	československý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
slovenštinou	slovenština	k1gFnSc7	slovenština
a	a	k8xC	a
češtinou	čeština	k1gFnSc7	čeština
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
má	mít	k5eAaImIp3nS	mít
jednodušší	jednoduchý	k2eAgInSc4d2	jednodušší
(	(	kIx(	(
<g/>
pravidelnější	pravidelní	k2eAgInSc4d2	pravidelní
<g/>
)	)	kIx)	)
pravopis	pravopis	k1gInSc4	pravopis
i	i	k8xC	i
gramatiku	gramatika	k1gFnSc4	gramatika
než	než	k8xS	než
čeština	čeština	k1gFnSc1	čeština
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
se	se	k3xPyFc4	se
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
rody	rod	k1gInPc1	rod
u	u	k7c2	u
tvarů	tvar	k1gInPc2	tvar
sloves	sloveso	k1gNnPc2	sloveso
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
dievčatá	dievčatat	k5eAaPmIp3nS	dievčatat
boli	boli	k6eAd1	boli
<g/>
)	)	kIx)	)
a	a	k8xC	a
přechodníků	přechodník	k1gInPc2	přechodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
==	==	k?	==
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgInSc7d1	státní
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Státním	státní	k2eAgInSc7d1	státní
jazykem	jazyk	k1gInSc7	jazyk
Slovenska	Slovensko	k1gNnSc2	Slovensko
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
platí	platit	k5eAaImIp3nS	platit
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
staví	stavit	k5eAaPmIp3nS	stavit
slovenštinu	slovenština	k1gFnSc4	slovenština
nad	nad	k7c4	nad
ostatní	ostatní	k2eAgInPc4d1	ostatní
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgNnP	být
dotčena	dotčen	k2eAgNnPc1d1	dotčeno
práva	právo	k1gNnPc1	právo
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
chrání	chránit	k5eAaImIp3nS	chránit
slovenský	slovenský	k2eAgInSc1d1	slovenský
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
občanům	občan	k1gMnPc3	občan
právo	právo	k1gNnSc4	právo
domluvit	domluvit	k5eAaPmF	domluvit
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
osvojit	osvojit	k5eAaPmF	osvojit
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
slovem	slovem	k6eAd1	slovem
i	i	k9	i
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Upravuje	upravovat	k5eAaImIp3nS	upravovat
používání	používání	k1gNnSc1	používání
slovenštiny	slovenština	k1gFnSc2	slovenština
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
a	a	k8xC	a
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
při	při	k7c6	při
poskytování	poskytování	k1gNnSc6	poskytování
informací	informace	k1gFnPc2	informace
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nP	řešit
rovněž	rovněž	k9	rovněž
závaznost	závaznost	k1gFnSc4	závaznost
kodifikované	kodifikovaný	k2eAgFnSc2d1	kodifikovaná
podoby	podoba	k1gFnSc2	podoba
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dvojjazyčné	dvojjazyčný	k2eAgInPc4d1	dvojjazyčný
nápisy	nápis	k1gInPc4	nápis
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
větším	veliký	k2eAgNnSc7d2	veliký
písmem	písmo	k1gNnSc7	písmo
než	než	k8xS	než
nápis	nápis	k1gInSc1	nápis
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kodifikace	kodifikace	k1gFnSc2	kodifikace
===	===	k?	===
</s>
</p>
<p>
<s>
Kodifikovanou	kodifikovaný	k2eAgFnSc4d1	kodifikovaná
formu	forma	k1gFnSc4	forma
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovenštiny	slovenština	k1gFnSc2	slovenština
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
jazyce	jazyk	k1gInSc6	jazyk
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jazykoví	jazykový	k2eAgMnPc1d1	jazykový
odborníci	odborník	k1gMnPc1	odborník
(	(	kIx(	(
<g/>
slovakisté	slovakista	k1gMnPc1	slovakista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vědce	vědec	k1gMnPc4	vědec
z	z	k7c2	z
Jazykovědného	jazykovědný	k2eAgInSc2d1	jazykovědný
ústavu	ústav	k1gInSc2	ústav
Ľudovíta	Ľudovíta	k1gMnSc1	Ľudovíta
Štúra	Štúr	k1gMnSc2	Štúr
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Kodifikační	kodifikační	k2eAgFnSc2d1	kodifikační
příručky	příručka	k1gFnSc2	příručka
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
formou	forma	k1gFnSc7	forma
opatření	opatření	k1gNnSc1	opatření
je	on	k3xPp3gFnPc4	on
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
a	a	k8xC	a
uveřejňuje	uveřejňovat	k5eAaImIp3nS	uveřejňovat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Závaznými	závazný	k2eAgFnPc7d1	závazná
kodifikačními	kodifikační	k2eAgFnPc7d1	kodifikační
normami	norma	k1gFnPc7	norma
jsou	být	k5eAaImIp3nP	být
Pravidla	pravidlo	k1gNnPc1	pravidlo
slovenské	slovenský	k2eAgFnSc2d1	slovenská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
Pravidla	pravidlo	k1gNnPc4	pravidlo
slovenského	slovenský	k2eAgInSc2d1	slovenský
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
Krátký	krátký	k2eAgInSc1d1	krátký
slovník	slovník	k1gInSc1	slovník
slovenského	slovenský	k2eAgInSc2d1	slovenský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
Morfologie	morfologie	k1gFnSc2	morfologie
slovenského	slovenský	k2eAgInSc2d1	slovenský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kodifikovaná	kodifikovaný	k2eAgFnSc1d1	kodifikovaná
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
závazná	závazný	k2eAgFnSc1d1	závazná
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Závaznost	závaznost	k1gFnSc1	závaznost
používání	používání	k1gNnSc2	používání
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovenštiny	slovenština	k1gFnSc2	slovenština
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jak	jak	k6eAd1	jak
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
při	při	k7c6	při
označování	označování	k1gNnSc6	označování
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
při	při	k7c6	při
prodeji	prodej	k1gFnSc6	prodej
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postavení	postavení	k1gNnSc1	postavení
slovenštiny	slovenština	k1gFnSc2	slovenština
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přetrvávající	přetrvávající	k2eAgFnSc7d1	přetrvávající
širokou	široký	k2eAgFnSc7d1	široká
srozumitelností	srozumitelnost	k1gFnSc7	srozumitelnost
slovenštiny	slovenština	k1gFnSc2	slovenština
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
český	český	k2eAgInSc1d1	český
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
užívat	užívat	k5eAaImF	užívat
slovenštinu	slovenština	k1gFnSc4	slovenština
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
menšinových	menšinový	k2eAgInPc2d1	menšinový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
překladatele	překladatel	k1gMnSc2	překladatel
či	či	k8xC	či
tlumočníka	tlumočník	k1gMnSc2	tlumočník
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
právních	právní	k2eAgInPc2d1	právní
a	a	k8xC	a
úředních	úřední	k2eAgInPc2d1	úřední
úkonů	úkon	k1gInPc2	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mnoha	mnoho	k4c2	mnoho
aspektů	aspekt	k1gInPc2	aspekt
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Správní	správní	k2eAgInSc1d1	správní
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
500	[number]	k4	500
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
písemnosti	písemnost	k1gFnPc4	písemnost
se	se	k3xPyFc4	se
vyhotovují	vyhotovovat	k5eAaImIp3nP	vyhotovovat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
řízení	řízení	k1gNnSc2	řízení
mohou	moct	k5eAaImIp3nP	moct
jednat	jednat	k5eAaImF	jednat
a	a	k8xC	a
písemnosti	písemnost	k1gFnPc1	písemnost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předkládány	předkládat	k5eAaImNgInP	předkládat
i	i	k9	i
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
slovenském	slovenský	k2eAgInSc6d1	slovenský
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
§	§	k?	§
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
odstavec	odstavec	k1gInSc1	odstavec
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
správě	správa	k1gFnSc6	správa
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
(	(	kIx(	(
<g/>
337	[number]	k4	337
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
<g/>
:	:	kIx,	:
Před	před	k7c7	před
správcem	správce	k1gMnSc7	správce
daně	daň	k1gFnSc2	daň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
českém	český	k2eAgInSc6d1	český
nebo	nebo	k8xC	nebo
slovenském	slovenský	k2eAgInSc6d1	slovenský
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgNnPc1	veškerý
písemná	písemný	k2eAgNnPc1d1	písemné
podání	podání	k1gNnPc1	podání
se	se	k3xPyFc4	se
předkládají	předkládat	k5eAaImIp3nP	předkládat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nebo	nebo	k8xC	nebo
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
§	§	k?	§
3	[number]	k4	3
<g/>
,	,	kIx,	,
odstavec	odstavec	k1gInSc1	odstavec
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
správě	správa	k1gFnSc6	správa
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
daňovým	daňový	k2eAgInSc7d1	daňový
řádem	řád	k1gInSc7	řád
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
280	[number]	k4	280
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
§	§	k?	§
76	[number]	k4	76
daňového	daňový	k2eAgInSc2d1	daňový
řádu	řád	k1gInSc2	řád
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
daní	daň	k1gFnPc2	daň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
písemnosti	písemnost	k1gFnPc4	písemnost
se	se	k3xPyFc4	se
vyhotovují	vyhotovovat	k5eAaImIp3nP	vyhotovovat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
tak	tak	k6eAd1	tak
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
ztratila	ztratit	k5eAaPmAgFnS	ztratit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
správy	správa	k1gFnSc2	správa
daní	danit	k5eAaImIp3nS	danit
své	svůj	k3xOyFgNnSc4	svůj
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
literatura	literatura	k1gFnSc1	literatura
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
konce	konec	k1gInSc2	konec
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
literatury	literatura	k1gFnSc2	literatura
podle	podle	k7c2	podle
období	období	k1gNnSc2	období
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Starší	starý	k2eAgFnSc1d2	starší
slovenská	slovenský	k2eAgFnSc1d1	slovenská
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Humanistická	humanistický	k2eAgFnSc1d1	humanistická
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc1d1	renesanční
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Novější	nový	k2eAgFnSc1d2	novější
slovenská	slovenský	k2eAgFnSc1d1	slovenská
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
1780	[number]	k4	1780
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
a	a	k8xC	a
preromantizmus	preromantizmus	k1gInSc1	preromantizmus
(	(	kIx(	(
<g/>
1780	[number]	k4	1780
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Od	od	k7c2	od
romantismu	romantismus	k1gInSc2	romantismus
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
realismu	realismus	k1gInSc2	realismus
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
literární	literární	k2eAgFnSc1d1	literární
moderna	moderna	k1gFnSc1	moderna
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Meziválečná	meziválečný	k2eAgFnSc1d1	meziválečná
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Písmo	písmo	k1gNnSc1	písmo
a	a	k8xC	a
abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
upravenou	upravený	k2eAgFnSc7d1	upravená
latinkou	latinka	k1gFnSc7	latinka
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
přidanými	přidaný	k2eAgInPc7d1	přidaný
znaky	znak	k1gInPc7	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
á	á	k0	á
ä	ä	k?	ä
č	č	k0	č
ď	ď	k?	ď
dz	dz	k?	dz
dž	dž	k?	dž
é	é	k0	é
ch	ch	k0	ch
í	í	k0	í
ĺ	ĺ	k?	ĺ
ľ	ľ	k?	ľ
ň	ň	k?	ň
ó	ó	k0	ó
ô	ô	k?	ô
ŕ	ŕ	k?	ŕ
š	š	k?	š
ť	ť	k?	ť
ú	ú	k0	ú
ý	ý	k?	ý
ž	ž	k?	ž
</s>
</p>
<p>
<s>
===	===	k?	===
Výslovnost	výslovnost	k1gFnSc1	výslovnost
===	===	k?	===
</s>
</p>
<p>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
následující	následující	k2eAgNnPc4d1	následující
písmena	písmeno	k1gNnPc4	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ä	ä	k?	ä
–	–	k?	–
v	v	k7c6	v
neutrálním	neutrální	k2eAgInSc6d1	neutrální
stylu	styl	k1gInSc6	styl
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovenštiny	slovenština	k1gFnSc2	slovenština
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
e	e	k0	e
(	(	kIx(	(
<g/>
päť	päť	k?	päť
<g/>
,	,	kIx,	,
pamäť	pamäť	k1gFnSc1	pamäť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ľ	ľ	k?	ľ
–	–	k?	–
měkké	měkký	k2eAgNnSc1d1	měkké
l	l	kA	l
(	(	kIx(	(
<g/>
ľahký	ľahký	k2eAgInSc4d1	ľahký
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ĺ	ĺ	k?	ĺ
–	–	k?	–
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
slabikotvorné	slabikotvorný	k2eAgFnSc2d1	slabikotvorný
l	l	kA	l
(	(	kIx(	(
<g/>
dĺžka	dĺžka	k1gMnSc1	dĺžka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ŕ	ŕ	k?	ŕ
–	–	k?	–
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
slabikotvorné	slabikotvorný	k2eAgFnSc2d1	slabikotvorný
r	r	kA	r
(	(	kIx(	(
<g/>
vŕba	vŕba	k1gMnSc1	vŕba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ô	ô	k?	ô
–	–	k?	–
uo	uo	k?	uo
(	(	kIx(	(
<g/>
hôrka	hôrka	k1gMnSc1	hôrka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Dvouhlásky	Dvouhláska	k1gFnSc2	Dvouhláska
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
dvouhlásky	dvouhláska	k1gFnPc1	dvouhláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
slabiky	slabika	k1gFnPc1	slabika
a	a	k8xC	a
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
se	se	k3xPyFc4	se
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ia	ia	k0	ia
–	–	k?	–
ja	ja	k?	ja
(	(	kIx(	(
<g/>
priadza	priadza	k1gFnSc1	priadza
/	/	kIx~	/
příze	příze	k1gFnSc1	příze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ie	ie	k?	ie
–	–	k?	–
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
miera	miera	k1gFnSc1	miera
/	/	kIx~	/
míra	míra	k1gFnSc1	míra
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iu	iu	k?	iu
–	–	k?	–
ju	ju	k0	ju
(	(	kIx(	(
<g/>
vyššiu	vyššius	k1gMnSc3	vyššius
/	/	kIx~	/
vyšší	vysoký	k2eAgFnSc3d2	vyšší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Platí	platit	k5eAaImIp3nS	platit
tady	tady	k6eAd1	tady
také	také	k6eAd1	také
pravidlo	pravidlo	k1gNnSc1	pravidlo
tzv.	tzv.	kA	tzv.
rytmického	rytmický	k2eAgNnSc2d1	rytmické
krácení	krácení	k1gNnSc2	krácení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
systémově	systémově	k6eAd1	systémově
následovat	následovat	k5eAaImF	následovat
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
slabiky	slabika	k1gFnPc4	slabika
(	(	kIx(	(
<g/>
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
samohláskou	samohláska	k1gFnSc7	samohláska
nebo	nebo	k8xC	nebo
s	s	k7c7	s
dvouhláskou	dvouhláska	k1gFnSc7	dvouhláska
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
slabika	slabika	k1gFnSc1	slabika
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
vo-lám	voat	k5eAaPmIp1nS	vo-lat
<g/>
,	,	kIx,	,
ve-šiam	ve-šiam	k6eAd1	ve-šiam
(	(	kIx(	(
<g/>
věším	věšet	k5eAaImIp1nS	věšet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spá-vam	spáam	k6eAd1	spá-vam
(	(	kIx(	(
<g/>
spávám	spávat	k5eAaImIp1nS	spávat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spie-vam	spieam	k1gInSc1	spie-vam
(	(	kIx(	(
<g/>
zpívám	zpívat	k5eAaImIp1nS	zpívat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
koncovky	koncovka	k1gFnSc2	koncovka
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
líščí	líšč	k1gFnPc2	líšč
<g/>
,	,	kIx,	,
líščia	líščium	k1gNnPc1	líščium
<g/>
,	,	kIx,	,
býčí	býčí	k2eAgNnPc1d1	býčí
<g/>
,	,	kIx,	,
páví	páví	k2eAgNnPc1d1	páví
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
na	na	k7c6	na
-ie	e	k?	-ie
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádě	pád	k1gInSc6	pád
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
vzor	vzor	k1gInSc1	vzor
"	"	kIx"	"
<g/>
vysvedčenie	vysvedčenie	k1gFnSc1	vysvedčenie
<g/>
"	"	kIx"	"
-	-	kIx~	-
lístie	lístie	k1gFnSc2	lístie
<g/>
,	,	kIx,	,
lístiu	lístium	k1gNnSc6	lístium
<g/>
,	,	kIx,	,
lístím	lístit	k5eAaPmIp1nS	lístit
<g/>
,	,	kIx,	,
dianie	dianie	k1gFnSc1	dianie
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nářečí	nářečí	k1gNnSc2	nářečí
==	==	k?	==
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
je	být	k5eAaImIp3nS	být
nářečně	nářečně	k6eAd1	nářečně
více	hodně	k6eAd2	hodně
diferenciovaná	diferenciovaný	k2eAgFnSc1d1	diferenciovaná
než	než	k8xS	než
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Hrubě	hrubě	k6eAd1	hrubě
lze	lze	k6eAd1	lze
vydělit	vydělit	k5eAaPmF	vydělit
nářečí	nářečí	k1gNnSc4	nářečí
západoslovenská	západoslovenský	k2eAgFnSc1d1	Západoslovenská
(	(	kIx(	(
<g/>
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
východomoravským	východomoravský	k2eAgInPc3d1	východomoravský
dialektům	dialekt	k1gInPc3	dialekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
středoslovenská	středoslovenský	k2eAgFnSc1d1	Středoslovenská
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
nejblíže	blízce	k6eAd3	blízce
spisovné	spisovný	k2eAgFnSc6d1	spisovná
podobě	podoba	k1gFnSc6	podoba
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
znaků	znak	k1gInPc2	znak
společných	společný	k2eAgInPc2d1	společný
s	s	k7c7	s
jihoslovanskými	jihoslovanský	k2eAgInPc7d1	jihoslovanský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
východoslovenská	východoslovenský	k2eAgFnSc1d1	Východoslovenská
(	(	kIx(	(
<g/>
mající	mající	k2eAgInPc1d1	mající
některé	některý	k3yIgInPc1	některý
rysy	rys	k1gInPc1	rys
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
polštinou	polština	k1gFnSc7	polština
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
fonologické	fonologický	k2eAgFnSc2d1	fonologická
délky	délka	k1gFnSc2	délka
či	či	k8xC	či
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
penultimě	penultima	k1gFnSc6	penultima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
slovenského	slovenský	k2eAgNnSc2d1	slovenské
jazykového	jazykový	k2eAgNnSc2d1	jazykové
území	území	k1gNnSc2	území
–	–	k?	–
na	na	k7c6	na
Spiši	Spiš	k1gFnSc6	Spiš
<g/>
,	,	kIx,	,
Oravě	Orava	k1gFnSc6	Orava
a	a	k8xC	a
Horních	horní	k2eAgFnPc6d1	horní
Kysucích	Kysuce	k1gFnPc6	Kysuce
–	–	k?	–
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tzv.	tzv.	kA	tzv.
goralská	goralský	k2eAgNnPc4d1	goralské
nářečí	nářečí	k1gNnSc4	nářečí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
dialekty	dialekt	k1gInPc4	dialekt
blízké	blízký	k2eAgFnSc3d1	blízká
polštině	polština	k1gFnSc3	polština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
nářečí	nářečí	k1gNnSc1	nářečí
od	od	k7c2	od
českých	český	k2eAgNnPc2d1	české
nářečí	nářečí	k1gNnPc2	nářečí
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
používání	používání	k1gNnSc6	používání
následujících	následující	k2eAgFnPc2d1	následující
hlásek	hláska	k1gFnPc2	hláska
<g/>
:	:	kIx,	:
dz	dz	k?	dz
dž	dž	k?	dž
ä	ä	k?	ä
ŕ	ŕ	k?	ŕ
ĺ	ĺ	k?	ĺ
ľ	ľ	k?	ľ
a	a	k8xC	a
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
ia	ia	k0	ia
ie	ie	k?	ie
iu	iu	k?	iu
ô	ô	k?	ô
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
nářečích	nářečí	k1gNnPc6	nářečí
nepoužívá	používat	k5eNaImIp3nS	používat
a	a	k8xC	a
-	-	kIx~	-
ř	ř	k?	ř
-	-	kIx~	-
ě	ě	k?	ě
ů	ů	k?	ů
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
slovenských	slovenský	k2eAgNnPc6d1	slovenské
nářečích	nářečí	k1gNnPc6	nářečí
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
</s>
</p>
<p>
<s>
ukázka	ukázka	k1gFnSc1	ukázka
výslovnosti	výslovnost	k1gFnSc2	výslovnost
slova	slovo	k1gNnSc2	slovo
NE	ne	k9	ne
v	v	k7c6	v
slovenských	slovenský	k2eAgNnPc6d1	slovenské
nářečích	nářečí	k1gNnPc6	nářečí
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
(	(	kIx(	(
<g/>
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ne	ne	k9	ne
–	–	k?	–
nie	nie	k?	nie
–	–	k?	–
nie	nie	k?	nie
<g/>
/	/	kIx~	/
<g/>
ňje	ňje	k?	ňje
–	–	k?	–
ně	on	k3xPp3gNnSc4	on
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
slovenských	slovenský	k2eAgNnPc2d1	slovenské
nářečí	nářečí	k1gNnPc2	nářečí
===	===	k?	===
</s>
</p>
<p>
<s>
Západoslovenské	západoslovenský	k2eAgFnPc1d1	Západoslovenská
<g/>
:	:	kIx,	:
záhorské	záhorský	k2eAgFnPc1d1	Záhorská
<g/>
,	,	kIx,	,
bratislavské	bratislavský	k2eAgFnPc1d1	Bratislavská
<g/>
,	,	kIx,	,
trnavské	trnavský	k2eAgFnPc1d1	Trnavská
<g/>
,	,	kIx,	,
nitrianske	nitriansk	k1gInPc1	nitriansk
<g/>
,	,	kIx,	,
trenčianske	trenčianske	k1gNnPc1	trenčianske
<g/>
,	,	kIx,	,
hornonitrianske	hornonitriansk	k1gInPc1	hornonitriansk
<g/>
,	,	kIx,	,
komárňanské	komárňanský	k2eAgInPc1d1	komárňanský
</s>
</p>
<p>
<s>
středoslovenské	středoslovenský	k2eAgFnPc1d1	Středoslovenská
<g/>
:	:	kIx,	:
tekovské	tekovská	k1gFnPc1	tekovská
<g/>
,	,	kIx,	,
hontianske	hontiansk	k1gInPc1	hontiansk
<g/>
,	,	kIx,	,
novohradske	novohradsk	k1gFnPc1	novohradsk
<g/>
,	,	kIx,	,
gemerské	gemerský	k2eAgFnPc1d1	gemerský
<g/>
,	,	kIx,	,
zvolenské	zvolenský	k2eAgFnPc1d1	Zvolenská
<g/>
,	,	kIx,	,
horehronské	horehronský	k2eAgFnPc1d1	horehronský
<g/>
,	,	kIx,	,
liptovské	liptovský	k2eAgFnPc1d1	Liptovská
<g/>
,	,	kIx,	,
oravské	oravský	k2eAgFnPc1d1	Oravská
<g/>
,	,	kIx,	,
turčianske	turčiansk	k1gFnPc1	turčiansk
<g/>
,	,	kIx,	,
kysucké	kysucký	k2eAgFnPc1d1	kysucký
<g/>
,	,	kIx,	,
považskobystrické	považskobystrický	k2eAgFnPc1d1	považskobystrický
</s>
</p>
<p>
<s>
východoslovenské	východoslovenský	k2eAgFnPc1d1	Východoslovenská
<g/>
:	:	kIx,	:
spišské	spišský	k2eAgFnPc1d1	Spišská
<g/>
,	,	kIx,	,
šarišské	šarišský	k2eAgFnPc1d1	Šarišská
<g/>
,	,	kIx,	,
turnianske	turniansk	k1gFnPc1	turniansk
<g/>
,	,	kIx,	,
zemplínske	zemplínsk	k1gFnPc1	zemplínsk
<g/>
,	,	kIx,	,
abovské	abovský	k2eAgFnPc1d1	abovský
<g/>
,	,	kIx,	,
popradské	popradský	k2eAgFnPc1d1	Popradská
<g/>
,	,	kIx,	,
zamagurské	zamagurský	k2eAgFnPc1d1	zamagurský
</s>
</p>
<p>
<s>
Dolnozemské	dolnozemský	k2eAgNnSc1d1	dolnozemský
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
:	:	kIx,	:
mimo	mimo	k6eAd1	mimo
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Panonské	panonský	k2eAgFnSc6d1	Panonská
nížině	nížina	k1gFnSc6	nížina
v	v	k7c6	v
srbské	srbský	k2eAgFnSc6d1	Srbská
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
západním	západní	k2eAgNnSc6d1	západní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnSc6d1	chorvatská
části	část	k1gFnSc6	část
Syrmia	Syrmium	k1gNnSc2	Syrmium
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc4	ukázka
slovenského	slovenský	k2eAgInSc2d1	slovenský
textu	text	k1gInSc2	text
a	a	k8xC	a
lexika	lexikon	k1gNnSc2	lexikon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Znamení	znamení	k1gNnSc2	znamení
horoskopu	horoskop	k1gInSc2	horoskop
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotné	jednotný	k2eAgNnSc4d1	jednotné
a	a	k8xC	a
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
==	==	k?	==
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
jednotné	jednotný	k2eAgFnSc2d1	jednotná
a	a	k8xC	a
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
pomnožná	pomnožný	k2eAgFnSc1d1	pomnožná
a	a	k8xC	a
hromadná	hromadný	k2eAgNnPc1d1	hromadné
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomnožná	pomnožný	k2eAgNnPc1d1	pomnožné
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
označují	označovat	k5eAaImIp3nP	označovat
jednu	jeden	k4xCgFnSc4	jeden
věc	věc	k1gFnSc4	věc
(	(	kIx(	(
<g/>
nástenné	nástenný	k2eAgFnPc1d1	nástenný
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
nohavice	nohavice	k1gFnPc1	nohavice
<g/>
,	,	kIx,	,
nožnice	nožnice	k?	nožnice
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hromadná	hromadný	k2eAgNnPc1d1	hromadné
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nemají	mít	k5eNaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
označují	označovat	k5eAaImIp3nP	označovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
(	(	kIx(	(
<g/>
ľudstvo	ľudstvo	k1gNnSc1	ľudstvo
<g/>
,	,	kIx,	,
lístie	lístie	k1gFnSc1	lístie
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příslovce	příslovce	k1gNnSc2	příslovce
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
okolnosti	okolnost	k1gFnPc4	okolnost
dějů	děj	k1gInPc2	děj
<g/>
,	,	kIx,	,
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
příslovečných	příslovečný	k2eAgNnPc2d1	příslovečné
určení	určení	k1gNnPc2	určení
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
příslovce	příslovce	k1gNnPc1	příslovce
odvozená	odvozený	k2eAgNnPc1d1	odvozené
od	od	k7c2	od
jakostních	jakostní	k2eAgNnPc2d1	jakostní
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stupňovat	stupňovat	k5eAaImF	stupňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
příslovcí	příslovce	k1gNnPc2	příslovce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
miesta	miest	k1gMnSc4	miest
(	(	kIx(	(
<g/>
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
hore	hore	k?	hore
<g/>
,	,	kIx,	,
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
vrchom	vrchom	k1gInSc4	vrchom
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
dnu	dno	k1gNnSc3	dno
<g/>
,	,	kIx,	,
zdola	zdola	k6eAd1	zdola
<g/>
;	;	kIx,	;
ptáme	ptat	k5eAaImIp1nP	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
kde	kde	k6eAd1	kde
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
kade	kade	k1gFnSc5	kade
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
odkiaľ	odkiaľ	k?	odkiaľ
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
pokiaľ	pokiaľ	k?	pokiaľ
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
teraz	teraz	k1gInSc4	teraz
<g/>
,	,	kIx,	,
hneď	hnedit	k5eAaPmRp2nS	hnedit
<g/>
,	,	kIx,	,
zajtra	zajtra	k?	zajtra
<g/>
,	,	kIx,	,
včera	včera	k6eAd1	včera
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
<g/>
;	;	kIx,	;
ptáme	ptat	k5eAaImIp1nP	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
<g/>
:	:	kIx,	:
kedy	kedy	k1gInPc4	kedy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
odkedy	odked	k1gMnPc4	odked
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
dokiaľ	dokiaľ	k?	dokiaľ
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
spôsobu	spôsoba	k1gFnSc4	spôsoba
(	(	kIx(	(
<g/>
způsobu	způsob	k1gInSc2	způsob
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dobre	dobr	k1gMnSc5	dobr
<g/>
,	,	kIx,	,
zle	zle	k6eAd1	zle
<g/>
,	,	kIx,	,
pekne	peknout	k5eAaImIp3nS	peknout
<g/>
,	,	kIx,	,
veselo	veselo	k6eAd1	veselo
<g/>
;	;	kIx,	;
<g/>
ptáme	ptat	k5eAaImIp1nP	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
ako	ako	k?	ako
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
príčiny	príčin	k2eAgFnPc4d1	príčin
(	(	kIx(	(
<g/>
příčiny	příčina	k1gFnPc4	příčina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
náročky	náročka	k1gFnPc4	náročka
<g/>
,	,	kIx,	,
naschvál	naschvál	k1gInSc4	naschvál
<g/>
,	,	kIx,	,
náhodou	náhodou	k6eAd1	náhodou
<g/>
;	;	kIx,	;
<g/>
ptáme	ptat	k5eAaImIp1nP	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
prečo	prečo	k6eAd1	prečo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
načo	nača	k1gFnSc5	nača
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Příjmení	příjmení	k1gNnSc2	příjmení
==	==	k?	==
</s>
</p>
<p>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
většinou	většinou	k6eAd1	většinou
až	až	k6eAd1	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
přechylují	přechylovat	k5eAaImIp3nP	přechylovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
slovenštinou	slovenština	k1gFnSc7	slovenština
a	a	k8xC	a
češtinou	čeština	k1gFnSc7	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
Slovenské	slovenský	k2eAgInPc1d1	slovenský
hlásky	hlásek	k1gInPc1	hlásek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
abecedě	abeceda	k1gFnSc6	abeceda
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ä	ä	k?	ä
<g/>
,	,	kIx,	,
dz	dz	k?	dz
<g/>
,	,	kIx,	,
dž	dž	k?	dž
<g/>
,	,	kIx,	,
ľ	ľ	k?	ľ
<g/>
,	,	kIx,	,
ŕ	ŕ	k?	ŕ
<g/>
,	,	kIx,	,
ĺ.	ĺ.	k?	ĺ.
Česká	český	k2eAgNnPc1d1	české
písmena	písmeno	k1gNnPc1	písmeno
ř	ř	k?	ř
<g/>
,	,	kIx,	,
ě	ě	k?	ě
<g/>
,	,	kIx,	,
ů	ů	k?	ů
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
používá	používat	k5eAaImIp3nS	používat
oproti	oproti	k7c3	oproti
češtině	čeština	k1gFnSc3	čeština
dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
ia	ia	k0	ia
<g/>
,	,	kIx,	,
iu	iu	k?	iu
<g/>
,	,	kIx,	,
ie	ie	k?	ie
a	a	k8xC	a
ô.	ô.	k?	ô.
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
více	hodně	k6eAd2	hodně
palatalizuje	palatalizovat	k5eAaBmIp3nS	palatalizovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zní	znět	k5eAaImIp3nS	znět
měkčeji	měkko	k6eAd2	měkko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spodoba	spodoba	k1gFnSc1	spodoba
znělosti	znělost	k1gFnSc2	znělost
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgMnSc1d2	silnější
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
pravidelnější	pravidelní	k2eAgFnSc4d2	pravidelní
gramatiku	gramatika	k1gFnSc4	gramatika
než	než	k8xS	než
spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
zatížena	zatížit	k5eAaPmNgFnS	zatížit
ideálem	ideál	k1gInSc7	ideál
jazyka	jazyk	k1gInSc2	jazyk
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zjednodušení	zjednodušení	k1gNnSc2	zjednodušení
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
češtině	čeština	k1gFnSc6	čeština
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
pronikala	pronikat	k5eAaImAgFnS	pronikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgFnPc4d1	jiná
koncovky	koncovka	k1gFnPc4	koncovka
a	a	k8xC	a
vzory	vzor	k1gInPc4	vzor
skloňování	skloňování	k1gNnSc2	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc2	časování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
6	[number]	k4	6
pádů	pád	k1gInPc2	pád
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
oslovujeme	oslovovat	k5eAaImIp1nP	oslovovat
<g/>
,	,	kIx,	,
voláme	volat	k5eAaImIp1nP	volat
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádem	pád	k1gInSc7	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
základní	základní	k2eAgNnPc1d1	základní
slova	slovo	k1gNnPc1	slovo
si	se	k3xPyFc3	se
vůbec	vůbec	k9	vůbec
nejsou	být	k5eNaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
a	a	k8xC	a
zase	zase	k9	zase
některá	některý	k3yIgNnPc1	některý
podobná	podobný	k2eAgNnPc1d1	podobné
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
odborná	odborný	k2eAgFnSc1d1	odborná
terminologie	terminologie	k1gFnSc1	terminologie
byla	být	k5eAaImAgFnS	být
přejata	přejmout	k5eAaPmNgFnS	přejmout
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
do	do	k7c2	do
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
názvy	název	k1gInPc4	název
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
příklady	příklad	k1gInPc1	příklad
rozdílných	rozdílný	k2eAgNnPc2d1	rozdílné
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
:	:	kIx,	:
hovoriť	hovoriť	k1gFnSc1	hovoriť
–	–	k?	–
mluvit	mluvit	k5eAaImF	mluvit
<g/>
;	;	kIx,	;
hej	hej	k6eAd1	hej
–	–	k?	–
jo	jo	k9	jo
<g/>
;	;	kIx,	;
ak	ak	k?	ak
–	–	k?	–
jestli	jestli	k9	jestli
<g/>
,	,	kIx,	,
-li	i	k?	-li
<g/>
;	;	kIx,	;
Dovidenia	Dovidenium	k1gNnSc2	Dovidenium
–	–	k?	–
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
<g/>
;	;	kIx,	;
január	január	k1gInSc1	január
–	–	k?	–
leden	leden	k1gInSc1	leden
<g/>
;	;	kIx,	;
mačka	mačka	k1gFnSc1	mačka
–	–	k?	–
kočka	kočka	k1gFnSc1	kočka
<g/>
;	;	kIx,	;
bozkávať	bozkávatit	k5eAaPmRp2nS	bozkávatit
–	–	k?	–
líbat	líbat	k5eAaImF	líbat
<g/>
;	;	kIx,	;
teraz	teraz	k1gInSc1	teraz
–	–	k?	–
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
<g/>
;	;	kIx,	;
tovar	tovar	k1gInSc1	tovar
–	–	k?	–
zboží	zboží	k1gNnSc2	zboží
</s>
</p>
<p>
<s>
kel	kel	k1gInSc1	kel
–	–	k?	–
kapusta	kapusta	k1gFnSc1	kapusta
</s>
</p>
<p>
<s>
kapusta	kapusta	k1gFnSc1	kapusta
–	–	k?	–
zelí	zelí	k1gNnSc1	zelí
</s>
</p>
<p>
<s>
kaleráb	kaleráb	k1gInSc1	kaleráb
–	–	k?	–
kedlubna	kedlubna	k1gFnSc1	kedlubna
</s>
</p>
<p>
<s>
poľovník	poľovník	k1gMnSc1	poľovník
–	–	k?	–
myslivec	myslivec	k1gMnSc1	myslivec
</s>
</p>
<p>
<s>
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
koncovky	koncovka	k1gFnPc1	koncovka
(	(	kIx(	(
<g/>
-cia	-cia	k1gMnSc1	-cia
<g/>
,	,	kIx,	,
-dlo	lo	k1gMnSc1	-dlo
<g/>
,	,	kIx,	,
-ť	-ť	k?	-ť
<g/>
,	,	kIx,	,
-om	m	k?	-om
x	x	k?	x
-c	-c	k?	-c
<g/>
(	(	kIx(	(
<g/>
i	i	k8xC	i
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
-tko	-tko	k1gMnSc1	-tko
<g/>
,	,	kIx,	,
-t	-t	k?	-t
<g/>
,	,	kIx,	,
-em	m	k?	-em
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyjádření	vyjádření	k1gNnSc1	vyjádření
(	(	kIx(	(
<g/>
treba	treba	k1gFnSc1	treba
<g/>
,	,	kIx,	,
možno	možno	k6eAd1	možno
x	x	k?	x
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnPc4	předložka
(	(	kIx(	(
<g/>
na	na	k7c6	na
x	x	k?	x
k	k	k7c3	k
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
<g/>
)	)	kIx)	)
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
příklady	příklad	k1gInPc1	příklad
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
významů	význam	k1gInPc2	význam
<g/>
:	:	kIx,	:
topiť	topiť	k1gFnSc1	topiť
(	(	kIx(	(
<g/>
topit	topit	k5eAaImF	topit
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
x	x	k?	x
topit	topit	k5eAaImF	topit
(	(	kIx(	(
<g/>
kúriť	kúriť	k1gFnSc4	kúriť
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kúriť	kúriť	k1gFnSc1	kúriť
(	(	kIx(	(
<g/>
topit	topit	k5eAaImF	topit
<g/>
)	)	kIx)	)
x	x	k?	x
kouřit	kouřit	k5eAaImF	kouřit
(	(	kIx(	(
<g/>
fajčiť	fajčiť	k1gFnSc4	fajčiť
cigaretu	cigareta	k1gFnSc4	cigareta
<g/>
,	,	kIx,	,
dymiť	dymiť	k1gFnSc4	dymiť
z	z	k7c2	z
komína	komín	k1gInSc2	komín
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
horký	horký	k2eAgInSc4d1	horký
(	(	kIx(	(
<g/>
hořký	hořký	k2eAgInSc4d1	hořký
<g/>
)	)	kIx)	)
x	x	k?	x
horký	horký	k2eAgInSc4d1	horký
(	(	kIx(	(
<g/>
horúci	horúce	k1gFnSc4	horúce
<g/>
)	)	kIx)	)
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
používá	používat	k5eAaImIp3nS	používat
trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
tvořený	tvořený	k2eAgInSc1d1	tvořený
pomocí	pomocí	k7c2	pomocí
trpného	trpný	k2eAgNnSc2d1	trpné
příčestí	příčestí	k1gNnSc2	příčestí
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
než	než	k8xS	než
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
tvaru	tvar	k1gInSc2	tvar
se	s	k7c7	s
zvratným	zvratný	k2eAgNnSc7d1	zvratné
zájmenem	zájmeno	k1gNnSc7	zájmeno
sa	sa	k?	sa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Slovake	Slovake	k1gFnSc1	Slovake
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
–	–	k?	–
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
s	s	k7c7	s
bezplatnými	bezplatný	k2eAgInPc7d1	bezplatný
kurzy	kurz	k1gInPc7	kurz
slovenštiny	slovenština	k1gFnSc2	slovenština
</s>
</p>
<p>
<s>
slovenská	slovenský	k2eAgFnSc1d1	slovenská
abeceda	abeceda	k1gFnSc1	abeceda
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slovenština	slovenština	k1gFnSc1	slovenština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
slovenština	slovenština	k1gFnSc1	slovenština
ve	v	k7c6	v
WikislovníkuKurz	WikislovníkuKurz	k1gMnSc1	WikislovníkuKurz
slovenštiny	slovenština	k1gFnPc4	slovenština
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
</s>
</p>
<p>
<s>
Slovensko-český	slovensko-český	k2eAgInSc1d1	slovensko-český
/	/	kIx~	/
česko-slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
online	onlinout	k5eAaPmIp3nS	onlinout
slovník	slovník	k1gInSc1	slovník
</s>
</p>
<p>
<s>
Slovenské	slovenský	k2eAgInPc1d1	slovenský
výkladové	výkladový	k2eAgInPc1d1	výkladový
slovníky	slovník	k1gInPc1	slovník
</s>
</p>
<p>
<s>
Slovenské	slovenský	k2eAgInPc1d1	slovenský
lingvistické	lingvistický	k2eAgInPc1d1	lingvistický
časopisy	časopis	k1gInPc1	časopis
a	a	k8xC	a
knihy	kniha	k1gFnPc1	kniha
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
slovenském	slovenský	k2eAgInSc6d1	slovenský
a	a	k8xC	a
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
Grammatica	Grammatic	k2eAgFnSc1d1	Grammatica
slavico-bohemica	slavicoohemica	k1gFnSc1	slavico-bohemica
<g/>
,	,	kIx,	,
:	:	kIx,	:
in	in	k?	in
qua	qua	k?	qua
<g/>
,	,	kIx,	,
praeter	praeter	k1gMnSc1	praeter
alia	alia	k1gMnSc1	alia
<g/>
,	,	kIx,	,
ratio	ratio	k1gMnSc1	ratio
accuratae	accurata	k1gFnSc2	accurata
scriptionis	scriptionis	k1gInSc1	scriptionis
et	et	k?	et
flexionis	flexionis	k1gInSc4	flexionis
....	....	k?	....
Posonii	Posonie	k1gFnSc4	Posonie
:	:	kIx,	:
Typis	Typis	k1gFnSc1	Typis
Royerianis	Royerianis	k1gFnSc1	Royerianis
<g/>
,	,	kIx,	,
An	An	k1gFnSc1	An
<g/>
.	.	kIx.	.
1746	[number]	k4	1746
<g/>
.	.	kIx.	.
321	[number]	k4	321
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
