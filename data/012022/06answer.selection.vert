<s>
Slovenština	slovenština	k1gFnSc1	slovenština
je	být	k5eAaImIp3nS	být
západoslovanský	západoslovanský	k2eAgInSc4d1	západoslovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
češtině	čeština	k1gFnSc6	čeština
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
historické	historický	k2eAgFnSc2d1	historická
příbuznosti	příbuznost	k1gFnSc2	příbuznost
i	i	k8xC	i
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
zejména	zejména	k9	zejména
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
společného	společný	k2eAgNnSc2d1	společné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
existoval	existovat	k5eAaImAgInS	existovat
pasivní	pasivní	k2eAgInSc1d1	pasivní
bilingvismus	bilingvismus	k1gInSc1	bilingvismus
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
podobách	podoba	k1gFnPc6	podoba
jednoho	jeden	k4xCgInSc2	jeden
československého	československý	k2eAgInSc2d1	československý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
