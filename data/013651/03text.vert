<s>
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
</s>
<s>
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
forem	forma	k1gFnPc2
obchodních	obchodní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
lze	lze	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
za	za	k7c7
účelem	účel	k1gInSc7
podnikání	podnikání	k1gNnSc2
založit	založit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
nejrozšířenější	rozšířený	k2eAgFnSc4d3
formu	forma	k1gFnSc4
podnikání	podnikání	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
rovněž	rovněž	k9
se	se	k3xPyFc4
celosvětově	celosvětově	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
běžnou	běžný	k2eAgFnSc4d1
formu	forma	k1gFnSc4
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonem	zákon	k1gInSc7
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
společnost	společnost	k1gFnSc4
kapitálovou	kapitálový	k2eAgFnSc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
vykazuje	vykazovat	k5eAaImIp3nS
však	však	k9
i	i	k9
některé	některý	k3yIgInPc4
rysy	rys	k1gInPc4
společnosti	společnost	k1gFnSc2
osobní	osobní	k2eAgFnSc7d1
a	a	k8xC
odbornou	odborný	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
je	být	k5eAaImIp3nS
tedy	tedy	k9
mnohdy	mnohdy	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
tzv.	tzv.	kA
smíšenou	smíšený	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
statutárním	statutární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
oprávněn	oprávnit	k5eAaPmNgInS
společnost	společnost	k1gFnSc1
zastupovat	zastupovat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednatel	jednatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgInSc6d1
právním	právní	k2eAgInSc6d1
řádu	řád	k1gInSc6
ji	on	k3xPp3gFnSc4
upravuje	upravovat	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
90	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkratka	zkratka	k1gFnSc1
obchodní	obchodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
s.	s.	kA
r.	r.	kA
o.	o.	kA
</s>
<s>
Založení	založení	k1gNnSc1
a	a	k8xC
vznik	vznik	k1gInSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
Počet	počet	k1gInSc1
nově	nově	k6eAd1
založených	založený	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
ve	v	k7c6
Veřejném	veřejný	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Společnost	společnost	k1gFnSc1
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
může	moct	k5eAaImIp3nS
založit	založit	k5eAaPmF
jediný	jediný	k2eAgMnSc1d1
společník	společník	k1gMnSc1
nebo	nebo	k8xC
více	hodně	k6eAd2
společníků	společník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodní	obchodní	k2eAgFnSc1d1
firma	firma	k1gFnSc1
společnosti	společnost	k1gFnSc2
musí	muset	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
označení	označení	k1gNnSc4
„	„	k?
<g/>
společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
<g/>
“	“	k?
(	(	kIx(
<g/>
nebo	nebo	k8xC
jako	jako	k9
zkratku	zkratka	k1gFnSc4
s.	s.	k?
r.	r.	kA
o.	o.	k?
či	či	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
o.	o.	k?
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Minimální	minimální	k2eAgInSc1d1
vklad	vklad	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
společníka	společník	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
i	i	k9
minimální	minimální	k2eAgFnSc1d1
výše	výše	k1gFnSc1
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
1	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
její	její	k3xOp3gMnPc1
společníci	společník	k1gMnPc1
dohodnou	dohodnout	k5eAaPmIp3nP
na	na	k7c6
obsahu	obsah	k1gInSc6
společenské	společenský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
podepíší	podepsat	k5eAaPmIp3nP
ji	on	k3xPp3gFnSc4
u	u	k7c2
notáře	notář	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
vzniká	vznikat	k5eAaImIp3nS
až	až	k6eAd1
okamžikem	okamžik	k1gInSc7
zápisu	zápis	k1gInSc2
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
podání	podání	k1gNnSc2
návrhu	návrh	k1gInSc2
na	na	k7c4
zápis	zápis	k1gInSc4
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
splaceny	splatit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
nepeněžité	peněžitý	k2eNgInPc1d1
vklady	vklad	k1gInPc1
<g/>
,	,	kIx,
vkladové	vkladový	k2eAgNnSc1d1
ážio	ážio	k1gNnSc1
a	a	k8xC
na	na	k7c4
každý	každý	k3xTgInSc4
peněžitý	peněžitý	k2eAgInSc4d1
vklad	vklad	k1gInSc4
nejméně	málo	k6eAd3
jeho	jeho	k3xOp3gInSc7
30	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
vklad	vklad	k1gInSc4
nepeněžitý	peněžitý	k2eNgInSc4d1
(	(	kIx(
<g/>
tj.	tj.	kA
nemovité	movitý	k2eNgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
akcie	akcie	k1gFnPc4
<g/>
,	,	kIx,
auta	auto	k1gNnSc2
atd.	atd.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
jej	on	k3xPp3gMnSc4
ocenit	ocenit	k5eAaPmF
soudní	soudní	k2eAgMnSc1d1
znalec	znalec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Nepeněžitým	peněžitý	k2eNgInSc7d1
vkladem	vklad	k1gInSc7
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
práce	práce	k1gFnPc4
nebo	nebo	k8xC
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
vznikem	vznik	k1gInSc7
společnosti	společnost	k1gFnSc2
přijímá	přijímat	k5eAaImIp3nS
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
vklady	vklad	k1gInPc4
správce	správce	k1gMnSc4
vkladů	vklad	k1gInPc2
<g/>
,	,	kIx,
jmenovaný	jmenovaný	k2eAgInSc1d1
společenskou	společenský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
vznikne	vzniknout	k5eAaPmIp3nS
až	až	k9
zápisem	zápis	k1gInSc7
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
založením	založení	k1gNnSc7
a	a	k8xC
vznikem	vznik	k1gInSc7
ještě	ještě	k6eAd1
nemá	mít	k5eNaImIp3nS
právní	právní	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
úkony	úkon	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
založení	založení	k1gNnSc4
účtu	účet	k1gInSc2
<g/>
,	,	kIx,
nájemní	nájemní	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
)	)	kIx)
však	však	k9
může	moct	k5eAaImIp3nS
učinit	učinit	k5eAaPmF,k5eAaImF
již	již	k6eAd1
před	před	k7c7
zápisem	zápis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
založení	založení	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
koupě	koupě	k1gFnSc1
tzv.	tzv.	kA
„	„	k?
<g/>
ready-made	ready-mást	k5eAaPmIp3nS
<g/>
“	“	k?
společnosti	společnost	k1gFnSc2
neboli	neboli	k8xC
předzaložené	předzaložený	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
firmu	firma	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zapsaná	zapsaný	k2eAgFnSc1d1
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
název	název	k1gInSc4
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc4
<g/>
,	,	kIx,
jednatele	jednatel	k1gMnSc2
<g/>
,	,	kIx,
identifikační	identifikační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
a	a	k8xC
také	také	k9
plně	plně	k6eAd1
splacený	splacený	k2eAgInSc1d1
základní	základní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
však	však	k9
nevyvíjí	vyvíjet	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
obchodní	obchodní	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
za	za	k7c7
účelem	účel	k1gInSc7
prodeje	prodej	k1gInSc2
konečnému	konečný	k2eAgMnSc3d1
zákazníkovi	zákazník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Právní	právní	k2eAgFnSc1d1
odpovědnost	odpovědnost	k1gFnSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
za	za	k7c4
porušení	porušení	k1gNnSc4
svých	svůj	k3xOyFgInPc2
závazků	závazek	k1gInPc2
celým	celý	k2eAgInSc7d1
svým	svůj	k3xOyFgInSc7
majetkem	majetek	k1gInSc7
<g/>
,	,	kIx,
společníci	společník	k1gMnPc1
pak	pak	k6eAd1
společně	společně	k6eAd1
a	a	k8xC
nerozdílně	rozdílně	k6eNd1
ručí	ručit	k5eAaImIp3nS
za	za	k7c4
závazky	závazek	k1gInPc4
společnosti	společnost	k1gFnSc2
jen	jen	k9
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
,	,	kIx,
v	v	k7c6
jaké	jaký	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
nesplnili	splnit	k5eNaPmAgMnP
vkladové	vkladový	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
podle	podle	k7c2
stavu	stav	k1gInSc2
zapsaného	zapsaný	k2eAgInSc2d1
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byli	být	k5eAaImAgMnP
věřitelem	věřitel	k1gMnSc7
vyzváni	vyzvat	k5eAaPmNgMnP
k	k	k7c3
plnění	plnění	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Ručí	ručit	k5eAaImIp3nS
tedy	tedy	k9
i	i	k9
společník	společník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
svou	svůj	k3xOyFgFnSc4
část	část	k1gFnSc4
vkladu	vklást	k5eAaPmIp1nS
již	již	k6eAd1
splatil	splatit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
zbylí	zbylý	k2eAgMnPc1d1
společníci	společník	k1gMnPc1
ještě	ještě	k6eAd1
nesplatili	splatit	k5eNaPmAgMnP
svoje	svůj	k3xOyFgInPc4
vklady	vklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaplacením	zaplacení	k1gNnSc7
kterémukoliv	kterýkoliv	k3yIgMnSc3
z	z	k7c2
věřitelů	věřitel	k1gMnPc2
ručení	ručení	k1gNnSc2
nezaniká	zanikat	k5eNaImIp3nS
ani	ani	k8xC
se	se	k3xPyFc4
nesnižuje	snižovat	k5eNaImIp3nS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
rozsah	rozsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápisem	zápis	k1gInSc7
splacení	splacení	k1gNnSc2
všech	všecek	k3xTgInPc2
vkladů	vklad	k1gInPc2
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
ručení	ručení	k1gNnPc2
společníků	společník	k1gMnPc2
zaniká	zanikat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednatelé	jednatel	k1gMnPc1
mají	mít	k5eAaImIp3nP
povinnost	povinnost	k1gFnSc4
jednat	jednat	k5eAaImF
s	s	k7c7
péčí	péče	k1gFnSc7
řádného	řádný	k2eAgMnSc2d1
hospodáře	hospodář	k1gMnSc2
a	a	k8xC
s	s	k7c7
uvážením	uvážení	k1gNnSc7
všech	všecek	k3xTgInPc2
jim	on	k3xPp3gMnPc3
známých	známý	k2eAgFnPc2d1
a	a	k8xC
dostupných	dostupný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Orgány	orgán	k1gInPc1
společnosti	společnost	k1gFnSc2
</s>
<s>
Valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
orgán	orgán	k1gInSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členy	člen	k1gMnPc7
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
jsou	být	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
společníci	společník	k1gMnPc1
(	(	kIx(
<g/>
tedy	tedy	k9
majitelé	majitel	k1gMnPc1
<g/>
,	,	kIx,
vlastníci	vlastník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svolává	svolávat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
jednatel	jednatel	k1gMnSc1
společnosti	společnost	k1gFnSc3
nejméně	málo	k6eAd3
jedenkrát	jedenkrát	k6eAd1
ročně	ročně	k6eAd1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
do	do	k7c2
její	její	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
rozhodování	rozhodování	k1gNnSc1
o	o	k7c6
změně	změna	k1gFnSc6
společenské	společenský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
změně	změna	k1gFnSc3
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c6
připuštění	připuštění	k1gNnSc6
nepeněžitého	peněžitý	k2eNgInSc2d1
vkladu	vklad	k1gInSc2
<g/>
,	,	kIx,
volba	volba	k1gFnSc1
a	a	k8xC
odvolání	odvolání	k1gNnSc1
jednatele	jednatel	k1gMnSc2
a	a	k8xC
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
byla	být	k5eAaImAgFnS
<g/>
-li	-li	k?
zřízena	zřízen	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
schvaluje	schvalovat	k5eAaImIp3nS
například	například	k6eAd1
rozdělení	rozdělení	k1gNnSc1
zisku	zisk	k1gInSc2
<g/>
,	,	kIx,
účetní	účetní	k2eAgFnSc4d1
závěrku	závěrka	k1gFnSc4
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jednočlenné	jednočlenný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
vykonává	vykonávat	k5eAaImIp3nS
působnost	působnost	k1gFnSc4
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
její	její	k3xOp3gMnSc1
společník	společník	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednatelé	jednatel	k1gMnPc1
–	–	k?
statutárním	statutární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
společnosti	společnost	k1gFnSc2
jsou	být	k5eAaImIp3nP
jeden	jeden	k4xCgMnSc1
nebo	nebo	k8xC
více	hodně	k6eAd2
jednatelů	jednatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednateli	jednatel	k1gMnPc7
přísluší	příslušet	k5eAaImIp3nS
obchodní	obchodní	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Jednatelé	jednatel	k1gMnPc1
jsou	být	k5eAaImIp3nP
uvedeni	uveden	k2eAgMnPc1d1
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dozorčí	dozorčí	k2eAgFnSc1d1
rada	rada	k1gFnSc1
–	–	k?
tento	tento	k3xDgInSc1
nepovinně	povinně	k6eNd1
vytvářený	vytvářený	k2eAgInSc1d1
orgán	orgán	k1gInSc1
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
činnost	činnost	k1gFnSc4
jednatelů	jednatel	k1gMnPc2
<g/>
,	,	kIx,
kontroluje	kontrolovat	k5eAaImIp3nS
účetní	účetní	k2eAgFnSc4d1
dokumentaci	dokumentace	k1gFnSc4
<g/>
,	,	kIx,
jednou	jeden	k4xCgFnSc7
ročně	ročně	k6eAd1
předkládá	předkládat	k5eAaImIp3nS
zprávu	zpráva	k1gFnSc4
o	o	k7c4
své	svůj	k3xOyFgFnPc4
činnosti	činnost	k1gFnPc4
valné	valný	k2eAgFnSc3d1
hromadě	hromada	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Společníci	společník	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
postavení	postavení	k1gNnSc1
</s>
<s>
V	v	k7c6
postavení	postavení	k1gNnSc6
společníků	společník	k1gMnPc2
se	se	k3xPyFc4
akcentuje	akcentovat	k5eAaImIp3nS
kapitálová	kapitálový	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
<g/>
,	,	kIx,
nikoli	nikoli	k9
osobní	osobní	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsah	rozsah	k1gInSc1
práv	právo	k1gNnPc2
a	a	k8xC
povinností	povinnost	k1gFnPc2
je	být	k5eAaImIp3nS
nerovný	rovný	k2eNgMnSc1d1
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
se	se	k3xPyFc4
majetkovou	majetkový	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
společníků	společník	k1gMnPc2
na	na	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
členství	členství	k1gNnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
</s>
<s>
Originární	Originární	k2eAgMnPc1d1
–	–	k?
zakladatelé	zakladatel	k1gMnPc1
a	a	k8xC
přistupující	přistupující	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
členství	členství	k1gNnSc1
vzniká	vznikat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
uzavřením	uzavření	k1gNnSc7
společenské	společenský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
vydáním	vydání	k1gNnSc7
zakladatelské	zakladatelský	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
jedním	jeden	k4xCgMnSc7
zakladatelem	zakladatel	k1gMnSc7
<g/>
;	;	kIx,
</s>
<s>
při	při	k7c6
zvýšení	zvýšení	k1gNnSc6
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Derivativní	derivativní	k2eAgInSc1d1
–	–	k?
při	při	k7c6
převodu	převod	k1gInSc6
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
nebo	nebo	k8xC
přechodu	přechod	k1gInSc2
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
<g/>
,	,	kIx,
členství	členství	k1gNnSc4
vzniká	vznikat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
převodem	převod	k1gInSc7
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
smlouvou	smlouva	k1gFnSc7
nebo	nebo	k8xC
rubopisem	rubopis	k1gInSc7
na	na	k7c6
kmenovém	kmenový	k2eAgInSc6d1
listu	list	k1gInSc6
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
děděním	dědění	k1gNnSc7
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
univerzálním	univerzální	k2eAgNnSc7d1
právním	právní	k2eAgNnSc7d1
nástupnictvím	nástupnictví	k1gNnSc7
po	po	k7c6
zaniklé	zaniklý	k2eAgFnSc6d1
právnické	právnický	k2eAgFnSc6d1
osobě	osoba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
společníkem	společník	k1gMnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
převodem	převod	k1gInSc7
uvolněného	uvolněný	k2eAgInSc2d1
podílu	podíl	k1gInSc2
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Práva	právo	k1gNnPc1
společníků	společník	k1gMnPc2
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
podíl	podíl	k1gInSc4
na	na	k7c6
zisku	zisk	k1gInSc6
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc7d1
podmínkou	podmínka	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
musí	muset	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc4
zisk	zisk	k1gInSc4
vytvořit	vytvořit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
o	o	k7c4
užití	užití	k1gNnSc4
zisku	zisk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
vytvořila	vytvořit	k5eAaPmAgFnS
<g/>
,	,	kIx,
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
působnosti	působnost	k1gFnSc2
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
na	na	k7c6
zisku	zisk	k1gInSc6
se	se	k3xPyFc4
mezi	mezi	k7c4
společníky	společník	k1gMnPc4
dělí	dělit	k5eAaImIp3nS
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
obchodních	obchodní	k2eAgInPc2d1
podílů	podíl	k1gInPc2
společníků	společník	k1gMnPc2
(	(	kIx(
<g/>
lze	lze	k6eAd1
sjednat	sjednat	k5eAaPmF
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
na	na	k7c6
zisku	zisk	k1gInSc6
je	být	k5eAaImIp3nS
splatný	splatný	k2eAgMnSc1d1
do	do	k7c2
3	#num#	k4
měsíců	měsíc	k1gInPc2
od	od	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
rozdělení	rozdělení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zálohu	záloh	k1gInSc2
na	na	k7c4
výplatu	výplata	k1gFnSc4
podílu	podíl	k1gInSc2
na	na	k7c6
zisku	zisk	k1gInSc6
lze	lze	k6eAd1
vyplácet	vyplácet	k5eAaImF
jen	jen	k9
na	na	k7c6
základě	základ	k1gInSc6
mezitímní	mezitímní	k2eAgFnSc2d1
účetní	účetní	k2eAgFnSc2d1
závěrky	závěrka	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
vyplyne	vyplynout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
obchodní	obchodní	k2eAgFnSc1d1
korporace	korporace	k1gFnSc1
má	mít	k5eAaImIp3nS
dostatek	dostatek	k1gInSc4
prostředků	prostředek	k1gInPc2
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
vypořádací	vypořádací	k2eAgInSc4d1
podíl	podíl	k1gInSc4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
právo	právo	k1gNnSc4
na	na	k7c4
likvidační	likvidační	k2eAgInSc4d1
podíl	podíl	k1gInSc4
nastupuje	nastupovat	k5eAaImIp3nS
při	při	k7c6
zániku	zánik	k1gInSc6
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
na	na	k7c4
vypořádací	vypořádací	k2eAgInSc4d1
podíl	podíl	k1gInSc4
vzniká	vznikat	k5eAaImIp3nS
ve	v	k7c6
fázi	fáze	k1gFnSc6
trvající	trvající	k2eAgFnSc2d1
existence	existence	k1gFnSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
některý	některý	k3yIgMnSc1
ze	z	k7c2
společníků	společník	k1gMnPc2
opouští	opouštět	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
bez	bez	k7c2
právního	právní	k2eAgMnSc2d1
nástupce	nástupce	k1gMnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
přechází	přecházet	k5eAaImIp3nS
<g/>
-li	-li	k?
uvolněný	uvolněný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniká	zanikat	k5eAaImIp3nS
<g/>
-li	-li	k?
účast	účast	k1gFnSc1
společníka	společník	k1gMnSc2
na	na	k7c6
společnosti	společnost	k1gFnSc6
jinak	jinak	k6eAd1
než	než	k8xS
převodem	převod	k1gInSc7
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
zaniká	zanikat	k5eAaImIp3nS
<g/>
-li	-li	k?
smrtí	smrt	k1gFnSc7
fyzické	fyzický	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
vyloučila	vyloučit	k5eAaPmAgFnS
přechod	přechod	k1gInSc4
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
na	na	k7c4
dědice	dědic	k1gMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zánikem	zánik	k1gInSc7
právnické	právnický	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
vyloučila	vyloučit	k5eAaPmAgFnS
přechod	přechod	k1gInSc4
obchodního	obchodní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyloučením	vyloučení	k1gNnSc7
společníka	společník	k1gMnSc2
(	(	kIx(
<g/>
kaduční	kaduční	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
,	,	kIx,
konkurs	konkurs	k1gInSc1
<g/>
,	,	kIx,
exekuce	exekuce	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vystoupením	vystoupení	k1gNnSc7
společníka	společník	k1gMnSc2
(	(	kIx(
<g/>
§	§	k?
164	#num#	k4
<g/>
,	,	kIx,
§	§	k?
202	#num#	k4
a	a	k8xC
§	§	k?
207	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
ZOK	ZOK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zrušením	zrušení	k1gNnSc7
účasti	účast	k1gFnSc2
společníka	společník	k1gMnSc4
soudem	soud	k1gInSc7
(	(	kIx(
<g/>
§	§	k?
205	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	výše	k1gFnSc1
vypořádacího	vypořádací	k2eAgInSc2d1
podílu	podíl	k1gInSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
odhadu	odhad	k1gInSc2
hodnoty	hodnota	k1gFnSc2
obchodní	obchodní	k2eAgFnSc2d1
korporace	korporace	k1gFnSc2
a	a	k8xC
poměru	poměr	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
společník	společník	k1gMnSc1
na	na	k7c6
hodnotě	hodnota	k1gFnSc6
podílel	podílet	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
podíl	podíl	k1gInSc4
na	na	k7c6
likvidačním	likvidační	k2eAgInSc6d1
zůstatku	zůstatek	k1gInSc6
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podíl	podíl	k1gInSc1
na	na	k7c6
likvidačním	likvidační	k2eAgInSc6d1
zůstatku	zůstatek	k1gInSc6
náleží	náležet	k5eAaImIp3nS
společníkovi	společník	k1gMnSc3
<g/>
,	,	kIx,
zaniká	zanikat	k5eAaImIp3nS
<g/>
-li	-li	k?
obchodní	obchodní	k2eAgFnSc1d1
korporace	korporace	k1gFnSc1
s	s	k7c7
likvidací	likvidace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Likvidační	likvidační	k2eAgInSc1d1
podíl	podíl	k1gInSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
mezi	mezi	k7c7
společníky	společník	k1gMnPc7
nejprve	nejprve	k6eAd1
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
,	,	kIx,
v	v	k7c6
jaké	jaký	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
splnili	splnit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
vkladovou	vkladový	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestačí	stačit	k5eNaBmIp3nS
<g/>
-li	-li	k?
likvidační	likvidační	k2eAgInSc1d1
zůstatek	zůstatek	k1gInSc1
na	na	k7c4
toto	tento	k3xDgNnSc4
rozdělení	rozdělení	k1gNnSc4
<g/>
,	,	kIx,
podílejí	podílet	k5eAaImIp3nP
se	se	k3xPyFc4
společníci	společník	k1gMnPc1
na	na	k7c6
likvidačním	likvidační	k2eAgInSc6d1
zůstatku	zůstatek	k1gInSc6
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
výši	výše	k1gFnSc3,k1gFnSc3wB
svých	svůj	k3xOyFgInPc2
splacených	splacený	k2eAgInPc2d1
či	či	k8xC
vnesených	vnesený	k2eAgInPc2d1
vkladů	vklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neměl	mít	k5eNaImAgMnS
<g/>
-li	-li	k?
žádný	žádný	k1gMnSc1
ze	z	k7c2
společníků	společník	k1gMnPc2
vkladovou	vkladový	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
<g/>
,	,	kIx,
rozdělí	rozdělit	k5eAaPmIp3nS
se	se	k3xPyFc4
likvidační	likvidační	k2eAgInSc1d1
zůstatek	zůstatek	k1gInSc1
mezi	mezi	k7c4
společníky	společník	k1gMnPc4
rovným	rovný	k2eAgInSc7d1
dílem	díl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Právo	právo	k1gNnSc1
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
řízení	řízení	k1gNnSc4
společnosti	společnost	k1gFnSc2
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S.	S.	kA
r.	r.	kA
o.	o.	k?
jako	jako	k8xS,k8xC
kapitálová	kapitálový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
má	mít	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
orgánů	orgán	k1gInPc2
<g/>
,	,	kIx,
nevylučuje	vylučovat	k5eNaImIp3nS
manažerské	manažerský	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
prostřednictvím	prostřednictvím	k7c2
nespolečníka	nespolečník	k1gMnSc2
<g/>
,	,	kIx,
jednateli	jednatel	k1gMnPc7
tak	tak	k8xS,k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
společníci	společník	k1gMnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jiné	jiný	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
–	–	k?
jednatelé	jednatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
jmenování	jmenování	k1gNnSc4
společenskou	společenský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
nebo	nebo	k8xC
valnou	valný	k2eAgFnSc7d1
hromadou	hromada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
není	být	k5eNaImIp3nS
právem	právo	k1gNnSc7
společníka	společník	k1gMnSc2
být	být	k5eAaImF
jednatelem	jednatel	k1gMnSc7
či	či	k8xC
členem	člen	k1gMnSc7
jiného	jiný	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
řízení	řízení	k1gNnSc4
společnosti	společnost	k1gFnSc2
uskutečňuje	uskutečňovat	k5eAaImIp3nS
společník	společník	k1gMnSc1
prostřednictvím	prostřednictvím	k7c2
své	svůj	k3xOyFgFnSc2
účasti	účast	k1gFnSc2
na	na	k7c6
valné	valný	k2eAgFnSc6d1
hromadě	hromada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úprava	úprava	k1gFnSc1
organizace	organizace	k1gFnSc2
a	a	k8xC
hlasování	hlasování	k1gNnSc2
je	být	k5eAaImIp3nS
stanovena	stanovit	k5eAaPmNgFnS
ve	v	k7c6
společenské	společenský	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
(	(	kIx(
<g/>
podle	podle	k7c2
zákona	zákon	k1gInSc2
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
hlas	hlas	k1gInSc1
na	na	k7c4
1	#num#	k4
Kč	Kč	kA
vkladu	vklást	k5eAaPmIp1nS
společníka	společník	k1gMnSc4
<g/>
,	,	kIx,
ledaže	ledaže	k8xS
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
stanoví	stanovit	k5eAaPmIp3nS
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozastavení	pozastavení	k1gNnSc1
hlasovacích	hlasovací	k2eAgNnPc2d1
práv	právo	k1gNnPc2
nastává	nastávat	k5eAaImIp3nS
např.	např.	kA
pokud	pokud	k8xS
se	se	k3xPyFc4
dotýká	dotýkat	k5eAaImIp3nS
hlasování	hlasování	k1gNnSc1
společníka	společník	k1gMnSc2
(	(	kIx(
<g/>
např.	např.	kA
rozhoduje	rozhodovat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
vyloučení	vyloučení	k1gNnSc4
či	či	k8xC
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
odvolání	odvolání	k1gNnSc4
z	z	k7c2
funkce	funkce	k1gFnSc2
orgánu	orgán	k1gInSc2
společnosti	společnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
informace	informace	k1gFnPc4
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Právo	právo	k1gNnSc4
žádat	žádat	k5eAaImF
od	od	k7c2
jednatelů	jednatel	k1gMnPc2
informace	informace	k1gFnSc2
o	o	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
nahlížet	nahlížet	k5eAaImF
do	do	k7c2
dokladů	doklad	k1gInPc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kontrolovat	kontrolovat	k5eAaImF
údaje	údaj	k1gInPc1
obsažené	obsažený	k2eAgInPc1d1
v	v	k7c6
předložených	předložený	k2eAgInPc6d1
dokladech	doklad	k1gInPc6
a	a	k8xC
další	další	k2eAgNnPc4d1
práva	právo	k1gNnPc4
určená	určený	k2eAgFnSc1d1
společenskou	společenský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
kontrolovat	kontrolovat	k5eAaImF
doklady	doklad	k1gInPc4
společnosti	společnost	k1gFnSc2
mají	mít	k5eAaImIp3nP
společníci	společník	k1gMnPc1
sami	sám	k3xTgMnPc1
či	či	k8xC
skrze	skrze	k?
svého	svůj	k3xOyFgMnSc4
zástupce	zástupce	k1gMnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
<g/>
-li	-li	k?
tento	tento	k3xDgMnSc1
zástupce	zástupce	k1gMnSc1
zavázán	zavázat	k5eAaPmNgMnS
alespoň	alespoň	k9
ke	k	k7c3
stejné	stejná	k1gFnSc3
mlčenlivosti	mlčenlivost	k1gFnSc2
jako	jako	k8xS,k8xC
společník	společník	k1gMnSc1
a	a	k8xC
společnosti	společnost	k1gFnPc1
tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
doloží	doložit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mlčenlivost	mlčenlivost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
doložena	doložit	k5eAaPmNgFnS
smlouvou	smlouva	k1gFnSc7
nebo	nebo	k8xC
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
zástupce	zástupce	k1gMnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
mlčenlivosti	mlčenlivost	k1gFnSc2
vázán	vázat	k5eAaImNgMnS
ze	z	k7c2
zákona	zákon	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
advokát	advokát	k1gMnSc1
<g/>
,	,	kIx,
daňový	daňový	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
společnickou	společnický	k2eAgFnSc4d1
žalobu	žaloba	k1gFnSc4
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společník	společník	k1gMnSc1
může	moct	k5eAaImIp3nS
v	v	k7c6
zákonem	zákon	k1gInSc7
vymezených	vymezený	k2eAgInPc6d1
případech	případ	k1gInPc6
podat	podat	k5eAaPmF
žalobu	žaloba	k1gFnSc4
proti	proti	k7c3
členovi	člen	k1gMnSc3
orgánu	orgán	k1gInSc2
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
jednatel	jednatel	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
nejde	jít	k5eNaImIp3nS
o	o	k7c4
žalobu	žaloba	k1gFnSc4
společníka	společník	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jenom	jenom	k9
o	o	k7c4
žalobu	žaloba	k1gFnSc4
podanou	podaný	k2eAgFnSc4d1
společníkem	společník	k1gMnSc7
jménem	jméno	k1gNnSc7
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
žaloba	žaloba	k1gFnSc1
ut	ut	k?
singuli	singule	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žaloba	žaloba	k1gFnSc1
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nS
především	především	k9
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jednatel	jednatel	k1gMnSc1
či	či	k8xC
člen	člen	k1gMnSc1
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
svým	svůj	k3xOyFgNnSc7
jednáním	jednání	k1gNnSc7
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
povinností	povinnost	k1gFnSc7
péče	péče	k1gFnSc2
řádného	řádný	k2eAgMnSc2d1
hospodáře	hospodář	k1gMnSc2
způsobil	způsobit	k5eAaPmAgInS
společnosti	společnost	k1gFnSc3
újmu	újma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
§	§	k?
4	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
ZOK	ZOK	kA
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
obrácení	obrácení	k1gNnSc3
důkazního	důkazní	k2eAgNnSc2d1
břemene	břemeno	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
žalovaná	žalovaný	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
musí	muset	k5eAaImIp3nS
prokázat	prokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nedopustila	dopustit	k5eNaPmAgFnS
protiprávního	protiprávní	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
ledaže	ledaže	k8xS
soud	soud	k1gInSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
po	po	k7c6
ní	on	k3xPp3gFnSc6
nelze	lze	k6eNd1
spravedlivě	spravedlivě	k6eAd1
požadovat	požadovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Minoritní	minoritní	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
menšiny	menšina	k1gFnSc2
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
na	na	k7c4
společníky	společník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mají	mít	k5eAaImIp3nP
vklady	vklad	k1gInPc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
minimálně	minimálně	k6eAd1
10	#num#	k4
%	%	kIx~
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
právo	právo	k1gNnSc4
požádat	požádat	k5eAaPmF
jednatele	jednatel	k1gMnSc4
o	o	k7c4
svolání	svolání	k1gNnSc4
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
ke	k	k7c3
svolání	svolání	k1gNnSc3
valné	valný	k2eAgFnSc2d1
hromady	hromada	k1gFnSc2
nedojde	dojít	k5eNaPmIp3nS
do	do	k7c2
jednoho	jeden	k4xCgInSc2
měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
svolat	svolat	k5eAaPmF
sami	sám	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s>
Povinnosti	povinnost	k1gFnPc1
společníků	společník	k1gMnPc2
</s>
<s>
Vkladová	vkladový	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výši	výše	k1gFnSc4
vkladové	vkladový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
společníků	společník	k1gMnPc2
zákon	zákon	k1gInSc1
nestanoví	stanovit	k5eNaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
věcí	věc	k1gFnSc7
společenské	společenský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
určila	určit	k5eAaPmAgFnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
velký	velký	k2eAgInSc1d1
vklad	vklad	k1gInSc1
musí	muset	k5eAaImIp3nS
do	do	k7c2
společnosti	společnost	k1gFnSc2
společníci	společník	k1gMnPc1
vnést	vnést	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepeněžitý	peněžitý	k2eNgInSc1d1
vklad	vklad	k1gInSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
do	do	k7c2
společnosti	společnost	k1gFnSc2
vnesen	vnést	k5eAaPmNgInS
před	před	k7c7
jejím	její	k3xOp3gInSc7
vznikem	vznik	k1gInSc7
(	(	kIx(
<g/>
zápisem	zápis	k1gInSc7
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
peněžitý	peněžitý	k2eAgInSc4d1
vklad	vklad	k1gInSc4
musí	muset	k5eAaImIp3nS
společník	společník	k1gMnSc1
splatit	splatit	k5eAaPmF
ve	v	k7c6
lhůtě	lhůta	k1gFnSc6
stanovené	stanovený	k2eAgInPc1d1
společenskou	společenský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
<g/>
,	,	kIx,
nejpozději	pozdě	k6eAd3
však	však	k9
do	do	k7c2
5	#num#	k4
let	léto	k1gNnPc2
ode	ode	k7c2
dne	den	k1gInSc2
vzniku	vznik	k1gInSc2
společnosti	společnost	k1gFnSc2
nebo	nebo	k8xC
od	od	k7c2
převzetí	převzetí	k1gNnSc2
vkladové	vkladový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
za	za	k7c4
trvání	trvání	k1gNnSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
společník	společník	k1gMnSc1
v	v	k7c6
prodlení	prodlení	k1gNnSc6
se	s	k7c7
splácením	splácení	k1gNnSc7
peněžitého	peněžitý	k2eAgInSc2d1
vkladu	vklad	k1gInSc2
<g/>
,	,	kIx,
uhradí	uhradit	k5eAaPmIp3nS
společnosti	společnost	k1gFnSc3
úrok	úrok	k1gInSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
dvojnásobku	dvojnásobek	k1gInSc2
sazby	sazba	k1gFnSc2
úroku	úrok	k1gInSc2
z	z	k7c2
prodlení	prodlení	k1gNnSc2
stanovené	stanovený	k2eAgNnSc1d1
nařízením	nařízení	k1gNnSc7
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
neurčí	určit	k5eNaPmIp3nS
<g/>
-li	-li	k?
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Společník	společník	k1gMnSc1
nesmí	smět	k5eNaImIp3nS
po	po	k7c4
dobu	doba	k1gFnSc4
prodlení	prodlení	k1gNnSc2
s	s	k7c7
plněním	plnění	k1gNnSc7
vkladové	vkladový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
vykonávat	vykonávat	k5eAaImF
hlasovací	hlasovací	k2eAgNnSc1d1
právo	právo	k1gNnSc1
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
hlasu	hlas	k1gInSc3
se	se	k3xPyFc4
nepřihlíží	přihlížet	k5eNaImIp3nS
při	při	k7c6
zjištění	zjištění	k1gNnSc6
usnášeníschopnosti	usnášeníschopnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
možnosti	možnost	k1gFnSc2
požadovat	požadovat	k5eAaImF
úrok	úrok	k1gInSc4
z	z	k7c2
prodlení	prodlení	k1gNnSc2
může	moct	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
přistoupit	přistoupit	k5eAaPmF
i	i	k9
k	k	k7c3
vyloučení	vyloučení	k1gNnSc3
společníka	společník	k1gMnSc2
v	v	k7c6
tzv.	tzv.	kA
kadučním	kadučnit	k5eAaPmIp1nS
řízení	řízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
vyloučení	vyloučení	k1gNnSc6
rozhoduje	rozhodovat	k5eAaImIp3nS
valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
alespoň	alespoň	k9
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
rozhodováním	rozhodování	k1gNnSc7
o	o	k7c4
vyloučení	vyloučení	k1gNnSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
společník	společník	k1gMnSc1
vyzván	vyzvat	k5eAaPmNgMnS
ke	k	k7c3
splnění	splnění	k1gNnSc3
vkladové	vkladový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
a	a	k8xC
musí	muset	k5eAaImIp3nP
mu	on	k3xPp3gMnSc3
být	být	k5eAaImF
poskytnuta	poskytnut	k2eAgFnSc1d1
přiměřená	přiměřený	k2eAgFnSc1d1
lhůta	lhůta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příplatková	příplatkový	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tuto	tento	k3xDgFnSc4
povinnost	povinnost	k1gFnSc4
může	moct	k5eAaImIp3nS
stanovit	stanovit	k5eAaPmF
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
musí	muset	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
nebo	nebo	k8xC
může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
společníka	společník	k1gMnSc2
se	s	k7c7
souhlasem	souhlas	k1gInSc7
jednatele	jednatel	k1gMnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
tak	tak	k6eAd1
nestanoví	stanovit	k5eNaPmIp3nS
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
vlastního	vlastní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
společník	společník	k1gMnSc1
svým	svůj	k3xOyFgInSc7
příplatkem	příplatek	k1gInSc7
přináší	přinášet	k5eAaImIp3nS
společnosti	společnost	k1gFnSc3
další	další	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
financování	financování	k1gNnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
tento	tento	k3xDgInSc1
příplatek	příplatek	k1gInSc1
měl	mít	k5eAaImAgInS
vliv	vliv	k1gInSc4
na	na	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
společníkova	společníkův	k2eAgInSc2d1
vkladu	vklad	k1gInSc2
nebo	nebo	k8xC
na	na	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
stanovit	stanovit	k5eAaPmF
příspěvky	příspěvek	k1gInPc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
také	také	k9
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
výši	výše	k1gFnSc4
nesmí	smět	k5eNaImIp3nP
příplatky	příplatek	k1gInPc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
souhrnu	souhrn	k1gInSc6
překročit	překročit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Společník	společník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pro	pro	k7c4
příplatkovou	příplatkový	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
nehlasoval	hlasovat	k5eNaImAgMnS
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
příplatkovou	příplatkový	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
odmítnout	odmítnout	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
vystoupit	vystoupit	k5eAaPmF
ze	z	k7c2
společnosti	společnost	k1gFnSc2
ohledně	ohledně	k7c2
podílu	podíl	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
příplatková	příplatkový	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
vázána	vázat	k5eAaImNgFnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
již	již	k6eAd1
zcela	zcela	k6eAd1
splnil	splnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
vkladovou	vkladový	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
spojenou	spojený	k2eAgFnSc4d1
s	s	k7c7
tímto	tento	k3xDgInSc7
podílem	podíl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystoupením	vystoupení	k1gNnSc7
ze	z	k7c2
společnosti	společnost	k1gFnSc2
jeho	jeho	k3xOp3gFnSc1
příplatková	příplatkový	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
zanikne	zaniknout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Prodlení	prodlení	k1gNnSc1
se	s	k7c7
splácením	splácení	k1gNnSc7
příplatků	příplatek	k1gInPc2
má	mít	k5eAaImIp3nS
stejné	stejný	k2eAgInPc4d1
následky	následek	k1gInPc4
jako	jako	k8xS,k8xC
při	při	k7c6
prodlení	prodlení	k1gNnSc6
se	s	k7c7
splácením	splácení	k1gNnSc7
vkladů	vklad	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
i	i	k9
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
nesplnění	nesplnění	k1gNnSc2
povinnosti	povinnost	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
společník	společník	k1gMnSc1
ze	z	k7c2
společnosti	společnost	k1gFnSc2
vyloučen	vyloučit	k5eAaPmNgInS
v	v	k7c6
kadučním	kaduční	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zákaz	zákaz	k1gInSc1
konkurence	konkurence	k1gFnSc2
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
omezuje	omezovat	k5eAaImIp3nS
působení	působení	k1gNnSc4
jednatele	jednatel	k1gMnSc2
ve	v	k7c6
společnostech	společnost	k1gFnPc6
s	s	k7c7
obdobným	obdobný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společenská	společenský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
může	moct	k5eAaImIp3nS
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zákaz	zákaz	k1gInSc1
konkurence	konkurence	k1gFnSc2
vztahuje	vztahovat	k5eAaImIp3nS
i	i	k9
na	na	k7c4
společníky	společník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Povinnost	povinnost	k1gFnSc1
loajality	loajalita	k1gFnSc2
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Povinností	povinnost	k1gFnSc7
společníka	společník	k1gMnSc2
je	být	k5eAaImIp3nS
chovat	chovat	k5eAaImF
se	se	k3xPyFc4
ke	k	k7c3
společnosti	společnost	k1gFnSc3
čestně	čestně	k6eAd1
a	a	k8xC
zachovávat	zachovávat	k5eAaImF
její	její	k3xOp3gInSc4
vnitřní	vnitřní	k2eAgInSc4d1
řád	řád	k1gInSc4
(	(	kIx(
<g/>
zejména	zejména	k9
společenskou	společenský	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dějinný	dějinný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
byla	být	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
uzákoněna	uzákoněn	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
forma	forma	k1gFnSc1
obchodní	obchodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
roku	rok	k1gInSc2
1892	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
německým	německý	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
byl	být	k5eAaImAgInS
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
přijat	přijmout	k5eAaPmNgInS
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1906	#num#	k4
zákon	zákon	k1gInSc1
č.	č.	k?
58	#num#	k4
<g/>
/	/	kIx~
<g/>
1906	#num#	k4
ř.	ř.	k?
z.	z.	k?
o	o	k7c6
společnostech	společnost	k1gFnPc6
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
platil	platit	k5eAaImAgInS
i	i	k9
v	v	k7c6
Československu	Československo	k1gNnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
přestala	přestat	k5eAaPmAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
existovat	existovat	k5eAaImF
<g/>
;	;	kIx,
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
platí	platit	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
se	se	k3xPyFc4
do	do	k7c2
československého	československý	k2eAgInSc2d1
právního	právní	k2eAgInSc2d1
řádu	řád	k1gInSc2
vrátila	vrátit	k5eAaPmAgFnS
až	až	k9
novelou	novela	k1gFnSc7
hospodářského	hospodářský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
upravena	upravit	k5eAaPmNgFnS
zákonem	zákon	k1gInSc7
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
,	,	kIx,
obecné	obecný	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
právnické	právnický	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
pak	pak	k6eAd1
občanským	občanský	k2eAgInSc7d1
zákoníkem	zákoník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
upravena	upravit	k5eAaPmNgFnS
zákonem	zákon	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Chile	Chile	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Belgii	Belgie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
uzákoněna	uzákonit	k5eAaPmNgFnS
rozšířením	rozšíření	k1gNnSc7
územní	územní	k2eAgFnSc2d1
působnosti	působnost	k1gFnSc2
zákona	zákon	k1gInSc2
č.	č.	k?
58	#num#	k4
<g/>
/	/	kIx~
<g/>
1906	#num#	k4
ř.	ř.	k?
z.	z.	k?
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Změny	změna	k1gFnPc1
ve	v	k7c6
světle	světlo	k1gNnSc6
zákona	zákon	k1gInSc2
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
</s>
<s>
Způsob	způsob	k1gInSc1
zakládání	zakládání	k1gNnSc1
a	a	k8xC
chod	chod	k1gInSc1
společnosti	společnost	k1gFnSc2
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgInSc7d1
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
změnil	změnit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
účinný	účinný	k2eAgMnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
odstranil	odstranit	k5eAaPmAgInS
povinnost	povinnost	k1gFnSc4
vytvoření	vytvoření	k1gNnSc2
základního	základní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
200	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
namísto	namísto	k7c2
něj	on	k3xPp3gMnSc2
zavedl	zavést	k5eAaPmAgInS
minimální	minimální	k2eAgInSc1d1
vklad	vklad	k1gInSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
1	#num#	k4
Kč	Kč	kA
(	(	kIx(
<g/>
§	§	k?
142	#num#	k4
ZOK	ZOK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
významnou	významný	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
vytvořit	vytvořit	k5eAaPmF
více	hodně	k6eAd2
druhů	druh	k1gInPc2
podílů	podíl	k1gInPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
je	být	k5eAaImIp3nS
spjato	spjat	k2eAgNnSc1d1
více	hodně	k6eAd2
druhů	druh	k1gInPc2
práv	právo	k1gNnPc2
a	a	k8xC
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Podíly	podíl	k1gInPc1
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
vtělit	vtělit	k5eAaPmF
do	do	k7c2
volně	volně	k6eAd1
převoditelného	převoditelný	k2eAgInSc2d1
cenného	cenný	k2eAgInSc2d1
papíru	papír	k1gInSc2
–	–	k?
kmenového	kmenový	k2eAgInSc2d1
listu	list	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednatelem	jednatel	k1gMnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nově	nově	k6eAd1
také	také	k9
právnická	právnický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
povinnost	povinnost	k1gFnSc1
zřizovat	zřizovat	k5eAaImF
rezervní	rezervní	k2eAgInSc4d1
fond	fond	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Označení	označení	k1gNnSc1
entity	entita	k1gFnSc2
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
StátNázev	StátNázet	k5eAaPmDgMnS
subjektuZkratka	subjektuZkratka	k1gFnSc1
</s>
<s>
USA	USA	kA
USA	USA	kA
</s>
<s>
Limited	limited	k2eAgInPc1d1
liability	liabilit	k1gInPc1
companyLLC	companyLLC	k?
</s>
<s>
Privately	Privatela	k1gFnPc1
held	helda	k1gFnPc2
company	compana	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Private	Privat	k1gMnSc5
company	compan	k1gMnPc4
limited	limited	k2eAgInSc1d1
by	by	kYmCp3nS
shares	shares	k1gInSc1
</s>
<s>
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc2
</s>
<s>
Hongkong	Hongkong	k1gInSc1
Hongkong	Hongkong	k1gInSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
Irsko	Irsko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Besloten	Besloten	k2eAgInSc1d1
vennootschap	vennootschap	k1gInSc1
met	meta	k1gFnPc2
beperkte	beperkt	k1gInSc5
aansprakelijkheid	aansprakelijkheida	k1gFnPc2
</s>
<s>
bv	bv	k?
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
bvba	bvba	k6eAd1
</s>
<s>
Société	Sociétý	k2eAgInPc1d1
Privée	Privé	k1gInPc1
à	à	k?
Responsabilité	Responsabilitý	k2eAgFnSc2d1
LimitéeSPRL	LimitéeSPRL	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Société	Sociéta	k1gMnPc1
à	à	k?
responsabilité	responsabilitý	k2eAgMnPc4d1
limitée	limité	k1gMnPc4
</s>
<s>
SARL	SARL	kA
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
società	società	k?
a	a	k8xC
garanzia	garanzius	k1gMnSc2
limitata	limitat	k1gMnSc2
</s>
<s>
Gesellschaft	Gesellschaft	k1gMnSc1
mit	mit	k?
beschränkter	beschränkter	k1gMnSc1
Haftung	Haftung	k1gMnSc1
</s>
<s>
GmbH	GmbH	k?
/	/	kIx~
GesmbH	GesmbH	k1gFnSc1
/	/	kIx~
Ges	ges	k1gNnSc1
<g/>
.	.	kIx.
<g/>
m.	m.	k?
<g/>
b.H.	b.H.	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Unternehmergesellschaft	Unternehmergesellschaft	k1gInSc1
(	(	kIx(
<g/>
haftungsbeschränkt	haftungsbeschränkt	k1gInSc1
<g/>
)	)	kIx)
<g/>
UG	UG	kA
(	(	kIx(
<g/>
haftungsbeschränkt	haftungsbeschränkt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
ČeskoSpolečnost	ČeskoSpolečnost	k1gFnSc4
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
</s>
<s>
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
SlovenskoSpoločnosť	SlovenskoSpoločnosť	k1gFnSc2
s	s	k7c7
ručením	ručení	k1gNnSc7
obmedzeným	obmedzený	k2eAgNnSc7d1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
MaďarskoKorlátolt	MaďarskoKorlátolt	k1gMnSc1
felelősségű	felelősségű	k?
társaságkft	társaságkft	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Polsko	Polsko	k1gNnSc1
PolskoSpółka	PolskoSpółek	k1gMnSc2
z	z	k7c2
ograniczoną	ograniczoną	k?
odpowiedzialnościąsp	odpowiedzialnościąsp	k1gInSc1
<g/>
.	.	kIx.
z	z	k7c2
o.o.	o.o.	k?
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Društvo	Društvo	k6eAd1
s	s	k7c7
ograničenom	ograničenom	k1gInSc1
odgovornošću	odgovornošć	k1gMnSc6
</s>
<s>
d.o.o.	d.o.o.	k?
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
Severní	severní	k2eAgNnSc1d1
MakedonieDruštvo	MakedonieDruštvo	k1gNnSc1
so	so	k?
ograničena	ograničen	k2eAgFnSc1d1
odgovornost	odgovornost	k1gFnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
SlovinskoDružba	SlovinskoDružb	k1gMnSc2
z	z	k7c2
omejeno	omejen	k2eAgNnSc4d1
odgovornostjo	odgovornostja	k1gFnSc5
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Sociedad	Sociedad	k1gInSc1
de	de	k?
responsabilidad	responsabilidad	k1gInSc1
limitada	limitada	k1gFnSc1
</s>
<s>
SRL	SRL	kA
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Peru	Peru	k1gNnSc1
Peru	prát	k5eAaImIp1nS
</s>
<s>
Argentina	Argentina	k1gFnSc1
Argentina	Argentina	k1gFnSc1
</s>
<s>
Chile	Chile	k1gNnSc1
Chile	Chile	k1gNnSc2
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
ItálieSocietà	ItálieSocietà	k1gFnSc1
a	a	k8xC
responsabilità	responsabilità	k?
limitataSRL	limitataSRL	k?
/	/	kIx~
srl	srl	k?
<g/>
.	.	kIx.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
PortugalskoSociedade	PortugalskoSociedad	k1gInSc5
de	de	k?
responsabilidade	responsabilidást	k5eAaPmIp3nS
limitadaLda	limitadaLda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
BrazílieSociedade	BrazílieSociedad	k1gInSc5
limitadaLtda	limitadaLtdo	k1gNnPc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Lda	Lda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rusko	Rusko	k1gNnSc1
RuskoО	RuskoО	k1gFnSc2
с	с	k?
о	о	k?
о	о	k?
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
DánskoAnpartsselskabApS	DánskoAnpartsselskabApS	k1gFnSc2
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoPrivat	ŠvédskoPrivat	k1gFnSc2
aktiebolagAB	aktiebolagAB	k?
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Công	Công	k1gMnSc1
ty	ten	k3xDgFnPc4
trách	trách	k1gMnSc1
nhiệ	nhiệ	k6eAd1
hữ	hữ	k5eAaPmIp1nS
hạ	hạ	k1gInSc1
</s>
<s>
Ty	ten	k3xDgFnPc1
TNHH	TNHH	kA
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Yksityinen	Yksityinen	k1gInSc1
<g/>
)	)	kIx)
osakeyhtiöOY	osakeyhtiöOY	k?
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieProprietary	AustrálieProprietara	k1gFnSc2
limited	limited	k2eAgFnSc2d1
companyPty	companyPta	k1gFnSc2
Ltd	ltd	kA
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
MalajsieSyarikat	MalajsieSyarikat	k1gMnSc1
Sendirian	Sendirian	k1gMnSc1
BerhadSdn	BerhadSdn	k1gMnSc1
Bhd	Bhd	k1gMnSc1
</s>
<s>
Singapur	Singapur	k1gInSc1
SingapurPrivate	SingapurPrivat	k1gInSc5
limited	limited	k2eAgMnPc4d1
companyPte	companyPte	k5eAaPmIp2nP
Ltd	ltd	kA
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
EstonskoOsaühingOÜ	EstonskoOsaühingOÜ	k1gFnSc2
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
LotyšskoSabiedrī	LotyšskoSabiedrī	k1gMnSc1
ar	ar	k1gInSc4
ierobežotu	ierobežot	k1gInSc2
atbildī	atbildī	k?
</s>
<s>
Litva	Litva	k1gFnSc1
LitvaUždaroji	LitvaUždaroj	k1gInSc3
akcinė	akcinė	k?
bendrovė	bendrovė	k?
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc4
UzbekistánMasuliyati	UzbekistánMasuliyat	k1gMnPc1
Cheklangan	Cheklangan	k1gMnSc1
JamiyatMCHJ	JamiyatMCHJ	k1gMnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
RumunskoSocietate	RumunskoSocietat	k1gInSc5
cu	cu	k?
Răspundere	Răspunder	k1gMnSc5
LimitatăSRL	LimitatăSRL	k1gMnSc5
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
BulharskoД	BulharskoД	k1gFnSc2
с	с	k?
о	о	k?
о	о	k?
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
UkrajinaТ	UkrajinaТ	k1gFnSc2
з	з	k?
о	о	k?
в	в	k?
</s>
<s>
Albánie	Albánie	k1gFnSc1
AlbánieShoqëria	AlbánieShoqërium	k1gNnSc2
Me	Me	k1gFnSc2
Përgjegjësi	Përgjegjës	k1gMnSc3
Të	Të	k1gMnSc3
Kufizuarshpk	Kufizuarshpk	k1gInSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
se	se	k3xPyFc4
sice	sice	k8xC
za	za	k7c7
tečkami	tečka	k1gFnPc7
i	i	k8xC
ve	v	k7c6
zkratkách	zkratka	k1gFnPc6
vždy	vždy	k6eAd1
píšou	psát	k5eAaImIp3nP
mezery	mezera	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
zákon	zákon	k1gInSc1
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
ustanovení	ustanovení	k1gNnSc6
§	§	k?
132	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
používá	používat	k5eAaImIp3nS
pouze	pouze	k6eAd1
zkratky	zkratka	k1gFnPc1
bez	bez	k7c2
mezer	mezera	k1gFnPc2
<g/>
;	;	kIx,
v	v	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
dřívějšího	dřívější	k2eAgInSc2d1
obchodního	obchodní	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
,	,	kIx,
platného	platný	k2eAgInSc2d1
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
ustanovení	ustanovení	k1gNnSc6
§	§	k?
107	#num#	k4
tyto	tento	k3xDgFnPc4
zkratky	zkratka	k1gFnPc4
uváděl	uvádět	k5eAaImAgMnS
s	s	k7c7
mezerami	mezera	k1gFnPc7
„	„	k?
<g/>
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
o.	o.	k?
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
“	“	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
90	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
(	(	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
"	"	kIx"
<g/>
ZOK	ZOK	kA
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
1	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Veřejný	veřejný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
–	–	k?
Zkratky	zkratka	k1gFnSc2
čistě	čistě	k6eAd1
grafické	grafický	k2eAgFnSc2d1
<g/>
↑	↑	k?
§	§	k?
132	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
142	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
8	#num#	k4
<g/>
,	,	kIx,
§	§	k?
776	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
23	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
§	§	k?
148	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
143	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
17	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
18	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
89	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
(	(	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
"	"	kIx"
<g/>
OZ	OZ	kA
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
126	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
132	#num#	k4
ZOK	ZOK	kA
<g/>
,	,	kIx,
§	§	k?
1872	#num#	k4
OZ	OZ	kA
<g/>
↑	↑	k?
§	§	k?
181	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
190	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
11	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
194-195	194-195	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
304	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
veřejných	veřejný	k2eAgInPc6d1
rejstřících	rejstřík	k1gInPc6
<g />
.	.	kIx.
</s>
<s hack="1">
právnických	právnický	k2eAgFnPc2d1
a	a	k8xC
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
"	"	kIx"
<g/>
rejstříkový	rejstříkový	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
25	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
g	g	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
122	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
208-210	208-210	k4
zákona	zákon	k1gInSc2
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
↑	↑	k?
§	§	k?
42	#num#	k4
<g/>
,	,	kIx,
§	§	k?
211	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
42	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
213-215	213-215	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
34	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
,	,	kIx,
§	§	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
,	,	kIx,
161	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
36	#num#	k4
<g/>
,	,	kIx,
§	§	k?
213-215	213-215	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
ŠTENGLOVÁ	ŠTENGLOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
;	;	kIx,
CILEČEK	CILEČEK	kA
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
;	;	kIx,
KUHN	KUHN	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
ŠUK	šuk	k0
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentář	komentář	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7400	#num#	k4
<g/>
-	-	kIx~
<g/>
480	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
37	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
168	#num#	k4
<g/>
-	-	kIx~
<g/>
169	#num#	k4
<g/>
,	,	kIx,
§	§	k?
173	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
155-156	155-156	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
157-160	157-160	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
<g />
.	.	kIx.
</s>
<s hack="1">
187	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
23	#num#	k4
<g/>
,	,	kIx,
§	§	k?
150	#num#	k4
<g/>
-	-	kIx~
<g/>
151	#num#	k4
<g/>
,	,	kIx,
§	§	k?
169	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
§	§	k?
171	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
b	b	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
187	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
d	d	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
239	#num#	k4
OZ	OZ	kA
<g/>
↑	↑	k?
162-166	162-166	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
199	#num#	k4
ZOK	ZOK	kA
<g/>
↑	↑	k?
§	§	k?
212	#num#	k4
OZ	OZ	kA
<g/>
↑	↑	k?
Zlatakoruna	Zlatakoruna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
<g/>
:	:	kIx,
Víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
že	že	k8xS
základní	základní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
1	#num#	k4
<g/>
,	,	kIx,
<g/>
-	-	kIx~
Kč	Kč	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
↑	↑	k?
RANDA	RANDA	kA
HAVEL	Havel	k1gMnSc1
LEGAL	LEGAL	kA
<g/>
:	:	kIx,
Rekodifikace	Rekodifikace	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
společnost	společnost	k1gFnSc4
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Část	část	k1gFnSc1
zákona	zákon	k1gInSc2
o	o	k7c6
obchodních	obchodní	k2eAgFnPc6d1
korporacích	korporace	k1gFnPc6
<g/>
,	,	kIx,
věnující	věnující	k2eAgFnPc1d1
se	se	k3xPyFc4
společnosti	společnost	k1gFnSc2
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
<g/>
,	,	kIx,
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Právní	právní	k2eAgFnSc2d1
formy	forma	k1gFnSc2
podnikání	podnikání	k1gNnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
Obchodní	obchodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Evropské	evropský	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Evropská	evropský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
·	·	k?
Komanditní	komanditní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
·	·	k?
Veřejná	veřejný	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
·	·	k?
Zájmové	zájmový	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
Evropské	evropský	k2eAgNnSc1d1
hospodářské	hospodářský	k2eAgNnSc1d1
zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
podnikání	podnikání	k1gNnSc2
</s>
<s>
Tichá	tichý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
·	·	k?
Societa	societa	k1gFnSc1
·	·	k?
Holding	holding	k1gInSc1
·	·	k?
Joint	Joint	k1gInSc1
venture	ventur	k1gMnSc5
·	·	k?
Družstvo	družstvo	k1gNnSc1
(	(	kIx(
<g/>
Evropská	evropský	k2eAgFnSc1d1
družstevní	družstevní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Obchodní	obchodní	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
·	·	k?
Osoba	osoba	k1gFnSc1
samostatně	samostatně	k6eAd1
výdělečně	výdělečně	k6eAd1
činná	činný	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Právo	právo	k1gNnSc1
|	|	kIx~
Ekonomie	ekonomie	k1gFnSc1
</s>
