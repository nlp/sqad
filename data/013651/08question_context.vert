<s>
Společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
forem	forma	k1gFnPc2
obchodních	obchodní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
lze	lze	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
za	za	k7c7
účelem	účel	k1gInSc7
podnikání	podnikání	k1gNnSc2
založit	založit	k5eAaPmF
<g/>
.	.	kIx.
</s>