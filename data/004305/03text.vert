<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
je	být	k5eAaImIp3nS	být
vozidlo	vozidlo	k1gNnSc4	vozidlo
nebo	nebo	k8xC	nebo
vlak	vlak	k1gInSc4	vlak
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
označoval	označovat	k5eAaImAgInS	označovat
samotnou	samotný	k2eAgFnSc4d1	samotná
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
metonymickým	metonymický	k2eAgInSc7d1	metonymický
přenosem	přenos	k1gInSc7	přenos
změnil	změnit	k5eAaPmAgInS	změnit
význam	význam	k1gInSc4	význam
na	na	k7c4	na
označení	označení	k1gNnSc4	označení
kolejového	kolejový	k2eAgNnSc2d1	kolejové
vozidla	vozidlo	k1gNnSc2	vozidlo
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
na	na	k7c4	na
podniky	podnik	k1gInPc4	podnik
provozující	provozující	k2eAgNnSc1d1	provozující
takové	takový	k3xDgFnPc4	takový
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
tramway	tramwaa	k1gFnSc2	tramwaa
(	(	kIx(	(
<g/>
tram	tram	k1gFnSc2	tram
označovalo	označovat	k5eAaImAgNnS	označovat
kolej	kolej	k1gFnSc4	kolej
nebo	nebo	k8xC	nebo
důlní	důlní	k2eAgInSc4d1	důlní
vozík	vozík	k1gInSc4	vozík
<g/>
,	,	kIx,	,
way	way	k?	way
cestu	cesta	k1gFnSc4	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
užívalo	užívat	k5eAaImAgNnS	užívat
u	u	k7c2	u
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
tram	tram	k1gFnSc2	tram
pro	pro	k7c4	pro
kolej	kolej	k1gFnSc4	kolej
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
saských	saský	k2eAgInPc6d1	saský
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
podlahy	podlaha	k1gFnPc4	podlaha
kladeny	klást	k5eAaImNgInP	klást
trámy	trám	k1gInPc1	trám
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
přepravy	přeprava	k1gFnSc2	přeprava
důlních	důlní	k2eAgInPc2d1	důlní
vozíků	vozík	k1gInPc2	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
trámů	trám	k1gInPc2	trám
byla	být	k5eAaImAgNnP	být
posléze	posléze	k6eAd1	posléze
vyhloubena	vyhlouben	k2eAgNnPc1d1	vyhloubeno
drážka	drážka	k1gNnPc1	drážka
a	a	k8xC	a
vozíky	vozík	k1gInPc1	vozík
byly	být	k5eAaImAgInP	být
opatřeny	opatřit	k5eAaPmNgInP	opatřit
kolíkem	kolík	k1gInSc7	kolík
klouzajícím	klouzající	k2eAgInSc7d1	klouzající
v	v	k7c6	v
drážce	drážka	k1gFnSc6	drážka
pro	pro	k7c4	pro
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
vedení	vedení	k1gNnSc4	vedení
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předchůdců	předchůdce	k1gMnPc2	předchůdce
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
koněspřežného	koněspřežný	k2eAgInSc2d1	koněspřežný
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
přenesl	přenést	k5eAaPmAgInS	přenést
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
na	na	k7c4	na
parní	parní	k2eAgFnSc4d1	parní
či	či	k8xC	či
jinou	jiný	k2eAgFnSc4d1	jiná
trakci	trakce	k1gFnSc4	trakce
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
s	s	k7c7	s
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
trakcí	trakce	k1gFnSc7	trakce
s	s	k7c7	s
vrchním	vrchní	k2eAgNnSc7d1	vrchní
trolejovým	trolejový	k2eAgNnSc7d1	trolejové
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
tramvaj	tramvaj	k1gFnSc1	tramvaj
(	(	kIx(	(
<g/>
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
)	)	kIx)	)
nazývala	nazývat	k5eAaImAgNnP	nazývat
pouliční	pouliční	k2eAgFnSc1d1	pouliční
nebo	nebo	k8xC	nebo
elektrická	elektrický	k2eAgFnSc1d1	elektrická
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
elektrika	elektrika	k1gFnSc1	elektrika
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
električka	električka	k1gFnSc1	električka
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
spisovné	spisovný	k2eAgFnSc6d1	spisovná
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
hantecu	hantecus	k1gInSc6	hantecus
se	se	k3xPyFc4	se
tramvaji	tramvaj	k1gFnSc3	tramvaj
říká	říkat	k5eAaImIp3nS	říkat
šalina	šalina	k1gFnSc1	šalina
(	(	kIx(	(
<g/>
zkomolením	zkomolení	k1gNnSc7	zkomolení
německého	německý	k2eAgNnSc2d1	německé
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
elektrische	elektrische	k1gFnSc1	elektrische
Linie	linie	k1gFnSc1	linie
[	[	kIx(	[
<g/>
elektryše	elektryše	k1gFnSc1	elektryše
línye	línye	k1gFnSc1	línye
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
šmirgl	šmirgl	k?	šmirgl
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
výraz	výraz	k1gInSc4	výraz
tramvajka	tramvajka	k1gFnSc1	tramvajka
<g/>
.	.	kIx.	.
větší	veliký	k2eAgFnSc1d2	veliký
přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
oproti	oproti	k7c3	oproti
autobusu	autobus	k1gInSc3	autobus
(	(	kIx(	(
<g/>
i	i	k8xC	i
metrobusu	metrobus	k1gInSc2	metrobus
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
případně	případně	k6eAd1	případně
trolejbusu	trolejbus	k1gInSc2	trolejbus
městská	městský	k2eAgFnSc1d1	městská
tramvaj	tramvaj	k1gFnSc1	tramvaj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provozována	provozovat	k5eAaImNgFnS	provozovat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
běžné	běžný	k2eAgFnSc2d1	běžná
uliční	uliční	k2eAgFnSc2d1	uliční
sítě	síť	k1gFnSc2	síť
i	i	k9	i
na	na	k7c6	na
samostatném	samostatný	k2eAgNnSc6d1	samostatné
tělese	těleso	k1gNnSc6	těleso
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k8xC	jako
meziměstský	meziměstský	k2eAgInSc4d1	meziměstský
prostředek	prostředek	k1gInSc4	prostředek
vnější	vnější	k2eAgFnSc2d1	vnější
dopravy	doprava	k1gFnSc2	doprava
u	u	k7c2	u
městských	městský	k2eAgFnPc2d1	městská
konurbací	konurbace	k1gFnPc2	konurbace
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Jablonec	Jablonec	k1gInSc1	Jablonec
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
-	-	kIx~	-
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
městská	městský	k2eAgFnSc1d1	městská
tramvaj	tramvaj	k1gFnSc1	tramvaj
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
průchodnost	průchodnost	k1gFnSc4	průchodnost
hustou	hustý	k2eAgFnSc7d1	hustá
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nižší	nízký	k2eAgFnSc1d2	nižší
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
autobusy	autobus	k1gInPc7	autobus
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tramvaj	tramvaj	k1gFnSc1	tramvaj
provozována	provozován	k2eAgFnSc1d1	provozována
na	na	k7c6	na
samostatném	samostatný	k2eAgInSc6d1	samostatný
nebo	nebo	k8xC	nebo
odděleném	oddělený	k2eAgNnSc6d1	oddělené
tělese	těleso	k1gNnSc6	těleso
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
dopravní	dopravní	k2eAgFnSc4d1	dopravní
situaci	situace	k1gFnSc4	situace
(	(	kIx(	(
<g/>
nezdržují	zdržovat	k5eNaImIp3nP	zdržovat
ji	on	k3xPp3gFnSc4	on
auta	auto	k1gNnPc1	auto
<g/>
)	)	kIx)	)
tramvaj	tramvaj	k1gFnSc1	tramvaj
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
rychlodrážní	rychlodrážní	k2eAgInSc4d1	rychlodrážní
provoz	provoz	k1gInSc4	provoz
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
rychlá	rychlý	k2eAgFnSc1d1	rychlá
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
)	)	kIx)	)
a	a	k8xC	a
podzemní	podzemní	k2eAgInSc1d1	podzemní
provoz	provoz	k1gInSc1	provoz
(	(	kIx(	(
<g/>
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
současný	současný	k2eAgInSc1d1	současný
provoz	provoz	k1gInSc1	provoz
městské	městský	k2eAgFnSc2d1	městská
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgFnSc2d1	rychlá
a	a	k8xC	a
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
tramvaje	tramvaj	k1gFnSc2	tramvaj
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
sítě	síť	k1gFnSc2	síť
tramvaj	tramvaj	k1gFnSc1	tramvaj
je	být	k5eAaImIp3nS	být
ideálním	ideální	k2eAgInSc7d1	ideální
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
preferenčních	preferenční	k2eAgNnPc2d1	preferenční
opatření	opatření	k1gNnPc2	opatření
na	na	k7c6	na
dopravní	dopravní	k2eAgFnSc6d1	dopravní
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
signálních	signální	k2eAgInPc2d1	signální
plánů	plán	k1gInPc2	plán
světelné	světelný	k2eAgFnSc2d1	světelná
signalizace	signalizace	k1gFnSc2	signalizace
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
nižší	nízký	k2eAgFnSc1d2	nižší
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
hlučnost	hlučnost	k1gFnSc1	hlučnost
přepravy	přeprava	k1gFnSc2	přeprava
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
umí	umět	k5eAaImIp3nS	umět
lépe	dobře	k6eAd2	dobře
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vyřešit	vyřešit	k5eAaPmF	vyřešit
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
nízkou	nízký	k2eAgFnSc4d1	nízká
podlahu	podlaha	k1gFnSc4	podlaha
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
vozidla	vozidlo	k1gNnSc2	vozidlo
díky	díky	k7c3	díky
konstrukci	konstrukce	k1gFnSc3	konstrukce
nezávislého	závislý	k2eNgInSc2d1	nezávislý
pohonu	pohon	k1gInSc2	pohon
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přátelštější	přátelský	k2eAgInSc4d2	přátelštější
k	k	k7c3	k
handicapovaným	handicapovaný	k2eAgFnPc3d1	handicapovaná
osobám	osoba	k1gFnPc3	osoba
větší	veliký	k2eAgFnSc1d2	veliký
setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
valivý	valivý	k2eAgInSc1d1	valivý
odpor	odpor	k1gInSc1	odpor
možnost	možnost	k1gFnSc4	možnost
zvýhodnění	zvýhodnění	k1gNnPc2	zvýhodnění
před	před	k7c7	před
jinými	jiný	k2eAgInPc7d1	jiný
městskými	městský	k2eAgInPc7d1	městský
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
oproti	oproti	k7c3	oproti
dieselovému	dieselový	k2eAgInSc3d1	dieselový
pohonu	pohon	k1gInSc3	pohon
nižší	nízký	k2eAgNnSc1d2	nižší
znečištění	znečištění	k1gNnSc1	znečištění
<g />
.	.	kIx.	.
</s>
<s>
emisemi	emise	k1gFnPc7	emise
spalování	spalování	k1gNnSc2	spalování
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
provozu	provoz	k1gInSc2	provoz
větší	veliký	k2eAgFnSc1d2	veliký
energetická	energetický	k2eAgFnSc1d1	energetická
účinnost	účinnost	k1gFnSc1	účinnost
(	(	kIx(	(
<g/>
I	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
energie	energie	k1gFnSc1	energie
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
spálením	spálení	k1gNnSc7	spálení
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
energetická	energetický	k2eAgFnSc1d1	energetická
účinnost	účinnost	k1gFnSc1	účinnost
systému	systém	k1gInSc2	systém
palivo-elektrárna-dálkové	palivolektrárnaálek	k1gMnPc1	palivo-elektrárna-dálek
vedení-měnírna-trolejové	vedeníěnírnarolejový	k2eAgInPc1d1	vedení-měnírna-trolejový
vedení-elektromotor	vedenílektromotor	k1gInSc1	vedení-elektromotor
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
energetická	energetický	k2eAgFnSc1d1	energetická
účinnost	účinnost	k1gFnSc1	účinnost
systému	systém	k1gInSc2	systém
palivo-spalovací	palivopalovací	k2eAgInSc1d1	palivo-spalovací
motor	motor	k1gInSc1	motor
autobusu	autobus	k1gInSc2	autobus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
nejsou	být	k5eNaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgFnPc4	žádný
energetické	energetický	k2eAgFnPc4d1	energetická
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vozové	vozový	k2eAgFnPc1d1	vozová
jednotky	jednotka	k1gFnPc1	jednotka
stojí	stát	k5eAaImIp3nP	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
zastávkách	zastávka	k1gFnPc6	zastávka
a	a	k8xC	a
na	na	k7c6	na
křižovatkách	křižovatka	k1gFnPc6	křižovatka
úspora	úspora	k1gFnSc1	úspora
energie	energie	k1gFnSc1	energie
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
část	část	k1gFnSc1	část
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
napájecí	napájecí	k2eAgFnSc2d1	napájecí
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
rekuperace	rekuperace	k1gFnPc1	rekuperace
<g/>
)	)	kIx)	)
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
brzdy	brzda	k1gFnPc1	brzda
šetří	šetřit	k5eAaImIp3nP	šetřit
mechanické	mechanický	k2eAgFnPc1d1	mechanická
brzdy	brzda	k1gFnPc1	brzda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snižují	snižovat	k5eAaImIp3nP	snižovat
provozní	provozní	k2eAgInPc1d1	provozní
náklady	náklad	k1gInPc1	náklad
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
účinnější	účinný	k2eAgInSc4d2	účinnější
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
a	a	k8xC	a
náledí	náledí	k1gNnSc6	náledí
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
za	za	k7c2	za
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
náledí	náledí	k1gNnSc1	náledí
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
problematicky	problematicky	k6eAd1	problematicky
spolehlivým	spolehlivý	k2eAgInSc7d1	spolehlivý
autobusem	autobus	k1gInSc7	autobus
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
řízení	řízení	k1gNnSc4	řízení
(	(	kIx(	(
<g/>
netočí	točit	k5eNaImIp3nS	točit
se	se	k3xPyFc4	se
volantem	volant	k1gInSc7	volant
<g/>
,	,	kIx,	,
neparkuje	parkovat	k5eNaImIp3nS	parkovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nepředjíždí	předjíždět	k5eNaImIp3nS	předjíždět
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
menší	malý	k2eAgInPc4d2	menší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
řidiče	řidič	k1gMnPc4	řidič
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
odděleným	oddělený	k2eAgFnPc3d1	oddělená
kabinám	kabina	k1gFnPc3	kabina
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
<g />
.	.	kIx.	.
</s>
<s>
obtěžováni	obtěžován	k2eAgMnPc1d1	obtěžován
cestujícími	cestující	k1gMnPc7	cestující
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
klimatizace	klimatizace	k1gFnSc1	klimatizace
apod.	apod.	kA	apod.
bezpečnější	bezpečný	k2eAgInSc4d2	bezpečnější
prostředek	prostředek	k1gInSc4	prostředek
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
pěší	pěší	k2eAgFnSc2d1	pěší
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
díky	díky	k7c3	díky
ochranným	ochranný	k2eAgInPc3d1	ochranný
prostředkům	prostředek	k1gInPc3	prostředek
<g/>
,	,	kIx,	,
znemožňujícím	znemožňující	k2eAgNnSc7d1	znemožňující
pád	pád	k1gInSc4	pád
osob	osoba	k1gFnPc2	osoba
pod	pod	k7c4	pod
vozidlo	vozidlo	k1gNnSc4	vozidlo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
cestujících	cestující	k1gMnPc2	cestující
uvnitř	uvnitř	k7c2	uvnitř
tramvaje	tramvaj	k1gFnSc2	tramvaj
díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
masivní	masivní	k2eAgFnSc3d1	masivní
konstrukci	konstrukce	k1gFnSc3	konstrukce
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
tramvaje	tramvaj	k1gFnSc2	tramvaj
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
i	i	k9	i
její	její	k3xOp3gNnSc1	její
směrové	směrový	k2eAgNnSc1d1	směrové
vedení	vedení	k1gNnSc1	vedení
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
kolejnicích	kolejnice	k1gFnPc6	kolejnice
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
aspekt	aspekt	k1gInSc1	aspekt
je	být	k5eAaImIp3nS	být
určitou	určitý	k2eAgFnSc7d1	určitá
komplikací	komplikace	k1gFnSc7	komplikace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
účastníků	účastník	k1gMnPc2	účastník
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
řidič	řidič	k1gMnSc1	řidič
se	se	k3xPyFc4	se
neumí	umět	k5eNaImIp3nS	umět
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
)	)	kIx)	)
nižší	nízký	k2eAgFnSc4d2	nižší
provozní	provozní	k2eAgFnSc4d1	provozní
náročnost	náročnost	k1gFnSc4	náročnost
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
cestujícího	cestující	k1gMnSc4	cestující
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
započtení	započtení	k1gNnSc2	započtení
investičních	investiční	k2eAgInPc2d1	investiční
nákladů	náklad	k1gInPc2	náklad
<g/>
)	)	kIx)	)
možnost	možnost	k1gFnSc1	možnost
kombinace	kombinace	k1gFnSc1	kombinace
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
(	(	kIx(	(
<g/>
tram-train	tramrain	k1gInSc1	tram-train
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
větší	veliký	k2eAgNnSc4d2	veliký
pohodlí	pohodlí	k1gNnSc4	pohodlí
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
autobusem	autobus	k1gInSc7	autobus
i	i	k8xC	i
trolejbusem	trolejbus	k1gInSc7	trolejbus
prokázaný	prokázaný	k2eAgInSc4d1	prokázaný
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
počet	počet	k1gInSc4	počet
cestujících	cestující	k1gMnPc2	cestující
-	-	kIx~	-
vždy	vždy	k6eAd1	vždy
když	když	k8xS	když
se	se	k3xPyFc4	se
tramvaje	tramvaj	k1gFnSc2	tramvaj
zrušili	zrušit	k5eAaPmAgMnP	zrušit
<g/>
,	,	kIx,	,
cestujích	cestují	k1gNnPc6	cestují
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
obnovily	obnovit	k5eAaPmAgFnP	obnovit
<g/>
,	,	kIx,	,
pasažérů	pasažér	k1gMnPc2	pasažér
přibylo	přibýt	k5eAaPmAgNnS	přibýt
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
zrušení	zrušení	k1gNnSc6	zrušení
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
začala	začít	k5eAaPmAgFnS	začít
jezdit	jezdit	k5eAaImF	jezdit
auty	auto	k1gNnPc7	auto
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
obnovení	obnovení	k1gNnSc6	obnovení
se	se	k3xPyFc4	se
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
v	v	k7c6	v
MHD	MHD	kA	MHD
zvedl	zvednout	k5eAaPmAgMnS	zvednout
o	o	k7c4	o
43	[number]	k4	43
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
snadnější	snadný	k2eAgFnSc4d2	snazší
viditelnost	viditelnost	k1gFnSc4	viditelnost
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
autobusů	autobus	k1gInPc2	autobus
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
snadněji	snadno	k6eAd2	snadno
všimnou	všimnout	k5eAaPmIp3nP	všimnout
díky	díky	k7c3	díky
trolejím	trolej	k1gFnPc3	trolej
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolejím	kolej	k1gFnPc3	kolej
zajištěné	zajištěný	k2eAgFnSc2d1	zajištěná
MHD	MHD	kA	MHD
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
fakty	faktum	k1gNnPc7	faktum
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
na	na	k7c4	na
takto	takto	k6eAd1	takto
nákladná	nákladný	k2eAgFnSc1d1	nákladná
síť	síť	k1gFnSc1	síť
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
vyšší	vysoký	k2eAgNnSc4d2	vyšší
vytížení	vytížení	k1gNnSc4	vytížení
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
kratší	krátký	k2eAgInPc4d2	kratší
intervaly	interval	k1gInPc4	interval
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
tramvaj	tramvaj	k1gFnSc1	tramvaj
je	být	k5eAaImIp3nS	být
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
,,	,,	k?	,,
<g/>
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
nevidí	vidět	k5eNaImIp3nS	vidět
<g/>
''	''	k?	''
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
snadněji	snadno	k6eAd2	snadno
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
použít	použít	k5eAaPmF	použít
veřenou	veřený	k2eAgFnSc4d1	veřený
dopravu	doprava	k1gFnSc4	doprava
místo	místo	k7c2	místo
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
nižší	nízký	k2eAgInPc4d2	nižší
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
provoz	provoz	k1gInSc4	provoz
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
autobusů	autobus	k1gInPc2	autobus
to	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
kratším	krátký	k2eAgInPc3d2	kratší
intervalům	interval	k1gInPc3	interval
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zvýšených	zvýšený	k2eAgInPc2d1	zvýšený
přepravních	přepravní	k2eAgInPc2d1	přepravní
nároků	nárok	k1gInPc2	nárok
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
vlídnost	vlídnost	k1gFnSc1	vlídnost
k	k	k7c3	k
chodcům	chodec	k1gMnPc3	chodec
na	na	k7c6	na
pěších	pěší	k2eAgFnPc6d1	pěší
zónách	zóna	k1gFnPc6	zóna
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
autobusu	autobus	k1gInSc2	autobus
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
díky	díky	k7c3	díky
kolejím	kolej	k1gFnPc3	kolej
předpovědět	předpovědět	k5eAaPmF	předpovědět
pohyb	pohyb	k1gInSc1	pohyb
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
zeleně	zeleň	k1gFnSc2	zeleň
ve	v	k7c6	v
městech	město	k1gNnPc6	město
–	–	k?	–
pokud	pokud	k8xS	pokud
trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
mimo	mimo	k7c4	mimo
silnici	silnice	k1gFnSc4	silnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
zatravnit	zatravnit	k5eAaPmF	zatravnit
<g/>
.	.	kIx.	.
</s>
<s>
Zeleň	zeleň	k1gFnSc1	zeleň
navíc	navíc	k6eAd1	navíc
působí	působit	k5eAaImIp3nS	působit
přátelsky	přátelsky	k6eAd1	přátelsky
a	a	k8xC	a
esteticky	esteticky	k6eAd1	esteticky
<g/>
.	.	kIx.	.
špatná	špatný	k2eAgFnSc1d1	špatná
manévrovatelnost	manévrovatelnost	k1gFnSc1	manévrovatelnost
<g/>
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
kolejemi	kolej	k1gFnPc7	kolej
–	–	k?	–
nemožnost	nemožnost	k1gFnSc4	nemožnost
objíždět	objíždět	k5eAaImF	objíždět
vadný	vadný	k2eAgInSc4d1	vadný
vlak	vlak	k1gInSc4	vlak
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc4d1	jiná
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
možnost	možnost	k1gFnSc1	možnost
odklonů	odklon	k1gInPc2	odklon
a	a	k8xC	a
objížděk	objížďka	k1gFnPc2	objížďka
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
střetů	střet	k1gInPc2	střet
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
,	,	kIx,	,
chodci	chodec	k1gMnPc7	chodec
a	a	k8xC	a
překážkami	překážka	k1gFnPc7	překážka
kvůli	kvůli	k7c3	kvůli
nemožnosti	nemožnost	k1gFnSc3	nemožnost
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc6d2	delší
brzdné	brzdný	k2eAgFnSc6d1	brzdná
dráze	dráha	k1gFnSc6	dráha
větší	veliký	k2eAgFnSc2d2	veliký
<g />
.	.	kIx.	.
</s>
<s>
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
delší	dlouhý	k2eAgFnSc1d2	delší
brzdná	brzdný	k2eAgFnSc1d1	brzdná
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
možnost	možnost	k1gFnSc4	možnost
použití	použití	k1gNnSc2	použití
magnetické	magnetický	k2eAgFnSc2d1	magnetická
brzdy	brzda	k1gFnSc2	brzda
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
pomalá	pomalý	k2eAgFnSc1d1	pomalá
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
tramvajových	tramvajový	k2eAgFnPc6d1	tramvajová
křižovatkách	křižovatka	k1gFnPc6	křižovatka
a	a	k8xC	a
v	v	k7c6	v
zatáčkách	zatáčka	k1gFnPc6	zatáčka
s	s	k7c7	s
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
auta	auto	k1gNnPc1	auto
toto	tento	k3xDgNnSc4	tento
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
projet	projet	k5eAaPmF	projet
rychleji	rychle	k6eAd2	rychle
riziko	riziko	k1gNnSc4	riziko
smyku	smyk	k1gInSc2	smyk
a	a	k8xC	a
skluzu	skluz	k1gInSc2	skluz
dané	daný	k2eAgFnSc2d1	daná
<g />
.	.	kIx.	.
</s>
<s>
kontaktem	kontakt	k1gInSc7	kontakt
kov	kov	k1gInSc1	kov
na	na	k7c4	na
kov	kov	k1gInSc4	kov
mezi	mezi	k7c7	mezi
kolem	kolo	k1gNnSc7	kolo
a	a	k8xC	a
kolejnicí	kolejnice	k1gFnSc7	kolejnice
vyšší	vysoký	k2eAgFnSc1d2	vyšší
prašnost	prašnost	k1gFnSc1	prašnost
vlivem	vlivem	k7c2	vlivem
rutinního	rutinní	k2eAgNnSc2d1	rutinní
užívání	užívání	k1gNnSc2	užívání
posypu	posypat	k5eAaPmIp1nS	posypat
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
smyku	smyk	k1gInSc2	smyk
a	a	k8xC	a
skluzu	skluz	k1gInSc2	skluz
větší	veliký	k2eAgFnSc4d2	veliký
hlučnost	hlučnost	k1gFnSc4	hlučnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
obloucích	oblouk	k1gInPc6	oblouk
větší	veliký	k2eAgNnPc4d2	veliký
ohrožování	ohrožování	k1gNnPc4	ohrožování
staveb	stavba	k1gFnPc2	stavba
vibracemi	vibrace	k1gFnPc7	vibrace
choulostivost	choulostivost	k1gFnSc4	choulostivost
na	na	k7c4	na
špatnou	špatný	k2eAgFnSc4d1	špatná
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
provozních	provozní	k2eAgInPc2d1	provozní
financích	finance	k1gFnPc6	finance
-	-	kIx~	-
zatímco	zatímco	k8xS	zatímco
i	i	k9	i
na	na	k7c6	na
špatném	špatný	k2eAgInSc6d1	špatný
asfaltovém	asfaltový	k2eAgInSc6d1	asfaltový
povrchu	povrch	k1gInSc6	povrch
lze	lze	k6eAd1	lze
jezdit	jezdit	k5eAaImF	jezdit
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
neudržované	udržovaný	k2eNgFnPc4d1	neudržovaná
koleje	kolej	k1gFnPc4	kolej
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
jet	jet	k5eAaImF	jet
plnou	plný	k2eAgFnSc7d1	plná
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgFnPc4	některý
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
tratě	trať	k1gFnPc4	trať
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jen	jen	k9	jen
asi	asi	k9	asi
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
(	(	kIx(	(
<g/>
při	při	k7c6	při
kvalitní	kvalitní	k2eAgFnSc6d1	kvalitní
údržbě	údržba	k1gFnSc6	údržba
se	se	k3xPyFc4	se
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
nahoru	nahoru	k6eAd1	nahoru
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
likvidaci	likvidace	k1gFnSc4	likvidace
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
<g/>
:	:	kIx,	:
nedostatek	nedostatek	k1gInSc1	nedostatek
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
<g/>
→	→	k?	→
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
údržba	údržba	k1gFnSc1	údržba
<g/>
→	→	k?	→
<g/>
nižší	nízký	k2eAgFnSc4d2	nižší
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
zastaralá	zastaralý	k2eAgNnPc4d1	zastaralé
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
→	→	k?	→
<g/>
úbytek	úbytek	k1gInSc4	úbytek
cestujících	cestující	k1gMnPc2	cestující
do	do	k7c2	do
aut	auto	k1gNnPc2	auto
<g/>
→	→	k?	→
<g/>
likvidace	likvidace	k1gFnSc2	likvidace
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
závislá	závislý	k2eAgFnSc1d1	závislá
trakce	trakce	k1gFnSc1	trakce
-	-	kIx~	-
zastavení	zastavení	k1gNnSc1	zastavení
provozu	provoz	k1gInSc2	provoz
při	při	k7c6	při
výpadku	výpadek	k1gInSc6	výpadek
napájení	napájení	k1gNnSc2	napájení
zadrátování	zadrátování	k1gNnSc2	zadrátování
veřejného	veřejný	k2eAgInSc2d1	veřejný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
zohyzdění	zohyzdění	k1gNnSc2	zohyzdění
<g />
.	.	kIx.	.
</s>
<s>
výhledu	výhled	k1gInSc2	výhled
na	na	k7c4	na
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
městská	městský	k2eAgNnPc1d1	Městské
panoramata	panorama	k1gNnPc1	panorama
<g/>
,	,	kIx,	,
ztížení	ztížení	k1gNnSc1	ztížení
průjezdu	průjezd	k1gInSc2	průjezd
vysokých	vysoký	k2eAgNnPc2d1	vysoké
silničních	silniční	k2eAgNnPc2d1	silniční
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
riziko	riziko	k1gNnSc1	riziko
poškození	poškození	k1gNnSc2	poškození
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
nerentabilnost	nerentabilnost	k1gFnSc4	nerentabilnost
tramvaje	tramvaj	k1gFnSc2	tramvaj
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
nízkých	nízký	k2eAgInPc2d1	nízký
přepravních	přepravní	k2eAgInPc2d1	přepravní
proudů	proud	k1gInPc2	proud
nízká	nízký	k2eAgFnSc1d1	nízká
provozní	provozní	k2eAgFnSc1d1	provozní
a	a	k8xC	a
prostorová	prostorový	k2eAgFnSc1d1	prostorová
flexibilita	flexibilita	k1gFnSc1	flexibilita
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
tramvajovém	tramvajový	k2eAgNnSc6d1	tramvajové
tělese	těleso	k1gNnSc6	těleso
tramvaj	tramvaj	k1gFnSc1	tramvaj
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
smyčková	smyčkový	k2eAgNnPc4d1	smyčkové
obratiště	obratiště	k1gNnPc4	obratiště
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
aplikována	aplikován	k2eAgFnSc1d1	aplikována
koncepce	koncepce	k1gFnSc1	koncepce
obratišť	obratiště	k1gNnPc2	obratiště
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
obousměrné	obousměrný	k2eAgInPc4d1	obousměrný
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
organizace	organizace	k1gFnSc1	organizace
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
prostoru	prostor	k1gInSc2	prostor
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
sezení	sezení	k1gNnSc4	sezení
v	v	k7c6	v
případě	případ	k1gInSc6	případ
výpadků	výpadek	k1gInPc2	výpadek
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
tramvajovém	tramvajový	k2eAgNnSc6d1	tramvajové
tělese	těleso	k1gNnSc6	těleso
(	(	kIx(	(
<g/>
překážka	překážka	k1gFnSc1	překážka
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnPc4	havárie
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc4	oprava
<g/>
)	)	kIx)	)
jen	jen	k9	jen
omezená	omezený	k2eAgFnSc1d1	omezená
možnost	možnost	k1gFnSc1	možnost
přenosu	přenos	k1gInSc2	přenos
provozu	provoz	k1gInSc2	provoz
tramvaje	tramvaj	k1gFnSc2	tramvaj
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
náhradní	náhradní	k2eAgFnSc2d1	náhradní
stopy	stopa	k1gFnSc2	stopa
<g />
.	.	kIx.	.
</s>
<s>
problematický	problematický	k2eAgInSc1d1	problematický
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
napájení	napájení	k1gNnSc4	napájení
shora	shora	k6eAd1	shora
a	a	k8xC	a
zdola	zdola	k6eAd1	zdola
<g/>
,	,	kIx,	,
konzervuje	konzervovat	k5eAaBmIp3nS	konzervovat
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
způsob	způsob	k1gInSc1	způsob
napájení	napájení	k1gNnSc2	napájení
nízká	nízký	k2eAgFnSc1d1	nízká
stoupavost	stoupavost	k1gFnSc1	stoupavost
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
česká	český	k2eAgFnSc1d1	Česká
koncepce	koncepce	k1gFnSc1	koncepce
tramvajového	tramvajový	k2eAgNnSc2d1	tramvajové
tělesa	těleso	k1gNnSc2	těleso
uprostřed	uprostřed	k7c2	uprostřed
vozovky	vozovka	k1gFnSc2	vozovka
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přechod	přechod	k1gInSc1	přechod
cestujících	cestující	k1gMnPc2	cestující
na	na	k7c4	na
zastávku	zastávka	k1gFnSc4	zastávka
(	(	kIx(	(
<g/>
tramvaj	tramvaj	k1gFnSc1	tramvaj
nezastavuje	zastavovat	k5eNaImIp3nS	zastavovat
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
chodníku	chodník	k1gInSc2	chodník
<g/>
)	)	kIx)	)
vysoké	vysoký	k2eAgInPc4d1	vysoký
nárazové	nárazový	k2eAgInPc4d1	nárazový
náklady	náklad	k1gInPc4	náklad
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
pořízení	pořízení	k1gNnSc4	pořízení
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
nezbytnost	nezbytnost	k1gFnSc1	nezbytnost
výstavby	výstavba	k1gFnSc2	výstavba
měníren	měnírna	k1gFnPc2	měnírna
<g/>
,	,	kIx,	,
dep	depo	k1gNnPc2	depo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
spíše	spíše	k9	spíše
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
nových	nový	k2eAgNnPc6d1	nové
místech	místo	k1gNnPc6	místo
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
technické	technický	k2eAgFnSc3d1	technická
koncepci	koncepce	k1gFnSc3	koncepce
současné	současný	k2eAgNnSc1d1	současné
působení	působení	k1gNnSc1	působení
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
přepravců	přepravce	k1gMnPc2	přepravce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jejich	jejich	k3xOp3gFnSc4	jejich
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
takové	takový	k3xDgNnSc4	takový
společné	společný	k2eAgNnSc4d1	společné
působení	působení	k1gNnSc4	působení
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
přepravců	přepravce	k1gMnPc2	přepravce
značně	značně	k6eAd1	značně
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
První	první	k4xOgFnPc1	první
tramvaje	tramvaj	k1gFnPc1	tramvaj
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
spíše	spíše	k9	spíše
za	za	k7c4	za
atrakci	atrakce	k1gFnSc4	atrakce
a	a	k8xC	a
technickým	technický	k2eAgInSc7d1	technický
výstřelkem	výstřelek	k1gInSc7	výstřelek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tramvaje	tramvaj	k1gFnPc1	tramvaj
byly	být	k5eAaImAgFnP	být
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
taženy	táhnout	k5eAaImNgInP	táhnout
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
dráhy	dráha	k1gFnPc1	dráha
začínaly	začínat	k5eAaImAgFnP	začínat
s	s	k7c7	s
koněspřežným	koněspřežný	k2eAgInSc7d1	koněspřežný
provozem	provoz	k1gInSc7	provoz
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
dálkových	dálkový	k2eAgFnPc6d1	dálková
(	(	kIx(	(
<g/>
železničních	železniční	k2eAgFnPc6d1	železniční
<g/>
)	)	kIx)	)
dráhách	dráha	k1gFnPc6	dráha
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
koně	kůň	k1gMnPc1	kůň
víceméně	víceméně	k9	víceméně
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
parní	parní	k2eAgFnSc7d1	parní
trakcí	trakce	k1gFnSc7	trakce
<g/>
.	.	kIx.	.
</s>
<s>
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
byla	být	k5eAaImAgFnS	být
nástupcem	nástupce	k1gMnSc7	nástupce
nekolejových	kolejový	k2eNgInPc2d1	kolejový
omnibusů	omnibus	k1gInPc2	omnibus
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
prvních	první	k4xOgInPc2	první
tramvajových	tramvajový	k2eAgMnPc2d1	tramvajový
vozů	vůz	k1gInPc2	vůz
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
železničních	železniční	k2eAgInPc2d1	železniční
vagónů	vagón	k1gInPc2	vagón
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
zmenšené	zmenšený	k2eAgFnSc6d1	zmenšená
a	a	k8xC	a
odlehčené	odlehčený	k2eAgFnSc6d1	odlehčená
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
tažení	tažení	k1gNnSc4	tažení
koňmi	kůň	k1gMnPc7	kůň
a	a	k8xC	a
projíždění	projíždění	k1gNnSc1	projíždění
ostrými	ostrý	k2eAgFnPc7d1	ostrá
zatáčkami	zatáčka	k1gFnPc7	zatáčka
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Koněspřežné	koněspřežný	k2eAgFnPc1d1	koněspřežná
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
dráhy	dráha	k1gFnPc1	dráha
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
tramvajemi	tramvaj	k1gFnPc7	tramvaj
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
pohonem	pohon	k1gInSc7	pohon
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
parními	parní	k2eAgFnPc7d1	parní
tramvajemi	tramvaj	k1gFnPc7	tramvaj
a	a	k8xC	a
poté	poté	k6eAd1	poté
tramvajemi	tramvaj	k1gFnPc7	tramvaj
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
zpravidla	zpravidla	k6eAd1	zpravidla
odebírá	odebírat	k5eAaImIp3nS	odebírat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
pomocí	pomocí	k7c2	pomocí
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
i	i	k9	i
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
jiného	jiný	k2eAgInSc2d1	jiný
způsobu	způsob	k1gInSc2	způsob
přívodu	přívod	k1gInSc2	přívod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
akumulátorový	akumulátorový	k2eAgInSc4d1	akumulátorový
<g/>
,	,	kIx,	,
boční	boční	k2eAgInSc4d1	boční
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInSc4d1	spodní
přívod	přívod	k1gInSc4	přívod
z	z	k7c2	z
provozní	provozní	k2eAgFnSc2d1	provozní
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ale	ale	k9	ale
inženýři	inženýr	k1gMnPc1	inženýr
a	a	k8xC	a
technici	technik	k1gMnPc1	technik
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
vyvinout	vyvinout	k5eAaPmF	vyvinout
hlavně	hlavně	k9	hlavně
spodní	spodní	k2eAgInSc4d1	spodní
přívod	přívod	k1gInSc4	přívod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
odstranil	odstranit	k5eAaPmAgInS	odstranit
nevzhledná	vzhledný	k2eNgNnPc4d1	nevzhledné
trolejová	trolejový	k2eAgNnPc4d1	trolejové
vedení	vedení	k1gNnPc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
zvolen	zvolen	k2eAgInSc4d1	zvolen
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jako	jako	k9	jako
standard	standard	k1gInSc1	standard
používá	používat	k5eAaImIp3nS	používat
rozchod	rozchod	k1gInSc4	rozchod
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
české	český	k2eAgFnPc4d1	Česká
tramvaje	tramvaj	k1gFnPc4	tramvaj
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
továrna	továrna	k1gFnSc1	továrna
Františka	František	k1gMnSc2	František
Ringhoffera	Ringhoffer	k1gMnSc2	Ringhoffer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
pro	pro	k7c4	pro
pražskou	pražský	k2eAgFnSc4d1	Pražská
koňku	koňka	k1gFnSc4	koňka
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
otevřené	otevřený	k2eAgInPc4d1	otevřený
vlečné	vlečný	k2eAgInPc4d1	vlečný
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
Ringhofferovy	Ringhofferův	k2eAgInPc1d1	Ringhofferův
závody	závod	k1gInPc1	závod
i	i	k8xC	i
elektrické	elektrický	k2eAgFnSc2d1	elektrická
tramvaje	tramvaj	k1gFnSc2	tramvaj
–	–	k?	–
pražský	pražský	k2eAgInSc1d1	pražský
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tvořen	tvořit	k5eAaImNgInS	tvořit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
vozy	vůz	k1gInPc1	vůz
od	od	k7c2	od
Ringhoffera	Ringhoffero	k1gNnSc2	Ringhoffero
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
znárodněna	znárodnit	k5eAaPmNgFnS	znárodnit
a	a	k8xC	a
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c6	na
Vagónku	vagónek	k1gInSc6	vagónek
Tatra	Tatra	k1gFnSc1	Tatra
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
podniku	podnik	k1gInSc2	podnik
ČKD	ČKD	kA	ČKD
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
dvounápravové	dvounápravový	k2eAgInPc4d1	dvounápravový
motorové	motorový	k2eAgInPc4d1	motorový
a	a	k8xC	a
vlečné	vlečný	k2eAgInPc4d1	vlečný
vozy	vůz	k1gInPc4	vůz
pro	pro	k7c4	pro
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
typ	typ	k1gInSc4	typ
4	[number]	k4	4
<g/>
MT	MT	kA	MT
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostravu	Ostrava	k1gFnSc4	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvaje	tramvaj	k1gFnSc2	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvaje	tramvaj	k1gFnSc2	tramvaj
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
