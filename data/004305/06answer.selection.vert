<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
zvolen	zvolen	k2eAgInSc4d1	zvolen
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jako	jako	k9	jako
standard	standard	k1gInSc1	standard
používá	používat	k5eAaImIp3nS	používat
rozchod	rozchod	k1gInSc4	rozchod
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
