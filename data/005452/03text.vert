<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Muzejník	muzejník	k1gMnSc1	muzejník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
český	český	k2eAgInSc1d1	český
stále	stále	k6eAd1	stále
vycházející	vycházející	k2eAgInSc1d1	vycházející
vědecký	vědecký	k2eAgInSc1d1	vědecký
časopis	časopis	k1gInSc1	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
jako	jako	k9	jako
čtvrtletník	čtvrtletník	k1gInSc4	čtvrtletník
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
jako	jako	k8xS	jako
dvojčíslo	dvojčíslo	k1gNnSc4	dvojčíslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
historickou	historický	k2eAgFnSc4d1	historická
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
přírodovědnou	přírodovědný	k2eAgFnSc4d1	přírodovědná
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
osamostatnila	osamostatnit	k5eAaPmAgNnP	osamostatnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
anglicky	anglicky	k6eAd1	anglicky
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
the	the	k?	the
National	National	k1gFnSc7	National
Museum	museum	k1gNnSc4	museum
(	(	kIx(	(
<g/>
Prague	Prague	k1gInSc1	Prague
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
historikem	historik	k1gMnSc7	historik
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
při	při	k7c6	při
společnosti	společnost	k1gFnSc6	společnost
Vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
musea	museum	k1gNnSc2	museum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
časopisu	časopis	k1gInSc2	časopis
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
názvem	název	k1gInSc7	název
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
vycházel	vycházet	k5eAaImAgMnS	vycházet
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Časopis	časopis	k1gInSc1	časopis
společnosti	společnost	k1gFnSc2	společnost
vlastenského	vlastenský	k2eAgInSc2d1	vlastenský
Museum	museum	k1gNnSc1	museum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
Časopis	časopis	k1gInSc1	časopis
Českého	český	k2eAgNnSc2d1	české
musea	museum	k1gNnSc2	museum
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
až	až	k9	až
dosud	dosud	k6eAd1	dosud
Časopis	časopis	k1gInSc1	časopis
Musea	museum	k1gNnSc2	museum
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
Časopis	časopis	k1gInSc1	časopis
NM	NM	kA	NM
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
historická	historický	k2eAgFnSc1d1	historická
má	mít	k5eAaImIp3nS	mít
přiřazenou	přiřazený	k2eAgFnSc4d1	přiřazená
ISSN	ISSN	kA	ISSN
1214-0627	[number]	k4	1214-0627
a	a	k8xC	a
přírodovědná	přírodovědný	k2eAgFnSc1d1	přírodovědná
ISSN	ISSN	kA	ISSN
1802-6842	[number]	k4	1802-6842
pro	pro	k7c4	pro
tištěné	tištěný	k2eAgInPc4d1	tištěný
a	a	k8xC	a
ISSN	ISSN	kA	ISSN
1802-6850	[number]	k4	1802-6850
pro	pro	k7c4	pro
elektronické	elektronický	k2eAgNnSc4d1	elektronické
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
redakce	redakce	k1gFnSc2	redakce
stáli	stát	k5eAaImAgMnP	stát
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
Pavel	Pavel	k1gMnSc1	Pavel
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
do	do	k7c2	do
r.	r.	kA	r.
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Erazim	Erazim	k1gMnSc1	Erazim
Vocel	Vocel	k1gMnSc1	Vocel
do	do	k7c2	do
r.	r.	kA	r.
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
ho	on	k3xPp3gInSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Josef	Josef	k1gMnSc1	Josef
Jireček	Jireček	k1gMnSc1	Jireček
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Bolemír	Bolemír	k1gMnSc1	Bolemír
Nebeský	nebeský	k2eAgInSc1d1	nebeský
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrťátko	Vrťátko	k1gNnSc1	Vrťátko
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Lepař	Lepař	k1gMnSc1	Lepař
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Krejčí	Krejčí	k1gMnSc1	Krejčí
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šafařík	Šafařík	k1gMnSc1	Šafařík
společně	společně	k6eAd1	společně
r.	r.	kA	r.
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
J.	J.	kA	J.
Lepař	Lepař	k1gMnSc1	Lepař
sám	sám	k3xTgInSc1	sám
do	do	k7c2	do
r.	r.	kA	r.
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
do	do	k7c2	do
r.	r.	kA	r.
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Antonín	Antonín	k1gMnSc1	Antonín
Truhlář	Truhlář	k1gMnSc1	Truhlář
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
Zíbrt	Zíbrt	k1gInSc1	Zíbrt
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Máchal	Máchal	k1gMnSc1	Máchal
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
redakce	redakce	k1gFnSc1	redakce
historické	historický	k2eAgFnSc2d1	historická
řady	řada	k1gFnSc2	řada
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Archivu	archiv	k1gInSc2	archiv
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Na	na	k7c6	na
zátorách	zátora	k1gFnPc6	zátora
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
redaktorkou	redaktorka	k1gFnSc7	redaktorka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Klára	Klára	k1gFnSc1	Klára
Woitschová	Woitschová	k1gFnSc1	Woitschová
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Čechura	Čechura	k1gMnSc1	Čechura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přírodovědnou	přírodovědný	k2eAgFnSc4d1	přírodovědná
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Mlíkovský	Mlíkovský	k2eAgMnSc1d1	Mlíkovský
<g/>
.	.	kIx.	.
</s>
<s>
SCHULZ	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatel	ukazatel	k1gInSc4	ukazatel
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
padesáti	padesát	k4xCc7	padesát
ročníkům	ročník	k1gInPc3	ročník
Časopisu	časopis	k1gInSc2	časopis
Musea	museum	k1gNnPc1	museum
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
SVRČEK	SVRČEK	kA	SVRČEK
<g/>
,	,	kIx,	,
Mirko	Mirko	k1gMnSc1	Mirko
<g/>
;	;	kIx,	;
KNEIDL	KNEIDL	kA	KNEIDL
<g/>
,	,	kIx,	,
Pravoslav	Pravoslav	k1gMnSc1	Pravoslav
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
BŘEŇOVÁ	BŘEŇOVÁ	kA	BŘEŇOVÁ
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
KNEIDL	KNEIDL	kA	KNEIDL
<g/>
,	,	kIx,	,
Pravoslav	Pravoslav	k1gMnSc1	Pravoslav
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
:	:	kIx,	:
Řada	řada	k1gFnSc1	řada
historická	historický	k2eAgFnSc1d1	historická
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
the	the	k?	the
National	National	k1gFnSc4	National
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Prague	Prague	k1gFnSc1	Prague
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Natural	Natural	k?	Natural
History	Histor	k1gMnPc4	Histor
Series	Series	k1gInSc1	Series
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnPc1	muzeum
</s>
