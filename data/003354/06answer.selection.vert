<s>
Po	po	k7c6	po
nalezení	nalezení	k1gNnSc6	nalezení
stabilního	stabilní	k2eAgNnSc2d1	stabilní
obsazení	obsazení	k1gNnSc2	obsazení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Megadeth	Megadetha	k1gFnPc2	Megadetha
nahráli	nahrát	k5eAaBmAgMnP	nahrát
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
a	a	k8xC	a
platinová	platinový	k2eAgNnPc4d1	platinové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c4	na
Grammy	Gramma	k1gFnPc4	Gramma
nominované	nominovaný	k2eAgFnSc2d1	nominovaná
multi-platinové	multilatinový	k2eAgFnSc2d1	multi-platinový
Countdown	Countdown	k1gMnSc1	Countdown
to	ten	k3xDgNnSc1	ten
Extinction	Extinction	k1gInSc1	Extinction
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
