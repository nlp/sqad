<s>
Planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
Merkur	Merkur	k1gInSc1	Merkur
(	(	kIx(	(
<g/>
☿	☿	k?	☿
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
♀	♀	k?	♀
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
♁	♁	k?	♁
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
♂	♂	k?	♂
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
(	(	kIx(	(
<g/>
♄	♄	k?	♄
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
♅	♅	k?	♅
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
(	(	kIx(	(
<g/>
♆	♆	k?	♆
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
