<p>
<s>
Ostružiník	ostružiník	k1gInSc1	ostružiník
maliník	maliník	k1gInSc1	maliník
(	(	kIx(	(
<g/>
Rubus	Rubus	k1gInSc1	Rubus
idaeus	idaeus	k1gInSc1	idaeus
Linné	Linné	k2eAgInSc1d1	Linné
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
maliník	maliník	k1gInSc1	maliník
obecný	obecný	k2eAgInSc1d1	obecný
je	být	k5eAaImIp3nS	být
až	až	k9	až
2	[number]	k4	2
metry	metr	k1gInPc4	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
listnatý	listnatý	k2eAgInSc4d1	listnatý
opadavý	opadavý	k2eAgInSc4d1	opadavý
keř	keř	k1gInSc4	keř
s	s	k7c7	s
prutovitými	prutovitý	k2eAgInPc7d1	prutovitý
výhony	výhon	k1gInPc7	výhon
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgNnSc1d1	náležející
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
růžovité	růžovitý	k2eAgFnSc2d1	růžovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
četné	četný	k2eAgInPc1d1	četný
<g/>
,	,	kIx,	,
střídavé	střídavý	k2eAgInPc1d1	střídavý
a	a	k8xC	a
lichozpeřené	lichozpeřený	k2eAgInPc1d1	lichozpeřený
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květy	Květa	k1gFnPc1	Květa
<g/>
:	:	kIx,	:
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
četné	četný	k2eAgInPc1d1	četný
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
<g/>
,	,	kIx,	,
kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
<g/>
:	:	kIx,	:
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
malina	malina	k1gFnSc1	malina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
souplodí	souplodí	k1gNnSc4	souplodí
drobných	drobný	k2eAgFnPc2d1	drobná
peckovic	peckovice	k1gFnPc2	peckovice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
peckoviček	peckovička	k1gFnPc2	peckovička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Maliník	maliník	k1gInSc1	maliník
(	(	kIx(	(
<g/>
Rubus	Rubus	k1gMnSc1	Rubus
idaeus	idaeus	k1gMnSc1	idaeus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgFnSc7d1	původní
domácí	domácí	k2eAgFnSc7d1	domácí
rostlinou	rostlina	k1gFnSc7	rostlina
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc3	Asie
včetně	včetně	k7c2	včetně
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nikdy	nikdy	k6eAd1	nikdy
nevzbudil	vzbudit	k5eNaPmAgInS	vzbudit
zájem	zájem	k1gInSc1	zájem
žádného	žádný	k3yNgInSc2	žádný
starověkého	starověký	k2eAgInSc2d1	starověký
národa	národ	k1gInSc2	národ
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Neobjevil	objevit	k5eNaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
žádných	žádný	k3yNgFnPc6	žádný
řeckých	řecký	k2eAgFnPc6d1	řecká
<g/>
,	,	kIx,	,
latinských	latinský	k2eAgFnPc6d1	Latinská
nebo	nebo	k8xC	nebo
čínských	čínský	k2eAgFnPc6d1	čínská
písemnostech	písemnost	k1gFnPc6	písemnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nezískal	získat	k5eNaPmAgInS	získat
maliník	maliník	k1gInSc1	maliník
žádný	žádný	k3yNgInSc4	žádný
opravdový	opravdový	k2eAgInSc4d1	opravdový
význam	význam	k1gInSc4	význam
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ani	ani	k8xC	ani
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ani	ani	k8xC	ani
pěstován	pěstován	k2eAgMnSc1d1	pěstován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
Johna	John	k1gMnSc2	John
Tradescanta	Tradescant	k1gMnSc2	Tradescant
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zapsána	zapsán	k2eAgFnSc1d1	zapsána
řada	řada	k1gFnSc1	řada
kultivarů	kultivar	k1gInPc2	kultivar
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
bílé	bílý	k2eAgInPc1d1	bílý
i	i	k8xC	i
červené	červený	k2eAgInPc1d1	červený
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
značné	značný	k2eAgNnSc4d1	značné
stáří	stáří	k1gNnSc4	stáří
procesu	proces	k1gInSc2	proces
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čím	co	k3yRnSc7	co
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
častěji	často	k6eAd2	často
ho	on	k3xPp3gMnSc4	on
popisují	popisovat	k5eAaImIp3nP	popisovat
jako	jako	k8xC	jako
kyselé	kyselý	k2eAgInPc4d1	kyselý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
pěstování	pěstování	k1gNnSc6	pěstování
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
kvalitnějších	kvalitní	k2eAgFnPc2d2	kvalitnější
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maliny	malina	k1gFnPc1	malina
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ponejprv	ponejprv	k6eAd1	ponejprv
pěstováno	pěstovat	k5eAaImNgNnS	pěstovat
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
domkáři	domkář	k1gMnPc1	domkář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přenesli	přenést	k5eAaPmAgMnP	přenést
pruty	prut	k1gInPc4	prut
maliníku	maliník	k1gInSc2	maliník
z	z	k7c2	z
divočiny	divočina	k1gFnSc2	divočina
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
zahrádek	zahrádka	k1gFnPc2	zahrádka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
udělali	udělat	k5eAaPmAgMnP	udělat
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
planými	planý	k2eAgFnPc7d1	planá
lesními	lesní	k2eAgFnPc7d1	lesní
jahodami	jahoda	k1gFnPc7	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
zkvalitnění	zkvalitnění	k1gNnSc4	zkvalitnění
druhů	druh	k1gInPc2	druh
tu	tu	k6eAd1	tu
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ovoce	ovoce	k1gNnSc1	ovoce
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
zájem	zájem	k1gInSc4	zájem
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
pěstitelů	pěstitel	k1gMnPc2	pěstitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
hor.	hor.	k?	hor.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Světlé	světlý	k2eAgInPc1d1	světlý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
mýtiny	mýtina	k1gFnPc1	mýtina
a	a	k8xC	a
slunné	slunný	k2eAgFnPc1d1	slunná
stráně	stráň	k1gFnPc1	stráň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Maliny	malina	k1gFnPc1	malina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
ceněny	cenit	k5eAaImNgFnP	cenit
spotřebiteli	spotřebitel	k1gMnSc3	spotřebitel
drobného	drobný	k2eAgNnSc2d1	drobné
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jejich	jejich	k3xOp3gFnSc2	jejich
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
populární	populární	k2eAgInSc4d1	populární
u	u	k7c2	u
pěstitelů	pěstitel	k1gMnPc2	pěstitel
ovoce	ovoce	k1gNnSc2	ovoce
z	z	k7c2	z
několika	několik	k4yIc2	několik
důležitých	důležitý	k2eAgInPc2d1	důležitý
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
maliníky	maliník	k1gInPc1	maliník
zpravidla	zpravidla	k6eAd1	zpravidla
nejsou	být	k5eNaImIp3nP	být
produktivní	produktivní	k2eAgInPc1d1	produktivní
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
lokalitách	lokalita	k1gFnPc6	lokalita
</s>
</p>
<p>
<s>
některá	některý	k3yIgNnPc1	některý
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
napadají	napadat	k5eAaImIp3nP	napadat
maliník	maliník	k1gInSc4	maliník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vážnému	vážný	k2eAgNnSc3d1	vážné
a	a	k8xC	a
trvalé	trvalý	k2eAgNnSc4d1	trvalé
poškození	poškození	k1gNnSc4	poškození
</s>
</p>
<p>
<s>
keře	keř	k1gInPc1	keř
nejsou	být	k5eNaImIp3nP	být
silně	silně	k6eAd1	silně
produktivní	produktivní	k2eAgInPc1d1	produktivní
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
sklizeň	sklizeň	k1gFnSc4	sklizeň
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgFnPc4d1	značná
</s>
</p>
<p>
<s>
maliny	malina	k1gFnPc1	malina
špatně	špatně	k6eAd1	špatně
snáší	snášet	k5eAaImIp3nP	snášet
přepravu	přeprava	k1gFnSc4	přeprava
(	(	kIx(	(
<g/>
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
jemnost	jemnost	k1gFnSc1	jemnost
plodu	plod	k1gInSc2	plod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
zvládnout	zvládnout	k5eAaPmF	zvládnout
přepravu	přeprava	k1gFnSc4	přeprava
bez	bez	k7c2	bez
značného	značný	k2eAgNnSc2d1	značné
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
pěstováním	pěstování	k1gNnSc7	pěstování
malin	malina	k1gFnPc2	malina
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
výhod	výhoda	k1gFnPc2	výhoda
jejich	jejich	k3xOp3gMnPc7	jejich
pěstování	pěstování	k1gNnSc1	pěstování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
maliník	maliník	k1gInSc1	maliník
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
prodáván	prodávat	k5eAaImNgInS	prodávat
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
uspokojivé	uspokojivý	k2eAgFnPc4d1	uspokojivá
ceny	cena	k1gFnPc4	cena
</s>
</p>
<p>
<s>
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
malinách	malina	k1gFnPc6	malina
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
</s>
</p>
<p>
<s>
dobré	dobrý	k2eAgInPc1d1	dobrý
výtěžky	výtěžek	k1gInPc1	výtěžek
maliníku	maliník	k1gInSc2	maliník
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
pečlivým	pečlivý	k2eAgNnSc7d1	pečlivé
umístěním	umístění	k1gNnSc7	umístění
plantáže	plantáž	k1gFnSc2	plantáž
<g/>
,	,	kIx,	,
pěstováním	pěstování	k1gNnSc7	pěstování
a	a	k8xC	a
hnojením	hnojení	k1gNnSc7	hnojení
</s>
</p>
<p>
<s>
prodej	prodej	k1gInSc1	prodej
malin	malina	k1gFnPc2	malina
je	být	k5eAaImIp3nS	být
žádoucím	žádoucí	k2eAgInSc7d1	žádoucí
příspěvkem	příspěvek	k1gInSc7	příspěvek
ovocnáře	ovocnář	k1gMnSc2	ovocnář
k	k	k7c3	k
dobrému	dobrý	k2eAgNnSc3d1	dobré
postavení	postavení	k1gNnSc3	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pěstitel	pěstitel	k1gMnSc1	pěstitel
získá	získat	k5eAaPmIp3nS	získat
peníze	peníz	k1gInPc4	peníz
již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
maliny	malina	k1gFnSc2	malina
mu	on	k3xPp3gMnSc3	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
zajistit	zajistit	k5eAaPmF	zajistit
zákazníky	zákazník	k1gMnPc4	zákazník
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
pozdější	pozdní	k2eAgInPc4d2	pozdější
výpěstky	výpěstek	k1gInPc4	výpěstek
<g/>
.	.	kIx.	.
<g/>
Život	život	k1gInSc1	život
průměrného	průměrný	k2eAgInSc2d1	průměrný
keře	keř	k1gInSc2	keř
maliníku	maliník	k1gInSc2	maliník
je	být	k5eAaImIp3nS	být
8-15	[number]	k4	8-15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
chorob	choroba	k1gFnPc2	choroba
životnost	životnost	k1gFnSc1	životnost
keře	keř	k1gInPc4	keř
významně	významně	k6eAd1	významně
nesníží	snížit	k5eNaPmIp3nP	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
žádné	žádný	k3yNgFnPc4	žádný
bezprostřední	bezprostřední	k2eAgFnPc4d1	bezprostřední
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c6	na
nadprodukci	nadprodukce	k1gFnSc6	nadprodukce
malin	malina	k1gFnPc2	malina
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
pro	pro	k7c4	pro
ovoce	ovoce	k1gNnSc4	ovoce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodávka	dodávka	k1gFnSc1	dodávka
bobulí	bobule	k1gFnPc2	bobule
dobré	dobrý	k2eAgFnSc2d1	dobrá
kvality	kvalita	k1gFnSc2	kvalita
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
poptávkou	poptávka	k1gFnSc7	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Maliny	malina	k1gFnPc1	malina
jsou	být	k5eAaImIp3nP	být
nejpopulárnější	populární	k2eAgNnSc4d3	nejpopulárnější
keřové	keřový	k2eAgNnSc4d1	keřové
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
zrát	zrát	k5eAaImF	zrát
3-4	[number]	k4	3-4
týdny	týden	k1gInPc7	týden
po	po	k7c6	po
jahodách	jahoda	k1gFnPc6	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Maliny	malina	k1gFnPc1	malina
jsou	být	k5eAaImIp3nP	být
odolné	odolný	k2eAgFnPc1d1	odolná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Maliník	maliník	k1gInSc4	maliník
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pěstovat	pěstovat	k5eAaImF	pěstovat
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
lze	lze	k6eAd1	lze
polovinu	polovina	k1gFnSc4	polovina
běžné	běžný	k2eAgFnSc2d1	běžná
úrody	úroda	k1gFnSc2	úroda
ovoce	ovoce	k1gNnSc2	ovoce
očekávat	očekávat	k5eAaImF	očekávat
u	u	k7c2	u
maliníku	maliník	k1gInSc2	maliník
už	už	k6eAd1	už
druhý	druhý	k4xOgInSc1	druhý
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třetího	třetí	k4xOgNnSc2	třetí
až	až	k6eAd1	až
do	do	k7c2	do
pátého	pátý	k4xOgInSc2	pátý
roku	rok	k1gInSc2	rok
plantáže	plantáž	k1gFnPc1	plantáž
produkují	produkovat	k5eAaImIp3nP	produkovat
o	o	k7c4	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgFnSc4d2	veliký
úrodu	úroda	k1gFnSc4	úroda
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
očekávat	očekávat	k5eAaImF	očekávat
další	další	k2eAgNnSc4d1	další
zvýšení	zvýšení	k1gNnSc4	zvýšení
po	po	k7c6	po
pátém	pátý	k4xOgInSc6	pátý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
asi	asi	k9	asi
1500	[number]	k4	1500
litrů	litr	k1gInPc2	litr
na	na	k7c4	na
hektar	hektar	k1gInSc4	hektar
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
běžnou	běžný	k2eAgFnSc4d1	běžná
plnou	plný	k2eAgFnSc4d1	plná
sklizeň	sklizeň	k1gFnSc4	sklizeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Půdní	půdní	k2eAgFnPc4d1	půdní
podmínky	podmínka	k1gFnPc4	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Maliny	malina	k1gFnPc1	malina
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nP	dařit
pěstovat	pěstovat	k5eAaImF	pěstovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
druzích	druh	k1gInPc6	druh
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejlépe	dobře	k6eAd3	dobře
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgFnSc6d1	vlhká
<g/>
,	,	kIx,	,
humózní	humózní	k2eAgFnSc6d1	humózní
a	a	k8xC	a
propustné	propustný	k2eAgFnSc6d1	propustná
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
odvodnění	odvodnění	k1gNnSc1	odvodnění
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgNnSc1d1	zásadní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
půda	půda	k1gFnSc1	půda
příliš	příliš	k6eAd1	příliš
suchá	suchý	k2eAgFnSc1d1	suchá
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
špatná	špatný	k2eAgFnSc1d1	špatná
jako	jako	k8xS	jako
půda	půda	k1gFnSc1	půda
příliš	příliš	k6eAd1	příliš
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
celkový	celkový	k2eAgInSc1d1	celkový
dojem	dojem	k1gInSc1	dojem
z	z	k7c2	z
testů	test	k1gInPc2	test
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
maliny	malina	k1gFnPc1	malina
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nP	dařit
pěstovat	pěstovat	k5eAaImF	pěstovat
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
štěrkovité	štěrkovitý	k2eAgFnSc6d1	štěrkovitá
a	a	k8xC	a
písčité	písčitý	k2eAgFnSc6d1	písčitá
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hlinité	hlinitý	k2eAgFnSc6d1	hlinitá
půdě	půda	k1gFnSc6	půda
byly	být	k5eAaImAgFnP	být
plodiny	plodina	k1gFnPc1	plodina
o	o	k7c4	o
54	[number]	k4	54
procent	procento	k1gNnPc2	procento
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
lehčích	lehký	k2eAgFnPc6d2	lehčí
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
plodiny	plodina	k1gFnPc4	plodina
na	na	k7c6	na
těžších	těžký	k2eAgFnPc6d2	těžší
půdách	půda	k1gFnPc6	půda
mají	mít	k5eAaImIp3nP	mít
64	[number]	k4	64
procent	procento	k1gNnPc2	procento
větší	veliký	k2eAgInSc1d2	veliký
výnos	výnos	k1gInSc1	výnos
než	než	k8xS	než
na	na	k7c6	na
lehčích	lehký	k2eAgFnPc6d2	lehčí
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
přesvědčivý	přesvědčivý	k2eAgInSc1d1	přesvědčivý
argument	argument	k1gInSc1	argument
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pěstování	pěstování	k1gNnSc3	pěstování
malin	malina	k1gFnPc2	malina
na	na	k7c6	na
těžších	těžký	k2eAgFnPc6d2	těžší
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
maliny	malina	k1gFnPc4	malina
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
bez	bez	k7c2	bez
zhoubných	zhoubný	k2eAgFnPc2d1	zhoubná
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
vytrvalých	vytrvalý	k2eAgInPc2d1	vytrvalý
plevelů	plevel	k1gInPc2	plevel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expozice	expozice	k1gFnSc1	expozice
===	===	k?	===
</s>
</p>
<p>
<s>
Maliníku	maliník	k1gInSc3	maliník
dělá	dělat	k5eAaImIp3nS	dělat
nejlépe	dobře	k6eAd3	dobře
severní	severní	k2eAgFnSc1d1	severní
nebo	nebo	k8xC	nebo
západní	západní	k2eAgFnSc1d1	západní
expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
takové	takový	k3xDgNnSc4	takový
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
svahy	svah	k1gInPc1	svah
vhodnější	vhodný	k2eAgInPc1d2	vhodnější
pro	pro	k7c4	pro
komerční	komerční	k2eAgFnPc4d1	komerční
plantáže	plantáž	k1gFnPc4	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
a	a	k8xC	a
nejsladší	sladký	k2eAgNnSc1d3	nejsladší
ovoce	ovoce	k1gNnSc1	ovoce
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
chladných	chladný	k2eAgNnPc6d1	chladné
<g/>
,	,	kIx,	,
stinných	stinný	k2eAgNnPc6d1	stinné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zajistit	zajistit	k5eAaPmF	zajistit
na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
a	a	k8xC	a
západních	západní	k2eAgInPc6d1	západní
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výsadbu	výsadba	k1gFnSc4	výsadba
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
volba	volba	k1gFnSc1	volba
expozice	expozice	k1gFnSc2	expozice
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
chladné	chladný	k2eAgNnSc1d1	chladné
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Maliny	malina	k1gFnPc1	malina
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
množeny	množit	k5eAaImNgInP	množit
z	z	k7c2	z
výmladků	výmladek	k1gInPc2	výmladek
a	a	k8xC	a
od	od	k7c2	od
kořenových	kořenový	k2eAgInPc2d1	kořenový
řízků	řízek	k1gInPc2	řízek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
množení	množení	k1gNnSc2	množení
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
skoro	skoro	k6eAd1	skoro
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
výmladky	výmladek	k1gInPc1	výmladek
jsou	být	k5eAaImIp3nP	být
výhony	výhon	k1gInPc4	výhon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přirozeně	přirozeně	k6eAd1	přirozeně
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
blízko	blízko	k7c2	blízko
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výmladky	výmladek	k1gInPc1	výmladek
často	často	k6eAd1	často
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
kořeny	kořen	k1gInPc1	kořen
maliníku	maliník	k1gInSc2	maliník
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
při	při	k7c6	při
kultivaci	kultivace	k1gFnSc6	kultivace
<g/>
.	.	kIx.	.
</s>
<s>
Výmladky	výmladek	k1gInPc1	výmladek
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgFnPc1d1	oddělena
a	a	k8xC	a
vykopány	vykopán	k2eAgFnPc1d1	vykopána
lopatou	lopata	k1gFnSc7	lopata
a	a	k8xC	a
rýčem	rýč	k1gInSc7	rýč
kořeny	kořen	k2eAgInPc4d1	kořen
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
maliníky	maliník	k1gInPc4	maliník
množeny	množen	k2eAgInPc4d1	množen
kořenovými	kořenový	k2eAgInPc7d1	kořenový
řízky	řízek	k1gInPc7	řízek
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
kousky	kousek	k1gInPc4	kousek
kořenů	kořen	k1gInPc2	kořen
10-13	[number]	k4	10-13
centimetrů	centimetr	k1gInPc2	centimetr
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
asi	asi	k9	asi
tak	tak	k6eAd1	tak
tlusté	tlustý	k2eAgFnPc1d1	tlustá
jako	jako	k9	jako
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
odřezány	odřezán	k2eAgFnPc1d1	odřezán
od	od	k7c2	od
silné	silný	k2eAgFnSc2d1	silná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
pupeny	pupen	k1gInPc1	pupen
a	a	k8xC	a
řízky	řízek	k1gInPc1	řízek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
brázdě	brázda	k1gFnSc6	brázda
a	a	k8xC	a
přikryté	přikrytý	k2eAgFnSc3d1	přikrytá
pěti	pět	k4xCc7	pět
nebo	nebo	k8xC	nebo
sedmi	sedm	k4xCc7	sedm
cm	cm	kA	cm
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
výhony	výhon	k1gInPc4	výhon
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
připraveny	připravit	k5eAaPmNgInP	připravit
pro	pro	k7c4	pro
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
stanovišti	stanoviště	k1gNnSc6	stanoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
z	z	k7c2	z
mladých	mladý	k2eAgInPc2d1	mladý
keřů	keř	k1gInPc2	keř
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
odebírání	odebírání	k1gNnSc4	odebírání
řízků	řízek	k1gInPc2	řízek
a	a	k8xC	a
výmladků	výmladek	k1gInPc2	výmladek
vhodnější	vhodný	k2eAgMnSc1d2	vhodnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
sílu	síla	k1gFnSc4	síla
než	než	k8xS	než
rostliny	rostlina	k1gFnPc4	rostlina
odebrané	odebraný	k2eAgFnPc4d1	odebraná
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
méně	málo	k6eAd2	málo
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
šířit	šířit	k5eAaImF	šířit
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výsadba	výsadba	k1gFnSc1	výsadba
===	===	k?	===
</s>
</p>
<p>
<s>
Maliníky	maliník	k1gInPc1	maliník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vysazovány	vysazován	k2eAgFnPc1d1	vysazována
buď	buď	k8xC	buď
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
jarní	jarní	k2eAgFnSc1d1	jarní
výsadba	výsadba	k1gFnSc1	výsadba
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgFnSc1d2	častější
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
maliny	malina	k1gFnPc4	malina
vysazovány	vysazován	k2eAgFnPc4d1	vysazována
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pěstitelé	pěstitel	k1gMnPc1	pěstitel
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
výsadbě	výsadba	k1gFnSc6	výsadba
do	do	k7c2	do
řádků	řádek	k1gInPc2	řádek
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
6	[number]	k4	6
nebo	nebo	k8xC	nebo
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgInPc2d1	tvořený
keři	keř	k1gInPc7	keř
4	[number]	k4	4
-	-	kIx~	-
6	[number]	k4	6
stop	stopa	k1gFnPc2	stopa
od	od	k7c2	od
sebe	se	k3xPyFc2	se
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
pěstitelé	pěstitel	k1gMnPc1	pěstitel
volí	volit	k5eAaImIp3nP	volit
spon	spon	k1gInSc4	spon
4	[number]	k4	4
stopy	stopa	k1gFnSc2	stopa
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
směru	směr	k1gInSc6	směr
a	a	k8xC	a
omezují	omezovat	k5eAaImIp3nP	omezovat
rostliny	rostlina	k1gFnPc1	rostlina
na	na	k7c6	na
určené	určený	k2eAgFnSc6d1	určená
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
J.H.	J.H.	k?	J.H.
Hale	hala	k1gFnSc3	hala
tvrdí	tvrdý	k2eAgMnPc1d1	tvrdý
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
zajištěny	zajistit	k5eAaPmNgInP	zajistit
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
výnosy	výnos	k1gInPc1	výnos
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nevhodnější	vhodný	k2eNgFnSc1d2	nevhodnější
výsadba	výsadba	k1gFnSc1	výsadba
nakopčených	nakopčený	k2eAgFnPc2d1	nakopčený
rostlin	rostlina	k1gFnPc2	rostlina
7	[number]	k4	7
-	-	kIx~	-
8	[number]	k4	8
stop	stopa	k1gFnPc2	stopa
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
pěstitel	pěstitel	k1gMnSc1	pěstitel
malin	malina	k1gFnPc2	malina
obvykle	obvykle	k6eAd1	obvykle
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pěstování	pěstování	k1gNnSc1	pěstování
keřů	keř	k1gInPc2	keř
v	v	k7c6	v
řádcích	řádek	k1gInPc6	řádek
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
6	[number]	k4	6
stop	stopa	k1gFnPc2	stopa
od	od	k7c2	od
sebe	se	k3xPyFc2	se
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
řádku	řádek	k1gInSc6	řádek
3	[number]	k4	3
stopy	stopa	k1gFnSc2	stopa
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
je	být	k5eAaImIp3nS	být
uspokojivé	uspokojivý	k2eAgNnSc1d1	uspokojivé
a	a	k8xC	a
že	že	k8xS	že
úroda	úroda	k1gFnSc1	úroda
bude	být	k5eAaImBp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
vysadí	vysadit	k5eAaPmIp3nP	vysadit
rostliny	rostlina	k1gFnPc1	rostlina
na	na	k7c4	na
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
jinou	jiný	k2eAgFnSc4d1	jiná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
keře	keř	k1gInPc1	keř
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
houstnout	houstnout	k5eAaImF	houstnout
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
vysazeny	vysazen	k2eAgFnPc1d1	vysazena
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
proředěny	proředět	k5eAaPmNgInP	proředět
v	v	k7c6	v
řádcích	řádek	k1gInPc6	řádek
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
odstranit	odstranit	k5eAaPmF	odstranit
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
keř	keř	k1gInSc4	keř
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
půdy	půda	k1gFnSc2	půda
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
maliníku	maliník	k1gInSc2	maliník
keřů	keř	k1gInPc2	keř
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
6	[number]	k4	6
stop	stop	k1gInSc4	stop
od	od	k7c2	od
sebe	se	k3xPyFc2	se
a	a	k8xC	a
3	[number]	k4	3
stopy	stopa	k1gFnSc2	stopa
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
řádcích	řádek	k1gInPc6	řádek
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
rovné	rovný	k2eAgFnPc4d1	rovná
brázdy	brázda	k1gFnPc4	brázda
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
přes	přes	k7c4	přes
pole	pole	k1gNnSc4	pole
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6	[number]	k4	6
stop	stopa	k1gFnPc2	stopa
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
asi	asi	k9	asi
3	[number]	k4	3
cm	cm	kA	cm
hluboko	hluboko	k6eAd1	hluboko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výsadbě	výsadba	k1gFnSc6	výsadba
by	by	kYmCp3nS	by
rostliny	rostlina	k1gFnPc1	rostlina
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kořeny	kořen	k1gInPc1	kořen
zakryty	zakryt	k2eAgInPc1d1	zakryt
vlhkou	vlhký	k2eAgFnSc7d1	vlhká
pytlovinou	pytlovina	k1gFnSc7	pytlovina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zlomené	zlomený	k2eAgInPc1d1	zlomený
kořeny	kořen	k1gInPc1	kořen
s	s	k7c7	s
roztřepenými	roztřepený	k2eAgInPc7d1	roztřepený
konci	konec	k1gInPc7	konec
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zastřiženy	zastřihnout	k5eAaPmNgFnP	zastřihnout
na	na	k7c4	na
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
řez	řez	k1gInSc1	řez
bude	být	k5eAaImBp3nS	být
hojit	hojit	k5eAaImF	hojit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Agrotechnika	agrotechnik	k1gMnSc2	agrotechnik
v	v	k7c6	v
malinové	malinový	k2eAgFnSc6d1	malinová
plantáži	plantáž	k1gFnSc6	plantáž
===	===	k?	===
</s>
</p>
<p>
<s>
Orba	orba	k1gFnSc1	orba
mezi	mezi	k7c7	mezi
řádky	řádek	k1gInPc7	řádek
v	v	k7c6	v
malinové	malinový	k2eAgFnSc6d1	malinová
plantáži	plantáž	k1gFnSc6	plantáž
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
není	být	k5eNaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
růst	růst	k1gInSc4	růst
příliš	příliš	k6eAd1	příliš
mnoha	mnoho	k4c2	mnoho
výhonů	výhon	k1gInPc2	výhon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
růst	růst	k1gInSc1	růst
dřeva	dřevo	k1gNnSc2	dřevo
sníží	snížit	k5eAaPmIp3nS	snížit
úrodu	úroda	k1gFnSc4	úroda
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Jarní	jarní	k2eAgFnSc1d1	jarní
orba	orba	k1gFnSc1	orba
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
řádně	řádně	k6eAd1	řádně
obdělávána	obdělávat	k5eAaImNgFnS	obdělávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulé	minulý	k2eAgFnSc2d1	minulá
sezony	sezona	k1gFnSc2	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
řádky	řádek	k1gInPc7	řádek
lze	lze	k6eAd1	lze
pěstovat	pěstovat	k5eAaImF	pěstovat
meziplodiny	meziplodina	k1gFnPc4	meziplodina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
vikvovitých	vikvovitý	k2eAgMnPc2d1	vikvovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řez	řez	k1gInSc1	řez
maliníku	maliník	k1gInSc2	maliník
===	===	k?	===
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
prořezávat	prořezávat	k5eAaImF	prořezávat
maliny	malina	k1gFnPc4	malina
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výhony	výhon	k1gInPc1	výhon
této	tento	k3xDgFnSc2	tento
rostliny	rostlina	k1gFnSc2	rostlina
jsou	být	k5eAaImIp3nP	být
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
dvouleté	dvouletý	k2eAgFnPc1d1	dvouletá
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
trvalé	trvalý	k2eAgInPc1d1	trvalý
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výhonky	výhonek	k1gInPc1	výhonek
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
sezóny	sezóna	k1gFnSc2	sezóna
nesou	nést	k5eAaImIp3nP	nést
ovoce	ovoce	k1gNnPc4	ovoce
v	v	k7c6	v
příští	příští	k2eAgFnSc6d1	příští
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
a	a	k8xC	a
hynou	hynout	k5eAaImIp3nP	hynout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgNnSc2	druhý
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
maliníku	maliník	k1gInSc6	maliník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
rok	rok	k1gInSc1	rok
starý	starý	k2eAgInSc1d1	starý
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
obvykle	obvykle	k6eAd1	obvykle
výhony	výhon	k1gInPc4	výhon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přináší	přinášet	k5eAaImIp3nP	přinášet
plody	plod	k1gInPc4	plod
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
ovoce	ovoce	k1gNnSc2	ovoce
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
prořezávání	prořezávání	k1gNnSc1	prořezávání
jsou	být	k5eAaImIp3nP	být
tenké	tenký	k2eAgInPc1d1	tenký
a	a	k8xC	a
mladé	mladý	k2eAgInPc4d1	mladý
výhony	výhon	k1gInPc4	výhon
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nesly	nést	k5eAaImAgFnP	nést
velkou	velký	k2eAgFnSc4d1	velká
úrodu	úroda	k1gFnSc4	úroda
velkých	velký	k2eAgFnPc2d1	velká
bobulí	bobule	k1gFnPc2	bobule
<g/>
,	,	kIx,	,
a	a	k8xC	a
odstranění	odstranění	k1gNnSc1	odstranění
starých	starý	k2eAgInPc2d1	starý
výhonů	výhon	k1gInPc2	výhon
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
odplodí	odplodit	k5eAaPmIp3nS	odplodit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgInS	zůstat
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
nových	nový	k2eAgFnPc2d1	nová
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
po	po	k7c6	po
výsadbě	výsadba	k1gFnSc6	výsadba
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prořezávání	prořezávání	k1gNnSc1	prořezávání
důležité	důležitý	k2eAgNnSc1d1	důležité
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
prováděno	provádět	k5eAaImNgNnS	provádět
systematicky	systematicky	k6eAd1	systematicky
<g/>
.	.	kIx.	.
</s>
<s>
Tendence	tendence	k1gFnSc1	tendence
rostliny	rostlina	k1gFnSc2	rostlina
je	být	k5eAaImIp3nS	být
vegetativně	vegetativně	k6eAd1	vegetativně
růst	růst	k5eAaImF	růst
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
ovoce	ovoce	k1gNnSc2	ovoce
dobré	dobrý	k2eAgFnSc2d1	dobrá
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
plodů	plod	k1gInPc2	plod
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
počet	počet	k1gInSc1	počet
výhonů	výhon	k1gInPc2	výhon
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
řezu	řez	k1gInSc2	řez
maliníku	maliník	k1gInSc2	maliník
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
různých	různý	k2eAgMnPc2d1	různý
pěstitelů	pěstitel	k1gMnPc2	pěstitel
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
pěstitelé	pěstitel	k1gMnPc1	pěstitel
nechávají	nechávat	k5eAaImIp3nP	nechávat
růst	růst	k5eAaImF	růst
volně	volně	k6eAd1	volně
maliník	maliník	k1gInSc4	maliník
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
seřezávají	seřezávat	k5eAaImIp3nP	seřezávat
jej	on	k3xPp3gMnSc4	on
po	po	k7c6	po
zimě	zima	k1gFnSc6	zima
nebo	nebo	k8xC	nebo
brzy	brzy	k6eAd1	brzy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
pěstitelé	pěstitel	k1gMnPc1	pěstitel
obhajují	obhajovat	k5eAaImIp3nP	obhajovat
řez	řez	k1gInSc4	řez
terminálních	terminální	k2eAgInPc2d1	terminální
výhonů	výhon	k1gInPc2	výhon
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
nutí	nutit	k5eAaImIp3nS	nutit
růst	růst	k5eAaImF	růst
boční	boční	k2eAgInPc4d1	boční
výhony	výhon	k1gInPc4	výhon
před	před	k7c7	před
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
ponechat	ponechat	k5eAaPmF	ponechat
mladý	mladý	k2eAgInSc4d1	mladý
maliník	maliník	k1gInSc4	maliník
růst	růst	k1gInSc4	růst
volně	volně	k6eAd1	volně
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
následujícího	následující	k2eAgNnSc2d1	následující
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
již	již	k6eAd1	již
pomine	pominout	k5eAaPmIp3nS	pominout
riziko	riziko	k1gNnSc4	riziko
silných	silný	k2eAgInPc2d1	silný
mrazů	mráz	k1gInPc2	mráz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
seřezány	seřezat	k5eAaPmNgInP	seřezat
všechny	všechen	k3xTgInPc1	všechen
výhony	výhon	k1gInPc1	výhon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
nést	nést	k5eAaImF	nést
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoleté	jednoletý	k2eAgInPc1d1	jednoletý
výhony	výhon	k1gInPc1	výhon
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
podle	podle	k7c2	podle
libosti	libost	k1gFnSc2	libost
první	první	k4xOgFnSc1	první
léto	léto	k1gNnSc4	léto
jsou	být	k5eAaImIp3nP	být
sníženy	snížit	k5eAaPmNgInP	snížit
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Plodné	plodný	k2eAgInPc1d1	plodný
výhony	výhon	k1gInPc1	výhon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
seřezané	seřezaný	k2eAgInPc1d1	seřezaný
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
vyženou	vyhnat	k5eAaPmIp3nP	vyhnat
velké	velká	k1gFnPc1	velká
množství	množství	k1gNnSc2	množství
silných	silný	k2eAgFnPc2d1	silná
bočních	boční	k2eAgFnPc2d1	boční
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
boční	boční	k2eAgInPc1d1	boční
výhony	výhon	k1gInPc1	výhon
budou	být	k5eAaImBp3nP	být
tvořit	tvořit	k5eAaImF	tvořit
tolik	tolik	k4yIc4	tolik
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
jednarosltina	jednarosltina	k1gFnSc1	jednarosltina
dokáže	dokázat	k5eAaPmIp3nS	dokázat
uživit	uživit	k5eAaPmF	uživit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
řez	řez	k1gInSc4	řez
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
výhony	výhon	k1gInPc1	výhon
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
poškozeny	poškozen	k2eAgInPc1d1	poškozen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
zkušených	zkušený	k2eAgMnPc2d1	zkušený
pěstitelů	pěstitel	k1gMnPc2	pěstitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c6	na
pochybách	pochyba	k1gFnPc6	pochyba
o	o	k7c6	o
vhodnosti	vhodnost	k1gFnSc6	vhodnost
tohoto	tento	k3xDgInSc2	tento
letního	letní	k2eAgInSc2d1	letní
řezu	řez	k1gInSc2	řez
mladých	mladý	k2eAgInPc2d1	mladý
maliníků	maliník	k1gInPc2	maliník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
se	s	k7c7	s
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
počet	počet	k1gInSc4	počet
výmladků	výmladek	k1gInPc2	výmladek
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nadměrnému	nadměrný	k2eAgInSc3d1	nadměrný
vegetativnímu	vegetativní	k2eAgInSc3d1	vegetativní
růstu	růst	k1gInSc3	růst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
schopnost	schopnost	k1gFnSc4	schopnost
rostliny	rostlina	k1gFnSc2	rostlina
plodit	plodit	k5eAaImF	plodit
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
zakrácení	zakrácení	k1gNnSc1	zakrácení
terminálních	terminální	k2eAgInPc2d1	terminální
pupenů	pupen	k1gInPc2	pupen
mladých	mladý	k2eAgInPc2d1	mladý
hole	hole	k6eAd1	hole
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
silných	silný	k2eAgInPc2d1	silný
bočních	boční	k2eAgInPc2d1	boční
výhonů	výhon	k1gInPc2	výhon
ze	z	k7c2	z
spodních	spodní	k2eAgInPc2d1	spodní
pupenů	pupen	k1gInPc2	pupen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podporuje	podporovat	k5eAaImIp3nS	podporovat
tvorbu	tvorba	k1gFnSc4	tvorba
slabých	slabý	k2eAgFnPc2d1	slabá
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
poškozeny	poškozen	k2eAgInPc4d1	poškozen
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
plodné	plodný	k2eAgFnPc1d1	plodná
boční	boční	k2eAgFnPc1d1	boční
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
po	po	k7c6	po
řezu	řez	k1gInSc6	řez
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
spící	spící	k2eAgNnPc1d1	spící
až	až	k6eAd1	až
do	do	k7c2	do
následujícího	následující	k2eAgNnSc2d1	následující
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
dobré	dobrý	k2eAgInPc1d1	dobrý
plodné	plodný	k2eAgInPc1d1	plodný
boční	boční	k2eAgInPc1d1	boční
výhony	výhon	k1gInPc1	výhon
metodou	metoda	k1gFnSc7	metoda
prořezávání	prořezávání	k1gNnSc4	prořezávání
popsanou	popsaný	k2eAgFnSc4d1	popsaná
výše	výše	k1gFnSc1	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
řez	řez	k1gInSc1	řez
maliníku	maliník	k1gInSc2	maliník
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
brzy	brzy	k6eAd1	brzy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
odrůd	odrůda	k1gFnPc2	odrůda
maliníku	maliník	k1gInSc2	maliník
a	a	k8xC	a
maliníku	maliník	k1gInSc2	maliník
vysazeného	vysazený	k2eAgInSc2d1	vysazený
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
živné	živný	k2eAgFnSc6d1	živná
půdě	půda	k1gFnSc6	půda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
třeba	třeba	k6eAd1	třeba
provést	provést	k5eAaPmF	provést
řez	řez	k1gInSc4	řez
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
případě	případ	k1gInSc6	případ
však	však	k9	však
většina	většina	k1gFnSc1	většina
energie	energie	k1gFnSc2	energie
rostlin	rostlina	k1gFnPc2	rostlina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
vegetativní	vegetativní	k2eAgInSc4d1	vegetativní
růst	růst	k1gInSc4	růst
a	a	k8xC	a
výnos	výnos	k1gInSc4	výnos
plodů	plod	k1gInPc2	plod
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k8xC	jak
maliníky	maliník	k1gInPc1	maliník
stárnou	stárnout	k5eAaImIp3nP	stárnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
pečlivě	pečlivě	k6eAd1	pečlivě
řezané	řezaný	k2eAgFnPc1d1	řezaná
<g/>
,	,	kIx,	,
prostory	prostora	k1gFnPc1	prostora
mezi	mezi	k7c7	mezi
rostlinami	rostlina	k1gFnPc7	rostlina
postupně	postupně	k6eAd1	postupně
zaplní	zaplnit	k5eAaPmIp3nP	zaplnit
a	a	k8xC	a
řádky	řádek	k1gInPc1	řádek
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
téměř	téměř	k6eAd1	téměř
pevným	pevný	k2eAgInSc7d1	pevný
plotem	plot	k1gInSc7	plot
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
sklizně	sklizeň	k1gFnSc2	sklizeň
a	a	k8xC	a
pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
udržení	udržení	k1gNnSc1	udržení
vitality	vitalita	k1gFnSc2	vitalita
rostlin	rostlina	k1gFnPc2	rostlina
by	by	kYmCp3nP	by
nemělo	mít	k5eNaImAgNnS	mít
být	být	k5eAaImF	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rostlina	rostlina	k1gFnSc1	rostlina
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
výhonů	výhon	k1gInPc2	výhon
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
výhonů	výhon	k1gInPc2	výhon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
podle	podle	k7c2	podle
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
místní	místní	k2eAgFnPc1d1	místní
podmínky	podmínka	k1gFnPc1	podmínka
výrazně	výrazně	k6eAd1	výrazně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
růst	růst	k1gInSc4	růst
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Řádky	řádek	k1gInPc1	řádek
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
co	co	k9	co
nejužší	úzký	k2eAgInPc1d3	nejužší
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
silného	silný	k2eAgInSc2d1	silný
řezu	řez	k1gInSc2	řez
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kultivace	kultivace	k1gFnSc1	kultivace
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
povrchu	povrch	k1gInSc2	povrch
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
a	a	k8xC	a
okopávání	okopávání	k1gNnSc1	okopávání
v	v	k7c6	v
řádcích	řádek	k1gInPc6	řádek
nebude	být	k5eNaImBp3nS	být
příliš	příliš	k6eAd1	příliš
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
<g/>
Nářadí	nářadí	k1gNnSc1	nářadí
pro	pro	k7c4	pro
řezVýhony	řezVýhona	k1gFnPc4	řezVýhona
malin	malina	k1gFnPc2	malina
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
ostružin	ostružina	k1gFnPc2	ostružina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
chráněny	chránit	k5eAaImNgInP	chránit
trny	trn	k1gInPc7	trn
<g/>
,	,	kIx,	,
že	že	k8xS	že
řez	řez	k1gInSc1	řez
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
případě	případ	k1gInSc6	případ
nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
nůžek	nůžky	k1gFnPc2	nůžky
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
pracovník	pracovník	k1gMnSc1	pracovník
vybaven	vybavit	k5eAaPmNgInS	vybavit
koženými	kožený	k2eAgFnPc7d1	kožená
rukavicemi	rukavice	k1gFnPc7	rukavice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usnadní	usnadnit	k5eAaPmIp3nP	usnadnit
práci	práce	k1gFnSc4	práce
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnojení	hnojení	k1gNnSc1	hnojení
===	===	k?	===
</s>
</p>
<p>
<s>
Ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
hnojení	hnojení	k1gNnSc1	hnojení
malin	malina	k1gFnPc2	malina
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
plantáži	plantáž	k1gFnSc6	plantáž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nehospodárná	hospodárný	k2eNgFnSc1d1	nehospodárná
aplikace	aplikace	k1gFnSc1	aplikace
hnojiv	hnojivo	k1gNnPc2	hnojivo
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
prohnojené	prohnojený	k2eAgInPc1d1	prohnojený
a	a	k8xC	a
obdělávané	obdělávaný	k2eAgInPc1d1	obdělávaný
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přihnojení	přihnojení	k1gNnSc2	přihnojení
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
živinami	živina	k1gFnPc7	živina
nemá	mít	k5eNaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
maliník	maliník	k1gInSc4	maliník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
použití	použití	k1gNnSc1	použití
množství	množství	k1gNnSc2	množství
hnojiva	hnojivo	k1gNnSc2	hnojivo
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
půdách	půda	k1gFnPc6	půda
bylo	být	k5eAaImAgNnS	být
plýtváním	plýtvání	k1gNnPc3	plýtvání
penězi	peníze	k1gInPc7	peníze
<g/>
.	.	kIx.	.
<g/>
Stájový	stájový	k2eAgInSc1d1	stájový
hnůj	hnůj	k1gInSc1	hnůj
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc4d1	dobré
hnojivo	hnojivo	k1gNnSc4	hnojivo
pro	pro	k7c4	pro
maliny	malina	k1gFnPc4	malina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
u	u	k7c2	u
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
vlhké	vlhký	k2eAgFnSc6d1	vlhká
<g/>
,	,	kIx,	,
úrodné	úrodný	k2eAgFnSc6d1	úrodná
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
přílišný	přílišný	k2eAgInSc4d1	přílišný
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Meziplodiny	meziplodina	k1gFnPc1	meziplodina
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc4d1	cenný
u	u	k7c2	u
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přispívají	přispívat	k5eAaImIp3nP	přispívat
humusem	humus	k1gInSc7	humus
<g/>
,	,	kIx,	,
a	a	k8xC	a
luštěniny	luštěnina	k1gFnPc1	luštěnina
mohou	moct	k5eAaImIp3nP	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
dusík	dusík	k1gInSc4	dusík
potřebný	potřebný	k2eAgInSc4d1	potřebný
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgInPc1d1	dobrý
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xC	jako
meziplodina	meziplodina	k1gFnSc1	meziplodina
pro	pro	k7c4	pro
maliny	malina	k1gFnPc4	malina
luštěniny	luštěnina	k1gFnSc2	luštěnina
<g/>
,	,	kIx,	,
jetel	jetel	k1gInSc4	jetel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
moučka	moučka	k1gFnSc1	moučka
<g/>
,	,	kIx,	,
struska	struska	k1gFnSc1	struska
a	a	k8xC	a
Thomasova	Thomasův	k2eAgFnSc1d1	Thomasova
moučka	moučka	k1gFnSc1	moučka
jsou	být	k5eAaImIp3nP	být
dobré	dobrý	k2eAgInPc1d1	dobrý
materiály	materiál	k1gInPc1	materiál
pro	pro	k7c4	pro
dodání	dodání	k1gNnSc4	dodání
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
pro	pro	k7c4	pro
maliník	maliník	k1gInSc4	maliník
<g/>
,	,	kIx,	,
také	také	k9	také
popel	popel	k1gInSc1	popel
z	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
draselná	draselný	k2eAgFnSc1d1	draselná
sůl	sůl	k1gFnSc1	sůl
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Efektivita	efektivita	k1gFnSc1	efektivita
hnojení	hnojení	k1gNnSc2	hnojení
maliníku	maliník	k1gInSc2	maliník
====	====	k?	====
</s>
</p>
<p>
<s>
Výhodně	výhodně	k6eAd1	výhodně
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
použití	použití	k1gNnSc2	použití
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
průmyslových	průmyslový	k2eAgNnPc6d1	průmyslové
hnojivech	hnojivo	k1gNnPc6	hnojivo
ve	v	k7c6	v
snadno	snadno	k6eAd1	snadno
rozpustné	rozpustný	k2eAgFnSc6d1	rozpustná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
hnojiva	hnojivo	k1gNnPc1	hnojivo
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
vyvolat	vyvolat	k5eAaPmF	vyvolat
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
zdravý	zdravý	k2eAgInSc4d1	zdravý
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
hnojiva	hnojivo	k1gNnSc2	hnojivo
brzy	brzy	k6eAd1	brzy
během	během	k7c2	během
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dřevo	dřevo	k1gNnSc1	dřevo
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
vyzrát	vyzrát	k5eAaPmF	vyzrát
před	před	k7c7	před
zimou	zima	k1gFnSc7	zima
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
upravit	upravit	k5eAaPmF	upravit
hnojení	hnojení	k1gNnSc4	hnojení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
maliny	malina	k1gFnPc1	malina
nereagují	reagovat	k5eNaBmIp3nP	reagovat
na	na	k7c4	na
hnojiva	hnojivo	k1gNnPc4	hnojivo
stejně	stejně	k6eAd1	stejně
silně	silně	k6eAd1	silně
jako	jako	k8xC	jako
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výzkumy	výzkum	k1gInPc4	výzkum
přinesly	přinést	k5eAaPmAgFnP	přinést
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hnoje	hnůj	k1gInPc1	hnůj
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ziskové	ziskový	k2eAgNnSc1d1	ziskové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
šetření	šetření	k1gNnSc6	šetření
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejčastější	častý	k2eAgFnSc1d3	nejčastější
praxe	praxe	k1gFnSc1	praxe
pěstitelů	pěstitel	k1gMnPc2	pěstitel
bylo	být	k5eAaImAgNnS	být
aplikovat	aplikovat	k5eAaBmF	aplikovat
hnojiva	hnojivo	k1gNnPc4	hnojivo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
stájového	stájový	k2eAgInSc2d1	stájový
hnoje	hnůj	k1gInSc2	hnůj
nebo	nebo	k8xC	nebo
směsí	směs	k1gFnPc2	směs
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
rostliny	rostlina	k1gFnPc1	rostlina
byly	být	k5eAaImAgFnP	být
vysázeny	vysázet	k5eAaPmNgFnP	vysázet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
rostliny	rostlina	k1gFnPc1	rostlina
maliníku	maliník	k1gInSc2	maliník
přišly	přijít	k5eAaPmAgFnP	přijít
do	do	k7c2	do
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
pěstitelé	pěstitel	k1gMnPc1	pěstitel
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
stájový	stájový	k2eAgInSc4d1	stájový
hnůj	hnůj	k1gInSc4	hnůj
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
použili	použít	k5eAaPmAgMnP	použít
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
hnojiva	hnojivo	k1gNnPc4	hnojivo
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
nepoužívalo	používat	k5eNaImAgNnS	používat
žádná	žádný	k3yNgNnPc1	žádný
hnojiva	hnojivo	k1gNnPc1	hnojivo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
na	na	k7c6	na
třiaosmdesáti	třiaosmdesát	k4xCc6	třiaosmdesát
farmách	farma	k1gFnPc6	farma
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
47	[number]	k4	47
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
farem	farma	k1gFnPc2	farma
nepoužilo	použít	k5eNaPmAgNnS	použít
ani	ani	k8xC	ani
hnůj	hnůj	k1gInSc4	hnůj
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
hnojiva	hnojivo	k1gNnPc1	hnojivo
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
byl	být	k5eAaImAgInS	být
116,69	[number]	k4	116,69
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
akru	akr	k1gInSc2	akr
plantáží	plantáž	k1gFnPc2	plantáž
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
možnosti	možnost	k1gFnPc1	možnost
zisku	zisk	k1gInSc2	zisk
maliníku	maliník	k1gInSc2	maliník
i	i	k8xC	i
za	za	k7c2	za
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
asi	asi	k9	asi
15	[number]	k4	15
procentech	procento	k1gNnPc6	procento
farem	farma	k1gFnPc2	farma
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
jak	jak	k8xC	jak
hnůj	hnůj	k1gInSc1	hnůj
tak	tak	k6eAd1	tak
i	i	k9	i
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
farmách	farma	k1gFnPc6	farma
byl	být	k5eAaImAgInS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
176,69	[number]	k4	176,69
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
akru	akr	k1gInSc2	akr
maliníkových	maliníkový	k2eAgFnPc2d1	Maliníková
plantáží	plantáž	k1gFnPc2	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
51	[number]	k4	51
procent	procento	k1gNnPc2	procento
vyšší	vysoký	k2eAgInSc1d2	vyšší
zisk	zisk	k1gInSc1	zisk
plynoucí	plynoucí	k2eAgInSc1d1	plynoucí
z	z	k7c2	z
hnojení	hnojení	k1gNnPc2	hnojení
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
plantážemi	plantáž	k1gFnPc7	plantáž
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
nebyla	být	k5eNaImAgNnP	být
použita	použít	k5eAaPmNgNnP	použít
žádná	žádný	k3yNgNnPc1	žádný
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zisku	zisk	k1gInSc2	zisk
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
odečteny	odečten	k2eAgInPc4d1	odečten
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
hnojení	hnojení	k1gNnSc4	hnojení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
získané	získaný	k2eAgFnPc1d1	získaná
statistickým	statistický	k2eAgInSc7d1	statistický
výzkumem	výzkum	k1gInSc7	výzkum
však	však	k9	však
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
velmi	velmi	k6eAd1	velmi
jasně	jasně	k6eAd1	jasně
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
prospěšná	prospěšný	k2eAgNnPc1d1	prospěšné
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
a	a	k8xC	a
výnosy	výnos	k1gInPc4	výnos
maliníku	maliník	k1gInSc2	maliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
asi	asi	k9	asi
26	[number]	k4	26
procentech	procento	k1gNnPc6	procento
farem	farma	k1gFnPc2	farma
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pouze	pouze	k6eAd1	pouze
stájový	stájový	k2eAgInSc1d1	stájový
hnůj	hnůj	k1gInSc1	hnůj
před	před	k7c7	před
výsadbou	výsadba	k1gFnSc7	výsadba
a	a	k8xC	a
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
farmách	farma	k1gFnPc6	farma
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
170,50	[number]	k4	170,50
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
akru	akr	k1gInSc2	akr
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
příjmu	příjem	k1gInSc2	příjem
o	o	k7c4	o
47	[number]	k4	47
procent	procento	k1gNnPc2	procento
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
farmami	farma	k1gFnPc7	farma
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
není	být	k5eNaImIp3nS	být
hnůj	hnůj	k1gInSc1	hnůj
ani	ani	k8xC	ani
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
hnojivo	hnojivo	k1gNnSc1	hnojivo
aplikováno	aplikován	k2eAgNnSc1d1	aplikováno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
menší	malý	k2eAgFnPc4d2	menší
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4	[number]	k4	4
procenta	procento	k1gNnSc2	procento
nižší	nízký	k2eAgNnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
zisk	zisk	k1gInSc4	zisk
zjištěný	zjištěný	k2eAgInSc4d1	zjištěný
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
jak	jak	k8xS	jak
hnoje	hnůj	k1gInPc4	hnůj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
hnoje	hnůj	k1gInSc2	hnůj
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
hnojiva	hnojivo	k1gNnPc1	hnojivo
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
efektivní	efektivní	k2eAgFnPc1d1	efektivní
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
ekonomická	ekonomický	k2eAgNnPc1d1	ekonomické
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tak	tak	k6eAd1	tak
malé	malý	k2eAgNnSc4d1	malé
zvýšení	zvýšení	k1gNnSc4	zvýšení
příjmů	příjem	k1gInPc2	příjem
nezaplatí	zaplatit	k5eNaPmIp3nP	zaplatit
náklady	náklad	k1gInPc1	náklad
za	za	k7c4	za
hnojiva	hnojivo	k1gNnPc4	hnojivo
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Poškození	poškozený	k2eAgMnPc1d1	poškozený
mrazem	mráz	k1gInSc7	mráz
====	====	k?	====
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
poškození	poškození	k1gNnSc2	poškození
chladem	chlad	k1gInSc7	chlad
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
u	u	k7c2	u
maliníků	maliník	k1gInPc2	maliník
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
jim	on	k3xPp3gMnPc3	on
být	být	k5eAaImF	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
určitá	určitý	k2eAgFnSc1d1	určitá
forma	forma	k1gFnSc1	forma
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Nejuspokojivější	uspokojivý	k2eAgInSc1d3	nejuspokojivější
způsob	způsob	k1gInSc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
naorání	naorání	k1gNnSc1	naorání
půdy	půda	k1gFnSc2	půda
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
ohnutí	ohnutí	k1gNnSc1	ohnutí
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
keře	keř	k1gInSc2	keř
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
a	a	k8xC	a
zakrytí	zakrytí	k1gNnSc1	zakrytí
zeminou	zemina	k1gFnSc7	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Výhony	výhon	k1gInPc1	výhon
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
skloněny	sklonit	k5eAaPmNgInP	sklonit
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
v	v	k7c6	v
řádku	řádek	k1gInSc6	řádek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
výhony	výhon	k1gInPc4	výhon
jednoho	jeden	k4xCgNnSc2	jeden
keře	keř	k1gInPc4	keř
navazovala	navazovat	k5eAaImAgFnS	navazovat
koruna	koruna	k1gFnSc1	koruna
dalšího	další	k1gNnSc2	další
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
jsou	být	k5eAaImIp3nP	být
vrcholy	vrchol	k1gInPc1	vrchol
keře	keř	k1gInSc2	keř
ohnuty	ohnut	k2eAgInPc1d1	ohnut
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
malý	malý	k2eAgInSc1d1	malý
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
otočeny	otočit	k5eAaPmNgInP	otočit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Výhony	výhon	k1gInPc1	výhon
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
narovnány	narovnat	k5eAaPmNgInP	narovnat
brzy	brzy	k6eAd1	brzy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
navršena	navršen	k2eAgFnSc1d1	navršena
na	na	k7c6	na
kořenech	kořen	k1gInPc6	kořen
na	na	k7c6	na
uvolněné	uvolněný	k2eAgFnSc6d1	uvolněná
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
obzvláště	obzvláště	k6eAd1	obzvláště
účinný	účinný	k2eAgInSc1d1	účinný
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
namrzání	namrzání	k1gNnSc2	namrzání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sklizeň	sklizeň	k1gFnSc1	sklizeň
a	a	k8xC	a
marketing	marketing	k1gInSc1	marketing
===	===	k?	===
</s>
</p>
<p>
<s>
Maliník	maliník	k1gInSc1	maliník
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
sklizen	sklizen	k2eAgInSc1d1	sklizen
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
plod	plod	k1gInSc4	plod
nelze	lze	k6eNd1	lze
oddělit	oddělit	k5eAaPmF	oddělit
snadno	snadno	k6eAd1	snadno
od	od	k7c2	od
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
sklizen	sklidit	k5eAaPmNgInS	sklidit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc1	fáze
zralosti	zralost	k1gFnSc2	zralost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
tak	tak	k9	tak
zralým	zralý	k2eAgMnSc7d1	zralý
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
střeseno	střást	k5eAaPmNgNnS	střást
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
bobulí	bobule	k1gFnPc2	bobule
z	z	k7c2	z
keře	keř	k1gInSc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
být	být	k5eAaImF	být
sklizeno	sklizen	k2eAgNnSc1d1	sklizeno
mokré	mokrý	k2eAgNnSc1d1	mokré
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
jeho	jeho	k3xOp3gFnPc4	jeho
přepravní	přepravní	k2eAgFnPc4d1	přepravní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
<g/>
Maliny	malina	k1gFnPc4	malina
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
hodně	hodně	k6eAd1	hodně
péče	péče	k1gFnSc2	péče
ve	v	k7c6	v
sběru	sběr	k1gInSc6	sběr
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
manipulaci	manipulace	k1gFnSc6	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
sběrači	sběrač	k1gMnPc1	sběrač
malin	malina	k1gFnPc2	malina
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ne	ne	k9	ne
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
hbité	hbitý	k2eAgInPc1d1	hbitý
a	a	k8xC	a
rychlé	rychlý	k2eAgInPc1d1	rychlý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
přirozeně	přirozeně	k6eAd1	přirozeně
sbírat	sbírat	k5eAaImF	sbírat
plody	plod	k1gInPc4	plod
lehce	lehko	k6eAd1	lehko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zabrání	zabránit	k5eAaPmIp3nS	zabránit
otlakům	otlak	k1gInPc3	otlak
<g/>
.	.	kIx.	.
</s>
<s>
Malina	malina	k1gFnSc1	malina
je	být	k5eAaImIp3nS	být
dutá	dutý	k2eAgFnSc1d1	dutá
a	a	k8xC	a
nevydrží	vydržet	k5eNaPmIp3nS	vydržet
velký	velký	k2eAgInSc4d1	velký
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
plody	plod	k1gInPc4	plod
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
prodávány	prodávat	k5eAaImNgInP	prodávat
v	v	k7c6	v
mělkých	mělký	k2eAgInPc6d1	mělký
půllitrových	půllitrův	k2eAgInPc6d1	půllitrův
košících	košík	k1gInPc6	košík
vyrobených	vyrobený	k2eAgInPc6d1	vyrobený
z	z	k7c2	z
dýhy	dýha	k1gFnSc2	dýha
<g/>
.	.	kIx.	.
</s>
<s>
Sběrači	sběrač	k1gMnPc1	sběrač
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
po	po	k7c4	po
nachystání	nachystání	k1gNnSc4	nachystání
malin	malina	k1gFnPc2	malina
<g/>
.	.	kIx.	.
</s>
<s>
Sběr	sběr	k1gInSc1	sběr
ovoce	ovoce	k1gNnSc2	ovoce
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
pečlivě	pečlivě	k6eAd1	pečlivě
proveden	provést	k5eAaPmNgInS	provést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mebylo	mebýt	k5eAaPmAgNnS	mebýt
třeba	třeba	k9	třeba
dalšího	další	k2eAgNnSc2d1	další
třídění	třídění	k1gNnSc2	třídění
nebo	nebo	k8xC	nebo
přečištění	přečištění	k1gNnSc2	přečištění
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
pro	pro	k7c4	pro
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
v	v	k7c6	v
případě	případ	k1gInSc6	případ
malin	malina	k1gFnPc2	malina
ještě	ještě	k6eAd1	ještě
zdaleka	zdaleka	k6eAd1	zdaleka
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
druhů	druh	k1gMnPc2	druh
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
citlivé	citlivý	k2eAgFnSc2d1	citlivá
povahy	povaha	k1gFnSc2	povaha
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
má	mít	k5eAaImIp3nS	mít
pěstitel	pěstitel	k1gMnSc1	pěstitel
ovoce	ovoce	k1gNnSc2	ovoce
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc4d2	veliký
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
může	moct	k5eAaImIp3nS	moct
prodat	prodat	k5eAaPmF	prodat
své	svůj	k3xOyFgFnPc4	svůj
maliny	malina	k1gFnPc4	malina
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
musí	muset	k5eAaImIp3nP	muset
dopravit	dopravit	k5eAaPmF	dopravit
zboží	zboží	k1gNnSc4	zboží
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Ada	Ada	kA	Ada
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
-	-	kIx~	-
remontuje	remontovat	k5eAaBmIp3nS	remontovat
</s>
</p>
<p>
<s>
Apricot	Apricot	k1gInSc1	Apricot
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
-	-	kIx~	-
remontuje	remontovat	k5eAaBmIp3nS	remontovat
</s>
</p>
<p>
<s>
Autumn	Autumn	k1gMnSc1	Autumn
bliss	bliss	k6eAd1	bliss
</s>
</p>
<p>
<s>
Canby	Canba	k1gFnPc1	Canba
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Granát	granát	k1gInSc1	granát
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ljulin	Ljulin	k1gInSc1	Ljulin
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lloyd	Lloyd	k1gMnSc1	Lloyd
George	Georg	k1gMnSc2	Georg
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Medea	Medea	k1gFnSc1	Medea
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
-	-	kIx~	-
remontuje	remontovat	k5eAaBmIp3nS	remontovat
</s>
</p>
<p>
<s>
Polana	polana	k1gFnSc1	polana
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poranna	Poranen	k2eAgFnSc1d1	Poranen
Rosa	Rosa	k1gFnSc1	Rosa
-	-	kIx~	-
žluté	žlutý	k2eAgInPc1d1	žlutý
plody	plod	k1gInPc1	plod
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
bylo	být	k5eAaImAgNnS	být
vypěstováno	vypěstovat	k5eAaPmNgNnS	vypěstovat
mnoho	mnoho	k4c1	mnoho
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
maliníku	maliník	k1gInSc2	maliník
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
na	na	k7c6	na
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maliny	malina	k1gFnPc4	malina
lze	lze	k6eAd1	lze
prodávat	prodávat	k5eAaImF	prodávat
syrové	syrový	k2eAgNnSc4d1	syrové
<g/>
,	,	kIx,	,
konzervované	konzervovaný	k2eAgNnSc4d1	konzervované
<g/>
,	,	kIx,	,
zmražené	zmražený	k2eAgNnSc4d1	zmražené
<g/>
,	,	kIx,	,
zpracovat	zpracovat	k5eAaPmF	zpracovat
na	na	k7c4	na
marmelády	marmeláda	k1gFnPc4	marmeláda
a	a	k8xC	a
džemy	džem	k1gInPc1	džem
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgFnPc1d1	ovocná
šťávy	šťáva	k1gFnPc1	šťáva
<g/>
,	,	kIx,	,
sirupy	sirup	k1gInPc1	sirup
a	a	k8xC	a
mošty	mošt	k1gInPc1	mošt
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
hroznové	hroznový	k2eAgFnSc2d1	Hroznová
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sušené	sušený	k2eAgFnPc4d1	sušená
maliny	malina	k1gFnPc4	malina
–	–	k?	–
maliny	malina	k1gFnPc4	malina
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
sušeny	sušit	k5eAaImNgInP	sušit
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sušené	sušený	k2eAgFnPc1d1	sušená
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taková	takový	k3xDgFnSc1	takový
praxe	praxe	k1gFnSc1	praxe
není	být	k5eNaImIp3nS	být
rentabilní	rentabilní	k2eAgFnSc1d1	rentabilní
<g/>
.	.	kIx.	.
</s>
<s>
Maliny	malina	k1gFnPc1	malina
ztratí	ztratit	k5eAaPmIp3nP	ztratit
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
váhu	váha	k1gFnSc4	váha
při	při	k7c6	při
vysušení	vysušení	k1gNnSc6	vysušení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
ztráty	ztráta	k1gFnSc2	ztráta
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
maliník	maliník	k1gInSc4	maliník
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
změní	změnit	k5eAaPmIp3nS	změnit
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
neatraktivní	atraktivní	k2eNgFnSc4d1	neatraktivní
temně	temně	k6eAd1	temně
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
populární	populární	k2eAgNnSc1d1	populární
<g/>
.	.	kIx.	.
<g/>
Plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
šťáva	šťáva	k1gFnSc1	šťáva
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
antioxidantů	antioxidant	k1gInPc2	antioxidant
cenné	cenný	k2eAgFnSc3d1	cenná
při	pře	k1gFnSc3	pře
horečnatých	horečnatý	k2eAgFnPc2d1	horečnatá
a	a	k8xC	a
zánětlivých	zánětlivý	k2eAgNnPc6d1	zánětlivé
onemocněních	onemocnění	k1gNnPc6	onemocnění
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
průdušek	průduška	k1gFnPc2	průduška
<g/>
,	,	kIx,	,
revma	revma	k1gNnSc1	revma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
kuřáků	kuřák	k1gMnPc2	kuřák
a	a	k8xC	a
alkoholiků	alkoholik	k1gMnPc2	alkoholik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
léčitelství	léčitelství	k1gNnSc6	léčitelství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
sušený	sušený	k2eAgInSc1d1	sušený
list	list	k1gInSc1	list
maliníku	maliník	k1gInSc2	maliník
-	-	kIx~	-
výluhy	výluh	k1gInPc1	výluh
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
kloktání	kloktání	k1gNnSc3	kloktání
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgInSc1d1	ústní
a	a	k8xC	a
k	k	k7c3	k
potírání	potírání	k1gNnSc3	potírání
kožních	kožní	k2eAgFnPc2d1	kožní
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hodnotnou	hodnotný	k2eAgFnSc4d1	hodnotná
součást	součást	k1gFnSc4	součást
celoročních	celoroční	k2eAgFnPc2d1	celoroční
bylinných	bylinný	k2eAgFnPc2d1	bylinná
čajových	čajový	k2eAgFnPc2d1	čajová
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Fermentací	fermentace	k1gFnSc7	fermentace
se	se	k3xPyFc4	se
senzorická	senzorický	k2eAgFnSc1d1	senzorická
hodnota	hodnota	k1gFnSc1	hodnota
drogy	droga	k1gFnSc2	droga
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hyams	Hyams	k1gInSc1	Hyams
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
;	;	kIx,	;
Rostliny	rostlina	k1gFnPc1	rostlina
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
člověka	člověk	k1gMnSc2	člověk
<g/>
;	;	kIx,	;
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
srstky	srstka	k1gFnPc1	srstka
<g/>
,	,	kIx,	,
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
maliny	malina	k1gFnPc1	malina
a	a	k8xC	a
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ovocnický	ovocnický	k2eAgInSc1d1	ovocnický
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Maliny	malina	k1gFnSc2	malina
a	a	k8xC	a
ostružiny	ostružina	k1gFnSc2	ostružina
<g/>
,	,	kIx,	,
s.	s.	k?	s.
179	[number]	k4	179
<g/>
-	-	kIx~	-
<g/>
190	[number]	k4	190
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ostružiník	ostružiník	k1gInSc1	ostružiník
maliník	maliník	k1gInSc4	maliník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ostružiník	ostružiník	k1gInSc1	ostružiník
maliník	maliník	k1gInSc1	maliník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
https://web.archive.org/web/20071016140953/http://www.rendy.eu/rostliny-seznam/malinik-obecny	[url]	k1gFnPc1	https://web.archive.org/web/20071016140953/http://www.rendy.eu/rostliny-seznam/malinik-obecny
</s>
</p>
