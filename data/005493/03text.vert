<s>
Žák	Žák	k1gMnSc1	Žák
(	(	kIx(	(
<g/>
přechýlená	přechýlený	k2eAgFnSc1d1	přechýlená
podoba	podoba	k1gFnSc1	podoba
žákyně	žákyně	k1gFnSc1	žákyně
nebo	nebo	k8xC	nebo
žačka	žačka	k1gFnSc1	žačka
<g/>
,	,	kIx,	,
hromadně	hromadně	k6eAd1	hromadně
žactvo	žactvo	k1gNnSc1	žactvo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
procesu	proces	k1gInSc2	proces
vzdělává	vzdělávat	k5eAaImIp3nS	vzdělávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
základního	základní	k2eAgInSc2d1	základní
(	(	kIx(	(
<g/>
hovorové	hovorový	k2eAgNnSc4d1	hovorové
synonymum	synonymum	k1gNnSc4	synonymum
školák	školák	k1gMnSc1	školák
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
středního	střední	k2eAgNnSc2d1	střední
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
často	často	k6eAd1	často
neformálně	formálně	k6eNd1	formálně
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pojem	pojem	k1gInSc4	pojem
student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
žáka	žák	k1gMnSc4	žák
vzdělává	vzdělávat	k5eAaImIp3nS	vzdělávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
proces	proces	k1gInSc1	proces
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
kurs	kurs	k1gInSc1	kurs
<g/>
,	,	kIx,	,
školení	školení	k1gNnSc1	školení
apod.	apod.	kA	apod.
Slovem	slovem	k6eAd1	slovem
žák	žák	k1gMnSc1	žák
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgFnP	označovat
osoby	osoba	k1gFnPc1	osoba
vzdělávající	vzdělávající	k2eAgFnPc1d1	vzdělávající
se	se	k3xPyFc4	se
na	na	k7c6	na
nižších	nízký	k2eAgInPc6d2	nižší
typech	typ	k1gInPc6	typ
škol	škola	k1gFnPc2	škola
-	-	kIx~	-
typicky	typicky	k6eAd1	typicky
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
typech	typ	k1gInPc6	typ
škol	škola	k1gFnPc2	škola
bývají	bývat	k5eAaImIp3nP	bývat
žáci	žák	k1gMnPc1	žák
označováni	označován	k2eAgMnPc1d1	označován
slovem	slovem	k6eAd1	slovem
studující	studující	k2eAgMnPc1d1	studující
či	či	k8xC	či
student	student	k1gMnSc1	student
-	-	kIx~	-
typicky	typicky	k6eAd1	typicky
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Žákem	Žák	k1gMnSc7	Žák
se	se	k3xPyFc4	se
osoba	osoba	k1gFnSc1	osoba
stává	stávat	k5eAaImIp3nS	stávat
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nějaké	nějaký	k3yIgFnSc2	nějaký
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
instituce	instituce	k1gFnSc2	instituce
–	–	k?	–
nejčastěji	často	k6eAd3	často
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
je	být	k5eAaImIp3nS	být
organizováno	organizovat	k5eAaBmNgNnS	organizovat
po	po	k7c6	po
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
součástí	součást	k1gFnSc7	součást
nějaké	nějaký	k3yIgFnSc2	nějaký
třídy	třída	k1gFnSc2	třída
nebo	nebo	k8xC	nebo
studijní	studijní	k2eAgFnSc2d1	studijní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Žák	Žák	k1gMnSc1	Žák
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
a	a	k8xC	a
soustavný	soustavný	k2eAgInSc4d1	soustavný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
žák	žák	k1gMnSc1	žák
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
získává	získávat	k5eAaImIp3nS	získávat
nové	nový	k2eAgFnPc4d1	nová
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
si	se	k3xPyFc3	se
soustavně	soustavně	k6eAd1	soustavně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
etapy	etapa	k1gFnPc4	etapa
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
procesu	proces	k1gInSc2	proces
zakončeny	zakončen	k2eAgFnPc1d1	zakončena
ověřováním	ověřování	k1gNnSc7	ověřování
nabytých	nabytý	k2eAgFnPc2d1	nabytá
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
ústní	ústní	k2eAgMnSc1d1	ústní
nebo	nebo	k8xC	nebo
písemná	písemný	k2eAgFnSc1d1	písemná
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
,	,	kIx,	,
test	test	k1gInSc1	test
apod.	apod.	kA	apod.
Děti	dítě	k1gFnPc4	dítě
navštěvující	navštěvující	k2eAgFnSc2d1	navštěvující
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
předškoláci	předškolák	k1gMnPc1	předškolák
<g/>
.	.	kIx.	.
</s>
<s>
Podkoní	podkoní	k1gMnSc1	podkoní
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
Student	student	k1gMnSc1	student
Učeň	učeň	k1gMnSc1	učeň
Žákovská	žákovský	k2eAgFnSc1d1	žákovská
knížka	knížka	k1gFnSc1	knížka
Žákovská	žákovský	k2eAgFnSc1d1	žákovská
poezie	poezie	k1gFnSc1	poezie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
žák	žák	k1gMnSc1	žák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
žák	žák	k1gMnSc1	žák
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Žák	Žák	k1gMnSc1	Žák
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
