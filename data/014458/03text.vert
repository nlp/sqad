<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
Název	název	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Synot	Synot	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
Založen	založen	k2eAgMnSc1d1
</s>
<s>
2000	#num#	k4
Asociace	asociace	k1gFnSc1
</s>
<s>
FAČR	FAČR	kA
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Městský	městský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Miroslava	Miroslav	k1gMnSc2
Valenty	Valenta	k1gMnSc2
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
8	#num#	k4
121	#num#	k4
Vedení	vedení	k1gNnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
JUDr.	JUDr.	kA
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Zemek	zemek	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Svědík	Svědík	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
[[	[[	k?
<g/>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
]]	]]	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
První	první	k4xOgInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc1
Slovácko	Slovácko	k1gNnSc4
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Synot	Synota	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
profesionální	profesionální	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
moravském	moravský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
ve	v	k7c6
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2000	#num#	k4
jako	jako	k9
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Synot	Synota	k1gFnPc2
sloučením	sloučení	k1gNnSc7
odvěkých	odvěký	k2eAgMnPc2d1
rivalů	rival	k1gMnPc2
FC	FC	kA
SYNOT	SYNOT	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
TJ	tj	kA
Jiskra	jiskra	k1gFnSc1
Staré	Stará	k1gFnSc2
Město	město	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
FC	FC	kA
SYNOT	SYNOT	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
české	český	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
fotbalové	fotbalový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
Městském	městský	k2eAgInSc6d1
fotbalovém	fotbalový	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
Miroslava	Miroslav	k1gMnSc2
Valenty	Valenta	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
kapacitu	kapacita	k1gFnSc4
8	#num#	k4
121	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
ze	z	k7c2
Starého	Starého	k2eAgNnSc2d1
Města	město	k1gNnSc2
</s>
<s>
Klub	klub	k1gInSc1
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
jako	jako	k8xS,k8xC
SK	Sk	kA
Staré	staré	k1gNnSc1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
pak	pak	k6eAd1
ve	v	k7c6
městě	město	k1gNnSc6
působily	působit	k5eAaImAgInP
celkem	celkem	k6eAd1
4	#num#	k4
kluby	klub	k1gInPc4
<g/>
:	:	kIx,
SK	Sk	kA
<g/>
,	,	kIx,
Orel	Orel	k1gMnSc1
<g/>
,	,	kIx,
Rudá	rudý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
a	a	k8xC
Viktoria	Viktoria	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
přejmenoval	přejmenovat	k5eAaPmAgInS
na	na	k7c4
Sokol	Sokol	k1gInSc4
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
o	o	k7c4
5	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
pak	pak	k6eAd1
na	na	k7c6
Jiskra	jiskra	k1gFnSc1
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
klub	klub	k1gInSc1
pohyboval	pohybovat	k5eAaImAgInS
v	v	k7c6
krajských	krajský	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
opustil	opustit	k5eAaPmAgInS
klub	klub	k1gInSc1
Jiskru	jiskra	k1gFnSc4
a	a	k8xC
přejmenoval	přejmenovat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
SFK	SFK	kA
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
ve	v	k7c6
staroměstském	staroměstský	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
začala	začít	k5eAaPmAgFnS
angažovat	angažovat	k5eAaBmF
firma	firma	k1gFnSc1
Synot	Synota	k1gFnPc2
a	a	k8xC
po	po	k7c6
fúzi	fúze	k1gFnSc6
občanského	občanský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
s	s	k7c7
ní	on	k3xPp3gFnSc7
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgInS
klub	klub	k1gInSc1
název	název	k1gInSc1
FC	FC	kA
Synot	Synot	k1gInSc4
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
až	až	k9
do	do	k7c2
postupu	postup	k1gInSc2
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
působil	působit	k5eAaImAgInS
klub	klub	k1gInSc1
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc6d3
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNL	ČNL	kA
nebo	nebo	k8xC
MSFL	MSFL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
potom	potom	k8xC
Synot	Synot	k1gInSc1
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
sloučení	sloučení	k1gNnSc3
se	s	k7c7
Slováckou	slovácký	k2eAgFnSc7d1
Slavií	slavie	k1gFnSc7
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
z	z	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
Nástup	nástup	k1gInSc1
hráčů	hráč	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
před	před	k7c7
venkovním	venkovní	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
s	s	k7c7
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
</s>
<s>
Fotbal	fotbal	k1gInSc1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
hrát	hrát	k5eAaImF
kolem	kolem	k7c2
roku	rok	k1gInSc2
1894	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgInSc1
organizovaný	organizovaný	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
začátku	začátek	k1gInSc6
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
letech	let	k1gInPc6
změnil	změnit	k5eAaPmAgInS
název	název	k1gInSc1
na	na	k7c4
AC	AC	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
vydržel	vydržet	k5eAaPmAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c6
SK	Sk	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
a	a	k8xC
následně	následně	k6eAd1
na	na	k7c4
Sokol	Sokol	k1gInSc4
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
změna	změna	k1gFnSc1
názvu	název	k1gInSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
o	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
TJ	tj	kA
Spartak	Spartak	k1gInSc4
Hradišťan	Hradišťan	k1gMnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
přišel	přijít	k5eAaPmAgInS
úspěch	úspěch	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
postupu	postup	k1gInSc2
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
klub	klub	k1gInSc4
vydržel	vydržet	k5eAaPmAgMnS
4	#num#	k4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
během	během	k7c2
nichž	jenž	k3xRgMnPc2
se	se	k3xPyFc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c6
TJ	tj	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujících	následující	k2eAgInPc2d1
28	#num#	k4
let	léto	k1gNnPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
let	léto	k1gNnPc2
1981	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
hrál	hrát	k5eAaImAgInS
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
vystoupil	vystoupit	k5eAaPmAgInS
fotbalový	fotbalový	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
z	z	k7c2
TJ	tj	kA
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
FC	FC	kA
TIC	TIC	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
postoupit	postoupit	k5eAaPmF
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
hned	hned	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
vyhrála	vyhrát	k5eAaPmAgFnS
a	a	k8xC
zaznamenala	zaznamenat	k5eAaPmAgFnS
tak	tak	k6eAd1
historický	historický	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
postupu	postup	k1gInSc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
však	však	k9
skončila	skončit	k5eAaPmAgFnS
poslední	poslední	k2eAgFnSc1d1
a	a	k8xC
sestoupila	sestoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
firma	firma	k1gFnSc1
TIC	TIC	kA
měla	mít	k5eAaImAgFnS
finanční	finanční	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
odstoupila	odstoupit	k5eAaPmAgFnS
z	z	k7c2
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
a	a	k8xC
místo	místo	k1gNnSc4
ní	on	k3xPp3gFnSc3
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
firma	firma	k1gFnSc1
JOKO	JOKO	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenal	znamenat	k5eAaImAgMnS
změnu	změna	k1gFnSc4
názvu	název	k1gInSc2
na	na	k7c4
FC	FC	kA
JOKO	JOKO	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
té	ten	k3xDgFnSc2
se	se	k3xPyFc4
však	však	k9
nepodařilo	podařit	k5eNaPmAgNnS
finančně	finančně	k6eAd1
stabilizovat	stabilizovat	k5eAaBmF
klub	klub	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
následoval	následovat	k5eAaImAgInS
sestup	sestup	k1gInSc1
až	až	k6eAd1
do	do	k7c2
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
už	už	k6eAd1
bez	bez	k7c2
„	„	k?
<g/>
JOKO	JOKO	kA
<g/>
“	“	k?
v	v	k7c6
názvu	název	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
sloučení	sloučení	k1gNnSc4
se	s	k7c7
staroměstským	staroměstský	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Společná	společný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Hned	hned	k6eAd1
první	první	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
po	po	k7c6
postupu	postup	k1gInSc6
do	do	k7c2
první	první	k4xOgFnSc2
ligy	liga	k1gFnSc2
nový	nový	k2eAgInSc4d1
klub	klub	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Synot	Synot	k1gInSc1
vybojoval	vybojovat	k5eAaPmAgInS
místo	místo	k1gNnSc4
v	v	k7c6
Intertoto	Intertota	k1gFnSc5
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
došel	dojít	k5eAaPmAgMnS
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synot	Synot	k1gMnSc1
měl	mít	k5eAaImAgMnS
ambice	ambice	k1gFnPc4
bojovat	bojovat	k5eAaImF
o	o	k7c4
nejvyšší	vysoký	k2eAgFnPc4d3
příčky	příčka	k1gFnPc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
2003	#num#	k4
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
(	(	kIx(
<g/>
Městský	městský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Miroslava	Miroslav	k1gMnSc2
Valenty	Valenta	k1gMnSc2
<g/>
)	)	kIx)
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
8	#num#	k4
121	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
371	#num#	k4
krytých	krytý	k2eAgInPc2d1
<g/>
)	)	kIx)
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
tak	tak	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
malý	malý	k2eAgInSc1d1
zastaralý	zastaralý	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Širůch	Širůcha	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
korupční	korupční	k2eAgFnSc6d1
aféře	aféra	k1gFnSc6
v	v	k7c6
českém	český	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
ale	ale	k8xC
klub	klub	k1gInSc4
opustila	opustit	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Synot	Synot	k1gMnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
a	a	k8xC
s	s	k7c7
finančními	finanční	k2eAgInPc7d1
problémy	problém	k1gInPc7
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
sestoupil	sestoupit	k5eAaPmAgMnS
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gambrinus	gambrinus	k1gInSc1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
po	po	k7c6
sestupu	sestup	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
klub	klub	k1gInSc1
znovu	znovu	k6eAd1
změnil	změnit	k5eAaPmAgInS
majitele	majitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2009	#num#	k4
klub	klub	k1gInSc1
koupil	koupit	k5eAaPmAgInS
licenci	licence	k1gFnSc4
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
ligu	liga	k1gFnSc4
od	od	k7c2
Čáslavi	Čáslav	k1gFnSc2
a	a	k8xC
po	po	k7c6
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
znovu	znovu	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
mezi	mezi	k7c4
českou	český	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
elitu	elita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc2
FC	FC	kA
SYNOT	SYNOT	kA
a	a	k8xC
FC	FC	kA
SYNOT	SYNOT	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
=	=	kIx~
<g/>
>	>	kIx)
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Synot	Synota	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Football	Football	k1gInSc1
Club	club	k1gInSc4
SYNOT	SYNOT	kA
<g/>
,	,	kIx,
a.s	a.s	k?
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
a.s	a.s	k?
<g/>
)	)	kIx)
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2021	#num#	k4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Michal	Michal	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Hofmann	Hofmann	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Timur	Timur	k1gMnSc1
Melekescev	Melekescva	k1gFnPc2
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Jan	Jan	k1gMnSc1
Kliment	Kliment	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jan	Jan	k1gMnSc1
Navrátil	Navrátil	k1gMnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Milan	Milan	k1gMnSc1
Petržela	Petržela	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Michal	Michal	k1gMnSc1
Kohút	Kohút	k1gMnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Josef	Josef	k1gMnSc1
Divíšek	Divíšek	k1gMnSc1
</s>
<s>
15	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Václav	Václav	k1gMnSc1
Jurečka	Jurečka	k1gMnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Patrik	Patrik	k1gMnSc1
Šimko	Šimko	k1gNnSc1
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc4
</s>
<s>
18	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Sadílek	Sadílek	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jan	Jan	k1gMnSc1
Kalabiška	Kalabišek	k1gInSc2
</s>
<s>
20	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Marek	Marek	k1gMnSc1
Havlík	Havlík	k1gMnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Vincour	Vincour	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Rigino	Rigino	k1gNnSc1
Cicilia	Cicilium	k1gNnSc2
</s>
<s>
23	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Petr	Petr	k1gMnSc1
Reinberk	Reinberk	k1gInSc4
</s>
<s>
24	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jakub	Jakub	k1gMnSc1
Rezek	Rezek	k1gMnSc1
</s>
<s>
25	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Vít	Vít	k1gMnSc1
Nemrava	Nemrava	k1gMnSc1
</s>
<s>
26	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Filip	Filip	k1gMnSc1
Kubala	Kubala	k1gMnSc1
</s>
<s>
27	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Michal	Michal	k1gMnSc1
Tomič	Tomič	k1gMnSc1
</s>
<s>
28	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1
Daníček	Daníček	k1gMnSc1
</s>
<s>
91	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Pavol	Pavol	k1gInSc1
Bajza	Bajza	k1gMnSc1
</s>
<s>
Změny	změna	k1gFnPc1
v	v	k7c6
kádru	kádr	k1gInSc6
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
přestupovém	přestupový	k2eAgNnSc6d1
období	období	k1gNnSc6
2021	#num#	k4
</s>
<s>
Příchody	příchod	k1gInPc1
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Odkud	odkud	k6eAd1
přišel	přijít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Odchody	odchod	k1gInPc4
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Kam	kam	k6eAd1
odešel	odejít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
ZPatrik	ZPatrik	k1gMnSc1
Hellebrand	Hellebrand	k1gInSc4
<g/>
21	#num#	k4
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Prahanávrat	Prahanávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
B-tým	B-té	k1gNnSc7
</s>
<s>
Soupiska	soupiska	k1gFnSc1
rezervního	rezervní	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nastupuje	nastupovat	k5eAaImIp3nS
v	v	k7c6
MSFL	MSFL	kA
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
23	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2020	#num#	k4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc4
</s>
<s>
B	B	kA
</s>
<s>
Tomas	Tomas	k1gMnSc1
Dzeminsky	Dzeminsky	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Radim	Radim	k1gMnSc1
Olšanský	olšanský	k2eAgMnSc1d1
</s>
<s>
O	o	k7c6
</s>
<s>
Jakub	Jakub	k1gMnSc1
Jamný	Jamný	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Dominik	Dominik	k1gMnSc1
Jahoda	Jahoda	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Jakub	Jakub	k1gMnSc1
Janík	Janík	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Leskovjan	Leskovjan	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Michal	Michal	k1gMnSc1
Miklík	Miklík	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Adam	Adam	k1gMnSc1
Řihák	Řihák	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Srubek	srubek	k1gInSc4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Filip	Filip	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Martin	Martin	k1gMnSc1
Baran	Baran	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Jan	Jan	k1gMnSc1
Pek	Pek	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Martin	Martin	k1gMnSc1
Řehola	řehola	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Marek	Marek	k1gMnSc1
Polášek	Polášek	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Tomaštík	Tomaštík	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Marcel	Marcel	k1gMnSc1
Hladký	Hladký	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Marek	Marek	k1gMnSc1
Janša	Janša	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Filip	Filip	k1gMnSc1
Vecheta	Vecheto	k1gNnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
37	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
36	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
40	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
+6	+6	k4
</s>
<s>
48	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+8	+8	k4
</s>
<s>
32	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+1	+1	k4
</s>
<s>
38	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
19	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
+13	+13	k4
</s>
<s>
48	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
39	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
31	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
41	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
37	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
40	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
37	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
40	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
ePojisteni	ePojisten	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
32	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
HET	HET	kA
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
–	–	k?
<g/>
9	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
135136164347-445	135136164347-445	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
130119103535042	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
130	#num#	k4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
Jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc1
korupční	korupční	k2eAgFnSc2d1
aféry	aféra	k1gFnSc2
bylo	být	k5eAaImAgNnS
Slovácku	Slovácko	k1gNnSc3
odečteno	odečten	k2eAgNnSc1d1
12	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
Slovácko	Slovácko	k1gNnSc1
odkoupilo	odkoupit	k5eAaPmAgNnS
licenci	licence	k1gFnSc4
na	na	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
od	od	k7c2
Čáslavi	Čáslav	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
SEP	SEP	kA
–	–	k?
Středoevropský	středoevropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
PI	pi	k0
2001	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PI	pi	k0
2002	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PI	pi	k0
2003	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Valenta	Valenta	k1gMnSc1
st.	st.	kA
</s>
<s>
čestný	čestný	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc4
a	a	k8xC
FanClubu	FanCluba	k1gFnSc4
Slovácko	Slovácko	k1gNnSc4
<g/>
,	,	kIx,
zesnul	zesnout	k5eAaPmAgMnS
v	v	k7c6
létě	léto	k1gNnSc6
2008	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
MFS	MFS	kA
Miroslava	Miroslav	k1gMnSc2
Valenty	Valenta	k1gMnSc2
</s>
<s>
Milan	Milan	k1gMnSc1
Kerbr	Kerbr	k1gMnSc1
</s>
<s>
odchovanec	odchovanec	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgInS
hlavně	hlavně	k9
za	za	k7c4
Sigmu	Sigma	k1gFnSc4
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
stříbrná	stříbrný	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
na	na	k7c4
EURO	euro	k1gNnSc4
1996	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Němčický	němčický	k2eAgMnSc1d1
</s>
<s>
kromě	kromě	k7c2
dvou	dva	k4xCgFnPc2
sezón	sezóna	k1gFnPc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
celou	celý	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kowalík	Kowalík	k1gMnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
34	#num#	k4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
ligy	liga	k1gFnSc2
v	v	k7c6
sezoně	sezona	k1gFnSc6
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Malár	Malár	k1gMnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgInS
také	také	k9
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
Praha	Praha	k1gFnSc1
a	a	k8xC
Slavii	slavie	k1gFnSc4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
19	#num#	k4
gólů	gól	k1gInPc2
</s>
<s>
Libor	Libor	k1gMnSc1
Soldán	Soldán	k1gMnSc1
</s>
<s>
univerzál	univerzál	k1gInSc1
<g/>
,	,	kIx,
odehrál	odehrát	k5eAaPmAgInS
za	za	k7c4
Slovácko	Slovácko	k1gNnSc4
ligovou	ligový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Podaný	podaný	k2eAgMnSc1d1
</s>
<s>
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
strůjců	strůjce	k1gMnPc2
postupu	postup	k1gInSc2
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Kufa	Kufa	k1gMnSc1
</s>
<s>
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
klubu	klub	k1gInSc2
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
</s>
<s>
odchovanec	odchovanec	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
stříbrná	stříbrný	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
na	na	k7c4
EURO	euro	k1gNnSc4
1996	#num#	k4
</s>
<s>
Andrej	Andrej	k1gMnSc1
Ljubčenko	Ljubčenka	k1gFnSc5
</s>
<s>
brankář	brankář	k1gMnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
</s>
<s>
odchovanec	odchovanec	k1gMnSc1
<g/>
,	,	kIx,
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
FC	FC	kA
Vítkovice	Vítkovice	k1gInPc4
a	a	k8xC
FC	FC	kA
Porto	porto	k1gNnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
</s>
<s>
brankář	brankář	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
Klubu	klub	k1gInSc2
ligových	ligový	k2eAgMnPc2d1
brankářů	brankář	k1gMnPc2
</s>
<s>
Michal	Michal	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
</s>
<s>
obránce	obránce	k1gMnSc1
<g/>
,	,	kIx,
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
aktuálně	aktuálně	k6eAd1
hraje	hrát	k5eAaImIp3nS
za	za	k7c4
1	#num#	k4
<g/>
.	.	kIx.
<g/>
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
je	být	k5eAaImIp3nS
rezervní	rezervní	k2eAgInSc4d1
tým	tým	k1gInSc4
Slovácka	Slovácko	k1gNnSc2
<g/>
,	,	kIx,
hrající	hrající	k2eAgFnSc7d1
do	do	k7c2
sezóny	sezóna	k1gFnSc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
Moravskoslezskou	moravskoslezský	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
z	z	k7c2
bývalého	bývalý	k2eAgInSc2d1
týmu	tým	k1gInSc2
Slovácké	slovácký	k2eAgFnSc2d1
Slavie	slavie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
sezoně	sezona	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odhlášení	odhlášení	k1gNnSc3
B-týmu	B-tým	k1gInSc2
z	z	k7c2
MSFL	MSFL	kA
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
bývalého	bývalý	k2eAgInSc2d1
C-týmu	C-tým	k1gInSc2
stalo	stát	k5eAaPmAgNnS
B-mužstvo	B-mužstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
tak	tak	k6eAd1
nastupovalo	nastupovat	k5eAaImAgNnS
v	v	k7c6
I.	I.	kA
B	B	kA
třídě	třída	k1gFnSc6
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
znovu	znovu	k6eAd1
přihlášení	přihlášení	k1gNnSc1
béčka	béčko	k1gNnSc2
do	do	k7c2
MSFL	MSFL	kA
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
po	po	k7c6
zrušení	zrušení	k1gNnSc6
Juniorské	juniorský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
D	D	kA
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33095164856	#num#	k4
<g/>
−	−	k?
<g/>
832	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330114153838037	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330117123640	#num#	k4
<g/>
−	−	k?
<g/>
440	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33015965637	#num#	k4
<g/>
+	+	kIx~
<g/>
1954	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330116134948	#num#	k4
<g/>
−	−	k?
<g/>
139	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330910113838037	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330154114733	#num#	k4
<g/>
+	+	kIx~
<g/>
1449	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330134143638	#num#	k4
<g/>
−	−	k?
<g/>
242	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33086162737	#num#	k4
<g/>
−	−	k?
<g/>
1030	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
42815675627	#num#	k4
<g/>
+	+	kIx~
<g/>
2951	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
32894153150	#num#	k4
<g/>
−	−	k?
<g/>
1931	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330118115353041	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330126125957	#num#	k4
<g/>
+	+	kIx~
<g/>
242	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33012994039	#num#	k4
<g/>
+	+	kIx~
<g/>
145	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330155106347	#num#	k4
<g/>
+	+	kIx~
<g/>
1650	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33098135046	#num#	k4
<g/>
+	+	kIx~
<g/>
435	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
3301010104141040	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
726111	#num#	k4
/	/	kIx~
3115654	#num#	k4
<g/>
+	+	kIx~
<g/>
238	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72697104551-637	72697104551-637	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
**	**	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
3185310193618	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
:	:	kIx,
Vítězné	vítězný	k2eAgNnSc1d1
mužstvo	mužstvo	k1gNnSc1
FK	FK	kA
Šardice	Šardice	k1gFnSc2
se	se	k3xPyFc4
postupu	postup	k1gInSc6
zřeklo	zřeknout	k5eAaPmAgNnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
rezervního	rezervní	k2eAgInSc2d1
týmu	tým	k1gInSc2
Slovácka	Slovácko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
:	:	kIx,
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
juniorského	juniorský	k2eAgInSc2d1
týmu	tým	k1gInSc2
využilo	využít	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
Slovácko	Slovácko	k1gNnSc1
možnost	možnost	k1gFnSc1
přihlášení	přihlášení	k1gNnSc1
B	B	kA
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
do	do	k7c2
třetí	třetí	k4xOgFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
**	**	k?
<g/>
=	=	kIx~
sezona	sezona	k1gFnSc1
předčasně	předčasně	k6eAd1
ukončena	ukončit	k5eAaPmNgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
pandemie	pandemie	k1gFnSc2
COVID-	COVID-	k1gFnSc2
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
byl	být	k5eAaImAgInS
druhý	druhý	k4xOgInSc1
rezervní	rezervní	k2eAgInSc1d1
tým	tým	k1gInSc1
Slovácka	Slovácko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
hrál	hrát	k5eAaImAgMnS
naposled	naposled	k6eAd1
v	v	k7c6
sezoně	sezona	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
I.	I.	kA
B	B	kA
třídu	třída	k1gFnSc4
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
sezoně	sezona	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odhlášení	odhlášení	k1gNnSc3
B-týmu	B-tým	k1gInSc2
z	z	k7c2
MSFL	MSFL	kA
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
bývalého	bývalý	k2eAgInSc2d1
C-týmu	C-tým	k1gInSc2
stalo	stát	k5eAaPmAgNnS
B-mužstvo	B-mužstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
A	a	k8xC
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
:	:	kIx,
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72694134757	#num#	k4
<g/>
−	−	k?
<g/>
1031	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72687114553	#num#	k4
<g/>
−	−	k?
<g/>
831	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72615657139	#num#	k4
<g/>
+	+	kIx~
<g/>
3251	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72615387339	#num#	k4
<g/>
+	+	kIx~
<g/>
3448	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
726104124853	#num#	k4
<g/>
−	−	k?
<g/>
534	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
726115104447	#num#	k4
<g/>
−	−	k?
<g/>
338	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72611785949	#num#	k4
<g/>
+	+	kIx~
<g/>
1040	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
72619436828	#num#	k4
<g/>
+	+	kIx~
<g/>
4061	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
62611694838	#num#	k4
<g/>
+	+	kIx~
<g/>
1039	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
62697104149	#num#	k4
<g/>
−	−	k?
<g/>
834	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
624112114441	#num#	k4
<g/>
+	+	kIx~
<g/>
335	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
62613495948	#num#	k4
<g/>
+	+	kIx~
<g/>
1143	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2
</s>
<s>
82615384436	#num#	k4
<g/>
+	+	kIx~
<g/>
848	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2
</s>
<s>
82615296349	#num#	k4
<g/>
+	+	kIx~
<g/>
1447	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2
</s>
<s>
82617367944	#num#	k4
<g/>
+	+	kIx~
<g/>
3554	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
726181	#num#	k4
/	/	kIx~
345132	#num#	k4
<g/>
+	+	kIx~
<g/>
1959	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
726161	#num#	k4
/	/	kIx~
186860	#num#	k4
<g/>
+	+	kIx~
<g/>
851	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
:	:	kIx,
Rezervní	rezervní	k2eAgInSc1d1
tým	tým	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
sezóně	sezóna	k1gFnSc6
přihlášen	přihlášen	k2eAgMnSc1d1
pouze	pouze	k6eAd1
do	do	k7c2
Okresního	okresní	k2eAgInSc2d1
přeboru	přebor	k1gInSc2
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
:	:	kIx,
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
ve	v	k7c6
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
Pokud	pokud	k8xS
zápas	zápas	k1gInSc1
skončí	skončit	k5eAaPmIp3nS
nerozhodně	rozhodně	k6eNd1
<g/>
,	,	kIx,
kope	kopat	k5eAaImIp3nS
se	se	k3xPyFc4
penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
vítěz	vítěz	k1gMnSc1
bere	brát	k5eAaImIp3nS
2	#num#	k4
body	bod	k1gInPc4
<g/>
,	,	kIx,
poražený	poražený	k2eAgInSc4d1
pak	pak	k6eAd1
jeden	jeden	k4xCgInSc1
bod	bod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výhru	výhra	k1gFnSc4
po	po	k7c6
90	#num#	k4
minutách	minuta	k1gFnPc6
jsou	být	k5eAaImIp3nP
3	#num#	k4
body	bod	k1gInPc4
<g/>
,	,	kIx,
za	za	k7c4
prohru	prohra	k1gFnSc4
po	po	k7c6
90	#num#	k4
minutách	minuta	k1gFnPc6
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
"	"	kIx"
<g/>
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcslovacko	fcslovacko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Archiv	archiv	k1gInSc1
Rudého	rudý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
1965-19891	1965-19891	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Nižší	nízký	k2eAgFnSc2d2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
(	(	kIx(
<g/>
jfk-fotbal	jfk-fotbal	k1gInSc1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
1896	#num#	k4
–	–	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Libri	Libre	k1gFnSc4
1997.1	1997.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Archiv	archiv	k1gInSc1
soutěží	soutěžit	k5eAaImIp3nS
<g/>
,	,	kIx,
výsledkový	výsledkový	k2eAgInSc1d1
servis	servis	k1gInSc1
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
↑	↑	k?
DIVIZE	divize	k1gFnSc2
<g/>
,	,	kIx,
skup	skupa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
D	D	kA
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
czechsoccernet	czechsoccernet	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
od	od	k7c2
sezony	sezona	k1gFnSc2
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
béčka	béčko	k1gNnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Archiv	archiv	k1gInSc1
soutěží	soutěž	k1gFnPc2
Zlínského	zlínský	k2eAgInSc2d1
KFS	KFS	kA
od	od	k7c2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
včetně	včetně	k7c2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
souteze	souteze	k1gFnSc1
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://vysledky.lidovky.cz/soutez.php?id_soutez=94771	http://vysledky.lidovky.cz/soutez.php?id_soutez=94771	k4
2	#num#	k4
"	"	kIx"
<g/>
Archiv	archiv	k1gInSc1
Zlínského	zlínský	k2eAgNnSc2d1
KFS	KFS	kA
(	(	kIx(
<g/>
od	od	k7c2
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
včetně	včetně	k7c2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ofisport	ofisport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Archiv	archiv	k1gInSc1
sezon	sezona	k1gFnPc2
FK	FK	kA
Baník	Baník	k1gInSc1
Dubňany	Dubňan	k1gMnPc7
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fkbanikdubnany	fkbanikdubnana	k1gFnPc1
<g/>
.	.	kIx.
<g/>
estranky	estranka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archiv	archiv	k1gInSc1
sezon	sezona	k1gFnPc2
FC	FC	kA
Strání	stráň	k1gFnSc7
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcstrani	fcstran	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
-	-	kIx~
Uh	uh	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradiště	Hradiště	k1gNnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vysledky	vysledka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
-	-	kIx~
Uh	uh	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradiště	Hradiště	k1gNnSc1
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vysledky	vysledka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
-	-	kIx~
Uh	uh	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradiště	Hradiště	k1gNnSc1
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vysledky	vysledka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc4
fanclubu	fanclub	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc1
fanoušků	fanoušek	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
historie	historie	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
•	•	k?
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
•	•	k?
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
SK	Sk	kA
Sigma	sigma	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
•	•	k?
SFC	SFC	kA
Opava	Opava	k1gFnSc1
•	•	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
•	•	k?
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
1905	#num#	k4
•	•	k?
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Příbram	Příbram	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
•	•	k?
FK	FK	kA
Teplice	teplice	k1gFnSc2
•	•	k?
FC	FC	kA
Fastav	Fastav	k1gFnSc2
Zlín	Zlín	k1gInSc1
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
•	•	k?
FC	FC	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
Stadiony	stadion	k1gInPc7
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
•	•	k?
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Příbram	Příbram	k1gFnSc1
•	•	k?
Slovácko	Slovácko	k1gNnSc1
•	•	k?
Teplice	teplice	k1gFnSc2
•	•	k?
Zlín	Zlín	k1gInSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Brno	Brno	k1gNnSc1
Sezóny	sezóna	k1gFnSc2
(	(	kIx(
<g/>
historický	historický	k2eAgInSc4d1
přehled	přehled	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1925	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
•	•	k?
1927	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
Česko	Česko	k1gNnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
v	v	k7c6
ČSR	ČSR	kA
éře	éra	k1gFnSc6
pouze	pouze	k6eAd1
české	český	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nuselský	nuselský	k2eAgInSc4d1
SK	Sk	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Meteor	meteor	k1gInSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Kolín	Kolín	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DFC	DFC	kA
Prag	Prag	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DSV	DSV	kA
Saaz	Saaz	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teplitzer	Teplitzer	k1gInSc1
FK	FK	kA
(	(	kIx(
<g/>
1935	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Moravská	moravský	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rusj	Rusj	k1gInSc1
Užhorod	Užhorod	k1gInSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Nusle	Nusle	k1gFnPc1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Polaban	Polaban	k1gMnSc1
Nymburk	Nymburk	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SK	Sk	kA
Prostějov	Prostějov	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rakovník	Rakovník	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Olomouc	Olomouc	k1gFnSc1
ASO	ASO	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Židenice	Židenice	k1gFnSc2
2011	#num#	k4
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VTJ	VTJ	kA
Dukla	Dukla	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jiskra	jiskra	k1gFnSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dukla	Dukla	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
•	•	k?
TJ	tj	kA
Rudá	rudý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
LeRK	LeRK	k1gFnSc7
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Motorlet	motorlet	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
SK	Sk	kA
Baťov	Baťov	k1gInSc4
1930	#num#	k4
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
1899	#num#	k4
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Frýdek-Místek	Frýdek-Místek	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Vítkovice	Vítkovice	k1gInPc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Benešov	Benešov	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Union	union	k1gInSc1
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
SYNOT	SYNOT	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Atlantic	Atlantice	k1gFnPc2
Lázně	lázeň	k1gFnSc2
Bohdaneč	Bohdaneč	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Karviná	Karviná	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Drnovice	Drnovice	k1gInPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Chmel	chmel	k1gInSc4
Blšany	Blšana	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Baník	Baník	k1gInSc1
Most	most	k1gInSc1
1909	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Kladno	Kladno	k1gNnSc4
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SC	SC	kA
Znojmo	Znojmo	k1gNnSc1
FK	FK	kA
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Vysočina	vysočina	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
a	a	k8xC
trenér	trenér	k1gMnSc1
měsíce	měsíc	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Přihraj	přihrát	k5eAaPmRp2nS
<g/>
:	:	kIx,
<g/>
Král	Král	k1gMnSc1
asistencí	asistence	k1gFnPc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Česko-slovenský	česko-slovenský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Československa	Československo	k1gNnSc2
•	•	k?
Kluby	klub	k1gInPc7
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Pražské	pražský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
Fotbal	fotbal	k1gInSc4
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
I.	I.	kA
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
•	•	k?
Česká	český	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
FK	FK	kA
Blansko	Blansko	k1gNnSc1
•	•	k?
FC	FC	kA
Dolní	dolní	k2eAgInSc1d1
Benešov	Benešov	k1gInSc1
•	•	k?
MFK	MFK	kA
Frýdek-Místek	Frýdek-Místek	k1gInSc1
•	•	k?
FC	FC	kA
Hlučín	Hlučín	k1gInSc1
•	•	k?
SK	Sk	kA
Hanácká	hanácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
SK	Sk	kA
Sigma	sigma	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Otrokovice	Otrokovice	k1gFnPc1
•	•	k?
FC	FC	kA
Odra	Odra	k1gFnSc1
Petřkovice	Petřkovice	k1gFnPc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gMnSc1
Rosice	Rosice	k1gFnPc4
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
ČSK	ČSK	kA
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
SK	Sk	kA
Uničov	Uničov	k1gInSc1
•	•	k?
FC	FC	kA
Velké	velký	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
SFK	SFK	kA
Vrchovina	vrchovina	k1gFnSc1
Nové	Nová	k1gFnSc2
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
MFK	MFK	kA
Vyškov	Vyškov	k1gInSc1
•	•	k?
FC	FC	kA
Fastav	Fastav	k1gFnSc2
Zlín	Zlín	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SC	SC	kA
Znojmo	Znojmo	k1gNnSc1
FK	FK	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
206668	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
4543147553275453800007	#num#	k4
</s>
