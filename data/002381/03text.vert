<s>
Humanae	Humanae	k6eAd1	Humanae
vitae	vitae	k6eAd1	vitae
je	být	k5eAaImIp3nS	být
encyklika	encyklika	k1gFnSc1	encyklika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
církevních	církevní	k2eAgInPc2d1	církevní
dokumentů	dokument	k1gInPc2	dokument
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
slov	slovo	k1gNnPc2	slovo
latinského	latinský	k2eAgInSc2d1	latinský
originálu	originál	k1gInSc2	originál
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
předávání	předávání	k1gNnSc4	předávání
<g/>
]	]	kIx)	]
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Encyklika	encyklika	k1gFnSc1	encyklika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
manželskou	manželský	k2eAgFnSc7d1	manželská
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
odpovědným	odpovědný	k2eAgNnSc7d1	odpovědné
rodičovstvím	rodičovství	k1gNnSc7	rodičovství
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
tradiční	tradiční	k2eAgInSc4d1	tradiční
postoj	postoj	k1gInSc4	postoj
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
sexualitě	sexualita	k1gFnSc3	sexualita
a	a	k8xC	a
plánovanému	plánovaný	k2eAgNnSc3d1	plánované
rodičovství	rodičovství	k1gNnSc3	rodičovství
<g/>
,	,	kIx,	,
vyjádřený	vyjádřený	k2eAgInSc1d1	vyjádřený
předtím	předtím	k6eAd1	předtím
např.	např.	kA	např.
v	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
Casti	Casť	k1gFnSc2	Casť
connubii	connubie	k1gFnSc4	connubie
Pia	Pius	k1gMnSc2	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
ty	ten	k3xDgInPc4	ten
způsoby	způsob	k1gInPc4	způsob
kontroly	kontrola	k1gFnSc2	kontrola
početí	početí	k1gNnSc2	početí
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
záměrně	záměrně	k6eAd1	záměrně
způsobena	způsoben	k2eAgFnSc1d1	způsobena
neplodnost	neplodnost	k1gFnSc1	neplodnost
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
styku	styk	k1gInSc2	styk
(	(	kIx(	(
<g/>
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
i	i	k9	i
umělý	umělý	k2eAgInSc4d1	umělý
potrat	potrat	k1gInSc4	potrat
(	(	kIx(	(
<g/>
interrupci	interrupce	k1gFnSc4	interrupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
odpovědnému	odpovědný	k2eAgNnSc3d1	odpovědné
plánování	plánování	k1gNnSc3	plánování
rodiny	rodina	k1gFnSc2	rodina
pomocí	pomocí	k7c2	pomocí
metod	metoda	k1gFnPc2	metoda
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
plánování	plánování	k1gNnSc2	plánování
rodičovství	rodičovství	k1gNnSc4	rodičovství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
manželé	manžel	k1gMnPc1	manžel
"	"	kIx"	"
<g/>
oprávněně	oprávněně	k6eAd1	oprávněně
využívají	využívat	k5eAaImIp3nP	využívat
přirozené	přirozený	k2eAgFnPc1d1	přirozená
dispozice	dispozice	k1gFnPc1	dispozice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
nerozlučitelnost	nerozlučitelnost	k1gFnSc1	nerozlučitelnost
dvojího	dvojí	k4xRgInSc2	dvojí
významu	význam	k1gInSc2	význam
manželského	manželský	k2eAgInSc2d1	manželský
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
styku	styk	k1gInSc2	styk
<g/>
:	:	kIx,	:
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolickém	katolický	k2eAgNnSc6d1	katolické
teologickém	teologický	k2eAgNnSc6d1	teologické
prostředí	prostředí	k1gNnSc6	prostředí
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
názorové	názorový	k2eAgInPc1d1	názorový
proudy	proud	k1gInPc1	proud
(	(	kIx(	(
<g/>
teologický	teologický	k2eAgInSc1d1	teologický
disent	disent	k1gInSc1	disent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
některé	některý	k3yIgFnPc1	některý
závěry	závěra	k1gFnPc1	závěra
této	tento	k3xDgFnSc2	tento
encykliky	encyklika	k1gFnSc2	encyklika
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tuto	tento	k3xDgFnSc4	tento
encykliku	encyklika	k1gFnSc4	encyklika
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
<g/>
,	,	kIx,	,
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
a	a	k8xC	a
učitelskou	učitelský	k2eAgFnSc7d1	učitelská
autoritou	autorita	k1gFnSc7	autorita
podpořila	podpořit	k5eAaPmAgFnS	podpořit
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
oficiálních	oficiální	k2eAgInPc2d1	oficiální
církevních	církevní	k2eAgInPc2d1	církevní
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
např.	např.	kA	např.
exhortace	exhortace	k1gFnSc1	exhortace
Familiaris	Familiaris	k1gFnSc2	Familiaris
consortio	consortio	k1gNnSc1	consortio
<g/>
,	,	kIx,	,
encykliky	encyklika	k1gFnPc1	encyklika
Evangelium	evangelium	k1gNnSc1	evangelium
vitae	vitae	k1gNnSc1	vitae
a	a	k8xC	a
Veritatis	Veritatis	k1gFnSc1	Veritatis
splendor	splendora	k1gFnPc2	splendora
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
encykliky	encyklika	k1gFnSc2	encyklika
Deus	Deusa	k1gFnPc2	Deusa
caritas	caritas	k1gMnSc1	caritas
est	est	k?	est
a	a	k8xC	a
Caritas	Caritas	k1gInSc1	Caritas
in	in	k?	in
veritate	veritat	k1gInSc5	veritat
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
