<s>
Marchese	Marchese	k1gFnSc5	Marchese
Guglielmo	Guglielma	k1gFnSc5	Guglielma
Marconi	Marcoň	k1gFnSc6	Marcoň
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1874	[number]	k4	1874
Griffona	griffon	k1gMnSc2	griffon
u	u	k7c2	u
Bologne	Bologna	k1gFnSc2	Bologna
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Marconi	Marcoň	k1gFnSc3	Marcoň
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
bezdrátového	bezdrátový	k2eAgInSc2d1	bezdrátový
telegrafu	telegraf	k1gInSc2	telegraf
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
způsobu	způsob	k1gInSc3	způsob
radiového	radiový	k2eAgNnSc2d1	radiové
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
si	se	k3xPyFc3	se
ale	ale	k9	ale
stejný	stejný	k2eAgInSc4d1	stejný
vynález	vynález	k1gInSc4	vynález
patentoval	patentovat	k5eAaBmAgMnS	patentovat
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
společností	společnost	k1gFnPc2	společnost
podnikajících	podnikající	k2eAgFnPc2d1	podnikající
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
radiotelegrafického	radiotelegrafický	k2eAgNnSc2d1	radiotelegrafické
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
Radio	radio	k1gNnSc4	radio
Vatikán	Vatikán	k1gInSc1	Vatikán
(	(	kIx(	(
<g/>
provoz	provoz	k1gInSc1	provoz
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Braunem	Braun	k1gMnSc7	Braun
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Italské	italský	k2eAgFnSc2d1	italská
královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
výsledky	výsledek	k1gInPc4	výsledek
pokusů	pokus	k1gInPc2	pokus
Heinricha	Heinrich	k1gMnSc2	Heinrich
Hertze	hertz	k1gInSc5	hertz
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
zopakovat	zopakovat	k5eAaPmF	zopakovat
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1896	[number]	k4	1896
získává	získávat	k5eAaImIp3nS	získávat
patent	patent	k1gInSc4	patent
na	na	k7c4	na
bezdrátový	bezdrátový	k2eAgInSc4d1	bezdrátový
telegraf	telegraf	k1gInSc4	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
zakládá	zakládat	k5eAaImIp3nS	zakládat
telegrafní	telegrafní	k2eAgFnSc1d1	telegrafní
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
15	[number]	k4	15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
provádí	provádět	k5eAaImIp3nS	provádět
spojení	spojení	k1gNnSc4	spojení
z	z	k7c2	z
palub	paluba	k1gFnPc2	paluba
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
první	první	k4xOgFnSc4	první
sportovní	sportovní	k2eAgFnSc4d1	sportovní
reportáž	reportáž	k1gFnSc4	reportáž
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgNnSc4	první
transatlantické	transatlantický	k2eAgNnSc4d1	transatlantické
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
vynálezů	vynález	k1gInPc2	vynález
(	(	kIx(	(
<g/>
magnetický	magnetický	k2eAgInSc1d1	magnetický
detektor	detektor	k1gInSc1	detektor
<g/>
,	,	kIx,	,
duplexní	duplexní	k2eAgFnSc1d1	duplexní
radiotelegrafie	radiotelegrafie	k1gFnSc1	radiotelegrafie
<g/>
,	,	kIx,	,
rotační	rotační	k2eAgNnSc1d1	rotační
jiskřiště	jiskřiště	k1gNnSc1	jiskřiště
<g/>
,	,	kIx,	,
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
směrová	směrový	k2eAgFnSc1d1	směrová
anténa	anténa	k1gFnSc1	anténa
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
sporné	sporný	k2eAgFnPc1d1	sporná
-	-	kIx~	-
americký	americký	k2eAgInSc4d1	americký
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
ochranu	ochrana	k1gFnSc4	ochrana
některých	některý	k3yIgInPc2	některý
jeho	jeho	k3xOp3gInPc2	jeho
patentů	patent	k1gInPc2	patent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
zrušil	zrušit	k5eAaPmAgMnS	zrušit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
v	v	k7c6	v
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
starších	starý	k2eAgMnPc2d2	starší
patentech	patent	k1gInPc6	patent
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gMnSc2	Tesla
<g/>
.	.	kIx.	.
</s>
