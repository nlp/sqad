<p>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Jedunov	Jedunov	k1gInSc1	Jedunov
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
záložník	záložník	k1gMnSc1	záložník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
FK	FK	kA	FK
Šinnik	Šinnik	k1gMnSc1	Šinnik
Jaroslavl	Jaroslavl	k1gMnSc1	Jaroslavl
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Biokhimik-Mordovia	Biokhimik-Mordovia	k1gFnSc1	Biokhimik-Mordovia
Saransk	Saransk	k1gInSc1	Saransk
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Nara-Desna	Nara-Desna	k1gFnSc1	Nara-Desna
Naro-Fominsk	Naro-Fominsk	k1gInSc1	Naro-Fominsk
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Anži	Anž	k1gFnPc1	Anž
Machačkala	Machačkala	k1gMnSc1	Machačkala
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Spartak	Spartak	k1gInSc1	Spartak
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Chernomorets	Chernomorets	k1gInSc1	Chernomorets
Novorossiysk	Novorossiysk	k1gInSc1	Novorossiysk
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Nosta	Nosta	k1gFnSc1	Nosta
Novotroitsk	Novotroitsk	k1gInSc1	Novotroitsk
<g/>
,	,	kIx,	,
rezervu	rezerva	k1gFnSc4	rezerva
FC	FC	kA	FC
Lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Dynamo	dynamo	k1gNnSc4	dynamo
Stavropol	Stavropol	k1gInSc1	Stavropol
a	a	k8xC	a
FC	FC	kA	FC
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
sezóny	sezóna	k1gFnSc2	sezóna
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
k	k	k7c3	k
7	[number]	k4	7
utkáním	utkání	k1gNnPc3	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Worldfootball	Worldfootball	k1gInSc1	Worldfootball
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
<p>
<s>
Hradecký	hradecký	k2eAgMnSc1d1	hradecký
Rus	Rus	k1gMnSc1	Rus
Jedunov	Jedunov	k1gInSc1	Jedunov
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
Spartaku	Spartak	k1gInSc6	Spartak
nás	my	k3xPp1nPc4	my
učili	učít	k5eAaPmAgMnP	učít
hrát	hrát	k5eAaImF	hrát
kombinačně	kombinačně	k6eAd1	kombinačně
</s>
</p>
<p>
<s>
Player	Player	k1gInSc1	Player
History	Histor	k1gInPc1	Histor
</s>
</p>
