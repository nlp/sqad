<p>
<s>
Bajt	bajt	k1gInSc1	bajt
<g/>
,	,	kIx,	,
původním	původní	k2eAgInSc7d1	původní
<g/>
,	,	kIx,	,
anglickým	anglický	k2eAgInSc7d1	anglický
zápisem	zápis	k1gInSc7	zápis
byte	byte	k1gInSc1	byte
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
také	také	k9	také
slabika	slabika	k1gFnSc1	slabika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
množství	množství	k1gNnSc2	množství
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
většinou	většinou	k6eAd1	většinou
osm	osm	k4xCc1	osm
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
osmiciferné	osmiciferný	k2eAgNnSc4d1	osmiciferné
binární	binární	k2eAgNnSc4d1	binární
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
informace	informace	k1gFnSc2	informace
může	moct	k5eAaImIp3nS	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
například	například	k6eAd1	například
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
255	[number]	k4	255
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
bajt	bajt	k1gInSc1	bajt
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nejmenší	malý	k2eAgInSc4d3	nejmenší
objem	objem	k1gInSc4	objem
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
procesor	procesor	k1gInSc1	procesor
<g/>
)	)	kIx)	)
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
najednou	najednou	k6eAd1	najednou
<g/>
)	)	kIx)	)
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgFnPc2d1	stará
architektur	architektura	k1gFnPc2	architektura
mohl	moct	k5eAaImAgInS	moct
1	[number]	k4	1
bajt	bajt	k1gInSc1	bajt
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
jedno	jeden	k4xCgNnSc4	jeden
slovo	slovo	k1gNnSc4	slovo
<g/>
)	)	kIx)	)
označovat	označovat	k5eAaImF	označovat
jiný	jiný	k2eAgInSc4d1	jiný
počet	počet	k1gInSc4	počet
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
standardech	standard	k1gInPc6	standard
používá	používat	k5eAaImIp3nS	používat
místo	místo	k1gNnSc1	místo
termínu	termín	k1gInSc2	termín
bajt	bajt	k1gInSc1	bajt
termín	termín	k1gInSc1	termín
oktet	oktet	k1gInSc4	oktet
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
síťové	síťový	k2eAgInPc1d1	síťový
protokoly	protokol	k1gInPc1	protokol
nebo	nebo	k8xC	nebo
formáty	formát	k1gInPc1	formát
souborů	soubor	k1gInPc2	soubor
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnSc2d1	různá
architektury	architektura	k1gFnSc2	architektura
počítačů	počítač	k1gMnPc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
bajt	bajt	k1gInSc1	bajt
kodifikovaný	kodifikovaný	k2eAgInSc1d1	kodifikovaný
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
standardu	standard	k1gInSc6	standard
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
80000-13	[number]	k4	80000-13
<g/>
:	:	kIx,	:
<g/>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
byte	byte	k1gInSc1	byte
<g/>
"	"	kIx"	"
zavedl	zavést	k5eAaPmAgMnS	zavést
Werner	Werner	k1gMnSc1	Werner
Buchholz	Buchholz	k1gMnSc1	Buchholz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
IBM	IBM	kA	IBM
Stretch	Stretch	k1gInSc1	Stretch
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
popisoval	popisovat	k5eAaImAgInS	popisovat
skupinu	skupina	k1gFnSc4	skupina
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
osmibitový	osmibitový	k2eAgInSc4d1	osmibitový
bajt	bajt	k1gInSc4	bajt
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgMnS	udát
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
osmibitový	osmibitový	k2eAgInSc1d1	osmibitový
bajt	bajt	k1gInSc1	bajt
stal	stát	k5eAaPmAgInS	stát
standardem	standard	k1gInSc7	standard
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
System	Syst	k1gInSc7	Syst
<g/>
/	/	kIx~	/
<g/>
360	[number]	k4	360
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
pak	pak	k6eAd1	pak
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
osmibitový	osmibitový	k2eAgInSc1d1	osmibitový
bajt	bajt	k1gInSc1	bajt
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
byte	byte	k1gInSc1	byte
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
bite	bit	k1gInSc5	bit
(	(	kIx(	(
<g/>
sousto	sousto	k1gNnSc1	sousto
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nejmenší	malý	k2eAgInSc1d3	nejmenší
objem	objem	k1gInSc1	objem
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
počítač	počítač	k1gInSc1	počítač
dokáže	dokázat	k5eAaPmIp3nS	dokázat
"	"	kIx"	"
<g/>
přechroustat	přechroustat	k5eAaPmF	přechroustat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
záměně	záměna	k1gFnSc3	záměna
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
bit	bit	k1gInSc1	bit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgInPc2	všecek
dnešních	dnešní	k2eAgInPc2d1	dnešní
počítačů	počítač	k1gInPc2	počítač
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1	[number]	k4	1
bajt	bajt	k1gInSc1	bajt
právě	právě	k6eAd1	právě
8	[number]	k4	8
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
strojový	strojový	k2eAgInSc4d1	strojový
kód	kód	k1gInSc4	kód
s	s	k7c7	s
osmi	osm	k4xCc7	osm
číslicemi	číslice	k1gFnPc7	číslice
1111 1111	[number]	k4	1111 1111
<g/>
)	)	kIx)	)
–	–	k?	–
plný	plný	k2eAgInSc4d1	plný
bajt	bajt	k1gInSc4	bajt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
architekturách	architektura	k1gFnPc6	architektura
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
bajty	bajt	k1gInPc1	bajt
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
např.	např.	kA	např.
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
či	či	k8xC	či
9	[number]	k4	9
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
počítač	počítač	k1gInSc1	počítač
PDP-10	PDP-10	k1gFnSc2	PDP-10
měl	mít	k5eAaImAgInS	mít
dokonce	dokonce	k9	dokonce
nastavitelnou	nastavitelný	k2eAgFnSc4d1	nastavitelná
délku	délka	k1gFnSc4	délka
bajtu	bajt	k1gInSc2	bajt
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jednoznačnosti	jednoznačnost	k1gFnSc2	jednoznačnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
formálních	formální	k2eAgFnPc6d1	formální
specifikacích	specifikace	k1gFnPc6	specifikace
pro	pro	k7c4	pro
posloupnost	posloupnost	k1gFnSc4	posloupnost
právě	právě	k6eAd1	právě
osmi	osm	k4xCc2	osm
bitů	bit	k1gInPc2	bit
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
oktet	oktet	k1gInSc1	oktet
(	(	kIx(	(
<g/>
angl	angl	k1gInSc1	angl
<g/>
.	.	kIx.	.
octet	octet	k1gInSc1	octet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oktetem	oktet	k1gInSc7	oktet
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
termínu	termín	k1gInSc2	termín
bajt	bajt	k1gInSc1	bajt
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
ve	v	k7c6	v
frankofonních	frankofonní	k2eAgFnPc6d1	frankofonní
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bajtu	bajt	k1gInSc2	bajt
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
uložit	uložit	k5eAaPmF	uložit
celkem	celkem	k6eAd1	celkem
2n	[number]	k4	2n
různých	různý	k2eAgFnPc2d1	různá
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
kde	kde	k9	kde
n	n	k0	n
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc4	velikost
bajtu	bajt	k1gInSc2	bajt
v	v	k7c6	v
bitech	bit	k1gInPc6	bit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
osmibitový	osmibitový	k2eAgInSc4d1	osmibitový
bajt	bajt	k1gInSc4	bajt
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
256	[number]	k4	256
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
např.	např.	kA	např.
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
255	[number]	k4	255
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
hodnoty	hodnota	k1gFnPc4	hodnota
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
hexadecimálních	hexadecimální	k2eAgFnPc2d1	hexadecimální
číslic	číslice	k1gFnPc2	číslice
(	(	kIx(	(
<g/>
00H	[number]	k4	00H
<g/>
–	–	k?	–
<g/>
FFH	FFH	kA	FFH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reprezentace	reprezentace	k1gFnSc1	reprezentace
znaků	znak	k1gInPc2	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
bajtu	bajt	k1gInSc2	bajt
lze	lze	k6eAd1	lze
uložit	uložit	k5eAaPmF	uložit
256	[number]	k4	256
různých	různý	k2eAgFnPc2d1	různá
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bohatě	bohatě	k6eAd1	bohatě
stačí	stačit	k5eAaBmIp3nS	stačit
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
velkých	velká	k1gFnPc2	velká
i	i	k8xC	i
malých	malý	k2eAgNnPc2d1	malé
písmen	písmeno	k1gNnPc2	písmeno
anglické	anglický	k2eAgFnSc2d1	anglická
abecedy	abeceda	k1gFnSc2	abeceda
včetně	včetně	k7c2	včetně
číslic	číslice	k1gFnPc2	číslice
a	a	k8xC	a
základních	základní	k2eAgNnPc2d1	základní
interpunkčních	interpunkční	k2eAgNnPc2d1	interpunkční
znamének	znaménko	k1gNnPc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Bajt	bajt	k1gInSc1	bajt
proto	proto	k8xC	proto
zpočátku	zpočátku	k6eAd1	zpočátku
mohl	moct	k5eAaImAgInS	moct
sloužit	sloužit	k5eAaImF	sloužit
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
jednoho	jeden	k4xCgInSc2	jeden
znaku	znak	k1gInSc2	znak
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
kódování	kódování	k1gNnSc2	kódování
znaku	znak	k1gInSc2	znak
na	na	k7c4	na
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
uložit	uložit	k5eAaPmF	uložit
do	do	k7c2	do
bajtu	bajt	k1gInSc2	bajt
<g/>
,	,	kIx,	,
popisoval	popisovat	k5eAaImAgMnS	popisovat
například	například	k6eAd1	například
kód	kód	k1gInSc4	kód
ASCII	ascii	kA	ascii
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kód	kód	k1gInSc1	kód
však	však	k9	však
vůbec	vůbec	k9	vůbec
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
znaky	znak	k1gInPc4	znak
s	s	k7c7	s
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
verze	verze	k1gFnPc1	verze
kódu	kód	k1gInSc2	kód
pak	pak	k6eAd1	pak
přidávají	přidávat	k5eAaImIp3nP	přidávat
různé	různý	k2eAgInPc1d1	různý
znaky	znak	k1gInPc1	znak
užívané	užívaný	k2eAgInPc1d1	užívaný
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
takových	takový	k3xDgNnPc2	takový
kódování	kódování	k1gNnPc2	kódování
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xC	jako
znakové	znakový	k2eAgFnSc2d1	znaková
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgMnPc4	všechen
byly	být	k5eAaImAgFnP	být
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
vzájemně	vzájemně	k6eAd1	vzájemně
nekompatibilní	kompatibilní	k2eNgFnSc1d1	nekompatibilní
<g/>
.	.	kIx.	.
256	[number]	k4	256
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
příliš	příliš	k6eAd1	příliš
málo	málo	k6eAd1	málo
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
všech	všecek	k3xTgInPc2	všecek
znaků	znak	k1gInPc2	znak
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
píšících	píšící	k2eAgFnPc2d1	píšící
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
nemluvě	nemluva	k1gFnSc3	nemluva
o	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
abecedách	abeceda	k1gFnPc6	abeceda
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
japonština	japonština	k1gFnSc1	japonština
či	či	k8xC	či
korejština	korejština	k1gFnSc1	korejština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vícebajtová	vícebajtová	k1gFnSc1	vícebajtová
kódování	kódování	k1gNnSc2	kódování
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
však	však	k9	však
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
už	už	k6eAd1	už
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
jednomu	jeden	k4xCgInSc3	jeden
bajtu	bajt	k1gInSc3	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
používaná	používaný	k2eAgFnSc1d1	používaná
tabulka	tabulka	k1gFnSc1	tabulka
znaků	znak	k1gInPc2	znak
Unicode	Unicod	k1gInSc5	Unicod
používá	používat	k5eAaImIp3nS	používat
nejčastěji	často	k6eAd3	často
dvou	dva	k4xCgMnPc2	dva
<g/>
-	-	kIx~	-
či	či	k8xC	či
čtyřbajtová	čtyřbajtový	k2eAgNnPc4d1	čtyřbajtový
kódování	kódování	k1gNnPc4	kódování
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
délkou	délka	k1gFnSc7	délka
(	(	kIx(	(
<g/>
UTF-32	UTF-32	k1gFnSc7	UTF-32
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
UCS-2	UCS-2	k1gFnSc1	UCS-2
<g/>
)	)	kIx)	)
či	či	k8xC	či
kódování	kódování	k1gNnSc1	kódování
s	s	k7c7	s
proměnlivou	proměnlivý	k2eAgFnSc7d1	proměnlivá
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc6	který
různé	různý	k2eAgInPc1d1	různý
znaky	znak	k1gInPc1	znak
zabírají	zabírat	k5eAaImIp3nP	zabírat
různé	různý	k2eAgInPc1d1	různý
počty	počet	k1gInPc1	počet
bajtů	bajt	k1gInPc2	bajt
(	(	kIx(	(
<g/>
UTF-8	UTF-8	k1gMnSc1	UTF-8
<g/>
,	,	kIx,	,
UTF-16	UTF-16	k1gMnSc1	UTF-16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
bajt	bajt	k1gInSc1	bajt
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
jednotka	jednotka	k1gFnSc1	jednotka
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
B	B	kA	B
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
malým	malé	k1gNnSc7	malé
b	b	k?	b
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
bit	bit	k1gInSc1	bit
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
definuje	definovat	k5eAaBmIp3nS	definovat
značky	značka	k1gFnPc4	značka
také	také	k9	také
norma	norma	k1gFnSc1	norma
IEEE	IEEE	kA	IEEE
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
B	B	kA	B
se	se	k3xPyFc4	se
však	však	k9	však
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
pro	pro	k7c4	pro
jednotku	jednotka	k1gFnSc4	jednotka
hladinu	hladina	k1gFnSc4	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
bel	bela	k1gFnPc2	bela
(	(	kIx(	(
<g/>
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
značky	značka	k1gFnPc1	značka
začínající	začínající	k2eAgFnSc2d1	začínající
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
vyhrazeny	vyhradit	k5eAaPmNgFnP	vyhradit
pro	pro	k7c4	pro
jednotky	jednotka	k1gFnPc4	jednotka
odvozené	odvozený	k2eAgFnPc4d1	odvozená
ze	z	k7c2	z
jmen	jméno	k1gNnPc2	jméno
osob	osoba	k1gFnPc2	osoba
<g/>
;	;	kIx,	;
bajt	bajt	k1gInSc1	bajt
však	však	k9	však
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
soustavy	soustava	k1gFnSc2	soustava
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
co	co	k3yRnSc4	co
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
jednoznačnost	jednoznačnost	k1gFnSc1	jednoznačnost
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
bit	bit	k1gInSc1	bit
někdy	někdy	k6eAd1	někdy
nezkracuje	zkracovat	k5eNaImIp3nS	zkracovat
a	a	k8xC	a
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
bit	bit	k1gInSc1	bit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mbit	Mbit	k1gInSc1	Mbit
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
pro	pro	k7c4	pro
megabit	megabit	k5eAaImF	megabit
za	za	k7c2	za
sekundu	sekund	k1gInSc2	sekund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
také	také	k9	také
norma	norma	k1gFnSc1	norma
IEC	IEC	kA	IEC
60027.	[number]	k4	60027.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
frankofonních	frankofonní	k2eAgFnPc6d1	frankofonní
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
bajt	bajt	k1gInSc4	bajt
používá	používat	k5eAaImIp3nS	používat
značka	značka	k1gFnSc1	značka
o	o	k7c4	o
jako	jako	k9	jako
octet	octet	k5eAaImF	octet
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
příslušných	příslušný	k2eAgFnPc2d1	příslušná
předpon	předpona	k1gFnPc2	předpona
pro	pro	k7c4	pro
násobky	násobek	k1gInPc4	násobek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
ko	ko	k?	ko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mo	Mo	k1gFnSc1	Mo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Násobky	násobek	k1gInPc1	násobek
a	a	k8xC	a
užívané	užívaný	k2eAgFnPc1d1	užívaná
předpony	předpona	k1gFnPc1	předpona
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
bajty	bajt	k1gInPc7	bajt
i	i	k8xC	i
bity	bit	k1gInPc1	bit
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
běžné	běžný	k2eAgFnPc1d1	běžná
předpony	předpona	k1gFnPc1	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
jako	jako	k9	jako
kilo	kilo	k1gNnSc4	kilo
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
mega	mega	k1gNnSc1	mega
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
giga	giga	k1gFnSc1	giga
<g/>
-	-	kIx~	-
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
např.	např.	kA	např.
10	[number]	k4	10
GB	GB	kA	GB
<g/>
,	,	kIx,	,
11	[number]	k4	11
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
.	.	kIx.	.
Tyto	tento	k3xDgFnPc1	tento
předpony	předpona	k1gFnPc1	předpona
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
někdy	někdy	k6eAd1	někdy
odlišný	odlišný	k2eAgInSc4d1	odlišný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
rozlišit	rozlišit	k5eAaPmF	rozlišit
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
</p>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
megabajtu	megabajt	k1gInSc6	megabajt
je	být	k5eAaImIp3nS	být
100000	[number]	k4	100000
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<p>
<s>
Z	z	k7c2	z
technologických	technologický	k2eAgInPc2d1	technologický
důvodů	důvod	k1gInPc2	důvod
jsou	být	k5eAaImIp3nP	být
velikosti	velikost	k1gFnPc4	velikost
některých	některý	k3yIgFnPc2	některý
počítačových	počítačový	k2eAgFnPc2d1	počítačová
pamětí	paměť	k1gFnPc2	paměť
obvykle	obvykle	k6eAd1	obvykle
násobkem	násobek	k1gInSc7	násobek
nějaké	nějaký	k3yIgFnSc2	nějaký
mocniny	mocnina	k1gFnSc2	mocnina
dvou	dva	k4xCgMnPc6	dva
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
počítač	počítač	k1gInSc1	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
Model	model	k1gInSc4	model
5150	[number]	k4	5150
měl	mít	k5eAaImAgMnS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
65 536	[number]	k4	65 536
B	B	kA	B
=	=	kIx~	=
64	[number]	k4	64
<g/>
·	·	k?	·
<g/>
210	[number]	k4	210
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
se	se	k3xPyFc4	se
však	však	k9	však
taková	takový	k3xDgFnSc1	takový
paměť	paměť	k1gFnSc1	paměť
neoznačovala	označovat	k5eNaImAgFnS	označovat
jako	jako	k9	jako
65,5	[number]	k4	65,5
kilobajt	kilobajt	k1gInSc1	kilobajt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojem	pojem	k1gInSc1	pojem
kilobajt	kilobajt	k1gInSc1	kilobajt
se	s	k7c7	s
"	"	kIx"	"
<g/>
mírně	mírně	k6eAd1	mírně
<g/>
"	"	kIx"	"
upravil	upravit	k5eAaPmAgMnS	upravit
na	na	k7c4	na
210	[number]	k4	210
=	=	kIx~	=
1024	[number]	k4	1024
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
paměti	paměť	k1gFnSc2	paměť
se	se	k3xPyFc4	se
označila	označit	k5eAaPmAgFnS	označit
prostě	prostě	k6eAd1	prostě
jako	jako	k8xS	jako
64	[number]	k4	64
KB	kb	kA	kb
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
zpravidla	zpravidla	k6eAd1	zpravidla
značila	značit	k5eAaImAgFnS	značit
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
–	–	k?	–
KB	kb	kA	kb
–	–	k?	–
a	a	k8xC	a
neformálně	formálně	k6eNd1	formálně
se	se	k3xPyFc4	se
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
velké	velký	k2eAgNnSc1d1	velké
kilo	kilo	k1gNnSc1	kilo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tradičně	tradičně	k6eAd1	tradičně
pojatého	pojatý	k2eAgInSc2d1	pojatý
kilobajtu	kilobajt	k1gInSc2	kilobajt
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
malého	malý	k2eAgNnSc2d1	malé
kila	kilo	k1gNnSc2	kilo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
značilo	značit	k5eAaImAgNnS	značit
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
–	–	k?	–
kB	kb	kA	kb
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
užívat	užívat	k5eAaImF	užívat
další	další	k2eAgFnPc1d1	další
upravené	upravený	k2eAgFnPc1d1	upravená
předpony	předpona	k1gFnPc1	předpona
mega	mega	k1gNnSc2	mega
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
giga	giga	k1gFnSc1	giga
<g/>
-	-	kIx~	-
atd.	atd.	kA	atd.
(	(	kIx(	(
<g/>
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
už	už	k6eAd1	už
kontext	kontext	k1gInSc4	kontext
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
písmena	písmeno	k1gNnSc2	písmeno
rozlišit	rozlišit	k5eAaPmF	rozlišit
nelze	lze	k6eNd1	lze
<g/>
)	)	kIx)	)
označující	označující	k2eAgFnSc1d1	označující
220	[number]	k4	220
<g/>
,	,	kIx,	,
230	[number]	k4	230
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Takto	takto	k6eAd1	takto
upravené	upravený	k2eAgFnPc1d1	upravená
předpony	předpona	k1gFnPc1	předpona
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zejména	zejména	k9	zejména
při	při	k7c6	při
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
velikosti	velikost	k1gFnSc2	velikost
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
pamětí	paměť	k1gFnPc2	paměť
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
kapacity	kapacita	k1gFnPc1	kapacita
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
používají	používat	k5eAaImIp3nP	používat
dekadické	dekadický	k2eAgFnPc1d1	dekadická
předpony	předpona	k1gFnPc1	předpona
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
znalosti	znalost	k1gFnSc2	znalost
kontextu	kontext	k1gInSc2	kontext
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
složité	složitý	k2eAgInPc4d1	složitý
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlet	k5eAaImNgInS	zamýšlet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozdíl	rozdíl	k1gInSc1	rozdíl
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
několika	několik	k4yIc2	několik
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
nejasnosti	nejasnost	k1gFnPc1	nejasnost
kolem	kolem	k7c2	kolem
skutečné	skutečný	k2eAgFnSc2d1	skutečná
kapacity	kapacita	k1gFnSc2	kapacita
prodávaných	prodávaný	k2eAgFnPc2d1	prodávaná
pamětí	paměť	k1gFnPc2	paměť
dokonce	dokonce	k9	dokonce
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
právním	právní	k2eAgInPc3d1	právní
sporům	spor	k1gInPc3	spor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
proto	proto	k8xC	proto
IEC	IEC	kA	IEC
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
dodatek	dodatek	k1gInSc4	dodatek
k	k	k7c3	k
normě	norma	k1gFnSc3	norma
IEC	IEC	kA	IEC
60027-2	[number]	k4	60027-2
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
převzatá	převzatý	k2eAgFnSc1d1	převzatá
jako	jako	k8xC	jako
ČSN	ČSN	kA	ČSN
IEC	IEC	kA	IEC
60027-2	[number]	k4	60027-2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zavedla	zavést	k5eAaPmAgFnS	zavést
pro	pro	k7c4	pro
počítačové	počítačový	k2eAgFnPc4d1	počítačová
jednotky	jednotka	k1gFnPc4	jednotka
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
označování	označování	k1gNnSc2	označování
násobků	násobek	k1gInPc2	násobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
původní	původní	k2eAgNnSc4d1	původní
"	"	kIx"	"
<g/>
velké	velký	k2eAgNnSc4d1	velké
kilo	kilo	k1gNnSc4	kilo
<g/>
"	"	kIx"	"
=	=	kIx~	=
1024	[number]	k4	1024
B	B	kA	B
navrženo	navržen	k2eAgNnSc1d1	navrženo
označení	označení	k1gNnSc1	označení
kibibajt	kibibajta	k1gFnPc2	kibibajta
a	a	k8xC	a
značka	značka	k1gFnSc1	značka
KiB	KiB	k1gFnSc1	KiB
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jednotka	jednotka	k1gFnSc1	jednotka
kilobajt	kilobajt	k1gInSc1	kilobajt
(	(	kIx(	(
<g/>
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
kB	kb	kA	kb
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
1000	[number]	k4	1000
B	B	kA	B
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
binární	binární	k2eAgFnPc1d1	binární
předpony	předpona	k1gFnPc1	předpona
(	(	kIx(	(
<g/>
kibi	kib	k1gFnPc1	kib
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
mebi	mebi	k1gNnSc1	mebi
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
gibi	gibi	k1gNnSc1	gibi
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
definované	definovaný	k2eAgInPc1d1	definovaný
také	také	k9	také
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
80000.	[number]	k4	80000.
</s>
</p>
<p>
</p>
<p>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
převod	převod	k1gInSc1	převod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
<g/>
-li	i	k?	-li
převést	převést	k5eAaPmF	převést
KiB	KiB	k1gFnSc1	KiB
na	na	k7c6	na
kB	kb	kA	kb
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
počet	počet	k1gInSc4	počet
KiB	KiB	k1gFnSc2	KiB
vynásobit	vynásobit	k5eAaPmF	vynásobit
podílem	podíl	k1gInSc7	podíl
1000/1024	[number]	k4	1000/1024
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obráceného	obrácený	k2eAgInSc2d1	obrácený
převodu	převod	k1gInSc2	převod
(	(	kIx(	(
<g/>
kB	kb	kA	kb
→	→	k?	→
KiB	KiB	k1gFnSc1	KiB
<g/>
)	)	kIx)	)
násobíte	násobit	k5eAaImIp2nP	násobit
převrácenou	převrácený	k2eAgFnSc7d1	převrácená
hodnotou	hodnota	k1gFnSc7	hodnota
(	(	kIx(	(
<g/>
1024	[number]	k4	1024
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
převodů	převod	k1gInPc2	převod
mezi	mezi	k7c7	mezi
MB	MB	kA	MB
a	a	k8xC	a
MiB	MiB	k1gFnSc1	MiB
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
čísla	číslo	k1gNnPc4	číslo
10002	[number]	k4	10002
a	a	k8xC	a
10242	[number]	k4	10242
<g/>
,	,	kIx,	,
u	u	k7c2	u
GB	GB	kA	GB
a	a	k8xC	a
GiB	GiB	k1gFnSc1	GiB
10003	[number]	k4	10003
a	a	k8xC	a
10243	[number]	k4	10243
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
atd.	atd.	kA	atd.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bit	bit	k1gInSc1	bit
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
</s>
</p>
<p>
<s>
Nibble	Nibble	k6eAd1	Nibble
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bajt	bajt	k1gInSc1	bajt
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Byte	byte	k1gInSc1	byte
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
hackerského	hackerský	k2eAgInSc2d1	hackerský
žargonu	žargon	k1gInSc2	žargon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Bit	bit	k1gInSc1	bit
a	a	k8xC	a
byte	byte	k1gInSc1	byte
<g/>
/	/	kIx~	/
<g/>
pro	pro	k7c4	pro
SŠ	SŠ	kA	SŠ
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
