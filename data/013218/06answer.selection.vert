<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
kamzíci	kamzík	k1gMnPc1	kamzík
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
divokou	divoký	k2eAgFnSc7d1	divoká
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neopomíjejí	opomíjet	k5eNaImIp3nP	opomíjet
ani	ani	k8xC	ani
bobovité	bobovitý	k2eAgFnPc1d1	bobovitá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
alpský	alpský	k2eAgInSc1d1	alpský
jetel	jetel	k1gInSc1	jetel
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
dávají	dávat	k5eAaImIp3nP	dávat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.	.
</s>
