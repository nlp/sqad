<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Rupicapra	Rupicapra	k1gMnSc1	Rupicapra
rupicapra	rupicapra	k1gMnSc1	rupicapra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
divoké	divoký	k2eAgFnSc2d1	divoká
horské	horský	k2eAgFnSc2d1	horská
kozy	koza	k1gFnSc2	koza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
biotop	biotop	k1gInSc1	biotop
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
původním	původní	k2eAgInSc7d1	původní
domovem	domov	k1gInSc7	domov
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
vysokohorské	vysokohorský	k2eAgFnPc1d1	vysokohorská
oblasti	oblast	k1gFnPc1	oblast
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
chráněn	chránit	k5eAaImNgInS	chránit
v	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
Ortles	Ortlesa	k1gFnPc2	Ortlesa
-	-	kIx~	-
3000	[number]	k4	3000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
Grajských	Grajský	k2eAgFnPc6d1	Grajský
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
a	a	k8xC	a
Lužických	lužický	k2eAgFnPc6d1	Lužická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
uměle	uměle	k6eAd1	uměle
vysazen	vysadit	k5eAaPmNgInS	vysadit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
záznamy	záznam	k1gInPc1	záznam
výskytu	výskyt	k1gInSc2	výskyt
kamzíka	kamzík	k1gMnSc2	kamzík
na	na	k7c6	na
Králickém	králický	k2eAgInSc6d1	králický
Sněžníku	Sněžník	k1gInSc6	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Kamzičí	kamzičí	k2eAgFnPc1d1	kamzičí
populace	populace	k1gFnPc1	populace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
nejzdravější	zdravý	k2eAgMnSc1d3	nejzdravější
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kamzíků	kamzík	k1gMnPc2	kamzík
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgFnSc1d1	častá
prašivina	prašivina	k1gFnSc1	prašivina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
se	se	k3xPyFc4	se
často	často	k6eAd1	často
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
skalní	skalní	k2eAgFnSc2d1	skalní
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
obliba	obliba	k1gFnSc1	obliba
pro	pro	k7c4	pro
pastviště	pastviště	k1gNnSc4	pastviště
ležící	ležící	k2eAgFnSc2d1	ležící
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
výškách	výška	k1gFnPc6	výška
než	než	k8xS	než
2000	[number]	k4	2000
m.	m.	k?	m.
Toto	tento	k3xDgNnSc1	tento
však	však	k9	však
vždy	vždy	k6eAd1	vždy
neplatí	platit	k5eNaImIp3nS	platit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pravidelně	pravidelně	k6eAd1	pravidelně
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
lidé	člověk	k1gMnPc1	člověk
spatří	spatřit	k5eAaPmIp3nP	spatřit
obtížněji	obtížně	k6eAd2	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
mu	on	k3xPp3gMnSc3	on
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
nutnou	nutný	k2eAgFnSc4d1	nutná
potravu	potrava	k1gFnSc4	potrava
ani	ani	k8xC	ani
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
kvůli	kvůli	k7c3	kvůli
většímu	veliký	k2eAgNnSc3d2	veliký
ohrožení	ohrožení	k1gNnSc3	ohrožení
v	v	k7c6	v
nekrytém	krytý	k2eNgInSc6d1	nekrytý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
kamzíkům	kamzík	k1gMnPc3	kamzík
v	v	k7c6	v
lese	les	k1gInSc6	les
daří	dařit	k5eAaImIp3nS	dařit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
lesích	lese	k1gFnPc6	lese
vyšších	vysoký	k2eAgFnPc2d2	vyšší
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Detekce	detekce	k1gFnSc1	detekce
výskytišť	výskytiště	k1gNnPc2	výskytiště
kamzíků	kamzík	k1gMnPc2	kamzík
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
nad	nad	k7c4	nad
2500	[number]	k4	2500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
přítomnost	přítomnost	k1gFnSc4	přítomnost
této	tento	k3xDgFnSc2	tento
zvěře	zvěř	k1gFnSc2	zvěř
ještě	ještě	k9	ještě
nad	nad	k7c4	nad
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
příčinou	příčina	k1gFnSc7	příčina
těchto	tento	k3xDgInPc2	tento
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
případů	případ	k1gInPc2	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
časté	častý	k2eAgNnSc1d1	časté
rušení	rušení	k1gNnSc1	rušení
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
letní	letní	k2eAgNnPc4d1	letní
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
je	být	k5eAaImIp3nS	být
elegantní	elegantní	k2eAgFnSc1d1	elegantní
a	a	k8xC	a
silná	silný	k2eAgFnSc1d1	silná
koza	koza	k1gFnSc1	koza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
činí	činit	k5eAaImIp3nS	činit
0,70	[number]	k4	0,70
-	-	kIx~	-
0,85	[number]	k4	0,85
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
1	[number]	k4	1
-	-	kIx~	-
1,30	[number]	k4	1,30
m	m	kA	m
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
11	[number]	k4	11
-	-	kIx~	-
36	[number]	k4	36
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
měří	měřit	k5eAaImIp3nS	měřit
3	[number]	k4	3
-	-	kIx~	-
8	[number]	k4	8
cm	cm	kA	cm
a	a	k8xC	a
lebka	lebka	k1gFnSc1	lebka
18,3	[number]	k4	18,3
-	-	kIx~	-
22	[number]	k4	22
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
příkrém	příkrý	k2eAgInSc6d1	příkrý
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
masivní	masivní	k2eAgFnSc1d1	masivní
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
velmi	velmi	k6eAd1	velmi
tmavě	tmavě	k6eAd1	tmavě
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
má	mít	k5eAaImIp3nS	mít
bílé	bílý	k2eAgFnPc4d1	bílá
skvrny	skvrna	k1gFnPc4	skvrna
a	a	k8xC	a
dolní	dolní	k2eAgFnPc4d1	dolní
části	část	k1gFnPc4	část
tváří	tvář	k1gFnPc2	tvář
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
má	mít	k5eAaImIp3nS	mít
srst	srst	k1gFnSc1	srst
výrazně	výrazně	k6eAd1	výrazně
světlejší	světlý	k2eAgFnSc1d2	světlejší
<g/>
,	,	kIx,	,
jedině	jedině	k6eAd1	jedině
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
tváří	tvář	k1gFnPc2	tvář
<g/>
,	,	kIx,	,
hřbetní	hřbetní	k2eAgInSc1d1	hřbetní
podélný	podélný	k2eAgInSc1d1	podélný
pruh	pruh	k1gInSc1	pruh
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
končetin	končetina	k1gFnPc2	končetina
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
hodně	hodně	k6eAd1	hodně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Hrdý	hrdý	k2eAgInSc4d1	hrdý
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
hákovité	hákovitý	k2eAgInPc4d1	hákovitý
růžky	růžek	k1gInPc4	růžek
<g/>
,	,	kIx,	,
zdobící	zdobící	k2eAgFnSc4d1	zdobící
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInPc4	první
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
kamzíka	kamzík	k1gMnSc4	kamzík
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
větší	veliký	k2eAgInPc4d2	veliký
rozměry	rozměr	k1gInPc4	rozměr
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
stejného	stejný	k2eAgInSc2d1	stejný
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
mění	měnit	k5eAaImIp3nS	měnit
paroží	paroží	k1gNnSc1	paroží
<g/>
,	,	kIx,	,
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
kamzík	kamzík	k1gMnSc1	kamzík
růžky	růžek	k1gInPc4	růžek
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
kamzíci	kamzík	k1gMnPc1	kamzík
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
divokou	divoký	k2eAgFnSc7d1	divoká
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neopomíjejí	opomíjet	k5eNaImIp3nP	opomíjet
ani	ani	k8xC	ani
bobovité	bobovitý	k2eAgFnPc1d1	bobovitá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
alpský	alpský	k2eAgInSc1d1	alpský
jetel	jetel	k1gInSc1	jetel
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
dávají	dávat	k5eAaImIp3nP	dávat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čelí	čelit	k5eAaImIp3nS	čelit
omezením	omezení	k1gNnSc7	omezení
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
a	a	k8xC	a
pokud	pokud	k8xS	pokud
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uspokojí	uspokojit	k5eAaPmIp3nP	uspokojit
se	s	k7c7	s
suchými	suchý	k2eAgFnPc7d1	suchá
bylinami	bylina	k1gFnPc7	bylina
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
pak	pak	k6eAd1	pak
k	k	k7c3	k
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
vystačí	vystačit	k5eAaBmIp3nS	vystačit
s	s	k7c7	s
větévkami	větévka	k1gFnPc7	větévka
listnatých	listnatý	k2eAgFnPc2d1	listnatá
dřevin	dřevina	k1gFnPc2	dřevina
(	(	kIx(	(
<g/>
jeřáby	jeřáb	k1gInPc1	jeřáb
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc2	olše
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc1	lišejník
a	a	k8xC	a
i	i	k9	i
letorosty	letorost	k1gInPc1	letorost
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Kamzíci	kamzík	k1gMnPc1	kamzík
jsou	být	k5eAaImIp3nP	být
pospolitá	pospolitý	k2eAgFnSc1d1	pospolitá
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
roku	rok	k1gInSc2	rok
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
samci	samec	k1gMnPc1	samec
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedeny	veden	k2eAgInPc1d1	veden
jsou	být	k5eAaImIp3nP	být
zásadně	zásadně	k6eAd1	zásadně
starší	starý	k2eAgFnSc7d2	starší
samicí	samice	k1gFnSc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
periodách	perioda	k1gFnPc6	perioda
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
zcela	zcela	k6eAd1	zcela
zřejmou	zřejmý	k2eAgFnSc4d1	zřejmá
separaci	separace	k1gFnSc4	separace
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
stáda	stádo	k1gNnPc1	stádo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
výlučně	výlučně	k6eAd1	výlučně
kamzice	kamzice	k1gFnPc4	kamzice
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
skupiny	skupina	k1gFnPc1	skupina
složeny	složit	k5eAaPmNgFnP	složit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
kozlů	kozel	k1gMnPc2	kozel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
vznikají	vznikat	k5eAaImIp3nP	vznikat
velká	velký	k2eAgNnPc1d1	velké
shromáždění	shromáždění	k1gNnSc2	shromáždění
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
znak	znak	k1gInSc1	znak
blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
říje	říje	k1gFnSc2	říje
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tíhnou	tíhnout	k5eAaImIp3nP	tíhnout
k	k	k7c3	k
polygamii	polygamie	k1gFnSc3	polygamie
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
provokují	provokovat	k5eAaImIp3nP	provokovat
v	v	k7c6	v
duelech	duel	k1gInPc6	duel
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
zaujmout	zaujmout	k5eAaPmF	zaujmout
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
prudké	prudký	k2eAgInPc1d1	prudký
<g/>
,	,	kIx,	,
zvýrazněné	zvýrazněný	k2eAgInPc1d1	zvýrazněný
akrobatickými	akrobatický	k2eAgInPc7d1	akrobatický
výkony	výkon	k1gInPc7	výkon
kozlů	kozel	k1gMnPc2	kozel
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
pronásledováním	pronásledování	k1gNnSc7	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Souboje	souboj	k1gInPc4	souboj
většinou	většinou	k6eAd1	většinou
končí	končit	k5eAaImIp3nS	končit
určením	určení	k1gNnSc7	určení
dominantního	dominantní	k2eAgInSc2d1	dominantní
samce	samec	k1gInSc2	samec
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
)	)	kIx)	)
a	a	k8xC	a
poraženého	poražený	k2eAgMnSc2d1	poražený
soupeře	soupeř	k1gMnSc2	soupeř
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
odtáhne	odtáhnout	k5eAaPmIp3nS	odtáhnout
pryč	pryč	k6eAd1	pryč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
na	na	k7c6	na
citlivém	citlivý	k2eAgNnSc6d1	citlivé
místě	místo	k1gNnSc6	místo
těla	tělo	k1gNnSc2	tělo
může	moct	k5eAaImIp3nS	moct
přivodit	přivodit	k5eAaBmF	přivodit
smrt	smrt	k1gFnSc4	smrt
účastníka	účastník	k1gMnSc2	účastník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
říje	říje	k1gFnSc2	říje
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
zvěř	zvěř	k1gFnSc1	zvěř
se	se	k3xPyFc4	se
odebírá	odebírat	k5eAaImIp3nS	odebírat
do	do	k7c2	do
zimních	zimní	k2eAgNnPc2d1	zimní
stanovišť	stanoviště	k1gNnPc2	stanoviště
a	a	k8xC	a
kamzíci	kamzík	k1gMnPc1	kamzík
zcela	zcela	k6eAd1	zcela
ztratí	ztratit	k5eAaPmIp3nP	ztratit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
160	[number]	k4	160
-	-	kIx~	-
170	[number]	k4	170
dní	den	k1gInPc2	den
a	a	k8xC	a
20	[number]	k4	20
-	-	kIx~	-
30	[number]	k4	30
dní	den	k1gInPc2	den
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
oddělí	oddělit	k5eAaPmIp3nS	oddělit
od	od	k7c2	od
stáda	stádo	k1gNnSc2	stádo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
porodu	porod	k1gInSc3	porod
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
mládě	mládě	k1gNnSc1	mládě
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
všude	všude	k6eAd1	všude
následuje	následovat	k5eAaImIp3nS	následovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
kojení	kojení	k1gNnSc1	kojení
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
kůzle	kůzle	k1gNnSc1	kůzle
samo	sám	k3xTgNnSc1	sám
spásá	spásat	k5eAaPmIp3nS	spásat
jemnou	jemný	k2eAgFnSc4d1	jemná
vegetaci	vegetace	k1gFnSc4	vegetace
už	už	k9	už
30	[number]	k4	30
dní	den	k1gInPc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
kamzice	kamzice	k1gFnSc2	kamzice
rodí	rodit	k5eAaImIp3nS	rodit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lov	lov	k1gInSc1	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Lov	lov	k1gInSc1	lov
kamzíků	kamzík	k1gMnPc2	kamzík
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
ceněn	cenit	k5eAaImNgInS	cenit
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc1	důkaz
mužnosti	mužnost	k1gFnSc2	mužnost
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
obtížném	obtížný	k2eAgInSc6d1	obtížný
horském	horský	k2eAgInSc6d1	horský
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
často	často	k6eAd1	často
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
provádět	provádět	k5eAaImF	provádět
přímo	přímo	k6eAd1	přímo
horolezecké	horolezecký	k2eAgInPc4d1	horolezecký
výkony	výkon	k1gInPc4	výkon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
ulovenou	ulovený	k2eAgFnSc7d1	ulovená
kořistí	kořist	k1gFnSc7	kořist
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
sloužily	sloužit	k5eAaImAgInP	sloužit
kamzičí	kamzičí	k2eAgInPc4d1	kamzičí
oštěpy	oštěp	k1gInPc4	oštěp
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
5-6	[number]	k4	5-6
m.	m.	k?	m.
Lovci	lovec	k1gMnSc3	lovec
kamzíky	kamzík	k1gMnPc4	kamzík
zahnali	zahnat	k5eAaPmAgMnP	zahnat
na	na	k7c4	na
skály	skála	k1gFnPc4	skála
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	on	k3xPp3gFnPc4	on
zespodu	zespodu	k6eAd1	zespodu
probodli	probodnout	k5eAaPmAgMnP	probodnout
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
oštěpy	oštěp	k1gInPc7	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
kamzíky	kamzík	k1gMnPc7	kamzík
stříleli	střílet	k5eAaImAgMnP	střílet
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
a	a	k8xC	a
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
i	i	k8xC	i
palnými	palný	k2eAgFnPc7d1	palná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ceněny	ceněn	k2eAgInPc1d1	ceněn
byly	být	k5eAaImAgInP	být
hlavně	hlavně	k9	hlavně
bezoáry	bezoár	k1gInPc1	bezoár
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
předžaludcích	předžaludek	k1gInPc6	předžaludek
kamzíků	kamzík	k1gMnPc2	kamzík
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
uplatnění	uplatnění	k1gNnSc4	uplatnění
i	i	k9	i
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
alchymii	alchymie	k1gFnSc6	alchymie
<g/>
.	.	kIx.	.
</s>
<s>
Zastřelení	zastřelení	k1gNnSc1	zastřelení
bílého	bílý	k2eAgMnSc2d1	bílý
kamzíka	kamzík	k1gMnSc2	kamzík
bylo	být	k5eAaImAgNnS	být
tabu	tabu	k1gNnSc1	tabu
a	a	k8xC	a
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
na	na	k7c4	na
takové	takový	k3xDgNnSc4	takový
zvíře	zvíře	k1gNnSc4	zvíře
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
se	se	k3xPyFc4	se
vyprávěly	vyprávět	k5eAaImAgFnP	vyprávět
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
zlatorogovi	zlatorog	k1gMnSc6	zlatorog
<g/>
,	,	kIx,	,
králi	král	k1gMnSc6	král
kamzíků	kamzík	k1gMnPc2	kamzík
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
růžky	růžek	k1gInPc7	růžek
<g/>
.	.	kIx.	.
</s>
<s>
Lovu	lov	k1gInSc2	lov
na	na	k7c4	na
kamzíky	kamzík	k1gMnPc4	kamzík
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaPmAgMnP	věnovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I	i	k9	i
a	a	k8xC	a
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Kamzíci	kamzík	k1gMnPc1	kamzík
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
většinou	většina	k1gFnSc7	většina
šoulačkou	šoulačka	k1gFnSc7	šoulačka
v	v	k7c6	v
horském	horský	k2eAgInSc6d1	horský
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fyzicky	fyzicky	k6eAd1	fyzicky
náročný	náročný	k2eAgInSc4d1	náročný
způsob	způsob	k1gInSc4	způsob
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
klade	klást	k5eAaImIp3nS	klást
velké	velký	k2eAgInPc4d1	velký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
lovcovu	lovcův	k2eAgFnSc4d1	Lovcova
výstroj	výstroj	k1gFnSc4	výstroj
i	i	k8xC	i
kondici	kondice	k1gFnSc4	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Možný	možný	k2eAgInSc1d1	možný
je	být	k5eAaImIp3nS	být
také	také	k9	také
lov	lov	k1gInSc4	lov
na	na	k7c6	na
čekané	čekaná	k1gFnSc6	čekaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
koncentrací	koncentrace	k1gFnSc7	koncentrace
kamzičí	kamzičí	k2eAgFnSc2d1	kamzičí
zvěře	zvěř	k1gFnSc2	zvěř
se	se	k3xPyFc4	se
lov	lov	k1gInSc1	lov
provádí	provádět	k5eAaImIp3nS	provádět
i	i	k9	i
naháňkami	naháňka	k1gFnPc7	naháňka
<g/>
.	.	kIx.	.
</s>
<s>
Trofejí	trofej	k1gFnSc7	trofej
z	z	k7c2	z
kamzíka	kamzík	k1gMnSc2	kamzík
jsou	být	k5eAaImIp3nP	být
růžky	růžek	k1gInPc1	růžek
(	(	kIx(	(
<g/>
háky	hák	k1gInPc1	hák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bezoárové	bezoárový	k2eAgFnSc2d1	bezoárová
koule	koule	k1gFnSc2	koule
a	a	k8xC	a
kamzičí	kamzičí	k2eAgInSc1d1	kamzičí
vous	vous	k1gInSc1	vous
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
kamzík	kamzík	k1gMnSc1	kamzík
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
bradě	brada	k1gFnSc6	brada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
alpský	alpský	k2eAgMnSc1d1	alpský
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapra	k1gMnSc1	rupicapra
rupicapra	rupicapra	k1gMnSc1	rupicapra
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
tatranský	tatranský	k2eAgMnSc1d1	tatranský
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapra	k1gMnSc1	rupicapra
tatrica	tatrica	k1gMnSc1	tatrica
<g/>
;	;	kIx,	;
Blahout	Blahout	k1gMnSc1	Blahout
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
balkánský	balkánský	k2eAgMnSc1d1	balkánský
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapr	k1gMnSc2	rupicapr
balcanica	balcanicus	k1gMnSc2	balcanicus
<g/>
;	;	kIx,	;
Bolkay	Bolkaa	k1gMnSc2	Bolkaa
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
karpatský	karpatský	k2eAgMnSc1d1	karpatský
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapra	k1gMnSc1	rupicapra
carpatica	carpatica	k1gMnSc1	carpatica
<g/>
;	;	kIx,	;
Couturier	Couturier	k1gMnSc1	Couturier
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
turecký	turecký	k2eAgMnSc1d1	turecký
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapra	k1gMnSc1	rupicapra
asiatica	asiatica	k1gMnSc1	asiatica
<g/>
;	;	kIx,	;
Lydekker	Lydekker	k1gMnSc1	Lydekker
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rupicapra	rupicapra	k1gMnSc1	rupicapra
caucasica	caucasica	k1gMnSc1	caucasica
<g/>
;	;	kIx,	;
Lydekker	Lydekker	k1gMnSc1	Lydekker
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
středozemní	středozemní	k2eAgMnSc1d1	středozemní
(	(	kIx(	(
<g/>
Rupicapra	Rupicapra	k1gMnSc1	Rupicapra
pyrenaica	pyrenaica	k1gMnSc1	pyrenaica
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
R.	R.	kA	R.
rupicapra	rupicapra	k1gFnSc1	rupicapra
pyrenaica	pyrenaica	k1gFnSc1	pyrenaica
<g/>
)	)	kIx)	)
-	-	kIx~	-
nově	nově	k6eAd1	nově
samostatný	samostatný	k2eAgInSc1d1	samostatný
druh	druh	k1gInSc1	druh
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Gaisler	Gaisler	k1gInSc1	Gaisler
J.	J.	kA	J.
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
Zoologie	zoologie	k1gFnPc1	zoologie
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
-	-	kIx~	-
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
536	[number]	k4	536
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roček	Roček	k1gMnSc1	Roček
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
<g/>
,	,	kIx,	,
fylogenze	fylogenze	k1gFnSc1	fylogenze
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
-	-	kIx~	-
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
512	[number]	k4	512
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlasák	Vlasák	k1gMnSc1	Vlasák
P.	P.	kA	P.
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Ekologie	ekologie	k1gFnSc1	ekologie
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
-	-	kIx~	-
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
291	[number]	k4	291
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anděra	Anděra	k1gFnSc1	Anděra
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
I.	I.	kA	I.
Savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
143	[number]	k4	143
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
</s>
</p>
<p>
<s>
Lov	lov	k1gInSc1	lov
</s>
</p>
<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Rupicapra	Rupicapr	k1gMnSc2	Rupicapr
rupicapra	rupicapr	k1gMnSc2	rupicapr
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
na	na	k7c6	na
Biolibu	Biolib	k1gInSc6	Biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
