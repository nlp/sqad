<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
synagoga	synagoga	k1gFnSc1	synagoga
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
Kůzová	Kůzový	k2eAgFnSc1d1	Kůzový
(	(	kIx(	(
<g/>
Wallisgrün	Wallisgrüno	k1gNnPc2	Wallisgrüno
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
čp.	čp.	k?	čp.
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podlouhlou	podlouhlý	k2eAgFnSc4d1	podlouhlá
roubenou	roubený	k2eAgFnSc4d1	roubená
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
využívána	využívat	k5eAaPmNgFnS	využívat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
zakoupena	zakoupit	k5eAaPmNgFnS	zakoupit
do	do	k7c2	do
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vůastnictví	vůastnictví	k1gNnSc2	vůastnictví
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
využívána	využíván	k2eAgNnPc1d1	využíváno
jako	jako	k8xS	jako
rekreační	rekreační	k2eAgNnPc1d1	rekreační
obydlí	obydlí	k1gNnPc1	obydlí
<g/>
.	.	kIx.	.
<g/>
Místní	místní	k2eAgFnSc1d1	místní
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
templ	templ	k1gInSc1	templ
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
stavby	stavba	k1gFnSc2	stavba
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
také	také	k6eAd1	také
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
mezi	mezi	k7c7	mezi
dnešními	dnešní	k2eAgInPc7d1	dnešní
Kožlany	Kožlan	k1gInPc7	Kožlan
a	a	k8xC	a
Strachovicemi	Strachovice	k1gFnPc7	Strachovice
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Kožlanech	Kožlan	k1gInPc6	Kožlan
<g/>
.	.	kIx.	.
</s>
<s>
Mikve	Mikvat	k5eAaPmIp3nS	Mikvat
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
místní	místní	k2eAgFnSc2d1	místní
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
židovských	židovský	k2eAgFnPc2d1	židovská
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Kožlanech	Kožlan	k1gInPc6	Kožlan
</s>
</p>
<p>
<s>
Kůzová	Kůzovat	k5eAaImIp3nS	Kůzovat
</s>
</p>
