<s>
Mangaka	Mangak	k1gMnSc4	Mangak
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
漫	漫	k?	漫
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgInSc1d1	japonský
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
mangy	mango	k1gNnPc7	mango
nebo	nebo	k8xC	nebo
karikaturistu	karikaturista	k1gMnSc4	karikaturista
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Japonsko	Japonsko	k1gNnSc4	Japonsko
slovo	slovo	k1gNnSc1	slovo
manga	mango	k1gNnSc2	mango
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
japonský	japonský	k2eAgInSc4d1	japonský
komiks	komiks	k1gInSc4	komiks
a	a	k8xC	a
mangaka	mangak	k1gMnSc4	mangak
znamená	znamenat	k5eAaImIp3nS	znamenat
autor	autor	k1gMnSc1	autor
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
Japonec	Japonec	k1gMnSc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
studují	studovat	k5eAaImIp3nP	studovat
léta	léto	k1gNnPc4	léto
na	na	k7c6	na
umělecké	umělecký	k2eAgFnSc6d1	umělecká
univerzitě	univerzita	k1gFnSc6	univerzita
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
učněm	učeň	k1gMnSc7	učeň
jiného	jiné	k1gNnSc2	jiné
mangaky	mangak	k1gInPc4	mangak
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
mangy	mango	k1gNnPc7	mango
jako	jako	k9	jako
profesionální	profesionální	k2eAgInSc4d1	profesionální
mangaka	mangak	k1gMnSc2	mangak
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
průmyslu	průmysl	k1gInSc2	průmysl
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
předtím	předtím	k6eAd1	předtím
byli	být	k5eAaImAgMnP	být
asistenti	asistent	k1gMnPc1	asistent
<g/>
.	.	kIx.	.
<g/>
Například	například	k6eAd1	například
Naoko	naoko	k6eAd1	naoko
Takeuchi	Takeuch	k1gMnPc1	Takeuch
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Sailor	Sailor	k1gMnSc1	Sailor
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
sponzorovanou	sponzorovaný	k2eAgFnSc4d1	sponzorovaná
Kodansha	Kodansh	k1gMnSc2	Kodansh
a	a	k8xC	a
manga	mango	k1gNnPc1	mango
umělcem	umělec	k1gMnSc7	umělec
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc4	Tezuk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Mangaka	Mangak	k1gMnSc2	Mangak
se	se	k3xPyFc4	se
proslaví	proslavit	k5eAaPmIp3nS	proslavit
díky	díky	k7c3	díky
uznání	uznání	k1gNnSc3	uznání
svých	svůj	k3xOyFgFnPc2	svůj
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
<g/>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
i	i	k9	i
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
<g/>
které	který	k3yQgFnPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
umělcům	umělec	k1gMnPc3	umělec
mangaky	mangak	k1gInPc1	mangak
postoupit	postoupit	k5eAaPmF	postoupit
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
vydavatelů	vydavatel	k1gMnPc2	vydavatel
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Japonsko	Japonsko	k1gNnSc1	Japonsko
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
prosperující	prosperující	k2eAgInSc4d1	prosperující
nezavislý	zavislý	k2eNgInSc4d1	nezavislý
komický	komický	k2eAgInSc4d1	komický
trh	trh	k1gInSc4	trh
pro	pro	k7c4	pro
amaterské	amaterský	k2eAgMnPc4d1	amaterský
a	a	k8xC	a
poloprofesionální	poloprofesionální	k2eAgMnPc4d1	poloprofesionální
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
<g/>
Vytvoření	vytvoření	k1gNnSc1	vytvoření
mangy	mango	k1gNnPc7	mango
je	být	k5eAaImIp3nS	být
zřídka	zřídka	k6eAd1	zřídka
dolo	dolo	k1gNnSc1	dolo
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
Mangaka	Mangak	k1gMnSc4	Mangak
musí	muset	k5eAaImIp3nS	muset
pracovat	pracovat	k5eAaImF	pracovat
se	se	k3xPyFc4	se
sortimentem	sortiment	k1gInSc7	sortiment
druhých	druhý	k4xOgFnPc2	druhý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
práce	práce	k1gFnSc1	práce
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
,	,	kIx,	,
<g/>
publikována	publikovat	k5eAaBmNgFnS	publikovat
a	a	k8xC	a
koupená	koupený	k2eAgFnSc1d1	koupená
<g/>
.	.	kIx.	.
</s>
<s>
Editor	editor	k1gMnSc1	editor
Většina	většina	k1gFnSc1	většina
profesionálně	profesionálně	k6eAd1	profesionálně
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
mangaka	mangak	k1gMnSc2	mangak
práce	práce	k1gFnSc2	práce
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
editorem	editor	k1gInSc7	editor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
šéfa	šéf	k1gMnSc4	šéf
mangaky	mangak	k1gMnPc7	mangak
a	a	k8xC	a
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
seriovou	seriový	k2eAgFnSc4d1	seriová
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Editor	editor	k1gInSc1	editor
radí	radit	k5eAaImIp3nS	radit
o	o	k7c4	o
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
a	a	k8xC	a
tempo	tempo	k1gNnSc4	tempo
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
splněny	splněn	k2eAgFnPc4d1	splněna
lhůty	lhůta	k1gFnPc4	lhůta
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
manga	mango	k1gNnPc4	mango
zůstane	zůstat	k5eAaPmIp3nS	zůstat
do	do	k7c2	do
firemních	firemní	k2eAgInPc2d1	firemní
standartů	standart	k1gInPc2	standart
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
Akira	Akira	k1gFnSc1	Akira
Toriyama	Toriyama	k1gFnSc1	Toriyama
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
bývalý	bývalý	k2eAgInSc1d1	bývalý
editor	editor	k1gInSc1	editor
Kazuhiko	Kazuhika	k1gFnSc5	Kazuhika
Torishima	Torishimum	k1gNnPc5	Torishimum
<g/>
.	.	kIx.	.
</s>
<s>
Editor	editor	k1gInSc1	editor
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
návrhy	návrh	k1gInPc4	návrh
licencovaného	licencovaný	k2eAgNnSc2d1	licencované
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
adaptace	adaptace	k1gFnSc2	adaptace
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
toto	tento	k3xDgNnSc1	tento
clo	clo	k1gNnSc1	clo
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
spadat	spadat	k5eAaPmF	spadat
do	do	k7c2	do
manga	mango	k1gNnSc2	mango
umělce	umělec	k1gMnSc2	umělec
nebo	nebo	k8xC	nebo
agenta	agent	k1gMnSc2	agent
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Mangaka	Mangak	k1gMnSc2	Mangak
může	moct	k5eAaImIp3nS	moct
psát	psát	k5eAaImF	psát
i	i	k8xC	i
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
série	série	k1gFnPc4	série
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mangaka	Mangak	k1gMnSc4	Mangak
má	mít	k5eAaImIp3nS	mít
typický	typický	k2eAgInSc1d1	typický
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
každý	každý	k3xTgInSc1	každý
rozhovor	rozhovor	k1gInSc1	rozhovor
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vejít	vejít	k5eAaPmF	vejít
do	do	k7c2	do
fyzických	fyzický	k2eAgFnPc2d1	fyzická
omezení	omezení	k1gNnPc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Takeshi	Takeshi	k6eAd1	Takeshi
Obata	Obata	k1gFnSc1	Obata
z	z	k7c2	z
Death	Deatha	k1gFnPc2	Deatha
Note	Not	k1gFnSc2	Not
<g/>
,	,	kIx,	,
Tetsuo	Tetsuo	k1gMnSc1	Tetsuo
Hara	Hara	k1gMnSc1	Hara
z	z	k7c2	z
Fist	Fist	k1gMnSc1	Fist
of	of	k?	of
north	north	k1gMnSc1	north
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Ryoichi	Ryoichi	k1gNnSc2	Ryoichi
Ikegami	Ikega	k1gFnPc7	Ikega
z	z	k7c2	z
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
<g/>
,	,	kIx,	,
Aki	Aki	k1gFnSc4	Aki
Shirou	Shirý	k2eAgFnSc4d1	Shirý
a	a	k8xC	a
Hajime	Hajim	k1gInSc5	Hajim
Kamoshida	Kamoshida	k1gFnSc1	Kamoshida
z	z	k7c2	z
Sakura-so	Sakuraa	k1gFnSc5	Sakura-sa
ne	ne	k9	ne
Pet	Pet	k1gFnPc3	Pet
na	na	k7c4	na
Kanojo	Kanojo	k1gNnSc4	Kanojo
jsou	být	k5eAaImIp3nP	být
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
manga	mango	k1gNnPc1	mango
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
autory	autor	k1gMnPc7	autor
většinou	většina	k1gFnSc7	většina
své	svůj	k3xOyFgFnSc2	svůj
kariery	kariera	k1gFnSc2	kariera
<g/>
.	.	kIx.	.
</s>
<s>
Asistenti	asistent	k1gMnPc1	asistent
Většina	většina	k1gFnSc1	většina
umělců	umělec	k1gMnPc2	umělec
mají	mít	k5eAaImIp3nP	mít
asistenty	asistent	k1gMnPc4	asistent
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jim	on	k3xPp3gMnPc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
dokončit	dokončit	k5eAaPmF	dokončit
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
a	a	k8xC	a
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
manga	mango	k1gNnPc1	mango
umělců	umělec	k1gMnPc2	umělec
nemají	mít	k5eNaImIp3nP	mít
vůbec	vůbec	k9	vůbec
asistenty	asistent	k1gMnPc4	asistent
<g/>
,	,	kIx,	,
a	a	k8xC	a
raději	rád	k6eAd2	rád
dělají	dělat	k5eAaImIp3nP	dělat
všechno	všechen	k3xTgNnSc1	všechen
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
asistentů	asistent	k1gMnPc2	asistent
také	také	k9	také
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
mangaka	mangak	k1gMnSc4	mangak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Asistenti	asistent	k1gMnPc1	asistent
jsou	být	k5eAaImIp3nP	být
používání	používání	k1gNnSc4	používání
pro	pro	k7c4	pro
barvení	barvení	k1gNnSc4	barvení
<g/>
,	,	kIx,	,
písma	písmo	k1gNnPc1	písmo
a	a	k8xC	a
stínování	stínování	k1gNnPc1	stínování
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
manga	mango	k1gNnPc1	mango
umělci	umělec	k1gMnPc1	umělec
děljí	děljet	k5eAaImIp3nP	děljet
sketchwork	sketchwork	k1gInSc4	sketchwork
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
asistenty	asistent	k1gMnPc4	asistent
aby	aby	kYmCp3nS	aby
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
všechny	všechen	k3xTgInPc1	všechen
detaily	detail	k1gInPc1	detail
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgNnSc1d2	běžnější
pro	pro	k7c4	pro
asistenty	asistent	k1gMnPc4	asistent
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
pozadím	pozadí	k1gNnSc7	pozadí
apod.	apod.	kA	apod.
Většina	většina	k1gFnSc1	většina
manga	mango	k1gNnSc2	mango
umělců	umělec	k1gMnPc2	umělec
začali	začít	k5eAaPmAgMnP	začít
jako	jako	k9	jako
asistenti	asistent	k1gMnPc1	asistent
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Miwa	Miw	k2eAgFnSc1d1	Miw
Ueda	Ueda	k1gFnSc1	Ueda
s	s	k7c7	s
Naoko	naoko	k6eAd1	naoko
Takeuchi	Takeuch	k1gInPc7	Takeuch
<g/>
,	,	kIx,	,
Leji	lít	k5eAaImIp1nS	lít
Matsumoto	Matsumota	k1gFnSc5	Matsumota
s	s	k7c7	s
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc4	Tezuk
<g/>
,	,	kIx,	,
Kaoru	Kaora	k1gFnSc4	Kaora
Shintani	Shintaň	k1gFnSc3	Shintaň
s	s	k7c7	s
Leiji	Leij	k1gMnPc7	Leij
Matsumoto	Matsumota	k1gFnSc5	Matsumota
<g/>
,	,	kIx,	,
Eiichiro	Eiichiro	k1gNnSc4	Eiichiro
Oda	Oda	k1gFnSc2	Oda
s	s	k7c7	s
Hiroyuki	Hiroyuki	k1gNnSc7	Hiroyuki
a	a	k8xC	a
Mikio	Mikio	k1gMnSc1	Mikio
Ito	Ito	k1gMnSc1	Ito
s	s	k7c7	s
Nobuhiro	Nobuhiro	k1gNnSc1	Nobuhiro
Watsuki	Watsuki	k1gNnSc3	Watsuki
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
asistenty	asistent	k1gMnPc4	asistent
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
mít	mít	k5eAaImF	mít
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
jako	jako	k9	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nezávislým	závislý	k2eNgInSc7d1	nezávislý
manga	mango	k1gNnPc1	mango
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
různými	různý	k2eAgInPc7d1	různý
mangaky	mangak	k1gInPc7	mangak
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Manga	mango	k1gNnPc1	mango
a	a	k8xC	a
ka	ka	k?	ka
-ka	a	k?	-ka
(	(	kIx(	(
<g/>
家	家	k?	家
<g/>
)	)	kIx)	)
přípona	přípona	k1gFnSc1	přípona
znamená	znamenat	k5eAaImIp3nS	znamenat
určitý	určitý	k2eAgInSc4d1	určitý
stupeň	stupeň	k1gInSc4	stupeň
odbornosti	odbornost	k1gFnSc2	odbornost
<g/>
.	.	kIx.	.
</s>
