<p>
<s>
MADETA	MADETA	kA	MADETA
a.	a.	k?	a.
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
firma	firma	k1gFnSc1	firma
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
výrobou	výroba	k1gFnSc7	výroba
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Závody	závod	k1gInPc1	závod
firmy	firma	k1gFnSc2	firma
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
litrů	litr	k1gInPc2	litr
mléka	mléko	k1gNnSc2	mléko
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
240	[number]	k4	240
druhů	druh	k1gInPc2	druh
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Nivu	niva	k1gFnSc4	niva
a	a	k8xC	a
pomazánkové	pomazánkový	k2eAgNnSc4d1	pomazánkové
máslo	máslo	k1gNnSc4	máslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
exportují	exportovat	k5eAaBmIp3nP	exportovat
své	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
do	do	k7c2	do
15	[number]	k4	15
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mlékárenské	mlékárenský	k2eAgNnSc1d1	mlékárenské
družstvo	družstvo	k1gNnSc1	družstvo
táborské	táborský	k2eAgNnSc1d1	táborské
<g/>
.	.	kIx.	.
</s>
<s>
Zkratkou	zkratka	k1gFnSc7	zkratka
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písmen	písmeno	k1gNnPc2	písmeno
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
akronym	akronym	k1gInSc1	akronym
<g/>
)	)	kIx)	)
značka	značka	k1gFnSc1	značka
MADETA	MADETA	kA	MADETA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
registrována	registrován	k2eAgFnSc1d1	registrována
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
největším	veliký	k2eAgMnSc7d3	veliký
zpracovatelem	zpracovatel	k1gMnSc7	zpracovatel
mléka	mléko	k1gNnSc2	mléko
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
znárodnění	znárodnění	k1gNnSc3	znárodnění
provozů	provoz	k1gInPc2	provoz
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
následoval	následovat	k5eAaImAgInS	následovat
vznik	vznik	k1gInSc4	vznik
podniku	podnik	k1gInSc2	podnik
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
mlékárny	mlékárna	k1gFnSc2	mlékárna
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yIgInSc4	který
podnik	podnik	k1gInSc4	podnik
MADETA	MADETA	kA	MADETA
náležel	náležet	k5eAaImAgMnS	náležet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
listopadové	listopadový	k2eAgFnSc6d1	listopadová
revoluci	revoluce	k1gFnSc6	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
k	k	k7c3	k
restrukturalizaci	restrukturalizace	k1gFnSc3	restrukturalizace
a	a	k8xC	a
koncentrování	koncentrování	k1gNnSc3	koncentrování
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
firma	firma	k1gFnSc1	firma
přijala	přijmout	k5eAaPmAgFnS	přijmout
název	název	k1gInSc4	název
MADETA	MADETA	kA	MADETA
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
českobudějovický	českobudějovický	k2eAgMnSc1d1	českobudějovický
podnikatel	podnikatel	k1gMnSc1	podnikatel
Milan	Milan	k1gMnSc1	Milan
Teplý	Teplý	k1gMnSc1	Teplý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
firma	firma	k1gFnSc1	firma
pět	pět	k4xCc4	pět
výrobních	výrobní	k2eAgInPc2d1	výrobní
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
specializovaných	specializovaný	k2eAgNnPc2d1	specializované
na	na	k7c4	na
část	část	k1gFnSc4	část
sortimentu	sortiment	k1gInSc2	sortiment
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
–	–	k?	–
CZ	CZ	kA	CZ
3704	[number]	k4	3704
</s>
</p>
<p>
<s>
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
CZ	CZ	kA	CZ
16	[number]	k4	16
</s>
</p>
<p>
<s>
Planá	Planá	k1gFnSc1	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
–	–	k?	–
CZ	CZ	kA	CZ
208	[number]	k4	208
</s>
</p>
<p>
<s>
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
–	–	k?	–
CZ	CZ	kA	CZ
218	[number]	k4	218
</s>
</p>
<p>
<s>
Řípec	Řípec	k1gInSc1	Řípec
–	–	k?	–
CZ	CZ	kA	CZ
615	[number]	k4	615
<g/>
Madeta	Madeto	k1gNnSc2	Madeto
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
firem	firma	k1gFnPc2	firma
Madeta	Madeto	k1gNnSc2	Madeto
a.	a.	k?	a.
s.	s.	k?	s.
a	a	k8xC	a
Madeta	Madet	k2eAgNnPc4d1	Madet
Agro	Agro	k1gNnSc4	Agro
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
nákupem	nákup	k1gInSc7	nákup
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
poskytováním	poskytování	k1gNnSc7	poskytování
služeb	služba	k1gFnPc2	služba
zemědělcům	zemědělec	k1gMnPc3	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
třetí	třetí	k4xOgFnSc1	třetí
samostatná	samostatný	k2eAgFnSc1d1	samostatná
firma	firma	k1gFnSc1	firma
MADETA	MADETA	kA	MADETA
Logistic	Logistice	k1gFnPc2	Logistice
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
logistiku	logistika	k1gFnSc4	logistika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
převzata	převzat	k2eAgFnSc1d1	převzata
Madetou	Madeta	k1gFnSc7	Madeta
a.	a.	k?	a.
s.	s.	k?	s.
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
