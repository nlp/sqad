<s>
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
je	být	k5eAaImIp3nS
kambodžská	kambodžský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
společnosti	společnost	k1gFnSc2
Prince	princ	k1gMnSc2
International	International	k1gMnSc2
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c4
Phnom	Phnom	k1gInSc4
Penhu	Penh	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
registrační	registrační	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
200	#num#	k4
miliónů	milión	k4xCgInPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
její	její	k3xOp3gNnSc4
sídlo	sídlo	k1gNnSc4
je	být	k5eAaImIp3nS
také	také	k9
Phnom	Phnom	k1gInSc1
Penhu	Penh	k1gInSc2
v	v	k7c6
Kambodži	Kambodža	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
stát	stát	k1gInSc1
hlavním	hlavní	k2eAgMnSc7d1
leteckým	letecký	k2eAgMnSc7d1
dopravcem	dopravce	k1gMnSc7
v	v	k7c6
Kambodži	Kambodža	k1gFnSc6
<g/>
.	.	kIx.
</s>