<s>
Cambodia	Cambodium	k1gNnPc4
Airways	Airways	k1gInSc1
</s>
<s>
Cambodia	Cambodium	k1gNnPc4
AirwaysAirbus	AirwaysAirbus	k1gInSc1
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
A319-100	A319-100	k1gMnSc1
XU-797	XU-797	k1gMnSc1
</s>
<s>
IATAKR	IATAKR	kA
</s>
<s>
ICAOKME	ICAOKME	kA
</s>
<s>
CALLSIGNCambodia	CALLSIGNCambodium	k1gNnPc4
Airways	Airways	k1gInSc1
</s>
<s>
Zahájení	zahájení	k1gNnSc1
činnosti	činnost	k1gFnSc2
<g/>
2018	#num#	k4
<g/>
SídloPhnom	SídloPhnom	k1gInSc1
PenhHlavní	PenhHlavní	k2eAgInSc4d1
základnaPhnom	základnaPhnom	k1gInSc4
Penh	Penh	k1gMnSc1
International	International	k1gMnSc1
AirportVelikost	AirportVelikost	k1gFnSc4
flotily	flotila	k1gFnSc2
<g/>
5	#num#	k4
<g/>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
https://www.cambodia-airways.com/#/main/home/booking	https://www.cambodia-airways.com/#/main/home/booking	k1gInSc1
</s>
<s>
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
je	být	k5eAaImIp3nS
kambodžská	kambodžský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
společnosti	společnost	k1gFnSc2
Prince	princ	k1gMnSc2
International	International	k1gMnSc2
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c4
Phnom	Phnom	k1gInSc4
Penhu	Penh	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
registrační	registrační	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
200	#num#	k4
miliónů	milión	k4xCgInPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
její	její	k3xOp3gNnSc4
sídlo	sídlo	k1gNnSc4
je	být	k5eAaImIp3nS
také	také	k9
Phnom	Phnom	k1gInSc1
Penhu	Penh	k1gInSc2
v	v	k7c6
Kambodži	Kambodža	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
stát	stát	k1gInSc1
hlavním	hlavní	k2eAgMnSc7d1
leteckým	letecký	k2eAgMnSc7d1
dopravcem	dopravce	k1gMnSc7
v	v	k7c6
Kambodži	Kambodža	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2017	#num#	k4
získala	získat	k5eAaPmAgFnS
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
licenci	licence	k1gFnSc4
od	od	k7c2
Council	Councila	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Development	Development	k1gMnSc1
of	of	k?
Cambodia	Cambodium	k1gNnSc2
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
obdržela	obdržet	k5eAaPmAgFnS
od	od	k7c2
obchodní	obchodní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
zakládací	zakládací	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
získala	získat	k5eAaPmAgFnS
certifikát	certifikát	k1gInSc4
AOC	AOC	kA
s	s	k7c7
kódem	kód	k1gInSc7
IATA	IATA	kA
KR.	KR.	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Provoz	provoz	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
letecká	letecký	k2eAgFnSc1d1
společnos	společnosa	k1gFnPc2
zahájila	zahájit	k5eAaPmAgFnS
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
a	a	k8xC
první	první	k4xOgFnSc7
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
z	z	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Kambodže	Kambodža	k1gFnSc2
Phnom	Phnom	k1gInSc1
Penhu	Penha	k1gFnSc4
do	do	k7c2
Siem	Siema	k1gFnPc2
Reap	Reap	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
branou	brána	k1gFnSc7
do	do	k7c2
Angkoru	Angkor	k1gInSc2
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
první	první	k4xOgInSc1
let	let	k1gInSc1
do	do	k7c2
Sihanoukville	Sihanoukville	k1gFnSc2
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
první	první	k4xOgInSc1
mezinárodní	mezinárodní	k2eAgInSc1d1
let	let	k1gInSc1
do	do	k7c2
Macaa	Macao	k1gNnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Destinace	destinace	k1gFnSc1
</s>
<s>
Centrem	centr	k1gInSc7
společnosti	společnost	k1gFnSc2
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
je	být	k5eAaImIp3nS
Phnom	Phnom	k1gInSc1
Penh	Penha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kambodži	Kambodža	k1gFnSc6
létá	létat	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
do	do	k7c2
Siem	Siema	k1gFnPc2
Reap	Reap	k1gInSc1
a	a	k8xC
Sihanoukville	Sihanoukville	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
vedou	vést	k5eAaImIp3nP
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
společnost	společnost	k1gFnSc1
létá	létat	k5eAaImIp3nS
do	do	k7c2
letiště	letiště	k1gNnSc2
Macau	Macao	k1gNnSc3
ze	z	k7c2
všech	všecek	k3xTgInPc2
tří	tři	k4xCgNnPc2
letišť	letiště	k1gNnPc2
v	v	k7c6
Kambodži	Kambodža	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tchaj-wan	Tchaj-wan	k1gInSc1
je	být	k5eAaImIp3nS
obsluhován	obsluhován	k2eAgInSc1d1
letiště	letiště	k1gNnSc2
Siem	Siem	k1gMnSc1
Reap	Reap	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
k	k	k7c3
Taipeie	Taipeie	k1gFnPc1
je	on	k3xPp3gMnPc4
pouze	pouze	k6eAd1
pouze	pouze	k6eAd1
charterová	charterový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
pravidelné	pravidelný	k2eAgInPc4d1
lety	let	k1gInPc4
do	do	k7c2
Taichungu	Taichung	k1gInSc2
začali	začít	k5eAaPmAgMnP
v	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
plánované	plánovaný	k2eAgFnPc4d1
mezinárodní	mezinárodní	k2eAgFnPc4d1
destinace	destinace	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Hongkong	Hongkong	k1gInSc1
<g/>
,	,	kIx,
Manila	Manila	k1gFnSc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc1
<g/>
,	,	kIx,
Kuala	Kuala	k1gMnSc1
Lumpur	Lumpur	k1gMnSc1
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
a	a	k8xC
Bangkok	Bangkok	k1gInSc1
v	v	k7c6
Thajsku	Thajsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
října	říjen	k1gInSc2
2018	#num#	k4
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc2
Taipei	Taipei	k1gNnPc2
na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
obsluhováno	obsluhovat	k5eAaImNgNnS
přímo	přímo	k6eAd1
z	z	k7c2
Siem	Siema	k1gFnPc2
Reap	Reap	k1gInSc4
s	s	k7c7
přímým	přímý	k2eAgNnSc7d1
spojením	spojení	k1gNnSc7
s	s	k7c7
Tchaj-čungem	Tchaj-čung	k1gInSc7
naplánované	naplánovaný	k2eAgFnSc2d1
na	na	k7c4
listopad	listopad	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jsou	být	k5eAaImIp3nP
plánované	plánovaný	k2eAgInPc4d1
charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
taktéž	taktéž	k?
do	do	k7c2
Koreje	Korea	k1gFnSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
října	říjen	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
společnosti	společnost	k1gFnSc2
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
pěti	pět	k4xCc2
letadel	letadlo	k1gNnPc2
s	s	k7c7
průměrným	průměrný	k2eAgNnSc7d1
stářím	stáří	k1gNnSc7
7	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
plánuje	plánovat	k5eAaImIp3nS
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
koupit	koupit	k5eAaPmF
20	#num#	k4
letadel	letadlo	k1gNnPc2
rodiny	rodina	k1gFnSc2
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
330	#num#	k4
<g/>
s	s	k7c7
bude	být	k5eAaImBp3nS
pořizován	pořizován	k2eAgMnSc1d1
pro	pro	k7c4
lety	let	k1gInPc4
do	do	k7c2
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
nákup	nákup	k1gInSc4
letadel	letadlo	k1gNnPc2
zatím	zatím	k6eAd1
nebylo	být	k5eNaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
pevné	pevný	k2eAgNnSc1d1
datum	datum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
typ	typ	k1gInSc1
letadla	letadlo	k1gNnSc2
</s>
<s>
číslo	číslo	k1gNnSc1
</s>
<s>
objednáno	objednán	k2eAgNnSc1d1
</s>
<s>
registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Airbus	airbus	k1gInSc1
A319-100	A319-100	k1gFnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
XU-787	XU-787	k4
</s>
<s>
XU-797	XU-797	k4
</s>
<s>
XU-878	XU-878	k4
</s>
<s>
Airbus	airbus	k1gInSc1
A320-200	A320-200	k1gFnSc2
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
XU-761	XU-761	k4
</s>
<s>
leasing	leasing	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Aero	aero	k1gNnSc1
K.	K.	kA
</s>
<s>
XU-762	XU-762	k4
</s>
<s>
leasing	leasing	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Aero	aero	k1gNnSc1
K.	K.	kA
</s>
<s>
XU-763	XU-763	k4
</s>
<s>
objednáno	objednán	k2eAgNnSc1d1
<g/>
,	,	kIx,
leasing	leasing	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Aero	aero	k1gNnSc1
K.	K.	kA
</s>
<s>
celkově	celkově	k6eAd1
</s>
<s>
5	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Khmer	Khmer	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-21	2018-06-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
signs	signsa	k1gFnPc2
long-term	long-term	k1gInSc1
agreement	agreement	k1gMnSc1
with	with	k1gMnSc1
Sabre	Sabr	k1gInSc5
to	ten	k3xDgNnSc1
support	support	k1gInSc1
ambitious	ambitious	k1gMnSc1
growth	growth	k1gMnSc1
objectives	objectives	k1gMnSc1
«	«	k?
Sabre	Sabr	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
secures	securesa	k1gFnPc2
AOC	AOC	kA
<g/>
,	,	kIx,
launches	launches	k1gInSc1
<g/>
.	.	kIx.
ch-aviation	ch-aviation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
files	files	k1gMnSc1
operational	operationat	k5eAaPmAgMnS,k5eAaImAgMnS
network	network	k1gInSc4
from	from	k1gMnSc1
July	Jula	k1gFnSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Routesonline	Routesonlin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
to	ten	k3xDgNnSc4
launch	launch	k1gMnSc1
Siem	Siem	k1gMnSc1
Reap-Taichung	Reap-Taichung	k1gMnSc1
flights	flights	k1gInSc1
|	|	kIx~
Economics	Economics	k1gInSc1
|	|	kIx~
FOCUS	FOCUS	kA
TAIWAN	TAIWAN	kA
-	-	kIx~
CNA	CNA	kA
ENGLISH	ENGLISH	kA
NEWS	NEWS	kA
<g/>
.	.	kIx.
focustaiwan	focustaiwan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
tw	tw	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
files	files	k1gMnSc1
operational	operationat	k5eAaImAgMnS,k5eAaPmAgMnS
network	network	k1gInSc4
from	from	k1gMnSc1
July	Jula	k1gFnSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Routesonline	Routesonlin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Please	Pleasa	k1gFnSc3
refresh	refresha	k1gFnPc2
this	this	k6eAd1
page	page	k6eAd1
<g/>
.	.	kIx.
www.planespotters.net	www.planespotters.net	k5eAaImF,k5eAaBmF,k5eAaPmF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Cambodia	Cambodium	k1gNnSc2
Airways	Airwaysa	k1gFnPc2
secures	securesa	k1gFnPc2
AOC	AOC	kA
<g/>
,	,	kIx,
launches	launches	k1gInSc1
<g/>
.	.	kIx.
ch-aviation	ch-aviation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Fotografie	fotografie	k1gFnSc1
letadel	letadlo	k1gNnPc2
Cambodia	Cambodium	k1gNnSc2
Airways	Airways	k1gInSc4
na	na	k7c4
Airline	Airlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
