<s>
Hadron	Hadron	k1gInSc1	Hadron
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
silně	silně	k6eAd1	silně
interagující	interagující	k2eAgFnSc1d1	interagující
subatomární	subatomární	k2eAgFnSc1d1	subatomární
částice	částice	k1gFnSc1	částice
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
atom	atom	k1gInSc4	atom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
kvarky	kvark	k1gInPc1	kvark
<g/>
,	,	kIx,	,
antikvarky	antikvark	k1gInPc1	antikvark
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
také	také	k9	také
gluony	gluon	k1gInPc1	gluon
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
složeny	složit	k5eAaPmNgFnP	složit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
gluonů	gluon	k1gInPc2	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
spinu	spin	k1gInSc2	spin
a	a	k8xC	a
kvarkového	kvarkový	k2eAgNnSc2d1	kvarkový
složení	složení	k1gNnSc2	složení
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
:	:	kIx,	:
baryony	baryona	k1gFnPc1	baryona
-	-	kIx~	-
hadrony	hadron	k1gInPc1	hadron
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
(	(	kIx(	(
<g/>
fermiony	fermion	k1gInPc1	fermion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
kvarků	kvark	k1gInPc2	kvark
(	(	kIx(	(
<g/>
antibaryony	antibaryona	k1gFnSc2	antibaryona
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
antikvarků	antikvark	k1gInPc2	antikvark
<g/>
)	)	kIx)	)
mezony	mezon	k1gInPc1	mezon
-	-	kIx~	-
hadrony	hadron	k1gInPc1	hadron
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
(	(	kIx(	(
<g/>
bosony	bosona	k1gFnSc2	bosona
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
kvarku	kvark	k1gInSc2	kvark
a	a	k8xC	a
antikvarku	antikvark	k1gInSc2	antikvark
a	a	k8xC	a
exotické	exotický	k2eAgFnSc3d1	exotická
nově	nova	k1gFnSc3	nova
objevené	objevený	k2eAgFnSc2d1	objevená
složené	složený	k2eAgFnSc2d1	složená
částice	částice	k1gFnSc2	částice
<g/>
:	:	kIx,	:
tetrakvarky	tetrakvarka	k1gFnSc2	tetrakvarka
-	-	kIx~	-
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
2	[number]	k4	2
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
2	[number]	k4	2
antikvarků	antikvark	k1gInPc2	antikvark
<g/>
;	;	kIx,	;
pentakvarky	pentakvark	k1gInPc4	pentakvark
-	-	kIx~	-
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
4	[number]	k4	4
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
1	[number]	k4	1
antikvarku	antikvark	k1gInSc2	antikvark
<g/>
;	;	kIx,	;
dibaryony	dibaryona	k1gFnPc1	dibaryona
-	-	kIx~	-
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
6	[number]	k4	6
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
experimentálně	experimentálně	k6eAd1	experimentálně
nepotvrzenými	potvrzený	k2eNgInPc7d1	nepotvrzený
hadrony	hadron	k1gInPc7	hadron
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
standardním	standardní	k2eAgInSc7d1	standardní
modelem	model	k1gInSc7	model
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
glueballs	glueballs	k6eAd1	glueballs
<g/>
/	/	kIx~	/
<g/>
gluebally	glueball	k1gInPc1	glueball
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
gluonia	gluonium	k1gNnSc2	gluonium
<g/>
)	)	kIx)	)
–	–	k?	–
exotické	exotický	k2eAgInPc4d1	exotický
hadrony	hadron	k1gInPc4	hadron
složené	složený	k2eAgInPc4d1	složený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
gluonů	gluon	k1gInPc2	gluon
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
hybridní	hybridní	k2eAgMnSc1d1	hybridní
<g/>
"	"	kIx"	"
hadrony	hadron	k1gInPc1	hadron
<g/>
,	,	kIx,	,
vázané	vázaný	k2eAgInPc1d1	vázaný
stavy	stav	k1gInPc1	stav
kvarků	kvark	k1gInPc2	kvark
<g/>
/	/	kIx~	/
<g/>
antikvarků	antikvark	k1gInPc2	antikvark
a	a	k8xC	a
(	(	kIx(	(
<g/>
ne	ne	k9	ne
virtuálních	virtuální	k2eAgInPc2d1	virtuální
<g/>
)	)	kIx)	)
gluonů	gluon	k1gInPc2	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Nukleony	nukleon	k1gInPc1	nukleon
jsou	být	k5eAaImIp3nP	být
hadrony	hadron	k1gInPc4	hadron
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
kvarků	kvark	k1gInPc2	kvark
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgInPc7	dva
kvarky	kvark	k1gInPc7	kvark
u	u	k7c2	u
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
d	d	k?	d
(	(	kIx(	(
<g/>
uud	uud	k?	uud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
neutron	neutron	k1gInSc1	neutron
jedním	jeden	k4xCgInSc7	jeden
kvarkem	kvark	k1gInSc7	kvark
u	u	k7c2	u
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
d	d	k?	d
(	(	kIx(	(
<g/>
udd	udd	k?	udd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
základní	základní	k2eAgInPc1d1	základní
stavy	stav	k1gInPc1	stav
s	s	k7c7	s
výsledným	výsledný	k2eAgNnSc7d1	výsledné
izospinem	izospino	k1gNnSc7	izospino
1⁄	1⁄	k?	1⁄
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc4d1	stejné
složení	složení	k1gNnSc4	složení
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
baryonové	baryonový	k2eAgFnPc1d1	baryonová
rezonance	rezonance	k1gFnPc1	rezonance
N	N	kA	N
(	(	kIx(	(
<g/>
izospin	izospin	k1gMnSc1	izospin
1⁄	1⁄	k?	1⁄
<g/>
)	)	kIx)	)
a	a	k8xC	a
Δ	Δ	k?	Δ
(	(	kIx(	(
<g/>
izospin	izospin	k1gInSc1	izospin
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
silnou	silný	k2eAgFnSc4d1	silná
interakci	interakce	k1gFnSc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Hadron	Hadron	k1gInSc1	Hadron
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
má	mít	k5eAaImIp3nS	mít
barevný	barevný	k2eAgInSc1d1	barevný
náboj	náboj	k1gInSc1	náboj
roven	roven	k2eAgInSc1d1	roven
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
barevné	barevný	k2eAgInPc1d1	barevný
náboje	náboj	k1gInPc1	náboj
kvarků	kvark	k1gInPc2	kvark
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
vyruší	vyrušit	k5eAaPmIp3nS	vyrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
hadronu	hadron	k1gInSc6	hadron
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
uvedených	uvedený	k2eAgInPc2d1	uvedený
kvarků	kvark	k1gInPc2	kvark
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
gluony	gluon	k1gInPc1	gluon
a	a	k8xC	a
páry	pára	k1gFnSc2	pára
kvark-antikvark	kvarkntikvark	k1gInSc1	kvark-antikvark
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
kvark	kvark	k1gInSc1	kvark
uvnitř	uvnitř	k7c2	uvnitř
hadronu	hadron	k1gInSc2	hadron
si	se	k3xPyFc3	se
neustále	neustále	k6eAd1	neustále
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
barevné	barevný	k2eAgInPc4d1	barevný
náboje	náboj	k1gInPc4	náboj
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
kvarky	kvark	k1gInPc7	kvark
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
hadronu	hadron	k1gInSc6	hadron
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
hadronu	hadron	k1gInSc3	hadron
existuje	existovat	k5eAaImIp3nS	existovat
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
antičástice	antičástice	k1gFnSc2	antičástice
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
antikvarků	antikvark	k1gInPc2	antikvark
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hadron	hadron	k1gMnSc1	hadron
<g/>
,	,	kIx,	,
nerozpadající	rozpadající	k2eNgMnSc1d1	rozpadající
se	se	k3xPyFc4	se
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
stavu	stav	k1gInSc6	stav
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
excitovaných	excitovaný	k2eAgInPc6d1	excitovaný
stavech	stav	k1gInPc6	stav
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
odlišné	odlišný	k2eAgFnPc4d1	odlišná
hodnoty	hodnota	k1gFnPc4	hodnota
spinu	spin	k1gInSc2	spin
a	a	k8xC	a
parity	parita	k1gFnSc2	parita
<g/>
)	)	kIx)	)
-	-	kIx~	-
takové	takový	k3xDgInPc1	takový
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
rezonance	rezonance	k1gFnPc1	rezonance
<g/>
.	.	kIx.	.
</s>
<s>
Rezonance	rezonance	k1gFnSc1	rezonance
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
za	za	k7c4	za
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
24	[number]	k4	24
s	s	k7c7	s
<g/>
)	)	kIx)	)
vlivem	vliv	k1gInSc7	vliv
silné	silný	k2eAgFnSc2d1	silná
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
rezonance	rezonance	k1gFnSc1	rezonance
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
označovány	označovat	k5eAaImNgInP	označovat
i	i	k9	i
hadrony	hadron	k1gInPc4	hadron
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
stav	stav	k1gInSc4	stav
stabilní	stabilní	k2eAgInSc4d1	stabilní
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
silné	silný	k2eAgFnSc3d1	silná
interakci	interakce	k1gFnSc3	interakce
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tetrakvarky	tetrakvarka	k1gFnSc2	tetrakvarka
a	a	k8xC	a
pentakvarky	pentakvarka	k1gFnSc2	pentakvarka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
částice	částice	k1gFnSc1	částice
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
chromodynamika	chromodynamika	k1gFnSc1	chromodynamika
</s>
