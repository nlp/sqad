<s>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
bulharská	bulharský	k2eAgFnSc1d1
osvobozenecká	osvobozenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
mezi	mezi	k7c7
Ruskou	ruský	k2eAgFnSc7d1
říší	říše	k1gFnSc7
a	a	k8xC
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>