<s desamb="1">
Válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
ruským	ruský	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
<g/>
,	,	kIx,
uzavřením	uzavření	k1gNnSc7
předběžného	předběžný	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
San	San	k1gMnSc6
Stefanu	Stefan	k1gMnSc6
<g/>
,	,	kIx,
kterým	který	k3yRgFnPc3,k3yQgFnPc3,k3yIgFnPc3
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
osvobozené	osvobozený	k2eAgNnSc1d1
novodobé	novodobý	k2eAgNnSc1d1
Velké	velký	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
hranice	hranice	k1gFnSc1
a	a	k8xC
suverenita	suverenita	k1gFnSc1
byly	být	k5eAaImAgFnP
drasticky	drasticky	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
nátlakem	nátlak	k1gInSc7
velmocí	velmoc	k1gFnPc2
během	během	k7c2
Berlínského	berlínský	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
.	.	kIx.
</s>