<p>
<s>
Folk	folk	k1gInSc1	folk
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
folk	folk	k1gInSc1	folk
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
[	[	kIx(	[
<g/>
fə	fə	k?	fə
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
folk	folk	k1gInSc1	folk
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tedy	tedy	k9	tedy
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
anglosaskou	anglosaský	k2eAgFnSc4d1	anglosaská
lidovou	lidový	k2eAgFnSc4d1	lidová
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
současné	současný	k2eAgFnSc2d1	současná
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
přesně	přesně	k6eAd1	přesně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
hudební	hudební	k2eAgFnSc4d1	hudební
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
amatérské	amatérský	k2eAgNnSc4d1	amatérské
hudební	hudební	k2eAgNnSc4d1	hudební
provozování	provozování	k1gNnSc4	provozování
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
amatérské	amatérský	k2eAgNnSc1d1	amatérské
písničkářství	písničkářství	k1gNnSc1	písničkářství
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Akademia	Akademia	k1gFnSc1	Akademia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgNnSc1d1	slovní
spojení	spojení	k1gNnSc1	spojení
folk	folk	k1gInSc1	folk
music	music	k1gMnSc1	music
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
etnická	etnický	k2eAgFnSc1d1	etnická
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
–	–	k?	–
stylizovanou	stylizovaný	k2eAgFnSc7d1	stylizovaná
nápodobou	nápodoba	k1gFnSc7	nápodoba
–	–	k?	–
folková	folkový	k2eAgFnSc1d1	folková
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
moderní	moderní	k2eAgFnSc2d1	moderní
folkové	folkový	k2eAgFnSc2d1	folková
hudby	hudba	k1gFnSc2	hudba
od	od	k7c2	od
etnické	etnický	k2eAgFnSc2d1	etnická
se	se	k3xPyFc4	se
v	v	k7c6	v
angloamerických	angloamerický	k2eAgFnPc6d1	angloamerická
zemích	zem	k1gFnPc6	zem
užívají	užívat	k5eAaImIp3nP	užívat
názvy	název	k1gInPc1	název
contemporary	contemporara	k1gFnSc2	contemporara
[	[	kIx(	[
<g/>
kə	kə	k?	kə
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
současný	současný	k2eAgInSc1d1	současný
<g/>
)	)	kIx)	)
folk	folk	k1gInSc1	folk
či	či	k8xC	či
modern	modern	k1gInSc1	modern
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgInSc1d1	moderní
<g/>
)	)	kIx)	)
folk	folk	k1gInSc1	folk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
lidovou	lidový	k2eAgFnSc4d1	lidová
hudbu	hudba	k1gFnSc4	hudba
etnickou	etnický	k2eAgFnSc7d1	etnická
užívá	užívat	k5eAaImIp3nS	užívat
spíše	spíše	k9	spíše
termín	termín	k1gInSc1	termín
folklór	folklór	k1gInSc1	folklór
(	(	kIx(	(
<g/>
laicky	laicky	k6eAd1	laicky
také	také	k9	také
"	"	kIx"	"
<g/>
česká	český	k2eAgFnSc1d1	Česká
lidovka	lidovka	k1gFnSc1	lidovka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
pojmem	pojem	k1gInSc7	pojem
folk	folk	k1gInSc1	folk
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
moderní	moderní	k2eAgFnSc1d1	moderní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
folk	folk	k1gInSc1	folk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Trampská	trampský	k2eAgFnSc1d1	trampská
tradice	tradice	k1gFnSc1	tradice
===	===	k?	===
</s>
</p>
<p>
<s>
Žánr	žánr	k1gInSc1	žánr
české	český	k2eAgFnSc2d1	Česká
folkové	folkový	k2eAgFnSc2d1	folková
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lidová	lidový	k2eAgFnSc1d1	lidová
tvorba	tvorba	k1gFnSc1	tvorba
umělých	umělý	k2eAgFnPc2d1	umělá
písní	píseň	k1gFnPc2	píseň
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
mnohaletou	mnohaletý	k2eAgFnSc4d1	mnohaletá
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
či	či	k8xC	či
moderní	moderní	k2eAgFnSc6d1	moderní
trampské	trampský	k2eAgFnSc6d1	trampská
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
<g/>
,	,	kIx,	,
osobitou	osobitý	k2eAgFnSc4d1	osobitá
i	i	k8xC	i
zcela	zcela	k6eAd1	zcela
svébytnou	svébytný	k2eAgFnSc7d1	svébytná
odrůdou	odrůda	k1gFnSc7	odrůda
českého	český	k2eAgInSc2d1	český
folku	folk	k1gInSc2	folk
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
kontextu	kontext	k1gInSc6	kontext
celé	celý	k2eAgFnSc2d1	celá
české	český	k2eAgFnSc2d1	Česká
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
skupiny	skupina	k1gFnSc2	skupina
Brontosauři	brontosaurus	k1gMnPc5	brontosaurus
<g/>
,	,	kIx,	,
Settleři	Settler	k1gMnPc5	Settler
<g/>
,	,	kIx,	,
Hop	hop	k0	hop
Trop	tropit	k5eAaImRp2nS	tropit
nebo	nebo	k8xC	nebo
Wabi	Wabi	k1gNnSc1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
či	či	k8xC	či
Jarka	Jarka	k1gFnSc1	Jarka
Mottl	Mottl	k1gMnSc1	Mottl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
trampská	trampský	k2eAgFnSc1d1	trampská
hudba	hudba	k1gFnSc1	hudba
tedy	tedy	k9	tedy
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
existovala	existovat	k5eAaImAgFnS	existovat
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
30	[number]	k4	30
let	léto	k1gNnPc2	léto
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
folku	folk	k1gInSc2	folk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ona	onen	k3xDgFnSc1	onen
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
inspirační	inspirační	k2eAgInPc4d1	inspirační
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
tradici	tradice	k1gFnSc6	tradice
kramářských	kramářský	k2eAgFnPc2d1	kramářská
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
obrozeneckého	obrozenecký	k2eAgNnSc2d1	obrozenecké
zpívání	zpívání	k1gNnSc2	zpívání
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kabaretních	kabaretní	k2eAgInPc2d1	kabaretní
kupletů	kuplet	k1gInPc2	kuplet
jakož	jakož	k8xC	jakož
i	i	k9	i
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
dělnických	dělnický	k2eAgInPc2d1	dělnický
zpěváckých	zpěvácký	k2eAgInPc2d1	zpěvácký
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
původní	původní	k2eAgFnSc2d1	původní
trampské	trampský	k2eAgFnSc2d1	trampská
písničky	písnička	k1gFnSc2	písnička
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
podobách	podoba	k1gFnPc6	podoba
přežila	přežít	k5eAaPmAgFnS	přežít
celé	celý	k2eAgNnSc4d1	celé
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tradice	tradice	k1gFnPc4	tradice
"	"	kIx"	"
<g/>
české	český	k2eAgFnPc4d1	Česká
lidovky	lidovky	k1gFnPc4	lidovky
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
přijímat	přijímat	k5eAaImF	přijímat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
klišovitě	klišovitě	k6eAd1	klišovitě
schematické	schematický	k2eAgNnSc4d1	schematické
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
folk	folk	k1gInSc1	folk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
tvrzení	tvrzení	k1gNnSc1	tvrzení
by	by	kYmCp3nS	by
nám	my	k3xPp1nPc3	my
vlastně	vlastně	k9	vlastně
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
obdobím	období	k1gNnSc7	období
neexistovala	existovat	k5eNaImAgFnS	existovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
žádná	žádný	k3yNgFnSc1	žádný
amatérská	amatérský	k2eAgFnSc1d1	amatérská
hudební	hudební	k2eAgFnSc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
vytvářená	vytvářený	k2eAgFnSc1d1	vytvářená
především	především	k9	především
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
potěchu	potěcha	k1gFnSc4	potěcha
autora	autor	k1gMnSc2	autor
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
či	či	k8xC	či
jemu	on	k3xPp3gMnSc3	on
blízkých	blízký	k2eAgFnPc2d1	blízká
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tzv.	tzv.	kA	tzv.
tradiční	tradiční	k2eAgFnSc1d1	tradiční
česká	český	k2eAgFnSc1d1	Česká
trampská	trampský	k2eAgFnSc1d1	trampská
hudba	hudba	k1gFnSc1	hudba
nutně	nutně	k6eAd1	nutně
musela	muset	k5eAaImAgFnS	muset
vyjít	vyjít	k5eAaPmF	vyjít
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zde	zde	k6eAd1	zde
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
z	z	k7c2	z
mohutných	mohutný	k2eAgFnPc2d1	mohutná
tradic	tradice	k1gFnPc2	tradice
české	český	k2eAgFnPc1d1	Česká
lidovky	lidovky	k1gFnPc1	lidovky
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
lidových	lidový	k2eAgMnPc2d1	lidový
či	či	k8xC	či
písní	píseň	k1gFnPc2	píseň
jakkoliv	jakkoliv	k6eAd1	jakkoliv
zlidovělých	zlidovělý	k2eAgFnPc2d1	zlidovělá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
folk	folk	k1gInSc1	folk
zničehonic	zničehonic	k6eAd1	zničehonic
jakoby	jakoby	k8xS	jakoby
spadl	spadnout	k5eAaPmAgMnS	spadnout
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzal	vzít	k5eAaPmAgMnS	vzít
tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zjevný	zjevný	k2eAgInSc1d1	zjevný
nesmysl	nesmysl	k1gInSc1	nesmysl
a	a	k8xC	a
lež	lež	k1gFnSc1	lež
<g/>
.	.	kIx.	.
</s>
<s>
Folk	folk	k1gInSc1	folk
samozřejmě	samozřejmě	k6eAd1	samozřejmě
existoval	existovat	k5eAaImAgInS	existovat
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
neříkalo	říkat	k5eNaImAgNnS	říkat
folk	folk	k1gInSc4	folk
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
českého	český	k2eAgInSc2d1	český
amatérského	amatérský	k2eAgInSc2d1	amatérský
muzicírování	muzicírování	k?	muzicírování
patrně	patrně	k6eAd1	patrně
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
velmi	velmi	k6eAd1	velmi
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
i	i	k8xC	i
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
.	.	kIx.	.
</s>
<s>
Venkovské	venkovský	k2eAgFnPc1d1	venkovská
kutálky	kutálka	k1gFnPc1	kutálka
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnPc1d1	lidová
cimbálové	cimbálový	k2eAgFnPc1d1	cimbálová
muziky	muzika	k1gFnPc1	muzika
<g/>
,	,	kIx,	,
hornické	hornický	k2eAgFnPc1d1	hornická
dechovky	dechovka	k1gFnPc1	dechovka
<g/>
,	,	kIx,	,
studentská	studentský	k2eAgFnSc1d1	studentská
obrozenecká	obrozenecký	k2eAgFnSc1d1	obrozenecká
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
západočeská	západočeský	k2eAgFnSc1d1	Západočeská
a	a	k8xC	a
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
dudácká	dudácký	k2eAgFnSc1d1	dudácká
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nS	tvořit
podhoubí	podhoubí	k1gNnSc4	podhoubí
českého	český	k2eAgInSc2d1	český
folku	folk	k1gInSc2	folk
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc4d1	hudební
materii	materie	k1gFnSc4	materie
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
český	český	k2eAgInSc1d1	český
folk	folk	k1gInSc1	folk
vzešel	vzejít	k5eAaPmAgInS	vzejít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
jistě	jistě	k9	jistě
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
prakticky	prakticky	k6eAd1	prakticky
téměř	téměř	k6eAd1	téměř
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
anglosaské	anglosaský	k2eAgInPc1d1	anglosaský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgNnSc1d1	české
písničkářství	písničkářství	k1gNnSc1	písničkářství
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
folku	folk	k1gInSc2	folk
ovšemže	ovšemže	k9	ovšemže
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
izolovaně	izolovaně	k6eAd1	izolovaně
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
ba	ba	k9	ba
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
je	být	k5eAaImIp3nS	být
jimi	on	k3xPp3gFnPc7	on
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
česká	český	k2eAgFnSc1d1	Česká
písničkářská	písničkářský	k2eAgFnSc1d1	písničkářská
tradice	tradice	k1gFnSc1	tradice
je	být	k5eAaImIp3nS	být
také	také	k9	také
mimořádně	mimořádně	k6eAd1	mimořádně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
i	i	k8xC	i
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
prvním	první	k4xOgInSc7	první
opravdovým	opravdový	k2eAgInSc7d1	opravdový
(	(	kIx(	(
<g/>
velkým	velký	k2eAgNnPc3d1	velké
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
též	též	k9	též
moderním	moderní	k2eAgInSc7d1	moderní
<g/>
)	)	kIx)	)
českým	český	k2eAgMnSc7d1	český
písničkářem	písničkář	k1gMnSc7	písničkář
byl	být	k5eAaImAgMnS	být
pan	pan	k1gMnSc1	pan
Karel	Karel	k1gMnSc1	Karel
Hašler	Hašler	k1gMnSc1	Hašler
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
jeho	jeho	k3xOp3gFnSc7	jeho
písní	píseň	k1gFnSc7	píseň
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
rádi	rád	k2eAgMnPc1d1	rád
zpívají	zpívat	k5eAaImIp3nP	zpívat
podnes	podnes	k6eAd1	podnes
<g/>
.	.	kIx.	.
</s>
<s>
Velkými	velký	k2eAgMnPc7d1	velký
písničkáři	písničkář	k1gMnPc7	písničkář
se	se	k3xPyFc4	se
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
stali	stát	k5eAaPmAgMnP	stát
i	i	k9	i
pánové	pán	k1gMnPc1	pán
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Werich	Werich	k1gMnSc1	Werich
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
zas	zas	k6eAd1	zas
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
30	[number]	k4	30
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
navázala	navázat	k5eAaPmAgFnS	navázat
vynikající	vynikající	k2eAgFnSc1d1	vynikající
autorská	autorský	k2eAgFnSc1d1	autorská
dvojice	dvojice	k1gFnSc1	dvojice
Suchý-Šlitr	Suchý-Šlitr	k1gInSc1	Suchý-Šlitr
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
tyto	tento	k3xDgMnPc4	tento
tvůrce	tvůrce	k1gMnPc4	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
vynikající	vynikající	k2eAgNnSc1d1	vynikající
hudební	hudební	k2eAgNnSc1d1	hudební
dědictví	dědictví	k1gNnSc1	dědictví
nám	my	k3xPp1nPc3	my
zanechali	zanechat	k5eAaPmAgMnP	zanechat
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k1gMnPc1	jiný
další	další	k2eAgFnSc2d1	další
–	–	k?	–
několik	několik	k4yIc4	několik
příkladů	příklad	k1gInPc2	příklad
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
:	:	kIx,	:
pan	pan	k1gMnSc1	pan
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
nebo	nebo	k8xC	nebo
Emil	Emil	k1gMnSc1	Emil
František	František	k1gMnSc1	František
Burian	Burian	k1gMnSc1	Burian
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Jana	Jan	k1gMnSc2	Jan
Buriana	Burian	k1gMnSc2	Burian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
písničkářství	písničkářství	k1gNnSc1	písničkářství
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
také	také	k9	také
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
vynikající	vynikající	k2eAgFnSc6d1	vynikající
textařské	textařský	k2eAgFnSc6d1	textařská
práci	práce	k1gFnSc6	práce
celé	celý	k2eAgFnSc2d1	celá
plejády	plejáda	k1gFnSc2	plejáda
výborných	výborný	k2eAgMnPc2d1	výborný
českých	český	k2eAgMnPc2d1	český
textařů	textař	k1gMnPc2	textař
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
samotné	samotný	k2eAgNnSc1d1	samotné
české	český	k2eAgNnSc1d1	české
folkové	folkový	k2eAgNnSc1d1	folkové
písničkářství	písničkářství	k1gNnSc1	písničkářství
mělo	mít	k5eAaImAgNnS	mít
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
stále	stále	k6eAd1	stále
i	i	k9	i
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
své	svůj	k3xOyFgInPc4	svůj
bohaté	bohatý	k2eAgInPc4d1	bohatý
hudební	hudební	k2eAgInPc4d1	hudební
inspirační	inspirační	k2eAgInPc4d1	inspirační
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
folkového	folkový	k2eAgInSc2d1	folkový
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Nemluvě	nemluva	k1gFnSc3	nemluva
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
české	český	k2eAgFnSc6d1	Česká
lidové	lidový	k2eAgFnSc6d1	lidová
slovesnosti	slovesnost	k1gFnSc6	slovesnost
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jistě	jistě	k9	jistě
další	další	k2eAgInSc1d1	další
bohatý	bohatý	k2eAgInSc1d1	bohatý
inspirační	inspirační	k2eAgInSc1d1	inspirační
zdroj	zdroj	k1gInSc1	zdroj
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
žánru	žánr	k1gInSc2	žánr
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
žánr	žánr	k1gInSc1	žánr
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
přirozeně	přirozeně	k6eAd1	přirozeně
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
společenskopolitické	společenskopolitický	k2eAgFnSc6d1	společenskopolitická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
poměrně	poměrně	k6eAd1	poměrně
zapeklitá	zapeklitý	k2eAgFnSc1d1	zapeklitá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
naráz	naráz	k6eAd1	naráz
zcela	zcela	k6eAd1	zcela
izolováno	izolován	k2eAgNnSc1d1	izolováno
od	od	k7c2	od
západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
svobodné	svobodný	k2eAgFnSc2d1	svobodná
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
kulturní	kulturní	k2eAgInPc4d1	kulturní
vlivy	vliv	k1gInPc4	vliv
pocházející	pocházející	k2eAgFnSc4d1	pocházející
ze	z	k7c2	z
svobodného	svobodný	k2eAgInSc2d1	svobodný
světa	svět	k1gInSc2	svět
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
pronikaly	pronikat	k5eAaImAgFnP	pronikat
velice	velice	k6eAd1	velice
nesnadno	snadno	k6eNd1	snadno
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
–	–	k?	–
slušně	slušně	k6eAd1	slušně
řečeno	řečen	k2eAgNnSc1d1	řečeno
–	–	k?	–
za	za	k7c2	za
velkého	velký	k2eAgNnSc2d1	velké
nepochopení	nepochopení	k1gNnSc2	nepochopení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
i	i	k8xC	i
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
stranických	stranický	k2eAgNnPc2d1	stranické
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
také	také	k9	také
bigbeatu	bigbeat	k1gInSc3	bigbeat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
zejména	zejména	k9	zejména
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
bouřlivých	bouřlivý	k2eAgNnPc2d1	bouřlivé
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
na	na	k7c4	na
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
ke	k	k7c3	k
krátkému	krátký	k2eAgNnSc3d1	krátké
politickému	politický	k2eAgNnSc3d1	politické
uvolnění	uvolnění	k1gNnSc3	uvolnění
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
folku	folk	k1gInSc2	folk
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
klasické	klasický	k2eAgFnSc6d1	klasická
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
podobě	podoba	k1gFnSc6	podoba
i	i	k8xC	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
budiž	budiž	k9	budiž
třeba	třeba	k6eAd1	třeba
skupina	skupina	k1gFnSc1	skupina
Spirituál	spirituál	k1gMnSc1	spirituál
kvintet	kvintet	k1gInSc1	kvintet
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
Český	český	k2eAgInSc1d1	český
skiffle	skiffle	k1gInSc1	skiffle
pana	pan	k1gMnSc2	pan
Jiřího	Jiří	k1gMnSc2	Jiří
Traxlera	Traxler	k1gMnSc2	Traxler
jr	jr	k?	jr
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
první	první	k4xOgFnSc1	první
výrazná	výrazný	k2eAgFnSc1d1	výrazná
skupina	skupina	k1gFnSc1	skupina
českých	český	k2eAgMnPc2d1	český
písničkářů	písničkář	k1gMnPc2	písničkář
sdružených	sdružený	k2eAgMnPc2d1	sdružený
ve	v	k7c6	v
sdružení	sdružení	k1gNnSc6	sdružení
Šafrán	šafrán	k1gInSc4	šafrán
<g/>
,	,	kIx,	,
snad	snad	k9	snad
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
písničkářem	písničkář	k1gMnSc7	písničkář
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
pan	pan	k1gMnSc1	pan
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
i	i	k9	i
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
zvaný	zvaný	k2eAgInSc4d1	zvaný
country	country	k2eAgInSc4d1	country
and	and	k?	and
western	western	k1gInSc4	western
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
bluegrassové	bluegrassový	k2eAgFnSc2d1	bluegrassová
odnože	odnož	k1gFnSc2	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
ono	onen	k3xDgNnSc1	onen
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
(	(	kIx(	(
<g/>
multi	multi	k1gNnSc1	multi
<g/>
)	)	kIx)	)
<g/>
žánrové	žánrový	k2eAgNnSc1d1	žánrové
zmatení	zmatení	k1gNnSc1	zmatení
z	z	k7c2	z
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgInPc4	všechen
příbuzné	příbuzný	k2eAgInPc4d1	příbuzný
hudební	hudební	k2eAgInPc4d1	hudební
žánry	žánr	k1gInPc4	žánr
v	v	k7c6	v
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
letech	léto	k1gNnPc6	léto
začaly	začít	k5eAaPmAgInP	začít
fakticky	fakticky	k6eAd1	fakticky
koexistovat	koexistovat	k5eAaImF	koexistovat
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
bývaly	bývat	k5eAaImAgInP	bývat
označovány	označovat	k5eAaImNgInP	označovat
dost	dost	k6eAd1	dost
nehezky	hezky	k6eNd1	hezky
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
portovní	portovní	k2eAgInPc4d1	portovní
žánry	žánr	k1gInPc4	žánr
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
festival	festival	k1gInSc1	festival
Porta	porta	k1gFnSc1	porta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
folku	folk	k1gInSc2	folk
==	==	k?	==
</s>
</p>
<p>
<s>
Folkovou	folkový	k2eAgFnSc4d1	folková
píseň	píseň	k1gFnSc4	píseň
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
typu	typ	k1gInSc2	typ
tedy	tedy	k9	tedy
charakterizujeme	charakterizovat	k5eAaBmIp1nP	charakterizovat
jako	jako	k8xS	jako
typ	typ	k1gInSc1	typ
moderní	moderní	k2eAgFnSc2d1	moderní
zpěvné	zpěvný	k2eAgFnSc2d1	zpěvná
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spontánně	spontánně	k6eAd1	spontánně
vznikala	vznikat	k5eAaImAgFnS	vznikat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
podmínkách	podmínka	k1gFnPc6	podmínka
jako	jako	k8xS	jako
protiklad	protiklad	k1gInSc4	protiklad
konvenčního	konvenční	k2eAgInSc2d1	konvenční
a	a	k8xC	a
zkomercionalizovaného	zkomercionalizovaný	k2eAgInSc2d1	zkomercionalizovaný
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
Folk	folk	k1gInSc1	folk
klade	klást	k5eAaImIp3nS	klást
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
myšlenky	myšlenka	k1gFnPc4	myšlenka
či	či	k8xC	či
emoce	emoce	k1gFnPc1	emoce
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
textu	text	k1gInSc6	text
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
folkové	folkový	k2eAgFnSc2d1	folková
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
nejcharakterističtějších	charakteristický	k2eAgInPc2d3	nejcharakterističtější
rysů	rys	k1gInPc2	rys
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
,	,	kIx,	,
adresně	adresně	k6eAd1	adresně
a	a	k8xC	a
obsahově	obsahově	k6eAd1	obsahově
konkrétně	konkrétně	k6eAd1	konkrétně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
stránky	stránka	k1gFnPc4	stránka
společenského	společenský	k2eAgNnSc2d1	společenské
dění	dění	k1gNnSc2	dění
či	či	k8xC	či
osobních	osobní	k2eAgInPc2d1	osobní
prožitků	prožitek	k1gInPc2	prožitek
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
folkové	folkový	k2eAgFnPc1d1	folková
písně	píseň	k1gFnPc1	píseň
interpretovány	interpretovat	k5eAaBmNgFnP	interpretovat
přímo	přímo	k6eAd1	přímo
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
osobní	osobní	k2eAgFnSc1d1	osobní
interpretace	interpretace	k1gFnSc1	interpretace
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jakožto	jakožto	k8xS	jakožto
moderní	moderní	k2eAgInSc4d1	moderní
šanson	šanson	k1gInSc4	šanson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Folkové	folkový	k2eAgFnPc1d1	folková
písně	píseň	k1gFnPc1	píseň
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
formu	forma	k1gFnSc4	forma
výrazně	výrazně	k6eAd1	výrazně
niterné	niterný	k2eAgFnSc2d1	niterná
osobní	osobní	k2eAgFnSc2d1	osobní
výpovědi	výpověď	k1gFnSc2	výpověď
o	o	k7c6	o
světě	svět	k1gInSc6	svět
okolo	okolo	k7c2	okolo
nás	my	k3xPp1nPc2	my
či	či	k8xC	či
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
autorovy	autorův	k2eAgFnSc2d1	autorova
duše	duše	k1gFnSc2	duše
či	či	k8xC	či
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
podobají	podobat	k5eAaImIp3nP	podobat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čemukoliv	cokoliv	k3yInSc3	cokoliv
jinému	jiný	k2eAgMnSc3d1	jiný
zhudebněné	zhudebněný	k2eAgFnSc3d1	zhudebněná
poezii	poezie	k1gFnSc3	poezie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
poezii	poezie	k1gFnSc4	poezie
všedního	všední	k2eAgInSc2d1	všední
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
začasté	začasté	k?	začasté
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
prostých	prostý	k2eAgFnPc6d1	prostá
věcech	věc	k1gFnPc6	věc
prostými	prostý	k2eAgFnPc7d1	prostá
slovy	slovo	k1gNnPc7	slovo
jednoduchým	jednoduchý	k2eAgNnSc7d1	jednoduché
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
sdělným	sdělný	k2eAgInSc7d1	sdělný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
folkoví	folkový	k2eAgMnPc1d1	folkový
interpreti	interpret	k1gMnPc1	interpret
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
volné	volný	k2eAgFnSc2d1	volná
tvorby	tvorba	k1gFnSc2	tvorba
zhudebňují	zhudebňovat	k5eAaImIp3nP	zhudebňovat
i	i	k9	i
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
poezii	poezie	k1gFnSc4	poezie
cizí	cizí	k2eAgFnSc4d1	cizí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
báseň	báseň	k1gFnSc1	báseň
Máj	máj	k1gFnSc1	máj
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
zhudebněná	zhudebněný	k2eAgFnSc1d1	zhudebněná
skupinou	skupina	k1gFnSc7	skupina
Český	český	k2eAgInSc1d1	český
skiffle	skiffle	k1gInSc1	skiffle
nebo	nebo	k8xC	nebo
Radůzino	Radůzino	k1gNnSc1	Radůzino
CD	CD	kA	CD
V	v	k7c6	v
salonu	salon	k1gInSc6	salon
barokních	barokní	k2eAgFnPc2d1	barokní
dam	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zhudebněna	zhudebněn	k2eAgFnSc1d1	zhudebněna
část	část	k1gFnSc1	část
jedné	jeden	k4xCgFnSc2	jeden
básně	báseň	k1gFnSc2	báseň
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Nerudy	Neruda	k1gMnSc2	Neruda
z	z	k7c2	z
Písní	píseň	k1gFnPc2	píseň
kosmických	kosmický	k2eAgFnPc2d1	kosmická
<g/>
,	,	kIx,	,
známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Skoumalovy	Skoumalův	k2eAgFnPc1d1	Skoumalova
půvabné	půvabný	k2eAgFnPc1d1	půvabná
adaptace	adaptace	k1gFnPc1	adaptace
nonsensových	nonsensový	k2eAgFnPc2d1	nonsensová
básní	báseň	k1gFnPc2	báseň
Emanuela	Emanuel	k1gMnSc2	Emanuel
Frynty	Frynta	k1gFnSc2	Frynta
na	na	k7c6	na
CD	CD	kA	CD
Kdyby	kdyby	kYmCp3nS	kdyby
prase	prase	k1gNnSc1	prase
mělo	mít	k5eAaImAgNnS	mít
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zdařilé	zdařilý	k2eAgFnPc1d1	zdařilá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Pavlicovy	Pavlicův	k2eAgFnPc1d1	Pavlicova
adaptace	adaptace	k1gFnPc1	adaptace
cizích	cizí	k2eAgInPc2d1	cizí
poetických	poetický	k2eAgInPc2d1	poetický
textů	text	k1gInPc2	text
s	s	k7c7	s
původně	původně	k6eAd1	původně
folklórní	folklórní	k2eAgFnSc7d1	folklórní
skupinou	skupina	k1gFnSc7	skupina
Hradišťan	Hradišťan	k1gMnSc1	Hradišťan
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nemusí	muset	k5eNaImIp3nS	muset
to	ten	k3xDgNnSc1	ten
nutně	nutně	k6eAd1	nutně
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
popěvky	popěvka	k1gFnPc1	popěvka
smutné	smutný	k2eAgFnPc1d1	smutná
<g/>
,	,	kIx,	,
vážné	vážný	k2eAgFnPc1d1	vážná
<g/>
,	,	kIx,	,
zachmuřené	zachmuřený	k2eAgFnPc1d1	zachmuřená
či	či	k8xC	či
niterně	niterně	k6eAd1	niterně
rozervané	rozervaný	k2eAgNnSc1d1	rozervané
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
laskavý	laskavý	k2eAgInSc4d1	laskavý
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
hezkou	hezký	k2eAgFnSc4d1	hezká
parodii	parodie	k1gFnSc4	parodie
<g/>
,	,	kIx,	,
mírnou	mírný	k2eAgFnSc4d1	mírná
perzifláž	perzifláž	k1gFnSc4	perzifláž
<g/>
,	,	kIx,	,
vkusnou	vkusný	k2eAgFnSc4d1	vkusná
legraci	legrace	k1gFnSc4	legrace
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
o	o	k7c4	o
vlídný	vlídný	k2eAgInSc4d1	vlídný
a	a	k8xC	a
laskavý	laskavý	k2eAgInSc4d1	laskavý
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
okolo	okolo	k7c2	okolo
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
o	o	k7c4	o
všední	všední	k2eAgFnSc4d1	všední
zpívanou	zpívaný	k2eAgFnSc4d1	zpívaná
poezii	poezie	k1gFnSc4	poezie
podanou	podaný	k2eAgFnSc4d1	podaná
vtipně	vtipně	k6eAd1	vtipně
<g/>
,	,	kIx,	,
vkusně	vkusně	k6eAd1	vkusně
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
šarmem	šarm	k1gInSc7	šarm
i	i	k8xC	i
osobním	osobní	k2eAgInSc7d1	osobní
nadhledem	nadhled	k1gInSc7	nadhled
autora	autor	k1gMnSc2	autor
samého	samý	k3xTgInSc2	samý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
parodická	parodický	k2eAgFnSc1d1	parodická
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Lokálka	lokálka	k1gFnSc1	lokálka
či	či	k8xC	či
recesistická	recesistický	k2eAgFnSc1d1	recesistická
skupina	skupina	k1gFnSc1	skupina
Barel	barel	k1gInSc4	barel
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Folk	folk	k1gInSc1	folk
mívá	mívat	k5eAaImIp3nS	mívat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
formu	forma	k1gFnSc4	forma
velmi	velmi	k6eAd1	velmi
upřímné	upřímný	k2eAgFnSc2d1	upřímná
a	a	k8xC	a
přímočaré	přímočarý	k2eAgFnSc2d1	přímočará
osobní	osobní	k2eAgFnSc2d1	osobní
výpovědi	výpověď	k1gFnSc2	výpověď
či	či	k8xC	či
zpovědi	zpověď	k1gFnSc2	zpověď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ventilována	ventilovat	k5eAaImNgFnS	ventilovat
lidovou	lidový	k2eAgFnSc7d1	lidová
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
obvykle	obvykle	k6eAd1	obvykle
formou	forma	k1gFnSc7	forma
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
jak	jak	k6eAd1	jak
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
radost	radost	k1gFnSc4	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
vážnou	vážný	k2eAgFnSc4d1	vážná
starost	starost	k1gFnSc4	starost
o	o	k7c4	o
stav	stav	k1gInSc4	stav
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
je	být	k5eAaImIp3nS	být
folk	folk	k1gInSc4	folk
také	také	k9	také
velice	velice	k6eAd1	velice
blízký	blízký	k2eAgInSc1d1	blízký
zejména	zejména	k6eAd1	zejména
kvalitnímu	kvalitní	k2eAgInSc3d1	kvalitní
šansonu	šanson	k1gInSc3	šanson
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
části	část	k1gFnSc3	část
tzv.	tzv.	kA	tzv.
art-rocku	artocka	k1gFnSc4	art-rocka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
hudební	hudební	k2eAgInPc1d1	hudební
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
také	také	k9	také
hodně	hodně	k6eAd1	hodně
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
zpívaného	zpívaný	k2eAgInSc2d1	zpívaný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
světových	světový	k2eAgMnPc2d1	světový
folkařů	folkař	k1gMnPc2	folkař
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kanada	Kanada	k1gFnSc1	Kanada
===	===	k?	===
</s>
</p>
<p>
<s>
Leonard	Leonard	k1gMnSc1	Leonard
Cohen	Cohna	k1gFnPc2	Cohna
</s>
</p>
<p>
<s>
Gordon	Gordon	k1gMnSc1	Gordon
Lightfoot	Lightfoot	k1gMnSc1	Lightfoot
</s>
</p>
<p>
<s>
Joni	Joni	k6eAd1	Joni
Mitchell	Mitchell	k1gInSc1	Mitchell
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
</s>
</p>
<p>
<s>
===	===	k?	===
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
===	===	k?	===
</s>
</p>
<p>
<s>
Donovan	Donovan	k1gMnSc1	Donovan
</s>
</p>
<p>
<s>
Fairport	Fairport	k1gInSc1	Fairport
Convention	Convention	k1gInSc1	Convention
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Thompson	Thompson	k1gMnSc1	Thompson
</s>
</p>
<p>
<s>
===	===	k?	===
Rusko	Rusko	k1gNnSc1	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vysockij	Vysockij	k1gMnSc1	Vysockij
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Piotr	Piotr	k1gMnSc1	Piotr
Lolek	Lolek	k1gMnSc1	Lolek
Sołoducha	Sołoducha	k1gFnSc1	Sołoducha
</s>
</p>
<p>
<s>
Joanna	Joanen	k2eAgFnSc1d1	Joanna
Słowińska	Słowińska	k1gFnSc1	Słowińska
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Grzesiuk	Grzesiuk	k1gInSc1	Grzesiuk
</s>
</p>
<p>
<s>
Jacek	Jacek	k1gMnSc1	Jacek
Ostaszewski	Ostaszewsk	k1gFnSc2	Ostaszewsk
</s>
</p>
<p>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sobczak	Sobczak	k1gInSc1	Sobczak
Kotnarowska	Kotnarowska	k1gFnSc1	Kotnarowska
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
českých	český	k2eAgMnPc2d1	český
folkařů	folkař	k1gMnPc2	folkař
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
folkové	folkový	k2eAgFnSc2d1	folková
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dorůžka	Dorůžka	k1gFnSc1	Dorůžka
<g/>
,	,	kIx,	,
Panoráma	panoráma	k1gFnSc1	panoráma
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
1918	[number]	k4	1918
<g/>
/	/	kIx~	/
<g/>
1978	[number]	k4	1978
–	–	k?	–
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
katalogové	katalogový	k2eAgNnSc1d1	Katalogové
číslo	číslo	k1gNnSc1	číslo
23-068-81	[number]	k4	23-068-81
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
PETRÁČKOVÁ	Petráčková	k1gFnSc1	Petráčková
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
A-	A-	k1gMnPc2	A-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
834	[number]	k4	834
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
607	[number]	k4	607
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zpívaná	zpívaná	k1gFnSc1	zpívaná
<g/>
,	,	kIx,	,
recitovaná	recitovaný	k2eAgFnSc1d1	recitovaná
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc1d1	experimentální
poezie	poezie	k1gFnSc1	poezie
</s>
</p>
<p>
<s>
Tramping	tramping	k1gInSc1	tramping
a	a	k8xC	a
Trampská	trampský	k2eAgFnSc1d1	trampská
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
Country	country	k2eAgFnPc1d1	country
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
</s>
</p>
<p>
<s>
Šanson	šanson	k1gInSc1	šanson
</s>
</p>
