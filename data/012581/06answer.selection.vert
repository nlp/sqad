<s>
Folk	folk	k1gInSc1	folk
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
folk	folk	k1gInSc1	folk
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
[	[	kIx(	[
<g/>
fə	fə	k?	fə
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
