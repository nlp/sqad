<s>
Nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
Neradice	Neradice	k1gFnSc2	Neradice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
na	na	k7c6	na
Biskupice	Biskupika	k1gFnSc6	Biskupika
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
pivovaru	pivovar	k1gInSc3	pivovar
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
7421	[number]	k4	7421
m	m	kA	m
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
1085	[number]	k4	1085
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
macev	macva	k1gFnPc2	macva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
stovka	stovka	k1gFnSc1	stovka
sem	sem	k6eAd1	sem
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
ze	z	k7c2	z
zdevastovaného	zdevastovaný	k2eAgInSc2d1	zdevastovaný
Starého	Starého	k2eAgInSc2d1	Starého
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
náhrobků	náhrobek	k1gInPc2	náhrobek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1601	[number]	k4	1601
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vchodu	vchod	k1gInSc2	vchod
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
"	"	kIx"	"
<g/>
eklektická	eklektický	k2eAgFnSc1d1	eklektická
<g/>
"	"	kIx"	"
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
pamětní	pamětní	k2eAgFnPc1d1	pamětní
desky	deska	k1gFnPc1	deska
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
asi	asi	k9	asi
600	[number]	k4	600
obětí	oběť	k1gFnPc2	oběť
holokaustu	holokaust	k1gInSc2	holokaust
ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Uherskobrodská	uherskobrodský	k2eAgFnSc1d1	Uherskobrodská
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bývalého	bývalý	k2eAgNnSc2d1	bývalé
ghetta	ghetto	k1gNnSc2	ghetto
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
hrazeného	hrazený	k2eAgNnSc2d1	hrazené
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
stará	starat	k5eAaImIp3nS	starat
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
synagoga	synagoga	k1gFnSc1	synagoga
i	i	k8xC	i
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
modlitebna	modlitebna	k1gFnSc1	modlitebna
v	v	k7c6	v
ghettu	ghetto	k1gNnSc6	ghetto
byly	být	k5eAaImAgFnP	být
zbořeny	zbořen	k2eAgMnPc4d1	zbořen
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
Starý	starý	k2eAgInSc1d1	starý
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
Seznam	seznam	k1gInSc1	seznam
židovských	židovský	k2eAgFnPc2d1	židovská
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
Seznam	seznam	k1gInSc4	seznam
židovských	židovský	k2eAgInPc2d1	židovský
hřbitovů	hřbitov	k1gInPc2	hřbitov
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Židovský	židovský	k2eAgInSc1d1	židovský
způsob	způsob	k1gInSc1	způsob
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Hřbitov	hřbitov	k1gInSc4	hřbitov
na	na	k7c4	na
www.holocaust.cz	www.holocaust.cz	k1gInSc4	www.holocaust.cz
Židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
</s>
