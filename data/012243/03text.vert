<p>
<s>
Kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Charadrius	Charadrius	k1gMnSc1	Charadrius
morinellus	morinellus	k1gMnSc1	morinellus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
bahňák	bahňák	k1gInSc1	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kulíkovitých	kulíkovitý	k2eAgMnPc2d1	kulíkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
šatu	šat	k1gInSc6	šat
má	mít	k5eAaImIp3nS	mít
rezavá	rezavý	k2eAgNnPc4d1	rezavé
prsa	prsa	k1gNnPc4	prsa
a	a	k8xC	a
černou	černý	k2eAgFnSc4d1	černá
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
šedou	šedý	k2eAgFnSc4d1	šedá
hruď	hruď	k1gFnSc4	hruď
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgNnSc1d1	tmavé
temeno	temeno	k1gNnSc1	temeno
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc1d1	bílé
líce	líce	k1gNnSc1	líce
a	a	k8xC	a
nadoční	nadoční	k2eAgInSc1d1	nadoční
proužek	proužek	k1gInSc1	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
výrazněji	výrazně	k6eAd2	výrazně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
lyskonohů	lyskonoh	k1gInPc2	lyskonoh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
hnědaví	hnědavý	k2eAgMnPc1d1	hnědavý
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznější	výrazný	k2eAgFnSc7d2	výraznější
kresbou	kresba	k1gFnSc7	kresba
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
ve	v	k7c6	v
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
polohách	poloha	k1gFnPc6	poloha
a	a	k8xC	a
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
tundře	tundra	k1gFnSc6	tundra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
relativně	relativně	k6eAd1	relativně
početně	početně	k6eAd1	početně
na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgInSc1d1	poslední
doklad	doklad	k1gInSc1	doklad
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
občasná	občasný	k2eAgNnPc4d1	občasné
pozorování	pozorování	k1gNnPc4	pozorování
bylo	být	k5eAaImAgNnS	být
další	další	k2eAgNnSc1d1	další
hnízdění	hnízdění	k1gNnSc1	hnízdění
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
byli	být	k5eAaImAgMnP	být
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
ptáci	pták	k1gMnPc1	pták
nebo	nebo	k8xC	nebo
páry	pára	k1gFnPc1	pára
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
hnízdění	hnízdění	k1gNnSc4	hnízdění
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
hnízdili	hnízdit	k5eAaImAgMnP	hnízdit
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
řada	řada	k1gFnSc1	řada
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
ojedinělá	ojedinělý	k2eAgNnPc4d1	ojedinělé
předpokládaná	předpokládaný	k2eAgNnPc4d1	předpokládané
hnízdění	hnízdění	k1gNnPc4	hnízdění
je	být	k5eAaImIp3nS	být
Hrubý	hrubý	k2eAgInSc1d1	hrubý
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Charadrius	Charadrius	k1gInSc1	Charadrius
morinellus	morinellus	k1gInSc1	morinellus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
