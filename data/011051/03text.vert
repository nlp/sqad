<p>
<s>
Foghat	Foghat	k5eAaBmF	Foghat
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
nejslavnější	slavný	k2eAgFnSc4d3	nejslavnější
éru	éra	k1gFnSc4	éra
si	se	k3xPyFc3	se
prožila	prožít	k5eAaPmAgFnS	prožít
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
hraje	hrát	k5eAaImIp3nS	hrát
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
blues-rock	bluesock	k1gInSc1	blues-rock
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
boogie	boogie	k1gFnSc1	boogie
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hudebními	hudební	k2eAgInPc7d1	hudební
nástroji	nástroj	k1gInPc7	nástroj
dominují	dominovat	k5eAaImIp3nP	dominovat
elektrické	elektrický	k2eAgFnPc4d1	elektrická
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
elektrická	elektrický	k2eAgFnSc1d1	elektrická
slidová	slidový	k2eAgFnSc1d1	slidový
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
pět	pět	k4xCc1	pět
zlatých	zlatá	k1gFnPc2	zlatá
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
během	běh	k1gInSc7	běh
rozběhlé	rozběhlý	k2eAgNnSc4d1	rozběhlé
disco	disco	k1gNnSc4	disco
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
start	start	k1gInSc1	start
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
-	-	kIx~	-
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Kapelu	kapela	k1gFnSc4	kapela
založili	založit	k5eAaPmAgMnP	založit
Dave	Dav	k1gInSc5	Dav
(	(	kIx(	(
<g/>
Lonesome	Lonesom	k1gInSc5	Lonesom
<g/>
)	)	kIx)	)
Peverett	Peverett	k2eAgInSc1d1	Peverett
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Stevens	Stevens	k1gInSc1	Stevens
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Roger	Roger	k1gMnSc1	Roger
Earl	earl	k1gMnSc1	earl
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Savoy	Savoa	k1gFnSc2	Savoa
Brown	Browna	k1gFnPc2	Browna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
přidal	přidat	k5eAaPmAgMnS	přidat
slidekytarista	slidekytarista	k1gMnSc1	slidekytarista
Rod	rod	k1gInSc4	rod
Price	Priec	k1gInSc2	Priec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydali	vydat	k5eAaPmAgMnP	vydat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Foghead	Foghead	k1gInSc1	Foghead
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
přezpívali	přezpívat	k5eAaPmAgMnP	přezpívat
hit	hit	k1gInSc4	hit
Willieho	Willie	k1gMnSc4	Willie
Dixona	Dixon	k1gMnSc4	Dixon
"	"	kIx"	"
<g/>
I	i	k9	i
Just	just	k6eAd1	just
Want	Want	k2eAgMnSc1d1	Want
to	ten	k3xDgNnSc4	ten
Make	Make	k1gNnSc4	Make
Love	lov	k1gInSc5	lov
to	ten	k3xDgNnSc1	ten
You	You	k1gFnPc1	You
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc4	Roll
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
ocenění	ocenění	k1gNnSc1	ocenění
zlatá	zlatá	k1gFnSc1	zlatá
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
následují	následovat	k5eAaImIp3nP	následovat
zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
Energized	Energized	k1gInSc1	Energized
a	a	k8xC	a
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Outlaws	Outlawsa	k1gFnPc2	Outlawsa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
však	však	k9	však
na	na	k7c4	na
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
s	s	k7c7	s
názvem	název	k1gInSc7	název
Fool	Foolum	k1gNnPc2	Foolum
for	forum	k1gNnPc2	forum
the	the	k?	the
City	city	k1gNnSc1	city
získalo	získat	k5eAaPmAgNnS	získat
platinové	platinový	k2eAgNnSc1d1	platinové
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Stevens	Stevens	k6eAd1	Stevens
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
skupinu	skupina	k1gFnSc4	skupina
opustit	opustit	k5eAaPmF	opustit
kvůli	kvůli	k7c3	kvůli
nekonečnému	konečný	k2eNgNnSc3d1	nekonečné
cestování	cestování	k1gNnSc3	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Nick	Nick	k1gMnSc1	Nick
Jameson	Jameson	k1gMnSc1	Jameson
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
posléze	posléze	k6eAd1	posléze
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Craig	Craig	k1gMnSc1	Craig
MacGregor	MacGregor	k1gMnSc1	MacGregor
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
postupně	postupně	k6eAd1	postupně
vydala	vydat	k5eAaPmAgFnS	vydat
tři	tři	k4xCgNnPc4	tři
úspěšná	úspěšný	k2eAgNnPc4d1	úspěšné
alba	album	k1gNnPc4	album
Night	Nighta	k1gFnPc2	Nighta
Shift	Shift	kA	Shift
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Foghat	Foghat	k1gMnSc2	Foghat
Live	Liv	k1gMnSc2	Liv
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
a	a	k8xC	a
Stone	ston	k1gInSc5	ston
Blue	Blue	k1gNnPc3	Blue
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
dostali	dostat	k5eAaPmAgMnP	dostat
ocenění	ocenění	k1gNnSc2	ocenění
zlatou	zlatý	k2eAgFnSc7d1	zlatá
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
velký	velký	k2eAgInSc1d1	velký
hit	hit	k1gInSc1	hit
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
albu	album	k1gNnSc6	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Slow	Slow	k1gFnSc1	Slow
Ride	Rid	k1gFnSc2	Rid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
vyprodalo	vyprodat	k5eAaPmAgNnS	vyprodat
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Drivin	Drivina	k1gFnPc2	Drivina
<g/>
'	'	kIx"	'
Wheel	Wheel	k1gInSc1	Wheel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Blue	Bluus	k1gMnSc5	Bluus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
Third	Third	k1gInSc1	Third
Time	Tim	k1gFnSc2	Tim
Lucky	Lucka	k1gFnSc2	Lucka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
First	First	k1gFnSc1	First
Time	Time	k1gFnSc1	Time
I	i	k8xC	i
Was	Was	k1gMnSc1	Was
a	a	k8xC	a
Fool	Fool	k1gMnSc1	Fool
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc1	změna
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
drobný	drobný	k2eAgInSc1d1	drobný
úpadek	úpadek	k1gInSc1	úpadek
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
-	-	kIx~	-
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Price	Price	k1gFnSc2	Price
však	však	k9	však
přestával	přestávat	k5eAaImAgMnS	přestávat
zvládat	zvládat	k5eAaImF	zvládat
nápor	nápor	k1gInSc4	nápor
neustálého	neustálý	k2eAgNnSc2d1	neustálé
cestování	cestování	k1gNnSc2	cestování
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbil	líbit	k5eNaImAgInS	líbit
odsun	odsun	k1gInSc1	odsun
od	od	k7c2	od
klasického	klasický	k2eAgInSc2d1	klasický
boogie	boogie	k1gFnSc2	boogie
stylu	styl	k1gInSc2	styl
ke	k	k7c3	k
stylům	styl	k1gInPc3	styl
New	New	k1gFnSc2	New
Wave	Wave	k1gNnSc2	Wave
a	a	k8xC	a
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
přišel	přijít	k5eAaPmAgInS	přijít
Eric	Eric	k1gInSc1	Eric
Cartwright	Cartwright	k2eAgInSc1d1	Cartwright
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
vydaná	vydaný	k2eAgFnSc1d1	vydaná
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
už	už	k9	už
nikdy	nikdy	k6eAd1	nikdy
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
takové	takový	k3xDgFnPc4	takový
prodávanosti	prodávanost	k1gFnPc4	prodávanost
jako	jako	k8xS	jako
alba	album	k1gNnPc1	album
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgInS	opustit
MacGregor	MacGregor	k1gInSc1	MacGregor
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Nick	Nick	k1gMnSc1	Nick
Jameson	Jameson	k1gMnSc1	Jameson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
nahrál	nahrát	k5eAaPmAgMnS	nahrát
alba	album	k1gNnPc4	album
In	In	k1gMnPc2	In
the	the	k?	the
Mood	Moodo	k1gNnPc2	Moodo
for	forum	k1gNnPc2	forum
Something	Something	k1gInSc1	Something
Rude	Rude	k1gFnSc7	Rude
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
a	a	k8xC	a
Zig-Zag	Zig-Zag	k1gMnSc1	Zig-Zag
Walk	Walk	k1gMnSc1	Walk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Dave	Dav	k1gInSc5	Dav
Peverett	Peverettum	k1gNnPc2	Peverettum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
nakrátko	nakrátko	k6eAd1	nakrátko
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativy	iniciativa	k1gFnPc1	iniciativa
se	se	k3xPyFc4	se
však	však	k9	však
chopil	chopit	k5eAaPmAgMnS	chopit
Roger	Roger	k1gMnSc1	Roger
Earl	earl	k1gMnSc1	earl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
MacGregorem	MacGregor	k1gMnSc7	MacGregor
a	a	k8xC	a
Cartwrightem	Cartwright	k1gMnSc7	Cartwright
obnovili	obnovit	k5eAaPmAgMnP	obnovit
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
přibrali	přibrat	k5eAaPmAgMnP	přibrat
nového	nový	k2eAgMnSc4d1	nový
zpěváka	zpěvák	k1gMnSc4	zpěvák
Erica	Eric	k1gInSc2	Eric
Burgesona	Burgesona	k1gFnSc1	Burgesona
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
nadále	nadále	k6eAd1	nadále
cestovala	cestovat	k5eAaImAgFnS	cestovat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
několik	několik	k4yIc1	několik
baskytaristů	baskytarista	k1gMnPc2	baskytarista
bratr	bratr	k1gMnSc1	bratr
Erica	Erica	k1gMnSc1	Erica
Cartwrighta	Cartwrighta	k1gMnSc1	Cartwrighta
Brett	Brett	k1gMnSc1	Brett
či	či	k8xC	či
Jeff	Jeff	k1gMnSc1	Jeff
Howell	Howell	k1gMnSc1	Howell
<g/>
.	.	kIx.	.
</s>
<s>
Burgesona	Burgesona	k1gFnSc1	Burgesona
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
leet	leet	k1gInSc1	leet
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
nejprve	nejprve	k6eAd1	nejprve
Phil	Phil	k1gMnSc1	Phil
Nudelman	Nudelman	k1gMnSc1	Nudelman
a	a	k8xC	a
poté	poté	k6eAd1	poté
Billy	Bill	k1gMnPc4	Bill
Davis	Davis	k1gInSc4	Davis
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
baskytarista	baskytarista	k1gMnSc1	baskytarista
Dave	Dav	k1gInSc5	Dav
Crigger	Crigger	k1gMnSc1	Crigger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Peverettův	Peverettův	k2eAgInSc4d1	Peverettův
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
-	-	kIx~	-
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Dave	Dav	k1gInSc5	Dav
Peverett	Peverettum	k1gNnPc2	Peverettum
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
však	však	k9	však
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
"	"	kIx"	"
<g/>
Lonesome	Lonesom	k1gInSc5	Lonesom
Dave	Dav	k1gInSc5	Dav
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Foghat	Foghat	k1gMnSc7	Foghat
<g/>
"	"	kIx"	"
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Bryanem	Bryan	k1gMnSc7	Bryan
Bassettem	Bassett	k1gMnSc7	Bassett
<g/>
,	,	kIx,	,
Stephenem	Stephen	k1gMnSc7	Stephen
Deesem	Dees	k1gMnSc7	Dees
a	a	k8xC	a
Eddiem	Eddius	k1gMnSc7	Eddius
Zynem	Zyn	k1gMnSc7	Zyn
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
hráli	hrát	k5eAaImAgMnP	hrát
také	také	k9	také
s	s	k7c7	s
duem	duo	k1gNnSc7	duo
Hall	Hall	k1gInSc1	Hall
&	&	k?	&
Oates	Oates	k1gInSc1	Oates
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
přilákal	přilákat	k5eAaPmAgMnS	přilákat
Deese	Dees	k1gMnSc2	Dees
baskytarista	baskytarista	k1gMnSc1	baskytarista
Rift	Rift	k1gMnSc1	Rift
West	West	k1gMnSc1	West
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Molly	Molla	k1gFnSc2	Molla
Hatchet	Hatcheta	k1gFnPc2	Hatcheta
a	a	k8xC	a
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
jako	jako	k9	jako
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
host	host	k1gMnSc1	host
objevil	objevit	k5eAaPmAgMnS	objevit
Rod	rod	k1gInSc4	rod
Price	Priec	k1gInSc2	Priec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Producent	producent	k1gMnSc1	producent
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
obnovení	obnovení	k1gNnSc4	obnovení
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Rubin	Rubin	k1gMnSc1	Rubin
dále	daleko	k6eAd2	daleko
nemohl	moct	k5eNaImAgMnS	moct
podporovat	podporovat	k5eAaImF	podporovat
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
od	od	k7c2	od
země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Return	Return	k1gInSc4	Return
of	of	k?	of
the	the	k?	the
Boogie	Boogie	k1gFnSc1	Boogie
Men	Men	k1gFnSc1	Men
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
následovala	následovat	k5eAaImAgFnS	následovat
dvě	dva	k4xCgNnPc4	dva
živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
Road	Road	k1gMnSc1	Road
Cases	Cases	k1gMnSc1	Cases
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
King	King	k1gMnSc1	King
Biscuit	Biscuit	k1gMnSc1	Biscuit
Flower	Flower	k1gMnSc1	Flower
Hour	Hour	k1gMnSc1	Hour
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Rob	roba	k1gFnPc2	roba
Price	Price	k1gMnSc1	Price
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Bryan	Bryan	k1gMnSc1	Bryan
Bassett	Bassett	k1gMnSc1	Bassett
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
hrál	hrát	k5eAaImAgMnS	hrát
u	u	k7c2	u
Molly	Molla	k1gFnSc2	Molla
Hatchet	Hatcheta	k1gFnPc2	Hatcheta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Černé	Černé	k2eAgNnSc4d1	Černé
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
nové	nový	k2eAgNnSc1d1	nové
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
nové	nový	k2eAgFnSc2d1	nová
dekády	dekáda	k1gFnSc2	dekáda
přinesl	přinést	k5eAaPmAgInS	přinést
velkou	velký	k2eAgFnSc4d1	velká
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2000	[number]	k4	2000
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
56	[number]	k4	56
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Dave	Dav	k1gInSc5	Dav
Peverett	Peverettum	k1gNnPc2	Peverettum
na	na	k7c4	na
důsledky	důsledek	k1gInPc4	důsledek
rakoviny	rakovina	k1gFnSc2	rakovina
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
zemřel	zemřít	k5eAaPmAgMnS	zemřít
i	i	k9	i
druhý	druhý	k4xOgMnSc1	druhý
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Rod	rod	k1gInSc4	rod
Price	Priec	k1gInSc2	Priec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
57	[number]	k4	57
let	léto	k1gNnPc2	léto
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
zástavu	zástava	k1gFnSc4	zástava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Earl	earl	k1gMnSc1	earl
dlouho	dlouho	k6eAd1	dlouho
nemohl	moct	k5eNaImAgMnS	moct
najít	najít	k5eAaPmF	najít
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c2	za
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
až	až	k9	až
konečně	konečně	k6eAd1	konečně
vybral	vybrat	k5eAaPmAgMnS	vybrat
Charlieho	Charlie	k1gMnSc4	Charlie
Huhna	Huhno	k1gNnSc2	Huhno
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydali	vydat	k5eAaPmAgMnP	vydat
živé	živý	k2eAgNnSc4d1	živé
DVD	DVD	kA	DVD
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Official	Official	k1gMnSc1	Official
Bootleg	Bootleg	k1gMnSc1	Bootleg
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
Tonyho	Tony	k1gMnSc2	Tony
Stevense	Stevens	k1gMnSc2	Stevens
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
znovu	znovu	k6eAd1	znovu
Craig	Craig	k1gMnSc1	Craig
MacGregor	MacGregor	k1gMnSc1	MacGregor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgMnPc4d1	další
živé	živý	k1gMnPc4	živý
cd	cd	kA	cd
s	s	k7c7	s
názvem	název	k1gInSc7	název
FOGHAT	FOGHAT	kA	FOGHAT
LIVE	LIVE	kA	LIVE
II	II	kA	II
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
nejprodávanějšího	prodávaný	k2eAgNnSc2d3	nejprodávanější
alba	album	k1gNnSc2	album
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Foghat	Foghat	k1gFnSc1	Foghat
Live	Live	k1gFnSc1	Live
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
získává	získávat	k5eAaImIp3nS	získávat
novou	nový	k2eAgFnSc4d1	nová
vlnu	vlna	k1gFnSc4	vlna
fanoušků	fanoušek	k1gMnPc2	fanoušek
např.	např.	kA	např.
vystoupením	vystoupení	k1gNnPc3	vystoupení
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
videohrách	videohra	k1gFnPc6	videohra
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Guitar	Guitar	k1gMnSc1	Guitar
Hero	Hero	k1gMnSc1	Hero
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nejnovější	nový	k2eAgFnSc1d3	nejnovější
deska	deska	k1gFnSc1	deska
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Last	Last	k2eAgInSc1d1	Last
Train	Train	k2eAgInSc1d1	Train
Home	Home	k1gInSc1	Home
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
takovém	takový	k3xDgNnSc6	takový
albu	album	k1gNnSc6	album
snili	snít	k5eAaImAgMnP	snít
společně	společně	k6eAd1	společně
Roger	Roger	k1gMnSc1	Roger
Earl	earl	k1gMnSc1	earl
s	s	k7c7	s
Davem	Dav	k1gInSc7	Dav
Peverettem	Peverett	k1gMnSc7	Peverett
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nahrávky	nahrávka	k1gFnPc4	nahrávka
jejich	jejich	k3xOp3gFnPc2	jejich
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
bluesových	bluesový	k2eAgFnPc2d1	bluesová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
vlastních	vlastní	k2eAgInPc2d1	vlastní
hitů	hit	k1gInPc2	hit
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
songy	song	k1gInPc4	song
od	od	k7c2	od
speciálního	speciální	k2eAgMnSc2d1	speciální
hosta	host	k1gMnSc2	host
Eddieho	Eddie	k1gMnSc2	Eddie
Kirklanda	Kirklanda	k1gFnSc1	Kirklanda
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
nikdy	nikdy	k6eAd1	nikdy
nepřestala	přestat	k5eNaPmAgFnS	přestat
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Peverettovy	Peverettův	k2eAgFnSc2d1	Peverettův
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
hraje	hrát	k5eAaImIp3nS	hrát
skupina	skupina	k1gFnSc1	skupina
de	de	k?	de
facto	facto	k1gNnSc1	facto
ve	v	k7c6	v
stálém	stálý	k2eAgNnSc6d1	stálé
složení	složení	k1gNnSc6	složení
Roger	Roger	k1gMnSc1	Roger
Earl	earl	k1gMnSc1	earl
<g/>
,	,	kIx,	,
Craig	Craig	k1gMnSc1	Craig
MacGregor	MacGregor	k1gMnSc1	MacGregor
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Huhn	Huhn	k1gMnSc1	Huhn
a	a	k8xC	a
Bryan	Bryan	k1gMnSc1	Bryan
Bassett	Bassett	k1gMnSc1	Bassett
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Bobby	Bobba	k1gFnSc2	Bobba
Rondinelli	Rondinell	k1gMnPc1	Rondinell
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
Rainbow	Rainbow	k1gFnPc2	Rainbow
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
Earla	earl	k1gMnSc2	earl
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
a	a	k8xC	a
poté	poté	k6eAd1	poté
Jeff	Jeff	k1gMnSc1	Jeff
Howell	Howell	k1gMnSc1	Howell
nahradil	nahradit	k5eAaPmAgMnS	nahradit
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
MacGregora	MacGregor	k1gMnSc2	MacGregor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
-	-	kIx~	-
Foghat	Foghat	k1gFnSc1	Foghat
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
Foghat	Foghat	k1gFnSc1	Foghat
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
-	-	kIx~	-
Energized	Energized	k1gInSc1	Energized
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Outlaws	Outlaws	k1gInSc1	Outlaws
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
-	-	kIx~	-
Fool	Foola	k1gFnPc2	Foola
for	forum	k1gNnPc2	forum
the	the	k?	the
City	City	k1gFnSc2	City
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
-	-	kIx~	-
Night	Night	k1gInSc1	Night
Shift	Shift	kA	Shift
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
Foghat	Foghat	k1gFnSc1	Foghat
Live	Live	k1gFnSc1	Live
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
Stone	ston	k1gInSc5	ston
Blue	Blu	k1gFnSc2	Blu
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
-	-	kIx~	-
Boogie	Boogie	k1gFnSc1	Boogie
Motel	motel	k1gInSc1	motel
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
Tight	Tight	k2eAgInSc1d1	Tight
Shoes	Shoes	k1gInSc1	Shoes
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Girls	girl	k1gFnPc2	girl
to	ten	k3xDgNnSc1	ten
Chat	chata	k1gFnPc2	chata
and	and	k?	and
Boys	boy	k1gMnPc2	boy
to	ten	k3xDgNnSc1	ten
Bounce	Bounka	k1gFnSc6	Bounka
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
-	-	kIx~	-
In	In	k1gMnPc2	In
the	the	k?	the
Mood	Moodo	k1gNnPc2	Moodo
for	forum	k1gNnPc2	forum
Something	Something	k1gInSc1	Something
Rude	Rude	k1gNnPc6	Rude
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
-	-	kIx~	-
Zig-Zag	Zig-Zag	k1gMnSc1	Zig-Zag
Walk	Walk	k1gMnSc1	Walk
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
-	-	kIx~	-
Return	Return	k1gInSc1	Return
of	of	k?	of
the	the	k?	the
Boogie	Boogie	k1gFnSc1	Boogie
Men	Men	k1gFnSc1	Men
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
-	-	kIx~	-
Road	Road	k1gMnSc1	Road
Cases	Cases	k1gMnSc1	Cases
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
King	King	k1gMnSc1	King
Biscuit	Biscuit	k1gMnSc1	Biscuit
Flower	Flower	k1gMnSc1	Flower
Hour	Hour	k1gMnSc1	Hour
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
-	-	kIx~	-
Road	Road	k1gMnSc1	Road
Cases	Cases	k1gMnSc1	Cases
Version	Version	k1gInSc4	Version
2	[number]	k4	2
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Decades	Decades	k1gInSc1	Decades
Live	Live	k1gNnSc1	Live
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Family	Famila	k1gFnSc2	Famila
Joules	Joules	k1gInSc1	Joules
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
II	II	kA	II
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Last	Last	k2eAgInSc1d1	Last
Train	Train	k2eAgInSc1d1	Train
Home	Home	k1gInSc1	Home
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Under	Under	k1gInSc1	Under
the	the	k?	the
Influence	influence	k1gFnSc2	influence
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Foghat	Foghat	k1gFnSc2	Foghat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
