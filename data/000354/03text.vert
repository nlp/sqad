<s>
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Góry	Gór	k2eAgFnPc4d1	Góra
Orlickie	Orlickie	k1gFnPc4	Orlickie
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Adlergebirge	Adlergebirge	k1gFnSc1	Adlergebirge
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
celek	celek	k1gInSc4	celek
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
Kladskem	Kladsko	k1gNnSc7	Kladsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Velká	velký	k2eAgFnSc1d1	velká
Deštná	deštný	k2eAgFnSc1d1	Deštná
<g/>
,	,	kIx,	,
1115	[number]	k4	1115
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Název	název	k1gInSc1	název
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tudy	tudy	k6eAd1	tudy
po	po	k7c6	po
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
hranici	hranice	k1gFnSc6	hranice
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Divoká	divoký	k2eAgFnSc1d1	divoká
Orlice	Orlice	k1gFnSc1	Orlice
na	na	k7c6	na
východě	východ	k1gInSc6	východ
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Orlické	orlický	k2eAgFnPc4d1	Orlická
hory	hora	k1gFnPc4	hora
od	od	k7c2	od
Bystřických	Bystřická	k1gFnPc2	Bystřická
(	(	kIx(	(
<g/>
Góry	Gór	k2eAgFnPc1d1	Góra
Bystrzyckie	Bystrzyckie	k1gFnPc1	Bystrzyckie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
konci	konec	k1gInSc6	konec
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
přecházejí	přecházet	k5eAaImIp3nP	přecházet
ve	v	k7c4	v
Wzgórza	Wzgórz	k1gMnSc4	Wzgórz
Lewińskie	Lewińskie	k1gFnSc2	Lewińskie
a	a	k8xC	a
Góry	Góra	k1gFnSc2	Góra
Stołowe	Stołow	k1gInSc2	Stołow
<g/>
,	,	kIx,	,
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
pak	pak	k6eAd1	pak
v	v	k7c4	v
Podorlickou	podorlický	k2eAgFnSc4d1	Podorlická
pahorkatinu	pahorkatina	k1gFnSc4	pahorkatina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
Hronova	Hronov	k1gInSc2	Hronov
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
Broumovskou	broumovský	k2eAgFnSc4d1	Broumovská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Podorlická	podorlický	k2eAgFnSc1d1	Podorlická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
také	také	k9	také
lemuje	lemovat	k5eAaImIp3nS	lemovat
Orlické	orlický	k2eAgFnPc4d1	Orlická
hory	hora	k1gFnPc4	hora
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
jejich	jejich	k3xOp3gFnSc6	jejich
délce	délka	k1gFnSc6	délka
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
až	až	k9	až
k	k	k7c3	k
Tiché	Tiché	k2eAgFnSc3d1	Tiché
Orlici	Orlice	k1gFnSc3	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
část	část	k1gFnSc1	část
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
Bukovohorská	Bukovohorský	k2eAgFnSc1d1	Bukovohorská
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
Králíky	Králík	k1gMnPc4	Králík
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
částí	část	k1gFnSc7	část
Kladské	kladský	k2eAgFnSc2d1	Kladská
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
,	,	kIx,	,
za	za	k7c4	za
níž	nízce	k6eAd2	nízce
Sudety	Sudety	k1gFnPc1	Sudety
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
masívem	masív	k1gInSc7	masív
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
masivu	masiv	k1gInSc2	masiv
Krkonošsko-jesenické	krkonošskoesenický	k2eAgFnSc2d1	krkonošsko-jesenický
subprovincie	subprovincie	k1gFnSc2	subprovincie
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
členění	členění	k1gNnSc1	členění
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
hor	hora	k1gFnPc2	hora
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgFnPc4	všechen
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
hory	hora	k1gFnPc4	hora
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
nad	nad	k7c4	nad
1000	[number]	k4	1000
m	m	kA	m
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
nejsevernějším	severní	k2eAgInSc6d3	nejsevernější
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
podcelku	podcelek	k1gInSc6	podcelek
Deštenské	Deštenský	k2eAgFnPc1d1	Deštenská
hornatině	hornatina	k1gFnSc3	hornatina
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
Suchý	suchý	k2eAgInSc4d1	suchý
vrch	vrch	k1gInSc4	vrch
z	z	k7c2	z
Bukovohorské	Bukovohorský	k2eAgFnSc2d1	Bukovohorská
hornatiny	hornatina	k1gFnSc2	hornatina
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
tisícimetrové	tisícimetrový	k2eAgFnSc2d1	tisícimetrová
hranice	hranice	k1gFnSc2	hranice
pouhých	pouhý	k2eAgInPc2d1	pouhý
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
vrcholy	vrchol	k1gInPc7	vrchol
jsou	být	k5eAaImIp3nP	být
Velká	velký	k2eAgFnSc1d1	velká
Deštná	deštný	k2eAgFnSc1d1	Deštná
(	(	kIx(	(
<g/>
1115	[number]	k4	1115
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
1101	[number]	k4	1101
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Deštná	deštný	k2eAgFnSc1d1	Deštná
(	(	kIx(	(
<g/>
1090	[number]	k4	1090
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
(	(	kIx(	(
<g/>
převýšení	převýšení	k1gNnSc1	převýšení
od	od	k7c2	od
sedla	sedlo	k1gNnSc2	sedlo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
Velká	velká	k1gFnSc1	velká
Deštná	deštný	k2eAgFnSc1d1	Deštná
<g/>
,	,	kIx,	,
Vrchmezí	Vrchmeze	k1gFnSc7	Vrchmeze
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
polské	polský	k2eAgFnSc2d1	polská
části	část	k1gFnSc2	část
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tetřevec	tetřevec	k1gMnSc1	tetřevec
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seznam	seznam	k1gInSc1	seznam
tisícovek	tisícovka	k1gFnPc2	tisícovka
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
vrcholy	vrchol	k1gInPc4	vrchol
patří	patřit	k5eAaImIp3nS	patřit
Suchý	suchý	k2eAgInSc1d1	suchý
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
995	[number]	k4	995
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anenský	anenský	k2eAgInSc1d1	anenský
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
991	[number]	k4	991
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Komáří	komáří	k2eAgInSc1d1	komáří
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
991	[number]	k4	991
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Buková	bukový	k2eAgFnSc1d1	Buková
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
958	[number]	k4	958
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
vrcholy	vrchol	k1gInPc1	vrchol
jsou	být	k5eAaImIp3nP	být
uvedené	uvedený	k2eAgInPc1d1	uvedený
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Góry	Gór	k2eAgFnPc1d1	Góra
Orlickie	Orlickie	k1gFnPc1	Orlickie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
vrcholy	vrchol	k1gInPc4	vrchol
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
česko-polských	českoolský	k2eAgFnPc6d1	česko-polská
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Výšku	výška	k1gFnSc4	výška
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
tyto	tento	k3xDgInPc1	tento
3	[number]	k4	3
hory	hora	k1gFnSc2	hora
<g/>
:	:	kIx,	:
Vrchmezí	Vrchmeze	k1gFnPc2	Vrchmeze
(	(	kIx(	(
<g/>
Orlica	Orlica	k1gFnSc1	Orlica
<g/>
)	)	kIx)	)
-	-	kIx~	-
1084	[number]	k4	1084
m	m	kA	m
Nad	nad	k7c7	nad
Bukačkou	Bukačka	k1gFnSc7	Bukačka
(	(	kIx(	(
<g/>
Zielony	Zielon	k1gInPc4	Zielon
Garb	Garb	k1gInSc1	Garb
<g/>
)	)	kIx)	)
-	-	kIx~	-
1035	[number]	k4	1035
m	m	kA	m
Šerlich	Šerlich	k1gMnSc1	Šerlich
(	(	kIx(	(
<g/>
Szerlich	Szerlich	k1gMnSc1	Szerlich
<g/>
)	)	kIx)	)
-	-	kIx~	-
1025	[number]	k4	1025
m	m	kA	m
V	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
střediska	středisko	k1gNnPc1	středisko
Deštné	deštný	k2eAgFnSc2d1	Deštná
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Říčky	říčka	k1gFnPc1	říčka
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Olešnice	Olešnice	k1gFnPc1	Olešnice
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Sedloňov	Sedloňov	k1gInSc1	Sedloňov
<g/>
,	,	kIx,	,
Čenkovice	Čenkovice	k1gFnSc1	Čenkovice
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc1d1	populární
turistická	turistický	k2eAgFnSc1d1	turistická
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
chata	chata	k1gFnSc1	chata
na	na	k7c6	na
Šerlichu	Šerlich	k1gInSc6	Šerlich
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hřebeni	hřeben	k1gInSc6	hřeben
vede	vést	k5eAaImIp3nS	vést
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Celým	celý	k2eAgNnSc7d1	celé
pohořím	pohoří	k1gNnSc7	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
pás	pás	k1gInSc4	pás
opevnění	opevnění	k1gNnPc2	opevnění
budovaných	budovaný	k2eAgNnPc2d1	budované
před	před	k7c4	před
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válkou	válka	k1gFnSc7	válka
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
Československa	Československo	k1gNnSc2	Československo
před	před	k7c7	před
očekávaným	očekávaný	k2eAgInSc7d1	očekávaný
útokem	útok	k1gInSc7	útok
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejnovější	nový	k2eAgFnPc4d3	nejnovější
zajímavosti	zajímavost	k1gFnPc4	zajímavost
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Czech	Czech	k1gInSc1	Czech
Hobbiton	Hobbiton	k1gInSc1	Hobbiton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Šediviny	šedivina	k1gFnSc2	šedivina
<g/>
.	.	kIx.	.
</s>
<s>
Stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
204	[number]	k4	204
km2	km2	k4	km2
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
28.12	[number]	k4	28.12
<g/>
.1969	.1969	k4	.1969
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
CHKO	CHKO	kA	CHKO
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
i	i	k9	i
několik	několik	k4yIc4	několik
maloplošných	maloplošný	k2eAgNnPc2d1	maloplošné
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
2	[number]	k4	2
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
-	-	kIx~	-
NPR	NPR	kA	NPR
Bukačka	Bukačka	k1gFnSc1	Bukačka
a	a	k8xC	a
NPR	NPR	kA	NPR
Trčkov	Trčkov	k1gInSc4	Trčkov
6	[number]	k4	6
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
-	-	kIx~	-
mj.	mj.	kA	mj.
PP	PP	kA	PP
Kačenčina	Kačenčin	k2eAgFnSc1d1	Kačenčina
zahrádka	zahrádka	k1gFnSc1	zahrádka
12	[number]	k4	12
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
-	-	kIx~	-
mj.	mj.	kA	mj.
PR	pr	k0	pr
Rašeliniště	rašeliniště	k1gNnPc1	rašeliniště
Kačerov	Kačerov	k1gInSc1	Kačerov
Podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
Bukovohorské	Bukovohorský	k2eAgFnSc2d1	Bukovohorská
hornatiny	hornatina	k1gFnSc2	hornatina
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xS	jako
Přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
Suchý	Suchý	k1gMnSc1	Suchý
<g />
.	.	kIx.	.
</s>
<s>
vrch	vrch	k1gInSc1	vrch
-	-	kIx~	-
Buková	bukový	k2eAgFnSc1d1	Buková
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Hynkovice	Hynkovice	k1gFnSc2	Hynkovice
chráněné	chráněný	k2eAgNnSc4d1	chráněné
zimoviště	zimoviště	k1gNnSc4	zimoviště
netopýrů	netopýr	k1gMnPc2	netopýr
Dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
tvrz	tvrz	k1gFnSc1	tvrz
Bouda	bouda	k1gFnSc1	bouda
Údolí	údolí	k1gNnSc1	údolí
Tiché	Tiché	k2eAgFnSc2d1	Tiché
Orlice	Orlice	k1gFnSc2	Orlice
mezi	mezi	k7c7	mezi
Mladkovem	Mladkov	k1gInSc7	Mladkov
a	a	k8xC	a
Jablonným	Jablonné	k1gNnSc7	Jablonné
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Orlice	Orlice	k1gFnSc1	Orlice
Oblast	oblast	k1gFnSc1	oblast
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xS	jako
Přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
Údolí	údolí	k1gNnSc2	údolí
Rokytenky	Rokytenka	k1gFnSc2	Rokytenka
a	a	k8xC	a
Hvězdné	hvězdný	k2eAgNnSc1d1	Hvězdné
<g />
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Bukovohorské	Bukovohorský	k2eAgFnSc2d1	Bukovohorská
hornatiny	hornatina	k1gFnSc2	hornatina
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ptačí	ptačí	k2eAgFnSc2d1	ptačí
oblasti	oblast	k1gFnSc2	oblast
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
luční	luční	k2eAgFnSc1d1	luční
enkláva	enkláva	k1gFnSc1	enkláva
Orlickozáhorské	orlickozáhorský	k2eAgFnSc2d1	orlickozáhorský
brázdy	brázda	k1gFnSc2	brázda
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xS	jako
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
oblast	oblast	k1gFnSc1	oblast
Orlické	orlický	k2eAgFnSc2d1	Orlická
Záhoří	Záhoří	k1gNnSc4	Záhoří
Mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
velkoplošných	velkoplošný	k2eAgNnPc2d1	velkoplošné
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgNnPc4	dva
maloplošná	maloplošný	k2eAgNnPc4d1	maloplošné
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
<g/>
:	:	kIx,	:
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Čenkovička	Čenkovička	k1gFnSc1	Čenkovička
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Údolí	údolí	k1gNnSc2	údolí
Záhorského	záhorský	k2eAgInSc2d1	záhorský
potoka	potok	k1gInSc2	potok
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnPc4	video
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránky	stránka	k1gFnSc2	stránka
Správy	správa	k1gFnSc2	správa
CHKO	CHKO	kA	CHKO
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
MojeOrlickehory	MojeOrlickehora	k1gFnSc2	MojeOrlickehora
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
oficiální	oficiální	k2eAgInSc1d1	oficiální
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
destinace	destinace	k1gFnSc2	destinace
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Podorlicko	Podorlicko	k1gNnSc1	Podorlicko
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
Destinační	destinační	k2eAgFnSc7d1	destinační
společností	společnost	k1gFnSc7	společnost
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Podorlicko	Podorlicko	k1gNnSc1	Podorlicko
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Orlické	orlický	k2eAgFnPc4d1	Orlická
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
Region	region	k1gInSc1	region
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
-	-	kIx~	-
kompletní	kompletní	k2eAgInSc1d1	kompletní
turistický	turistický	k2eAgInSc1d1	turistický
servis	servis	k1gInSc1	servis
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
úhlů	úhel	k1gInPc2	úhel
pohledu	pohled	k1gInSc2	pohled
</s>
