<s>
Žižkovský	žižkovský	k2eAgInSc1d1	žižkovský
vysílač	vysílač	k1gInSc1	vysílač
(	(	kIx(	(
<g/>
též	též	k9	též
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
věž	věž	k1gFnSc1	věž
či	či	k8xC	či
TVPM	TVPM	kA	TVPM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pražských	pražský	k2eAgFnPc2d1	Pražská
dominant	dominanta	k1gFnPc2	dominanta
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
(	(	kIx(	(
<g/>
216	[number]	k4	216
m	m	kA	m
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tedy	tedy	k9	tedy
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stabilní	stabilní	k2eAgInSc4d1	stabilní
bod	bod	k1gInSc4	bod
ve	v	k7c6	v
městě	město	k1gNnSc6	město
–	–	k?	–
kótu	kóta	k1gFnSc4	kóta
474	[number]	k4	474
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Žižkova	Žižkov	k1gInSc2	Žižkov
a	a	k8xC	a
Vinohrad	Vinohrady	k1gInPc2	Vinohrady
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mahlerových	Mahlerův	k2eAgInPc2d1	Mahlerův
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
