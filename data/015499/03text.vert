<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
císař	císař	k1gMnSc1
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Busta	busta	k1gFnSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
v	v	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
Glyptotéce	glyptotéka	k1gFnSc6
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
161	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
180	#num#	k4
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
Antoninus	Antoninus	k1gMnSc1
Augustus	Augustus	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
121	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
180	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Vindobona	Vindobona	k1gFnSc1
nebo	nebo	k8xC
Sirmium	Sirmium	k1gNnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Hadrianovo	Hadrianův	k2eAgNnSc1d1
mauzoleum	mauzoleum	k1gNnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Antoninus	Antoninus	k1gMnSc1
Pius	Pius	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Commodus	Commodus	k1gMnSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Faustina	Faustin	k2eAgFnSc1d1
mladší	mladý	k2eAgFnSc1d2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
13	#num#	k4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Commoda	Commod	k1gMnSc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
adoptivních	adoptivní	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
Annius	Annius	k1gMnSc1
Verus	Verus	k1gMnSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Domitia	Domitia	k1gFnSc1
Lucilla	Lucilla	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
Antoninus	Antoninus	k1gMnSc1
Augustus	Augustus	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
121	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
180	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
římským	římský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
v	v	k7c6
letech	léto	k1gNnPc6
161	#num#	k4
až	až	k9
180	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
náležel	náležet	k5eAaImAgMnS
k	k	k7c3
tzv.	tzv.	kA
adoptivním	adoptivní	k2eAgMnSc7d1
císařům	císař	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
rovněž	rovněž	k9
mezi	mezi	k7c4
význačné	význačný	k2eAgMnPc4d1
představitele	představitel	k1gMnPc4
pozdní	pozdní	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
vývoje	vývoj	k1gInSc2
filozofie	filozofie	k1gFnSc2
stoicismu	stoicismus	k1gInSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
bývá	bývat	k5eAaImIp3nS
nazýván	nazýván	k2eAgMnSc1d1
„	„	k?
<g/>
filosof	filosof	k1gMnSc1
na	na	k7c6
trůně	trůn	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
jeho	jeho	k3xOp3gFnSc3
vysoké	vysoký	k2eAgFnSc3d1
mravní	mravní	k2eAgFnSc3d1
integritě	integrita	k1gFnSc3
a	a	k8xC
výjimečným	výjimečný	k2eAgFnPc3d1
osobním	osobní	k2eAgFnPc3d1
schopnostem	schopnost	k1gFnPc3
skončilo	skončit	k5eAaPmAgNnS
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
dlouhotrvající	dlouhotrvající	k2eAgNnSc1d1
období	období	k1gNnSc1
míru	mír	k1gInSc2
a	a	k8xC
prosperity	prosperita	k1gFnSc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
stabilita	stabilita	k1gFnSc1
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
více	hodně	k6eAd2
nahlodávána	nahlodávat	k5eAaImNgFnS
jak	jak	k6eAd1
zahraničními	zahraniční	k2eAgFnPc7d1
hrozbami	hrozba	k1gFnPc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
vnitřními	vnitřní	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
nástupu	nástup	k1gInSc6
na	na	k7c4
trůn	trůn	k1gInSc4
byl	být	k5eAaImAgMnS
Marcus	Marcus	k1gMnSc1
nucen	nutit	k5eAaImNgMnS
čelit	čelit	k5eAaImF
útoku	útok	k1gInSc2
parthské	parthský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c6
východních	východní	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedením	vedení	k1gNnSc7
války	válka	k1gFnSc2
pověřil	pověřit	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
a	a	k8xC
spoluvládce	spoluvládce	k1gMnSc4
Lucia	Lucius	k1gMnSc4
Vera	Verus	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
podřízení	podřízený	k1gMnPc1
si	se	k3xPyFc3
do	do	k7c2
roku	rok	k1gInSc2
166	#num#	k4
Parthy	Partha	k1gFnSc2
podrobili	podrobit	k5eAaPmAgMnP
a	a	k8xC
ovládli	ovládnout	k5eAaPmAgMnP
Mezopotámii	Mezopotámie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
v	v	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
době	doba	k1gFnSc6
však	však	k9
barbarské	barbarský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
obývající	obývající	k2eAgFnSc4d1
Germánii	Germánie	k1gFnSc4
napadly	napadnout	k5eAaPmAgInP
a	a	k8xC
prolomily	prolomit	k5eAaPmAgInP
dunajskou	dunajský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
oslabenou	oslabený	k2eAgFnSc4d1
odesláním	odeslání	k1gNnSc7
části	část	k1gFnSc2
zdejších	zdejší	k2eAgFnPc2d1
sil	síla	k1gFnPc2
do	do	k7c2
boje	boj	k1gInSc2
s	s	k7c7
Parthy	Parth	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
zahájen	zahájen	k2eAgInSc4d1
dlouhý	dlouhý	k2eAgInSc4d1
a	a	k8xC
náročný	náročný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
známý	známý	k2eAgInSc4d1
jako	jako	k8xS,k8xC
markomanské	markomanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vyplnil	vyplnit	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
zbývajících	zbývající	k2eAgNnPc2d1
let	léto	k1gNnPc2
Markova	Markův	k2eAgNnSc2d1
vládnutí	vládnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vážných	vážný	k2eAgFnPc2d1
vnějších	vnější	k2eAgFnPc2d1
výzev	výzva	k1gFnPc2
se	se	k3xPyFc4
císař	císař	k1gMnSc1
musel	muset	k5eAaImAgMnS
potýkat	potýkat	k5eAaImF
také	také	k9
s	s	k7c7
katastrofálními	katastrofální	k2eAgInPc7d1
následky	následek	k1gInPc7
antoninovského	antoninovský	k2eAgInSc2d1
moru	mor	k1gInSc2
<g/>
,	,	kIx,
zavlečeného	zavlečený	k2eAgInSc2d1
do	do	k7c2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
vojáky	voják	k1gMnPc4
vracejícími	vracející	k2eAgMnPc7d1
se	se	k3xPyFc4
z	z	k7c2
Orientu	Orient	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
neklidem	neklid	k1gInSc7
vyvolaným	vyvolaný	k2eAgNnSc7d1
spontánním	spontánní	k2eAgNnSc7d1
pronásledováním	pronásledování	k1gNnSc7
křesťanů	křesťan	k1gMnPc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
docházelo	docházet	k5eAaImAgNnS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
175	#num#	k4
se	se	k3xPyFc4
zdárně	zdárně	k6eAd1
vypořádal	vypořádat	k5eAaPmAgMnS
se	s	k7c7
vzpourou	vzpoura	k1gFnSc7
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
provinciích	provincie	k1gFnPc6
<g/>
,	,	kIx,
vedenou	vedený	k2eAgFnSc4d1
Avidiem	Avidium	k1gNnSc7
Cassiem	Cassius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
ještě	ještě	k6eAd1
než	než	k8xS
stačil	stačit	k5eAaBmAgMnS
Germány	Germán	k1gMnPc4
zcela	zcela	k6eAd1
potlačit	potlačit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
180	#num#	k4
Marcus	Marcus	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
moc	moc	k6eAd1
nad	nad	k7c7
říší	říš	k1gFnSc7
přešla	přejít	k5eAaPmAgFnS
na	na	k7c4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
syna	syn	k1gMnSc4
Commoda	Commod	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
vojenských	vojenský	k2eAgNnPc2d1
tažení	tažení	k1gNnPc2
v	v	k7c6
letech	léto	k1gNnPc6
170	#num#	k4
až	až	k9
180	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
řecky	řecky	k6eAd1
psané	psaný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
„	„	k?
<g/>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
“	“	k?
(	(	kIx(
<g/>
Ta	ten	k3xDgFnSc1
eis	eis	k1gNnSc4
heauton	heauton	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
literární	literární	k2eAgInSc4d1
klenot	klenot	k1gInSc4
<g/>
,	,	kIx,
oslavující	oslavující	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
vlády	vláda	k1gFnSc2
jako	jako	k8xS,k8xC
služby	služba	k1gFnSc2
a	a	k8xC
povinnosti	povinnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
ceněno	cenit	k5eAaImNgNnS
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
znamenitý	znamenitý	k2eAgInSc4d1
a	a	k8xC
vytříbený	vytříbený	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
mládí	mládí	k1gNnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
121	#num#	k4
v	v	k7c6
Římě	Řím	k1gInSc6
jako	jako	k8xC,k8xS
syn	syn	k1gMnSc1
praetora	praetor	k1gMnSc2
Annia	Annium	k1gNnSc2
Vera	Verum	k1gNnSc2
a	a	k8xC
Domitie	Domitie	k1gFnSc2
Lucilly	Lucilla	k1gFnSc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnPc1d1
z	z	k7c2
bohatého	bohatý	k2eAgInSc2d1
a	a	k8xC
vznešeného	vznešený	k2eAgInSc2d1
italského	italský	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markovo	Markův	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
původně	původně	k6eAd1
znělo	znět	k5eAaImAgNnS
Marcus	Marcus	k1gInSc4
Annius	Annius	k1gMnSc1
Verus	Verus	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
Marcus	Marcus	k1gMnSc1
Annius	Annius	k1gMnSc1
Catilius	Catilius	k1gMnSc1
Severus	Severus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
praděd	praděd	k1gMnSc1
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
přesídlil	přesídlit	k5eAaPmAgInS
za	za	k7c2
Neronovy	Neronův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
z	z	k7c2
Hispánie	Hispánie	k1gFnSc2
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
domohl	domoct	k5eAaPmAgMnS
úřadu	úřad	k1gInSc3
praetora	praetor	k1gMnSc2
a	a	k8xC
posléze	posléze	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
senátu	senát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Marcus	Marcus	k1gMnSc1
Annius	Annius	k1gMnSc1
Verus	Verus	k1gMnSc1
<g/>
,	,	kIx,
Markův	Markův	k2eAgMnSc1d1
děd	děd	k1gMnSc1
<g/>
,	,	kIx,
zastával	zastávat	k5eAaImAgMnS
celkem	celkem	k6eAd1
třikrát	třikrát	k6eAd1
konsulát	konsulát	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
výjimečná	výjimečný	k2eAgFnSc1d1
pocta	pocta	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
za	za	k7c2
principátu	principát	k1gInSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
jen	jen	k9
několik	několik	k4yIc1
málo	málo	k4c1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
působil	působit	k5eAaImAgMnS
i	i	k9
jako	jako	k9
městský	městský	k2eAgMnSc1d1
prefekt	prefekt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
předčasné	předčasný	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
vyrůstal	vyrůstat	k5eAaImAgMnS
Marcus	Marcus	k1gMnSc1
v	v	k7c6
domě	dům	k1gInSc6
svého	svůj	k3xOyFgMnSc2
děda	děd	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ho	on	k3xPp3gMnSc4
adoptoval	adoptovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
raném	raný	k2eAgInSc6d1
věku	věk	k1gInSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
studiem	studio	k1gNnSc7
filosofie	filosofie	k1gFnSc2
a	a	k8xC
řecké	řecký	k2eAgFnSc2d1
a	a	k8xC
latinské	latinský	k2eAgFnSc2d1
rétoriky	rétorika	k1gFnSc2
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3,k3yQnSc3
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
s	s	k7c7
nevšedním	všední	k2eNgNnSc7d1
nadšením	nadšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
vzdělávání	vzdělávání	k1gNnSc4
dohlížela	dohlížet	k5eAaImAgFnS
řada	řada	k1gFnSc1
vynikajících	vynikající	k2eAgMnPc2d1
soudobých	soudobý	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
a	a	k8xC
myslitelů	myslitel	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Heroda	Herod	k1gMnSc2
Attika	Attika	k1gFnSc1
<g/>
,	,	kIx,
Alexandra	Alexandra	k1gFnSc1
z	z	k7c2
Kotiaeia	Kotiaeius	k1gMnSc2
<g/>
,	,	kIx,
Aelia	Aelius	k1gMnSc2
Aristeida	Aristeid	k1gMnSc2
a	a	k8xC
Marka	Marek	k1gMnSc2
Cornelia	Cornelius	k1gMnSc2
Frontona	Fronton	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
posledně	posledně	k6eAd1
zmiňovaným	zmiňovaný	k2eAgInSc7d1
udržoval	udržovat	k5eAaImAgMnS
Marcus	Marcus	k1gMnSc1
čilou	čilý	k2eAgFnSc4d1
korespondenci	korespondence	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
část	část	k1gFnSc1
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
dopisech	dopis	k1gInPc6
se	se	k3xPyFc4
budoucí	budoucí	k2eAgMnSc1d1
císař	císař	k1gMnSc1
projevuje	projevovat	k5eAaImIp3nS
jako	jako	k9
inteligentní	inteligentní	k2eAgMnSc1d1
<g/>
,	,	kIx,
vážně	vážně	k6eAd1
založený	založený	k2eAgInSc1d1
a	a	k8xC
pilný	pilný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Důstojnost	důstojnost	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
a	a	k8xC
výtečné	výtečný	k2eAgInPc1d1
povahové	povahový	k2eAgInPc1d1
rysy	rys	k1gInPc1
mladého	mladý	k2eAgMnSc2d1
Marka	Marek	k1gMnSc2
záhy	záhy	k6eAd1
upoutaly	upoutat	k5eAaPmAgFnP
pozornost	pozornost	k1gFnSc4
císaře	císař	k1gMnSc2
Hadriana	Hadrian	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ho	on	k3xPp3gMnSc4
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
pravdymilovnost	pravdymilovnost	k1gFnSc4
nazýval	nazývat	k5eAaImAgMnS
Verissimus	Verissimus	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
nejpravdivější	pravdivý	k2eAgFnSc1d3
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
již	již	k6eAd1
jako	jako	k9
osmiletého	osmiletý	k2eAgInSc2d1
ho	on	k3xPp3gMnSc4
nechal	nechat	k5eAaPmAgMnS
přijmout	přijmout	k5eAaPmF
do	do	k7c2
kněžského	kněžský	k2eAgInSc2d1
sboru	sbor	k1gInSc2
Saliů	Sali	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
136	#num#	k4
si	se	k3xPyFc3
Hadrianus	Hadrianus	k1gMnSc1
zvolil	zvolit	k5eAaPmAgMnS
Lucia	Lucius	k1gMnSc4
Aelia	Aelius	k1gMnSc4
Caesara	Caesar	k1gMnSc4
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
nástupce	nástupce	k1gMnSc4
a	a	k8xC
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byl	být	k5eAaImAgInS
Marcus	Marcus	k1gInSc1
zasnouben	zasnouben	k2eAgInSc1d1
s	s	k7c7
Ceionií	Ceionie	k1gFnSc7
Fabií	fabia	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Aelia	Aelius	k1gMnSc2
Caesara	Caesar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasnoubení	zasnoubení	k1gNnSc1
bylo	být	k5eAaImAgNnS
však	však	k9
anulováno	anulovat	k5eAaBmNgNnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
138	#num#	k4
Aelius	Aelius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
ustavil	ustavit	k5eAaPmAgMnS
Hadrianus	Hadrianus	k1gMnSc1
svým	svůj	k3xOyFgMnSc7
adoptivním	adoptivní	k2eAgMnSc7d1
synem	syn	k1gMnSc7
a	a	k8xC
následníkem	následník	k1gMnSc7
předního	přední	k2eAgMnSc2d1
senátora	senátor	k1gMnSc2
Tita	Tita	k1gFnSc1
Aurelia	Aurelia	k1gFnSc1
Antonina	Antonina	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
známého	známý	k2eAgNnSc2d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Antoninus	Antoninus	k1gMnSc1
Pius	Pius	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
manželka	manželka	k1gFnSc1
Faustina	Faustin	k2eAgFnSc1d1
starší	starý	k2eAgFnSc1d2
byla	být	k5eAaImAgFnS
Markovou	Markův	k2eAgFnSc7d1
tetou	teta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
podmínek	podmínka	k1gFnPc2
ujednání	ujednání	k1gNnSc2
s	s	k7c7
Hadrianem	Hadrian	k1gInSc7
musel	muset	k5eAaImAgMnS
Antoninus	Antoninus	k1gInSc4
na	na	k7c4
oplátku	oplátka	k1gFnSc4
adoptovat	adoptovat	k5eAaPmF
Lucia	Lucius	k1gMnSc4
Vera	Verus	k1gMnSc4
<g/>
,	,	kIx,
syna	syn	k1gMnSc4
zesnulého	zesnulý	k1gMnSc4
Lucia	Lucius	k1gMnSc4
Aelia	Aelius	k1gMnSc4
Caesara	Caesar	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
svého	svůj	k3xOyFgMnSc4
tehdy	tehdy	k6eAd1
sedmnáctiletého	sedmnáctiletý	k2eAgMnSc4d1
synovce	synovec	k1gMnSc4
Marka	Marek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Hadrianus	Hadrianus	k1gMnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
138	#num#	k4
skonal	skonat	k5eAaPmAgInS
<g/>
,	,	kIx,
odebral	odebrat	k5eAaPmAgInS
se	se	k3xPyFc4
Marcus	Marcus	k1gInSc1
do	do	k7c2
císařského	císařský	k2eAgInSc2d1
paláce	palác	k1gInSc2
za	za	k7c7
svým	svůj	k3xOyFgMnSc7
adoptivním	adoptivní	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Zapojení	zapojení	k1gNnSc1
do	do	k7c2
vládnutí	vládnutí	k1gNnSc2
a	a	k8xC
převzetí	převzetí	k1gNnSc2
moci	moc	k1gFnSc2
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
Před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
Marcus	Marcus	k1gMnSc1
chopil	chopit	k5eAaPmAgMnS
vlády	vláda	k1gFnPc4
<g/>
,	,	kIx,
uplynulo	uplynout	k5eAaPmAgNnS
téměř	téměř	k6eAd1
dvacet	dvacet	k4xCc4
tři	tři	k4xCgInPc4
let	léto	k1gNnPc2
Antoninova	Antoninův	k2eAgNnSc2d1
panování	panování	k1gNnSc2
<g/>
,	,	kIx,
během	během	k7c2
nichž	jenž	k3xRgInPc2
prošel	projít	k5eAaPmAgInS
patřičnou	patřičný	k2eAgFnSc7d1
průpravou	průprava	k1gFnSc7
umožňující	umožňující	k2eAgFnSc7d1
mu	on	k3xPp3gMnSc3
náležité	náležitý	k2eAgNnSc1d1
obeznámení	obeznámení	k1gNnSc1
s	s	k7c7
nároky	nárok	k1gInPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
na	na	k7c4
něho	on	k3xPp3gMnSc4
měla	mít	k5eAaImAgFnS
klást	klást	k5eAaImF
císařská	císařský	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
získal	získat	k5eAaPmAgMnS
bezvadné	bezvadný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
o	o	k7c4
fungování	fungování	k1gNnSc4
správních	správní	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
působení	působení	k1gNnSc2
v	v	k7c6
různých	různý	k2eAgInPc6d1
veřejných	veřejný	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
vzrůstal	vzrůstat	k5eAaImAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vliv	vliv	k1gInSc4
na	na	k7c4
chod	chod	k1gInSc4
státu	stát	k1gInSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
140	#num#	k4
<g/>
,	,	kIx,
145	#num#	k4
a	a	k8xC
161	#num#	k4
se	se	k3xPyFc4
opakovaně	opakovaně	k6eAd1
účastnil	účastnit	k5eAaImAgInS
rozhodování	rozhodování	k1gNnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
konzula	konzul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
147	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
prokonsulské	prokonsulský	k2eAgNnSc4d1
imperium	imperium	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
tribunskou	tribunský	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
(	(	kIx(
<g/>
tribunitia	tribunitia	k1gFnSc1
potestas	potestas	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
představovaly	představovat	k5eAaImAgFnP
formální	formální	k2eAgInSc4d1
základ	základ	k1gInSc4
principových	principový	k2eAgFnPc2d1
pravomocí	pravomoc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Marcus	Marcus	k1gMnSc1
měl	mít	k5eAaImAgMnS
výborný	výborný	k2eAgInSc4d1
cit	cit	k1gInSc4
pro	pro	k7c4
výběr	výběr	k1gInSc4
vhodných	vhodný	k2eAgMnPc2d1
uchazečů	uchazeč	k1gMnPc2
a	a	k8xC
kandidátů	kandidát	k1gMnPc2
na	na	k7c4
různé	různý	k2eAgInPc4d1
úřední	úřední	k2eAgInPc4d1
posty	post	k1gInPc4
<g/>
,	,	kIx,
spoléhal	spoléhat	k5eAaImAgInS
se	se	k3xPyFc4
Antoninus	Antoninus	k1gInSc1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
otázkách	otázka	k1gFnPc6
obsazení	obsazení	k1gNnSc1
administrativy	administrativa	k1gFnSc2
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
mínění	mínění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
knize	kniha	k1gFnSc6
Hovorů	hovor	k1gInPc2
k	k	k7c3
sobě	se	k3xPyFc3
líčí	líčit	k5eAaImIp3nS
Marcus	Marcus	k1gInSc4
svého	svůj	k3xOyFgMnSc2
adoptivního	adoptivní	k2eAgMnSc2d1
otce	otec	k1gMnSc2
Antonina	Antonina	k1gFnSc1
jako	jako	k8xC,k8xS
příklad	příklad	k1gInSc1
vzorného	vzorný	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
míru	míra	k1gFnSc4
respektu	respekt	k1gInSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
vůči	vůči	k7c3
němu	on	k3xPp3gNnSc3
pociťoval	pociťovat	k5eAaImAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
U	u	k7c2
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
jsem	být	k5eAaImIp1nS
vídal	vídat	k5eAaImAgMnS
vlídnost	vlídnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
neoblomné	oblomný	k2eNgNnSc1d1
setrvání	setrvání	k1gNnSc1
na	na	k7c6
každém	každý	k3xTgNnSc6
zrale	zrale	k6eAd1
uváženém	uvážený	k2eAgNnSc6d1
rozhodnutí	rozhodnutí	k1gNnSc6
<g/>
;	;	kIx,
také	také	k9
lhostejnost	lhostejnost	k1gFnSc4
k	k	k7c3
takzvaným	takzvaný	k2eAgFnPc3d1
poctám	pocta	k1gFnPc3
a	a	k8xC
zálibu	záliba	k1gFnSc4
i	i	k8xC
houževnatost	houževnatost	k1gFnSc4
v	v	k7c6
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rád	rád	k6eAd1
naslouchal	naslouchat	k5eAaImAgMnS
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
vytasili	vytasit	k5eAaPmAgMnP
s	s	k7c7
návrhem	návrh	k1gInSc7
obecně	obecně	k6eAd1
prospěšným	prospěšný	k2eAgInSc7d1
<g/>
,	,	kIx,
a	a	k8xC
každému	každý	k3xTgMnSc3
měřil	měřit	k5eAaImAgMnS
neodchylně	odchylně	k6eNd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
si	se	k3xPyFc3
zasloužil	zasloužit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
zkušenost	zkušenost	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
potřebí	potřebí	k6eAd1
otěže	otěž	k1gFnPc4
přitáhnout	přitáhnout	k5eAaPmF
a	a	k8xC
kde	kde	k6eAd1
povolit	povolit	k5eAaPmF
<g/>
.	.	kIx.
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
Odmítal	odmítat	k5eAaImAgMnS
ovace	ovace	k1gFnPc4
a	a	k8xC
všeliké	všeliký	k3yIgNnSc4
lichocení	lichocení	k1gNnSc4
<g/>
,	,	kIx,
ustavičně	ustavičně	k6eAd1
bděl	bdít	k5eAaImAgInS
nad	nad	k7c7
potřebami	potřeba	k1gFnPc7
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
omezoval	omezovat	k5eAaImAgInS
veřejná	veřejný	k2eAgNnPc4d1
vydání	vydání	k1gNnPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
Nikdo	nikdo	k3yNnSc1
o	o	k7c6
něm	on	k3xPp3gInSc6
nemohl	moct	k5eNaImAgMnS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
sofista	sofista	k1gMnSc1
<g/>
,	,	kIx,
tlachal	tlachal	k1gMnSc1
nebo	nebo	k8xC
pedant	pedant	k1gMnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
každý	každý	k3xTgMnSc1
vycítil	vycítit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zralý	zralý	k2eAgInSc1d1
a	a	k8xC
dokonalý	dokonalý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
nepřístupný	přístupný	k2eNgInSc1d1
pochlebování	pochlebování	k1gNnSc2
a	a	k8xC
schopný	schopný	k2eAgMnSc1d1
řídit	řídit	k5eAaImF
záležitosti	záležitost	k1gFnPc4
svoje	své	k1gNnSc1
i	i	k8xC
jiných	jiný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
si	se	k3xPyFc3
dovedl	dovést	k5eAaPmAgMnS
vážit	vážit	k5eAaImF
opravdových	opravdový	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ostatní	ostatní	k2eAgMnSc1d1
nezlehčoval	zlehčovat	k5eNaImAgMnS
<g/>
,	,	kIx,
ani	ani	k8xC
se	se	k3xPyFc4
jimi	on	k3xPp3gMnPc7
nedával	dávat	k5eNaImAgMnS
zavádět	zavádět	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzájemné	vzájemný	k2eAgNnSc1d1
úzké	úzký	k2eAgNnSc1d1
pouto	pouto	k1gNnSc1
mezi	mezi	k7c7
Markem	Marek	k1gMnSc7
a	a	k8xC
Antoninem	Antonin	k1gInSc7
bylo	být	k5eAaImAgNnS
ještě	ještě	k6eAd1
prohloubeno	prohloubit	k5eAaPmNgNnS
upevněním	upevnění	k1gNnSc7
příbuzenského	příbuzenský	k2eAgInSc2d1
svazku	svazek	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
v	v	k7c6
roce	rok	k1gInSc6
145	#num#	k4
posloužila	posloužit	k5eAaPmAgFnS
svatba	svatba	k1gFnSc1
Marka	marka	k1gFnSc1
s	s	k7c7
Antoninovou	Antoninový	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
Faustinou	Faustina	k1gFnSc7
mladší	mladý	k2eAgFnSc3d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
porodila	porodit	k5eAaPmAgFnS
Markovi	Marek	k1gMnSc3
celkem	celkem	k6eAd1
třináct	třináct	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
většina	většina	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
mladém	mladý	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
161	#num#	k4
Antoninus	Antoninus	k1gMnSc1
Pius	Pius	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
Marcus	Marcus	k1gMnSc1
pod	pod	k7c7
nově	nově	k6eAd1
přijatým	přijatý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Marcus	Marcus	k1gInSc1
Aurelius	Aurelius	k1gInSc1
Antoninus	Antoninus	k1gInSc1
ujal	ujmout	k5eAaPmAgInS
vlády	vláda	k1gFnPc4
nad	nad	k7c7
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
jeho	jeho	k3xOp3gInSc3
nástupu	nástup	k1gInSc3
se	se	k3xPyFc4
nezvedla	zvednout	k5eNaPmAgFnS
žádná	žádný	k3yNgFnSc1
opozice	opozice	k1gFnSc1
<g/>
,	,	kIx,
nevyskytly	vyskytnout	k5eNaPmAgInP
se	se	k3xPyFc4
ani	ani	k8xC
žádné	žádný	k3yNgInPc1
projevy	projev	k1gInPc1
nesouhlasu	nesouhlas	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
všeobecně	všeobecně	k6eAd1
požíval	požívat	k5eAaImAgMnS
úctu	úcta	k1gFnSc4
a	a	k8xC
vážnost	vážnost	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
spoluobčanů	spoluobčan	k1gMnPc2
pro	pro	k7c4
výjimečně	výjimečně	k6eAd1
ctnostný	ctnostný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
povýšení	povýšení	k1gNnSc6
prokázal	prokázat	k5eAaPmAgInS
oddanost	oddanost	k1gFnSc4
Hadrianovým	Hadrianův	k2eAgInPc3d1
záměrům	záměr	k1gInPc3
<g/>
,	,	kIx,
když	když	k8xS
ustanovil	ustanovit	k5eAaPmAgMnS
Lucia	Lucium	k1gNnSc2
Vera	Ver	k1gInSc2
svým	svůj	k3xOyFgMnSc7
spoluvládcem	spoluvládce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
měla	mít	k5eAaImAgFnS
tudíž	tudíž	k8xC
římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
čele	čelo	k1gNnSc6
dva	dva	k4xCgMnPc4
jedince	jedinec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
sdílené	sdílený	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
byl	být	k5eAaImAgInS
jakousi	jakýsi	k3yIgFnSc7
reminiscencí	reminiscence	k1gFnSc7
republikánského	republikánský	k2eAgInSc2d1
principu	princip	k1gInSc2
kolegiality	kolegialita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
i	i	k8xC
přes	přes	k7c4
formálně	formálně	k6eAd1
rovnocenné	rovnocenný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
i	i	k8xC
pravomoc	pravomoc	k1gFnSc4
obou	dva	k4xCgMnPc2
císařů	císař	k1gMnPc2
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnSc1d2
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
méně	málo	k6eAd2
oblíbený	oblíbený	k2eAgInSc4d1
Verus	Verus	k1gInSc4
byl	být	k5eAaImAgMnS
svému	svůj	k3xOyFgMnSc3
bratrovi	bratr	k1gMnSc3
v	v	k7c6
jistém	jistý	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
podřízen	podřídit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
předčil	předčit	k5eAaBmAgMnS,k5eAaPmAgMnS
Vera	Vera	k1gMnSc1
svou	svůj	k3xOyFgFnSc7
autoritou	autorita	k1gFnSc7
(	(	kIx(
<g/>
auctoritas	auctoritas	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
si	se	k3xPyFc3
pro	pro	k7c4
sebe	sebe	k3xPyFc4
vyhradil	vyhradit	k5eAaPmAgMnS
úřad	úřad	k1gInSc4
nejvyššího	vysoký	k2eAgMnSc2d3
velekněze	velekněz	k1gMnSc2
(	(	kIx(
<g/>
pontifex	pontifex	k1gMnSc1
maximus	maximus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
utvrzení	utvrzení	k1gNnSc3
tohoto	tento	k3xDgNnSc2
uspořádání	uspořádání	k1gNnSc2
oženil	oženit	k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
164	#num#	k4
Vera	Ver	k2eAgFnSc1d1
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
dcerou	dcera	k1gFnSc7
Lucillou	Lucilla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císařové	Císař	k1gMnPc1
byli	být	k5eAaImAgMnP
záhy	záhy	k6eAd1
nuceni	nutit	k5eAaImNgMnP
vypořádávat	vypořádávat	k5eAaImF
se	se	k3xPyFc4
s	s	k7c7
nepříznivou	příznivý	k2eNgFnSc7d1
zahraničněpolitickou	zahraničněpolitický	k2eAgFnSc7d1
situací	situace	k1gFnSc7
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
dosud	dosud	k6eAd1
převážně	převážně	k6eAd1
mírové	mírový	k2eAgInPc4d1
poměry	poměr	k1gInPc4
na	na	k7c6
hranicích	hranice	k1gFnPc6
impéria	impérium	k1gNnSc2
byly	být	k5eAaImAgInP
ohroženy	ohrožen	k2eAgInPc1d1
útoky	útok	k1gInPc1
Parthů	Parth	k1gInPc2
a	a	k8xC
následnými	následný	k2eAgInPc7d1
vpády	vpád	k1gInPc7
barbarů	barbar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
vyslal	vyslat	k5eAaPmAgMnS
proto	proto	k8xC
Vera	Vera	k1gMnSc1
na	na	k7c4
Východ	východ	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tam	tam	k6eAd1
velel	velet	k5eAaImAgInS
vojenským	vojenský	k2eAgFnPc3d1
operacím	operace	k1gFnPc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
chopil	chopit	k5eAaPmAgInS
faktického	faktický	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
s	s	k7c7
Parthy	Parth	k1gInPc7
</s>
<s>
Markův	Markův	k2eAgInSc1d1
vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
v	v	k7c6
Tripolisu	Tripolis	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
161	#num#	k4
odrazili	odrazit	k5eAaPmAgMnP
místní	místní	k2eAgMnPc1d1
legáti	legát	k1gMnPc1
nájezdy	nájezd	k1gInPc4
barbarů	barbar	k1gMnPc2
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Raetie	Raetie	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
mnohem	mnohem	k6eAd1
závažnější	závažný	k2eAgNnSc1d2
nebezpečí	nebezpečí	k1gNnSc1
pro	pro	k7c4
římskou	římský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
Orientu	Orient	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
Antoninovy	Antoninův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
výrazně	výrazně	k6eAd1
vzrostlo	vzrůst	k5eAaPmAgNnS
napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
Římany	Říman	k1gMnPc7
a	a	k8xC
Parthy	Parth	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
sporu	spor	k1gInSc2
byla	být	k5eAaImAgFnS
kontrola	kontrola	k1gFnSc1
nad	nad	k7c7
Arménií	Arménie	k1gFnSc7
<g/>
,	,	kIx,
královstvím	království	k1gNnSc7
fungujícím	fungující	k2eAgNnSc7d1
jako	jako	k8xS,k8xC
nárazníkový	nárazníkový	k2eAgInSc1d1
stát	stát	k1gInSc1
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
mocnostmi	mocnost	k1gFnPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
usilovaly	usilovat	k5eAaImAgFnP
dosadit	dosadit	k5eAaPmF
na	na	k7c4
zdejší	zdejší	k2eAgInSc4d1
trůn	trůn	k1gInSc4
monarchu	monarcha	k1gMnSc4
poslušného	poslušný	k2eAgMnSc4d1
jejich	jejich	k3xOp3gInPc3
zájmům	zájem	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parthský	Parthský	k1gMnSc1
král	král	k1gMnSc1
Vologaisés	Vologaisésa	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
konající	konající	k2eAgFnSc1d1
už	už	k9
za	za	k7c2
Antoninova	Antoninův	k2eAgInSc2d1
života	život	k1gInSc2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
válečné	válečný	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
<g/>
,	,	kIx,
hodlal	hodlat	k5eAaImAgMnS
využít	využít	k5eAaPmF
mocenských	mocenský	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
spojené	spojený	k2eAgFnPc1d1
nejistoty	nejistota	k1gFnSc2
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
a	a	k8xC
neprodleně	prodleně	k6eNd1
udeřil	udeřit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	nedlouho	k1gNnSc1
po	po	k7c6
Antoninově	Antoninově	k1gFnSc6
skonu	skon	k1gInSc2
přepadl	přepadnout	k5eAaPmAgInS
Arménii	Arménie	k1gFnSc4
a	a	k8xC
pronikl	proniknout	k5eAaPmAgInS
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
přemoci	přemoct	k5eAaPmF
dvě	dva	k4xCgNnPc4
římská	římský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byl	být	k5eAaImAgInS
poražen	porazit	k5eAaPmNgMnS
římský	římský	k2eAgMnSc1d1
místodržitel	místodržitel	k1gMnSc1
v	v	k7c6
Kappadokii	Kappadokie	k1gFnSc6
<g/>
,	,	kIx,
odebral	odebrat	k5eAaPmAgInS
se	se	k3xPyFc4
Verus	Verus	k1gInSc1
v	v	k7c6
čele	čelo	k1gNnSc6
legií	legie	k1gFnPc2
na	na	k7c4
Východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k9
před	před	k7c7
vlastním	vlastní	k2eAgNnSc7d1
vyplutím	vyplutí	k1gNnSc7
se	se	k3xPyFc4
Verus	Verus	k1gInSc1
zdržel	zdržet	k5eAaPmAgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
nemoci	nemoc	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Canusium	Canusium	k1gNnSc4
a	a	k8xC
teprve	teprve	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
162	#num#	k4
a	a	k8xC
163	#num#	k4
dorazil	dorazit	k5eAaPmAgInS
do	do	k7c2
Antiochie	Antiochie	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
řídil	řídit	k5eAaImAgMnS
reorganizaci	reorganizace	k1gFnSc4
demoralizovaného	demoralizovaný	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
a	a	k8xC
koordinoval	koordinovat	k5eAaBmAgInS
zásobování	zásobování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
postrádal	postrádat	k5eAaImAgInS
zkušenosti	zkušenost	k1gFnPc4
s	s	k7c7
velením	velení	k1gNnSc7
a	a	k8xC
lehkomyslně	lehkomyslně	k6eAd1
projevoval	projevovat	k5eAaImAgMnS
zájem	zájem	k1gInSc4
spíše	spíše	k9
o	o	k7c4
rozmařilý	rozmařilý	k2eAgInSc4d1
a	a	k8xC
nevázaný	vázaný	k2eNgInSc4d1
život	život	k1gInSc4
než	než	k8xS
o	o	k7c4
vážné	vážný	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
ho	on	k3xPp3gMnSc4
prozíravě	prozíravě	k6eAd1
obklopil	obklopit	k5eAaPmAgMnS
nejzdatnějšími	zdatný	k2eAgMnPc7d3
římskými	římský	k2eAgMnPc7d1
generály	generál	k1gMnPc7
<g/>
,	,	kIx,
pod	pod	k7c7
jejichž	jejichž	k3xOyRp3gNnSc7
vedením	vedení	k1gNnSc7
přešli	přejít	k5eAaPmAgMnP
Římané	Říman	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
163	#num#	k4
do	do	k7c2
protiútoku	protiútok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
vypudili	vypudit	k5eAaPmAgMnP
Parthy	Parth	k1gMnPc4
z	z	k7c2
Arménie	Arménie	k1gFnSc2
a	a	k8xC
ustavili	ustavit	k5eAaPmAgMnP
zdejším	zdejší	k2eAgMnPc3d1
vládcem	vládce	k1gMnSc7
prořímského	prořímský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
Sohaema	Sohaem	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
zahájili	zahájit	k5eAaPmAgMnP
ofenzívu	ofenzíva	k1gFnSc4
v	v	k7c6
Mezopotámii	Mezopotámie	k1gFnSc6
<g/>
,	,	kIx,
obsadili	obsadit	k5eAaPmAgMnP
Osroénu	Osroéna	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
165	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
zpustošili	zpustošit	k5eAaPmAgMnP
parthská	parthský	k2eAgNnPc4d1
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
Seleukii	Seleukie	k1gFnSc3
a	a	k8xC
Ktésifón	Ktésifón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
Římané	Říman	k1gMnPc1
postoupili	postoupit	k5eAaPmAgMnP
až	až	k9
do	do	k7c2
Médie	Médie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
přesvědčivým	přesvědčivý	k2eAgInPc3d1
římským	římský	k2eAgInPc3d1
úspěchům	úspěch	k1gInPc3
se	se	k3xPyFc4
parthský	parthský	k2eAgMnSc1d1
král	král	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
ukončit	ukončit	k5eAaPmF
konflikt	konflikt	k1gInSc4
a	a	k8xC
přenechat	přenechat	k5eAaPmF
Arménii	Arménie	k1gFnSc4
Římanům	Říman	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
výpravy	výprava	k1gFnSc2
oslavil	oslavit	k5eAaPmAgInS
Verus	Verus	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
Markem	Marek	k1gMnSc7
a	a	k8xC
ostatními	ostatní	k2eAgMnPc7d1
členy	člen	k1gMnPc7
rodiny	rodina	k1gFnSc2
výjimečný	výjimečný	k2eAgInSc4d1
triumf	triumf	k1gInSc4
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
největší	veliký	k2eAgInSc1d3
podíl	podíl	k1gInSc1
na	na	k7c4
vítězství	vítězství	k1gNnSc4
příslušel	příslušet	k5eAaImAgMnS
vojevůdci	vojevůdce	k1gMnPc1
Avidiu	Avidium	k1gNnSc3
Cassiovi	Cassius	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
římští	římský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
Mezopotámii	Mezopotámie	k1gFnSc6
nakazili	nakazit	k5eAaPmAgMnP
nemocí	nemoc	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
celé	celý	k2eAgFnSc2d1
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
zhruba	zhruba	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
165	#num#	k4
až	až	k9
180	#num#	k4
měla	mít	k5eAaImAgFnS
velmi	velmi	k6eAd1
devastující	devastující	k2eAgInSc4d1
účinek	účinek	k1gInSc4
na	na	k7c4
stav	stav	k1gInSc4
populace	populace	k1gFnSc2
všech	všecek	k3xTgFnPc2
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
epidemie	epidemie	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
označena	označit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
antoninovský	antoninovský	k2eAgInSc4d1
mor	mor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
starověkých	starověký	k2eAgFnPc2d1
epidemií	epidemie	k1gFnPc2
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
nelze	lze	k6eNd1
s	s	k7c7
jistotou	jistota	k1gFnSc7
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
o	o	k7c4
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
chorobu	choroba	k1gFnSc4
se	se	k3xPyFc4
doopravdy	doopravdy	k6eAd1
jednalo	jednat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
převládajícího	převládající	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
mělo	mít	k5eAaImAgNnS
jít	jít	k5eAaImF
buď	buď	k8xC
o	o	k7c4
neštovice	neštovice	k1gFnPc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
o	o	k7c4
spalničky	spalničky	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historika	historik	k1gMnSc2
Cassia	Cassius	k1gMnSc2
Diona	Dion	k1gMnSc2
propukla	propuknout	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
nemoc	nemoc	k1gFnSc1
s	s	k7c7
novou	nový	k2eAgFnSc7d1
intenzitou	intenzita	k1gFnSc7
zhruba	zhruba	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
177	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenom	jenom	k6eAd1
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
Římě	Řím	k1gInSc6
jí	on	k3xPp3gFnSc2
tehdy	tehdy	k6eAd1
denně	denně	k6eAd1
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
za	za	k7c4
oběť	oběť	k1gFnSc4
2000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
čtvrtina	čtvrtina	k1gFnSc1
všech	všecek	k3xTgMnPc2
nakažených	nakažený	k2eAgMnPc2d1
této	tento	k3xDgFnSc3
chorobě	choroba	k1gFnSc3
podlehla	podlehnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
mrtvých	mrtvý	k1gMnPc2
byl	být	k5eAaImAgInS
odhadován	odhadován	k2eAgInSc1d1
na	na	k7c4
pět	pět	k4xCc4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
úmrtnost	úmrtnost	k1gFnSc1
mezi	mezi	k7c7
vojáky	voják	k1gMnPc7
podlamovala	podlamovat	k5eAaImAgFnS
sílu	síla	k1gFnSc4
legií	legie	k1gFnPc2
<g/>
,	,	kIx,
současně	současně	k6eAd1
se	se	k3xPyFc4
snížilo	snížit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
vybraných	vybraný	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
negativní	negativní	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
státní	státní	k2eAgNnSc4d1
hospodaření	hospodaření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
čínských	čínský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
římských	římský	k2eAgMnPc6d1
vyslancích	vyslanec	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
166	#num#	k4
dorazit	dorazit	k5eAaPmF
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
muži	muž	k1gMnPc1
s	s	k7c7
sebou	se	k3xPyFc7
přinesli	přinést	k5eAaPmAgMnP
dary	dar	k1gInPc4
a	a	k8xC
prohlašovali	prohlašovat	k5eAaImAgMnP
se	se	k3xPyFc4
za	za	k7c2
vyslance	vyslanec	k1gMnSc2
jistého	jistý	k2eAgMnSc2d1
Andogni	Andogn	k1gMnPc1
(	(	kIx(
<g/>
Antoninus	Antoninus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
lze	lze	k6eAd1
ztotožnit	ztotožnit	k5eAaPmF
s	s	k7c7
Markem	Marek	k1gMnSc7
Aureliem	Aurelium	k1gNnSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
s	s	k7c7
Antoninem	Antonin	k1gInSc7
Piem	Pius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
ovšem	ovšem	k9
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
nešlo	jít	k5eNaImAgNnS
o	o	k7c4
skutečné	skutečný	k2eAgMnPc4d1
vyslance	vyslanec	k1gMnPc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
obyčejné	obyčejný	k2eAgMnPc4d1
římské	římský	k2eAgMnPc4d1
obchodníky	obchodník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Germánie	Germánie	k1gFnSc1
a	a	k8xC
Dunaj	Dunaj	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Markomanské	markomanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
na	na	k7c4
Kapitolu	kapitola	k1gFnSc4
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
zvyšovala	zvyšovat	k5eAaImAgFnS
intensita	intensita	k1gFnSc1
nájezdů	nájezd	k1gInPc2
Germánů	Germán	k1gMnPc2
a	a	k8xC
jiných	jiný	k2eAgMnPc2d1
barbarů	barbar	k1gMnPc2
směřujících	směřující	k2eAgMnPc2d1
do	do	k7c2
Galie	Galie	k1gFnSc2
a	a	k8xC
za	za	k7c4
Dunaj	Dunaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pohyb	pohyb	k1gInSc1
barbarů	barbar	k1gMnPc2
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
západ	západ	k1gInSc1
byl	být	k5eAaImAgInS
zřejmě	zřejmě	k6eAd1
vyvolán	vyvolat	k5eAaPmNgInS
tlakem	tlak	k1gInSc7
kmenů	kmen	k1gInPc2
sídlících	sídlící	k2eAgInPc2d1
dále	daleko	k6eAd2
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
nepřátelská	přátelský	k2eNgFnSc1d1
invase	invase	k1gFnSc1
<g/>
,	,	kIx,
vpád	vpád	k1gInSc1
Chattů	Chatta	k1gMnPc2
do	do	k7c2
Horní	horní	k2eAgFnSc2d1
Germánie	Germánie	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
odražena	odrazit	k5eAaPmNgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
162	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejspíše	nejspíše	k9
koncem	koncem	k7c2
roku	rok	k1gInSc2
166	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
167	#num#	k4
překročilo	překročit	k5eAaPmAgNnS
6000	#num#	k4
Langobardů	Langobard	k1gInPc2
a	a	k8xC
Obiů	Obi	k1gInPc2
Dunaj	Dunaj	k1gInSc1
a	a	k8xC
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
Panonie	Panonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barbaři	barbar	k1gMnPc1
byli	být	k5eAaImAgMnP
brzy	brzy	k6eAd1
poraženi	poražen	k2eAgMnPc1d1
<g/>
,	,	kIx,
načež	načež	k6eAd1
zdejší	zdejší	k2eAgMnSc1d1
římský	římský	k2eAgMnSc1d1
místodržitel	místodržitel	k1gMnSc1
zahájil	zahájit	k5eAaPmAgMnS
vyjednávání	vyjednávání	k1gNnSc4
se	s	k7c7
zástupci	zástupce	k1gMnPc7
několika	několik	k4yIc2
okolních	okolní	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pomocí	pomoc	k1gFnSc7
markomanského	markomanský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ballomara	Ballomar	k1gMnSc2
se	se	k3xPyFc4
sice	sice	k8xC
podařilo	podařit	k5eAaPmAgNnS
dohodnout	dohodnout	k5eAaPmF
mír	mír	k1gInSc4
mezi	mezi	k7c7
Římany	Říman	k1gMnPc7
a	a	k8xC
jejich	jejich	k3xOp3gMnPc7
barbarskými	barbarský	k2eAgMnPc7d1
sousedy	soused	k1gMnPc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
situace	situace	k1gFnSc1
na	na	k7c6
hranicích	hranice	k1gFnPc6
zůstávala	zůstávat	k5eAaImAgFnS
nadále	nadále	k6eAd1
neklidná	klidný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
Dácie	Dácie	k1gFnSc1
přepadena	přepadnout	k5eAaPmNgFnS
Vandaly	Vandal	k1gMnPc7
a	a	k8xC
sarmatskými	sarmatský	k2eAgMnPc7d1
Jazygy	Jazyg	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Neblahý	blahý	k2eNgInSc1d1
vývoj	vývoj	k1gInSc1
na	na	k7c6
severních	severní	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
zhoršovaný	zhoršovaný	k2eAgInSc1d1
šířícím	šířící	k2eAgMnSc7d1
se	se	k3xPyFc4
morem	mor	k1gInSc7
přiměl	přimět	k5eAaPmAgMnS
Marka	Marek	k1gMnSc4
k	k	k7c3
horlivému	horlivý	k2eAgNnSc3d1
konání	konání	k1gNnSc3
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
chtěl	chtít	k5eAaImAgMnS
usmířit	usmířit	k5eAaPmF
rozhněvané	rozhněvaný	k2eAgMnPc4d1
bohy	bůh	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
168	#num#	k4
vyrazili	vyrazit	k5eAaPmAgMnP
oba	dva	k4xCgMnPc1
císařové	císař	k1gMnPc1
z	z	k7c2
Říma	Řím	k1gInSc2
na	na	k7c4
dunajskou	dunajský	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
hlavní	hlavní	k2eAgInSc4d1
stan	stan	k1gInSc4
umístili	umístit	k5eAaPmAgMnP
do	do	k7c2
severoitalského	severoitalský	k2eAgNnSc2d1
města	město	k1gNnSc2
Aquileia	Aquileium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
organizovali	organizovat	k5eAaBmAgMnP
obranu	obrana	k1gFnSc4
římského	římský	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
prováděli	provádět	k5eAaImAgMnP
inspekci	inspekce	k1gFnSc4
vojsk	vojsko	k1gNnPc2
a	a	k8xC
dali	dát	k5eAaPmAgMnP
odvést	odvést	k5eAaPmF
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
legie	legie	k1gFnPc4
<g/>
,	,	kIx,
legio	legio	k1gMnSc1
II	II	kA
Italica	Italica	k1gMnSc1
a	a	k8xC
legio	legio	k1gMnSc1
III	III	kA
Italica	Italica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomnost	přítomnost	k1gFnSc1
Marka	marka	k1gFnSc1
a	a	k8xC
Vera	Vera	k1gFnSc1
v	v	k7c6
Panonii	Panonie	k1gFnSc6
a	a	k8xC
Noriku	Noricum	k1gNnSc6
pomohla	pomoct	k5eAaPmAgNnP
stabilisovat	stabilisovat	k5eAaBmF
tamní	tamní	k2eAgFnPc4d1
poměry	poměra	k1gFnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
kvůli	kvůli	k7c3
řádění	řádění	k1gNnSc3
moru	mor	k1gInSc2
mezi	mezi	k7c7
legionáři	legionář	k1gMnPc7
se	se	k3xPyFc4
na	na	k7c4
radu	rada	k1gFnSc4
dvorního	dvorní	k2eAgMnSc4d1
lékaře	lékař	k1gMnSc4
Galéna	Galén	k1gMnSc4
rozhodli	rozhodnout	k5eAaPmAgMnP
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
Verus	Verus	k1gMnSc1
patrně	patrně	k6eAd1
v	v	k7c6
důsledku	důsledek	k1gInSc6
mrtvice	mrtvice	k1gFnSc2
v	v	k7c6
lednu	leden	k1gInSc6
nebo	nebo	k8xC
únoru	únor	k1gInSc6
169	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
od	od	k7c2
tohoto	tento	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
musel	muset	k5eAaImAgMnS
Marcus	Marcus	k1gMnSc1
trávit	trávit	k5eAaImF
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
na	na	k7c6
taženích	tažení	k1gNnPc6
proti	proti	k7c3
nepřátelům	nepřítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
ovdovělou	ovdovělý	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
Lucillu	Lucill	k1gInSc2
provdal	provdat	k5eAaPmAgInS
i	i	k9
přes	přes	k7c4
její	její	k3xOp3gInSc4
odpor	odpor	k1gInSc4
za	za	k7c2
Tiberia	Tiberium	k1gNnSc2
Claudia	Claudia	k1gFnSc1
Pompeiana	Pompeiana	k1gFnSc1
<g/>
,	,	kIx,
mimořádně	mimořádně	k6eAd1
schopného	schopný	k2eAgMnSc4d1
důstojníka	důstojník	k1gMnSc4
syrského	syrský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Invaze	invaze	k1gFnSc1
Markomanů	Markoman	k1gMnPc2
a	a	k8xC
Kvádů	Kvád	k1gMnPc2
do	do	k7c2
Itálie	Itálie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
170	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
170	#num#	k4
ztroskotal	ztroskotat	k5eAaPmAgInS
první	první	k4xOgInSc1
vážnější	vážní	k2eAgInSc1d2
římský	římský	k2eAgInSc1d1
pokus	pokus	k1gInSc1
o	o	k7c4
protiofensivu	protiofensiva	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
20	#num#	k4
000	#num#	k4
Římanů	Říman	k1gMnPc2
poraženo	porazit	k5eAaPmNgNnS
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Carnuntum	Carnuntum	k1gNnSc1
germánskou	germánský	k2eAgFnSc7d1
koalicí	koalice	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Ballomarem	Ballomar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markomani	Markoman	k1gMnPc1
a	a	k8xC
Kvádové	Kvád	k1gMnPc1
pak	pak	k6eAd1
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgMnPc1d1
barbaři	barbar	k1gMnPc1
vyplenili	vyplenit	k5eAaPmAgMnP
Noricum	Noricum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ballomarem	Ballomar	k1gInSc7
vedení	vedený	k2eAgMnPc1d1
Germáni	Germán	k1gMnPc1
zdolali	zdolat	k5eAaPmAgMnP
Alpy	Alpy	k1gFnPc4
<g/>
,	,	kIx,
vydrancovali	vydrancovat	k5eAaPmAgMnP
město	město	k1gNnSc4
Opitergium	Opitergium	k1gNnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Oderzo	Oderza	k1gFnSc5
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
nápor	nápor	k1gInSc1
vyvrcholil	vyvrcholit	k5eAaPmAgInS
obležením	obležení	k1gNnSc7
Aquileie	Aquileie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
od	od	k7c2
porážky	porážka	k1gFnSc2
Kimbrů	Kimbr	k1gMnPc2
a	a	k8xC
Teutonů	Teuton	k1gMnPc2
na	na	k7c6
konci	konec	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
tak	tak	k6eAd1
nepřátelským	přátelský	k2eNgFnPc3d1
silám	síla	k1gFnPc3
podařilo	podařit	k5eAaPmAgNnS
stanout	stanout	k5eAaPmF
na	na	k7c6
italské	italský	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vážně	vážně	k6eAd1
otřáslo	otřást	k5eAaPmAgNnS
sebedůvěrou	sebedůvěra	k1gFnSc7
obyvatelstva	obyvatelstvo	k1gNnSc2
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Kostobokové	Kostobokový	k2eAgInPc1d1
<g/>
,	,	kIx,
příchozí	příchozí	k1gFnPc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Karpat	Karpaty	k1gInPc2
<g/>
,	,	kIx,
využili	využít	k5eAaPmAgMnP
slabosti	slabost	k1gFnSc2
Římanů	Říman	k1gMnPc2
<g/>
,	,	kIx,
zpustošili	zpustošit	k5eAaPmAgMnP
Moesii	Moesie	k1gFnSc4
<g/>
,	,	kIx,
Thrákii	Thrákie	k1gFnSc4
a	a	k8xC
Makedonii	Makedonie	k1gFnSc4
a	a	k8xC
dorazili	dorazit	k5eAaPmAgMnP
až	až	k9
do	do	k7c2
Eleusíny	Eleusína	k1gFnSc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
jiné	jiný	k2eAgFnSc2d1
části	část	k1gFnSc2
říše	říš	k1gFnSc2
byly	být	k5eAaImAgInP
svírány	svírán	k2eAgInPc1d1
loupeživými	loupeživý	k2eAgInPc7d1
nájezdy	nájezd	k1gInPc7
a	a	k8xC
povstáními	povstání	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
přemohli	přemoct	k5eAaPmAgMnP
Maurové	Maurové	k?
posádky	posádka	k1gFnPc4
pohraničních	pohraniční	k2eAgInPc2d1
kastelů	kastel	k1gInPc2
v	v	k7c6
Mauretánii	Mauretánie	k1gFnSc6
a	a	k8xC
po	po	k7c4
dvě	dva	k4xCgNnPc4
léta	léto	k1gNnPc4
sužovali	sužovat	k5eAaImAgMnP
svými	svůj	k3xOyFgMnPc7
útoky	útok	k1gInPc7
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
ještě	ještě	k6eAd1
Ammianus	Ammianus	k1gMnSc1
Marcellinus	Marcellinus	k1gMnSc1
<g/>
,	,	kIx,
přední	přední	k2eAgMnSc1d1
dějepisec	dějepisec	k1gMnSc1
pozdní	pozdní	k2eAgFnSc2d1
antiky	antika	k1gFnSc2
<g/>
,	,	kIx,
připomínal	připomínat	k5eAaImAgInS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
události	událost	k1gFnSc2
markomanských	markomanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
jevící	jevící	k2eAgMnSc1d1
se	se	k3xPyFc4
s	s	k7c7
odstupem	odstup	k1gInSc7
času	čas	k1gInSc2
jako	jako	k8xS,k8xC
předzvěst	předzvěst	k1gFnSc1
mnohem	mnohem	k6eAd1
hrozivějších	hrozivý	k2eAgInPc2d2
barbarských	barbarský	k2eAgInPc2d1
útoků	útok	k1gInPc2
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
staletích	staletí	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1
lidských	lidský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
zapříčiněný	zapříčiněný	k2eAgInSc1d1
morem	mor	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
finanční	finanční	k2eAgFnPc1d1
těžkosti	těžkost	k1gFnPc1
přiměly	přimět	k5eAaPmAgFnP
Římany	Říman	k1gMnPc4
k	k	k7c3
přijetí	přijetí	k1gNnSc3
mimořádných	mimořádný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
legií	legie	k1gFnPc2
byli	být	k5eAaImAgMnP
rekrutováni	rekrutován	k2eAgMnPc1d1
otroci	otrok	k1gMnPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
gladiátoři	gladiátor	k1gMnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
útočníci	útočník	k1gMnPc1
pozvolna	pozvolna	k6eAd1
zatlačeni	zatlačen	k2eAgMnPc1d1
zpět	zpět	k6eAd1
za	za	k7c4
Dunaj	Dunaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
některými	některý	k3yIgInPc7
kmeny	kmen	k1gInPc7
byly	být	k5eAaImAgFnP
navázány	navázán	k2eAgInPc4d1
diplomatické	diplomatický	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
nichž	jenž	k3xRgInPc2
bylo	být	k5eAaImAgNnS
dojednáno	dojednán	k2eAgNnSc1d1
spojenectví	spojenectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
germánské	germánský	k2eAgInPc1d1
pomocné	pomocný	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
zapojily	zapojit	k5eAaPmAgFnP
do	do	k7c2
konfliktu	konflikt	k1gInSc2
na	na	k7c6
straně	strana	k1gFnSc6
Římanů	Říman	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
izolovat	izolovat	k5eAaBmF
nejvzpurnější	vzpurný	k2eAgInPc4d3
kmeny	kmen	k1gInPc4
a	a	k8xC
vést	vést	k5eAaImF
s	s	k7c7
nimi	on	k3xPp3gInPc7
boj	boj	k1gInSc4
odděleně	odděleně	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
mohli	moct	k5eAaImAgMnP
Římané	Říman	k1gMnPc1
snáze	snadno	k6eAd2
využít	využít	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
drtivou	drtivý	k2eAgFnSc4d1
početní	početní	k2eAgFnSc4d1
a	a	k8xC
logistickou	logistický	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strategie	strategie	k1gFnPc1
se	se	k3xPyFc4
ale	ale	k9
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
velice	velice	k6eAd1
nákladná	nákladný	k2eAgNnPc1d1
a	a	k8xC
zdlouhavá	zdlouhavý	k2eAgNnPc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
enormní	enormní	k2eAgNnSc4d1
množství	množství	k1gNnSc4
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejímu	její	k3xOp3gNnSc3
uskutečnění	uskutečnění	k1gNnSc3
bylo	být	k5eAaImAgNnS
navíc	navíc	k6eAd1
zapotřebí	zapotřebí	k6eAd1
značných	značný	k2eAgInPc2d1
vojenských	vojenský	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
něž	jenž	k3xRgMnPc4
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
vystavěny	vystavěn	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
pevnosti	pevnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byla	být	k5eAaImAgFnS
třeba	třeba	k6eAd1
Castra	Castra	k1gFnSc1
Regina	Regina	k1gFnSc1
(	(	kIx(
<g/>
Regensburg	Regensburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
byly	být	k5eAaImAgInP
učiněny	učinit	k5eAaPmNgInP,k5eAaImNgInP
nepříliš	příliš	k6eNd1
úspěšné	úspěšný	k2eAgInPc1d1
kroky	krok	k1gInPc1
k	k	k7c3
usazení	usazení	k1gNnSc3
určitých	určitý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
barbarů	barbar	k1gMnPc2
na	na	k7c6
římském	římský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
Římané	Říman	k1gMnPc1
obnovili	obnovit	k5eAaPmAgMnP
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Norikem	Norik	k1gMnSc7
<g/>
,	,	kIx,
přemístil	přemístit	k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
své	svůj	k3xOyFgNnSc4
hlavní	hlavní	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
do	do	k7c2
Carnunta	Carnunt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příležitostně	příležitostně	k6eAd1
řídil	řídit	k5eAaImAgMnS
vojenské	vojenský	k2eAgFnPc4d1
operace	operace	k1gFnPc4
také	také	k9
z	z	k7c2
táborů	tábor	k1gInPc2
v	v	k7c6
Sirmiu	Sirmium	k1gNnSc6
(	(	kIx(
<g/>
Sremska	Sremska	k1gFnSc1
Mitrovica	Mitrovica	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
Vindoboně	Vindobon	k1gInSc6
(	(	kIx(
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
172	#num#	k4
podnikli	podniknout	k5eAaPmAgMnP
Římané	Říman	k1gMnPc1
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
Markomanům	Markoman	k1gMnPc3
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
si	se	k3xPyFc3
podrobili	podrobit	k5eAaPmAgMnP
Kvády	Kvád	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
Marcus	Marcus	k1gMnSc1
naložil	naložit	k5eAaPmAgMnS
s	s	k7c7
Jazygy	Jazyg	k1gInPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
přinutil	přinutit	k5eAaPmAgMnS
vydat	vydat	k5eAaPmF
tisíce	tisíc	k4xCgInPc1
římských	římský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznámky	poznámka	k1gFnSc2
v	v	k7c6
Historii	historie	k1gFnSc6
Augustě	Augusta	k1gFnSc6
o	o	k7c6
Markově	Markův	k2eAgInSc6d1
úmyslu	úmysl	k1gInSc6
zřídit	zřídit	k5eAaPmF
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Dunaje	Dunaj	k1gInSc2
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgFnPc4d1
Marcomannia	Marcomannium	k1gNnSc2
a	a	k8xC
Sarmatia	Sarmatium	k1gNnSc2
(	(	kIx(
<g/>
zahrnující	zahrnující	k2eAgFnSc4d1
přibližně	přibližně	k6eAd1
nynější	nynější	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc4
a	a	k8xC
východní	východní	k2eAgNnSc4d1
Maďarsko	Maďarsko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
absenci	absence	k1gFnSc3
shodných	shodný	k2eAgNnPc2d1
tvrzení	tvrzení	k1gNnPc2
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
<g/>
,	,	kIx,
zpochybňují	zpochybňovat	k5eAaImIp3nP
a	a	k8xC
považují	považovat	k5eAaImIp3nP
za	za	k7c4
kontroversní	kontroversní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
pohoří	pohořet	k5eAaPmIp3nS
na	na	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
těchto	tento	k3xDgNnPc2
teritorií	teritorium	k1gNnPc2
mohla	moct	k5eAaImAgFnS
tvořit	tvořit	k5eAaImF
lépe	dobře	k6eAd2
hájitelnou	hájitelný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
jakou	jaký	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
představoval	představovat	k5eAaImAgInS
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
vyčerpaná	vyčerpaný	k2eAgFnSc1d1
dlouhotrvajícími	dlouhotrvající	k2eAgInPc7d1
válečnými	válečný	k2eAgInPc7d1
střety	střet	k1gInPc7
<g/>
,	,	kIx,
nedisponovala	disponovat	k5eNaBmAgFnS
prostředky	prostředek	k1gInPc7
nezbytnými	nezbytný	k2eAgInPc7d1,k2eNgInPc7d1
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
těchto	tento	k3xDgFnPc2
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
už	už	k6eAd1
byly	být	k5eAaImAgFnP
Markovy	Markův	k2eAgInPc4d1
záměry	záměr	k1gInPc4
jakékoli	jakýkoli	k3yIgInPc4
<g/>
,	,	kIx,
usurpace	usurpace	k1gFnSc1
Avidia	Avidium	k1gNnSc2
Cassia	Cassia	k1gFnSc1
mu	on	k3xPp3gMnSc3
zabránila	zabránit	k5eAaPmAgFnS
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
uskutečnění	uskutečnění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
177	#num#	k4
pokračovali	pokračovat	k5eAaImAgMnP
Římané	Říman	k1gMnPc1
ve	v	k7c6
válečných	válečný	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
proti	proti	k7c3
Markomanům	Markoman	k1gMnPc3
a	a	k8xC
Kvádům	Kvád	k1gMnPc3
na	na	k7c6
středním	střední	k2eAgInSc6d1
Dunaji	Dunaj	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
vytrvali	vytrvat	k5eAaPmAgMnP
do	do	k7c2
samotného	samotný	k2eAgInSc2d1
závěru	závěr	k1gInSc2
Markova	Markův	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Markův	Markův	k2eAgInSc1d1
denár	denár	k1gInSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgMnS
velkou	velký	k2eAgFnSc7d1
svědomitostí	svědomitost	k1gFnSc7
při	při	k7c6
plnění	plnění	k1gNnSc6
svých	svůj	k3xOyFgFnPc2
administrativních	administrativní	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obtížné	obtížný	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
neváhal	váhat	k5eNaImAgMnS
svěřovat	svěřovat	k5eAaImF
nejkompetentnějším	kompetentní	k2eAgMnPc3d3
mužům	muž	k1gMnPc3
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
svými	svůj	k3xOyFgFnPc7
schopnostmi	schopnost	k1gFnPc7
zastíní	zastínit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimořádnou	mimořádný	k2eAgFnSc7d1
pozorností	pozornost	k1gFnSc7
zahrnul	zahrnout	k5eAaPmAgInS
sociálně	sociálně	k6eAd1
nejslabší	slabý	k2eAgFnPc4d3
příslušníky	příslušník	k1gMnPc7
římské	římský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
:	:	kIx,
otroky	otrok	k1gMnPc7
<g/>
,	,	kIx,
ženy	žena	k1gFnPc4
a	a	k8xC
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
jimž	jenž	k3xRgFnPc3
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
ulehčit	ulehčit	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
tíživou	tíživý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
dochovaných	dochovaný	k2eAgInPc2d1
zákonodárných	zákonodárný	k2eAgInPc2d1
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
aktů	akt	k1gInPc2
<g/>
,	,	kIx,
vydaných	vydaný	k2eAgInPc2d1
za	za	k7c4
Marka	Marek	k1gMnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
zaměřovala	zaměřovat	k5eAaImAgFnS
na	na	k7c4
zlepšení	zlepšení	k1gNnSc4
právního	právní	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
těchto	tento	k3xDgFnPc2
skupin	skupina	k1gFnPc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
akty	akt	k1gInPc4
často	často	k6eAd1
sloužily	sloužit	k5eAaImAgInP
k	k	k7c3
odstranění	odstranění	k1gNnSc3
nadměrné	nadměrný	k2eAgFnSc2d1
tvrdosti	tvrdost	k1gFnSc2
a	a	k8xC
nepřesností	nepřesnost	k1gFnPc2
v	v	k7c6
občanském	občanský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejnou	stejný	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
projevoval	projevovat	k5eAaImAgMnS
i	i	k9
při	při	k7c6
uplatňování	uplatňování	k1gNnSc6
soudní	soudní	k2eAgFnSc2d1
pravomoci	pravomoc	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc2
vykonával	vykonávat	k5eAaImAgMnS
se	s	k7c7
vzornou	vzorný	k2eAgFnSc7d1
pečlivostí	pečlivost	k1gFnSc7
a	a	k8xC
bezpříkladnou	bezpříkladný	k2eAgFnSc7d1
obětavostí	obětavost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Historie	historie	k1gFnSc2
Augusty	Augusta	k1gMnSc2
nechal	nechat	k5eAaPmAgMnS
zvýšit	zvýšit	k5eAaPmF
počet	počet	k1gInSc4
dnů	den	k1gInPc2
vyhrazených	vyhrazený	k2eAgInPc2d1
k	k	k7c3
projednávání	projednávání	k1gNnSc3
soudních	soudní	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
soudní	soudní	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
nepolevoval	polevovat	k5eNaImAgMnS
ani	ani	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
vytáhl	vytáhnout	k5eAaPmAgMnS
do	do	k7c2
boje	boj	k1gInSc2
s	s	k7c7
Germány	Germán	k1gMnPc7
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
se	se	k3xPyFc4
zmiňuje	zmiňovat	k5eAaImIp3nS
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
řízení	řízení	k1gNnSc4
museli	muset	k5eAaImAgMnP
dokonce	dokonce	k9
kvůli	kvůli	k7c3
projednání	projednání	k1gNnSc3
přicestovat	přicestovat	k5eAaPmF
za	za	k7c7
císařem	císař	k1gMnSc7
do	do	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vojenského	vojenský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitý	určitý	k2eAgInSc4d1
úpadek	úpadek	k1gInSc4
signalizoval	signalizovat	k5eAaImAgMnS
počátek	počátek	k1gInSc4
aplikace	aplikace	k1gFnSc2
kategorií	kategorie	k1gFnPc2
honestiores	honestiores	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
vážení	vážení	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
humiliores	humiliores	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
nižší	nízký	k2eAgFnSc1d2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc4
prostřednictvím	prostřednictvím	k7c2
bylo	být	k5eAaImAgNnS
v	v	k7c6
odvětví	odvětví	k1gNnSc6
trestního	trestní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
zohledňován	zohledňován	k2eAgInSc1d1
společenský	společenský	k2eAgInSc1d1
stav	stav	k1gInSc1
při	při	k7c6
rozhodování	rozhodování	k1gNnSc6
o	o	k7c6
míře	míra	k1gFnSc6
potrestání	potrestání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudní	soudní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Itálie	Itálie	k1gFnSc2
byla	být	k5eAaImAgFnS
přenechána	přenechán	k2eAgFnSc1d1
úředníkům	úředník	k1gMnPc3
<g/>
,	,	kIx,
nazývaným	nazývaný	k2eAgMnPc3d1
iuridici	iuridice	k1gFnSc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
vystupovali	vystupovat	k5eAaImAgMnP
jménem	jméno	k1gNnSc7
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
praxí	praxe	k1gFnSc7
<g/>
,	,	kIx,
napomáhající	napomáhající	k2eAgFnSc4d1
centralisaci	centralisace	k1gFnSc4
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
navazoval	navazovat	k5eAaImAgInS
Marcus	Marcus	k1gInSc1
na	na	k7c4
někdejší	někdejší	k2eAgNnSc4d1
Hadrianovo	Hadrianův	k2eAgNnSc4d1
soudní	soudní	k2eAgNnSc4d1
členění	členění	k1gNnSc4
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgInSc2
bylo	být	k5eAaImAgNnS
upuštěno	upustit	k5eAaPmNgNnS
za	za	k7c4
Antonina	Antonin	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Sloup	sloup	k1gInSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
na	na	k7c4
Piazza	Piazz	k1gMnSc4
Colonna	Colonn	k1gMnSc4
v	v	k7c6
Římě	Řím	k1gInSc6
</s>
<s>
V	v	k7c6
úvodní	úvodní	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
Markova	Markův	k2eAgNnSc2d1
panování	panování	k1gNnSc2
byla	být	k5eAaImAgFnS
římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
zasažena	zasáhnout	k5eAaPmNgFnS
řadou	řada	k1gFnSc7
katastrof	katastrofa	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
patřily	patřit	k5eAaImAgFnP
k	k	k7c3
nejzávažnějším	závažný	k2eAgInPc3d3
ničivé	ničivý	k2eAgInPc1d1
rozvodnění	rozvodnění	k1gNnSc4
Tiberu	Tiber	k1gInSc2
a	a	k8xC
především	především	k9
antoninovský	antoninovský	k2eAgInSc4d1
mor	mor	k1gInSc4
<g/>
,	,	kIx,
zanesený	zanesený	k2eAgInSc4d1
do	do	k7c2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
vojáky	voják	k1gMnPc4
vracejícími	vracející	k2eAgMnPc7d1
se	se	k3xPyFc4
z	z	k7c2
Východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epidemie	epidemie	k1gFnSc1
pozvolna	pozvolna	k6eAd1
postihla	postihnout	k5eAaPmAgFnS
takřka	takřka	k6eAd1
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
hustě	hustě	k6eAd1
obydleného	obydlený	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
vypuknutí	vypuknutí	k1gNnSc2
války	válka	k1gFnSc2
s	s	k7c7
Germány	Germán	k1gMnPc7
byla	být	k5eAaImAgFnS
Itálie	Itálie	k1gFnSc1
zle	zle	k6eAd1
sužována	sužovat	k5eAaImNgFnS
hladem	hlad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následky	následek	k1gInPc4
těchto	tento	k3xDgNnPc2
neštěstí	neštěstí	k1gNnPc2
Marcus	Marcus	k1gInSc1
usiloval	usilovat	k5eAaImAgInS
zmírnit	zmírnit	k5eAaPmF
rozdílením	rozdílení	k1gNnSc7
obilí	obilí	k1gNnSc4
všem	všecek	k3xTgNnPc3
italským	italský	k2eAgNnPc3d1
městům	město	k1gNnPc3
a	a	k8xC
zaváděním	zavádění	k1gNnSc7
různých	různý	k2eAgNnPc2d1
sanitačních	sanitační	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
bránících	bránící	k2eAgFnPc2d1
šíření	šíření	k1gNnPc2
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodní	přírodní	k2eAgFnPc1d1
pohromy	pohroma	k1gFnPc1
<g/>
,	,	kIx,
epidemie	epidemie	k1gFnPc1
a	a	k8xC
vysoké	vysoký	k2eAgInPc1d1
válečné	válečný	k2eAgInPc1d1
náklady	náklad	k1gInPc1
neúměrně	úměrně	k6eNd1
zatěžovaly	zatěžovat	k5eAaImAgInP
císařskou	císařský	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Marcus	Marcus	k1gMnSc1
ulevoval	ulevovat	k5eAaImAgMnS
státním	státní	k2eAgMnSc7d1
výdajům	výdaj	k1gInPc3
svojí	svůj	k3xOyFgFnSc7
příkladnou	příkladný	k2eAgFnSc7d1
osobní	osobní	k2eAgFnSc7d1
zdrženlivostí	zdrženlivost	k1gFnSc7
a	a	k8xC
střídmostí	střídmost	k1gFnSc7
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
uchýlit	uchýlit	k5eAaPmF
k	k	k7c3
mírnému	mírný	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
obsahu	obsah	k1gInSc2
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
v	v	k7c6
mincích	mince	k1gFnPc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
nevyhnutelně	vyhnutelně	k6eNd1
přiživil	přiživit	k5eAaPmAgMnS
inflaci	inflace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
získal	získat	k5eAaPmAgInS
dodatečné	dodatečný	k2eAgInPc4d1
peněžní	peněžní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
omezil	omezit	k5eAaPmAgMnS
závody	závod	k1gInPc4
a	a	k8xC
gladiátorské	gladiátorský	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
v	v	k7c6
Cirku	cirk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobně	osobně	k6eAd1
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
uhrazení	uhrazení	k1gNnSc3
financování	financování	k1gNnSc2
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
nechal	nechat	k5eAaPmAgMnS
na	na	k7c6
Foru	forum	k1gNnSc6
vydražit	vydražit	k5eAaPmF
četné	četný	k2eAgInPc4d1
cenné	cenný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
uložené	uložený	k2eAgInPc4d1
v	v	k7c6
císařském	císařský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězným	vítězný	k2eAgNnSc7d1
legionářům	legionář	k1gMnPc3
odmítl	odmítnout	k5eAaPmAgMnS
udělit	udělit	k5eAaPmF
zvláštní	zvláštní	k2eAgFnSc4d1
odměnu	odměna	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
dožadovali	dožadovat	k5eAaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
upozorňoval	upozorňovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
taková	takový	k3xDgFnSc1
platba	platba	k1gFnSc1
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
zajištěna	zajistit	k5eAaPmNgFnS
na	na	k7c4
úkor	úkor	k1gInSc4
jejich	jejich	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
a	a	k8xC
příbuzných	příbuzný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
Markova	Markův	k2eAgNnSc2d1
vládnutí	vládnutí	k1gNnSc2
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
jen	jen	k6eAd1
nevelké	velký	k2eNgNnSc1d1
množství	množství	k1gNnSc1
význačných	význačný	k2eAgInPc2d1
monumentů	monument	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
se	se	k3xPyFc4
zřetelně	zřetelně	k6eAd1
odlišil	odlišit	k5eAaPmAgInS
od	od	k7c2
mnoha	mnoho	k4c2
svých	svůj	k3xOyFgMnPc2
předchůdců	předchůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
počest	počest	k1gFnSc4
Antonina	Antonina	k1gFnSc1
byl	být	k5eAaImAgInS
vybudován	vybudován	k2eAgInSc1d1
sloup	sloup	k1gInSc1
na	na	k7c6
Martově	Martův	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
ústředním	ústřední	k2eAgInSc7d1
motivem	motiv	k1gInSc7
byla	být	k5eAaImAgFnS
apoteosa	apoteosa	k1gFnSc1
zemřelého	zemřelý	k1gMnSc2
císaře	císař	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Faustiny	Faustin	k2eAgMnPc4d1
starší	starší	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstavec	podstavec	k1gInSc1
tohoto	tento	k3xDgInSc2
sloupu	sloup	k1gInSc2
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
vystaven	vystavit	k5eAaPmNgMnS
ve	v	k7c6
Vatikánských	vatikánský	k2eAgNnPc6d1
museích	museum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markovy	Markův	k2eAgInPc1d1
a	a	k8xC
Verovy	Verův	k2eAgInPc1d1
válečné	válečný	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
Římě	Řím	k1gInSc6
oslaveny	oslavit	k5eAaPmNgFnP
celkem	celkem	k6eAd1
třemi	tři	k4xCgInPc7
vítěznými	vítězný	k2eAgInPc7d1
oblouky	oblouk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotnému	samotný	k2eAgMnSc3d1
Markovi	Marek	k1gMnSc3
byly	být	k5eAaImAgInP
určeny	určit	k5eAaPmNgInP
dva	dva	k4xCgMnPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
žádný	žádný	k3yNgMnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
nedochoval	dochovat	k5eNaPmAgInS
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
některé	některý	k3yIgInPc1
pravoúhlé	pravoúhlý	k2eAgInPc1d1
reliéfy	reliéf	k1gInPc1
byly	být	k5eAaImAgInP
využity	využít	k5eAaPmNgInP
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
Konstantinova	Konstantinův	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřejmě	zřejmě	k6eAd1
nejvěhlasnější	věhlasný	k2eAgFnSc7d3
stavbou	stavba	k1gFnSc7
připomínající	připomínající	k2eAgInSc1d1
Markův	Markův	k2eAgInSc1d1
principát	principát	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
sloup	sloup	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nS
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Piazza	Piazz	k1gMnSc2
Colonna	Colonn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
díle	dílo	k1gNnSc6
jsou	být	k5eAaImIp3nP
vyobrazena	vyobrazen	k2eAgNnPc1d1
Markova	Markův	k2eAgNnPc1d1
tažení	tažení	k1gNnPc1
proti	proti	k7c3
Markomanům	Markoman	k1gMnPc3
a	a	k8xC
Sarmatům	Sarmat	k1gMnPc3
v	v	k7c6
letech	léto	k1gNnPc6
172	#num#	k4
až	až	k9
175	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
vlysů	vlys	k1gInPc2
je	být	k5eAaImIp3nS
zachycena	zachytit	k5eAaPmNgFnS
událost	událost	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
zázračný	zázračný	k2eAgInSc1d1
déšť	déšť	k1gInSc1
v	v	k7c6
zemi	zem	k1gFnSc6
Kvádů	Kvád	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
podle	podle	k7c2
římského	římský	k2eAgNnSc2d1
líčení	líčení	k1gNnSc2
vyslyšel	vyslyšet	k5eAaPmAgMnS
bůh	bůh	k1gMnSc1
císařovy	císařův	k2eAgFnSc2d1
modlitby	modlitba	k1gFnSc2
a	a	k8xC
sesláním	seslání	k1gNnSc7
bouře	bouř	k1gFnSc2
odvrátil	odvrátit	k5eAaPmAgMnS
nebezpečí	nebezpečí	k1gNnSc4
<g/>
,	,	kIx,
hrozící	hrozící	k2eAgInSc4d1
obklíčeným	obklíčený	k2eAgMnSc7d1
římským	římský	k2eAgMnSc7d1
vojákům	voják	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejslavnější	slavný	k2eAgFnPc4d3
zpodobnění	zpodobnění	k1gNnPc4
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
představuje	představovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
bronzová	bronzový	k2eAgFnSc1d1
jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
renesance	renesance	k1gFnSc1
vystavená	vystavená	k1gFnSc1
Michelangelem	Michelangel	k1gInSc7
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Piazza	Piazz	k1gMnSc2
del	del	k?
Campidoglio	Campidoglio	k1gMnSc1
na	na	k7c6
římském	římský	k2eAgInSc6d1
Kapitolu	Kapitol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
replika	replika	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
originál	originál	k1gMnSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgMnS
v	v	k7c6
Kapitolských	kapitolský	k2eAgNnPc6d1
museích	museum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
socha	socha	k1gFnSc1
je	být	k5eAaImIp3nS
znázorněna	znázornit	k5eAaPmNgFnS
i	i	k9
na	na	k7c6
italské	italský	k2eAgFnSc6d1
versi	verse	k1gFnSc6
padesáticentové	padesáticentový	k2eAgMnPc4d1
euromince	eurominec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1
křesťanů	křesťan	k1gMnPc2
</s>
<s>
Marcus	Marcus	k1gMnSc1
provádějící	provádějící	k2eAgFnSc4d1
rituální	rituální	k2eAgFnSc4d1
oběť	oběť	k1gFnSc4
u	u	k7c2
oltáře	oltář	k1gInSc2
Jova	Jova	k1gMnSc1
Kapitolského	kapitolský	k2eAgInSc2d1
</s>
<s>
Přestože	přestože	k8xS
byl	být	k5eAaImAgInS
Marcus	Marcus	k1gInSc4
významný	významný	k2eAgMnSc1d1
myslitel	myslitel	k1gMnSc1
a	a	k8xC
filosof	filosof	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
<g/>
,	,	kIx,
alespoň	alespoň	k9
navenek	navenek	k6eAd1
<g/>
,	,	kIx,
hluboce	hluboko	k6eAd1
nábožensky	nábožensky	k6eAd1
založený	založený	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgInSc1d1
kult	kult	k1gInSc1
byl	být	k5eAaImAgInS
za	za	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
panování	panování	k1gNnSc2
náležitě	náležitě	k6eAd1
uctíván	uctívat	k5eAaImNgMnS
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
tolerována	tolerován	k2eAgFnSc1d1
i	i	k8xC
odlišná	odlišný	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
<g/>
,	,	kIx,
takže	takže	k8xS
různorodost	různorodost	k1gFnSc1
náboženství	náboženství	k1gNnSc2
nepůsobila	působit	k5eNaImAgFnS
žádné	žádný	k3yNgInPc4
rozpory	rozpor	k1gInPc4
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
a	a	k8xC
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
pravidla	pravidlo	k1gNnSc2
existovala	existovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
jediná	jediný	k2eAgFnSc1d1
výjimka	výjimka	k1gFnSc1
reprezentovaná	reprezentovaný	k2eAgFnSc1d1
křesťanstvím	křesťanství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
zaujímal	zaujímat	k5eAaImAgMnS
vůči	vůči	k7c3
křesťanům	křesťan	k1gMnPc3
stejné	stejný	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
<g/>
,	,	kIx,
jaké	jaký	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
prosazoval	prosazovat	k5eAaImAgMnS
už	už	k9
Traianus	Traianus	k1gMnSc1
<g/>
:	:	kIx,
pokud	pokud	k8xS
upustili	upustit	k5eAaPmAgMnP
od	od	k7c2
veřejného	veřejný	k2eAgNnSc2d1
vyznávání	vyznávání	k1gNnSc2
své	svůj	k3xOyFgFnSc2
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
neměla	mít	k5eNaImAgFnS
jim	on	k3xPp3gMnPc3
být	být	k5eAaImF
státní	státní	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
činěna	činěn	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
újma	újma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
v	v	k7c6
soukromém	soukromý	k2eAgInSc6d1
životě	život	k1gInSc6
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
křesťanství	křesťanství	k1gNnSc1
nerušeně	nerušeně	k6eAd1
praktikováno	praktikovat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
nebyla	být	k5eNaImAgFnS
tudíž	tudíž	k8xC
nijak	nijak	k6eAd1
výrazně	výrazně	k6eAd1
omezována	omezován	k2eAgFnSc1d1
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
enormnímu	enormní	k2eAgInSc3d1
rozmachu	rozmach	k1gInSc3
jejího	její	k3xOp3gInSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritické	kritický	k2eAgFnPc1d1
vnější	vnější	k2eAgFnPc1d1
i	i	k8xC
vnitřní	vnitřní	k2eAgFnPc1d1
okolnosti	okolnost	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
říše	říše	k1gFnSc1
ocitla	ocitnout	k5eAaPmAgFnS
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
způsobily	způsobit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
dosavadní	dosavadní	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
nebyla	být	k5eNaImAgFnS
respektována	respektován	k2eAgFnSc1d1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
byla	být	k5eAaImAgFnS
narušena	narušit	k5eAaPmNgFnS
osobní	osobní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
neradostné	radostný	k2eNgFnSc6d1
době	doba	k1gFnSc6
byla	být	k5eAaImAgNnP
vydávána	vydáván	k2eAgNnPc1d1
nařízení	nařízení	k1gNnPc1
vybízející	vybízející	k2eAgMnPc4d1
Římany	Říman	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
usmíření	usmíření	k1gNnSc3
bohů	bůh	k1gMnPc2
pravidelným	pravidelný	k2eAgNnSc7d1
prováděním	provádění	k1gNnSc7
žertev	žertva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
vzrůstaly	vzrůstat	k5eAaImAgFnP
represe	represe	k1gFnPc4
proti	proti	k7c3
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zdráhali	zdráhat	k5eAaImAgMnP
obětovat	obětovat	k5eAaBmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
postihovalo	postihovat	k5eAaImAgNnS
obzvláště	obzvláště	k6eAd1
křesťany	křesťan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
166	#num#	k4
až	až	k9
168	#num#	k4
byli	být	k5eAaImAgMnP
křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
především	především	k9
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
vystaveni	vystavit	k5eAaPmNgMnP
hněvu	hněv	k1gInSc3
ostatního	ostatní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
vyvolaného	vyvolaný	k2eAgInSc2d1
pravděpodobně	pravděpodobně	k6eAd1
šířením	šíření	k1gNnSc7
epidemie	epidemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřátelství	nepřátelství	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
zcela	zcela	k6eAd1
živelně	živelně	k6eAd1
bez	bez	k7c2
jakékoli	jakýkoli	k3yIgFnSc2
iniciativy	iniciativa	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
pronásledování	pronásledování	k1gNnSc1
křesťanů	křesťan	k1gMnPc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
Galii	Galie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
177	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
prapříčinu	prapříčina	k1gFnSc4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
tehdejší	tehdejší	k2eAgFnSc4d1
rozkolísanost	rozkolísanost	k1gFnSc4
veřejných	veřejný	k2eAgFnPc2d1
financí	finance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
doplnění	doplnění	k1gNnSc3
stavu	stav	k1gInSc2
legií	legie	k1gFnPc2
byly	být	k5eAaImAgInP
hojně	hojně	k6eAd1
užívány	užíván	k2eAgInPc1d1
odvody	odvod	k1gInPc1
gladiátorů	gladiátor	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
najímání	najímání	k1gNnPc2
za	za	k7c7
účelem	účel	k1gInSc7
pořádání	pořádání	k1gNnSc4
her	hra	k1gFnPc2
se	se	k3xPyFc4
tedy	tedy	k9
stávalo	stávat	k5eAaImAgNnS
stále	stále	k6eAd1
nákladnějším	nákladný	k2eAgInSc7d2
a	a	k8xC
problematičtějším	problematický	k2eAgInSc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nárůst	nárůst	k1gInSc4
výdajů	výdaj	k1gInPc2
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
konáním	konání	k1gNnSc7
gladiátorských	gladiátorský	k2eAgInPc2d1
soubojů	souboj	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
uhrazením	uhrazení	k1gNnSc7
byli	být	k5eAaImAgMnP
pověřeni	pověřen	k2eAgMnPc1d1
vykonavatelé	vykonavatel	k1gMnPc1
městské	městský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
provinciích	provincie	k1gFnPc6
<g/>
,	,	kIx,
záhy	záhy	k6eAd1
překročil	překročit	k5eAaPmAgMnS
únosnou	únosný	k2eAgFnSc4d1
mez	mez	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklé	vzniklý	k2eAgFnPc1d1
obtíže	obtíž	k1gFnPc1
přiměly	přimět	k5eAaPmAgFnP
císaře	císař	k1gMnPc4
k	k	k7c3
vydání	vydání	k1gNnSc3
usnesení	usnesení	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgNnSc2
směli	smět	k5eAaImAgMnP
být	být	k5eAaImF
odsouzení	odsouzený	k2eAgMnPc1d1
zločinci	zločinec	k1gMnPc1
prodáváni	prodávat	k5eAaImNgMnP
do	do	k7c2
arén	aréna	k1gFnPc2
jako	jako	k8xC,k8xS
gladiátoři	gladiátor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
městské	městský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
Lugdunu	Lugdun	k1gInSc6
(	(	kIx(
<g/>
Lyon	Lyon	k1gInSc1
<g/>
)	)	kIx)
hodlala	hodlat	k5eAaImAgFnS
tohoto	tento	k3xDgNnSc2
nařízení	nařízení	k1gNnSc2
využít	využít	k5eAaPmF
k	k	k7c3
zásahu	zásah	k1gInSc3
proti	proti	k7c3
tamním	tamní	k2eAgMnPc3d1
křesťanům	křesťan	k1gMnPc3
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
nezřekli	zřeknout	k5eNaPmAgMnP
své	svůj	k3xOyFgFnPc4
víry	víra	k1gFnPc4
<g/>
,	,	kIx,
hrozilo	hrozit	k5eAaImAgNnS
odsouzení	odsouzení	k1gNnSc1
místními	místní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
při	při	k7c6
volbě	volba	k1gFnSc6
křesťanů	křesťan	k1gMnPc2
zřejmě	zřejmě	k6eAd1
sehrálo	sehrát	k5eAaPmAgNnS
i	i	k8xC
nepřátelství	nepřátelství	k1gNnSc1
vůči	vůči	k7c3
cizincům	cizinec	k1gMnPc3
<g/>
,	,	kIx,
protože	protože	k8xS
mezi	mezi	k7c7
pozdějšími	pozdní	k2eAgMnPc7d2
mučedníky	mučedník	k1gMnPc7
bylo	být	k5eAaImAgNnS
zastoupeno	zastoupit	k5eAaPmNgNnS
mnoho	mnoho	k4c1
osob	osoba	k1gFnPc2
s	s	k7c7
řecky	řecky	k6eAd1
znějícími	znějící	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Příslušný	příslušný	k2eAgMnSc1d1
prokurátor	prokurátor	k1gMnSc1
vznesl	vznést	k5eAaPmAgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
dotaz	dotaz	k1gInSc4
ohledně	ohledně	k7c2
postupu	postup	k1gInSc2
v	v	k7c6
záležitosti	záležitost	k1gFnSc6
souzení	souzení	k1gNnSc1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
císařova	císařův	k2eAgInSc2d1
reskriptu	reskript	k1gInSc2
<g/>
,	,	kIx,
odkazujícího	odkazující	k2eAgInSc2d1
na	na	k7c4
Traianem	Traian	k1gInSc7
vydané	vydaný	k2eAgNnSc4d1
ustanovení	ustanovení	k1gNnSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
popraven	popravit	k5eAaPmNgMnS
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
veřejně	veřejně	k6eAd1
přiznal	přiznat	k5eAaPmAgMnS
k	k	k7c3
víře	víra	k1gFnSc3
v	v	k7c6
Krista	Krista	k1gFnSc1
odmítáním	odmítání	k1gNnSc7
konání	konání	k1gNnSc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťané	křesťan	k1gMnPc1
byli	být	k5eAaImAgMnP
poté	poté	k6eAd1
v	v	k7c6
lugdunské	lugdunský	k2eAgFnSc6d1
aréně	aréna	k1gFnSc6
nelítostně	lítostně	k6eNd1
zabíjeni	zabíjen	k2eAgMnPc1d1
pro	pro	k7c4
pobavení	pobavení	k1gNnSc4
obecenstva	obecenstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
Marcus	Marcus	k1gMnSc1
se	se	k3xPyFc4
osobně	osobně	k6eAd1
nezapojil	zapojit	k5eNaPmAgMnS
do	do	k7c2
těchto	tento	k3xDgFnPc2
protikřesťanských	protikřesťanský	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
,	,	kIx,
nijak	nijak	k6eAd1
jim	on	k3xPp3gMnPc3
ani	ani	k9
nebránil	bránit	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
lugdunském	lugdunský	k2eAgInSc6d1
masakru	masakr	k1gInSc6
nejsou	být	k5eNaImIp3nP
již	již	k6eAd1
z	z	k7c2
období	období	k1gNnSc2
Markova	Markův	k2eAgNnSc2d1
panování	panování	k1gNnSc2
zaznamenány	zaznamenat	k5eAaPmNgFnP
žádné	žádný	k3yNgFnPc1
jiné	jiný	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
stíhání	stíhání	k1gNnSc2
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bližším	blízký	k2eAgNnSc6d2
obeznámení	obeznámení	k1gNnSc6
se	se	k3xPyFc4
s	s	k7c7
touto	tento	k3xDgFnSc7
událostí	událost	k1gFnSc7
císař	císař	k1gMnSc1
patrně	patrně	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgMnS
o	o	k7c6
adekvátních	adekvátní	k2eAgNnPc6d1
opatřeních	opatření	k1gNnPc6
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
zamezil	zamezit	k5eAaPmAgMnS
obdobným	obdobný	k2eAgFnPc3d1
ukrutnostem	ukrutnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
do	do	k7c2
poloviny	polovina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
znamenaly	znamenat	k5eAaImAgInP
podstatnější	podstatný	k2eAgFnSc4d2
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
trvalejší	trvalý	k2eAgFnSc4d2
existenci	existence	k1gFnSc4
církve	církev	k1gFnSc2
spíše	spíše	k9
vnitřní	vnitřní	k2eAgInPc1d1
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
křesťanskými	křesťanský	k2eAgInPc7d1
směry	směr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Usurpace	usurpace	k1gFnSc1
Avidia	Avidium	k1gNnSc2
Cassia	Cassium	k1gNnSc2
</s>
<s>
Busta	busta	k1gFnSc1
Faustiny	Faustina	k1gFnSc2
mladší	mladý	k2eAgFnSc1d2
v	v	k7c6
Louvru	Louvre	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
175	#num#	k4
se	se	k3xPyFc4
vzbouřil	vzbouřit	k5eAaPmAgMnS
syrský	syrský	k2eAgMnSc1d1
místodržitel	místodržitel	k1gMnSc1
Avidius	Avidius	k1gMnSc1
Cassius	Cassius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčiny	příčina	k1gFnPc4
této	tento	k3xDgFnSc2
usurpace	usurpace	k1gFnSc2
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
objasněny	objasněn	k2eAgFnPc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
Historia	Historium	k1gNnSc2
Augusta	August	k1gMnSc2
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
císařovna	císařovna	k1gFnSc1
Faustina	Faustina	k1gFnSc1
znepokojená	znepokojený	k2eAgNnPc4d1
nedobrým	dobrý	k2eNgInSc7d1
zdravotním	zdravotní	k2eAgInSc7d1
stavem	stav	k1gInSc7
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
navázala	navázat	k5eAaPmAgFnS
s	s	k7c7
Cassiem	Cassium	k1gNnSc7
kontakt	kontakt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faustina	Faustina	k1gFnSc1
se	se	k3xPyFc4
nejspíše	nejspíše	k9
strachovala	strachovat	k5eAaImAgFnS
o	o	k7c4
další	další	k2eAgNnSc4d1
trvání	trvání	k1gNnSc4
dynastie	dynastie	k1gFnSc2
<g/>
,	,	kIx,
poněvadž	poněvadž	k8xS
její	její	k3xOp3gMnSc1
jediný	jediný	k2eAgMnSc1d1
žijící	žijící	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Commodus	Commodus	k1gMnSc1
byl	být	k5eAaImAgMnS
dosud	dosud	k6eAd1
příliš	příliš	k6eAd1
mladý	mladý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
obávala	obávat	k5eAaImAgFnS
ztráty	ztráta	k1gFnPc4
své	svůj	k3xOyFgFnSc2
posice	posice	k1gFnSc2
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
po	po	k7c6
Markově	Markův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
stal	stát	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
než	než	k8xS
Commodus	Commodus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpoura	vzpoura	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
též	též	k9
vyprovokována	vyprovokován	k2eAgFnSc1d1
zvěstmi	zvěst	k1gFnPc7
o	o	k7c6
Markově	Markův	k2eAgNnSc6d1
úmrtí	úmrtí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avidius	Avidius	k1gMnSc1
Cassius	Cassius	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
výborně	výborně	k6eAd1
osvědčil	osvědčit	k5eAaPmAgInS
jako	jako	k9
velitel	velitel	k1gMnSc1
v	v	k7c6
parthské	parthský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c6
Východě	východ	k1gInSc6
těšil	těšit	k5eAaImAgInS
značné	značný	k2eAgFnSc3d1
popularitě	popularita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
později	pozdě	k6eAd2
seznal	seznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
císař	císař	k1gMnSc1
je	být	k5eAaImIp3nS
naživu	naživu	k6eAd1
<g/>
,	,	kIx,
místo	místo	k7c2
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Markovi	Marek	k1gMnSc3
podrobil	podrobit	k5eAaPmAgMnS
<g/>
,	,	kIx,
přikročil	přikročit	k5eAaPmAgMnS
ke	k	k7c3
konfrontaci	konfrontace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
početní	početní	k2eAgFnSc3d1
přesile	přesila	k1gFnSc3
dunajských	dunajský	k2eAgFnPc2d1
legií	legie	k1gFnPc2
nebyly	být	k5eNaImAgFnP
ale	ale	k9
Cassiovy	Cassiův	k2eAgFnPc1d1
vyhlídky	vyhlídka	k1gFnPc1
příliš	příliš	k6eAd1
nadějné	nadějný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cassius	Cassius	k1gInSc1
byl	být	k5eAaImAgInS
proto	proto	k8xC
vlastními	vlastní	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
zavražděn	zavražděn	k2eAgInSc4d1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mohla	moct	k5eAaImAgFnS
naplno	naplno	k6eAd1
rozhořet	rozhořet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
na	na	k7c6
Dunaji	Dunaj	k1gInSc6
dostihl	dostihnout	k5eAaPmAgMnS
Marka	Marek	k1gMnSc2
Commodus	Commodus	k1gInSc1
přivolaný	přivolaný	k2eAgInSc1d1
z	z	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
vzápětí	vzápětí	k6eAd1
uzavřel	uzavřít	k5eAaPmAgMnS
mír	mír	k1gInSc4
se	se	k3xPyFc4
Jazygy	Jazyga	k1gFnSc2
a	a	k8xC
neprodleně	prodleně	k6eNd1
se	se	k3xPyFc4
vypravil	vypravit	k5eAaPmAgInS
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vypořádal	vypořádat	k5eAaPmAgMnS
s	s	k7c7
tamější	tamější	k2eAgFnSc7d1
neklidnou	klidný	k2eNgFnSc7d1
situací	situace	k1gFnSc7
a	a	k8xC
obnovil	obnovit	k5eAaPmAgInS
pořádek	pořádek	k1gInSc4
v	v	k7c6
provinciích	provincie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
si	se	k3xPyFc3
počínal	počínat	k5eAaImAgInS
velice	velice	k6eAd1
ohleduplně	ohleduplně	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
promíjel	promíjet	k5eAaImAgMnS
městům	město	k1gNnPc3
i	i	k8xC
jednotlivcům	jednotlivec	k1gMnPc3
účast	účast	k1gFnSc4
na	na	k7c6
Cassiově	Cassiův	k2eAgFnSc6d1
vzpouře	vzpoura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
prokázal	prokázat	k5eAaPmAgInS
svoji	svůj	k3xOyFgFnSc4
moudrost	moudrost	k1gFnSc4
a	a	k8xC
shovívavost	shovívavost	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
velkoryse	velkoryse	k6eAd1
přesvědčil	přesvědčit	k5eAaPmAgInS
senát	senát	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
omilostnil	omilostnit	k5eAaPmAgMnS
všechny	všechen	k3xTgMnPc4
členy	člen	k1gMnPc4
usurpátorovy	usurpátorův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
četl	číst	k5eAaImAgMnS
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
spálit	spálit	k5eAaPmF
Cassiovu	Cassiův	k2eAgFnSc4d1
korespondenci	korespondence	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
mohla	moct	k5eAaImAgFnS
kompromitovat	kompromitovat	k5eAaBmF
leckteré	leckterý	k3yIgMnPc4
senátory	senátor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
cesty	cesta	k1gFnSc2
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
zimě	zima	k1gFnSc6
175	#num#	k4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Kappadokii	Kappadokie	k1gFnSc6
Markova	Markův	k2eAgFnSc1d1
choť	choť	k1gFnSc1
Faustina	Faustina	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
pověst	pověst	k1gFnSc1
utrpěla	utrpět	k5eAaPmAgFnS
znatelné	znatelný	k2eAgInPc4d1
šrámy	šrám	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
antických	antický	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
je	být	k5eAaImIp3nS
obviňována	obviňován	k2eAgFnSc1d1
z	z	k7c2
užívání	užívání	k1gNnSc2
jedů	jed	k1gInPc2
a	a	k8xC
vraždění	vraždění	k1gNnSc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
z	z	k7c2
provozování	provozování	k1gNnSc2
neřestného	řestný	k2eNgInSc2d1
života	život	k1gInSc2
udržováním	udržování	k1gNnSc7
styků	styk	k1gInPc2
s	s	k7c7
gladiátory	gladiátor	k1gMnPc7
<g/>
,	,	kIx,
námořníky	námořník	k1gMnPc7
i	i	k8xC
muži	muž	k1gMnPc1
vyššího	vysoký	k2eAgNnSc2d2
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gInSc1
jí	on	k3xPp3gFnSc7
ovšem	ovšem	k9
bezvýhradně	bezvýhradně	k6eAd1
důvěřoval	důvěřovat	k5eAaImAgMnS
a	a	k8xC
vytrvale	vytrvale	k6eAd1
ji	on	k3xPp3gFnSc4
hájil	hájit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faustina	Faustina	k1gFnSc1
ho	on	k3xPp3gMnSc4
doprovázela	doprovázet	k5eAaImAgFnS
na	na	k7c6
četných	četný	k2eAgInPc6d1
taženích	tažení	k1gNnPc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
si	se	k3xPyFc3
od	od	k7c2
něho	on	k3xPp3gNnSc2
vysloužila	vysloužit	k5eAaPmAgFnS
titul	titul	k1gInSc4
Matka	matka	k1gFnSc1
táborů	tábor	k1gInPc2
(	(	kIx(
<g/>
Mater	Matra	k1gFnPc2
castrorum	castrorum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
jí	jíst	k5eAaImIp3nS
zasvětil	zasvětit	k5eAaPmAgMnS
chrám	chrám	k1gInSc4
a	a	k8xC
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zemřela	zemřít	k5eAaPmAgFnS
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
přejmenovat	přejmenovat	k5eAaPmF
na	na	k7c4
Faustinopolis	Faustinopolis	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
dokončil	dokončit	k5eAaPmAgInS
konsolidaci	konsolidace	k1gFnSc4
východních	východní	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
<g/>
,	,	kIx,
vyrazil	vyrazit	k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
zpět	zpět	k6eAd1
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
krátkou	krátký	k2eAgFnSc4d1
zastávku	zastávka	k1gFnSc4
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
podpořil	podpořit	k5eAaPmAgMnS
místní	místní	k2eAgFnPc4d1
filosofické	filosofický	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Následnictví	následnictví	k1gNnSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
</s>
<s>
Koncem	koncem	k7c2
prosince	prosinec	k1gInSc2
176	#num#	k4
oslavil	oslavit	k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
synem	syn	k1gMnSc7
Commodem	Commod	k1gMnSc7
v	v	k7c6
Římě	Řím	k1gInSc6
triumf	triumf	k1gInSc1
nad	nad	k7c7
Germány	Germán	k1gMnPc7
a	a	k8xC
Sarmaty	Sarmat	k1gMnPc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
obdařil	obdařit	k5eAaPmAgMnS
Commoda	Commoda	k1gMnSc1
titulem	titul	k1gInSc7
augustus	augustus	k1gMnSc1
a	a	k8xC
ustavil	ustavit	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
svým	svůj	k3xOyFgMnSc7
spoluvládcem	spoluvládce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
tak	tak	k9
dal	dát	k5eAaPmAgMnS
zřetelně	zřetelně	k6eAd1
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
po	po	k7c6
něm	on	k3xPp3gNnSc6
nastoupí	nastoupit	k5eAaPmIp3nS
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
toto	tento	k3xDgNnSc1
jeho	jeho	k3xOp3gNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
mnohými	mnohý	k2eAgFnPc7d1
historiky	historik	k1gMnPc4
hodnoceno	hodnocen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
závažné	závažný	k2eAgNnSc1d1
pochybení	pochybení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úmyslně	úmyslně	k6eAd1
totiž	totiž	k9
odmítl	odmítnout	k5eAaPmAgMnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
tradici	tradice	k1gFnSc6
předávání	předávání	k1gNnSc2
vlády	vláda	k1gFnSc2
nad	nad	k7c7
říší	říš	k1gFnSc7
adopcí	adopce	k1gFnPc2
nejzdatnějšího	zdatný	k2eAgMnSc2d3
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
mu	on	k3xPp3gMnSc3
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
známo	znám	k2eAgNnSc1d1
Commodovo	Commodův	k2eAgNnSc1d1
neuspokojivé	uspokojivý	k2eNgNnSc1d1
chování	chování	k1gNnSc1
<g/>
,	,	kIx,
diskvalifikující	diskvalifikující	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
z	z	k7c2
výkonu	výkon	k1gInSc2
císařské	císařský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
se	se	k3xPyFc4
nepochybně	pochybně	k6eNd1
zaobíral	zaobírat	k5eAaImAgInS
oběma	dva	k4xCgMnPc7
těmito	tento	k3xDgNnPc7
hledisky	hledisko	k1gNnPc7
a	a	k8xC
nakonec	nakonec	k6eAd1
je	být	k5eAaImIp3nS
zohlednil	zohlednit	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
výsledném	výsledný	k2eAgNnSc6d1
rozhodnutí	rozhodnutí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
adoptivní	adoptivní	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
nezanechali	zanechat	k5eNaPmAgMnP
žádné	žádný	k3yNgMnPc4
vlastní	vlastní	k2eAgMnPc4d1
mužské	mužský	k2eAgMnPc4d1
dědice	dědic	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
učinit	učinit	k5eAaPmF,k5eAaImF
svými	svůj	k3xOyFgMnPc7
nástupci	nástupce	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nárok	nárok	k1gInSc1
Commoda	Commod	k1gMnSc4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
již	již	k6eAd1
v	v	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
jako	jako	k8xC,k8xS
předpokládanému	předpokládaný	k2eAgMnSc3d1
pretendentu	pretendent	k1gMnSc3
trůnu	trůn	k1gInSc2
udělen	udělen	k2eAgInSc4d1
titul	titul	k1gInSc4
caesara	caesar	k1gMnSc2
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
proto	proto	k8xC
dosavadní	dosavadní	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
adopcí	adopce	k1gFnPc2
nijak	nijak	k6eAd1
zpochybněn	zpochybnit	k5eAaPmNgInS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
po	po	k7c6
formální	formální	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
nebyla	být	k5eNaImAgFnS
císařská	císařský	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
dědičná	dědičný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
vývoji	vývoj	k1gInSc3
událostí	událost	k1gFnPc2
lze	lze	k6eAd1
soudit	soudit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
konečným	konečný	k2eAgInSc7d1
impulsem	impuls	k1gInSc7
ke	k	k7c3
Commodově	Commodův	k2eAgFnSc3d1
povýšení	povýšení	k1gNnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
usurpace	usurpace	k1gFnSc1
Avidia	Avidium	k1gNnSc2
Cassia	Cassia	k1gFnSc1
<g/>
,	,	kIx,
naznačující	naznačující	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
císařovo	císařův	k2eAgNnSc4d1
nevalné	valný	k2eNgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
jeho	jeho	k3xOp3gNnSc4
úmrtí	úmrtí	k1gNnSc4
by	by	kYmCp3nP
mohly	moct	k5eAaImAgInP
vyvolat	vyvolat	k5eAaPmF
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
otázka	otázka	k1gFnSc1
následnictví	následnictví	k1gNnSc2
nebyla	být	k5eNaImAgFnS
včas	včas	k6eAd1
upravena	upravit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dané	daný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
se	se	k3xPyFc4
tedy	tedy	k9
nejjistějším	jistý	k2eAgInSc6d3
a	a	k8xC
nejméně	málo	k6eAd3
zpochybnitelným	zpochybnitelný	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
jevila	jevit	k5eAaImAgFnS
být	být	k5eAaImF
volba	volba	k1gFnSc1
Commoda	Commoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Commodus	Commodus	k1gMnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
si	se	k3xPyFc3
sice	sice	k8xC
všiml	všimnout	k5eAaPmAgMnS
problematických	problematický	k2eAgInPc2d1
povahových	povahový	k2eAgInPc2d1
rysů	rys	k1gInPc2
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
ale	ale	k8xC
setrval	setrvat	k5eAaPmAgMnS
v	v	k7c4
naději	naděje	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
dospívání	dospívání	k1gNnSc2
doroste	dorůst	k5eAaPmIp3nS
Commodus	Commodus	k1gInSc1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
budoucí	budoucí	k2eAgFnSc2d1
role	role	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
některým	některý	k3yIgMnPc3
svým	svůj	k3xOyFgFnPc3
vzdálenějším	vzdálený	k2eAgFnPc3d2
příbuzným	příbuzná	k1gFnPc3
nebo	nebo	k8xC
jiným	jiný	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
by	by	kYmCp3nS
patrně	patrně	k6eAd1
v	v	k7c6
zájmu	zájem	k1gInSc6
zajištění	zajištění	k1gNnSc2
své	svůj	k3xOyFgFnSc2
moci	moc	k1gFnSc2
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
Commodův	Commodův	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
175	#num#	k4
až	až	k9
do	do	k7c2
března	březen	k1gInSc2
180	#num#	k4
doprovázel	doprovázet	k5eAaImAgMnS
Commodus	Commodus	k1gMnSc1
<g/>
,	,	kIx,
potvrzený	potvrzený	k2eAgMnSc1d1
už	už	k6eAd1
jako	jako	k8xS,k8xC
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
cestách	cesta	k1gFnPc6
a	a	k8xC
válečných	válečný	k2eAgNnPc6d1
taženích	tažení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
způsob	způsob	k1gInSc1
a	a	k8xC
délka	délka	k1gFnSc1
Commodovy	Commodův	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
na	na	k7c4
císařskou	císařský	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
zůstaly	zůstat	k5eAaPmAgInP
nesrovnatelné	srovnatelný	k2eNgInPc1d1
s	s	k7c7
možnostmi	možnost	k1gFnPc7
<g/>
,	,	kIx,
jakými	jaký	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
disponoval	disponovat	k5eAaBmAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Commodus	Commodus	k1gInSc1
začal	začít	k5eAaPmAgInS
brzy	brzy	k6eAd1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
intronisaci	intronisace	k1gFnSc6
vystupovat	vystupovat	k5eAaImF
jako	jako	k9
gladiátor	gladiátor	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
vydával	vydávat	k5eAaPmAgInS,k5eAaImAgInS
za	za	k7c4
Herkula	Herkul	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určití	určitý	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
tuto	tento	k3xDgFnSc4
stylisaci	stylisace	k1gFnSc4
nepovažují	považovat	k5eNaImIp3nP
za	za	k7c4
natolik	natolik	k6eAd1
scestnou	scestný	k2eAgFnSc4d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
může	moct	k5eAaImIp3nS
jevit	jevit	k5eAaImF
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Herkules	Herkules	k1gMnSc1
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
symbol	symbol	k1gInSc4
neúnavného	únavný	k2eNgMnSc2d1
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ve	v	k7c6
shodě	shoda	k1gFnSc6
se	s	k7c7
stoickými	stoický	k2eAgFnPc7d1
zásadami	zásada	k1gFnPc7
očisťuje	očisťovat	k5eAaImIp3nS
svět	svět	k1gInSc1
od	od	k7c2
útrap	útrapa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
178	#num#	k4
zamířili	zamířit	k5eAaPmAgMnP
Marcus	Marcus	k1gMnSc1
a	a	k8xC
Commodus	Commodus	k1gMnSc1
zpět	zpět	k6eAd1
k	k	k7c3
Dunaji	Dunaj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
výpravy	výprava	k1gFnSc2
podlehl	podlehnout	k5eAaPmAgMnS
císař	císař	k1gMnSc1
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
180	#num#	k4
blíže	blízce	k6eAd2
neznámé	známý	k2eNgFnSc3d1
chorobě	choroba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
badatelé	badatel	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
příčinu	příčina	k1gFnSc4
smrti	smrt	k1gFnSc2
antoninovský	antoninovský	k2eAgInSc1d1
mor	mor	k1gInSc1
<g/>
,	,	kIx,
jiní	jiný	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
trpěl	trpět	k5eAaImAgInS
nádorovým	nádorový	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
jako	jako	k9
místo	místo	k7c2
Markova	Markův	k2eAgNnSc2d1
úmrtí	úmrtí	k1gNnSc2
udává	udávat	k5eAaImIp3nS
Vindobona	Vindobona	k1gFnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
Tertullianus	Tertullianus	k1gMnSc1
podotýká	podotýkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Sirmiu	Sirmium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
nemoci	nemoc	k1gFnSc2
a	a	k8xC
v	v	k7c6
předtuše	předtucha	k1gFnSc6
blížící	blížící	k2eAgFnSc6d1
se	se	k3xPyFc4
smrti	smrt	k1gFnSc6
si	se	k3xPyFc3
Marcus	Marcus	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
předvolat	předvolat	k5eAaPmF
Commoda	Commoda	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
upomínal	upomínat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vytrval	vytrvat	k5eAaPmAgMnS
v	v	k7c4
tažení	tažení	k1gNnSc4
proti	proti	k7c3
barbarům	barbar	k1gMnPc3
až	až	k8xS
do	do	k7c2
završení	završení	k1gNnSc2
vítězství	vítězství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Commodus	Commodus	k1gInSc1
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
ze	z	k7c2
strachu	strach	k1gInSc2
z	z	k7c2
nákazy	nákaza	k1gFnSc2
spěchal	spěchat	k5eAaImAgMnS
rychle	rychle	k6eAd1
vzdálit	vzdálit	k5eAaPmF
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
blízkosti	blízkost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
si	se	k3xPyFc3
Marcus	Marcus	k1gMnSc1
přál	přát	k5eAaImAgMnS
urychlit	urychlit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
konec	konec	k1gInSc4
<g/>
,	,	kIx,
odmítal	odmítat	k5eAaImAgMnS
přijímat	přijímat	k5eAaImF
potravu	potrava	k1gFnSc4
a	a	k8xC
pití	pití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dochovaného	dochovaný	k2eAgNnSc2d1
podání	podání	k1gNnSc2
se	se	k3xPyFc4
krátce	krátce	k6eAd1
před	před	k7c7
svým	svůj	k3xOyFgInSc7
skonem	skon	k1gInSc7
obrátil	obrátit	k5eAaPmAgMnS
k	k	k7c3
truchlícím	truchlící	k2eAgMnPc3d1
přátelům	přítel	k1gMnPc3
se	s	k7c7
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Proč	proč	k6eAd1
pláčete	plakat	k5eAaImIp2nP
nade	nad	k7c7
mnou	já	k3xPp1nSc7
a	a	k8xC
nesnažíte	snažit	k5eNaImIp2nP
se	se	k3xPyFc4
raději	rád	k6eAd2
zabránit	zabránit	k5eAaPmF
moru	mor	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
kosí	kosit	k5eAaImIp3nS
lidi	člověk	k1gMnPc4
hromadně	hromadně	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
byly	být	k5eAaImAgInP
pochovány	pochovat	k5eAaPmNgInP
v	v	k7c6
Římě	Řím	k1gInSc6
v	v	k7c6
Hadriánově	Hadriánův	k2eAgNnSc6d1
mausoleu	mausoleum	k1gNnSc6
<g/>
,	,	kIx,
známém	známý	k2eAgNnSc6d1
dnes	dnes	k6eAd1
jako	jako	k9
Andělský	andělský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Markova	Markův	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
</s>
<s>
Aquincum	Aquincum	k1gNnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Budapešť	Budapešť	k1gFnSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Marcus	Marcus	k1gInSc1
tvořil	tvořit	k5eAaImAgInS
své	svůj	k3xOyFgNnSc4
dílo	dílo	k1gNnSc4
„	„	k?
<g/>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
“	“	k?
</s>
<s>
Filosofické	filosofický	k2eAgInPc1d1
postoje	postoj	k1gInPc1
Markových	Markových	k2eAgMnPc2d1
učitelů	učitel	k1gMnPc2
výrazně	výrazně	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
příklon	příklon	k1gInSc4
ke	k	k7c3
stoicismu	stoicismus	k1gInSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
role	role	k1gFnSc1
v	v	k7c6
procesu	proces	k1gInSc6
utváření	utváření	k1gNnSc2
Markovy	Markův	k2eAgFnSc2d1
filosofické	filosofický	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
příslušela	příslušet	k5eAaImAgFnS
Epiktétovi	Epiktét	k1gMnSc3
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
dílo	dílo	k1gNnSc4
Rozpravy	rozprava	k1gFnPc1
(	(	kIx(
<g/>
Diatribai	Diatriba	k1gFnPc1
<g/>
)	)	kIx)
si	se	k3xPyFc3
velice	velice	k6eAd1
oblíbil	oblíbit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlubšího	hluboký	k2eAgNnSc2d2
poznání	poznání	k1gNnSc2
Markových	Markových	k2eAgFnPc2d1
filosofických	filosofický	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
<g/>
,	,	kIx,
přesvědčení	přesvědčení	k1gNnSc2
a	a	k8xC
zásad	zásada	k1gFnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
se	se	k3xPyFc4
řídil	řídit	k5eAaImAgMnS
<g/>
,	,	kIx,
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
studiem	studio	k1gNnSc7
toho	ten	k3xDgNnSc2
nejcennějšího	cenný	k2eAgNnSc2d3
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
po	po	k7c6
sobě	se	k3xPyFc3
zanechal	zanechat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
díla	dílo	k1gNnSc2
„	„	k?
<g/>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
“	“	k?
(	(	kIx(
<g/>
Ta	ten	k3xDgFnSc1
eis	eis	k1gNnSc4
heauton	heauton	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Meditations	Meditations	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
zamýšlel	zamýšlet	k5eAaImAgMnS
obeznámit	obeznámit	k5eAaPmF
lidstvo	lidstvo	k1gNnSc4
s	s	k7c7
tímto	tento	k3xDgInSc7
spisem	spis	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
kusé	kusý	k2eAgFnPc4d1
a	a	k8xC
místy	místy	k6eAd1
rozvláčné	rozvláčný	k2eAgFnPc4d1
osobní	osobní	k2eAgFnPc4d1
poznámky	poznámka	k1gFnPc4
a	a	k8xC
úvahy	úvaha	k1gFnPc4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
při	při	k7c6
vedení	vedení	k1gNnSc6
vojenských	vojenský	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
a	a	k8xC
řízení	řízení	k1gNnSc2
administrativních	administrativní	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
spíše	spíše	k9
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
jejich	jejich	k3xOp3gNnSc7
čtením	čtení	k1gNnSc7
dodal	dodat	k5eAaPmAgMnS
odvahy	odvaha	k1gFnSc2
ke	k	k7c3
snášení	snášení	k1gNnSc3
tíživé	tíživý	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
představovalo	představovat	k5eAaImAgNnS
břímě	břímě	k1gNnSc1
vládnutí	vládnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
si	se	k3xPyFc3
v	v	k7c6
něm	on	k3xPp3gInSc6
neustále	neustále	k6eAd1
vytyčoval	vytyčovat	k5eAaImAgMnS
obtížně	obtížně	k6eAd1
dosažitelné	dosažitelný	k2eAgInPc4d1
principy	princip	k1gInPc4
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
rozvažoval	rozvažovat	k5eAaImAgMnS
nad	nad	k7c7
všedností	všednost	k1gFnSc7
<g/>
,	,	kIx,
nekultivovaností	nekultivovanost	k1gFnSc7
a	a	k8xC
pomíjivostí	pomíjivost	k1gFnSc7
hmotného	hmotný	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabýval	zabývat	k5eAaImAgMnS
se	se	k3xPyFc4
také	také	k9
úlohou	úloha	k1gFnSc7
člověka	člověk	k1gMnSc2
ve	v	k7c6
světě	svět	k1gInSc6
a	a	k8xC
zvláště	zvláště	k6eAd1
sebe	sebe	k3xPyFc4
samého	samý	k3xTgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevěřil	věřit	k5eNaImAgMnS
v	v	k7c4
žádný	žádný	k3yNgInSc4
jiný	jiný	k2eAgInSc4d1
svět	svět	k1gInSc4
a	a	k8xC
cítil	cítit	k5eAaImAgMnS
se	se	k3xPyFc4
beznadějně	beznadějně	k6eAd1
připoután	připoután	k2eAgMnSc1d1
ke	k	k7c3
svým	svůj	k3xOyFgFnPc3
povinnostem	povinnost	k1gFnPc3
a	a	k8xC
službě	služba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytrvale	vytrvale	k6eAd1
se	se	k3xPyFc4
proměňující	proměňující	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
lidská	lidský	k2eAgFnSc1d1
duše	duše	k1gFnSc1
<g/>
,	,	kIx,
ztělesňoval	ztělesňovat	k5eAaImAgMnS
podle	podle	k7c2
Marka	Marek	k1gMnSc2
celek	celek	k1gInSc1
řízený	řízený	k2eAgInSc1d1
rozumem	rozum	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
mohl	moct	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
i	i	k9
ve	v	k7c6
stavu	stav	k1gInSc6
bezbrannosti	bezbrannost	k1gFnSc2
a	a	k8xC
osamocenosti	osamocenost	k1gFnSc2
obstát	obstát	k5eAaPmF
tváří	tvář	k1gFnSc7
v	v	k7c4
tvář	tvář	k1gFnSc4
chaosu	chaos	k1gInSc2
a	a	k8xC
marnosti	marnost	k1gFnSc2
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
nezkažený	zkažený	k2eNgInSc1d1
a	a	k8xC
ctnostný	ctnostný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Markovo	Markův	k2eAgNnSc4d1
chápání	chápání	k1gNnSc4
bytí	bytí	k1gNnSc2
byl	být	k5eAaImAgInS
prvořadý	prvořadý	k2eAgInSc1d1
soulad	soulad	k1gInSc1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
konání	konání	k1gNnSc2
resp.	resp.	kA
jednota	jednota	k1gFnSc1
slov	slovo	k1gNnPc2
a	a	k8xC
činů	čin	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
odráželo	odrážet	k5eAaImAgNnS
i	i	k9
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
vnímání	vnímání	k1gNnSc6
posuzování	posuzování	k1gNnSc2
a	a	k8xC
realizování	realizování	k1gNnSc2
vládnutí	vládnutí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
spolu	spolu	k6eAd1
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
úzce	úzko	k6eAd1
sdruženy	sdružen	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Přestaň	přestat	k5eAaPmRp2nS
už	už	k6eAd1
vykládat	vykládat	k5eAaImF
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
asi	asi	k9
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dobrý	dobrý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
;	;	kIx,
raději	rád	k6eAd2
už	už	k6eAd1
jím	jíst	k5eAaImIp1nS
buď	buď	k8xC
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Ani	ani	k8xC
v	v	k7c6
psaní	psaní	k1gNnSc6
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
čtení	čtení	k1gNnSc6
nebudeš	být	k5eNaImBp2nS
moci	moct	k5eAaImF
dávat	dávat	k5eAaImF
návod	návod	k1gInSc4
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
dříve	dříve	k6eAd2
návodu	návod	k1gInSc2
nepodrobíš	podrobit	k5eNaPmIp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
to	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
v	v	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
kladl	klást	k5eAaImAgInS
na	na	k7c4
smysl	smysl	k1gInSc4
pro	pro	k7c4
pravdu	pravda	k1gFnSc4
a	a	k8xC
pro	pro	k7c4
realitu	realita	k1gFnSc4
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
si	se	k3xPyFc3
na	na	k7c6
něm	on	k3xPp3gInSc6
cenil	cenit	k5eAaImAgInS
Hadrianus	Hadrianus	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Jestliže	jestliže	k8xS
mi	já	k3xPp1nSc3
někdo	někdo	k3yInSc1
dovede	dovést	k5eAaPmIp3nS
přesvědčivě	přesvědčivě	k6eAd1
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
nesprávně	správně	k6eNd1
soudím	soudit	k5eAaImIp1nS
nebo	nebo	k8xC
jednám	jednat	k5eAaImIp1nS
<g/>
,	,	kIx,
milerád	milerád	k2eAgMnSc1d1
své	svůj	k3xOyFgNnSc4
mínění	mínění	k1gNnSc4
změním	změnit	k5eAaPmIp1nS
<g/>
;	;	kIx,
neboť	neboť	k8xC
hledám	hledat	k5eAaImIp1nS
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
ještě	ještě	k6eAd1
nikdy	nikdy	k6eAd1
nikdo	nikdo	k3yNnSc1
škodu	škoda	k1gFnSc4
neutrpěl	utrpět	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škodu	škoda	k1gFnSc4
mívá	mívat	k5eAaImIp3nS
leda	leda	k8xS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
setrvává	setrvávat	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
klamu	klam	k1gInSc6
a	a	k8xC
nevědomosti	nevědomost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Kdykoli	kdykoli	k6eAd1
tě	ty	k3xPp2nSc4
roztrpčí	roztrpčit	k5eAaPmIp3nS
něčí	něčí	k3xOyIgNnSc1
nestoudnost	nestoudnost	k1gFnSc1
<g/>
,	,	kIx,
ihned	ihned	k6eAd1
se	se	k3xPyFc4
ptej	ptat	k5eAaImRp2nS
sebe	sebe	k3xPyFc4
sama	sám	k3xTgMnSc4
<g/>
:	:	kIx,
Cožpak	cožpak	k9
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nebyli	být	k5eNaImAgMnP
ve	v	k7c6
světě	svět	k1gInSc6
nestoudní	stoudný	k2eNgMnPc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nežádej	žádat	k5eNaImRp2nS
tedy	tedy	k8xC
nemožnosti	nemožnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
byly	být	k5eAaImAgFnP
jakousi	jakýsi	k3yIgFnSc7
formou	forma	k1gFnSc7
duševního	duševní	k2eAgNnSc2d1
cvičení	cvičení	k1gNnSc2
<g/>
,	,	kIx,
zaměřeného	zaměřený	k2eAgNnSc2d1
na	na	k7c6
zachování	zachování	k1gNnSc6
a	a	k8xC
rozvíjení	rozvíjení	k1gNnSc6
způsobu	způsob	k1gInSc2
života	život	k1gInSc2
a	a	k8xC
vědomí	vědomí	k1gNnSc2
odpovídajícímu	odpovídající	k2eAgInSc3d1
zásadám	zásada	k1gFnPc3
stoicismu	stoicismus	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
na	na	k7c4
kontrolu	kontrola	k1gFnSc4
emocí	emoce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Markově	Markův	k2eAgInSc6d1
postoji	postoj	k1gInSc6
k	k	k7c3
lidem	člověk	k1gMnPc3
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
stvořeni	stvořit	k5eAaPmNgMnP
kvůli	kvůli	k7c3
sobě	se	k3xPyFc3
navzájem	navzájem	k6eAd1
<g/>
;	;	kIx,
buď	buď	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k8xC
poučuj	poučovat	k5eAaImRp2nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
snášej	snášet	k5eAaImRp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Kdykoli	kdykoli	k6eAd1
se	se	k3xPyFc4
chceš	chtít	k5eAaImIp2nS
potěšit	potěšit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
duchu	duch	k1gMnSc6
si	se	k3xPyFc3
vybavuj	vybavovat	k5eAaImRp2nS
přednosti	přednost	k1gFnPc4
svých	svůj	k3xOyFgMnPc2
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
podnikavost	podnikavost	k1gFnSc1
jednoho	jeden	k4xCgInSc2
<g/>
,	,	kIx,
skromnost	skromnost	k1gFnSc4
druhého	druhý	k4xOgMnSc4
<g/>
,	,	kIx,
štědrost	štědrost	k1gFnSc4
třetího	třetí	k4xOgNnSc2
a	a	k8xC
jinou	jiný	k2eAgFnSc4d1
ctnost	ctnost	k1gFnSc4
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
nic	nic	k3yNnSc1
nepůsobí	působit	k5eNaImIp3nS
takové	takový	k3xDgNnSc4
potěšení	potěšení	k1gNnSc4
jako	jako	k8xS,k8xC
vzory	vzor	k1gInPc4
ctností	ctnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zrcadlí	zrcadlit	k5eAaImIp3nP
v	v	k7c6
mravech	mrav	k1gInPc6
našich	náš	k3xOp1gMnPc2
vrstevníků	vrstevník	k1gMnPc2
a	a	k8xC
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
v	v	k7c6
jednotlivci	jednotlivec	k1gMnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
možno	možno	k6eAd1
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc4
slučují	slučovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
také	také	k9
záhodno	záhodno	k6eAd1
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
oživovat	oživovat	k5eAaImF
v	v	k7c6
paměti	paměť	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úvahy	úvaha	k1gFnPc1
v	v	k7c6
duchu	duch	k1gMnSc6
stoy	stoa	k1gFnSc2
mu	on	k3xPp3gMnSc3
měly	mít	k5eAaImAgFnP
napomoci	napomoct	k5eAaPmF
rovněž	rovněž	k9
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přestál	přestát	k5eAaPmAgInS
rozmanitá	rozmanitý	k2eAgNnPc4d1
protivenství	protivenství	k1gNnSc4
<g/>
,	,	kIx,
odolal	odolat	k5eAaPmAgInS
ranám	rána	k1gFnPc3
osudu	osud	k1gInSc2
a	a	k8xC
vypořádal	vypořádat	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
nedokonalostí	nedokonalost	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Nebuď	budit	k5eNaImRp2nS
rozmrzelý	rozmrzelý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
neochabuj	ochabovat	k5eNaImRp2nS
<g/>
,	,	kIx,
ani	ani	k8xC
se	se	k3xPyFc4
nevzdávej	vzdávat	k5eNaImRp2nS
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
ti	ty	k3xPp2nSc3
nedaří	dařit	k5eNaImIp3nS
provést	provést	k5eAaPmF
každé	každý	k3xTgNnSc4
dílo	dílo	k1gNnSc4
podle	podle	k7c2
správných	správný	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
<g/>
,	,	kIx,
raději	rád	k6eAd2
se	se	k3xPyFc4
znova	znova	k6eAd1
vracej	vracet	k5eAaImRp2nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
ti	ty	k3xPp2nSc3
povedlo	povést	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
buď	buď	k8xC
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
aspoň	aspoň	k9
většina	většina	k1gFnSc1
tvých	tvůj	k3xOp2gInPc2
skutků	skutek	k1gInPc2
odpovídá	odpovídat	k5eAaImIp3nS
lidské	lidský	k2eAgFnPc4d1
přirozenosti	přirozenost	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
měj	mít	k5eAaImRp2nS
v	v	k7c6
lásce	láska	k1gFnSc3
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
k	k	k7c3
čemu	co	k3yInSc3,k3yRnSc3,k3yQnSc3
se	se	k3xPyFc4
vracíš	vracet	k5eAaImIp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
ve	v	k7c6
tvé	tvůj	k3xOp2gFnSc6
moci	moc	k1gFnSc6
učinit	učinit	k5eAaImF,k5eAaPmF
něco	něco	k3yInSc4
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
proč	proč	k6eAd1
to	ten	k3xDgNnSc1
neuděláš	udělat	k5eNaPmIp2nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
v	v	k7c4
moci	moce	k1gFnSc4
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgNnSc2d1
<g/>
,	,	kIx,
komu	kdo	k3yInSc3,k3yQnSc3,k3yRnSc3
chceš	chtít	k5eAaImIp2nS
dělat	dělat	k5eAaImF
výčitky	výčitka	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Slepé	slepý	k2eAgFnSc6d1
náhodě	náhoda	k1gFnSc6
nebo	nebo	k8xC
bohům	bůh	k1gMnPc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
Obojí	oboj	k1gFnPc2
je	být	k5eAaImIp3nS
nesmyslné	smyslný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikomu	nikdo	k3yNnSc3
nemáš	mít	k5eNaImIp2nS
činit	činit	k5eAaImF
výčitky	výčitka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
můžeš	moct	k5eAaImIp2nS
<g/>
,	,	kIx,
naprav	napravit	k5eAaPmRp2nS
alespoň	alespoň	k9
věc	věc	k1gFnSc1
samu	sám	k3xTgFnSc4
<g/>
;	;	kIx,
a	a	k8xC
nemůžeš	moct	k5eNaImIp2nS
<g/>
-li	-li	k?
ani	ani	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
ti	ty	k3xPp2nSc3
prospěje	prospět	k5eAaPmIp3nS
tvé	tvůj	k3xOp2gNnSc1
počínání	počínání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
nic	nic	k3yNnSc1
se	se	k3xPyFc4
nemá	mít	k5eNaImIp3nS
dělat	dělat	k5eAaImF
neúčelně	účelně	k6eNd1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Nevyhnutelný	vyhnutelný	k2eNgInSc1d1
osud	osud	k1gInSc1
se	se	k3xPyFc4
vznáší	vznášet	k5eAaImIp3nS
nad	nad	k7c7
tebou	ty	k3xPp2nSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
žiješ	žít	k5eAaImIp2nS
<g/>
,	,	kIx,
dokud	dokud	k8xS
ještě	ještě	k9
můžeš	moct	k5eAaImIp2nS
<g/>
,	,	kIx,
staň	stanout	k5eAaPmRp2nS
se	se	k3xPyFc4
dobrým	dobrý	k2eAgMnSc7d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marcus	Marcus	k1gMnSc1
nepociťoval	pociťovat	k5eNaImAgMnS
pražádný	pražádný	k3yNgInSc4
strach	strach	k1gInSc4
ze	z	k7c2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
chápal	chápat	k5eAaImAgInS
jako	jako	k9
vysvobození	vysvobození	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
...	...	k?
abychom	aby	kYmCp1nP
očekávali	očekávat	k5eAaImAgMnP
smrt	smrt	k1gFnSc4
s	s	k7c7
odevzdanou	odevzdaný	k2eAgFnSc7d1
myslí	mysl	k1gFnSc7
jako	jako	k8xC,k8xS
přirozené	přirozený	k2eAgNnSc1d1
rozloučení	rozloučení	k1gNnSc1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
každé	každý	k3xTgNnSc1
stvoření	stvoření	k1gNnSc1
skládá	skládat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
však	však	k9
pro	pro	k7c4
prvky	prvek	k1gInPc4
samé	samý	k3xTgFnSc6
nic	nic	k3yNnSc1
hrozného	hrozný	k2eAgNnSc2d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
každý	každý	k3xTgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
ustavičně	ustavičně	k6eAd1
přeměňuje	přeměňovat	k5eAaImIp3nS
v	v	k7c4
jiný	jiný	k2eAgInSc4d1
<g/>
,	,	kIx,
proč	proč	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
děsil	děsit	k5eAaImAgMnS
přeměny	přeměna	k1gFnPc4
a	a	k8xC
rozkladu	rozklást	k5eAaPmIp1nS
všech	všecek	k3xTgMnPc2
dohromady	dohromady	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vždyť	vždyť	k8xC
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
děje	dít	k5eAaImIp3nS
ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
<g/>
;	;	kIx,
a	a	k8xC
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zlo	zlo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
byly	být	k5eAaImAgFnP
po	po	k7c4
mnoho	mnoho	k4c4
generací	generace	k1gFnPc2
pokládány	pokládat	k5eAaImNgFnP
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
knih	kniha	k1gFnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markovu	Markův	k2eAgInSc3d1
dílu	díl	k1gInSc3
byla	být	k5eAaImAgFnS
přesto	přesto	k8xC
občas	občas	k6eAd1
vytýkána	vytýkán	k2eAgFnSc1d1
nedostatečná	dostatečný	k2eNgFnSc1d1
originalita	originalita	k1gFnSc1
filosofického	filosofický	k2eAgInSc2d1
přínosu	přínos	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
ideje	idea	k1gFnPc1
uvedené	uvedený	k2eAgFnPc1d1
v	v	k7c6
této	tento	k3xDgFnSc6
knize	kniha	k1gFnSc6
byly	být	k5eAaImAgFnP
v	v	k7c6
podstatě	podstata	k1gFnSc6
odvozeny	odvodit	k5eAaPmNgFnP
z	z	k7c2
mravních	mravní	k2eAgNnPc2d1
východisek	východisko	k1gNnPc2
stoicismu	stoicismus	k1gInSc2
<g/>
,	,	kIx,
převzatých	převzatý	k2eAgInPc2d1
od	od	k7c2
Epiktéta	Epiktéto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
byla	být	k5eAaImAgFnS
obvykle	obvykle	k6eAd1
opomíjena	opomíjen	k2eAgFnSc1d1
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Marcus	Marcus	k1gMnSc1
neusiloval	usilovat	k5eNaImAgMnS
o	o	k7c4
původnost	původnost	k1gFnSc4
svých	svůj	k3xOyFgInPc2
zápisků	zápisek	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
sepisoval	sepisovat	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
drobných	drobný	k2eAgInPc6d1
aspektech	aspekt	k1gInPc6
se	se	k3xPyFc4
Marcus	Marcus	k1gInSc1
odchýlil	odchýlit	k5eAaPmAgInS
od	od	k7c2
stoické	stoický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
směrech	směr	k1gInPc6
se	se	k3xPyFc4
přiblížil	přiblížit	k5eAaPmAgMnS
platónismu	platónismus	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
pozvolna	pozvolna	k6eAd1
vyvíjel	vyvíjet	k5eAaImAgMnS
v	v	k7c4
novoplatónismus	novoplatónismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
neodklonil	odklonit	k5eNaPmAgMnS
se	se	k3xPyFc4
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
připustil	připustit	k5eAaPmAgMnS
jakoukoli	jakýkoli	k3yIgFnSc4
formu	forma	k1gFnSc4
posmrtné	posmrtný	k2eAgFnSc2d1
existence	existence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgInPc1d1
principy	princip	k1gInPc1
</s>
<s>
Markova	Markův	k2eAgFnSc1d1
busta	busta	k1gFnSc1
</s>
<s>
K	k	k7c3
Markově	markově	k6eAd1
věhlasu	věhlas	k1gInSc2
<g/>
,	,	kIx,
přetrvávajícímu	přetrvávající	k2eAgInSc3d1
po	po	k7c6
všechny	všechen	k3xTgFnPc4
historické	historický	k2eAgFnPc4d1
epochy	epocha	k1gFnPc4
<g/>
,	,	kIx,
nepochybně	pochybně	k6eNd1
silně	silně	k6eAd1
přispěla	přispět	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
proslulost	proslulost	k1gFnSc4
filosofa	filosof	k1gMnSc2
na	na	k7c6
trůně	trůn	k1gInSc6
<g/>
,	,	kIx,
aplikujícího	aplikující	k2eAgMnSc2d1
filosofické	filosofický	k2eAgFnPc1d1
zásady	zásada	k1gFnPc4
při	při	k7c6
procesu	proces	k1gInSc6
realisování	realisování	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
doklady	doklad	k1gInPc1
Markových	Markových	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
úvah	úvaha	k1gFnPc2
lze	lze	k6eAd1
seznat	seznat	k5eAaPmF
v	v	k7c6
Hovorech	hovor	k1gInPc6
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
nadčasové	nadčasový	k2eAgNnSc1d1
a	a	k8xC
dosud	dosud	k6eAd1
nepřekonané	překonaný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Bratr	bratr	k1gMnSc1
Severus	Severus	k1gMnSc1
mi	já	k3xPp1nSc3
byl	být	k5eAaImAgMnS
vzorem	vzor	k1gInSc7
lásky	láska	k1gFnSc2
k	k	k7c3
rodině	rodina	k1gFnSc3
a	a	k8xC
lásky	láska	k1gFnSc2
k	k	k7c3
pravdě	pravda	k1gFnSc3
a	a	k8xC
spravedlnosti	spravedlnost	k1gFnSc3
<g/>
;	;	kIx,
...	...	k?
od	od	k7c2
něho	on	k3xPp3gMnSc2
jsem	být	k5eAaImIp1nS
získal	získat	k5eAaPmAgMnS
představu	představa	k1gFnSc4
svobodného	svobodný	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
vládne	vládnout	k5eAaImIp3nS
dokonalá	dokonalý	k2eAgFnSc1d1
rovnoprávnost	rovnoprávnost	k1gFnSc1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
a	a	k8xC
v	v	k7c6
němž	jenž	k3xRgInSc6
ničeho	nic	k3yNnSc2
není	být	k5eNaImIp3nS
dbáno	dbán	k2eAgNnSc1d1
více	hodně	k6eAd2
než	než	k8xS
svobody	svoboda	k1gFnSc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svoboda	svoboda	k1gFnSc1
a	a	k8xC
spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
ve	v	k7c6
smyslu	smysl	k1gInSc6
stejných	stejný	k2eAgNnPc2d1
práv	právo	k1gNnPc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
,	,	kIx,
náležely	náležet	k5eAaImAgFnP
k	k	k7c3
Markovým	Marková	k1gFnPc3
časně	časně	k6eAd1
osvojeným	osvojený	k2eAgMnPc3d1
a	a	k8xC
ustavičně	ustavičně	k6eAd1
prosazovaným	prosazovaný	k2eAgMnPc3d1
<g/>
,	,	kIx,
stěžejním	stěžejní	k2eAgMnPc3d1
politickým	politický	k2eAgMnPc3d1
principům	princip	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
pokušením	pokušení	k1gNnSc7
zneužívat	zneužívat	k5eAaImF
absolutní	absolutní	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
postavení	postavení	k1gNnSc6
nevyhnutelně	vyhnutelně	k6eNd1
vystaven	vystavit	k5eAaPmNgInS
<g/>
,	,	kIx,
ho	on	k3xPp3gMnSc4
ochraňovalo	ochraňovat	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc4
filosofické	filosofický	k2eAgNnSc1d1
založení	založení	k1gNnSc1
a	a	k8xC
výstrahy	výstraha	k1gFnPc1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
zahrnoval	zahrnovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Dej	dát	k5eAaPmRp2nS
pozor	pozor	k1gInSc4
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
nezcísařštěl	zcísařštět	k5eNaPmAgMnS,k5eNaImAgMnS
<g/>
:	:	kIx,
abys	aby	kYmCp2nS
nenačichl	načichnout	k5eNaPmAgMnS
císařstvím	císařství	k1gNnSc7
<g/>
;	;	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
snadno	snadno	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Bojuj	bojovat	k5eAaImRp2nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
zůstal	zůstat	k5eAaPmAgMnS
takovým	takový	k3xDgNnSc7
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
tě	ty	k3xPp2nSc4
chtěla	chtít	k5eAaImAgFnS
mít	mít	k5eAaImF
filosofie	filosofie	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
Neboť	neboť	k8xC
Cesta	cesta	k1gFnSc1
po	po	k7c4
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
kráčejí	kráčet	k5eAaImIp3nP
naše	náš	k3xOp1gMnPc4
koně	kůň	k1gMnPc4
je	být	k5eAaImIp3nS
zrádná	zrádný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
jejich	jejich	k3xOp3gFnSc1
povaha	povaha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marcus	Marcus	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
příliš	příliš	k6eAd1
vědom	vědom	k2eAgMnSc1d1
omezených	omezený	k2eAgFnPc2d1
možností	možnost	k1gFnSc7
svého	svůj	k3xOyFgNnSc2
konání	konání	k1gNnSc2
a	a	k8xC
chatrnosti	chatrnost	k1gFnSc2
utopických	utopický	k2eAgInPc2d1
modelů	model	k1gInPc2
společnosti	společnost	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Neblouzni	blouznit	k5eNaImRp2nS
o	o	k7c6
státě	stát	k1gInSc6
Platónově	platónově	k6eAd1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
spokoj	spokojit	k5eAaPmRp2nS
se	se	k3xPyFc4
i	i	k9
s	s	k7c7
nejmenším	malý	k2eAgInSc7d3
krůčkem	krůček	k1gInSc7
vpřed	vpřed	k6eAd1
a	a	k8xC
nepokládej	pokládat	k5eNaImRp2nS
ani	ani	k8xC
tento	tento	k3xDgInSc4
úspěch	úspěch	k1gInSc4
za	za	k7c4
nevýznamný	významný	k2eNgInSc4d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
dovede	dovést	k5eAaPmIp3nS
změnit	změnit	k5eAaPmF
utkvělé	utkvělý	k2eAgFnPc4d1
představy	představa	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
bez	bez	k7c2
jejich	jejich	k3xOp3gFnSc2
změny	změna	k1gFnSc2
–	–	k?
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
jiného	jiné	k1gNnSc2
čekat	čekat	k5eAaImF
než	než	k8xS
otročení	otročení	k1gNnSc1
vzdychajících	vzdychající	k2eAgInPc2d1
a	a	k8xC
poslušnost	poslušnost	k1gFnSc4
pokrytců	pokrytec	k1gMnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
politické	politický	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
musí	muset	k5eAaImIp3nS
zohledňovat	zohledňovat	k5eAaImF
smýšlení	smýšlení	k1gNnSc4
lidí	člověk	k1gMnPc2
a	a	k8xC
proto	proto	k8xC
upřednostňoval	upřednostňovat	k5eAaImAgInS
občanskou	občanský	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
také	také	k9
ve	v	k7c6
vyjadřování	vyjadřování	k1gNnSc6
názoru	názor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záleželo	záležet	k5eAaImAgNnS
mu	on	k3xPp3gNnSc3
na	na	k7c6
rozumně	rozumně	k6eAd1
uplatňovaném	uplatňovaný	k2eAgInSc6d1
a	a	k8xC
veřejnému	veřejný	k2eAgInSc3d1
prospěchu	prospěch	k1gInSc3
zasvěceném	zasvěcený	k2eAgNnSc6d1
užívání	užívání	k1gNnSc6
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
nakládal	nakládat	k5eAaImAgMnS
přiměřeně	přiměřeně	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zohledňoval	zohledňovat	k5eAaImAgMnS
dobré	dobrý	k2eAgNnSc4d1
porozumění	porozumění	k1gNnSc4
věci	věc	k1gFnSc3
a	a	k8xC
nalezení	nalezení	k1gNnSc3
řešení	řešení	k1gNnSc2
problému	problém	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Stačí	stačit	k5eAaBmIp3nS
můj	můj	k3xOp1gInSc4
rozum	rozum	k1gInSc4
na	na	k7c4
tento	tento	k3xDgInSc4
úkol	úkol	k1gInSc4
či	či	k8xC
ne	ne	k9
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
stačí	stačit	k5eAaBmIp3nS
<g/>
,	,	kIx,
pak	pak	k6eAd1
ho	on	k3xPp3gMnSc4
užívám	užívat	k5eAaImIp1nS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
provedení	provedení	k1gNnSc4
jako	jako	k8xC,k8xS
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
mě	já	k3xPp1nSc2
vybavila	vybavit	k5eAaPmAgFnS
vesmírná	vesmírný	k2eAgFnSc1d1
příroda	příroda	k1gFnSc1
<g/>
;	;	kIx,
pakli	pakli	k8xS
nestačí	stačit	k5eNaBmIp3nS
<g/>
,	,	kIx,
buď	buď	k8xC
postoupím	postoupit	k5eAaPmIp1nS
svůj	svůj	k3xOyFgInSc4
úkol	úkol	k1gInSc4
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
ho	on	k3xPp3gMnSc4
dovede	dovést	k5eAaPmIp3nS
vykonat	vykonat	k5eAaPmF
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
mou	můj	k3xOp1gFnSc7
přímou	přímý	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
<g/>
,	,	kIx,
anebo	anebo	k8xC
ho	on	k3xPp3gMnSc4
konám	konat	k5eAaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dovedu	dovést	k5eAaPmIp1nS
<g/>
,	,	kIx,
ještě	ještě	k9
s	s	k7c7
přispěním	přispění	k1gNnSc7
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgMnSc4d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
podporován	podporovat	k5eAaImNgInS
mým	můj	k3xOp1gInSc7
rozumem	rozum	k1gInSc7
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
pak	pak	k6eAd1
provést	provést	k5eAaPmF
čin	čin	k1gInSc4
obecnému	obecný	k2eAgNnSc3d1
blahu	blaho	k1gNnSc3
právě	právě	k9
teď	teď	k6eAd1
příhodný	příhodný	k2eAgMnSc1d1
a	a	k8xC
prospěšný	prospěšný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
každý	každý	k3xTgInSc1
skutek	skutek	k1gInSc1
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
ho	on	k3xPp3gNnSc4
konám	konat	k5eAaImIp1nS
sám	sám	k3xTgMnSc1
nebo	nebo	k8xC
s	s	k7c7
přispěním	přispění	k1gNnSc7
jiného	jiné	k1gNnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
směřovat	směřovat	k5eAaImF
jen	jen	k9
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
prospěšné	prospěšný	k2eAgNnSc4d1
a	a	k8xC
přiměřené	přiměřený	k2eAgNnSc4d1
obecnému	obecný	k2eAgNnSc3d1
dobru	dobro	k1gNnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
dobrém	dobrý	k2eAgInSc6d1
výkonu	výkon	k1gInSc6
soudnictví	soudnictví	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
činností	činnost	k1gFnSc7
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
pociťoval	pociťovat	k5eAaImAgInS
nejvyšší	vysoký	k2eAgFnSc4d3
míru	míra	k1gFnSc4
zodpovědnosti	zodpovědnost	k1gFnSc2
<g/>
,	,	kIx,
spočíval	spočívat	k5eAaImAgInS
podle	podle	k7c2
Marka	Marek	k1gMnSc2
základ	základ	k1gInSc4
veškerého	veškerý	k3xTgInSc2
společenského	společenský	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Dovedeš	dovést	k5eAaPmIp2nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
bystře	bystro	k6eAd1
dívat	dívat	k5eAaImF
<g/>
,	,	kIx,
dívej	dívat	k5eAaImRp2nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
a	a	k8xC
suď	soudit	k5eAaImRp2nS
pokud	pokud	k8xS
možno	možno	k6eAd1
nejmoudřeji	moudro	k6eAd3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svému	svůj	k3xOyFgNnSc3
bytí	bytí	k1gNnSc3
přiřazoval	přiřazovat	k5eAaImAgInS
Marcus	Marcus	k1gInSc1
i	i	k9
určitý	určitý	k2eAgInSc4d1
kosmopolitní	kosmopolitní	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Pro	pro	k7c4
mne	já	k3xPp1nSc4
<g/>
,	,	kIx,
občana	občan	k1gMnSc4
Antonina	Antonin	k2eAgMnSc4d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
obcí	obec	k1gFnPc2
a	a	k8xC
vlastí	vlast	k1gFnPc2
Řím	Řím	k1gInSc1
<g/>
;	;	kIx,
pro	pro	k7c4
mne	já	k3xPp1nSc4
<g/>
,	,	kIx,
Antonina	Antonin	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
vesmír	vesmír	k1gInSc1
<g/>
:	:	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
těmto	tento	k3xDgFnPc3
obcím	obec	k1gFnPc3
na	na	k7c4
prospěch	prospěch	k1gInSc4
<g/>
,	,	kIx,
jenom	jenom	k9
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
mne	já	k3xPp1nSc4
dobrem	dobro	k1gNnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
Markovým	Markův	k2eAgInSc7d1
životem	život	k1gInSc7
a	a	k8xC
vládnutím	vládnutí	k1gNnSc7
se	se	k3xPyFc4
zaobírá	zaobírat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
pramenů	pramen	k1gInPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
přináší	přinášet	k5eAaImIp3nS
spíše	spíše	k9
neuspokojivá	uspokojivý	k2eNgNnPc4d1
a	a	k8xC
neúplná	úplný	k2eNgNnPc4d1
fakta	faktum	k1gNnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
Markově	Markův	k2eAgNnSc6d1
období	období	k1gNnSc6
nepůsobil	působit	k5eNaImAgMnS
žádný	žádný	k3yNgMnSc1
významný	významný	k2eAgMnSc1d1
historik	historik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
literární	literární	k2eAgInSc4d1
pramen	pramen	k1gInSc4
představuje	představovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnPc4
biografie	biografie	k1gFnPc4
v	v	k7c6
díle	dílo	k1gNnSc6
Historia	Historium	k1gNnSc2
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
doplněná	doplněný	k2eAgFnSc1d1
o	o	k7c4
sdělení	sdělení	k1gNnSc4
týkající	týkající	k2eAgNnSc4d1
se	se	k3xPyFc4
Marka	marka	k1gFnSc1
v	v	k7c6
životopisech	životopis	k1gInPc6
Hadriana	Hadrian	k1gMnSc2
<g/>
,	,	kIx,
Antonina	Antonin	k2eAgMnSc2d1
<g/>
,	,	kIx,
Vera	Ver	k1gInSc2
a	a	k8xC
Avidia	Avidium	k1gNnSc2
Cassia	Cassium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diskuse	diskuse	k1gFnSc1
o	o	k7c6
kvalitě	kvalita	k1gFnSc6
a	a	k8xC
spolehlivosti	spolehlivost	k1gFnSc6
tohoto	tento	k3xDgInSc2
souboru	soubor	k1gInSc2
biografií	biografie	k1gFnPc2
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
se	se	k3xPyFc4
vedly	vést	k5eAaImAgFnP
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
velkou	velký	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
jediným	jediný	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
píšícím	píšící	k2eAgInSc7d1
koncem	konec	k1gInSc7
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
časovému	časový	k2eAgInSc3d1
odstupu	odstup	k1gInSc3
vzniku	vznik	k1gInSc2
díla	dílo	k1gNnSc2
od	od	k7c2
popisovaných	popisovaný	k2eAgInPc2d1
jevů	jev	k1gInPc2
a	a	k8xC
k	k	k7c3
značně	značně	k6eAd1
rozličné	rozličný	k2eAgFnSc3d1
hodnověrnosti	hodnověrnost	k1gFnSc3
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
autor	autor	k1gMnSc1
vycházel	vycházet	k5eAaImAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
důvěryhodnost	důvěryhodnost	k1gFnSc1
uváděných	uváděný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
velice	velice	k6eAd1
různí	různý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
Historie	historie	k1gFnSc1
Augusta	August	k1gMnSc2
mnohdy	mnohdy	k6eAd1
přináší	přinášet	k5eAaImIp3nS
velmi	velmi	k6eAd1
přesné	přesný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
množství	množství	k1gNnSc1
smyšlenek	smyšlenka	k1gFnPc2
a	a	k8xC
výmyslů	výmysl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
<g/>
,	,	kIx,
píšící	píšící	k2eAgMnSc1d1
zřejmě	zřejmě	k6eAd1
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zhotovil	zhotovit	k5eAaPmAgMnS
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
ale	ale	k8xC
dochovaly	dochovat	k5eAaPmAgFnP
jen	jen	k9
v	v	k7c6
notně	notně	k6eAd1
zestručněné	zestručněný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aurelius	Aurelius	k1gMnSc1
Victor	Victor	k1gMnSc1
a	a	k8xC
Eutropius	Eutropius	k1gMnSc1
<g/>
,	,	kIx,
dějepisci	dějepisec	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
dílech	dílo	k1gNnPc6
příležitostně	příležitostně	k6eAd1
přinášejí	přinášet	k5eAaImIp3nP
určité	určitý	k2eAgFnPc1d1
informace	informace	k1gFnPc1
osvětlující	osvětlující	k2eAgInPc4d1
momenty	moment	k1gInPc4
Markova	Markův	k2eAgNnSc2d1
panování	panování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užitečné	užitečný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
spisy	spis	k1gInPc1
Markova	Markův	k2eAgMnSc2d1
učitele	učitel	k1gMnSc2
a	a	k8xC
řečníka	řečník	k1gMnSc2
Frontona	Fronton	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
důležitost	důležitost	k1gFnSc1
přísluší	příslušet	k5eAaImIp3nS
i	i	k8xC
literatuře	literatura	k1gFnSc3
církevních	církevní	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
Tertullianus	Tertullianus	k1gMnSc1
<g/>
,	,	kIx,
Eusebios	Eusebios	k1gMnSc1
nebo	nebo	k8xC
Orosius	Orosius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznatky	poznatek	k1gInPc7
z	z	k7c2
papyrů	papyr	k1gInPc2
<g/>
,	,	kIx,
nápisů	nápis	k1gInPc2
<g/>
,	,	kIx,
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
právních	právní	k2eAgInPc2d1
aktů	akt	k1gInPc2
společně	společně	k6eAd1
s	s	k7c7
výsledky	výsledek	k1gInPc7
archeologického	archeologický	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
a	a	k8xC
výklady	výklad	k1gInPc4
architektonických	architektonický	k2eAgInPc2d1
monumentů	monument	k1gInPc2
dále	daleko	k6eAd2
poskytují	poskytovat	k5eAaImIp3nP
vodítka	vodítko	k1gNnPc1
pro	pro	k7c4
bližší	bližší	k1gNnSc4
pochopení	pochopení	k1gNnSc2
událostí	událost	k1gFnSc7
doby	doba	k1gFnSc2
Markova	Markův	k2eAgNnSc2d1
vládnutí	vládnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Markova	Markův	k2eAgFnSc1d1
busta	busta	k1gFnSc1
v	v	k7c4
Metropolitan	metropolitan	k1gInSc4
Museum	museum	k1gNnSc4
of	of	k?
Art	Art	k1gMnSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
</s>
<s>
Markovo	Markův	k2eAgNnSc4d1
počínání	počínání	k1gNnSc4
jako	jako	k9
panovníka	panovník	k1gMnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
jeho	jeho	k3xOp3gFnPc4
dochované	dochovaný	k2eAgFnPc4d1
filosofické	filosofický	k2eAgFnPc4d1
úvahy	úvaha	k1gFnPc4
mu	on	k3xPp3gMnSc3
vynesly	vynést	k5eAaPmAgInP
respekt	respekt	k1gInSc4
a	a	k8xC
obdiv	obdiv	k1gInSc4
v	v	k7c6
očích	oko	k1gNnPc6
současníků	současník	k1gMnPc2
i	i	k9
později	pozdě	k6eAd2
narozených	narozený	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
přestože	přestože	k8xS
byli	být	k5eAaImAgMnP
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
vystaveni	vystaven	k2eAgMnPc1d1
pronásledování	pronásledování	k1gNnSc2
<g/>
,	,	kIx,
považovali	považovat	k5eAaImAgMnP
Marka	Marek	k1gMnSc4
za	za	k7c4
dobrého	dobrý	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tertullianus	Tertullianus	k1gInSc1
ho	on	k3xPp3gMnSc4
dokonce	dokonce	k9
označil	označit	k5eAaPmAgMnS
za	za	k7c4
přítele	přítel	k1gMnSc4
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecná	všeobecný	k2eAgFnSc1d1
úcta	úcta	k1gFnSc1
prokazovaná	prokazovaný	k2eAgFnSc1d1
Markovi	Marek	k1gMnSc3
širokými	široký	k2eAgInPc7d1
vrstvami	vrstva	k1gFnPc7
římského	římský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
ještě	ještě	k6eAd1
zesílila	zesílit	k5eAaPmAgFnS
vlivem	vlivem	k7c2
vzrůstajících	vzrůstající	k2eAgInPc2d1
zmatků	zmatek	k1gInPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
panování	panování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžik	okamžik	k1gInSc4
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
být	být	k5eAaImF
určitým	určitý	k2eAgInSc7d1
zlomem	zlom	k1gInSc7
a	a	k8xC
počátkem	počátek	k1gInSc7
úpadku	úpadek	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
ostatně	ostatně	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgMnS
soudobý	soudobý	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rokem	rok	k1gInSc7
180	#num#	k4
je	být	k5eAaImIp3nS
proto	proto	k8xC
leckdy	leckdy	k6eAd1
vymezován	vymezován	k2eAgInSc1d1
závěr	závěr	k1gInSc1
epochy	epocha	k1gFnSc2
Pax	Pax	k1gMnSc2
Romana	Roman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markova	Markův	k2eAgFnSc1d1
reputace	reputace	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
staletích	staletí	k1gNnPc6
zcela	zcela	k6eAd1
bez	bez	k1gInSc4
úhony	úhona	k1gFnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
ho	on	k3xPp3gMnSc4
ani	ani	k8xC
zdaleka	zdaleka	k6eAd1
nelze	lze	k6eNd1
označit	označit	k5eAaPmF
za	za	k7c4
„	„	k?
<g/>
dokonalého	dokonalý	k2eAgMnSc4d1
<g/>
“	“	k?
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
projevoval	projevovat	k5eAaImAgMnS
příliš	příliš	k6eAd1
velkou	velký	k2eAgFnSc4d1
toleranci	tolerance	k1gFnSc4
k	k	k7c3
chybám	chyba	k1gFnPc3
druhých	druhý	k4xOgMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
Markovo	Markův	k2eAgNnSc1d1
největší	veliký	k2eAgNnSc1d3
selhání	selhání	k1gNnSc1
bude	být	k5eAaImBp3nS
patrně	patrně	k6eAd1
vždy	vždy	k6eAd1
vnímán	vnímat	k5eAaImNgInS
výběr	výběr	k1gInSc4
Commoda	Commod	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
jediného	jediný	k2eAgMnSc4d1
přeživšího	přeživší	k2eAgMnSc4d1
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
nástupcem	nástupce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
často	často	k6eAd1
podrobován	podrobován	k2eAgInSc1d1
kritice	kritika	k1gFnSc3
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
odklonem	odklon	k1gInSc7
od	od	k7c2
adoptivního	adoptivní	k2eAgInSc2d1
principu	princip	k1gInSc2
odevzdávání	odevzdávání	k1gNnSc2
moci	moc	k1gFnSc2
přerušil	přerušit	k5eAaPmAgInS
dlouhou	dlouhý	k2eAgFnSc4d1
a	a	k8xC
přínosnou	přínosný	k2eAgFnSc4d1
éru	éra	k1gFnSc4
římských	římský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
ovšem	ovšem	k9
možné	možný	k2eAgNnSc1d1
přehlížet	přehlížet	k5eAaImF
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
kdyby	kdyby	kYmCp3nS
neučinil	učinit	k5eNaPmAgMnS
Commoda	Commoda	k1gMnSc1
augustem	august	k1gMnSc7
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
by	by	kYmCp3nS
tím	ten	k3xDgNnSc7
svému	svůj	k3xOyFgMnSc3
synovi	syn	k1gMnSc3
přivodil	přivodit	k5eAaBmAgMnS,k5eAaPmAgMnS
záhubu	záhuba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
tomu	ten	k3xDgNnSc3
lze	lze	k6eAd1
jen	jen	k9
stěží	stěží	k6eAd1
z	z	k7c2
Marka	Marek	k1gMnSc2
sejmout	sejmout	k5eAaPmF
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
důsledku	důsledek	k1gInSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
rozhodnutí	rozhodnutí	k1gNnSc2
se	se	k3xPyFc4
Římu	Řím	k1gInSc3
dostalo	dostat	k5eAaPmAgNnS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejzhýralejších	zhýralý	k2eAgMnPc2d3
a	a	k8xC
nejukrutnějších	ukrutný	k2eAgMnPc2d3
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
novověku	novověk	k1gInSc6
byl	být	k5eAaImAgInS
Marcus	Marcus	k1gInSc1
vystaven	vystaven	k2eAgInSc1d1
nové	nový	k2eAgFnSc3d1
pozornosti	pozornost	k1gFnSc3
a	a	k8xC
zájmu	zájem	k1gInSc2
filosofů	filosof	k1gMnPc2
<g/>
,	,	kIx,
myslitelů	myslitel	k1gMnPc2
a	a	k8xC
literátů	literát	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
znovu	znovu	k6eAd1
pozvedli	pozvednout	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
proslulost	proslulost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
osvícenství	osvícenství	k1gNnSc2
se	se	k3xPyFc4
„	„	k?
<g/>
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
“	“	k?
staly	stát	k5eAaPmAgFnP
módní	módní	k2eAgFnSc7d1
četbou	četba	k1gFnSc7
řady	řada	k1gFnSc2
vzdělanců	vzdělanec	k1gMnPc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
Voltaire	Voltair	k1gInSc5
jimi	on	k3xPp3gMnPc7
byl	být	k5eAaImAgInS
uchvácen	uchvácen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markova	Markův	k2eAgFnSc1d1
moudrost	moudrost	k1gFnSc1
<g/>
,	,	kIx,
lidskost	lidskost	k1gFnSc4
a	a	k8xC
smysl	smysl	k1gInSc4
pro	pro	k7c4
povinnost	povinnost	k1gFnSc4
z	z	k7c2
něho	on	k3xPp3gMnSc2
činily	činit	k5eAaImAgFnP
vzor	vzor	k1gInSc4
spravedlivého	spravedlivý	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historik	historik	k1gMnSc1
Edward	Edward	k1gMnSc1
Gibbon	gibbon	k1gMnSc1
se	se	k3xPyFc4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
stěžejním	stěžejní	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
History	Histor	k1gMnPc4
of	of	k?
the	the	k?
Decline	Declin	k1gInSc5
and	and	k?
Fall	Fall	k1gInSc1
of	of	k?
the	the	k?
Roman	Roman	k1gMnSc1
Empire	empir	k1gInSc5
přiklonil	přiklonit	k5eAaPmAgInS
k	k	k7c3
postoji	postoj	k1gInSc3
Cassia	Cassius	k1gMnSc2
Diona	Dion	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
formuloval	formulovat	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Markovou	Markův	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
skončil	skončit	k5eAaPmAgInS
zlatý	zlatý	k2eAgInSc1d1
věk	věk	k1gInSc1
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
novověku	novověk	k1gInSc2
až	až	k6eAd1
do	do	k7c2
současnosti	současnost	k1gFnSc2
se	se	k3xPyFc4
četné	četný	k2eAgFnPc1d1
význačné	význačný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgInPc4
náleželi	náležet	k5eAaImAgMnP
monarchové	monarcha	k1gMnPc1
<g/>
,	,	kIx,
politici	politik	k1gMnPc1
i	i	k8xC
spisovatelé	spisovatel	k1gMnPc1
<g/>
,	,	kIx,
prohlašovaly	prohlašovat	k5eAaImAgInP
za	za	k7c4
příznivce	příznivec	k1gMnPc4
tohoto	tento	k3xDgMnSc2
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paradoxně	paradoxně	k6eAd1
sám	sám	k3xTgMnSc1
Marcus	Marcus	k1gMnSc1
pociťoval	pociťovat	k5eAaImAgMnS
vůči	vůči	k7c3
posmrtné	posmrtný	k2eAgFnSc3d1
slávě	sláva	k1gFnSc3
naprostou	naprostý	k2eAgFnSc4d1
lhostejnost	lhostejnost	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Slovní	slovní	k2eAgInPc4d1
výrazy	výraz	k1gInPc4
kdysi	kdysi	k6eAd1
obvyklé	obvyklý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
teď	teď	k6eAd1
zastaralé	zastaralý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
i	i	k9
jména	jméno	k1gNnPc1
kdysi	kdysi	k6eAd1
věhlasných	věhlasný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
jsou	být	k5eAaImIp3nP
teď	teď	k6eAd1
jistou	jistý	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
zastaralá	zastaralý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Camillus	Camillus	k1gInSc1
<g/>
,	,	kIx,
Caeso	Caesa	k1gFnSc5
<g/>
,	,	kIx,
Volesus	Volesus	k1gMnSc1
<g/>
,	,	kIx,
Dentatus	Dentatus	k1gMnSc1
a	a	k8xC
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
nevidět	vidět	k5eNaImF
i	i	k8xC
jméno	jméno	k1gNnSc4
Scipio	Scipio	k6eAd1
a	a	k8xC
Cato	Cato	k6eAd1
<g/>
,	,	kIx,
pak	pak	k6eAd1
i	i	k9
Augustus	Augustus	k1gMnSc1
a	a	k8xC
konečně	konečně	k6eAd1
Hadrianus	Hadrianus	k1gMnSc1
a	a	k8xC
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
pomíjivé	pomíjivý	k2eAgNnSc1d1
a	a	k8xC
brzy	brzy	k6eAd1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
bájí	báj	k1gFnSc7
a	a	k8xC
klesá	klesat	k5eAaImIp3nS
v	v	k7c4
hrob	hrob	k1gInSc4
čirého	čirý	k2eAgNnSc2d1
zapomnění	zapomnění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
16	#num#	k4
<g/>
↑	↑	k?
Portréty	portrét	k1gInPc1
světovládců	světovládce	k1gMnPc2
I.	I.	kA
Životopis	životopis	k1gInSc1
filozofa	filozof	k1gMnSc2
Marka	marka	k1gFnSc1
Antonina	Antonina	k1gFnSc1
28	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
X	X	kA
<g/>
,	,	kIx,
16	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
XI	XI	kA
<g/>
,	,	kIx,
29	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VI	VI	kA
<g/>
,	,	kIx,
21	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IX	IX	kA
<g/>
,	,	kIx,
42	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
<g/>
,	,	kIx,
59	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VI	VI	kA
<g/>
,	,	kIx,
48	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
<g/>
,	,	kIx,
17	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
,	,	kIx,
17	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
14	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VI	VI	kA
<g/>
,	,	kIx,
30	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IX	IX	kA
<g/>
,	,	kIx,
29	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VII	VII	kA
<g/>
,	,	kIx,
5	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
<g/>
,	,	kIx,
38	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
VII	VII	kA
<g/>
,	,	kIx,
40	#num#	k4
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
<g/>
,	,	kIx,
Antoninus	Antoninus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
33	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BURIAN	Burian	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římské	římský	k2eAgInPc1d1
impérium	impérium	k1gNnSc1
:	:	kIx,
vrchol	vrchol	k1gInSc1
a	a	k8xC
proměny	proměna	k1gFnPc1
antické	antický	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-205-0536-9	80-205-0536-9	k4
</s>
<s>
GIBBON	gibbon	k1gMnSc1
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpadek	úpadek	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Levné	levný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
KMa	KMa	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7309-189-5	80-7309-189-5	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
antického	antický	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
<g/>
/	/	kIx~
<g/>
art	art	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7341-930-0	80-7341-930-0	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc1d1
císařové	císař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7257-731-X	80-7257-731-X	k4
</s>
<s>
HÉRÓDIANOS	HÉRÓDIANOS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řím	Řím	k1gInSc1
po	po	k7c6
Marku	Marek	k1gMnSc6
Aureliovi	Aurelius	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1975	#num#	k4
</s>
<s>
HOŠEK	Hošek	k1gMnSc1
<g/>
,	,	kIx,
Radislav	Radislav	k1gMnSc1
<g/>
,	,	kIx,
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řím	Řím	k1gInSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-204-0083-4	80-204-0083-4	k4
</s>
<s>
MAŠKIN	MAŠKIN	kA
<g/>
,	,	kIx,
Nikolaj	Nikolaj	k1gMnSc1
A.	A.	kA
Dějiny	dějiny	k1gFnPc1
starověkého	starověký	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
SNPL	SNPL	kA
<g/>
,	,	kIx,
1957	#num#	k4
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
psané	psaný	k2eAgFnSc2d1
Římem	Řím	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
:	:	kIx,
Perfekt	perfektum	k1gNnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-8046-297-6	80-8046-297-6	k4
</s>
<s>
Portréty	portrét	k1gInPc1
světovládců	světovládce	k1gMnPc2
I	i	k8xC
(	(	kIx(
<g/>
Od	od	k7c2
Hadriana	Hadrian	k1gMnSc2
po	po	k7c4
Alexandra	Alexandr	k1gMnSc4
Severa	Severa	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Antoninovská	Antoninovský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
</s>
<s>
Sloup	sloup	k1gInSc1
Marka	Marek	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
</s>
<s>
Stoicismus	stoicismus	k1gInSc1
</s>
<s>
Markomanské	markomanský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Antoninovský	Antoninovský	k2eAgInSc1d1
mor	mor	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Marcus	Marcus	k1gInSc1
Aurelius	Aurelius	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
(	(	kIx(
<g/>
stránky	stránka	k1gFnPc4
Antika	antika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roman	Romana	k1gFnPc2
History	Histor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epitome	Epitom	k1gInSc5
of	of	k?
Book	Booko	k1gNnPc2
LXXII	LXXII	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
(	(	kIx(
<g/>
De	De	k?
Imperatoribus	Imperatoribus	k1gInSc1
Romanis	Romanis	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
Internet	Internet	k1gInSc4
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosopha	k1gMnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Volně	volně	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
Hovorů	hovor	k1gInPc2
k	k	k7c3
sobě	se	k3xPyFc3
</s>
<s>
Citáty	citát	k1gInPc1
Marca	Marcus	k1gMnSc2
Aurelia	Aurelius	k1gMnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Antoninus	Antoninus	k1gMnSc1
Pius	Pius	k1gMnSc1
</s>
<s>
Římský	římský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
161	#num#	k4
<g/>
–	–	k?
<g/>
180	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Commodus	Commodus	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981001808	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118577468	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1031	#num#	k4
946X	946X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80051702	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500115701	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
102895066	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80051702	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Starověký	starověký	k2eAgInSc4d1
Řím	Řím	k1gInSc4
</s>
