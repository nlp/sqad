<s desamb="1">
Její	její	k3xOp3gInSc1
rozkvět	rozkvět	k1gInSc1
nastal	nastat	k5eAaPmAgInS
především	především	k9
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	rok	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
zemích	země	k1gFnPc6
OECD	OECD	kA
a	a	k8xC
obzvlášť	obzvlášť	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
měnila	měnit	k5eAaImAgFnS
strategie	strategie	k1gFnSc1
ochrany	ochrana	k1gFnSc2
životních	životní	k2eAgNnPc2d1
prostředí	prostředí	k1gNnPc2
-	-	kIx~
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
klást	klást	k5eAaImF
důraz	důraz	k1gInSc1
na	na	k7c4
prevenci	prevence	k1gFnSc4
vzniku	vznik	k1gInSc2
odpadů	odpad	k1gInPc2
a	a	k8xC
na	na	k7c4
inovace	inovace	k1gFnPc4
technologií	technologie	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
méně	málo	k6eAd2
zatěžovaly	zatěžovat	k5eAaImAgFnP
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>