<s>
Zelená	zelený	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
udržitelná	udržitelný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nebo	nebo	k8xC
prasinochemie	prasinochemie	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
prasinos	prasinos	k1gInSc1
=	=	kIx~
zelený	zelený	k2eAgInSc1d1
a	a	k8xC
chymie	chymie	k1gFnSc1
=	=	kIx~
chemie	chemie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
chemie	chemie	k1gFnSc2
a	a	k8xC
chemického	chemický	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
navrhování	navrhování	k1gNnSc6
produktů	produkt	k1gInPc2
a	a	k8xC
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
minimalizují	minimalizovat	k5eAaBmIp3nP
nebo	nebo	k8xC
vylučují	vylučovat	k5eAaImIp3nP
použití	použití	k1gNnSc4
a	a	k8xC
tvorbu	tvorba	k1gFnSc4
nebezpečných	bezpečný	k2eNgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
chemie	chemie	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
účinky	účinek	k1gInPc4
znečišťujících	znečišťující	k2eAgFnPc2d1
chemikálií	chemikálie	k1gFnPc2
na	na	k7c4
přírodu	příroda	k1gFnSc4
<g/>
,	,	kIx,
zelená	zelený	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
celkový	celkový	k2eAgInSc4d1
environmentální	environmentální	k2eAgInSc4d1
dopad	dopad	k1gInSc4
chemie	chemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeší	řešit	k5eAaImIp3nS
i	i	k9
spotřebu	spotřeba	k1gFnSc4
energie	energie	k1gFnSc2
a	a	k8xC
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
rizika	riziko	k1gNnSc2
pro	pro	k7c4
pracovníky	pracovník	k1gMnPc4
nebo	nebo	k8xC
pravděpodobnost	pravděpodobnost	k1gFnSc4
nehod	nehoda	k1gFnPc2
v	v	k7c6
chemických	chemický	k2eAgInPc6d1
procesech	proces	k1gInPc6
a	a	k8xC
výrobách	výroba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
zelené	zelený	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
je	být	k5eAaImIp3nS
nalézat	nalézat	k5eAaImF
postupy	postup	k1gInPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
udělat	udělat	k5eAaPmF
chemickou	chemický	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
hospodárnější	hospodárný	k2eAgFnPc1d2
a	a	k8xC
bezpečnější	bezpečný	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
se	se	k3xPyFc4
formovala	formovat	k5eAaImAgFnS
během	během	k7c2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
v	v	k7c6
průmyslových	průmyslový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
především	především	k9
v	v	k7c6
USA	USA	kA
a	a	k8xC
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
zhoršujícího	zhoršující	k2eAgMnSc2d1
se	se	k3xPyFc4
stavu	stav	k1gInSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
nutnosti	nutnost	k1gFnSc2
hospodárnějšího	hospodárný	k2eAgNnSc2d2
využívání	využívání	k1gNnSc2
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc1
rozkvět	rozkvět	k1gInSc1
nastal	nastat	k5eAaPmAgInS
především	především	k9
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	rok	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
zemích	země	k1gFnPc6
OECD	OECD	kA
a	a	k8xC
obzvlášť	obzvlášť	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
měnila	měnit	k5eAaImAgFnS
strategie	strategie	k1gFnSc1
ochrany	ochrana	k1gFnSc2
životních	životní	k2eAgNnPc2d1
prostředí	prostředí	k1gNnPc2
-	-	kIx~
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
klást	klást	k5eAaImF
důraz	důraz	k1gInSc1
na	na	k7c4
prevenci	prevence	k1gFnSc4
vzniku	vznik	k1gInSc2
odpadů	odpad	k1gInPc2
a	a	k8xC
na	na	k7c4
inovace	inovace	k1gFnPc4
technologií	technologie	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
méně	málo	k6eAd2
zatěžovaly	zatěžovat	k5eAaImAgFnP
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
vznikala	vznikat	k5eAaImAgFnS
řada	řada	k1gFnSc1
iniciativ	iniciativa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
podporovaly	podporovat	k5eAaImAgFnP
spolupráci	spolupráce	k1gFnSc4
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
vládních	vládní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
a	a	k8xC
akademické	akademický	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
udržitelnějších	udržitelný	k2eAgFnPc2d2
technologií	technologie	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
zavádění	zavádění	k1gNnPc4
do	do	k7c2
praxe	praxe	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1999	#num#	k4
vyšlo	vyjít	k5eAaPmAgNnS
první	první	k4xOgNnSc1
číslo	číslo	k1gNnSc1
časopisu	časopis	k1gInSc2
Green	Green	k2eAgInSc4d1
Chemistry	Chemistr	k1gMnPc7
<g/>
,	,	kIx,
prvního	první	k4xOgNnSc2
vědeckého	vědecký	k2eAgNnSc2d1
periodika	periodikum	k1gNnSc2
zaměřeného	zaměřený	k2eAgNnSc2d1
na	na	k7c4
zelenou	zelený	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Principy	princip	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
zveřejnili	zveřejnit	k5eAaPmAgMnP
Paul	Paul	k1gMnSc1
Anastas	Anastas	k1gMnSc1
a	a	k8xC
John	John	k1gMnSc1
Warner	Warner	k1gMnSc1
dvanáct	dvanáct	k4xCc4
principů	princip	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
definují	definovat	k5eAaBmIp3nP
zelenou	zelený	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prevence	prevence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předcházet	předcházet	k5eAaImF
vzniku	vznik	k1gInSc6
odpadů	odpad	k1gInPc2
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgMnSc1d2
než	než	k8xS
je	být	k5eAaImIp3nS
zpracovávat	zpracovávat	k5eAaImF
a	a	k8xC
likvidovat	likvidovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Atomová	atomový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntézy	syntéza	k1gFnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
plánovat	plánovat	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
konečný	konečný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
maximum	maximum	k1gNnSc4
atomů	atom	k1gInPc2
z	z	k7c2
výchozích	výchozí	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Bezpečnější	bezpečný	k2eAgFnPc1d2
chemické	chemický	k2eAgFnPc1d1
syntézy	syntéza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntetické	syntetický	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
by	by	kYmCp3nP
neměly	mít	k5eNaImAgFnP
využívat	využívat	k5eAaImF,k5eAaPmF
ani	ani	k8xC
generovat	generovat	k5eAaImF
látky	látka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
škodí	škodit	k5eAaImIp3nS
lidskému	lidský	k2eAgNnSc3d1
zdraví	zdraví	k1gNnSc3
nebo	nebo	k8xC
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Bezpečnější	bezpečný	k2eAgFnPc1d2
chemikálie	chemikálie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
chemické	chemický	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
produkt	produkt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
splní	splnit	k5eAaPmIp3nS
očekávanou	očekávaný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
bude	být	k5eAaImBp3nS
co	co	k9
nejméně	málo	k6eAd3
toxický	toxický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Bezpečnější	bezpečný	k2eAgNnPc1d2
rozpouštědla	rozpouštědlo	k1gNnPc1
a	a	k8xC
činidla	činidlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
rozpouštědel	rozpouštědlo	k1gNnPc2
a	a	k8xC
pomocných	pomocný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
omezit	omezit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
nutné	nutný	k2eAgFnPc1d1
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
by	by	kYmCp3nP
být	být	k5eAaImF
co	co	k9
nejbezpečnější	bezpečný	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
účinnost	účinnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procesy	proces	k1gInPc1
by	by	kYmCp3nS
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
navrženy	navrhnout	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
spotřebovávaly	spotřebovávat	k5eAaImAgFnP
co	co	k9
nejméně	málo	k6eAd3
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Využívání	využívání	k1gNnSc1
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suroviny	surovina	k1gFnPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
z	z	k7c2
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
technicky	technicky	k6eAd1
a	a	k8xC
ekonomicky	ekonomicky	k6eAd1
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Omezení	omezení	k1gNnSc1
derivátů	derivát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroky	krok	k1gInPc4
jako	jako	k8xS,k8xC
chránění	chránění	k1gNnSc4
<g/>
/	/	kIx~
<g/>
odchránění	odchránění	k1gNnSc1
nebo	nebo	k8xC
dočasná	dočasný	k2eAgFnSc1d1
derivatizace	derivatizace	k1gFnSc1
by	by	kYmCp3nP
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
omezit	omezit	k5eAaPmF
na	na	k7c4
minimum	minimum	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
spotřebovávají	spotřebovávat	k5eAaImIp3nP
další	další	k2eAgMnPc4d1
reaktanty	reaktant	k1gMnPc4
a	a	k8xC
můžou	můžou	k?
produkovat	produkovat	k5eAaImF
odpady	odpad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Katalýza	katalýza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalyzátory	katalyzátor	k1gInPc7
jsou	být	k5eAaImIp3nP
vhodnější	vhodný	k2eAgFnSc1d2
než	než	k8xS
reaktanty	reaktant	k1gMnPc4
ve	v	k7c6
stechiometrickém	stechiometrický	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Degradovatelnost	Degradovatelnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produkty	produkt	k1gInPc1
by	by	kYmCp3nS
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
navrženy	navrhnout	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
nehromadily	hromadit	k5eNaImAgFnP
v	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
použití	použití	k1gNnSc6
by	by	kYmCp3nP
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
rozložit	rozložit	k5eAaPmF
na	na	k7c4
neškodné	škodný	k2eNgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Průběžná	průběžný	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
jako	jako	k8xS,k8xC
prevence	prevence	k1gFnSc1
znečištění	znečištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
monitorován	monitorovat	k5eAaImNgInS
a	a	k8xC
řízen	řídit	k5eAaImNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nevznikaly	vznikat	k5eNaImAgFnP
nežádoucí	žádoucí	k2eNgInPc4d1
vedlejší	vedlejší	k2eAgInPc4d1
produkty	produkt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Prevence	prevence	k1gFnSc1
nehod	nehoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloučeniny	sloučenina	k1gFnSc2
použité	použitý	k2eAgFnSc2d1
v	v	k7c6
procesu	proces	k1gInSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
forma	forma	k1gFnSc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
zvoleny	zvolit	k5eAaPmNgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
minimalizovala	minimalizovat	k5eAaBmAgFnS
rizika	riziko	k1gNnSc2
úniků	únik	k1gInPc2
<g/>
,	,	kIx,
požárů	požár	k1gInPc2
a	a	k8xC
explozí	exploze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Metriky	metrika	k1gFnPc1
</s>
<s>
Chemické	chemický	k2eAgInPc1d1
postupy	postup	k1gInPc1
lze	lze	k6eAd1
typicky	typicky	k6eAd1
realizovat	realizovat	k5eAaBmF
řadou	řada	k1gFnSc7
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
žádný	žádný	k3yNgInSc1
nesplňuje	splňovat	k5eNaImIp3nS
principy	princip	k1gInPc4
zelené	zelený	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
účely	účel	k1gInPc4
srovnávání	srovnávání	k1gNnSc2
je	být	k5eAaImIp3nS
proto	proto	k8xC
potřeba	potřeba	k1gFnSc1
„	„	k?
<g/>
zelenost	zelenost	k1gFnSc1
<g/>
“	“	k?
nějak	nějak	k6eAd1
kvantifikovat	kvantifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
řada	řada	k1gFnSc1
metrik	metrika	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
například	například	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Atomová	atomový	k2eAgFnSc1d1
efektivita	efektivita	k1gFnSc1
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc4
molekulové	molekulový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
produktu	produkt	k1gInSc2
k	k	k7c3
součtu	součet	k1gInSc3
molekulových	molekulový	k2eAgFnPc2d1
hmotností	hmotnost	k1gFnPc2
všech	všecek	k3xTgMnPc2
reaktantů	reaktant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metrika	metrik	k1gMnSc4
je	být	k5eAaImIp3nS
výhodná	výhodný	k2eAgFnSc1d1
např.	např.	kA
pro	pro	k7c4
předběžné	předběžný	k2eAgNnSc4d1
srovnávání	srovnávání	k1gNnSc4
různých	různý	k2eAgInPc2d1
syntetických	syntetický	k2eAgInPc2d1
postupů	postup	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
nevyžaduje	vyžadovat	k5eNaImIp3nS
žádná	žádný	k3yNgNnPc4
experimentální	experimentální	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zanedbává	zanedbávat	k5eAaImIp3nS
výtěžky	výtěžek	k1gInPc4
reakcí	reakce	k1gFnPc2
<g/>
,	,	kIx,
toxicitu	toxicita	k1gFnSc4
činidel	činidlo	k1gNnPc2
a	a	k8xC
energetickou	energetický	k2eAgFnSc4d1
náročnost	náročnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
E-faktor	E-faktor	k1gInSc1
(	(	kIx(
<g/>
environmentální	environmentální	k2eAgInSc1d1
faktor	faktor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
hmotnosti	hmotnost	k1gFnSc2
produktu	produkt	k1gInSc2
vůči	vůči	k7c3
hmotnosti	hmotnost	k1gFnSc3
všech	všecek	k3xTgInPc2
odpadů	odpad	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
vznikly	vzniknout	k5eAaPmAgInP
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metrika	metrik	k1gMnSc4
umožňuje	umožňovat	k5eAaImIp3nS
rychlé	rychlý	k2eAgNnSc1d1
a	a	k8xC
jednoduché	jednoduchý	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
materiálové	materiálový	k2eAgFnSc2d1
efektivity	efektivita	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
dostupná	dostupný	k2eAgNnPc4d1
experimentální	experimentální	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanedbává	zanedbávat	k5eAaImIp3nS
ale	ale	k9
toxikologické	toxikologický	k2eAgInPc4d1
a	a	k8xC
bezpečnostní	bezpečnostní	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Eko-škála	Eko-škála	k1gFnSc1
je	být	k5eAaImIp3nS
index	index	k1gInSc4
pro	pro	k7c4
zjištění	zjištění	k1gNnSc4
efektivity	efektivita	k1gFnSc2
a	a	k8xC
environmentální	environmentální	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
výše	vysoce	k6eAd2
zmíněným	zmíněný	k2eAgFnPc3d1
metrikám	metrika	k1gFnPc3
je	být	k5eAaImIp3nS
komplexnější	komplexní	k2eAgFnSc1d2
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
např.	např.	kA
i	i	k8xC
hodnocení	hodnocení	k1gNnSc2
toxicity	toxicita	k1gFnSc2
a	a	k8xC
energetické	energetický	k2eAgFnSc2d1
náročnosti	náročnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnocení	hodnocení	k1gNnPc1
podle	podle	k7c2
eko-škály	eko-škála	k1gFnSc2
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
hodnot	hodnota	k1gFnPc2
mezi	mezi	k7c7
0	#num#	k4
a	a	k8xC
100	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
0	#num#	k4
je	být	k5eAaImIp3nS
naprosto	naprosto	k6eAd1
nevyhovující	vyhovující	k2eNgInSc4d1
postup	postup	k1gInSc4
a	a	k8xC
100	#num#	k4
je	být	k5eAaImIp3nS
dokonale	dokonale	k6eAd1
zelený	zelený	k2eAgInSc1d1
postup	postup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítá	počítat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
od	od	k7c2
stovky	stovka	k1gFnSc2
odečítají	odečítat	k5eAaImIp3nP
tzv.	tzv.	kA
trestné	trestný	k2eAgInPc1d1
body	bod	k1gInPc4
podle	podle	k7c2
dané	daný	k2eAgFnSc2d1
metodiky	metodika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Nevýhodou	nevýhoda	k1gFnSc7
eko-škály	eko-škála	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přiřazení	přiřazení	k1gNnSc4
některých	některý	k3yIgInPc2
trestných	trestný	k2eAgInPc2d1
bodů	bod	k1gInPc2
je	být	k5eAaImIp3nS
nejednoznačné	jednoznačný	k2eNgNnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ovlivněno	ovlivnit	k5eAaPmNgNnS
subjektivním	subjektivní	k2eAgNnSc7d1
vnímáním	vnímání	k1gNnSc7
hodnotitele	hodnotitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Green	Green	k2eAgInSc1d1
chemistry	chemistr	k1gMnPc7
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
US	US	kA
EPA	EPA	kA
<g/>
,	,	kIx,
OCSPP	OCSPP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Green	Grena	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
EPA	EPA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-24	2013-01-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LINTHORST	LINTHORST	kA
<g/>
,	,	kIx,
J.	J.	kA
A.	A.	kA
An	An	k1gMnSc1
overview	overview	k?
<g/>
:	:	kIx,
origins	origins	k1gInSc1
and	and	k?
development	development	k1gInSc1
of	of	k?
green	grena	k1gFnPc2
chemistry	chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foundations	Foundations	k1gInSc1
of	of	k?
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1572	#num#	k4
<g/>
-	-	kIx~
<g/>
8463	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
10698	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
9079	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Green	Grena	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Society	societa	k1gFnSc2
of	of	k?
Chemistry	Chemistr	k1gMnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ANASTAS	ANASTAS	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
T.	T.	kA
<g/>
;	;	kIx,
WARNER	WARNER	kA
<g/>
,	,	kIx,
J.C.	J.C.	k1gFnSc1
Green	Grena	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
:	:	kIx,
Theory	Theor	k1gMnPc4
and	and	k?
Practice	Practice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
12	#num#	k4
Principles	Principles	k1gInSc1
of	of	k?
Green	Grena	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
Chemical	Chemical	k1gFnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠIMBERA	ŠIMBERA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
přístupů	přístup	k1gInPc2
Zelené	Zelené	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
na	na	k7c4
vybrané	vybraný	k2eAgFnPc4d1
syntézy	syntéza	k1gFnPc4
pro	pro	k7c4
chemický	chemický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
<	<	kIx(
<g/>
https://is.muni.cz/th/rdfgo/	https://is.muni.cz/th/rdfgo/	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disertační	disertační	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VAN	van	k1gInSc1
AKEN	AKEN	kA
<g/>
,	,	kIx,
Koen	Koen	k1gMnSc1
<g/>
;	;	kIx,
STREKOWSKI	STREKOWSKI	kA
<g/>
,	,	kIx,
Lucjan	Lucjan	k1gInSc1
<g/>
;	;	kIx,
PATINY	patina	k1gFnPc1
<g/>
,	,	kIx,
Luc	Luc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
EcoScale	EcoScala	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
semi-quantitative	semi-quantitativ	k1gInSc5
tool	toonout	k5eAaPmAgInS
to	ten	k3xDgNnSc1
select	select	k2eAgInSc1d1
an	an	k?
organic	organice	k1gFnPc2
preparation	preparation	k1gInSc1
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
economical	economicat	k5eAaPmAgMnS
and	and	k?
ecological	ecologicat	k5eAaPmAgMnS
parameters	parameters	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beilstein	Beilstein	k1gInSc1
Journal	Journal	k1gFnSc2
of	of	k?
Organic	Organice	k1gFnPc2
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1860	#num#	k4
<g/>
-	-	kIx~
<g/>
5397	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.118	10.118	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1860	#num#	k4
<g/>
-	-	kIx~
<g/>
5397	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16542013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
