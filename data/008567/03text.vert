<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Šalamon	Šalamon	k1gMnSc1	Šalamon
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
Trebišov	Trebišov	k1gInSc1	Trebišov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
útočník	útočník	k1gMnSc1	útočník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
jako	jako	k9	jako
obránce	obránce	k1gMnSc1	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
činovník	činovník	k1gMnSc1	činovník
(	(	kIx(	(
<g/>
funkcionář	funkcionář	k1gMnSc1	funkcionář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Trebišově	Trebišov	k1gInSc6	Trebišov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
Sokol	Sokol	k1gInSc4	Sokol
NV	NV	kA	NV
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
dobový	dobový	k2eAgInSc1d1	dobový
název	název	k1gInSc1	název
Slovanu	Slovan	k1gInSc2	Slovan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Iskru	Iskra	k1gFnSc4	Iskra
<g/>
/	/	kIx~	/
<g/>
Dynamo	dynamo	k1gNnSc1	dynamo
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
skóroval	skórovat	k5eAaBmAgMnS	skórovat
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1952	[number]	k4	1952
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Sokol	Sokol	k1gMnSc1	Sokol
NV	NV	kA	NV
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
Kovosmalt	kovosmalt	k1gInSc1	kovosmalt
Trnava	Trnava	k1gFnSc1	Trnava
(	(	kIx(	(
<g/>
nerozhodně	rozhodně	k6eNd1	rozhodně
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
spojce	spojka	k1gFnSc6	spojka
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Hlavatého	Hlavatý	k1gMnSc2	Hlavatý
<g/>
.	.	kIx.	.
<g/>
Začínal	začínat	k5eAaImAgInS	začínat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodišti	rodiště	k1gNnSc6	rodiště
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
hráčem	hráč	k1gMnSc7	hráč
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
Trebišova	Trebišov	k1gInSc2	Trebišov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
hrál	hrát	k5eAaImAgInS	hrát
I.	I.	kA	I.
ligu	liga	k1gFnSc4	liga
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
osobnostem	osobnost	k1gFnPc3	osobnost
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
trebišovské	trebišovský	k2eAgFnSc2d1	Trebišovská
kopané	kopaná	k1gFnSc2	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
v	v	k7c6	v
Nižné	nižný	k2eAgFnSc6d1	Nižná
Myšľe	Myšľe	k1gFnSc6	Myšľe
<g/>
.	.	kIx.	.
<g/>
Trénovali	trénovat	k5eAaImAgMnP	trénovat
jej	on	k3xPp3gInSc4	on
mj.	mj.	kA	mj.
Karol	Karol	k1gInSc4	Karol
Bučko	Bučko	k1gNnSc4	Bučko
<g/>
,	,	kIx,	,
Arpád	Arpád	k1gMnSc1	Arpád
Regecký	Regecký	k2eAgMnSc1d1	Regecký
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Zibrínyi	Zibríny	k1gFnSc2	Zibríny
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Molnár	Molnár	k1gMnSc1	Molnár
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Kertész	Kertész	k1gMnSc1	Kertész
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prvoligová	prvoligový	k2eAgFnSc1d1	prvoligová
bilance	bilance	k1gFnSc1	bilance
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Trenérská	trenérský	k2eAgFnSc1d1	trenérská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1965	[number]	k4	1965
<g/>
/	/	kIx~	/
<g/>
66	[number]	k4	66
trénoval	trénovat	k5eAaImAgMnS	trénovat
druholigový	druholigový	k2eAgMnSc1d1	druholigový
Slavoj	Slavoj	k1gMnSc1	Slavoj
Trebišov	Trebišov	k1gInSc1	Trebišov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převzal	převzít	k5eAaPmAgInS	převzít
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Zibrínyiho	Zibrínyi	k1gMnSc2	Zibrínyi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkcionářská	funkcionářský	k2eAgFnSc1d1	funkcionářská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Trebišově	Trebišov	k1gInSc6	Trebišov
předsedou	předseda	k1gMnSc7	předseda
Okresního	okresní	k2eAgInSc2d1	okresní
výboru	výbor	k1gInSc2	výbor
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
organizaci	organizace	k1gFnSc6	organizace
setrval	setrvat	k5eAaPmAgInS	setrvat
47	[number]	k4	47
let	léto	k1gNnPc2	léto
až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
delegátem	delegát	k1gMnSc7	delegát
SFZ	SFZ	kA	SFZ
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
také	také	k9	také
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Slovenského	slovenský	k2eAgInSc2d1	slovenský
svazu	svaz	k1gInSc2	svaz
tělesné	tělesný	k2eAgFnSc2d1	tělesná
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
na	na	k7c6	na
oblastním	oblastní	k2eAgInSc6d1	oblastní
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
<g/>
:	:	kIx,	:
Československý	československý	k2eAgInSc1d1	československý
fotbal	fotbal	k1gInSc1	fotbal
v	v	k7c6	v
číslech	číslo	k1gNnPc6	číslo
a	a	k8xC	a
faktech	fakt	k1gInPc6	fakt
–	–	k?	–
Olympia	Olympia	k1gFnSc1	Olympia
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
–	–	k?	–
Libri	Libri	k1gNnSc1	Libri
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jenšík	Jenšík	k1gMnSc1	Jenšík
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
–	–	k?	–
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Čech	Čech	k1gMnSc1	Čech
trebišovským	trebišovský	k2eAgInSc7d1	trebišovský
futbalistom	futbalistom	k1gInSc1	futbalistom
storočia	storočium	k1gNnSc2	storočium
<g/>
,	,	kIx,	,
profutbal	profutbal	k1gInSc1	profutbal
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Új	Új	k?	Új
Szó	Szó	k1gFnSc1	Szó
(	(	kIx(	(
<g/>
10.03	[number]	k4	10.03
<g/>
.1956	.1956	k4	.1956
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
A	a	k9	a
köztársasági	köztársasági	k6eAd1	köztársasági
labdarúgó-bajnokság	labdarúgóajnokság	k1gInSc1	labdarúgó-bajnokság
–	–	k?	–
idényének	idényének	k1gInSc1	idényének
rajtja	rajtja	k1gMnSc1	rajtja
előtt	előtt	k1gMnSc1	előtt
<g/>
,	,	kIx,	,
library	librar	k1gInPc1	librar
<g/>
.	.	kIx.	.
<g/>
hungaricana	hungaricana	k1gFnSc1	hungaricana
<g/>
.	.	kIx.	.
<g/>
hu	hu	k0	hu
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
</s>
</p>
