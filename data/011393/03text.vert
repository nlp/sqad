<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
(	(	kIx(	(
<g/>
Megadyptes	Megadyptes	k1gMnSc1	Megadyptes
antipodes	antipodes	k1gMnSc1	antipodes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
žijící	žijící	k2eAgInSc4d1	žijící
druh	druh	k1gInSc4	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
pouze	pouze	k6eAd1	pouze
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
přilehlé	přilehlý	k2eAgInPc4d1	přilehlý
poloostrovy	poloostrov	k1gInPc4	poloostrov
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
3,6	[number]	k4	3,6
<g/>
–	–	k?	–
<g/>
8,9	[number]	k4	8,9
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
žlutý	žlutý	k2eAgInSc1d1	žlutý
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
jednoho	jeden	k4xCgNnSc2	jeden
oka	oko	k1gNnSc2	oko
přes	přes	k7c4	přes
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
až	až	k8xS	až
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
oku	oko	k1gNnSc3	oko
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Duhovky	duhovka	k1gFnPc1	duhovka
jsou	být	k5eAaImIp3nP	být
oranžovo-žluté	oranžovo-žlutý	k2eAgFnPc1d1	oranžovo-žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
kresby	kresba	k1gFnSc2	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
také	také	k9	také
podle	podle	k7c2	podle
zbarvení	zbarvení	k1gNnPc2	zbarvení
duhovek	duhovka	k1gFnPc2	duhovka
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
–	–	k?	–
žlutooký	žlutooký	k2eAgInSc1d1	žlutooký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rybožravý	rybožravý	k2eAgInSc1d1	rybožravý
<g/>
.	.	kIx.	.
</s>
<s>
K	K	kA	K
hnízdění	hnízdění	k1gNnSc1	hnízdění
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
strmé	strmý	k2eAgInPc4d1	strmý
svahy	svah	k1gInPc4	svah
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
porostlé	porostlý	k2eAgNnSc1d1	porostlé
hustou	hustý	k2eAgFnSc7d1	hustá
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
monogamní	monogamní	k2eAgInPc1d1	monogamní
páry	pár	k1gInPc1	pár
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc6d1	malá
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
do	do	k7c2	do
vyhloubené	vyhloubený	k2eAgFnSc2d1	vyhloubená
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
upravené	upravený	k2eAgFnPc1d1	upravená
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
51	[number]	k4	51
dnů	den	k1gInPc2	den
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
téměř	téměř	k6eAd1	téměř
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
nepřetržitě	přetržitě	k6eNd1	přetržitě
rodiči	rodič	k1gMnPc7	rodič
střeženi	střežen	k2eAgMnPc1d1	střežen
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
osamostatní	osamostatnit	k5eAaPmIp3nS	osamostatnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
buďto	buďto	k8xC	buďto
se	se	k3xPyFc4	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
v	v	k7c6	v
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
utváří	utvářet	k5eAaImIp3nP	utvářet
skupinky	skupinka	k1gFnPc1	skupinka
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
až	až	k6eAd1	až
sedmi	sedm	k4xCc2	sedm
vrstevnících	vrstevník	k1gMnPc6	vrstevník
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
opeřena	opeřit	k5eAaPmNgNnP	opeřit
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolonii	kolonie	k1gFnSc4	kolonie
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálý	vyzrálý	k2eAgMnSc1d1	vyzrálý
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejenom	nejenom	k6eAd1	nejenom
kvůli	kvůli	k7c3	kvůli
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
a	a	k8xC	a
kolísavé	kolísavý	k2eAgFnSc3d1	kolísavá
populaci	populace	k1gFnSc3	populace
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4000	[number]	k4	4000
přeživších	přeživší	k2eAgMnPc2d1	přeživší
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
těsnému	těsný	k2eAgInSc3d1	těsný
výskytu	výskyt	k1gInSc3	výskyt
kolonií	kolonie	k1gFnPc2	kolonie
soustředěnému	soustředěný	k2eAgInSc3d1	soustředěný
se	se	k3xPyFc4	se
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
událost	událost	k1gFnSc1	událost
ohrozit	ohrozit	k5eAaPmF	ohrozit
jeho	jeho	k3xOp3gFnSc4	jeho
budoucnost	budoucnost	k1gFnSc4	budoucnost
v	v	k7c6	v
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
žijící	žijící	k2eAgInSc1d1	žijící
druh	druh	k1gInSc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Megadyptes	Megadyptesa	k1gFnPc2	Megadyptesa
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
druh	druh	k1gInSc1	druh
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
-	-	kIx~	-
Megadyptes	Megadyptes	k1gMnSc1	Megadyptes
waitaha	waitaha	k1gMnSc1	waitaha
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
již	již	k6eAd1	již
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
n.	n.	k?	n.
l.	l.	k?	l.
Za	za	k7c4	za
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
může	moct	k5eAaImIp3nS	moct
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
lovecká	lovecký	k2eAgFnSc1d1	lovecká
aktivita	aktivita	k1gFnSc1	aktivita
a	a	k8xC	a
mohutné	mohutný	k2eAgNnSc1d1	mohutné
odlesňování	odlesňování	k1gNnSc1	odlesňování
<g/>
,	,	kIx,	,
když	když	k8xS	když
ostrov	ostrov	k1gInSc1	ostrov
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
obývali	obývat	k5eAaImAgMnP	obývat
průzkumníci	průzkumník	k1gMnPc1	průzkumník
z	z	k7c2	z
Polynésie	Polynésie	k1gFnSc2	Polynésie
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
si	se	k3xPyFc3	se
tito	tento	k3xDgMnPc1	tento
tučňáci	tučňák	k1gMnPc1	tučňák
přirozeně	přirozeně	k6eAd1	přirozeně
konkurovali	konkurovat	k5eAaImAgMnP	konkurovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vymizení	vymizení	k1gNnSc1	vymizení
druhu	druh	k1gInSc2	druh
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
kolonizoval	kolonizovat	k5eAaBmAgMnS	kolonizovat
další	další	k2eAgInPc4d1	další
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
druhu	druh	k1gInSc6	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
od	od	k7c2	od
slavných	slavný	k2eAgMnPc2d1	slavný
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
a	a	k8xC	a
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
Jacquese	Jacques	k1gMnSc2	Jacques
Bernarda	Bernard	k1gMnSc2	Bernard
Hombrona	Hombron	k1gMnSc2	Hombron
a	a	k8xC	a
Honorého	Honorý	k2eAgMnSc2d1	Honorý
Jacquinota	Jacquinota	k1gFnSc1	Jacquinota
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
blízký	blízký	k2eAgMnSc1d1	blízký
příbuzný	příbuzný	k1gMnSc1	příbuzný
tučňáka	tučňák	k1gMnSc2	tučňák
nejmenšího	malý	k2eAgInSc2d3	nejmenší
(	(	kIx(	(
<g/>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInSc1d1	molekulární
výzkum	výzkum	k1gInSc1	výzkum
ale	ale	k9	ale
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
tučňákům	tučňák	k1gMnPc3	tučňák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
oddělil	oddělit	k5eAaPmAgInS	oddělit
asi	asi	k9	asi
před	před	k7c7	před
15	[number]	k4	15
miliony	milion	k4xCgInPc7	milion
lety	let	k1gInPc7	let
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
žlutookého	žlutooký	k2eAgInSc2d1	žlutooký
nebyly	být	k5eNaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
žádné	žádný	k3yNgInPc1	žádný
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
monotypický	monotypický	k2eAgInSc4d1	monotypický
taxon	taxon	k1gInSc4	taxon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
obývá	obývat	k5eAaImIp3nS	obývat
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
poloostrovy	poloostrov	k1gInPc1	poloostrov
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
poolostrov	poolostrov	k1gInSc1	poolostrov
Otago	Otago	k6eAd1	Otago
<g/>
,	,	kIx,	,
Stewartův	Stewartův	k2eAgInSc1d1	Stewartův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Aucklandovy	Aucklandův	k2eAgInPc1d1	Aucklandův
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Campbellovy	Campbellův	k2eAgInPc1d1	Campbellův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
<g/>
Kolonie	kolonie	k1gFnPc1	kolonie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
blízko	blízko	k7c2	blízko
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
k	k	k7c3	k
pobřežním	pobřežní	k2eAgInPc3d1	pobřežní
lesům	les	k1gInPc3	les
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
dobře	dobře	k6eAd1	dobře
ukryty	ukryt	k2eAgFnPc1d1	ukryta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mimo	mimo	k6eAd1	mimo
hnízdním	hnízdní	k2eAgNnSc6d1	hnízdní
období	období	k1gNnSc6	období
obvykle	obvykle	k6eAd1	obvykle
nemigruje	migrovat	k5eNaImIp3nS	migrovat
daleko	daleko	k6eAd1	daleko
a	a	k8xC	a
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
poblíž	poblíž	k7c2	poblíž
svých	svůj	k3xOyFgNnPc2	svůj
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nedospělých	nedospělý	k1gMnPc2	nedospělý
(	(	kIx(	(
<g/>
do	do	k7c2	do
4	[number]	k4	4
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
však	však	k9	však
chovné	chovný	k2eAgFnSc2d1	chovná
oblasti	oblast	k1gFnSc2	oblast
opouštějí	opouštět	k5eAaImIp3nP	opouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistují	existovat	k5eNaImIp3nP	existovat
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
přesném	přesný	k2eAgInSc6d1	přesný
výskytu	výskyt	k1gInSc6	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
táhnou	táhnout	k5eAaImIp3nP	táhnout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
větší	veliký	k2eAgInPc4d2	veliký
druhy	druh	k1gInPc4	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
cm	cm	kA	cm
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
70	[number]	k4	70
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samci	samec	k1gMnPc1	samec
bývají	bývat	k5eAaImIp3nP	bývat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
kolem	kolem	k7c2	kolem
3,6	[number]	k4	3,6
<g/>
–	–	k?	–
<g/>
8,9	[number]	k4	8,9
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
a	a	k8xC	a
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
pečují	pečovat	k5eAaImIp3nP	pečovat
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
a	a	k8xC	a
střídavě	střídavě	k6eAd1	střídavě
hladoví	hladovět	k5eAaImIp3nS	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
hladoví	hladovět	k5eAaImIp3nS	hladovět
při	při	k7c6	při
přepeřování	přepeřování	k1gNnSc6	přepeřování
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
přepeřením	přepeření	k1gNnSc7	přepeření
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
<g/>
,	,	kIx,	,
po	po	k7c6	po
přepeření	přepeření	k1gNnSc6	přepeření
naopak	naopak	k6eAd1	naopak
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
až	až	k8xS	až
kritické	kritický	k2eAgFnPc1d1	kritická
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
se	se	k3xPyFc4	se
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
nelze	lze	k6eNd1	lze
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
splést	splést	k5eAaPmF	splést
<g/>
.	.	kIx.	.
</s>
<s>
Odlišný	odlišný	k2eAgInSc1d1	odlišný
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vydává	vydávat	k5eAaImIp3nS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
má	mít	k5eAaImIp3nS	mít
povětšinou	povětšinou	k6eAd1	povětšinou
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
dvoření	dvoření	k1gNnSc6	dvoření
se	se	k3xPyFc4	se
samici	samice	k1gFnSc3	samice
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
několik	několik	k4yIc4	několik
hlasitých	hlasitý	k2eAgInPc2d1	hlasitý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
trylkování	trylkování	k1gNnSc2	trylkování
<g/>
,	,	kIx,	,
ostrých	ostrý	k2eAgInPc2d1	ostrý
pronikavých	pronikavý	k2eAgInPc2d1	pronikavý
výkřiků	výkřik	k1gInPc2	výkřik
a	a	k8xC	a
vrčení	vrčení	k1gNnSc4	vrčení
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgNnSc1d1	kontaktní
volání	volání	k1gNnSc1	volání
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
dvěma	dva	k4xCgMnPc7	dva
pronikavými	pronikavý	k2eAgMnPc7d1	pronikavý
<g/>
,	,	kIx,	,
dvojslabičnými	dvojslabičný	k2eAgInPc7d1	dvojslabičný
tóny	tón	k1gInPc7	tón
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
žlutý	žlutý	k2eAgInSc1d1	žlutý
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
jednoho	jeden	k4xCgNnSc2	jeden
oka	oko	k1gNnSc2	oko
přes	přes	k7c4	přes
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
až	až	k9	až
k	k	k7c3	k
druhého	druhý	k4xOgMnSc4	druhý
oku	oko	k1gNnSc3	oko
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Duhovky	duhovka	k1gFnPc1	duhovka
jsou	být	k5eAaImIp3nP	být
oranžovo-žluté	oranžovo-žlutý	k2eAgFnPc1d1	oranžovo-žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
kresby	kresba	k1gFnSc2	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
také	také	k9	také
podle	podle	k7c2	podle
zbarvení	zbarvení	k1gNnPc2	zbarvení
očí	oko	k1gNnPc2	oko
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
(	(	kIx(	(
<g/>
žlutooký	žlutooký	k2eAgInSc1d1	žlutooký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
zprvu	zprvu	k6eAd1	zprvu
šedo-hnědá	šedonědý	k2eAgNnPc4d1	šedo-hnědý
a	a	k8xC	a
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
opeření	opeření	k1gNnSc2	opeření
olivově	olivově	k6eAd1	olivově
šedá	šedat	k5eAaImIp3nS	šedat
<g/>
.	.	kIx.	.
</s>
<s>
Nedospělý	nedospělý	k1gMnSc1	nedospělý
–	–	k?	–
pohlavně	pohlavně	k6eAd1	pohlavně
nevyzrálý	vyzrálý	k2eNgMnSc1d1	nevyzrálý
–	–	k?	–
jedinec	jedinec	k1gMnSc1	jedinec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
neúplným	úplný	k2eNgMnSc7d1	neúplný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nejasně	jasně	k6eNd1	jasně
zbarveným	zbarvený	k2eAgInSc7d1	zbarvený
páskem	pásek	k1gInSc7	pásek
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
až	až	k9	až
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
dožil	dožít	k5eAaPmAgMnS	dožít
až	až	k6eAd1	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
ale	ale	k9	ale
zpravidla	zpravidla	k6eAd1	zpravidla
nižšího	nízký	k2eAgInSc2d2	nižší
věku	věk	k1gInSc2	věk
<g/>
;	;	kIx,	;
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jen	jen	k9	jen
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
než	než	k8xS	než
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
rybožravý	rybožravý	k2eAgInSc1d1	rybožravý
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
loví	lovit	k5eAaImIp3nP	lovit
stylem	styl	k1gInSc7	styl
pronásledovaného	pronásledovaný	k2eAgNnSc2d1	pronásledované
potápění	potápění	k1gNnSc2	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
tresek	treska	k1gFnPc2	treska
a	a	k8xC	a
šprotů	šprot	k1gInPc2	šprot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
se	se	k3xPyFc4	se
umí	umět	k5eAaImIp3nS	umět
potopit	potopit	k5eAaPmF	potopit
i	i	k9	i
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
přesahující	přesahující	k2eAgInSc4d1	přesahující
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
mořské	mořský	k2eAgNnSc1d1	mořské
dno	dno	k1gNnSc1	dno
nachází	nacházet	k5eAaImIp3nS	nacházet
hlouběji	hluboko	k6eAd2	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
míří	mířit	k5eAaImIp3nS	mířit
za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
při	při	k7c6	při
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
ke	k	k7c3	k
svým	svůj	k3xOyFgNnPc3	svůj
mláďatům	mládě	k1gNnPc3	mládě
vrací	vracet	k5eAaImIp3nS	vracet
zpravidla	zpravidla	k6eAd1	zpravidla
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
hojnost	hojnost	k1gFnSc1	hojnost
potravy	potrava	k1gFnSc2	potrava
mizivá	mizivý	k2eAgFnSc1d1	mizivá
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
dvou	dva	k4xCgMnPc6	dva
až	až	k9	až
třídenní	třídenní	k2eAgFnSc1d1	třídenní
prodleva	prodleva	k1gFnSc1	prodleva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
predátory	predátor	k1gMnPc4	predátor
tučňáka	tučňák	k1gMnSc4	tučňák
žlutookého	žlutooký	k2eAgMnSc4d1	žlutooký
patří	patřit	k5eAaImIp3nS	patřit
invazivní	invazivní	k2eAgInPc4d1	invazivní
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
lasice	lasice	k1gFnSc2	lasice
a	a	k8xC	a
divoké	divoký	k2eAgFnSc2d1	divoká
kočky	kočka	k1gFnSc2	kočka
domací	domací	k1gFnSc2	domací
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
hnízda	hnízdo	k1gNnPc4	hnízdo
tučňáků	tučňák	k1gMnPc2	tučňák
dobře	dobře	k6eAd1	dobře
skryta	skryt	k2eAgFnSc1d1	skryta
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
takové	takový	k3xDgMnPc4	takový
predátory	predátor	k1gMnPc4	predátor
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
mláďata	mládě	k1gNnPc4	mládě
usmrtit	usmrtit	k5eAaPmF	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
potravou	potrava	k1gFnSc7	potrava
pro	pro	k7c4	pro
barakudy	barakuda	k1gFnPc4	barakuda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
jeho	jeho	k3xOp3gFnSc4	jeho
přeživší	přeživší	k2eAgFnSc4d1	přeživší
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
nejspíš	nejspíš	k9	nejspíš
mladé	mladý	k2eAgMnPc4d1	mladý
nezkušené	zkušený	k2eNgMnPc4d1	nezkušený
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
smrtelně	smrtelně	k6eAd1	smrtelně
poraní	poranit	k5eAaPmIp3nS	poranit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Campbellově	Campbellův	k2eAgInSc6d1	Campbellův
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
také	také	k9	také
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
lachtany	lachtan	k1gMnPc4	lachtan
novozélandskými	novozélandský	k2eAgFnPc7d1	novozélandská
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
v	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
chov	chov	k1gInSc1	chov
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Rozmnožovací	rozmnožovací	k2eAgFnPc1d1	rozmnožovací
aktivity	aktivita	k1gFnPc1	aktivita
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k6eAd1	až
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
monogamní	monogamní	k2eAgNnSc1d1	monogamní
<g/>
,	,	kIx,	,
a	a	k8xC	a
pár	pár	k4xCyI	pár
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
a	a	k8xC	a
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
jedinci	jedinec	k1gMnPc1	jedinec
mohou	moct	k5eAaImIp3nP	moct
ale	ale	k9	ale
tuto	tento	k3xDgFnSc4	tento
procentuální	procentuální	k2eAgFnSc4d1	procentuální
bilanci	bilance	k1gFnSc4	bilance
narušit	narušit	k5eAaPmF	narušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páry	pár	k1gInPc1	pár
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc6d1	malá
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hnízdění	hnízdění	k1gNnPc1	hnízdění
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
strmé	strmý	k2eAgInPc4d1	strmý
svahy	svah	k1gInPc4	svah
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgInPc1d1	porostlý
vysokou	vysoký	k2eAgFnSc7d1	vysoká
trávou	tráva	k1gFnSc7	tráva
či	či	k8xC	či
křovím	křoví	k1gNnSc7	křoví
(	(	kIx(	(
<g/>
takovému	takový	k3xDgInSc3	takový
biotopu	biotop	k1gInSc3	biotop
se	se	k3xPyFc4	se
nepřizpůsobil	přizpůsobit	k5eNaPmAgMnS	přizpůsobit
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
druh	druh	k1gMnSc1	druh
tučňáka	tučňák	k1gMnSc2	tučňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nejdále	daleko	k6eAd3	daleko
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
hnízdo	hnízdo	k1gNnSc1	hnízdo
ležící	ležící	k2eAgNnSc1d1	ležící
až	až	k9	až
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
hnízda	hnízdo	k1gNnPc4	hnízdo
dostatečně	dostatečně	k6eAd1	dostatečně
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
,	,	kIx,	,
nechová	chovat	k5eNaImIp3nS	chovat
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
nikterak	nikterak	k6eAd1	nikterak
agresivně	agresivně	k6eAd1	agresivně
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
snese	snést	k5eAaPmIp3nS	snést
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
při	při	k7c6	při
hájení	hájení	k1gNnSc6	hájení
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
teritoriálním	teritoriální	k2eAgNnSc7d1	teritoriální
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Pakliže	pakliže	k8xS	pakliže
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
konfrontaci	konfrontace	k1gFnSc3	konfrontace
se	s	k7c7	s
sokem	sok	k1gMnSc7	sok
<g/>
,	,	kIx,	,
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
obranný	obranný	k2eAgInSc1d1	obranný
postoj	postoj	k1gInSc1	postoj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
krk	krk	k1gInSc4	krk
a	a	k8xC	a
zobák	zobák	k1gInSc4	zobák
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
–	–	k?	–
jako	jako	k9	jako
když	když	k8xS	když
rytíř	rytíř	k1gMnSc1	rytíř
tasí	tasit	k5eAaPmIp3nS	tasit
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
postojem	postoj	k1gInSc7	postoj
zase	zase	k9	zase
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
soka	sok	k1gMnSc4	sok
neustálým	neustálý	k2eAgNnSc7d1	neustálé
otáčením	otáčení	k1gNnSc7	otáčení
hlavy	hlava	k1gFnSc2	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bedlivě	bedlivě	k6eAd1	bedlivě
sleduje	sledovat	k5eAaImIp3nS	sledovat
jeho	jeho	k3xOp3gInSc1	jeho
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
se	se	k3xPyFc4	se
při	při	k7c6	při
takovém	takový	k3xDgInSc6	takový
vzrušení	vzrušení	k1gNnSc6	vzrušení
načechrává	načechrávat	k5eAaImIp3nS	načechrávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
jeho	jeho	k3xOp3gFnSc2	jeho
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
,	,	kIx,	,
ohání	ohánět	k5eAaImIp3nS	ohánět
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
silným	silný	k2eAgInSc7d1	silný
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
si	se	k3xPyFc3	se
také	také	k9	také
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
samice	samice	k1gFnSc1	samice
vejce	vejce	k1gNnSc2	vejce
snese	snést	k5eAaPmIp3nS	snést
<g/>
,	,	kIx,	,
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
pár	pár	k1gInSc1	pár
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
do	do	k7c2	do
vyhloubené	vyhloubený	k2eAgFnSc2d1	vyhloubená
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
upravené	upravený	k2eAgFnPc1d1	upravená
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
<g/>
,	,	kIx,	,
asi	asi	k9	asi
dvouleté	dvouletý	k2eAgFnSc2d1	dvouletá
samice	samice	k1gFnSc2	samice
snáší	snášet	k5eAaImIp3nS	snášet
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
neplodné	plodný	k2eNgNnSc1d1	neplodné
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
tříleté	tříletý	k2eAgFnSc2d1	tříletá
a	a	k8xC	a
starší	starší	k1gMnPc4	starší
snáší	snášet	k5eAaImIp3nS	snášet
jediné	jediný	k2eAgNnSc1d1	jediné
vejce	vejce	k1gNnSc1	vejce
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
sta	sto	k4xCgNnSc2	sto
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Rozestup	rozestup	k1gInSc1	rozestup
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
snesenými	snesený	k2eAgMnPc7d1	snesený
vejci	vejce	k1gNnPc7	vejce
je	on	k3xPp3gMnPc4	on
tří	tři	k4xCgFnPc2	tři
až	až	k8xS	až
pětidenní	pětidenní	k2eAgInPc1d1	pětidenní
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnSc1	vejce
váží	vážit	k5eAaImIp3nS	vážit
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
138	[number]	k4	138
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
podobně	podobně	k6eAd1	podobně
velká	velký	k2eAgNnPc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
jsou	být	k5eAaImIp3nP	být
oválná	oválný	k2eAgFnSc1d1	oválná
a	a	k8xC	a
skořápka	skořápka	k1gFnSc1	skořápka
má	mít	k5eAaImIp3nS	mít
hladký	hladký	k2eAgInSc4d1	hladký
a	a	k8xC	a
matný	matný	k2eAgInSc4d1	matný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
jsou	být	k5eAaImIp3nP	být
namodralá	namodralý	k2eAgNnPc1d1	namodralé
nebo	nebo	k8xC	nebo
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
příštích	příští	k2eAgInPc2d1	příští
24	[number]	k4	24
až	až	k9	až
36	[number]	k4	36
hodin	hodina	k1gFnPc2	hodina
zbělají	zbělat	k5eAaPmIp3nP	zbělat
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
51	[number]	k4	51
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
sedět	sedět	k5eAaImF	sedět
začnou	začít	k5eAaPmIp3nP	začít
až	až	k6eAd1	až
po	po	k7c6	po
snesení	snesení	k1gNnSc1	snesení
druhého	druhý	k4xOgNnSc2	druhý
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
střídá	střídat	k5eAaImIp3nS	střídat
zhruba	zhruba	k6eAd1	zhruba
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
a	a	k8xC	a
tak	tak	k6eAd1	tak
hladoví	hladovět	k5eAaImIp3nS	hladovět
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
<g/>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
téměř	téměř	k6eAd1	téměř
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jsou	být	k5eAaImIp3nP	být
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
zhruba	zhruba	k6eAd1	zhruba
108	[number]	k4	108
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
osamocená	osamocený	k2eAgNnPc1d1	osamocené
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
buďto	buďto	k8xC	buďto
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
v	v	k7c6	v
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
utváří	utvářet	k5eAaImIp3nP	utvářet
skupinky	skupinka	k1gFnPc1	skupinka
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
až	až	k6eAd1	až
sedmi	sedm	k4xCc2	sedm
vrstevnících	vrstevník	k1gMnPc6	vrstevník
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
procento	procento	k1gNnSc1	procento
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
odváží	odvážit	k5eAaPmIp3nS	odvážit
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nP	vracet
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
anebo	anebo	k8xC	anebo
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
<g/>
,	,	kIx,	,
a	a	k8xC	a
mladé	mladý	k2eAgFnPc1d1	mladá
nakrmí	nakrmit	k5eAaPmIp3nP	nakrmit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	on	k3xPp3gFnPc4	on
potravy	potrava	k1gFnPc4	potrava
dostatek	dostatek	k1gInSc1	dostatek
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
potomky	potomek	k1gMnPc7	potomek
vychovat	vychovat	k5eAaPmF	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
páru	pár	k1gInSc2	pár
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
osud	osud	k1gInSc1	osud
mláďat	mládě	k1gNnPc2	mládě
zpečetěn	zpečetěn	k2eAgMnSc1d1	zpečetěn
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
bývá	bývat	k5eAaImIp3nS	bývat
pravidlem	pravidlo	k1gNnSc7	pravidlo
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potravy	potrava	k1gFnSc2	potrava
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
rostou	růst	k5eAaImIp3nP	růst
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
malých	malý	k2eAgFnPc2d1	malá
hmotností	hmotnost	k1gFnPc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
tak	tak	k9	tak
uhynou	uhynout	k5eAaPmIp3nP	uhynout
ještě	ještě	k9	ještě
před	před	k7c7	před
posledním	poslední	k2eAgNnSc7d1	poslední
opeřením	opeření	k1gNnSc7	opeření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
mohla	moct	k5eAaImAgFnS	moct
sama	sám	k3xTgFnSc1	sám
nakrmit	nakrmit	k5eAaPmF	nakrmit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
opeřena	opeřit	k5eAaPmNgNnP	opeřit
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kolonii	kolonie	k1gFnSc3	kolonie
opustí	opustit	k5eAaPmIp3nP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálá	vyzrálý	k2eAgNnPc1d1	vyzrálé
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
línají	línat	k5eAaImIp3nP	línat
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
až	až	k6eAd1	až
čtyř	čtyři	k4xCgMnPc2	čtyři
týdenním	týdenní	k2eAgNnSc6d1	týdenní
období	období	k1gNnSc6	období
nemohou	moct	k5eNaImIp3nP	moct
lovit	lovit	k5eAaImF	lovit
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zranitelní	zranitelný	k2eAgMnPc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populace	populace	k1gFnSc2	populace
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc2	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňáka	tučňák	k1gMnSc4	tučňák
žlutookého	žlutooký	k2eAgInSc2d1	žlutooký
postihuje	postihovat	k5eAaImIp3nS	postihovat
řada	řada	k1gFnSc1	řada
chorob	choroba	k1gFnPc2	choroba
i	i	k8xC	i
neznámého	známý	k2eNgInSc2d1	neznámý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandě	Zéland	k1gInSc6	Zéland
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
1990	[number]	k4	1990
polovina	polovina	k1gFnSc1	polovina
místních	místní	k2eAgInPc2d1	místní
chovných	chovný	k2eAgInPc2d1	chovný
párů	pár	k1gInPc2	pár
kvůli	kvůli	k7c3	kvůli
nespecifikované	specifikovaný	k2eNgFnSc3d1	nespecifikovaná
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
uhynulo	uhynout	k5eAaPmAgNnS	uhynout
50-60	[number]	k4	50-60
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Otago	Otago	k1gNnSc1	Otago
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
bylo	být	k5eAaImAgNnS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
infekcí	infekce	k1gFnPc2	infekce
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
záškrt	záškrt	k1gInSc1	záškrt
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k8xS	jako
diphtheritic	diphtheritice	k1gFnPc2	diphtheritice
stomatitis	stomatitis	k1gFnSc1	stomatitis
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sekundární	sekundární	k2eAgFnSc4d1	sekundární
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
a	a	k8xC	a
primární	primární	k2eAgInSc1d1	primární
patogen	patogen	k1gInSc1	patogen
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
problém	problém	k1gInSc1	problém
postihl	postihnout	k5eAaPmAgInS	postihnout
i	i	k9	i
populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
Stewartových	Stewartových	k2eAgInPc6d1	Stewartových
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neznámé	známý	k2eNgFnSc2d1	neznámá
příčiny	příčina	k1gFnSc2	příčina
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
také	také	k9	také
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
dospělých	dospělý	k2eAgMnPc2d1	dospělý
tučňáků	tučňák	k1gMnPc2	tučňák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
Asi	asi	k9	asi
největší	veliký	k2eAgFnSc4d3	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
tohoto	tento	k3xDgMnSc4	tento
ptáka	pták	k1gMnSc4	pták
představuje	představovat	k5eAaImIp3nS	představovat
invazivní	invazivní	k2eAgInSc1d1	invazivní
(	(	kIx(	(
<g/>
nepůvodní	původní	k2eNgInSc1d1	nepůvodní
<g/>
)	)	kIx)	)
druh	druh	k1gInSc1	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
člověk	člověk	k1gMnSc1	člověk
zavlekl	zavleknout	k5eAaPmAgMnS	zavleknout
(	(	kIx(	(
<g/>
zatoulaní	zatoulaný	k2eAgMnPc1d1	zatoulaný
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
lasice	lasice	k1gFnPc1	lasice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
jak	jak	k6eAd1	jak
dospělé	dospělý	k2eAgFnPc1d1	dospělá
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
útoky	útok	k1gInPc1	útok
barakud	barakuda	k1gFnPc2	barakuda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
převážně	převážně	k6eAd1	převážně
smrtelná	smrtelný	k2eAgNnPc4d1	smrtelné
poranění	poranění	k1gNnPc4	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
nekorigovaný	korigovaný	k2eNgInSc1d1	nekorigovaný
rybolov	rybolov	k1gInSc1	rybolov
–	–	k?	–
bývá	bývat	k5eAaImIp3nS	bývat
lapen	lapit	k5eAaPmNgInS	lapit
v	v	k7c6	v
rybářských	rybářský	k2eAgFnPc6d1	rybářská
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
takové	takový	k3xDgInPc1	takový
zásahy	zásah	k1gInPc1	zásah
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
hojnost	hojnost	k1gFnSc4	hojnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
expertů	expert	k1gMnPc2	expert
se	se	k3xPyFc4	se
ale	ale	k9	ale
především	především	k6eAd1	především
oteplují	oteplovat	k5eAaImIp3nP	oteplovat
oceány	oceán	k1gInPc1	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
potravní	potravní	k2eAgInPc4d1	potravní
zdroje	zdroj	k1gInPc4	zdroj
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Znepokojující	znepokojující	k2eAgFnSc1d1	znepokojující
je	být	k5eAaImIp3nS	být
také	také	k9	také
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgNnSc3	který
je	být	k5eAaImIp3nS	být
narušován	narušován	k2eAgInSc1d1	narušován
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
klid	klid	k1gInSc1	klid
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
jako	jako	k8xS	jako
ikonické	ikonický	k2eAgNnSc1d1	ikonické
zvíře	zvíře	k1gNnSc1	zvíře
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
láká	lákat	k5eAaImIp3nS	lákat
mnoho	mnoho	k4c1	mnoho
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
člověk	člověk	k1gMnSc1	člověk
ústavičně	ústavičně	k6eAd1	ústavičně
ničí	ničit	k5eAaImIp3nS	ničit
pobřežní	pobřežní	k2eAgInPc4d1	pobřežní
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
následně	následně	k6eAd1	následně
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
narušování	narušování	k1gNnSc3	narušování
jeho	on	k3xPp3gInSc2	on
biotopu	biotop	k1gInSc2	biotop
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc4d1	vzácný
a	a	k8xC	a
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nejenom	nejenom	k6eAd1	nejenom
kvůli	kvůli	k7c3	kvůli
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
a	a	k8xC	a
kolísavé	kolísavý	k2eAgFnSc3d1	kolísavá
populaci	populace	k1gFnSc3	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
rozloze	rozloha	k1gFnSc3	rozloha
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
nevelkého	velký	k2eNgInSc2d1	nevelký
počtu	počet	k1gInSc2	počet
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
událost	událost	k1gFnSc1	událost
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
přežívá	přežívat	k5eAaImIp3nS	přežívat
už	už	k6eAd1	už
jen	jen	k9	jen
necelých	celý	k2eNgInPc2d1	necelý
4000	[number]	k4	4000
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Yellow-eyed	Yellowyed	k1gMnSc1	Yellow-eyed
Penguin	Penguin	k1gMnSc1	Penguin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
70	[number]	k4	70
<g/>
,	,	kIx,	,
71	[number]	k4	71
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Megadyptes	Megadyptes	k1gInSc1	Megadyptes
antipodes	antipodes	k1gInSc1	antipodes
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
na	na	k7c4	na
BioLib	BioLib	k1gInSc4	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
potravy	potrava	k1gFnSc2	potrava
</s>
</p>
