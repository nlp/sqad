<s>
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
nebo	nebo	k8xC	nebo
také	také	k9	také
jednorázová	jednorázový	k2eAgFnSc1d1	jednorázová
tabulková	tabulkový	k2eAgFnSc1d1	tabulková
šifra	šifra	k1gFnSc1	šifra
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
one-time	oneimat	k5eAaPmIp3nS	one-timat
pad	padnout	k5eAaPmDgInS	padnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
šifrovací	šifrovací	k2eAgInSc4d1	šifrovací
postup	postup	k1gInSc4	postup
patentovaný	patentovaný	k2eAgInSc4d1	patentovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Gilbertem	Gilbert	k1gMnSc7	Gilbert
Vernamem	Vernam	k1gInSc7	Vernam
<g/>
.	.	kIx.	.
</s>
