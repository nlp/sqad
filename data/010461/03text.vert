<p>
<s>
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
nebo	nebo	k8xC	nebo
také	také	k9	také
jednorázová	jednorázový	k2eAgFnSc1d1	jednorázová
tabulková	tabulkový	k2eAgFnSc1d1	tabulková
šifra	šifra	k1gFnSc1	šifra
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
one-time	oneimat	k5eAaPmIp3nS	one-timat
pad	padnout	k5eAaPmDgInS	padnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
šifrovací	šifrovací	k2eAgInSc4d1	šifrovací
postup	postup	k1gInSc4	postup
patentovaný	patentovaný	k2eAgInSc4d1	patentovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Gilbertem	Gilbert	k1gMnSc7	Gilbert
Vernamem	Vernam	k1gInSc7	Vernam
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
posunu	posun	k1gInSc6	posun
každého	každý	k3xTgInSc2	každý
znaku	znak	k1gInSc2	znak
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c4	o
náhodně	náhodně	k6eAd1	náhodně
zvolený	zvolený	k2eAgInSc4d1	zvolený
počet	počet	k1gInSc4	počet
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
rovná	rovnat	k5eAaImIp3nS	rovnat
náhradě	náhrada	k1gFnSc3	náhrada
zcela	zcela	k6eAd1	zcela
náhodným	náhodný	k2eAgNnSc7d1	náhodné
písmenem	písmeno	k1gNnSc7	písmeno
a	a	k8xC	a
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
faktu	fakt	k1gInSc6	fakt
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
nerozluštitelná	rozluštitelný	k2eNgFnSc1d1	nerozluštitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postup	postup	k1gInSc4	postup
šifrování	šifrování	k1gNnSc2	šifrování
==	==	k?	==
</s>
</p>
<p>
<s>
Vezmeme	vzít	k5eAaPmIp1nP	vzít
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
písmena	písmeno	k1gNnPc4	písmeno
tajné	tajný	k2eAgFnPc4d1	tajná
zprávy	zpráva	k1gFnPc4	zpráva
a	a	k8xC	a
každé	každý	k3xTgFnPc4	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
posuneme	posunout	k5eAaPmIp1nP	posunout
o	o	k7c4	o
několik	několik	k4yIc4	několik
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
první	první	k4xOgNnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
je	být	k5eAaImIp3nS	být
posunuto	posunout	k5eAaPmNgNnS	posunout
o	o	k7c4	o
5	[number]	k4	5
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
o	o	k7c4	o
1	[number]	k4	1
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
o	o	k7c4	o
14	[number]	k4	14
<g/>
,	,	kIx,	,
čtvrté	čtvrtá	k1gFnPc1	čtvrtá
o	o	k7c4	o
24	[number]	k4	24
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
o	o	k7c4	o
9	[number]	k4	9
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
při	při	k7c6	při
posouvání	posouvání	k1gNnSc6	posouvání
překročíme	překročit	k5eAaPmIp1nP	překročit
konec	konec	k1gInSc4	konec
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
pokračujeme	pokračovat	k5eAaImIp1nP	pokračovat
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
ALDEBARAN	ALDEBARAN	kA	ALDEBARAN
tak	tak	k6eAd1	tak
dostaneme	dostat	k5eAaPmIp1nP	dostat
šifrový	šifrový	k2eAgInSc4d1	šifrový
text	text	k1gInSc4	text
FMRCKAUJG	FMRCKAUJG	kA	FMRCKAUJG
<g/>
.	.	kIx.	.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
5	[number]	k4	5
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
19	[number]	k4	19
je	být	k5eAaImIp3nS	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
rozluštění	rozluštění	k1gNnSc3	rozluštění
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
snadno	snadno	k6eAd1	snadno
posunout	posunout	k5eAaPmF	posunout
písmena	písmeno	k1gNnPc4	písmeno
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
původní	původní	k2eAgInSc4d1	původní
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
znalosti	znalost	k1gFnSc2	znalost
klíče	klíč	k1gInSc2	klíč
je	být	k5eAaImIp3nS	být
luštění	luštění	k1gNnSc4	luštění
odposlechnuté	odposlechnutý	k2eAgFnSc2d1	odposlechnutá
zprávy	zpráva	k1gFnSc2	zpráva
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmínky	podmínka	k1gFnPc1	podmínka
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
==	==	k?	==
</s>
</p>
<p>
<s>
Porušení	porušení	k1gNnSc1	porušení
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
podmínek	podmínka	k1gFnPc2	podmínka
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
oslabení	oslabení	k1gNnSc4	oslabení
šifry	šifra	k1gFnSc2	šifra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
nerozluštitelná	rozluštitelný	k2eNgFnSc1d1	nerozluštitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k8xC	jako
přenášená	přenášený	k2eAgFnSc1d1	přenášená
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
šifrovací	šifrovací	k2eAgInPc1d1	šifrovací
systémy	systém	k1gInPc1	systém
používají	používat	k5eAaImIp3nP	používat
kratší	krátký	k2eAgInPc1d2	kratší
klíče	klíč	k1gInPc1	klíč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
možných	možný	k2eAgInPc2d1	možný
klíčů	klíč	k1gInPc2	klíč
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
počet	počet	k1gInSc1	počet
možných	možný	k2eAgFnPc2d1	možná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgInSc1d2	kratší
klíč	klíč	k1gInSc1	klíč
v	v	k7c6	v
principu	princip	k1gInSc6	princip
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
útok	útok	k1gInSc4	útok
hrubou	hrubý	k2eAgFnSc7d1	hrubá
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
náhodný	náhodný	k2eAgInSc1d1	náhodný
<g/>
.	.	kIx.	.
</s>
<s>
Nepřipadají	připadat	k5eNaImIp3nP	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
počítačové	počítačový	k2eAgInPc1d1	počítačový
generátory	generátor	k1gInPc1	generátor
pseudonáhodných	pseudonáhodný	k2eAgNnPc2d1	pseudonáhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gFnSc4	jejich
činnost	činnost	k1gFnSc4	činnost
lze	lze	k6eAd1	lze
předvídat	předvídat	k5eAaImF	předvídat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
užití	užití	k1gNnSc1	užití
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
metod	metoda	k1gFnPc2	metoda
-	-	kIx~	-
hardwarový	hardwarový	k2eAgInSc1d1	hardwarový
generátor	generátor	k1gInSc1	generátor
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klíč	klíč	k1gInSc1	klíč
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podmínka	podmínka	k1gFnSc1	podmínka
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
důsledkem	důsledek	k1gInSc7	důsledek
předchozí	předchozí	k2eAgFnSc2d1	předchozí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
klíč	klíč	k1gInSc4	klíč
není	být	k5eNaImIp3nS	být
náhodný	náhodný	k2eAgMnSc1d1	náhodný
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
-li	i	k?	-li
útočník	útočník	k1gMnSc1	útočník
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
dvě	dva	k4xCgFnPc4	dva
zprávy	zpráva	k1gFnPc4	zpráva
zašifrované	zašifrovaný	k2eAgFnSc2d1	zašifrovaná
týmž	týž	k3xTgInSc7	týž
klíčem	klíč	k1gInSc7	klíč
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
snadnou	snadný	k2eAgFnSc4d1	snadná
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
rozluštění	rozluštění	k1gNnSc3	rozluštění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možnosti	možnost	k1gFnSc3	možnost
útoku	útok	k1gInSc2	útok
==	==	k?	==
</s>
</p>
<p>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
kryptoanalýza	kryptoanalýza	k1gFnSc1	kryptoanalýza
je	být	k5eAaImIp3nS	být
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
náhodným	náhodný	k2eAgInSc7d1	náhodný
charakterem	charakter	k1gInSc7	charakter
šifrového	šifrový	k2eAgInSc2d1	šifrový
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
zjistit	zjistit	k5eAaPmF	zjistit
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
četnosti	četnost	k1gFnPc4	četnost
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
zprávě	zpráva	k1gFnSc6	zpráva
ani	ani	k9	ani
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
skupinami	skupina	k1gFnPc7	skupina
znaků	znak	k1gInPc2	znak
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
písmene	písmeno	k1gNnSc2	písmeno
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jiné	jiný	k2eAgNnSc1d1	jiné
zcela	zcela	k6eAd1	zcela
náhodně	náhodně	k6eAd1	náhodně
volené	volený	k2eAgNnSc4d1	volené
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
útok	útok	k1gInSc4	útok
hrubou	hrubý	k2eAgFnSc7d1	hrubá
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
kterému	který	k3yQgInSc3	který
není	být	k5eNaImIp3nS	být
odolná	odolný	k2eAgFnSc1d1	odolná
prakticky	prakticky	k6eAd1	prakticky
žádná	žádný	k3yNgFnSc1	žádný
jiná	jiný	k2eAgFnSc1d1	jiná
šifra	šifra	k1gFnSc1	šifra
<g/>
,	,	kIx,	,
neuspěje	uspět	k5eNaPmIp3nS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
měl	mít	k5eAaImAgMnS	mít
útočník	útočník	k1gMnSc1	útočník
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
neomezený	omezený	k2eNgInSc4d1	neomezený
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
kvantové	kvantový	k2eAgInPc4d1	kvantový
počítače	počítač	k1gInPc4	počítač
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
systematicky	systematicky	k6eAd1	systematicky
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
klíče	klíč	k1gInPc4	klíč
délky	délka	k1gFnSc2	délka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
výsledkem	výsledek	k1gInSc7	výsledek
snažení	snažení	k1gNnSc2	snažení
bude	být	k5eAaImBp3nS	být
pouze	pouze	k6eAd1	pouze
posloupnost	posloupnost	k1gFnSc4	posloupnost
všech	všecek	k3xTgFnPc2	všecek
možných	možný	k2eAgFnPc2d1	možná
zpráv	zpráva	k1gFnPc2	zpráva
délky	délka	k1gFnSc2	délka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Nebude	být	k5eNaImBp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nalézt	nalézt	k5eAaPmF	nalézt
tu	ten	k3xDgFnSc4	ten
správnou	správný	k2eAgFnSc4d1	správná
<g/>
,	,	kIx,	,
nezíská	získat	k5eNaPmIp3nS	získat
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
žádnou	žádný	k3yNgFnSc4	žádný
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
pořadí	pořadí	k1gNnSc1	pořadí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
zprávy	zpráva	k1gFnPc4	zpráva
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
neřekne	říct	k5eNaPmIp3nS	říct
útočníkovi	útočník	k1gMnSc3	útočník
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
náhodné	náhodný	k2eAgFnSc2d1	náhodná
volby	volba	k1gFnSc2	volba
klíče	klíč	k1gInSc2	klíč
je	být	k5eAaImIp3nS	být
také	také	k9	také
zcela	zcela	k6eAd1	zcela
náhodné	náhodný	k2eAgNnSc1d1	náhodné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důkaz	důkaz	k1gInSc4	důkaz
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
==	==	k?	==
</s>
</p>
<p>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
Vernam	Vernam	k1gInSc4	Vernam
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
nerozluštitelná	rozluštitelný	k2eNgFnSc1d1	nerozluštitelná
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
exaktním	exaktní	k2eAgInSc7d1	exaktní
důkazem	důkaz	k1gInSc7	důkaz
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgMnS	přijít
až	až	k9	až
</s>
</p>
<p>
<s>
C.	C.	kA	C.
E.	E.	kA	E.
Shannon	Shannon	k1gMnSc1	Shannon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc4	důkaz
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgMnS	založit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
náhodný	náhodný	k2eAgInSc1d1	náhodný
posun	posun	k1gInSc1	posun
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
nahrazení	nahrazení	k1gNnSc1	nahrazení
zcela	zcela	k6eAd1	zcela
náhodným	náhodný	k2eAgNnSc7d1	náhodné
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
šifrovaný	šifrovaný	k2eAgInSc4d1	šifrovaný
text	text	k1gInSc4	text
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
zcela	zcela	k6eAd1	zcela
náhodné	náhodný	k2eAgFnSc2d1	náhodná
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Považujeme	považovat	k5eAaImIp1nP	považovat
<g/>
-li	i	k?	-li
tajnou	tajný	k2eAgFnSc4d1	tajná
zprávu	zpráva	k1gFnSc4	zpráva
za	za	k7c4	za
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
veličinu	veličina	k1gFnSc4	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
klíč	klíč	k1gInSc1	klíč
za	za	k7c4	za
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
veličinu	veličina	k1gFnSc4	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
rovnoměrné	rovnoměrný	k2eAgNnSc4d1	rovnoměrné
rozložení	rozložení	k1gNnSc4	rozložení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c4	na
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zašifrovaná	zašifrovaný	k2eAgFnSc1d1	zašifrovaná
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
také	také	k9	také
náhodou	náhodou	k6eAd1	náhodou
veličinou	veličina	k1gFnSc7	veličina
s	s	k7c7	s
rovnoměrným	rovnoměrný	k2eAgNnSc7d1	rovnoměrné
rozložením	rozložení	k1gNnSc7	rozložení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c4	na
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
šifrový	šifrový	k2eAgInSc4d1	šifrový
text	text	k1gInSc4	text
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádnou	žádný	k3yNgFnSc4	žádný
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
původní	původní	k2eAgFnSc6d1	původní
zprávě	zpráva	k1gFnSc6	zpráva
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
útočník	útočník	k1gMnSc1	útočník
v	v	k7c6	v
principu	princip	k1gInSc6	princip
nemá	mít	k5eNaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
cokoli	cokoli	k3yInSc4	cokoli
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Binární	binární	k2eAgFnSc1d1	binární
varianta	varianta	k1gFnSc1	varianta
==	==	k?	==
</s>
</p>
<p>
<s>
Dodejme	dodat	k5eAaPmRp1nP	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
funguje	fungovat	k5eAaImIp3nS	fungovat
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
nad	nad	k7c7	nad
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
množinou	množina	k1gFnSc7	množina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jen	jen	k9	jen
0	[number]	k4	0
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
digitální	digitální	k2eAgInPc4d1	digitální
počítače	počítač	k1gInPc4	počítač
pracující	pracující	k2eAgInPc4d1	pracující
ve	v	k7c6	v
dvojkové	dvojkový	k2eAgFnSc6d1	dvojková
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Posun	posun	k1gInSc4	posun
písmen	písmeno	k1gNnPc2	písmeno
nad	nad	k7c7	nad
dvouznakovou	dvouznakový	k2eAgFnSc7d1	dvouznaková
abecedou	abeceda	k1gFnSc7	abeceda
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
1	[number]	k4	1
místo	místo	k6eAd1	místo
nebo	nebo	k8xC	nebo
0	[number]	k4	0
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
klíčem	klíč	k1gInSc7	klíč
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
také	také	k9	také
posloupnost	posloupnost	k1gFnSc1	posloupnost
0	[number]	k4	0
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
bit	bit	k1gInSc1	bit
klíče	klíč	k1gInSc2	klíč
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jednomu	jeden	k4xCgNnSc3	jeden
bitu	bit	k2eAgFnSc4d1	bita
zprávy	zpráva	k1gFnPc4	zpráva
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
bit	bit	k1gInSc1	bit
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
šifrování	šifrování	k1gNnSc6	šifrování
změnit	změnit	k5eAaPmF	změnit
nebo	nebo	k8xC	nebo
nechat	nechat	k5eAaPmF	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
operace	operace	k1gFnSc1	operace
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
bity	bit	k1gInPc7	bit
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
XOR	XOR	kA	XOR
(	(	kIx(	(
<g/>
výlučné	výlučný	k2eAgNnSc4d1	výlučné
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
exclusive	exclusiev	k1gFnSc2	exclusiev
or	or	k?	or
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⊗	⊗	k?	⊗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
otimes	otimes	k1gInSc4	otimes
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dešifrování	dešifrování	k1gNnSc3	dešifrování
stačí	stačit	k5eAaBmIp3nS	stačit
vzít	vzít	k5eAaPmF	vzít
šifrovaný	šifrovaný	k2eAgInSc4d1	šifrovaný
text	text	k1gInSc4	text
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
XOR	XOR	kA	XOR
znovu	znovu	k6eAd1	znovu
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
klíčem	klíč	k1gInSc7	klíč
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
jako	jako	k9	jako
posun	posun	k1gInSc1	posun
písmen	písmeno	k1gNnPc2	písmeno
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Změněné	změněný	k2eAgInPc4d1	změněný
bity	bit	k1gInPc4	bit
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
nezměněné	změněný	k2eNgFnPc1d1	nezměněná
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opakování	opakování	k1gNnSc2	opakování
klíče	klíč	k1gInSc2	klíč
===	===	k?	===
</s>
</p>
<p>
<s>
Binární	binární	k2eAgFnSc1d1	binární
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
obzvláště	obzvláště	k6eAd1	obzvláště
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
opakované	opakovaný	k2eAgNnSc4d1	opakované
použití	použití	k1gNnSc4	použití
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
vlastnost	vlastnost	k1gFnSc1	vlastnost
operace	operace	k1gFnSc2	operace
XOR	XOR	kA	XOR
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
⊗	⊗	k?	⊗
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⊗	⊗	k?	⊗
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
⊗	⊗	k?	⊗
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
⊗	⊗	k?	⊗
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
otimes	otimes	k1gInSc1	otimes
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
otimes	otimes	k1gMnSc1	otimes
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
otimes	otimes	k1gMnSc1	otimes
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
otimes	otimes	k1gInSc1	otimes
B	B	kA	B
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
útočník	útočník	k1gMnSc1	útočník
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
dvě	dva	k4xCgFnPc1	dva
zprávy	zpráva	k1gFnPc1	zpráva
šifrované	šifrovaný	k2eAgFnPc1d1	šifrovaná
týmž	týž	k3xTgInSc7	týž
klíčem	klíč	k1gInSc7	klíč
a	a	k8xC	a
provede	provést	k5eAaPmIp3nS	provést
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
XOR	XOR	kA	XOR
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
XOR	XOR	kA	XOR
dvou	dva	k4xCgFnPc6	dva
původních	původní	k2eAgFnPc2d1	původní
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
zbaví	zbavit	k5eAaPmIp3nS	zbavit
se	se	k3xPyFc4	se
veškeré	veškerý	k3xTgFnSc2	veškerý
náhodnosti	náhodnost	k1gFnSc2	náhodnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
klíči	klíč	k1gInSc6	klíč
a	a	k8xC	a
která	který	k3yQgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
šifře	šifra	k1gFnSc6	šifra
její	její	k3xOp3gFnSc4	její
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
kryptoanalýza	kryptoanalýza	k1gFnSc1	kryptoanalýza
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
docela	docela	k6eAd1	docela
lehce	lehko	k6eAd1	lehko
umožní	umožnit	k5eAaPmIp3nS	umožnit
zprávy	zpráva	k1gFnPc4	zpráva
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Popsané	popsaný	k2eAgNnSc1d1	popsané
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
klíčem	klíč	k1gInSc7	klíč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
náhodný	náhodný	k2eAgMnSc1d1	náhodný
klíč	klíč	k1gInSc4	klíč
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
generování	generování	k1gNnSc1	generování
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
klíč	klíč	k1gInSc1	klíč
zná	znát	k5eAaImIp3nS	znát
pouze	pouze	k6eAd1	pouze
odesilatel	odesilatel	k1gMnSc1	odesilatel
a	a	k8xC	a
příjemce	příjemce	k1gMnSc1	příjemce
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Komunikující	komunikující	k2eAgFnPc1d1	komunikující
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
předem	předem	k6eAd1	předem
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
klíči	klíč	k1gInSc6	klíč
nějakým	nějaký	k3yIgInSc7	nějaký
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
hned	hned	k6eAd1	hned
po	po	k7c6	po
odeslání	odeslání	k1gNnSc6	odeslání
první	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
klíč	klíč	k1gInSc4	klíč
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
mohli	moct	k5eAaImAgMnP	moct
bezpečně	bezpečně	k6eAd1	bezpečně
odeslat	odeslat	k5eAaPmF	odeslat
třeba	třeba	k9	třeba
2	[number]	k4	2
MB	MB	kA	MB
tajných	tajný	k2eAgFnPc2d1	tajná
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
předem	předem	k6eAd1	předem
bezpečně	bezpečně	k6eAd1	bezpečně
odeslat	odeslat	k5eAaPmF	odeslat
2	[number]	k4	2
MB	MB	kA	MB
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
klíč	klíč	k1gInSc1	klíč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
používala	používat	k5eAaImAgFnS	používat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
krytí	krytí	k1gNnSc4	krytí
horké	horký	k2eAgFnSc2d1	horká
linky	linka	k1gFnSc2	linka
mezi	mezi	k7c7	mezi
Moskvou	Moskva	k1gFnSc7	Moskva
a	a	k8xC	a
Washingtonem	Washington	k1gInSc7	Washington
za	za	k7c2	za
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
špioni	špion	k1gMnPc1	špion
tuto	tento	k3xDgFnSc4	tento
šifru	šifra	k1gFnSc4	šifra
využívali	využívat	k5eAaPmAgMnP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
historici	historik	k1gMnPc1	historik
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
zpráv	zpráva	k1gFnPc2	zpráva
zašifrovaných	zašifrovaný	k2eAgFnPc2d1	zašifrovaná
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Tajný	tajný	k2eAgInSc1d1	tajný
text	text	k1gInSc1	text
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
zůstane	zůstat	k5eAaPmIp3nS	zůstat
skryt	skrýt	k5eAaPmNgInS	skrýt
navěky	navěky	k6eAd1	navěky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
výhledy	výhled	k1gInPc1	výhled
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
problému	problém	k1gInSc2	problém
distribuce	distribuce	k1gFnSc2	distribuce
klíče	klíč	k1gInSc2	klíč
dala	dát	k5eAaPmAgFnS	dát
Vernamově	Vernamově	k1gFnSc4	Vernamově
šifře	šifra	k1gFnSc3	šifra
kvantová	kvantový	k2eAgFnSc1d1	kvantová
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
matematickou	matematický	k2eAgFnSc4d1	matematická
nepodmíněnou	podmíněný	k2eNgFnSc4d1	nepodmíněná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
Vernamovy	Vernamův	k2eAgFnSc2d1	Vernamova
šifry	šifra	k1gFnSc2	šifra
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
nepodmíněně	podmíněně	k6eNd1	podmíněně
bezpečnou	bezpečný	k2eAgFnSc7d1	bezpečná
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
metodou	metoda	k1gFnSc7	metoda
přenosu	přenos	k1gInSc2	přenos
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Monoalfabetická	Monoalfabetický	k2eAgFnSc1d1	Monoalfabetická
šifra	šifra	k1gFnSc1	šifra
</s>
</p>
<p>
<s>
Vigenè	Vigenè	k?	Vigenè
šifra	šifra	k1gFnSc1	šifra
</s>
</p>
<p>
<s>
Homofonní	homofonní	k2eAgFnSc1d1	homofonní
šifra	šifra	k1gFnSc1	šifra
</s>
</p>
<p>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
šifra	šifra	k1gFnSc1	šifra
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hála	Hála	k1gMnSc1	Hála
<g/>
:	:	kIx,	:
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
kryptografie	kryptografie	k1gFnSc1	kryptografie
–	–	k?	–
Aldebaran	Aldebaran	k1gInSc1	Aldebaran
bulletin	bulletin	k1gInSc1	bulletin
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Simon	Simon	k1gMnSc1	Simon
Singh	Singh	k1gMnSc1	Singh
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
kódů	kód	k1gInPc2	kód
a	a	k8xC	a
šifer	šifra	k1gFnPc2	šifra
<g/>
,	,	kIx,	,
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
