<s>
Tunis	Tunis	k1gInSc1	Tunis
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Túnis	Túnis	k1gFnSc1	Túnis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
žilo	žít	k5eAaImAgNnS	žít
780	[number]	k4	780
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
aglomeraci	aglomerace	k1gFnSc6	aglomerace
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tunis	Tunis	k1gInSc1	Tunis
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tuniska	Tunisko	k1gNnSc2	Tunisko
poblíž	poblíž	k7c2	poblíž
jezera	jezero	k1gNnSc2	jezero
Le	Le	k1gMnSc2	Le
Lac	Lac	k1gMnSc2	Lac
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
150	[number]	k4	150
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
pouhých	pouhý	k2eAgInPc2d1	pouhý
3	[number]	k4	3
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
okolo	okolo	k7c2	okolo
26	[number]	k4	26
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
spadne	spadnout	k5eAaPmIp3nS	spadnout
průměrně	průměrně	k6eAd1	průměrně
70	[number]	k4	70
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
0	[number]	k4	0
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Tunis	Tunis	k1gInSc1	Tunis
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
osadu	osada	k1gFnSc4	osada
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Kartágo	Kartágo	k1gNnSc4	Kartágo
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
814	[number]	k4	814
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
založili	založit	k5eAaPmAgMnP	založit
Féničané	Féničan	k1gMnPc1	Féničan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
3	[number]	k4	3
<g/>
.	.	kIx.	.
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
srovnali	srovnat	k5eAaPmAgMnP	srovnat
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadil	usadit	k5eAaPmAgInS	usadit
germánský	germánský	k2eAgInSc4d1	germánský
kmen	kmen	k1gInSc4	kmen
Vandalů	Vandal	k1gMnPc2	Vandal
<g/>
,	,	kIx,	,
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oblast	oblast	k1gFnSc1	oblast
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
tehdy	tehdy	k6eAd1	tehdy
mocná	mocný	k2eAgFnSc1d1	mocná
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Tunis	Tunis	k1gInSc1	Tunis
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
arabské	arabský	k2eAgNnSc1d1	arabské
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
získal	získat	k5eAaPmAgInS	získat
status	status	k1gInSc1	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
východomagribského	východomagribský	k2eAgInSc2d1	východomagribský
státu	stát	k1gInSc2	stát
Chavsidů	Chavsid	k1gInPc2	Chavsid
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
město	město	k1gNnSc4	město
dobyla	dobýt	k5eAaPmAgFnS	dobýt
vojska	vojsko	k1gNnSc2	vojsko
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1574	[number]	k4	1574
se	se	k3xPyFc4	se
Tunis	Tunis	k1gInSc1	Tunis
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
její	její	k3xOp3gFnSc2	její
tuniské	tuniský	k2eAgFnSc2d1	Tuniská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
měl	mít	k5eAaImAgInS	mít
Tunis	Tunis	k1gInSc1	Tunis
roli	role	k1gFnSc4	role
administrativního	administrativní	k2eAgNnSc2d1	administrativní
střediska	středisko	k1gNnSc2	středisko
francouzského	francouzský	k2eAgInSc2d1	francouzský
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Tunis	Tunis	k1gInSc1	Tunis
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
stojí	stát	k5eAaImIp3nS	stát
citadela	citadela	k1gFnSc1	citadela
Kasba	Kasba	k1gFnSc1	Kasba
s	s	k7c7	s
mešitou	mešita	k1gFnSc7	mešita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
Arabů	Arab	k1gMnPc2	Arab
Medina	Medina	k1gFnSc1	Medina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
islámských	islámský	k2eAgFnPc2d1	islámská
středověkých	středověký	k2eAgFnPc2d1	středověká
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
středem	střed	k1gInSc7	střed
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
mešita	mešita	k1gFnSc1	mešita
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
Muslimská	muslimský	k2eAgFnSc1d1	muslimská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
paláci	palác	k1gInSc6	palác
beje	bej	k1gMnSc2	bej
sídlí	sídlet	k5eAaImIp3nP	sídlet
muzeum	muzeum	k1gNnSc4	muzeum
Bardo	Bardo	k1gNnSc1	Bardo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
mnoho	mnoho	k4c1	mnoho
památek	památka	k1gFnPc2	památka
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
na	na	k7c6	na
ruinách	ruina	k1gFnPc6	ruina
Antoniových	Antoniův	k2eAgFnPc2d1	Antoniova
lázní	lázeň	k1gFnPc2	lázeň
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Medina	Medina	k1gFnSc1	Medina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zbylo	zbýt	k5eAaPmAgNnS	zbýt
z	z	k7c2	z
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Tunis	Tunis	k1gInSc1	Tunis
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
začala	začít	k5eAaPmAgNnP	začít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
podle	podle	k7c2	podle
Habíba	Habíb	k1gMnSc2	Habíb
Burgiba	Burgib	k1gMnSc2	Burgib
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
prezidenta	prezident	k1gMnSc2	prezident
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Lemují	lemovat	k5eAaImIp3nP	lemovat
ji	on	k3xPp3gFnSc4	on
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nP	sídlet
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnPc1d1	obchodní
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgInPc1d1	vládní
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Obytné	obytný	k2eAgFnPc1d1	obytná
čtvrti	čtvrt	k1gFnPc1	čtvrt
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
parku	park	k1gInSc3	park
Belveder	Belvedra	k1gFnPc2	Belvedra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zalesněných	zalesněný	k2eAgInPc6d1	zalesněný
svazích	svah	k1gInPc6	svah
parku	park	k1gInSc2	park
stojí	stát	k5eAaImIp3nS	stát
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
muslimský	muslimský	k2eAgInSc1d1	muslimský
pavilon	pavilon	k1gInSc1	pavilon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
stojí	stát	k5eAaImIp3nS	stát
řada	řada	k1gFnSc1	řada
potravinářských	potravinářský	k2eAgMnPc2d1	potravinářský
<g/>
,	,	kIx,	,
strojírenských	strojírenský	k2eAgInPc2d1	strojírenský
<g/>
,	,	kIx,	,
hutnických	hutnický	k2eAgInPc2d1	hutnický
<g/>
,	,	kIx,	,
cementárenských	cementárenský	k2eAgInPc2d1	cementárenský
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
rafinerie	rafinerie	k1gFnSc1	rafinerie
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
dnešního	dnešní	k2eAgInSc2d1	dnešní
názvu	název	k1gInSc2	název
Tunisu	Tunis	k1gInSc2	Tunis
sahá	sahat	k5eAaImIp3nS	sahat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
do	do	k7c2	do
období	období	k1gNnSc2	období
před	před	k7c7	před
punskými	punský	k2eAgFnPc7d1	punská
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Polybios	Polybios	k1gMnSc1	Polybios
z	z	k7c2	z
Megalapole	Megalapole	k1gFnSc2	Megalapole
(	(	kIx(	(
<g/>
asi	asi	k9	asi
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
120	[number]	k4	120
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
Tínés	Tínés	k1gInSc1	Tínés
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
město	město	k1gNnSc4	město
nazývali	nazývat	k5eAaImAgMnP	nazývat
buď	buď	k8xC	buď
Túnus	Túnus	k1gInSc4	Túnus
nebo	nebo	k8xC	nebo
Túnis	Túnis	k1gInSc4	Túnis
<g/>
.	.	kIx.	.
</s>
<s>
Ibn	Ibn	k?	Ibn
Chaldún	Chaldún	k1gInSc1	Chaldún
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1332	[number]	k4	1332
-	-	kIx~	-
1406	[number]	k4	1406
<g/>
)	)	kIx)	)
-	-	kIx~	-
arabský	arabský	k2eAgMnSc1d1	arabský
učenec	učenec	k1gMnSc1	učenec
a	a	k8xC	a
všestranný	všestranný	k2eAgMnSc1d1	všestranný
vědec	vědec	k1gMnSc1	vědec
Fuád	Fuád	k1gMnSc1	Fuád
Mebazá	Mebazá	k1gMnSc1	Mebazá
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
tuniský	tuniský	k2eAgMnSc1d1	tuniský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Tuniska	Tunisko	k1gNnSc2	Tunisko
Nicola	Nicola	k1gFnSc1	Nicola
Pietrangeli	Pietrangel	k1gInSc6	Pietrangel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
italský	italský	k2eAgMnSc1d1	italský
tenista	tenista	k1gMnSc1	tenista
Georges	Georges	k1gMnSc1	Georges
Wolinski	Wolinske	k1gFnSc4	Wolinske
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1934	[number]	k4	1934
-	-	kIx~	-
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
francouzský	francouzský	k2eAgMnSc1d1	francouzský
karikaturista	karikaturista	k1gMnSc1	karikaturista
Claudia	Claudia	k1gFnSc1	Claudia
Cardinalová	Cardinalová	k1gFnSc1	Cardinalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
italská	italský	k2eAgFnSc1d1	italská
herečka	herečka	k1gFnSc1	herečka
Bertrand	Bertrand	k1gInSc1	Bertrand
Delanoë	Delanoë	k1gFnSc1	Delanoë
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
-	-	kIx~	-
2004	[number]	k4	2004
Marc	Marc	k1gInSc1	Marc
Gicquel	Gicquel	k1gInSc4	Gicquel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
francouzský	francouzský	k2eAgMnSc1d1	francouzský
tenista	tenista	k1gMnSc1	tenista
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tunis	Tunis	k1gInSc4	Tunis
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Tunis	Tunis	k1gInSc4	Tunis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
