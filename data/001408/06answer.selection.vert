<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Württembersku	Württembersko	k1gNnSc6	Württembersko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
