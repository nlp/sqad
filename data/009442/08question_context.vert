<s>
Viktorija	Viktorij	k2eAgFnSc1d1	Viktorija
Jermoljeva	Jermoljeva	k1gFnSc1	Jermoljeva
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
В	В	k?	В
Є	Є	k?	Є
<g/>
,	,	kIx,	,
narozena	narozen	k2eAgFnSc1d1	narozena
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
pianistka	pianistka	k1gFnSc1	pianistka
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
hrála	hrát	k5eAaImAgFnS	hrát
pouze	pouze	k6eAd1	pouze
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
i	i	k9	i
několik	několik	k4yIc4	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
slavnou	slavný	k2eAgFnSc7d1	slavná
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
kanálu	kanál	k1gInSc2	kanál
Youtube	Youtub	k1gInSc5	Youtub
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
vkgoeswild	vkgoeswild	k6eAd1	vkgoeswild
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>

