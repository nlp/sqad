<p>
<s>
Viktorija	Viktorij	k2eAgFnSc1d1	Viktorija
Jermoljeva	Jermoljeva	k1gFnSc1	Jermoljeva
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
В	В	k?	В
Є	Є	k?	Є
<g/>
,	,	kIx,	,
narozena	narozen	k2eAgFnSc1d1	narozena
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
pianistka	pianistka	k1gFnSc1	pianistka
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
hrála	hrát	k5eAaImAgFnS	hrát
pouze	pouze	k6eAd1	pouze
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
i	i	k9	i
několik	několik	k4yIc4	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
slavnou	slavný	k2eAgFnSc7d1	slavná
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
kanálu	kanál	k1gInSc2	kanál
Youtube	Youtub	k1gInSc5	Youtub
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
vkgoeswild	vkgoeswild	k6eAd1	vkgoeswild
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
kanálu	kanál	k1gInSc6	kanál
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
videa	video	k1gNnSc2	video
slavných	slavný	k2eAgFnPc2d1	slavná
rockových	rockový	k2eAgFnPc2d1	rocková
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
sama	sám	k3xTgFnSc1	sám
přetransformovala	přetransformovat	k5eAaPmAgFnS	přetransformovat
pro	pro	k7c4	pro
piáno	piáno	k?	piáno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
videa	video	k1gNnPc1	video
už	už	k9	už
viděli	vidět	k5eAaImAgMnP	vidět
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
úspěchu	úspěch	k1gInSc3	úspěch
postupně	postupně	k6eAd1	postupně
přešla	přejít	k5eAaPmAgNnP	přejít
z	z	k7c2	z
hraní	hraní	k1gNnSc2	hraní
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
</p>
