<s>
Zaraapelta	Zaraapelta	k1gFnSc1
</s>
<s>
ZaraapeltaStratigrafický	ZaraapeltaStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Svrchní	svrchní	k2eAgFnSc1d1
křída	křída	k1gFnSc1
<g/>
,	,	kIx,
před	před	k7c7
75	#num#	k4
až	až	k8xS
71	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc1
</s>
<s>
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
Dinosauria	Dinosaurium	k1gNnPc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
ptakopánví	ptakopánví	k1gNnSc1
(	(	kIx(
<g/>
Ornithischia	Ornithischia	k1gFnSc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
Thyreophora	Thyreophora	k1gFnSc1
Infrařád	Infrařáda	k1gFnPc2
</s>
<s>
Ankylosauria	Ankylosaurium	k1gNnPc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Ankylosauridae	Ankylosauridae	k1gFnSc1
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
Ankylosaurinae	Ankylosaurinae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
ZaraapeltaArbourová	ZaraapeltaArbourový	k2eAgFnSc1d1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2014	#num#	k4
Binomické	binomický	k2eAgInPc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Zaraapelta	Zaraapelta	k1gFnSc1
nomadisArbourová	nomadisArbourový	k2eAgFnSc1d1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2014	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zaraapelta	Zaraapelta	k1gFnSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
ježčí	ježčí	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
podle	podle	k7c2
"	"	kIx"
<g/>
ostnitého	ostnitý	k2eAgInSc2d1
<g/>
"	"	kIx"
profilu	profil	k1gInSc2
tělního	tělní	k2eAgInSc2d1
pancíře	pancíř	k1gInSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
ankylosauridního	ankylosauridní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
žijícího	žijící	k2eAgMnSc2d1
v	v	k7c6
období	období	k1gNnSc6
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
(	(	kIx(
<g/>
souvrství	souvrství	k1gNnSc1
Barun	Baruna	k1gFnPc2
Gojot	Gojota	k1gFnPc2
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
75	#num#	k4
až	až	k8xS
71	#num#	k4
miliony	milion	k4xCgInPc7
let	rok	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
současného	současný	k2eAgNnSc2d1
Mongolska	Mongolsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Holotyp	Holotyp	k1gInSc4
tohoto	tento	k3xDgMnSc2
dinosaura	dinosaurus	k1gMnSc2
(	(	kIx(
<g/>
kat	kat	k1gMnSc1
<g/>
.	.	kIx.
ozn	ozn	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
MPC	MPC	kA
D-	D-	k1gFnSc1
<g/>
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1388	#num#	k4
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
nekompletní	kompletní	k2eNgFnSc4d1
fosilní	fosilní	k2eAgFnSc4d1
lebku	lebka	k1gFnSc4
<g/>
,	,	kIx,
dlouhou	dlouhý	k2eAgFnSc4d1
kolem	kolem	k6eAd1
40	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
objevena	objevit	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
Robertem	Robert	k1gMnSc7
Gabbardem	Gabbard	k1gMnSc7
<g/>
,	,	kIx,
členem	člen	k1gMnSc7
paleontologické	paleontologický	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
kanadským	kanadský	k2eAgInSc7d1
paleontologem	paleontolog	k1gMnSc7
Philipem	Philip	k1gInSc7
J.	J.	kA
Curriem	Currium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
lebka	lebka	k1gFnSc1
popsána	popsán	k2eAgFnSc1d1
roku	rok	k1gInSc2
2014	#num#	k4
Victorií	Victorie	k1gFnPc2
Megan	Megana	k1gFnPc2
Arbourovou	Arbourův	k2eAgFnSc7d1
a	a	k8xC
jejími	její	k3xOp3gMnPc7
kolegy	kolega	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Typovým	typový	k2eAgInSc7d1
a	a	k8xC
jediným	jediný	k2eAgInSc7d1
známým	známý	k2eAgInSc7d1
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
Z.	Z.	kA
nomadis	nomadis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zařazení	zařazení	k1gNnSc1
</s>
<s>
Zaraapelta	Zaraapelta	k1gMnSc1
byl	být	k5eAaImAgMnS
rod	rod	k1gInSc4
<g/>
,	,	kIx,
spadající	spadající	k2eAgMnSc1d1
do	do	k7c2
čeledi	čeleď	k1gFnSc2
Ankylosauridae	Ankylosaurida	k1gFnSc2
a	a	k8xC
do	do	k7c2
podčeledi	podčeleď	k1gFnSc2
Ankylosaurinae	Ankylosaurina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc3
nejbližším	blízký	k2eAgMnPc3d3
příbuzným	příbuzný	k1gMnPc3
byl	být	k5eAaImAgInS
zřejmě	zřejmě	k6eAd1
rod	rod	k1gInSc1
Tarchia	Tarchium	k1gNnSc2
<g/>
,	,	kIx,
o	o	k7c4
trochu	trocha	k1gFnSc4
vzdálenějším	vzdálený	k2eAgInSc7d2
rodem	rod	k1gInSc7
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
Saichania	Saichanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Arbour	Arbour	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
M.	M.	kA
<g/>
;	;	kIx,
Currie	Currie	k1gFnSc1
<g/>
,	,	kIx,
P.	P.	kA
J.	J.	kA
<g/>
;	;	kIx,
Badamgarav	Badamgarav	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gFnSc1
ankylosaurid	ankylosaurid	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
of	of	k?
the	the	k?
Upper	Upper	k1gMnSc1
Cretaceous	Cretaceous	k1gMnSc1
Baruungoyot	Baruungoyot	k1gMnSc1
and	and	k?
Nemegt	Nemegt	k1gMnSc1
formations	formations	k6eAd1
of	of	k?
Mongolia	Mongolia	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoological	Zoological	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
Linnean	Linnean	k1gInSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
172	#num#	k4
<g/>
:	:	kIx,
631	#num#	k4
<g/>
–	–	k?
<g/>
652	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
zoj	zoj	k?
<g/>
.12185	.12185	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Arbour	Arboura	k1gFnPc2
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
M.	M.	kA
<g/>
;	;	kIx,
Evans	Evans	k1gInSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
C.	C.	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
new	new	k?
ankylosaurine	ankylosaurin	k1gInSc5
dinosaur	dinosaura	k1gFnPc2
from	from	k1gInSc4
the	the	k?
Judith	Judith	k1gInSc1
River	River	k1gMnSc1
Formation	Formation	k1gInSc1
of	of	k?
Montana	Montana	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
an	an	k?
exceptional	exceptionat	k5eAaImAgInS,k5eAaPmAgInS
skeleton	skeleton	k1gInSc1
with	with	k1gInSc1
soft	soft	k?
tissue	tissue	k1gInSc1
preservation	preservation	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Society	societa	k1gFnSc2
Open	Opena	k1gFnPc2
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
161086	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsos	rsos	k6eAd1
<g/>
.161086	.161086	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c6
databázi	databáze	k1gFnSc6
Fossilworks	Fossilworksa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
