<s>
Zaraapelta	Zaraapelta	k1gFnSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
ježčí	ježčí	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
podle	podle	k7c2
"	"	kIx"
<g/>
ostnitého	ostnitý	k2eAgInSc2d1
<g/>
"	"	kIx"
profilu	profil	k1gInSc2
tělního	tělní	k2eAgInSc2d1
pancíře	pancíř	k1gInSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
ankylosauridního	ankylosauridní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
žijícího	žijící	k2eAgMnSc2d1
v	v	k7c6
období	období	k1gNnSc6
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
(	(	kIx(
<g/>
souvrství	souvrství	k1gNnSc1
Barun	Baruna	k1gFnPc2
Gojot	Gojota	k1gFnPc2
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
75	#num#	k4
až	až	k8xS
71	#num#	k4
miliony	milion	k4xCgInPc7
let	rok	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
současného	současný	k2eAgNnSc2d1
Mongolska	Mongolsko	k1gNnSc2
<g/>
.	.	kIx.
</s>