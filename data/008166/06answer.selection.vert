<s>
Bachovo	Bachův	k2eAgNnSc1d1	Bachovo
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
hudby	hudba	k1gFnSc2	hudba
počínaje	počínaje	k7c7	počínaje
W.	W.	kA	W.
A.	A.	kA	A.
Mozartem	Mozart	k1gMnSc7	Mozart
a	a	k8xC	a
Ludwigem	Ludwig	k1gMnSc7	Ludwig
van	vana	k1gFnPc2	vana
Beethovenem	Beethoven	k1gMnSc7	Beethoven
až	až	k9	až
po	po	k7c4	po
Arnolda	Arnold	k1gMnSc4	Arnold
Schoenberga	Schoenberg	k1gMnSc4	Schoenberg
nebo	nebo	k8xC	nebo
Henryka	Henryek	k1gMnSc4	Henryek
Góreckého	Górecký	k2eAgMnSc4d1	Górecký
<g/>
.	.	kIx.	.
</s>
