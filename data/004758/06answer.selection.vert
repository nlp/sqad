<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
genocida	genocida	k1gFnSc1	genocida
<g/>
"	"	kIx"	"
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Raphael	Raphael	k1gMnSc1	Raphael
Lemkin	Lemkin	k1gMnSc1	Lemkin
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
polsko-židovského	polsko-židovský	k2eAgInSc2d1	polsko-židovský
původu	původ	k1gInSc2	původ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
kořenu	kořen	k1gInSc2	kořen
slova	slovo	k1gNnSc2	slovo
génos	génosa	k1gFnPc2	génosa
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narození	narození	k1gNnSc1	narození
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
a	a	k8xC	a
latinské	latinský	k2eAgFnSc2d1	Latinská
přípony	přípona	k1gFnSc2	přípona
-	-	kIx~	-
<g/>
cidium	cidium	k1gNnSc4	cidium
(	(	kIx(	(
<g/>
zabít	zabít	k5eAaPmF	zabít
<g/>
)	)	kIx)	)
skrze	skrze	k?	skrze
francouzské	francouzský	k2eAgFnPc4d1	francouzská
-	-	kIx~	-
<g/>
cide	cid	k1gFnPc4	cid
<g/>
.	.	kIx.	.
</s>
