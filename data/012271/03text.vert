<p>
<s>
Psohlavci	psohlavec	k1gMnSc3	psohlavec
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
a	a	k8xC	a
nejdříve	dříve	k6eAd3	dříve
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
života	život	k1gInSc2	život
vyšel	vyjít	k5eAaPmAgMnS	vyjít
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
osmadvacetkrát	osmadvacetkrát	k6eAd1	osmadvacetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
v	v	k7c6	v
románu	román	k1gInSc6	román
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
chodským	chodský	k2eAgNnSc7d1	Chodské
nářečím	nářečí	k1gNnSc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgMnPc1	tři
Chodové	Chod	k1gMnPc1	Chod
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
<g/>
,	,	kIx,	,
Kryštof	Kryštof	k1gMnSc1	Kryštof
Hrubý	Hrubý	k1gMnSc1	Hrubý
a	a	k8xC	a
Matěj	Matěj	k1gMnSc1	Matěj
Přibek	Přibek	k1gMnSc1	Přibek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
románu	román	k1gInSc2	román
"	"	kIx"	"
<g/>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
přezdívku	přezdívka	k1gFnSc4	přezdívka
Chodů	Chod	k1gMnPc2	Chod
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
nadávky	nadávka	k1gFnSc2	nadávka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
exotická	exotický	k2eAgFnSc1d1	exotická
obluda	obluda	k1gFnSc1	obluda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
nabývá	nabývat	k5eAaImIp3nS	nabývat
nádechu	nádech	k1gInSc3	nádech
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
zpracování	zpracování	k1gNnSc1	zpracování
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
vůdci	vůdce	k1gMnSc6	vůdce
Chodů	chod	k1gInPc2	chod
Janu	Jan	k1gMnSc6	Jan
Kozinovi	Kozin	k1gMnSc6	Kozin
<g/>
.	.	kIx.	.
</s>
<s>
Chodové	Chod	k1gMnPc1	Chod
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
střežit	střežit	k5eAaImF	střežit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Čechami	Čechy	k1gFnPc7	Čechy
a	a	k8xC	a
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
přímými	přímý	k2eAgInPc7d1	přímý
poddanými	poddaná	k1gFnPc7	poddaná
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
a	a	k8xC	a
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
těšili	těšit	k5eAaImAgMnP	těšit
mnohým	mnohý	k2eAgFnPc3d1	mnohá
privilegiím	privilegium	k1gNnPc3	privilegium
a	a	k8xC	a
výsadám	výsada	k1gFnPc3	výsada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	být	k5eAaImIp3nS	být
sociálně	sociálně	k6eAd1	sociálně
vydělovaly	vydělovat	k5eAaImAgFnP	vydělovat
z	z	k7c2	z
okolního	okolní	k2eAgNnSc2d1	okolní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
Chodové	Chod	k1gMnPc1	Chod
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
přišli	přijít	k5eAaPmAgMnP	přijít
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
pozemky	pozemka	k1gFnPc1	pozemka
byly	být	k5eAaImAgFnP	být
prodány	prodat	k5eAaPmNgFnP	prodat
Lomikarovi	Lomikar	k1gMnSc6	Lomikar
<g/>
.	.	kIx.	.
</s>
<s>
Chodové	Chod	k1gMnPc1	Chod
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nechtěli	chtít	k5eNaImAgMnP	chtít
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
stížnosti	stížnost	k1gFnPc1	stížnost
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebyly	být	k5eNaImAgFnP	být
vyslyšeny	vyslyšen	k2eAgFnPc1d1	vyslyšena
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začali	začít	k5eAaPmAgMnP	začít
organizovat	organizovat	k5eAaBmF	organizovat
povstání	povstání	k1gNnPc4	povstání
proti	proti	k7c3	proti
vrchnosti	vrchnost	k1gFnSc3	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
postavou	postava	k1gFnSc7	postava
odboje	odboj	k1gInSc2	odboj
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
a	a	k8xC	a
Kozina	kozina	k1gFnSc1	kozina
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
oběšením	oběšení	k1gNnPc3	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Exekuce	exekuce	k1gFnSc1	exekuce
byla	být	k5eAaImAgFnS	být
vykonána	vykonat	k5eAaPmNgFnS	vykonat
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1695	[number]	k4	1695
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kozina	kozina	k1gFnSc1	kozina
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
zvolal	zvolat	k5eAaPmAgInS	zvolat
mocným	mocný	k2eAgInSc7d1	mocný
hlasem	hlas	k1gInSc7	hlas
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lomikare	Lomikar	k1gMnSc5	Lomikar
<g/>
,	,	kIx,	,
Lomikare	Lomikar	k1gMnSc5	Lomikar
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
do	do	k7c2	do
dne	den	k1gInSc2	den
zvu	zvát	k5eAaImIp1nS	zvát
tě	ty	k3xPp2nSc4	ty
Lomikare	Lomikar	k1gMnSc5	Lomikar
na	na	k7c4	na
Boží	božit	k5eAaImIp3nS	božit
súd	súd	k?	súd
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lomikar	Lomikar	k1gMnSc1	Lomikar
skutečně	skutečně	k6eAd1	skutečně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
zrovna	zrovna	k6eAd1	zrovna
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
hostině	hostina	k1gFnSc6	hostina
posmíval	posmívat	k5eAaImAgMnS	posmívat
Kozinovým	Kozinův	k2eAgNnPc3d1	Kozinovo
prorockým	prorocký	k2eAgNnPc3d1	prorocké
slovům	slovo	k1gNnPc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Klenčí	Klenčí	k1gNnSc6	Klenčí
pod	pod	k7c7	pod
Čerchovem	Čerchov	k1gInSc7	Čerchov
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
hrob	hrob	k1gInSc1	hrob
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
několikrát	několikrát	k6eAd1	několikrát
vypleněn	vypleněn	k2eAgMnSc1d1	vypleněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historickým	historický	k2eAgInSc7d1	historický
předobrazem	předobraz	k1gInSc7	předobraz
Lomikara	Lomikar	k1gMnSc2	Lomikar
byl	být	k5eAaImAgMnS	být
Wolf	Wolf	k1gMnSc1	Wolf
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Lammingern	Lammingern	k1gMnSc1	Lammingern
z	z	k7c2	z
Albenreuthu	Albenreuth	k1gInSc2	Albenreuth
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
je	být	k5eAaImIp3nS	být
i	i	k9	i
chodské	chodský	k2eAgNnSc1d1	Chodské
povstání	povstání	k1gNnSc1	povstání
a	a	k8xC	a
spor	spor	k1gInSc1	spor
o	o	k7c4	o
dřívější	dřívější	k2eAgNnPc4d1	dřívější
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
když	když	k8xS	když
Domažlice	Domažlice	k1gFnPc1	Domažlice
a	a	k8xC	a
okolní	okolní	k2eAgFnPc1d1	okolní
obce	obec	k1gFnPc1	obec
ztratily	ztratit	k5eAaPmAgFnP	ztratit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
důvěru	důvěra	k1gFnSc4	důvěra
panovníka	panovník	k1gMnSc2	panovník
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jevištní	jevištní	k2eAgFnSc1d1	jevištní
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Brzo	brzo	k6eAd1	brzo
po	po	k7c6	po
napsání	napsání	k1gNnSc6	napsání
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jeho	jeho	k3xOp3gFnPc1	jeho
úpravy	úprava	k1gFnPc1	úprava
pro	pro	k7c4	pro
jeviště	jeviště	k1gNnSc4	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
dramatizací	dramatizace	k1gFnPc2	dramatizace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
další	další	k2eAgFnSc2d1	další
dramatizace	dramatizace	k1gFnSc2	dramatizace
byl	být	k5eAaImAgMnS	být
J.	J.	kA	J.
B.	B.	kA	B.
Kühnl	Kühnl	k1gInSc4	Kühnl
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
uvedlo	uvést	k5eAaPmAgNnS	uvést
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
Švandovo	Švandův	k2eAgNnSc1d1	Švandovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
;	;	kIx,	;
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
však	však	k9	však
protestoval	protestovat	k5eAaBmAgMnS	protestovat
a	a	k8xC	a
soudně	soudně	k6eAd1	soudně
zakročil	zakročit	k5eAaPmAgMnS	zakročit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k9	teprve
dramatizaci	dramatizace	k1gFnSc4	dramatizace
Antonína	Antonín	k1gMnSc2	Antonín
Fencla	Fencl	k1gMnSc2	Fencl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
Jirásek	Jirásek	k1gMnSc1	Jirásek
schválil	schválit	k5eAaPmAgMnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
však	však	k9	však
nenamítal	namítat	k5eNaImAgMnS	namítat
proti	proti	k7c3	proti
opernímu	operní	k2eAgNnSc3d1	operní
zpracování	zpracování	k1gNnSc3	zpracování
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
napsali	napsat	k5eAaPmAgMnP	napsat
libretista	libretista	k1gMnSc1	libretista
Karel	Karel	k1gMnSc1	Karel
Šípek	Šípek	k1gMnSc1	Šípek
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Karel	Karel	k1gMnSc1	Karel
Kovařovic	Kovařovice	k1gFnPc2	Kovařovice
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
uvedení	uvedení	k1gNnSc2	uvedení
této	tento	k3xDgFnSc2	tento
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
požadoval	požadovat	k5eAaImAgMnS	požadovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
"	"	kIx"	"
<g/>
garderobní	garderobní	k2eAgFnSc2d1	garderobní
komise	komise	k1gFnSc2	komise
<g/>
"	"	kIx"	"
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Kozinův	Kozinův	k2eAgInSc1d1	Kozinův
svatební	svatební	k2eAgInSc1d1	svatební
kabát	kabát	k1gInSc1	kabát
byl	být	k5eAaImAgInS	být
modrý	modrý	k2eAgInSc1d1	modrý
s	s	k7c7	s
vyšíváním	vyšívání	k1gNnSc7	vyšívání
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
ředitel	ředitel	k1gMnSc1	ředitel
divadla	divadlo	k1gNnSc2	divadlo
Vendelín	Vendelín	k1gMnSc1	Vendelín
Budil	Budil	k1gMnSc1	Budil
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hru	hra	k1gFnSc4	hra
předtím	předtím	k6eAd1	předtím
inscenoval	inscenovat	k5eAaBmAgMnS	inscenovat
již	již	k6eAd1	již
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
dokonce	dokonce	k9	dokonce
za	za	k7c2	za
pohostinského	pohostinský	k2eAgNnSc2d1	pohostinské
hostování	hostování	k1gNnSc2	hostování
tenora	tenor	k1gMnSc2	tenor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Bohumila	Bohumil	k1gMnSc2	Bohumil
Ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
přivezl	přivézt	k5eAaPmAgMnS	přivézt
ze	z	k7c2	z
šatny	šatna	k1gFnSc2	šatna
ND	ND	kA	ND
kabát	kabát	k1gInSc4	kabát
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Vendelín	Vendelín	k1gMnSc1	Vendelín
Budil	Budil	k1gMnSc1	Budil
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
dobrozdání	dobrozdání	k1gNnSc4	dobrozdání
nakonec	nakonec	k6eAd1	nakonec
samotného	samotný	k2eAgMnSc4d1	samotný
Aloise	Alois	k1gMnSc4	Alois
Jiráska	Jirásek	k1gMnSc4	Jirásek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
dopisem	dopis	k1gInSc7	dopis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
historický	historický	k2eAgInSc1d1	historický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Innemanna	Innemann	k1gMnSc2	Innemann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
historický	historický	k2eAgInSc1d1	historický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Martina	Martin	k1gMnSc2	Martin
Friče	Frič	k1gMnSc2	Frič
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
–	–	k?	–
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
opery	opera	k1gFnSc2	opera
režiséra	režisér	k1gMnSc2	režisér
Milana	Milan	k1gMnSc2	Milan
Macků	Macků	k1gMnSc2	Macků
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Bohuš	Bohuš	k1gMnSc1	Bohuš
Pavel	Pavel	k1gMnSc1	Pavel
Alois	Alois	k1gMnSc1	Alois
Lepař	Lepař	k1gMnSc1	Lepař
přeložil	přeložit	k5eAaPmAgMnS	přeložit
román	román	k1gInSc4	román
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Chodische	Chodische	k1gNnSc2	Chodische
Freiheitskämpfer	Freiheitskämpfra	k1gFnPc2	Freiheitskämpfra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olav	Olav	k1gMnSc1	Olav
Rytter	Rytter	k1gMnSc1	Rytter
přeložil	přeložit	k5eAaPmAgMnS	přeložit
román	román	k1gInSc4	román
do	do	k7c2	do
norštiny	norština	k1gFnSc2	norština
(	(	kIx(	(
<g/>
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Stormtid	Stormtida	k1gFnPc2	Stormtida
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
Det	Det	k1gFnSc3	Det
Norske	Norske	k1gFnSc3	Norske
Samlaget	Samlageta	k1gFnPc2	Samlageta
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejpřekládanějším	překládaný	k2eAgNnPc3d3	překládaný
dílům	dílo	k1gNnPc3	dílo
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
bulharsky	bulharsky	k6eAd1	bulharsky
(	(	kIx(	(
<g/>
Pesoglavci	Pesoglavec	k1gMnPc1	Pesoglavec
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Tê	Tê	k1gFnSc2	Tê
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gruzínsky	gruzínsky	k6eAd1	gruzínsky
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
chorvatsky	chorvatsky	k6eAd1	chorvatsky
(	(	kIx(	(
<g/>
Pasoglavci	Pasoglavec	k1gMnPc1	Pasoglavec
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lotyšsky	lotyšsky	k6eAd1	lotyšsky
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
(	(	kIx(	(
<g/>
A	a	k9	a
kutyafejüek	kutyafejüek	k6eAd1	kutyafejüek
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moldavsky	moldavsky	k6eAd1	moldavsky
(	(	kIx(	(
<g/>
Kapete	kapat	k5eAaImIp2nP	kapat
de	de	k?	de
kyn	kyn	k1gInSc4wB	kyn
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
Chodische	Chodische	k1gNnPc1	Chodische
Freiheitskämpfer	Freiheitskämpfra	k1gFnPc2	Freiheitskämpfra
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
;	;	kIx,	;
Die	Die	k1gMnSc5	Die
Hundsköpfe	Hundsköpf	k1gMnSc5	Hundsköpf
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
(	(	kIx(	(
<g/>
Stormtid	Stormtid	k1gInSc1	Stormtid
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
(	(	kIx(	(
<g/>
Psiogłowcy	Psiogłowcy	k1gInPc1	Psiogłowcy
nebo	nebo	k8xC	nebo
Psogłowcy	Psogłowcy	k1gInPc1	Psogłowcy
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
(	(	kIx(	(
<g/>
Capete	capat	k5eAaImIp2nP	capat
de	de	k?	de
câini	câin	k1gMnPc1	câin
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
(	(	kIx(	(
<g/>
Psoglavcy	Psoglavcy	k1gInPc1	Psoglavcy
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovinsky	slovinsky	k6eAd1	slovinsky
(	(	kIx(	(
<g/>
Pasjeglavci	Pasjeglavec	k1gMnPc1	Pasjeglavec
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srbochorvatsky	srbochorvatsky	k6eAd1	srbochorvatsky
(	(	kIx(	(
<g/>
Psoglavci	Psoglavec	k1gMnPc1	Psoglavec
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
(	(	kIx(	(
<g/>
Pesyholovci	Pesyholovec	k1gMnPc1	Pesyholovec
<g/>
,	,	kIx,	,
1930,1949	[number]	k4	1930,1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
románu	román	k1gInSc6	román
Psohlavci	psohlavec	k1gMnPc1	psohlavec
===	===	k?	===
</s>
</p>
<p>
<s>
CUŘÍN	CUŘÍN	kA	CUŘÍN
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Jiráskových	Jiráskových	k2eAgMnPc2d1	Jiráskových
Psohlavců	psohlavec	k1gMnPc2	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
filologii	filologie	k1gFnSc4	filologie
<g/>
,	,	kIx,	,
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANÁČKOVÁ	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
monografie	monografie	k1gFnPc1	monografie
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
581	[number]	k4	581
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
kapitolu	kapitola	k1gFnSc4	kapitola
"	"	kIx"	"
<g/>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
"	"	kIx"	"
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
177	[number]	k4	177
<g/>
–	–	k?	–
<g/>
201	[number]	k4	201
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
JÍLEK	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
kapitol	kapitola	k1gFnPc2	kapitola
o	o	k7c6	o
Jiráskových	Jiráskových	k2eAgMnPc6d1	Jiráskových
Psohlavcích	psohlavec	k1gMnPc6	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Sborník	sborník	k1gInSc1	sborník
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
373	[number]	k4	373
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
otištěna	otisknout	k5eAaPmNgFnS	otisknout
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
97	[number]	k4	97
<g/>
–	–	k?	–
<g/>
227	[number]	k4	227
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
Psohlavců	psohlavec	k1gMnPc2	psohlavec
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
===	===	k?	===
</s>
</p>
<p>
<s>
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Podivuhodné	podivuhodný	k2eAgInPc1d1	podivuhodný
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
321	[number]	k4	321
s.	s.	k?	s.
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Říkali	říkat	k5eAaImAgMnP	říkat
mu	on	k3xPp3gMnSc3	on
Kozina	kozin	k2eAgNnPc1d1	Kozino
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
157	[number]	k4	157
<g/>
–	–	k?	–
<g/>
241	[number]	k4	241
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KRAMAŘÍK	kramařík	k1gMnSc1	kramařík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Kozina	kozina	k1gFnSc1	kozina
a	a	k8xC	a
Lomikar	Lomikar	k1gMnSc1	Lomikar
v	v	k7c6	v
chodské	chodský	k2eAgFnSc6d1	Chodská
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
195	[number]	k4	195
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MAUR	Maur	k1gMnSc1	Maur
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Chodové	Chod	k1gMnPc1	Chod
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
126	[number]	k4	126
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MAUR	Maur	k1gMnSc1	Maur
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Kozina	kozina	k1gFnSc1	kozina
a	a	k8xC	a
Lomikar	Lomikar	k1gMnSc1	Lomikar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
40	[number]	k4	40
s.	s.	k?	s.
Slovo	slovo	k1gNnSc4	slovo
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
<g/>
;	;	kIx,	;
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
č.	č.	k?	č.
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROUBÍK	Roubík	k1gMnSc1	Roubík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Chodů	chod	k1gInPc2	chod
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
666	[number]	k4	666
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chodské	chodský	k2eAgNnSc1d1	Chodské
povstání	povstání	k1gNnSc1	povstání
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
</s>
</p>
<p>
<s>
Wolf	Wolf	k1gMnSc1	Wolf
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Lamingen	Lamingen	k1gInSc4	Lamingen
z	z	k7c2	z
Albenreuthu	Albenreuth	k1gInSc2	Albenreuth
(	(	kIx(	(
<g/>
Lomikar	Lomikar	k1gMnSc1	Lomikar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
e-kniha	eniha	k1gMnSc1	e-kniha
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Psohlavci	psohlavec	k1gMnSc3	psohlavec
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
260	[number]	k4	260
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
