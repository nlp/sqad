<s>
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Rudohoří	Rudohoří	k1gNnSc2	Rudohoří
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Erzgebirge	Erzgebirge	k1gNnSc1	Erzgebirge
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
Fergunna	Fergunn	k1gInSc2	Fergunn
či	či	k8xC	či
Mirihwidu	Mirihwid	k1gInSc2	Mirihwid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
celek	celek	k1gInSc4	celek
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
podél	podél	k7c2	podél
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
