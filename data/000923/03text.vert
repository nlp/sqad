<s>
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Rudohoří	Rudohoří	k1gNnSc2	Rudohoří
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Erzgebirge	Erzgebirge	k1gNnSc1	Erzgebirge
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
Fergunna	Fergunn	k1gInSc2	Fergunn
či	či	k8xC	či
Mirihwidu	Mirihwid	k1gInSc2	Mirihwid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
celek	celek	k1gInSc4	celek
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
podél	podél	k7c2	podél
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
souvislé	souvislý	k2eAgNnSc1d1	souvislé
horské	horský	k2eAgNnSc1d1	horské
pásmo	pásmo	k1gNnSc1	pásmo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
130	[number]	k4	130
km	km	kA	km
a	a	k8xC	a
průměrné	průměrný	k2eAgFnSc6d1	průměrná
šířce	šířka	k1gFnSc6	šířka
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
střeží	střežit	k5eAaImIp3nS	střežit
Krušné	krušný	k2eAgFnPc4d1	krušná
hory	hora	k1gFnPc4	hora
bájný	bájný	k2eAgMnSc1d1	bájný
duch	duch	k1gMnSc1	duch
Rudovřes	Rudovřes	k1gMnSc1	Rudovřes
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
Svatavy	Svatava	k1gFnSc2	Svatava
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
od	od	k7c2	od
Smrčin	Smrčiny	k1gFnPc2	Smrčiny
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Elstergebirge	Elstergebirg	k1gFnSc2	Elstergebirg
a	a	k8xC	a
Vogtlandu	Vogtland	k1gInSc2	Vogtland
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
na	na	k7c4	na
Krušné	krušný	k2eAgFnPc4d1	krušná
hory	hora	k1gFnPc4	hora
navazuje	navazovat	k5eAaImIp3nS	navazovat
u	u	k7c2	u
Krásného	krásný	k2eAgInSc2d1	krásný
Lesa	les	k1gInSc2	les
a	a	k8xC	a
Nakléřovského	nakléřovský	k2eAgInSc2d1	nakléřovský
průsmyku	průsmyk	k1gInSc2	průsmyk
Děčínská	děčínský	k2eAgFnSc1d1	Děčínská
vrchovina	vrchovina	k1gFnSc1	vrchovina
(	(	kIx(	(
<g/>
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
celky	celek	k1gInPc7	celek
tedy	tedy	k8xC	tedy
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
je	být	k5eAaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
dálnice	dálnice	k1gFnSc1	dálnice
D8	D8	k1gMnPc2	D8
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
jsou	být	k5eAaImIp3nP	být
ostře	ostro	k6eAd1	ostro
ohraničeny	ohraničit	k5eAaPmNgInP	ohraničit
údolím	údolí	k1gNnSc7	údolí
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
Mosteckou	mostecký	k2eAgFnSc7d1	Mostecká
pánví	pánev	k1gFnSc7	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
klesání	klesání	k1gNnSc3	klesání
obtížně	obtížně	k6eAd1	obtížně
definovatelná	definovatelný	k2eAgFnSc1d1	definovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
leží	ležet	k5eAaImIp3nS	ležet
až	až	k9	až
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
Collmbergu	Collmberg	k1gInSc2	Collmberg
(	(	kIx(	(
<g/>
312	[number]	k4	312
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchovina	vrchovina	k1gFnSc1	vrchovina
mezi	mezi	k7c7	mezi
Chemnitzem	Chemnitz	k1gInSc7	Chemnitz
a	a	k8xC	a
Zwickau	Zwickaus	k1gInSc2	Zwickaus
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Erzgebirgsvorland	Erzgebirgsvorland	k1gInSc1	Erzgebirgsvorland
(	(	kIx(	(
<g/>
Krušnohorské	krušnohorský	k2eAgNnSc1d1	Krušnohorské
podhůří	podhůří	k1gNnSc1	podhůří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
českého	český	k2eAgNnSc2d1	české
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
členění	členění	k1gNnSc2	členění
jsou	být	k5eAaImIp3nP	být
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Krušnohorské	krušnohorský	k2eAgFnSc2d1	Krušnohorská
hornatiny	hornatina	k1gFnSc2	hornatina
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
Klínoveckou	klínovecký	k2eAgFnSc4d1	Klínovecká
a	a	k8xC	a
Loučenskou	Loučenský	k2eAgFnSc4d1	Loučenská
hornatinu	hornatina	k1gFnSc4	hornatina
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
členění	členění	k1gNnSc1	členění
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
hor	hora	k1gFnPc2	hora
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
německého	německý	k2eAgNnSc2d1	německé
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Naturraumy	Naturraum	k1gInPc4	Naturraum
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgFnPc4d1	přírodní
oblasti	oblast	k1gFnPc4	oblast
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
Erzgebirge	Erzgebirge	k1gFnSc1	Erzgebirge
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
jednotku	jednotka	k1gFnSc4	jednotka
třetí	třetí	k4xOgFnSc2	třetí
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Haupteinheitengruppe	Haupteinheitengrupp	k1gInSc5	Haupteinheitengrupp
(	(	kIx(	(
<g/>
skupinu	skupina	k1gFnSc4	skupina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
a	a	k8xC	a
nižší	nízký	k2eAgFnPc1d2	nižší
jednotky	jednotka	k1gFnPc1	jednotka
už	už	k6eAd1	už
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
členění	členění	k1gNnSc4	členění
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
nenavazují	navazovat	k5eNaImIp3nP	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Nadřazenou	nadřazený	k2eAgFnSc7d1	nadřazená
jednotkou	jednotka	k1gFnSc7	jednotka
druhé	druhý	k4xOgFnSc2	druhý
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
Region	region	k1gInSc1	region
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Östliche	Östliche	k1gFnSc1	Östliche
Mittelgebirge	Mittelgebirge	k1gFnSc1	Mittelgebirge
(	(	kIx(	(
<g/>
Východní	východní	k2eAgNnSc1d1	východní
středohoří	středohoří	k1gNnSc1	středohoří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celá	celý	k2eAgFnSc1d1	celá
německá	německý	k2eAgFnSc1d1	německá
část	část	k1gFnSc1	část
České	český	k2eAgFnSc2d1	Česká
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Nepočítá	počítat	k5eNaImIp3nS	počítat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
už	už	k6eAd1	už
Erzgebirgsvorland	Erzgebirgsvorlanda	k1gFnPc2	Erzgebirgsvorlanda
(	(	kIx(	(
<g/>
Krušnohorské	krušnohorský	k2eAgNnSc1d1	Krušnohorské
podhůří	podhůří	k1gNnSc1	podhůří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Sächsisches	Sächsisches	k1gInSc1	Sächsisches
Hügelland	Hügellanda	k1gFnPc2	Hügellanda
(	(	kIx(	(
<g/>
Saskou	saský	k2eAgFnSc7d1	saská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
celek	celek	k1gInSc4	celek
D19	D19	k1gFnSc2	D19
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
regionu	region	k1gInSc2	region
Nordostdeutsches	Nordostdeutsches	k1gInSc4	Nordostdeutsches
Tiefland	Tiefland	k1gInSc1	Tiefland
(	(	kIx(	(
<g/>
Severovýchodoněmecká	Severovýchodoněmecký	k2eAgFnSc1d1	Severovýchodoněmecký
nížina	nížina	k1gFnSc1	nížina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
dělí	dělit	k5eAaImIp3nP	dělit
Krušné	krušný	k2eAgFnPc4d1	krušná
hory	hora	k1gFnPc4	hora
na	na	k7c4	na
2	[number]	k4	2
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
3	[number]	k4	3
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Západní	západní	k2eAgFnPc1d1	západní
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Westerzgebirge	Westerzgebirg	k1gFnPc1	Westerzgebirg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc2d1	východní
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
Osterzgebirge	Osterzgebirge	k1gInSc1	Osterzgebirge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ještě	ještě	k9	ještě
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
Střední	střední	k2eAgFnPc1d1	střední
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Mittelerzgebirge	Mittelerzgebirge	k1gInSc1	Mittelerzgebirge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
Západními	západní	k2eAgFnPc7d1	západní
a	a	k8xC	a
Východními	východní	k2eAgFnPc7d1	východní
Krušnými	krušný	k2eAgFnPc7d1	krušná
horami	hora	k1gFnPc7	hora
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc1	potok
Flöha	Flöha	k1gFnSc1	Flöha
(	(	kIx(	(
<g/>
Flájský	Flájský	k2eAgInSc1d1	Flájský
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
staršího	starý	k2eAgNnSc2d2	starší
dělení	dělení	k1gNnSc2	dělení
Emila	Emil	k1gMnSc2	Emil
Meynena	Meyneno	k1gNnSc2	Meyneno
se	se	k3xPyFc4	se
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
celek	celek	k1gInSc1	celek
42	[number]	k4	42
<g/>
)	)	kIx)	)
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
5	[number]	k4	5
podcelků	podcelek	k1gInPc2	podcelek
<g/>
:	:	kIx,	:
420	[number]	k4	420
Südabdachung	Südabdachung	k1gInSc1	Südabdachung
des	des	k1gNnSc2	des
Erzgebirges	Erzgebirgesa	k1gFnPc2	Erzgebirgesa
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
úbočí	úbočí	k1gNnSc4	úbočí
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
<g/>
;	;	kIx,	;
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
území	území	k1gNnSc4	území
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
jen	jen	k9	jen
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
)	)	kIx)	)
421	[number]	k4	421
Oberes	Oberesa	k1gFnPc2	Oberesa
Westerzgebirge	Westerzgebirge	k1gFnPc2	Westerzgebirge
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnPc1d1	horní
Západní	západní	k2eAgFnPc1d1	západní
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
422	[number]	k4	422
Oberes	Oberesa	k1gFnPc2	Oberesa
Osterzgebirge	Osterzgebirge	k1gFnPc2	Osterzgebirge
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnSc2d1	horní
Východní	východní	k2eAgFnSc2d1	východní
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
423	[number]	k4	423
Unteres	Unteresa	k1gFnPc2	Unteresa
Westerzgebirge	Westerzgebirge	k1gFnPc2	Westerzgebirge
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnPc1d1	dolní
Západní	západní	k2eAgFnPc1d1	západní
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
424	[number]	k4	424
Unteres	Unteresa	k1gFnPc2	Unteresa
Osterzgebirge	Osterzgebirge	k1gFnPc2	Osterzgebirge
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnPc1d1	dolní
Východní	východní	k2eAgFnPc1d1	východní
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
kerné	kerný	k2eAgNnSc4d1	kerný
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	on	k3xPp3gFnPc4	on
nakloněná	nakloněný	k2eAgFnSc1d1	nakloněná
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
byl	být	k5eAaImAgInS	být
vyzvednut	vyzvednout	k5eAaPmNgInS	vyzvednout
podél	podél	k7c2	podél
zemského	zemský	k2eAgInSc2d1	zemský
zlomu	zlom	k1gInSc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
klesají	klesat	k5eAaImIp3nP	klesat
pozvolně	pozvolně	k6eAd1	pozvolně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
m	m	kA	m
vysokým	vysoký	k2eAgInSc7d1	vysoký
příkrým	příkrý	k2eAgInSc7d1	příkrý
svahem	svah	k1gInSc7	svah
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
vrcholy	vrchol	k1gInPc7	vrchol
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
JZ	JZ	kA	JZ
k	k	k7c3	k
SV	sv	kA	sv
:	:	kIx,	:
Počátecký	Počátecký	k2eAgInSc1d1	Počátecký
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
821	[number]	k4	821
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kraslický	kraslický	k2eAgInSc1d1	kraslický
Špičák	špičák	k1gInSc1	špičák
(	(	kIx(	(
<g/>
991	[number]	k4	991
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plešivec	plešivec	k1gMnSc1	plešivec
(	(	kIx(	(
<g/>
1028	[number]	k4	1028
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Božídarský	božídarský	k2eAgInSc1d1	božídarský
Špičák	špičák	k1gInSc1	špičák
(	(	kIx(	(
<g/>
1115	[number]	k4	1115
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fichtelberg	Fichtelberg	k1gMnSc1	Fichtelberg
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1214	[number]	k4	1214
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Klínovec	Klínovec	k1gInSc1	Klínovec
(	(	kIx(	(
<g/>
1244	[number]	k4	1244
m	m	kA	m
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jelení	jelení	k2eAgFnSc1d1	jelení
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
994	[number]	k4	994
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Medvědí	medvědí	k2eAgFnSc1d1	medvědí
skála	skála	k1gFnSc1	skála
(	(	kIx(	(
<g/>
924	[number]	k4	924
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Loučná	Loučný	k2eAgFnSc1d1	Loučná
(	(	kIx(	(
<g/>
956	[number]	k4	956
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Komáří	komáří	k2eAgFnSc1d1	komáří
hůrka	hůrka	k1gFnSc1	hůrka
(	(	kIx(	(
<g/>
808	[number]	k4	808
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
vrcholy	vrchol	k1gInPc7	vrchol
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
jsou	být	k5eAaImIp3nP	být
Klínovec	Klínovec	k1gInSc4	Klínovec
(	(	kIx(	(
<g/>
1244	[number]	k4	1244
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fichtelberg	Fichtelberg	k1gMnSc1	Fichtelberg
(	(	kIx(	(
<g/>
1214	[number]	k4	1214
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Božídarský	božídarský	k2eAgInSc1d1	božídarský
Špičák	špičák	k1gInSc1	špičák
(	(	kIx(	(
<g/>
1115	[number]	k4	1115
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
Klínovec	Klínovec	k1gInSc4	Klínovec
(	(	kIx(	(
<g/>
748	[number]	k4	748
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
netisícovky	netisícovka	k1gFnPc1	netisícovka
<g/>
"	"	kIx"	"
Loučná	Loučný	k2eAgFnSc1d1	Loučná
(	(	kIx(	(
<g/>
236	[number]	k4	236
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jelení	jelení	k2eAgFnSc1d1	jelení
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
177	[number]	k4	177
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
a	a	k8xC	a
nejprominentnějších	prominentní	k2eAgFnPc2d3	nejprominentnější
krušnohorských	krušnohorský	k2eAgFnPc2d1	Krušnohorská
hor	hora	k1gFnPc2	hora
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Seznam	seznam	k1gInSc1	seznam
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
všechny	všechen	k3xTgFnPc4	všechen
krušnohorské	krušnohorský	k2eAgFnPc4d1	Krušnohorská
tisícovky	tisícovka	k1gFnPc4	tisícovka
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
pod	pod	k7c7	pod
Krušnými	krušný	k2eAgFnPc7d1	krušná
horami	hora	k1gFnPc7	hora
patří	patřit	k5eAaImIp3nS	patřit
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Krupka	krupka	k1gFnSc1	krupka
<g/>
,	,	kIx,	,
Chabařovice	Chabařovice	k1gFnPc1	Chabařovice
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
Duchcov	Duchcov	k1gInSc1	Duchcov
<g/>
,	,	kIx,	,
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Zwickau	Zwickaa	k1gFnSc4	Zwickaa
<g/>
,	,	kIx,	,
Freiberg	Freiberg	k1gInSc1	Freiberg
<g/>
,	,	kIx,	,
Chemnitz	Chemnitz	k1gInSc1	Chemnitz
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Merklín	Merklín	k1gInSc1	Merklín
<g/>
.	.	kIx.	.
</s>
<s>
Geologická	geologický	k2eAgFnSc1d1	geologická
historie	historie	k1gFnSc1	historie
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
předprvohorním	předprvohorní	k2eAgNnSc6d1	předprvohorní
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
nejstarší	starý	k2eAgFnPc1d3	nejstarší
usazeniny	usazenina	k1gFnPc1	usazenina
a	a	k8xC	a
vyvřeliny	vyvřelina	k1gFnPc1	vyvřelina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
změněné	změněný	k2eAgInPc1d1	změněný
vlivem	vlivem	k7c2	vlivem
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
šedé	šedý	k2eAgFnPc1d1	šedá
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
ruly	rula	k1gFnPc1	rula
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
vývoj	vývoj	k1gInSc1	vývoj
celé	celý	k2eAgFnSc2d1	celá
soustavy	soustava	k1gFnSc2	soustava
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
až	až	k9	až
třetihorní	třetihorní	k2eAgInSc1d1	třetihorní
zlomovou	zlomový	k2eAgFnSc7d1	zlomová
tektonikou	tektonika	k1gFnSc7	tektonika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
silné	silný	k2eAgInPc4d1	silný
poklesy	pokles	k1gInPc4	pokles
na	na	k7c4	na
jv	jv	k?	jv
<g/>
.	.	kIx.	.
straně	strana	k1gFnSc6	strana
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
jezerních	jezerní	k2eAgFnPc2d1	jezerní
depresí	deprese	k1gFnPc2	deprese
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
Komořanské	komořanský	k2eAgNnSc1d1	Komořanské
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Mostecku	Mostecko	k1gNnSc6	Mostecko
<g/>
.	.	kIx.	.
</s>
<s>
Pohyby	pohyb	k1gInPc1	pohyb
na	na	k7c6	na
zlomových	zlomový	k2eAgFnPc6d1	zlomová
liniích	linie	k1gFnPc6	linie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
opakovaly	opakovat	k5eAaImAgFnP	opakovat
<g/>
,	,	kIx,	,
usnadnily	usnadnit	k5eAaPmAgFnP	usnadnit
také	také	k6eAd1	také
práci	práce	k1gFnSc4	práce
povrchové	povrchový	k2eAgFnSc3d1	povrchová
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
přispěly	přispět	k5eAaPmAgInP	přispět
tak	tak	k9	tak
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
hlubokých	hluboký	k2eAgNnPc2d1	hluboké
příčných	příčný	k2eAgNnPc2d1	příčné
údolí	údolí	k1gNnSc2	údolí
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
mocných	mocný	k2eAgNnPc2d1	mocné
kamenitých	kamenitý	k2eAgNnPc2d1	kamenité
sutí	sutí	k1gNnPc2	sutí
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zvětralin	zvětralina	k1gFnPc2	zvětralina
na	na	k7c6	na
horských	horský	k2eAgInPc6d1	horský
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
odvodněna	odvodněn	k2eAgFnSc1d1	odvodněn
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
do	do	k7c2	do
Ohře	Ohře	k1gFnSc2	Ohře
a	a	k8xC	a
Bíliny	Bílina	k1gFnSc2	Bílina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
vodní	vodní	k2eAgFnSc7d1	vodní
osou	osa	k1gFnSc7	osa
je	být	k5eAaImIp3nS	být
Ohře	Ohře	k1gFnSc1	Ohře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
Chebské	chebský	k2eAgFnSc6d1	Chebská
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
značný	značný	k2eAgInSc4d1	značný
spád	spád	k1gInSc4	spád
a	a	k8xC	a
až	až	k9	až
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Žatce	žatec	k1gMnSc2	žatec
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
její	její	k3xOp3gNnSc1	její
tok	tok	k1gInSc1	tok
pozvolným	pozvolný	k2eAgInSc7d1	pozvolný
<g/>
.	.	kIx.	.
</s>
<s>
Mosteckou	mostecký	k2eAgFnSc4d1	Mostecká
pánev	pánev	k1gFnSc4	pánev
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
Bílina	Bílina	k1gFnSc1	Bílina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
úbočích	úboč	k1gFnPc6	úboč
hor	hora	k1gFnPc2	hora
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Přijímá	přijímat	k5eAaImIp3nS	přijímat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
potoků	potok	k1gInPc2	potok
jak	jak	k8xC	jak
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
tak	tak	k6eAd1	tak
i	i	k9	i
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
dalšího	další	k2eAgInSc2d1	další
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
odvodňována	odvodňovat	k5eAaImNgFnS	odvodňovat
Jílovským	jílovský	k2eAgInSc7d1	jílovský
potokem	potok	k1gInSc7	potok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Jílovským	jílovský	k2eAgNnSc7d1	Jílovské
údolím	údolí	k1gNnSc7	údolí
teče	téct	k5eAaImIp3nS	téct
k	k	k7c3	k
východu	východ	k1gInSc3	východ
a	a	k8xC	a
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horských	horský	k2eAgInPc6d1	horský
potocích	potok	k1gInPc6	potok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
umělých	umělý	k2eAgFnPc2d1	umělá
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
největší	veliký	k2eAgMnPc4d3	veliký
patří	patřit	k5eAaImIp3nP	patřit
Přísečnická	Přísečnický	k2eAgFnSc1d1	Přísečnická
<g/>
,	,	kIx,	,
Flájská	Flájský	k2eAgFnSc1d1	Flájská
a	a	k8xC	a
Křimovská	Křimovský	k2eAgFnSc1d1	Křimovský
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnPc1	sloužící
jako	jako	k8xS	jako
zásobárny	zásobárna	k1gFnPc1	zásobárna
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Nechranice	Nechranice	k1gFnSc1	Nechranice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
i	i	k9	i
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Toky	toka	k1gFnPc1	toka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
netečou	téct	k5eNaImIp3nP	téct
podél	podél	k7c2	podél
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
hřebene	hřeben	k1gInSc2	hřeben
pryč	pryč	k6eAd1	pryč
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
nížin	nížina	k1gFnPc2	nížina
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
však	však	k9	však
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
sejde	sejít	k5eAaPmIp3nS	sejít
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Muldě	mulda	k1gFnSc6	mulda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Ohře	Ohře	k1gFnSc1	Ohře
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
Mulda	mulda	k1gFnSc1	mulda
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
potoky	potok	k1gInPc1	potok
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
odedávna	odedávna	k6eAd1	odedávna
těžily	těžit	k5eAaImAgFnP	těžit
rudy	ruda	k1gFnPc1	ruda
obsahující	obsahující	k2eAgFnSc4d1	obsahující
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
železo	železo	k1gNnSc1	železo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidaly	přidat	k5eAaPmAgFnP	přidat
ještě	ještě	k6eAd1	ještě
kobalt	kobalt	k1gInSc4	kobalt
<g/>
,	,	kIx,	,
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc4	wolfram
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
ložiska	ložisko	k1gNnPc1	ložisko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
Cínovec	Cínovec	k1gInSc1	Cínovec
<g/>
,	,	kIx,	,
Krupka	krupka	k1gFnSc1	krupka
<g/>
,	,	kIx,	,
Měděnec	měděnec	k1gInSc1	měděnec
<g/>
,	,	kIx,	,
Přísečnice	Přísečnice	k1gFnSc1	Přísečnice
a	a	k8xC	a
Kovářská	kovářský	k2eAgFnSc1d1	Kovářská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
surovin	surovina	k1gFnPc2	surovina
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
v	v	k7c6	v
podkrušnohorských	podkrušnohorský	k2eAgFnPc6d1	Podkrušnohorská
pánvích	pánev	k1gFnPc6	pánev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jíly	jíl	k1gInPc1	jíl
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
hnědouhelných	hnědouhelný	k2eAgFnPc2d1	hnědouhelná
slojí	sloj	k1gFnPc2	sloj
a	a	k8xC	a
třetihorní	třetihorní	k2eAgInPc1d1	třetihorní
keramické	keramický	k2eAgInPc1d1	keramický
jíly	jíl	k1gInPc1	jíl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
Skalné	skalný	k2eAgFnSc6d1	Skalná
u	u	k7c2	u
Chebu	Cheb	k1gInSc2	Cheb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
krušnohorské	krušnohorský	k2eAgFnPc1d1	Krušnohorská
rašeliny	rašelina	k1gFnPc1	rašelina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
zásobárnu	zásobárna	k1gFnSc4	zásobárna
i	i	k8xC	i
zdroj	zdroj	k1gInSc4	zdroj
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgInP	chránit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
těžbu	těžba	k1gFnSc4	těžba
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
využívání	využívání	k1gNnSc4	využívání
minerálních	minerální	k2eAgInPc2d1	minerální
pramenů	pramen	k1gInPc2	pramen
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
podkrušnohorském	podkrušnohorský	k2eAgInSc6d1	podkrušnohorský
prolomu	prolom	k1gInSc6	prolom
-	-	kIx~	-
Teplice	teplice	k1gFnSc1	teplice
<g/>
,	,	kIx,	,
Bílina	Bílina	k1gFnSc1	Bílina
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Proslulé	proslulý	k2eAgInPc1d1	proslulý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
dříve	dříve	k6eAd2	dříve
těžených	těžený	k2eAgFnPc6d1	těžená
rudných	rudný	k2eAgFnPc6d1	Rudná
žilách	žíla	k1gFnPc6	žíla
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hřebene	hřeben	k1gInSc2	hřeben
je	být	k5eAaImIp3nS	být
drsnější	drsný	k2eAgNnSc1d2	drsnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
prudkými	prudký	k2eAgFnPc7d1	prudká
bouřemi	bouř	k1gFnPc7	bouř
<g/>
,	,	kIx,	,
s	s	k7c7	s
větry	vítr	k1gInPc7	vítr
zejména	zejména	k9	zejména
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkým	krátké	k1gNnSc7	krátké
<g/>
,	,	kIx,	,
několikatýdenním	několikatýdenní	k2eAgNnSc7d1	několikatýdenní
létem	léto	k1gNnSc7	léto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
900	[number]	k4	900
m	m	kA	m
jsou	být	k5eAaImIp3nP	být
kolem	kolem	k7c2	kolem
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kolem	kolem	k7c2	kolem
2,5	[number]	k4	2,5
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
hory	hora	k1gFnPc1	hora
turisty	turist	k1gMnPc4	turist
vyhledávanou	vyhledávaný	k2eAgFnSc7d1	vyhledávaná
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
místy	místy	k6eAd1	místy
až	až	k9	až
4	[number]	k4	4
m.	m.	k?	m.
Sníh	sníh	k1gInSc1	sníh
tu	tu	k6eAd1	tu
padá	padat	k5eAaImIp3nS	padat
až	až	k9	až
100	[number]	k4	100
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
214	[number]	k4	214
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrazíky	mrazík	k1gInPc1	mrazík
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
převládají	převládat	k5eAaImIp3nP	převládat
severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
západní	západní	k2eAgInPc1d1	západní
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
a	a	k8xC	a
studené	studený	k2eAgInPc1d1	studený
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
rychlou	rychlý	k2eAgFnSc4d1	rychlá
změnu	změna	k1gFnSc4	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zimní	zimní	k2eAgFnPc1d1	zimní
mlhy	mlha	k1gFnPc1	mlha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
700	[number]	k4	700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
90	[number]	k4	90
<g/>
×	×	k?	×
-	-	kIx~	-
124	[number]	k4	124
<g/>
×	×	k?	×
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloze	poloha	k1gFnSc3	poloha
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
tu	tu	k6eAd1	tu
ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
1000	[number]	k4	1000
až	až	k9	až
1200	[number]	k4	1200
mm	mm	kA	mm
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
méně	málo	k6eAd2	málo
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krušné	krušný	k2eAgFnPc4d1	krušná
hory	hora	k1gFnPc4	hora
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tzv.	tzv.	kA	tzv.
srážkový	srážkový	k2eAgInSc4d1	srážkový
stín	stín	k1gInSc4	stín
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
podkrušnohorských	podkrušnohorský	k2eAgFnPc2d1	Podkrušnohorská
pánví	pánev	k1gFnPc2	pánev
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
srážky	srážka	k1gFnPc1	srážka
pak	pak	k6eAd1	pak
dopadají	dopadat	k5eAaImIp3nP	dopadat
až	až	k9	až
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
tedy	tedy	k9	tedy
v	v	k7c6	v
pánevní	pánevní	k2eAgFnSc6d1	pánevní
oblasti	oblast	k1gFnSc6	oblast
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
500	[number]	k4	500
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Podkrušnohoří	Podkrušnohoří	k1gNnSc1	Podkrušnohoří
není	být	k5eNaImIp3nS	být
nejvhodnějším	vhodný	k2eAgNnSc7d3	nejvhodnější
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgNnPc4d1	žijící
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
osídlení	osídlení	k1gNnSc4	osídlení
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
průmysl	průmysl	k1gInSc1	průmysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnPc1d2	lepší
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
zalesněných	zalesněný	k2eAgFnPc6d1	zalesněná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
zajíci	zajíc	k1gMnPc1	zajíc
(	(	kIx(	(
<g/>
Lepus	Lepus	k1gMnSc1	Lepus
europaeus	europaeus	k1gMnSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ježci	ježek	k1gMnPc1	ježek
(	(	kIx(	(
<g/>
Erinaceus	Erinaceus	k1gMnSc1	Erinaceus
europaeus	europaeus	k1gMnSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezevci	jezevec	k1gMnPc1	jezevec
(	(	kIx(	(
<g/>
Meles	Meles	k1gMnSc1	Meles
meles	meles	k1gMnSc1	meles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeleni	jelen	k1gMnPc1	jelen
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gMnSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Chebsku	Chebsko	k1gNnSc6	Chebsko
a	a	k8xC	a
Ašsku	Ašsko	k1gNnSc6	Ašsko
i	i	k9	i
daňci	daněk	k1gMnPc1	daněk
(	(	kIx(	(
<g/>
Dama	Dama	k1gMnSc1	Dama
dama	dama	k1gMnSc1	dama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
výrazně	výrazně	k6eAd1	výrazně
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
pralesovité	pralesovitý	k2eAgInPc1d1	pralesovitý
porosty	porost	k1gInPc1	porost
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgInPc1d1	tvořený
smíšenými	smíšený	k2eAgInPc7d1	smíšený
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
během	během	k7c2	během
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc2	zpracování
rud	ruda	k1gFnPc2	ruda
vykáceny	vykácet	k5eAaPmNgInP	vykácet
a	a	k8xC	a
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
smrkovými	smrkový	k2eAgFnPc7d1	smrková
monokulturami	monokultura	k1gFnPc7	monokultura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
těžce	těžce	k6eAd1	těžce
poškozeny	poškodit	k5eAaPmNgInP	poškodit
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
imisemi	imise	k1gFnPc7	imise
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kyselé	kyselý	k2eAgInPc1d1	kyselý
deště	dešť	k1gInPc1	dešť
<g/>
)	)	kIx)	)
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
přemnožením	přemnožení	k1gNnSc7	přemnožení
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
,	,	kIx,	,
vichřicemi	vichřice	k1gFnPc7	vichřice
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
námrazou	námraza	k1gFnSc7	námraza
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
likvidaci	likvidace	k1gFnSc3	likvidace
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
holiny	holina	k1gFnPc1	holina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
systematicky	systematicky	k6eAd1	systematicky
zalesňovány	zalesňován	k2eAgFnPc1d1	zalesňována
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lépe	dobře	k6eAd2	dobře
snášejí	snášet	k5eAaImIp3nP	snášet
zdejší	zdejší	k2eAgFnPc4d1	zdejší
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
břízami	bříza	k1gFnPc7	bříza
<g/>
,	,	kIx,	,
modříny	modřín	k1gInPc1	modřín
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
a	a	k8xC	a
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
smrky	smrk	k1gInPc7	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
lesů	les	k1gInPc2	les
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
stromem	strom	k1gInSc7	strom
tu	tu	k6eAd1	tu
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
Picea	Picea	k1gMnSc1	Picea
abies	abies	k1gMnSc1	abies
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
až	až	k9	až
do	do	k7c2	do
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
kleč	kleč	k1gFnSc1	kleč
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
plochách	plocha	k1gFnPc6	plocha
krušnohorských	krušnohorský	k2eAgNnPc2d1	Krušnohorské
rašelinišť	rašeliniště	k1gNnPc2	rašeliniště
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Božídarské	božídarský	k2eAgNnSc1d1	Božídarské
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc3	míra
borovicím	borovice	k1gFnPc3	borovice
<g/>
,	,	kIx,	,
břízám	bříza	k1gFnPc3	bříza
a	a	k8xC	a
vřesu	vřes	k1gInSc6	vřes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
např.	např.	kA	např.
vzácný	vzácný	k2eAgInSc4d1	vzácný
náprstník	náprstník	k1gInSc4	náprstník
červený	červený	k2eAgInSc4d1	červený
(	(	kIx(	(
<g/>
Digitalis	Digitalis	k1gInSc4	Digitalis
purpurea	purpure	k1gInSc2	purpure
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diviznu	divizna	k1gFnSc4	divizna
velkokvětou	velkokvětý	k2eAgFnSc4d1	velkokvětá
(	(	kIx(	(
<g/>
Verbascum	Verbascum	k1gNnSc4	Verbascum
thapsiforme	thapsiform	k1gInSc5	thapsiform
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
smetanku	smetanka	k1gFnSc4	smetanka
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gNnSc4	Taraxacum
officinale	officinale	k6eAd1	officinale
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
podkrušnohorské	podkrušnohorský	k2eAgFnSc6d1	Podkrušnohorská
pánvi	pánev	k1gFnSc6	pánev
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
,	,	kIx,	,
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Bílina	Bílina	k1gFnSc1	Bílina
<g/>
,	,	kIx,	,
Duchcov	Duchcov	k1gInSc1	Duchcov
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Děčín	Děčín	k1gInSc1	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
místa	místo	k1gNnPc1	místo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
jsou	být	k5eAaImIp3nP	být
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
Nejdek	Nejdek	k1gInSc1	Nejdek
<g/>
,	,	kIx,	,
Kraslice	kraslice	k1gFnSc1	kraslice
<g/>
,	,	kIx,	,
Měděnec	měděnec	k1gInSc1	měděnec
<g/>
,	,	kIx,	,
Cínovec	Cínovec	k1gInSc1	Cínovec
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
podkrušnohorské	podkrušnohorský	k2eAgFnSc2d1	Podkrušnohorská
pánve	pánev	k1gFnSc2	pánev
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
těží	těžet	k5eAaImIp3nS	těžet
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
palivo-energetickou	palivonergetický	k2eAgFnSc4d1	palivo-energetický
základnu	základna	k1gFnSc4	základna
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
doly	dol	k1gInPc1	dol
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Mostecké	mostecký	k2eAgFnSc6d1	Mostecká
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc3d2	menší
a	a	k8xC	a
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnPc4d1	kvalitní
pak	pak	k6eAd1	pak
v	v	k7c6	v
Sokolovské	sokolovský	k2eAgFnSc6d1	Sokolovská
a	a	k8xC	a
Chebské	chebský	k2eAgFnSc6d1	Chebská
<g/>
.	.	kIx.	.
</s>
<s>
Hnědé	hnědý	k2eAgNnSc1d1	hnědé
uhlí	uhlí	k1gNnSc1	uhlí
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
okamžitě	okamžitě	k6eAd1	okamžitě
využíváno	využívat	k5eAaPmNgNnS	využívat
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
tepelné	tepelný	k2eAgFnSc2d1	tepelná
a	a	k8xC	a
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
elektrárny	elektrárna	k1gFnPc1	elektrárna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Tušimicích	Tušimice	k1gFnPc6	Tušimice
<g/>
,	,	kIx,	,
Prunéřově	Prunéřův	k2eAgNnSc6d1	Prunéřův
a	a	k8xC	a
Bílině	Bílina	k1gFnSc6	Bílina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
přes	přes	k7c4	přes
100	[number]	k4	100
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
průmyslem	průmysl	k1gInSc7	průmysl
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgMnSc1d1	chemický
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgMnS	soustředit
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Litvínov	Litvínov	k1gInSc4	Litvínov
(	(	kIx(	(
<g/>
Chemopetrol	chemopetrol	k1gInSc1	chemopetrol
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
Rafinérská	rafinérský	k2eAgFnSc1d1	rafinérská
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
Spolchemie	Spolchemie	k1gFnSc2	Spolchemie
<g/>
,	,	kIx,	,
Setuza	Setuza	k1gFnSc1	Setuza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
sklářský	sklářský	k2eAgInSc1d1	sklářský
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
(	(	kIx(	(
<g/>
AGC	AGC	kA	AGC
Flat	Flat	k2eAgInSc1d1	Flat
Glass	Glass	k1gInSc1	Glass
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
či	či	k8xC	či
porcelánky	porcelánka	k1gFnPc1	porcelánka
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Klášterci	Klášterec	k1gInSc6	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
přetvářelo	přetvářet	k5eAaImAgNnS	přetvářet
krajinu	krajina	k1gFnSc4	krajina
zejména	zejména	k9	zejména
dolování	dolování	k1gNnSc4	dolování
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
u	u	k7c2	u
Freibergu	Freiberg	k1gInSc2	Freiberg
našlo	najít	k5eAaPmAgNnS	najít
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
těžil	těžit	k5eAaImAgMnS	těžit
uran	uran	k1gInSc4	uran
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
útlumu	útlum	k1gInSc6	útlum
těžby	těžba	k1gFnSc2	těžba
je	být	k5eAaImIp3nS	být
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
krušnohorským	krušnohorský	k2eAgNnSc7d1	Krušnohorské
odvětvím	odvětví	k1gNnSc7	odvětví
řezbářství	řezbářství	k1gNnSc2	řezbářství
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
hraček	hračka	k1gFnPc2	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
plodinami	plodina	k1gFnPc7	plodina
jsou	být	k5eAaImIp3nP	být
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
oves	oves	k1gInSc4	oves
a	a	k8xC	a
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc1d1	značný
je	být	k5eAaImIp3nS	být
rozsah	rozsah	k1gInSc1	rozsah
luk	louka	k1gFnPc2	louka
<g/>
,	,	kIx,	,
pastvin	pastvina	k1gFnPc2	pastvina
a	a	k8xC	a
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
výrobě	výroba	k1gFnSc6	výroba
převládá	převládat	k5eAaImIp3nS	převládat
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
