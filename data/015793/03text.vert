<s>
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Velká	velký	k2eAgFnSc1d1
vlastenecká	vlastenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Něvský	něvský	k2eAgInSc1d1
prospekt	prospekt	k1gInSc1
během	během	k7c2
obležení	obležení	k1gNnSc2
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1941	#num#	k4
-	-	kIx~
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1944	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Leningrad	Leningrad	k1gInSc1
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
říše	říše	k1gFnSc1
Finsko	Finsko	k1gNnSc4
Finsko	Finsko	k1gNnSc4
Itálie	Itálie	k1gFnSc2
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
W.	W.	kA
R.	R.	kA
von	von	k1gInSc1
Leeb	Leeb	k1gInSc1
Georg	Georg	k1gInSc1
von	von	k1gInSc1
Küchler	Küchler	k1gInSc4
C.	C.	kA
G.	G.	kA
E.	E.	kA
Mannerheim	Mannerheim	k1gMnSc1
Agustín	Agustín	k1gMnSc1
Grandes	Grandes	k1gMnSc1
</s>
<s>
Kliment	Kliment	k1gMnSc1
Vorošilov	Vorošilov	k1gInSc4
Georgij	Georgij	k1gMnPc2
Žukov	Žukov	k1gInSc1
Markjan	Markjana	k1gFnPc2
Popov	Popov	k1gInSc1
Ivan	Ivan	k1gMnSc1
Feďuninskij	Feďuninskij	k1gFnPc2
Leonid	Leonida	k1gFnPc2
Govorov	Govorov	k1gInSc1
Michail	Michail	k1gMnSc1
Chozin	Chozin	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Tribuc	Tribuc	k1gInSc4
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
725	#num#	k4
000	#num#	k4
</s>
<s>
930	#num#	k4
000	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
sever	sever	k1gInSc1
<g/>
:	:	kIx,
1941	#num#	k4
<g/>
:	:	kIx,
85	#num#	k4
371	#num#	k4
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
mrtví	mrtvý	k1gMnPc1
<g/>
,	,	kIx,
zranění	zraněný	k2eAgMnPc1d1
<g/>
,	,	kIx,
pohřešovaní	pohřešovaný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
1942	#num#	k4
<g/>
:	:	kIx,
267	#num#	k4
327	#num#	k4
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
mrtví	mrtvý	k1gMnPc1
<g/>
,	,	kIx,
zranění	zraněný	k2eAgMnPc1d1
<g/>
,	,	kIx,
pohřešovaní	pohřešovaný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
]	]	kIx)
<g/>
1943	#num#	k4
<g/>
:	:	kIx,
205	#num#	k4
937	#num#	k4
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
mrtví	mrtvý	k1gMnPc1
<g/>
,	,	kIx,
zranění	zraněný	k2eAgMnPc1d1
<g/>
,	,	kIx,
pohřešovaní	pohřešovaný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
1944	#num#	k4
<g/>
:	:	kIx,
21	#num#	k4
350	#num#	k4
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
mrtví	mrtvý	k1gMnPc1
<g/>
,	,	kIx,
zranění	zraněný	k2eAgMnPc1d1
<g/>
,	,	kIx,
pohřešovaní	pohřešovaný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
579	#num#	k4
985	#num#	k4
ztrát	ztráta	k1gFnPc2
</s>
<s>
Severní	severní	k2eAgInSc1d1
front	front	k1gInSc1
podle	podle	k7c2
studenoválečného	studenoválečný	k2eAgMnSc2d1
amerického	americký	k2eAgMnSc2d1
historika	historik	k1gMnSc2
Glantze	Glantze	k1gFnSc2
<g/>
:	:	kIx,
<g/>
1	#num#	k4
017	#num#	k4
881	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
zajatých	zajatý	k1gMnPc2
či	či	k8xC
pohřešovaných	pohřešovaný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
418	#num#	k4
185	#num#	k4
raněných	raněný	k1gMnPc2
<g/>
,	,	kIx,
nemocných	nemocný	k1gMnPc2
<g/>
,	,	kIx,
umrzlých	umrzlý	k2eAgMnPc2d1
a	a	k8xC
hladových	hladový	k2eAgMnPc2d1
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
3	#num#	k4
436	#num#	k4
066	#num#	k4
ztrát	ztráta	k1gFnPc2
</s>
<s>
Civilisté	civilista	k1gMnPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
642	#num#	k4
000	#num#	k4
během	během	k7c2
obležení	obležení	k1gNnSc2
<g/>
,	,	kIx,
400	#num#	k4
000	#num#	k4
při	při	k7c6
evakuaci	evakuace	k1gFnSc6
</s>
<s>
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
též	též	k9
blokáda	blokáda	k1gFnSc1
Leningradu	Leningrad	k1gInSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1941	#num#	k4
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
klíčových	klíčový	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c6
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejdelších	dlouhý	k2eAgFnPc2d3
obléhacích	obléhací	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
nejen	nejen	k6eAd1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
dějinách	dějiny	k1gFnPc6
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
ztrát	ztráta	k1gFnPc2
šlo	jít	k5eAaImAgNnS
zřejmě	zřejmě	k6eAd1
o	o	k7c4
nejkrvavější	krvavý	k2eAgNnSc4d3
obléhání	obléhání	k1gNnSc4
v	v	k7c6
lidských	lidský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Leningrad	Leningrad	k1gInSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Petrohrad	Petrohrad	k1gInSc1
neboli	neboli	k8xC
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
druhé	druhý	k4xOgNnSc1
největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
SSSR	SSSR	kA
<g/>
,	,	kIx,
důležité	důležitý	k2eAgNnSc1d1
průmyslové	průmyslový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
základna	základna	k1gFnSc1
Baltského	baltský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
a	a	k8xC
významný	významný	k2eAgInSc1d1
komunikační	komunikační	k2eAgInSc1d1
uzel	uzel	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
počátku	počátek	k1gInSc2
bojů	boj	k1gInPc2
jedním	jeden	k4xCgInSc7
z	z	k7c2
primárních	primární	k2eAgInPc2d1
cílů	cíl	k1gInPc2
německého	německý	k2eAgInSc2d1
útoku	útok	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Operace	operace	k1gFnSc2
Barbarossa	Barbarossa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
nesmírně	smírně	k6eNd1
důležitým	důležitý	k2eAgNnSc7d1
průmyslovým	průmyslový	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
výrobcem	výrobce	k1gMnSc7
oceli	ocel	k1gFnSc2
<g/>
,	,	kIx,
lokomotiv	lokomotiva	k1gFnPc2
<g/>
,	,	kIx,
centrem	centrum	k1gNnSc7
strojírenského	strojírenský	k2eAgInSc2d1
a	a	k8xC
elektrotechnického	elektrotechnický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
se	se	k3xPyFc4
také	také	k9
nalézal	nalézat	k5eAaImAgInS
největší	veliký	k2eAgInSc1d3
gumárenský	gumárenský	k2eAgInSc1d1
závod	závod	k1gInSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
ale	ale	k8xC
nedokázaly	dokázat	k5eNaPmAgFnP
splnit	splnit	k5eAaPmF
zadání	zadání	k1gNnSc4
a	a	k8xC
dobýt	dobýt	k5eAaPmF
dobře	dobře	k6eAd1
opevněné	opevněný	k2eAgNnSc1d1
město	město	k1gNnSc1
jediným	jediný	k2eAgInSc7d1
úderem	úder	k1gInSc7
<g/>
,	,	kIx,
musely	muset	k5eAaImAgInP
je	on	k3xPp3gNnSc4
tedy	tedy	k9
oblehnout	oblehnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obležení	obležení	k1gNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
dokonalé	dokonalý	k2eAgNnSc1d1
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc4
zásobovací	zásobovací	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
uzavřít	uzavřít	k5eAaPmF
zcela	zcela	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
tzv.	tzv.	kA
Cesta	cesta	k1gFnSc1
života	život	k1gInSc2
–	–	k?
ledová	ledový	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
přes	přes	k7c4
Ladožské	ladožský	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Efektivitě	efektivita	k1gFnSc6
obléhání	obléhání	k1gNnSc1
hodně	hodně	k6eAd1
uškodil	uškodit	k5eAaPmAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
finský	finský	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
odmítl	odmítnout	k5eAaPmAgInS
nejen	nejen	k6eAd1
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
město	město	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zakázal	zakázat	k5eAaPmAgInS
jakékoliv	jakýkoliv	k3yIgNnSc4
významnější	významný	k2eAgNnSc1d2
použití	použití	k1gNnSc1
finského	finský	k2eAgNnSc2d1
území	území	k1gNnSc2
pro	pro	k7c4
tento	tento	k3xDgInSc4
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
zaznamenaly	zaznamenat	k5eAaPmAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
vojáků	voják	k1gMnPc2
i	i	k8xC
techniky	technika	k1gFnSc2
a	a	k8xC
ve	v	k7c6
městě	město	k1gNnSc6
zemřelo	zemřít	k5eAaPmAgNnS
hladem	hlad	k1gMnSc7
odhadem	odhad	k1gInSc7
kolem	kolem	k7c2
jednoho	jeden	k4xCgInSc2
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
zimě	zima	k1gFnSc6
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
na	na	k7c6
počátku	počátek	k1gInSc6
obležení	obležení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
část	část	k1gFnSc4
lidí	člověk	k1gMnPc2
evakuovat	evakuovat	k5eAaBmF
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stále	stále	k6eAd1
více	hodně	k6eAd2
nahlodávalo	nahlodávat	k5eAaImAgNnS
obklíčení	obklíčení	k1gNnSc1
města	město	k1gNnSc2
a	a	k8xC
rostly	růst	k5eAaImAgInP
dodávky	dodávka	k1gFnPc4
potravin	potravina	k1gFnPc2
i	i	k8xC
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
1944	#num#	k4
se	se	k3xPyFc4
obležení	obležení	k1gNnSc2
definitivně	definitivně	k6eAd1
zhroutilo	zhroutit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
</s>
<s>
Mobilizace	mobilizace	k1gFnSc1
v	v	k7c6
Leningradě	Leningrad	k1gInSc6
v	v	k7c6
létě	léto	k1gNnSc6
1941	#num#	k4
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
představoval	představovat	k5eAaImAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
Operace	operace	k1gFnSc2
Barbarossa	Barbarossa	k1gMnSc1
důležitý	důležitý	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
úkolem	úkol	k1gInSc7
německé	německý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
polního	polní	k2eAgMnSc2d1
maršála	maršál	k1gMnSc2
von	von	k1gInSc4
Leeb	Leeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
na	na	k7c6
celé	celý	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
začal	začít	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
a	a	k8xC
přes	přes	k7c4
odpor	odpor	k1gInSc4
sovětských	sovětský	k2eAgFnPc2d1
pohraničních	pohraniční	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
zaznamenal	zaznamenat	k5eAaPmAgInS
Wehrmacht	wehrmacht	k1gInSc1
významný	významný	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
hloubky	hloubka	k1gFnSc2
70	#num#	k4
km	km	kA
<g/>
,	,	kIx,
ale	ale	k8xC
zde	zde	k6eAd1
narazil	narazit	k5eAaPmAgMnS
na	na	k7c4
odpor	odpor	k1gInSc4
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
používala	používat	k5eAaImAgFnS
k	k	k7c3
protiútokům	protiútok	k1gInPc3
nové	nový	k2eAgFnSc2d1
tanky	tank	k1gInPc1
T-	T-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
,	,	kIx,
KV-1	KV-1	k1gFnSc1
a	a	k8xC
KV-	KV-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
odolávaly	odolávat	k5eAaImAgInP
střelám	střela	k1gFnPc3
stávajících	stávající	k2eAgInPc2d1
německých	německý	k2eAgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
musela	muset	k5eAaImAgFnS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
zneškodnění	zneškodnění	k1gNnSc3
použít	použít	k5eAaPmF
těžké	těžký	k2eAgInPc1d1
flaky	flak	k1gInPc1
(	(	kIx(
<g/>
především	především	k9
protiletadlové	protiletadlový	k2eAgInPc4d1
kanóny	kanón	k1gInPc4
ráže	ráže	k1gFnSc2
88	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
a	a	k8xC
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlý	rychlý	k2eAgInSc4d1
postup	postup	k1gInSc4
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
mohl	moct	k5eAaImAgMnS
úspěšně	úspěšně	k6eAd1
pokračovat	pokračovat	k5eAaImF
i	i	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
byla	být	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
hustá	hustý	k2eAgFnSc1d1
silniční	silniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňovalo	umožňovat	k5eAaImAgNnS
rychlé	rychlý	k2eAgInPc4d1
přesuny	přesun	k1gInPc4
německých	německý	k2eAgFnPc2d1
tankových	tankový	k2eAgFnPc2d1
a	a	k8xC
motorizovaných	motorizovaný	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětská	sovětský	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
přelomu	přelom	k1gInSc6
června	červen	k1gInSc2
a	a	k8xC
července	červenec	k1gInSc2
zkonsolidovala	zkonsolidovat	k5eAaPmAgFnS
a	a	k8xC
připravila	připravit	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
obraně	obrana	k1gFnSc3
na	na	k7c6
Stalinově	Stalinův	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ležela	ležet	k5eAaImAgFnS
na	na	k7c6
staré	starý	k2eAgFnSc6d1
ruské	ruský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tuto	tento	k3xDgFnSc4
linii	linie	k1gFnSc4
se	se	k3xPyFc4
vedly	vést	k5eAaImAgInP
tvrdé	tvrdý	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
Němci	Němec	k1gMnPc1
ji	on	k3xPp3gFnSc4
dokázali	dokázat	k5eAaPmAgMnP
prorazit	prorazit	k5eAaPmF
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
Pskov	Pskov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
směr	směr	k1gInSc1
útoku	útok	k1gInSc2
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
prostoru	prostor	k1gInSc6
mezi	mezi	k7c7
Ilmeňským	Ilmeňský	k2eAgNnSc7d1
a	a	k8xC
Čudským	čudský	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
útoku	útok	k1gInSc3
Finska	Finsko	k1gNnSc2
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdé	tvrdé	k1gNnSc1
boje	boj	k1gInSc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
s	s	k7c7
Wehrmachtem	wehrmacht	k1gInSc7
a	a	k8xC
jednotkami	jednotka	k1gFnPc7
Waffen-SS	Waffen-SS	k1gMnSc2
pokračovaly	pokračovat	k5eAaImAgInP
na	na	k7c6
přístupech	přístup	k1gInPc6
k	k	k7c3
Leningradu	Leningrad	k1gInSc3
během	během	k7c2
července	červenec	k1gInSc2
a	a	k8xC
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
září	září	k1gNnSc2
stanuly	stanout	k5eAaPmAgFnP
německé	německý	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
již	již	k9
před	před	k7c7
vnějším	vnější	k2eAgNnSc7d1
leningradským	leningradský	k2eAgNnSc7d1
opevněním	opevnění	k1gNnSc7
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
se	se	k3xPyFc4
rozhořely	rozhořet	k5eAaPmAgInP
prudké	prudký	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
německým	německý	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
podařilo	podařit	k5eAaPmAgNnS
uzavřít	uzavřít	k5eAaPmF
kruh	kruh	k1gInSc4
kolem	kolem	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
přístupné	přístupný	k2eAgNnSc1d1
pouze	pouze	k6eAd1
ze	z	k7c2
vzduchu	vzduch	k1gInSc2
nebo	nebo	k8xC
přes	přes	k7c4
vody	voda	k1gFnPc4
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prudké	Prudké	k2eAgInPc1d1
útoky	útok	k1gInPc1
na	na	k7c4
město	město	k1gNnSc4
dále	daleko	k6eAd2
pokračovaly	pokračovat	k5eAaImAgFnP
a	a	k8xC
situace	situace	k1gFnSc1
obránců	obránce	k1gMnPc2
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
kritická	kritický	k2eAgFnSc1d1
<g/>
,	,	kIx,
sovětské	sovětský	k2eAgFnSc3d1
obraně	obrana	k1gFnSc3
v	v	k7c6
září	září	k1gNnSc6
velel	velet	k5eAaImAgMnS
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Georgij	Georgij	k1gFnSc2
Konstantinovič	Konstantinovič	k1gMnSc1
Žukov	Žukov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraně	obraně	k6eAd1
města	město	k1gNnSc2
tehdy	tehdy	k6eAd1
velmi	velmi	k6eAd1
pomáhalo	pomáhat	k5eAaImAgNnS
lodní	lodní	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
pálící	pálící	k2eAgNnSc1d1
z	z	k7c2
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Baltského	baltský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1941	#num#	k4
přišla	přijít	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
z	z	k7c2
německého	německý	k2eAgInSc2d1
hlavního	hlavní	k2eAgInSc2d1
stanu	stan	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
Leningrad	Leningrad	k1gInSc1
nemá	mít	k5eNaImIp3nS
být	být	k5eAaImF
obsazen	obsadit	k5eAaPmNgInS
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
obklíčen	obklíčen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgFnPc1d1
útočné	útočný	k2eAgFnPc1d1
tankové	tankový	k2eAgFnPc1d1
a	a	k8xC
letecké	letecký	k2eAgInPc1d1
svazky	svazek	k1gInPc1
byly	být	k5eAaImAgInP
staženy	stáhnout	k5eAaPmNgInP
a	a	k8xC
poslány	poslat	k5eAaPmNgInP
na	na	k7c4
doplnění	doplnění	k1gNnSc4
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
utrpěla	utrpět	k5eAaPmAgFnS
těžké	těžký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
v	v	k7c6
bojích	boj	k1gInPc6
u	u	k7c2
Smolenska	Smolensko	k1gNnSc2
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
potřebovala	potřebovat	k5eAaImAgFnS
doplnit	doplnit	k5eAaPmF
síly	síla	k1gFnPc4
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
útoku	útok	k1gInSc3
na	na	k7c4
Moskvu	Moskva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Obležení	obležení	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
obležení	obležení	k1gNnSc2
Leningradu	Leningrad	k1gInSc2
květen	květno	k1gNnPc2
1942	#num#	k4
-	-	kIx~
leden	leden	k1gInSc1
1943	#num#	k4
</s>
<s>
Oběti	oběť	k1gFnPc1
blokády	blokáda	k1gFnSc2
jsou	být	k5eAaImIp3nP
pohřbívány	pohřbívat	k5eAaImNgFnP
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
Volkovo	Volkův	k2eAgNnSc1d1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
1942	#num#	k4
</s>
<s>
Město	město	k1gNnSc1
Leningrad	Leningrad	k1gInSc4
bylo	být	k5eAaImAgNnS
obleženo	oblehnout	k5eAaPmNgNnS
německými	německý	k2eAgInPc7d1
a	a	k8xC
finskými	finský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
téměř	téměř	k6eAd1
900	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
městem	město	k1gNnSc7
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
urputným	urputný	k2eAgInPc3d1
leteckým	letecký	k2eAgInPc3d1
soubojům	souboj	k1gInPc3
<g/>
,	,	kIx,
do	do	k7c2
boje	boj	k1gInSc2
se	se	k3xPyFc4
zapojili	zapojit	k5eAaPmAgMnP
i	i	k9
námořníci	námořník	k1gMnPc1
z	z	k7c2
vyřazených	vyřazený	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
revolučního	revoluční	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Aurora	Aurora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
používal	používat	k5eAaImAgMnS
ke	k	k7c3
zničení	zničení	k1gNnSc3
města	město	k1gNnSc2
jakýchkoliv	jakýkoliv	k3yIgInPc2
prostředků	prostředek	k1gInPc2
včetně	včetně	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
některých	některý	k3yIgInPc6
dnech	den	k1gInPc6
na	na	k7c4
něj	on	k3xPp3gMnSc4
nasměroval	nasměrovat	k5eAaPmAgInS
přes	přes	k7c4
1000	#num#	k4
bombardérů	bombardér	k1gInPc2
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
německé	německý	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
do	do	k7c2
města	město	k1gNnSc2
nikdy	nikdy	k6eAd1
nevstoupily	vstoupit	k5eNaPmAgFnP
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
ztráty	ztráta	k1gFnPc1
na	na	k7c6
lidských	lidský	k2eAgInPc6d1
životech	život	k1gInPc6
v	v	k7c6
obleženém	obležený	k2eAgNnSc6d1
městě	město	k1gNnSc6
ohromné	ohromný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynulo	zahynout	k5eAaPmAgNnS
přes	přes	k7c4
půl	půl	k1xP
milionu	milion	k4xCgInSc2
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okupační	okupační	k2eAgInSc1d1
vojska	vojsko	k1gNnSc2
tehdy	tehdy	k6eAd1
vyplenila	vyplenit	k5eAaPmAgFnS
blízká	blízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
významná	významný	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
blokády	blokáda	k1gFnSc2
se	se	k3xPyFc4
zásoby	zásoba	k1gFnPc1
potravin	potravina	k1gFnPc2
v	v	k7c6
obleženém	obležený	k2eAgInSc6d1
Leningradě	Leningrad	k1gInSc6
rychle	rychle	k6eAd1
tenčily	tenčit	k5eAaImAgFnP
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prudkému	prudký	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
přídělů	příděl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
závisel	záviset	k5eAaImAgInS
osud	osud	k1gInSc1
města	město	k1gNnSc2
na	na	k7c6
vodní	vodní	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
přes	přes	k7c4
Ladožské	ladožský	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
blokády	blokáda	k1gFnSc2
se	se	k3xPyFc4
denní	denní	k2eAgFnPc1d1
dávky	dávka	k1gFnPc1
chleba	chléb	k1gInSc2
snížily	snížit	k5eAaPmAgFnP
na	na	k7c4
250	#num#	k4
gramů	gram	k1gInPc2
pro	pro	k7c4
dělníky	dělník	k1gMnPc4
a	a	k8xC
techniky	technik	k1gMnPc4
a	a	k8xC
125	#num#	k4
gramů	gram	k1gInPc2
pro	pro	k7c4
kancelářské	kancelářský	k2eAgMnPc4d1
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
rodinné	rodinný	k2eAgMnPc4d1
příslušníky	příslušník	k1gMnPc4
a	a	k8xC
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
stěží	stěží	k6eAd1
postačovalo	postačovat	k5eAaImAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
normální	normální	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
udržel	udržet	k5eAaPmAgMnS
při	při	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
téměř	téměř	k6eAd1
bez	bez	k7c2
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravu	doprava	k1gFnSc4
na	na	k7c6
ulicích	ulice	k1gFnPc6
vyřadily	vyřadit	k5eAaPmAgInP
silné	silný	k2eAgInPc1d1
mrazy	mráz	k1gInPc1
a	a	k8xC
voda	voda	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
přivážet	přivážet	k5eAaImF
ze	z	k7c2
zamrzlé	zamrzlý	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Něvy	Něva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc4
přejít	přejít	k5eAaPmF
od	od	k7c2
dobytí	dobytí	k1gNnSc2
k	k	k7c3
obléhání	obléhání	k1gNnSc3
Leningradu	Leningrad	k1gInSc2
patrně	patrně	k6eAd1
vyvolal	vyvolat	k5eAaPmAgInS
i	i	k9
postoj	postoj	k1gInSc1
Finů	Fin	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polní	polní	k2eAgMnPc1d1
maršál	maršál	k1gMnSc1
Carl	Carl	k1gMnSc1
Gustaf	Gustaf	k1gMnSc1
Emil	Emil	k1gMnSc1
Mannerheim	Mannerheim	k1gMnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgMnSc1d3
finský	finský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
,	,	kIx,
nechtěl	chtít	k5eNaImAgMnS
překročit	překročit	k5eAaPmF
starou	starý	k2eAgFnSc4d1
finskou	finský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
na	na	k7c6
Karelské	karelský	k2eAgFnSc6d1
šíji	šíj	k1gFnSc6
a	a	k8xC
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maršál	maršál	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pamětech	paměť	k1gFnPc6
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nechtěl	chtít	k5eNaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
finská	finský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
podílela	podílet	k5eAaImAgNnP
na	na	k7c6
předpokládaném	předpokládaný	k2eAgInSc6d1
hrozném	hrozný	k2eAgInSc6d1
zničení	zničení	k1gNnSc6
Leningradu	Leningrad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mannerheim	Mannerheim	k1gMnSc1
tak	tak	k9
zůstal	zůstat	k5eAaPmAgMnS
věrný	věrný	k2eAgMnSc1d1
své	svůj	k3xOyFgFnSc6
zásadě	zásada	k1gFnSc6
„	„	k?
<g/>
aktivní	aktivní	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
neměla	mít	k5eNaImAgFnS
stát	stát	k5eAaPmF,k5eAaImF
válka	válka	k1gFnSc1
dobyvatelská	dobyvatelský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
vojenského	vojenský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
by	by	kYmCp3nS
pádem	pád	k1gInSc7
Leningradu	Leningrad	k1gInSc2
a	a	k8xC
kotle	kotel	k1gInSc2
u	u	k7c2
Oranienbaumu	Oranienbaum	k1gInSc2
bylo	být	k5eAaImAgNnS
odzbrojeno	odzbrojit	k5eAaPmNgNnS
téměř	téměř	k6eAd1
40	#num#	k4
sovětských	sovětský	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
i	i	k9
vyřazení	vyřazení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
jako	jako	k8xC,k8xS
zbrojního	zbrojní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždyť	vždyť	k8xC
tanková	tankový	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
i	i	k8xC
dělové	dělový	k2eAgInPc1d1
a	a	k8xC
muniční	muniční	k2eAgInPc1d1
závody	závod	k1gInPc1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
vyráběly	vyrábět	k5eAaImAgInP
a	a	k8xC
zásobovaly	zásobovat	k5eAaImAgInP
Rudou	rudý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
dodávkami	dodávka	k1gFnPc7
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Leningradu	Leningrad	k1gInSc2
by	by	kYmCp3nS
navíc	navíc	k6eAd1
uvolnil	uvolnit	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
německou	německý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
pro	pro	k7c4
jiné	jiný	k2eAgFnPc4d1
operace	operace	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
tady	tady	k6eAd1
musela	muset	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
hrát	hrát	k5eAaImF
úlohu	úloha	k1gFnSc4
stráží	stráž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
blokáda	blokáda	k1gFnSc1
</s>
<s>
Vykládaní	vykládaný	k2eAgMnPc1d1
zásob	zásoba	k1gFnPc2
z	z	k7c2
člunu	člun	k1gInSc2
u	u	k7c2
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
na	na	k7c4
úzkorozchodný	úzkorozchodný	k2eAgInSc4d1
vlak	vlak	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
obležení	obležení	k1gNnSc2
postačovaly	postačovat	k5eAaImAgFnP
zásoby	zásoba	k1gFnPc1
ropných	ropný	k2eAgMnPc2d1
produktů	produkt	k1gInPc2
na	na	k7c4
tři	tři	k4xCgInPc4
týdny	týden	k1gInPc4
běžného	běžný	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
zásoby	zásoba	k1gFnPc4
uhlí	uhlí	k1gNnSc2
pro	pro	k7c4
80	#num#	k4
dnů	den	k1gInPc2
a	a	k8xC
zásoby	zásoba	k1gFnPc4
palivového	palivový	k2eAgNnSc2d1
dříví	dříví	k1gNnSc2
pro	pro	k7c4
18	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
pracovala	pracovat	k5eAaImAgFnS
jediná	jediný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
ojedinělé	ojedinělý	k2eAgInPc1d1
lokální	lokální	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
elektřiny	elektřina	k1gFnSc2
byla	být	k5eAaImAgFnS
přebudována	přebudovat	k5eAaPmNgFnS
na	na	k7c6
spalování	spalování	k1gNnSc6
dříví	dříví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1941	#num#	k4
činila	činit	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
jednu	jeden	k4xCgFnSc4
sedminu	sedmina	k1gFnSc4
předválečné	předválečný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustálé	neustálý	k2eAgNnSc1d1
omezování	omezování	k1gNnSc1
trolejbusové	trolejbusový	k2eAgFnSc2d1
a	a	k8xC
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
lednu	leden	k1gInSc6
1942	#num#	k4
konečným	konečný	k2eAgNnSc7d1
zastavením	zastavení	k1gNnSc7
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Energetický	energetický	k2eAgInSc1d1
výdaj	výdaj	k1gInSc1
na	na	k7c6
dvou	dva	k4xCgFnPc6
až	až	k6eAd1
tříhodinovou	tříhodinový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
práce	práce	k1gFnSc2
a	a	k8xC
zpět	zpět	k6eAd1
se	se	k3xPyFc4
projevil	projevit	k5eAaPmAgInS
nejen	nejen	k6eAd1
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
výkonech	výkon	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
úmrtnosti	úmrtnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
umírali	umírat	k5eAaImAgMnP
náhle	náhle	k6eAd1
při	při	k7c6
chůzi	chůze	k1gFnSc6
do	do	k7c2
práce	práce	k1gFnSc2
na	na	k7c4
následky	následek	k1gInPc4
vyčerpání	vyčerpání	k1gNnSc2
hladem	hlad	k1gInSc7
a	a	k8xC
chladem	chlad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektřinou	elektřina	k1gFnSc7
byly	být	k5eAaImAgFnP
zásobovány	zásobován	k2eAgInPc1d1
pouze	pouze	k6eAd1
nemocnice	nemocnice	k1gFnPc1
<g/>
,	,	kIx,
palác	palác	k1gInSc1
Smolnyj	Smolnyj	k1gFnSc4
a	a	k8xC
pekárny	pekárna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediný	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
fosilního	fosilní	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
ležely	ležet	k5eAaImAgFnP
v	v	k7c6
rašeliništích	rašeliniště	k1gNnPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
města	město	k1gNnSc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
o	o	k7c6
použití	použití	k1gNnSc6
14	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
pro	pro	k7c4
těžbu	těžba	k1gFnSc4
zmrzlých	zmrzlý	k2eAgInPc2d1
kusů	kus	k1gInPc2
rašeliny	rašelina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
většinou	většinou	k6eAd1
o	o	k7c4
ženy	žena	k1gFnPc4
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
14	#num#	k4
let	léto	k1gNnPc2
při	při	k7c6
přídělech	příděl	k1gInPc6
250	#num#	k4
g	g	kA
chleba	chléb	k1gInSc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
dodávky	dodávka	k1gFnPc1
umožnily	umožnit	k5eAaPmAgFnP
roztopení	roztopení	k1gNnSc4
kotle	kotel	k1gInSc2
tepelné	tepelný	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
Rudý	rudý	k2eAgInSc1d1
říjen	říjen	k1gInSc1
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Zátoka	zátoka	k1gFnSc1
Utky	Utka	k1gFnSc2
na	na	k7c6
břehu	břeh	k1gInSc6
Něvy	Něva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
energetiky	energetika	k1gFnSc2
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
i	i	k9
materiál	materiál	k1gInSc4
z	z	k7c2
15	#num#	k4
000	#num#	k4
dřevěných	dřevěný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1942	#num#	k4
se	se	k3xPyFc4
tak	tak	k9
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
pohybu	pohyb	k1gInSc2
opět	opět	k6eAd1
první	první	k4xOgFnPc4
nákladní	nákladní	k2eAgFnPc4d1
tramvaje	tramvaj	k1gFnPc4
<g/>
,	,	kIx,
o	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
zprovozněno	zprovoznit	k5eAaPmNgNnS
šest	šest	k4xCc1
tras	trasa	k1gFnPc2
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS
první	první	k4xOgFnSc1
<g/>
,	,	kIx,
nejstrašnější	strašný	k2eAgFnSc1d3
zima	zima	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
blokády	blokáda	k1gFnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ústupem	ústup	k1gInSc7
mrazů	mráz	k1gInPc2
prudce	prudko	k6eAd1
poklesla	poklesnout	k5eAaPmAgFnS
i	i	k9
úmrtnost	úmrtnost	k1gFnSc1
na	na	k7c4
následky	následek	k1gInPc4
vyčerpání	vyčerpání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc1
německých	německý	k2eAgFnPc2d1
armád	armáda	k1gFnPc2
se	se	k3xPyFc4
zastavil	zastavit	k5eAaPmAgMnS
a	a	k8xC
fronta	fronta	k1gFnSc1
se	se	k3xPyFc4
stabilizovala	stabilizovat	k5eAaBmAgFnS
i	i	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
Leningradu	Leningrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
mezi	mezi	k7c7
Volchovským	Volchovský	k2eAgInSc7d1
frontem	front	k1gInSc7
a	a	k8xC
skupinou	skupina	k1gFnSc7
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc1
probíhala	probíhat	k5eAaImAgNnP
řekou	řeka	k1gFnSc7
Volchov	Volchov	k1gInSc4
jižně	jižně	k6eAd1
od	od	k7c2
vodní	vodní	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
četné	četný	k2eAgInPc4d1
letecké	letecký	k2eAgInPc4d1
útoky	útok	k1gInPc4
nebyla	být	k5eNaImAgFnS
vodní	vodní	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgInSc4d1
pobřeží	pobřeží	k1gNnSc1
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
však	však	k9
zůstalo	zůstat	k5eAaPmAgNnS
pod	pod	k7c7
dohledem	dohled	k1gInSc7
Němců	Němec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediné	k1gNnSc1
spojení	spojení	k1gNnSc2
s	s	k7c7
Volchovem	Volchovo	k1gNnSc7
umožňovalo	umožňovat	k5eAaImAgNnS
opět	opět	k6eAd1
Ladožské	ladožský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vypracován	vypracovat	k5eAaPmNgInS
plán	plán	k1gInSc1
na	na	k7c4
spojení	spojení	k1gNnSc4
města	město	k1gNnSc2
a	a	k8xC
elektrárny	elektrárna	k1gFnSc2
pomocí	pomocí	k7c2
kabelu	kabel	k1gInSc2
<g/>
,	,	kIx,
položeného	položený	k2eAgInSc2d1
na	na	k7c4
dno	dno	k1gNnSc4
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
cíle	cíl	k1gInPc4
energetického	energetický	k2eAgInSc2d1
významu	význam	k1gInSc2
bylo	být	k5eAaImAgNnS
svrženo	svrhnout	k5eAaPmNgNnS
přes	přes	k7c4
300	#num#	k4
klasických	klasický	k2eAgInPc2d1
a	a	k8xC
1000	#num#	k4
zápalných	zápalný	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
a	a	k8xC
vypáleno	vypálen	k2eAgNnSc4d1
přes	přes	k7c4
3	#num#	k4
000	#num#	k4
granátů	granát	k1gInPc2
kalibru	kalibr	k1gInSc2
přes	přes	k7c4
150	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
cíle	cíl	k1gInPc4
nepřátelských	přátelský	k2eNgInPc2d1
útoků	útok	k1gInPc2
se	se	k3xPyFc4
ocitl	ocitnout	k5eAaPmAgInS
i	i	k9
závod	závod	k1gInSc1
Sevkabel	Sevkabel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
zcela	zcela	k6eAd1
opuštěn	opustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obnovení	obnovení	k1gNnSc6
provozu	provoz	k1gInSc2
v	v	k7c6
březnu	březen	k1gInSc6
1942	#num#	k4
dokázal	dokázat	k5eAaPmAgMnS
do	do	k7c2
konce	konec	k1gInSc2
léta	léto	k1gNnSc2
vyrobit	vyrobit	k5eAaPmF
200	#num#	k4
t	t	k?
měděného	měděný	k2eAgInSc2d1
drátu	drát	k1gInSc2
a	a	k8xC
300	#num#	k4
t	t	k?
kabelu	kabel	k1gInSc6
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
přes	přes	k7c4
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatek	nedostatek	k1gInSc1
izolačního	izolační	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
vyřešila	vyřešit	k5eAaPmAgFnS
Státní	státní	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
cenin	cenina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
uvolnila	uvolnit	k5eAaPmAgFnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
izolace	izolace	k1gFnSc2
kabelu	kabel	k1gInSc2
potřebné	potřebný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
papíru	papír	k1gInSc2
s	s	k7c7
vodoznakem	vodoznak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabel	kabel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
dostal	dostat	k5eAaPmAgInS
název	název	k1gInSc4
Kabel	kabela	k1gFnPc2
života	život	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
získal	získat	k5eAaPmAgInS
i	i	k9
přezdívku	přezdívka	k1gFnSc4
Kabel	kabela	k1gFnPc2
s	s	k7c7
penězi	peníze	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
léta	léto	k1gNnSc2
1942	#num#	k4
během	během	k7c2
utajené	utajený	k2eAgFnSc2d1
operace	operace	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
46	#num#	k4
nocí	noc	k1gFnPc2
položeno	položit	k5eAaPmNgNnS
5	#num#	k4
kabelů	kabel	k1gInPc2
na	na	k7c6
délce	délka	k1gFnSc6
přes	přes	k7c4
20	#num#	k4
km	km	kA
na	na	k7c4
dno	dno	k1gNnSc4
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
20	#num#	k4
až	až	k9
25	#num#	k4
km	km	kA
od	od	k7c2
okupovaného	okupovaný	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
elektrárně	elektrárna	k1gFnSc6
Volchov	Volchov	k1gInSc1
byla	být	k5eAaImAgNnP
instalována	instalovat	k5eAaBmNgNnP
soustrojí	soustrojí	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
před	před	k7c7
rokem	rok	k1gInSc7
před	před	k7c7
postupující	postupující	k2eAgFnSc7d1
frontou	fronta	k1gFnSc7
odvezena	odvezen	k2eAgMnSc4d1
za	za	k7c4
Ural	Ural	k1gInSc4
a	a	k8xC
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nyní	nyní	k6eAd1
poskytla	poskytnout	k5eAaPmAgFnS
elektrickou	elektrický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
odpovídající	odpovídající	k2eAgFnSc4d1
výkonu	výkon	k1gInSc3
řeky	řeka	k1gFnSc2
při	při	k7c6
nízkých	nízký	k2eAgInPc6d1
podzimních	podzimní	k2eAgInPc6d1
a	a	k8xC
zimních	zimní	k2eAgInPc6d1
průtocích	průtok	k1gInPc6
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1942	#num#	k4
začal	začít	k5eAaPmAgMnS
do	do	k7c2
obleženého	obležený	k2eAgInSc2d1
Leningradu	Leningrad	k1gInSc2
od	od	k7c2
řeky	řeka	k1gFnSc2
Volchov	Volchov	k1gInSc1
téct	téct	k5eAaImF
pro	pro	k7c4
napětím	napětí	k1gNnSc7
10	#num#	k4
kV	kV	k?
proud	proud	k1gInSc1
<g/>
,	,	kIx,
poskytující	poskytující	k2eAgInSc1d1
příkon	příkon	k1gInSc1
mezi	mezi	k7c7
20	#num#	k4
až	až	k9
25	#num#	k4
000	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s>
Přívod	přívod	k1gInSc1
energie	energie	k1gFnSc2
do	do	k7c2
obytných	obytný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
měl	mít	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc1d1
psychologický	psychologický	k2eAgInSc1d1
význam	význam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
zimě	zima	k1gFnSc6
obležení	obležení	k1gNnSc6
si	se	k3xPyFc3
obyvatelé	obyvatel	k1gMnPc1
Leningradu	Leningrad	k1gInSc2
mohli	moct	k5eAaImAgMnP
každý	každý	k3xTgInSc4
den	den	k1gInSc4
na	na	k7c4
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
zapnout	zapnout	k5eAaPmF
40	#num#	k4
W	W	kA
žárovku	žárovka	k1gFnSc4
a	a	k8xC
energii	energie	k1gFnSc4
<g/>
,	,	kIx,
získanou	získaný	k2eAgFnSc4d1
příjmem	příjem	k1gInSc7
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
ušetřit	ušetřit	k5eAaPmF
použitím	použití	k1gNnSc7
dopravního	dopravní	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1942	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
přibližné	přibližný	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
Kabelu	kabela	k1gFnSc4
života	život	k1gInSc2
svařeno	svařen	k2eAgNnSc1d1
na	na	k7c6
dně	dno	k1gNnSc6
jezera	jezero	k1gNnSc2
i	i	k8xC
potrubí	potrubí	k1gNnSc2
o	o	k7c6
průměru	průměr	k1gInSc6
100	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
schopné	schopný	k2eAgNnSc1d1
město	město	k1gNnSc1
zásobovat	zásobovat	k5eAaImF
kapalným	kapalný	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
o	o	k7c6
průtoku	průtok	k1gInSc6
300	#num#	k4
t	t	k?
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Přenos	přenos	k1gInSc1
energie	energie	k1gFnSc2
z	z	k7c2
vodní	vodní	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
byl	být	k5eAaImAgMnS
po	po	k7c4
zamrznutí	zamrznutí	k1gNnSc4
hladiny	hladina	k1gFnSc2
jezera	jezero	k1gNnSc2
posílen	posílit	k5eAaPmNgInS
ještě	ještě	k6eAd1
o	o	k7c4
tzv.	tzv.	kA
Ledovou	ledový	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
tvořily	tvořit	k5eAaImAgInP
sloupy	sloup	k1gInPc1
zamrzlé	zamrzlý	k2eAgInPc1d1
přímo	přímo	k6eAd1
v	v	k7c6
ledu	led	k1gInSc6
<g/>
,	,	kIx,
nesoucí	nesoucí	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
35	#num#	k4
kV	kV	k?
asi	asi	k9
15	#num#	k4
km	km	kA
severněji	severně	k6eAd2
od	od	k7c2
trasy	trasa	k1gFnSc2
kabelu	kabel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
akci	akce	k1gFnSc6
Jiskra	jiskra	k1gFnSc1
<g/>
,	,	kIx,
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
toto	tento	k3xDgNnSc4
vedení	vedení	k1gNnSc4
před	před	k7c7
táním	tání	k1gNnSc7
ledu	led	k1gInSc2
přemístěno	přemístit	k5eAaPmNgNnS
na	na	k7c6
území	území	k1gNnSc6
vybojovaného	vybojovaný	k2eAgInSc2d1
zemního	zemní	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
a	a	k8xC
vedeno	veden	k2eAgNnSc1d1
pomocí	pomocí	k7c2
sloupů	sloup	k1gInPc2
z	z	k7c2
rozbitého	rozbitý	k2eAgInSc2d1
železničního	železniční	k2eAgInSc2d1
mostu	most	k1gInSc2
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Něvu	Něva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
těžkých	těžký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
při	při	k7c6
pokládce	pokládka	k1gFnSc6
kabelu	kabela	k1gFnSc4
i	i	k9
při	při	k7c6
budování	budování	k1gNnSc6
Ledové	ledový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
vykonávaly	vykonávat	k5eAaImAgFnP
ženy	žena	k1gFnPc1
a	a	k8xC
dívky	dívka	k1gFnPc1
od	od	k7c2
14	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
osvobození	osvobození	k1gNnSc6
města	město	k1gNnSc2
v	v	k7c6
lednu	leden	k1gInSc6
1944	#num#	k4
byl	být	k5eAaImAgInS
kabel	kabel	k1gInSc1
vyzdvižen	vyzdvihnout	k5eAaPmNgInS
ze	z	k7c2
dna	dno	k1gNnSc2
jezera	jezero	k1gNnSc2
a	a	k8xC
použit	použít	k5eAaPmNgMnS
v	v	k7c6
podzemí	podzemí	k1gNnSc6
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
je	být	k5eAaImIp3nS
uložena	uložit	k5eAaPmNgFnS
pod	pod	k7c7
Něvským	něvský	k2eAgInSc7d1
prospektem	prospekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázky	ukázka	k1gFnSc2
Kabelu	kabel	k1gInSc2
života	život	k1gInSc2
jsou	být	k5eAaImIp3nP
atraktivní	atraktivní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
sbírky	sbírka	k1gFnSc2
Muzea	muzeum	k1gNnSc2
blokády	blokáda	k1gFnSc2
Leningradu	Leningrad	k1gInSc2
i	i	k8xC
Muzea	muzeum	k1gNnSc2
Volchovské	Volchovský	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Německý	německý	k2eAgInSc1d1
pokus	pokus	k1gInSc1
o	o	k7c4
úplné	úplný	k2eAgNnSc4d1
obklíčení	obklíčení	k1gNnSc4
</s>
<s>
Deník	deník	k1gInSc1
jedenáctileté	jedenáctiletý	k2eAgFnSc2d1
Taťjany	Taťjana	k1gFnSc2
Savičevové	Savičevová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
psala	psát	k5eAaImAgFnS
o	o	k7c4
úmrtí	úmrtí	k1gNnSc4
členů	člen	k1gMnPc2
své	svůj	k3xOyFgFnSc2
rodiny	rodina	k1gFnSc2
a	a	k8xC
kamarádů	kamarád	k1gMnPc2
</s>
<s>
U	u	k7c2
Leningradu	Leningrad	k1gInSc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
nasazen	nasadit	k5eAaPmNgInS
Tiger	Tiger	k1gInSc1
</s>
<s>
Německé	německý	k2eAgNnSc1d1
vrchní	vrchní	k2eAgNnSc1d1
velení	velení	k1gNnSc1
ale	ale	k9
nemělo	mít	k5eNaImAgNnS
Leningrad	Leningrad	k1gInSc4
zcela	zcela	k6eAd1
obklíčen	obklíčen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
překážky	překážka	k1gFnPc1
-	-	kIx~
jezera	jezero	k1gNnPc1
<g/>
,	,	kIx,
toky	tok	k1gInPc1
řek	řeka	k1gFnPc2
a	a	k8xC
bažiny	bažina	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
v	v	k7c6
létě	léto	k1gNnSc6
pomáhaly	pomáhat	k5eAaImAgFnP
německé	německý	k2eAgFnSc3d1
obléhací	obléhací	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
Ladožské	ladožský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
a	a	k8xC
řeka	řeka	k1gFnSc1
Něva	Něva	k1gFnSc1
zamrzly	zamrznout	k5eAaPmAgFnP
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
dobrými	dobrý	k2eAgFnPc7d1
dopravními	dopravní	k2eAgFnPc7d1
cestami	cesta	k1gFnPc7
a	a	k8xC
tedy	tedy	k9
obrovskými	obrovský	k2eAgFnPc7d1
dírami	díra	k1gFnPc7
v	v	k7c6
obkličovací	obkličovací	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
dírami	díra	k1gFnPc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
město	město	k1gNnSc1
stále	stále	k6eAd1
zásobovat	zásobovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komisař	komisař	k1gMnSc1
pro	pro	k7c4
obranu	obrana	k1gFnSc4
Ždanov	Ždanov	k1gInSc1
nechal	nechat	k5eAaPmAgInS
po	po	k7c6
příchodu	příchod	k1gInSc6
zimy	zima	k1gFnSc2
vybudovat	vybudovat	k5eAaPmF
na	na	k7c6
ledě	led	k1gInSc6
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
„	„	k?
<g/>
Silnici	silnice	k1gFnSc3
života	život	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
silnici	silnice	k1gFnSc4
a	a	k8xC
železniční	železniční	k2eAgFnSc4d1
přípojku	přípojka	k1gFnSc4
k	k	k7c3
Murmanské	murmanský	k2eAgFnSc3d1
dráze	dráha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
ledové	ledový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
zásobováno	zásobovat	k5eAaImNgNnS
z	z	k7c2
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgInSc1d1
obkličovací	obkličovací	k2eAgInSc1d1
prstenec	prstenec	k1gInSc1
tedy	tedy	k9
rozbil	rozbít	k5eAaPmAgInS
bez	bez	k7c2
boje	boj	k1gInSc2
„	„	k?
<g/>
generál	generál	k1gMnSc1
Mráz	Mráz	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc4
proto	proto	k8xC
zahájila	zahájit	k5eAaPmAgFnS
k	k	k7c3
uzavření	uzavření	k1gNnSc3
této	tento	k3xDgFnSc2
zimní	zimní	k2eAgFnSc2d1
mezery	mezera	k1gFnSc2
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
Tichvinskou	Tichvinský	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
obklíčit	obklíčit	k5eAaPmF
i	i	k9
Ladožské	ladožský	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
a	a	k8xC
uzavřít	uzavřít	k5eAaPmF
Leningrad	Leningrad	k1gInSc4
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
byl	být	k5eAaImAgInS
takový	takový	k3xDgInSc1
<g/>
,	,	kIx,
že	že	k8xS
Finové	Fin	k1gMnPc1
překročí	překročit	k5eAaPmIp3nP
od	od	k7c2
severu	sever	k1gInSc2
řeku	řeka	k1gFnSc4
Svir	Svira	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
si	se	k3xPyFc3
východně	východně	k6eAd1
od	od	k7c2
jezera	jezero	k1gNnSc2
podají	podat	k5eAaPmIp3nP
ruce	ruka	k1gFnPc4
se	s	k7c7
16	#num#	k4
<g/>
.	.	kIx.
německou	německý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
sbor	sbor	k1gInSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
s	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
tankovou	tankový	k2eAgFnSc7d1
a	a	k8xC
s	s	k7c7
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgMnPc4d1
(	(	kIx(
<g/>
motorizovanou	motorizovaný	k2eAgFnSc7d1
<g/>
)	)	kIx)
divizí	divize	k1gFnPc2
z	z	k7c2
volchovských	volchovský	k2eAgNnPc2d1
předmostí	předmostí	k1gNnPc2
<g/>
,	,	kIx,
přes	přes	k7c4
tuto	tento	k3xDgFnSc4
velkou	velký	k2eAgFnSc4d1
řeku	řeka	k1gFnSc4
na	na	k7c4
cestu	cesta	k1gFnSc4
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
cílem	cíl	k1gInSc7
byl	být	k5eAaImAgInS
Tichvin	Tichvina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
zde	zde	k6eAd1
měli	mít	k5eAaImAgMnP
přerušit	přerušit	k5eAaPmF
poslední	poslední	k2eAgNnSc4d1
železniční	železniční	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
z	z	k7c2
Vologdy	Vologda	k1gFnSc2
do	do	k7c2
Leningradu	Leningrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalším	další	k2eAgInSc6d1
postupu	postup	k1gInSc6
měli	mít	k5eAaImAgMnP
dosáhnout	dosáhnout	k5eAaPmF
řeky	řeka	k1gFnPc4
Svir	Svira	k1gFnPc2
a	a	k8xC
setkat	setkat	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
Finy	Fin	k1gMnPc7
a	a	k8xC
tím	ten	k3xDgNnSc7
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
obklíčení	obklíčení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
i	i	k8xC
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
dokončeno	dokončen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgNnSc1d1
velení	velení	k1gNnSc1
ale	ale	k9
poznalo	poznat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
smělá	smělý	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
tanková	tankový	k2eAgFnSc1d1
operace	operace	k1gFnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
spojení	spojení	k1gNnSc4
s	s	k7c7
Finy	Fin	k1gMnPc7
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalin	Stalin	k1gMnSc1
proto	proto	k8xC
nasadil	nasadit	k5eAaPmAgMnS
proti	proti	k7c3
Němcům	Němec	k1gMnPc3
další	další	k2eAgFnSc2d1
sibiřské	sibiřský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgInSc4d1
39	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
sbor	sbor	k1gInSc1
nemohl	moct	k5eNaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
hrozné	hrozný	k2eAgFnSc6d1
pustině	pustina	k1gFnSc6
své	svůj	k3xOyFgNnSc4
postavení	postavení	k1gNnSc4
udržet	udržet	k5eAaPmF
proti	proti	k7c3
neustálým	neustálý	k2eAgInPc3d1
útokům	útok	k1gInPc3
sibiřských	sibiřský	k2eAgFnPc2d1
úderných	úderný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
von	von	k1gInSc4
Arnim	Arnim	k1gMnSc1
<g/>
,	,	kIx,
Schmidtův	Schmidtův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
svoje	svůj	k3xOyFgFnPc4
divize	divize	k1gFnPc4
stáhl	stáhnout	k5eAaPmAgMnS
na	na	k7c4
Volchov	Volchov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
otužilí	otužilý	k2eAgMnPc1d1
Finové	Fin	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
klima	klima	k1gNnSc1
severoruské	severoruský	k2eAgFnSc2d1
tajgy	tajga	k1gFnSc2
bylo	být	k5eAaImAgNnS
důvěrně	důvěrně	k6eAd1
známé	známý	k2eAgNnSc1d1
<g/>
,	,	kIx,
přes	přes	k7c4
Svir	Svir	k1gInSc4
nepřešli	přejít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
byly	být	k5eAaImAgInP
zcela	zcela	k6eAd1
vyčerpané	vyčerpaný	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
39	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
sboru	sbor	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
při	při	k7c6
52	#num#	k4
stupních	stupeň	k1gInPc6
mrazu	mráz	k1gInSc2
převezeny	převezen	k2eAgFnPc4d1
přes	přes	k7c4
Volchov	Volchov	k1gInSc4
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
za	za	k7c7
sebou	se	k3xPyFc7
strašné	strašný	k2eAgNnSc4d1
dobrodružství	dobrodružství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
samotná	samotný	k2eAgFnSc1d1
slezská	slezský	k2eAgFnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc3d1
(	(	kIx(
<g/>
motorizovaná	motorizovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
divize	divize	k1gFnSc1
při	při	k7c6
něm	on	k3xPp3gMnSc6
ztratila	ztratit	k5eAaPmAgFnS
9	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
bojová	bojový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
činila	činit	k5eAaImAgFnS
k	k	k7c3
tomuto	tento	k3xDgInSc3
dni	den	k1gInSc3
741	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
boj	boj	k1gInSc1
o	o	k7c6
Tichvin	Tichvina	k1gFnPc2
podepsal	podepsat	k5eAaPmAgMnS
na	na	k7c6
bojové	bojový	k2eAgFnSc6d1
síle	síla	k1gFnSc6
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
demonstruje	demonstrovat	k5eAaBmIp3nS
osud	osud	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
(	(	kIx(
<g/>
motorizovaného	motorizovaný	k2eAgInSc2d1
<g/>
)	)	kIx)
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pochodu	pochod	k1gInSc6
z	z	k7c2
Čudova	Čudův	k2eAgInSc2d1
do	do	k7c2
Tichvinu	Tichvina	k1gFnSc4
ztratil	ztratit	k5eAaPmAgMnS
za	za	k7c2
mrazu	mráz	k1gInSc2
-40	-40	k4
stupňů	stupeň	k1gInPc2
250	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
polovinu	polovina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
bojové	bojový	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
kvůli	kvůli	k7c3
omrzlinám	omrzlina	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fronta	fronta	k1gFnSc1
mezi	mezi	k7c7
Leningradem	Leningrad	k1gInSc7
a	a	k8xC
Volchovem	Volchov	k1gInSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
trvalým	trvalý	k2eAgNnSc7d1
ohniskem	ohnisko	k1gNnSc7
nebezpečí	nebezpečí	k1gNnSc1
pro	pro	k7c4
německou	německý	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
pravděpodobně	pravděpodobně	k6eAd1
výsledkem	výsledek	k1gInSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
ustoupeno	ustoupen	k2eAgNnSc1d1
od	od	k7c2
dobytí	dobytí	k1gNnSc2
Leningradu	Leningrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
výsledek	výsledek	k1gInSc1
snahy	snaha	k1gFnSc2
dobýt	dobýt	k5eAaPmF
příliš	příliš	k6eAd1
mnoho	mnoho	k6eAd1
na	na	k7c6
příliš	příliš	k6eAd1
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
nedosáhl	dosáhnout	k5eNaPmAgInS
svoje	svůj	k3xOyFgInPc4
operační	operační	k2eAgInPc4d1
cíle	cíl	k1gInPc4
ani	ani	k8xC
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
ani	ani	k8xC
na	na	k7c6
střední	střední	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leningrad	Leningrad	k1gInSc4
i	i	k9
Moskva	Moskva	k1gFnSc1
zůstaly	zůstat	k5eAaPmAgFnP
pro	pro	k7c4
Němce	Němec	k1gMnSc4
nedobytné	dobytný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1
protiútoky	protiútok	k1gInPc1
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
zákopu	zákop	k1gInSc6
před	před	k7c7
Leningradem	Leningrad	k1gInSc7
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
1941	#num#	k4
</s>
<s>
Medaile	medaile	k1gFnSc1
za	za	k7c4
obranu	obrana	k1gFnSc4
Leningradu	Leningrad	k1gInSc2
</s>
<s>
Pro	pro	k7c4
rok	rok	k1gInSc4
1942	#num#	k4
stanovilo	stanovit	k5eAaPmAgNnS
německé	německý	k2eAgNnSc1d1
vrchní	vrchní	k2eAgNnSc1d1
velení	velení	k1gNnSc1
na	na	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
fronty	fronta	k1gFnSc2
několik	několik	k4yIc1
primárních	primární	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
/	/	kIx~
Zmocnit	zmocnit	k5eAaPmF
se	se	k3xPyFc4
Leningradu	Leningrad	k1gInSc3
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
již	již	k6eAd1
nikoliv	nikoliv	k9
přímým	přímý	k2eAgInSc7d1
útokem	útok	k1gInSc7
pro	pro	k7c4
nějž	jenž	k3xRgMnSc4
chyběly	chybět	k5eAaImAgFnP
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
blokádou	blokáda	k1gFnSc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
Definitivně	definitivně	k6eAd1
vyřadit	vyřadit	k5eAaPmF
letectvem	letectvo	k1gNnSc7
Baltské	baltský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
/	/	kIx~
Posílit	posílit	k5eAaPmF
blokádní	blokádní	k2eAgMnSc1d1
postavení	postavení	k1gNnSc4
ve	v	k7c6
Finském	finský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
před	před	k7c7
Kronštadtem	Kronštadt	k1gInSc7
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
sovětské	sovětský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
ponorky	ponorka	k1gFnSc2
<g/>
,	,	kIx,
nemohlo	moct	k5eNaImAgNnS
proniknout	proniknout	k5eAaPmF
na	na	k7c4
otevřené	otevřený	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Ani	ani	k8xC
sovětské	sovětský	k2eAgNnSc1d1
velení	velení	k1gNnSc1
však	však	k9
nezahálelo	zahálet	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1942	#num#	k4
se	se	k3xPyFc4
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pokusila	pokusit	k5eAaPmAgFnS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
německé	německý	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
v	v	k7c6
prostoru	prostor	k1gInSc6
Volchova	Volchův	k2eAgNnSc2d1
a	a	k8xC
dostat	dostat	k5eAaPmF
tak	tak	k6eAd1
město	město	k1gNnSc4
z	z	k7c2
kleští	kleště	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
útok	útok	k1gInSc1
začal	začít	k5eAaPmAgInS
poměrně	poměrně	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
ovšem	ovšem	k9
jednotky	jednotka	k1gFnSc2
Wehrmachtu	wehrmacht	k1gInSc2
posílené	posílený	k2eAgFnPc4d1
o	o	k7c4
vojska	vojsko	k1gNnPc4
Waffen-SS	Waffen-SS	k1gFnSc2
nakonec	nakonec	k6eAd1
převzaly	převzít	k5eAaPmAgFnP
iniciativu	iniciativa	k1gFnSc4
a	a	k8xC
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
obklíčily	obklíčit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgNnSc6
německém	německý	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
byl	být	k5eAaImAgMnS
zajat	zajat	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Andrejevič	Andrejevič	k1gMnSc1
Vlasov	Vlasovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílčí	dílčí	k2eAgInPc1d1
boje	boj	k1gInPc1
pokračovaly	pokračovat	k5eAaImAgInP
i	i	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
výrazných	výrazný	k2eAgInPc2d1
posunů	posun	k1gInPc2
na	na	k7c6
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
G.	G.	kA
K.	K.	kA
Žukova	Žukov	k1gInSc2
dostat	dostat	k5eAaPmF
Leningrad	Leningrad	k1gInSc4
z	z	k7c2
kleští	kleště	k1gFnPc2
znovu	znovu	k6eAd1
a	a	k8xC
začala	začít	k5eAaPmAgFnS
podnikat	podnikat	k5eAaImF
protiútoky	protiútok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgMnS
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1943	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
sovětské	sovětský	k2eAgFnSc2d1
Operace	operace	k1gFnSc2
Jiskra	jiskra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
vyklidit	vyklidit	k5eAaPmF
jižní	jižní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Ladožského	ladožský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
u	u	k7c2
Šlisselburgu	Šlisselburg	k1gInSc2
a	a	k8xC
Rudé	rudý	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
se	se	k3xPyFc4
po	po	k7c6
urputných	urputný	k2eAgInPc6d1
bojích	boj	k1gInPc6
podařilo	podařit	k5eAaPmAgNnS
prolomit	prolomit	k5eAaPmF
blokádu	blokáda	k1gFnSc4
pouze	pouze	k6eAd1
v	v	k7c6
úzké	úzký	k2eAgFnSc6d1
šíji	šíj	k1gFnSc6
ne	ne	k9
širší	široký	k2eAgInSc4d2
než	než	k8xS
20	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Leningradu	Leningrad	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
tomto	tento	k3xDgInSc6
úzkém	úzký	k2eAgInSc6d1
koridoru	koridor	k1gInSc6
urychleně	urychleně	k6eAd1
vybudována	vybudován	k2eAgFnSc1d1
provizorní	provizorní	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
i	i	k8xC
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1943	#num#	k4
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
ní	on	k3xPp3gFnSc3
se	se	k3xPyFc4
zvětšil	zvětšit	k5eAaPmAgInS
příděl	příděl	k1gInSc1
chleba	chléb	k1gInSc2
pro	pro	k7c4
dělníky	dělník	k1gMnPc4
z	z	k7c2
250	#num#	k4
g	g	kA
na	na	k7c4
600	#num#	k4
g	g	kA
a	a	k8xC
pro	pro	k7c4
ostatní	ostatní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
ze	z	k7c2
125	#num#	k4
g	g	kA
na	na	k7c4
400	#num#	k4
g	g	kA
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
podařil	podařit	k5eAaPmAgInS
dobýt	dobýt	k5eAaPmF
právě	právě	k6eAd1
jen	jen	k9
tento	tento	k3xDgInSc4
úzký	úzký	k2eAgInSc4d1
pás	pás	k1gInSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
ohrožován	ohrožován	k2eAgInSc1d1
nepřátelským	přátelský	k2eNgNnSc7d1
odstřelováním	odstřelování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
sovětské	sovětský	k2eAgFnPc1d1
vrchní	vrchní	k1gFnPc1
velení	velení	k1gNnSc1
rozhodlo	rozhodnout	k5eAaPmAgNnS
k	k	k7c3
dalším	další	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
však	však	k9
kvůli	kvůli	k7c3
silnému	silný	k2eAgNnSc3d1
německému	německý	k2eAgNnSc3d1
opevnění	opevnění	k1gNnSc3
a	a	k8xC
vysokým	vysoký	k2eAgFnPc3d1
ztrátám	ztráta	k1gFnPc3
ztroskotaly	ztroskotat	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ofenzíva	ofenzíva	k1gFnSc1
byla	být	k5eAaImAgFnS
proto	proto	k8xC
koncem	koncem	k7c2
března	březen	k1gInSc2
1943	#num#	k4
ukončena	ukončen	k2eAgFnSc1d1
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
probíhaly	probíhat	k5eAaImAgInP
pouze	pouze	k6eAd1
dílčí	dílčí	k2eAgInPc1d1
místní	místní	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečnou	konečný	k2eAgFnSc7d1
ofenzívou	ofenzíva	k1gFnSc7
<g/>
,	,	kIx,
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
definitivnímu	definitivní	k2eAgNnSc3d1
prolomení	prolomení	k1gNnSc3
blokády	blokáda	k1gFnSc2
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Leningradsko-novgorodská	leningradsko-novgorodský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
začala	začít	k5eAaPmAgNnP
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
ofenzívy	ofenzíva	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
a	a	k8xC
1943	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1944	#num#	k4
zahájila	zahájit	k5eAaPmAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
mohutnou	mohutný	k2eAgFnSc4d1
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
následoval	následovat	k5eAaImAgInS
útok	útok	k1gInSc1
tankových	tankový	k2eAgNnPc2d1
a	a	k8xC
mechanizovaných	mechanizovaný	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
za	za	k7c2
masívní	masívní	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
letectva	letectvo	k1gNnSc2
na	na	k7c4
vojska	vojsko	k1gNnPc4
německé	německý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1944	#num#	k4
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
konečnému	konečný	k2eAgNnSc3d1
prolomení	prolomení	k1gNnSc3
fronty	fronta	k1gFnSc2
v	v	k7c6
důležitých	důležitý	k2eAgInPc6d1
komunikačních	komunikační	k2eAgInPc6d1
uzlech	uzel	k1gInPc6
a	a	k8xC
blokáda	blokáda	k1gFnSc1
Leningradu	Leningrad	k1gInSc2
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
definitivně	definitivně	k6eAd1
prolomena	prolomen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
25	#num#	k4
October	October	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
28	#num#	k4
December	December	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
25	#num#	k4
May	May	k1gMnSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
October	October	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Glantz	Glantza	k1gFnPc2
2001	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
179	#num#	k4
<g/>
↑	↑	k?
Eight	Eight	k2eAgMnSc1d1
Horrific	Horrific	k1gMnSc1
Facts	Factsa	k1gFnPc2
About	About	k1gMnSc1
the	the	k?
Siege	Siege	k1gInSc1
of	of	k?
Leningrad	Leningrad	k1gInSc1
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryCollection	HistoryCollection	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Obležení	obležení	k1gNnSc2
Leningradu	Leningrad	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Blokáda	blokáda	k1gFnSc1
a	a	k8xC
kanibalismus	kanibalismus	k1gInSc1
</s>
<s>
1941-1942	1941-1942	k4
–	–	k?
Skupina	skupina	k1gFnSc1
Armád	armáda	k1gFnPc2
Sever	sever	k1gInSc4
</s>
<s>
Blokáda	blokáda	k1gFnSc1
Leningradu	Leningrad	k1gInSc2
–	–	k?
fotografie	fotografia	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Operace	operace	k1gFnSc1
a	a	k8xC
bitvy	bitva	k1gFnPc1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Operace	operace	k1gFnSc1
Barbarossa	Barbarossa	k1gMnSc1
•	•	k?
Obrana	obrana	k1gFnSc1
Brestské	brestský	k2eAgFnPc1d1
pevnosti	pevnost	k1gFnPc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bělostoku	Bělostok	k1gInSc2
a	a	k8xC
Minsku	Minsk	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Brodů	Brod	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Smolenska	Smolensko	k1gNnSc2
•	•	k?
Obležení	obležení	k1gNnSc2
Oděsy	Oděsa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kyjevský	kyjevský	k2eAgInSc4d1
kotel	kotel	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
před	před	k7c7
Moskvou	Moskva	k1gFnSc7
•	•	k?
Pokračovací	pokračovací	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Sevastopol	Sevastopol	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Rostov	Rostov	k1gInSc4
•	•	k?
Kerčsko-feodosijská	Kerčsko-feodosijský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Děmjanský	Děmjanský	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
kotel	kotel	k1gInSc1
•	•	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
o	o	k7c4
Charkov	Charkov	k1gInSc4
•	•	k?
Operace	operace	k1gFnSc1
Blau	Blaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Voroněže	Voroněž	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Ržev	Ržev	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kavkaz	Kavkaz	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Stalingradu	Stalingrad	k1gInSc2
•	•	k?
Operace	operace	k1gFnSc1
Uran	Uran	k1gInSc1
•	•	k?
Operace	operace	k1gFnSc1
Saturn	Saturn	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Velikije	Velikije	k1gFnSc6
Luki	Luk	k1gFnSc2
•	•	k?
Operace	operace	k1gFnSc1
Mars	Mars	k1gInSc1
•	•	k?
Mansteinova	Mansteinův	k2eAgFnSc1d1
jarní	jarní	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Sokolova	Sokolov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
Kurska	Kursk	k1gInSc2
•	•	k?
Operace	operace	k1gFnSc1
Kutuzov	Kutuzovo	k1gNnPc2
•	•	k?
Operace	operace	k1gFnSc2
Vojevůdce	vojevůdce	k1gMnSc1
Rumjancev	Rumjancev	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc1
Suvorov	Suvorov	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Dněpr	Dněpr	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kyjev	Kyjev	k1gInSc4
•	•	k?
Žytomyrsko-berdyčevská	Žytomyrsko-berdyčevský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Leningradsko-novgorodská	leningradsko-novgorodský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Korsuň-ševčenkovská	Korsuň-ševčenkovský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kamence	Kamenec	k1gInSc2
Podolského	podolský	k2eAgInSc2d1
•	•	k?
Krymská	krymský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc1
Bagration	Bagration	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tali-Ihantala	Tali-Ihantal	k1gMnSc2
•	•	k?
Lvovsko-sandoměřská	Lvovsko-sandoměřský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Jasko-kišiněvská	Jasko-kišiněvský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Karpatsko-dukelská	karpatsko-dukelský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Baltická	baltický	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bělehradská	bělehradský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Budapešťská	budapešťský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Viselsko-oderská	viselsko-oderský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Západokarpatská	Západokarpatský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Východopruská	východopruský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jasla	Jaslo	k1gNnSc2
•	•	k?
Boje	boj	k1gInPc1
o	o	k7c4
Liptovský	liptovský	k2eAgInSc4d1
Mikuláš	mikuláš	k1gInSc4
•	•	k?
Východopomořanská	Východopomořanský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc2
Jarní	jarní	k2eAgNnSc1d1
probuzení	probuzení	k1gNnSc1
•	•	k?
Ostravská	ostravský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Hornoslezská	hornoslezský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bratislavsko-brněnská	bratislavsko-brněnský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Vídeňská	vídeňský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
•	•	k?
Pražská	pražský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
249644	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4193689-9	4193689-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
93001009	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
93001009	#num#	k4
</s>
