<s>
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
též	též	k9
blokáda	blokáda	k1gFnSc1
Leningradu	Leningrad	k1gInSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1941	#num#	k4
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
klíčových	klíčový	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>