<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c4
PrazeAkademie	PrazeAkademie	k1gFnPc4
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Logo	logo	k1gNnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
AMU	AMU	kA
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1945	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgNnPc1d1
Vedení	vedení	k1gNnPc1
Rektorka	rektorka	k1gFnSc1
</s>
<s>
PhDr.	PhDr.	kA
Ingeborg	Ingeborg	k1gInSc1
Radok	Radok	k1gInSc1
Žádná	žádný	k3yNgNnPc4
Kvestor	kvestor	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Ladislav	Ladislav	k1gMnSc1
Paluska	Palusek	k1gMnSc2
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
studijní	studijní	k2eAgFnPc4d1
a	a	k8xC
pedagogické	pedagogický	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
</s>
<s>
prof.	prof.	kA
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Janeček	Janeček	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
mezinárodní	mezinárodní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
,	,	kIx,
uměleckou	umělecký	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
a	a	k8xC
styk	styk	k1gInSc4
s	s	k7c7
absolventy	absolvent	k1gMnPc7
a	a	k8xC
veřejností	veřejnost	k1gFnSc7
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hančil	Hančil	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vědecko-výzkumnou	vědecko-výzkumný	k2eAgFnSc4d1
a	a	k8xC
umělecko-výzkumnou	umělecko-výzkumný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
</s>
<s>
Mgr.	Mgr.	kA
et	et	k?
Mgr.	Mgr.	kA
Eliška	Eliška	k1gFnSc1
Děcká	Děcká	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
703	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
545	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
134	#num#	k4
Počet	počet	k1gInSc4
studentů	student	k1gMnPc2
</s>
<s>
1	#num#	k4
470	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
398	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
3	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Malostranské	malostranský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
259	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
118	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
Hartigovský	Hartigovský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Členství	členství	k1gNnSc1
</s>
<s>
Asociace	asociace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
</s>
<s>
www.amu.cz	www.amu.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
zkr.	zkr.	kA
AMU	AMU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
umělecká	umělecký	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1945	#num#	k4
prezidentským	prezidentský	k2eAgInSc7d1
zřizovacím	zřizovací	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výuce	výuka	k1gFnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
AMU	AMU	kA
obecně	obecně	k6eAd1
kladen	klást	k5eAaImNgInS
důraz	důraz	k1gInSc1
na	na	k7c4
individuální	individuální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
talentu	talent	k1gInSc2
a	a	k8xC
prohlubování	prohlubování	k1gNnSc2
osobnostního	osobnostní	k2eAgInSc2d1
projevu	projev	k1gInSc2
v	v	k7c6
divadelních	divadelní	k2eAgFnPc6d1
<g/>
,	,	kIx,
filmových	filmový	k2eAgInPc2d1
<g/>
,	,	kIx,
televizních	televizní	k2eAgInPc2d1
<g/>
,	,	kIx,
hudebních	hudební	k2eAgInPc2d1
a	a	k8xC
tanečních	taneční	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělání	vzdělání	k1gNnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
studenta	student	k1gMnSc4
jako	jako	k8xC,k8xS
specifického	specifický	k2eAgMnSc4d1
jednotlivce	jednotlivec	k1gMnSc4
a	a	k8xC
výuka	výuka	k1gFnSc1
proto	proto	k8xC
většinou	většinou	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
malých	malý	k2eAgFnPc6d1
studijních	studijní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
HAMU	HAMU	kA
jde	jít	k5eAaImIp3nS
u	u	k7c2
instrumentálních	instrumentální	k2eAgInPc2d1
oborů	obor	k1gInPc2
o	o	k7c4
striktně	striktně	k6eAd1
individuální	individuální	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc4
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
studentů	student	k1gMnPc2
na	na	k7c6
této	tento	k3xDgFnSc6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
činí	činit	k5eAaImIp3nS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
existovalo	existovat	k5eAaImAgNnS
135	#num#	k4
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
uskutečňovaných	uskutečňovaný	k2eAgInPc2d1
ve	v	k7c6
všech	všecek	k3xTgInPc6
stupních	stupeň	k1gInPc6
(	(	kIx(
<g/>
bakalářském	bakalářský	k2eAgMnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgInSc6d1
i	i	k8xC
doktorském	doktorský	k2eAgInSc6d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
studovalo	studovat	k5eAaImAgNnS
celkem	celkem	k6eAd1
1	#num#	k4
525	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
programů	program	k1gInPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
má	mít	k5eAaImIp3nS
AMU	AMU	kA
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
roku	rok	k1gInSc3
2018	#num#	k4
studovalo	studovat	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
125	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
také	také	k9
kurzy	kurz	k1gInPc7
celoživotního	celoživotní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
češtině	čeština	k1gFnSc6
a	a	k8xC
krátkodobé	krátkodobý	k2eAgInPc4d1
i	i	k8xC
dlouhodobé	dlouhodobý	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
pro	pro	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
zájemce	zájemce	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
AMU	AMU	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
–	–	k?
Filmové	filmový	k2eAgFnSc2d1
a	a	k8xC
televizní	televizní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hudební	hudební	k2eAgFnSc2d1
a	a	k8xC
taneční	taneční	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
HAMU	HAMU	kA
<g/>
)	)	kIx)
a	a	k8xC
Divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pod	pod	k7c4
ni	on	k3xPp3gFnSc4
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
Galerie	galerie	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
GAMU	game	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
(	(	kIx(
<g/>
NAMU	NAMU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
<g/>
,	,	kIx,
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
<g/>
,	,	kIx,
studio	studio	k1gNnSc1
TON	TON	kA
<g/>
,	,	kIx,
koleje	kolej	k1gFnPc4
a	a	k8xC
další	další	k2eAgNnPc4d1
ubytovací	ubytovací	k2eAgNnPc4d1
a	a	k8xC
účelová	účelový	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
dekretem	dekret	k1gInSc7
č.	č.	k?
127	#num#	k4
prezidenta	prezident	k1gMnSc2
Edvarda	Edvard	k1gMnSc2
Beneše	Beneš	k1gMnSc2
„	„	k?
<g/>
o	o	k7c6
zřízení	zřízení	k1gNnSc6
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Akademie	akademie	k1gFnSc1
musických	musický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
akademickým	akademický	k2eAgInSc7d1
rokem	rok	k1gInSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rok	rok	k1gInSc1
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
§	§	k?
1	#num#	k4
zmíněného	zmíněný	k2eAgInSc2d1
dekretu	dekret	k1gInSc2
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Tato	tento	k3xDgFnSc1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
má	mít	k5eAaImIp3nS
4	#num#	k4
odbory	odbor	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
odbor	odbor	k1gInSc1
hudební	hudební	k2eAgInSc1d1
<g/>
,	,	kIx,
odbor	odbor	k1gInSc1
dramatický	dramatický	k2eAgInSc1d1
<g/>
,	,	kIx,
odbor	odbor	k1gInSc1
taneční	taneční	k2eAgInSc1d1
a	a	k8xC
odbor	odbor	k1gInSc1
filmový	filmový	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
školy	škola	k1gFnSc2
otevřel	otevřít	k5eAaPmAgInS
filmový	filmový	k2eAgInSc1d1
odbor	odbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
uchazečům	uchazeč	k1gMnPc3
začal	začít	k5eAaPmAgMnS
nabízet	nabízet	k5eAaImF
obory	obor	k1gInPc4
režie	režie	k1gFnSc2
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc2
a	a	k8xC
filmového	filmový	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
pátou	pátá	k1gFnSc4
filmovou	filmový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
–	–	k?
po	po	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
Římu	Řím	k1gInSc3
a	a	k8xC
Paříži	Paříž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgFnSc1d1
i	i	k8xC
divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
provoz	provoz	k1gInSc4
prvních	první	k4xOgInPc2
oborů	obor	k1gInPc2
velice	velice	k6eAd1
záhy	záhy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
HAMU	HAMU	kA
byly	být	k5eAaImAgInP
zřízeny	zřízen	k2eAgInPc1d1
obory	obor	k1gInPc1
<g/>
:	:	kIx,
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
<g/>
,	,	kIx,
varhany	varhany	k1gInPc1
<g/>
,	,	kIx,
nástroje	nástroj	k1gInPc1
smyčcového	smyčcový	k2eAgNnSc2d1
kvarteta	kvarteto	k1gNnSc2
<g/>
,	,	kIx,
kontrabas	kontrabas	k1gInSc1
<g/>
,	,	kIx,
harfa	harfa	k1gFnSc1
a	a	k8xC
nástroje	nástroj	k1gInPc1
dechového	dechový	k2eAgNnSc2d1
kvinteta	kvinteto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadelní	divadelní	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
výrazně	výrazně	k6eAd1
formoval	formovat	k5eAaImAgMnS
vliv	vliv	k1gInSc4
Jiřího	Jiří	k1gMnSc2
Frejky	Frejka	k1gFnSc2
<g/>
,	,	kIx,
Františka	František	k1gMnSc2
Tröstra	Tröstr	k1gMnSc2
<g/>
,	,	kIx,
Josefa	Josef	k1gMnSc2
Trägra	Trägr	k1gMnSc2
<g/>
,	,	kIx,
Františka	František	k1gMnSc2
Salzra	Salzr	k1gMnSc2
a	a	k8xC
Miroslava	Miroslav	k1gMnSc2
Hallera	Haller	k1gMnSc2
a	a	k8xC
otevřely	otevřít	k5eAaPmAgFnP
se	se	k3xPyFc4
obory	obora	k1gFnSc2
režie	režie	k1gFnSc2
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc2
a	a	k8xC
scénografie	scénografie	k1gFnSc2
<g/>
;	;	kIx,
obor	obor	k1gInSc1
herectví	herectví	k1gNnSc2
prozatím	prozatím	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
na	na	k7c6
konzervatoři	konzervatoř	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
studijních	studijní	k2eAgInPc2d1
plánů	plán	k1gInPc2
začlenil	začlenit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
i	i	k8xC
divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
v	v	k7c6
Paláci	palác	k1gInSc6
Unitaria	Unitarium	k1gNnSc2
v	v	k7c6
Karlově	Karlův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
č.	č.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Inscenace	inscenace	k1gFnSc1
v	v	k7c6
divadle	divadlo	k1gNnSc6
DISK	disk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
z	z	k7c2
původních	původní	k2eAgInPc2d1
oborů	obor	k1gInPc2
FAMU	FAMU	kA
postupně	postupně	k6eAd1
staly	stát	k5eAaPmAgFnP
katedry	katedra	k1gFnPc1
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
přibyla	přibýt	k5eAaPmAgFnS
katedra	katedra	k1gFnSc1
produkce	produkce	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
dramaturgii	dramaturgie	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
specializace	specializace	k1gFnPc4
na	na	k7c4
filmovou	filmový	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HAMU	HAMU	kA
otevřela	otevřít	k5eAaPmAgFnS
možnost	možnost	k1gFnSc1
studovat	studovat	k5eAaImF
trubku	trubka	k1gFnSc4
a	a	k8xC
trombon	trombon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
významné	významný	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Emma	Emma	k1gFnSc1
Fierlingová	Fierlingový	k2eAgFnSc1d1
nebo	nebo	k8xC
Jarmila	Jarmila	k1gFnSc1
Kröschlová	Kröschlová	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
na	na	k7c4
prohloubení	prohloubení	k1gNnSc4
výuky	výuka	k1gFnSc2
hlasového	hlasový	k2eAgInSc2d1
projevu	projev	k1gInSc2
a	a	k8xC
jevištního	jevištní	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
divadelní	divadelní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
dramaturgie	dramaturgie	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
vývoj	vývoj	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
opět	opět	k6eAd1
neblaze	blaze	k6eNd1
poznamenán	poznamenat	k5eAaPmNgInS
totalitní	totalitní	k2eAgFnSc7d1
ideologií	ideologie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1960	#num#	k4
se	se	k3xPyFc4
tedy	tedy	k9
výuka	výuka	k1gFnSc1
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c4
Filozofickou	filozofický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
přišel	přijít	k5eAaPmAgInS
na	na	k7c4
katedru	katedra	k1gFnSc4
režie	režie	k1gFnSc2
FAMU	FAMU	kA
Otakar	Otakar	k1gMnSc1
Vávra	Vávra	k1gMnSc1
s	s	k7c7
novou	nový	k2eAgFnSc7d1
koncepcí	koncepce	k1gFnSc7
přednášek	přednáška	k1gFnPc2
a	a	k8xC
přijímacího	přijímací	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobně	osobně	k6eAd1
si	se	k3xPyFc3
vybíral	vybírat	k5eAaImAgMnS
uchazeče	uchazeč	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
vychovával	vychovávat	k5eAaImAgMnS
jádro	jádro	k1gNnSc4
takzvané	takzvaný	k2eAgFnSc2d1
nové	nový	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
Věra	Věra	k1gFnSc1
Chytilová	Chytilová	k1gFnSc1
<g/>
,	,	kIx,
Evald	Evald	k1gInSc1
Schorm	Schorm	k1gInSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Menzel	Menzel	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Forman	Forman	k1gMnSc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
na	na	k7c6
HAMU	HAMU	kA
rozšířila	rozšířit	k5eAaPmAgFnS
nabídka	nabídka	k1gFnSc1
oborů	obor	k1gInPc2
(	(	kIx(
<g/>
cembalo	cembalo	k1gNnSc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
přecházely	přecházet	k5eAaImAgInP
taneční	taneční	k2eAgInPc1d1
obory	obor	k1gInPc1
<g/>
:	:	kIx,
taneční	taneční	k2eAgFnPc1d1
pedagogiky	pedagogika	k1gFnPc1
<g/>
,	,	kIx,
choreografie	choreografie	k1gFnSc1
<g/>
,	,	kIx,
taneční	taneční	k2eAgFnSc1d1
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
teorie	teorie	k1gFnSc1
tance	tanec	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
vznikl	vzniknout	k5eAaPmAgInS
obor	obor	k1gInSc4
nonverbálního	nonverbální	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
(	(	kIx(
<g/>
původně	původně	k6eAd1
pantomima	pantomim	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
sovětské	sovětský	k2eAgFnSc6d1
okupaci	okupace	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
byla	být	k5eAaImAgFnS
řada	řada	k1gFnSc1
předních	přední	k2eAgMnPc2d1
pedagogů	pedagog	k1gMnPc2
FAMU	FAMU	kA
(	(	kIx(
<g/>
Evald	Evald	k1gInSc1
Schorm	Schorm	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kachyňa	Kachyňa	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
Pojar	Pojar	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Kundera	Kundera	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnSc1d1
<g/>
)	)	kIx)
nucena	nutit	k5eAaImNgFnS
ukončit	ukončit	k5eAaPmF
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
uplatňovala	uplatňovat	k5eAaImAgFnS
cenzura	cenzura	k1gFnSc1
<g/>
,	,	kIx,
škola	škola	k1gFnSc1
se	se	k3xPyFc4
potýkala	potýkat	k5eAaImAgFnS
s	s	k7c7
řadou	řada	k1gFnSc7
omezení	omezení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
musela	muset	k5eAaImAgFnS
pracovat	pracovat	k5eAaImF
ve	v	k7c6
ztížených	ztížený	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
normalizace	normalizace	k1gFnSc2
DAMU	DAMU	kA
výrazně	výrazně	k6eAd1
stagnovala	stagnovat	k5eAaImAgFnS
a	a	k8xC
v	v	k7c6
původně	původně	k6eAd1
vytyčených	vytyčený	k2eAgInPc6d1
cílech	cíl	k1gInPc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
soustavněji	soustavně	k6eAd2
pokračovat	pokračovat	k5eAaImF
až	až	k9
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
novém	nový	k2eAgNnSc6d1
směřování	směřování	k1gNnSc6
a	a	k8xC
systému	systém	k1gInSc6
studia	studio	k1gNnSc2
začali	začít	k5eAaPmAgMnP
podílet	podílet	k5eAaImF
noví	nový	k2eAgMnPc1d1
pedagogové	pedagog	k1gMnPc1
-	-	kIx~
Jana	Jana	k1gFnSc1
Hlaváčová	Hlaváčová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
Galatíková	Galatíková	k1gFnSc1
<g/>
,	,	kIx,
Boris	Boris	k1gMnSc1
Rösner	Rösner	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Salzmannová	Salzmannová	k1gFnSc1
<g/>
,	,	kIx,
Svatopluk	Svatopluk	k1gMnSc1
Skopal	Skopal	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAMU	FAMU	kA
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
poprvé	poprvé	k6eAd1
pořádala	pořádat	k5eAaImAgFnS
festival	festival	k1gInSc4
FAMUFEST	FAMUFEST	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
studenti	student	k1gMnPc1
aktivně	aktivně	k6eAd1
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
okupační	okupační	k2eAgFnSc2d1
stávky	stávka	k1gFnSc2
na	na	k7c6
DAMU	DAMU	kA
a	a	k8xC
podíleli	podílet	k5eAaImAgMnP
se	se	k3xPyFc4
tak	tak	k9
na	na	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
měly	mít	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
fakulty	fakulta	k1gFnSc2
možnost	možnost	k1gFnSc4
rehabilitovat	rehabilitovat	k5eAaBmF
své	svůj	k3xOyFgInPc4
původní	původní	k2eAgInPc4d1
plány	plán	k1gInPc4
<g/>
,	,	kIx,
prohloubit	prohloubit	k5eAaPmF
demokratické	demokratický	k2eAgFnPc4d1
zásady	zásada	k1gFnPc4
vedení	vedení	k1gNnSc2
školy	škola	k1gFnSc2
i	i	k8xC
přijímacího	přijímací	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nových	nový	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
se	se	k3xPyFc4
také	také	k9
podstatně	podstatně	k6eAd1
rozvinuly	rozvinout	k5eAaPmAgFnP
zahraniční	zahraniční	k2eAgInPc4d1
styky	styk	k1gInPc4
a	a	k8xC
zejména	zejména	k9
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
vysokými	vysoký	k2eAgFnPc7d1
uměleckými	umělecký	k2eAgFnPc7d1
školami	škola	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
FAMU	FAMU	kA
pedagogy	pedagog	k1gMnPc4
stali	stát	k5eAaPmAgMnP
významní	významný	k2eAgMnPc1d1
tvůrci	tvůrce	k1gMnPc1
jako	jako	k9
například	například	k6eAd1
Jiří	Jiří	k1gMnSc1
Menzel	Menzel	k1gMnSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
Sommerová	Sommerová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
Chytilová	Chytilová	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Němec	Němec	k1gMnSc1
nebo	nebo	k8xC
Jan	Jan	k1gMnSc1
Špáta	Špáta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
vznikla	vzniknout	k5eAaPmAgFnS
katedra	katedra	k1gFnSc1
animované	animovaný	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
při	při	k7c6
ní	on	k3xPp3gFnSc6
později	pozdě	k6eAd2
i	i	k8xC
specializace	specializace	k1gFnSc1
na	na	k7c4
multimediální	multimediální	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
a	a	k8xC
počítačovou	počítačový	k2eAgFnSc4d1
animaci	animace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
HAMU	HAMU	kA
samostatným	samostatný	k2eAgInSc7d1
studijním	studijní	k2eAgInSc7d1
oborem	obor	k1gInSc7
stala	stát	k5eAaPmAgFnS
Hudební	hudební	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
na	na	k7c6
škole	škola	k1gFnSc6
vyučována	vyučovat	k5eAaImNgFnS
již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgFnPc1d1
změny	změna	k1gFnPc1
nastaly	nastat	k5eAaPmAgFnP
také	také	k9
na	na	k7c6
divadelní	divadelní	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
plně	plně	k6eAd1
rozvinula	rozvinout	k5eAaPmAgFnS
katedra	katedra	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
pod	pod	k7c4
vedení	vedení	k1gNnSc4
režiséra	režisér	k1gMnSc2
Josefa	Josef	k1gMnSc2
Krofty	Krofta	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
význačných	význačný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
zahájilo	zahájit	k5eAaPmAgNnS
také	také	k9
plno	plno	k1gNnSc1
režisérů	režisér	k1gMnPc2
tzv.	tzv.	kA
studiových	studiový	k2eAgFnPc2d1
divadel	divadlo	k1gNnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Jan	Jan	k1gMnSc1
Schmid	Schmida	k1gFnPc2
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Krobot	Krobot	k?
či	či	k8xC
Jan	Jan	k1gMnSc1
Borna	Born	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sloučením	sloučení	k1gNnSc7
kabinetů	kabinet	k1gInPc2
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
historie	historie	k1gFnSc2
a	a	k8xC
teorie	teorie	k1gFnSc2
fotografie	fotografia	k1gFnSc2
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
na	na	k7c6
FAMU	FAMU	kA
Centrum	centrum	k1gNnSc1
audiovizuálních	audiovizuální	k2eAgNnPc2d1
studií	studio	k1gNnPc2
(	(	kIx(
<g/>
CAS	CAS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízena	zřízen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
intermediální	intermediální	k2eAgFnSc1d1
laboratoř	laboratoř	k1gFnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
VŠUP	VŠUP	kA
a	a	k8xC
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2002	#num#	k4
Akademie	akademie	k1gFnSc1
zřídila	zřídit	k5eAaPmAgFnS
Nakladatelství	nakladatelství	k1gNnSc4
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
NAMU	NAMU	kA
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
informační	informační	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
zajišťující	zajišťující	k2eAgNnSc1d1
nakladatelskou	nakladatelský	k2eAgFnSc7d1
a	a	k8xC
ediční	ediční	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
všech	všecek	k3xTgFnPc2
fakult	fakulta	k1gFnPc2
a	a	k8xC
složek	složka	k1gFnPc2
AMU	AMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
jako	jako	k8xC,k8xS
nástupce	nástupce	k1gMnSc1
Edičního	ediční	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
AMU	AMU	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
na	na	k7c6
základě	základ	k1gInSc6
přiděleného	přidělený	k2eAgInSc2d1
grantu	grant	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
pak	pak	k6eAd1
pod	pod	k7c7
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
spustila	spustit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
provoz	provoz	k1gInSc4
Galerie	galerie	k1gFnSc2
AMU	AMU	kA
(	(	kIx(
<g/>
GAMU	game	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Hartigovský	Hartigovský	k2eAgInSc1d1
palác	palác	k1gInSc1
na	na	k7c6
Malostranském	malostranský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
-	-	kIx~
budova	budova	k1gFnSc1
rektorátu	rektorát	k1gInSc2
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Rektorát	rektorát	k1gInSc1
AMU	AMU	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Hartigovském	Hartigovský	k2eAgInSc6d1
paláci	palác	k1gInSc6
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
propojen	propojit	k5eAaPmNgInS
s	s	k7c7
Lichtenštejnským	lichtenštejnský	k2eAgInSc7d1
palácem	palác	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
sídlí	sídlet	k5eAaImIp3nS
HAMU	HAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Paláci	palác	k1gInSc6
Kokořovských	Kokořovských	k2eAgFnSc1d1
a	a	k8xC
Filmová	filmový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
v	v	k7c6
Paláci	palác	k1gInSc6
Lažanských	Lažanský	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společným	společný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
fakulty	fakulta	k1gFnPc4
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
nově	nově	k6eAd1
zrekonstruovaný	zrekonstruovaný	k2eAgInSc1d1
měšťanský	měšťanský	k2eAgInSc1d1
dům	dům	k1gInSc1
U	u	k7c2
Bílého	bílý	k1gMnSc2
jelena	jelen	k1gMnSc2
na	na	k7c6
horním	horní	k2eAgInSc6d1
konci	konec	k1gInSc6
malostranského	malostranský	k2eAgNnSc2d1
Tržiště	tržiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
AMU	AMU	kA
patří	patřit	k5eAaImIp3nP
také	také	k9
budova	budova	k1gFnSc1
Koleje	kolej	k1gFnSc2
učebního	učební	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
Hradební	hradební	k2eAgFnSc1d1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
pod	pod	k7c7
označením	označení	k1gNnSc7
Kolej	kolej	k1gFnSc1
dr	dr	kA
<g/>
.	.	kIx.
Ivana	Ivan	k1gMnSc2
Sekaniny	Sekanina	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
historickém	historický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ubytování	ubytování	k1gNnPc4
poskytuje	poskytovat	k5eAaImIp3nS
i	i	k9
zrekonstruované	zrekonstruovaný	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
u	u	k7c2
Malostranského	malostranský	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
Pražským	pražský	k2eAgInSc7d1
hradem	hrad	k1gInSc7
na	na	k7c6
Tržišti	tržiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
AMU	AMU	kA
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
také	také	k9
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
v	v	k7c6
Berouně	Beroun	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
poskytuje	poskytovat	k5eAaImIp3nS
několik	několik	k4yIc1
účelových	účelový	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
katedry	katedra	k1gFnSc2
a	a	k8xC
pracoviště	pracoviště	k1gNnSc2
napříč	napříč	k7c7
celou	celý	k2eAgFnSc7d1
AMU	AMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budově	budova	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
ateliérů	ateliér	k1gInPc2
<g/>
,	,	kIx,
badatelny	badatelna	k1gFnSc2
či	či	k8xC
fotografická	fotografický	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UVS	UVS	kA
Beroun	Beroun	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
také	také	k9
jako	jako	k9
centrální	centrální	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
spisových	spisový	k2eAgInPc2d1
<g/>
,	,	kIx,
specializovaných	specializovaný	k2eAgInPc2d1
archivů	archiv	k1gInPc2
<g/>
,	,	kIx,
spisoven	spisovna	k1gFnPc2
<g/>
,	,	kIx,
skladů	sklad	k1gInPc2
fundusu	fundus	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
scénografického	scénografický	k2eAgInSc2d1
depozitáře	depozitář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středisko	středisko	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využívat	využívat	k5eAaImF,k5eAaPmF
k	k	k7c3
nejrůznějším	různý	k2eAgInPc3d3
studijním	studijní	k2eAgInPc3d1
pobytům	pobyt	k1gInPc3
<g/>
,	,	kIx,
soustředěním	soustředění	k1gNnSc7
<g/>
,	,	kIx,
workshopům	workshop	k1gInPc3
atd.	atd.	kA
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
překročení	překročení	k1gNnSc1
ubytovací	ubytovací	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
na	na	k7c6
Koleji	kolej	k1gFnSc6
Hradební	hradební	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
lze	lze	k6eAd1
UVS	UVS	kA
Beroun	Beroun	k1gInSc1
využít	využít	k5eAaPmF
také	také	k9
jako	jako	k9
kolej	kolej	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Poněšice	Poněšice	k1gFnSc2
je	být	k5eAaImIp3nS
využíváno	využívat	k5eAaPmNgNnS,k5eAaImNgNnS
pro	pro	k7c4
soustředění	soustředění	k1gNnSc4
<g/>
,	,	kIx,
rekreační	rekreační	k2eAgInPc4d1
pobyty	pobyt	k1gInPc4
či	či	k8xC
vodácké	vodácký	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kolej	kolej	k1gFnSc1
HradebníDivadlo	HradebníDivadlo	k1gNnSc1
DISK	disk	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
přímo	přímo	k6eAd1
v	v	k7c6
budově	budova	k1gFnSc6
DAMU	DAMU	kA
v	v	k7c6
Karlově	Karlův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
parametry	parametr	k1gInPc7
a	a	k8xC
zázemím	zázemí	k1gNnSc7
odpovídá	odpovídat	k5eAaImIp3nS
menšímu	malý	k2eAgNnSc3d2
repertoárovému	repertoárový	k2eAgNnSc3d1
divadlu	divadlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
praktické	praktický	k2eAgFnSc3d1
výuce	výuka	k1gFnSc3
studentů	student	k1gMnPc2
zejména	zejména	k9
posledních	poslední	k2eAgInPc2d1
ročníků	ročník	k1gInPc2
některých	některý	k3yIgInPc2
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
DAMU	DAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
DISK	disk	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
premiérou	premiéra	k1gFnSc7
Mahenova	Mahenův	k2eAgFnSc1d1
Nasreddina	Nasreddina	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
sídlo	sídlo	k1gNnSc1
na	na	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
přesunulo	přesunout	k5eAaPmAgNnS
z	z	k7c2
dřívějšího	dřívější	k2eAgInSc2d1
Pöttingovského	Pöttingovský	k2eAgInSc2d1
paláce	palác	k1gInSc2
(	(	kIx(
<g/>
Unitaria	Unitarium	k1gNnSc2
<g/>
)	)	kIx)
do	do	k7c2
Divadla	divadlo	k1gNnSc2
v	v	k7c4
Celetné	Celetný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
nynějšího	nynější	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
v	v	k7c6
atriu	atrium	k1gNnSc6
Divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ročníkoví	ročníkový	k2eAgMnPc1d1
pedagogové	pedagog	k1gMnPc1
stojí	stát	k5eAaImIp3nP
v	v	k7c6
čele	čelo	k1gNnSc6
dvou	dva	k4xCgInPc2
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
tvoří	tvořit	k5eAaImIp3nP
studenti	student	k1gMnPc1
4	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
herectví	herectví	k1gNnSc1
Katedry	katedra	k1gFnSc2
činoherního	činoherní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
a	a	k8xC
Katedry	katedra	k1gFnSc2
alternativního	alternativní	k2eAgMnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
inscenací	inscenace	k1gFnPc2
se	se	k3xPyFc4
ujímají	ujímat	k5eAaImIp3nP
studenti	student	k1gMnPc1
příslušného	příslušný	k2eAgInSc2d1
oboru	obor	k1gInSc2
každé	každý	k3xTgFnSc2
z	z	k7c2
kateder	katedra	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
pedagogové	pedagog	k1gMnPc1
ročníku	ročník	k1gInSc2
nebo	nebo	k8xC
externí	externí	k2eAgMnPc1d1
profesionálové	profesionál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpravy	výprava	k1gFnPc4
k	k	k7c3
inscenacím	inscenace	k1gFnPc3
tvoří	tvořit	k5eAaImIp3nP
studenti	student	k1gMnPc1
scénografických	scénografický	k2eAgInPc2d1
oborů	obor	k1gInPc2
DAMU	DAMU	kA
a	a	k8xC
organizaci	organizace	k1gFnSc6
každého	každý	k3xTgInSc2
projektu	projekt	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
tým	tým	k1gInSc4
dvou	dva	k4xCgInPc2
až	až	k9
tří	tři	k4xCgInPc2
studentů	student	k1gMnPc2
Katedry	katedra	k1gFnSc2
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
divadelní	divadelní	k2eAgFnSc2d1
profese	profes	k1gFnSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
fakultním	fakultní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
zajišťovány	zajišťován	k2eAgFnPc1d1
profesionálními	profesionální	k2eAgMnPc7d1
zaměstnanci	zaměstnanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
studentům	student	k1gMnPc3
truhlářská	truhlářský	k2eAgFnSc1d1
<g/>
,	,	kIx,
zámečnická	zámečnický	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
,	,	kIx,
malírna	malírna	k1gFnSc1
<g/>
,	,	kIx,
kašérna	kašérna	k1gFnSc1
a	a	k8xC
krejčovna	krejčovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
akademického	akademický	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
DISKu	disk	k1gInSc6
vyprodukuje	vyprodukovat	k5eAaPmIp3nS
nejméně	málo	k6eAd3
osm	osm	k4xCc4
inscenací	inscenace	k1gFnPc2
<g/>
,	,	kIx,
měsíčně	měsíčně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
odehraje	odehrát	k5eAaPmIp3nS
přes	přes	k7c4
20	#num#	k4
představení	představení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
aktivitám	aktivita	k1gFnPc3
divadla	divadlo	k1gNnSc2
patří	patřit	k5eAaImIp3nP
také	také	k9
zájezdy	zájezd	k1gInPc1
-	-	kIx~
absolvující	absolvující	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
účastní	účastnit	k5eAaImIp3nP
mezinárodních	mezinárodní	k2eAgInPc2d1
studentských	studentský	k2eAgInPc2d1
festivalů	festival	k1gInPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
příležitostně	příležitostně	k6eAd1
i	i	k9
dalších	další	k2eAgFnPc2d1
přehlídek	přehlídka	k1gFnPc2
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
je	být	k5eAaImIp3nS
specializovaným	specializovaný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
FAMU	FAMU	kA
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
úlohou	úloha	k1gFnSc7
je	být	k5eAaImIp3nS
zajištění	zajištění	k1gNnSc1
výroby	výroba	k1gFnSc2
všech	všecek	k3xTgNnPc2
praktických	praktický	k2eAgNnPc2d1
cvičení	cvičení	k1gNnPc2
posluchačů	posluchač	k1gMnPc2
filmové	filmový	k2eAgFnSc2d1
a	a	k8xC
televizní	televizní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
praktických	praktický	k2eAgNnPc2d1
cvičení	cvičení	k1gNnPc2
studentů	student	k1gMnPc2
oboru	obora	k1gFnSc4
fotografie	fotografia	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
Studio	studio	k1gNnSc4
Katedry	katedra	k1gFnSc2
fotografie	fotografia	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
disponuje	disponovat	k5eAaBmIp3nS
vlastními	vlastní	k2eAgFnPc7d1
kapacitami	kapacita	k1gFnPc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
cvičení	cvičení	k1gNnSc2
-	-	kIx~
od	od	k7c2
16	#num#	k4
<g/>
mm	mm	kA
filmových	filmový	k2eAgFnPc2d1
kamer	kamera	k1gFnPc2
<g/>
,	,	kIx,
filmových	filmový	k2eAgFnPc2d1
střižen	střižna	k1gFnPc2
<g/>
,	,	kIx,
dvou	dva	k4xCgInPc2
zvukových	zvukový	k2eAgInPc2d1
ateliérů	ateliér	k1gInPc2
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
moderní	moderní	k2eAgNnPc4d1
postprodukční	postprodukční	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
Studia	studio	k1gNnSc2
FAMU	FAMU	kA
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
modernizace	modernizace	k1gFnSc1
vybavení	vybavení	k1gNnSc2
i	i	k8xC
prostor	prostora	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Inspirace	inspirace	k1gFnSc2
je	být	k5eAaImIp3nS
scénickým	scénický	k2eAgInSc7d1
ateliérem	ateliér	k1gInSc7
hudební	hudební	k2eAgFnSc2d1
a	a	k8xC
taneční	taneční	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
vybudovaným	vybudovaný	k2eAgNnPc3d1
ve	v	k7c6
sklepních	sklepní	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
Lichtenštejnského	lichtenštejnský	k2eAgInSc2d1
paláce	palác	k1gInSc2
na	na	k7c6
Malostranském	malostranský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
pro	pro	k7c4
účely	účel	k1gInPc4
prezentace	prezentace	k1gFnSc2
výsledků	výsledek	k1gInPc2
hudební	hudební	k2eAgFnSc2d1
a	a	k8xC
taneční	taneční	k2eAgFnSc2d1
divadelní	divadelní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
má	mít	k5eAaImIp3nS
jednoduché	jednoduchý	k2eAgNnSc1d1
pódium	pódium	k1gNnSc1
bez	bez	k7c2
orchestřiště	orchestřiště	k1gNnSc2
<g/>
,	,	kIx,
světelnou	světelný	k2eAgFnSc4d1
a	a	k8xC
zvukovou	zvukový	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
kapacita	kapacita	k1gFnSc1
sálu	sál	k1gInSc2
jen	jen	k9
45	#num#	k4
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nP
spíše	spíše	k9
hrám	hra	k1gFnPc3
a	a	k8xC
koncertům	koncert	k1gInPc3
komorního	komorní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelně	pravidelně	k6eAd1
zde	zde	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
studentská	studentský	k2eAgNnPc1d1
představení	představení	k1gNnPc1
Katedry	katedra	k1gFnSc2
pantomimy	pantomima	k1gFnSc2
(	(	kIx(
<g/>
středy	středa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konají	konat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
koncerty	koncert	k1gInPc4
oddělení	oddělení	k1gNnSc2
jazzové	jazzový	k2eAgFnSc2d1
interpretace	interpretace	k1gFnSc2
HAMU	HAMU	kA
(	(	kIx(
<g/>
čtvrtky	čtvrtka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Katedra	katedra	k1gFnSc1
zpěvu	zpěv	k1gInSc2
a	a	k8xC
operní	operní	k2eAgFnSc1d1
režie	režie	k1gFnSc1
zde	zde	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
svá	svůj	k3xOyFgNnPc4
komorní	komorní	k2eAgNnPc4d1
operní	operní	k2eAgNnPc4d1
představení	představení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
pronajímáno	pronajímán	k2eAgNnSc1d1
pro	pro	k7c4
komorní	komorní	k2eAgInPc4d1
projekty	projekt	k1gInPc4
divadel	divadlo	k1gNnPc2
malých	malý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
scén	scéna	k1gFnPc2
každoročního	každoroční	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Fringe	Fring	k1gFnSc2
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
(	(	kIx(
<g/>
každé	každý	k3xTgNnSc4
úterý	úterý	k1gNnSc4
<g/>
)	)	kIx)
zde	zde	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
hudebně-dramatickými	hudebně-dramatický	k2eAgFnPc7d1
produkcemi	produkce	k1gFnPc7
společnost	společnost	k1gFnSc1
Okolostola	Okolostola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fakulty	fakulta	k1gFnPc1
</s>
<s>
Budova	budova	k1gFnSc1
Divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
s	s	k7c7
divadlem	divadlo	k1gNnSc7
DISK	disk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Lichtenštejnský	lichtenštejnský	k2eAgInSc1d1
palác	palác	k1gInSc1
na	na	k7c6
Malostranském	malostranský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
hudební	hudební	k2eAgFnSc2d1
a	a	k8xC
taneční	taneční	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
nabízí	nabízet	k5eAaImIp3nS
studium	studium	k1gNnSc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
oborech	obor	k1gInPc6
divadelní	divadelní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
:	:	kIx,
herectví	herectví	k1gNnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc1
<g/>
,	,	kIx,
scénografie	scénografie	k1gFnSc1
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc1
a	a	k8xC
kritiky	kritika	k1gFnSc2
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
autorské	autorský	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
dramatické	dramatický	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
činoherního	činoherní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
tak	tak	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
divadla	divadlo	k1gNnSc2
alternativního	alternativní	k2eAgNnSc2d1
i	i	k8xC
loutkového	loutkový	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
bakalářském	bakalářský	k2eAgMnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgInSc6d1
i	i	k8xC
doktorském	doktorský	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mezi	mezi	k7c7
pedagogy	pedagog	k1gMnPc7
patří	patřit	k5eAaImIp3nP
významné	významný	k2eAgFnPc1d1
české	český	k2eAgFnPc1d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnPc1d1
divadelní	divadelní	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
DAMU	DAMU	kA
působí	působit	k5eAaImIp3nP
v	v	k7c6
nezávislých	závislý	k2eNgFnPc6d1
divadelních	divadelní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
,	,	kIx,
uplatňují	uplatňovat	k5eAaImIp3nP
se	se	k3xPyFc4
jako	jako	k9
divadelníci	divadelník	k1gMnPc1
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgMnPc1d1
kritici	kritik	k1gMnPc1
<g/>
,	,	kIx,
producenti	producent	k1gMnPc1
<g/>
,	,	kIx,
pedagogové	pedagog	k1gMnPc1
a	a	k8xC
v	v	k7c6
nejširším	široký	k2eAgInSc6d3
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
autoři	autor	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
filmových	filmový	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
FAMU	FAMU	kA
opustili	opustit	k5eAaPmAgMnP
mladí	mladý	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
později	pozdě	k6eAd2
vešli	vejít	k5eAaPmAgMnP
ve	v	k7c4
známost	známost	k1gFnSc4
jako	jako	k8xS,k8xC
Česká	český	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
představovala	představovat	k5eAaImAgFnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejvýznamnější	významný	k2eAgInSc1d3
přínos	přínos	k1gInSc1
světové	světový	k2eAgFnSc2d1
kinematografii	kinematografie	k1gFnSc3
a	a	k8xC
Československu	Československo	k1gNnSc3
vynesla	vynést	k5eAaPmAgFnS
–	–	k?
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
–	–	k?
dva	dva	k4xCgInPc4
Oscary	Oscar	k1gInPc4
za	za	k7c4
nejlepší	dobrý	k2eAgInPc4d3
zahraniční	zahraniční	k2eAgInPc4d1
filmy	film	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
FAMU	FAMU	kA
na	na	k7c6
seznamu	seznam	k1gInSc6
15	#num#	k4
nejlepších	dobrý	k2eAgFnPc2d3
mezinárodních	mezinárodní	k2eAgFnPc2d1
filmových	filmový	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
každoročně	každoročně	k6eAd1
zveřejňuje	zveřejňovat	k5eAaImIp3nS
časopis	časopis	k1gInSc4
The	The	k1gMnPc2
Hollywood	Hollywood	k1gInSc1
Reporter	Reportra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
je	být	k5eAaImIp3nS
zakládajícím	zakládající	k2eAgMnSc7d1
členem	člen	k1gMnSc7
mezinárodní	mezinárodní	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
filmových	filmový	k2eAgFnPc2d1
a	a	k8xC
televizních	televizní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
CILECT	CILECT	kA
<g/>
,	,	kIx,
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
uměleckých	umělecký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
(	(	kIx(
<g/>
ELIA	ELIA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Evropského	evropský	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
filmových	filmový	k2eAgFnPc2d1
a	a	k8xC
televizních	televizní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
(	(	kIx(
<g/>
GEECT	GEECT	kA
<g/>
)	)	kIx)
a	a	k8xC
Evropské	evropský	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
institucionálním	institucionální	k2eAgMnSc7d1
členem	člen	k1gMnSc7
České	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
pro	pro	k7c4
filmová	filmový	k2eAgNnPc4d1
studia	studio	k1gNnPc4
(	(	kIx(
<g/>
CEFS	CEFS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAMU	FAMU	kA
má	mít	k5eAaImIp3nS
šest	šest	k4xCc1
akreditovaných	akreditovaný	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
a	a	k8xC
jedenáct	jedenáct	k4xCc1
kateder	katedra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vyučují	vyučovat	k5eAaImIp3nP
12	#num#	k4
oborů	obor	k1gInPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
a	a	k8xC
4	#num#	k4
obory	obor	k1gInPc7
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
HAMU	HAMU	kA
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
vzdělání	vzdělání	k1gNnSc1
v	v	k7c6
programech	program	k1gInPc6
hudebního	hudební	k2eAgMnSc2d1
a	a	k8xC
tanečního	taneční	k2eAgNnSc2d1
umění	umění	k1gNnSc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
třech	tři	k4xCgInPc6
stupních	stupeň	k1gInPc6
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
bakalářském	bakalářský	k2eAgNnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgNnSc6d1
<g/>
,	,	kIx,
doktorském	doktorský	k2eAgNnSc6d1
<g/>
)	)	kIx)
v	v	k7c6
českém	český	k2eAgInSc6d1
i	i	k8xC
anglickém	anglický	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
systematicky	systematicky	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
domácí	domácí	k2eAgFnSc7d1
profesionální	profesionální	k2eAgFnSc7d1
uměleckou	umělecký	k2eAgFnSc7d1
sférou	sféra	k1gFnSc7
<g/>
,	,	kIx,
jejími	její	k3xOp3gMnPc7
partnery	partner	k1gMnPc7
jsou	být	k5eAaImIp3nP
jak	jak	k6eAd1
přední	přední	k2eAgInPc1d1
orchestry	orchestr	k1gInPc1
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
filharmonie	filharmonie	k1gFnSc1
<g/>
,	,	kIx,
PKF	PKF	kA
<g/>
,	,	kIx,
FOK	FOK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
festivaly	festival	k1gInPc1
(	(	kIx(
<g/>
Pražské	pražský	k2eAgNnSc1d1
jaro	jaro	k1gNnSc1
<g/>
,	,	kIx,
Dvořákova	Dvořákův	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Smetanova	Smetanův	k2eAgFnSc1d1
Litomyšl	Litomyšl	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
divadla	divadlo	k1gNnSc2
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
J.	J.	kA
K.	K.	kA
Tyla	Tyl	k1gMnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
NoD	NoD	k1gFnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Ponec	Ponec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
média	médium	k1gNnSc2
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejné	veřejný	k2eAgFnPc4d1
prezentace	prezentace	k1gFnPc4
tvůrčí	tvůrčí	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
studentů	student	k1gMnPc2
se	se	k3xPyFc4
uskutečňují	uskutečňovat	k5eAaImIp3nP
ve	v	k7c6
fakultních	fakultní	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
jsou	být	k5eAaImIp3nP
Sál	sál	k1gInSc4
Martinů	Martinů	k1gFnSc2
a	a	k8xC
Galerie	galerie	k1gFnSc2
HAMU	HAMU	kA
<g/>
,	,	kIx,
pro	pro	k7c4
scénická	scénický	k2eAgNnPc4d1
vystoupení	vystoupení	k1gNnPc4
slouží	sloužit	k5eAaImIp3nS
Divadlo	divadlo	k1gNnSc1
Inspirace	inspirace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HAMU	HAMU	kA
je	být	k5eAaImIp3nS
pořadatelem	pořadatel	k1gMnSc7
mnoha	mnoho	k4c2
studentských	studentský	k2eAgMnPc2d1
i	i	k8xC
jiných	jiný	k2eAgMnPc2d1
koncertů	koncert	k1gInPc2
a	a	k8xC
akcí	akce	k1gFnPc2
(	(	kIx(
<g/>
orchestrální	orchestrální	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
Ti	ty	k3xPp2nSc3
nejlepší	dobrý	k2eAgInSc1d3
<g/>
,	,	kIx,
festival	festival	k1gInSc1
Skrznaskrz	skrznaskrz	k6eAd1
<g/>
,	,	kIx,
My	my	k3xPp1nPc1
Mime	mim	k1gMnSc5
<g/>
,	,	kIx,
Jazzové	jazzový	k2eAgNnSc1d1
nádvoří	nádvoří	k1gNnSc1
<g/>
,	,	kIx,
Prague	Prague	k1gNnSc1
Shakuhashi	Shakuhash	k1gFnSc2
Festival	festival	k1gInSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
vlastním	vlastní	k2eAgNnSc7d1
zvukovým	zvukový	k2eAgNnSc7d1
a	a	k8xC
nahrávacím	nahrávací	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
<g/>
,	,	kIx,
hudební	hudební	k2eAgFnSc7d1
a	a	k8xC
taneční	taneční	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
s	s	k7c7
fonotékou	fonotéka	k1gFnSc7
<g/>
,	,	kIx,
cvičebnami	cvičebna	k1gFnPc7
a	a	k8xC
výcvikovými	výcvikový	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
v	v	k7c6
Berouně	Beroun	k1gInSc6
a	a	k8xC
Poněšicích	Poněšice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Omylem	omylem	k6eAd1
je	být	k5eAaImIp3nS
asociování	asociování	k1gNnSc4
zkratky	zkratka	k1gFnSc2
JAMU	jam	k1gInSc2
s	s	k7c7
některou	některý	k3yIgFnSc7
z	z	k7c2
fakult	fakulta	k1gFnPc2
této	tento	k3xDgFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedená	uvedený	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
patří	patřit	k5eAaImIp3nS
samostatné	samostatný	k2eAgFnSc3d1
Janáčkově	Janáčkův	k2eAgFnSc3d1
akademii	akademie	k1gFnSc3
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
AMU	AMU	kA
(	(	kIx(
<g/>
GAMU	game	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
AMU	AMU	kA
sídlí	sídlet	k5eAaImIp3nS
na	na	k7c6
Malostranském	malostranský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galerijní	galerijní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
zázemí	zázemí	k1gNnSc1
pro	pro	k7c4
prezentaci	prezentace	k1gFnSc4
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
nejmladší	mladý	k2eAgFnSc4d3
generaci	generace	k1gFnSc4
tvůrců	tvůrce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součást	součást	k1gFnSc1
výstavního	výstavní	k2eAgInSc2d1
programu	program	k1gInSc2
přirozeně	přirozeně	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
klauzurní	klauzurní	k2eAgFnPc1d1
a	a	k8xC
absolventské	absolventský	k2eAgFnPc1d1
výstavy	výstava	k1gFnPc1
z	z	k7c2
AMU	AMU	kA
a	a	k8xC
vybrané	vybraný	k2eAgInPc1d1
studentské	studentský	k2eAgInPc1d1
projekty	projekt	k1gInPc1
z	z	k7c2
dalších	další	k2eAgFnPc2d1
uměleckých	umělecký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
kurátorsky	kurátorsky	k6eAd1
koncipované	koncipovaný	k2eAgFnPc1d1
výstavy	výstava	k1gFnPc1
již	již	k6eAd1
etablovaných	etablovaný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
českých	český	k2eAgMnPc2d1
i	i	k8xC
zahraničních	zahraniční	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
dramaturgie	dramaturgie	k1gFnSc1
GAMU	game	k1gInSc2
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
oblast	oblast	k1gFnSc4
nových	nový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
překračování	překračování	k1gNnSc6
hranic	hranice	k1gFnPc2
tradičních	tradiční	k2eAgFnPc2d1
uměleckých	umělecký	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
program	program	k1gInSc1
doplněn	doplnit	k5eAaPmNgInS
i	i	k9
o	o	k7c4
jednorázové	jednorázový	k2eAgFnPc4d1
akce	akce	k1gFnPc4
hudebního	hudební	k2eAgNnSc2d1
<g/>
,	,	kIx,
sound-artového	sound-artový	k2eAgMnSc2d1
a	a	k8xC
multimediálního	multimediální	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
(	(	kIx(
<g/>
NAMU	NAMU	kA
<g/>
)	)	kIx)
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
literaturu	literatura	k1gFnSc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
filmu	film	k1gInSc2
<g/>
,	,	kIx,
televize	televize	k1gFnSc2
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnSc2
<g/>
,	,	kIx,
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
hudby	hudba	k1gFnSc2
a	a	k8xC
tance	tanec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
odborné	odborný	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
od	od	k7c2
monografií	monografie	k1gFnPc2
<g/>
,	,	kIx,
přes	přes	k7c4
tematické	tematický	k2eAgInPc4d1
sborníky	sborník	k1gInPc4
až	až	k9
po	po	k7c4
studijní	studijní	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vydávání	vydávání	k1gNnSc2
publikací	publikace	k1gFnPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
NAMU	NAMU	kA
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
tiskárně	tiskárna	k1gFnSc6
a	a	k8xC
knihařské	knihařský	k2eAgFnSc3d1
dílně	dílna	k1gFnSc3
tiskové	tiskový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
fakult	fakulta	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
AMU	AMU	kA
<g/>
.	.	kIx.
</s>
<s>
Akce	akce	k1gFnSc1
AMU	AMU	kA
</s>
<s>
Festival	festival	k1gInSc1
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
již	již	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
v	v	k7c6
podzimních	podzimní	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
je	být	k5eAaImIp3nS
festival	festival	k1gInSc4
tanečních	taneční	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgNnPc7
vystupují	vystupovat	k5eAaImIp3nP
i	i	k9
studenti	student	k1gMnPc1
z	z	k7c2
Katedry	katedra	k1gFnSc2
nonverbálního	nonverbální	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
či	či	k8xC
z	z	k7c2
Katedry	katedra	k1gFnSc2
tance	tanec	k1gInSc2
HAMU	HAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Inspirace	inspirace	k1gFnSc2
či	či	k8xC
v	v	k7c6
Divadle	divadlo	k1gNnSc6
DISK	disk	k1gInSc1
na	na	k7c6
DAMU	DAMU	kA
<g/>
.	.	kIx.
</s>
<s>
Jazzové	jazzový	k2eAgNnSc1d1
nádvoří	nádvoří	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
sérii	série	k1gFnSc4
tří	tři	k4xCgInPc2
velkých	velký	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
iniciované	iniciovaný	k2eAgFnSc2d1
dvěma	dva	k4xCgFnPc7
českými	český	k2eAgFnPc7d1
jazzovými	jazzový	k2eAgFnPc7d1
katedrami	katedra	k1gFnPc7
-	-	kIx~
pražskou	pražský	k2eAgFnSc7d1
HAMU	HAMU	kA
a	a	k8xC
brněnskou	brněnský	k2eAgFnSc7d1
JAMU	jam	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Festival	festival	k1gInSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
již	již	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
vždy	vždy	k6eAd1
v	v	k7c6
červnu	červen	k1gInSc6
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
den	den	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jazzové	jazzový	k2eAgNnSc1d1
nádvoří	nádvoří	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FAMUFEST	FAMUFEST	kA
je	být	k5eAaImIp3nS
multižánrový	multižánrový	k2eAgInSc1d1
filmový	filmový	k2eAgInSc1d1
studentský	studentský	k2eAgInSc1d1
festival	festival	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
na	na	k7c4
podzim	podzim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
záměrem	záměr	k1gInSc7
je	být	k5eAaImIp3nS
představit	představit	k5eAaPmF
talenty	talent	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
budou	být	k5eAaImBp3nP
spoluvytvářet	spoluvytvářet	k5eAaImF
tvář	tvář	k1gFnSc4
české	český	k2eAgFnSc2d1
kinematografie	kinematografie	k1gFnSc2
<g/>
,	,	kIx,
televizní	televizní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnSc2
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
již	již	k6eAd1
35	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
tohoto	tento	k3xDgInSc2
festivalu	festival	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Festival	festival	k1gInSc1
Proces	proces	k1gInSc1
pořádá	pořádat	k5eAaImIp3nS
dvakrát	dvakrát	k6eAd1
ročně	ročně	k6eAd1
(	(	kIx(
<g/>
vždy	vždy	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
klauzur	klauzura	k1gFnPc2
<g/>
)	)	kIx)
Katedra	katedra	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
DAMU	DAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
festivalu	festival	k1gInSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
prací	práce	k1gFnPc2
studentů	student	k1gMnPc2
všech	všecek	k3xTgInPc2
oborů	obor	k1gInPc2
a	a	k8xC
ročníků	ročník	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
náhledů	náhled	k1gInPc2
do	do	k7c2
otevřených	otevřený	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
workshopů	workshop	k1gInPc2
s	s	k7c7
hostujícími	hostující	k2eAgMnPc7d1
pedagogy	pedagog	k1gMnPc7
<g/>
,	,	kIx,
přes	přes	k7c4
semestrální	semestrální	k2eAgNnSc4d1
zadání	zadání	k1gNnSc4
až	až	k9
po	po	k7c4
absolventské	absolventský	k2eAgFnPc4d1
inscenace	inscenace	k1gFnPc4
studentů	student	k1gMnPc2
bakalářského	bakalářský	k2eAgMnSc2d1
a	a	k8xC
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Festival	festival	k1gInSc1
Děti-Výchova-Divadlo	Děti-Výchova-Divadlo	k1gNnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
dětskou	dětský	k2eAgFnSc4d1
a	a	k8xC
studentskou	studentský	k2eAgFnSc4d1
jevištní	jevištní	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
a	a	k8xC
přednes	přednes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
rok	rok	k1gInSc4
je	být	k5eAaImIp3nS
festival	festival	k1gInSc1
pořádán	pořádán	k2eAgInSc1d1
studenty	student	k1gMnPc7
Katedry	katedra	k1gFnSc2
výchovné	výchovný	k2eAgFnSc2d1
dramatiky	dramatika	k1gFnSc2
DAMU	DAMU	kA
společně	společně	k6eAd1
s	s	k7c7
jejich	jejich	k3xOp3gMnPc7
pedagogy	pedagog	k1gMnPc7
a	a	k8xC
ve	v	k7c4
spolupráci	spolupráce	k1gFnSc4
se	s	k7c7
Sdružením	sdružení	k1gNnSc7
pro	pro	k7c4
výchovnou	výchovný	k2eAgFnSc4d1
dramatiku	dramatika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
ročník	ročník	k1gInSc1
festivalu	festival	k1gInSc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
z	z	k7c2
přehlídky	přehlídka	k1gFnSc2
3	#num#	k4
<g/>
D	D	kA
<g/>
:	:	kIx,
děti-drama-divadlo	děti-drama-divadlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
festivalu	festival	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zhlédnout	zhlédnout	k5eAaPmF
představení	představení	k1gNnPc4
dětských	dětský	k2eAgMnPc2d1
a	a	k8xC
studentských	studentský	k2eAgMnPc2d1
divadelních	divadelní	k2eAgMnPc2d1
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
souborů	soubor	k1gInPc2
zabývajících	zabývající	k2eAgInPc2d1
se	se	k3xPyFc4
divadlem	divadlo	k1gNnSc7
ve	v	k7c6
výchově	výchova	k1gFnSc6
<g/>
,	,	kIx,
vystoupení	vystoupení	k1gNnSc1
recitátorů	recitátor	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ukázky	ukázka	k1gFnPc4
z	z	k7c2
tvorby	tvorba	k1gFnSc2
současných	současný	k2eAgMnPc2d1
studentů	student	k1gMnPc2
katedry	katedra	k1gFnSc2
výchovné	výchovný	k2eAgFnSc2d1
dramatiky	dramatika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Festival	festival	k1gInSc1
Zlomvaz	Zlomvaz	k1gInSc1
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc1d1
studentský	studentský	k2eAgInSc1d1
divadelní	divadelní	k2eAgInSc1d1
festival	festival	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
v	v	k7c6
květnu	květen	k1gInSc6
pořádají	pořádat	k5eAaImIp3nP
studenti	student	k1gMnPc1
2	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
bakalářského	bakalářský	k2eAgInSc2d1
programu	program	k1gInSc2
Katedry	katedra	k1gFnSc2
produkce	produkce	k1gFnSc2
DAMU	DAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Festival	festival	k1gInSc1
je	být	k5eAaImIp3nS
přehlídkou	přehlídka	k1gFnSc7
studentských	studentský	k2eAgFnPc2d1
absolventských	absolventský	k2eAgFnPc2d1
inscenací	inscenace	k1gFnPc2
z	z	k7c2
JAMU	jam	k1gInSc2
<g/>
,	,	kIx,
bratislavské	bratislavský	k2eAgFnPc4d1
VŠMÚ	VŠMÚ	kA
a	a	k8xC
zahraničních	zahraniční	k2eAgFnPc2d1
divadelních	divadelní	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
AMU	AMU	kA
párty	párty	k1gFnSc1
je	být	k5eAaImIp3nS
akce	akce	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
pedagogy	pedagog	k1gMnPc4
<g/>
,	,	kIx,
zaměstnance	zaměstnanec	k1gMnPc4
i	i	k8xC
absolventy	absolvent	k1gMnPc4
napříč	napříč	k7c7
fakultami	fakulta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konala	konat	k5eAaImAgNnP
se	se	k3xPyFc4
třikrát	třikrát	k6eAd1
vždy	vždy	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
akademického	akademický	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
květen	květen	k1gInSc1
nebo	nebo	k8xC
červen	červen	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
ve	v	k7c6
Vile	vila	k1gFnSc6
Štvanici	Štvanice	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Tiskárně	tiskárna	k1gFnSc6
na	na	k7c6
vzduchu	vzduch	k1gInSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
opět	opět	k6eAd1
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c4
Štvanici	Štvanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMU	AMU	kA
párty	párty	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
70	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
vzniku	vznik	k1gInSc2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
události	událost	k1gFnSc2
je	být	k5eAaImIp3nS
většinou	většina	k1gFnSc7
složen	složit	k5eAaPmNgInS
z	z	k7c2
vystoupení	vystoupení	k1gNnSc2
hudebních	hudební	k2eAgFnPc2d1
kapel	kapela	k1gFnPc2
stávajících	stávající	k2eAgMnPc2d1
studentů	student	k1gMnPc2
nebo	nebo	k8xC
absolventů	absolvent	k1gMnPc2
AMU	AMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMU	AMU	kA
párty	párty	k1gFnSc2
ve	v	k7c6
Vile	vila	k1gFnSc6
Štvanici	Štvanice	k1gFnSc6
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
AMU	AMU	kA
Dodatek	dodatek	k1gInSc1
k	k	k7c3
diplomu	diplom	k1gInSc3
(	(	kIx(
<g/>
DS	DS	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
získala	získat	k5eAaPmAgFnS
AMU	AMU	kA
Diploma	Diploma	k1gFnSc1
Supplement	Supplement	k1gMnSc1
Label	Label	k1gMnSc1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2013	#num#	k4
byl	být	k5eAaImAgInS
DSL	DSL	kA
na	na	k7c6
základě	základ	k1gInSc6
žádosti	žádost	k1gFnSc2
úspěšně	úspěšně	k6eAd1
obnoven	obnovit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
2014	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
získala	získat	k5eAaPmAgFnS
AMU	AMU	kA
ERASMUS	ERASMUS	kA
Charter	Charter	k1gInSc4
a	a	k8xC
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
programu	program	k1gInSc2
ERASMUS	ERASMUS	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
AMU	AMU	kA
jako	jako	k8xS,k8xC
celek	celek	k1gInSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
evropských	evropský	k2eAgFnPc2d1
univerzitních	univerzitní	k2eAgFnPc2d1
a	a	k8xC
uměleckých	umělecký	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
s	s	k7c7
přesahem	přesah	k1gInSc7
i	i	k9
mimo	mimo	k7c4
evropský	evropský	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
EUA	EUA	kA
a	a	k8xC
ELIA	ELIA	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
se	se	k3xPyFc4
zapojuje	zapojovat	k5eAaImIp3nS
do	do	k7c2
evropské	evropský	k2eAgFnSc2d1
vzdělávací	vzdělávací	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
účast	účast	k1gFnSc1
na	na	k7c6
konferenci	konference	k1gFnSc6
EUA	EUA	kA
o	o	k7c6
kvalitě	kvalita	k1gFnSc6
v	v	k7c6
Göteborgu	Göteborg	k1gInSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
fakulty	fakulta	k1gFnSc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
členy	člen	k1gInPc1
oborových	oborový	k2eAgFnPc2d1
mezinárodních	mezinárodní	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
všech	všecek	k3xTgFnPc6
fakultách	fakulta	k1gFnPc6
je	být	k5eAaImIp3nS
kladen	klást	k5eAaImNgInS
důraz	důraz	k1gInSc1
na	na	k7c6
rozšiřování	rozšiřování	k1gNnSc6
a	a	k8xC
zkvalitňování	zkvalitňování	k1gNnSc6
nabídky	nabídka	k1gFnSc2
anglických	anglický	k2eAgInPc2d1
akreditovaných	akreditovaný	k2eAgInPc2d1
programů	program	k1gInPc2
i	i	k8xC
krátkodobých	krátkodobý	k2eAgInPc2d1
neakreditovaných	akreditovaný	k2eNgInPc2d1
kurzů	kurz	k1gInPc2
a	a	k8xC
letních	letní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
na	na	k7c4
AMU	AMU	kA
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
umělecké	umělecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
poslání	poslání	k1gNnSc4
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecké	vědecký	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
jsou	být	k5eAaImIp3nP
rozvíjeny	rozvíjet	k5eAaImNgFnP
na	na	k7c6
všech	všecek	k3xTgFnPc6
třech	tři	k4xCgFnPc6
fakultách	fakulta	k1gFnPc6
AMU	AMU	kA
v	v	k7c6
individuálních	individuální	k2eAgInPc6d1
<g/>
,	,	kIx,
katederních	katederní	k2eAgInPc6d1
i	i	k8xC
interdisciplinárních	interdisciplinární	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
výzkumnými	výzkumný	k2eAgInPc7d1
záměry	záměr	k1gInPc7
dlouhodobě	dlouhodobě	k6eAd1
převažuje	převažovat	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
uměnovědy	uměnověda	k1gFnSc2
<g/>
,	,	kIx,
metodicky	metodicky	k6eAd1
opřený	opřený	k2eAgMnSc1d1
nejčastěji	často	k6eAd3
o	o	k7c4
historický	historický	k2eAgInSc4d1
<g/>
,	,	kIx,
antropologický	antropologický	k2eAgInSc4d1
či	či	k8xC
psychologický	psychologický	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc7
dominantními	dominantní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
jsou	být	k5eAaImIp3nP
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
tanec	tanec	k1gInSc1
<g/>
,	,	kIx,
film	film	k1gInSc1
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
<g/>
,	,	kIx,
restaurování	restaurování	k1gNnSc1
a	a	k8xC
moderní	moderní	k2eAgFnPc1d1
audiovizuální	audiovizuální	k2eAgFnPc1d1
formy	forma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikovaný	aplikovaný	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
AMU	AMU	kA
soustředěn	soustředit	k5eAaPmNgInS
na	na	k7c6
specializovaných	specializovaný	k2eAgNnPc6d1
pracovištích	pracoviště	k1gNnPc6
na	na	k7c6
poli	pole	k1gNnSc6
akustiky	akustika	k1gFnSc2
<g/>
,	,	kIx,
optiky	optika	k1gFnSc2
a	a	k8xC
restaurování	restaurování	k1gNnSc1
i	i	k8xC
archivace	archivace	k1gFnSc1
audiovizuálního	audiovizuální	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
stále	stále	k6eAd1
důrazněji	důrazně	k6eAd2
prosazuje	prosazovat	k5eAaImIp3nS
výzkum	výzkum	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
umění	umění	k1gNnSc2
či	či	k8xC
výzkum	výzkum	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c4
propojení	propojení	k1gNnSc4
teorie	teorie	k1gFnSc2
s	s	k7c7
praxí	praxe	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Rektoři	rektor	k1gMnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
rektorů	rektor	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Ladislav	Ladislav	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Frejka	Frejka	k1gFnSc1
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
Martin	Martin	k1gMnSc1
Brousil	brousit	k5eAaImAgMnS
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Budíková-Jeremiášová	Budíková-Jeremiášová	k1gFnSc1
</s>
<s>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Martínek	Martínek	k1gMnSc1
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
:	:	kIx,
Ilja	Ilja	k1gMnSc1
Bojanovský	Bojanovský	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1991	#num#	k4
–	–	k?
1992	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Urbánek	Urbánek	k1gMnSc1
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Vostrý	Vostrý	k2eAgMnSc1d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
–	–	k?
1999	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Malina	Malina	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Toperczer	Toperczer	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
–	–	k?
2013	#num#	k4
<g/>
:	:	kIx,
Ivo	Ivo	k1gMnSc1
Mathé	Mathý	k2eAgFnSc2d1
</s>
<s>
2013	#num#	k4
–	–	k?
2021	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Hančil	Hančil	k1gMnSc1
</s>
<s>
od	od	k7c2
2021	#num#	k4
<g/>
:	:	kIx,
Ingeborg	Ingeborg	k1gInSc1
Radok	Radok	k1gInSc1
Žádná	žádný	k3yNgNnPc4
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Posluchačka	posluchačka	k1gFnSc1
katedry	katedra	k1gFnSc2
režie	režie	k1gFnSc1
FAMU	FAMU	kA
Zuzana	Zuzana	k1gFnSc1
Kirschnerová-Špidlová	Kirschnerová-Špidlový	k2eAgFnSc1d1
zvítězila	zvítězit	k5eAaPmAgFnS
s	s	k7c7
filmem	film	k1gInSc7
Bába	bába	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Cinéfondation	Cinéfondation	k1gInSc1
na	na	k7c6
Mezinárodním	mezinárodní	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
Cannes	Cannes	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
:	:	kIx,
Dechové	dechový	k2eAgNnSc1d1
trio	trio	k1gNnSc1
TRIFOGLIO	TRIFOGLIO	kA
(	(	kIx(
<g/>
Jana	Jan	k1gMnSc2
Kopicová	Kopicový	k2eAgFnSc1d1
–	–	k?
hoboj	hoboj	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Černohouzová	Černohouzová	k1gFnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
Denisa	Denisa	k1gFnSc1
Propilková	Propilková	k1gFnSc1
–	–	k?
fagot	fagot	k1gInSc1
<g/>
)	)	kIx)
získalo	získat	k5eAaPmAgNnS
3	#num#	k4
<g/>
.	.	kIx.
cenu	cena	k1gFnSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
komorních	komorní	k2eAgInPc2d1
souborů	soubor	k1gInPc2
na	na	k7c6
soutěži	soutěž	k1gFnSc6
Chieri	Chier	k1gFnSc2
International	International	k1gFnSc2
Competition	Competition	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
trio	trio	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
titul	titul	k1gInSc4
laureáta	laureát	k1gMnSc2
a	a	k8xC
absolutního	absolutní	k2eAgMnSc2d1
vítěze	vítěz	k1gMnSc2
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
Karla	Karel	k1gMnSc2
Ditterse	Ditterse	k1gFnSc2
z	z	k7c2
Dittersdorfu	Dittersdorf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
Projekt	projekt	k1gInSc1
Korekce	korekce	k1gFnSc2
českého	český	k2eAgInSc2d1
souboru	soubor	k1gInSc2
VerTeDance	VerTeDance	k1gFnSc2
získal	získat	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Herald	Herald	k1gMnSc1
Angel	Angela	k1gFnPc2
edinburského	edinburský	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Fringe	Fring	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
projektu	projekt	k1gInSc6
se	se	k3xPyFc4
autorsky	autorsky	k6eAd1
podíleli	podílet	k5eAaImAgMnP
mladí	mladý	k2eAgMnPc1d1
pedagogové	pedagog	k1gMnPc1
AMU	AMU	kA
Tereza	Tereza	k1gFnSc1
Ondrová	Ondrová	k1gFnSc1
(	(	kIx(
<g/>
HAMU	HAMU	kA
<g/>
)	)	kIx)
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Havelka	Havelka	k1gMnSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
Inscenace	inscenace	k1gFnSc1
Rituální	rituální	k2eAgFnSc1d1
vražda	vražda	k1gFnSc1
Gorge	Gorge	k1gFnSc4
Mastromase	Mastromasa	k1gFnSc3
pražského	pražský	k2eAgNnSc2d1
Divadla	divadlo	k1gNnSc2
DISK	disk	k1gInSc1
v	v	k7c6
režii	režie	k1gFnSc6
Adama	Adam	k1gMnSc2
Svozila	Svozil	k1gMnSc2
získala	získat	k5eAaPmAgFnS
Cenu	cena	k1gFnSc4
Marka	Marek	k1gMnSc2
Ravenhilla	Ravenhill	k1gMnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
českou	český	k2eAgFnSc4d1
divadelní	divadelní	k2eAgFnSc4d1
inscenaci	inscenace	k1gFnSc4
nového	nový	k2eAgInSc2d1
textu	text	k1gInSc2
za	za	k7c4
rok	rok	k1gInSc4
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Čerstvá	čerstvý	k2eAgFnSc1d1
absolventka	absolventka	k1gFnSc1
HAMU	HAMU	kA
<g/>
,	,	kIx,
hornistka	hornistka	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Javůrková	Javůrková	k1gFnSc1
<g/>
,	,	kIx,
uspěla	uspět	k5eAaPmAgFnS
v	v	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
interpretační	interpretační	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
ARD	ARD	kA
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
a	a	k8xC
získala	získat	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
první	první	k4xOgFnSc2
nebylo	být	k5eNaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Hraný	hraný	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Furiant	furiant	k1gInSc1
režiséra	režisér	k1gMnSc2
a	a	k8xC
scenáristy	scenárista	k1gMnSc2
Ondřeje	Ondřej	k1gMnSc2
Hudečka	Hudeček	k1gMnSc2
získal	získat	k5eAaPmAgMnS
zvláštní	zvláštní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
poroty	porota	k1gFnSc2
pro	pro	k7c4
krátkometrážní	krátkometrážní	k2eAgInSc4d1
film	film	k1gInSc4
za	za	k7c4
režii	režie	k1gFnSc4
na	na	k7c4
Sundance	Sundance	k1gFnPc4
Film	film	k1gInSc4
Festivalu	festival	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
také	také	k9
ocenění	ocenění	k1gNnSc4
na	na	k7c6
festivalu	festival	k1gInSc6
Palm	Palma	k1gFnPc2
Springs	Springsa	k1gFnPc2
International	International	k1gMnSc2
ShortFest	ShortFest	k1gFnSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
studentský	studentský	k2eAgInSc1d1
hraný	hraný	k2eAgInSc1d1
film	film	k1gInSc1
do	do	k7c2
patnácti	patnáct	k4xCc2
minut	minuta	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
studentského	studentský	k2eAgMnSc4d1
Oscara	Oscar	k1gMnSc4
Americké	americký	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
Natálie	Natálie	k1gFnSc1
Schwamová	Schwamový	k2eAgFnSc1d1
vyhrála	vyhrát	k5eAaPmAgFnS
klavírní	klavírní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
YAMAHA	yamaha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
Tvůrčí	tvůrčí	k2eAgInSc1d1
tým	tým	k1gInSc1
souboru	soubor	k1gInSc2
Pomezí	pomezí	k1gNnSc2
<g/>
,	,	kIx,
složený	složený	k2eAgInSc1d1
z	z	k7c2
absolventů	absolvent	k1gMnPc2
DAMU	DAMU	kA
(	(	kIx(
<g/>
Kateřina	Kateřina	k1gFnSc1
Součková	Součková	k1gFnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Brychta	Brychta	k1gMnSc1
a	a	k8xC
Štěpán	Štěpán	k1gMnSc1
Tretiag	Tretiag	k1gMnSc1
<g/>
)	)	kIx)
získal	získat	k5eAaPmAgInS
Cenu	cena	k1gFnSc4
Divadelních	divadelní	k2eAgFnPc2d1
novin	novina	k1gFnPc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Alternativní	alternativní	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
:	:	kIx,
Dokument	dokument	k1gInSc1
Mečiar	Mečiar	k1gMnSc1
režisérky	režisérka	k1gFnSc2
a	a	k8xC
studentky	studentka	k1gFnSc2
FAMU	FAMU	kA
Terezy	Tereza	k1gFnSc2
Nvotové	Nvotová	k1gFnSc2
byl	být	k5eAaImAgInS
oceněn	ocenit	k5eAaPmNgInS
HLAVNÍ	hlavní	k2eAgFnSc7d1
CENOU	cena	k1gFnSc7
TRILOBIT	trilobit	k1gMnSc1
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významní	významný	k2eAgMnPc1d1
absolventi	absolvent	k1gMnPc1
</s>
<s>
Absolventi	absolvent	k1gMnPc1
DAMU	DAMU	kA
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
<g/>
,	,	kIx,
Taťjana	Taťjana	k1gFnSc1
Medvecká	Medvecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Trojan	Trojan	k1gMnSc1
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
Chýlková	Chýlková	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Holubová	Holubová	k1gFnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Klepl	klepnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Dyk	Dyk	k?
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Burešová	Burešová	k1gFnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
David	David	k1gMnSc1
Pařízek	Pařízek	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Špinar	Špinar	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kolečko	kolečko	k1gNnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Vostrý	Vostrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Vyskočil	Vyskočil	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Lukeš	Lukeš	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Poul	Poul	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Forman	Forman	k1gMnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
Tomicová	Tomicová	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Vondráček	Vondráček	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Krofta	Krofta	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Havelka	Havelka	k1gMnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
Konečná	Konečná	k1gFnSc1
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Pražák	Pražák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gMnSc1
Tempír	Tempír	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Chocholoušek	chocholoušek	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Kejkrtová-Měřičková	Kejkrtová-Měřičková	k1gFnSc1
<g/>
,	,	kIx,
Renata	Renata	k1gFnSc1
Drössler	Drössler	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Absolventi	absolvent	k1gMnPc1
FAMU	FAMU	kA
<g/>
:	:	kIx,
Miloš	Miloš	k1gMnSc1
Forman	Forman	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Passer	Passer	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Menzel	Menzel	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Svěrák	Svěrák	k1gMnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
Chytilová	Chytilová	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Němec	Němec	k1gMnSc1
<g/>
,	,	kIx,
Fero	Fero	k1gMnSc1
Fenič	Fenič	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Kundera	Kundera	k1gFnSc1
<g/>
,	,	kIx,
Agnieszka	Agnieszka	k1gFnSc1
Holland	Holland	k1gInSc1
<g/>
,	,	kIx,
Emir	Emir	k1gMnSc1
Kusturica	Kusturica	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Bubeníček	Bubeníček	k1gMnSc1
<g/>
,	,	kIx,
Grímur	Grímur	k1gMnSc1
Hákonarson	Hákonarson	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Helena	Helena	k1gFnSc1
Třeštíková	Třeštíková	k1gFnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Jasný	jasný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
Kallista	Kallista	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Cudlín	Cudlín	k1gInSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Hřebejk	Hřebejk	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Michálek	Michálek	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
Najbrt	Najbrt	k1gInSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Podskalský	podskalský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Irena	Irena	k1gFnSc1
Pavlásková	Pavlásková	k1gFnSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Polesný	polesný	k1gMnSc1
<g/>
,	,	kIx,
Oskar	Oskar	k1gMnSc1
Reif	Reif	k1gMnSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
Sommerová	Sommerová	k1gFnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
(	(	kIx(
<g/>
Woody	Wood	k1gInPc1
<g/>
)	)	kIx)
Vašulka	Vašulka	k1gFnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Duda	Duda	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Bouček	Bouček	k1gMnSc1
<g/>
,	,	kIx,
Čestmír	Čestmír	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Marhoul	Marhoul	k1gInSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Mathé	Mathý	k2eAgInPc4d1
a	a	k8xC
další	další	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Absolventi	absolvent	k1gMnPc1
HAMU	HAMU	kA
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Bělohlávek	Bělohlávek	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
Plachetka	plachetka	k1gFnSc1
<g/>
,	,	kIx,
Oliver	Oliver	k1gInSc1
Dohnányi	Dohnány	k1gFnSc2
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Hrůša	Hrůša	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Kahánek	kahánek	k1gInSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Netopil	topit	k5eNaImAgMnS
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Srnka	srnka	k1gFnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Pešek	Pešek	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Jonášová	Jonášová	k1gFnSc1
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Plocek	Plocka	k1gFnPc2
<g/>
,	,	kIx,
Boris	Boris	k1gMnSc1
Hybner	Hybner	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Děpoltová	Děpoltová	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Gabriela	Gabriela	k1gFnSc1
Demeterová	Demeterová	k1gFnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Kusnjer	Kusnjer	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Suk	Suk	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Bořkovec	Bořkovec	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Šporlc	Šporlc	k1gFnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Eben	eben	k1gInSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Zuska	Zuska	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Hudeček	Hudeček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Bárta	Bárta	k1gMnSc1
<g/>
,	,	kIx,
Nora	Nora	k1gFnSc1
Grumlíková	Grumlíková	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Chuchro	Chuchra	k1gFnSc5
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Kohout	Kohout	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Pauer	Pauer	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Čestné	čestný	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1996	#num#	k4
</s>
<s>
Miloš	Miloš	k1gMnSc1
Forman	Forman	k1gMnSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1998	#num#	k4
</s>
<s>
sir	sir	k1gMnSc1
Charles	Charles	k1gMnSc1
Mackerras	Mackerras	k1gMnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Husa	Husa	k1gMnSc1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2000	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kylián	Kylián	k1gMnSc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
</s>
<s>
Otomar	Otomar	k1gMnSc1
Krejča	Krejča	k1gMnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Švankmajer	Švankmajer	k1gMnSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2003	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Suk	Suk	k1gMnSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
</s>
<s>
Radovan	Radovan	k1gMnSc1
Lukavský	Lukavský	k2eAgMnSc1d1
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2005	#num#	k4
</s>
<s>
Oleg	Oleg	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
Tabakov	Tabakov	k1gInSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
</s>
<s>
J.V.	J.V.	k?
<g/>
Norodom	Norodom	k1gInSc1
Sihamoni	Sihamon	k1gMnPc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Růžičková	Růžičková	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
</s>
<s>
Woody	Wooda	k1gMnSc2
Bohuslav	Bohuslav	k1gMnSc1
Vašulka	Vašulka	k1gFnSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Borisovič	Borisovič	k1gMnSc1
Norštejn	Norštejn	k1gMnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Smoček	Smoček	k1gMnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bělohlávek	Bělohlávek	k1gMnSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
</s>
<s>
Agnieszka	Agnieszka	k1gFnSc1
Holland	Hollanda	k1gFnPc2
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
a	a	k8xC
hospodaření	hospodaření	k1gNnSc1
AMU	AMU	kA
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
,	,	kIx,
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
AMU	AMU	kA
<g/>
.	.	kIx.
www.amu.cz	www.amu.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Učební	učební	k2eAgFnSc1d1
<g/>
,	,	kIx,
výcvikové	výcvikový	k2eAgNnSc1d1
a	a	k8xC
ubytovací	ubytovací	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Beroun	Beroun	k1gInSc1
<g/>
.	.	kIx.
www.amu.cz	www.amu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Poněšice	Poněšice	k1gFnSc1
<g/>
.	.	kIx.
www.amu.cz	www.amu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DISK	disk	k1gInSc1
-	-	kIx~
O	o	k7c6
Disku	disk	k1gInSc6
<g/>
.	.	kIx.
www.divadlodisk.cz	www.divadlodisk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
Studiu	studio	k1gNnSc6
FAMU	FAMU	kA
<g/>
.	.	kIx.
www.famu.cz	www.famu.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Divadlo	divadlo	k1gNnSc1
Inspirace	inspirace	k1gFnSc1
<g/>
.	.	kIx.
www.hamu.cz	www.hamu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vše	všechen	k3xTgNnSc1
o	o	k7c6
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
www.damu.cz	www.damu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vše	všechen	k3xTgNnSc1
o	o	k7c6
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
www.hamu.cz	www.hamu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Galerie	galerie	k1gFnSc1
AMU	AMU	kA
<g/>
.	.	kIx.
www.amu.cz	www.amu.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MĚSÍC	měsíc	k1gInSc1
<g/>
,	,	kIx,
Oliver	Oliver	k1gMnSc1
Staša	Staša	k1gMnSc1
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAMUFEST	FAMUFEST	kA
—	—	k?
ABOUT	ABOUT	kA
<g/>
.	.	kIx.
www.famufest.cz	www.famufest.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Festival	festival	k1gInSc1
Proces	proces	k1gInSc1
<g/>
.	.	kIx.
www.damu.cz	www.damu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Festival	festival	k1gInSc1
DVD	DVD	kA
-	-	kIx~
Děti-Výchova-Divadlo	Děti-Výchova-Divadlo	k1gNnSc1
<g/>
.	.	kIx.
www.damu.cz	www.damu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AMU	AMU	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akad	Akad	k1gInSc1
71	#num#	k4
Seiten	Seitno	k1gNnPc2
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7331	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7331	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
266973972	#num#	k4
↑	↑	k?
FRANC	Franc	k1gMnSc1
<g/>
,	,	kIx,
MARTIN	Martin	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
vydání	vydání	k1gNnPc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
382	#num#	k4
pages	pages	k1gMnSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7331	#num#	k4
<g/>
-	-	kIx~
<g/>
422	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7331	#num#	k4
<g/>
-	-	kIx~
<g/>
422	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
1034548521	#num#	k4
↑	↑	k?
Úspěchy	úspěch	k1gInPc1
a	a	k8xC
ocenění	ocenění	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.amu.cz	www.amu.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Pedagogická	pedagogický	k2eAgFnSc1d1
pracoviště	pracoviště	k1gNnSc4
</s>
<s>
Katedra	katedra	k1gFnSc1
cizích	cizí	k2eAgMnPc2d1
jazyků	jazyk	k1gMnPc2
AMU	AMU	kA
•	•	k?
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
AMU	AMU	kA
Informační	informační	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
</s>
<s>
Knihovna	knihovna	k1gFnSc1
AMU	AMU	kA
•	•	k?
Počítačové	počítačový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
AMU	AMU	kA
•	•	k?
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
Jednotky	jednotka	k1gFnSc2
fakult	fakulta	k1gFnPc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
•	•	k?
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
Účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Kolej	kolej	k1gFnSc1
a	a	k8xC
učební	učební	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
•	•	k?
Ubytovací	ubytovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
•	•	k?
Učební	učební	k2eAgMnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c4
Vltavou-Poněšice	Vltavou-Poněšic	k1gMnSc4
•	•	k?
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Beroun	Beroun	k1gInSc1
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709005	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0395	#num#	k4
0562	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
87110176	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
158366424	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
87110176	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Film	film	k1gInSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
