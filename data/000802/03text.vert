<s>
Hyundai	Hyundai	k1gNnSc1	Hyundai
(	(	kIx(	(
<g/>
korejsky	korejsky	k6eAd1	korejsky
현	현	k?	현
Hjonde	Hjond	k1gMnSc5	Hjond
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jihokorejský	jihokorejský	k2eAgInSc1d1	jihokorejský
podnik	podnik	k1gInSc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
výrobní	výrobní	k2eAgMnSc1d1	výrobní
naplní	naplnit	k5eAaPmIp3nS	naplnit
je	být	k5eAaImIp3nS	být
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Firmu	firma	k1gFnSc4	firma
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Čong	Čong	k1gMnSc1	Čong
Ču-jong	Čuong	k1gMnSc1	Ču-jong
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Chung	Chung	k1gInSc1	Chung
Ju	ju	k0	ju
Yung	Yunga	k1gFnPc2	Yunga
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
opravárenský	opravárenský	k2eAgInSc1d1	opravárenský
podnik	podnik	k1gInSc1	podnik
pro	pro	k7c4	pro
nákladní	nákladní	k2eAgInPc4d1	nákladní
a	a	k8xC	a
osobní	osobní	k2eAgInPc4d1	osobní
vozy	vůz	k1gInPc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
divize	divize	k1gFnSc1	divize
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
automobily	automobil	k1gInPc4	automobil
Hyundai	Hyunda	k1gFnSc2	Hyunda
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
další	další	k2eAgFnSc1d1	další
divize	divize	k1gFnSc1	divize
Hyundai	Hyunda	k1gFnSc2	Hyunda
Heavy	Heava	k1gFnSc2	Heava
Industries	Industries	k1gMnSc1	Industries
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
společnost	společnost	k1gFnSc4	společnost
Hyundai	Hyundai	k1gNnSc7	Hyundai
jihokorejského	jihokorejský	k2eAgMnSc2d1	jihokorejský
výrobce	výrobce	k1gMnSc2	výrobce
automobilů	automobil	k1gInPc2	automobil
Kia	Kia	k1gMnSc1	Kia
Motors	Motors	k1gInSc1	Motors
a	a	k8xC	a
Hyundai	Hyundai	k1gNnSc1	Hyundai
Motor	motor	k1gInSc4	motor
Company	Compana	k1gFnSc2	Compana
byla	být	k5eAaImAgFnS	být
přetvořena	přetvořit	k5eAaPmNgFnS	přetvořit
na	na	k7c6	na
Hyundai	Hyunda	k1gInSc6	Hyunda
Kia	Kia	k1gFnSc2	Kia
Automotive	Automotiv	k1gInSc5	Automotiv
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Hyundai	Hyundai	k6eAd1	Hyundai
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
nadále	nadále	k6eAd1	nadále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
výhradně	výhradně	k6eAd1	výhradně
tuto	tento	k3xDgFnSc4	tento
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
vozů	vůz	k1gInPc2	vůz
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Hyundai	Hyundai	k6eAd1	Hyundai
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kii	Kii	k1gFnSc7	Kii
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
obrovský	obrovský	k2eAgInSc1d1	obrovský
pokrok	pokrok	k1gInSc1	pokrok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xS	jako
žádná	žádný	k3yNgFnSc1	žádný
jiná	jiný	k2eAgFnSc1d1	jiná
automobilka	automobilka	k1gFnSc1	automobilka
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
Hyundai	Hyunda	k1gFnSc2	Hyunda
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
4	[number]	k4	4
645	[number]	k4	645
776	[number]	k4	776
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
vozy	vůz	k1gInPc7	vůz
5	[number]	k4	5
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
automobilovým	automobilový	k2eAgMnSc7d1	automobilový
výrobcem	výrobce	k1gMnSc7	výrobce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
továrna	továrna	k1gFnSc1	továrna
Hyundai	Hyunda	k1gFnSc2	Hyunda
Motor	motor	k1gInSc1	motor
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Czech	Czech	k1gInSc1	Czech
v	v	k7c6	v
Nošovicích	Nošovice	k1gFnPc6	Nošovice
<g/>
.	.	kIx.	.
</s>
<s>
i	i	k9	i
<g/>
10	[number]	k4	10
i	i	k9	i
<g/>
20	[number]	k4	20
i	i	k9	i
<g/>
30	[number]	k4	30
Elantra	Elantrum	k1gNnSc2	Elantrum
i	i	k9	i
<g/>
40	[number]	k4	40
ix	ix	k?	ix
<g/>
20	[number]	k4	20
ix	ix	k?	ix
<g/>
35	[number]	k4	35
Santa	Santa	k1gFnSc1	Santa
Fe	Fe	k1gFnSc1	Fe
ix	ix	k?	ix
<g/>
55	[number]	k4	55
H-1	H-1	k1gFnSc6	H-1
Genesis	Genesis	k1gFnSc4	Genesis
Veloster	Veloster	k1gMnSc1	Veloster
Hyundai	Hyunda	k1gFnSc2	Hyunda
Grandeur	Grandeur	k1gMnSc1	Grandeur
<g/>
(	(	kIx(	(
<g/>
Azera	Azer	k1gInSc2	Azer
<g/>
)	)	kIx)	)
Hyundai	Hyunda	k1gFnSc2	Hyunda
Equus	Equus	k1gMnSc1	Equus
Atos	Atosa	k1gFnPc2	Atosa
Getz	Getz	k1gMnSc1	Getz
Pony	pony	k1gMnSc1	pony
Accent	Accent	k1gMnSc1	Accent
Lantra	Lantra	k1gFnSc1	Lantra
Elantra	Elantr	k1gMnSc2	Elantr
Sonata	Sonat	k1gMnSc2	Sonat
Grandeur	Grandeur	k1gMnSc1	Grandeur
Equus	Equus	k1gMnSc1	Equus
Matrix	Matrix	k1gInSc4	Matrix
Trajet	Trajet	k1gMnSc1	Trajet
Tucson	Tucson	k1gMnSc1	Tucson
Galloper	Galloper	k1gMnSc1	Galloper
Terracan	Terracana	k1gFnPc2	Terracana
Coupé	Coupý	k2eAgNnSc4d1	Coupé
S-Coupé	S-Coupý	k2eAgNnSc4d1	S-Coupý
Slovo	slovo	k1gNnSc4	slovo
hjonde	hjond	k1gInSc5	hjond
znamená	znamenat	k5eAaImIp3nS	znamenat
korejsky	korejsky	k6eAd1	korejsky
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgMnSc1d1	moderní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
Hyundai	Hyunda	k1gFnSc2	Hyunda
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996-1999	[number]	k4	1996-1999
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
díly	díl	k1gInPc4	díl
z	z	k7c2	z
vozu	vůz	k1gInSc2	vůz
Accent	Accent	k1gInSc4	Accent
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
vozidel	vozidlo	k1gNnPc2	vozidlo
Tatra	Tatra	k1gFnSc1	Tatra
Beta	beta	k1gNnSc4	beta
<g/>
.	.	kIx.	.
</s>
