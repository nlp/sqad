<p>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ס	ס	k?	ס
ה	ה	k?	ה
ה	ה	k?	ה
<g/>
,	,	kIx,	,
Sochnut	Sochnut	k2eAgMnSc1d1	Sochnut
ha-Halal	ha-Halat	k5eAaImAgMnS	ha-Halat
ha-Jisraelit	ha-Jisraelit	k5eAaPmF	ha-Jisraelit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelská	izraelský	k2eAgFnSc1d1	izraelská
státní	státní	k2eAgFnSc1d1	státní
agentura	agentura	k1gFnSc1	agentura
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
Israel	Israel	k1gInSc1	Israel
Space	Space	k1gMnSc2	Space
Agency	Agenca	k1gMnSc2	Agenca
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
zkratka	zkratka	k1gFnSc1	zkratka
organizace	organizace	k1gFnSc2	organizace
ISA	ISA	kA	ISA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Agentura	agentura	k1gFnSc1	agentura
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
koordninátorka	koordninátorka	k1gFnSc1	koordninátorka
kosmického	kosmický	k2eAgInSc2d1	kosmický
programu	program	k1gInSc2	program
Izraele	Izrael	k1gInSc2	Izrael
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
Národní	národní	k2eAgInSc4d1	národní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
National	National	k1gFnSc4	National
Committee	Committe	k1gFnSc2	Committe
for	forum	k1gNnPc2	forum
Space	Space	k1gMnSc1	Space
Research	Research	k1gMnSc1	Research
<g/>
,	,	kIx,	,
NCSR	NCSR	kA	NCSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
vysláním	vyslání	k1gNnSc7	vyslání
prvního	první	k4xOgInSc2	první
satelitu	satelit	k1gInSc2	satelit
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
Ofek	Ofeka	k1gFnPc2	Ofeka
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
tragický	tragický	k2eAgInSc4d1	tragický
zánik	zánik	k1gInSc4	zánik
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
palubě	paluba	k1gFnSc6	paluba
zahynul	zahynout	k5eAaPmAgMnS	zahynout
první	první	k4xOgMnSc1	první
izraelský	izraelský	k2eAgMnSc1d1	izraelský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Ilan	Ilana	k1gFnPc2	Ilana
Ramon	Ramona	k1gFnPc2	Ramona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současný	současný	k2eAgInSc1d1	současný
program	program	k1gInSc1	program
==	==	k?	==
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
vysílání	vysílání	k1gNnSc4	vysílání
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
komerčních	komerční	k2eAgFnPc2d1	komerční
družic	družice	k1gFnPc2	družice
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
vojenské	vojenský	k2eAgFnPc1d1	vojenská
a	a	k8xC	a
komerční	komerční	k2eAgInPc1d1	komerční
projekty	projekt	k1gInPc1	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
plánuje	plánovat	k5eAaImIp3nS	plánovat
vynesení	vynesení	k1gNnSc4	vynesení
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
TAUVEX	TAUVEX	kA	TAUVEX
(	(	kIx(	(
<g/>
Tel	tel	kA	tel
Aviv	Aviv	k1gMnSc1	Aviv
University	universita	k1gFnSc2	universita
Ultraviolet	Ultraviolet	k1gInSc1	Ultraviolet
Explorer	Explorer	k1gInSc1	Explorer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
připravován	připravovat	k5eAaImNgInS	připravovat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Indíí	Indí	k1gFnSc7	Indí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zázemí	zázemí	k1gNnSc2	zázemí
==	==	k?	==
</s>
</p>
<p>
<s>
ISA	ISA	kA	ISA
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc1d1	dobré
zázemí	zázemí	k1gNnSc1	zázemí
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vyspělého	vyspělý	k2eAgInSc2d1	vyspělý
izraelského	izraelský	k2eAgInSc2d1	izraelský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
tradičním	tradiční	k2eAgMnSc7d1	tradiční
silným	silný	k2eAgMnSc7d1	silný
spojencem	spojenec	k1gMnSc7	spojenec
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ISA	ISA	kA	ISA
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
také	také	k9	také
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
rozpočet	rozpočet	k1gInSc1	rozpočet
agentury	agentura	k1gFnSc2	agentura
je	být	k5eAaImIp3nS	být
pouhý	pouhý	k2eAgInSc1d1	pouhý
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
částka	částka	k1gFnSc1	částka
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
Venus	Venus	k1gInSc4	Venus
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
získá	získat	k5eAaPmIp3nS	získat
izraelský	izraelský	k2eAgInSc1d1	izraelský
vojenský	vojenský	k2eAgInSc1d1	vojenský
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgInPc1d1	komerční
projekty	projekt	k1gInPc1	projekt
jsou	být	k5eAaImIp3nP	být
financované	financovaný	k2eAgInPc1d1	financovaný
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
rozpočtů	rozpočet	k1gInPc2	rozpočet
<g/>
.	.	kIx.	.
</s>
</p>
