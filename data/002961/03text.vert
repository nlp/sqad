<s>
Obec	obec	k1gFnSc1	obec
Dolní	dolní	k2eAgFnSc1d1	dolní
Branná	branný	k2eAgFnSc1d1	Branná
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hennersdorf	Hennersdorf	k1gInSc1	Hennersdorf
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
7,91	[number]	k4	7,91
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
346	[number]	k4	346
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
necelých	celý	k2eNgInPc2d1	necelý
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
295	[number]	k4	295
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Barokní	barokní	k2eAgFnSc1d1	barokní
kaplička	kaplička	k1gFnSc1	kaplička
sv.	sv.	kA	sv.
Josefa	Josefa	k1gFnSc1	Josefa
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
Sloup	sloup	k1gInSc1	sloup
s	s	k7c7	s
krucifixem	krucifix	k1gInSc7	krucifix
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
vesnice	vesnice	k1gFnSc2	vesnice
Venkovský	venkovský	k2eAgInSc4d1	venkovský
dům	dům	k1gInSc4	dům
čp.	čp.	k?	čp.
179	[number]	k4	179
Vila	vila	k1gFnSc1	vila
čp.	čp.	k?	čp.
208	[number]	k4	208
Bakov	Bakov	k1gInSc1	Bakov
Rafanda	Rafando	k1gNnSc2	Rafando
Bachtíkov	Bachtíkov	k1gInSc4	Bachtíkov
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dolní	dolní	k2eAgFnSc2d1	dolní
Branná	branný	k2eAgFnSc1d1	Branná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
