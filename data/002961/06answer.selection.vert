<s>
Obec	obec	k1gFnSc1	obec
Dolní	dolní	k2eAgFnSc1d1	dolní
Branná	branný	k2eAgFnSc1d1	Branná
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hennersdorf	Hennersdorf	k1gInSc1	Hennersdorf
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
7,91	[number]	k4	7,91
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
