<p>
<s>
Réunion	Réunion	k1gInSc1	Réunion
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
zámořský	zámořský	k2eAgInSc4d1	zámořský
departement	departement	k1gInSc4	departement
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zámořský	zámořský	k2eAgInSc1d1	zámořský
region	region	k1gInSc1	region
Francie	Francie	k1gFnSc2	Francie
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvzdálenějších	vzdálený	k2eAgInPc2d3	nejvzdálenější
regionů	region	k1gInPc2	region
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Réunion	Réunion	k1gInSc1	Réunion
znamená	znamenat	k5eAaImIp3nS	znamenat
francouzsky	francouzsky	k6eAd1	francouzsky
"	"	kIx"	"
<g/>
shledání	shledání	k1gNnSc1	shledání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
zámořské	zámořský	k2eAgFnSc2d1	zámořská
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Oceánii	Oceánie	k1gFnSc6	Oceánie
i	i	k8xC	i
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Réunion	Réunion	k1gInSc1	Réunion
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mauriciem	Mauricium	k1gNnSc7	Mauricium
a	a	k8xC	a
Rodriguesem	Rodrigues	k1gInSc7	Rodrigues
tvoří	tvořit	k5eAaImIp3nS	tvořit
souostroví	souostroví	k1gNnSc1	souostroví
Maskarény	Maskaréna	k1gFnSc2	Maskaréna
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
východně	východně	k6eAd1	východně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Piton	Piton	k1gMnSc1	Piton
des	des	k1gNnSc2	des
Neiges	Neiges	k1gMnSc1	Neiges
měřící	měřící	k2eAgFnSc1d1	měřící
3	[number]	k4	3
069	[number]	k4	069
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
2	[number]	k4	2
631	[number]	k4	631
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
činná	činný	k2eAgFnSc1d1	činná
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Piton	Piton	k1gMnSc1	Piton
de	de	k?	de
la	la	k1gNnSc4	la
Fournaise	Fournaise	k1gFnSc2	Fournaise
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejaktivnější	aktivní	k2eAgFnSc7d3	nejaktivnější
sopkou	sopka	k1gFnSc7	sopka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
ostrova	ostrov	k1gInSc2	ostrov
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
této	tento	k3xDgFnSc2	tento
nejlidnatější	lidnatý	k2eAgFnSc2d3	nejlidnatější
zámořské	zámořský	k2eAgFnSc2d1	zámořská
části	část	k1gFnSc2	část
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
Saint-Denis	Saint-Denis	k1gFnSc1	Saint-Denis
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
velké	velký	k2eAgNnSc1d1	velké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Saint-Pierre	Saint-Pierr	k1gMnSc5	Saint-Pierr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
afrických	africký	k2eAgMnPc2d1	africký
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
francouzských	francouzský	k2eAgMnPc2d1	francouzský
osadníků	osadník	k1gMnPc2	osadník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
příchodem	příchod	k1gInSc7	příchod
nebyl	být	k5eNaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
trvale	trvale	k6eAd1	trvale
obydlen	obydlet	k5eAaPmNgInS	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
pouze	pouze	k6eAd1	pouze
novodobé	novodobý	k2eAgMnPc4d1	novodobý
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
(	(	kIx(	(
<g/>
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
<g/>
)	)	kIx)	)
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
starousedlíky	starousedlík	k1gMnPc7	starousedlík
<g/>
.	.	kIx.	.
</s>
<s>
Mísením	mísení	k1gNnSc7	mísení
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
současná	současný	k2eAgFnSc1d1	současná
mulatská	mulatský	k2eAgFnSc1d1	mulatská
populace	populace	k1gFnSc1	populace
používající	používající	k2eAgFnSc1d1	používající
při	při	k7c6	při
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
styku	styk	k1gInSc6	styk
tzv.	tzv.	kA	tzv.
kreolizovanou	kreolizovaný	k2eAgFnSc4d1	kreolizovaná
francouzštinu	francouzština	k1gFnSc4	francouzština
-	-	kIx~	-
réunionštinu	réunionština	k1gFnSc4	réunionština
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
z	z	k7c2	z
normanské	normanský	k2eAgFnSc2d1	normanská
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
zejména	zejména	k9	zejména
malgaštiny	malgaština	k1gFnSc2	malgaština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
převládá	převládat	k5eAaImIp3nS	převládat
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
formálně	formálně	k6eAd1	formálně
zabrala	zabrat	k5eAaPmAgFnS	zabrat
Réunion	Réunion	k1gInSc4	Réunion
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Île-de-Bourbon	Îlee-Bourbon	k1gNnSc1	Île-de-Bourbon
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
jej	on	k3xPp3gMnSc4	on
začala	začít	k5eAaPmAgFnS	začít
osídlovat	osídlovat	k5eAaImF	osídlovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Compagnie	Compagnie	k1gFnSc2	Compagnie
des	des	k1gNnSc2	des
Indes	Indes	k1gMnSc1	Indes
Orientales	Orientales	k1gMnSc1	Orientales
(	(	kIx(	(
<g/>
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
přejmenované	přejmenovaný	k2eAgFnSc2d1	přejmenovaná
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
Compagnie	Compagnie	k1gFnPc4	Compagnie
des	des	k1gNnPc2	des
Indes	Indesa	k1gFnPc2	Indesa
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
Indií	Indie	k1gFnSc7	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1718	[number]	k4	1718
jí	jíst	k5eAaImIp3nS	jíst
fakticky	fakticky	k6eAd1	fakticky
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
sousední	sousední	k2eAgInSc1d1	sousední
Mauricius	Mauricius	k1gInSc1	Mauricius
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Île-de-Maurice	Îlee-Maurika	k1gFnSc3	Île-de-Maurika
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1722	[number]	k4	1722
Île-de-France	Îlee-France	k1gFnSc1	Île-de-France
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
i	i	k9	i
všechny	všechen	k3xTgInPc4	všechen
další	další	k2eAgInPc4d1	další
neobydlené	obydlený	k2eNgInPc4d1	neobydlený
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
Seychely	Seychely	k1gFnPc1	Seychely
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Îles	Îles	k1gInSc1	Îles
de	de	k?	de
La	la	k1gNnSc2	la
Bourdonnais	Bourdonnais	k1gFnPc2	Bourdonnais
<g/>
,	,	kIx,	,
1756	[number]	k4	1756
zabrány	zabrána	k1gFnPc1	zabrána
jako	jako	k8xS	jako
Îles	Îles	k1gInSc1	Îles
de	de	k?	de
Séchelles	Séchelles	k1gInSc1	Séchelles
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Čagoské	Čagoský	k2eAgInPc1d1	Čagoský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Britské	britský	k2eAgNnSc1d1	Britské
indickooceánské	indickooceánský	k2eAgNnSc1d1	indickooceánský
území	území	k1gNnSc1	území
<g/>
)	)	kIx)	)
atp.	atp.	kA	atp.
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
ostrovy	ostrov	k1gInPc1	ostrov
kromě	kromě	k7c2	kromě
Réunionu	Réunion	k1gInSc2	Réunion
ale	ale	k8xC	ale
ztratila	ztratit	k5eAaPmAgFnS	ztratit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
mauricijského	mauricijský	k2eAgNnSc2d1	mauricijský
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zámořským	zámořský	k2eAgInSc7d1	zámořský
departementem	departement	k1gInSc7	departement
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
i	i	k9	i
zámořským	zámořský	k2eAgInSc7d1	zámořský
regionem	region	k1gInSc7	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
ostrova	ostrov	k1gInSc2	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Réunion	Réunion	k1gInSc1	Réunion
je	být	k5eAaImIp3nS	být
francouzským	francouzský	k2eAgInSc7d1	francouzský
departementem	departement	k1gInSc7	departement
(	(	kIx(	(
<g/>
č.	č.	k?	č.
974	[number]	k4	974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
už	už	k6eAd1	už
dřív	dříve	k6eAd2	dříve
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
stavem	stav	k1gInSc7	stav
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
4	[number]	k4	4
arrondisementů	arrondisement	k1gInPc2	arrondisement
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
25	[number]	k4	25
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
24	[number]	k4	24
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Arrondisementy	Arrondisement	k1gInPc1	Arrondisement
a	a	k8xC	a
kantony	kanton	k1gInPc1	kanton
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k6eAd1	především
k	k	k7c3	k
administraci	administrace	k1gFnSc3	administrace
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
statistice	statistika	k1gFnSc3	statistika
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc4	žádný
zastupitelsvo	zastupitelsvo	k1gNnSc4	zastupitelsvo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
obce	obec	k1gFnPc1	obec
jsou	být	k5eAaImIp3nP	být
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
s	s	k7c7	s
voleným	volený	k2eAgNnSc7d1	volené
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
(	(	kIx(	(
<g/>
obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavou	hlava	k1gFnSc7	hlava
Réunionu	Réunion	k1gInSc2	Réunion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
plnoprávnou	plnoprávný	k2eAgFnSc7d1	plnoprávná
součástí	součást	k1gFnSc7	součást
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
představitel	představitel	k1gMnSc1	představitel
každého	každý	k3xTgInSc2	každý
francouzského	francouzský	k2eAgInSc2d1	francouzský
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
regionální	regionální	k2eAgFnSc2d1	regionální
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Réunion	Réunion	k1gInSc1	Réunion
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
zástupců	zástupce	k1gMnPc2	zástupce
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
komoře	komora	k1gFnSc6	komora
francouzského	francouzský	k2eAgInSc2d1	francouzský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
komoře	komora	k1gFnSc6	komora
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Louis-Charles	Louis-Charles	k1gMnSc1	Louis-Charles
Mahé	Mahé	k1gNnSc2	Mahé
de	de	k?	de
La	la	k1gNnSc2	la
Bourdonnais	Bourdonnais	k1gFnPc2	Bourdonnais
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
</s>
</p>
<p>
<s>
Michel	Michel	k1gMnSc1	Michel
Houellebecq	Houellebecq	k1gMnSc1	Houellebecq
</s>
</p>
<p>
<s>
Dimitri	Dimitri	k6eAd1	Dimitri
Payet	Payet	k1gInSc1	Payet
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
Hoarau	Hoaraum	k1gNnSc3	Hoaraum
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc4	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
Richardson	Richardson	k1gMnSc1	Richardson
(	(	kIx(	(
<g/>
házená	házená	k1gFnSc1	házená
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeremy	Jerem	k1gInPc1	Jerem
Flores	Floresa	k1gFnPc2	Floresa
(	(	kIx(	(
<g/>
surf	surf	k1gInSc1	surf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Réunion	Réunion	k1gInSc1	Réunion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
materiál	materiál	k1gInSc1	materiál
Geografického	geografický	k2eAgInSc2d1	geografický
institutu	institut	k1gInSc2	institut
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Réunion	Réunion	k1gInSc1	Réunion
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvzdálenějších	vzdálený	k2eAgInPc2d3	nejvzdálenější
koutů	kout	k1gInPc2	kout
EU	EU	kA	EU
</s>
</p>
