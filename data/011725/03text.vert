<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
Motor	motor	k1gInSc1	motor
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
ス	ス	k?	ス
<g/>
,	,	kIx,	,
Suzuki	suzuki	k1gNnSc1	suzuki
kabušikigaiša	kabušikigaiš	k1gInSc2	kabušikigaiš
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgMnSc1d1	japonský
výrobce	výrobce	k1gMnSc1	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
lodních	lodní	k2eAgInPc2d1	lodní
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Hamamacu	Hamamacus	k1gInSc6	Hamamacus
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
více	hodně	k6eAd2	hodně
automobilů	automobil	k1gInPc2	automobil
než	než	k8xS	než
BMW	BMW	kA	BMW
<g/>
,	,	kIx,	,
Mazda	Mazda	k1gFnSc1	Mazda
a	a	k8xC	a
Subaru	Subar	k1gMnSc3	Subar
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
5	[number]	k4	5
letech	léto	k1gNnPc6	léto
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
japonském	japonský	k2eAgInSc6d1	japonský
trhu	trh	k1gInSc6	trh
prodejnost	prodejnost	k1gFnSc1	prodejnost
malých	malý	k2eAgNnPc2d1	malé
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kei	kei	k?	kei
cars	cars	k1gInSc1	cars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
značkou	značka	k1gFnSc7	značka
Maruti	Maruť	k1gFnSc2	Maruť
Suzuki	suzuki	k1gNnSc2	suzuki
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
evropské	evropský	k2eAgInPc1d1	evropský
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Automobily	automobil	k1gInPc1	automobil
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
současná	současný	k2eAgFnSc1d1	současná
produkce	produkce	k1gFnSc1	produkce
===	===	k?	===
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Baleno	balen	k2eAgNnSc1d1	baleno
(	(	kIx(	(
<g/>
od	od	k7c2	od
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Kizashi	Kizash	k1gFnSc2	Kizash
(	(	kIx(	(
<g/>
od	od	k7c2	od
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Grand	grand	k1gMnSc1	grand
Vitara	Vitar	k1gMnSc2	Vitar
II	II	kA	II
(	(	kIx(	(
<g/>
od	od	k7c2	od
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Ignis	Ignis	k1gFnPc2	Ignis
(	(	kIx(	(
<g/>
od	od	k7c2	od
2003	[number]	k4	2003
do	do	k7c2	do
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Jimny	Jimna	k1gFnSc2	Jimna
(	(	kIx(	(
<g/>
od	od	k7c2	od
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Splash	Splasha	k1gFnPc2	Splasha
(	(	kIx(	(
<g/>
od	od	k7c2	od
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Swift	Swifta	k1gFnPc2	Swifta
II	II	kA	II
(	(	kIx(	(
<g/>
od	od	k7c2	od
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
SX4	SX4	k1gFnPc2	SX4
(	(	kIx(	(
<g/>
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
XL7	XL7	k1gFnPc2	XL7
(	(	kIx(	(
<g/>
od	od	k7c2	od
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
starší	starý	k2eAgInPc1d2	starší
modely	model	k1gInPc1	model
===	===	k?	===
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Alto	Alto	k6eAd1	Alto
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Baleno	balen	k2eAgNnSc1d1	baleno
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Grand	grand	k1gMnSc1	grand
Vitara	Vitar	k1gMnSc2	Vitar
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Liana	Liana	k1gFnSc1	Liana
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Samurai	Samura	k1gFnSc2	Samura
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Swift	Swifta	k1gFnPc2	Swifta
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Vitara	Vitar	k1gMnSc2	Vitar
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Wagon	Wagona	k1gFnPc2	Wagona
R	R	kA	R
<g/>
+	+	kIx~	+
</s>
</p>
<p>
<s>
===	===	k?	===
modely	model	k1gInPc1	model
pro	pro	k7c4	pro
Japonsko	Japonsko	k1gNnSc4	Japonsko
===	===	k?	===
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Cervo	Cervo	k1gNnSc1	Cervo
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Landy	Landa	k1gMnSc2	Landa
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc4	suzuki
MR	MR	kA	MR
Wagon	Wagon	k1gNnSc4	Wagon
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Palette	Palett	k1gMnSc5	Palett
</s>
</p>
<p>
<s>
==	==	k?	==
Motocykly	motocykl	k1gInPc4	motocykl
==	==	k?	==
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
B-King	B-King	k1gInSc1	B-King
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Burgman	Burgman	k1gMnSc1	Burgman
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
DL650	DL650	k1gFnSc2	DL650
V-Strom	V-Strom	k1gInSc1	V-Strom
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
DL1000	DL1000	k1gFnSc2	DL1000
V-Strom	V-Strom	k1gInSc1	V-Strom
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
DR	dr	kA	dr
125	[number]	k4	125
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
DR	dr	kA	dr
350	[number]	k4	350
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
DR	dr	kA	dr
600	[number]	k4	600
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
DR	dr	kA	dr
650	[number]	k4	650
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
DR	dr	kA	dr
800	[number]	k4	800
Big	Big	k1gFnSc2	Big
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
DR-Z	DR-Z	k1gFnSc2	DR-Z
125L	[number]	k4	125L
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
DR-Z	DR-Z	k1gFnSc2	DR-Z
400SM	[number]	k4	400SM
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
DR-Z	DR-Z	k1gFnSc2	DR-Z
400E	[number]	k4	400E
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GN	GN	kA	GN
125	[number]	k4	125
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GR	GR	kA	GR
650	[number]	k4	650
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GS	GS	kA	GS
500	[number]	k4	500
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSF	GSF	kA	GSF
650	[number]	k4	650
Bandit	Bandit	k1gFnSc2	Bandit
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSF	GSF	kA	GSF
1200S	[number]	k4	1200S
Bandit	Bandit	k1gFnSc2	Bandit
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSF	GSF	kA	GSF
1250	[number]	k4	1250
Bandit	Bandit	k1gFnSc2	Bandit
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSR	GSR	kA	GSR
600	[number]	k4	600
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSR	GSR	kA	GSR
750	[number]	k4	750
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSX	GSX	kA	GSX
750	[number]	k4	750
Inazuma	Inazumum	k1gNnSc2	Inazumum
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSX	GSX	kA	GSX
750F	[number]	k4	750F
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSX	GSX	kA	GSX
1200	[number]	k4	1200
Inazuma	Inazumum	k1gNnSc2	Inazumum
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GSX	GSX	kA	GSX
1400	[number]	k4	1400
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
GSX-R	GSX-R	k1gFnSc2	GSX-R
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
GSX-R	GSX-R	k1gFnSc2	GSX-R
1300	[number]	k4	1300
Hayabusa	Hayabus	k1gMnSc2	Hayabus
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GZ	GZ	kA	GZ
125	[number]	k4	125
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
GZ	GZ	kA	GZ
250	[number]	k4	250
Marauder	Maraudero	k1gNnPc2	Maraudero
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Intruder	Intrudra	k1gFnPc2	Intrudra
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
LS	LS	kA	LS
650	[number]	k4	650
Savage	Savag	k1gInSc2	Savag
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
RF	RF	kA	RF
600	[number]	k4	600
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
RF	RF	kA	RF
900	[number]	k4	900
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
RM	RM	kA	RM
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
SFV	SFV	kA	SFV
650	[number]	k4	650
Gladius	Gladius	k1gInSc4	Gladius
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
SV	sv	kA	sv
650	[number]	k4	650
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
SV	sv	kA	sv
1000	[number]	k4	1000
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
TL	TL	kA	TL
1000	[number]	k4	1000
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc1	suzuki
Van	vana	k1gFnPc2	vana
Van	vana	k1gFnPc2	vana
125	[number]	k4	125
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
VL	VL	kA	VL
800	[number]	k4	800
Volusia	Volusium	k1gNnSc2	Volusium
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
VL	VL	kA	VL
1500	[number]	k4	1500
LC	LC	kA	LC
Intruder	Intrudra	k1gFnPc2	Intrudra
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
VS	VS	kA	VS
800	[number]	k4	800
Intruder	Intrudero	k1gNnPc2	Intrudero
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
VX	VX	kA	VX
800	[number]	k4	800
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
VZ	VZ	kA	VZ
800	[number]	k4	800
Marauder	Maraudero	k1gNnPc2	Maraudero
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnSc7	suzuki
XF	XF	kA	XF
650	[number]	k4	650
Freewind	Freewinda	k1gFnPc2	Freewinda
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
značek	značka	k1gFnPc2	značka
automobilů	automobil	k1gInPc2	automobil
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Suzuki	suzuki	k1gNnSc2	suzuki
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Suzuki	suzuki	k1gNnPc4	suzuki
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
