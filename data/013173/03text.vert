<p>
<s>
Bábovka	bábovka	k1gFnSc1	bábovka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
sladkého	sladký	k2eAgNnSc2d1	sladké
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pečením	pečeně	k1gFnPc3	pečeně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typické	typický	k2eAgNnSc4d1	typické
pečivo	pečivo	k1gNnSc4	pečivo
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
jižních	jižní	k2eAgFnPc6d1	jižní
částech	část	k1gFnPc6	část
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k9	jako
Gugelhupf	Gugelhupf	k1gInSc4	Gugelhupf
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
bábovce	bábovka	k1gFnSc3	bábovka
podobá	podobat	k5eAaImIp3nS	podobat
i	i	k9	i
italský	italský	k2eAgMnSc1d1	italský
Panettone	Panetton	k1gInSc5	Panetton
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
Baba	baba	k1gFnSc1	baba
au	au	k0	au
rhum	rhum	k6eAd1	rhum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
bábovka	bábovka	k1gFnSc1	bábovka
připravuje	připravovat	k5eAaImIp3nS	připravovat
nejčastěji	často	k6eAd3	často
dvoubarevná	dvoubarevný	k2eAgFnSc1d1	dvoubarevná
–	–	k?	–
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
a	a	k8xC	a
tmavou	tmavý	k2eAgFnSc7d1	tmavá
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
do	do	k7c2	do
těsta	těsto	k1gNnSc2	těsto
před	před	k7c7	před
pečením	pečení	k1gNnSc7	pečení
přidávají	přidávat	k5eAaImIp3nP	přidávat
rozinky	rozinka	k1gFnPc4	rozinka
<g/>
,	,	kIx,	,
kandované	kandovaný	k2eAgNnSc4d1	kandované
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
kokos	kokos	k1gInSc4	kokos
či	či	k8xC	či
kousky	kousek	k1gInPc4	kousek
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Bábovka	bábovka	k1gFnSc1	bábovka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
speciální	speciální	k2eAgFnSc6d1	speciální
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
pečivu	pečivo	k1gNnSc3	pečivo
typický	typický	k2eAgInSc4d1	typický
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
otvorem	otvor	k1gInSc7	otvor
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
a	a	k8xC	a
dekoruje	dekorovat	k5eAaBmIp3nS	dekorovat
se	se	k3xPyFc4	se
posypáním	posypání	k1gNnSc7	posypání
moučkovým	moučkový	k2eAgInSc7d1	moučkový
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
do	do	k7c2	do
bábovky	bábovka	k1gFnSc2	bábovka
přidat	přidat	k5eAaPmF	přidat
potravinářská	potravinářský	k2eAgNnPc4d1	potravinářské
barviva	barvivo	k1gNnPc4	barvivo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
různé	různý	k2eAgFnPc4d1	různá
netradiční	tradiční	k2eNgFnPc4d1	netradiční
barevné	barevný	k2eAgFnPc4d1	barevná
variace	variace	k1gFnPc4	variace
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
bábovičky	bábovička	k1gFnPc1	bábovička
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
hromádky	hromádka	k1gFnPc1	hromádka
vlhkého	vlhký	k2eAgInSc2d1	vlhký
písku	písek	k1gInSc2	písek
vytvářené	vytvářený	k2eAgInPc1d1	vytvářený
malými	malý	k2eAgFnPc7d1	malá
dětmi	dítě	k1gFnPc7	dítě
pomocí	pomocí	k7c2	pomocí
stejnojmenných	stejnojmenný	k2eAgFnPc2d1	stejnojmenná
formiček	formička	k1gFnPc2	formička
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
podobných	podobný	k2eAgInPc2d1	podobný
zmenšené	zmenšený	k2eAgFnSc6d1	zmenšená
formě	forma	k1gFnSc6	forma
na	na	k7c4	na
bábovku	bábovka	k1gFnSc4	bábovka
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
pískovišti	pískoviště	k1gNnSc6	pískoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Třená	třený	k2eAgFnSc1d1	třená
bábovka	bábovka	k1gFnSc1	bábovka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
vajíček	vajíčko	k1gNnPc2	vajíčko
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Našleháním	našlehání	k1gNnSc7	našlehání
bílku	bílek	k1gInSc2	bílek
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
světlou	světlý	k2eAgFnSc4d1	světlá
složku	složka	k1gFnSc4	složka
bábovky	bábovka	k1gFnSc2	bábovka
<g/>
.	.	kIx.	.
</s>
<s>
Tmavá	tmavý	k2eAgFnSc1d1	tmavá
složka	složka	k1gFnSc1	složka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
přidáním	přidání	k1gNnSc7	přidání
kakaa	kakao	k1gNnSc2	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
varianta	varianta	k1gFnSc1	varianta
bábovky	bábovka	k1gFnSc2	bábovka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
namísto	namísto	k7c2	namísto
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těsto	těsto	k1gNnSc1	těsto
na	na	k7c4	na
kynutou	kynutý	k2eAgFnSc4d1	kynutá
bábovku	bábovka	k1gFnSc4	bábovka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
másla	máslo	k1gNnSc2	máslo
a	a	k8xC	a
droždí	droždí	k1gNnSc2	droždí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
buchta	buchta	k1gFnSc1	buchta
</s>
</p>
<p>
<s>
koláč	koláč	k1gInSc1	koláč
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bábovka	bábovka	k1gFnSc1	bábovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bábovka	bábovka	k1gFnSc1	bábovka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Recept	recept	k1gInSc1	recept
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
bábovky	bábovka	k1gFnSc2	bábovka
</s>
</p>
