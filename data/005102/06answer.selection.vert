<s>
Mícha	mícha	k1gFnSc1	mícha
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
medulla	medulla	k1gFnSc1	medulla
spinalis	spinalis	k1gFnSc1	spinalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
tenká	tenký	k2eAgFnSc1d1	tenká
nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
buněk	buňka	k1gFnPc2	buňka
uvnitř	uvnitř	k7c2	uvnitř
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
míchu	mícha	k1gFnSc4	mícha
v	v	k7c6	v
mozkovém	mozkový	k2eAgInSc6d1	mozkový
kmeni	kmen	k1gInSc6	kmen
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
skrz	skrz	k7c4	skrz
velký	velký	k2eAgInSc4d1	velký
týlní	týlní	k2eAgInSc4d1	týlní
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
nad	nad	k7c7	nad
druhým	druhý	k4xOgInSc7	druhý
bederním	bederní	k2eAgInSc7d1	bederní
obratlem	obratel	k1gInSc7	obratel
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
conus	conus	k1gInSc1	conus
medullaris	medullaris	k1gFnSc3	medullaris
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
filum	filum	k1gInSc1	filum
terminale	terminale	k6eAd1	terminale
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
míšní	míšní	k2eAgInPc4d1	míšní
nervy	nerv	k1gInPc4	nerv
<g/>
.	.	kIx.	.
</s>
