<s>
Mícha	mícha	k1gFnSc1	mícha
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
medulla	medulla	k1gFnSc1	medulla
spinalis	spinalis	k1gFnSc1	spinalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
tenká	tenký	k2eAgFnSc1d1	tenká
nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
buněk	buňka	k1gFnPc2	buňka
uvnitř	uvnitř	k7c2	uvnitř
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
míchu	mícha	k1gFnSc4	mícha
v	v	k7c6	v
mozkovém	mozkový	k2eAgInSc6d1	mozkový
kmeni	kmen	k1gInSc6	kmen
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
skrz	skrz	k7c4	skrz
velký	velký	k2eAgInSc4d1	velký
týlní	týlní	k2eAgInSc4d1	týlní
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
nad	nad	k7c7	nad
druhým	druhý	k4xOgInSc7	druhý
bederním	bederní	k2eAgInSc7d1	bederní
obratlem	obratel	k1gInSc7	obratel
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
conus	conus	k1gInSc1	conus
medullaris	medullaris	k1gFnSc3	medullaris
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
filum	filum	k1gInSc1	filum
terminale	terminale	k6eAd1	terminale
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
míšní	míšní	k2eAgInPc4d1	míšní
nervy	nerv	k1gInPc4	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
a	a	k8xC	a
mozek	mozek	k1gInSc1	mozek
tvoří	tvořit	k5eAaImIp3nS	tvořit
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
míchy	mícha	k1gFnSc2	mícha
vycházejí	vycházet	k5eAaImIp3nP	vycházet
nervy	nerv	k1gInPc4	nerv
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
přenáší	přenášet	k5eAaImIp3nS	přenášet
organismus	organismus	k1gInSc4	organismus
informaci	informace	k1gFnSc4	informace
mezi	mezi	k7c7	mezi
mozkem	mozek	k1gInSc7	mozek
a	a	k8xC	a
periferní	periferní	k2eAgFnSc7d1	periferní
nervovou	nervový	k2eAgFnSc7d1	nervová
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
venkovního	venkovní	k2eAgNnSc2d1	venkovní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopná	schopný	k2eAgFnSc1d1	schopná
některých	některý	k3yIgFnPc2	některý
autonomních	autonomní	k2eAgFnPc2d1	autonomní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
míšní	míšní	k2eAgInPc1d1	míšní
reflexy	reflex	k1gInPc1	reflex
<g/>
)	)	kIx)	)
a	a	k8xC	a
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
některé	některý	k3yIgInPc4	některý
reflexy	reflex	k1gInPc4	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
míchy	mícha	k1gFnSc2	mícha
leží	ležet	k5eAaImIp3nS	ležet
centrální	centrální	k2eAgInSc1d1	centrální
kanálek	kanálek	k1gInSc1	kanálek
míšní	míšeň	k1gFnPc2	míšeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mozkomíšní	mozkomíšní	k2eAgInSc4d1	mozkomíšní
mok	mok	k1gInSc4	mok
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
míchy	mícha	k1gFnSc2	mícha
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
45	[number]	k4	45
cm	cm	kA	cm
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
43	[number]	k4	43
cm	cm	kA	cm
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
směrem	směr	k1gInSc7	směr
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
13	[number]	k4	13
mm	mm	kA	mm
na	na	k7c4	na
6.4	[number]	k4	6.4
mm	mm	kA	mm
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
krční	krční	k2eAgFnSc2d1	krční
páteře	páteř	k1gFnSc2	páteř
v	v	k7c6	v
hrudní	hrudní	k2eAgFnSc6d1	hrudní
a	a	k8xC	a
v	v	k7c6	v
lumbální	lumbální	k2eAgFnSc6d1	lumbální
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
zesílena	zesílen	k2eAgFnSc1d1	zesílena
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
nervy	nerv	k1gInPc1	nerv
pro	pro	k7c4	pro
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průřezu	průřez	k1gInSc6	průřez
oválná	oválný	k2eAgFnSc1d1	oválná
<g/>
,	,	kIx,	,
vpředu	vpředu	k6eAd1	vpředu
je	být	k5eAaImIp3nS	být
zářez	zářez	k1gInSc1	zářez
<g/>
,	,	kIx,	,
vzadu	vzadu	k6eAd1	vzadu
je	být	k5eAaImIp3nS	být
žlábek	žlábek	k1gInSc1	žlábek
a	a	k8xC	a
laterální	laterální	k2eAgFnSc1d1	laterální
(	(	kIx(	(
<g/>
z	z	k7c2	z
boku	bok	k1gInSc2	bok
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
žlábky	žlábek	k1gInPc1	žlábek
–	–	k?	–
zadní	zadní	k2eAgInSc1d1	zadní
žlábek	žlábek	k1gInSc1	žlábek
postranní	postranní	k2eAgInSc1d1	postranní
a	a	k8xC	a
přední	přední	k2eAgInSc1d1	přední
žlábek	žlábek	k1gInSc1	žlábek
postranní	postranní	k2eAgInSc1d1	postranní
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
přední	přední	k2eAgInPc1d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgInPc1d1	zadní
míšní	míšní	k2eAgInPc1d1	míšní
kořeny	kořen	k1gInPc1	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákna	k1gFnSc1	vlákna
předního	přední	k2eAgInSc2d1	přední
a	a	k8xC	a
zadního	zadní	k2eAgInSc2d1	zadní
kořenu	kořen	k1gInSc2	kořen
míšního	míšní	k2eAgInSc2d1	míšní
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
nerv	nerv	k1gInSc4	nerv
míšní	míšní	k2eAgInSc4d1	míšní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přední	přední	k2eAgInSc4d1	přední
kořen	kořen	k1gInSc4	kořen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
převážně	převážně	k6eAd1	převážně
vlákna	vlákna	k1gFnSc1	vlákna
motorická	motorický	k2eAgFnSc1d1	motorická
(	(	kIx(	(
<g/>
eferentní	eferentní	k2eAgFnSc1d1	eferentní
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadní	zadní	k2eAgInSc1d1	zadní
kořen	kořen	k1gInSc1	kořen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
převážně	převážně	k6eAd1	převážně
vlákna	vlákna	k1gFnSc1	vlákna
senzitivní	senzitivní	k2eAgFnSc1d1	senzitivní
(	(	kIx(	(
<g/>
aferentní	aferentní	k2eAgFnSc1d1	aferentní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
smyslové	smyslový	k2eAgInPc1d1	smyslový
signály	signál	k1gInPc1	signál
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
až	až	k9	až
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
jiné	jiná	k1gFnPc1	jiná
vedou	vést	k5eAaImIp3nP	vést
z	z	k7c2	z
mozku	mozek	k1gInSc2	mozek
do	do	k7c2	do
míchy	mícha	k1gFnSc2	mícha
(	(	kIx(	(
<g/>
sestupné	sestupný	k2eAgFnPc1d1	sestupná
motorické	motorický	k2eAgFnPc1d1	motorická
dráhy	dráha	k1gFnPc1	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
poranění	poranění	k1gNnSc1	poranění
míchy	mícha	k1gFnSc2	mícha
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
paréza	paréza	k1gFnSc1	paréza
<g/>
,	,	kIx,	,
plegie	plegie	k1gFnSc1	plegie
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
se	se	k3xPyFc4	se
topograficky	topograficky	k6eAd1	topograficky
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
31	[number]	k4	31
segmentů	segment	k1gInPc2	segment
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jeden	jeden	k4xCgInSc1	jeden
nerv	nerv	k1gInSc1	nerv
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
krčních	krční	k2eAgMnPc2d1	krční
<g/>
,	,	kIx,	,
12	[number]	k4	12
hrudních	hrudní	k2eAgMnPc2d1	hrudní
<g/>
,	,	kIx,	,
5	[number]	k4	5
bederních	bederní	k2eAgMnPc2d1	bederní
<g/>
,	,	kIx,	,
5	[number]	k4	5
křížových	křížový	k2eAgFnPc2d1	křížová
a	a	k8xC	a
1	[number]	k4	1
kostrční	kostrční	k2eAgInPc1d1	kostrční
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
výstupu	výstup	k1gInSc2	výstup
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
C	C	kA	C
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
krční	krční	k2eAgInPc1d1	krční
nervy	nerv	k1gInPc1	nerv
(	(	kIx(	(
<g/>
nervi	rvát	k5eNaImRp2nS	rvát
cervicales	cervicales	k1gInSc1	cervicales
<g/>
)	)	kIx)	)
–	–	k?	–
8	[number]	k4	8
párů	pár	k1gInPc2	pár
<g/>
;	;	kIx,	;
inervují	inervovat	k5eAaImIp3nP	inervovat
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
horních	horní	k2eAgFnPc2d1	horní
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
(	(	kIx(	(
<g/>
Th	Th	k1gFnSc1	Th
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Th	Th	k1gFnSc1	Th
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
hrudní	hrudní	k2eAgInPc1d1	hrudní
nervy	nerv	k1gInPc1	nerv
(	(	kIx(	(
<g/>
nervi	rvát	k5eNaImRp2nS	rvát
thoracici	thoracic	k1gMnSc5	thoracic
<g/>
)	)	kIx)	)
–	–	k?	–
12	[number]	k4	12
párů	pár	k1gInPc2	pár
<g/>
;	;	kIx,	;
inervují	inervovat	k5eAaImIp3nP	inervovat
mezižeberní	mezižeberní	k2eAgInPc4d1	mezižeberní
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
svaly	sval	k1gInPc1	sval
zad	záda	k1gNnPc2	záda
a	a	k8xC	a
hrudníku	hrudník	k1gInSc2	hrudník
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
bederní	bederní	k2eAgInPc1d1	bederní
nervy	nerv	k1gInPc1	nerv
(	(	kIx(	(
<g/>
nervi	rvát	k5eNaImRp2nS	rvát
lumbales	lumbales	k1gInSc1	lumbales
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
5	[number]	k4	5
párů	pár	k1gInPc2	pár
<g/>
;	;	kIx,	;
inervují	inervovat	k5eAaImIp3nP	inervovat
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
kůži	kůže	k1gFnSc4	kůže
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
stehna	stehno	k1gNnSc2	stehno
a	a	k8xC	a
kůži	kůže	k1gFnSc4	kůže
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
S	s	k7c7	s
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
křížové	křížový	k2eAgInPc4d1	křížový
nervy	nerv	k1gInPc4	nerv
(	(	kIx(	(
<g/>
nervi	rvát	k5eNaImRp2nS	rvát
sacrales	sacrales	k1gInSc1	sacrales
<g/>
)	)	kIx)	)
–	–	k?	–
5	[number]	k4	5
párů	pár	k1gInPc2	pár
<g/>
;	;	kIx,	;
inervují	inervovat	k5eAaImIp3nP	inervovat
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
sedací	sedací	k2eAgInPc1d1	sedací
svaly	sval	k1gInPc1	sval
(	(	kIx(	(
<g/>
Co	co	k9	co
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
Co	co	k9	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
kostrční	kostrční	k2eAgInPc1d1	kostrční
nervy	nerv	k1gInPc1	nerv
(	(	kIx(	(
<g/>
nervus	nervus	k1gMnSc1	nervus
coccygeus	coccygeus	k1gMnSc1	coccygeus
<g/>
)	)	kIx)	)
–	–	k?	–
1	[number]	k4	1
pár	pár	k4xCyI	pár
<g/>
;	;	kIx,	;
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
bez	bez	k7c2	bez
funkce	funkce	k1gFnSc2	funkce
Shluky	shluk	k1gInPc1	shluk
těl	tělo	k1gNnPc2	tělo
neuronů	neuron	k1gInPc2	neuron
uložená	uložený	k2eAgFnSc1d1	uložená
na	na	k7c6	na
zadních	zadní	k2eAgInPc6d1	zadní
kořenech	kořen	k1gInPc6	kořen
(	(	kIx(	(
<g/>
spinální	spinální	k2eAgNnSc4d1	spinální
ganglion	ganglion	k1gNnSc4	ganglion
<g/>
)	)	kIx)	)
a	a	k8xC	a
přední	přední	k2eAgMnPc1d1	přední
kořeny	kořen	k1gInPc1	kořen
tvoří	tvořit	k5eAaImIp3nP	tvořit
míšních	míšeň	k1gFnPc6	míšeň
nervy	nerv	k1gInPc4	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
motorická	motorický	k2eAgNnPc1d1	motorické
vlákna	vlákno	k1gNnPc1	vlákno
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
míše	mícha	k1gFnSc6	mícha
<g/>
,	,	kIx,	,
senzitivní	senzitivní	k2eAgNnPc1d1	senzitivní
vlákna	vlákno	k1gNnPc1	vlákno
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
ležících	ležící	k2eAgFnPc2d1	ležící
mimo	mimo	k7c4	mimo
míchu	mícha	k1gFnSc4	mícha
ve	v	k7c6	v
vřetenovitých	vřetenovitý	k2eAgNnPc6d1	vřetenovité
zduřeních	zduření	k1gNnPc6	zduření
na	na	k7c6	na
zadních	zadní	k2eAgInPc6d1	zadní
míšních	míšní	k2eAgInPc6d1	míšní
kořenech	kořen	k1gInPc6	kořen
(	(	kIx(	(
<g/>
spinální	spinální	k2eAgNnSc1d1	spinální
ganglion	ganglion	k1gNnSc1	ganglion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nervem	nerv	k1gInSc7	nerv
smíšeným	smíšený	k2eAgInSc7d1	smíšený
je	být	k5eAaImIp3nS	být
míšní	míšní	k2eAgFnSc1d1	míšní
nerv	nerv	k1gInSc1	nerv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlákna	vlákna	k1gFnSc1	vlákna
motorická	motorický	k2eAgFnSc1d1	motorická
i	i	k8xC	i
senzitivní	senzitivní	k2eAgFnSc1d1	senzitivní
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
centrálního	centrální	k2eAgInSc2d1	centrální
míšního	míšní	k2eAgInSc2d1	míšní
kanálku	kanálek	k1gInSc2	kanálek
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
šedá	šedý	k2eAgFnSc1d1	šedá
hmota	hmota	k1gFnSc1	hmota
míšní	míšní	k2eAgFnSc1d1	míšní
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
motýlích	motýlí	k2eAgNnPc2d1	motýlí
křídel	křídlo	k1gNnPc2	křídlo
–	–	k?	–
přední	přední	k2eAgMnSc1d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgInPc1d1	zadní
rohy	roh	k1gInPc1	roh
(	(	kIx(	(
<g/>
v	v	k7c6	v
hrudním	hrudní	k2eAgInSc6d1	hrudní
úseku	úsek	k1gInSc6	úsek
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
boční	boční	k2eAgInSc4d1	boční
roh	roh	k1gInSc4	roh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předních	přední	k2eAgInPc2d1	přední
rohů	roh	k1gInPc2	roh
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
přední	přední	k2eAgInPc1d1	přední
míšní	míšní	k2eAgInPc1d1	míšní
kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
ze	z	k7c2	z
zadních	zadní	k2eAgInPc2d1	zadní
rohů	roh	k1gInPc2	roh
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
zadní	zadní	k2eAgInPc1d1	zadní
kořeny	kořen	k1gInPc1	kořen
<g/>
.	.	kIx.	.
bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
hmotu	hmota	k1gFnSc4	hmota
šedou	šedý	k2eAgFnSc4d1	šedá
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodivý	vodivý	k2eAgInSc4d1	vodivý
systém	systém	k1gInSc4	systém
míšní	míšeň	k1gFnPc2	míšeň
<g/>
.	.	kIx.	.
</s>
<s>
Vsunuté	vsunutý	k2eAgFnPc1d1	vsunutá
buňky	buňka	k1gFnPc1	buňka
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
šedé	šedý	k2eAgFnSc6d1	šedá
hmotě	hmota	k1gFnSc6	hmota
síť	síť	k1gFnSc4	síť
interneuronů	interneuron	k1gMnPc2	interneuron
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzruchy	vzruch	k1gInPc1	vzruch
šířit	šířit	k5eAaImF	šířit
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
provazcové	provazcový	k2eAgFnPc1d1	provazcová
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zadních	zadní	k2eAgInPc6d1	zadní
sloupcích	sloupec	k1gInPc6	sloupec
a	a	k8xC	a
vysílají	vysílat	k5eAaImIp3nP	vysílat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
neurity	neurit	k1gInPc1	neurit
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stoupají	stoupat	k5eAaImIp3nP	stoupat
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
v	v	k7c6	v
provazcích	provazec	k1gInPc6	provazec
míšních	míšní	k2eAgInPc6d1	míšní
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
systém	systém	k1gInSc1	systém
míchy	mícha	k1gFnSc2	mícha
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nervová	nervový	k2eAgFnSc1d1	nervová
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
uskutečňující	uskutečňující	k2eAgInSc1d1	uskutečňující
inter	inter	k1gInSc1	inter
a	a	k8xC	a
intra	intra	k6eAd1	intra
segmentální	segmentální	k2eAgNnSc4d1	segmentální
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přepojovací	přepojovací	k2eAgFnSc7d1	přepojovací
stanicí	stanice	k1gFnSc7	stanice
spinálních	spinální	k2eAgInPc2d1	spinální
reflexů	reflex	k1gInPc2	reflex
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
přenosu	přenos	k1gInSc6	přenos
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Impulsy	impuls	k1gInPc1	impuls
přicházející	přicházející	k2eAgInPc1d1	přicházející
od	od	k7c2	od
proprioreceptorů	proprioreceptor	k1gInPc2	proprioreceptor
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
šlach	šlacha	k1gFnPc2	šlacha
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
přepojují	přepojovat	k5eAaImIp3nP	přepojovat
na	na	k7c4	na
motoneurony	motoneuron	k1gMnPc4	motoneuron
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
udržují	udržovat	k5eAaImIp3nP	udržovat
svaly	sval	k1gInPc1	sval
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
tonu	tonus	k1gInSc6	tonus
(	(	kIx(	(
<g/>
spinální	spinální	k2eAgInSc4d1	spinální
svalový	svalový	k2eAgInSc4d1	svalový
tonus	tonus	k1gInSc4	tonus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
snižuje	snižovat	k5eAaImIp3nS	snižovat
(	(	kIx(	(
<g/>
hypotonie	hypotonie	k1gFnSc1	hypotonie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
blokuje	blokovat	k5eAaImIp3nS	blokovat
(	(	kIx(	(
<g/>
atonie	atonie	k1gFnSc1	atonie
<g/>
)	)	kIx)	)
svalové	svalový	k2eAgNnSc1d1	svalové
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Vodivý	vodivý	k2eAgInSc1d1	vodivý
systém	systém	k1gInSc1	systém
míchy	mícha	k1gFnSc2	mícha
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
ascendentních	ascendentní	k2eAgFnPc2d1	ascendentní
(	(	kIx(	(
<g/>
vzestupných	vzestupný	k2eAgFnPc2d1	vzestupná
<g/>
)	)	kIx)	)
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
descendentních	descendentní	k2eAgFnPc2d1	descendentní
(	(	kIx(	(
<g/>
sestupných	sestupný	k2eAgFnPc2d1	sestupná
<g/>
)	)	kIx)	)
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnPc4d2	vyšší
centra	centrum	k1gNnPc4	centrum
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
vnějšího	vnější	k2eAgNnSc2d1	vnější
a	a	k8xC	a
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
napětí	napětí	k1gNnSc6	napětí
pohybových	pohybový	k2eAgFnPc2d1	pohybová
ústrojí	ústroj	k1gFnPc2	ústroj
<g/>
.	.	kIx.	.
</s>
<s>
Axony	Axona	k1gFnPc1	Axona
jsou	být	k5eAaImIp3nP	být
rozvětvené	rozvětvený	k2eAgFnPc1d1	rozvětvená
<g/>
,	,	kIx,	,
konce	konec	k1gInPc1	konec
mohou	moct	k5eAaImIp3nP	moct
přenášet	přenášet	k5eAaImF	přenášet
impulsy	impuls	k1gInPc4	impuls
na	na	k7c4	na
několik	několik	k4yIc4	několik
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
nervová	nervový	k2eAgFnSc1d1	nervová
buňka	buňka	k1gFnSc1	buňka
může	moct	k5eAaImIp3nS	moct
přijímat	přijímat	k5eAaImF	přijímat
impulsy	impuls	k1gInPc4	impuls
z	z	k7c2	z
tisíců	tisíc	k4xCgInPc2	tisíc
neuritů	neurit	k1gInPc2	neurit
<g/>
.	.	kIx.	.
</s>
