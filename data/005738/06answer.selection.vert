<s>
Velká	velká	k1gFnSc1	velká
části	část	k1gFnSc2	část
odborníků	odborník	k1gMnPc2	odborník
však	však	k9	však
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
rod	rod	k1gInSc4	rod
Homo	Homo	k6eAd1	Homo
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
rychlému	rychlý	k2eAgInSc3d1	rychlý
pohybu	pohyb	k1gInSc3	pohyb
a	a	k8xC	a
běhu	běh	k1gInSc6	běh
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
savaně	savana	k1gFnSc6	savana
<g/>
,	,	kIx,	,
australopitékové	australopitékové	k2eAgFnSc4d1	australopitékové
pomalejší	pomalý	k2eAgFnSc4d2	pomalejší
pozemní	pozemní	k2eAgFnSc4d1	pozemní
chůzi	chůze	k1gFnSc4	chůze
alespoň	alespoň	k9	alespoň
zčásti	zčásti	k6eAd1	zčásti
kombinovali	kombinovat	k5eAaImAgMnP	kombinovat
se	s	k7c7	s
šplháním	šplhání	k1gNnSc7	šplhání
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patrně	patrně	k6eAd1	patrně
hledali	hledat	k5eAaImAgMnP	hledat
potravu	potrava	k1gFnSc4	potrava
i	i	k8xC	i
úkryt	úkryt	k1gInSc4	úkryt
před	před	k7c7	před
dravci	dravec	k1gMnPc7	dravec
<g/>
.	.	kIx.	.
</s>
