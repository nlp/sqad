<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
pascha	pascha	k1gFnSc1	pascha
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
π	π	k?	π
–	–	k?	–
pascha	pascha	k1gFnSc1	pascha
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
פ	פ	k?	פ
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ּ	ּ	k?	ּ
<g/>
ס	ס	k?	ס
<g/>
ַ	ַ	k?	ַ
<g/>
ח	ח	k?	ח
pesach	pesach	k1gInSc1	pesach
–	–	k?	–
přechod	přechod	k1gInSc1	přechod
<g/>
,	,	kIx,	,
přejití	přejití	k1gNnPc1	přejití
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
oslavou	oslava	k1gFnSc7	oslava
zmrtvýchvstání	zmrtvýchvstání	k1gNnPc2	zmrtvýchvstání
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
