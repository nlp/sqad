<s>
Rezistence	rezistence	k1gFnSc1
(	(	kIx(
<g/>
zahradnictví	zahradnictví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc1
hostitele	hostitel	k1gMnSc2
překonat	překonat	k5eAaPmF
nebo	nebo	k8xC
významně	významně	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
aktivitu	aktivita	k1gFnSc4
patogenního	patogenní	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezistence	rezistence	k1gFnSc1
může	moct	k5eAaImIp3nS
účinkovat	účinkovat	k5eAaImF
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
,	,	kIx,
různé	různý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
<g/>
,	,	kIx,
různou	různý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
proti	proti	k7c3
různě	různě	k6eAd1
rozsáhlým	rozsáhlý	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
patogenů	patogen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Konstitutivní	konstitutivní	k2eAgInPc1d1
a	a	k8xC
indukované	indukovaný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Na	na	k7c6
ochraně	ochrana	k1gFnSc6
rostliny	rostlina	k1gFnSc2
vůči	vůči	k7c3
patogenům	patogen	k1gInPc3
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
mnoho	mnoho	k4c1
různých	různý	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bariéry	bariéra	k1gFnSc2
omezující	omezující	k2eAgInSc1d1
průnik	průnik	k1gInSc1
patogenu	patogen	k1gInSc2
do	do	k7c2
hostitele	hostitel	k1gMnSc2
nebo	nebo	k8xC
jeho	jeho	k3xOp3gNnSc4
další	další	k2eAgNnSc4d1
šíření	šíření	k1gNnSc4
jsou	být	k5eAaImIp3nP
nazývány	nazýván	k2eAgInPc1d1
strukturní	strukturní	k2eAgInPc1d1
mechanizmy	mechanizmus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokryv	pokryv	k1gInSc4
nebo	nebo	k8xC
povrch	povrch	k1gInSc4
pokožky	pokožka	k1gFnSc2
listů	list	k1gInPc2
a	a	k8xC
stonků	stonek	k1gInPc2
<g/>
,	,	kIx,
velikost	velikost	k1gFnSc1
a	a	k8xC
struktura	struktura	k1gFnSc1
stomat	stomat	k2eAgNnSc2d1
a	a	k8xC
lenticel	lenticela	k1gFnPc2
<g/>
,	,	kIx,
tloušťka	tloušťka	k1gFnSc1
a	a	k8xC
struktura	struktura	k1gFnSc1
buněčných	buněčný	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
obranné	obranný	k2eAgInPc1d1
mechanismy	mechanismus	k1gInPc1
jsou	být	k5eAaImIp3nP
přirozenými	přirozený	k2eAgInPc7d1
prvky	prvek	k1gInPc7
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
se	se	k3xPyFc4
rostliny	rostlina	k1gFnPc1
brání	bránit	k5eAaImIp3nP
proti	proti	k7c3
patogenům	patogen	k1gInPc3
a	a	k8xC
jsou	být	k5eAaImIp3nP
stálými	stálý	k2eAgFnPc7d1
rostlinnými	rostlinný	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Konstitutivní	konstitutivní	k2eAgInPc1d1
ochranné	ochranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vosky	vosk	k1gInPc1
a	a	k8xC
povrchové	povrchový	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
trichomy	trichom	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
kutikula	kutikula	k1gFnSc1
</s>
<s>
buněčné	buněčný	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
</s>
<s>
lignifikace	lignifikace	k1gFnSc1
</s>
<s>
chemické	chemický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
s	s	k7c7
negativním	negativní	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
pro	pro	k7c4
patogenní	patogenní	k2eAgInPc4d1
organismy	organismus	k1gInPc4
</s>
<s>
Indukované	indukovaný	k2eAgFnPc1d1
bariéry	bariéra	k1gFnPc1
nebo	nebo	k8xC
mechanismy	mechanismus	k1gInPc1
jsou	být	k5eAaImIp3nP
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnPc1
po	po	k7c6
napadení	napadení	k1gNnSc6
hostitele	hostitel	k1gMnSc2
patogenem	patogen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovým	takový	k3xDgNnPc3
opatřením	opatření	k1gNnPc3
je	být	k5eAaImIp3nS
opad	opad	k1gInSc1
listů	list	k1gInPc2
<g/>
,	,	kIx,
tvorba	tvorba	k1gFnSc1
nekróz	nekróza	k1gFnPc2
<g/>
,	,	kIx,
ztlustlých	ztlustlý	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Významným	významný	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
na	na	k7c4
ochranu	ochrana	k1gFnSc4
hostitele	hostitel	k1gMnSc2
po	po	k7c6
napadení	napadení	k1gNnSc6
je	být	k5eAaImIp3nS
hypersenzitivní	hypersenzitivní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napadené	napadený	k2eAgFnSc2d1
tkáně	tkáň	k1gFnSc2
hostitele	hostitel	k1gMnPc4
rychle	rychle	k6eAd1
hynou	hynout	k5eAaImIp3nP
<g/>
,	,	kIx,
takže	takže	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
zastavení	zastavení	k1gNnSc3
šíření	šíření	k1gNnSc2
patogenu	patogen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hypersenzitivní	hypersenzitivní	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
látky	látka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
hubí	hubit	k5eAaImIp3nP
jak	jak	k6eAd1
patogen	patogen	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
vlastní	vlastní	k2eAgFnPc1d1
<g/>
,	,	kIx,
napadené	napadený	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hypersenzitivní	hypersenzitivní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
vznikem	vznik	k1gInSc7
nekróz	nekróza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hypersenzitivní	hypersenzitivní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
je	být	k5eAaImIp3nS
projevem	projev	k1gInSc7
vertikální	vertikální	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biochemické	biochemický	k2eAgInPc1d1
mechanizmy	mechanizmus	k1gInPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c4
vytváření	vytváření	k1gNnSc4
sloučenin	sloučenina	k1gFnPc2
s	s	k7c7
fungicidním	fungicidní	k2eAgInSc7d1
<g/>
,	,	kIx,
baktericidním	baktericidní	k2eAgInSc7d1
nebo	nebo	k8xC
insekticidním	insekticidní	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinné	účinný	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
běžně	běžně	k6eAd1
přítomny	přítomen	k2eAgInPc1d1
v	v	k7c6
rostlině	rostlina	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
pouze	pouze	k6eAd1
při	při	k7c6
napadením	napadení	k1gNnPc3
patogenem	patogen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
látek	látka	k1gFnPc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
intenzitu	intenzita	k1gFnSc4
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
například	například	k6eAd1
u	u	k7c2
fytoalexinů	fytoalexin	k1gInPc2
<g/>
,	,	kIx,
obranných	obranný	k2eAgFnPc2d1
antibakteriálních	antibakteriální	k2eAgFnPc2d1
a	a	k8xC
antimykotických	antimykotický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezistence	rezistence	k1gFnSc1
vůči	vůči	k7c3
patogennímu	patogenní	k2eAgInSc3d1
organismu	organismus	k1gInSc3
je	být	k5eAaImIp3nS
často	často	k6eAd1
dědičná	dědičný	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
systémové	systémový	k2eAgFnSc6d1
rezistenci	rezistence	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
aktivaci	aktivace	k1gFnSc3
transkripce	transkripce	k1gFnSc2
genů	gen	k1gInPc2
kódujících	kódující	k2eAgInPc2d1
různé	různý	k2eAgInPc4d1
komponenty	komponent	k1gInPc4
buněčné	buněčný	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
polysacharidy	polysacharid	k1gInPc1
<g/>
,	,	kIx,
lignin	lignin	k1gInSc1
<g/>
,	,	kIx,
suberin	suberin	k1gInSc1
<g/>
,	,	kIx,
saponin	saponin	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
bariérou	bariéra	k1gFnSc7
pro	pro	k7c4
infekci	infekce	k1gFnSc4
patogenu	patogen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infekci	infekce	k1gFnSc4
mohou	moct	k5eAaImIp3nP
zabránit	zabránit	k5eAaPmF
i	i	k9
rostlinné	rostlinný	k2eAgInPc4d1
enzymy	enzym	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
rozkládají	rozkládat	k5eAaImIp3nP
komponenty	komponent	k1gInPc4
buněčné	buněčný	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
hmyzích	hmyzí	k2eAgMnPc2d1
škůdců	škůdce	k1gMnPc2
<g/>
,	,	kIx,
houbových	houbová	k1gFnPc2
nebo	nebo	k8xC
bakteriálních	bakteriální	k2eAgInPc2d1
patogenů	patogen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obrana	obrana	k1gFnSc1
rostliny	rostlina	k1gFnSc2
tak	tak	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
aktivní	aktivní	k2eAgInSc4d1
nebo	nebo	k8xC
pasivní	pasivní	k2eAgInSc4d1
<g/>
,	,	kIx,
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
účinné	účinný	k2eAgInPc1d1
obranné	obranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
hostitele	hostitel	k1gMnSc2
již	již	k6eAd1
přítomny	přítomen	k2eAgInPc1d1
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
až	až	k9
po	po	k7c4
signalizaci	signalizace	k1gFnSc4
a	a	k8xC
během	během	k7c2
napadení	napadení	k1gNnPc2
hostitele	hostitel	k1gMnSc2
patogenem	patogen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Synergie	synergie	k1gFnSc1
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
obrana	obrana	k1gFnSc1
rostlin	rostlina	k1gFnPc2
může	moct	k5eAaImIp3nS
více	hodně	k6eAd2
mechanismy	mechanismus	k1gInPc4
působit	působit	k5eAaImF
na	na	k7c4
patogenní	patogenní	k2eAgInPc4d1
organismy	organismus	k1gInPc4
a	a	k8xC
společný	společný	k2eAgInSc4d1
účinek	účinek	k1gInSc4
je	být	k5eAaImIp3nS
silnější	silný	k2eAgMnSc1d2
než	než	k8xS
účinek	účinek	k1gInSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
rostliny	rostlina	k1gFnPc4
oslabené	oslabený	k2eAgFnPc4d1
například	například	k6eAd1
abiotických	abiotický	k2eAgFnPc2d1
stresem	stres	k1gInSc7
<g/>
,	,	kIx,
hůře	zle	k6eAd2
odolávají	odolávat	k5eAaImIp3nP
napadení	napadení	k1gNnSc4
patogeny	patogen	k1gInPc4
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
dokonce	dokonce	k9
poškozovány	poškozován	k2eAgInPc1d1
i	i	k8xC
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
nejsou	být	k5eNaImIp3nP
primárně	primárně	k6eAd1
patogenní	patogenní	k2eAgMnPc4d1
<g/>
,	,	kIx,
destruenty	destruent	k1gInPc4
(	(	kIx(
<g/>
chřadnutí	chřadnutí	k1gNnSc1
a	a	k8xC
prosychání	prosychání	k1gNnSc1
borovic	borovice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
jsou	být	k5eAaImIp3nP
také	také	k9
popisována	popisován	k2eAgNnPc4d1
napadení	napadení	k1gNnPc4
hostitelů	hostitel	k1gMnPc2
několika	několik	k4yIc7
méně	málo	k6eAd2
významnými	významný	k2eAgInPc7d1
patogeny	patogen	k1gInPc7
současně	současně	k6eAd1
s	s	k7c7
významnými	významný	k2eAgInPc7d1
účinky	účinek	k1gInPc7
(	(	kIx(
<g/>
sypavka	sypavka	k1gFnSc1
smrku	smrk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
tvorba	tvorba	k1gFnSc1
rezistence	rezistence	k1gFnSc2
rostlinou	rostlina	k1gFnSc7
komplikovanější	komplikovaný	k2eAgFnSc7d2
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
rezistence	rezistence	k1gFnSc2
u	u	k7c2
rostlin	rostlina	k1gFnPc2
</s>
<s>
Podle	podle	k7c2
účinků	účinek	k1gInPc2
na	na	k7c4
typ	typ	k1gInSc4
patogenu	patogen	k1gInSc2
</s>
<s>
horizontální	horizontální	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
částečná	částečný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
hostitele	hostitel	k1gMnSc2
získaná	získaný	k2eAgFnSc1d1
vlivem	vlivem	k7c2
více	hodně	k6eAd2
genů	gen	k1gInPc2
malého	malý	k2eAgInSc2d1
účinku	účinek	k1gInSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
však	však	k9
nespecifická	specifický	k2eNgFnSc1d1
<g/>
,	,	kIx,
proti	proti	k7c3
všem	všecek	k3xTgInPc3
poddruhům	poddruh	k1gInPc3
a	a	k8xC
odchylným	odchylný	k2eAgInPc3d1
typům	typ	k1gInPc3
patogenního	patogenní	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
ochrana	ochrana	k1gFnSc1
je	být	k5eAaImIp3nS
stálá	stálý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
vertikální	vertikální	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
naprostá	naprostý	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
hostitele	hostitel	k1gMnSc2
ovšem	ovšem	k9
jen	jen	k6eAd1
vůči	vůči	k7c3
některým	některý	k3yIgInPc3
typům	typ	k1gInPc3
patogenního	patogenní	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
působením	působení	k1gNnSc7
jednoho	jeden	k4xCgInSc2
nebo	nebo	k8xC
několika	několik	k4yIc2
genů	gen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výskytu	výskyt	k1gInSc6
jiného	jiný	k2eAgInSc2d1
typu	typ	k1gInSc2
téhož	týž	k3xTgInSc2
druhu	druh	k1gInSc2
patogenního	patogenní	k2eAgInSc2d1
je	být	k5eAaImIp3nS
vertikální	vertikální	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
nefunkční	funkční	k2eNgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
obranná	obranný	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
není	být	k5eNaImIp3nS
vyvolána	vyvolat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Příkladem	příklad	k1gInSc7
vertikální	vertikální	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
je	být	k5eAaImIp3nS
rezistence	rezistence	k1gFnSc1
jabloní	jabloň	k1gFnPc2
vůči	vůči	k7c3
strupovitosti	strupovitost	k1gFnSc3
jabloní	jabloň	k1gFnPc2
pomocí	pomocí	k7c2
genu	gen	k1gInSc2
Vf	Vf	k1gFnSc2
<g/>
,	,	kIx,
Ventura	Ventura	kA
floribunda	floribunda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
způsobu	způsob	k1gInSc2
dědičnosti	dědičnost	k1gFnSc2
rezistence	rezistence	k1gFnSc2
</s>
<s>
monogenní	monogenní	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
řízena	řízen	k2eAgFnSc1d1
jedním	jeden	k4xCgInSc7
genem	gen	k1gInSc7
s	s	k7c7
výrazným	výrazný	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
</s>
<s>
oligogenní	oligogenní	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
řízena	řídit	k5eAaImNgFnS
několika	několik	k4yIc7
málo	málo	k6eAd1
geny	gen	k1gInPc1
s	s	k7c7
poměrně	poměrně	k6eAd1
silným	silný	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
</s>
<s>
polygenní	polygenní	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
řízena	řídit	k5eAaImNgFnS
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
genů	gen	k1gInPc2
s	s	k7c7
obvykle	obvykle	k6eAd1
slabším	slabý	k2eAgInSc7d2
účinkem	účinek	k1gInSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
genového	genový	k2eAgInSc2d1
účinku	účinek	k1gInSc2
</s>
<s>
geny	gen	k1gInPc1
velkého	velký	k2eAgInSc2d1
účinku	účinek	k1gInSc2
(	(	kIx(
<g/>
major	major	k1gMnSc1
geny	gen	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
malý	malý	k2eAgInSc4d1
počet	počet	k1gInSc4
genů	gen	k1gInPc2
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
velké	velký	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
projevu	projev	k1gInSc6
napadení	napadení	k1gNnSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
významně	významně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
vnějšími	vnější	k2eAgInPc7d1
faktory	faktor	k1gInPc7
<g/>
,	,	kIx,
projev	projev	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
vertikální	vertikální	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
geny	gen	k1gInPc1
malého	malý	k2eAgInSc2d1
účinku	účinek	k1gInSc2
(	(	kIx(
<g/>
minor	minor	k2eAgInPc1d1
geny	gen	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
více	hodně	k6eAd2
genů	gen	k1gInPc2
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
projev	projev	k1gInSc4
napadení	napadení	k1gNnSc2
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
spojitá	spojitý	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
ovlivněný	ovlivněný	k2eAgInSc1d1
vnějšími	vnější	k2eAgInPc7d1
faktory	faktor	k1gInPc7
<g/>
,	,	kIx,
projev	projev	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
horizontální	horizontální	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
jiných	jiný	k2eAgNnPc2d1
hledisek	hledisko	k1gNnPc2
</s>
<s>
Podle	podle	k7c2
hlediska	hledisko	k1gNnSc2
kvantita	kvantita	k1gFnSc1
<g/>
/	/	kIx~
<g/>
kvalita	kvalita	k1gFnSc1
</s>
<s>
rezistence	rezistence	k1gFnSc1
kvalitativní	kvalitativní	k2eAgFnSc2d1
–	–	k?
hodnocení	hodnocení	k1gNnSc3
podle	podle	k7c2
typu	typ	k1gInSc2
poškození	poškození	k1gNnSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
x	x	k?
není	být	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
rezistence	rezistence	k1gFnSc1
kvantitativní	kvantitativní	k2eAgFnSc2d1
–	–	k?
hodnocení	hodnocení	k1gNnSc3
podle	podle	k7c2
rozsahu	rozsah	k1gInSc2
poškození	poškození	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
počet	počet	k1gInSc4
lézí	léze	k1gFnPc2
na	na	k7c6
listech	list	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tolerance	tolerance	k1gFnSc1
–	–	k?
schopnost	schopnost	k1gFnSc4
omezit	omezit	k5eAaPmF
následky	následek	k1gInPc4
působení	působení	k1gNnSc2
patogenního	patogenní	k2eAgInSc2d1
organismu	organismus	k1gInSc2
napadeným	napadený	k2eAgMnSc7d1
hostitelem	hostitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tolerantní	tolerantní	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
nevykazují	vykazovat	k5eNaImIp3nP
vliv	vliv	k1gInSc4
infekce	infekce	k1gFnSc2
z	z	k7c2
hospodářského	hospodářský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
například	například	k6eAd1
pěstovány	pěstován	k2eAgFnPc1d1
tolerantní	tolerantní	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
slivoní	slivoň	k1gFnSc7
vůči	vůči	k7c3
šarce	šarka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toleranci	tolerance	k1gFnSc4
vůči	vůči	k7c3
napadení	napadení	k1gNnSc3
mšicemi	mšice	k1gFnPc7
lze	lze	k6eAd1
zvýšit	zvýšit	k5eAaPmF
vyváženou	vyvážený	k2eAgFnSc7d1
výživou	výživa	k1gFnSc7
hostitele	hostitel	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
růstové	růstový	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
hostitelské	hostitelský	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
</s>
<s>
rezistence	rezistence	k1gFnPc1
juvenilní	juvenilní	k2eAgFnPc1d1
–	–	k?
mladé	mladý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
jsou	být	k5eAaImIp3nP
rezistentní	rezistentní	k2eAgFnPc1d1
<g/>
,	,	kIx,
starší	starý	k2eAgFnPc1d2
rostliny	rostlina	k1gFnPc1
nejsou	být	k5eNaImIp3nP
(	(	kIx(
<g/>
březovník	březovník	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rezistence	rezistence	k1gFnSc1
adultivní	adultivnit	k5eAaPmIp3nS
–	–	k?
pouze	pouze	k6eAd1
mladé	mladý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
jsou	být	k5eAaImIp3nP
napadány	napadán	k2eAgFnPc1d1
(	(	kIx(
<g/>
padání	padání	k1gNnSc4
klíčních	klíční	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
indukované	indukovaný	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
</s>
<s>
Indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
je	být	k5eAaImIp3nS
stav	stav	k1gInSc4
zvýšené	zvýšený	k2eAgFnSc2d1
obranyschopnosti	obranyschopnost	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vlivem	vlivem	k7c2
podnětů	podnět	k1gInPc2
okolního	okolní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
schopnosti	schopnost	k1gFnSc2
rostliny	rostlina	k1gFnSc2
vzdorovat	vzdorovat	k5eAaImF
patogenům	patogen	k1gInPc3
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
krátkodobě	krátkodobě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
navozena	navozit	k5eAaBmNgFnS,k5eAaPmNgFnS
příbuzným	příbuzný	k2eAgInSc7d1
druhem	druh	k1gInSc7
nebo	nebo	k8xC
avirulentní	avirulentní	k2eAgFnSc7d1
rasou	rasa	k1gFnSc7
patogenu	patogen	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
aplikací	aplikace	k1gFnSc7
některých	některý	k3yIgFnPc2
chemických	chemický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
rasově	rasově	k6eAd1
nespecifická	specifický	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozlišit	rozlišit	k5eAaPmF
lze	lze	k6eAd1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
lokálně	lokálně	k6eAd1
získaná	získaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
(	(	kIx(
<g/>
LAR	LAR	kA
–	–	k?
local	local	k1gInSc1
acquired	acquired	k1gInSc1
resistance	resistance	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
systémově	systémově	k6eAd1
získaná	získaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
(	(	kIx(
<g/>
SAR	SAR	kA
–	–	k?
systemic	systemic	k1gMnSc1
acquired	acquired	k1gMnSc1
resistance	resistance	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
indukovaná	indukovaný	k2eAgFnSc1d1
systémová	systémový	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
(	(	kIx(
<g/>
ISR	ISR	kA
–	–	k?
induced	induced	k1gInSc1
systemic	systemic	k1gMnSc1
resistance	resistance	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
proti	proti	k7c3
poranění	poranění	k1gNnSc1
býložravým	býložravý	k2eAgInSc7d1
hmyzem	hmyz	k1gInSc7
</s>
<s>
Popsána	popsán	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
proti	proti	k7c3
padlí	padlí	k1gNnSc3
travnímu	travní	k2eAgInSc3d1
pomocí	pomoc	k1gFnSc7
extraktu	extrakt	k1gInSc2
z	z	k7c2
křídlatky	křídlatka	k1gFnSc2
sachalinské	sachalinský	k2eAgFnSc2d1
nebo	nebo	k8xC
zázvoru	zázvor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
omezení	omezení	k1gNnSc1
reprodukce	reprodukce	k1gFnSc2
mšic	mšice	k1gFnPc2
jako	jako	k8xC,k8xS
agrotechnické	agrotechnický	k2eAgNnSc1d1
opatření	opatření	k1gNnSc1
pomocí	pomocí	k7c2
zvýšené	zvýšený	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
draslíku	draslík	k1gInSc2
v	v	k7c6
pěstovaných	pěstovaný	k2eAgFnPc6d1
rostlinách	rostlina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
formy	forma	k1gFnPc1
rezistence	rezistence	k1gFnSc2
</s>
<s>
pseudorezistence	pseudorezistence	k1gFnSc1
–	–	k?
hostitel	hostitel	k1gMnSc1
není	být	k5eNaImIp3nS
napaden	napadnout	k5eAaPmNgMnS
nebo	nebo	k8xC
poškozen	poškodit	k5eAaPmNgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
odlišných	odlišný	k2eAgInPc2d1
morfologických	morfologický	k2eAgInPc2d1
nebo	nebo	k8xC
fyziologických	fyziologický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
omezují	omezovat	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
napadení	napadení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
imunita	imunita	k1gFnSc1
–	–	k?
vlastnost	vlastnost	k1gFnSc1
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
hostiteli	hostitel	k1gMnSc3
propůjčuje	propůjčovat	k5eAaImIp3nS
naprostou	naprostý	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
vůči	vůči	k7c3
napadení	napadení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
specifická	specifický	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
–	–	k?
je	být	k5eAaImIp3nS
navozena	navozen	k2eAgFnSc1d1
účinkem	účinek	k1gInSc7
konkrétního	konkrétní	k2eAgInSc2d1
genu	gen	k1gInSc2
vůči	vůči	k7c3
konkrétní	konkrétní	k2eAgFnSc3d1
rase	rasa	k1gFnSc3
patogenu	patogen	k1gInSc2
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
vertikální	vertikální	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
částečná	částečný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
–	–	k?
stav	stav	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
u	u	k7c2
hostitele	hostitel	k1gMnSc2
za	za	k7c2
vysokého	vysoký	k2eAgInSc2d1
infekčního	infekční	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
pomalejšímu	pomalý	k2eAgInSc3d2
rozvoji	rozvoj	k1gInSc3
choroby	choroba	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
náchylným	náchylný	k2eAgInSc7d1
genotypem	genotyp	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Obranné	obranný	k2eAgInPc1d1
mechanizmy	mechanizmus	k1gInPc1
rostlin	rostlina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
ŘEPKOVÁ	řepkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odolnost	odolnost	k1gFnSc1
rostlin	rostlina	k1gFnPc2
k	k	k7c3
patogenům	patogen	k1gInPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
Univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rezistence	rezistence	k1gFnPc4
podle	podle	k7c2
způsobu	způsob	k1gInSc2
dědění	dědění	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Slovníček	slovníček	k1gInSc1
základních	základní	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
VONDRÁŠKOVÁ	Vondrášková	k1gFnSc1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukce	redukce	k1gFnSc1
škodlivosti	škodlivost	k1gFnSc2
škůdců	škůdce	k1gMnPc2
polních	polní	k2eAgFnPc2d1
plodin	plodina	k1gFnPc2
zvýšením	zvýšení	k1gNnSc7
rezistence	rezistence	k1gFnPc1
a	a	k8xC
tolerance	tolerance	k1gFnPc1
rostlin	rostlina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Biologická	biologický	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
a	a	k8xC
indukovaná	indukovaný	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
rostlin	rostlina	k1gFnPc2
k	k	k7c3
chorobám	choroba	k1gFnPc3
a	a	k8xC
škůdcům	škůdce	k1gMnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
typ	typ	k1gInSc1
rezistence	rezistence	k1gFnSc2
k	k	k7c3
herbicidům	herbicid	k1gInPc3
</s>
<s>
ANTIFUNGÁLNÍ	ANTIFUNGÁLNÍ	kA
PROTEINY	protein	k1gInPc4
ROSTLIN	rostlina	k1gFnPc2
<g/>
,	,	kIx,
KLASIFIKACE	klasifikace	k1gFnSc2
<g/>
,	,	kIx,
CHARAKTERISTIKA	charakteristikon	k1gNnPc4
<g/>
,	,	kIx,
MOŽNOSTI	možnost	k1gFnPc4
VYUŽITÍ	využití	k1gNnSc2
</s>
<s>
OBRANNÁ	obranný	k2eAgFnSc1d1
REAKCE	reakce	k1gFnSc1
ROSTLIN	rostlina	k1gFnPc2
<g/>
,	,	kIx,
SLEDOVÁNÍ	sledování	k1gNnSc4
OBRANNÉ	obranný	k2eAgFnSc2d1
REAKCE	reakce	k1gFnSc2
RÉVY	réva	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Zahrada	zahrada	k1gFnSc1
a	a	k8xC
zahradnictví	zahradnictví	k1gNnSc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
</s>
