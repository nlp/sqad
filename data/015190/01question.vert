<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
ochranným	ochranný	k2eAgInPc3d1
prvkům	prvek	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
až	až	k6eAd1
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnSc1
po	po	k7c6
napadení	napadení	k1gNnSc6
hostitelské	hostitelský	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
patogenem	patogen	k1gInSc7
<g/>
?	?	kIx.
</s>