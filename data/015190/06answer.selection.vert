<s>
Indukované	indukovaný	k2eAgFnPc1d1
bariéry	bariéra	k1gFnPc1
nebo	nebo	k8xC
mechanismy	mechanismus	k1gInPc1
jsou	být	k5eAaImIp3nP
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnPc1
po	po	k7c6
napadení	napadení	k1gNnSc6
hostitele	hostitel	k1gMnSc2
patogenem	patogen	k1gInSc7
<g/>
.	.	kIx.
</s>