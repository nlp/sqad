<p>
<s>
Gambrinus	gambrinus	k1gInSc1	gambrinus
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgNnPc2d3	nejprodávanější
piv	pivo	k1gNnPc2	pivo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
mu	on	k3xPp3gMnSc3	on
patřila	patřit	k5eAaImAgFnS	patřit
zhruba	zhruba	k6eAd1	zhruba
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
českého	český	k2eAgInSc2d1	český
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Gambrinus	gambrinus	k1gInSc1	gambrinus
Plzeň	Plzeň	k1gFnSc4	Plzeň
získal	získat	k5eAaPmAgInS	získat
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
Plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
pod	pod	k7c7	pod
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
vařilo	vařit	k5eAaImAgNnS	vařit
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tamní	tamní	k2eAgMnSc1d1	tamní
První	první	k4xOgInSc1	první
akciový	akciový	k2eAgInSc1d1	akciový
pivovar	pivovar	k1gInSc1	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
králi	král	k1gMnSc6	král
Gambrinovi	Gambrinus	k1gMnSc6	Gambrinus
se	se	k3xPyFc4	se
tradovala	tradovat	k5eAaImAgFnS	tradovat
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
v	v	k7c6	v
bavorské	bavorský	k2eAgFnSc6d1	bavorská
kronice	kronika	k1gFnSc6	kronika
Burkarta	Burkarta	k1gFnSc1	Burkarta
Wallise	Wallise	k1gFnSc1	Wallise
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
pověsti	pověst	k1gFnSc2	pověst
začal	začít	k5eAaPmAgMnS	začít
král	král	k1gMnSc1	král
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
naučil	naučit	k5eAaPmAgMnS	naučit
od	od	k7c2	od
Isis	Isis	k1gFnSc1	Isis
<g/>
,	,	kIx,	,
staroegyptské	staroegyptský	k2eAgFnPc1d1	staroegyptská
bohyně	bohyně	k1gFnPc1	bohyně
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
hodokvasu	hodokvasu	k?	hodokvasu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
vaření	vaření	k1gNnSc2	vaření
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
než	než	k8xS	než
ona	onen	k3xDgFnSc1	onen
středověká	středověký	k2eAgFnSc1d1	středověká
pověst	pověst	k1gFnSc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
vařilo	vařit	k5eAaImAgNnS	vařit
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
i	i	k8xC	i
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pšeničném	pšeničný	k2eAgNnSc6d1	pšeničné
pivě	pivo	k1gNnSc6	pivo
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
i	i	k9	i
Bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
<g/>
Král	Král	k1gMnSc1	Král
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
má	mít	k5eAaImIp3nS	mít
zdánlivě	zdánlivě	k6eAd1	zdánlivě
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zkomoleninou	zkomolenina	k1gFnSc7	zkomolenina
titulu	titul	k1gInSc2	titul
Jean	Jean	k1gMnSc1	Jean
Primus	primus	k1gMnSc1	primus
=	=	kIx~	=
Jan	Jan	k1gMnSc1	Jan
První	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Brabantský	Brabantský	k2eAgMnSc1d1	Brabantský
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Worringenu	Worringen	k1gInSc2	Worringen
nedaleko	nedaleko	k7c2	nedaleko
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1288	[number]	k4	1288
porazil	porazit	k5eAaPmAgInS	porazit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spojenci	spojenec	k1gMnPc7	spojenec
lucemburského	lucemburský	k2eAgMnSc4d1	lucemburský
hraběte	hrabě	k1gMnSc4	hrabě
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
definitivně	definitivně	k6eAd1	definitivně
připadlo	připadnout	k5eAaPmAgNnS	připadnout
vévodům	vévoda	k1gMnPc3	vévoda
brabantským	brabantský	k2eAgFnPc3d1	brabantská
Limbursko	Limbursko	k1gNnSc1	Limbursko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prosbu	prosba	k1gFnSc4	prosba
bruselského	bruselský	k2eAgInSc2d1	bruselský
sladovnického	sladovnický	k2eAgInSc2d1	sladovnický
cechu	cech	k1gInSc2	cech
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
(	(	kIx(	(
<g/>
patronem	patron	k1gInSc7	patron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
býval	bývat	k5eAaImAgInS	bývat
v	v	k7c6	v
cechovních	cechovní	k2eAgFnPc6d1	cechovní
místnostech	místnost	k1gFnPc6	místnost
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
jako	jako	k9	jako
opulentní	opulentní	k2eAgMnSc1d1	opulentní
usměvavý	usměvavý	k2eAgMnSc1d1	usměvavý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
z	z	k7c2	z
chmelových	chmelový	k2eAgFnPc2d1	chmelová
ratolestí	ratolest	k1gFnPc2	ratolest
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
s	s	k7c7	s
pohárem	pohár	k1gInSc7	pohár
pěnícího	pěnící	k2eAgNnSc2d1	pěnící
piva	pivo	k1gNnSc2	pivo
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
častým	častý	k2eAgMnSc7d1	častý
nástupcem	nástupce	k1gMnSc7	nástupce
římského	římský	k2eAgMnSc2d1	římský
boha	bůh	k1gMnSc2	bůh
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
a	a	k8xC	a
novověkým	novověký	k2eAgInSc7d1	novověký
patronem	patron	k1gInSc7	patron
pivovarnictví	pivovarnictví	k1gNnSc2	pivovarnictví
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Brabantsku	Brabantsek	k1gInSc6	Brabantsek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
severní	severní	k2eAgFnSc2d1	severní
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
(	(	kIx(	(
<g/>
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Lovaň	Lovaň	k1gFnPc1	Lovaň
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Breda	Breda	k1gMnSc1	Breda
<g/>
,	,	kIx,	,
Hertogenbosch	Hertogenbosch	k1gMnSc1	Hertogenbosch
<g/>
,	,	kIx,	,
Tilburg	Tilburg	k1gMnSc1	Tilburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
barokní	barokní	k2eAgFnSc2d1	barokní
také	také	k9	také
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
"	"	kIx"	"
<g/>
pivařských	pivařský	k2eAgFnPc6d1	pivařská
<g/>
"	"	kIx"	"
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
tradice	tradice	k1gFnSc1	tradice
krále	král	k1gMnSc2	král
Gambrina	Gambrinus	k1gMnSc2	Gambrinus
doložena	doložit	k5eAaPmNgFnS	doložit
teprve	teprve	k9	teprve
spíše	spíše	k9	spíše
až	až	k9	až
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
obrazy	obraz	k1gInPc4	obraz
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
či	či	k8xC	či
názvem	název	k1gInSc7	název
pivnice	pivnice	k1gFnSc2	pivnice
"	"	kIx"	"
<g/>
U	u	k7c2	u
krále	král	k1gMnSc2	král
Brabantského	Brabantský	k2eAgMnSc2d1	Brabantský
<g/>
"	"	kIx"	"
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
už	už	k9	už
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
i	i	k8xC	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
všeobecně	všeobecně	k6eAd1	všeobecně
populární	populární	k2eAgFnPc1d1	populární
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
propagaci	propagace	k1gFnSc6	propagace
ho	on	k3xPp3gMnSc4	on
používal	používat	k5eAaImAgInS	používat
například	například	k6eAd1	například
pražský	pražský	k2eAgInSc1d1	pražský
pivovar	pivovar	k1gInSc1	pivovar
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Gambrinus	gambrinus	k1gInSc1	gambrinus
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
prodávány	prodávat	k5eAaImNgInP	prodávat
také	také	k6eAd1	také
známkově	známkově	k6eAd1	známkově
chráněné	chráněný	k2eAgInPc4d1	chráněný
"	"	kIx"	"
<g/>
pivní	pivní	k2eAgInPc4d1	pivní
bonbony	bonbon	k1gInPc4	bonbon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
názvu	název	k1gInSc2	název
tohoto	tento	k3xDgNnSc2	tento
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
vedly	vést	k5eAaImAgFnP	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
prozaické	prozaický	k2eAgInPc4d1	prozaický
reklamní	reklamní	k2eAgInPc4d1	reklamní
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Pilsner	Pilsner	k1gMnSc1	Pilsner
Kaiserquell	Kaiserquell	k1gMnSc1	Kaiserquell
byl	být	k5eAaImAgMnS	být
až	až	k6eAd1	až
příliš	příliš	k6eAd1	příliš
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
starým	starý	k2eAgNnSc7d1	staré
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc4	co
však	však	k9	však
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
fungovalo	fungovat	k5eAaImAgNnS	fungovat
jako	jako	k9	jako
dobrý	dobrý	k2eAgInSc4d1	dobrý
marketingový	marketingový	k2eAgInSc4d1	marketingový
tahák	tahák	k1gInSc4	tahák
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
už	už	k6eAd1	už
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
anachronismus	anachronismus	k1gInSc4	anachronismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc7	druh
českého	český	k2eAgNnSc2d1	české
piva	pivo	k1gNnSc2	pivo
značky	značka	k1gFnSc2	značka
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
==	==	k?	==
</s>
</p>
<p>
<s>
Nepasterizovaná	pasterizovaný	k2eNgFnSc1d1	nepasterizovaná
10	[number]	k4	10
Gambrinus	gambrinus	k1gInSc4	gambrinus
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašené	kvašený	k2eAgNnSc4d1	kvašené
světlé	světlý	k2eAgNnSc4d1	světlé
výčepní	výčepní	k2eAgNnSc4d1	výčepní
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
nepasterizované	pasterizovaný	k2eNgNnSc4d1	nepasterizované
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
4,2	[number]	k4	4,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
Nepasterizovaná	pasterizovaný	k2eNgFnSc1d1	nepasterizovaná
12	[number]	k4	12
Gambrinus	gambrinus	k1gInSc4	gambrinus
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašený	kvašený	k2eAgInSc1d1	kvašený
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
<g/>
,	,	kIx,	,
nepasterizovaný	pasterizovaný	k2eNgInSc1d1	nepasterizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
5,2	[number]	k4	5,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
Nefiltrovaný	filtrovaný	k2eNgMnSc1d1	nefiltrovaný
ležák	ležák	k1gMnSc1	ležák
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašený	kvašený	k2eAgInSc1d1	kvašený
nefiltrovaný	filtrovaný	k2eNgInSc1d1	nefiltrovaný
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
<g/>
,	,	kIx,	,
nepasterizovaný	pasterizovaný	k2eNgInSc1d1	nepasterizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
4,8	[number]	k4	4,8
%	%	kIx~	%
</s>
</p>
<p>
<s>
Gambrinus	gambrinus	k1gInSc1	gambrinus
Originál	originál	k1gInSc1	originál
10	[number]	k4	10
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašené	kvašený	k2eAgNnSc1d1	kvašené
světlé	světlý	k2eAgNnSc1d1	světlé
výčepní	výčepní	k2eAgNnSc1d1	výčepní
pivo	pivo	k1gNnSc1	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
4,3	[number]	k4	4,3
%	%	kIx~	%
</s>
</p>
<p>
<s>
Gambrinus	gambrinus	k1gInSc1	gambrinus
Plná	plný	k2eAgFnSc1d1	plná
12	[number]	k4	12
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašený	kvašený	k2eAgInSc1d1	kvašený
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
5,2	[number]	k4	5,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
Gambrinus	gambrinus	k1gInSc1	gambrinus
Polotmavá	polotmavý	k2eAgFnSc1d1	polotmavá
12	[number]	k4	12
<g/>
:	:	kIx,	:
Spodně	spodně	k6eAd1	spodně
kvašený	kvašený	k2eAgInSc1d1	kvašený
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
5,2	[number]	k4	5,2
%	%	kIx~	%
<g/>
Gambrinus	gambrinus	k1gInSc1	gambrinus
Dry	Dry	k1gFnSc2	Dry
<g/>
:	:	kIx,	:
Světlé	světlý	k2eAgNnSc1d1	světlé
pivo	pivo	k1gNnSc1	pivo
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
obsahem	obsah	k1gInSc7	obsah
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
4,0	[number]	k4	4,0
%	%	kIx~	%
<g/>
Pod	pod	k7c7	pod
všemi	všecek	k3xTgInPc7	všecek
druhy	druh	k1gInPc7	druh
českého	český	k2eAgNnSc2d1	české
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgMnS	podepsat
vrchní	vrchní	k2eAgMnSc1d1	vrchní
sládek	sládek	k1gMnSc1	sládek
pan	pan	k1gMnSc1	pan
Jan	Jan	k1gMnSc1	Jan
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
pivní	pivní	k2eAgFnSc1d1	pivní
etiketa	etiketa	k1gFnSc1	etiketa
piva	pivo	k1gNnSc2	pivo
značky	značka	k1gFnSc2	značka
Gambrinus	gambrinus	k1gInSc1	gambrinus
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
pivovary	pivovar	k1gInPc1	pivovar
užívající	užívající	k2eAgFnSc4d1	užívající
značku	značka	k1gFnSc4	značka
Gambrinus	gambrinus	k1gInSc1	gambrinus
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
značka	značka	k1gFnSc1	značka
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
i	i	k9	i
mnohými	mnohý	k2eAgInPc7d1	mnohý
jinými	jiný	k2eAgInPc7d1	jiný
pivovary	pivovar	k1gInPc7	pivovar
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mendigu	Mendig	k1gInSc6	Mendig
(	(	kIx(	(
<g/>
Porýní-Falc	Porýní-Falc	k1gFnSc1	Porýní-Falc
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
pivní	pivní	k2eAgFnSc1d1	pivní
slavnost	slavnost	k1gFnSc1	slavnost
Gambrinusfest	Gambrinusfest	k1gFnSc1	Gambrinusfest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Gambrinus	gambrinus	k1gInSc1	gambrinus
ve	v	k7c6	v
Weiden	Weidno	k1gNnPc2	Weidno
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Oberpfalz	Oberpfalz	k1gInSc1	Oberpfalz
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Gambrinusbräu	Gambrinusbräus	k1gInSc2	Gambrinusbräus
<g/>
,	,	kIx,	,
Oberhaid	Oberhaid	k1gInSc1	Oberhaid
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Gambrinus	gambrinus	k1gInSc1	gambrinus
<g/>
,	,	kIx,	,
Nagold	Nagold	k1gInSc1	Nagold
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Gambrinus	gambrinus	k1gInSc1	gambrinus
<g/>
,	,	kIx,	,
Naila	Naila	k1gFnSc1	Naila
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Mohrenbrauerei	Mohrenbrauere	k1gFnSc2	Mohrenbrauere
August	August	k1gMnSc1	August
Huber	Huber	k1gMnSc1	Huber
(	(	kIx(	(
<g/>
Vorarlberg	Vorarlberg	k1gInSc1	Vorarlberg
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
černé	černý	k2eAgNnSc1d1	černé
pivo	pivo	k1gNnSc1	pivo
této	tento	k3xDgFnSc2	tento
značky	značka	k1gFnSc2	značka
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Ottakringer	Ottakringra	k1gFnPc2	Ottakringra
Brauerei	Brauere	k1gFnSc2	Brauere
AG	AG	kA	AG
vaří	vařit	k5eAaImIp3nS	vařit
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
Gambrinus	gambrinus	k1gInSc1	gambrinus
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Hancock	Hancocka	k1gFnPc2	Hancocka
ve	v	k7c4	v
Skive	Skiev	k1gFnPc4	Skiev
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc4	Dánsko
</s>
</p>
<p>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
Grivita	Grivita	k1gFnSc1	Grivita
<g/>
,	,	kIx,	,
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gambrinus	gambrinus	k1gInSc1	gambrinus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Gambrinus	gambrinus	k1gInSc1	gambrinus
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
českého	český	k2eAgNnSc2d1	české
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
pivovaru	pivovar	k1gInSc2	pivovar
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
rakouského	rakouský	k2eAgNnSc2d1	rakouské
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
</s>
</p>
<p>
<s>
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
<g/>
́	́	k?	́
Mug	Mug	k1gMnSc1	Mug
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
pivovary	pivovar	k1gInPc1	pivovar
Gambrinus	gambrinus	k1gInSc4	gambrinus
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
Originál	originál	k1gMnSc1	originál
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
Excelent	Excelent	k1gMnSc1	Excelent
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
piva	pivo	k1gNnSc2	pivo
Gambrinus	gambrinus	k1gInSc1	gambrinus
Premium	Premium	k1gNnSc1	Premium
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
piva	pivo	k1gNnSc2	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
Dry	Dry	k1gMnSc1	Dry
</s>
</p>
<p>
<s>
Námořnická	námořnický	k2eAgFnSc1d1	námořnická
pivnice	pivnice	k1gFnSc1	pivnice
v	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
pivem	pivo	k1gNnSc7	pivo
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
</s>
</p>
