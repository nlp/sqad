<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
symfonickém	symfonický	k2eAgMnSc6d1	symfonický
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
písní	píseň	k1gFnPc2	píseň
tradiční	tradiční	k2eAgInPc1d1	tradiční
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
uváděné	uváděný	k2eAgFnPc4d1	uváděná
jako	jako	k8xS	jako
'	'	kIx"	'
<g/>
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
'	'	kIx"	'
vokály	vokál	k1gInPc1	vokál
<g/>
.	.	kIx.	.
</s>
