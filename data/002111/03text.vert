<s>
Québec	Québec	k1gInSc4	Québec
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
[	[	kIx(	[
<g/>
kebek	kebek	k1gMnSc1	kebek
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
též	též	k9	též
Ville	Ville	k1gFnSc1	Ville
de	de	k?	de
Québec	Québec	k1gInSc1	Québec
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Quebec	Quebec	k1gMnSc1	Quebec
City	City	k1gFnSc2	City
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gMnSc1	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Quebecké	quebecký	k2eAgNnSc1d1	quebecké
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Vieux-Québec	Vieux-Québec	k1gInSc1	Vieux-Québec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c6	na
Seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
a	a	k8xC	a
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnPc4d3	nejstarší
americká	americký	k2eAgNnPc4d1	americké
města	město	k1gNnPc4	město
ležící	ležící	k2eAgNnPc4d1	ležící
severně	severně	k6eAd1	severně
od	od	k7c2	od
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
městské	městský	k2eAgFnPc1d1	městská
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Málokteré	málokterý	k3yIgNnSc1	málokterý
město	město	k1gNnSc1	město
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
tak	tak	k6eAd1	tak
evropský	evropský	k2eAgInSc4d1	evropský
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
založil	založit	k5eAaPmAgMnS	založit
Francouz	Francouz	k1gMnSc1	Francouz
Samuel	Samuel	k1gMnSc1	Samuel
de	de	k?	de
Champlain	Champlain	k1gMnSc1	Champlain
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gMnSc4	on
algonkinským	algonkinský	k2eAgInSc7d1	algonkinský
výrazem	výraz	k1gInSc7	výraz
Kebec	Kebec	k1gMnSc1	Kebec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
o	o	k7c4	o
73	[number]	k4	73
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
plavbě	plavba	k1gFnSc6	plavba
přistál	přistát	k5eAaImAgMnS	přistát
francouzský	francouzský	k2eAgMnSc1d1	francouzský
objevitel	objevitel	k1gMnSc1	objevitel
Jacques	Jacques	k1gMnSc1	Jacques
Cartier	Cartier	k1gMnSc1	Cartier
a	a	k8xC	a
nalezl	nalézt	k5eAaBmAgMnS	nalézt
tu	ten	k3xDgFnSc4	ten
irokézskou	irokézský	k2eAgFnSc4d1	Irokézská
osadu	osada	k1gFnSc4	osada
Stadacona	Stadacon	k1gMnSc2	Stadacon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gInSc4	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
vrátili	vrátit	k5eAaPmAgMnP	vrátit
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Québec	Québec	k1gMnSc1	Québec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
centrem	centr	k1gMnSc7	centr
kolonie	kolonie	k1gFnSc2	kolonie
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Střety	střet	k1gInPc1	střet
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
opakovaly	opakovat	k5eAaImAgFnP	opakovat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1759	[number]	k4	1759
Angličané	Angličan	k1gMnPc1	Angličan
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Abrahamových	Abrahamův	k2eAgFnPc6d1	Abrahamova
pláních	pláň	k1gFnPc6	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
přiřkla	přiřknout	k5eAaPmAgFnS	přiřknout
celou	celý	k2eAgFnSc4d1	celá
Kanadu	Kanada	k1gFnSc4	Kanada
anglické	anglický	k2eAgFnSc3d1	anglická
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
pokusili	pokusit	k5eAaPmAgMnP	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
Québec	Québec	k1gInSc4	Québec
američtí	americký	k2eAgMnPc1d1	americký
revolucionáři	revolucionář	k1gMnPc1	revolucionář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěli	uspět	k5eNaPmAgMnP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Kanady	Kanada	k1gFnSc2	Kanada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
Québec	Québec	k1gMnSc1	Québec
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
muselo	muset	k5eAaImAgNnS	muset
později	pozdě	k6eAd2	pozdě
dočasně	dočasně	k6eAd1	dočasně
přepustit	přepustit	k5eAaPmF	přepustit
Montréalu	Montréala	k1gFnSc4	Montréala
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
provinční	provinční	k2eAgFnSc1d1	provinční
vláda	vláda	k1gFnSc1	vláda
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Sarrazin	Sarrazin	k1gMnSc1	Sarrazin
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
-	-	kIx~	-
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Marc	Marc	k1gInSc4	Marc
Garneau	Garneaus	k1gInSc2	Garneaus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
Guy	Guy	k1gMnSc2	Guy
Laliberté	Lalibertý	k2eAgFnSc2d1	Lalibertý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
Alain	Alain	k1gMnSc1	Alain
Vigneault	Vigneault	k1gMnSc1	Vigneault
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Kevin	Kevin	k1gMnSc1	Kevin
Dineen	Dineen	k2eAgMnSc1d1	Dineen
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Francis	Francis	k1gFnSc2	Francis
Leclerc	Leclerc	k1gFnSc1	Leclerc
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Martin	Martin	k1gMnSc1	Martin
Biron	Biron	k1gMnSc1	Biron
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Mathieu	Mathieus	k1gInSc2	Mathieus
Biron	Biron	k1gMnSc1	Biron
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Simon	Simon	k1gMnSc1	Simon
Gagné	Gagné	k2eAgMnSc1d1	Gagné
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g />
.	.	kIx.	.
</s>
<s>
Erik	Erik	k1gMnSc1	Erik
Guay	Guaa	k1gFnSc2	Guaa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sjezdový	sjezdový	k2eAgMnSc1d1	sjezdový
lyžař	lyžař	k1gMnSc1	lyžař
Yan	Yan	k1gMnSc1	Yan
Šťastný	Šťastný	k1gMnSc1	Šťastný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Paul	Paul	k1gMnSc1	Paul
Šťastný	Šťastný	k1gMnSc1	Šťastný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Québec	Québec	k1gInSc1	Québec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Quebec	Quebec	k1gInSc1	Quebec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
