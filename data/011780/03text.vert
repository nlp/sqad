<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Weiherburg	Weiherburg	k1gInSc1	Weiherburg
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgNnSc4d1	gotické
sídlo	sídlo	k1gNnSc4	sídlo
umístěné	umístěný	k2eAgNnSc4d1	umístěné
v	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Hötting	Hötting	k1gInSc4	Hötting
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
Alpenzoo	Alpenzoo	k6eAd1	Alpenzoo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
místního	místní	k2eAgInSc2d1	místní
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přemístění	přemístění	k1gNnSc6	přemístění
knížecího	knížecí	k2eAgNnSc2d1	knížecí
sídla	sídlo	k1gNnSc2	sídlo
z	z	k7c2	z
Merana	Meran	k1gMnSc2	Meran
do	do	k7c2	do
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
Fridrichem	Fridrich	k1gMnSc7	Fridrich
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Habsburským	habsburský	k2eAgInSc7d1	habsburský
<g/>
,	,	kIx,	,
šlechtici	šlechtic	k1gMnPc1	šlechtic
a	a	k8xC	a
bohatí	bohatý	k2eAgMnPc1d1	bohatý
občané	občan	k1gMnPc1	občan
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
řadu	řad	k1gInSc2	řad
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
postavil	postavit	k5eAaPmAgInS	postavit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1460	[number]	k4	1460
Christian	Christian	k1gMnSc1	Christian
Tänzl	Tänzl	k1gMnSc1	Tänzl
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Tänzlů	Tänzl	k1gMnPc2	Tänzl
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
těžbě	těžba	k1gFnSc3	těžba
stříbra	stříbro	k1gNnSc2	stříbro
stala	stát	k5eAaPmAgFnS	stát
bohatou	bohatý	k2eAgFnSc7d1	bohatá
a	a	k8xC	a
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
také	také	k9	také
hrad	hrad	k1gInSc4	hrad
Tratzberg	Tratzberg	k1gInSc1	Tratzberg
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
prodal	prodat	k5eAaPmAgMnS	prodat
Tänzl	Tänzl	k1gMnSc3	Tänzl
zámek	zámek	k1gInSc4	zámek
tyrolskému	tyrolský	k2eAgMnSc3d1	tyrolský
panovníkovi	panovník	k1gMnSc3	panovník
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
abdikaci	abdikace	k1gFnSc6	abdikace
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
přešlo	přejít	k5eAaPmAgNnS	přejít
panství	panství	k1gNnSc1	panství
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
Maximiliána	Maximilián	k1gMnSc4	Maximilián
I.	I.	kA	I.
On	on	k3xPp3gMnSc1	on
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
Weiherburg	Weiherburg	k1gInSc4	Weiherburg
v	v	k7c4	v
1493	[number]	k4	1493
na	na	k7c6	na
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
sídlo	sídlo	k1gNnSc4	sídlo
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
svému	svůj	k3xOyFgMnSc3	svůj
kancléři	kancléř	k1gMnSc3	kancléř
Oswaldu	Oswald	k1gMnSc3	Oswald
von	von	k1gInSc4	von
Hausen	Hausen	k2eAgInSc4d1	Hausen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
koupil	koupit	k5eAaPmAgMnS	koupit
Weiherburg	Weiherburg	k1gMnSc1	Weiherburg
Veit	Veit	k1gMnSc1	Veit
Langenmantel	Langenmantel	k1gMnSc1	Langenmantel
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
postavil	postavit	k5eAaPmAgMnS	postavit
freskami	freska	k1gFnPc7	freska
zdobenou	zdobený	k2eAgFnSc4d1	zdobená
přístavbu	přístavba	k1gFnSc4	přístavba
Langenmantel	Langenmantela	k1gFnPc2	Langenmantela
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolský	tyrolský	k2eAgInSc1d1	tyrolský
zámek	zámek	k1gInSc1	zámek
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
zoo	zoo	k1gFnSc4	zoo
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Alpenzoo	Alpenzoo	k1gNnSc4	Alpenzoo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
změnách	změna	k1gFnPc6	změna
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
převzalo	převzít	k5eAaPmAgNnS	převzít
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
město	město	k1gNnSc1	město
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976-1978	[number]	k4	1976-1978
ji	on	k3xPp3gFnSc4	on
nechalo	nechat	k5eAaPmAgNnS	nechat
zrestaurovat	zrestaurovat	k5eAaPmF	zrestaurovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Weiherburg	Weiherburg	k1gInSc1	Weiherburg
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnPc4	Anna
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1481-1513	[number]	k4	1481-1513
a	a	k8xC	a
ozdobena	ozdoben	k2eAgFnSc1d1	ozdobena
freskami	freska	k1gFnPc7	freska
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Stricknera	Strickner	k1gMnSc2	Strickner
<g/>
.	.	kIx.	.
</s>
<s>
Oltář	Oltář	k1gInSc1	Oltář
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Pannu	Panna	k1gFnSc4	Panna
Marii	Maria	k1gFnSc3	Maria
mezi	mezi	k7c7	mezi
sv.	sv.	kA	sv.
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
sv.	sv.	kA	sv.
Krištofem	Krištof	k1gMnSc7	Krištof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Schloss	Schloss	k1gInSc1	Schloss
Weiherburg	Weiherburg	k1gInSc1	Weiherburg
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
