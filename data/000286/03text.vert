<s>
Lyže	lyže	k1gFnPc1	lyže
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
desky	deska	k1gFnPc1	deska
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
se	s	k7c7	s
zvednutou	zvednutý	k2eAgFnSc7d1	zvednutá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
lyžování	lyžování	k1gNnSc3	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ze	z	k7c2	z
sklolaminátů	sklolaminát	k1gInPc2	sklolaminát
anebo	anebo	k8xC	anebo
jiných	jiný	k2eAgInPc2d1	jiný
kompozitních	kompozitní	k2eAgInPc2d1	kompozitní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Připevňují	připevňovat	k5eAaImIp3nP	připevňovat
se	se	k3xPyFc4	se
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
pomocí	pomocí	k7c2	pomocí
lyžařského	lyžařský	k2eAgNnSc2d1	lyžařské
vázání	vázání	k1gNnSc2	vázání
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnPc1	lyže
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
způsobů	způsob	k1gInPc2	způsob
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
důležitým	důležitý	k2eAgNnPc3d1	důležité
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
jediným	jediný	k2eAgInSc7d1	jediný
prostředkem	prostředek	k1gInSc7	prostředek
umožňující	umožňující	k2eAgInSc1d1	umožňující
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
zasněženém	zasněžený	k2eAgNnSc6d1	zasněžené
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
ze	z	k7c2	z
sněžnic	sněžnice	k1gFnPc2	sněžnice
jejich	jejich	k3xOp3gNnSc7	jejich
postupným	postupný	k2eAgNnSc7d1	postupné
prodlužováním	prodlužování	k1gNnSc7	prodlužování
a	a	k8xC	a
zužováním	zužování	k1gNnSc7	zužování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
používaly	používat	k5eAaImAgFnP	používat
lyže	lyže	k1gFnPc1	lyže
jak	jak	k8xS	jak
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
se	se	k3xPyFc4	se
tak	tak	k9	tak
široká	široký	k2eAgFnSc1d1	široká
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
závody	závod	k1gInPc1	závod
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
putoval	putovat	k5eAaImAgMnS	putovat
norský	norský	k2eAgMnSc1d1	norský
cestovatel	cestovatel	k1gMnSc1	cestovatel
Fridtjof	Fridtjof	k1gMnSc1	Fridtjof
Nansen	Nansen	k2eAgMnSc1d1	Nansen
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výprava	výprava	k1gFnSc1	výprava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
horaly	horal	k1gMnPc4	horal
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přivezli	přivézt	k5eAaPmAgMnP	přivézt
lyže	lyže	k1gFnPc4	lyže
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
je	on	k3xPp3gMnPc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
<g/>
.	.	kIx.	.
</s>
<s>
Severské	severský	k2eAgFnPc1d1	severská
lyžařské	lyžařský	k2eAgFnPc1d1	lyžařská
techniky	technika	k1gFnPc1	technika
ovšem	ovšem	k9	ovšem
nevyhovovaly	vyhovovat	k5eNaImAgFnP	vyhovovat
vysokohorskému	vysokohorský	k2eAgInSc3d1	vysokohorský
terénu	terén	k1gInSc3	terén
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
lyže	lyže	k1gFnPc1	lyže
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgFnPc4	první
lyže	lyže	k1gFnPc4	lyže
přivezl	přivézt	k5eAaPmAgMnS	přivézt
propagátor	propagátor	k1gMnSc1	propagátor
sportu	sport	k1gInSc2	sport
Josef	Josef	k1gMnSc1	Josef
Rössler-Ořovský	Rössler-Ořovský	k1gMnSc1	Rössler-Ořovský
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
lyží	lyže	k1gFnPc2	lyže
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
řemeslné	řemeslný	k2eAgFnSc2d1	řemeslná
výroby	výroba	k1gFnSc2	výroba
na	na	k7c4	na
technicky	technicky	k6eAd1	technicky
náročnou	náročný	k2eAgFnSc4d1	náročná
profesi	profese	k1gFnSc4	profese
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
lyže	lyže	k1gFnPc1	lyže
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
lyžařských	lyžařský	k2eAgFnPc2d1	lyžařská
technik	technika	k1gFnPc2	technika
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
měnit	měnit	k5eAaImF	měnit
i	i	k9	i
technologie	technologie	k1gFnSc1	technologie
výroby	výroba	k1gFnSc2	výroba
lyží	lyže	k1gFnPc2	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
z	z	k7c2	z
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
lyží	lyže	k1gFnPc2	lyže
začínají	začínat	k5eAaImIp3nP	začínat
prosazovat	prosazovat	k5eAaImF	prosazovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
plastické	plastický	k2eAgFnSc2d1	plastická
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
skleněné	skleněný	k2eAgNnSc1d1	skleněné
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
kevlar	kevlar	k1gInSc1	kevlar
<g/>
,	,	kIx,	,
titan	titan	k1gInSc1	titan
nebo	nebo	k8xC	nebo
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
materiál	materiál	k1gInSc1	materiál
(	(	kIx(	(
<g/>
titanal	titanat	k5eAaImAgInS	titanat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnými	vhodný	k2eAgFnPc7d1	vhodná
kombinacemi	kombinace	k1gFnPc7	kombinace
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
laminátu	laminát	k1gInSc2	laminát
s	s	k7c7	s
dřevem	dřevo	k1gNnSc7	dřevo
či	či	k8xC	či
plastickými	plastický	k2eAgFnPc7d1	plastická
hmotami	hmota	k1gFnPc7	hmota
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgInPc4d1	nový
druhy	druh	k1gInPc4	druh
lyží	lyže	k1gFnPc2	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnPc1	lyže
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
délkou	délka	k1gFnSc7	délka
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Běžecké	běžecký	k2eAgFnPc1d1	běžecká
lyže	lyže	k1gFnPc1	lyže
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
běžky	běžka	k1gFnPc4	běžka
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
lehké	lehký	k2eAgNnSc1d1	lehké
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1,2	[number]	k4	1,2
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
z	z	k7c2	z
kevlarových	kevlarová	k1gFnPc2	kevlarová
vláken	vlákno	k1gNnPc2	vlákno
s	s	k7c7	s
komůrkovou	komůrkový	k2eAgFnSc7d1	Komůrková
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnPc1	lyže
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
úzké	úzký	k2eAgInPc4d1	úzký
(	(	kIx(	(
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
běžeckých	běžecký	k2eAgFnPc2d1	běžecká
lyží	lyže	k1gFnPc2	lyže
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgInSc1d2	menší
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
mělký	mělký	k2eAgInSc4d1	mělký
žlábek	žlábek	k1gInSc4	žlábek
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c4	na
točivost	točivost	k1gFnSc4	točivost
<g/>
,	,	kIx,	,
zatáčky	zatáčka	k1gFnPc1	zatáčka
se	se	k3xPyFc4	se
projíždějí	projíždět	k5eAaImIp3nP	projíždět
odšlapováním	odšlapování	k1gNnSc7	odšlapování
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
styl	styl	k1gInSc1	styl
-	-	kIx~	-
lyže	lyže	k1gFnPc1	lyže
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
tuhá	tuhý	k2eAgNnPc4d1	tuhé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
na	na	k7c6	na
bruslení	bruslení	k1gNnSc6	bruslení
(	(	kIx(	(
<g/>
až	až	k9	až
2,30	[number]	k4	2,30
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rovnoměrněji	rovnoměrně	k6eAd2	rovnoměrně
rozdělovaly	rozdělovat	k5eAaImAgFnP	rozdělovat
váhu	váha	k1gFnSc4	váha
lyžaře	lyžař	k1gMnSc2	lyžař
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
jsou	být	k5eAaImIp3nP	být
pružné	pružný	k2eAgFnPc1d1	pružná
a	a	k8xC	a
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
kotníku	kotník	k1gInSc2	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
styl	styl	k1gInSc1	styl
-	-	kIx~	-
lyže	lyže	k1gFnPc1	lyže
mají	mít	k5eAaImIp3nP	mít
někdy	někdy	k6eAd1	někdy
širší	široký	k2eAgFnSc4d2	širší
přední	přední	k2eAgFnSc4d1	přední
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
bruslení	bruslení	k1gNnSc4	bruslení
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
zdvižené	zdvižený	k2eAgFnPc1d1	zdvižená
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1,75	[number]	k4	1,75
do	do	k7c2	do
2	[number]	k4	2
m	m	kA	m
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sněhových	sněhový	k2eAgFnPc6d1	sněhová
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc1	sníh
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
tím	ten	k3xDgNnSc7	ten
kratší	krátký	k2eAgFnSc1d2	kratší
lyže	lyže	k1gFnSc1	lyže
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Bota	bota	k1gFnSc1	bota
musí	muset	k5eAaImIp3nS	muset
poskytovat	poskytovat	k5eAaImF	poskytovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
boční	boční	k2eAgFnSc4d1	boční
oporu	opora	k1gFnSc4	opora
pro	pro	k7c4	pro
kotník	kotník	k1gInSc4	kotník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podepírá	podepírat	k5eAaImIp3nS	podepírat
plastová	plastový	k2eAgFnSc1d1	plastová
výztuha	výztuha	k1gFnSc1	výztuha
<g/>
.	.	kIx.	.
</s>
<s>
Podrážka	podrážka	k1gFnSc1	podrážka
je	být	k5eAaImIp3nS	být
tuhá	tuhý	k2eAgFnSc1d1	tuhá
<g/>
.	.	kIx.	.
</s>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
-	-	kIx~	-
délka	délka	k1gFnSc1	délka
lyží	lyže	k1gFnPc2	lyže
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
maximálně	maximálně	k6eAd1	maximálně
o	o	k7c4	o
4	[number]	k4	4
cm	cm	kA	cm
kratší	krátký	k2eAgNnSc1d2	kratší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
lyžaře	lyžař	k1gMnSc2	lyžař
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
není	být	k5eNaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
Sjezdové	sjezdový	k2eAgFnPc4d1	sjezdová
lyže	lyže	k1gFnPc4	lyže
(	(	kIx(	(
<g/>
sjezdovky	sjezdovka	k1gFnPc4	sjezdovka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
tvaru	tvar	k1gInSc6	tvar
určovány	určovat	k5eAaImNgFnP	určovat
především	především	k6eAd1	především
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vodivé	vodivý	k2eAgFnPc1d1	vodivá
i	i	k8xC	i
točivé	točivý	k2eAgFnPc1d1	točivá
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
útlum	útlum	k1gInSc1	útlum
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
rozkmitání	rozkmitání	k1gNnSc6	rozkmitání
vlivem	vlivem	k7c2	vlivem
nerovnosti	nerovnost	k1gFnSc2	nerovnost
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
lyží	lyže	k1gFnPc2	lyže
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
cm	cm	kA	cm
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
užší	úzký	k2eAgFnPc4d2	užší
než	než	k8xS	než
lyže	lyže	k1gFnPc4	lyže
slalomové	slalomový	k2eAgFnPc4d1	slalomová
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
lyže	lyže	k1gFnPc1	lyže
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
rovné	rovný	k2eAgFnPc1d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
lyže	lyže	k1gFnPc1	lyže
výrazněji	výrazně	k6eAd2	výrazně
vykrojené	vykrojený	k2eAgFnPc1d1	vykrojená
(	(	kIx(	(
<g/>
lyže	lyže	k1gFnPc1	lyže
carvingové	carvingový	k2eAgFnPc1d1	carvingová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Špička	špička	k1gFnSc1	špička
a	a	k8xC	a
patka	patka	k1gFnSc1	patka
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
střední	střední	k2eAgFnSc3d1	střední
části	část	k1gFnSc3	část
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
plocha	plocha	k1gFnSc1	plocha
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnSc1	lyže
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
ovládatelná	ovládatelný	k2eAgFnSc1d1	ovládatelná
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Širší	široký	k2eAgFnSc1d2	širší
špička	špička	k1gFnSc1	špička
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
zahájení	zahájení	k1gNnSc4	zahájení
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
patka	patka	k1gFnSc1	patka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
vyjetí	vyjetí	k1gNnSc1	vyjetí
z	z	k7c2	z
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Slalomové	slalomový	k2eAgFnPc1d1	slalomová
lyže	lyže	k1gFnPc1	lyže
(	(	kIx(	(
<g/>
slalomky	slalomka	k1gFnPc1	slalomka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
obří	obří	k2eAgInSc4d1	obří
slalom	slalom	k1gInSc4	slalom
obřačky	obřačka	k1gFnSc2	obřačka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
sjezdových	sjezdový	k2eAgFnPc2d1	sjezdová
především	především	k9	především
menším	malý	k2eAgInSc7d2	menší
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
točivost	točivost	k1gFnSc4	točivost
<g/>
,	,	kIx,	,
vodivost	vodivost	k1gFnSc4	vodivost
především	především	k9	především
v	v	k7c6	v
obloucích	oblouk	k1gInPc6	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Slalomové	slalomový	k2eAgFnPc1d1	slalomová
lyže	lyže	k1gFnPc1	lyže
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
kratší	krátký	k2eAgFnPc1d2	kratší
než	než	k8xS	než
lyže	lyže	k1gFnPc1	lyže
sjezdové	sjezdový	k2eAgFnPc1d1	sjezdová
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
širší	široký	k2eAgFnSc4d2	širší
a	a	k8xC	a
lehčí	lehký	k2eAgFnSc4d2	lehčí
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
též	též	k9	též
jako	jako	k8xS	jako
letmý	letmý	k2eAgInSc4d1	letmý
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
lyží	lyže	k1gFnPc2	lyže
je	být	k5eAaImIp3nS	být
220	[number]	k4	220
<g/>
-	-	kIx~	-
<g/>
240	[number]	k4	240
cm	cm	kA	cm
<g/>
,	,	kIx,	,
špička	špička	k1gFnSc1	špička
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
neznatelná	znatelný	k2eNgFnSc1d1	neznatelná
<g/>
,	,	kIx,	,
skluznice	skluznice	k1gFnSc1	skluznice
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
hrany	hrana	k1gFnPc1	hrana
nejsou	být	k5eNaImIp3nP	být
ostré	ostrý	k2eAgFnPc1d1	ostrá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
prudkému	prudký	k2eAgNnSc3d1	prudké
zatočení	zatočení	k1gNnSc3	zatočení
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
kg	kg	kA	kg
pro	pro	k7c4	pro
pár	pár	k4xCyI	pár
s	s	k7c7	s
vázáním	vázání	k1gNnSc7	vázání
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přes	přes	k7c4	přes
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
.	.	kIx.	.
</s>
<s>
Skokanské	skokanský	k2eAgFnPc1d1	skokanská
lyže	lyže	k1gFnPc1	lyže
(	(	kIx(	(
<g/>
skočky	skočka	k1gFnPc1	skočka
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
funkční	funkční	k2eAgFnPc4d1	funkční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
jako	jako	k8xC	jako
lyže	lyže	k1gFnPc4	lyže
pro	pro	k7c4	pro
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rychlé	rychlý	k2eAgFnSc3d1	rychlá
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgFnSc3d1	pevná
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
vodivé	vodivý	k2eAgFnPc4d1	vodivá
v	v	k7c6	v
rovné	rovný	k2eAgFnSc6d1	rovná
jízdě	jízda	k1gFnSc6	jízda
a	a	k8xC	a
správně	správně	k6eAd1	správně
vyvážené	vyvážený	k2eAgFnPc1d1	vyvážená
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
až	až	k9	až
235	[number]	k4	235
cm	cm	kA	cm
avšak	avšak	k8xC	avšak
nesmí	smět	k5eNaImIp3nS	smět
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
výšku	výška	k1gFnSc4	výška
skokana	skokan	k1gMnSc2	skokan
o	o	k7c6	o
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
80	[number]	k4	80
cm	cm	kA	cm
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
88	[number]	k4	88
mm	mm	kA	mm
<g/>
,	,	kIx,	,
skluznice	skluznice	k1gFnSc1	skluznice
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
vodících	vodící	k2eAgInPc2d1	vodící
žlábků	žlábek	k1gInPc2	žlábek
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
7,27	[number]	k4	7,27
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnPc1	lyže
nemají	mít	k5eNaImIp3nP	mít
hrany	hrana	k1gFnPc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Vázání	vázání	k1gNnSc1	vázání
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
volnou	volný	k2eAgFnSc7d1	volná
patou	pata	k1gFnSc7	pata
a	a	k8xC	a
na	na	k7c4	na
lyže	lyže	k1gFnPc4	lyže
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
špičky	špička	k1gFnSc2	špička
nebyla	být	k5eNaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
jak	jak	k8xS	jak
57	[number]	k4	57
<g/>
%	%	kIx~	%
délky	délka	k1gFnSc2	délka
lyže	lyže	k1gFnSc2	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
lyžařskou	lyžařský	k2eAgFnSc4d1	lyžařská
akrobacii	akrobacie	k1gFnSc4	akrobacie
a	a	k8xC	a
podobají	podobat	k5eAaImIp3nP	podobat
se	se	k3xPyFc4	se
lyžím	lyže	k1gFnPc3	lyže
slalomovým	slalomový	k2eAgFnPc3d1	slalomová
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
kratší	krátký	k2eAgInPc1d2	kratší
a	a	k8xC	a
patky	patka	k1gFnPc4	patka
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
špičky	špička	k1gFnPc1	špička
jsou	být	k5eAaImIp3nP	být
mírně	mírně	k6eAd1	mírně
zvednuté	zvednutý	k2eAgFnPc1d1	zvednutá
a	a	k8xC	a
zaoblené	zaoblený	k2eAgFnPc1d1	zaoblená
<g/>
.	.	kIx.	.
</s>
<s>
Lyže	lyže	k1gFnPc1	lyže
pro	pro	k7c4	pro
skoky	skok	k1gInPc4	skok
a	a	k8xC	a
jízdu	jízda	k1gFnSc4	jízda
v	v	k7c6	v
boulích	boule	k1gFnPc6	boule
-	-	kIx~	-
minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
190	[number]	k4	190
cm	cm	kA	cm
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
180	[number]	k4	180
cm	cm	kA	cm
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
jsou	být	k5eAaImIp3nP	být
pružné	pružný	k2eAgFnPc1d1	pružná
a	a	k8xC	a
patky	patka	k1gFnPc1	patka
vytočené	vytočený	k2eAgFnPc1d1	vytočená
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Skluznice	skluznice	k1gFnSc1	skluznice
nemá	mít	k5eNaImIp3nS	mít
drážky	drážka	k1gFnPc4	drážka
<g/>
,	,	kIx,	,
lyže	lyže	k1gFnPc4	lyže
jsou	být	k5eAaImIp3nP	být
vykrojené	vykrojený	k2eAgNnSc1d1	vykrojené
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
snazší	snadný	k2eAgNnSc1d2	snazší
zahájit	zahájit	k5eAaPmF	zahájit
oblouk	oblouk	k1gInSc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Skokani	skokan	k1gMnPc1	skokan
si	se	k3xPyFc3	se
posouvají	posouvat	k5eAaImIp3nP	posouvat
vázání	vázání	k1gNnSc4	vázání
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
cm	cm	kA	cm
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Akrobatické	akrobatický	k2eAgFnPc1d1	akrobatická
lyže	lyže	k1gFnPc1	lyže
-	-	kIx~	-
Minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
81	[number]	k4	81
<g/>
%	%	kIx~	%
výšky	výška	k1gFnSc2	výška
lyžaře	lyžař	k1gMnSc2	lyžař
(	(	kIx(	(
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
160	[number]	k4	160
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skluznice	skluznice	k1gFnSc1	skluznice
je	být	k5eAaImIp3nS	být
konvexní	konvexní	k2eAgFnSc1d1	konvexní
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
drážek	drážka	k1gFnPc2	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Patky	patka	k1gFnPc1	patka
jsou	být	k5eAaImIp3nP	být
vytočené	vytočený	k2eAgFnPc1d1	vytočená
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
klouzat	klouzat	k5eAaImF	klouzat
vzad	vzad	k6eAd1	vzad
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
sportovní	sportovní	k2eAgFnSc1d1	sportovní
načiní	načinit	k5eAaPmIp3nP	načinit
určené	určený	k2eAgInPc1d1	určený
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
trénink	trénink	k1gInSc4	trénink
jsou	být	k5eAaImIp3nP	být
kolečkové	kolečkový	k2eAgFnPc1d1	kolečková
lyže	lyže	k1gFnPc1	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lyže	lyže	k1gFnSc2	lyže
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lyže	lyže	k1gFnSc2	lyže
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kupujete	kupovat	k5eAaImIp2nP	kupovat
běžky	běžka	k1gFnPc1	běžka
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
na	na	k7c4	na
co	co	k3yInSc4	co
je	on	k3xPp3gMnPc4	on
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
