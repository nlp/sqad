<s>
Dejeanova	Dejeanův	k2eAgFnSc1d1	Dejeanův
fontána	fontána	k1gFnSc1	fontána
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Fontaine	Fontain	k1gInSc5	Fontain
Dejean	Dejean	k1gInSc1	Dejean
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fontána	fontána	k1gFnSc1	fontána
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Kašna	kašna	k1gFnSc1	kašna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Place	plac	k1gInSc6	plac
Pasdeloup	Pasdeloup	k1gInSc4	Pasdeloup
poblíž	poblíž	k7c2	poblíž
Cirque	Cirqu	k1gFnSc2	Cirqu
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
hiver	hiver	k1gMnSc1	hiver
<g/>
.	.	kIx.	.
</s>
<s>
Fontána	fontána	k1gFnSc1	fontána
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Camille	Camill	k1gMnSc2	Camill
Formigé	Formigé	k1gNnSc2	Formigé
<g/>
,	,	kIx,	,
sochařskou	sochařský	k2eAgFnSc4d1	sochařská
výzdobu	výzdoba	k1gFnSc4	výzdoba
provedl	provést	k5eAaPmAgInS	provést
Charles-Louis	Charles-Louis	k1gFnSc4	Charles-Louis
Malric	Malrice	k1gInPc2	Malrice
<g/>
.	.	kIx.	.
</s>
<s>
Fontána	fontána	k1gFnSc1	fontána
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
plánu	plán	k1gInSc2	plán
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
12	[number]	k4	12
kašen	kašna	k1gFnPc2	kašna
pro	pro	k7c4	pro
poskytování	poskytování	k1gNnSc4	poskytování
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
architekt	architekt	k1gMnSc1	architekt
François	François	k1gFnSc2	François
Eugè	Eugè	k1gMnSc1	Eugè
Dejean	Dejean	k1gMnSc1	Dejean
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
realizována	realizovat	k5eAaBmNgFnS	realizovat
pouze	pouze	k6eAd1	pouze
tato	tento	k3xDgFnSc1	tento
kašna	kašna	k1gFnSc1	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Kašna	kašna	k1gFnSc1	kašna
má	mít	k5eAaImIp3nS	mít
osmiboký	osmiboký	k2eAgInSc4d1	osmiboký
podstavec	podstavec	k1gInSc4	podstavec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
nádržky	nádržka	k1gFnPc1	nádržka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lastury	lastura	k1gFnSc2	lastura
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
stéká	stékat	k5eAaImIp3nS	stékat
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
chrličů	chrlič	k1gInPc2	chrlič
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
delfínů	delfín	k1gMnPc2	delfín
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
medailon	medailon	k1gInSc1	medailon
a	a	k8xC	a
dedikační	dedikační	k2eAgInSc4d1	dedikační
nápis	nápis	k1gInSc4	nápis
dárci	dárce	k1gMnSc3	dárce
F.	F.	kA	F.
E.	E.	kA	E.
Dejeanovi	Dejeanovi	k1gRnPc1	Dejeanovi
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
špici	špice	k1gFnSc6	špice
sedí	sedit	k5eAaImIp3nS	sedit
pelikán	pelikán	k1gInSc4	pelikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Fontaine	Fontain	k1gInSc5	Fontain
Dejean	Dejeana	k1gFnPc2	Dejeana
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Popis	popis	k1gInSc1	popis
fontány	fontána	k1gFnSc2	fontána
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Popis	popis	k1gInSc1	popis
fontány	fontána	k1gFnSc2	fontána
</s>
