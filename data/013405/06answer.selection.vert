<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
napadaný	napadaný	k2eAgInSc1d1	napadaný
sníh	sníh	k1gInSc1	sníh
odráží	odrážet	k5eAaImIp3nS	odrážet
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
=	=	kIx~	=
0,9	[number]	k4	0,9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
staršího	starý	k2eAgInSc2d2	starší
sněhu	sníh	k1gInSc2	sníh
odrazivost	odrazivost	k1gFnSc4	odrazivost
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
