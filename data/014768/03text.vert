<s>
Pakůň	pakůň	k1gMnSc1
</s>
<s>
Pakůň	pakůň	k1gMnSc1
Pakůň	pakůň	k1gMnSc1
žíhaný	žíhaný	k2eAgMnSc1d1
Pakůň	pakůň	k1gMnSc1
běloocasý	běloocasý	k2eAgMnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Nadtřída	nadtřída	k1gFnSc1
</s>
<s>
čtyřnožci	čtyřnožec	k1gMnPc1
(	(	kIx(
<g/>
Tetrapoda	Tetrapoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
sudokopytníci	sudokopytník	k1gMnPc1
(	(	kIx(
<g/>
Artiodactyla	Artiodactyla	k1gMnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
přežvýkaví	přežvýkavý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Ruminantia	Ruminantia	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
turovití	turovití	k1gMnPc1
(	(	kIx(
<g/>
Bovidae	Bovidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
buvolci	buvolec	k1gMnPc1
(	(	kIx(
<g/>
Alcelaphinae	Alcelaphinae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
pakůň	pakůň	k1gMnSc1
(	(	kIx(
<g/>
Connochaetes	Connochaetes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Lichtenstein	Lichtenstein	k1gMnSc1
<g/>
,	,	kIx,
1812	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pakůň	pakůň	k1gMnSc1
(	(	kIx(
<g/>
Connochaetes	Connochaetes	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
velkých	velký	k2eAgMnPc2d1
přežvýkavých	přežvýkavý	k2eAgMnPc2d1
savců	savec	k1gMnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
turovitých	turovití	k1gMnPc2
(	(	kIx(
<g/>
Bovidae	Bovidae	k1gFnSc1
<g/>
)	)	kIx)
patřící	patřící	k2eAgFnSc1d1
mezi	mezi	k7c4
buvolce	buvolec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupci	zástupce	k1gMnPc1
rodu	rod	k1gInSc2
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
do	do	k7c2
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
a	a	k8xC
žijí	žít	k5eAaImIp3nP
v	v	k7c6
subsaharské	subsaharský	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
převážně	převážně	k6eAd1
v	v	k7c6
savanách	savana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Žijící	žijící	k2eAgFnSc1d1
</s>
<s>
pakůň	pakůň	k1gMnSc1
běloocasý	běloocasý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Connochaetes	Connochaetes	k1gInSc1
gnou	gnous	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
pakůň	pakůň	k1gMnSc1
žíhaný	žíhaný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Connochaetes	Connochaetes	k1gMnSc1
taurinus	taurinus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Fosilní	fosilní	k2eAgMnSc1d1
</s>
<s>
Connochaetes	Connochaetes	k1gMnSc1
africanus	africanus	k1gMnSc1
(	(	kIx(
<g/>
Hopwood	Hopwood	k1gInSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
)	)	kIx)
†	†	k?
</s>
<s>
Connochaetes	Connochaetes	k1gInSc1
gentryi	gentry	k1gFnSc2
Harris	Harris	k1gFnSc2
<g/>
,	,	kIx,
1991	#num#	k4
†	†	k?
</s>
<s>
Connochaetes	Connochaetes	k1gMnSc1
laticornutus	laticornutus	k1gMnSc1
(	(	kIx(
<g/>
van	van	k1gInSc1
Hoepen	Hoepen	k2eAgInSc1d1
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
)	)	kIx)
†	†	k?
</s>
<s>
Connochaetes	Connochaetes	k1gInSc1
tournoueri	tournouer	k1gFnSc2
(	(	kIx(
<g/>
P.	P.	kA
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
1884	#num#	k4
<g/>
)	)	kIx)
†	†	k?
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Pakůň	pakůň	k1gMnSc1
žíhaný	žíhaný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
pakůň	pakůň	k1gMnSc1
běloocasý	běloocasý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	samec	k1gInSc3
prvně	prvně	k?
jmenovaného	jmenovaný	k1gMnSc2
dosahují	dosahovat	k5eAaImIp3nP
výšky	výška	k1gFnPc1
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
asi	asi	k9
150	#num#	k4
cm	cm	kA
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
okolo	okolo	k7c2
250	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
,	,	kIx,
tyto	tento	k3xDgFnPc4
míry	míra	k1gFnPc4
pro	pro	k7c4
samice	samice	k1gFnPc4
jsou	být	k5eAaImIp3nP
asi	asi	k9
135	#num#	k4
cm	cm	kA
a	a	k8xC
180	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	samec	k1gInSc3
pakoňů	pakůň	k1gMnPc2
běloocasých	běloocasý	k2eAgMnPc2d1
měří	měřit	k5eAaImIp3nS
v	v	k7c6
ramenou	rameno	k1gNnPc6
cca	cca	kA
120	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
a	a	k8xC
váží	vážit	k5eAaImIp3nS
okolo	okolo	k7c2
150	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
samice	samice	k1gFnPc1
pak	pak	k6eAd1
dosahují	dosahovat	k5eAaImIp3nP
výšky	výška	k1gFnSc2
zhruba	zhruba	k6eAd1
100	#num#	k4
cm	cm	kA
a	a	k8xC
váhy	váha	k1gFnSc2
okolo	okolo	k7c2
120	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
rozeznávacím	rozeznávací	k2eAgInSc7d1
znakem	znak	k1gInSc7
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
druhy	druh	k1gInPc7
je	být	k5eAaImIp3nS
tvar	tvar	k1gInSc4
rohů	roh	k1gInPc2
a	a	k8xC
barva	barva	k1gFnSc1
ocasu	ocas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pakoně	pakůň	k1gMnSc4
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
spásáním	spásání	k1gNnSc7
trávy	tráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gInPc4
neuloví	ulovit	k5eNaPmIp3nP
predátoři	predátor	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
především	především	k9
lvi	lev	k1gMnPc1
a	a	k8xC
hyeny	hyena	k1gFnPc1
skvrnité	skvrnitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
dožít	dožít	k5eAaPmF
i	i	k9
více	hodně	k6eAd2
než	než	k8xS
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
je	být	k5eAaImIp3nS
pakůň	pakůň	k1gMnSc1
žíhaný	žíhaný	k2eAgMnSc1d1
nejhojnějším	hojný	k2eAgInSc7d3
druhem	druh	k1gInSc7
velké	velký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc1
stavy	stav	k1gInPc1
jsou	být	k5eAaImIp3nP
mnohonásobně	mnohonásobně	k6eAd1
vyšší	vysoký	k2eAgInPc1d2
než	než	k8xS
počty	počet	k1gInPc1
pakoňů	pakůň	k1gMnPc2
běloocasých	běloocasý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
jeho	jeho	k3xOp3gFnPc1
populace	populace	k1gFnPc1
podnikají	podnikat	k5eAaImIp3nP
každoročně	každoročně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
migrace	migrace	k1gFnPc4
na	na	k7c4
nové	nový	k2eAgFnPc4d1
pastviny	pastvina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pakůň	pakůň	k1gMnSc1
běloocasý	běloocasý	k2eAgInSc1d1
tyto	tento	k3xDgFnPc4
velké	velký	k2eAgFnPc4d1
migrace	migrace	k1gFnPc4
nepodniká	podnikat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmnožování	rozmnožování	k1gNnSc1
obou	dva	k4xCgMnPc2
druhů	druh	k1gMnPc2
probíhá	probíhat	k5eAaImIp3nS
po	po	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
na	na	k7c6
konci	konec	k1gInSc6
období	období	k1gNnSc2
dešťů	dešť	k1gInPc2
a	a	k8xC
telata	tele	k1gNnPc1
jsou	být	k5eAaImIp3nP
brzy	brzy	k6eAd1
aktivní	aktivní	k2eAgFnSc1d1
a	a	k8xC
schopna	schopen	k2eAgFnSc1d1
se	se	k3xPyFc4
pohybovat	pohybovat	k5eAaImF
se	s	k7c7
stádem	stádo	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
přežití	přežití	k1gNnSc4
nezbytné	nezbytný	k2eAgNnSc4d1,k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Pakoně	pakůň	k1gMnPc1
se	se	k3xPyFc4
často	často	k6eAd1
pasou	pást	k5eAaImIp3nP
ve	v	k7c6
smíšených	smíšený	k2eAgNnPc6d1
stádech	stádo	k1gNnPc6
se	s	k7c7
zebrami	zebra	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
šance	šance	k1gFnPc4
spatřit	spatřit	k5eAaPmF
potenciální	potenciální	k2eAgMnPc4d1
predátory	predátor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pakoně	pakůň	k1gMnPc1
jsou	být	k5eAaImIp3nP
turistickou	turistický	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
soupeří	soupeřit	k5eAaImIp3nS
s	s	k7c7
domestikovanými	domestikovaný	k2eAgNnPc7d1
hospodářskými	hospodářský	k2eAgNnPc7d1
zvířaty	zvíře	k1gNnPc7
na	na	k7c6
pastvinách	pastvina	k1gFnPc6
a	a	k8xC
zemědělci	zemědělec	k1gMnPc1
jim	on	k3xPp3gInPc3
někdy	někdy	k6eAd1
vyčítají	vyčítat	k5eAaImIp3nP
přenos	přenos	k1gInSc4
nemocí	nemoc	k1gFnPc2
a	a	k8xC
parazitů	parazit	k1gMnPc2
na	na	k7c4
jejich	jejich	k3xOp3gInSc4
dobytek	dobytek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitý	určitý	k2eAgInSc4d1
nelegální	legální	k2eNgInSc4d1
lov	lov	k1gInSc4
přetrvává	přetrvávat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
populační	populační	k2eAgInSc1d1
trend	trend	k1gInSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
populace	populace	k1gFnSc2
žijí	žít	k5eAaImIp3nP
v	v	k7c6
národních	národní	k2eAgInPc6d1
parcích	park	k1gInPc6
nebo	nebo	k8xC
na	na	k7c6
soukromých	soukromý	k2eAgInPc6d1
pozemcích	pozemek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
oba	dva	k4xCgMnPc4
druhy	druh	k1gMnPc4
jako	jako	k8xC,k8xS
málo	málo	k6eAd1
dotčené	dotčený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
pakůň	pakůň	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pakůň	pakůň	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Živočichové	živočich	k1gMnPc1
</s>
