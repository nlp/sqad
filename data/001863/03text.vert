<s>
Atol	atol	k1gInSc1	atol
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kruhovitého	kruhovitý	k2eAgInSc2d1	kruhovitý
korálového	korálový	k2eAgInSc2d1	korálový
útesu	útes	k1gInSc2	útes
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
lagunu	laguna	k1gFnSc4	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Atoly	atol	k1gInPc1	atol
vznikají	vznikat	k5eAaImIp3nP	vznikat
postupnou	postupný	k2eAgFnSc7d1	postupná
přeměnou	přeměna	k1gFnSc7	přeměna
podmořských	podmořský	k2eAgFnPc2d1	podmořská
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
erupce	erupce	k1gFnSc2	erupce
podmořské	podmořský	k2eAgFnSc2d1	podmořská
sopky	sopka	k1gFnSc2	sopka
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
(	(	kIx(	(
<g/>
či	či	k8xC	či
opakovaná	opakovaný	k2eAgFnSc1d1	opakovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynoří	vynořit	k5eAaPmIp3nS	vynořit
se	se	k3xPyFc4	se
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
oceánu	oceán	k1gInSc2	oceán
sopečný	sopečný	k2eAgInSc4d1	sopečný
kužel	kužel	k1gInSc4	kužel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
sopky	sopka	k1gFnPc1	sopka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybem	pohyb	k1gInSc7	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
pomalu	pomalu	k6eAd1	pomalu
posouvat	posouvat	k5eAaImF	posouvat
a	a	k8xC	a
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgNnSc2d1	původní
epicentra	epicentrum	k1gNnSc2	epicentrum
mohou	moct	k5eAaImIp3nP	moct
postupně	postupně	k6eAd1	postupně
vznikat	vznikat	k5eAaImF	vznikat
další	další	k2eAgInPc1d1	další
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
sopečné	sopečný	k2eAgInPc1d1	sopečný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
celých	celý	k2eAgNnPc2d1	celé
sopečných	sopečný	k2eAgNnPc2d1	sopečné
souostroví	souostroví	k1gNnPc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
atolu	atol	k1gInSc2	atol
je	být	k5eAaImIp3nS	být
teplá	teplý	k2eAgFnSc1d1	teplá
a	a	k8xC	a
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
korály	korál	k1gInPc1	korál
rostou	růst	k5eAaImIp3nP	růst
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
30	[number]	k4	30
<g/>
°	°	k?	°
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
vyhaslé	vyhaslý	k2eAgFnSc2d1	vyhaslá
sopky	sopka	k1gFnSc2	sopka
začne	začít	k5eAaPmIp3nS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
korálový	korálový	k2eAgInSc4d1	korálový
útes	útes	k1gInSc4	útes
<g/>
,	,	kIx,	,
lemující	lemující	k2eAgNnSc4d1	lemující
pobřeží	pobřeží	k1gNnSc4	pobřeží
sopečného	sopečný	k2eAgInSc2d1	sopečný
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
geologického	geologický	k2eAgNnSc2d1	geologické
podloží	podloží	k1gNnSc2	podloží
a	a	k8xC	a
eroze	eroze	k1gFnSc2	eroze
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sopečný	sopečný	k2eAgInSc1d1	sopečný
kužel	kužel	k1gInSc1	kužel
začne	začít	k5eAaPmIp3nS	začít
rozpadat	rozpadat	k5eAaPmF	rozpadat
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vod	voda	k1gFnPc2	voda
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Korálový	korálový	k2eAgInSc1d1	korálový
útes	útes	k1gInSc1	útes
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
a	a	k8xC	a
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
světlem	světlo	k1gNnSc7	světlo
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
mořské	mořský	k2eAgFnSc3d1	mořská
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
postupně	postupně	k6eAd1	postupně
vzniká	vznikat	k5eAaImIp3nS	vznikat
bariérový	bariérový	k2eAgInSc4d1	bariérový
útes	útes	k1gInSc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
původní	původní	k2eAgInSc4d1	původní
sopečný	sopečný	k2eAgInSc4d1	sopečný
ostrov	ostrov	k1gInSc4	ostrov
úplně	úplně	k6eAd1	úplně
zmizí	zmizet	k5eAaPmIp3nP	zmizet
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
korálový	korálový	k2eAgInSc1d1	korálový
prstenec	prstenec	k1gInSc1	prstenec
-	-	kIx~	-
atol	atol	k1gInSc1	atol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
mořskou	mořský	k2eAgFnSc4d1	mořská
lagunu	laguna	k1gFnSc4	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
trvá	trvat	k5eAaImIp3nS	trvat
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
atol	atol	k1gInSc1	atol
není	být	k5eNaImIp3nS	být
konečnou	konečný	k2eAgFnSc7d1	konečná
fází	fáze	k1gFnSc7	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
a	a	k8xC	a
potápí	potápět	k5eAaImIp3nS	potápět
i	i	k9	i
korálový	korálový	k2eAgInSc4d1	korálový
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
zmizí	zmizet	k5eAaPmIp3nS	zmizet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
korálových	korálový	k2eAgInPc2d1	korálový
ostrovů	ostrov	k1gInPc2	ostrov
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Bikini	Bikini	k1gNnSc1	Bikini
Eniwetok	Eniwetok	k1gInSc1	Eniwetok
Kwajalein	Kwajalein	k1gMnSc1	Kwajalein
Maledivy	Maledivy	k1gFnPc4	Maledivy
Mururoa	Mururous	k1gMnSc2	Mururous
Nukulaelae	Nukulaela	k1gMnSc2	Nukulaela
Rangiroa	Rangirous	k1gMnSc2	Rangirous
Wake	Wak	k1gMnSc2	Wak
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atol	atol	k1gInSc4	atol
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Atol	atol	k1gInSc4	atol
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
