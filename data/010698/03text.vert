<p>
<s>
Prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
presentation	presentation	k1gInSc1	presentation
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
šesté	šestý	k4xOgFnSc2	šestý
vrstvy	vrstva	k1gFnSc2	vrstva
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
vrstvy	vrstva	k1gFnSc2	vrstva
je	být	k5eAaImIp3nS	být
transformovat	transformovat	k5eAaBmF	transformovat
data	datum	k1gNnPc4	datum
do	do	k7c2	do
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
dodávku	dodávka	k1gFnSc4	dodávka
a	a	k8xC	a
formátování	formátování	k1gNnSc4	formátování
informací	informace	k1gFnPc2	informace
aplikační	aplikační	k2eAgFnSc3d1	aplikační
vrstvě	vrstva	k1gFnSc3	vrstva
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
nebo	nebo	k8xC	nebo
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
aplikační	aplikační	k2eAgFnSc4d1	aplikační
vrstvu	vrstva	k1gFnSc4	vrstva
problému	problém	k1gInSc2	problém
ohledně	ohledně	k7c2	ohledně
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
rozdílů	rozdíl	k1gInPc2	rozdíl
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
koncových	koncový	k2eAgInPc6d1	koncový
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
prezentační	prezentační	k2eAgFnSc2d1	prezentační
služby	služba	k1gFnSc2	služba
je	být	k5eAaImIp3nS	být
převod	převod	k1gInSc1	převod
kódů	kód	k1gInPc2	kód
a	a	k8xC	a
abeced	abeceda	k1gFnPc2	abeceda
textového	textový	k2eAgInSc2d1	textový
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kódování	kódování	k1gNnSc2	kódování
EBCDIC	EBCDIC	kA	EBCDIC
do	do	k7c2	do
kódovaného	kódovaný	k2eAgInSc2d1	kódovaný
ASCII	ascii	kA	ascii
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
modifikace	modifikace	k1gFnSc1	modifikace
grafického	grafický	k2eAgNnSc2d1	grafické
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
pořadí	pořadí	k1gNnSc2	pořadí
bajtů	bajt	k1gInPc2	bajt
apod.	apod.	kA	apod.
Vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jen	jen	k9	jen
strukturou	struktura	k1gFnSc7	struktura
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
jejich	jejich	k3xOp3gInSc7	jejich
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jen	jen	k6eAd1	jen
vrstvě	vrstva	k1gFnSc3	vrstva
aplikační	aplikační	k2eAgInSc4d1	aplikační
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
dat	datum	k1gNnPc2	datum
prezentační	prezentační	k2eAgFnSc2d1	prezentační
vrstvy	vrstva	k1gFnSc2	vrstva
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
datové	datový	k2eAgFnSc2d1	datová
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
komunikujících	komunikující	k2eAgInPc6d1	komunikující
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
pro	pro	k7c4	pro
účel	účel	k1gInSc4	účel
přenosu	přenos	k1gInSc2	přenos
dat	datum	k1gNnPc2	datum
nižšími	nízký	k2eAgFnPc7d2	nižší
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
programátoři	programátor	k1gMnPc1	programátor
musejí	muset	k5eAaImIp3nP	muset
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
prezentaci	prezentace	k1gFnSc4	prezentace
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
odeslání	odeslání	k1gNnSc2	odeslání
dat	datum	k1gNnPc2	datum
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
datagramů	datagram	k1gInPc2	datagram
či	či	k8xC	či
paketů	paket	k1gInPc2	paket
mezi	mezi	k7c7	mezi
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
reprezentace	reprezentace	k1gFnSc2	reprezentace
řetězce	řetězec	k1gInSc2	řetězec
ať	ať	k8xC	ať
už	už	k6eAd1	už
používá	používat	k5eAaImIp3nS	používat
metodu	metoda	k1gFnSc4	metoda
Pascal	pascal	k1gInSc1	pascal
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
délka	délka	k1gFnSc1	délka
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgFnSc2d1	následovaná
řetězcem	řetězec	k1gInSc7	řetězec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
metodu	metoda	k1gFnSc4	metoda
C	C	kA	C
/	/	kIx~	/
C	C	kA	C
<g/>
++	++	k?	++
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
řetězce	řetězec	k1gInSc2	řetězec
označen	označit	k5eAaPmNgMnS	označit
pomocí	pomoc	k1gFnSc7	pomoc
\	\	kIx~	\
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
aplikační	aplikační	k2eAgFnSc1d1	aplikační
vrstva	vrstva	k1gFnSc1	vrstva
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
schopna	schopen	k2eAgFnSc1d1	schopna
ukázat	ukázat	k5eAaPmF	ukázat
na	na	k7c4	na
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
a	a	k8xC	a
prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
bude	být	k5eAaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Serializace	Serializace	k1gFnSc1	Serializace
složitých	složitý	k2eAgFnPc2d1	složitá
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
do	do	k7c2	do
jednorozměrného	jednorozměrný	k2eAgInSc2d1	jednorozměrný
řetězce	řetězec	k1gInSc2	řetězec
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
TLV	TLV	kA	TLV
nebo	nebo	k8xC	nebo
XML	XML	kA	XML
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
funkcí	funkce	k1gFnSc7	funkce
prezentační	prezentační	k2eAgFnSc2d1	prezentační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šifrování	šifrování	k1gNnSc1	šifrování
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
i	i	k8xC	i
když	když	k8xS	když
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
na	na	k7c4	na
aplikační	aplikační	k2eAgInSc4d1	aplikační
<g/>
,	,	kIx,	,
relační	relační	k2eAgFnSc3d1	relační
i	i	k8xC	i
transportní	transportní	k2eAgFnSc3d1	transportní
vrstvě	vrstva	k1gFnSc3	vrstva
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
výhody	výhoda	k1gFnPc4	výhoda
i	i	k8xC	i
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Dešifrování	dešifrování	k1gNnSc1	dešifrování
je	být	k5eAaImIp3nS	být
také	také	k9	také
záležitostí	záležitost	k1gFnSc7	záležitost
prezentační	prezentační	k2eAgFnSc2d1	prezentační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
šifrování	šifrování	k1gNnSc1	šifrování
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
využíváte	využívat	k5eAaPmIp2nP	využívat
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
bankou	banka	k1gFnSc7	banka
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetového	internetový	k2eAgNnSc2d1	internetové
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pomocí	pomocí	k7c2	pomocí
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
kousky	kousek	k1gInPc4	kousek
dat	datum	k1gNnPc2	datum
jsou	být	k5eAaImIp3nP	být
struktury	struktura	k1gFnPc1	struktura
a	a	k8xC	a
složitější	složitý	k2eAgFnPc1d2	složitější
věci	věc	k1gFnPc1	věc
normalizovány	normalizován	k2eAgFnPc1d1	Normalizována
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
běžné	běžný	k2eAgInPc1d1	běžný
příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
objekty	objekt	k1gInPc4	objekt
v	v	k7c6	v
objektově	objektově	k6eAd1	objektově
orientovaném	orientovaný	k2eAgNnSc6d1	orientované
programování	programování	k1gNnSc6	programování
a	a	k8xC	a
přesný	přesný	k2eAgInSc1d1	přesný
způsob	způsob	k1gInSc1	způsob
přenosu	přenos	k1gInSc2	přenos
streamovaného	streamovaný	k2eAgNnSc2d1	streamované
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgFnPc6d1	používaná
aplikacích	aplikace	k1gFnPc6	aplikace
a	a	k8xC	a
protokolech	protokol	k1gInPc6	protokol
se	se	k3xPyFc4	se
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
prezentační	prezentační	k2eAgFnSc7d1	prezentační
a	a	k8xC	a
aplikační	aplikační	k2eAgFnSc7d1	aplikační
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
HyperText	hypertext	k1gInSc1	hypertext
Transfer	transfer	k1gInSc1	transfer
Protokol	protokol	k1gInSc1	protokol
(	(	kIx(	(
<g/>
HTTP	HTTP	kA	HTTP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
protokol	protokol	k1gInSc4	protokol
aplikační	aplikační	k2eAgFnSc2d1	aplikační
vrstvy	vrstva	k1gFnSc2	vrstva
nese	nést	k5eAaImIp3nS	nést
prvky	prvek	k1gInPc4	prvek
prezentační	prezentační	k2eAgFnSc2d1	prezentační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
schopnost	schopnost	k1gFnSc4	schopnost
identifikovat	identifikovat	k5eAaBmF	identifikovat
znakové	znakový	k2eAgNnSc4d1	znakové
kódování	kódování	k1gNnSc4	kódování
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
konverzi	konverze	k1gFnSc4	konverze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
následně	následně	k6eAd1	následně
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
aplikační	aplikační	k2eAgFnSc6d1	aplikační
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vrstvení	vrstvení	k1gNnSc2	vrstvení
služeb	služba	k1gFnPc2	služba
architektury	architektura	k1gFnSc2	architektura
sítě	síť	k1gFnSc2	síť
OSI	OSI	kA	OSI
prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
žádosti	žádost	k1gFnPc4	žádost
od	od	k7c2	od
aplikační	aplikační	k2eAgFnSc2d1	aplikační
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
požadavky	požadavek	k1gInPc4	požadavek
relační	relační	k2eAgFnSc3d1	relační
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
modelu	model	k1gInSc6	model
OSI	OSI	kA	OSI
prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
aplikační	aplikační	k2eAgFnSc1d1	aplikační
vrstva	vrstva	k1gFnSc1	vrstva
jednoho	jeden	k4xCgInSc2	jeden
systému	systém	k1gInSc2	systém
odesílá	odesílat	k5eAaImIp3nS	odesílat
data	datum	k1gNnPc1	datum
čitelná	čitelný	k2eAgFnSc1d1	čitelná
aplikační	aplikační	k2eAgFnSc7d1	aplikační
vrstvou	vrstva	k1gFnSc7	vrstva
jiného	jiný	k2eAgInSc2d1	jiný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
pomocí	pomocí	k7c2	pomocí
EBCDIC	EBCDIC	kA	EBCDIC
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
pomocí	pomocí	k7c2	pomocí
ASCII	ascii	kA	ascii
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
prezentační	prezentační	k2eAgFnSc1d1	prezentační
vrstva	vrstva	k1gFnSc1	vrstva
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
schopna	schopen	k2eAgFnSc1d1	schopna
překládat	překládat	k5eAaImF	překládat
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
datovými	datový	k2eAgInPc7d1	datový
formáty	formát	k1gInPc7	formát
pomocí	pomocí	k7c2	pomocí
společného	společný	k2eAgInSc2d1	společný
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
TLS	TLS	kA	TLS
</s>
</p>
<p>
<s>
MIME	mim	k1gMnSc5	mim
</s>
</p>
<p>
<s>
XML	XML	kA	XML
</s>
</p>
<p>
<s>
Telnet	Telnet	k1gMnSc1	Telnet
</s>
</p>
<p>
<s>
Abstract	Abstracta	k1gFnPc2	Abstracta
Syntax	syntax	k1gFnSc1	syntax
Notation	Notation	k1gInSc1	Notation
One	One	k1gFnSc1	One
</s>
</p>
<p>
<s>
TDI	TDI	kA	TDI
</s>
</p>
<p>
<s>
External	Externat	k5eAaImAgMnS	Externat
Data	datum	k1gNnPc1	datum
Representation	Representation	k1gInSc1	Representation
</s>
</p>
<p>
<s>
NetWare	NetWar	k1gMnSc5	NetWar
Core	Corus	k1gMnSc5	Corus
Protocol	Protocol	k1gInSc4	Protocol
</s>
</p>
<p>
<s>
Apple	Apple	kA	Apple
Filing	Filing	k1gInSc1	Filing
Protocol	Protocol	k1gInSc1	Protocol
</s>
</p>
