<s>
Malárie	malárie	k1gFnSc1	malárie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
"	"	kIx"	"
<g/>
špatný	špatný	k2eAgInSc1d1	špatný
vzduch	vzduch	k1gInSc1	vzduch
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
rovněž	rovněž	k9	rovněž
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
bahenní	bahenní	k2eAgFnSc1d1	bahenní
zimnice	zimnice	k1gFnSc1	zimnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
nemocných	nemocná	k1gFnPc2	nemocná
malárii	malárie	k1gFnSc3	malárie
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
mladší	mladý	k2eAgFnPc1d2	mladší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obětí	oběť	k1gFnPc2	oběť
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
a	a	k8xC	a
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
mohla	moct	k5eAaImAgFnS	moct
zabít	zabít	k5eAaPmF	zabít
až	až	k9	až
polovinu	polovina	k1gFnSc4	polovina
lidí	člověk	k1gMnPc2	člověk
co	co	k3yQnSc4	co
kdy	kdy	k6eAd1	kdy
žilo	žít	k5eAaImAgNnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
Plasmodii	plasmodium	k1gNnPc7	plasmodium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
zimničky	zimnička	k1gFnPc4	zimnička
<g/>
,	,	kIx,	,
parazitickými	parazitický	k2eAgMnPc7d1	parazitický
prvoky	prvok	k1gMnPc7	prvok
kmene	kmen	k1gInSc2	kmen
Apicomplexa	Apicomplex	k1gInSc2	Apicomplex
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
přenašečem	přenašeč	k1gInSc7	přenašeč
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
komár	komár	k1gMnSc1	komár
rodu	rod	k1gInSc2	rod
Anopheles	Anopheles	k1gMnSc1	Anopheles
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
zoonózu	zoonóza	k1gFnSc4	zoonóza
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
napadá	napadat	k5eAaImIp3nS	napadat
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
původce	původce	k1gMnSc2	původce
malárie	malárie	k1gFnSc2	malárie
získal	získat	k5eAaPmAgMnS	získat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
Charles	Charles	k1gMnSc1	Charles
Louis	Louis	k1gMnSc1	Louis
Alphonse	Alphonse	k1gFnSc2	Alphonse
Laveran	Laveran	k1gInSc1	Laveran
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
Nobelovou	Nobelová	k1gFnSc4	Nobelová
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
i	i	k9	i
za	za	k7c4	za
objev	objev	k1gInSc4	objev
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
malárie	malárie	k1gFnSc2	malárie
na	na	k7c6	na
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
malárie	malárie	k1gFnSc2	malárie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
projeví	projevit	k5eAaPmIp3nS	projevit
8	[number]	k4	8
až	až	k9	až
25	[number]	k4	25
dní	den	k1gInPc2	den
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
infikace	infikace	k1gFnSc2	infikace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
využívajících	využívající	k2eAgMnPc2d1	využívající
antimalarickou	antimalarický	k2eAgFnSc4d1	antimalarická
profylaxi	profylaxe	k1gFnSc4	profylaxe
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
symptomy	symptom	k1gInPc1	symptom
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgInPc1d1	společný
pro	pro	k7c4	pro
nákazu	nákaza	k1gFnSc4	nákaza
všemi	všecek	k3xTgFnPc7	všecek
druhy	druh	k1gInPc1	druh
plasmodií	plasmodium	k1gNnPc2	plasmodium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
podobají	podobat	k5eAaImIp3nP	podobat
příznakům	příznak	k1gInPc3	příznak
chřipkového	chřipkový	k2eAgNnSc2d1	chřipkové
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
kloubů	kloub	k1gInPc2	kloub
či	či	k8xC	či
zvracení	zvracení	k1gNnSc2	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
životní	životní	k2eAgFnSc3d1	životní
strategii	strategie	k1gFnSc3	strategie
patogenního	patogenní	k2eAgMnSc2d1	patogenní
prvoka	prvok	k1gMnSc2	prvok
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
rozpadu	rozpad	k1gInSc3	rozpad
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
anémii	anémie	k1gFnSc3	anémie
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
vysoké	vysoký	k2eAgFnSc2d1	vysoká
koncentrace	koncentrace	k1gFnSc2	koncentrace
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zvětšení	zvětšení	k1gNnSc3	zvětšení
sleziny	slezina	k1gFnSc2	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečný	přebytečný	k2eAgInSc1d1	přebytečný
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nS	patřit
pocit	pocit	k1gInSc1	pocit
mravenčení	mravenčení	k1gNnSc2	mravenčení
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
malárie	malárie	k1gFnSc2	malárie
způsobené	způsobený	k2eAgFnSc2d1	způsobená
zimničkou	zimnička	k1gFnSc7	zimnička
Plasmodium	plasmodium	k1gNnSc1	plasmodium
falciparum	falciparum	k1gNnSc4	falciparum
<g/>
.	.	kIx.	.
</s>
<s>
Komplikace	komplikace	k1gFnPc1	komplikace
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
kóma	kóma	k1gNnSc4	kóma
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
úmrtí	úmrtí	k1gNnSc4	úmrtí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
malárie	malárie	k1gFnSc1	malárie
adekvátně	adekvátně	k6eAd1	adekvátně
léčena	léčen	k2eAgFnSc1d1	léčena
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příznakem	příznak	k1gInSc7	příznak
malárie	malárie	k1gFnSc1	malárie
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
návaly	nával	k1gInPc1	nával
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
zimnice	zimnice	k1gFnSc1	zimnice
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
každé	každý	k3xTgInPc1	každý
dva	dva	k4xCgInPc1	dva
(	(	kIx(	(
<g/>
P.	P.	kA	P.
vivax	vivax	k1gInSc1	vivax
a	a	k8xC	a
P.	P.	kA	P.
ovale	ovale	k1gInSc1	ovale
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
(	(	kIx(	(
<g/>
P.	P.	kA	P.
malariae	malaria	k1gInPc1	malaria
<g/>
)	)	kIx)	)
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažněji	závažně	k6eAd3	závažně
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
infekce	infekce	k1gFnSc1	infekce
P.	P.	kA	P.
falciparum	falciparum	k1gNnSc1	falciparum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
horečka	horečka	k1gFnSc1	horečka
vrací	vracet	k5eAaImIp3nS	vracet
po	po	k7c4	po
každých	každý	k3xTgFnPc2	každý
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
rizikový	rizikový	k2eAgInSc1d1	rizikový
je	být	k5eAaImIp3nS	být
průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
jako	jako	k9	jako
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
,	,	kIx,	,
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
primáti	primát	k1gMnPc1	primát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
též	též	k9	též
trpí	trpět	k5eAaImIp3nS	trpět
malárií	malárie	k1gFnSc7	malárie
<g/>
,	,	kIx,	,
způsobovanou	způsobovaný	k2eAgFnSc7d1	způsobovaná
ale	ale	k9	ale
obvykle	obvykle	k6eAd1	obvykle
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
zimniček	zimnička	k1gFnPc2	zimnička
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
specifickým	specifický	k2eAgNnSc7d1	specifické
hostitelům	hostitel	k1gMnPc3	hostitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
napadat	napadat	k5eAaImF	napadat
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
hostitele	hostitel	k1gMnPc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
samička	samička	k1gFnSc1	samička
komára	komár	k1gMnSc2	komár
Anopheles	Anopheles	k1gInSc1	Anopheles
přenáší	přenášet	k5eAaImIp3nS	přenášet
pohyblivé	pohyblivý	k2eAgNnSc4d1	pohyblivé
infekční	infekční	k2eAgNnSc4d1	infekční
stádium	stádium	k1gNnSc4	stádium
výtrusovce	výtrusovec	k1gMnSc2	výtrusovec
Plasmodium	plasmodium	k1gNnSc4	plasmodium
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
slinných	slinný	k2eAgFnPc6d1	slinná
žlázách	žláza	k1gFnPc6	žláza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
dochází	docházet	k5eAaImIp3nS	docházet
většinou	většinou	k6eAd1	většinou
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
samičky	samička	k1gFnPc4	samička
komárů	komár	k1gMnPc2	komár
nejaktivnější	aktivní	k2eAgInPc1d3	nejaktivnější
a	a	k8xC	a
sají	sát	k5eAaImIp3nP	sát
krev	krev	k1gFnSc4	krev
teplokrevných	teplokrevný	k2eAgInPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
přitom	přitom	k6eAd1	přitom
přenesou	přenést	k5eAaPmIp3nP	přenést
i	i	k8xC	i
Plasmodia	plasmodium	k1gNnPc1	plasmodium
(	(	kIx(	(
<g/>
zimničky	zimnička	k1gFnSc2	zimnička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
uchytí	uchytit	k5eAaPmIp3nP	uchytit
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
infikovaného	infikovaný	k2eAgInSc2d1	infikovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jaterních	jaterní	k2eAgFnPc6d1	jaterní
buňkách	buňka	k1gFnPc6	buňka
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
parazitů	parazit	k1gMnPc2	parazit
a	a	k8xC	a
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
stádia	stádium	k1gNnSc2	stádium
–	–	k?	–
merozoitu	merozoit	k1gMnSc3	merozoit
<g/>
.	.	kIx.	.
</s>
<s>
Merozoity	Merozoita	k1gFnPc1	Merozoita
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
množí	množit	k5eAaImIp3nP	množit
a	a	k8xC	a
v	v	k7c6	v
periodických	periodický	k2eAgInPc6d1	periodický
intervalech	interval	k1gInPc6	interval
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
masovému	masový	k2eAgNnSc3d1	masové
uvolňování	uvolňování	k1gNnSc3	uvolňování
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
typické	typický	k2eAgInPc1d1	typický
malarické	malarický	k2eAgInPc1d1	malarický
záchvaty	záchvat	k1gInPc1	záchvat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
každé	každý	k3xTgFnSc2	každý
3	[number]	k4	3
nebo	nebo	k8xC	nebo
4	[number]	k4	4
dny	den	k1gInPc1	den
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc1	krev
zaplavena	zaplavit	k5eAaPmNgFnS	zaplavit
čerstvou	čerstvý	k2eAgFnSc7d1	čerstvá
vlnou	vlna	k1gFnSc7	vlna
merozoitů	merozoit	k1gInPc2	merozoit
<g/>
.	.	kIx.	.
</s>
<s>
Parazit	parazit	k1gMnSc1	parazit
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
aktivitami	aktivita	k1gFnPc7	aktivita
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
uvnitř	uvnitř	k7c2	uvnitř
jaterních	jaterní	k2eAgFnPc2d1	jaterní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Cirkulující	cirkulující	k2eAgFnPc1d1	cirkulující
infikované	infikovaný	k2eAgFnPc1d1	infikovaná
krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
likvidovány	likvidovat	k5eAaBmNgInP	likvidovat
ve	v	k7c6	v
slezině	slezina	k1gFnSc6	slezina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plasmodium	plasmodium	k1gNnSc1	plasmodium
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nP	měnit
své	svůj	k3xOyFgInPc4	svůj
povrchové	povrchový	k2eAgInPc4d1	povrchový
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dokáže	dokázat	k5eAaPmIp3nS	dokázat
imunitní	imunitní	k2eAgFnSc4d1	imunitní
reakci	reakce	k1gFnSc4	reakce
účinně	účinně	k6eAd1	účinně
unikat	unikat	k5eAaImF	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
lidský	lidský	k2eAgInSc4d1	lidský
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
naučí	naučit	k5eAaPmIp3nP	naučit
rozeznat	rozeznat	k5eAaPmF	rozeznat
povrchový	povrchový	k2eAgInSc4d1	povrchový
protein	protein	k1gInSc4	protein
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
produkovat	produkovat	k5eAaImF	produkovat
specifické	specifický	k2eAgFnPc4d1	specifická
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
parazit	parazit	k1gMnSc1	parazit
přepne	přepnout	k5eAaPmIp3nS	přepnout
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
formy	forma	k1gFnSc2	forma
proteinu	protein	k1gInSc2	protein
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
znesnadní	znesnadnit	k5eAaPmIp3nS	znesnadnit
práci	práce	k1gFnSc4	práce
imunitnímu	imunitní	k2eAgInSc3d1	imunitní
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Přilnavost	přilnavost	k1gFnSc1	přilnavost
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
je	být	k5eAaImIp3nS	být
zvlášť	zvlášť	k6eAd1	zvlášť
výrazná	výrazný	k2eAgFnSc1d1	výrazná
u	u	k7c2	u
malárie	malárie	k1gFnSc2	malárie
způsobené	způsobený	k2eAgFnSc2d1	způsobená
zimničkou	zimnička	k1gFnSc7	zimnička
Plasmodium	plasmodium	k1gNnSc1	plasmodium
falciparum	falciparum	k1gNnSc1	falciparum
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
faktor	faktor	k1gInSc1	faktor
způsobující	způsobující	k2eAgFnSc2d1	způsobující
komplikace	komplikace	k1gFnSc2	komplikace
z	z	k7c2	z
krvácení	krvácení	k1gNnSc2	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
merozoity	merozoita	k1gFnPc1	merozoita
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
do	do	k7c2	do
samičích	samičí	k2eAgInPc2d1	samičí
a	a	k8xC	a
samčích	samčí	k2eAgInPc2d1	samčí
gametocytů	gametocyt	k1gInPc2	gametocyt
<g/>
.	.	kIx.	.
</s>
<s>
Bodne	bodnout	k5eAaPmIp3nS	bodnout
<g/>
-li	i	k?	-li
komár	komár	k1gMnSc1	komár
infikovanou	infikovaný	k2eAgFnSc4d1	infikovaná
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
nasaje	nasát	k5eAaPmIp3nS	nasát
<g/>
-li	i	k?	-li
gametocyty	gametocyt	k1gInPc1	gametocyt
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
oplodnění	oplodnění	k1gNnSc1	oplodnění
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
komára	komár	k1gMnSc2	komár
<g/>
,	,	kIx,	,
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
nové	nový	k2eAgFnPc4d1	nová
sporozoity	sporozoita	k1gFnPc4	sporozoita
a	a	k8xC	a
putují	putovat	k5eAaImIp3nP	putovat
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
slinné	slinný	k2eAgFnSc2d1	slinná
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
reprodukčního	reprodukční	k2eAgInSc2d1	reprodukční
cyklu	cyklus	k1gInSc2	cyklus
plasmodia	plasmodium	k1gNnSc2	plasmodium
<g/>
.	.	kIx.	.
</s>
<s>
Těhotné	těhotný	k2eAgFnPc1d1	těhotná
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
komáry	komár	k1gMnPc4	komár
zvláště	zvláště	k6eAd1	zvláště
přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
a	a	k8xC	a
malárie	malárie	k1gFnPc1	malárie
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
příčina	příčina	k1gFnSc1	příčina
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
mortality	mortalita	k1gFnSc2	mortalita
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
způsobující	způsobující	k2eAgFnSc4d1	způsobující
nákazu	nákaza	k1gFnSc4	nákaza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
Plasmodium	plasmodium	k1gNnSc4	plasmodium
falciparum	falciparum	k1gInSc1	falciparum
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
80	[number]	k4	80
%	%	kIx~	%
známých	známý	k2eAgInPc2d1	známý
případů	případ	k1gInPc2	případ
a	a	k8xC	a
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Plasmodium	plasmodium	k1gNnSc1	plasmodium
vivax	vivax	k1gInSc4	vivax
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Plasmodium	plasmodium	k1gNnSc1	plasmodium
ovale	ovale	k1gFnSc2	ovale
<g/>
,	,	kIx,	,
Plasmodium	plasmodium	k1gNnSc1	plasmodium
malaria	malarium	k1gNnSc2	malarium
<g/>
,	,	kIx,	,
Plasmodium	plasmodium	k1gNnSc1	plasmodium
knowlesi	knowlese	k1gFnSc3	knowlese
a	a	k8xC	a
Plasmodium	plasmodium	k1gNnSc4	plasmodium
semiovale	semiovale	k6eAd1	semiovale
jsou	být	k5eAaImIp3nP	být
známé	známý	k1gMnPc4	známý
jako	jako	k9	jako
původci	původce	k1gMnPc1	původce
malárie	malárie	k1gFnSc2	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
bakterii	bakterie	k1gFnSc3	bakterie
Treponema	Treponemum	k1gNnSc2	Treponemum
pallidum	pallidum	k1gInSc1	pallidum
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc1d1	způsobující
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
léčby	léčba	k1gFnSc2	léčba
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysoké	vysoký	k2eAgFnPc4d1	vysoká
horečky	horečka	k1gFnPc4	horečka
<g/>
,	,	kIx,	,
provázející	provázející	k2eAgFnSc4d1	provázející
malárii	malárie	k1gFnSc4	malárie
<g/>
,	,	kIx,	,
zabily	zabít	k5eAaPmAgFnP	zabít
i	i	k9	i
tuto	tento	k3xDgFnSc4	tento
bakterii	bakterie	k1gFnSc4	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
Julius	Julius	k1gMnSc1	Julius
Wagner-Jauregg	Wagner-Jauregg	k1gMnSc1	Wagner-Jauregg
a	a	k8xC	a
zkusil	zkusit	k5eAaPmAgMnS	zkusit
malárii	malárie	k1gFnSc4	malárie
aplikovat	aplikovat	k5eAaBmF	aplikovat
cíleně	cíleně	k6eAd1	cíleně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
malárie	malárie	k1gFnSc2	malárie
dostal	dostat	k5eAaPmAgInS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
nepoužíval	používat	k5eNaImAgMnS	používat
smrtící	smrtící	k2eAgNnSc1d1	smrtící
Plasmodium	plasmodium	k1gNnSc1	plasmodium
falciparum	falciparum	k1gInSc1	falciparum
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
opičí	opičí	k2eAgFnSc4d1	opičí
malárii	malárie	k1gFnSc4	malárie
Plasmodium	plasmodium	k1gNnSc4	plasmodium
knowlesi	knowlese	k1gFnSc4	knowlese
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
používání	používání	k1gNnSc2	používání
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
jedno	jeden	k4xCgNnSc1	jeden
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
:	:	kIx,	:
po	po	k7c4	po
asi	asi	k9	asi
170	[number]	k4	170
generacích	generace	k1gFnPc6	generace
umělého	umělý	k2eAgNnSc2d1	umělé
předávání	předávání	k1gNnSc2	předávání
choroby	choroba	k1gFnSc2	choroba
injekcemi	injekce	k1gFnPc7	injekce
mezi	mezi	k7c7	mezi
pacienty	pacient	k1gMnPc7	pacient
se	se	k3xPyFc4	se
původce	původce	k1gMnSc4	původce
malárie	malárie	k1gFnSc2	malárie
stal	stát	k5eAaPmAgInS	stát
podobně	podobně	k6eAd1	podobně
smrtící	smrtící	k2eAgInSc1d1	smrtící
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Plasmodium	plasmodium	k1gNnSc1	plasmodium
falciparum	falciparum	k1gNnSc1	falciparum
(	(	kIx(	(
<g/>
virulence	virulence	k1gFnSc1	virulence
postupně	postupně	k6eAd1	postupně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
asi	asi	k9	asi
500	[number]	k4	500
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
léčbou	léčba	k1gFnSc7	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
malárie	malárie	k1gFnSc2	malárie
se	se	k3xPyFc4	se
ve	v	k7c6	v
vážných	vážný	k2eAgInPc6d1	vážný
případech	případ	k1gInPc6	případ
používá	používat	k5eAaImIp3nS	používat
chinin	chinin	k1gInSc1	chinin
<g/>
.	.	kIx.	.
</s>
<s>
Malárii	malárie	k1gFnSc3	malárie
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nepodaří	podařit	k5eNaPmIp3nS	podařit
zcela	zcela	k6eAd1	zcela
vyléčit	vyléčit	k5eAaPmF	vyléčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vracet	vracet	k5eAaImF	vracet
i	i	k9	i
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
proti	proti	k7c3	proti
nákaze	nákaza	k1gFnSc3	nákaza
malárií	malárie	k1gFnPc2	malárie
je	být	k5eAaImIp3nS	být
moskytiéra	moskytiéra	k1gFnSc1	moskytiéra
napuštěná	napuštěný	k2eAgFnSc1d1	napuštěná
repelentem	repelent	k1gInSc7	repelent
a	a	k8xC	a
léky	lék	k1gInPc7	lék
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Malaron	Malaron	k1gMnSc1	Malaron
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgMnSc1d1	obsahující
atovaquon	atovaquon	k1gMnSc1	atovaquon
a	a	k8xC	a
proguanil	proguanit	k5eAaPmAgMnS	proguanit
<g/>
,	,	kIx,	,
Lariam	Lariam	k1gInSc1	Lariam
(	(	kIx(	(
<g/>
meflochin	meflochin	k1gInSc1	meflochin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Mephaquine	Mephaquin	k1gInSc5	Mephaquin
(	(	kIx(	(
<g/>
meflochin	meflochin	k1gInSc4	meflochin
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
Doxycyklin	Doxycyklin	k2eAgInSc1d1	Doxycyklin
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
malárii	malárie	k1gFnSc6	malárie
umírají	umírat	k5eAaImIp3nP	umírat
ročně	ročně	k6eAd1	ročně
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
90	[number]	k4	90
%	%	kIx~	%
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgFnSc1d1	tradiční
léčba	léčba	k1gFnSc1	léčba
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
neefektivní	efektivní	k2eNgMnSc1d1	neefektivní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
roste	růst	k5eAaImIp3nS	růst
rezistence	rezistence	k1gFnSc1	rezistence
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
léčiva	léčivo	k1gNnPc1	léčivo
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
rozvojové	rozvojový	k2eAgFnPc4d1	rozvojová
země	zem	k1gFnPc4	zem
příliš	příliš	k6eAd1	příliš
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
prevence	prevence	k1gFnSc1	prevence
–	–	k?	–
některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
dosud	dosud	k6eAd1	dosud
realizují	realizovat	k5eAaBmIp3nP	realizovat
postřiky	postřik	k1gInPc1	postřik
kolonií	kolonie	k1gFnPc2	kolonie
komárů	komár	k1gMnPc2	komár
DDT	DDT	kA	DDT
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
nejlevnější	levný	k2eAgFnSc1d3	nejlevnější
a	a	k8xC	a
i	i	k9	i
pro	pro	k7c4	pro
chudou	chudý	k2eAgFnSc4d1	chudá
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
zemi	zem	k1gFnSc4	zem
dostupný	dostupný	k2eAgMnSc1d1	dostupný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
katastrofální	katastrofální	k2eAgInSc1d1	katastrofální
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dopady	dopad	k1gInPc7	dopad
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
nejúčinnější	účinný	k2eAgInSc4d3	nejúčinnější
způsob	způsob	k1gInSc4	způsob
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
považována	považován	k2eAgFnSc1d1	považována
distribuce	distribuce	k1gFnSc1	distribuce
moskytiér	moskytiéra	k1gFnPc2	moskytiéra
–	–	k?	–
ty	ten	k3xDgInPc1	ten
totiž	totiž	k9	totiž
lehce	lehko	k6eAd1	lehko
ochrání	ochránit	k5eAaPmIp3nS	ochránit
hlavně	hlavně	k9	hlavně
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nemocí	nemoc	k1gFnSc7	nemoc
ohroženy	ohrozit	k5eAaPmNgInP	ohrozit
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc1d1	zásadní
je	být	k5eAaImIp3nS	být
také	také	k9	také
včasné	včasný	k2eAgNnSc1d1	včasné
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chybějí	chybět	k5eAaImIp3nP	chybět
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
,	,	kIx,	,
léky	lék	k1gInPc1	lék
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Zastavení	zastavení	k1gNnSc1	zastavení
šíření	šíření	k1gNnSc2	šíření
malárie	malárie	k1gFnSc2	malárie
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
cílů	cíl	k1gInPc2	cíl
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
projektů	projekt	k1gInPc2	projekt
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
Počet	počet	k1gInSc4	počet
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Guinea	guinea	k1gFnSc2	guinea
75	[number]	k4	75
386	[number]	k4	386
Botswana	Botswana	k1gFnSc1	Botswana
48	[number]	k4	48
704	[number]	k4	704
Burundi	Burundi	k1gNnSc4	Burundi
48	[number]	k4	48
098	[number]	k4	098
Zambie	Zambie	k1gFnSc1	Zambie
34	[number]	k4	34
204	[number]	k4	204
Malawi	Malawi	k1gNnSc4	Malawi
25	[number]	k4	25
948	[number]	k4	948
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
malárie	malárie	k1gFnSc1	malárie
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
i	i	k9	i
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
moderního	moderní	k2eAgInSc2d1	moderní
typu	typ	k1gInSc2	typ
zřejmě	zřejmě	k6eAd1	zřejmě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
15	[number]	k4	15
000	[number]	k4	000
až	až	k8xS	až
8	[number]	k4	8
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
doklad	doklad	k1gInSc1	doklad
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
výskytu	výskyt	k1gInSc6	výskyt
pochází	pocházet	k5eAaImIp3nS	pocházet
dokonce	dokonce	k9	dokonce
již	již	k6eAd1	již
z	z	k7c2	z
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
jantaru	jantar	k1gInSc2	jantar
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
asi	asi	k9	asi
15	[number]	k4	15
až	až	k8xS	až
20	[number]	k4	20
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Entomolog	entomolog	k1gMnSc1	entomolog
a	a	k8xC	a
badatel	badatel	k1gMnSc1	badatel
George	George	k1gFnPc2	George
Poinar	Poinar	k1gMnSc1	Poinar
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
ancestrální	ancestrální	k2eAgFnSc4d1	ancestrální
formu	forma	k1gFnSc4	forma
malárie	malárie	k1gFnSc2	malárie
již	již	k6eAd1	již
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
barmského	barmský	k2eAgInSc2d1	barmský
jantaru	jantar	k1gInSc2	jantar
starého	starý	k2eAgInSc2d1	starý
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
tedy	tedy	k9	tedy
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
druhohorní	druhohorní	k2eAgFnSc2d1	druhohorní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Parazitologie	parazitologie	k1gFnSc1	parazitologie
–	–	k?	–
aktuálně	aktuálně	k6eAd1	aktuálně
o	o	k7c6	o
malárii	malárie	k1gFnSc6	malárie
<g/>
,	,	kIx,	,
odborný	odborný	k2eAgInSc1d1	odborný
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
2017-04-04	[number]	k4	2017-04-04
http://www.malariacontrol.net/	[url]	k?	http://www.malariacontrol.net/
–	–	k?	–
projekt	projekt	k1gInSc1	projekt
modelu	model	k1gInSc2	model
šíření	šíření	k1gNnSc2	šíření
malárie	malárie	k1gFnSc2	malárie
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
léčení	léčení	k1gNnSc2	léčení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
genetickou	genetický	k2eAgFnSc7d1	genetická
úpravou	úprava	k1gFnSc7	úprava
komárů	komár	k1gMnPc2	komár
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
malárie	malárie	k1gFnSc2	malárie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
malárie	malárie	k1gFnSc2	malárie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
́	́	k?	́
</s>
