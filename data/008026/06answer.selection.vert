<s>
Malárie	malárie	k1gFnSc1	malárie
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
Plasmodii	plasmodium	k1gNnPc7	plasmodium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
zimničky	zimnička	k1gFnPc4	zimnička
<g/>
,	,	kIx,	,
parazitickými	parazitický	k2eAgMnPc7d1	parazitický
prvoky	prvok	k1gMnPc7	prvok
kmene	kmen	k1gInSc2	kmen
Apicomplexa	Apicomplex	k1gInSc2	Apicomplex
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
přenašečem	přenašeč	k1gInSc7	přenašeč
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
komár	komár	k1gMnSc1	komár
rodu	rod	k1gInSc2	rod
Anopheles	Anopheles	k1gMnSc1	Anopheles
<g/>
.	.	kIx.	.
</s>
