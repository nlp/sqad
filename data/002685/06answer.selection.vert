<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Vír	Vír	k1gInSc1	Vír
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
