<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
andorrským	andorrský	k2eAgMnSc7d1	andorrský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
je	být	k5eAaImIp3nS	být
Albert	Albert	k1gMnSc1	Albert
Salvadó	Salvadó	k1gMnSc1	Salvadó
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
i	i	k8xC	i
detektivek	detektivka	k1gFnPc2	detektivka
<g/>
.	.	kIx.	.
</s>
