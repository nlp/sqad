<s>
Neipyijto	Neipyijto	k1gNnSc1	Neipyijto
<g/>
,	,	kIx,	,
Nepjito	Nepjit	k2eAgNnSc1d1	Nepjit
nebo	nebo	k8xC	nebo
také	také	k9	také
Nepyito	Nepyit	k2eAgNnSc1d1	Nepyit
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Naypyidaw	Naypyidaw	k1gMnSc1	Naypyidaw
nebo	nebo	k8xC	nebo
Nay	Nay	k1gMnSc1	Nay
Pyi	Pyi	k1gMnSc1	Pyi
Taw	Taw	k1gMnSc1	Taw
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
podle	podle	k7c2	podle
gigantických	gigantický	k2eAgFnPc2d1	gigantická
soch	socha	k1gFnPc2	socha
barmských	barmský	k2eAgMnPc2d1	barmský
králů	král	k1gMnPc2	král
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Mandalajské	Mandalajský	k2eAgFnSc6d1	Mandalajský
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Pjinmany	Pjinman	k1gMnPc7	Pjinman
(	(	kIx(	(
<g/>
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
320	[number]	k4	320
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Rangúnu	Rangún	k1gInSc2	Rangún
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
uprostřed	uprostřed	k7c2	uprostřed
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
probíhaly	probíhat	k5eAaImAgFnP	probíhat
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
v	v	k7c6	v
přísném	přísný	k2eAgNnSc6d1	přísné
utajení	utajení	k1gNnSc6	utajení
před	před	k7c7	před
zahraničními	zahraniční	k2eAgNnPc7d1	zahraniční
médii	médium	k1gNnPc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
obytnou	obytný	k2eAgFnSc4d1	obytná
a	a	k8xC	a
administrativní	administrativní	k2eAgFnSc4d1	administrativní
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Neipyijto	Neipyijto	k1gNnSc4	Neipyijto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
