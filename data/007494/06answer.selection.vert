<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
provádí	provádět	k5eAaImIp3nS	provádět
postupnou	postupný	k2eAgFnSc4d1	postupná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
celého	celý	k2eAgInSc2d1	celý
značně	značně	k6eAd1	značně
zdevastovaného	zdevastovaný	k2eAgInSc2d1	zdevastovaný
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
