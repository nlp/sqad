<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
vzhlížely	vzhlížet	k5eAaImAgFnP	vzhlížet
kolonizované	kolonizovaný	k2eAgFnPc1d1	kolonizovaná
země	zem	k1gFnPc1	zem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc7	svůj
vlajkou	vlajka	k1gFnSc7	vlajka
–	–	k?	–
její	její	k3xOp3gMnSc1	její
zelená	zelenat	k5eAaImIp3nS	zelenat
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červená	k1gFnSc1	červená
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
panafrické	panafrický	k2eAgFnPc4d1	panafrická
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
