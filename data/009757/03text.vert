<p>
<s>
Rozinky	rozinka	k1gFnPc1	rozinka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
též	též	k9	též
hrozinky	hrozinka	k1gFnPc4	hrozinka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sušené	sušený	k2eAgInPc4d1	sušený
plody	plod	k1gInPc4	plod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bobule	bobule	k1gFnSc2	bobule
<g/>
)	)	kIx)	)
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
jíst	jíst	k5eAaImF	jíst
syrové	syrový	k2eAgFnSc3d1	syrová
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
pečení	pečení	k1gNnSc6	pečení
(	(	kIx(	(
<g/>
do	do	k7c2	do
těsta	těsto	k1gNnSc2	těsto
–	–	k?	–
např.	např.	kA	např.
bábovka	bábovka	k1gFnSc1	bábovka
<g/>
,	,	kIx,	,
mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
vánočka	vánočka	k1gFnSc1	vánočka
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
náplní	náplň	k1gFnPc2	náplň
–	–	k?	–
především	především	k9	především
tvarohové	tvarohový	k2eAgNnSc1d1	tvarohové
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
drobného	drobný	k2eAgNnSc2d1	drobné
sladkého	sladký	k2eAgNnSc2d1	sladké
pečiva	pečivo	k1gNnSc2	pečivo
–	–	k?	–
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
šátečky	šáteček	k1gInPc1	šáteček
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
či	či	k8xC	či
vaření	vaření	k1gNnSc1	vaření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
některé	některý	k3yIgFnPc1	některý
omáčky	omáčka	k1gFnPc1	omáčka
<g/>
,	,	kIx,	,
kapr	kapr	k1gMnSc1	kapr
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
<g/>
,	,	kIx,	,
čatní	čatní	k1gNnSc4	čatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozinky	rozinka	k1gFnPc1	rozinka
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
sladké	sladký	k2eAgFnSc2d1	sladká
díky	díky	k7c3	díky
velké	velký	k2eAgFnSc3d1	velká
koncentraci	koncentrace	k1gFnSc3	koncentrace
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozinky	rozinka	k1gFnPc1	rozinka
skladují	skladovat	k5eAaImIp3nP	skladovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
uvnitř	uvnitř	k6eAd1	uvnitř
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
<g/>
,	,	kIx,	,
krystalky	krystalka	k1gFnPc1	krystalka
se	se	k3xPyFc4	se
však	však	k9	však
dají	dát	k5eAaPmIp3nP	dát
rozpustit	rozpustit	k5eAaPmF	rozpustit
krátkým	krátký	k2eAgNnSc7d1	krátké
ponořením	ponoření	k1gNnSc7	ponoření
rozinek	rozinka	k1gFnPc2	rozinka
do	do	k7c2	do
nějaké	nějaký	k3yIgFnSc2	nějaký
tekutiny	tekutina	k1gFnSc2	tekutina
(	(	kIx(	(
<g/>
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
ovocná	ovocný	k2eAgFnSc1d1	ovocná
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
či	či	k8xC	či
horká	horký	k2eAgFnSc1d1	horká
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidová	lidový	k2eAgFnSc1d1	lidová
etymologie	etymologie	k1gFnSc1	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
rozinka	rozinka	k1gFnSc1	rozinka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
Rosine	Rosin	k1gMnSc5	Rosin
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
raisin	raisin	k1gMnSc1	raisin
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc4d1	suchý
<g/>
)	)	kIx)	)
hrozen	hrozen	k1gInSc4	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
latinském	latinský	k2eAgNnSc6d1	latinské
racē	racē	k?	racē
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
zrno	zrno	k1gNnSc1	zrno
<g/>
,	,	kIx,	,
hrozen	hrozen	k1gInSc1	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
hrozinky	hrozinka	k1gFnSc2	hrozinka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lidové	lidový	k2eAgFnSc6d1	lidová
etymologii	etymologie	k1gFnSc6	etymologie
přikloněním	přiklonění	k1gNnSc7	přiklonění
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
hrozen	hrozen	k1gInSc1	hrozen
již	již	k9	již
dávno	dávno	k6eAd1	dávno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
pravidel	pravidlo	k1gNnPc2	pravidlo
pravopisu	pravopis	k1gInSc2	pravopis
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
obě	dva	k4xCgFnPc1	dva
možnosti	možnost	k1gFnPc1	možnost
pravopisně	pravopisně	k6eAd1	pravopisně
správné	správný	k2eAgFnPc1d1	správná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sytost	sytost	k1gFnSc1	sytost
zabarvení	zabarvení	k1gNnSc2	zabarvení
===	===	k?	===
</s>
</p>
<p>
<s>
Různého	různý	k2eAgInSc2d1	různý
výsledného	výsledný	k2eAgInSc2d1	výsledný
odstínu	odstín	k1gInSc2	odstín
zabarvení	zabarvení	k1gNnSc2	zabarvení
rozinek	rozinka	k1gFnPc2	rozinka
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
při	při	k7c6	při
rozdělení	rozdělení	k1gNnSc6	rozdělení
jedné	jeden	k4xCgFnSc2	jeden
vstupní	vstupní	k2eAgFnSc2d1	vstupní
várky	várka	k1gFnSc2	várka
hroznů	hrozen	k1gInPc2	hrozen
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
dávky	dávka	k1gFnPc4	dávka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ujgurové	Ujgur	k1gMnPc1	Ujgur
v	v	k7c6	v
Turfanu	Turfan	k1gInSc6	Turfan
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
suší	sušit	k5eAaImIp3nP	sušit
své	svůj	k3xOyFgFnPc4	svůj
rozinky	rozinka	k1gFnPc4	rozinka
tradičně	tradičně	k6eAd1	tradičně
40	[number]	k4	40
dní	den	k1gInPc2	den
v	v	k7c6	v
tmavých	tmavý	k2eAgFnPc6d1	tmavá
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
chladných	chladný	k2eAgFnPc6d1	chladná
větrných	větrný	k2eAgFnPc6d1	větrná
sušárnách	sušárna	k1gFnPc6	sušárna
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bobule	bobule	k1gFnPc1	bobule
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
světlé	světlý	k2eAgFnPc1d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
žlutozelené	žlutozelený	k2eAgFnPc1d1	žlutozelená
bobule	bobule	k1gFnPc1	bobule
suší	sušit	k5eAaImIp3nP	sušit
na	na	k7c6	na
přímém	přímý	k2eAgNnSc6d1	přímé
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
ztmavnou	ztmavnout	k5eAaPmIp3nP	ztmavnout
<g/>
:	:	kIx,	:
Zhnědnou	zhnědnout	k5eAaPmIp3nP	zhnědnout
karamelizací	karamelizace	k1gFnSc7	karamelizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc1d1	zásadní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jas	jas	k1gInSc4	jas
<g/>
,	,	kIx,	,
zesvětluje	zesvětlovat	k5eAaImIp3nS	zesvětlovat
<g/>
:	:	kIx,	:
hnědé	hnědý	k2eAgFnPc1d1	hnědá
rozinky	rozinka	k1gFnPc1	rozinka
projasní	projasnit	k5eAaPmIp3nP	projasnit
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
až	až	k9	až
jantarově	jantarově	k6eAd1	jantarově
oranžova	oranžova	k6eAd1	oranžova
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
zelenkavý	zelenkavý	k2eAgInSc1d1	zelenkavý
nádech	nádech	k1gInSc1	nádech
ještě	ještě	k6eAd1	ještě
šťavnatých	šťavnatý	k2eAgFnPc2d1	šťavnatá
bobulí	bobule	k1gFnPc2	bobule
ale	ale	k8xC	ale
už	už	k6eAd1	už
vrátit	vrátit	k5eAaPmF	vrátit
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
bělicí	bělicí	k2eAgInSc4d1	bělicí
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
je	být	k5eAaImIp3nS	být
však	však	k9	však
hlavně	hlavně	k6eAd1	hlavně
používán	používán	k2eAgInSc1d1	používán
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
dezinfekční	dezinfekční	k2eAgInPc4d1	dezinfekční
a	a	k8xC	a
konzervační	konzervační	k2eAgInPc4d1	konzervační
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odrůdové	odrůdový	k2eAgMnPc4d1	odrůdový
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
a	a	k8xC	a
odrůdy	odrůda	k1gFnSc2	odrůda
hroznů	hrozen	k1gInPc2	hrozen
lze	lze	k6eAd1	lze
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
rozinky	rozinka	k1gFnPc4	rozinka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
smyrenské	smyrenský	k2eAgFnPc4d1	smyrenský
rozinky	rozinka	k1gFnPc4	rozinka
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgFnPc4d1	světlá
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
<g/>
:	:	kIx,	:
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sultánky	sultánka	k1gFnPc1	sultánka
-	-	kIx~	-
obzvlášť	obzvlášť	k6eAd1	obzvlášť
sladké	sladký	k2eAgFnPc4d1	sladká
rozinky	rozinka	k1gFnPc4	rozinka
sušené	sušený	k2eAgFnPc4d1	sušená
z	z	k7c2	z
vinných	vinný	k2eAgFnPc2d1	vinná
odrůd	odrůda	k1gFnPc2	odrůda
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
<g/>
,	,	kIx,	,
bezsemennými	bezsemenný	k2eAgInPc7d1	bezsemenný
plody	plod	k1gInPc7	plod
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
žlutozelenými	žlutozelený	k2eAgFnPc7d1	žlutozelená
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
damascénky	damascénka	k1gFnPc1	damascénka
a	a	k8xC	a
</s>
</p>
<p>
<s>
perské	perský	k2eAgFnPc1d1	perská
sultánky	sultánka	k1gFnPc1	sultánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
korintky	korintka	k1gFnPc1	korintka
-	-	kIx~	-
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
z	z	k7c2	z
odrůdy	odrůda	k1gFnSc2	odrůda
Korinthiaki	Korinthiak	k1gFnSc2	Korinthiak
<g/>
,	,	kIx,	,
též	též	k9	též
Zante	Zant	k1gInSc5	Zant
<g/>
,	,	kIx,	,
z	z	k7c2	z
Korintu	Korint	k1gInSc2	Korint
<g/>
,	,	kIx,	,
též	též	k9	též
Černý	černý	k2eAgInSc1d1	černý
Korint	Korint	k1gInSc1	Korint
<g/>
:	:	kIx,	:
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
cibéby	cibéba	k1gFnPc1	cibéba
-	-	kIx~	-
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
zibiba	zibib	k1gMnSc2	zibib
<g/>
)	)	kIx)	)
protáhlé	protáhlý	k2eAgFnPc1d1	protáhlá
a	a	k8xC	a
s	s	k7c7	s
peckami	pecka	k1gFnPc7	pecka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
italské	italský	k2eAgNnSc1d1	italské
-	-	kIx~	-
s	s	k7c7	s
vůní	vůně	k1gFnSc7	vůně
po	po	k7c6	po
muškátu	muškát	k1gInSc6	muškát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
rozinek	rozinka	k1gFnPc2	rozinka
byla	být	k5eAaImAgFnS	být
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvíce	hodně	k6eAd3	hodně
<g/>
:	:	kIx,	:
Turecko	Turecko	k1gNnSc1	Turecko
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
USA	USA	kA	USA
28	[number]	k4	28
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
13	[number]	k4	13
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
Chile	Chile	k1gNnSc1	Chile
po	po	k7c6	po
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
čínská	čínský	k2eAgFnSc1d1	čínská
roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
rozinek	rozinka	k1gFnPc2	rozinka
předpovězena	předpovězen	k2eAgFnSc1d1	předpovězena
mezinárodními	mezinárodní	k2eAgMnPc7d1	mezinárodní
experty	expert	k1gMnPc7	expert
na	na	k7c4	na
155	[number]	k4	155
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
75	[number]	k4	75
<g/>
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
z	z	k7c2	z
Turfanu	Turfan	k1gInSc2	Turfan
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
10	[number]	k4	10
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
prodloužení	prodloužení	k1gNnSc4	prodloužení
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
rozinek	rozinka	k1gFnPc2	rozinka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
jak	jak	k6eAd1	jak
kvalitu	kvalita	k1gFnSc4	kvalita
rozinek	rozinka	k1gFnPc2	rozinka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
alergiky	alergik	k1gMnPc4	alergik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zvířata	zvíře	k1gNnPc4	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
Rozinky	rozinka	k1gFnPc1	rozinka
jsou	být	k5eAaImIp3nP	být
toxické	toxický	k2eAgFnPc1d1	toxická
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
účinky	účinek	k1gInPc1	účinek
se	se	k3xPyFc4	se
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
i	i	k9	i
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
akutní	akutní	k2eAgNnSc4d1	akutní
renální	renální	k2eAgNnSc4d1	renální
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Toxin	toxin	k1gInSc1	toxin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tyto	tento	k3xDgFnPc4	tento
obtíže	obtíž	k1gFnPc4	obtíž
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
doposud	doposud	k6eAd1	doposud
zjištěn	zjistit	k5eAaPmNgInS	zjistit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
rodzinky	rodzinka	k1gFnSc2	rodzinka
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
dried	dried	k1gMnSc1	dried
fruit	fruit	k1gMnSc1	fruit
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
chunche	chunche	k1gFnSc6	chunche
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
cibéba	cibéba	k1gFnSc1	cibéba
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rozinka	rozinka	k1gFnSc1	rozinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rozinka	rozinka	k1gFnSc1	rozinka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
rozinky	rozinka	k1gFnPc4	rozinka
</s>
</p>
