<s>
Všechny	všechen	k3xTgFnPc1	všechen
provozované	provozovaný	k2eAgFnPc1d1	provozovaná
linky	linka	k1gFnPc1	linka
jsou	být	k5eAaImIp3nP	být
zapojeny	zapojit	k5eAaPmNgFnP	zapojit
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jádro	jádro	k1gNnSc1	jádro
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
právě	právě	k6eAd1	právě
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
