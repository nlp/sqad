<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1895	[number]	k4	1895
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1978	[number]	k4	1978
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
české	český	k2eAgFnSc2d1	Česká
předválečné	předválečný	k2eAgFnSc2d1	předválečná
demokratické	demokratický	k2eAgFnSc2d1	demokratická
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
díla	dílo	k1gNnSc2	dílo
Budování	budování	k1gNnSc2	budování
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pojednávajícího	pojednávající	k2eAgInSc2d1	pojednávající
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
novinářskou	novinářský	k2eAgFnSc4d1	novinářská
kariéru	kariéra	k1gFnSc4	kariéra
započal	započnout	k5eAaPmAgInS	započnout
hned	hned	k6eAd1	hned
po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
příspěvky	příspěvek	k1gInPc7	příspěvek
do	do	k7c2	do
Herbenova	Herbenův	k2eAgInSc2d1	Herbenův
časopisu	časopis	k1gInSc2	časopis
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1895	[number]	k4	1895
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
čp.	čp.	k?	čp.
668-II	[number]	k4	668-II
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Příčná	příčný	k2eAgFnSc1d1	příčná
668	[number]	k4	668
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
-Nové	-Nová	k1gFnSc6	-Nová
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
Emanuela	Emanuel	k1gMnSc2	Emanuel
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
,	,	kIx,	,
úředníka	úředník	k1gMnSc2	úředník
klubu	klub	k1gInSc2	klub
cukrovarníků	cukrovarník	k1gMnPc2	cukrovarník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Leontiny	Leontina	k1gFnSc2	Leontina
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Kopfové	Kopf	k1gMnPc1	Kopf
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Obory	obora	k1gFnSc2	obora
na	na	k7c6	na
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1894	[number]	k4	1894
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
ve	v	k7c6	v
Spálené	spálený	k2eAgFnSc6d1	spálená
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc1	jejich
první	první	k4xOgMnSc1	první
potomek	potomek	k1gMnSc1	potomek
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
ještě	ještě	k9	ještě
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
??	??	k?	??
<g/>
)	)	kIx)	)
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Ludmila	Ludmila	k1gFnSc1	Ludmila
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
??	??	k?	??
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
časopisu	časopis	k1gInSc2	časopis
Tribuna	tribuna	k1gFnSc1	tribuna
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
jako	jako	k8xC	jako
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
revue	revue	k1gFnSc2	revue
Přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
T.G.	T.G.	k1gMnSc7	T.G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
též	též	k9	též
pod	pod	k7c7	pod
různými	různý	k2eAgFnPc7d1	různá
šiframi	šifra	k1gFnPc7	šifra
často	často	k6eAd1	často
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
přispíval	přispívat	k5eAaImAgInS	přispívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924-1939	[number]	k4	1924-1939
byl	být	k5eAaImAgMnS	být
Peroutka	Peroutka	k1gMnSc1	Peroutka
také	také	k6eAd1	také
politickým	politický	k2eAgMnSc7d1	politický
komentátorem	komentátor	k1gMnSc7	komentátor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
kruhu	kruh	k1gInSc2	kruh
tzv.	tzv.	kA	tzv.
Pátečníků	pátečník	k1gMnPc2	pátečník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
v	v	k7c6	v
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
vile	vila	k1gFnSc6	vila
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
lánského	lánský	k2eAgInSc2d1	lánský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
novinář	novinář	k1gMnSc1	novinář
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
politických	politický	k2eAgInPc6d1	politický
komentářích	komentář	k1gInPc6	komentář
věnoval	věnovat	k5eAaImAgMnS	věnovat
širokému	široký	k2eAgNnSc3d1	široké
rozsahu	rozsah	k1gInSc6	rozsah
témat	téma	k1gNnPc2	téma
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
výrazným	výrazný	k2eAgMnPc3d1	výrazný
kritikům	kritik	k1gMnPc3	kritik
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
ideologie	ideologie	k1gFnPc4	ideologie
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
zhoubné	zhoubný	k2eAgInPc4d1	zhoubný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
během	během	k7c2	během
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
často	často	k6eAd1	často
napadán	napadán	k2eAgMnSc1d1	napadán
jak	jak	k8xC	jak
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
krajně	krajně	k6eAd1	krajně
pravicových	pravicový	k2eAgInPc2d1	pravicový
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc4	Hitler
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
lži	lež	k1gFnPc4	lež
a	a	k8xC	a
falešné	falešný	k2eAgInPc4d1	falešný
sliby	slib	k1gInPc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
z	z	k7c2	z
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
okupačních	okupační	k2eAgNnPc2d1	okupační
míst	místo	k1gNnPc2	místo
vyvíjen	vyvíjen	k2eAgInSc1d1	vyvíjen
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
protektorátních	protektorátní	k2eAgFnPc2d1	protektorátní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
jen	jen	k9	jen
zčásti	zčásti	k6eAd1	zčásti
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1939	[number]	k4	1939
pod	pod	k7c7	pod
Peroutkovým	Peroutkův	k2eAgNnSc7d1	Peroutkovo
jménem	jméno	k1gNnSc7	jméno
sice	sice	k8xC	sice
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c6	v
Přítomnosti	přítomnost	k1gFnSc6	přítomnost
článek	článek	k1gInSc4	článek
Dynamický	dynamický	k2eAgInSc4d1	dynamický
život	život	k1gInSc4	život
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autor	autor	k1gMnSc1	autor
uznal	uznat	k5eAaPmAgMnS	uznat
jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
razantně	razantně	k6eAd1	razantně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Češi	Čech	k1gMnPc1	Čech
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Peroutkova	Peroutkův	k2eAgNnSc2d1	Peroutkovo
vyjádření	vyjádření	k1gNnSc2	vyjádření
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
jeho	jeho	k3xOp3gNnSc2	jeho
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
uvěznění	uvěznění	k1gNnSc2	uvěznění
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Peroutkovu	Peroutkův	k2eAgInSc3d1	Peroutkův
prvnímu	první	k4xOgNnSc3	první
zatčení	zatčení	k1gNnSc3	zatčení
došlo	dojít	k5eAaPmAgNnS	dojít
už	už	k9	už
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
antifašisté	antifašista	k1gMnPc1	antifašista
byli	být	k5eAaImAgMnP	být
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
odtransportováni	odtransportovat	k5eAaPmNgMnP	odtransportovat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
nespadající	spadající	k2eNgMnPc1d1	nespadající
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
kategorií	kategorie	k1gFnPc2	kategorie
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
i	i	k9	i
Peroutkův	Peroutkův	k2eAgInSc1d1	Peroutkův
případ	případ	k1gInSc1	případ
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
však	však	k9	však
začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
1939	[number]	k4	1939
zatčen	zatknout	k5eAaPmNgMnS	zatknout
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
totiž	totiž	k9	totiž
Německo	Německo	k1gNnSc1	Německo
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
eminentní	eminentní	k2eAgInSc4d1	eminentní
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
policie	policie	k1gFnSc1	policie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
všechny	všechen	k3xTgMnPc4	všechen
potenciální	potenciální	k2eAgMnPc4d1	potenciální
protivníky	protivník	k1gMnPc4	protivník
preventivně	preventivně	k6eAd1	preventivně
pozatýkat	pozatýkat	k5eAaPmF	pozatýkat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
důvodům	důvod	k1gInPc3	důvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
příčinou	příčina	k1gFnSc7	příčina
tohoto	tento	k3xDgInSc2	tento
druhého	druhý	k4xOgNnSc2	druhý
Peroutkova	Peroutkův	k2eAgNnSc2d1	Peroutkovo
zatčení	zatčení	k1gNnSc2	zatčení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
on	on	k3xPp3gInSc1	on
sám	sám	k3xTgInSc1	sám
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
týž	týž	k3xTgInSc4	týž
článek	článek	k1gInSc4	článek
[	[	kIx(	[
<g/>
Dynamický	dynamický	k2eAgInSc4d1	dynamický
život	život	k1gInSc4	život
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
připravit	připravit	k5eAaPmF	připravit
svému	svůj	k3xOyFgMnSc3	svůj
autoru	autor	k1gMnSc3	autor
dvojí	dvojit	k5eAaImIp3nS	dvojit
osud	osud	k1gInSc1	osud
<g/>
:	:	kIx,	:
nejdříve	dříve	k6eAd3	dříve
[	[	kIx(	[
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g />
.	.	kIx.	.
</s>
<s>
1939	[number]	k4	1939
<g/>
]	]	kIx)	]
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
přečte	přečíst	k5eAaPmIp3nS	přečíst
komisař	komisař	k1gMnSc1	komisař
gestapa	gestapo	k1gNnSc2	gestapo
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
autor	autor	k1gMnSc1	autor
musí	muset	k5eAaImIp3nS	muset
zůstat	zůstat	k5eAaPmF	zůstat
doživotně	doživotně	k6eAd1	doživotně
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
[	[	kIx(	[
<g/>
tj.	tj.	kA	tj.
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
1945	[number]	k4	1945
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
cituje	citovat	k5eAaBmIp3nS	citovat
jiný	jiný	k2eAgMnSc1d1	jiný
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
zalichotit	zalichotit	k5eAaPmF	zalichotit
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
tak	tak	k6eAd1	tak
lhát	lhát	k5eAaImF	lhát
a	a	k8xC	a
nestydět	stydět	k5eNaImF	stydět
se	se	k3xPyFc4	se
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Peroutka	Peroutka	k1gMnSc1	Peroutka
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
vězněn	věznit	k5eAaImNgMnS	věznit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
,	,	kIx,	,
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pak	pak	k6eAd1	pak
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Buchenwald	Buchenwald	k1gInSc1	Buchenwald
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
dočkal	dočkat	k5eAaPmAgInS	dočkat
osvobození	osvobození	k1gNnSc4	osvobození
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
dočasně	dočasně	k6eAd1	dočasně
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
věznici	věznice	k1gFnSc6	věznice
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Peroutka	Peroutka	k1gMnSc1	Peroutka
stal	stát	k5eAaPmAgMnS	stát
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byly	být	k5eAaImAgFnP	být
noviny	novina	k1gFnPc4	novina
založené	založený	k2eAgFnPc4d1	založená
namísto	namísto	k7c2	namísto
předválečných	předválečný	k2eAgFnPc2d1	předválečná
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
redigoval	redigovat	k5eAaImAgInS	redigovat
i	i	k9	i
týdeník	týdeník	k1gInSc1	týdeník
Dnešek	dnešek	k1gInSc1	dnešek
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zase	zase	k9	zase
byl	být	k5eAaImAgInS	být
obdobou	obdoba	k1gFnSc7	obdoba
předválečné	předválečný	k2eAgFnSc2d1	předválečná
Přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
předválečné	předválečný	k2eAgInPc1d1	předválečný
názvy	název	k1gInPc1	název
periodik	periodikum	k1gNnPc2	periodikum
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
totiž	totiž	k9	totiž
vycházely	vycházet	k5eAaImAgInP	vycházet
i	i	k9	i
za	za	k7c2	za
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
prostředí	prostředí	k1gNnSc6	prostředí
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
diskvalifikovalo	diskvalifikovat	k5eAaBmAgNnS	diskvalifikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
i	i	k8xC	i
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
Peroutka	Peroutka	k1gMnSc1	Peroutka
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistů	komunista	k1gMnPc2	komunista
opakovaně	opakovaně	k6eAd1	opakovaně
nařknut	nařknout	k5eAaPmNgMnS	nařknout
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nacistům	nacista	k1gMnPc3	nacista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
článek	článek	k1gInSc4	článek
Dynamický	dynamický	k2eAgInSc4d1	dynamický
život	život	k1gInSc4	život
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
na	na	k7c4	na
text	text	k1gInSc4	text
Hitler	Hitler	k1gMnSc1	Hitler
po	po	k7c6	po
lidské	lidský	k2eAgFnSc6d1	lidská
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
Peroutkovi	Peroutka	k1gMnSc3	Peroutka
přisoudili	přisoudit	k5eAaPmAgMnP	přisoudit
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
(	(	kIx(	(
<g/>
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
poněkud	poněkud	k6eAd1	poněkud
smířlivému	smířlivý	k2eAgInSc3d1	smířlivý
pohledu	pohled	k1gInSc3	pohled
autora	autor	k1gMnSc2	autor
na	na	k7c4	na
předválečné	předválečný	k2eAgNnSc4d1	předválečné
Německo	Německo	k1gNnSc4	Německo
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
snaží	snažit	k5eAaImIp3nP	snažit
interpretovat	interpretovat	k5eAaBmF	interpretovat
Peroutkovy	Peroutkův	k2eAgFnPc4d1	Peroutkova
citace	citace	k1gFnPc4	citace
z	z	k7c2	z
předválečných	předválečný	k2eAgInPc2d1	předválečný
článků	článek	k1gInPc2	článek
jako	jako	k8xC	jako
proněmecké	proněmecký	k2eAgFnSc2d1	proněmecká
–	–	k?	–
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Peroutka	peroutka	k1gFnSc1	peroutka
však	však	k9	však
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
tyto	tento	k3xDgInPc4	tento
útoky	útok	k1gInPc4	útok
opakovaně	opakovaně	k6eAd1	opakovaně
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
a	a	k8xC	a
proněmecký	proněmecký	k2eAgInSc1d1	proněmecký
výklad	výklad	k1gInSc1	výklad
svých	svůj	k3xOyFgInPc2	svůj
článků	článek	k1gInPc2	článek
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
Peroutka	Peroutka	k1gMnSc1	Peroutka
rovněž	rovněž	k6eAd1	rovněž
poslancem	poslanec	k1gMnSc7	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
národně	národně	k6eAd1	národně
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
zasedal	zasedat	k5eAaImAgInS	zasedat
do	do	k7c2	do
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
do	do	k7c2	do
r.	r.	kA	r.
1961	[number]	k4	1961
vedl	vést	k5eAaImAgInS	vést
české	český	k2eAgNnSc4d1	české
oddělení	oddělení	k1gNnSc4	oddělení
rádia	rádius	k1gInSc2	rádius
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
sepsal	sepsat	k5eAaPmAgMnS	sepsat
ustavující	ustavující	k2eAgFnSc3d1	ustavující
deklaraci	deklarace	k1gFnSc3	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Peroutka	Peroutka	k1gMnSc1	Peroutka
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
představitelů	představitel	k1gMnPc2	představitel
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
totality	totalita	k1gFnSc2	totalita
v	v	k7c4	v
železnou	železný	k2eAgFnSc4d1	železná
oponou	opona	k1gFnSc7	opona
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
Československu	Československo	k1gNnSc6	Československo
méně	málo	k6eAd2	málo
známý	známý	k2eAgMnSc1d1	známý
než	než	k8xS	než
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
jeho	jeho	k3xOp3gInPc2	jeho
politických	politický	k2eAgInPc2d1	politický
rozborů	rozbor	k1gInPc2	rozbor
a	a	k8xC	a
komentářů	komentář	k1gInPc2	komentář
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
pronásledováno	pronásledován	k2eAgNnSc1d1	pronásledováno
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
o	o	k7c4	o
Karlu	Karla	k1gFnSc4	Karla
Čapkovi	Čapek	k1gMnSc3	Čapek
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
Pátečnících	pátečník	k1gInPc6	pátečník
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
a	a	k8xC	a
hovořilo	hovořit	k5eAaImAgNnS	hovořit
alespoň	alespoň	k9	alespoň
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
Peroutka	Peroutka	k1gMnSc1	Peroutka
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
neexistoval	existovat	k5eNaImAgMnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
během	během	k7c2	během
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
,	,	kIx,	,
Peroutkovo	Peroutkův	k2eAgNnSc4d1	Peroutkovo
jméno	jméno	k1gNnSc4	jméno
prakticky	prakticky	k6eAd1	prakticky
neměla	mít	k5eNaImAgFnS	mít
možnost	možnost	k1gFnSc4	možnost
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
postava	postava	k1gFnSc1	postava
Peroutky	peroutka	k1gFnSc2	peroutka
nesměla	smět	k5eNaImAgFnS	smět
objevit	objevit	k5eAaPmF	objevit
ani	ani	k8xC	ani
v	v	k7c6	v
Čapkově	Čapkův	k2eAgInSc6d1	Čapkův
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
zkáze	zkáza	k1gFnSc3	zkáza
z	z	k7c2	z
období	období	k1gNnSc2	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
politických	politický	k2eAgInPc2d1	politický
poměrů	poměr	k1gInPc2	poměr
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
pohřbeny	pohřben	k2eAgInPc4d1	pohřben
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Josef	Josef	k1gMnSc1	Josef
Protiva	protiva	k1gFnSc1	protiva
natočil	natočit	k5eAaBmAgMnS	natočit
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
Muž	muž	k1gMnSc1	muž
Přítomnosti	přítomnost	k1gFnSc2	přítomnost
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
mu	on	k3xPp3gMnSc3	on
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
udělil	udělit	k5eAaPmAgMnS	udělit
Řád	řád	k1gInSc4	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garrigu	k1gInSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
ukončení	ukončení	k1gNnSc2	ukončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
natočena	natočen	k2eAgFnSc1d1	natočena
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Valčík	valčík	k1gInSc4	valčík
na	na	k7c4	na
uvítanou	uvítaná	k1gFnSc4	uvítaná
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Peroutkova	Peroutkův	k2eAgInSc2d1	Peroutkův
románu	román	k1gInSc2	román
Oblak	oblak	k1gInSc1	oblak
a	a	k8xC	a
valčík	valčík	k1gInSc4	valčík
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
režíroval	režírovat	k5eAaImAgMnS	režírovat
Antonín	Antonín	k1gMnSc1	Antonín
Přidal	přidat	k5eAaPmAgInS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
známku	známka	k1gFnSc4	známka
s	s	k7c7	s
Peroutkovým	Peroutkův	k2eAgInSc7d1	Peroutkův
portrétem	portrét	k1gInSc7	portrét
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Cena	cena	k1gFnSc1	cena
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	peroutka	k1gFnSc2	peroutka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
ocenění	ocenění	k1gNnPc2	ocenění
pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
novináře	novinář	k1gMnPc4	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
Peroutkova	Peroutkův	k2eAgFnSc1d1	Peroutkova
třetí	třetí	k4xOgFnSc1	třetí
žena	žena	k1gFnSc1	žena
Slávka	Slávka	k1gFnSc1	Slávka
Peroutková	Peroutková	k1gFnSc1	Peroutková
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
knihu	kniha	k1gFnSc4	kniha
Muž	muž	k1gMnSc1	muž
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c6	v
Nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Deníky	deník	k1gInPc4	deník
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
napsal	napsat	k5eAaPmAgMnS	napsat
Jiří	Jiří	k1gMnSc1	Jiří
Kovtun	Kovtun	k1gMnSc1	Kovtun
a	a	k8xC	a
doslov	doslov	k1gInSc1	doslov
Pavel	Pavel	k1gMnSc1	Pavel
Tigrid	Tigrid	k1gMnSc1	Tigrid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
pak	pak	k6eAd1	pak
Peroutková	Peroutková	k1gFnSc1	Peroutková
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Dokořán	dokořán	k6eAd1	dokořán
svoje	svůj	k3xOyFgFnPc4	svůj
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
manžela	manžel	k1gMnSc4	manžel
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
-	-	kIx~	-
Třiatřicet	třiatřicet	k4xCc1	třiatřicet
let	léto	k1gNnPc2	léto
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Peroutkou	Peroutka	k1gMnSc7	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kauza	kauza	k1gFnSc1	kauza
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
Peroutka	Peroutka	k1gMnSc1	Peroutka
byl	být	k5eAaImAgMnS	být
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
nacismem	nacismus	k1gInSc7	nacismus
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgInS	napsat
i	i	k9	i
oslavný	oslavný	k2eAgInSc1d1	oslavný
článek	článek	k1gInSc1	článek
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
vyřčený	vyřčený	k2eAgInSc1d1	vyřčený
při	při	k7c6	při
proslovu	proslov	k1gInSc6	proslov
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
osvobození	osvobození	k1gNnSc2	osvobození
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
vášnivou	vášnivý	k2eAgFnSc4d1	vášnivá
debatu	debata	k1gFnSc4	debata
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
blogerů	bloger	k1gMnPc2	bloger
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
další	další	k2eAgFnPc4d1	další
citace	citace	k1gFnPc4	citace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zazněly	zaznět	k5eAaImAgFnP	zaznět
při	při	k7c6	při
proslovu	proslov	k1gInSc6	proslov
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
smyšlené	smyšlený	k2eAgInPc1d1	smyšlený
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
převzaté	převzatý	k2eAgInPc1d1	převzatý
z	z	k7c2	z
tendenčních	tendenční	k2eAgFnPc2d1	tendenční
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
neověřené	ověřený	k2eNgFnPc1d1	neověřená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gMnSc1	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
přesto	přesto	k8xC	přesto
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
cokoli	cokoli	k3yInSc1	cokoli
z	z	k7c2	z
proslovu	proslov	k1gInSc2	proslov
dementovat	dementovat	k5eAaBmF	dementovat
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
Peroutkův	Peroutkův	k2eAgInSc1d1	Peroutkův
údajný	údajný	k2eAgInSc1d1	údajný
článek	článek	k1gInSc1	článek
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
dohledá	dohledat	k5eAaPmIp3nS	dohledat
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
abcHistory	abcHistor	k1gInPc1	abcHistor
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
souvislosti	souvislost	k1gFnSc6	souvislost
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
znám	znám	k2eAgInSc1d1	znám
Neville	Neville	k1gInSc1	Neville
Chamberlain	Chamberlain	k2eAgInSc1d1	Chamberlain
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
duchu	duch	k1gMnSc3	duch
Peroutkových	Peroutkových	k2eAgFnSc2d1	Peroutkových
aktivit	aktivita	k1gFnPc2	aktivita
takové	takový	k3xDgInPc4	takový
vyjádření	vyjádření	k1gNnSc1	vyjádření
charakterově	charakterově	k6eAd1	charakterově
odporuje	odporovat	k5eAaImIp3nS	odporovat
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
také	také	k9	také
naznačil	naznačit	k5eAaPmAgInS	naznačit
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledaným	hledaný	k2eAgInSc7d1	hledaný
textem	text	k1gInSc7	text
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
by	by	kYmCp3nS	by
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
už	už	k6eAd1	už
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
kritizovaný	kritizovaný	k2eAgInSc1d1	kritizovaný
článek	článek	k1gInSc1	článek
Hitler	Hitler	k1gMnSc1	Hitler
po	po	k7c6	po
lidské	lidský	k2eAgFnSc6d1	lidská
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
musel	muset	k5eAaImAgMnS	muset
Peroutka	Peroutka	k1gMnSc1	Peroutka
absolvovat	absolvovat	k5eAaPmF	absolvovat
už	už	k9	už
v	v	k7c6	v
poválečných	poválečný	k2eAgInPc6d1	poválečný
letech	let	k1gInPc6	let
a	a	k8xC	a
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
i	i	k8xC	i
jasně	jasně	k6eAd1	jasně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
Peroutkova	Peroutkův	k2eAgFnSc1d1	Peroutkova
vnučka	vnučka	k1gFnSc1	vnučka
Terezie	Terezie	k1gFnSc1	Terezie
Kaslová	Kaslová	k1gFnSc1	Kaslová
podala	podat	k5eAaPmAgFnS	podat
za	za	k7c4	za
výroky	výrok	k1gInPc4	výrok
prezidenta	prezident	k1gMnSc2	prezident
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
jejího	její	k3xOp3gMnSc2	její
dědečka	dědeček	k1gMnSc2	dědeček
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnPc2	tvrzení
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
jeho	jeho	k3xOp3gMnSc1	jeho
mluvčí	mluvčí	k1gMnSc1	mluvčí
objevil	objevit	k5eAaPmAgMnS	objevit
při	při	k7c6	při
prohledávání	prohledávání	k1gNnSc6	prohledávání
písemností	písemnost	k1gFnPc2	písemnost
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	peroutka	k1gFnSc2	peroutka
jiná	jiný	k2eAgNnPc4d1	jiné
vyjádření	vyjádření	k1gNnPc4	vyjádření
<g/>
,	,	kIx,	,
než	než	k8xS	než
prezident	prezident	k1gMnSc1	prezident
citoval	citovat	k5eAaBmAgMnS	citovat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
fóru	fórum	k1gNnSc6	fórum
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dubna	duben	k1gInSc2	duben
začal	začít	k5eAaPmAgInS	začít
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
omluvy	omluva	k1gFnSc2	omluva
za	za	k7c2	za
nařčení	nařčení	k1gNnSc2	nařčení
či	či	k8xC	či
původního	původní	k2eAgInSc2d1	původní
článku	článek	k1gInSc2	článek
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
některé	některý	k3yIgInPc4	některý
Peroutkovy	Peroutkův	k2eAgInPc4d1	Peroutkův
články	článek	k1gInPc4	článek
z	z	k7c2	z
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
uveřejněny	uveřejněn	k2eAgInPc1d1	uveřejněn
články	článek	k1gInPc1	článek
"	"	kIx"	"
<g/>
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
židé	žid	k1gMnPc1	žid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Povinnost	povinnost	k1gFnSc1	povinnost
je	být	k5eAaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dynamický	dynamický	k2eAgInSc4d1	dynamický
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
poměr	poměr	k1gInSc1	poměr
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ani	ani	k8xC	ani
-	-	kIx~	-
Ani	ani	k9	ani
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
předcházené	předcházený	k2eAgNnSc1d1	předcházené
heslovitým	heslovitý	k2eAgInSc7d1	heslovitý
komentářem	komentář	k1gInSc7	komentář
a	a	k8xC	a
vybranými	vybraný	k2eAgInPc7d1	vybraný
citáty	citát	k1gInPc7	citát
<g/>
.	.	kIx.	.
</s>
<s>
Znalci	znalec	k1gMnPc1	znalec
Peroutkova	Peroutkův	k2eAgInSc2d1	Peroutkův
života	život	k1gInSc2	život
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
zpochybnili	zpochybnit	k5eAaPmAgMnP	zpochybnit
schopnost	schopnost	k1gFnSc4	schopnost
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
mluvčího	mluvčí	k1gMnSc2	mluvčí
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
vést	vést	k5eAaImF	vést
fundovanou	fundovaný	k2eAgFnSc4d1	fundovaná
diskusi	diskuse	k1gFnSc4	diskuse
o	o	k7c6	o
Ferdinandu	Ferdinand	k1gMnSc6	Ferdinand
Peroutkovi	Peroutka	k1gMnSc6	Peroutka
a	a	k8xC	a
daném	daný	k2eAgNnSc6d1	dané
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
Ovčáčkovu	ovčáčkův	k2eAgFnSc4d1	Ovčáčkova
schopnost	schopnost	k1gFnSc4	schopnost
pochopit	pochopit	k5eAaPmF	pochopit
obsah	obsah	k1gInSc4	obsah
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
zdůraznili	zdůraznit	k5eAaPmAgMnP	zdůraznit
tendenčnost	tendenčnost	k1gFnSc4	tendenčnost
jimi	on	k3xPp3gMnPc7	on
vybíraných	vybíraný	k2eAgInPc2d1	vybíraný
citací	citace	k1gFnSc7	citace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
upozornili	upozornit	k5eAaPmAgMnP	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
dokládat	dokládat	k5eAaImF	dokládat
prezidentovu	prezidentův	k2eAgFnSc4d1	prezidentova
původní	původní	k2eAgFnSc4d1	původní
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
Dynamický	dynamický	k2eAgInSc4d1	dynamický
život	život	k1gInSc4	život
[	[	kIx(	[
<g/>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
Peroutkova	Peroutkův	k2eAgNnSc2d1	Peroutkovo
uvěznění	uvěznění	k1gNnSc2	uvěznění
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dobovou	dobový	k2eAgFnSc4d1	dobová
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
psané	psaný	k2eAgInPc4d1	psaný
s	s	k7c7	s
odvahou	odvaha	k1gFnSc7	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Ovčáček	ovčáček	k1gMnSc1	ovčáček
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Peroutka	Peroutka	k1gMnSc1	Peroutka
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
článku	článek	k1gInSc3	článek
Dynamický	dynamický	k2eAgInSc1d1	dynamický
život	život	k1gInSc1	život
gestapem	gestapo	k1gNnSc7	gestapo
perzekvován	perzekvovat	k5eAaImNgInS	perzekvovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odůvodňuje	odůvodňovat	k5eAaImIp3nS	odůvodňovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
zatčení	zatčení	k1gNnSc3	zatčení
nedošlo	dojít	k5eNaPmAgNnS	dojít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
publikaci	publikace	k1gFnSc6	publikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
interpretaci	interpretace	k1gFnSc6	interpretace
článků	článek	k1gInPc2	článek
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
vlně	vlna	k1gFnSc3	vlna
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
mnoha	mnoho	k4c2	mnoho
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
komentátorů	komentátor	k1gMnPc2	komentátor
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
Zemanova	Zemanův	k2eAgNnSc2d1	Zemanovo
a	a	k8xC	a
Ovčáčkova	ovčáčkův	k2eAgNnSc2d1	ovčáčkův
úsilí	úsilí	k1gNnSc2	úsilí
pokus	pokus	k1gInSc1	pokus
odvést	odvést	k5eAaPmF	odvést
pozornost	pozornost	k1gFnSc4	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
médií	médium	k1gNnPc2	médium
od	od	k7c2	od
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
neochoty	neochota	k1gFnSc2	neochota
omluvit	omluvit	k5eAaPmF	omluvit
se	se	k3xPyFc4	se
za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
původní	původní	k2eAgInSc4d1	původní
nedoložený	doložený	k2eNgInSc4d1	nedoložený
výrok	výrok	k1gInSc4	výrok
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
kontroverzních	kontroverzní	k2eAgNnPc2d1	kontroverzní
témat	téma	k1gNnPc2	téma
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
prezidenta	prezident	k1gMnSc2	prezident
či	či	k8xC	či
snaha	snaha	k1gFnSc1	snaha
"	"	kIx"	"
<g/>
vyřídit	vyřídit	k5eAaPmF	vyřídit
si	se	k3xPyFc3	se
účty	účet	k1gInPc4	účet
<g/>
"	"	kIx"	"
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
českou	český	k2eAgFnSc7d1	Česká
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
obcí	obec	k1gFnSc7	obec
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
nechápou	chápat	k5eNaImIp3nP	chápat
jaký	jaký	k3yQgInSc4	jaký
smysl	smysl	k1gInSc4	smysl
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
mediální	mediální	k2eAgFnSc1d1	mediální
kampaň	kampaň	k1gFnSc1	kampaň
vůbec	vůbec	k9	vůbec
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
online	onlin	k1gInSc5	onlin
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Blesk	blesk	k1gInSc4	blesk
TV	TV	kA	TV
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
mluvčí	mluvčí	k1gMnSc1	mluvčí
Hradu	hrad	k1gInSc2	hrad
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
nenajde	najít	k5eNaPmIp3nS	najít
do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypíše	vypsat	k5eAaPmIp3nS	vypsat
Zeman	Zeman	k1gMnSc1	Zeman
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
prostředků	prostředek	k1gInPc2	prostředek
odměnu	odměna	k1gFnSc4	odměna
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ho	on	k3xPp3gMnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
také	také	k9	také
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
oslavný	oslavný	k2eAgInSc1d1	oslavný
článek	článek	k1gInSc1	článek
na	na	k7c4	na
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
za	za	k7c2	za
války	válka	k1gFnSc2	válka
napsal	napsat	k5eAaBmAgMnS	napsat
i	i	k9	i
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
nebyl	být	k5eNaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
vypsal	vypsat	k5eAaPmAgMnS	vypsat
výše	vysoce	k6eAd2	vysoce
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
historik	historik	k1gMnSc1	historik
Jan	Jan	k1gMnSc1	Jan
Galandauer	Galandauer	k1gMnSc1	Galandauer
našel	najít	k5eAaPmAgMnS	najít
článek	článek	k1gInSc4	článek
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
-	-	kIx~	-
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
nár	nár	k?	nár
<g/>
.	.	kIx.	.
soc	soc	kA	soc
<g/>
.	.	kIx.	.
-	-	kIx~	-
piráti	pirát	k1gMnPc1	pirát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
částečně	částečně	k6eAd1	částečně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Zemanovu	Zemanův	k2eAgInSc3d1	Zemanův
popisu	popis	k1gInSc3	popis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
autorován	autorován	k2eAgMnSc1d1	autorován
Peroutkou	Peroutka	k1gMnSc7	Peroutka
a	a	k8xC	a
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
právu	právo	k1gNnSc6	právo
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
nepodepsaný	podepsaný	k2eNgInSc4d1	nepodepsaný
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
Venkov	venkov	k1gInSc1	venkov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Vraný	vraný	k2eAgMnSc1d1	vraný
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jím	jíst	k5eAaImIp1nS	jíst
popsaný	popsaný	k2eAgInSc1d1	popsaný
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ovčáček	ovčáček	k1gMnSc1	ovčáček
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
povaze	povaha	k1gFnSc6	povaha
Jací	jaký	k3yIgMnPc1	jaký
jsme	být	k5eAaImIp1nP	být
a	a	k8xC	a
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
díla	dílo	k1gNnSc2	dílo
o	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
Československa	Československo	k1gNnSc2	Československo
Budování	budování	k1gNnSc2	budování
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
a	a	k8xC	a
ne	ne	k9	ne
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
Budování	budování	k1gNnSc2	budování
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
historií	historie	k1gFnSc7	historie
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
stěžejní	stěžejní	k2eAgNnSc4d1	stěžejní
dílo	dílo	k1gNnSc4	dílo
tvorby	tvorba	k1gFnSc2	tvorba
F.	F.	kA	F.
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Demokratický	demokratický	k2eAgInSc1d1	demokratický
manifest	manifest	k1gInSc1	manifest
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
chaos	chaos	k1gInSc1	chaos
a	a	k8xC	a
zlozvyky	zlozvyk	k1gInPc1	zlozvyk
Byl	být	k5eAaImAgMnS	být
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
vinen	vinen	k2eAgMnSc1d1	vinen
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
Z	z	k7c2	z
deníku	deník	k1gInSc2	deník
žurnalistova	žurnalistův	k2eAgFnSc1d1	žurnalistův
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
fejetony	fejeton	k1gInPc7	fejeton
Šťastlivec	šťastlivec	k1gMnSc1	šťastlivec
Sulla	Sulla	k1gMnSc1	Sulla
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Oblak	oblak	k1gInSc1	oblak
a	a	k8xC	a
valčík	valčík	k1gInSc1	valčík
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
;	;	kIx,	;
románová	románový	k2eAgFnSc1d1	románová
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishers	k1gInSc1	Publishers
<g/>
)	)	kIx)	)
Pozdější	pozdní	k2eAgInSc1d2	pozdější
život	život	k1gInSc1	život
Panny	Panna	k1gFnSc2	Panna
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
spekulativní	spekulativní	k2eAgInSc4d1	spekulativní
příběh	příběh	k1gInSc4	příběh
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgFnSc1d1	pojednávající
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dožila	dožít	k5eAaPmAgFnS	dožít
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
</s>
