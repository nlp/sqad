<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
kombinací	kombinace	k1gFnSc7	kombinace
dvou	dva	k4xCgFnPc2	dva
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nepřiléhajících	přiléhající	k2eNgFnPc2d1	nepřiléhající
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
z	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
optického	optický	k2eAgInSc2d1	optický
materiálu	materiál	k1gInSc2	materiál
lze	lze	k6eAd1	lze
sestavit	sestavit	k5eAaPmF	sestavit
takovou	takový	k3xDgFnSc4	takový
optickou	optický	k2eAgFnSc4d1	optická
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
zvolené	zvolený	k2eAgFnPc4d1	zvolená
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
celková	celkový	k2eAgFnSc1d1	celková
efektivní	efektivní	k2eAgFnSc1d1	efektivní
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
;	;	kIx,	;
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
achromatické	achromatický	k2eAgFnSc6d1	achromatická
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
objektivu	objektiv	k1gInSc6	objektiv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
