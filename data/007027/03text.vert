<s>
Chromatická	chromatický	k2eAgFnSc1d1	chromatická
aberace	aberace	k1gFnSc1	aberace
(	(	kIx(	(
<g/>
též	též	k9	též
chromatická	chromatický	k2eAgFnSc1d1	chromatická
vada	vada	k1gFnSc1	vada
nebo	nebo	k8xC	nebo
barevná	barevný	k2eAgFnSc1d1	barevná
vada	vada	k1gFnSc1	vada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
vada	vada	k1gFnSc1	vada
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
i	i	k9	i
složitější	složitý	k2eAgFnSc2d2	složitější
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
čoček	čočka	k1gFnPc2	čočka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
objektivu	objektiv	k1gInSc2	objektiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
závislostí	závislost	k1gFnSc7	závislost
ohniskové	ohniskový	k2eAgFnSc3d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
čoček	čočka	k1gFnPc2	čočka
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
podstatou	podstata	k1gFnSc7	podstata
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
závislosti	závislost	k1gFnSc3	závislost
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
průhledných	průhledný	k2eAgFnPc2d1	průhledná
látek	látka	k1gFnPc2	látka
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc1	čočka
pak	pak	k6eAd1	pak
lámou	lámat	k5eAaImIp3nP	lámat
světlo	světlo	k1gNnSc4	světlo
každé	každý	k3xTgFnSc2	každý
barvy	barva	k1gFnSc2	barva
jinak	jinak	k6eAd1	jinak
(	(	kIx(	(
<g/>
záření	záření	k1gNnSc1	záření
dlouhovlnné	dlouhovlnný	k2eAgNnSc1d1	dlouhovlnné
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
<g/>
,	,	kIx,	,
krátkovlnné	krátkovlnný	k2eAgFnPc1d1	krátkovlnná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
fialové	fialový	k2eAgInPc1d1	fialový
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
projeví	projevit	k5eAaPmIp3nS	projevit
jako	jako	k9	jako
barevné	barevný	k2eAgNnSc1d1	barevné
lemování	lemování	k1gNnSc1	lemování
ostrých	ostrý	k2eAgInPc2d1	ostrý
přechodů	přechod	k1gInPc2	přechod
mezi	mezi	k7c7	mezi
světlem	světlo	k1gNnSc7	světlo
a	a	k8xC	a
stínem	stín	k1gInSc7	stín
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
kombinací	kombinace	k1gFnSc7	kombinace
dvou	dva	k4xCgFnPc2	dva
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nepřiléhajících	přiléhající	k2eNgFnPc2d1	nepřiléhající
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
z	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
optického	optický	k2eAgInSc2d1	optický
materiálu	materiál	k1gInSc2	materiál
lze	lze	k6eAd1	lze
sestavit	sestavit	k5eAaPmF	sestavit
takovou	takový	k3xDgFnSc4	takový
optickou	optický	k2eAgFnSc4d1	optická
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
zvolené	zvolený	k2eAgFnPc4d1	zvolená
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
celková	celkový	k2eAgFnSc1d1	celková
efektivní	efektivní	k2eAgFnSc1d1	efektivní
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
;	;	kIx,	;
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
achromatické	achromatický	k2eAgFnSc6d1	achromatická
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
objektivu	objektiv	k1gInSc6	objektiv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
moderní	moderní	k2eAgInPc1d1	moderní
zoomové	zoomový	k2eAgInPc1d1	zoomový
objektivy	objektiv	k1gInPc1	objektiv
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
chromatická	chromatický	k2eAgFnSc1d1	chromatická
vada	vada	k1gFnSc1	vada
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
snímku	snímek	k1gInSc2	snímek
<g/>
)	)	kIx)	)
výrazným	výrazný	k2eAgInSc7d1	výrazný
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
kompenzaci	kompenzace	k1gFnSc4	kompenzace
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
do	do	k7c2	do
objektivů	objektiv	k1gInPc2	objektiv
přidávají	přidávat	k5eAaImIp3nP	přidávat
speciální	speciální	k2eAgInPc1d1	speciální
elementy	element	k1gInPc1	element
<g/>
:	:	kIx,	:
čočky	čočka	k1gFnPc1	čočka
z	z	k7c2	z
fluoridu	fluorid	k1gInSc2	fluorid
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
–	–	k?	–
fluorid	fluorid	k1gInSc1	fluorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
fluorit	fluorit	k1gInSc1	fluorit
čili	čili	k8xC	čili
kazivec	kazivec	k1gInSc1	kazivec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc1	minerál
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkým	nízký	k2eAgInSc7d1	nízký
rozptylem	rozptyl	k1gInSc7	rozptyl
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
barevná	barevný	k2eAgFnSc1d1	barevná
vada	vada	k1gFnSc1	vada
projevuje	projevovat	k5eAaImIp3nS	projevovat
méně	málo	k6eAd2	málo
difrakční	difrakční	k2eAgInPc1d1	difrakční
elementy	element	k1gInPc1	element
–	–	k?	–
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
difrakční	difrakční	k2eAgFnSc4d1	difrakční
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
barevnou	barevný	k2eAgFnSc4d1	barevná
vadu	vada	k1gFnSc4	vada
přesně	přesně	k6eAd1	přesně
opačnou	opačný	k2eAgFnSc4d1	opačná
než	než	k8xS	než
běžné	běžný	k2eAgFnSc2d1	běžná
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gInSc1	jejich
problém	problém	k1gInSc1	problém
kompenzuje	kompenzovat	k5eAaBmIp3nS	kompenzovat
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chromatická	chromatický	k2eAgFnSc1d1	chromatická
aberace	aberace	k1gFnSc1	aberace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Chromatická	chromatický	k2eAgFnSc1d1	chromatická
vada	vada	k1gFnSc1	vada
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
fotoroman	fotoroman	k1gMnSc1	fotoroman
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
aberace	aberace	k1gFnSc1	aberace
<g/>
,	,	kIx,	,
O	o	k7c6	o
barevné	barevný	k2eAgFnSc6d1	barevná
aberaci	aberace	k1gFnSc6	aberace
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
na	na	k7c6	na
serveru	server	k1gInSc6	server
digineff	digineff	k1gInSc1	digineff
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
a	a	k8xC	a
simulace	simulace	k1gFnPc4	simulace
chromatické	chromatický	k2eAgFnSc2d1	chromatická
aberace	aberace	k1gFnSc2	aberace
Animace	animace	k1gFnSc2	animace
chromatické	chromatický	k2eAgFnSc2d1	chromatická
aberace	aberace	k1gFnSc2	aberace
http://qed.ben.cz/lupa	[url]	k1gFnSc1	http://qed.ben.cz/lupa
http://qed.ben.cz/disperze	[url]	k1gFnSc1	http://qed.ben.cz/disperze
</s>
