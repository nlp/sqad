<s>
Švabach	švabach	k1gInSc1	švabach
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Schwabacher	Schwabachra	k1gFnPc2	Schwabachra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tiskové	tiskový	k2eAgNnSc1d1	tiskové
pozdně	pozdně	k6eAd1	pozdně
bastardní	bastardní	k2eAgNnSc1d1	bastardní
gotické	gotický	k2eAgNnSc1d1	gotické
lomené	lomený	k2eAgNnSc1d1	lomené
písmo	písmo	k1gNnSc1	písmo
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
zdobností	zdobnost	k1gFnSc7	zdobnost
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc4d1	pocházející
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
záhy	záhy	k6eAd1	záhy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k9	jako
základní	základní	k2eAgNnSc1d1	základní
tiskové	tiskový	k2eAgNnSc1d1	tiskové
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
oblasti	oblast	k1gFnSc6	oblast
vytlačen	vytlačen	k2eAgMnSc1d1	vytlačen
vyšší	vysoký	k2eAgMnSc1d2	vyšší
a	a	k8xC	a
užší	úzký	k2eAgFnSc7d2	užší
frakturou	fraktura	k1gFnSc7	fraktura
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
široké	široký	k2eAgNnSc1d1	široké
zaoblené	zaoblený	k2eAgNnSc1d1	zaoblené
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
pouze	pouze	k6eAd1	pouze
obtížně	obtížně	k6eAd1	obtížně
čitelné	čitelný	k2eAgFnPc1d1	čitelná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
kvůli	kvůli	k7c3	kvůli
jasné	jasný	k2eAgFnSc3d1	jasná
a	a	k8xC	a
čitelné	čitelný	k2eAgFnSc3d1	čitelná
formě	forma	k1gFnSc3	forma
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
dřívějším	dřívější	k2eAgNnPc3d1	dřívější
gotickým	gotický	k2eAgNnPc3d1	gotické
písmům	písmo	k1gNnPc3	písmo
ubylo	ubýt	k5eAaPmAgNnS	ubýt
např.	např.	kA	např.
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zkratek	zkratka	k1gFnPc2	zkratka
a	a	k8xC	a
ligatur	ligatura	k1gFnPc2	ligatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
české	český	k2eAgMnPc4d1	český
či	či	k8xC	či
německé	německý	k2eAgFnSc6d1	německá
písemnosti	písemnost	k1gFnSc6	písemnost
přechodovým	přechodový	k2eAgNnSc7d1	přechodové
písmem	písmo	k1gNnSc7	písmo
mezi	mezi	k7c7	mezi
gotickou	gotický	k2eAgFnSc7d1	gotická
a	a	k8xC	a
novogotickou	novogotický	k2eAgFnSc7d1	novogotická
etapou	etapa	k1gFnSc7	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Schwabach	Schwabacha	k1gFnPc2	Schwabacha
u	u	k7c2	u
Norimberku	Norimberk	k1gInSc2	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
švabachu	švabach	k1gInSc6	švabach
je	být	k5eAaImIp3nS	být
tištěná	tištěný	k2eAgFnSc1d1	tištěná
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1579	[number]	k4	1579
<g/>
-	-	kIx~	-
<g/>
1594	[number]	k4	1594
a	a	k8xC	a
tisky	tisk	k1gInPc1	tisk
Jiřího	Jiří	k1gMnSc2	Jiří
Melantricha	Melantrich	k1gMnSc2	Melantrich
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
švabach	švabach	k1gInSc4	švabach
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
navázala	navázat	k5eAaPmAgFnS	navázat
novogotická	novogotický	k2eAgFnSc1d1	novogotická
pozdně	pozdně	k6eAd1	pozdně
bastardní	bastardní	k2eAgFnSc1d1	bastardní
fraktura	fraktura	k1gFnSc1	fraktura
<g/>
,	,	kIx,	,
i	i	k8xC	i
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
švabach	švabach	k1gInSc1	švabach
používal	používat	k5eAaImAgInS	používat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jako	jako	k8xS	jako
vyznačovací	vyznačovací	k2eAgNnSc4d1	vyznačovací
písmo	písmo	k1gNnSc4	písmo
k	k	k7c3	k
fraktuře	fraktura	k1gFnSc3	fraktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
obliba	obliba	k1gFnSc1	obliba
švabachu	švabach	k1gInSc2	švabach
mimo	mimo	k7c4	mimo
jiná	jiný	k2eAgNnPc4d1	jiné
novogotická	novogotický	k2eAgNnPc4d1	novogotické
písma	písmo	k1gNnPc4	písmo
v	v	k7c6	v
tiscích	tisk	k1gInPc6	tisk
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
méně	málo	k6eAd2	málo
hodnotných	hodnotný	k2eAgNnPc2d1	hodnotné
děl	dělo	k1gNnPc2	dělo
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
fraktura	fraktura	k1gFnSc1	fraktura
byla	být	k5eAaImAgFnS	být
používaná	používaný	k2eAgFnSc1d1	používaná
jako	jako	k8xC	jako
písmo	písmo	k1gNnSc1	písmo
vyznačovací	vyznačovací	k2eAgNnSc1d1	vyznačovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
slovem	slovo	k1gNnSc7	slovo
švabach	švabach	k1gInSc1	švabach
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
přeneseně	přeneseně	k6eAd1	přeneseně
rozumět	rozumět	k5eAaImF	rozumět
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
novogotické	novogotický	k2eAgNnSc4d1	novogotické
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označoval	označovat	k5eAaImAgInS	označovat
kurent	kurent	k1gInSc1	kurent
<g/>
,	,	kIx,	,
vyučovaný	vyučovaný	k2eAgInSc1d1	vyučovaný
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
