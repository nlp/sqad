<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Air	Air	k1gMnSc1
Transport	transporta	k1gFnPc2
Association	Association	k1gInSc4
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Air	Air	k1gMnSc1
Transport	transporta	k1gFnPc2
Association	Association	k1gInSc1
Logo	logo	k1gNnSc4
IATA	IATA	kA
Vznik	vznik	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
spolek	spolek	k1gInSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Montréal	Montréal	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
2,16	2,16	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
73	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
42,12	42,12	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.iata.org	www.iata.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
International	International	k1gFnSc1
Air	Air	k1gFnSc1
Transport	transport	k1gInSc4
Association	Association	k1gInSc1
(	(	kIx(
<g/>
IATA	IATA	kA
<g/>
,	,	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
leteckých	letecký	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nevládní	vládní	k2eNgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
sdružující	sdružující	k2eAgMnPc4d1
letecké	letecký	k2eAgMnPc4d1
dopravce	dopravce	k1gMnPc4
<g/>
,	,	kIx,
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Montrealu	Montreal	k1gInSc6
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členy	člen	k1gMnPc4
IATA	IATA	kA
je	být	k5eAaImIp3nS
okolo	okolo	k7c2
280	#num#	k4
společností	společnost	k1gFnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
společnosti	společnost	k1gFnPc1
zajišťují	zajišťovat	k5eAaImIp3nP
okolo	okolo	k7c2
83	#num#	k4
%	%	kIx~
pravidelné	pravidelný	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
přepravy	přeprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
organizace	organizace	k1gFnSc2
</s>
<s>
IATA	IATA	kA
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
činnosti	činnost	k1gFnSc6
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
dřívější	dřívější	k2eAgFnSc4d1
International	International	k1gFnSc4
Air	Air	k1gFnSc1
Traffic	Traffic	k1gMnSc1
Association	Association	k1gInSc1
(	(	kIx(
<g/>
založena	založen	k2eAgFnSc1d1
1919	#num#	k4
v	v	k7c6
Haagu	Haag	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovy	stanova	k1gFnSc2
dnešní	dnešní	k2eAgFnSc2d1
IATA	IATA	kA
byly	být	k5eAaImAgInP
navrženy	navrhnout	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
samotná	samotný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Havaně	Havana	k1gFnSc6
na	na	k7c6
Kubě	Kuba	k1gFnSc6
v	v	k7c6
dubnu	duben	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladateli	zakladatel	k1gMnPc7
bylo	být	k5eAaImAgNnS
57	#num#	k4
společností	společnost	k1gFnPc2
z	z	k7c2
31	#num#	k4
států	stát	k1gInPc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gInPc2
byly	být	k5eAaImAgFnP
také	také	k9
ČSA	ČSA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kódy	kód	k1gInPc1
IATA	IATA	kA
</s>
<s>
IATA	IATA	kA
přiděluje	přidělovat	k5eAaImIp3nS
třípísmenné	třípísmenný	k2eAgInPc4d1
letištní	letištní	k2eAgInPc4d1
kódy	kód	k1gInPc4
a	a	k8xC
dvoupísmenné	dvoupísmenný	k2eAgInPc4d1
kódy	kód	k1gInPc4
přepravců	přepravce	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Svoje	svůj	k3xOyFgInPc4
kódy	kód	k1gInPc4
přiděluje	přidělovat	k5eAaImIp3nS
také	také	k9
organizace	organizace	k1gFnSc1
ICAO	ICAO	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
činnosti	činnost	k1gFnPc1
IATA	IATA	kA
</s>
<s>
IATA	IATA	kA
také	také	k9
stanovuje	stanovovat	k5eAaImIp3nS
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
nebezpečného	bezpečný	k2eNgInSc2d1
nákladu	náklad	k1gInSc2
a	a	k8xC
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
příručku	příručka	k1gFnSc4
Dangerous	Dangerous	k1gMnSc1
Goods	Goodsa	k1gFnPc2
Regulations	Regulationsa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
referenční	referenční	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
nebezpečného	bezpečný	k2eNgInSc2d1
nákladu	náklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Letečtí	letecký	k2eAgMnPc1d1
dopravci	dopravce	k1gMnPc1
mají	mít	k5eAaImIp3nP
sjednanou	sjednaný	k2eAgFnSc4d1
výjimku	výjimka	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
spolu	spolu	k6eAd1
konzultovat	konzultovat	k5eAaImF
ceny	cena	k1gFnPc4
prostřednictvím	prostřednictvím	k7c2
této	tento	k3xDgFnSc2
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
IATA	IATA	kA
byla	být	k5eAaImAgFnS
obviněna	obvinit	k5eAaPmNgFnS
z	z	k7c2
kartelového	kartelový	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
a	a	k8xC
mnoho	mnoho	k4c1
nízkonákladových	nízkonákladový	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
není	být	k5eNaImIp3nS
plnohodnotnými	plnohodnotný	k2eAgInPc7d1
členy	člen	k1gInPc7
IATA	IATA	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.iata.org	www.iata.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PRUŠA	PRUŠA	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7041	#num#	k4
<g/>
-	-	kIx~
<g/>
543	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.iata.org	www.iata.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
International	International	k1gFnSc2
Air	Air	k1gFnSc2
Transport	transport	k1gInSc1
Association	Association	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
IATA	IATA	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
www.iata.org	www.iata.org	k1gInSc1
–	–	k?
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1011-X	1011-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2173	#num#	k4
3092	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81038751	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
142731388	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81038751	#num#	k4
</s>
