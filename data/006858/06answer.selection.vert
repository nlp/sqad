<s>
John	John	k1gMnSc1	John
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
John	John	k1gMnSc1	John
Richard	Richard	k1gMnSc1	Richard
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
Londýn	Londýn	k1gInSc1	Londýn
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
Palm	Palmo	k1gNnPc2	Palmo
Springs	Springsa	k1gFnPc2	Springsa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
britské	britský	k2eAgFnSc2d1	britská
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
