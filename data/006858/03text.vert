<s>
John	John	k1gMnSc1	John
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
John	John	k1gMnSc1	John
Richard	Richard	k1gMnSc1	Richard
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
Londýn	Londýn	k1gInSc1	Londýn
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
Palm	Palmo	k1gNnPc2	Palmo
Springs	Springsa	k1gFnPc2	Springsa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
britské	britský	k2eAgFnSc2d1	britská
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
25	[number]	k4	25
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jehož	k3xOyRp3gFnSc4	jehož
režii	režie	k1gFnSc4	režie
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
snímky	snímek	k1gInPc4	snímek
patří	patřit	k5eAaImIp3nS	patřit
Drahoušek	drahoušek	k1gMnSc1	drahoušek
<g/>
,	,	kIx,	,
Mizerná	mizerný	k2eAgFnSc1d1	mizerná
neděle	neděle	k1gFnSc1	neděle
<g/>
,	,	kIx,	,
Maratónec	maratónec	k1gMnSc1	maratónec
či	či	k8xC	či
Amíci	Amíce	k1gMnPc1	Amíce
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
však	však	k9	však
režíroval	režírovat	k5eAaImAgMnS	režírovat
i	i	k9	i
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1	nezanedbatelná
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
představuje	představovat	k5eAaImIp3nS	představovat
televizní	televizní	k2eAgFnSc1d1	televizní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
židovského	židovský	k2eAgMnSc2d1	židovský
pediatra	pediatr	k1gMnSc2	pediatr
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
také	také	k9	také
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
John	John	k1gMnSc1	John
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgMnS	učit
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
dostal	dostat	k5eAaPmAgMnS	dostat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
9,5	[number]	k4	9,5
<g/>
mm	mm	kA	mm
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
pak	pak	k6eAd1	pak
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Uppinghamu	Uppingham	k1gInSc6	Uppingham
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
jednotce	jednotka	k1gFnSc3	jednotka
Combined	Combined	k1gMnSc1	Combined
Services	Services	k1gMnSc1	Services
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
,	,	kIx,	,
zábavní	zábavní	k2eAgFnPc1d1	zábavní
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc1d1	umělecká
armádní	armádní	k2eAgFnPc1d1	armádní
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
bavil	bavit	k5eAaImAgMnS	bavit
vojáky	voják	k1gMnPc4	voják
kouzelnickými	kouzelnický	k2eAgInPc7d1	kouzelnický
kousky	kousek	k1gInPc7	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následných	následný	k2eAgFnPc2d1	následná
studií	studie	k1gFnPc2	studie
anglické	anglický	k2eAgFnSc2d1	anglická
literatury	literatura	k1gFnSc2	literatura
na	na	k7c4	na
oxfordské	oxfordský	k2eAgInPc4d1	oxfordský
Balliol	Balliol	k1gInSc4	Balliol
College	Colleg	k1gFnSc2	Colleg
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
ve	v	k7c6	v
studentském	studentský	k2eAgNnSc6d1	studentské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
navázal	navázat	k5eAaPmAgMnS	navázat
rolemi	role	k1gFnPc7	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
jako	jako	k8xS	jako
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
Rosalinda	Rosalinda	k1gFnSc1	Rosalinda
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
La	la	k1gNnSc2	la
Plata	plato	k1gNnSc2	plato
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
či	či	k8xC	či
Bratři	bratr	k1gMnPc1	bratr
mezi	mezi	k7c7	mezi
paragrafy	paragraf	k1gInPc7	paragraf
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
epizodních	epizodní	k2eAgFnPc6d1	epizodní
rolích	role	k1gFnPc6	role
seriálů	seriál	k1gInPc2	seriál
The	The	k1gFnSc1	The
Adventures	Adventures	k1gInSc1	Adventures
of	of	k?	of
Robin	robin	k2eAgInSc1d1	robin
Hood	Hood	k1gInSc1	Hood
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
či	či	k8xC	či
Ivanhoe	Ivanhoe	k1gFnSc1	Ivanhoe
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
vlastní	vlastní	k2eAgFnPc4d1	vlastní
herecké	herecký	k2eAgFnPc4d1	herecká
zkušenosti	zkušenost	k1gFnPc4	zkušenost
údajně	údajně	k6eAd1	údajně
měli	mít	k5eAaImAgMnP	mít
napomoci	napomoct	k5eAaPmF	napomoct
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
skvělému	skvělý	k2eAgNnSc3d1	skvělé
vedení	vedení	k1gNnSc3	vedení
herců	herc	k1gInPc2	herc
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
režiséra	režisér	k1gMnSc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
za	za	k7c7	za
studií	studie	k1gFnSc7	studie
také	také	k9	také
natáčel	natáčet	k5eAaImAgMnS	natáčet
amatérské	amatérský	k2eAgInPc4d1	amatérský
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
prvním	první	k4xOgInSc7	první
režijním	režijní	k2eAgInSc7d1	režijní
počinem	počin	k1gInSc7	počin
byl	být	k5eAaImAgInS	být
Horror	horror	k1gInSc1	horror
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Alanem	Alan	k1gMnSc7	Alan
Cookem	Cooek	k1gMnSc7	Cooek
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
snímcích	snímek	k1gInPc6	snímek
jako	jako	k8xS	jako
Black	Black	k1gInSc1	Black
Legend	legenda	k1gFnPc2	legenda
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
či	či	k8xC	či
The	The	k1gMnSc1	The
Starfish	Starfish	k1gMnSc1	Starfish
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
epizodních	epizodní	k2eAgFnPc2d1	epizodní
hereckých	herecký	k2eAgFnPc2d1	herecká
rolí	role	k1gFnPc2	role
pak	pak	k6eAd1	pak
začínal	začínat	k5eAaImAgMnS	začínat
u	u	k7c2	u
filmu	film	k1gInSc2	film
jako	jako	k8xC	jako
režisér	režisér	k1gMnSc1	režisér
dokumentů	dokument	k1gInPc2	dokument
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
televizí	televize	k1gFnSc7	televize
podepsal	podepsat	k5eAaPmAgMnS	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
a	a	k8xC	a
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
točil	točit	k5eAaImAgInS	točit
dokumenty	dokument	k1gInPc7	dokument
z	z	k7c2	z
uměleckého	umělecký	k2eAgInSc2d1	umělecký
cyklu	cyklus	k1gInSc2	cyklus
Monitor	monitor	k1gInSc1	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
Anstey	Anstea	k1gFnSc2	Anstea
z	z	k7c2	z
British	Britisha	k1gFnPc2	Britisha
Transport	transporta	k1gFnPc2	transporta
Films	Films	k1gInSc1	Films
ho	on	k3xPp3gInSc4	on
poté	poté	k6eAd1	poté
oslovil	oslovit	k5eAaPmAgInS	oslovit
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
filmu	film	k1gInSc2	film
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
Nádraží	nádraží	k1gNnSc1	nádraží
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
půlhodinový	půlhodinový	k2eAgInSc1d1	půlhodinový
dokument	dokument	k1gInSc1	dokument
zachycující	zachycující	k2eAgInSc1d1	zachycující
24	[number]	k4	24
<g/>
hodinové	hodinový	k2eAgNnSc4d1	hodinové
každodenní	každodenní	k2eAgNnSc4d1	každodenní
drama	drama	k1gNnSc4	drama
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
stanici	stanice	k1gFnSc6	stanice
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Zlatým	zlatý	k2eAgMnSc7d1	zlatý
lvem	lev	k1gMnSc7	lev
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
i	i	k9	i
cenou	cena	k1gFnSc7	cena
Britské	britský	k2eAgFnSc2d1	britská
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgInS	zaujmout
také	také	k9	také
italského	italský	k2eAgMnSc2d1	italský
producenta	producent	k1gMnSc2	producent
Josepha	Joseph	k1gMnSc2	Joseph
Janniho	Janni	k1gMnSc2	Janni
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
režisérovi	režisér	k1gMnSc3	režisér
zadal	zadat	k5eAaPmAgInS	zadat
pár	pár	k4xCyI	pár
reklam	reklama	k1gFnPc2	reklama
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
první	první	k4xOgInSc1	první
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
Takové	takový	k3xDgNnSc4	takový
milování	milování	k1gNnSc4	milování
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zdařilou	zdařilý	k2eAgFnSc4d1	zdařilá
adaptaci	adaptace	k1gFnSc4	adaptace
knihy	kniha	k1gFnSc2	kniha
Stana	Staen	k2eAgFnSc1d1	Stana
Barstowa	Barstowa	k1gFnSc1	Barstowa
s	s	k7c7	s
první	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
příležitostí	příležitost	k1gFnSc7	příležitost
pro	pro	k7c4	pro
Alana	Alan	k1gMnSc4	Alan
Batese	Batese	k1gFnSc2	Batese
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Schlesingerovi	Schlesinger	k1gMnSc3	Schlesinger
vynesla	vynést	k5eAaPmAgFnS	vynést
na	na	k7c6	na
berlínském	berlínský	k2eAgInSc6d1	berlínský
festivalu	festival	k1gInSc6	festival
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
medvěda	medvěd	k1gMnSc2	medvěd
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
hraného	hraný	k2eAgInSc2d1	hraný
debutu	debut	k1gInSc2	debut
natočil	natočit	k5eAaBmAgMnS	natočit
Schlesinger	Schlesinger	k1gInSc4	Schlesinger
pro	pro	k7c4	pro
Janniho	Janni	k1gMnSc4	Janni
další	další	k2eAgFnSc4d1	další
trojici	trojice	k1gFnSc4	trojice
filmů	film	k1gInPc2	film
Billy	Bill	k1gMnPc4	Bill
lhář	lhář	k1gMnSc1	lhář
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Drahoušek	drahoušek	k1gMnSc1	drahoušek
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hlučícího	hlučící	k2eAgInSc2d1	hlučící
davu	dav	k1gInSc2	dav
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Billyho	Billyze	k6eAd1	Billyze
lháře	lhář	k1gMnSc4	lhář
napsali	napsat	k5eAaPmAgMnP	napsat
stejní	stejný	k2eAgMnPc1d1	stejný
tvůrci	tvůrce	k1gMnPc1	tvůrce
jako	jako	k9	jako
Takové	takový	k3xDgNnSc4	takový
milování	milování	k1gNnSc4	milování
–	–	k?	–
Keith	Keith	k1gInSc1	Keith
Waterhouse	Waterhouse	k1gFnSc2	Waterhouse
a	a	k8xC	a
Willis	Willis	k1gFnSc2	Willis
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
směsici	směsice	k1gFnSc4	směsice
poněkud	poněkud	k6eAd1	poněkud
přisprostlé	přisprostlý	k2eAgFnSc2d1	přisprostlá
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
dramatu	drama	k1gNnSc2	drama
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
Courtenayem	Courtenay	k1gMnSc7	Courtenay
v	v	k7c6	v
roli	role	k1gFnSc6	role
pomocníka	pomocník	k1gMnSc2	pomocník
v	v	k7c6	v
pohřebním	pohřební	k2eAgInSc6d1	pohřební
ústavu	ústav	k1gInSc6	ústav
s	s	k7c7	s
dosti	dosti	k6eAd1	dosti
extravagantními	extravagantní	k2eAgFnPc7d1	extravagantní
fantaziemi	fantazie	k1gFnPc7	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgInS	stát
žádným	žádný	k3yNgInSc7	žádný
komerčním	komerční	k2eAgInSc7d1	komerční
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
však	však	k9	však
vyniknout	vyniknout	k5eAaPmF	vyniknout
také	také	k9	také
Julii	Julie	k1gFnSc4	Julie
Christie	Christie	k1gFnSc2	Christie
v	v	k7c6	v
roli	role	k1gFnSc6	role
volnomyšlenkářské	volnomyšlenkářský	k2eAgFnSc2d1	volnomyšlenkářská
dívky	dívka	k1gFnSc2	dívka
Liz	liz	k1gInSc1	liz
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
představitelem	představitel	k1gMnSc7	představitel
titulní	titulní	k2eAgFnSc2d1	titulní
role	role	k1gFnSc2	role
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
většího	veliký	k2eAgInSc2d2	veliký
úspěchu	úspěch	k1gInSc2	úspěch
pak	pak	k6eAd1	pak
Julie	Julie	k1gFnSc1	Julie
Christie	Christie	k1gFnSc2	Christie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
Schlesingerově	Schlesingerův	k2eAgMnSc6d1	Schlesingerův
následujícím	následující	k2eAgMnSc6d1	následující
Drahouškovi	drahoušek	k1gMnSc6	drahoušek
<g/>
,	,	kIx,	,
když	když	k8xS	když
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
modelky	modelka	k1gFnSc2	modelka
stoupající	stoupající	k2eAgFnSc2d1	stoupající
po	po	k7c6	po
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
nejvíc	nejvíc	k6eAd1	nejvíc
vychvalovaných	vychvalovaný	k2eAgInPc2d1	vychvalovaný
i	i	k8xC	i
kritizovaných	kritizovaný	k2eAgInPc2d1	kritizovaný
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Julii	Julie	k1gFnSc4	Julie
Christie	Christie	k1gFnSc2	Christie
sekundovali	sekundovat	k5eAaImAgMnP	sekundovat
Dirk	Dirk	k1gInSc4	Dirk
Bogarde	Bogard	k1gMnSc5	Bogard
a	a	k8xC	a
Laurence	Laurenec	k1gMnSc2	Laurenec
Harvey	Harvea	k1gMnSc2	Harvea
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgInS	vynést
ocenění	ocenění	k1gNnSc4	ocenění
newyorských	newyorský	k2eAgMnPc2d1	newyorský
kritiků	kritik	k1gMnPc2	kritik
(	(	kIx(	(
<g/>
NYFCC	NYFCC	kA	NYFCC
<g/>
)	)	kIx)	)
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
i	i	k8xC	i
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgFnPc6	tento
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
anglicky	anglicky	k6eAd1	anglicky
mluveného	mluvený	k2eAgInSc2d1	mluvený
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
i	i	k9	i
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
film	film	k1gInSc1	film
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hlučícího	hlučící	k2eAgInSc2d1	hlučící
davu	dav	k1gInSc2	dav
představoval	představovat	k5eAaImAgMnS	představovat
–	–	k?	–
po	po	k7c6	po
předchozích	předchozí	k2eAgInPc6d1	předchozí
černobílých	černobílý	k2eAgInPc6d1	černobílý
snímcích	snímek	k1gInPc6	snímek
–	–	k?	–
velkorozpočtovou	velkorozpočtový	k2eAgFnSc4d1	velkorozpočtový
barevnou	barevný	k2eAgFnSc4d1	barevná
adaptaci	adaptace	k1gFnSc4	adaptace
knihy	kniha	k1gFnSc2	kniha
Thomase	Thomas	k1gMnSc2	Thomas
Hardyho	Hardy	k1gMnSc2	Hardy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
dostala	dostat	k5eAaPmAgFnS	dostat
hereckou	herecký	k2eAgFnSc4d1	herecká
příležitost	příležitost	k1gFnSc4	příležitost
Julie	Julie	k1gFnSc2	Julie
Christie	Christie	k1gFnSc2	Christie
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
Alan	Alan	k1gMnSc1	Alan
Bates	Bates	k1gMnSc1	Bates
a	a	k8xC	a
nově	nově	k6eAd1	nově
také	také	k9	také
Peter	Peter	k1gMnSc1	Peter
Finch	Finch	k1gMnSc1	Finch
a	a	k8xC	a
Terence	Terence	k1gFnSc1	Terence
Stamp	Stamp	k1gInSc1	Stamp
v	v	k7c6	v
rolích	role	k1gFnPc6	role
trojice	trojice	k1gFnSc2	trojice
velmi	velmi	k6eAd1	velmi
rozdílných	rozdílný	k2eAgMnPc2d1	rozdílný
nápadníků	nápadník	k1gMnPc2	nápadník
usilujících	usilující	k2eAgFnPc2d1	usilující
o	o	k7c4	o
přízeň	přízeň	k1gFnSc4	přízeň
Bathsheby	Bathsheba	k1gFnSc2	Bathsheba
Everdeneové	Everdeneová	k1gFnSc2	Everdeneová
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
tehdy	tehdy	k6eAd1	tehdy
40	[number]	k4	40
<g/>
letému	letý	k2eAgMnSc3d1	letý
režisérovi	režisér	k1gMnSc3	režisér
otevřel	otevřít	k5eAaPmAgMnS	otevřít
dveře	dveře	k1gFnPc4	dveře
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
přijetí	přijetí	k1gNnSc6	přijetí
a	a	k8xC	a
Hollywoodu	Hollywood	k1gInSc6	Hollywood
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ukázal	ukázat	k5eAaPmAgInS	ukázat
jeho	jeho	k3xOp3gFnSc4	jeho
odlišnost	odlišnost	k1gFnSc4	odlišnost
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
tvůrců	tvůrce	k1gMnPc2	tvůrce
britské	britský	k2eAgFnSc2d1	britská
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Schlesinger	Schlesingra	k1gFnPc2	Schlesingra
přesunul	přesunout	k5eAaPmAgMnS	přesunout
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
i	i	k8xC	i
bydliště	bydliště	k1gNnSc4	bydliště
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
k	k	k7c3	k
britské	britský	k2eAgFnSc3d1	britská
filmové	filmový	k2eAgFnSc3d1	filmová
produkci	produkce	k1gFnSc3	produkce
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
snímkem	snímek	k1gInSc7	snímek
Mizerná	mizerný	k2eAgFnSc1d1	mizerná
neděle	neděle	k1gFnSc1	neděle
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
oslabenou	oslabený	k2eAgFnSc7d1	oslabená
britskou	britský	k2eAgFnSc7d1	britská
kinematografií	kinematografie	k1gFnSc7	kinematografie
zapustil	zapustit	k5eAaPmAgInS	zapustit
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
americkým	americký	k2eAgInSc7d1	americký
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
a	a	k8xC	a
kritiky	kritik	k1gMnPc4	kritik
nejlépe	dobře	k6eAd3	dobře
přijatý	přijatý	k2eAgMnSc1d1	přijatý
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
ho	on	k3xPp3gMnSc4	on
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
s	s	k7c7	s
Jonem	Jon	k1gMnSc7	Jon
Voightem	Voight	k1gMnSc7	Voight
a	a	k8xC	a
Dustinem	Dustin	k1gMnSc7	Dustin
Hoffmanem	Hoffman	k1gMnSc7	Hoffman
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
společenských	společenský	k2eAgMnPc2d1	společenský
outsiderů	outsider	k1gMnPc2	outsider
<g/>
.	.	kIx.	.
</s>
<s>
Voight	Voight	k1gMnSc1	Voight
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
naivního	naivní	k2eAgMnSc4d1	naivní
venkovského	venkovský	k2eAgMnSc4d1	venkovský
mladíka	mladík	k1gMnSc4	mladík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkusil	zkusit	k5eAaPmAgMnS	zkusit
štěstí	štěstí	k1gNnSc4	štěstí
jako	jako	k8xC	jako
prostitut	prostitut	k1gMnSc1	prostitut
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hoffman	Hoffman	k1gMnSc1	Hoffman
malého	malý	k2eAgMnSc2d1	malý
podvodníčka	podvodníček	k1gMnSc2	podvodníček
trpícího	trpící	k2eAgInSc2d1	trpící
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
téma	téma	k1gNnSc4	téma
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
opovážlivé	opovážlivý	k2eAgNnSc1d1	opovážlivé
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
řemeslně	řemeslně	k6eAd1	řemeslně
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zpracované	zpracovaný	k2eAgFnPc1d1	zpracovaná
a	a	k8xC	a
skvěle	skvěle	k6eAd1	skvěle
zahrané	zahraný	k2eAgNnSc1d1	zahrané
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
získalo	získat	k5eAaPmAgNnS	získat
přízeň	přízeň	k1gFnSc4	přízeň
diváků	divák	k1gMnPc2	divák
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
Oscara	Oscar	k1gMnSc2	Oscar
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
sošky	soška	k1gFnSc2	soška
přinesl	přinést	k5eAaPmAgInS	přinést
Schlesingerovi	Schlesinger	k1gMnSc3	Schlesinger
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
Waldo	Waldo	k1gNnSc4	Waldo
Saltovi	Salt	k1gMnSc3	Salt
za	za	k7c4	za
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Voight	Voight	k1gMnSc1	Voight
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
cenu	cena	k1gFnSc4	cena
Newyorských	newyorský	k2eAgMnPc2d1	newyorský
kritiků	kritik	k1gMnPc2	kritik
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
nováčka	nováček	k1gMnSc4	nováček
a	a	k8xC	a
v	v	k7c6	v
obdobné	obdobný	k2eAgFnSc6d1	obdobná
kategorii	kategorie	k1gFnSc6	kategorie
také	také	k9	také
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
akademie	akademie	k1gFnSc1	akademie
ocenila	ocenit	k5eAaPmAgFnS	ocenit
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
i	i	k9	i
Hoffmana	Hoffman	k1gMnSc4	Hoffman
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
režiséra	režisér	k1gMnSc4	režisér
<g/>
,	,	kIx,	,
scenáristu	scenárista	k1gMnSc4	scenárista
i	i	k8xC	i
střihače	střihač	k1gMnSc4	střihač
a	a	k8xC	a
samotný	samotný	k2eAgInSc4d1	samotný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
však	však	k9	však
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
přistupoval	přistupovat	k5eAaImAgInS	přistupovat
poměrně	poměrně	k6eAd1	poměrně
zdrženlivě	zdrženlivě	k6eAd1	zdrženlivě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
někdy	někdy	k6eAd1	někdy
nějaký	nějaký	k3yIgInSc4	nějaký
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
vyložená	vyložený	k2eAgFnSc1d1	vyložená
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Netušil	Netušil	k1gMnSc1	Netušil
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
povede	vést	k5eAaImIp3nS	vést
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Každopádně	každopádně	k6eAd1	každopádně
mu	on	k3xPp3gMnSc3	on
Půlnoční	půlnoční	k1gFnSc6	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
zajistil	zajistit	k5eAaPmAgMnS	zajistit
dobré	dobrý	k2eAgNnSc4d1	dobré
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
možnost	možnost	k1gFnSc4	možnost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Jannim	Jannima	k1gFnPc2	Jannima
natočil	natočit	k5eAaBmAgMnS	natočit
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
britský	britský	k2eAgInSc4d1	britský
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
nejosobnější	osobní	k2eAgNnSc4d3	nejosobnější
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Mizernou	mizerný	k2eAgFnSc4d1	mizerná
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Finch	Finch	k1gMnSc1	Finch
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
bisexuálním	bisexuální	k2eAgInSc6d1	bisexuální
milostném	milostný	k2eAgInSc6d1	milostný
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
dělí	dělit	k5eAaImIp3nS	dělit
o	o	k7c4	o
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
mladému	mladý	k2eAgMnSc3d1	mladý
umělci	umělec	k1gMnSc3	umělec
(	(	kIx(	(
<g/>
Murray	Murraa	k1gFnSc2	Murraa
Head	Head	k1gInSc1	Head
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ženou	žena	k1gFnSc7	žena
(	(	kIx(	(
<g/>
Glenda	Glenda	k1gFnSc1	Glenda
Jacksonová	Jacksonová	k1gFnSc1	Jacksonová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
režisérových	režisérův	k2eAgFnPc6d1	režisérova
osobních	osobní	k2eAgFnPc6d1	osobní
zkušenostech	zkušenost	k1gFnPc6	zkušenost
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
měl	mít	k5eAaImAgMnS	mít
coby	coby	k?	coby
otevřený	otevřený	k2eAgMnSc1d1	otevřený
gay	gay	k1gMnSc1	gay
asi	asi	k9	asi
dvouletý	dvouletý	k2eAgInSc4d1	dvouletý
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
mladším	mladý	k2eAgMnSc7d2	mladší
mužem	muž	k1gMnSc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Zlatým	zlatý	k2eAgInSc7d1	zlatý
glóbem	glóbus	k1gInSc7	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
anglicky	anglicky	k6eAd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
cen	cena	k1gFnPc2	cena
Britské	britský	k2eAgFnSc2d1	britská
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
<g/>
,	,	kIx,	,
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
a	a	k8xC	a
střih	střih	k1gInSc4	střih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
čtyři	čtyři	k4xCgFnPc4	čtyři
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
té	ten	k3xDgFnSc2	ten
režijní	režijní	k2eAgNnPc4d1	režijní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
natočil	natočit	k5eAaBmAgInS	natočit
segment	segment	k1gInSc1	segment
"	"	kIx"	"
<g/>
Nejdelší	dlouhý	k2eAgMnSc1d3	nejdelší
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Longest	Longest	k1gFnSc1	Longest
<g/>
)	)	kIx)	)
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
maratonskému	maratonský	k2eAgInSc3d1	maratonský
běhu	běh	k1gInSc3	běh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
součástí	součást	k1gFnSc7	součást
kolektivního	kolektivní	k2eAgInSc2d1	kolektivní
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
dokumentu	dokument	k1gInSc2	dokument
Viděno	vidět	k5eAaImNgNnS	vidět
osmi	osm	k4xCc2	osm
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
osmi	osm	k4xCc2	osm
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
filmu	film	k1gInSc6	film
podíleli	podílet	k5eAaImAgMnP	podílet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
segmentu	segment	k1gInSc2	segment
o	o	k7c6	o
desetiboji	desetiboj	k1gInSc6	desetiboj
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
však	však	k9	však
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Olympiáda	olympiáda	k1gFnSc1	olympiáda
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
zastíněn	zastínit	k5eAaPmNgInS	zastínit
tragickým	tragický	k2eAgInSc7d1	tragický
atentátem	atentát	k1gInSc7	atentát
na	na	k7c4	na
izraelské	izraelský	k2eAgMnPc4d1	izraelský
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
následoval	následovat	k5eAaImAgInS	následovat
ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
snímek	snímek	k1gInSc1	snímek
Den	den	k1gInSc1	den
kobylek	kobylka	k1gFnPc2	kobylka
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hyperbolická	hyperbolický	k2eAgFnSc1d1	hyperbolická
adaptace	adaptace	k1gFnSc1	adaptace
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Nathanaela	Nathanael	k1gMnSc4	Nathanael
Westa	West	k1gMnSc4	West
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
s	s	k7c7	s
Donaldem	Donald	k1gMnSc7	Donald
Sutherlandem	Sutherlando	k1gNnSc7	Sutherlando
a	a	k8xC	a
Karen	Karen	k2eAgInSc1d1	Karen
Black	Black	k1gInSc1	Black
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
však	však	k9	však
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
poměrně	poměrně	k6eAd1	poměrně
kriticky	kriticky	k6eAd1	kriticky
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
herce	herec	k1gMnPc4	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Kasovním	kasovní	k2eAgInSc7d1	kasovní
hitem	hit	k1gInSc7	hit
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stal	stát	k5eAaPmAgInS	stát
thriller	thriller	k1gInSc1	thriller
Maratónec	maratónec	k1gMnSc1	maratónec
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
o	o	k7c6	o
nacistech	nacista	k1gMnPc6	nacista
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
sklidil	sklidit	k5eAaPmAgInS	sklidit
i	i	k9	i
kritické	kritický	k2eAgInPc4d1	kritický
ohlasy	ohlas	k1gInPc4	ohlas
za	za	k7c4	za
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
brutalitě	brutalita	k1gFnSc3	brutalita
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
známou	známá	k1gFnSc7	známá
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sadistický	sadistický	k2eAgMnSc1d1	sadistický
dentista	dentista	k1gMnSc1	dentista
(	(	kIx(	(
<g/>
Laurence	Laurence	k1gFnSc1	Laurence
Olivier	Olivier	k1gMnSc1	Olivier
<g/>
)	)	kIx)	)
trýzní	trýznit	k5eAaImIp3nS	trýznit
svou	svůj	k3xOyFgFnSc4	svůj
oběť	oběť	k1gFnSc4	oběť
(	(	kIx(	(
<g/>
Dustin	Dustin	k1gMnSc1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
)	)	kIx)	)
zubařským	zubařský	k2eAgInSc7d1	zubařský
zákrokem	zákrok	k1gInSc7	zákrok
bez	bez	k7c2	bez
anestezie	anestezie	k1gFnSc2	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
kategoriích	kategorie	k1gFnPc6	kategorie
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
pěti	pět	k4xCc6	pět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
té	ten	k3xDgFnSc2	ten
režijní	režijní	k2eAgFnSc2d1	režijní
<g/>
,	,	kIx,	,
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
proměnil	proměnit	k5eAaPmAgMnS	proměnit
jen	jen	k9	jen
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Laurence	Laurenec	k1gMnSc4	Laurenec
Oliviera	Olivier	k1gMnSc4	Olivier
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
kategorii	kategorie	k1gFnSc6	kategorie
nominován	nominovat	k5eAaBmNgMnS	nominovat
i	i	k9	i
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Schlesinger	Schlesinger	k1gInSc1	Schlesinger
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Jannim	Jannima	k1gFnPc2	Jannima
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Amíci	Amíce	k1gMnPc1	Amíce
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
před	před	k7c7	před
invazí	invaze	k1gFnSc7	invaze
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
začnou	začít	k5eAaPmIp3nP	začít
pletky	pletka	k1gFnPc4	pletka
s	s	k7c7	s
anglickými	anglický	k2eAgFnPc7d1	anglická
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
válečné	válečný	k2eAgFnSc2d1	válečná
romance	romance	k1gFnSc2	romance
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
Richard	Richard	k1gMnSc1	Richard
Gere	Ger	k1gFnSc2	Ger
a	a	k8xC	a
Vanessa	Vanessa	k1gFnSc1	Vanessa
Redgrave	Redgrav	k1gInSc5	Redgrav
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
několik	několik	k4yIc4	několik
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uspěl	uspět	k5eAaPmAgMnS	uspět
jen	jen	k9	jen
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Amících	Amíek	k1gInPc6	Amíek
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
režijní	režijní	k2eAgFnSc6d1	režijní
tvorbě	tvorba	k1gFnSc6	tvorba
největší	veliký	k2eAgInSc1d3	veliký
propad	propad	k1gInSc1	propad
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nevzpamatoval	vzpamatovat	k5eNaPmAgMnS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Frenetická	frenetický	k2eAgFnSc1d1	frenetická
komedie	komedie	k1gFnSc1	komedie
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
dálnice	dálnice	k1gFnSc1	dálnice
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
o	o	k7c6	o
americkém	americký	k2eAgNnSc6d1	americké
městečku	městečko	k1gNnSc6	městečko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
starosta	starosta	k1gMnSc1	starosta
pro	pro	k7c4	pro
nalákání	nalákání	k1gNnSc4	nalákání
turistů	turist	k1gMnPc2	turist
přemaloval	přemalovat	k5eAaPmAgMnS	přemalovat
na	na	k7c4	na
růžovo	růžovo	k1gNnSc4	růžovo
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
značných	značný	k2eAgFnPc2d1	značná
24	[number]	k4	24
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zcela	zcela	k6eAd1	zcela
propadl	propadnout	k5eAaPmAgInS	propadnout
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
stažen	stáhnout	k5eAaPmNgInS	stáhnout
distributorem	distributor	k1gMnSc7	distributor
už	už	k6eAd1	už
týden	týden	k1gInSc4	týden
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
se	se	k3xPyFc4	se
po	po	k7c6	po
nezdaru	nezdar	k1gInSc6	nezdar
obrátil	obrátit	k5eAaPmAgInS	obrátit
k	k	k7c3	k
televizní	televizní	k2eAgFnSc3d1	televizní
tvorbě	tvorba	k1gFnSc3	tvorba
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
Oddělené	oddělený	k2eAgInPc4d1	oddělený
stoly	stol	k1gInPc4	stol
a	a	k8xC	a
An	An	k1gFnSc2	An
Englishman	Englishman	k1gMnSc1	Englishman
Abroad	Abroad	k1gInSc1	Abroad
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
televizních	televizní	k2eAgInPc2d1	televizní
filmů	film	k1gInPc2	film
natočil	natočit	k5eAaBmAgInS	natočit
s	s	k7c7	s
Alanem	Alan	k1gMnSc7	Alan
Batesem	Bates	k1gMnSc7	Bates
a	a	k8xC	a
Coral	Coral	k1gInSc4	Coral
Brownovou	Brownová	k1gFnSc4	Brownová
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
vlastního	vlastní	k2eAgInSc2d1	vlastní
životního	životní	k2eAgInSc2d1	životní
příběhu	příběh	k1gInSc2	příběh
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
zapletla	zaplést	k5eAaPmAgFnS	zaplést
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
dvojitým	dvojitý	k2eAgMnSc7d1	dvojitý
špionem	špion	k1gMnSc7	špion
Guyem	Guy	k1gMnSc7	Guy
Burgessem	Burgess	k1gMnSc7	Burgess
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
napsal	napsat	k5eAaPmAgMnS	napsat
Alan	Alan	k1gMnSc1	Alan
Bennett	Bennett	k1gMnSc1	Bennett
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc1	kritika
byl	být	k5eAaImAgInS	být
chválen	chválit	k5eAaImNgInS	chválit
a	a	k8xC	a
nazýván	nazývat	k5eAaImNgInS	nazývat
televizní	televizní	k2eAgFnSc7d1	televizní
událostí	událost	k1gFnSc7	událost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
plátno	plátno	k1gNnSc4	plátno
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
nevydařeným	vydařený	k2eNgInSc7d1	nevydařený
politickým	politický	k2eAgInSc7d1	politický
thrillerem	thriller	k1gInSc7	thriller
Dravec	dravec	k1gMnSc1	dravec
a	a	k8xC	a
feťák	feťák	k?	feťák
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
natočeným	natočený	k2eAgInSc7d1	natočený
pochmurným	pochmurný	k2eAgInSc7d1	pochmurný
snímkem	snímek	k1gInSc7	snímek
The	The	k1gFnPc2	The
Believers	Believersa	k1gFnPc2	Believersa
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
i	i	k9	i
koprodukoval	koprodukovat	k5eAaPmAgMnS	koprodukovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neuspěl	uspět	k5eNaPmAgMnS	uspět
komerčně	komerčně	k6eAd1	komerčně
ani	ani	k8xC	ani
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
formy	forma	k1gFnSc2	forma
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k9	až
adaptací	adaptace	k1gFnSc7	adaptace
knihy	kniha	k1gFnSc2	kniha
Bernice	Bernice	k1gFnSc2	Bernice
Rubensové	Rubensová	k1gFnSc2	Rubensová
Madame	madame	k1gFnSc1	madame
Sousatzka	Sousatzka	k1gFnSc1	Sousatzka
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
s	s	k7c7	s
Shirley	Shirley	k1gInPc7	Shirley
MacLaine	MacLain	k1gInSc5	MacLain
v	v	k7c6	v
roli	role	k1gFnSc6	role
stárnoucí	stárnoucí	k2eAgFnSc2d1	stárnoucí
pianistky	pianistka	k1gFnSc2	pianistka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
výjimečně	výjimečně	k6eAd1	výjimečně
nadaného	nadaný	k2eAgMnSc4d1	nadaný
indického	indický	k2eAgMnSc4d1	indický
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
snímek	snímek	k1gInSc4	snímek
posledních	poslední	k2eAgFnPc2d1	poslední
dvou	dva	k4xCgFnPc2	dva
dekád	dekáda	k1gFnPc2	dekáda
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgMnSc1	sám
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
svých	svůj	k3xOyFgInPc2	svůj
nejzamilovanějších	zamilovaný	k2eAgInPc2d3	nejzamilovanější
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Takovým	takový	k3xDgNnSc7	takový
milováním	milování	k1gNnSc7	milování
<g/>
,	,	kIx,	,
Půlnočním	půlnoční	k2eAgMnSc7d1	půlnoční
kovbojem	kovboj	k1gMnSc7	kovboj
a	a	k8xC	a
Mizernou	mizerný	k2eAgFnSc7d1	mizerná
nedělí	neděle	k1gFnSc7	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
natočil	natočit	k5eAaBmAgMnS	natočit
komerčně	komerčně	k6eAd1	komerčně
úspěšnější	úspěšný	k2eAgInSc4d2	úspěšnější
thriller	thriller	k1gInSc4	thriller
Psychopat	psychopat	k1gMnSc1	psychopat
ze	z	k7c2	z
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
s	s	k7c7	s
Melanie	Melanie	k1gFnPc1	Melanie
Griffithovou	Griffithová	k1gFnSc7	Griffithová
a	a	k8xC	a
Michaelem	Michael	k1gMnSc7	Michael
Keatonem	Keaton	k1gInSc7	Keaton
jako	jako	k9	jako
jejím	její	k3xOp3gMnSc7	její
psychotickým	psychotický	k2eAgMnSc7d1	psychotický
sousedem	soused	k1gMnSc7	soused
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
působivosti	působivost	k1gFnSc3	působivost
filmu	film	k1gInSc2	film
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
hudba	hudba	k1gFnSc1	hudba
Hanse	Hans	k1gMnSc2	Hans
Zimmera	Zimmer	k1gMnSc2	Zimmer
<g/>
,	,	kIx,	,
propojující	propojující	k2eAgInPc1d1	propojující
syntetizéry	syntetizér	k1gInPc1	syntetizér
s	s	k7c7	s
jazzovými	jazzový	k2eAgMnPc7d1	jazzový
hráči	hráč	k1gMnPc7	hráč
a	a	k8xC	a
vokály	vokál	k1gInPc7	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
Nevinný	vinný	k2eNgMnSc1d1	nevinný
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc4	drama
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Iana	Ianus	k1gMnSc4	Ianus
McEwana	McEwan	k1gMnSc4	McEwan
o	o	k7c6	o
naivním	naivní	k2eAgMnSc6d1	naivní
mladém	mladý	k2eAgMnSc6d1	mladý
specialistovi	specialista	k1gMnSc6	specialista
(	(	kIx(	(
<g/>
Campbell	Campbell	k1gMnSc1	Campbell
Scott	Scott	k1gMnSc1	Scott
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c4	v
špiony	špion	k1gMnPc4	špion
prolezlém	prolezlý	k2eAgInSc6d1	prolezlý
Berlíně	Berlín	k1gInSc6	Berlín
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
s	s	k7c7	s
tragickými	tragický	k2eAgInPc7d1	tragický
následky	následek	k1gInPc7	následek
do	do	k7c2	do
tajuplné	tajuplný	k2eAgFnSc2d1	tajuplná
Němky	Němka	k1gFnSc2	Němka
(	(	kIx(	(
<g/>
Isabella	Isabella	k1gFnSc1	Isabella
Rosselliniová	Rosselliniový	k2eAgFnSc1d1	Rosselliniová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
žoviálního	žoviální	k2eAgMnSc2d1	žoviální
Američana	Američan	k1gMnSc2	Američan
Glasse	Glass	k1gMnSc2	Glass
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k8xC	i
Anthony	Anthon	k1gMnPc4	Anthon
Hopkins	Hopkins	k1gInSc4	Hopkins
<g/>
,	,	kIx,	,
snímek	snímek	k1gInSc4	snímek
ale	ale	k8xC	ale
mnoho	mnoho	k4c4	mnoho
úspěchů	úspěch	k1gInPc2	úspěch
nesklidil	sklidit	k5eNaPmAgMnS	sklidit
<g/>
.	.	kIx.	.
</s>
<s>
Točil	točit	k5eAaImAgMnS	točit
opět	opět	k6eAd1	opět
i	i	k9	i
televizní	televizní	k2eAgInPc4d1	televizní
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
cyklus	cyklus	k1gInSc4	cyklus
BBC	BBC	kA	BBC
"	"	kIx"	"
<g/>
Screen	Screen	k2eAgMnSc1d1	Screen
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Alana	Alan	k1gMnSc2	Alan
Bennetta	Bennett	k1gMnSc2	Bennett
A	a	k8xC	a
Question	Question	k1gInSc4	Question
of	of	k?	of
Attribution	Attribution	k1gInSc1	Attribution
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Foxem	fox	k1gInSc7	fox
v	v	k7c6	v
roli	role	k1gFnSc6	role
britského	britský	k2eAgMnSc2d1	britský
historika	historik	k1gMnSc2	historik
a	a	k8xC	a
sovětského	sovětský	k2eAgMnSc2d1	sovětský
agenta	agent	k1gMnSc2	agent
Anthonyho	Anthony	k1gMnSc2	Anthony
Blunta	Blunt	k1gMnSc2	Blunt
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Většího	veliký	k2eAgInSc2d2	veliký
úspěchu	úspěch	k1gInSc2	úspěch
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
pak	pak	k6eAd1	pak
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
televizní	televizní	k2eAgFnSc7d1	televizní
adaptací	adaptace	k1gFnSc7	adaptace
knihy	kniha	k1gFnSc2	kniha
Stelly	Stella	k1gFnSc2	Stella
Gibbonsové	Gibbonsová	k1gFnSc2	Gibbonsová
Farma	farma	k1gFnSc1	farma
Cold	Cold	k1gMnSc1	Cold
Comfort	Comfort	k1gInSc1	Comfort
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
romantická	romantický	k2eAgFnSc1d1	romantická
komedie	komedie	k1gFnSc1	komedie
s	s	k7c7	s
Eileen	Eilena	k1gFnPc2	Eilena
Atkinsovou	Atkinsův	k2eAgFnSc7d1	Atkinsova
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Beckinsaleovou	Beckinsaleův	k2eAgFnSc7d1	Beckinsaleův
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
a	a	k8xC	a
např.	např.	kA	např.
Stephenem	Stephen	k1gMnSc7	Stephen
Fryem	Fry	k1gMnSc7	Fry
či	či	k8xC	či
Ianem	Ianus	k1gMnSc7	Ianus
McKellenem	McKellen	k1gInSc7	McKellen
v	v	k7c6	v
rolích	role	k1gFnPc6	role
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
amerických	americký	k2eAgNnPc2d1	americké
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
přispěl	přispět	k5eAaPmAgMnS	přispět
svým	svůj	k3xOyFgInSc7	svůj
rozhovorem	rozhovor	k1gInSc7	rozhovor
ke	k	k7c3	k
koprodukčnímu	koprodukční	k2eAgInSc3d1	koprodukční
dokumentu	dokument	k1gInSc3	dokument
o	o	k7c4	o
zobrazování	zobrazování	k1gNnSc4	zobrazování
gayů	gay	k1gMnPc2	gay
a	a	k8xC	a
leseb	lesba	k1gFnPc2	lesba
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kinematografie	kinematografie	k1gFnSc2	kinematografie
The	The	k1gMnPc2	The
Celluloid	Celluloid	k1gInSc1	Celluloid
Closet	Closeta	k1gFnPc2	Closeta
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
4	[number]	k4	4
<g/>
%	%	kIx~	%
filmová	filmový	k2eAgNnPc4d1	filmové
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
dalším	další	k2eAgInSc6d1	další
thrilleru	thriller	k1gInSc6	thriller
Oko	oko	k1gNnSc1	oko
za	za	k7c4	za
oko	oko	k1gNnSc4	oko
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Sally	Sall	k1gMnPc4	Sall
Fieldová	Fieldová	k1gFnSc1	Fieldová
Karen	Karen	k1gInSc4	Karen
McCannovou	McCannová	k1gFnSc7	McCannová
<g/>
,	,	kIx,	,
odhodlanou	odhodlaný	k2eAgFnSc7d1	odhodlaná
pomstít	pomstít	k5eAaPmF	pomstít
znásilnění	znásilnění	k1gNnSc4	znásilnění
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
své	svůj	k3xOyFgFnSc2	svůj
17	[number]	k4	17
<g/>
leté	letý	k2eAgFnSc2d1	letá
dcery	dcera	k1gFnSc2	dcera
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vrah	vrah	k1gMnSc1	vrah
(	(	kIx(	(
<g/>
Kiefer	Kiefer	k1gInSc1	Kiefer
Sutherland	Sutherlanda	k1gFnPc2	Sutherlanda
<g/>
)	)	kIx)	)
kvůli	kvůli	k7c3	kvůli
procedurální	procedurální	k2eAgFnSc3d1	procedurální
chybě	chyba	k1gFnSc3	chyba
unikl	uniknout	k5eAaPmAgInS	uniknout
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
snímkem	snímek	k1gInSc7	snímek
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hororový	hororový	k2eAgInSc1d1	hororový
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Oholený	oholený	k2eAgMnSc1d1	oholený
klenotník	klenotník	k1gMnSc1	klenotník
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Chichesteru	Chichester	k1gInSc6	Chichester
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
jen	jen	k9	jen
na	na	k7c4	na
obrazovky	obrazovka	k1gFnPc4	obrazovka
televize	televize	k1gFnSc2	televize
Sky	Sky	k1gFnPc2	Sky
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Schlesinger	Schlesinger	k1gInSc1	Schlesinger
svým	svůj	k3xOyFgInSc7	svůj
posledním	poslední	k2eAgInSc7d1	poslední
dokončeným	dokončený	k2eAgInSc7d1	dokončený
filmem	film	k1gInSc7	film
s	s	k7c7	s
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
a	a	k8xC	a
Rupertem	Rupert	k1gMnSc7	Rupert
Everettem	Everett	k1gMnSc7	Everett
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
dramedie	dramedie	k1gFnSc1	dramedie
Příští	příští	k2eAgFnSc1d1	příští
správná	správný	k2eAgFnSc1d1	správná
věc	věc	k1gFnSc1	věc
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
o	o	k7c6	o
gayi	gay	k1gMnSc6	gay
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
kamarádce	kamarádka	k1gFnSc3	kamarádka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
společně	společně	k6eAd1	společně
strávené	strávený	k2eAgFnPc1d1	strávená
noci	noc	k1gFnPc1	noc
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
vychovávat	vychovávat	k5eAaImF	vychovávat
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
nezískala	získat	k5eNaPmAgFnS	získat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
hodnocení	hodnocení	k1gNnSc4	hodnocení
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgNnP	být
nominována	nominovat	k5eAaBmNgNnP	nominovat
na	na	k7c4	na
mediální	mediální	k2eAgFnSc4d1	mediální
cenu	cena	k1gFnSc4	cena
GLAAD	GLAAD	kA	GLAAD
<g/>
.	.	kIx.	.
</s>
<s>
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
příležitostně	příležitostně	k6eAd1	příležitostně
zavítal	zavítat	k5eAaPmAgInS	zavítat
i	i	k9	i
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
nebo	nebo	k8xC	nebo
operní	operní	k2eAgFnSc3d1	operní
režii	režie	k1gFnSc3	režie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
režíroval	režírovat	k5eAaImAgMnS	režírovat
svůj	svůj	k3xOyFgInSc4	svůj
jediný	jediný	k2eAgInSc4d1	jediný
muzikál	muzikál	k1gInSc4	muzikál
I	i	k9	i
And	Anda	k1gFnPc2	Anda
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
těžkou	těžký	k2eAgFnSc7d1	těžká
zkušeností	zkušenost	k1gFnSc7	zkušenost
a	a	k8xC	a
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgMnS	udržet
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
inscenace	inscenace	k1gFnSc1	inscenace
Domu	dům	k1gInSc2	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
od	od	k7c2	od
G.	G.	kA	G.
B.	B.	kA	B.
Shawa	Shawa	k1gMnSc1	Shawa
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
tamtéž	tamtéž	k6eAd1	tamtéž
nesetkal	setkat	k5eNaPmAgMnS	setkat
zdaleka	zdaleka	k6eAd1	zdaleka
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
zde	zde	k6eAd1	zde
uvedl	uvést	k5eAaPmAgMnS	uvést
také	také	k9	také
hru	hra	k1gFnSc4	hra
Sama	Sam	k1gMnSc2	Sam
Sheparda	Shepard	k1gMnSc2	Shepard
True	True	k1gInSc1	True
West	West	k2eAgInSc1d1	West
(	(	kIx(	(
<g/>
Pravý	pravý	k2eAgInSc1d1	pravý
či	či	k8xC	či
Pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
západ	západ	k1gInSc1	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přesedlal	přesedlat	k5eAaPmAgMnS	přesedlat
na	na	k7c4	na
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
připravil	připravit	k5eAaPmAgInS	připravit
pro	pro	k7c4	pro
londýnskou	londýnský	k2eAgFnSc4d1	londýnská
Royal	Royal	k1gMnSc1	Royal
Opera	opera	k1gFnSc1	opera
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Covent	Covent	k1gInSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
inscenaci	inscenace	k1gFnSc3	inscenace
Offenbachovy	Offenbachův	k2eAgFnSc2d1	Offenbachova
opery	opera	k1gFnSc2	opera
Hoffmannovy	Hoffmannův	k2eAgFnSc2d1	Hoffmannova
povídky	povídka	k1gFnSc2	povídka
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
Georgesem	Georges	k1gMnSc7	Georges
Prê	Prê	k1gMnSc7	Prê
a	a	k8xC	a
s	s	k7c7	s
Plácidem	Plácid	k1gMnSc7	Plácid
Domingem	Doming	k1gInSc7	Doming
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
tamtéž	tamtéž	k6eAd1	tamtéž
režíroval	režírovat	k5eAaImAgMnS	režírovat
Straussova	Straussův	k2eAgMnSc4d1	Straussův
Růžového	růžový	k2eAgMnSc4d1	růžový
kavalíra	kavalír	k1gMnSc4	kavalír
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
Georgem	Georg	k1gMnSc7	Georg
Soltim	Soltima	k1gFnPc2	Soltima
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
představení	představení	k1gNnPc1	představení
zůstala	zůstat	k5eAaPmAgNnP	zůstat
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
operního	operní	k2eAgInSc2d1	operní
domu	dům	k1gInSc2	dům
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
Hoffmanovy	Hoffmanův	k2eAgFnPc1d1	Hoffmanova
povídky	povídka	k1gFnPc1	povídka
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
uvedení	uvedení	k1gNnSc2	uvedení
ještě	ještě	k9	ještě
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
a	a	k8xC	a
Rosenkavalier	Rosenkavalier	k1gMnSc1	Rosenkavalier
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pak	pak	k6eAd1	pak
režíroval	režírovat	k5eAaImAgMnS	režírovat
na	na	k7c6	na
Salcburském	salcburský	k2eAgInSc6d1	salcburský
festivalu	festival	k1gInSc6	festival
Verdiho	Verdi	k1gMnSc2	Verdi
Maškarní	maškarní	k2eAgInSc1d1	maškarní
ples	ples	k1gInSc1	ples
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
pro	pro	k7c4	pro
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
Metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
operu	opera	k1gFnSc4	opera
režírovat	režírovat	k5eAaImF	režírovat
Verdiho	Verdi	k1gMnSc2	Verdi
Othella	Othello	k1gMnSc2	Othello
s	s	k7c7	s
Placidem	Placid	k1gMnSc7	Placid
Domingem	Doming	k1gInSc7	Doming
v	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
úvodních	úvodní	k2eAgNnPc6d1	úvodní
jednáních	jednání	k1gNnPc6	jednání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
seznal	seznat	k5eAaPmAgMnS	seznat
operní	operní	k2eAgInSc4d1	operní
dům	dům	k1gInSc4	dům
jeho	jeho	k3xOp3gMnSc3	jeho
produkci	produkce	k1gFnSc4	produkce
nerealizovatelnou	realizovatelný	k2eNgFnSc4d1	nerealizovatelná
a	a	k8xC	a
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
díla	dílo	k1gNnSc2	dílo
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
ho	on	k3xPp3gInSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
režisér	režisér	k1gMnSc1	režisér
Elijah	Elijah	k1gMnSc1	Elijah
Moshinsky	Moshinsky	k1gMnSc1	Moshinsky
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
udržoval	udržovat	k5eAaImAgMnS	udržovat
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
životním	životní	k2eAgMnSc7d1	životní
partnerem	partner	k1gMnSc7	partner
<g/>
,	,	kIx,	,
fotografem	fotograf	k1gMnSc7	fotograf
Michaelem	Michael	k1gMnSc7	Michael
Childersem	Childers	k1gMnSc7	Childers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
utrpěl	utrpět	k5eAaPmAgInS	utrpět
záchvat	záchvat	k1gInSc1	záchvat
mozkové	mozkový	k2eAgFnSc2d1	mozková
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
obtíže	obtíž	k1gFnPc4	obtíž
s	s	k7c7	s
mluvením	mluvení	k1gNnSc7	mluvení
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
dále	daleko	k6eAd2	daleko
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
středisku	středisko	k1gNnSc6	středisko
Desert	desert	k1gInSc1	desert
Region	region	k1gInSc4	region
v	v	k7c4	v
Palm	Palm	k1gInSc4	Palm
Springs	Springsa	k1gFnPc2	Springsa
na	na	k7c4	na
dýchací	dýchací	k2eAgFnPc4d1	dýchací
obtíže	obtíž	k1gFnPc4	obtíž
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
mrtvicí	mrtvice	k1gFnSc7	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Schlesingerovými	Schlesingerův	k2eAgMnPc7d1	Schlesingerův
pozůstalými	pozůstalý	k1gMnPc7	pozůstalý
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Roger	Roger	k1gMnSc1	Roger
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Hilary	Hilara	k1gFnSc2	Hilara
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
Příští	příští	k2eAgFnSc1d1	příští
správná	správný	k2eAgFnSc1d1	správná
věc	věc	k1gFnSc1	věc
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Next	Next	k1gMnSc1	Next
Best	Best	k1gMnSc1	Best
Thing	Thing	k1gMnSc1	Thing
<g/>
)	)	kIx)	)
–	–	k?	–
poslední	poslední	k2eAgInSc1d1	poslední
režijní	režijní	k2eAgInSc1d1	režijní
počin	počin	k1gInSc1	počin
1997	[number]	k4	1997
Oholený	oholený	k2eAgMnSc1d1	oholený
klenotník	klenotník	k1gMnSc1	klenotník
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Tale	Tal	k1gFnSc2	Tal
of	of	k?	of
Sweeney	Sweenea	k1gFnSc2	Sweenea
Todd	Todd	k1gMnSc1	Todd
<g/>
)	)	kIx)	)
–	–	k?	–
TV	TV	kA	TV
film	film	k1gInSc4	film
1996	[number]	k4	1996
Oko	oko	k1gNnSc1	oko
za	za	k7c4	za
oko	oko	k1gNnSc4	oko
(	(	kIx(	(
<g/>
An	An	k1gFnSc1	An
<g />
.	.	kIx.	.
</s>
<s>
Eye	Eye	k?	Eye
for	forum	k1gNnPc2	forum
an	an	k?	an
Eye	Eye	k1gFnSc2	Eye
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Farma	farma	k1gFnSc1	farma
Cold	Cold	k1gMnSc1	Cold
Comfort	Comfort	k1gInSc1	Comfort
/	/	kIx~	/
Farma	farma	k1gFnSc1	farma
chabé	chabý	k2eAgFnSc2d1	chabá
útěchy	útěcha	k1gFnSc2	útěcha
(	(	kIx(	(
<g/>
Cold	Cold	k1gInSc1	Cold
Comfort	Comfort	k1gInSc1	Comfort
Farm	Farm	k1gInSc4	Farm
<g/>
)	)	kIx)	)
–	–	k?	–
TV	TV	kA	TV
film	film	k1gInSc4	film
1993	[number]	k4	1993
Nevinný	vinný	k2eNgInSc4d1	nevinný
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Innocent	Innocent	k1gMnSc1	Innocent
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
A	a	k8xC	a
Question	Question	k1gInSc1	Question
of	of	k?	of
Attribution	Attribution	k1gInSc1	Attribution
–	–	k?	–
TV	TV	kA	TV
film	film	k1gInSc4	film
1990	[number]	k4	1990
Psychopat	psychopat	k1gMnSc1	psychopat
ze	z	k7c2	z
San	San	k1gFnSc2	San
Francisca	Francisca	k1gMnSc1	Francisca
(	(	kIx(	(
<g/>
Pacific	Pacific	k1gMnSc1	Pacific
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1988	[number]	k4	1988
Madame	madame	k1gFnSc1	madame
Sousatzka	Sousatzka	k1gFnSc1	Sousatzka
1987	[number]	k4	1987
Temné	temný	k2eAgFnPc1d1	temná
síly	síla	k1gFnPc1	síla
/	/	kIx~	/
Síly	síl	k1gInPc1	síl
temnot	temnota	k1gFnPc2	temnota
/	/	kIx~	/
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
věří	věřit	k5eAaImIp3nS	věřit
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Believers	Believersa	k1gFnPc2	Believersa
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
Dravec	dravec	k1gMnSc1	dravec
a	a	k8xC	a
feťák	feťák	k?	feťák
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Falcon	Falcon	k1gMnSc1	Falcon
And	Anda	k1gFnPc2	Anda
The	The	k1gMnSc1	The
Snowman	Snowman	k1gMnSc1	Snowman
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
An	An	k1gMnSc1	An
Englishman	Englishman	k1gMnSc1	Englishman
Abroad	Abroad	k1gInSc1	Abroad
–	–	k?	–
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc2	inscenace
1983	[number]	k4	1983
Oddělené	oddělený	k2eAgInPc4d1	oddělený
stoly	stol	k1gInPc4	stol
(	(	kIx(	(
<g/>
Separate	Separat	k1gInSc5	Separat
Tables	Tablesa	k1gFnPc2	Tablesa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc2	inscenace
1981	[number]	k4	1981
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
dálnice	dálnice	k1gFnSc1	dálnice
/	/	kIx~	/
Dálnice	dálnice	k1gFnSc1	dálnice
do	do	k7c2	do
Zapadákova	Zapadákov	k1gInSc2	Zapadákov
(	(	kIx(	(
<g/>
Honky	Honka	k1gFnPc1	Honka
Tonk	Tonk	k1gInSc4	Tonk
Freeway	Freewaa	k1gFnSc2	Freewaa
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
Amíci	Amíce	k1gFnSc4	Amíce
(	(	kIx(	(
<g/>
Yanks	Yanks	k1gInSc1	Yanks
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Maratónec	maratónec	k1gMnSc1	maratónec
(	(	kIx(	(
<g/>
Marathon	Marathon	k1gMnSc1	Marathon
Man	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
únor	únor	k1gInSc1	únor
1979	[number]	k4	1979
1975	[number]	k4	1975
Den	dna	k1gFnPc2	dna
kobylek	kobylka	k1gFnPc2	kobylka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Day	Day	k1gFnSc2	Day
of	of	k?	of
the	the	k?	the
Locust	Locust	k1gInSc1	Locust
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
červen	červen	k1gInSc1	červen
1978	[number]	k4	1978
1973	[number]	k4	1973
Viděno	vidět	k5eAaImNgNnS	vidět
osmi	osm	k4xCc2	osm
(	(	kIx(	(
<g/>
Visions	Visions	k1gInSc1	Visions
of	of	k?	of
Eight	Eight	k1gInSc1	Eight
<g/>
)	)	kIx)	)
–	–	k?	–
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
červen	červen	k1gInSc1	červen
1975	[number]	k4	1975
1971	[number]	k4	1971
Mizerná	mizerný	k2eAgFnSc1d1	mizerná
neděle	neděle	k1gFnSc1	neděle
(	(	kIx(	(
<g/>
Sunday	Sunda	k2eAgInPc1d1	Sunda
Bloody	Blood	k1gInPc1	Blood
Sunday	Sundaa	k1gFnSc2	Sundaa
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
Půlnoční	půlnoční	k1gFnSc1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
(	(	kIx(	(
<g/>
Midnight	Midnight	k1gMnSc1	Midnight
Cowboy	Cowboa	k1gFnSc2	Cowboa
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1974	[number]	k4	1974
1967	[number]	k4	1967
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hlučícího	hlučící	k2eAgInSc2d1	hlučící
davu	dav	k1gInSc2	dav
(	(	kIx(	(
<g/>
Far	fara	k1gFnPc2	fara
from	from	k1gInSc1	from
the	the	k?	the
Madding	Madding	k1gInSc1	Madding
Crowd	Crowd	k1gInSc1	Crowd
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
Drahoušek	drahoušek	k1gMnSc1	drahoušek
(	(	kIx(	(
<g/>
Darling	Darling	k1gInSc1	Darling
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
Billy	Bill	k1gMnPc4	Bill
lhář	lhář	k1gMnSc1	lhář
(	(	kIx(	(
<g/>
Billy	Bill	k1gMnPc4	Bill
Liar	Liar	k1gInSc4	Liar
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
Takové	takový	k3xDgNnSc1	takový
milování	milování	k1gNnSc1	milování
(	(	kIx(	(
<g/>
A	a	k9	a
Kind	Kind	k1gInSc1	Kind
of	of	k?	of
Loving	Loving	k1gInSc1	Loving
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgFnSc1	první
režie	režie	k1gFnSc1	režie
hraného	hraný	k2eAgInSc2d1	hraný
filmu	film	k1gInSc2	film
1961	[number]	k4	1961
Nádraží	nádraží	k1gNnSc2	nádraží
(	(	kIx(	(
<g/>
Terminus	terminus	k1gInSc1	terminus
<g/>
)	)	kIx)	)
–	–	k?	–
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
1965	[number]	k4	1965
Cena	cena	k1gFnSc1	cena
newyorských	newyorský	k2eAgMnPc2d1	newyorský
kritiků	kritik	k1gMnPc2	kritik
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
1970	[number]	k4	1970
Oscar	Oscara	k1gFnPc2	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
za	za	k7c4	za
film	film	k1gInSc4	film
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
1970	[number]	k4	1970
komandér	komandér	k1gMnSc1	komandér
Řádu	řád	k1gInSc2	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
přínos	přínos	k1gInSc1	přínos
pro	pro	k7c4	pro
film	film	k1gInSc4	film
1970	[number]	k4	1970
Cena	cena	k1gFnSc1	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
za	za	k7c4	za
film	film	k1gInSc4	film
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
kovboj	kovboj	k1gMnSc1	kovboj
(	(	kIx(	(
<g/>
i	i	k9	i
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
1972	[number]	k4	1972
Cena	cena	k1gFnSc1	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
za	za	k7c4	za
film	film	k1gInSc4	film
Mizerná	mizerný	k2eAgFnSc1d1	mizerná
neděle	neděle	k1gFnSc1	neděle
(	(	kIx(	(
<g/>
i	i	k9	i
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c4	na
Balliol	Balliol	k1gInSc4	Balliol
College	Colleg	k1gFnSc2	Colleg
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
2003	[number]	k4	2003
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
v	v	k7c4	v
Palm	Palm	k1gInSc4	Palm
Springs	Springs	k1gInSc1	Springs
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgInS	získat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
třikrát	třikrát	k6eAd1	třikrát
nominován	nominován	k2eAgInSc1d1	nominován
<g/>
:	:	kIx,	:
za	za	k7c4	za
Drahouška	drahoušek	k1gMnSc4	drahoušek
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Půlnočního	půlnoční	k2eAgMnSc4d1	půlnoční
kovboje	kovboj	k1gMnSc4	kovboj
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maratónce	maratónka	k1gFnSc6	maratónka
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
