<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Maria	Mario	k1gMnSc2	Mario
Rohrer	Rohrer	k1gMnSc1	Rohrer
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1838	[number]	k4	1838
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
;	;	kIx,	;
poslanec	poslanec	k1gMnSc1	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
majitel	majitel	k1gMnSc1	majitel
významné	významný	k2eAgFnSc2d1	významná
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
majitel	majitel	k1gMnSc1	majitel
významné	významný	k2eAgFnSc2d1	významná
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc4	vedení
spolku	spolek	k1gInSc2	spolek
Německý	německý	k2eAgInSc1d1	německý
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
též	též	k9	též
ve	v	k7c6	v
spolku	spolek	k1gInSc6	spolek
Deutsche	Deutsch	k1gFnSc2	Deutsch
Lesehalle	Lesehalle	k1gFnSc2	Lesehalle
<g/>
,	,	kIx,	,
Brünner	Brünner	k1gMnSc1	Brünner
Deutscher	Deutschra	k1gFnPc2	Deutschra
Turnverein	Turnverein	k1gMnSc1	Turnverein
nebo	nebo	k8xC	nebo
Brünner	Brünner	k1gMnSc1	Brünner
Freiwillige	Freiwillige	k1gFnPc2	Freiwillige
Rettungsgesellschaft	Rettungsgesellschaft	k1gMnSc1	Rettungsgesellschaft
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
viceprezident	viceprezident	k1gMnSc1	viceprezident
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1	Moravskoslezská
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
byl	být	k5eAaImAgInS	být
náměstkem	náměstek	k1gMnSc7	náměstek
starosty	starosta	k1gMnSc2	starosta
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
okresního	okresní	k2eAgInSc2d1	okresní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
město	město	k1gNnSc4	město
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
obecním	obecní	k2eAgInSc6d1	obecní
výboru	výbor	k1gInSc6	výbor
poprvé	poprvé	k6eAd1	poprvé
usedl	usednout	k5eAaPmAgMnS	usednout
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
<g/>
Získal	získat	k5eAaPmAgInS	získat
rytířský	rytířský	k2eAgInSc4d1	rytířský
titul	titul	k1gInSc4	titul
von	von	k1gInSc4	von
Rohrer	Rohrra	k1gFnPc2	Rohrra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
mu	on	k3xPp3gMnSc3	on
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
udělilo	udělit	k5eAaPmAgNnS	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
nesla	nést	k5eAaImAgFnS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
Rudolf-	Rudolf-	k1gMnSc2	Rudolf-
<g/>
M.	M.	kA	M.
<g/>
-Rohrer-Gasse	-Rohrer-Gass	k1gMnSc2	-Rohrer-Gass
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
ulice	ulice	k1gFnSc1	ulice
Na	na	k7c6	na
Baštách	bašta	k1gFnPc6	bašta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
získal	získat	k5eAaPmAgInS	získat
rytířský	rytířský	k2eAgInSc1d1	rytířský
Řád	řád	k1gInSc1	řád
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
získal	získat	k5eAaPmAgInS	získat
Řád	řád	k1gInSc1	řád
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
i	i	k8xC	i
Řád	řád	k1gInSc1	řád
železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
Moravský	moravský	k2eAgInSc4d1	moravský
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
kurii	kurie	k1gFnSc4	kurie
obchodních	obchodní	k2eAgFnPc2d1	obchodní
a	a	k8xC	a
živnostenských	živnostenský	k2eAgFnPc2d1	Živnostenská
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zde	zde	k6eAd1	zde
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
a	a	k8xC	a
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
německý	německý	k2eAgMnSc1d1	německý
liberál	liberál	k1gMnSc1	liberál
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Německá	německý	k2eAgFnSc1d1	německá
pokroková	pokrokový	k2eAgFnSc1d1	pokroková
strana	strana	k1gFnSc1	strana
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
ústavověrný	ústavověrný	k2eAgInSc4d1	ústavověrný
politický	politický	k2eAgInSc4d1	politický
proud	proud	k1gInSc4	proud
s	s	k7c7	s
liberální	liberální	k2eAgFnSc7d1	liberální
a	a	k8xC	a
centralistickou	centralistický	k2eAgFnSc7d1	centralistická
orientací	orientace	k1gFnSc7	orientace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
stejný	stejný	k2eAgInSc4d1	stejný
politický	politický	k2eAgInSc4d1	politický
tábor	tábor	k1gInSc4	tábor
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
a	a	k8xC	a
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1914	[number]	k4	1914
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
77	[number]	k4	77
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rohrer	Rohrer	k1gMnSc1	Rohrer
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
rodinném	rodinný	k2eAgNnSc6d1	rodinné
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vnukové	vnuk	k1gMnPc1	vnuk
Rudolf	Rudolf	k1gMnSc1	Rudolf
Maria	Mario	k1gMnSc2	Mario
Ernst	Ernst	k1gMnSc1	Ernst
Rohrer	Rohrer	k1gMnSc1	Rohrer
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Karl	Karl	k1gMnSc1	Karl
Ernst	Ernst	k1gMnSc1	Ernst
Rohrer	Rohrer	k1gMnSc1	Rohrer
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
