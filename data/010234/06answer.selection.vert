<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Maria	Mario	k1gMnSc2	Mario
Rohrer	Rohrer	k1gMnSc1	Rohrer
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1838	[number]	k4	1838
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
;	;	kIx,	;
poslanec	poslanec	k1gMnSc1	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
majitel	majitel	k1gMnSc1	majitel
významné	významný	k2eAgFnSc2d1	významná
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
