<s>
První	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
písně	píseň	k1gFnSc2	píseň
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc4	domov
můj	můj	k3xOp1gInSc4	můj
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
Československa	Československo	k1gNnSc2	Československo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
však	však	k9	však
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
i	i	k9	i
za	za	k7c4	za
hymnu	hymna	k1gFnSc4	hymna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvnitř	uvnitř	k7c2	uvnitř
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
tvořila	tvořit	k5eAaImAgFnS	tvořit
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
československé	československý	k2eAgFnSc2d1	Československá
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
následována	následován	k2eAgFnSc1d1	následována
první	první	k4xOgFnSc7	první
slokou	sloka	k1gFnSc7	sloka
nynější	nynější	k2eAgFnSc2d1	nynější
hymny	hymna	k1gFnSc2	hymna
slovenské	slovenský	k2eAgNnSc1d1	slovenské
(	(	kIx(	(
<g/>
Nad	nad	k7c7	nad
Tatrou	Tatra	k1gFnSc7	Tatra
sa	sa	k?	sa
blýska	blýska	k1gFnSc1	blýska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
mezi	mezi	k7c4	mezi
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
§	§	k?	§
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
užívat	užívat	k5eAaImF	užívat
jen	jen	k9	jen
vhodným	vhodný	k2eAgInSc7d1	vhodný
a	a	k8xC	a
důstojným	důstojný	k2eAgInSc7d1	důstojný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc4d1	státní
hymnu	hymna	k1gFnSc4	hymna
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
§	§	k?	§
12	[number]	k4	12
zákona	zákon	k1gInSc2	zákon
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
hrát	hrát	k5eAaImF	hrát
i	i	k8xC	i
zpívat	zpívat	k5eAaImF	zpívat
při	při	k7c6	při
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
a	a	k8xC	a
při	při	k7c6	při
jiných	jiný	k2eAgFnPc6d1	jiná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	tyla	k1gFnSc1	tyla
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
aneb	aneb	k?	aneb
Žádný	žádný	k3yNgInSc4	žádný
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
rvačka	rvačka	k1gFnSc1	rvačka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedené	uvedený	k2eAgFnPc1d1	uvedená
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
složil	složit	k5eAaPmAgMnS	složit
František	František	k1gMnSc1	František
Škroup	Škroup	k1gMnSc1	Škroup
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
interpretem	interpret	k1gMnSc7	interpret
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Karel	Karel	k1gMnSc1	Karel
Strakatý	strakatý	k2eAgMnSc1d1	strakatý
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
zlidověla	zlidovět	k5eAaPmAgFnS	zlidovět
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
postavení	postavení	k1gNnSc4	postavení
národní	národní	k2eAgFnSc2d1	národní
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
první	první	k4xOgInPc1	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
jako	jako	k9	jako
hymnu	hymna	k1gFnSc4	hymna
divadelní	divadelní	k2eAgInSc4d1	divadelní
šlágr	šlágr	k1gInSc4	šlágr
<g/>
.	.	kIx.	.
</s>
<s>
Hymnu	hymna	k1gFnSc4	hymna
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
oproti	oproti	k7c3	oproti
originálu	originál	k1gInSc3	originál
lehce	lehko	k6eAd1	lehko
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
<g/>
)	)	kIx)	)
sloka	sloka	k1gFnSc1	sloka
písně	píseň	k1gFnSc2	píseň
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
neznámá	známý	k2eNgNnPc1d1	neznámé
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
parodii	parodie	k1gFnSc4	parodie
v	v	k7c6	v
Pravém	pravý	k2eAgInSc6d1	pravý
výletu	výlet	k1gInSc6	výlet
pana	pan	k1gMnSc2	pan
Broučka	Brouček	k1gMnSc2	Brouček
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
od	od	k7c2	od
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hudební	hudební	k2eAgInSc4d1	hudební
motiv	motiv	k1gInSc4	motiv
písně	píseň	k1gFnSc2	píseň
Kde	kde	k6eAd1	kde
<g />
.	.	kIx.	.
</s>
<s>
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc4	můj
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
v	v	k7c6	v
Škroupově	Škroupův	k2eAgFnSc6d1	Škroupova
Chrudimské	chrudimský	k2eAgFnSc6d1	Chrudimská
předehře	předehra	k1gFnSc6	předehra
složené	složený	k2eAgFnSc6d1	složená
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Chrudimi	Chrudim	k1gFnSc6	Chrudim
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
Můj	můj	k1gMnSc1	můj
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
složil	složit	k5eAaPmAgMnS	složit
jako	jako	k8xS	jako
předehru	předehra	k1gFnSc4	předehra
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Šamberka	Šamberka	k1gFnSc1	Šamberka
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c6	v
textu	text	k1gInSc6	text
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c6	o
Čechách	Čechy	k1gFnPc6	Čechy
i	i	k8xC	i
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
Čechy	Čechy	k1gFnPc1	Čechy
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgFnP	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
Česká	český	k2eAgFnSc1d1	Česká
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
království	království	k1gNnSc1	království
České	český	k2eAgFnSc2d1	Česká
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
nápěv	nápěv	k1gInSc1	nápěv
hymny	hymna	k1gFnSc2	hymna
však	však	k9	však
je	on	k3xPp3gMnPc4	on
starší	starší	k1gMnPc4	starší
než	než	k8xS	než
Škroup	Škroup	k1gInSc4	Škroup
a	a	k8xC	a
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
jej	on	k3xPp3gNnSc4	on
u	u	k7c2	u
Mozarta	Mozart	k1gMnSc2	Mozart
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
větě	věta	k1gFnSc6	věta
jeho	jeho	k3xOp3gFnPc4	jeho
Koncertantní	koncertantní	k2eAgFnPc4d1	koncertantní
symfonie	symfonie	k1gFnPc4	symfonie
Es	es	k1gNnSc2	es
dur	dur	k1gNnSc2	dur
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc4	fagot
a	a	k8xC	a
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
K	k	k7c3	k
297	[number]	k4	297
b.	b.	k?	b.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgFnP	být
nahrány	nahrát	k5eAaPmNgFnP	nahrát
čtyři	čtyři	k4xCgFnPc1	čtyři
oficiální	oficiální	k2eAgFnPc1d1	oficiální
verze	verze	k1gFnPc1	verze
české	český	k2eAgFnSc2d1	Česká
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Ženskou	ženský	k2eAgFnSc4d1	ženská
sólovou	sólový	k2eAgFnSc4d1	sólová
verzi	verze	k1gFnSc4	verze
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kněžíková	Kněžíková	k1gFnSc1	Kněžíková
<g/>
.	.	kIx.	.
</s>
<s>
Mužskou	mužský	k2eAgFnSc4d1	mužská
pak	pak	k6eAd1	pak
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Adam	Adam	k1gMnSc1	Adam
Plachetka	plachetka	k1gFnSc1	plachetka
<g/>
.	.	kIx.	.
</s>
<s>
Sborovou	sborový	k2eAgFnSc4d1	sborová
verzi	verze	k1gFnSc4	verze
nahrál	nahrát	k5eAaBmAgInS	nahrát
Sbor	sbor	k1gInSc1	sbor
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
verzi	verze	k1gFnSc4	verze
nahrál	nahrát	k5eAaBmAgInS	nahrát
Orchestr	orchestr	k1gInSc1	orchestr
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hymna	hymna	k1gFnSc1	hymna
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
i	i	k8xC	i
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
státní	státní	k2eAgFnSc2d1	státní
hymny	hymna	k1gFnSc2	hymna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Kde	kde	k9	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc4	můj
někdy	někdy	k6eAd1	někdy
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
tázací	tázací	k2eAgFnSc1d1	tázací
věta	věta	k1gFnSc1	věta
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
s	s	k7c7	s
otazníkem	otazník	k1gInSc7	otazník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
už	už	k6eAd1	už
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
desítiletí	desítiletí	k1gNnPc4	desítiletí
a	a	k8xC	a
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
historického	historický	k2eAgInSc2d1	historický
kontextu	kontext	k1gInSc2	kontext
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
ideový	ideový	k2eAgInSc1d1	ideový
a	a	k8xC	a
jazykový	jazykový	k2eAgInSc1d1	jazykový
rozbor	rozbor	k1gInSc1	rozbor
textu	text	k1gInSc2	text
hymny	hymna	k1gFnSc2	hymna
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neopodstatněné	opodstatněný	k2eNgNnSc1d1	neopodstatněné
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
tázací	tázací	k2eAgFnSc4d1	tázací
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
oznamovací	oznamovací	k2eAgFnSc4d1	oznamovací
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
větu	věta	k1gFnSc4	věta
příslovečnou	příslovečný	k2eAgFnSc4d1	příslovečná
místní	místní	k2eAgFnSc4d1	místní
<g/>
.	.	kIx.	.
</s>
<s>
Psát	psát	k5eAaImF	psát
otazník	otazník	k1gInSc4	otazník
za	za	k7c7	za
názvem	název	k1gInSc7	název
a	a	k8xC	a
prvními	první	k4xOgInPc7	první
verši	verš	k1gInPc7	verš
textu	text	k1gInSc3	text
je	být	k5eAaImIp3nS	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
