<p>
<s>
Eroze	eroze	k1gFnSc1	eroze
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
proces	proces	k1gInSc4	proces
rozrušování	rozrušování	k1gNnSc2	rozrušování
a	a	k8xC	a
transportu	transport	k1gInSc2	transport
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
eroze	eroze	k1gFnSc2	eroze
je	být	k5eAaImIp3nS	být
mechanické	mechanický	k2eAgNnSc1d1	mechanické
působení	působení	k1gNnSc1	působení
pohybujících	pohybující	k2eAgInPc2d1	pohybující
se	se	k3xPyFc4	se
okolních	okolní	k2eAgFnPc2d1	okolní
látek	látka	k1gFnPc2	látka
-	-	kIx~	-
především	především	k9	především
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
proudící	proudící	k2eAgFnPc1d1	proudící
nebo	nebo	k8xC	nebo
vlnící	vlnící	k2eAgFnPc1d1	vlnící
se	se	k3xPyFc4	se
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
zvětralin	zvětralina	k1gFnPc2	zvětralina
a	a	k8xC	a
nezpevněných	zpevněný	k2eNgFnPc2d1	nezpevněná
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Erozí	eroze	k1gFnSc7	eroze
však	však	k9	však
není	být	k5eNaImIp3nS	být
chemické	chemický	k2eAgNnSc1d1	chemické
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
koroze	koroze	k1gFnSc1	koroze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eroze	eroze	k1gFnSc1	eroze
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
existujícím	existující	k2eAgInSc7d1	existující
přírodním	přírodní	k2eAgInSc7d1	přírodní
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
stupeň	stupeň	k1gInSc4	stupeň
eroze	eroze	k1gFnSc2	eroze
jako	jako	k8xS	jako
přírodního	přírodní	k2eAgInSc2d1	přírodní
jevu	jev	k1gInSc2	jev
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
ekosystémům	ekosystém	k1gInPc3	ekosystém
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
však	však	k9	však
eroze	eroze	k1gFnSc1	eroze
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zesilována	zesilovat	k5eAaImNgFnS	zesilovat
činností	činnost	k1gFnSc7	činnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
odnosem	odnos	k1gInSc7	odnos
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
termínu	termín	k1gInSc2	termín
eroze	eroze	k1gFnSc1	eroze
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc4	první
známé	známý	k2eAgNnSc4d1	známé
písemné	písemný	k2eAgNnSc4d1	písemné
použití	použití	k1gNnSc4	použití
termínu	termín	k1gInSc2	termín
eroze	eroze	k1gFnSc2	eroze
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
překladu	překlad	k1gInSc2	překlad
lékařského	lékařský	k2eAgInSc2d1	lékařský
textu	text	k1gInSc2	text
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Chauliace	Chauliace	k1gFnSc1	Chauliace
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Questyonary	Questyonara	k1gFnSc2	Questyonara
of	of	k?	of
Cyrurygens	Cyrurygens	k1gInSc1	Cyrurygens
<g/>
"	"	kIx"	"
od	od	k7c2	od
Roberta	Robert	k1gMnSc2	Robert
Coplanda	Coplando	k1gNnSc2	Coplando
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
<s>
Copland	Copland	k1gInSc1	Copland
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
ohnisek	ohnisko	k1gNnPc2	ohnisko
rozvinutých	rozvinutý	k2eAgNnPc2d1	rozvinuté
v	v	k7c6	v
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
eroze	eroze	k1gFnSc2	eroze
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
mimo	mimo	k7c4	mimo
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
<g/>
"	"	kIx"	"
jej	on	k3xPp3gNnSc4	on
použil	použít	k5eAaPmAgMnS	použít
Oliver	Oliver	k1gMnSc1	Oliver
Goldsmith	Goldsmith	k1gMnSc1	Goldsmith
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
píše	psát	k5eAaImIp3nS	psát
o	o	k7c4	o
erozi	eroze	k1gFnSc4	eroze
země	zem	k1gFnSc2	zem
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnSc2	příčina
eroze	eroze	k1gFnSc2	eroze
==	==	k?	==
</s>
</p>
<p>
<s>
Eroze	eroze	k1gFnSc1	eroze
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
gravitací	gravitace	k1gFnSc7	gravitace
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
dalších	další	k2eAgInPc2d1	další
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc4	sklon
svahu	svah	k1gInSc2	svah
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
pokryvu	pokryv	k1gInSc2	pokryv
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
využívání	využívání	k1gNnSc2	využívání
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
proudící	proudící	k2eAgFnSc3d1	proudící
hmotě	hmota	k1gFnSc3	hmota
m	m	kA	m
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
(	(	kIx(	(
<g/>
m.	m.	k?	m.
<g/>
v2	v2	k4	v2
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
eroze	eroze	k1gFnSc2	eroze
představuje	představovat	k5eAaImIp3nS	představovat
množství	množství	k1gNnSc1	množství
nebo	nebo	k8xC	nebo
mocnost	mocnost	k1gFnSc1	mocnost
materiálu	materiál	k1gInSc2	materiál
přemístěného	přemístěný	k2eAgMnSc4d1	přemístěný
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
překážkou	překážka	k1gFnSc7	překážka
je	být	k5eAaImIp3nS	být
tvrdost	tvrdost	k1gFnSc1	tvrdost
erodované	erodovaný	k2eAgFnSc2d1	erodovaná
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
v	v	k7c6	v
měkčích	měkký	k2eAgInPc6d2	měkčí
je	být	k5eAaImIp3nS	být
rozměrnější	rozměrný	k2eAgFnSc1d2	rozměrnější
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
selektivní	selektivní	k2eAgFnSc4d1	selektivní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
eroze	eroze	k1gFnSc2	eroze
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
intenzita	intenzita	k1gFnSc1	intenzita
eroze	eroze	k1gFnSc1	eroze
však	však	k9	však
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
spolupůsobení	spolupůsobení	k1gNnSc4	spolupůsobení
dalších	další	k2eAgInPc2d1	další
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
pokryv	pokryv	k1gInSc1	pokryv
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vyššímu	vysoký	k2eAgInSc3d2	vyšší
stupni	stupeň	k1gInSc3	stupeň
eroze	eroze	k1gFnSc2	eroze
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
odlesněné	odlesněný	k2eAgInPc1d1	odlesněný
svahy	svah	k1gInPc1	svah
či	či	k8xC	či
"	"	kIx"	"
<g/>
spasené	spasený	k2eAgInPc4d1	spasený
<g/>
"	"	kIx"	"
pozemky	pozemek	k1gInPc4	pozemek
podlehnou	podlehnout	k5eAaPmIp3nP	podlehnout
erozi	eroze	k1gFnSc4	eroze
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Půdy	půda	k1gFnSc2	půda
obsahující	obsahující	k2eAgNnSc4d1	obsahující
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
jílovitých	jílovitý	k2eAgInPc2d1	jílovitý
minerálů	minerál	k1gInPc2	minerál
přijímají	přijímat	k5eAaImIp3nP	přijímat
méně	málo	k6eAd2	málo
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
na	na	k7c6	na
volnějším	volný	k2eAgInSc6d2	volnější
svahu	svah	k1gInSc6	svah
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
narušovány	narušován	k2eAgFnPc1d1	narušována
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
činitelem	činitel	k1gInSc7	činitel
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Erozi	eroze	k1gFnSc4	eroze
půdy	půda	k1gFnSc2	půda
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
spásání	spásání	k1gNnSc1	spásání
travin	travina	k1gFnPc2	travina
<g/>
,	,	kIx,	,
nekontrolovaná	kontrolovaný	k2eNgFnSc1d1	nekontrolovaná
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
obydlí	obydlí	k1gNnSc2	obydlí
a	a	k8xC	a
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
stavba	stavba	k1gFnSc1	stavba
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
narušuje	narušovat	k5eAaImIp3nS	narušovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
říční	říční	k2eAgFnSc4d1	říční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
stékající	stékající	k2eAgFnSc1d1	stékající
po	po	k7c6	po
vozovce	vozovka	k1gFnSc6	vozovka
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
by	by	kYmCp3nS	by
normálně	normálně	k6eAd1	normálně
neproudila	proudit	k5eNaImAgFnS	proudit
a	a	k8xC	a
takto	takto	k6eAd1	takto
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
působící	působící	k2eAgFnSc3d1	působící
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
změna	změna	k1gFnSc1	změna
skladby	skladba	k1gFnSc2	skladba
pokryvu	pokryv	k1gInSc2	pokryv
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
erozi	eroze	k1gFnSc3	eroze
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
vegetace	vegetace	k1gFnSc2	vegetace
mají	mít	k5eAaImIp3nP	mít
odlišný	odlišný	k2eAgInSc4d1	odlišný
účinek	účinek	k1gInSc4	účinek
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
infiltrace	infiltrace	k1gFnSc2	infiltrace
deště	dešť	k1gInSc2	dešť
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
odlesňování	odlesňování	k1gNnSc2	odlesňování
či	či	k8xC	či
vypalování	vypalování	k1gNnSc2	vypalování
tropických	tropický	k2eAgInPc2d1	tropický
pralesů	prales	k1gInPc2	prales
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
eroze	eroze	k1gFnSc1	eroze
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc4	vznik
plošin	plošina	k1gFnPc2	plošina
se	s	k7c7	s
zemědělsky	zemědělsky	k6eAd1	zemědělsky
nevyužitelnou	využitelný	k2eNgFnSc7d1	nevyužitelná
půdou	půda	k1gFnSc7	půda
a	a	k8xC	a
četnými	četný	k2eAgFnPc7d1	četná
erozními	erozní	k2eAgFnPc7d1	erozní
roklemi	rokle	k1gFnPc7	rokle
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
ekonomicky	ekonomicky	k6eAd1	ekonomicky
chudých	chudý	k2eAgFnPc6d1	chudá
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
střetává	střetávat	k5eAaImIp3nS	střetávat
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
obživy	obživa	k1gFnSc2	obživa
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
potřebujících	potřebující	k2eAgFnPc2d1	potřebující
nekrytou	krytý	k2eNgFnSc4d1	nekrytá
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kultivaci	kultivace	k1gFnSc4	kultivace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
využívání	využívání	k1gNnSc1	využívání
pluhu	pluh	k1gInSc2	pluh
k	k	k7c3	k
obdělávání	obdělávání	k1gNnSc3	obdělávání
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
eroze	eroze	k1gFnSc1	eroze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
eroze	eroze	k1gFnSc1	eroze
===	===	k?	===
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
gravitace	gravitace	k1gFnSc2	gravitace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
svahové	svahový	k2eAgInPc4d1	svahový
pohyby	pohyb	k1gInPc4	pohyb
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nepřerušeně	přerušeně	k6eNd1	přerušeně
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
zvolna	zvolna	k6eAd1	zvolna
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
náhlých	náhlý	k2eAgInPc2d1	náhlý
pohybům	pohyb	k1gInPc3	pohyb
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
katastrofálními	katastrofální	k2eAgInPc7d1	katastrofální
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Pomalou	pomalý	k2eAgFnSc7d1	pomalá
erozí	eroze	k1gFnSc7	eroze
vznikají	vznikat	k5eAaImIp3nP	vznikat
suťové	suťový	k2eAgInPc1d1	suťový
kužele	kužel	k1gInSc2	kužel
na	na	k7c6	na
úpatích	úpatí	k1gNnPc6	úpatí
svahů	svah	k1gInPc2	svah
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
velehorský	velehorský	k2eAgInSc4d1	velehorský
reliéf	reliéf	k1gInSc4	reliéf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgFnPc1d1	vodní
eroze	eroze	k1gFnPc1	eroze
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
je	být	k5eAaImIp3nS	být
eroze	eroze	k1gFnSc1	eroze
způsobená	způsobený	k2eAgFnSc1d1	způsobená
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oddělování	oddělování	k1gNnSc3	oddělování
malých	malý	k2eAgFnPc2d1	malá
půdních	půdní	k2eAgFnPc2d1	půdní
částic	částice	k1gFnPc2	částice
dopadem	dopad	k1gInSc7	dopad
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
převýší	převýšit	k5eAaPmIp3nS	převýšit
infiltraci	infiltrace	k1gFnSc4	infiltrace
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
splachu	splach	k1gInSc3	splach
částic	částice	k1gFnPc2	částice
proudící	proudící	k2eAgFnSc7d1	proudící
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
eroze	eroze	k1gFnSc1	eroze
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horních	horní	k2eAgInPc6d1	horní
tocích	tok	k1gInPc6	tok
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnPc1	údolí
mají	mít	k5eAaImIp3nP	mít
typický	typický	k2eAgInSc4d1	typický
průřez	průřez	k1gInSc4	průřez
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
V	V	kA	V
a	a	k8xC	a
spád	spád	k1gInSc4	spád
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značný	značný	k2eAgInSc1d1	značný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
erozní	erozní	k2eAgInPc4d1	erozní
základy	základ	k1gInPc4	základ
začne	začít	k5eAaPmIp3nS	začít
převažovat	převažovat	k5eAaImF	převažovat
boční	boční	k2eAgInSc4d1	boční
(	(	kIx(	(
<g/>
laterární	laterární	k2eAgInSc4d1	laterární
<g/>
)	)	kIx)	)
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
mířící	mířící	k2eAgFnSc1d1	mířící
vodorovně	vodorovně	k6eAd1	vodorovně
na	na	k7c4	na
některou	některý	k3yIgFnSc4	některý
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
údolí	údolí	k1gNnSc2	údolí
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
úzká	úzký	k2eAgFnSc1d1	úzká
povodňová	povodňový	k2eAgFnSc1d1	povodňová
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
záplav	záplava	k1gFnPc2	záplava
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
eroze	eroze	k1gFnSc2	eroze
při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc3	zvýšení
množství	množství	k1gNnSc2	množství
protékané	protékaný	k2eAgFnSc2d1	protékaná
vody	voda	k1gFnSc2	voda
i	i	k8xC	i
unášeného	unášený	k2eAgInSc2d1	unášený
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horních	horní	k2eAgInPc6d1	horní
tocích	tok	k1gInPc6	tok
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
erozí	eroze	k1gFnSc7	eroze
<g/>
,	,	kIx,	,
prohlubováním	prohlubování	k1gNnSc7	prohlubování
koryta	koryto	k1gNnSc2	koryto
zpětným	zpětný	k2eAgInSc7d1	zpětný
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodopádů	vodopád	k1gInPc2	vodopád
se	se	k3xPyFc4	se
zpětná	zpětný	k2eAgFnSc1d1	zpětná
eroze	eroze	k1gFnSc1	eroze
projevuje	projevovat	k5eAaImIp3nS	projevovat
ústupem	ústup	k1gInSc7	ústup
prahu	práh	k1gInSc2	práh
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k8xC	i
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
říčnímu	říční	k2eAgNnSc3d1	říční
pirátství	pirátství	k1gNnSc3	pirátství
<g/>
.	.	kIx.	.
</s>
<s>
Vířivým	vířivý	k2eAgInSc7d1	vířivý
pohybem	pohyb	k1gInSc7	pohyb
vody	voda	k1gFnSc2	voda
vznikají	vznikat	k5eAaImIp3nP	vznikat
kotlovité	kotlovitý	k2eAgFnPc1d1	kotlovitá
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc1d1	zvaný
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Erodující	erodující	k2eAgInSc1d1	erodující
proud	proud	k1gInSc1	proud
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
koncové	koncový	k2eAgInPc4d1	koncový
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
svrchní	svrchní	k2eAgInPc1d1	svrchní
(	(	kIx(	(
<g/>
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
pramen	pramen	k1gInSc1	pramen
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodní	spodní	k2eAgFnPc1d1	spodní
(	(	kIx(	(
<g/>
ústí	ústit	k5eAaImIp3nS	ústit
<g/>
)	)	kIx)	)
výmolnou	výmolný	k2eAgFnSc4d1	výmolný
základnu	základna	k1gFnSc4	základna
(	(	kIx(	(
<g/>
erozní	erozní	k2eAgFnSc4d1	erozní
bázi	báze	k1gFnSc4	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
eroze	eroze	k1gFnSc1	eroze
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životě	život	k1gInSc6	život
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
erozní	erozní	k2eAgNnSc4d1	erozní
období	období	k1gNnSc4	období
s	s	k7c7	s
obdobími	období	k1gNnPc7	období
akumulačními	akumulační	k2eAgNnPc7d1	akumulační
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
střídání	střídání	k1gNnSc2	střídání
při	při	k7c6	při
spolupůsobení	spolupůsobení	k1gNnSc6	spolupůsobení
tektonických	tektonický	k2eAgInPc2d1	tektonický
<g/>
,	,	kIx,	,
klimatických	klimatický	k2eAgInPc2d1	klimatický
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
činitelů	činitel	k1gInPc2	činitel
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
říční	říční	k2eAgFnPc1d1	říční
terasy	terasa	k1gFnPc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Eroze	eroze	k1gFnSc1	eroze
též	též	k9	též
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
změnách	změna	k1gFnPc6	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
pohybech	pohyb	k1gInPc6	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
erozi	eroze	k1gFnSc4	eroze
neovlivňovaly	ovlivňovat	k5eNaImAgInP	ovlivňovat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	kYmCp3nS	by
k	k	k7c3	k
zarovnání	zarovnání	k1gNnSc3	zarovnání
souší	souš	k1gFnPc2	souš
asi	asi	k9	asi
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
kolem	kolem	k7c2	kolem
250	[number]	k4	250
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
K	k	k7c3	k
erozi	eroze	k1gFnSc3	eroze
způsobené	způsobený	k2eAgFnSc2d1	způsobená
vodou	voda	k1gFnSc7	voda
řadíme	řadit	k5eAaImIp1nP	řadit
i	i	k9	i
působení	působení	k1gNnSc4	působení
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
energie	energie	k1gFnSc1	energie
narážejících	narážející	k2eAgFnPc2d1	narážející
vln	vlna	k1gFnPc2	vlna
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
"	"	kIx"	"
<g/>
odlamování	odlamování	k1gNnSc4	odlamování
<g/>
"	"	kIx"	"
různě	různě	k6eAd1	různě
velkých	velký	k2eAgFnPc2d1	velká
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
neseny	nesen	k2eAgInPc1d1	nesen
pobřežními	pobřežní	k2eAgInPc7d1	pobřežní
proudy	proud	k1gInPc7	proud
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
účinnost	účinnost	k1gFnSc4	účinnost
eroze	eroze	k1gFnSc2	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
množství	množství	k1gNnSc1	množství
neseného	nesený	k2eAgInSc2d1	nesený
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
množství	množství	k1gNnSc1	množství
oderodovaného	oderodovaný	k2eAgInSc2d1	oderodovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
převažuje	převažovat	k5eAaImIp3nS	převažovat
unášený	unášený	k2eAgInSc4d1	unášený
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
písečné	písečný	k2eAgInPc1d1	písečný
či	či	k8xC	či
štěrkové	štěrkový	k2eAgInPc1d1	štěrkový
břehy	břeh	k1gInPc1	břeh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vlivem	vliv	k1gInSc7	vliv
proudění	proudění	k1gNnSc2	proudění
migrovat	migrovat	k5eAaImF	migrovat
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
střídavě	střídavě	k6eAd1	střídavě
jej	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Větrná	větrný	k2eAgFnSc1d1	větrná
eroze	eroze	k1gFnSc1	eroze
===	===	k?	===
</s>
</p>
<p>
<s>
Rušivou	rušivý	k2eAgFnSc4d1	rušivá
činnost	činnost	k1gFnSc4	činnost
větru	vítr	k1gInSc2	vítr
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
korazi	koraze	k1gFnSc4	koraze
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
obrus	obrus	k1gInSc4	obrus
třením	tření	k1gNnSc7	tření
větrem	vítr	k1gInSc7	vítr
transportovaného	transportovaný	k2eAgInSc2d1	transportovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
deflaci	deflace	k1gFnSc4	deflace
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
větrný	větrný	k2eAgInSc1d1	větrný
odnos	odnos	k1gInSc1	odnos
sypkého	sypký	k2eAgInSc2d1	sypký
zvětralého	zvětralý	k2eAgInSc2d1	zvětralý
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Koraze	koraze	k1gFnSc1	koraze
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
hrubosti	hrubost	k1gFnSc2	hrubost
unášeného	unášený	k2eAgInSc2d1	unášený
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
dopadajícího	dopadající	k2eAgInSc2d1	dopadající
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
hrance	hranec	k1gInPc1	hranec
<g/>
,	,	kIx,	,
eologlyptolity	eologlyptolit	k1gInPc1	eologlyptolit
<g/>
,	,	kIx,	,
houbovité	houbovitý	k2eAgInPc1d1	houbovitý
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
bašty	bašta	k1gFnPc1	bašta
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgNnPc1d1	skalní
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgInPc1d1	skalní
mosty	most	k1gInPc1	most
apod.	apod.	kA	apod.
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
koraze	koraze	k1gFnSc2	koraze
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc1	synonymum
"	"	kIx"	"
<g/>
abraze	abraze	k1gFnSc1	abraze
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
procesu	proces	k1gInSc2	proces
erodování	erodování	k1gNnSc2	erodování
povrchu	povrch	k1gInSc2	povrch
transportovaným	transportovaný	k2eAgInSc7d1	transportovaný
materiálem	materiál	k1gInSc7	materiál
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
obranným	obranný	k2eAgInSc7d1	obranný
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
vysazování	vysazování	k1gNnSc1	vysazování
větrolamů	větrolam	k1gInPc2	větrolam
a	a	k8xC	a
zvyšování	zvyšování	k1gNnSc2	zvyšování
hustoty	hustota	k1gFnSc2	hustota
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
pokryvu	pokryv	k1gInSc2	pokryv
osazováním	osazování	k1gNnSc7	osazování
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
písků	písek	k1gInPc2	písek
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eroze	eroze	k1gFnSc1	eroze
působením	působení	k1gNnSc7	působení
ledu	led	k1gInSc2	led
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
ledovcových	ledovcový	k2eAgInPc2d1	ledovcový
splazů	splaz	k1gInPc2	splaz
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
k	k	k7c3	k
vylamování	vylamování	k1gNnSc3	vylamování
a	a	k8xC	a
vlečení	vlečení	k1gNnSc3	vlečení
kusů	kus	k1gInPc2	kus
hornin	hornina	k1gFnPc2	hornina
skalního	skalní	k2eAgInSc2d1	skalní
podkladu	podklad	k1gInSc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Pohybem	pohyb	k1gInSc7	pohyb
ledovce	ledovec	k1gInSc2	ledovec
společně	společně	k6eAd1	společně
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
neseného	nesený	k2eAgInSc2d1	nesený
horninového	horninový	k2eAgInSc2d1	horninový
materiálu	materiál	k1gInSc2	materiál
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přemodelování	přemodelování	k1gNnSc3	přemodelování
údolí	údolí	k1gNnSc2	údolí
před	před	k7c7	před
ledovcem	ledovec	k1gInSc7	ledovec
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
tzv.	tzv.	kA	tzv.
trogů	trog	k1gInPc2	trog
<g/>
,	,	kIx,	,
ledovcových	ledovcový	k2eAgNnPc2d1	ledovcové
údolí	údolí	k1gNnPc2	údolí
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U.	U.	kA	U.
K	k	k7c3	k
erozi	eroze	k1gFnSc3	eroze
přispívá	přispívat	k5eAaImIp3nS	přispívat
pak	pak	k6eAd1	pak
i	i	k9	i
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
tajících	tající	k2eAgInPc2d1	tající
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
vířivým	vířivý	k2eAgInSc7d1	vířivý
pohybem	pohyb	k1gInSc7	pohyb
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
ledovcové	ledovcový	k2eAgInPc4d1	ledovcový
obří	obří	k2eAgInPc4d1	obří
hrnce	hrnec	k1gInPc4	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
erozním	erozní	k2eAgInSc7d1	erozní
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
působení	působení	k1gNnSc1	působení
ledu	led	k1gInSc2	led
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skalních	skalní	k2eAgFnPc6d1	skalní
prasklinkách	prasklinka	k1gFnPc6	prasklinka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zavodněny	zavodněn	k2eAgFnPc1d1	zavodněna
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
mrazu	mráz	k1gInSc2	mráz
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
rozpínání	rozpínání	k1gNnSc6	rozpínání
ledu	led	k1gInSc2	led
k	k	k7c3	k
odlamování	odlamování	k1gNnSc3	odlamování
částí	část	k1gFnPc2	část
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prudkých	prudký	k2eAgInPc6d1	prudký
svazích	svah	k1gInPc6	svah
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
stavebně-technické	stavebněechnický	k2eAgInPc4d1	stavebně-technický
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pod	pod	k7c7	pod
takto	takto	k6eAd1	takto
narušenou	narušený	k2eAgFnSc7d1	narušená
skálou	skála	k1gFnSc7	skála
vedou	vést	k5eAaImIp3nP	vést
komunikace	komunikace	k1gFnPc1	komunikace
či	či	k8xC	či
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lidská	lidský	k2eAgNnPc4d1	lidské
obydlí	obydlí	k1gNnPc4	obydlí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
například	například	k6eAd1	například
Hřensko	Hřensko	k1gNnSc1	Hřensko
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tektonické	tektonický	k2eAgInPc1d1	tektonický
účinky	účinek	k1gInPc1	účinek
eroze	eroze	k1gFnSc2	eroze
===	===	k?	===
</s>
</p>
<p>
<s>
Části	část	k1gFnPc1	část
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
jsou	být	k5eAaImIp3nP	být
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
proudící	proudící	k2eAgFnSc1d1	proudící
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
led	led	k1gInSc1	led
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
postupně	postupně	k6eAd1	postupně
rozrušovány	rozrušován	k2eAgFnPc1d1	rozrušována
a	a	k8xC	a
transportovány	transportován	k2eAgFnPc1d1	transportována
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
následným	následný	k2eAgInPc3d1	následný
tektonickým	tektonický	k2eAgInPc3d1	tektonický
či	či	k8xC	či
izostatickým	izostatický	k2eAgInPc3d1	izostatický
pohybům	pohyb	k1gInPc3	pohyb
následkem	následkem	k7c2	následkem
odlehčení	odlehčení	k1gNnSc2	odlehčení
podloží	podloží	k1gNnSc2	podloží
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
odnosu	odnos	k1gInSc2	odnos
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
(	(	kIx(	(
<g/>
kácení	kácení	k1gNnSc1	kácení
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
<g/>
)	)	kIx)	)
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc4	proces
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
<g/>
.	.	kIx.	.
</s>
<s>
Stavbami	stavba	k1gFnPc7	stavba
některých	některý	k3yIgInPc2	některý
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
zpevňování	zpevňování	k1gNnSc1	zpevňování
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgFnSc2d1	ochranná
zdi	zeď	k1gFnSc2	zeď
a	a	k8xC	a
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
betonové	betonový	k2eAgFnSc2d1	betonová
injektáže	injektáž	k1gFnSc2	injektáž
<g/>
,	,	kIx,	,
výsadba	výsadba	k1gFnSc1	výsadba
určitých	určitý	k2eAgFnPc2d1	určitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
erozi	eroze	k1gFnSc4	eroze
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
naopak	naopak	k6eAd1	naopak
záměrně	záměrně	k6eAd1	záměrně
brání	bránit	k5eAaImIp3nS	bránit
či	či	k8xC	či
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc1	vliv
člověka	člověk	k1gMnSc2	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
erozi	eroze	k1gFnSc6	eroze
jako	jako	k9	jako
problému	problém	k1gInSc3	problém
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
mluvit	mluvit	k5eAaImF	mluvit
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
vzestupu	vzestup	k1gInSc2	vzestup
mechanizace	mechanizace	k1gFnSc2	mechanizace
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
centrálního	centrální	k2eAgNnSc2d1	centrální
plánování	plánování	k1gNnSc2	plánování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
účincích	účinek	k1gInPc6	účinek
eroze	eroze	k1gFnSc2	eroze
neexistovaly	existovat	k5eNaImAgInP	existovat
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgInPc1	žádný
údaje	údaj	k1gInPc4	údaj
až	až	k9	až
do	do	k7c2	do
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávách	zpráva	k1gFnPc6	zpráva
jako	jako	k8xS	jako
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
eroze	eroze	k1gFnSc2	eroze
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
celosvětové	celosvětový	k2eAgFnSc3d1	celosvětová
ztrátě	ztráta	k1gFnSc3	ztráta
480	[number]	k4	480
tun	tuna	k1gFnPc2	tuna
svrchní	svrchní	k2eAgFnSc2d1	svrchní
půdy	půda	k1gFnSc2	půda
během	během	k7c2	během
období	období	k1gNnSc2	období
1970-1990	[number]	k4	1970-1990
a	a	k8xC	a
odhadu	odhad	k1gInSc3	odhad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
stává	stávat	k5eAaImIp3nS	stávat
6-7	[number]	k4	6-7
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
nepoužitelnou	použitelný	k2eNgFnSc4d1	nepoužitelná
díky	díky	k7c3	díky
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Metody	metoda	k1gFnPc1	metoda
zpomalování	zpomalování	k1gNnSc2	zpomalování
eroze	eroze	k1gFnSc1	eroze
===	===	k?	===
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
rámcově	rámcově	k6eAd1	rámcově
nastíněny	nastíněn	k2eAgFnPc1d1	nastíněna
obecné	obecný	k2eAgFnPc1d1	obecná
metody	metoda	k1gFnPc1	metoda
zamezení	zamezení	k1gNnSc2	zamezení
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
zpomalování	zpomalování	k1gNnSc1	zpomalování
eroze	eroze	k1gFnSc2	eroze
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orba	orba	k1gFnSc1	orba
po	po	k7c6	po
vrstevnici	vrstevnice	k1gFnSc6	vrstevnice
<g/>
.	.	kIx.	.
</s>
<s>
Minimalizuje	minimalizovat	k5eAaBmIp3nS	minimalizovat
se	se	k3xPyFc4	se
postupné	postupný	k2eAgNnSc1d1	postupné
sesouvání	sesouvání	k1gNnSc1	sesouvání
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
půdy	půda	k1gFnSc2	půda
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budování	budování	k1gNnSc1	budování
průlehů	průleh	k1gMnPc2	průleh
–	–	k?	–
příkopů	příkop	k1gInPc2	příkop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
delší	dlouhý	k2eAgInSc4d2	delší
prudký	prudký	k2eAgInSc4d1	prudký
svah	svah	k1gInSc4	svah
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgInPc2d2	menší
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
vodě	voda	k1gFnSc3	voda
vsáknout	vsáknout	k5eAaPmF	vsáknout
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
jen	jen	k9	jen
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
plodiny	plodina	k1gFnPc4	plodina
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
slučitelné	slučitelný	k2eAgInPc4d1	slučitelný
se	se	k3xPyFc4	se
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
velkovýrobou	velkovýroba	k1gFnSc7	velkovýroba
(	(	kIx(	(
<g/>
určité	určitý	k2eAgNnSc1d1	určité
snížení	snížení	k1gNnSc1	snížení
přístupnosti	přístupnost	k1gFnSc2	přístupnost
větším	veliký	k2eAgInPc3d2	veliký
strojům	stroj	k1gInPc3	stroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budování	budování	k1gNnSc4	budování
zasakovacích	zasakovací	k2eAgInPc2d1	zasakovací
pásů	pás	k1gInPc2	pás
–	–	k?	–
vystouplých	vystouplý	k2eAgFnPc2d1	vystouplá
teras	terasa	k1gFnPc2	terasa
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zastaví	zastavit	k5eAaPmIp3nP	zastavit
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
jí	jíst	k5eAaImIp3nS	jíst
se	se	k3xPyFc4	se
vsáknout	vsáknout	k5eAaPmF	vsáknout
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompostování	kompostování	k1gNnSc1	kompostování
<g/>
.	.	kIx.	.
</s>
<s>
Obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
kvalitu	kvalita	k1gFnSc4	kvalita
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
kvantitativně	kvantitativně	k6eAd1	kvantitativně
vyšší	vysoký	k2eAgInSc4d2	vyšší
růst	růst	k1gInSc4	růst
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
ochranných	ochranný	k2eAgFnPc2d1	ochranná
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
takových	takový	k3xDgInPc2	takový
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
kromě	kromě	k7c2	kromě
úrody	úroda	k1gFnSc2	úroda
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
zpevňování	zpevňování	k1gNnSc6	zpevňování
půdy	půda	k1gFnSc2	půda
svými	svůj	k3xOyFgInPc7	svůj
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smíšené	smíšený	k2eAgFnSc2d1	smíšená
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
pěstování	pěstování	k1gNnSc1	pěstování
erozně	erozně	k6eAd1	erozně
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
plodin	plodina	k1gFnPc2	plodina
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
silně	silně	k6eAd1	silně
erozně	erozně	k6eAd1	erozně
ohrožené	ohrožený	k2eAgFnSc6d1	ohrožená
půdě	půda	k1gFnSc6	půda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zákaz	zákaz	k1gInSc4	zákaz
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
řepy	řepa	k1gFnSc2	řepa
<g/>
,	,	kIx,	,
bobu	bob	k1gInSc2	bob
setého	setý	k2eAgInSc2d1	setý
<g/>
,	,	kIx,	,
sóji	sója	k1gFnSc2	sója
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc2	slunečnice
a	a	k8xC	a
čiroku	čirok	k1gInSc2	čirok
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cyklická	cyklický	k2eAgFnSc1d1	cyklická
změna	změna	k1gFnSc1	změna
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
v	v	k7c6	v
dlouhodobějším	dlouhodobý	k2eAgNnSc6d2	dlouhodobější
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
půdě	půda	k1gFnSc3	půda
obnovit	obnovit	k5eAaPmF	obnovit
živiny	živina	k1gFnPc4	živina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
ta	ten	k3xDgFnSc1	ten
která	který	k3yQgFnSc1	který
kultura	kultura	k1gFnSc1	kultura
využívala	využívat	k5eAaPmAgFnS	využívat
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
erozí	eroze	k1gFnSc7	eroze
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Evropské	evropský	k2eAgFnSc2d1	Evropská
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
erozi	eroze	k1gFnSc3	eroze
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
115	[number]	k4	115
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
42	[number]	k4	42
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
erozi	eroze	k1gFnSc4	eroze
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
se	se	k3xPyFc4	se
roční	roční	k2eAgFnPc1d1	roční
škody	škoda	k1gFnPc1	škoda
způsobené	způsobený	k2eAgFnPc1d1	způsobená
erozí	eroze	k1gFnSc7	eroze
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
700	[number]	k4	700
milionů	milion	k4xCgInPc2	milion
až	až	k6eAd1	až
14	[number]	k4	14
miliard	miliarda	k4xCgFnPc2	miliarda
euro	euro	k1gNnSc4	euro
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eroze	eroze	k1gFnSc1	eroze
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
</s>
</p>
<p>
<s>
Zvětrávání	zvětrávání	k1gNnSc1	zvětrávání
</s>
</p>
<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
eroze	eroze	k1gFnSc2	eroze
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
eroze	eroze	k1gFnSc1	eroze
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Geoportál	Geoportál	k1gMnSc1	Geoportál
SOWAC-GIS	SOWAC-GIS	k1gMnSc1	SOWAC-GIS
(	(	kIx(	(
<g/>
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
:	:	kIx,	:
Vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
půda	půda	k1gFnSc1	půda
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
hladomory	hladomor	k1gInPc4	hladomor
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
pole	pole	k1gNnPc1	pole
jsou	být	k5eAaImIp3nP	být
časovanou	časovaný	k2eAgFnSc7d1	časovaná
bombou	bomba	k1gFnSc7	bomba
<g/>
,	,	kIx,	,
u	u	k7c2	u
poloviny	polovina	k1gFnSc2	polovina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
eroze	eroze	k1gFnSc1	eroze
</s>
</p>
