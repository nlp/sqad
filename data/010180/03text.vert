<p>
<s>
Nagano	Nagano	k1gNnSc1	Nagano
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
長	長	k?	長
<g/>
;	;	kIx,	;
Nagano-ši	Nagano-š	k1gMnPc1	Nagano-š
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
prefektury	prefektura	k1gFnSc2	prefektura
Nagano	Nagano	k1gNnSc1	Nagano
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Čikuma	Čikumum	k1gNnSc2	Čikumum
a	a	k8xC	a
Sai	Sai	k1gFnSc2	Sai
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
378	[number]	k4	378
059	[number]	k4	059
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hustotu	hustota	k1gFnSc4	hustota
osídlení	osídlení	k1gNnPc2	osídlení
512,37	[number]	k4	512,37
ob.	ob.	k?	ob.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
737,86	[number]	k4	737,86
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
město	město	k1gNnSc1	město
Nagano	Nagano	k1gNnSc4	Nagano
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1966	[number]	k4	1966
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
druhému	druhý	k4xOgNnSc3	druhý
sloučení	sloučení	k1gNnSc3	sloučení
města	město	k1gNnSc2	město
s	s	k7c7	s
8	[number]	k4	8
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Nagano	Nagano	k1gNnSc1	Nagano
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Honšů	Honš	k1gMnPc2	Honš
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionu	region	k1gInSc6	region
Chū	Chū	k1gFnSc1	Chū
(	(	kIx(	(
<g/>
Kō	Kō	k1gFnSc1	Kō
<g/>
'	'	kIx"	'
<g/>
etsu	etsu	k6eAd1	etsu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prefektuře	prefektura	k1gFnSc3	prefektura
Nagano	Nagano	k1gNnSc1	Nagano
<g/>
,	,	kIx,	,
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
36	[number]	k4	36
<g/>
°	°	k?	°
<g/>
38	[number]	k4	38
<g/>
'	'	kIx"	'
<g/>
N	N	kA	N
138	[number]	k4	138
<g/>
°	°	k?	°
<g/>
11	[number]	k4	11
<g/>
'	'	kIx"	'
<g/>
E.	E.	kA	E.
</s>
</p>
<p>
<s>
==	==	k?	==
Klima	klima	k1gNnSc1	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
mix	mix	k1gInSc4	mix
subtropického	subtropický	k2eAgInSc2d1	subtropický
vlhkého	vlhký	k2eAgInSc2d1	vlhký
a	a	k8xC	a
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
vlhkého	vlhký	k2eAgInSc2d1	vlhký
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejší	teplý	k2eAgInSc1d3	nejteplejší
je	být	k5eAaImIp3nS	být
srpen	srpen	k1gInSc1	srpen
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejchladnější	chladný	k2eAgInSc1d3	nejchladnější
leden	leden	k1gInSc1	leden
s	s	k7c7	s
–	–	k?	–
<g/>
4	[number]	k4	4
°	°	k?	°
<g/>
C.	C.	kA	C.
Zima	zima	k6eAd1	zima
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
vydatnou	vydatný	k2eAgFnSc4d1	vydatná
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
pokrývku	pokrývka	k1gFnSc4	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
45	[number]	k4	45
mm	mm	kA	mm
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
do	do	k7c2	do
130	[number]	k4	130
mm	mm	kA	mm
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Nagano	Nagano	k1gNnSc4	Nagano
hostilo	hostit	k5eAaImAgNnS	hostit
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Zimní	zimní	k2eAgFnSc2d1	zimní
paralympijské	paralympijský	k2eAgFnSc2d1	paralympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
Speciální	speciální	k2eAgFnSc1d1	speciální
olympijské	olympijský	k2eAgFnPc4d1	olympijská
zimní	zimní	k2eAgFnPc4d1	zimní
světové	světový	k2eAgFnPc4d1	světová
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Nagano	Nagano	k1gNnSc1	Nagano
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
hostilo	hostit	k5eAaImAgNnS	hostit
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc1	tři
tyto	tento	k3xDgFnPc1	tento
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
hal	hala	k1gFnPc2	hala
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
i	i	k9	i
muzeum	muzeum	k1gNnSc1	muzeum
olympiády	olympiáda	k1gFnSc2	olympiáda
98	[number]	k4	98
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
suvenýry	suvenýr	k1gInPc7	suvenýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
základní	základní	k2eAgInPc1d1	základní
<g/>
,	,	kIx,	,
6	[number]	k4	6
středních	střední	k1gMnPc2	střední
<g/>
,	,	kIx,	,
4	[number]	k4	4
vyšší	vysoký	k2eAgFnPc1d2	vyšší
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
1	[number]	k4	1
(	(	kIx(	(
<g/>
technologickou	technologický	k2eAgFnSc4d1	technologická
<g/>
)	)	kIx)	)
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
dopravu	doprava	k1gFnSc4	doprava
má	mít	k5eAaImIp3nS	mít
Nagano	Nagano	k1gNnSc1	Nagano
jedno	jeden	k4xCgNnSc1	jeden
hlavní	hlavní	k2eAgFnSc7d1	hlavní
a	a	k8xC	a
pár	pár	k4xCyI	pár
menších	malý	k2eAgNnPc2d2	menší
okrajových	okrajový	k2eAgNnPc2d1	okrajové
nádraží	nádraží	k1gNnPc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
Hokuriku	Hokurik	k1gMnSc3	Hokurik
Šinkansen	Šinkansen	k2eAgMnSc1d1	Šinkansen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
město	město	k1gNnSc4	město
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Takasaki	Takasaki	k1gNnSc7	Takasaki
<g/>
,	,	kIx,	,
Gunma	Gunma	k1gFnSc1	Gunma
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnSc4d1	lokální
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
Matsumoto	Matsumota	k1gFnSc5	Matsumota
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
docela	docela	k6eAd1	docela
odlehlé	odlehlý	k2eAgNnSc1d1	odlehlé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
trvá	trvat	k5eAaImIp3nS	trvat
celých	celý	k2eAgFnPc2d1	celá
70	[number]	k4	70
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
Nagana	Nagano	k1gNnSc2	Nagano
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgInSc1d1	slavný
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
chrám	chrám	k1gInSc1	chrám
Zenkō	Zenkō	k1gFnSc2	Zenkō
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Zenkódži	Zenkódž	k1gFnSc3	Zenkódž
<g/>
.	.	kIx.	.
</s>
<s>
Nagano	Nagano	k1gNnSc1	Nagano
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
malé	malý	k2eAgNnSc1d1	malé
město	město	k1gNnSc1	město
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgInSc2	tento
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
městě	město	k1gNnSc6	město
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
společnost	společnost	k1gFnSc1	společnost
Seiko	Seiko	k1gNnSc1	Seiko
Epson	Epson	kA	Epson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Clearwater	Clearwater	k1gMnSc1	Clearwater
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-ťia-čuang	-ťia-čuang	k1gMnSc1	-ťia-čuang
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nagano	Nagano	k1gNnSc4	Nagano
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
Nagano	Nagano	k1gNnSc4	Nagano
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Nagano	Nagano	k1gNnSc1	Nagano
</s>
</p>
