<s>
Apocalyptica	Apocalyptica	k1gFnSc1	Apocalyptica
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
interpretace	interpretace	k1gFnSc1	interpretace
původně	původně	k6eAd1	původně
heavy	heava	k1gFnSc2	heava
metalových	metalový	k2eAgFnPc2d1	metalová
skladeb	skladba	k1gFnPc2	skladba
osobitým	osobitý	k2eAgInSc7d1	osobitý
způsobem	způsob	k1gInSc7	způsob
aranžovaných	aranžovaný	k2eAgFnPc2d1	aranžovaná
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
)	)	kIx)	)
klasických	klasický	k2eAgMnPc2d1	klasický
violoncellistů	violoncellista	k1gMnPc2	violoncellista
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
hraje	hrát	k5eAaImIp3nS	hrát
též	též	k9	též
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
věhlas	věhlas	k1gInSc4	věhlas
adaptacemi	adaptace	k1gFnPc7	adaptace
skladeb	skladba	k1gFnPc2	skladba
skupin	skupina	k1gFnPc2	skupina
Metallica	Metallic	k2eAgFnSc1d1	Metallica
a	a	k8xC	a
Sepultura	Sepultura	k1gFnSc1	Sepultura
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
i	i	k9	i
svoje	svůj	k3xOyFgNnPc1	svůj
originální	originální	k2eAgNnPc1d1	originální
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
Inquisition	Inquisition	k1gInSc1	Inquisition
Symphony	Symphona	k1gFnSc2	Symphona
{	{	kIx(	{
<g/>
editace	editace	k1gFnSc2	editace
<g/>
:	:	kIx,	:
Inquisition	Inquisition	k1gInSc4	Inquisition
Symphony	Symphona	k1gFnSc2	Symphona
nahrála	nahrát	k5eAaBmAgFnS	nahrát
Sepultura	Sepultura	k1gFnSc1	Sepultura
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
...	...	k?	...
<g/>
}	}	kIx)	}
a	a	k8xC	a
Cult	Cult	k1gMnSc1	Cult
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
znatelný	znatelný	k2eAgInSc1d1	znatelný
vliv	vliv	k1gInSc1	vliv
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
albem	album	k1gNnSc7	album
Reflections	Reflections	k1gInSc1	Reflections
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
mírně	mírně	k6eAd1	mírně
vzdálili	vzdálit	k5eAaPmAgMnP	vzdálit
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
cello	cello	k1gNnSc4	cello
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Eicca	Eicca	k6eAd1	Eicca
Toppinen	Toppinen	k2eAgMnSc1d1	Toppinen
(	(	kIx(	(
<g/>
Eino	Eino	k1gNnSc1	Eino
Toppinen	Toppinna	k1gFnPc2	Toppinna
<g/>
)	)	kIx)	)
-	-	kIx~	-
cello	cello	k1gNnSc1	cello
Paavo	Paavo	k1gNnSc1	Paavo
Lötjönen	Lötjönen	k1gInSc1	Lötjönen
-	-	kIx~	-
cello	cello	k1gNnSc1	cello
Perttu	Pertt	k1gInSc2	Pertt
Kivilaakso	Kivilaaksa	k1gFnSc5	Kivilaaksa
-	-	kIx~	-
cello	cello	k1gNnSc1	cello
Mikko	Mikko	k1gNnSc1	Mikko
Sirén	Siréna	k1gFnPc2	Siréna
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
cello	cello	k1gNnSc1	cello
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
také	také	k9	také
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Megaphone	Megaphon	k1gMnSc5	Megaphon
a	a	k8xC	a
Emigrate	Emigrat	k1gMnSc5	Emigrat
)	)	kIx)	)
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
připojí	připojit	k5eAaPmIp3nS	připojit
i	i	k9	i
dřívější	dřívější	k2eAgInSc1d1	dřívější
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Antero	Antero	k1gNnSc1	Antero
Manninen	Manninen	k1gInSc1	Manninen
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antero	Antero	k1gNnSc1	Antero
Manninen	Manninna	k1gFnPc2	Manninna
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Tahu	tah	k1gInSc6	tah
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
)	)	kIx)	)
Max	Max	k1gMnSc1	Max
Lilja	Lilja	k1gMnSc1	Lilja
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
skupinách	skupina	k1gFnPc6	skupina
Hevein	Hevein	k1gMnSc1	Hevein
,	,	kIx,	,
Tarja	Tarja	k1gMnSc1	Tarja
<g/>
)	)	kIx)	)
Hlavně	hlavně	k9	hlavně
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
<g/>
:	:	kIx,	:
Edvard	Edvard	k1gMnSc1	Edvard
Grieg	Grieg	k1gMnSc1	Grieg
Pantera	panter	k1gMnSc2	panter
Rammstein	Rammstein	k1gMnSc1	Rammstein
Sepultura	Sepultura	k1gFnSc1	Sepultura
Slayer	Slayer	k1gMnSc1	Slayer
Faith	Faith	k1gMnSc1	Faith
No	no	k9	no
More	mor	k1gInSc5	mor
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
členy	člen	k1gMnPc7	člen
Amon	Amono	k1gNnPc2	Amono
<g />
.	.	kIx.	.
</s>
<s>
Amarth	Amarth	k1gInSc1	Amarth
(	(	kIx(	(
<g/>
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Live	Live	k1gFnPc2	Live
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
Kill	Kill	k1gMnSc1	Kill
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Twilight	Twilight	k1gMnSc1	Twilight
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Thunder	Thunder	k1gMnSc1	Thunder
God	God	k1gMnSc1	God
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Sandra	Sandra	k1gFnSc1	Sandra
Nasic	Nasic	k1gMnSc1	Nasic
(	(	kIx(	(
<g/>
Guano	Guano	k1gNnSc1	Guano
Apes	Apesa	k1gFnPc2	Apesa
<g/>
)	)	kIx)	)
Cristina	Cristin	k2eAgFnSc1d1	Cristina
Scabbia	Scabbia	k1gFnSc1	Scabbia
(	(	kIx(	(
<g/>
Lacuna	Lacuna	k1gFnSc1	Lacuna
Coil	Coil	k1gMnSc1	Coil
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
(	(	kIx(	(
<g/>
Slayer	Slayer	k1gMnSc1	Slayer
<g/>
)	)	kIx)	)
Adam	Adam	k1gMnSc1	Adam
<g />
.	.	kIx.	.
</s>
<s>
Gontier	Gontier	k1gInSc1	Gontier
(	(	kIx(	(
<g/>
Three	Three	k1gFnSc1	Three
Days	Daysa	k1gFnPc2	Daysa
Grace	Grace	k1gMnSc2	Grace
<g/>
)	)	kIx)	)
Corey	Corea	k1gMnSc2	Corea
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
Slipknot	Slipknota	k1gFnPc2	Slipknota
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Sour	Sour	k1gMnSc1	Sour
<g/>
)	)	kIx)	)
Emmanuelle	Emmanuella	k1gFnSc6	Emmanuella
Monet	moneta	k1gFnPc2	moneta
<g/>
,	,	kIx,	,
aka	aka	k?	aka
Manu	mana	k1gFnSc4	mana
(	(	kIx(	(
<g/>
Dolly	Dolla	k1gFnSc2	Dolla
<g/>
)	)	kIx)	)
Lauri	Laur	k1gFnSc2	Laur
Ylönen	Ylönen	k2eAgMnSc1d1	Ylönen
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Rasmus	Rasmus	k1gMnSc1	Rasmus
<g/>
)	)	kIx)	)
Linda	Linda	k1gFnSc1	Linda
Sundblad	Sundblad	k1gInSc1	Sundblad
(	(	kIx(	(
<g/>
Lambretta	Lambretta	k1gFnSc1	Lambretta
<g/>
)	)	kIx)	)
Marta	Marta	k1gFnSc1	Marta
Jandová	Jandová	k1gFnSc1	Jandová
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
<g />
.	.	kIx.	.
</s>
<s>
Happy	Happ	k1gInPc1	Happ
<g/>
)	)	kIx)	)
Matt	Matt	k2eAgInSc1d1	Matt
Tuck	Tuck	k1gInSc1	Tuck
(	(	kIx(	(
<g/>
Bullet	Bullet	k1gInSc1	Bullet
for	forum	k1gNnPc2	forum
My	my	k3xPp1nPc1	my
Valentine	Valentin	k1gMnSc5	Valentin
<g/>
)	)	kIx)	)
Matthias	Matthias	k1gMnSc1	Matthias
Sayer	Sayer	k1gMnSc1	Sayer
(	(	kIx(	(
<g/>
Farmer	Farmer	k1gInSc1	Farmer
Boys	boy	k1gMnPc2	boy
<g/>
)	)	kIx)	)
Max	Max	k1gMnSc1	Max
Cavalera	Cavaler	k1gMnSc2	Cavaler
(	(	kIx(	(
<g/>
Soulfly	Soulfly	k1gFnSc1	Soulfly
<g/>
)	)	kIx)	)
Nina	Nina	k1gFnSc1	Nina
Hagen	Hagna	k1gFnPc2	Hagna
Till	Till	k1gMnSc1	Till
Lindemann	Lindemann	k1gMnSc1	Lindemann
(	(	kIx(	(
<g/>
Rammstein	Rammstein	k1gMnSc1	Rammstein
<g/>
)	)	kIx)	)
Tomoyasu	Tomoyas	k1gInSc2	Tomoyas
Hotei	Hote	k1gMnSc5	Hote
Ville	Vill	k1gMnSc5	Vill
Valo	Vala	k1gMnSc5	Vala
(	(	kIx(	(
<g/>
HIM	HIM	kA	HIM
<g/>
)	)	kIx)	)
Rammstein	Rammstein	k1gMnSc1	Rammstein
(	(	kIx(	(
<g/>
Benzin	benzin	k1gInSc1	benzin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Sebnem	Sebno	k1gNnSc7	Sebno
Ferah	Ferah	k1gMnSc1	Ferah
Sepultura	Sepultura	k1gFnSc1	Sepultura
Tool	Tool	k1gInSc4	Tool
Lacey	Lacea	k1gFnSc2	Lacea
Sturm	Sturm	k1gInSc1	Sturm
(	(	kIx(	(
<g/>
Flyleaf	Flyleaf	k1gInSc1	Flyleaf
<g/>
)	)	kIx)	)
Franky	Franky	k1gInPc1	Franky
Perez	Pereza	k1gFnPc2	Pereza
(	(	kIx(	(
Scars	Scars	k1gInSc1	Scars
on	on	k3xPp3gMnSc1	on
Broadway	Broadwaa	k1gMnSc2	Broadwaa
<g/>
,	,	kIx,	,
DKFXP	DKFXP	kA	DKFXP
<g/>
)	)	kIx)	)
Plays	Plays	k1gInSc1	Plays
Metallica	Metallic	k1gInSc2	Metallic
by	by	kYmCp3nS	by
Four	Four	k1gInSc4	Four
Cellos	Cellosa	k1gFnPc2	Cellosa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Inquisition	Inquisition	k1gInSc1	Inquisition
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Cult	Culta	k1gFnPc2	Culta
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Cult	Cult	k2eAgInSc1d1	Cult
Special	Special	k1gInSc1	Special
Edition	Edition	k1gInSc1	Edition
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Reflections	Reflectionsa	k1gFnPc2	Reflectionsa
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Reflections	Reflections	k1gInSc1	Reflections
Revised	Revised	k1gInSc1	Revised
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Apocalyptica	Apocalyptic	k1gInSc2	Apocalyptic
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Worlds	Worlds	k1gInSc1	Worlds
Collide	Collid	k1gInSc5	Collid
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
th	th	k?	th
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Shadowmaker	Shadowmakra	k1gFnPc2	Shadowmakra
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnPc7	The
Life	Lif	k1gFnSc2	Lif
Burns	Burns	k1gInSc1	Burns
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Best	Best	k1gMnSc1	Best
of	of	k?	of
Apocalyptica	Apocalyptica	k1gMnSc1	Apocalyptica
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Amplified	Amplified	k1gInSc1	Amplified
<g/>
:	:	kIx,	:
A	a	k9	a
Decade	Decad	k1gInSc5	Decad
of	of	k?	of
Reinventing	Reinventing	k1gInSc1	Reinventing
the	the	k?	the
Cello	cello	k1gNnSc1	cello
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Apocalyptica	Apocalyptic	k1gInSc2	Apocalyptic
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Harmageddon	Harmageddona	k1gFnPc2	Harmageddona
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Path	Patha	k1gFnPc2	Patha
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Sandra	Sandra	k1gFnSc1	Sandra
Nasic	Nasice	k1gFnPc2	Nasice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Hope	Hop	k1gFnPc4	Hop
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Matthias	Matthias	k1gMnSc1	Matthias
Sayer	Sayer	k1gMnSc1	Sayer
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Faraway	Farawaa	k1gFnPc4	Farawaa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Sundblad	Sundblad	k1gInSc1	Sundblad
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Seemann	Seemann	k1gMnSc1	Seemann
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Nina	Nina	k1gFnSc1	Nina
Hagen	Hagna	k1gFnPc2	Hagna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Bittersweet	Bittersweet	k1gMnSc1	Bittersweet
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ville	Vill	k1gMnSc5	Vill
Valo	Vala	k1gMnSc5	Vala
&	&	k?	&
Lauri	Laur	k1gFnSc3	Laur
Ylönen	Ylönen	k2eAgInSc1d1	Ylönen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Wie	Wie	k1gMnSc1	Wie
Weit	Weit	k1gMnSc1	Weit
<g/>
/	/	kIx~	/
<g/>
How	How	k1gFnSc1	How
Far	fara	k1gFnPc2	fara
<g/>
/	/	kIx~	/
<g/>
En	En	k1gMnSc1	En
Vie	Vie	k1gMnSc1	Vie
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
Jandová	Jandová	k1gFnSc1	Jandová
et	et	k?	et
Manu	manout	k5eAaImIp1nS	manout
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Life	Lif	k1gInSc2	Lif
Burns	Burns	k1gInSc1	Burns
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Lauri	Lauri	k6eAd1	Lauri
Ylönen	Ylönen	k2eAgMnSc1d1	Ylönen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Repressed	Repressed	k1gMnSc1	Repressed
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Cavalera	Cavaler	k1gMnSc2	Cavaler
&	&	k?	&
Matt	Matt	k1gMnSc1	Matt
Tuck	Tuck	k1gMnSc1	Tuck
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Jesus	Jesus	k1gMnSc1	Jesus
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Corey	Corea	k1gMnSc2	Corea
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
S.	S.	kA	S.
<g/>
O.S.	O.S.	k1gFnSc1	O.S.
(	(	kIx(	(
<g/>
Anything	Anything	k1gInSc1	Anything
But	But	k1gFnSc2	But
Love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Cristina	Cristin	k2eAgFnSc1d1	Cristina
Scabbia	Scabbia	k1gFnSc1	Scabbia
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
I	i	k8xC	i
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Care	car	k1gMnSc5	car
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
Gontier	Gontier	k1gMnSc1	Gontier
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
V	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
jména	jméno	k1gNnPc1	jméno
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
často	často	k6eAd1	často
skrývají	skrývat	k5eAaImIp3nP	skrývat
pod	pod	k7c7	pod
následujícími	následující	k2eAgFnPc7d1	následující
přezdívkami	přezdívka	k1gFnPc7	přezdívka
<g/>
:	:	kIx,	:
Eicca	Eicc	k2eAgFnSc1d1	Eicc
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Tease	Tease	k1gFnSc1	Tease
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Farmer	Farmer	k1gMnSc1	Farmer
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Tractor	Tractor	k1gInSc1	Tractor
<g/>
,	,	kIx,	,
Barbie	Barbie	k1gFnSc1	Barbie
Face	Face	k1gFnSc1	Face
<g/>
,	,	kIx,	,
Eicca-Barbie	Eicca-Barbie	k1gFnSc1	Eicca-Barbie
Paavo	Paavo	k1gNnSc1	Paavo
The	The	k1gFnSc2	The
Incredible	Incredible	k1gFnPc2	Incredible
Paavo	Paavo	k1gNnSc1	Paavo
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Uncrunchable	Uncrunchable	k1gFnSc1	Uncrunchable
One	One	k1gFnSc1	One
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Green	Green	k1gInSc1	Green
Perttu	Pertt	k1gInSc2	Pertt
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Pee	Pee	k?	Pee
<g/>
,	,	kIx,	,
Pretty	Prett	k1gInPc1	Prett
Pouty	pout	k1gInPc7	pout
Perttu	Pertt	k1gInSc2	Pertt
<g/>
,	,	kIx,	,
Pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
Pertsa	Pertsa	k1gFnSc1	Pertsa
<g/>
,	,	kIx,	,
Perttu-Elessar	Perttu-Elessar	k1gMnSc1	Perttu-Elessar
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Beast	Beast	k1gMnSc1	Beast
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Exclamation	Exclamation	k1gInSc4	Exclamation
Mark	Mark	k1gMnSc1	Mark
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Fainty	Fainta	k1gFnSc2	Fainta
One	One	k1gMnSc1	One
<g/>
,	,	kIx,	,
Rainbow	Rainbow	k1gMnSc1	Rainbow
Perttu	Pertt	k1gInSc2	Pertt
<g/>
,	,	kIx,	,
Perttula	Perttulum	k1gNnSc2	Perttulum
<g/>
,	,	kIx,	,
Perttichou	Pertticha	k1gFnSc7	Pertticha
<g/>
,	,	kIx,	,
Petty	Pett	k1gInPc7	Pett
Antero	Antero	k1gNnSc1	Antero
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
<g/>
Megaco	Megaco	k1gNnSc1	Megaco
<g/>
^	^	kIx~	^
<g/>
ol	ol	k?	ol
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
^	^	kIx~	^
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Baron	baron	k1gMnSc1	baron
of	of	k?	of
Pukkila	Pukkil	k1gMnSc4	Pukkil
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Freeze	Freeze	k1gFnSc1	Freeze
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Cod	coda	k1gFnPc2	coda
<g/>
,	,	kIx,	,
Mystery	Myster	k1gInPc1	Myster
Manninen	Manninna	k1gFnPc2	Manninna
Max	max	kA	max
The	The	k1gMnSc1	The
Troll	troll	k1gMnSc1	troll
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Techno	Techno	k6eAd1	Techno
Píseň	píseň	k1gFnSc1	píseň
Path	Patha	k1gFnPc2	Patha
z	z	k7c2	z
alba	album	k1gNnSc2	album
Cult	Culta	k1gFnPc2	Culta
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
a	a	k8xC	a
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
autorů	autor	k1gMnPc2	autor
použita	použít	k5eAaPmNgNnP	použít
v	v	k7c6	v
kontroverzním	kontroverzní	k2eAgInSc6d1	kontroverzní
předvolebním	předvolební	k2eAgInSc6d1	předvolební
spotu	spot	k1gInSc6	spot
české	český	k2eAgFnSc2d1	Česká
ultrapravicové	ultrapravicový	k2eAgFnSc2d1	ultrapravicová
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Apocalyptica	Apocalyptic	k2eAgFnSc1d1	Apocalyptica
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgMnSc3	ten
brzy	brzy	k6eAd1	brzy
ohradila	ohradit	k5eAaPmAgFnS	ohradit
s	s	k7c7	s
vyjádřením	vyjádření	k1gNnSc7	vyjádření
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
otevřeně	otevřeně	k6eAd1	otevřeně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
rasismu	rasismus	k1gInSc3	rasismus
a	a	k8xC	a
diskriminaci	diskriminace	k1gFnSc3	diskriminace
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
rozchází	rozcházet	k5eAaImIp3nP	rozcházet
s	s	k7c7	s
politickými	politický	k2eAgInPc7d1	politický
požadavky	požadavek	k1gInPc7	požadavek
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
dále	daleko	k6eAd2	daleko
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
podá	podat	k5eAaPmIp3nS	podat
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
neoprávněnému	oprávněný	k2eNgNnSc3d1	neoprávněné
využití	využití	k1gNnSc3	využití
jejich	jejich	k3xOp3gFnSc2	jejich
hudby	hudba	k1gFnSc2	hudba
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
porušení	porušení	k1gNnSc2	porušení
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
později	pozdě	k6eAd2	pozdě
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
spot	spot	k1gInSc1	spot
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
kritizovaný	kritizovaný	k2eAgMnSc1d1	kritizovaný
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnPc4	vyjadřování
rasistických	rasistický	k2eAgInPc2d1	rasistický
názorů	názor	k1gInPc2	názor
ohledně	ohledně	k7c2	ohledně
romského	romský	k2eAgNnSc2d1	romské
etnika	etnikum	k1gNnSc2	etnikum
<g/>
,	,	kIx,	,
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
13.7	[number]	k4	13.7
<g/>
.	.	kIx.	.
-	-	kIx~	-
16.7	[number]	k4	16.7
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
Masters	Mastersa	k1gFnPc2	Mastersa
Of	Of	k1gFnSc1	Of
Rock	rock	k1gInSc1	rock
2006	[number]	k4	2006
10.7	[number]	k4	10.7
<g/>
.	.	kIx.	.
-	-	kIx~	-
13.7	[number]	k4	13.7
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Masters	Mastersa	k1gFnPc2	Mastersa
Of	Of	k1gFnPc2	Of
Rock	rock	k1gInSc1	rock
<g />
.	.	kIx.	.
</s>
<s>
2008	[number]	k4	2008
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
od	od	k7c2	od
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Sychrov	Sychrovo	k1gNnPc2	Sychrovo
10.10	[number]	k4	10.10
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Forum	forum	k1gNnSc1	forum
Karlín	Karlín	k1gInSc1	Karlín
14.7	[number]	k4	14.7
<g/>
.	.	kIx.	.
-	-	kIx~	-
17.7	[number]	k4	17.7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
Masters	Mastersa	k1gFnPc2	Mastersa
Of	Of	k1gFnSc1	Of
Rock	rock	k1gInSc1	rock
2016	[number]	k4	2016
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Apocalyptica	Apocalyptic	k1gInSc2	Apocalyptic
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Apocalyptica	Apocalyptica	k1gFnSc1	Apocalyptica
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ruská	ruský	k2eAgFnSc1d1	ruská
stránka	stránka	k1gFnSc1	stránka
fanoušků	fanoušek	k1gMnPc2	fanoušek
(	(	kIx(	(
<g/>
také	také	k9	také
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
a	a	k8xC	a
španělštině	španělština	k1gFnSc6	španělština
<g/>
)	)	kIx)	)
</s>
