<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
)	)	kIx)	)
klasických	klasický	k2eAgMnPc2d1	klasický
violoncellistů	violoncellista	k1gMnPc2	violoncellista
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
hraje	hrát	k5eAaImIp3nS	hrát
též	též	k9	též
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
