<s>
Břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc1d2	starší
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
hlavnička	hlavnička	k1gFnSc1	hlavnička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nemoc	nemoc	k1gFnSc1	nemoc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
baktérií	baktérie	k1gFnSc7	baktérie
Salmonella	Salmonell	k1gMnSc2	Salmonell
typhi	typhi	k1gNnSc7	typhi
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
závažné	závažný	k2eAgNnSc1d1	závažné
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
přenášené	přenášený	k2eAgNnSc1d1	přenášené
stravou	strava	k1gFnSc7	strava
nebo	nebo	k8xC	nebo
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
absorbovány	absorbovat	k5eAaBmNgFnP	absorbovat
do	do	k7c2	do
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
u	u	k7c2	u
břišního	břišní	k2eAgInSc2d1	břišní
tyfu	tyf	k1gInSc2	tyf
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
-	-	kIx~	-
14	[number]	k4	14
dnů	den	k1gInPc2	den
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
bezpříznakově	bezpříznakově	k6eAd1	bezpříznakově
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
je	být	k5eAaImIp3nS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
kultivací	kultivace	k1gFnSc7	kultivace
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
nebo	nebo	k8xC	nebo
stolice	stolice	k1gFnSc2	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
podávání	podávání	k1gNnSc4	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
vysoká	vysoký	k2eAgFnSc1d1	vysoká
horečka	horečka	k1gFnSc1	horečka
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
zimnice	zimnice	k1gFnSc1	zimnice
nízká	nízký	k2eAgFnSc1d1	nízká
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
celková	celkový	k2eAgFnSc1d1	celková
slabost	slabost	k1gFnSc1	slabost
průjem	průjem	k1gInSc1	průjem
bolest	bolest	k1gFnSc4	bolest
hlavy	hlava	k1gFnSc2	hlava
bolesti	bolest	k1gFnSc2	bolest
svalů	sval	k1gInPc2	sval
nechutenství	nechutenství	k1gNnSc2	nechutenství
zácpa	zácpa	k1gFnSc1	zácpa
bolest	bolest	k1gFnSc1	bolest
břicha	břicho	k1gNnSc2	břicho
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
výskyt	výskyt	k1gInSc4	výskyt
plochých	plochý	k2eAgInPc2d1	plochý
<g/>
,	,	kIx,	,
růžově	růžově	k6eAd1	růžově
zbarvených	zbarvený	k2eAgInPc2d1	zbarvený
flíčků	flíček	k1gInPc2	flíček
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
i	i	k8xC	i
extrémní	extrémní	k2eAgInPc4d1	extrémní
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
perforace	perforace	k1gFnPc1	perforace
nebo	nebo	k8xC	nebo
krvácení	krvácení	k1gNnSc1	krvácení
ze	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
zmatenost	zmatenost	k1gFnSc1	zmatenost
apod.	apod.	kA	apod.
Břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
teplým	teplý	k2eAgNnSc7d1	teplé
klimatem	klima	k1gNnSc7	klima
a	a	k8xC	a
nízkým	nízký	k2eAgInSc7d1	nízký
hygienickým	hygienický	k2eAgInSc7d1	hygienický
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
je	být	k5eAaImIp3nS	být
odhadován	odhadován	k2eAgInSc1d1	odhadován
výskyt	výskyt	k1gInSc1	výskyt
onemocnění	onemocnění	k1gNnPc2	onemocnění
na	na	k7c4	na
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
souvisejících	související	k2eAgFnPc2d1	související
úmrtí	úmrť	k1gFnPc2	úmrť
<g/>
.	.	kIx.	.
</s>
<s>
Nakazí	nakazit	k5eAaPmIp3nS	nakazit
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
cestovatel	cestovatel	k1gMnSc1	cestovatel
z	z	k7c2	z
25-30	[number]	k4	25-30
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentě	subkontinent	k1gInSc6	subkontinent
1	[number]	k4	1
ze	z	k7c2	z
3	[number]	k4	3
tisíců	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslově	průmyslově	k6eAd1	průmyslově
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
stal	stát	k5eAaPmAgInS	stát
vzácným	vzácný	k2eAgNnSc7d1	vzácné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
dobrým	dobrý	k2eAgFnPc3d1	dobrá
hygienickým	hygienický	k2eAgFnPc3d1	hygienická
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
kvalitnímu	kvalitní	k2eAgNnSc3d1	kvalitní
zdravotnictví	zdravotnictví	k1gNnSc3	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
importována	importovat	k5eAaBmNgFnS	importovat
cestovateli	cestovatel	k1gMnPc7	cestovatel
nebo	nebo	k8xC	nebo
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tyfus	tyfus	k1gInSc4	tyfus
především	především	k9	především
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
u	u	k7c2	u
břišního	břišní	k2eAgInSc2d1	břišní
tyfu	tyf	k1gInSc2	tyf
činí	činit	k5eAaImIp3nS	činit
7-14	[number]	k4	7-14
dnů	den	k1gInPc2	den
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
bezpříznakově	bezpříznakově	k6eAd1	bezpříznakově
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horečkou	horečka	k1gFnSc7	horečka
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
nechutenstvím	nechutenství	k1gNnSc7	nechutenství
<g/>
,	,	kIx,	,
malátností	malátnost	k1gFnSc7	malátnost
<g/>
,	,	kIx,	,
vyrážkou	vyrážka	k1gFnSc7	vyrážka
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
ospalostí	ospalost	k1gFnSc7	ospalost
<g/>
,	,	kIx,	,
vyčerpaností	vyčerpanost	k1gFnSc7	vyčerpanost
<g/>
,	,	kIx,	,
zřetelně	zřetelně	k6eAd1	zřetelně
zastřeným	zastřený	k2eAgNnSc7d1	zastřené
vědomím	vědomí	k1gNnSc7	vědomí
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
průjmu	průjem	k1gInSc3	průjem
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
u	u	k7c2	u
léčených	léčený	k2eAgMnPc2d1	léčený
pacientů	pacient	k1gMnPc2	pacient
rychle	rychle	k6eAd1	rychle
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
během	během	k7c2	během
2-5	[number]	k4	2-5
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provázeno	provázet	k5eAaImNgNnS	provázet
komplikacemi	komplikace	k1gFnPc7	komplikace
jako	jako	k8xC	jako
například	například	k6eAd1	například
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
protržení	protržení	k1gNnSc1	protržení
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
pobřišnice	pobřišnice	k1gFnSc2	pobřišnice
nebo	nebo	k8xC	nebo
zánět	zánět	k1gInSc1	zánět
žlučníku	žlučník	k1gInSc2	žlučník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
bacilonosičství	bacilonosičství	k1gNnSc3	bacilonosičství
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
bývá	bývat	k5eAaImIp3nS	bývat
průběh	průběh	k1gInSc1	průběh
břišního	břišní	k2eAgInSc2d1	břišní
tyfu	tyf	k1gInSc2	tyf
mírnější	mírný	k2eAgInPc1d2	mírnější
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnPc2	onemocnění
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
bacilonosičství	bacilonosičství	k1gNnSc2	bacilonosičství
břišního	břišní	k2eAgInSc2d1	břišní
tyfu	tyf	k1gInSc2	tyf
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
film	film	k1gInSc1	film
Pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
vizita	vizita	k1gFnSc1	vizita
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
známou	známý	k2eAgFnSc7d1	známá
bacilonosičkou	bacilonosička	k1gFnSc7	bacilonosička
břišního	břišní	k2eAgInSc2d1	břišní
tyfu	tyf	k1gInSc2	tyf
tyfová	tyfový	k2eAgFnSc1d1	tyfová
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
tyfus	tyfus	k1gInSc1	tyfus
paratyfus	paratyfus	k1gInSc1	paratyfus
</s>
