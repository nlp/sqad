<s>
Epsilon	epsilon	k1gNnPc1
Eridani	Eridan	k1gMnPc1
</s>
<s>
Epsilon	epsilon	k1gNnSc1
Eridani	Eridaň	k1gFnSc3
Poloha	poloha	k1gFnSc1
ε	ε	k?
Eridani	Eridan	k1gMnPc1
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Eridanu	Eridan	k1gInSc2
(	(	kIx(
<g/>
označena	označen	k2eAgFnSc1d1
kroužkem	kroužek	k1gInSc7
<g/>
)	)	kIx)
Astrometrická	Astrometrický	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000.0	2000.0	k4
<g/>
)	)	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Eridanus	Eridanus	k1gInSc1
(	(	kIx(
<g/>
Eridanus	Eridanus	k1gInSc1
<g/>
)	)	kIx)
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
h	h	k?
32	#num#	k4
<g/>
m	m	kA
55,844	55,844	k4
96	#num#	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
-9	-9	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
29,731	29,731	k4
2	#num#	k4
<g/>
″	″	k?
Paralaxa	paralaxa	k1gFnSc1
</s>
<s>
310,94	310,94	k4
<g/>
±	±	k?
<g/>
0,16	0,16	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
<g/>
"	"	kIx"
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
10,49	10,49	k4
<g/>
±	±	k?
0,005	0,005	k4
ly	ly	k?
<g/>
(	(	kIx(
<g/>
3,216	3,216	k4
<g/>
±	±	k?
0,002	0,002	k4
pc	pc	k?
<g/>
)	)	kIx)
Barevný	barevný	k2eAgInSc1d1
index	index	k1gInSc1
(	(	kIx(
<g/>
U-B	U-B	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,58	0,58	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
B-V	B-V	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,88	0,88	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
V-R	V-R	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,73	0,73	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
R-I	R-I	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,46	0,46	k4
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
3,73	3,73	k4
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
6,19	6,19	k4
Vlastní	vlastní	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
v	v	k7c6
rektascenzi	rektascenze	k1gFnSc6
</s>
<s>
−	−	k?
mas	masa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
Vlastní	vlastní	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
v	v	k7c6
deklinaci	deklinace	k1gFnSc6
</s>
<s>
20,266	20,266	k4
mas	masa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Typ	typ	k1gInSc1
proměnnosti	proměnnost	k1gFnSc2
</s>
<s>
BY	by	kYmCp3nP
Draconis	Draconis	k1gInSc1
Spektrální	spektrální	k2eAgInSc1d1
typ	typ	k1gInSc1
</s>
<s>
K2V	K2V	k4
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
0,82	0,82	k4
M	M	kA
<g/>
☉	☉	k?
Poloměr	poloměr	k1gInSc1
</s>
<s>
0,735	0,735	k4
R	R	kA
<g/>
☉	☉	k?
Zářivý	zářivý	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
0,34	0,34	k4
L	L	kA
<g/>
☉	☉	k?
Povrchová	povrchový	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
5	#num#	k4
263	#num#	k4
K	k	k7c3
Stáří	stáří	k1gNnSc3
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
108	#num#	k4
Rotační	rotační	k2eAgFnSc1d1
perioda	perioda	k1gFnSc1
</s>
<s>
11,2	11,2	k4
dní	den	k1gInPc2
Rychlost	rychlost	k1gFnSc1
rotace	rotace	k1gFnSc1
</s>
<s>
2,4	2,4	k4
±	±	k?
0,5	0,5	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Další	další	k2eAgFnSc7d1
označení	označení	k1gNnSc6
Henry	henry	k1gInSc3
Draper	Drapra	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
HD	HD	kA
22049	#num#	k4
Bonner	Bonnero	k1gNnPc2
Durchmusterung	Durchmusterunga	k1gFnPc2
</s>
<s>
BD-09	BD-09	k4
697	#num#	k4
Bright	Brighta	k1gFnPc2
Star	Star	kA
katalog	katalog	k1gInSc1
</s>
<s>
HR	hr	k6eAd1
1084	#num#	k4
2MASS	2MASS	k4
</s>
<s>
2MASS	2MASS	k4
J03325591-0927298	J03325591-0927298	k1gFnPc2
SAO	SAO	kA
katalog	katalog	k1gInSc1
</s>
<s>
SAO	SAO	kA
130564	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katalog	katalog	k1gInSc1
Hipparcos	Hipparcosa	k1gFnPc2
</s>
<s>
HIP	hip	k0
16537	#num#	k4
Tychův	Tychův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
TYC	TYC	kA
5296-1533-1	5296-1533-1	k4
General	General	k1gFnSc2
Catalogue	Catalogue	k1gFnPc2
</s>
<s>
GC	GC	kA
4244	#num#	k4
Glieseho	Gliese	k1gMnSc2
katalog	katalog	k1gInSc4
</s>
<s>
Gl	Gl	k?
144	#num#	k4
Bayerovo	Bayerův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
ε	ε	k?
Eri	Eri	k1gFnSc2
Flamsteedovo	Flamsteedův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
18	#num#	k4
Eri	Eri	k1gFnSc1
Synonyma	synonymum	k1gNnSc2
</s>
<s>
LHS	LHS	kA
1557	#num#	k4
<g/>
,	,	kIx,
CCDM	CCDM	kA
J	J	kA
<g/>
0	#num#	k4
<g/>
3329	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
927	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
2E	2E	k4
788	#num#	k4
<g/>
,	,	kIx,
FK5	FK5	k1gFnSc1
127	#num#	k4
<g/>
,	,	kIx,
GCRV	GCRV	kA
1962	#num#	k4
<g/>
,	,	kIx,
GCTP	GCTP	kA
742.00	742.00	k4
<g/>
,	,	kIx,
GJ	GJ	kA
144	#num#	k4
<g/>
,	,	kIx,
IRAS	IRAS	kA
0	#num#	k4
<g/>
3305	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
937	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
JP11	JP11	k1gFnSc1
792	#num#	k4
<g/>
,	,	kIx,
LFT	LFT	kA
291	#num#	k4
<g/>
,	,	kIx,
LHS	LHS	kA
1557	#num#	k4
<g/>
,	,	kIx,
LPM	LPM	kA
158	#num#	k4
<g/>
,	,	kIx,
LTT	LTT	kA
1675	#num#	k4
<g/>
,	,	kIx,
2MASS	2MASS	k4
J	J	kA
<g/>
0	#num#	k4
<g/>
3325591	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
927298	#num#	k4
<g/>
,	,	kIx,
NLTT	NLTT	kA
11207	#num#	k4
<g/>
,	,	kIx,
PLX	PLX	kA
742	#num#	k4
<g/>
,	,	kIx,
PPM	PPM	kA
185905	#num#	k4
<g/>
,	,	kIx,
ROT	rota	k1gFnPc2
531	#num#	k4
<g/>
,	,	kIx,
TD1	TD1	k1gFnSc1
2301	#num#	k4
<g/>
,	,	kIx,
UBV	UBV	kA
3410	#num#	k4
<g/>
,	,	kIx,
WDS	WDS	kA
J03329-0927A	J03329-0927A	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgInSc6d1
světleNěkterá	světleNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Epsilon	epsilon	k1gNnSc3
Eridani	Eridan	k1gMnPc1
(	(	kIx(
<g/>
ε	ε	k?
Eridani	Eridan	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Ran	Rana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
hvězda	hvězda	k1gFnSc1
nacházející	nacházející	k2eAgFnSc1d1
se	se	k3xPyFc4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Eridanu	Eridan	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
deklinací	deklinace	k1gFnSc7
9.46	9.46	k4
<g/>
°	°	k?
jižně	jižně	k6eAd1
od	od	k7c2
nebeského	nebeský	k2eAgInSc2d1
rovníku	rovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
umístění	umístění	k1gNnSc6
ji	on	k3xPp3gFnSc4
činí	činit	k5eAaImIp3nS
viditelnou	viditelný	k2eAgFnSc4d1
z	z	k7c2
většiny	většina	k1gFnSc2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ran	Rana	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
10,5	10,5	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
(	(	kIx(
<g/>
3,2	3,2	k4
parseku	parsec	k1gInSc2
<g/>
)	)	kIx)
od	od	k7c2
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gFnSc1
zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
3,73	3,73	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
třetí	třetí	k4xOgFnSc4
nejbližší	blízký	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
viditelnou	viditelný	k2eAgFnSc4d1
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
<g/>
,	,	kIx,
po	po	k7c6
Alfě	alfa	k1gFnSc6
Centauri	Centaur	k1gFnSc6
a	a	k8xC
Síriu	Sírius	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Stáří	stáří	k1gNnSc1
Ranu	Rana	k1gFnSc4
je	být	k5eAaImIp3nS
odhadováno	odhadovat	k5eAaImNgNnS
pouze	pouze	k6eAd1
na	na	k7c4
méně	málo	k6eAd2
než	než	k8xS
miliardu	miliarda	k4xCgFnSc4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
úroveň	úroveň	k1gFnSc4
magnetické	magnetický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
než	než	k8xS
současné	současný	k2eAgNnSc1d1
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
hvězdným	hvězdný	k2eAgInSc7d1
větrem	vítr	k1gInSc7
dosahujícím	dosahující	k2eAgInSc7d1
až	až	k6eAd1
30	#num#	k4
<g/>
-násobné	-násobný	k2eAgFnSc2d1
intenzity	intenzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rotační	rotační	k2eAgFnSc1d1
perioda	perioda	k1gFnSc1
Ranu	Rana	k1gFnSc4
je	být	k5eAaImIp3nS
11,2	11,2	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
menší	malý	k2eAgMnSc1d2
než	než	k8xS
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
porovnatelně	porovnatelně	k6eAd1
nižším	nízký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
prvků	prvek	k1gInPc2
těžších	těžký	k2eAgMnPc2d2
než	než	k8xS
helium	helium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hvězdu	hvězda	k1gFnSc4
hlavní	hlavní	k2eAgFnSc2d1
posloupnosti	posloupnost	k1gFnSc2
<g/>
,	,	kIx,
spektrální	spektrální	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
K	k	k7c3
<g/>
2	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
energie	energie	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
jádře	jádro	k1gNnSc6
hvězdy	hvězda	k1gFnSc2
jadernou	jaderný	k2eAgFnSc7d1
fůzí	fůz	k1gFnSc7
vodíku	vodík	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
hvězdy	hvězda	k1gFnSc2
vyzařovaná	vyzařovaný	k2eAgFnSc1d1
při	při	k7c6
teplotě	teplota	k1gFnSc6
přibližně	přibližně	k6eAd1
5	#num#	k4
000	#num#	k4
K	K	kA
<g/>
,	,	kIx,
dávající	dávající	k2eAgFnSc4d1
jí	on	k3xPp3gFnSc3
oranžové	oranžový	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Epsilon	epsilon	k1gNnSc2
Eridani	Eridaň	k1gFnSc3
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
I	I	kA
<g/>
/	/	kIx~
<g/>
131	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
SAO	SAO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centre	centr	k1gInSc5
de	de	k?
Données	Données	k1gMnSc1
astronomiques	astronomiques	k1gMnSc1
de	de	k?
Strasbourg	Strasbourg	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IAU	IAU	kA
Catalog	Catalog	k1gMnSc1
of	of	k?
Star	Star	kA
Names	Names	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IAU	IAU	kA
Division	Division	k1gInSc4
C	C	kA
Working	Working	k1gInSc1
Group	Group	k1gInSc1
on	on	k3xPp3gMnSc1
Star	Star	kA
Names	Names	k1gMnSc1
(	(	kIx(
<g/>
WGSN	WGSN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2020-03-15	2020-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JANSON	JANSON	kA
<g/>
,	,	kIx,
Markus	Markus	k1gMnSc1
<g/>
;	;	kIx,
REFFERT	REFFERT	kA
<g/>
,	,	kIx,
Sabine	Sabin	k1gMnSc5
<g/>
;	;	kIx,
BRANDNER	BRANDNER	kA
<g/>
,	,	kIx,
Wolfgang	Wolfgang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
comprehensive	comprehensivat	k5eAaPmIp3nS
examination	examination	k1gInSc4
of	of	k?
the	the	k?
Eps	Eps	k1gMnSc7
Eri	Eri	k1gMnSc7
system	syst	k1gInSc7
--	--	k?
Verification	Verification	k1gInSc1
of	of	k?
a	a	k8xC
4	#num#	k4
micron	micron	k1gMnSc1
narrow-band	narrow-banda	k1gFnPc2
high-contrast	high-contrast	k5eAaPmF
imaging	imaging	k1gInSc4
approach	approach	k1gInSc4
for	forum	k1gNnPc2
planet	planeta	k1gFnPc2
searches	searchesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomy	astronom	k1gMnPc4
&	&	k?
Astrophysics	Astrophysics	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
488	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
771	#num#	k4
<g/>
–	–	k?
<g/>
780	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ArXiv	ArXiva	k1gFnPc2
<g/>
:	:	kIx,
0	#num#	k4
<g/>
807.030	807.030	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6361	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6361	#num#	k4
<g/>
:	:	kIx,
<g/>
200809984	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FOLCO	FOLCO	kA
<g/>
,	,	kIx,
E.	E.	kA
Di	Di	k1gFnSc1
<g/>
;	;	kIx,
THÉVENIN	THÉVENIN	kA
<g/>
,	,	kIx,
F.	F.	kA
<g/>
;	;	kIx,
KERVELLA	KERVELLA	kA
<g/>
,	,	kIx,
P.	P.	kA
VLTI	VLTI	kA
near-IR	near-IR	k?
interferometric	interferometric	k1gMnSc1
observations	observations	k6eAd1
of	of	k?
Vega-like	Vega-like	k1gInSc1
stars	stars	k1gInSc1
-	-	kIx~
Radius	Radius	k1gInSc1
and	and	k?
age	age	k?
of	of	k?
α	α	k?
PsA	pes	k1gMnSc2
<g/>
,	,	kIx,
β	β	k?
Leo	Leo	k1gMnSc1
<g/>
,	,	kIx,
β	β	k?
Pic	pic	k0
<g/>
,	,	kIx,
ϵ	ϵ	k?
Eri	Eri	k1gFnSc1
and	and	k?
τ	τ	k?
Cet	ceta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomy	astronom	k1gMnPc4
&	&	k?
Astrophysics	Astrophysics	k1gInSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
426	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
601	#num#	k4
<g/>
–	–	k?
<g/>
617	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6361	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6361	#num#	k4
<g/>
:	:	kIx,
<g/>
20047189	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Epsilon	epsilon	k1gNnSc2
Eridani	Eridaň	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Epsilon	epsilon	k1gNnPc1
Eridani	Eridan	k1gMnPc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Hubblova	Hubblův	k2eAgInSc2d1
dalekohledu	dalekohled	k1gInSc2
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdy	hvězda	k1gFnPc1
souhvězdí	souhvězdí	k1gNnSc2
Eridanu	Eridan	k1gInSc2
Bayer	Bayer	k1gMnSc1
</s>
<s>
α	α	k?
(	(	kIx(
<g/>
Achernar	Achernar	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
β	β	k?
(	(	kIx(
<g/>
Cursa	Cursa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
γ	γ	k?
(	(	kIx(
<g/>
Zaurak	Zaurak	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
δ	δ	k?
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
ε	ε	k?
(	(	kIx(
<g/>
Sadira	Sadira	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
ζ	ζ	k?
(	(	kIx(
<g/>
Zibal	Zibal	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
η	η	k?
(	(	kIx(
<g/>
Azha	Azha	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
θ	θ	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Acamar	Acamar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
ι	ι	k?
•	•	k?
κ	κ	k?
•	•	k?
λ	λ	k?
•	•	k?
μ	μ	k?
•	•	k?
ν	ν	k?
•	•	k?
ξ	ξ	k?
•	•	k?
ο	ο	k?
(	(	kIx(
<g/>
Beid	Beid	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
ο	ο	k?
(	(	kIx(
<g/>
Keid	Keid	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
π	π	k?
•	•	k?
ρ	ρ	k?
•	•	k?
ρ	ρ	k?
•	•	k?
ρ	ρ	k?
•	•	k?
τ	τ	k?
•	•	k?
τ	τ	k?
(	(	kIx(
<g/>
Angetenar	Angetenar	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
τ	τ	k?
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
τ	τ	k?
<g/>
4	#num#	k4
•	•	k?
τ	τ	k?
<g/>
5	#num#	k4
•	•	k?
τ	τ	k?
<g/>
6	#num#	k4
•	•	k?
τ	τ	k?
<g/>
7	#num#	k4
•	•	k?
τ	τ	k?
<g/>
8	#num#	k4
•	•	k?
τ	τ	k?
<g/>
9	#num#	k4
•	•	k?
υ	υ	k?
(	(	kIx(
<g/>
Theemin	Theemin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
υ	υ	k?
(	(	kIx(
<g/>
Beemin	Beemin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
υ	υ	k?
•	•	k?
υ	υ	k?
<g/>
4	#num#	k4
•	•	k?
φ	φ	k?
•	•	k?
χ	χ	k?
•	•	k?
ψ	ψ	k?
•	•	k?
ω	ω	k?
•	•	k?
b	b	k?
•	•	k?
c	c	k0
•	•	k?
d	d	k?
•	•	k?
e	e	k0
(	(	kIx(
<g/>
82	#num#	k4
G.	G.	kA
<g/>
)	)	kIx)
•	•	k?
f	f	k?
•	•	k?
g	g	kA
•	•	k?
h	h	k?
•	•	k?
i	i	k8xC
•	•	k?
l	l	kA
(	(	kIx(
<g/>
Sceptrum	sceptrum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
p	p	k?
•	•	k?
q¹	q¹	k?
•	•	k?
q²	q²	k?
•	•	k?
s	s	k7c7
•	•	k?
v	v	k7c6
•	•	k?
w	w	k?
•	•	k?
y	y	k?
•	•	k?
A	a	k9
Flamsteed	Flamsteed	k1gMnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
)	)	kIx)
•	•	k?
2	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
,	,	kIx,
Angetenar	Angetenar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
3	#num#	k4
(	(	kIx(
<g/>
η	η	k?
<g/>
,	,	kIx,
Azha	Azha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
(	(	kIx(
<g/>
ρ	ρ	k?
<g/>
)	)	kIx)
•	•	k?
9	#num#	k4
(	(	kIx(
<g/>
ρ	ρ	k?
<g/>
)	)	kIx)
•	•	k?
10	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
ρ	ρ	k?
<g/>
)	)	kIx)
•	•	k?
11	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
)	)	kIx)
•	•	k?
13	#num#	k4
(	(	kIx(
<g/>
ζ	ζ	k?
<g/>
,	,	kIx,
Zibal	Zibal	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
17	#num#	k4
•	•	k?
18	#num#	k4
(	(	kIx(
<g/>
ε	ε	k?
<g/>
,	,	kIx,
Sadira	Sadira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
19	#num#	k4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
τ	τ	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
20	#num#	k4
•	•	k?
21	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
(	(	kIx(
<g/>
δ	δ	k?
<g/>
,	,	kIx,
Rana	Rana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
24	#num#	k4
•	•	k?
25	#num#	k4
•	•	k?
26	#num#	k4
(	(	kIx(
<g/>
π	π	k?
<g/>
)	)	kIx)
•	•	k?
27	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
28	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
29	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
32	#num#	k4
(	(	kIx(
<g/>
w	w	k?
<g/>
)	)	kIx)
•	•	k?
33	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
34	#num#	k4
(	(	kIx(
<g/>
γ	γ	k?
<g/>
,	,	kIx,
Zaurak	Zaurak	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
35	#num#	k4
•	•	k?
36	#num#	k4
(	(	kIx(
<g/>
τ	τ	k?
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
37	#num#	k4
•	•	k?
38	#num#	k4
(	(	kIx(
<g/>
ο	ο	k?
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Beid	Beid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
39	#num#	k4
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
•	•	k?
40	#num#	k4
(	(	kIx(
<g/>
ο	ο	k?
<g/>
,	,	kIx,
Keid	Keid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
41	#num#	k4
(	(	kIx(
<g/>
υ	υ	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
42	#num#	k4
(	(	kIx(
<g/>
ξ	ξ	k?
<g/>
)	)	kIx)
•	•	k?
43	#num#	k4
(	(	kIx(
<g/>
υ	υ	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
45	#num#	k4
•	•	k?
46	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
47	#num#	k4
•	•	k?
48	#num#	k4
(	(	kIx(
<g/>
ν	ν	k?
<g/>
)	)	kIx)
•	•	k?
50	#num#	k4
(	(	kIx(
<g/>
υ	υ	k?
<g/>
,	,	kIx,
Theemin	Theemin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
51	#num#	k4
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
•	•	k?
52	#num#	k4
(	(	kIx(
<g/>
υ	υ	k?
<g/>
,	,	kIx,
Beemin	Beemin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
53	#num#	k4
(	(	kIx(
<g/>
l	l	kA
<g/>
,	,	kIx,
Sceptrum	sceptrum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
54	#num#	k4
•	•	k?
55	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
56	#num#	k4
•	•	k?
57	#num#	k4
(	(	kIx(
<g/>
μ	μ	k?
<g/>
)	)	kIx)
•	•	k?
58	#num#	k4
•	•	k?
59	#num#	k4
•	•	k?
60	#num#	k4
•	•	k?
61	#num#	k4
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
•	•	k?
62	#num#	k4
(	(	kIx(
<g/>
b	b	k?
<g/>
)	)	kIx)
•	•	k?
63	#num#	k4
•	•	k?
64	#num#	k4
•	•	k?
65	#num#	k4
(	(	kIx(
<g/>
ψ	ψ	k?
<g/>
)	)	kIx)
•	•	k?
66	#num#	k4
•	•	k?
67	#num#	k4
(	(	kIx(
<g/>
β	β	k?
<g/>
,	,	kIx,
Cursa	Cursa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
68	#num#	k4
•	•	k?
69	#num#	k4
(	(	kIx(
<g/>
λ	λ	k?
<g/>
)	)	kIx)
Nejbližší	blízký	k2eAgFnPc1d3
hvězdy	hvězda	k1gFnPc1
</s>
<s>
ε	ε	k?
(	(	kIx(
<g/>
Sadira	Sadira	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
DEN	den	k1gInSc1
0255-4700	0255-4700	k4
•	•	k?
ο	ο	k?
(	(	kIx(
<g/>
Keid	Keid	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
LHS	LHS	kA
1723	#num#	k4
•	•	k?
2MASS	2MASS	k4
0415-0935	0415-0935	k4
•	•	k?
e	e	k0
(	(	kIx(
<g/>
82	#num#	k4
G.	G.	kA
<g/>
)	)	kIx)
•	•	k?
LP	LP	kA
771-095	771-095	k4
•	•	k?
p	p	k?
•	•	k?
LP	LP	kA
655-48	655-48	k4
•	•	k?
HR	hr	k2eAgInPc2d1
1614	#num#	k4
•	•	k?
δ	δ	k?
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
hvězd	hvězda	k1gFnPc2
souhvězdí	souhvězdí	k1gNnPc2
Eridanu	Eridan	k1gMnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
