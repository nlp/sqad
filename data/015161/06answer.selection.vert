<s desamb="1">
Rotační	rotační	k2eAgFnSc1d1
perioda	perioda	k1gFnSc1
Ranu	Rana	k1gFnSc4
je	být	k5eAaImIp3nS
11,2	11,2	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
menší	malý	k2eAgMnSc1d2
než	než	k8xS
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
porovnatelně	porovnatelně	k6eAd1
nižším	nízký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
prvků	prvek	k1gInPc2
těžších	těžký	k2eAgMnPc2d2
než	než	k8xS
helium	helium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hvězdu	hvězda	k1gFnSc4
hlavní	hlavní	k2eAgFnSc2d1
posloupnosti	posloupnost	k1gFnSc2
<g/>
,	,	kIx,
spektrální	spektrální	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
K	k	k7c3
<g/>
2	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
energie	energie	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
jádře	jádro	k1gNnSc6
hvězdy	hvězda	k1gFnSc2
jadernou	jaderný	k2eAgFnSc7d1
fůzí	fůz	k1gFnSc7
vodíku	vodík	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
hvězdy	hvězda	k1gFnSc2
vyzařovaná	vyzařovaný	k2eAgFnSc1d1
při	při	k7c6
teplotě	teplota	k1gFnSc6
přibližně	přibližně	k6eAd1
5	#num#	k4
000	#num#	k4
K	K	kA
<g/>
,	,	kIx,
dávající	dávající	k2eAgFnSc4d1
jí	on	k3xPp3gFnSc3
oranžové	oranžový	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
<g/>
.	.	kIx.
</s>