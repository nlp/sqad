<s>
Binární	binární	k2eAgFnSc1d1	binární
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgFnSc1d1	idempotentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
její	její	k3xOp3gFnPc4	její
aplikace	aplikace	k1gFnPc4	aplikace
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
totožné	totožný	k2eAgInPc4d1	totožný
prvky	prvek	k1gInPc4	prvek
získáme	získat	k5eAaPmIp1nP	získat
původní	původní	k2eAgMnPc1d1	původní
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
