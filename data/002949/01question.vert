<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vlastnost	vlastnost	k1gFnSc1	vlastnost
určitého	určitý	k2eAgInSc2d1	určitý
prvku	prvek	k1gInSc2	prvek
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
prvek	prvek	k1gInSc4	prvek
vynásobit	vynásobit	k5eAaPmF	vynásobit
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
?	?	kIx.	?
</s>
