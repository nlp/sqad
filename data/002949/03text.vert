<s>
Idempotence	Idempotence	k1gFnSc1	Idempotence
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
algebře	algebra	k1gFnSc6	algebra
<g/>
,	,	kIx,	,
vlastnost	vlastnost	k1gFnSc1	vlastnost
algebraických	algebraický	k2eAgFnPc2d1	algebraická
operací	operace	k1gFnPc2	operace
či	či	k8xC	či
prvků	prvek	k1gInPc2	prvek
nějaké	nějaký	k3yIgFnSc2	nějaký
algebry	algebra	k1gFnSc2	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgFnSc1d1	idempotentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jejím	její	k3xOp3gNnSc7	její
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
použitím	použití	k1gNnSc7	použití
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
vstup	vstup	k1gInSc4	vstup
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
stejný	stejný	k2eAgInSc4d1	stejný
výstup	výstup	k1gInSc4	výstup
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jediným	jediný	k2eAgNnSc7d1	jediné
použitím	použití	k1gNnSc7	použití
dané	daný	k2eAgFnSc2d1	daná
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
například	například	k6eAd1	například
v	v	k7c6	v
lineární	lineární	k2eAgFnSc6d1	lineární
algebře	algebra	k1gFnSc6	algebra
u	u	k7c2	u
projekcí	projekce	k1gFnPc2	projekce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
definičních	definiční	k2eAgFnPc2d1	definiční
vlastností	vlastnost	k1gFnPc2	vlastnost
uzávěrového	uzávěrový	k2eAgInSc2d1	uzávěrový
operátoru	operátor	k1gInSc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
idempotence	idempotence	k1gFnSc2	idempotence
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
Benjamin	Benjamin	k1gMnSc1	Benjamin
Peirce	Peirce	k1gMnSc1	Peirce
v	v	k7c6	v
algebře	algebra	k1gFnSc6	algebra
pro	pro	k7c4	pro
ty	ten	k3xDgInPc4	ten
prvky	prvek	k1gInPc4	prvek
nějaké	nějaký	k3yIgFnSc2	nějaký
algebry	algebra	k1gFnSc2	algebra
s	s	k7c7	s
násobením	násobení	k1gNnSc7	násobení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
netečné	tečný	k2eNgFnPc1d1	netečná
vůči	vůči	k7c3	vůči
mocnění	mocnění	k1gNnSc3	mocnění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
může	moct	k5eAaImIp3nS	moct
idempotence	idempotence	k1gFnSc1	idempotence
nabývat	nabývat	k5eAaImF	nabývat
různých	různý	k2eAgInPc2d1	různý
významů	význam	k1gInPc2	význam
<g/>
:	:	kIx,	:
Unární	unární	k2eAgFnPc4d1	unární
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgNnSc1d1	idempotentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
její	její	k3xOp3gFnSc7	její
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
aplikací	aplikace	k1gFnSc7	aplikace
získáme	získat	k5eAaPmIp1nP	získat
totéž	týž	k3xTgNnSc1	týž
jako	jako	k8xC	jako
jednou	jeden	k4xCgFnSc7	jeden
aplikací	aplikace	k1gFnSc7	aplikace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
vstup	vstup	k1gInSc4	vstup
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
rovnost	rovnost	k1gFnSc1	rovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
například	například	k6eAd1	například
pro	pro	k7c4	pro
identitu	identita	k1gFnSc4	identita
či	či	k8xC	či
konstantní	konstantní	k2eAgFnSc4d1	konstantní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Binární	binární	k2eAgFnSc1d1	binární
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgFnSc1d1	idempotentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
její	její	k3xOp3gFnPc4	její
aplikace	aplikace	k1gFnPc4	aplikace
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
totožné	totožný	k2eAgInPc4d1	totožný
prvky	prvek	k1gInPc4	prvek
získáme	získat	k5eAaPmIp1nP	získat
původní	původní	k2eAgMnPc1d1	původní
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výpočet	výpočet	k1gInSc1	výpočet
minima	minimum	k1gNnSc2	minimum
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
reálných	reálný	k2eAgFnPc2d1	reálná
hodnot	hodnota	k1gFnPc2	hodnota
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgFnSc1d1	idempotentní
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
reálné	reálný	k2eAgInPc4d1	reálný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
rovnost	rovnost	k1gFnSc1	rovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
min	mina	k1gFnPc2	mina
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
min	mina	k1gFnPc2	mina
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zadanou	zadaný	k2eAgFnSc4d1	zadaná
binární	binární	k2eAgFnSc4d1	binární
operaci	operace	k1gFnSc4	operace
nazýváme	nazývat	k5eAaImIp1nP	nazývat
prvek	prvek	k1gInSc4	prvek
idempotentním	idempotentní	k2eAgInSc6d1	idempotentní
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
použití	použití	k1gNnSc1	použití
operace	operace	k1gFnSc2	operace
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
prvek	prvek	k1gInSc4	prvek
znovu	znovu	k6eAd1	znovu
vrací	vracet	k5eAaImIp3nS	vracet
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
násobení	násobení	k1gNnPc4	násobení
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
idempotentním	idempotentní	k2eAgInSc7d1	idempotentní
prvkem	prvek	k1gInSc7	prvek
<g/>
:	:	kIx,	:
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
⋅	⋅	k?	⋅
0	[number]	k4	0
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
idempotentní	idempotentní	k2eAgMnPc1d1	idempotentní
není	být	k5eNaImIp3nS	být
<g/>
:	:	kIx,	:
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
⋅	⋅	k?	⋅
2	[number]	k4	2
≠	≠	k?	≠
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
2	[number]	k4	2
<g/>
}	}	kIx)	}
.	.	kIx.	.
sjednocení	sjednocení	k1gNnSc1	sjednocení
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgMnSc1d1	idempotentní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
platí	platit	k5eAaImIp3nP	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
∪	∪	k?	∪
A	A	kA	A
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
A	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
.	.	kIx.	.
průnik	průnik	k1gInSc1	průnik
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgMnSc1d1	idempotentní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
platí	platit	k5eAaImIp3nP	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
∩	∩	k?	∩
A	A	kA	A
=	=	kIx~	=
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
daná	daný	k2eAgFnSc1d1	daná
operace	operace	k1gFnSc1	operace
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
žádný	žádný	k3yNgInSc4	žádný
idempotentní	idempotentní	k2eAgInSc4d1	idempotentní
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
idempotentního	idempotentní	k2eAgInSc2d1	idempotentní
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
