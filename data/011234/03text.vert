<p>
<s>
Mercosur	Mercosur	k1gMnSc1	Mercosur
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Mercado	Mercada	k1gFnSc5	Mercada
Común	Común	k1gMnSc1	Común
del	del	k?	del
Sur	Sur	k1gMnSc1	Sur
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Mercado	Mercada	k1gFnSc5	Mercada
Comum	Comum	k1gNnSc1	Comum
do	do	k7c2	do
Sul	sout	k5eAaImAgMnS	sout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sdružení	sdružení	k1gNnSc1	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgInPc1d1	zakládající
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgFnP	být
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Asunciónu	Asunción	k1gInSc2	Asunción
(	(	kIx(	(
<g/>
Tratado	Tratada	k1gFnSc5	Tratada
de	de	k?	de
Asunción	Asunción	k1gInSc1	Asunción
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
uruguayské	uruguayský	k2eAgNnSc4d1	Uruguayské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
.	.	kIx.	.
</s>
<s>
Celní	celní	k2eAgFnSc1d1	celní
unie	unie	k1gFnSc1	unie
mezi	mezi	k7c7	mezi
zúčastněnými	zúčastněný	k2eAgInPc7d1	zúčastněný
státy	stát	k1gInPc7	stát
byla	být	k5eAaImAgFnS	být
dobudována	dobudovat	k5eAaPmNgFnS	dobudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
doplnilo	doplnit	k5eAaPmAgNnS	doplnit
sdružení	sdružení	k1gNnSc1	sdružení
ustavující	ustavující	k2eAgNnSc1d1	ustavující
dokument	dokument	k1gInSc4	dokument
o	o	k7c4	o
nutnost	nutnost	k1gFnSc4	nutnost
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
být	být	k5eAaImF	být
demokraciemi	demokracie	k1gFnPc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
demokratická	demokratický	k2eAgFnSc1d1	demokratická
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přerušit	přerušit	k5eAaPmF	přerušit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
kooperaci	kooperace	k1gFnSc4	kooperace
se	s	k7c7	s
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
demokracii	demokracie	k1gFnSc4	demokracie
nedodržuje	dodržovat	k5eNaImIp3nS	dodržovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
vstup	vstup	k1gInSc1	vstup
Venezuely	Venezuela	k1gFnSc2	Venezuela
jako	jako	k8xS	jako
plnohodnotného	plnohodnotný	k2eAgMnSc2d1	plnohodnotný
člena	člen	k1gMnSc2	člen
do	do	k7c2	do
Mercosuru	Mercosur	k1gInSc2	Mercosur
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vstup	vstup	k1gInSc1	vstup
byl	být	k5eAaImAgInS	být
podmíněn	podmínit	k5eAaPmNgInS	podmínit
ratifikací	ratifikace	k1gFnSc7	ratifikace
v	v	k7c6	v
parlamentech	parlament	k1gInPc6	parlament
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
<g/>
;	;	kIx,	;
posledním	poslední	k2eAgInSc7d1	poslední
sborem	sbor	k1gInSc7	sbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neprojednal	projednat	k5eNaPmAgInS	projednat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
paraguayský	paraguayský	k2eAgInSc1d1	paraguayský
senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
byla	být	k5eAaImAgFnS	být
však	však	k9	však
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
slavnostně	slavnostně	k6eAd1	slavnostně
přijata	přijmout	k5eAaPmNgFnS	přijmout
za	za	k7c4	za
plnohodnotného	plnohodnotný	k2eAgMnSc4d1	plnohodnotný
člena	člen	k1gMnSc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bylo	být	k5eAaImAgNnS	být
členství	členství	k1gNnSc1	členství
Paraguaye	Paraguay	k1gFnSc2	Paraguay
pozastaveno	pozastavit	k5eAaPmNgNnS	pozastavit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
obviněním	obvinění	k1gNnSc7	obvinění
a	a	k8xC	a
odvoláním	odvolání	k1gNnSc7	odvolání
paraguayského	paraguayský	k2eAgMnSc2d1	paraguayský
prezidenta	prezident	k1gMnSc2	prezident
Fernanda	Fernando	k1gNnSc2	Fernando
Luga	Lugus	k1gMnSc2	Lugus
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
Mercosuru	Mercosura	k1gFnSc4	Mercosura
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Paraguay	Paraguay	k1gFnSc1	Paraguay
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Mercosuru	Mercosur	k1gInSc2	Mercosur
plně	plně	k6eAd1	plně
znovuzačleněna	znovuzačleněn	k2eAgFnSc1d1	znovuzačleněn
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
prezident	prezident	k1gMnSc1	prezident
Horacio	Horacio	k1gMnSc1	Horacio
Cartes	Cartes	k1gMnSc1	Cartes
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
i	i	k9	i
Paraguay	Paraguay	k1gFnSc1	Paraguay
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
začlenění	začlenění	k1gNnSc4	začlenění
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přidruženými	přidružený	k2eAgInPc7d1	přidružený
státy	stát	k1gInPc7	stát
organizace	organizace	k1gFnSc2	organizace
jsou	být	k5eAaImIp3nP	být
Chile	Chile	k1gNnPc1	Chile
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc1	Surinam
a	a	k8xC	a
Guyana	Guyana	k1gFnSc1	Guyana
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
srpen	srpen	k1gInSc1	srpen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přistupujícím	přistupující	k2eAgMnSc7d1	přistupující
členem	člen	k1gMnSc7	člen
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
vstup	vstup	k1gInSc1	vstup
Bolívie	Bolívie	k1gFnSc2	Bolívie
jako	jako	k8xS	jako
plnohodnotného	plnohodnotný	k2eAgMnSc2d1	plnohodnotný
člena	člen	k1gMnSc2	člen
do	do	k7c2	do
Mercosuru	Mercosur	k1gInSc2	Mercosur
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
však	však	k8xC	však
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
vstup	vstup	k1gInSc1	vstup
ratifikován	ratifikován	k2eAgInSc1d1	ratifikován
parlamenty	parlament	k1gInPc4	parlament
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
se	s	k7c7	s
státy	stát	k1gInPc7	stát
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
včetně	včetně	k7c2	včetně
států	stát	k1gInPc2	stát
současného	současný	k2eAgInSc2d1	současný
Mercosur	Mercosur	k1gMnSc1	Mercosur
velice	velice	k6eAd1	velice
slabé	slabý	k2eAgFnSc2d1	slabá
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
především	především	k9	především
na	na	k7c4	na
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
i	i	k8xC	i
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
vazby	vazba	k1gFnPc1	vazba
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
intenzivnějšími	intenzivní	k2eAgInPc7d2	intenzivnější
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
pak	pak	k9	pak
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
Dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
dle	dle	k7c2	dle
článků	článek	k1gInPc2	článek
113	[number]	k4	113
a	a	k8xC	a
225	[number]	k4	225
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
EHS	EHS	kA	EHS
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paraguayí	Paraguay	k1gFnSc7	Paraguay
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Uruguayí	Uruguay	k1gFnPc2	Uruguay
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
ES	es	k1gNnSc4	es
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
Rámcovou	rámcový	k2eAgFnSc4d1	rámcová
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
dle	dle	k7c2	dle
článků	článek	k1gInPc2	článek
133	[number]	k4	133
a	a	k8xC	a
308	[number]	k4	308
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
ES	ES	kA	ES
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
druhou	druhý	k4xOgFnSc7	druhý
Rámcovou	rámcový	k2eAgFnSc7d1	rámcová
dohodou	dohoda	k1gFnSc7	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
demokratizační	demokratizační	k2eAgFnSc4d1	demokratizační
doložku	doložka	k1gFnSc4	doložka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
demokratických	demokratický	k2eAgFnPc2d1	demokratická
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
naplňují	naplňovat	k5eAaImIp3nP	naplňovat
domácí	domácí	k2eAgFnSc4d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
Společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
součást	součást	k1gFnSc4	součást
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
smlouvou	smlouva	k1gFnSc7	smlouva
s	s	k7c7	s
Mercosur	Mercosur	k1gMnSc1	Mercosur
jako	jako	k8xS	jako
samotným	samotný	k2eAgInSc7d1	samotný
subjektem	subjekt	k1gInSc7	subjekt
je	být	k5eAaImIp3nS	být
Meziregionální	Meziregionální	k2eAgFnSc1d1	Meziregionální
rámcová	rámcový	k2eAgFnSc1d1	rámcová
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
mezi	mezi	k7c7	mezi
ES	ES	kA	ES
a	a	k8xC	a
Mercosur	Mercosur	k1gMnSc1	Mercosur
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
sjednána	sjednat	k5eAaPmNgFnS	sjednat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
a	a	k8xC	a
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vnesla	vnést	k5eAaPmAgFnS	vnést
do	do	k7c2	do
spolupráce	spolupráce	k1gFnSc2	spolupráce
obou	dva	k4xCgNnPc2	dva
uskupení	uskupení	k1gNnPc2	uskupení
nově	nově	k6eAd1	nově
i	i	k9	i
aspekt	aspekt	k1gInSc4	aspekt
politické	politický	k2eAgFnSc2d1	politická
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
obchodní	obchodní	k2eAgInSc1d1	obchodní
či	či	k8xC	či
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
základ	základ	k1gInSc4	základ
budoucí	budoucí	k2eAgFnSc2d1	budoucí
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
Latinskou	latinský	k2eAgFnSc7d1	Latinská
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgNnP	začít
vyjednávání	vyjednávání	k1gNnPc1	vyjednávání
o	o	k7c6	o
Asociační	asociační	k2eAgFnSc6d1	asociační
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
hlubší	hluboký	k2eAgInSc4d2	hlubší
politický	politický	k2eAgInSc4d1	politický
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,	,
kooperaci	kooperace	k1gFnSc4	kooperace
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnPc1	jednání
však	však	k9	však
byla	být	k5eAaImAgNnP	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
nad	nad	k7c7	nad
kapitolou	kapitola	k1gFnSc7	kapitola
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
obnovení	obnovení	k1gNnSc3	obnovení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
debaty	debata	k1gFnPc4	debata
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
i	i	k9	i
Dohoda	dohoda	k1gFnSc1	dohoda
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strategický	strategický	k2eAgInSc1d1	strategický
rámec	rámec	k1gInSc1	rámec
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
vztahů	vztah	k1gInPc2	vztah
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EU	EU	kA	EU
představuje	představovat	k5eAaImIp3nS	představovat
Latinskoamerický	latinskoamerický	k2eAgInSc4d1	latinskoamerický
regionální	regionální	k2eAgInSc4d1	regionální
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vymezoval	vymezovat	k5eAaImAgInS	vymezovat
alokaci	alokace	k1gFnSc4	alokace
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
rozpočtu	rozpočet	k1gInSc2	rozpočet
EU	EU	kA	EU
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
celkem	celek	k1gInSc7	celek
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prohloubit	prohloubit	k5eAaPmF	prohloubit
a	a	k8xC	a
rozšířit	rozšířit	k5eAaPmF	rozšířit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Mercosur	Mercosura	k1gFnPc2	Mercosura
<g/>
,	,	kIx,	,
redukovat	redukovat	k5eAaBmF	redukovat
chudobu	chudoba	k1gFnSc4	chudoba
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
nerovnosti	nerovnost	k1gFnPc4	nerovnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
podpořit	podpořit	k5eAaPmF	podpořit
regionální	regionální	k2eAgFnSc4d1	regionální
integraci	integrace	k1gFnSc4	integrace
a	a	k8xC	a
posílit	posílit	k5eAaPmF	posílit
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
porozumění	porozumění	k1gNnSc4	porozumění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Mercosur	Mercosura	k1gFnPc2	Mercosura
se	se	k3xPyFc4	se
nerozvíjí	rozvíjet	k5eNaImIp3nS	rozvíjet
pouze	pouze	k6eAd1	pouze
dialog	dialog	k1gInSc4	dialog
nad	nad	k7c7	nad
politickou	politický	k2eAgFnSc7d1	politická
spoluprací	spolupráce	k1gFnSc7	spolupráce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktéž	taktéž	k?	taktéž
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
celky	celek	k1gInPc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
Mercosuru	Mercosur	k1gInSc2	Mercosur
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
EU	EU	kA	EU
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
Mercosur	Mercosura	k1gFnPc2	Mercosura
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
41,5	[number]	k4	41,5
miliardy	miliarda	k4xCgFnSc2	miliarda
EUR	euro	k1gNnPc2	euro
a	a	k8xC	a
obráceně	obráceně	k6eAd1	obráceně
vývoz	vývoz	k1gInSc4	vývoz
Mercosuru	Mercosur	k1gInSc2	Mercosur
do	do	k7c2	do
EU	EU	kA	EU
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
40,6	[number]	k4	40,6
miliardy	miliarda	k4xCgFnPc4	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
exportní	exportní	k2eAgFnSc7d1	exportní
položkou	položka	k1gFnSc7	položka
do	do	k7c2	do
EU	EU	kA	EU
jsou	být	k5eAaImIp3nP	být
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
<g/>
,	,	kIx,	,
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
a	a	k8xC	a
živočišné	živočišný	k2eAgInPc1d1	živočišný
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
vývozy	vývoz	k1gInPc1	vývoz
EU	EU	kA	EU
do	do	k7c2	do
Mercosur	Mercosura	k1gFnPc2	Mercosura
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
strojní	strojní	k2eAgNnPc4d1	strojní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
či	či	k8xC	či
farmaceutické	farmaceutický	k2eAgInPc4d1	farmaceutický
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
ekonomiky	ekonomika	k1gFnPc1	ekonomika
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
představují	představovat	k5eAaImIp3nP	představovat
stále	stále	k6eAd1	stále
atraktivnější	atraktivní	k2eAgInPc4d2	atraktivnější
trhy	trh	k1gInPc4	trh
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
firmy	firma	k1gFnPc4	firma
a	a	k8xC	a
podnikatele	podnikatel	k1gMnPc4	podnikatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
současné	současný	k2eAgFnSc2d1	současná
diskuze	diskuze	k1gFnSc2	diskuze
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zóna	zóna	k1gFnSc1	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
států	stát	k1gInPc2	stát
Mercosur	Mercosura	k1gFnPc2	Mercosura
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
FTA	FTA	kA	FTA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
pro	pro	k7c4	pro
nedostatečnost	nedostatečnost	k1gFnSc4	nedostatečnost
nabídky	nabídka	k1gFnSc2	nabídka
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Mercosuru	Mercosur	k1gInSc2	Mercosur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
jednání	jednání	k1gNnSc4	jednání
znovu	znovu	k6eAd1	znovu
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
politické	politický	k2eAgFnSc3d1	politická
pozici	pozice	k1gFnSc3	pozice
Argentiny	Argentina	k1gFnSc2	Argentina
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnPc1	jednání
byla	být	k5eAaImAgNnP	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgNnP	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
a	a	k8xC	a
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
dostat	dostat	k5eAaPmF	dostat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	zářit	k5eAaImIp3nS	zářit
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
snížení	snížení	k1gNnSc2	snížení
cel	clo	k1gNnPc2	clo
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
EU	EU	kA	EU
být	být	k5eAaImF	být
zatím	zatím	k6eAd1	zatím
nejlukrativnější	lukrativní	k2eAgFnSc1d3	nejlukrativnější
obchodní	obchodní	k2eAgFnSc1d1	obchodní
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Spornými	sporný	k2eAgInPc7d1	sporný
body	bod	k1gInPc7	bod
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nadále	nadále	k6eAd1	nadále
evropské	evropský	k2eAgFnPc4d1	Evropská
dotace	dotace	k1gFnPc4	dotace
zemědělcům	zemědělec	k1gMnPc3	zemědělec
<g/>
,	,	kIx,	,
kvóty	kvóta	k1gFnPc4	kvóta
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
ochranářská	ochranářský	k2eAgNnPc1d1	ochranářské
opatření	opatření	k1gNnPc1	opatření
členů	člen	k1gMnPc2	člen
Mercosur	Mercosura	k1gFnPc2	Mercosura
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
automobilovém	automobilový	k2eAgInSc6d1	automobilový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
integrace	integrace	k1gFnSc1	integrace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mercosur	Mercosura	k1gFnPc2	Mercosura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
organizace	organizace	k1gFnSc2	organizace
</s>
</p>
