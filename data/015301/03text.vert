<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblast	oblast	k1gFnSc4
Český	český	k2eAgInSc1d1
lesIUCN	lesIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
,	,	kIx,
Huťský	huťský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
nedaleko	nedaleko	k7c2
Staré	Staré	k2eAgFnSc2d1
Knížecí	knížecí	k2eAgFnSc2d1
HutiZákladní	HutiZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
473	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Český	český	k2eAgInSc4d1
les	les	k1gInSc4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Přimda	Přimda	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
44	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.ceskyles.ochranaprirody.cz	www.ceskyles.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
473	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
80	#num#	k4
%	%	kIx~
pokryta	pokrýt	k5eAaPmNgFnS
lesem	les	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
nejcennější	cenný	k2eAgNnSc4d3
území	území	k1gNnSc4
příhraničního	příhraniční	k2eAgInSc2d1
pásu	pás	k1gInSc2
hor	hora	k1gFnPc2
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
od	od	k7c2
Broumova	Broumov	k1gInSc2
v	v	k7c6
okrese	okres	k1gInSc6
Tachov	Tachov	k1gInSc1
až	až	k6eAd1
po	po	k7c4
Českou	český	k2eAgFnSc4d1
Kubici	Kubica	k1gMnPc7
v	v	k7c6
okrese	okres	k1gInSc6
Domažlice	Domažlice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
ležela	ležet	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
CHKO	CHKO	kA
v	v	k7c6
hraničním	hraniční	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
zachovala	zachovat	k5eAaPmAgFnS
nenarušená	narušený	k2eNgFnSc1d1
příroda	příroda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
,	,	kIx,
rozdělených	rozdělený	k2eAgFnPc2d1
tělesem	těleso	k1gNnSc7
dálnice	dálnice	k1gFnSc2
D	D	kA
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
řadě	řad	k1gInSc6
míst	místo	k1gNnPc2
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgFnP
původní	původní	k2eAgFnPc1d1
cenné	cenný	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
<g/>
,	,	kIx,
vzácná	vzácný	k2eAgNnPc1d1
rašeliniště	rašeliniště	k1gNnPc1
s	s	k7c7
porosty	porost	k1gInPc7
borovice	borovice	k1gFnSc2
blatky	blatka	k1gFnSc2
a	a	k8xC
pestré	pestrý	k2eAgFnPc4d1
květnaté	květnatý	k2eAgFnPc4d1
louky	louka	k1gFnPc4
a	a	k8xC
pastviny	pastvina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
chráněných	chráněný	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
např.	např.	kA
ohrožená	ohrožený	k2eAgFnSc1d1
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
<g/>
,	,	kIx,
tetřívek	tetřívek	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
rys	rys	k1gMnSc1
ostrovid	ostrovid	k1gMnSc1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
lynx	lynx	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
bobr	bobr	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
Českém	český	k2eAgInSc6d1
lese	les	k1gInSc6
fotopast	fotopast	k1gFnSc4
poprvé	poprvé	k6eAd1
zachytila	zachytit	k5eAaPmAgFnS
rysa	rys	k1gMnSc4
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
sem	sem	k6eAd1
z	z	k7c2
Německa	Německo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgMnSc1d1
les	les	k1gInSc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Tabulky	tabulka	k1gFnPc1
a	a	k8xC
mapy	mapa	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
zelený	zelený	k2eAgInSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Domažlice	Domažlice	k1gFnPc1
Přírodní	přírodní	k2eAgFnPc1d1
parky	park	k1gInPc4
</s>
<s>
Sedmihoří	Sedmihořit	k5eAaPmIp3nP,k5eAaImIp3nP
•	•	k?
Zelenov	Zelenov	k1gInSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čerchovské	Čerchovský	k2eAgInPc1d1
hvozdy	hvozd	k1gInPc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bystřice	Bystřice	k1gFnSc1
•	•	k?
Dlouhý	dlouhý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Drahotínský	Drahotínský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Herštýn	Herštýn	k1gInSc1
•	•	k?
Jezvinec	Jezvinec	k1gInSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Zvon	zvon	k1gInSc1
•	•	k?
Nad	nad	k7c7
Hutí	huť	k1gFnSc7
•	•	k?
Netřeb	Netřba	k1gFnPc2
•	•	k?
Pleš	pleš	k1gFnSc4
•	•	k?
Postřekovské	Postřekovský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Smrčí	smrčí	k1gNnSc1
•	•	k?
Starý	starý	k2eAgMnSc1d1
Hirštejn	Hirštejn	k1gMnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Červený	červený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Hora	hora	k1gFnSc1
•	•	k?
Hvožďanská	Hvožďanský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Chodovské	chodovský	k2eAgFnPc4d1
skály	skála	k1gFnPc4
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Staré	Staré	k2eAgFnSc2d1
Huti	huť	k1gFnSc2
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Šnajberského	Šnajberský	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
•	•	k?
Mlýneček	mlýneček	k1gInSc1
•	•	k?
Mutěnínský	Mutěnínský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Orlovická	Orlovický	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Salka	Salka	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
na	na	k7c6
Sádku	sádek	k1gInSc6
•	•	k?
Sokolova	Sokolův	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
•	•	k?
Veský	veský	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Tachov	Tachov	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hadovka	hadovka	k1gFnSc1
•	•	k?
Sedmihoří	Sedmihoř	k1gFnPc2
•	•	k?
Úterský	Úterský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Valcha	valcha	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Na	na	k7c6
požárech	požár	k1gInPc6
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Broumovská	broumovský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Bučina	bučina	k1gFnSc1
u	u	k7c2
Žďáru	Žďár	k1gInSc2
•	•	k?
Diana	Diana	k1gFnSc1
•	•	k?
Farské	farský	k2eAgFnSc2d1
bažiny	bažina	k1gFnSc2
•	•	k?
Hradištský	Hradištský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Jezírka	jezírko	k1gNnSc2
u	u	k7c2
Rozvadova	Rozvadův	k2eAgInSc2d1
•	•	k?
Křížový	křížový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Lazurový	lazurový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Mělký	mělký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Ostrůvek	ostrůvek	k1gInSc1
•	•	k?
Pavlova	Pavlův	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Pavlovická	pavlovický	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Volfštejnem	Volfštejn	k1gInSc7
•	•	k?
Podkovák	Podkovák	k?
•	•	k?
Přimda	Přimda	k1gMnSc1
•	•	k?
Tisovské	tisovský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Tišina	tišina	k1gFnSc1
•	•	k?
U	u	k7c2
rybníčků	rybníček	k1gInPc2
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bonětice	Bonětice	k1gFnSc1
•	•	k?
Černošínský	Černošínský	k2eAgInSc1d1
bor	bor	k1gInSc1
•	•	k?
Čiperka	čiperka	k1gFnSc1
•	•	k?
Kolowratův	Kolowratův	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Krasíkov	Krasíkov	k1gInSc1
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Prostředního	prostřední	k2eAgInSc2d1
Žďáru	Žďár	k1gInSc2
•	•	k?
Maršovy	Maršovy	k?
Chody	chod	k1gInPc1
•	•	k?
Milov	Milov	k1gInSc1
•	•	k?
Na	na	k7c4
Kolmu	Kolma	k1gFnSc4
•	•	k?
Niva	niva	k1gFnSc1
Bílého	bílý	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Petrské	petrský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Pístovská	Pístovský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Šipínem	Šipín	k1gInSc7
•	•	k?
Prameniště	prameniště	k1gNnSc4
Kateřinského	kateřinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Racovské	Racovský	k2eAgInPc1d1
rybníčky	rybníček	k1gInPc1
•	•	k?
Šelmberk	Šelmberk	k1gInSc1
•	•	k?
Valcha	valcha	k1gFnSc1
•	•	k?
Žďár	Žďár	k1gInSc1
u	u	k7c2
Chodského	chodský	k2eAgNnSc2d1
Újezda	Újezdo	k1gNnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
