<p>
<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
(	(	kIx(	(
<g/>
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
západní	západní	k2eAgInPc4d1	západní
sektory	sektor	k1gInPc4	sektor
Berlína	Berlín	k1gInSc2	Berlín
zásobovány	zásobovat	k5eAaImNgInP	zásobovat
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
takzvaným	takzvaný	k2eAgInSc7d1	takzvaný
leteckým	letecký	k2eAgInSc7d1	letecký
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejzávažnějším	závažný	k2eAgFnPc3d3	nejzávažnější
konfrontacím	konfrontace	k1gFnPc3	konfrontace
poválečného	poválečný	k2eAgNnSc2d1	poválečné
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
provedli	provést	k5eAaPmAgMnP	provést
západní	západní	k2eAgMnPc1d1	západní
spojenci	spojenec	k1gMnPc1	spojenec
–	–	k?	–
po	po	k7c6	po
bezúspěšných	bezúspěšný	k2eAgFnPc6d1	bezúspěšná
konzultacích	konzultace	k1gFnPc6	konzultace
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
–	–	k?	–
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
okupačních	okupační	k2eAgFnPc6d1	okupační
zónách	zóna	k1gFnPc6	zóna
Německa	Německo	k1gNnSc2	Německo
měnovou	měnový	k2eAgFnSc4d1	měnová
reformu	reforma	k1gFnSc4	reforma
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čtyřmocenského	čtyřmocenský	k2eAgInSc2d1	čtyřmocenský
statusu	status	k1gInSc2	status
neměla	mít	k5eNaImAgFnS	mít
platit	platit	k5eAaImF	platit
pro	pro	k7c4	pro
Berlín	Berlín	k1gInSc4	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byla	být	k5eAaImAgFnS	být
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
provedena	provést	k5eAaPmNgFnS	provést
i	i	k9	i
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nová	nový	k2eAgFnSc1d1	nová
východní	východní	k2eAgFnSc1d1	východní
marka	marka	k1gFnSc1	marka
měla	mít	k5eAaImAgFnS	mít
platit	platit	k5eAaImF	platit
i	i	k9	i
pro	pro	k7c4	pro
západní	západní	k2eAgInPc4d1	západní
sektory	sektor	k1gInPc4	sektor
Berlína	Berlín	k1gInSc2	Berlín
<g/>
:	:	kIx,	:
tím	ten	k3xDgNnSc7	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
finančního	finanční	k2eAgNnSc2d1	finanční
a	a	k8xC	a
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
vázání	vázání	k1gNnSc2	vázání
západních	západní	k2eAgInPc2d1	západní
sektorů	sektor	k1gInPc2	sektor
na	na	k7c4	na
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
spojenci	spojenec	k1gMnPc1	spojenec
proto	proto	k8xC	proto
zavedli	zavést	k5eAaPmAgMnP	zavést
západní	západní	k2eAgFnSc4d1	západní
marku	marka	k1gFnSc4	marka
i	i	k9	i
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
berlínských	berlínský	k2eAgInPc6d1	berlínský
sektorech	sektor	k1gInPc6	sektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
pak	pak	k6eAd1	pak
sovětské	sovětský	k2eAgNnSc1d1	sovětské
velení	velení	k1gNnSc1	velení
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
přerušením	přerušení	k1gNnSc7	přerušení
zásobování	zásobování	k1gNnSc2	zásobování
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
pro	pro	k7c4	pro
západní	západní	k2eAgInPc4d1	západní
sektory	sektor	k1gInPc4	sektor
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
uzavřením	uzavření	k1gNnSc7	uzavření
všech	všecek	k3xTgFnPc2	všecek
pozemních	pozemní	k2eAgFnPc2d1	pozemní
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
přístupových	přístupový	k2eAgFnPc2d1	přístupová
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Blokáda	blokáda	k1gFnSc1	blokáda
a	a	k8xC	a
letecký	letecký	k2eAgInSc1d1	letecký
most	most	k1gInSc1	most
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
blokádu	blokáda	k1gFnSc4	blokáda
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
prezentovanou	prezentovaný	k2eAgFnSc4d1	prezentovaná
jako	jako	k8xS	jako
technické	technický	k2eAgFnSc2d1	technická
potíže	potíž	k1gFnSc2	potíž
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
západní	západní	k2eAgMnPc1d1	západní
spojenci	spojenec	k1gMnPc1	spojenec
připraveni	připravit	k5eAaPmNgMnP	připravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgInPc6d1	západní
sektorech	sektor	k1gInPc6	sektor
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
čítajících	čítající	k2eAgInPc2d1	čítající
tehdy	tehdy	k6eAd1	tehdy
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
závislých	závislý	k2eAgFnPc2d1	závislá
na	na	k7c4	na
zásobování	zásobování	k1gNnSc4	zásobování
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
zásoby	zásoba	k1gFnPc1	zásoba
potravin	potravina	k1gFnPc2	potravina
na	na	k7c4	na
36	[number]	k4	36
dnů	den	k1gInPc2	den
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c4	na
45	[number]	k4	45
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
spojenci	spojenec	k1gMnPc1	spojenec
nebyli	být	k5eNaImAgMnP	být
ani	ani	k8xC	ani
zajedno	zajedno	k6eAd1	zajedno
v	v	k7c6	v
budoucí	budoucí	k2eAgFnSc6d1	budoucí
politice	politika	k1gFnSc6	politika
vůči	vůči	k7c3	vůči
Berlínu	Berlín	k1gInSc3	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
americký	americký	k2eAgMnSc1d1	americký
vojenský	vojenský	k2eAgMnSc1d1	vojenský
guvernér	guvernér	k1gMnSc1	guvernér
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Lucius	Lucius	k1gInSc1	Lucius
D.	D.	kA	D.
Clay	Claa	k1gMnSc2	Claa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
dal	dát	k5eAaPmAgInS	dát
povel	povel	k1gInSc1	povel
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
leteckého	letecký	k2eAgInSc2d1	letecký
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
koridory	koridor	k1gInPc1	koridor
blokovány	blokován	k2eAgInPc1d1	blokován
nebyly	být	k5eNaImAgInP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
přistálo	přistát	k5eAaImAgNnS	přistát
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Tempelhof	Tempelhof	k1gInSc1	Tempelhof
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
sektoru	sektor	k1gInSc6	sektor
první	první	k4xOgNnSc1	první
letadlo	letadlo	k1gNnSc1	letadlo
operace	operace	k1gFnSc2	operace
Vittles	Vittlesa	k1gFnPc2	Vittlesa
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
započali	započnout	k5eAaPmAgMnP	započnout
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
Plain	Plain	k2eAgInSc1d1	Plain
Fare	Fare	k1gInSc1	Fare
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
používali	používat	k5eAaImAgMnP	používat
své	svůj	k3xOyFgNnSc4	svůj
vojenské	vojenský	k2eAgNnSc4d1	vojenské
letiště	letiště	k1gNnSc4	letiště
Gatow	Gatow	k1gFnPc2	Gatow
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
jezer	jezero	k1gNnPc2	jezero
pro	pro	k7c4	pro
nasazené	nasazený	k2eAgInPc4d1	nasazený
hydroplány	hydroplán	k1gInPc4	hydroplán
Short	Shorta	k1gFnPc2	Shorta
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
letci	letec	k1gMnPc1	letec
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
počátečních	počáteční	k2eAgFnPc2d1	počáteční
120	[number]	k4	120
tun	tuna	k1gFnPc2	tuna
denně	denně	k6eAd1	denně
se	se	k3xPyFc4	se
během	během	k7c2	během
týdnů	týden	k1gInPc2	týden
objem	objem	k1gInSc1	objem
zboží	zboží	k1gNnSc4	zboží
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
a	a	k8xC	a
rekordu	rekord	k1gInSc2	rekord
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1949	[number]	k4	1949
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
12	[number]	k4	12
840	[number]	k4	840
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
položkou	položka	k1gFnSc7	položka
(	(	kIx(	(
<g/>
62,8	[number]	k4	62,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
uhlí	uhlí	k1gNnSc4	uhlí
na	na	k7c4	na
topení	topení	k1gNnSc4	topení
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
benzín	benzín	k1gInSc4	benzín
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnPc4	pšenice
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
a	a	k8xC	a
také	také	k9	také
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
(	(	kIx(	(
<g/>
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
výstavbu	výstavba	k1gFnSc4	výstavba
letišť	letiště	k1gNnPc2	letiště
–	–	k?	–
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Tegel	Tegela	k1gFnPc2	Tegela
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
sektoru	sektor	k1gInSc6	sektor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
přistávala	přistávat	k5eAaImAgNnP	přistávat
v	v	k7c6	v
minutových	minutový	k2eAgInPc6d1	minutový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
blokáda	blokáda	k1gFnSc1	blokáda
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
původního	původní	k2eAgInSc2d1	původní
záměru	záměr	k1gInSc2	záměr
(	(	kIx(	(
<g/>
anexe	anexe	k1gFnSc1	anexe
západních	západní	k2eAgInPc2d1	západní
sektorů	sektor	k1gInPc2	sektor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
tranzitní	tranzitní	k2eAgNnSc4d1	tranzitní
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
opět	opět	k6eAd1	opět
otevřena	otevřen	k2eAgFnSc1d1	otevřena
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončil	skončit	k5eAaPmAgMnS	skončit
i	i	k9	i
letecký	letecký	k2eAgInSc4d1	letecký
most	most	k1gInSc4	most
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
kolem	kolem	k7c2	kolem
277	[number]	k4	277
000	[number]	k4	000
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
přepraveno	přepravit	k5eAaPmNgNnS	přepravit
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
leteckého	letecký	k2eAgInSc2d1	letecký
mostu	most	k1gInSc2	most
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
osob	osoba	k1gFnPc2	osoba
<g/>
:	:	kIx,	:
39	[number]	k4	39
Britů	Brit	k1gMnPc2	Brit
<g/>
,	,	kIx,	,
31	[number]	k4	31
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
6	[number]	k4	6
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Upomíná	upomínat	k5eAaImIp3nS	upomínat
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
pomník	pomník	k1gInSc4	pomník
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
letištěm	letiště	k1gNnSc7	letiště
Tempelhof	Tempelhof	k1gMnSc1	Tempelhof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
upomínkou	upomínka	k1gFnSc7	upomínka
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
"	"	kIx"	"
<g/>
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
"	"	kIx"	"
<g/>
rozinkových	rozinkový	k2eAgInPc2d1	rozinkový
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Tempelhof	Tempelhof	k1gInSc4	Tempelhof
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
berlínského	berlínský	k2eAgNnSc2d1	berlínské
technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
rozinkový	rozinkový	k2eAgInSc1d1	rozinkový
bombardér	bombardér	k1gInSc1	bombardér
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Rosinenbomber	Rosinenbomber	k1gInSc1	Rosinenbomber
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Gail	Gail	k1gInSc1	Gail
Halvorsen	Halvorsen	k2eAgInSc1d1	Halvorsen
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
letců	letec	k1gMnPc2	letec
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
náladě	nálada	k1gFnSc6	nálada
před	před	k7c7	před
přistáním	přistání	k1gNnSc7	přistání
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
na	na	k7c6	na
improvizovaném	improvizovaný	k2eAgInSc6d1	improvizovaný
padáčku	padáček	k1gInSc6	padáček
pár	pár	k4xCyI	pár
balíčků	balíček	k1gInPc2	balíček
se	s	k7c7	s
sladkostmi	sladkost	k1gFnPc7	sladkost
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
proslechlo	proslechnout	k5eAaPmAgNnS	proslechnout
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
dělala	dělat	k5eAaImAgFnS	dělat
většina	většina	k1gFnSc1	většina
pilotů	pilot	k1gMnPc2	pilot
a	a	k8xC	a
sbírání	sbírání	k1gNnPc2	sbírání
balíčků	balíček	k1gMnPc2	balíček
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Berlína	Berlín	k1gInSc2	Berlín
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Berlína	Berlín	k1gInSc2	Berlín
</s>
</p>
<p>
<s>
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
krize	krize	k1gFnSc2	krize
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Berliner	Berliner	k1gInSc1	Berliner
Blockade	Blockad	k1gInSc5	Blockad
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
na	na	k7c6	na
totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
