<s>
Thulium	thulium	k1gNnSc1	thulium
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejméně	málo	k6eAd3	málo
často	často	k6eAd1	často
a	a	k8xC	a
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
