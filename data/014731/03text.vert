<s>
Libido	libido	k1gNnSc1
</s>
<s>
Libido	libido	k1gNnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
touha	touha	k1gFnSc1
<g/>
,	,	kIx,
žádost	žádost	k1gFnSc1
<g/>
,	,	kIx,
pud	pud	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sexuální	sexuální	k2eAgFnSc1d1
touha	touha	k1gFnSc1
a	a	k8xC
pohlavní	pohlavní	k2eAgInSc1d1
pud	pud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někde	někde	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
„	„	k?
<g/>
sexuální	sexuální	k2eAgInSc1d1
apetit	apetit	k1gInSc1
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
energie	energie	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libido	libido	k1gNnSc1
má	mít	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
psychologických	psychologický	k2eAgFnPc6d1
teoriích	teorie	k1gFnPc6
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
a	a	k8xC
Carla	Carl	k1gMnSc2
Gustava	Gustav	k1gMnSc2
Junga	Jung	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
psychoanalýze	psychoanalýza	k1gFnSc6
je	být	k5eAaImIp3nS
libido	libido	k1gNnSc4
označení	označení	k1gNnSc2
pro	pro	k7c4
projevy	projev	k1gInPc4
pohlavního	pohlavní	k2eAgInSc2d1
pudu	pud	k1gInSc2
<g/>
,	,	kIx,
základní	základní	k2eAgFnSc2d1
pudové	pudový	k2eAgFnSc2d1
energie	energie	k1gFnSc2
a	a	k8xC
sexuální	sexuální	k2eAgFnSc2d1
touhy	touha	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyvěrá	vyvěrat	k5eAaImIp3nS
z	z	k7c2
podvědomí	podvědomí	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
dosahování	dosahování	k1gNnSc6
slasti	slast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Libido	libido	k1gNnSc1
je	být	k5eAaImIp3nS
hormonálně	hormonálně	k6eAd1
řízeno	řízen	k2eAgNnSc1d1
androgeny	androgen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgInP
převážně	převážně	k6eAd1
v	v	k7c6
nadledvinách	nadledvina	k1gFnPc6
<g/>
,	,	kIx,
ze	z	k7c2
40	#num#	k4
%	%	kIx~
však	však	k9
ve	v	k7c6
vaječníku	vaječník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Libido	libido	k1gNnSc1
v	v	k7c6
psychoanalýze	psychoanalýza	k1gFnSc6
</s>
<s>
Definice	definice	k1gFnSc1
libida	libido	k1gNnSc2
</s>
<s>
Pro	pro	k7c4
Sigmunda	Sigmund	k1gMnSc4
Freuda	Freud	k1gMnSc4
bylo	být	k5eAaImAgNnS
libido	libido	k1gNnSc1
klíčovým	klíčový	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
</s>
<s>
Carl	Carl	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Jung	Jung	k1gMnSc1
hovořil	hovořit	k5eAaImAgMnS
o	o	k7c6
libidu	libido	k1gNnSc6
jako	jako	k8xS,k8xC
o	o	k7c6
psychické	psychický	k2eAgFnSc6d1
energii	energie	k1gFnSc6
</s>
<s>
Libido	libido	k1gNnSc1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
užívaný	užívaný	k2eAgInSc1d1
především	především	k6eAd1
v	v	k7c6
psychoanalýze	psychoanalýza	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
označuje	označovat	k5eAaImIp3nS
psychickou	psychický	k2eAgFnSc4d1
<g/>
,	,	kIx,
respektive	respektive	k9
psychosexuální	psychosexuální	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libido	libido	k1gNnSc1
se	se	k3xPyFc4
mimo	mimo	k7c4
psychoanalýzu	psychoanalýza	k1gFnSc4
někdy	někdy	k6eAd1
definuje	definovat	k5eAaBmIp3nS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
pohlavní	pohlavní	k2eAgInSc4d1
pud	pud	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
psychoanalytiky	psychoanalytik	k1gMnPc4
však	však	k9
jde	jít	k5eAaImIp3nS
o	o	k7c4
energii	energie	k1gFnSc4
<g/>
,	,	kIx,
či	či	k8xC
dokonce	dokonce	k9
spíše	spíše	k9
o	o	k7c4
„	„	k?
<g/>
metaforu	metafora	k1gFnSc4
energie	energie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
zdroj	zdroj	k1gInSc4
buď	buď	k8xC
v	v	k7c6
nevědomí	nevědomí	k1gNnSc6
<g/>
,	,	kIx,
anebo	anebo	k8xC
v	v	k7c6
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
za	za	k7c4
zdroj	zdroj	k1gInSc4
považuje	považovat	k5eAaImIp3nS
tělo	tělo	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
libidem	libido	k1gNnSc7
myšlena	myšlen	k2eAgMnSc4d1
spíše	spíše	k9
energie	energie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
jsou	být	k5eAaImIp3nP
obsazeny	obsazen	k2eAgInPc1d1
výboje	výboj	k1gInPc1
pohlavního	pohlavní	k2eAgInSc2d1
pudu	pud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
za	za	k7c4
zdroj	zdroj	k1gInSc4
považuje	považovat	k5eAaImIp3nS
nevědomí	nevědomí	k1gNnSc4
<g/>
,	,	kIx,
vyniká	vynikat	k5eAaImIp3nS
“	“	k?
<g/>
metaforická	metaforický	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
<g/>
”	”	k?
pojmu	pojmout	k5eAaPmIp1nS
libido	libido	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
libido	libido	k1gNnSc1
Freud	Freuda	k1gFnPc2
převzal	převzít	k5eAaPmAgInS
z	z	k7c2
latiny	latina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
původně	původně	k6eAd1
značil	značit	k5eAaImAgMnS
přání	přání	k1gNnSc4
<g/>
,	,	kIx,
touhu	touha	k1gFnSc4
či	či	k8xC
nutkání	nutkání	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
akcentuje	akcentovat	k5eAaImIp3nS
pudový	pudový	k2eAgInSc1d1
charakter	charakter	k1gInSc1
této	tento	k3xDgFnSc2
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
Freud	Freud	k1gMnSc1
však	však	k9
stejně	stejně	k6eAd1
tak	tak	k6eAd1
akcentoval	akcentovat	k5eAaImAgInS
psychickou	psychický	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
této	tento	k3xDgFnSc2
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
o	o	k7c6
libidu	libido	k1gNnSc6
hovořil	hovořit	k5eAaImAgMnS
jako	jako	k9
o	o	k7c6
"	"	kIx"
<g/>
palivu	palivo	k1gNnSc6
psychického	psychický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
představy	představa	k1gFnPc1
jsou	být	k5eAaImIp3nP
u	u	k7c2
Freuda	Freud	k1gMnSc2
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
v	v	k7c6
mnoha	mnoho	k4c6
jiných	jiný	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
,	,	kIx,
zakotveny	zakotven	k2eAgFnPc1d1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
jeho	jeho	k3xOp3gFnSc2
tvorby	tvorba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zároveň	zároveň	k6eAd1
bylo	být	k5eAaImAgNnS
libido	libido	k1gNnSc1
pro	pro	k7c4
Freuda	Freud	k1gMnSc4
na	na	k7c4
trochu	trochu	k6eAd1
jiné	jiný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
abstrakce	abstrakce	k1gFnSc1
energií	energie	k1gFnPc2
pudu	pud	k1gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
C.	C.	kA
G.	G.	kA
Jung	Jung	k1gMnSc1
převzal	převzít	k5eAaPmAgInS
termín	termín	k1gInSc1
libido	libido	k1gNnSc1
od	od	k7c2
Freuda	Freud	k1gMnSc2
do	do	k7c2
své	svůj	k3xOyFgFnSc2
Analytické	analytický	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
definoval	definovat	k5eAaBmAgInS
ho	on	k3xPp3gInSc4
jako	jako	k9
psychickou	psychický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
může	moct	k5eAaImIp3nS
nést	nést	k5eAaImF
jakýkoli	jakýkoli	k3yIgInSc4
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
sexuální	sexuální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čili	čili	k8xC
chápal	chápat	k5eAaImAgMnS
libido	libido	k1gNnSc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
směřování	směřování	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
zájem	zájem	k1gInSc1
<g/>
)	)	kIx)
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovořil	hovořit	k5eAaImAgMnS
o	o	k7c6
něm	on	k3xPp3gNnSc6
jako	jako	k8xS,k8xC
o	o	k7c6
pojmu	pojem	k1gInSc6
vyjadřujícím	vyjadřující	k2eAgInSc6d1
cosi	cosi	k3yInSc4
„	„	k?
<g/>
nevýslovného	výslovný	k2eNgMnSc4d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
záhadného	záhadný	k2eAgMnSc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
spatřoval	spatřovat	k5eAaImAgMnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
„	„	k?
<g/>
kontinuální	kontinuální	k2eAgInSc1d1
životní	životní	k2eAgInSc1d1
pud	pud	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
vůli	vůle	k1gFnSc4
k	k	k7c3
životu	život	k1gInSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
libida	libido	k1gNnSc2
</s>
<s>
V	v	k7c6
prvotních	prvotní	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
Freud	Freudo	k1gNnPc2
vnímal	vnímat	k5eAaImAgInS
libido	libido	k1gNnSc4
jako	jako	k8xS,k8xC
energii	energie	k1gFnSc4
pudu	pud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
své	svůj	k3xOyFgNnSc4
libido	libido	k1gNnSc4
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
i	i	k9
Já	já	k3xPp1nSc1
(	(	kIx(
<g/>
Self	Self	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
narcistické	narcistický	k2eAgNnSc4d1
libido	libido	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
posléze	posléze	k6eAd1
dokonce	dokonce	k9
i	i	k9
Ego	ego	k1gNnSc1
–	–	k?
libido	libido	k1gNnSc1
Ega	ego	k1gNnSc2
je	být	k5eAaImIp3nS
energií	energie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
objekty	objekt	k1gInPc4
v	v	k7c6
průběhu	průběh	k1gInSc6
vývoje	vývoj	k1gInSc2
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
těchto	tento	k3xDgInPc2
objektů	objekt	k1gInPc2
se	se	k3xPyFc4
stáhla	stáhnout	k5eAaPmAgFnS
a	a	k8xC
Ego	ego	k1gNnSc4
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
přivlastnilo	přivlastnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vývoj	vývoj	k1gInSc1
Ega	ego	k1gNnSc2
totiž	totiž	k9
iniciuje	iniciovat	k5eAaBmIp3nS
frustrace	frustrace	k1gFnSc1
z	z	k7c2
pochopení	pochopení	k1gNnSc2
jinakosti	jinakost	k1gFnSc2
a	a	k8xC
oddělenosti	oddělenost	k1gFnSc2
rodičů	rodič	k1gMnPc2
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
dítě	dítě	k1gNnSc1
vytvoří	vytvořit	k5eAaPmIp3nS
z	z	k7c2
rodičů	rodič	k1gMnPc2
objekty	objekt	k1gInPc4
a	a	k8xC
obsadí	obsadit	k5eAaPmIp3nP
je	on	k3xPp3gFnPc4
libidinosní	libidinosní	k2eAgFnPc4d1
energií	energie	k1gFnSc7
<g/>
;	;	kIx,
tento	tento	k3xDgInSc4
konflikt	konflikt	k1gInSc4
s	s	k7c7
vnějšími	vnější	k2eAgInPc7d1
objekty	objekt	k1gInPc7
nabitými	nabitý	k2eAgInPc7d1
psychickou	psychický	k2eAgFnSc4d1
energií	energie	k1gFnSc7
subjektu	subjekt	k1gInSc2
dítě	dítě	k1gNnSc1
řeší	řešit	k5eAaImIp3nP
identifikací	identifikace	k1gFnSc7
s	s	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
následnou	následný	k2eAgFnSc7d1
introjekcí	introjekce	k1gFnSc7
rodičovských	rodičovský	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
;	;	kIx,
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
původní	původní	k2eAgFnSc2d1
energie	energie	k1gFnSc2
„	„	k?
<g/>
vrací	vracet	k5eAaImIp3nS
domů	domů	k6eAd1
<g/>
“	“	k?
a	a	k8xC
zmocní	zmocnit	k5eAaPmIp3nS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
Ego	ego	k1gNnPc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Libido	libido	k1gNnSc1
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
různě	různě	k6eAd1
projevovat	projevovat	k5eAaImF
<g/>
,	,	kIx,
obsazuje	obsazovat	k5eAaImIp3nS
procesy	proces	k1gInPc4
<g/>
,	,	kIx,
struktury	struktura	k1gFnPc4
<g/>
,	,	kIx,
objekty	objekt	k1gInPc4
i	i	k8xC
jejich	jejich	k3xOp3gFnPc4
reprezentace	reprezentace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
libida	libido	k1gNnSc2
</s>
<s>
Libido	libido	k1gNnSc1
se	se	k3xPyFc4
podle	podle	k7c2
Freuda	Freud	k1gMnSc2
vyvíjí	vyvíjet	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
prochází	procházet	k5eAaImIp3nS
různými	různý	k2eAgNnPc7d1
stádii	stádium	k1gNnPc7
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nP
zdroje	zdroj	k1gInPc1
a	a	k8xC
formy	forma	k1gFnPc1
sexuální	sexuální	k2eAgFnSc2d1
slasti	slast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Freud	Freud	k1gMnSc1
rozlišil	rozlišit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
klíčové	klíčový	k2eAgFnPc4d1
fáze	fáze	k1gFnPc4
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
hlavními	hlavní	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
slasti	slast	k1gFnSc2
jsou	být	k5eAaImIp3nP
postupně	postupně	k6eAd1
vlastní	vlastní	k2eAgNnPc4d1
ústa	ústa	k1gNnPc4
<g/>
,	,	kIx,
anál	anál	k1gInSc1
<g/>
,	,	kIx,
penis	penis	k1gInSc1
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
pozornost	pozornost	k1gFnSc1
a	a	k8xC
slast	slast	k1gFnSc1
konečně	konečně	k6eAd1
přesouvá	přesouvat	k5eAaImIp3nS
na	na	k7c4
objekt	objekt	k1gInSc4
(	(	kIx(
<g/>
rodičovský	rodičovský	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freud	Freud	k1gInSc1
spekuloval	spekulovat	k5eAaImAgInS
také	také	k9
o	o	k7c6
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
slast	slast	k1gFnSc1
rozložena	rozložen	k2eAgFnSc1d1
po	po	k7c6
celém	celý	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
a	a	k8xC
její	její	k3xOp3gNnSc1
vnímání	vnímání	k1gNnSc1
splývá	splývat	k5eAaImIp3nS
s	s	k7c7
vnímáním	vnímání	k1gNnSc7
celého	celý	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
;	;	kIx,
tato	tento	k3xDgFnSc1
fáze	fáze	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
polymorfně	polymorfně	k6eAd1
perverzní	perverzní	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
ohraničitelná	ohraničitelný	k2eAgNnPc4d1
pobytem	pobyt	k1gInSc7
v	v	k7c6
děloze	děloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libidinosní	Libidinosní	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
je	být	k5eAaImIp3nS
uzavřen	uzavřít	k5eAaPmNgInS
fází	fáze	k1gFnSc7
latence	latence	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
psychosexuální	psychosexuální	k2eAgNnSc4d1
zrání	zrání	k1gNnSc4
přešlapuje	přešlapovat	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
stabilizuje	stabilizovat	k5eAaBmIp3nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
naší	náš	k3xOp1gFnSc6
kultuře	kultura	k1gFnSc6
fáze	fáze	k1gFnSc2
latence	latence	k1gFnSc2
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
jasné	jasný	k2eAgNnSc1d1
do	do	k7c2
jaké	jaký	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
míry	míra	k1gFnSc2
je	být	k5eAaImIp3nS
její	její	k3xOp3gNnSc4
načasování	načasování	k1gNnSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
podmíněno	podmínit	k5eAaPmNgNnS
biologicky	biologicky	k6eAd1
a	a	k8xC
do	do	k7c2
jaké	jaký	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
kulturně	kulturně	k6eAd1
<g/>
)	)	kIx)
končí	končit	k5eAaImIp3nS
pubertou	puberta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Fixace	fixace	k1gFnSc1
a	a	k8xC
regrese	regrese	k1gFnSc1
libida	libido	k1gNnSc2
</s>
<s>
Každé	každý	k3xTgNnSc1
pozdější	pozdní	k2eAgInPc4d2
libidinosní	libidinosní	k2eAgInPc4d1
pohyby	pohyb	k1gInPc4
jiným	jiný	k2eAgInSc7d1
směrem	směr	k1gInSc7
než	než	k8xS
k	k	k7c3
heterosexualitě	heterosexualita	k1gFnSc3
(	(	kIx(
<g/>
protinarcistické	protinarcistický	k2eAgFnSc3d1
jinakosti	jinakost	k1gFnSc3
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
projevem	projev	k1gInSc7
fixace	fixace	k1gFnSc2
či	či	k8xC
regrese	regrese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	s	k7c7
průchod	průchod	k1gInSc4
vývojovou	vývojový	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
nezdaří	zdařit	k5eNaPmIp3nS
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
fixaci	fixace	k1gFnSc3
(	(	kIx(
<g/>
na	na	k7c4
fázi	fáze	k1gFnSc4
libida	libido	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
objekt	objekt	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
při	při	k7c6
komplikovaném	komplikovaný	k2eAgInSc6d1
průchodu	průchod	k1gInSc6
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
k	k	k7c3
regresi	regrese	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fixovaná	fixovaný	k2eAgFnSc1d1
nebo	nebo	k8xC
regredující	regredující	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
infantilní	infantilní	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
uchyluje	uchylovat	k5eAaImIp3nS
pod	pod	k7c7
vnějším	vnější	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
(	(	kIx(
<g/>
eventuálně	eventuálně	k6eAd1
si	se	k3xPyFc3
vybírá	vybírat	k5eAaImIp3nS
erotické	erotický	k2eAgInPc4d1
objekty	objekt	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
podobnosti	podobnost	k1gFnSc2
s	s	k7c7
fixovaným	fixovaný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
–	–	k?
např.	např.	kA
velká	velký	k2eAgFnSc1d1
ňadra	ňadro	k1gNnSc2
–	–	k?
případně	případně	k6eAd1
lze	lze	k6eAd1
indikovat	indikovat	k5eAaBmF
ztrátu	ztráta	k1gFnSc4
energie	energie	k1gFnSc2
uvázané	uvázaný	k2eAgFnSc2d1
na	na	k7c4
objekt	objekt	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Freuda	Freud	k1gMnSc2
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
fixace	fixace	k1gFnSc1
či	či	k8xC
regrese	regrese	k1gFnSc1
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
projevuje	projevovat	k5eAaImIp3nS
buď	buď	k8xC
psychickou	psychický	k2eAgFnSc7d1
poruchou	porucha	k1gFnSc7
anebo	anebo	k8xC
sexuální	sexuální	k2eAgFnSc7d1
perverzí	perverze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexuální	sexuální	k2eAgFnSc1d1
perverze	perverze	k1gFnSc1
otevřeně	otevřeně	k6eAd1
manifestuje	manifestovat	k5eAaBmIp3nS
původní	původní	k2eAgFnSc4d1
sexuální	sexuální	k2eAgFnSc4d1
tematiku	tematika	k1gFnSc4
<g/>
,	,	kIx,
původní	původní	k2eAgInPc4d1
objekty	objekt	k1gInPc4
a	a	k8xC
také	také	k9
vztah	vztah	k1gInSc4
k	k	k7c3
erotogenním	erotogenní	k2eAgFnPc3d1
zónám	zóna	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
neuróz	neuróza	k1gFnPc2
či	či	k8xC
jiných	jiný	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
psychoanalytik	psychoanalytik	k1gMnSc1
indikuje	indikovat	k5eAaBmIp3nS
téma	téma	k1gNnSc4
<g/>
,	,	kIx,
objekty	objekt	k1gInPc4
i	i	k8xC
drážděné	drážděný	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
problémy	problém	k1gInPc4
s	s	k7c7
vyměšováním	vyměšování	k1gNnSc7
<g/>
)	)	kIx)
analogicky	analogicky	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
perverzí	perverze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fixace	fixace	k1gFnSc1
na	na	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
fází	fáze	k1gFnPc2
vývoje	vývoj	k1gInSc2
libida	libido	k1gNnSc2
může	moct	k5eAaImIp3nS
dle	dle	k7c2
Freuda	Freud	k1gMnSc2
vyústit	vyústit	k5eAaPmF
i	i	k9
v	v	k7c4
sadomasochismus	sadomasochismus	k1gInSc4
</s>
<s>
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
perverze	perverze	k1gFnSc2
</s>
<s>
Perverze	perverze	k1gFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
Freuda	Freud	k1gMnSc4
svědky	svědek	k1gMnPc7
dějin	dějiny	k1gFnPc2
libida	libido	k1gNnSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
neurózy	neuróza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perverze	perverze	k1gFnPc1
jsou	být	k5eAaImIp3nP
negativy	negativ	k1gInPc4
neuróz	neuróza	k1gFnPc2
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
neurotik	neurotik	k1gMnSc1
potlačuje	potlačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
“	“	k?
<g/>
deviant	deviant	k1gMnSc1
<g/>
”	”	k?
přehrává	přehrávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
úkolem	úkol	k1gInSc7
perverze	perverze	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
bránit	bránit	k5eAaImF
Ego	ego	k1gNnSc4
před	před	k7c7
úzkostí	úzkost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítě	Dítě	k1gMnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
Freuda	Freud	k1gMnSc4
“	“	k?
<g/>
polymorfně	polymorfně	k6eAd1
perverzní	perverzní	k2eAgFnSc2d1
<g/>
”	”	k?
a	a	k8xC
sexuální	sexuální	k2eAgFnPc1d1
perverze	perverze	k1gFnPc1
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
představuje	představovat	k5eAaImIp3nS
regresi	regrese	k1gFnSc4
ke	k	k7c3
sklonům	sklon	k1gInPc3
infantilní	infantilní	k2eAgFnSc2d1
sexuality	sexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klíčovými	klíčový	k2eAgFnPc7d1
perverzemi	perverze	k1gFnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
rozezvučení	rozezvučení	k1gNnSc1
je	být	k5eAaImIp3nS
všelidsky	všelidsky	k6eAd1
apelativní	apelativní	k2eAgNnSc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ty	ten	k3xDgFnPc4
upomínající	upomínající	k2eAgFnPc4d1
na	na	k7c4
uzlové	uzlový	k2eAgInPc4d1
body	bod	k1gInPc4
libidinosního	libidinosní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
jimi	on	k3xPp3gFnPc7
obecně	obecně	k6eAd1
narcismus	narcismus	k1gInSc1
<g/>
,	,	kIx,
sado-masochismus	sado-masochismus	k1gInSc1
<g/>
,	,	kIx,
exhibicionismus	exhibicionismus	k1gInSc1
a	a	k8xC
voyeurismus	voyeurismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
vývojových	vývojový	k2eAgFnPc2d1
fází	fáze	k1gFnPc2
libida	libido	k1gNnSc2
a	a	k8xC
perverzí	perverze	k1gFnPc2
</s>
<s>
První	první	k4xOgFnSc7
zásadní	zásadní	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
vývoje	vývoj	k1gInSc2
libida	libido	k1gNnSc2
je	být	k5eAaImIp3nS
fáze	fáze	k1gFnSc1
orální	orální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
začíná	začínat	k5eAaImIp3nS
narozením	narození	k1gNnSc7
a	a	k8xC
končí	končit	k5eAaImIp3nS
odstavením	odstavení	k1gNnSc7
od	od	k7c2
prsu	prs	k1gInSc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
dobou	doba	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
dítě	dítě	k1gNnSc1
přeje	přát	k5eAaImIp3nS
být	být	k5eAaImF
odstaveno	odstavit	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdrojem	zdroj	k1gInSc7
slasti	slast	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc4
ústa	ústa	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
fixaci	fixace	k1gFnSc3
či	či	k8xC
regresi	regrese	k1gFnSc3
na	na	k7c4
tuto	tento	k3xDgFnSc4
fázi	fáze	k1gFnSc4
<g/>
,	,	kIx,
osoba	osoba	k1gFnSc1
s	s	k7c7
orálním	orální	k2eAgInSc7d1
charakterem	charakter	k1gInSc7
může	moct	k5eAaImIp3nS
inklinovat	inklinovat	k5eAaImF
k	k	k7c3
sado-masochismu	sado-masochismus	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
psychopatologické	psychopatologický	k2eAgFnSc2d1
realizace	realizace	k1gFnSc2
problematiky	problematika	k1gFnSc2
se	se	k3xPyFc4
dává	dávat	k5eAaImIp3nS
orální	orální	k2eAgInSc4d1
charakter	charakter	k1gInSc4
především	především	k9
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
maniodepresivní	maniodepresivní	k2eAgFnSc7d1
psychózou	psychóza	k1gFnSc7
a	a	k8xC
depresí	deprese	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
orální	orální	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
se	se	k3xPyFc4
libido	libido	k1gNnSc1
vyvíjí	vyvíjet	k5eAaImIp3nS
fází	fáze	k1gFnSc7
anální	anální	k2eAgFnSc7d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zdrojem	zdroj	k1gInSc7
slasti	slast	k1gFnSc2
je	být	k5eAaImIp3nS
především	především	k9
anál	anál	k1gInSc1
a	a	k8xC
defekace	defekace	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
manipulace	manipulace	k1gFnSc1
s	s	k7c7
výkaly	výkal	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
fixaci	fixace	k1gFnSc6
či	či	k8xC
regresi	regrese	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
psychopatologické	psychopatologický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
výsledkem	výsledek	k1gInSc7
nutkavá	nutkavý	k2eAgFnSc1d1
neuróza	neuróza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sexuálně-perverzní	sexuálně-perverzní	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
jí	on	k3xPp3gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
sadismus	sadismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
anální	anální	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
libidinosního	libidinosní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
následuje	následovat	k5eAaImIp3nS
fáze	fáze	k1gFnSc1
falická	falický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
chlapec	chlapec	k1gMnSc1
začíná	začínat	k5eAaImIp3nS
zabývat	zabývat	k5eAaImF
svým	svůj	k3xOyFgInSc7
penisem	penis	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnPc7
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
dívek	dívka	k1gFnPc2
pak	pak	k6eAd1
Freud	Freud	k1gInSc1
považoval	považovat	k5eAaImAgInS
za	za	k7c4
klíčovou	klíčový	k2eAgFnSc4d1
zkušenost	zkušenost	k1gFnSc4
úlek	úlek	k1gInSc1
ze	z	k7c2
ztráty	ztráta	k1gFnSc2
penisu	penis	k1gInSc2
následovaný	následovaný	k2eAgMnSc1d1
závistí	závist	k1gFnSc7
penisu	penis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
falickou	falický	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
souvisí	souviset	k5eAaImIp3nS
dílčí	dílčí	k2eAgInSc1d1
pud	pud	k1gInSc1
exhibicionismu	exhibicionismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapec	chlapec	k1gMnSc1
má	mít	k5eAaImIp3nS
potřebu	potřeba	k1gFnSc4
ukazovat	ukazovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
genitál	genitál	k1gInSc1
<g/>
,	,	kIx,
u	u	k7c2
dívek	dívka	k1gFnPc2
se	se	k3xPyFc4
rozvíjí	rozvíjet	k5eAaImIp3nS
obecný	obecný	k2eAgInSc1d1
feminní	feminný	k2eAgMnPc1d1
exhibicionismus	exhibicionismus	k1gInSc4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
je	být	k5eAaImIp3nS
kompenzována	kompenzován	k2eAgFnSc1d1
neúplnost	neúplnost	k1gFnSc1
–	–	k?
„	„	k?
<g/>
ztráta	ztráta	k1gFnSc1
penisu	penis	k1gInSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
musím	muset	k5eAaImIp1nS
něco	něco	k3yInSc1
mít	mít	k5eAaImF
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c4
mne	já	k3xPp1nSc4
dívají	dívat	k5eAaImIp3nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasivní	pasivní	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
exhibicionismu	exhibicionismus	k1gInSc2
je	být	k5eAaImIp3nS
voyeurismus	voyeurismus	k1gInSc1
<g/>
,	,	kIx,
slast	slast	k1gFnSc1
z	z	k7c2
dívání	dívání	k1gNnSc2
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narcistické	narcistický	k2eAgInPc1d1
zalíbení	zalíbení	k1gNnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
genitálu	genitál	k1gInSc6
také	také	k9
připravuje	připravovat	k5eAaImIp3nS
půdu	půda	k1gFnSc4
homosexualitě	homosexualita	k1gFnSc3
(	(	kIx(
<g/>
u	u	k7c2
chlapců	chlapec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
dějin	dějiny	k1gFnPc2
libida	libido	k1gNnSc2
<g/>
:	:	kIx,
Oidipus	Oidipus	k1gMnSc1
a	a	k8xC
kastrace	kastrace	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
třetím	třetí	k4xOgInSc7
až	až	k8xS
pátým	pátý	k4xOgInSc7
rokem	rok	k1gInSc7
života	život	k1gInSc2
se	se	k3xPyFc4
realizuje	realizovat	k5eAaBmIp3nS
fáze	fáze	k1gFnSc1
oidipovská	oidipovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1
oidipovským	oidipovský	k2eAgInPc3d1
(	(	kIx(
<g/>
u	u	k7c2
dívek	dívka	k1gFnPc2
elektřiným	elektřin	k2eAgInSc7d1
<g/>
)	)	kIx)
komplexem	komplex	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zájem	zájem	k1gInSc1
dítěte	dítě	k1gNnSc2
přesune	přesunout	k5eAaPmIp3nS
k	k	k7c3
vlastnění	vlastnění	k1gNnSc3
rodiče	rodič	k1gMnPc1
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
a	a	k8xC
odstranění	odstranění	k1gNnSc2
rodiče	rodič	k1gMnSc2
pohlaví	pohlaví	k1gNnSc2
stejného	stejné	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libido	libido	k1gNnSc1
se	s	k7c7
prvně	prvně	k?
ve	v	k7c6
vývoji	vývoj	k1gInSc6
masivně	masivně	k6eAd1
přesouvá	přesouvat	k5eAaImIp3nS
na	na	k7c4
celý	celý	k2eAgInSc4d1
lidský	lidský	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
prototyp	prototyp	k1gInSc4
sexuální	sexuální	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oidipovské	oidipovský	k2eAgNnSc1d1
drama	drama	k1gNnSc1
se	se	k3xPyFc4
řeší	řešit	k5eAaImIp3nS
identifikací	identifikace	k1gFnSc7
s	s	k7c7
rodičem	rodič	k1gMnSc7
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
a	a	k8xC
dočasným	dočasný	k2eAgNnSc7d1
zřeknutím	zřeknutí	k1gNnSc7
se	se	k3xPyFc4
pohlaví	pohlaví	k1gNnSc2
opačného	opačný	k2eAgNnSc2d1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
znovu	znovu	k6eAd1
objeveno	objevit	k5eAaPmNgNnS
až	až	k9
ve	v	k7c6
formě	forma	k1gFnSc6
dospělého	dospělý	k2eAgInSc2d1
sexuálního	sexuální	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
chlapců	chlapec	k1gMnPc2
předjímá	předjímat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
fázi	fáze	k1gFnSc4
kastrační	kastrační	k2eAgFnSc4d1
úzkost	úzkost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
psychopatologii	psychopatologie	k1gFnSc6
vyjevuje	vyjevovat	k5eAaImIp3nS
jako	jako	k9
úzkost	úzkost	k1gFnSc4
paranoidní	paranoidní	k2eAgFnSc4d1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
perverzním	perverzní	k2eAgInSc7d1
ekvivalentem	ekvivalent	k1gInSc7
je	být	k5eAaImIp3nS
submisivita	submisivita	k1gFnSc1
a	a	k8xC
latentní	latentní	k2eAgFnSc1d1
homosexualita	homosexualita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexuální	homosexuální	k2eAgInSc4d1
přání	přání	k1gNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
Freuda	Freudo	k1gNnPc4
reaktivací	reaktivace	k1gFnPc2
femininního	femininní	k2eAgNnSc2d1
přání	přání	k1gNnSc3
vůči	vůči	k7c3
otci	otec	k1gMnSc3
v	v	k7c6
oidipovské	oidipovský	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
u	u	k7c2
chlapců	chlapec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Libido	libido	k1gNnSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RYCROFT	RYCROFT	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
:	:	kIx,
Kritický	kritický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
psychoanalýzy	psychoanalýza	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-901601-1-5	80-901601-1-5	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
libido	libido	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sex	sex	k1gInSc1
Historie	historie	k1gFnSc2
</s>
<s>
dějiny	dějiny	k1gFnPc1
lidské	lidský	k2eAgFnSc2d1
sexuality	sexualita	k1gFnSc2
•	•	k?
dějiny	dějiny	k1gFnPc4
znázornění	znázornění	k1gNnSc2
erotiky	erotika	k1gFnSc2
•	•	k?
sexuální	sexuální	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
Sexuální	sexuální	k2eAgFnSc2d1
praktiky	praktika	k1gFnSc2
</s>
<s>
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
(	(	kIx(
<g/>
předehra	předehra	k1gFnSc1
•	•	k?
pozice	pozice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
orální	orální	k2eAgInSc4d1
sex	sex	k1gInSc4
(	(	kIx(
<g/>
felace	felace	k1gFnSc1
•	•	k?
cunnilingus	cunnilingus	k1gInSc1
•	•	k?
anilingus	anilingus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
skupinový	skupinový	k2eAgInSc4d1
sex	sex	k1gInSc4
(	(	kIx(
<g/>
gang	gang	k1gInSc1
bang	bang	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
masturbace	masturbace	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
autofelace	autofelace	k1gFnSc1
•	•	k?
facial	facial	k1gInSc1
•	•	k?
bukkake	bukkake	k1gInSc1
•	•	k?
gokkun	gokkun	k1gInSc1
•	•	k?
snowballing	snowballing	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
sex	sex	k1gInSc4
bez	bez	k7c2
penetrace	penetrace	k1gFnSc2
(	(	kIx(
<g/>
petting	petting	k1gInSc1
•	•	k?
stimulace	stimulace	k1gFnSc2
bradavek	bradavka	k1gFnPc2
•	•	k?
prstění	prstění	k1gNnSc1
/	/	kIx~
stimulace	stimulace	k1gFnSc1
penisu	penis	k1gInSc2
rukou	ruka	k1gFnPc2
•	•	k?
sumata	sumat	k1gMnSc2
•	•	k?
fisting	fisting	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
rychlovka	rychlovka	k1gFnSc1
•	•	k?
anální	anální	k2eAgInSc4d1
sex	sex	k1gInSc4
(	(	kIx(
<g/>
ass	ass	k?
to	ten	k3xDgNnSc4
mouth	mouth	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
creampie	creampie	k1gFnSc2
•	•	k?
BDSM	BDSM	kA
Fyziologie	fyziologie	k1gFnSc2
</s>
<s>
sexuální	sexuální	k2eAgNnSc1d1
vzrušení	vzrušení	k1gNnSc1
•	•	k?
erotogenní	erotogenní	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
•	•	k?
erekce	erekce	k1gFnSc2
•	•	k?
orgasmus	orgasmus	k1gInSc1
•	•	k?
ejakulace	ejakulace	k1gFnSc2
•	•	k?
inseminace	inseminace	k1gFnSc2
•	•	k?
odběr	odběr	k1gInSc1
spermatu	sperma	k1gNnSc2
•	•	k?
těhotenství	těhotenství	k1gNnSc2
•	•	k?
pohlaví	pohlaví	k1gNnSc2
(	(	kIx(
<g/>
hermafrodit	hermafrodit	k1gMnSc1
<g/>
)	)	kIx)
Zdraví	zdraví	k1gNnSc1
a	a	k8xC
výchova	výchova	k1gFnSc1
</s>
<s>
antikoncepce	antikoncepce	k1gFnSc1
•	•	k?
reprodukční	reprodukční	k2eAgNnSc4d1
lékařství	lékařství	k1gNnSc4
(	(	kIx(
<g/>
andrologie	andrologie	k1gFnSc1
•	•	k?
gynekologie	gynekologie	k1gFnSc2
•	•	k?
urologie	urologie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
bezpečný	bezpečný	k2eAgInSc4d1
sex	sex	k1gInSc4
•	•	k?
libido	libido	k1gNnSc1
(	(	kIx(
<g/>
hyposexualita	hyposexualita	k1gFnSc1
/	/	kIx~
hypersexualita	hypersexualita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
sexuální	sexuální	k2eAgFnSc2d1
dysfunkce	dysfunkce	k1gFnSc2
(	(	kIx(
<g/>
erektilní	erektilní	k2eAgFnSc1d1
dysfunkce	dysfunkce	k1gFnSc1
•	•	k?
frigidita	frigidita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
výchova	výchova	k1gFnSc1
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
•	•	k?
sexuálně	sexuálně	k6eAd1
přenosné	přenosný	k2eAgFnSc3d1
nemoci	nemoc	k1gFnSc3
•	•	k?
inkontinence	inkontinence	k1gFnSc1
•	•	k?
fimóza	fimóza	k1gFnSc1
Identita	identita	k1gFnSc1
a	a	k8xC
orientace	orientace	k1gFnSc1
</s>
<s>
sexuální	sexuální	k2eAgFnSc1d1
identita	identita	k1gFnSc1
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
(	(	kIx(
<g/>
heterosexualita	heterosexualita	k1gFnSc1
•	•	k?
bisexualita	bisexualita	k1gFnSc1
•	•	k?
homosexualita	homosexualita	k1gFnSc1
•	•	k?
pansexualita	pansexualita	k1gFnSc1
•	•	k?
asexualita	asexualita	k1gFnSc1
•	•	k?
demisexualita	demisexualita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
parafilie	parafilie	k1gFnSc1
(	(	kIx(
<g/>
efebofilie	efebofilie	k1gFnSc1
•	•	k?
fetišismus	fetišismus	k1gInSc1
•	•	k?
gerontofilie	gerontofilie	k1gFnSc2
•	•	k?
hebefilie	hebefilie	k1gFnSc2
•	•	k?
nepiofilie	nepiofilie	k1gFnSc2
•	•	k?
objektofilie	objektofilie	k1gFnSc2
•	•	k?
pedofilie	pedofilie	k1gFnSc2
•	•	k?
zoofilie	zoofilie	k1gFnSc1
<g/>
)	)	kIx)
Zákon	zákon	k1gInSc1
</s>
<s>
legální	legální	k2eAgInSc1d1
věk	věk	k1gInSc1
způsobilosti	způsobilost	k1gFnSc2
k	k	k7c3
pohlavnímu	pohlavní	k2eAgInSc3d1
styku	styk	k1gInSc2
•	•	k?
obscénnost	obscénnost	k1gFnSc1
•	•	k?
incest	incest	k1gInSc1
•	•	k?
sexuální	sexuální	k2eAgNnSc4d1
násilí	násilí	k1gNnSc4
(	(	kIx(
<g/>
znásilnění	znásilnění	k1gNnSc4
•	•	k?
sexuální	sexuální	k2eAgNnSc4d1
napadení	napadení	k1gNnSc4
•	•	k?
sexuální	sexuální	k2eAgNnSc4d1
obtěžování	obtěžování	k1gNnSc4
•	•	k?
pohlavní	pohlavní	k2eAgNnSc1d1
zneužívání	zneužívání	k1gNnSc1
<g/>
)	)	kIx)
Vztahy	vztah	k1gInPc1
a	a	k8xC
společnost	společnost	k1gFnSc1
</s>
<s>
fyzická	fyzický	k2eAgFnSc1d1
intimita	intimita	k1gFnSc1
•	•	k?
milostný	milostný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
•	•	k?
manželství	manželství	k1gNnSc2
•	•	k?
nevěra	nevěra	k1gFnSc1
•	•	k?
sexuální	sexuální	k2eAgFnSc4d1
přitažlivost	přitažlivost	k1gFnSc4
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
abstinence	abstinence	k1gFnSc1
•	•	k?
interrupce	interrupce	k1gFnSc2
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
etika	etika	k1gFnSc1
•	•	k?
náboženství	náboženství	k1gNnSc1
a	a	k8xC
sexualita	sexualita	k1gFnSc1
(	(	kIx(
<g/>
obřízka	obřízka	k1gFnSc1
/	/	kIx~
ženská	ženský	k2eAgFnSc1d1
obřízka	obřízka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
sexuální	sexuální	k2eAgFnSc2d1
asistence	asistence	k1gFnSc2
•	•	k?
nezávazný	závazný	k2eNgInSc4d1
sex	sex	k1gInSc4
Sexuální	sexuální	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
</s>
<s>
erotika	erotika	k1gFnSc1
•	•	k?
lubrikační	lubrikační	k2eAgInSc1d1
gel	gel	k1gInSc1
•	•	k?
paddle	paddle	k6eAd1
•	•	k?
pornografie	pornografie	k1gFnSc2
•	•	k?
prostituce	prostituce	k1gFnSc2
•	•	k?
erotické	erotický	k2eAgFnSc2d1
pomůcky	pomůcka	k1gFnSc2
(	(	kIx(
<g/>
umělý	umělý	k2eAgInSc1d1
penis	penis	k1gInSc1
•	•	k?
nafukovací	nafukovací	k2eAgFnSc1d1
panna	panna	k1gFnSc1
•	•	k?
umělá	umělý	k2eAgFnSc1d1
vagína	vagína	k1gFnSc1
•	•	k?
vibrátor	vibrátor	k1gInSc4
•	•	k?
venušiny	venušin	k2eAgFnSc2d1
kuličky	kulička	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
sex	sex	k1gInSc1
shop	shop	k1gInSc1
•	•	k?
sexuální	sexuální	k2eAgFnSc1d1
turistika	turistika	k1gFnSc1
Věda	věda	k1gFnSc1
</s>
<s>
erotologie	erotologie	k1gFnSc1
•	•	k?
sexuologie	sexuologie	k1gFnSc1
lidská	lidský	k2eAgFnSc1d1
sexualita	sexualita	k1gFnSc1
•	•	k?
lidské	lidský	k2eAgNnSc4d1
sexuální	sexuální	k2eAgNnSc4d1
chování	chování	k1gNnSc4
•	•	k?
zvířecí	zvířecí	k2eAgNnSc4d1
sexuální	sexuální	k2eAgNnSc4d1
chování	chování	k1gNnSc4
•	•	k?
sexuální	sexuální	k2eAgInSc1d1
slang	slang	k1gInSc1
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1
Hlavní	hlavní	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
</s>
<s>
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
•	•	k?
Anna	Anna	k1gFnSc1
Freudová	Freudová	k1gFnSc1
•	•	k?
Alfred	Alfred	k1gMnSc1
Adler	Adler	k1gMnSc1
•	•	k?
Carl	Carl	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Jung	Jung	k1gMnSc1
•	•	k?
Margaret	Margareta	k1gFnPc2
Mahlerová	Mahlerový	k2eAgFnSc1d1
•	•	k?
Melanie	Melanie	k1gFnSc1
Kleinová	Kleinová	k1gFnSc1
•	•	k?
Jacques	Jacques	k1gMnSc1
Lacan	Lacan	k1gMnSc1
•	•	k?
Donald	Donald	k1gMnSc1
Woods	Woods	k1gInSc4
Winnicott	Winnicott	k2eAgInSc4d1
Další	další	k2eAgFnSc4d1
osobnosti	osobnost	k1gFnPc4
</s>
<s>
Karl	Karl	k1gMnSc1
Abraham	Abraham	k1gMnSc1
•	•	k?
Ernest	Ernest	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Rank	rank	k1gInSc1
•	•	k?
Sándor	Sándor	k1gInSc4
Ferenczi	Ferencze	k1gFnSc4
•	•	k?
Theodor	Theodor	k1gMnSc1
Reik	Reik	k1gMnSc1
•	•	k?
Helene	Helen	k1gInSc5
Deutschová	Deutschová	k1gFnSc1
•	•	k?
Wilhelm	Wilhelm	k1gMnSc1
Reich	Reich	k?
•	•	k?
Edith	Edith	k1gInSc1
Jacobsonová	Jacobsonová	k1gFnSc1
•	•	k?
Herbert	Herbert	k1gMnSc1
Marcuse	Marcuse	k1gFnSc2
•	•	k?
Erich	Erich	k1gMnSc1
Fromm	Fromm	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Loewald	Loewald	k1gMnSc1
•	•	k?
Erik	Erik	k1gMnSc1
Erikson	Erikson	k1gMnSc1
•	•	k?
Slavoj	Slavoj	k1gMnSc1
Žižek	Žižek	k1gMnSc1
•	•	k?
Harry	Harra	k1gFnSc2
Stack	Stack	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Julia	Julius	k1gMnSc4
Kristeva	Kristev	k1gMnSc4
•	•	k?
Ronald	Ronald	k1gMnSc1
David	David	k1gMnSc1
Laing	Laing	k1gMnSc1
•	•	k?
Roy	Roy	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Schafer	Schafer	k1gMnSc1
•	•	k?
Heinz	Heinz	k1gMnSc1
Hartmann	Hartmann	k1gMnSc1
•	•	k?
Bruno	Bruno	k1gMnSc1
Bettelheim	Bettelheim	k1gMnSc1
•	•	k?
Janine	Janin	k1gInSc5
Chasseguet-Smirgelová	Chasseguet-Smirgelová	k1gFnSc1
•	•	k?
Judith	Juditha	k1gFnPc2
Butlerová	Butlerový	k2eAgFnSc1d1
•	•	k?
Nancy	Nancy	k1gFnSc1
Chodorowová	Chodorowová	k1gFnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Balint	Balint	k1gMnSc1
•	•	k?
Harry	Harra	k1gFnSc2
Guntrip	Guntrip	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Sandler	Sandler	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Kernberg	Kernberg	k1gMnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Fairbairn	Fairbairn	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Bowlby	Bowlba	k1gFnSc2
•	•	k?
Clara	Clara	k1gFnSc1
Thompsonová	Thompsonová	k1gFnSc1
•	•	k?
Edgar	Edgar	k1gMnSc1
Levenson	Levenson	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Stern	sternum	k1gNnPc2
•	•	k?
Stephen	Stephen	k2eAgMnSc1d1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
René	René	k1gMnSc1
Spitz	Spitz	k1gMnSc1
•	•	k?
Wilfred	Wilfred	k1gMnSc1
Bion	Bion	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Green	Green	k2eAgMnSc1d1
•	•	k?
Heinz	Heinz	k1gMnSc1
Kohut	Kohut	k1gMnSc1
Ovlivněni	ovlivněn	k2eAgMnPc1d1
psychoanalýzou	psychoanalýza	k1gFnSc7
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1
Brouk	brouk	k1gMnSc1
•	•	k?
Záviš	Záviš	k1gMnSc1
Kalandra	Kalandr	k1gMnSc2
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Effenberger	Effenberger	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Pechar	Pechar	k1gMnSc1
•	•	k?
Zbyněk	Zbyněk	k1gMnSc1
Havlíček	Havlíček	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Švankmajer	Švankmajer	k1gMnSc1
•	•	k?
Gaston	Gaston	k1gInSc1
Bachelard	Bachelard	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Breton	Breton	k1gMnSc1
•	•	k?
Salvador	Salvador	k1gMnSc1
Dalí	Dalí	k1gMnSc1
•	•	k?
Roland	Roland	k1gInSc1
Barthes	Barthes	k1gMnSc1
•	•	k?
Gilles	Gilles	k1gMnSc1
Deleuze	Deleuze	k1gFnSc2
•	•	k?
Paul	Paul	k1gMnSc1
Ricoeur	Ricoeur	k1gMnSc1
•	•	k?
Géza	géz	k1gMnSc4
Róheim	Róheim	k1gMnSc1
Koncepty	koncept	k1gInPc4
a	a	k8xC
pojmy	pojem	k1gInPc4
</s>
<s>
Nevědomí	nevědomí	k1gNnSc4
•	•	k?
Předvědomí	předvědomý	k2eAgMnPc1d1
•	•	k?
Libido	libido	k1gNnSc4
•	•	k?
Ego	ego	k1gNnSc2
<g/>
,	,	kIx,
superego	superego	k1gMnSc1
a	a	k8xC
id	idy	k1gFnPc2
•	•	k?
Freudovské	freudovský	k2eAgNnSc4d1
přeřeknutí	přeřeknutí	k1gNnSc4
•	•	k?
Oidipovský	oidipovský	k2eAgInSc4d1
komplex	komplex	k1gInSc4
•	•	k?
Vytěsnění	vytěsnění	k1gNnSc2
•	•	k?
Narcismus	narcismus	k1gInSc4
•	•	k?
Volné	volný	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
•	•	k?
Psychoanalytické	psychoanalytický	k2eAgInPc4d1
směry	směr	k1gInPc4
•	•	k?
Pud	pud	k1gInSc4
života	život	k1gInSc2
a	a	k8xC
pud	pud	k1gInSc1
smrti	smrt	k1gFnSc2
•	•	k?
Zrcadlová	zrcadlový	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
•	•	k?
Přenos	přenos	k1gInSc1
•	•	k?
Archetyp	archetyp	k1gInSc4
•	•	k?
Bytostné	bytostný	k2eAgFnSc2d1
Já	já	k3xPp1nSc1
•	•	k?
Projektivní	projektivní	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
•	•	k?
Attachment	Attachment	k1gInSc1
(	(	kIx(
<g/>
citová	citový	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sexualita	sexualita	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4140090-2	4140090-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2015001702	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2015001702	#num#	k4
</s>
