<s>
Dóm	dóm	k1gInSc1	dóm
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Fiore	Fior	k1gMnSc5	Fior
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
snoubí	snoubit	k5eAaImIp3nS	snoubit
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
toskánského	toskánský	k2eAgInSc2d1	toskánský
typu	typ	k1gInSc2	typ
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
kopulí	kopule	k1gFnSc7	kopule
<g/>
,	,	kIx,	,
dominuje	dominovat	k5eAaImIp3nS	dominovat
panoramatu	panorama	k1gNnSc3	panorama
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
