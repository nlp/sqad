<p>
<s>
Dóm	dóm	k1gInSc1	dóm
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Fiore	Fior	k1gMnSc5	Fior
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
snoubí	snoubit	k5eAaImIp3nS	snoubit
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
toskánského	toskánský	k2eAgInSc2d1	toskánský
typu	typ	k1gInSc2	typ
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
kopulí	kopule	k1gFnSc7	kopule
<g/>
,	,	kIx,	,
dominuje	dominovat	k5eAaImIp3nS	dominovat
panoramatu	panorama	k1gNnSc3	panorama
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Santa	Sant	k1gMnSc4	Sant
Reparata	Reparat	k1gMnSc4	Reparat
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
lodi	loď	k1gFnSc2	loď
dnešní	dnešní	k2eAgFnSc2d1	dnešní
katedrály	katedrála	k1gFnSc2	katedrála
stával	stávat	k5eAaImAgInS	stávat
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
Santa	Sant	k1gMnSc2	Sant
Reparata	Reparat	k1gMnSc2	Reparat
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
postavený	postavený	k2eAgMnSc1d1	postavený
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
válek	válek	k1gInSc4	válek
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
zničený	zničený	k2eAgMnSc1d1	zničený
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
během	během	k7c2	během
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
znovu	znovu	k6eAd1	znovu
postavený	postavený	k2eAgMnSc1d1	postavený
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
výstavby	výstavba	k1gFnSc2	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
zbořeno	zbořen	k2eAgNnSc1d1	zbořeno
průčelí	průčelí	k1gNnSc1	průčelí
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
stavba	stavba	k1gFnSc1	stavba
postupně	postupně	k6eAd1	postupně
obklopila	obklopit	k5eAaPmAgFnS	obklopit
původní	původní	k2eAgFnSc4d1	původní
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1375	[number]	k4	1375
konaly	konat	k5eAaImAgInP	konat
běžně	běžně	k6eAd1	běžně
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
Santa	Santa	k1gFnSc1	Santa
Reparata	Reparata	k1gFnSc1	Reparata
zbourána	zbourán	k2eAgFnSc1d1	zbourána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
dómu	dóm	k1gInSc2	dóm
<g/>
,	,	kIx,	,
restaurovány	restaurován	k2eAgFnPc1d1	restaurována
a	a	k8xC	a
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvník	návštěvník	k1gMnSc1	návštěvník
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
zachované	zachovaný	k2eAgFnPc4d1	zachovaná
fresky	freska	k1gFnPc4	freska
<g/>
,	,	kIx,	,
náhrobní	náhrobní	k2eAgFnPc4d1	náhrobní
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
také	také	k9	také
náhrobek	náhrobek	k1gInSc1	náhrobek
stavitele	stavitel	k1gMnSc2	stavitel
kupole	kupole	k1gFnSc2	kupole
Filippa	Filipp	k1gMnSc2	Filipp
Brunelleschiho	Brunelleschi	k1gMnSc2	Brunelleschi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nová	nový	k2eAgFnSc1d1	nová
katedrála	katedrála	k1gFnSc1	katedrála
===	===	k?	===
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
florentské	florentský	k2eAgFnSc2d1	florentská
katedrály	katedrála	k1gFnSc2	katedrála
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
Florentské	florentský	k2eAgFnSc2d1	florentská
republiky	republika	k1gFnSc2	republika
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1296	[number]	k4	1296
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
významného	významný	k2eAgMnSc2d1	významný
architekta	architekt	k1gMnSc2	architekt
Arnolfa	Arnolf	k1gMnSc2	Arnolf
di	di	k?	di
Cambio	Cambio	k6eAd1	Cambio
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
původní	původní	k2eAgInSc1d1	původní
kostel	kostel	k1gInSc1	kostel
Santa	Sant	k1gMnSc2	Sant
Reparata	Reparat	k1gMnSc2	Reparat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Arnolfově	Arnolfův	k2eAgFnSc6d1	Arnolfův
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1302	[number]	k4	1302
byly	být	k5eAaImAgFnP	být
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
30	[number]	k4	30
let	léto	k1gNnPc2	léto
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
pokračování	pokračování	k1gNnSc1	pokračování
díla	dílo	k1gNnSc2	dílo
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
Giottovi	Giottův	k2eAgMnPc1d1	Giottův
di	di	k?	di
Bondone	Bondon	k1gMnSc5	Bondon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
pouze	pouze	k6eAd1	pouze
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
projekt	projekt	k1gInSc4	projekt
vedle	vedle	k7c2	vedle
stojící	stojící	k2eAgFnSc2d1	stojící
zvonice	zvonice	k1gFnSc2	zvonice
(	(	kIx(	(
<g/>
Giottova	Giottův	k2eAgFnSc1d1	Giottova
kampanila	kampanila	k1gFnSc1	kampanila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Francesca	Francescus	k1gMnSc2	Francescus
Talentiho	Talenti	k1gMnSc2	Talenti
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Cambiovy	Cambiův	k2eAgInPc4d1	Cambiův
plány	plán	k1gInPc4	plán
výrazně	výrazně	k6eAd1	výrazně
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
mohutné	mohutný	k2eAgFnPc1d1	mohutná
křížové	křížový	k2eAgFnPc1d1	křížová
žebrové	žebrový	k2eAgFnPc1d1	žebrová
klenby	klenba	k1gFnPc1	klenba
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
lodích	loď	k1gFnPc6	loď
i	i	k9	i
plán	plán	k1gInSc4	plán
vytvořit	vytvořit	k5eAaPmF	vytvořit
nad	nad	k7c7	nad
křížením	křížení	k1gNnSc7	křížení
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
kupoli	kupole	k1gFnSc4	kupole
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
i	i	k9	i
chór	chór	k1gInSc1	chór
<g/>
.	.	kIx.	.
</s>
<s>
Podélný	podélný	k2eAgInSc1d1	podélný
trakt	trakt	k1gInSc1	trakt
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
pole	pole	k1gNnPc4	pole
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
mají	mít	k5eAaImIp3nP	mít
rozměry	rozměr	k1gInPc1	rozměr
20	[number]	k4	20
×	×	k?	×
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
proto	proto	k8xC	proto
působí	působit	k5eAaImIp3nS	působit
nesmírně	smírně	k6eNd1	smírně
pádným	pádný	k2eAgInSc7d1	pádný
<g/>
,	,	kIx,	,
monumentálním	monumentální	k2eAgInSc7d1	monumentální
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1386	[number]	k4	1386
byly	být	k5eAaImAgInP	být
budovány	budován	k2eAgInPc1d1	budován
základy	základ	k1gInPc1	základ
chórového	chórový	k2eAgInSc2d1	chórový
závěru	závěr	k1gInSc2	závěr
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1389	[number]	k4	1389
byly	být	k5eAaImAgInP	být
budovány	budován	k2eAgInPc1d1	budován
pilíře	pilíř	k1gInPc1	pilíř
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1413	[number]	k4	1413
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
tambur	tambur	k1gInSc1	tambur
kupole	kupole	k1gFnSc2	kupole
a	a	k8xC	a
vynořila	vynořit	k5eAaPmAgFnS	vynořit
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
její	její	k3xOp3gFnSc2	její
technické	technický	k2eAgFnSc2d1	technická
realizace	realizace	k1gFnSc2	realizace
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
realizaci	realizace	k1gFnSc6	realizace
kupole	kupole	k1gFnSc1	kupole
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vypsána	vypsán	k2eAgFnSc1d1	vypsána
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Filippo	Filippa	k1gFnSc5	Filippa
Brunelleschi	Brunelleschi	k1gNnSc2	Brunelleschi
<g/>
.	.	kIx.	.
</s>
<s>
Dvouplášťová	dvouplášťový	k2eAgFnSc1d1	dvouplášťová
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
kupole	kupole	k1gFnSc1	kupole
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
letech	let	k1gInPc6	let
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zázrak	zázrak	k1gInSc4	zázrak
stavební	stavební	k2eAgFnSc2d1	stavební
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
zvenčí	zvenčí	k6eAd1	zvenčí
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
kladených	kladený	k2eAgFnPc2d1	kladená
ve	v	k7c6	v
vzoru	vzor	k1gInSc6	vzor
rybí	rybí	k2eAgFnSc2d1	rybí
kosti	kost	k1gFnSc2	kost
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
lešení	lešení	k1gNnSc2	lešení
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
rozpětí	rozpětí	k1gNnSc1	rozpětí
činí	činit	k5eAaImIp3nS	činit
41,97	[number]	k4	41,97
m.	m.	k?	m.
To	to	k9	to
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
stavbou	stavba	k1gFnSc7	stavba
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc2	rozpětí
kupole	kupole	k1gFnSc2	kupole
svatopetrské	svatopetrský	k2eAgFnSc2d1	Svatopetrská
baziliky	bazilika	k1gFnSc2	bazilika
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
60	[number]	k4	60
cm	cm	kA	cm
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
základna	základna	k1gFnSc1	základna
kupole	kupole	k1gFnSc2	kupole
Pantheonu	Pantheon	k1gInSc2	Pantheon
pak	pak	k6eAd1	pak
činí	činit	k5eAaImIp3nS	činit
43,22	[number]	k4	43,22
m	m	kA	m
–	–	k?	–
tyto	tento	k3xDgFnPc1	tento
rozměry	rozměra	k1gFnPc1	rozměra
byly	být	k5eAaImAgFnP	být
překonány	překonat	k5eAaPmNgFnP	překonat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
Haly	hala	k1gFnSc2	hala
století	století	k1gNnSc2	století
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1436	[number]	k4	1436
byla	být	k5eAaImAgFnS	být
katedrála	katedrála	k1gFnSc1	katedrála
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
papežem	papež	k1gMnSc7	papež
Evženem	Evžen	k1gMnSc7	Evžen
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
tehdy	tehdy	k6eAd1	tehdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
dómu	dóm	k1gInSc6	dóm
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1438	[number]	k4	1438
<g/>
–	–	k?	–
<g/>
1439	[number]	k4	1439
basilejsko-ferrarsko-florentský	basilejskoerrarskolorentský	k2eAgInSc4d1	basilejsko-ferrarsko-florentský
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
osobně	osobně	k6eAd1	osobně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
eVIII	eVIII	k?	eVIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
konstantinopolský	konstantinopolský	k2eAgMnSc1d1	konstantinopolský
patriarcha	patriarcha	k1gMnSc1	patriarcha
Josefos	Josefos	k1gMnSc1	Josefos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1439	[number]	k4	1439
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
církevní	církevní	k2eAgFnSc1d1	církevní
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
rodiny	rodina	k1gFnSc2	rodina
Pazziů	Pazzi	k1gMnPc2	Pazzi
spáchán	spáchán	k2eAgMnSc1d1	spáchán
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Medicejské	Medicejský	k2eAgNnSc4d1	Medicejské
<g/>
.	.	kIx.	.
</s>
<s>
Lorenzovi	Lorenz	k1gMnSc3	Lorenz
Nádhernému	nádherný	k2eAgMnSc3d1	nádherný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Giuliano	Giuliana	k1gFnSc5	Giuliana
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořen	k2eAgNnSc1d1	zbořeno
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
průčelí	průčelí	k1gNnSc1	průčelí
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
stavěl	stavět	k5eAaImAgMnS	stavět
di	di	k?	di
Cambio	Cambio	k1gMnSc1	Cambio
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
řada	řada	k1gFnSc1	řada
konkursů	konkurs	k1gInPc2	konkurs
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
dostavbu	dostavba	k1gFnSc4	dostavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
návrh	návrh	k1gInSc4	návrh
architekta	architekt	k1gMnSc2	architekt
Emilia	Emilius	k1gMnSc2	Emilius
de	de	k?	de
Fabris	Fabris	k1gFnSc2	Fabris
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
respektoval	respektovat	k5eAaImAgMnS	respektovat
historický	historický	k2eAgInSc4d1	historický
vzhled	vzhled	k1gInSc4	vzhled
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Novogotická	novogotický	k2eAgFnSc1d1	novogotická
přední	přední	k2eAgFnSc1d1	přední
fasáda	fasáda	k1gFnSc1	fasáda
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
stěny	stěna	k1gFnPc4	stěna
je	být	k5eAaImIp3nS	být
také	také	k9	také
pro	pro	k7c4	pro
chrámové	chrámový	k2eAgNnSc4d1	chrámové
průčelí	průčelí	k1gNnSc4	průčelí
typické	typický	k2eAgNnSc1d1	typické
obložení	obložení	k1gNnSc1	obložení
třemi	tři	k4xCgNnPc7	tři
druhy	druh	k1gInPc1	druh
mramoru	mramor	k1gInSc2	mramor
<g/>
:	:	kIx,	:
bílým	bílé	k1gNnSc7	bílé
z	z	k7c2	z
Carrary	Carrara	k1gFnSc2	Carrara
<g/>
,	,	kIx,	,
zeleným	zelený	k2eAgInSc7d1	zelený
z	z	k7c2	z
Prata	Prat	k1gInSc2	Prat
a	a	k8xC	a
růžovým	růžový	k2eAgMnPc3d1	růžový
z	z	k7c2	z
Maremma	maremma	k1gFnSc1	maremma
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
bohatá	bohatý	k2eAgFnSc1d1	bohatá
figurální	figurální	k2eAgFnSc1d1	figurální
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interiér	interiér	k1gInSc1	interiér
==	==	k?	==
</s>
</p>
<p>
<s>
Prostorná	prostorný	k2eAgFnSc1d1	prostorná
katedrála	katedrála	k1gFnSc1	katedrála
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgInSc7	pátý
největším	veliký	k2eAgInSc7d3	veliký
chrámem	chrám	k1gInSc7	chrám
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
153	[number]	k4	153
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
38	[number]	k4	38
m	m	kA	m
<g/>
,	,	kIx,	,
90	[number]	k4	90
m	m	kA	m
v	v	k7c6	v
příčné	příčný	k2eAgFnSc6d1	příčná
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
dóm	dóm	k1gInSc1	dóm
pojme	pojmout	k5eAaPmIp3nS	pojmout
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
omítnutý	omítnutý	k2eAgInSc1d1	omítnutý
interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
projevem	projev	k1gInSc7	projev
italské	italský	k2eAgFnSc2d1	italská
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
kupole	kupole	k1gFnSc2	kupole
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
Bruneleschiho	Bruneleschi	k1gMnSc2	Bruneleschi
plánů	plán	k1gInPc2	plán
zůstat	zůstat	k5eAaPmF	zůstat
bez	bez	k7c2	bez
malířské	malířský	k2eAgFnSc2d1	malířská
výzdoby	výzdoba	k1gFnSc2	výzdoba
nebo	nebo	k8xC	nebo
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1572	[number]	k4	1572
<g/>
–	–	k?	–
<g/>
1579	[number]	k4	1579
však	však	k9	však
provedli	provést	k5eAaPmAgMnP	provést
Giorgio	Giorgio	k6eAd1	Giorgio
Vasari	Vasar	k1gMnPc1	Vasar
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
Federico	Federico	k6eAd1	Federico
Zuccari	Zuccare	k1gFnSc4	Zuccare
freskovou	freskový	k2eAgFnSc4d1	fresková
výzdobu	výzdoba	k1gFnSc4	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
dvou	dva	k4xCgNnPc6	dva
stoletích	století	k1gNnPc6	století
bylo	být	k5eAaImAgNnS	být
proto	proto	k6eAd1	proto
často	často	k6eAd1	často
uvažováno	uvažován	k2eAgNnSc1d1	uvažováno
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
odstranění	odstranění	k1gNnSc6	odstranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
stěně	stěna	k1gFnSc6	stěna
dómu	dóm	k1gInSc2	dóm
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
povšimnutí	povšimnutí	k1gNnSc4	povšimnutí
medailón	medailón	k1gInSc1	medailón
s	s	k7c7	s
poprsím	poprsí	k1gNnSc7	poprsí
Bruneleschiho	Bruneleschi	k1gMnSc2	Bruneleschi
<g/>
,	,	kIx,	,
vlevo	vlevo	k6eAd1	vlevo
Danteho	Dante	k1gMnSc4	Dante
portrét	portrét	k1gInSc4	portrét
od	od	k7c2	od
Domenica	Domenic	k1gInSc2	Domenic
di	di	k?	di
Michelino	Michelin	k2eAgNnSc1d1	Michelino
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
Florencie	Florencie	k1gFnSc2	Florencie
(	(	kIx(	(
<g/>
panorama	panorama	k1gNnSc4	panorama
s	s	k7c7	s
chrámovou	chrámový	k2eAgFnSc7d1	chrámová
kopulí	kopule	k1gFnSc7	kopule
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
ovšem	ovšem	k9	ovšem
neexistovalo	existovat	k5eNaImAgNnS	existovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Božskou	božský	k2eAgFnSc7d1	božská
komedií	komedie	k1gFnSc7	komedie
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Fiore	Fior	k1gInSc5	Fior
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc4	stránka
dómské	dómský	k2eAgFnSc2d1	Dómská
huti	huť	k1gFnSc2	huť
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
</s>
</p>
<p>
<s>
Virtual	Virtual	k1gMnSc1	Virtual
Tour	Tour	k1gMnSc1	Tour
Santa	Santa	k1gMnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
del	del	k?	del
Fiore	Fior	k1gMnSc5	Fior
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
del	del	k?	del
Fiore	Fior	k1gInSc5	Fior
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
