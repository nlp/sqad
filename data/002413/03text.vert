<s>
Dýchání	dýchání	k1gNnSc1	dýchání
(	(	kIx(	(
<g/>
respirace	respirace	k1gFnSc1	respirace
(	(	kIx(	(
<g/>
ve	v	k7c6	v
fyziologii	fyziologie	k1gFnSc6	fyziologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ventilace	ventilace	k1gFnSc1	ventilace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
výměny	výměna	k1gFnSc2	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
organismem	organismus	k1gInSc7	organismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
externím	externí	k2eAgNnSc7d1	externí
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
navenek	navenek	k6eAd1	navenek
je	být	k5eAaImIp3nS	být
dech	dech	k1gInSc4	dech
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
rovněž	rovněž	k9	rovněž
kaskádu	kaskáda	k1gFnSc4	kaskáda
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
buněčné	buněčný	k2eAgNnSc4d1	buněčné
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
užití	užití	k1gNnSc1	užití
kyslíku	kyslík	k1gInSc2	kyslík
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
ATP.	atp.	kA	atp.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
typický	typický	k2eAgInSc4d1	typický
pro	pro	k7c4	pro
aerobní	aerobní	k2eAgInPc4d1	aerobní
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Anaerobní	anaerobní	k2eAgNnSc1d1	anaerobní
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kvašení	kvašení	k1gNnSc4	kvašení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgNnSc4d1	vnější
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
zvané	zvaný	k2eAgNnSc4d1	zvané
respirační	respirační	k2eAgNnSc4d1	respirační
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Respiračním	respirační	k2eAgNnSc7d1	respirační
médiem	médium	k1gNnSc7	médium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
většiny	většina	k1gFnSc2	většina
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
vodní	vodní	k2eAgMnPc1d1	vodní
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
dýchání	dýchání	k1gNnSc2	dýchání
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
zaměnován	zaměnovat	k5eAaImNgInS	zaměnovat
s	s	k7c7	s
buněčným	buněčný	k2eAgNnSc7d1	buněčné
dýcháním	dýchání	k1gNnSc7	dýchání
-	-	kIx~	-
cell	cello	k1gNnPc2	cello
respiration	respiration	k1gInSc1	respiration
(	(	kIx(	(
<g/>
dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
k	k	k7c3	k
biochemickým	biochemický	k2eAgFnPc3d1	biochemická
reakcím	reakce	k1gFnPc3	reakce
za	za	k7c2	za
vstupu	vstup	k1gInSc2	vstup
glukózy	glukóza	k1gFnSc2	glukóza
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
získání	získání	k1gNnSc1	získání
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
výměna	výměna	k1gFnSc1	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
respirační	respirační	k2eAgInSc4d1	respirační
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismem	mechanismus	k1gInSc7	mechanismus
přenosu	přenos	k1gInSc2	přenos
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
difuze	difuze	k1gFnSc1	difuze
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
výměna	výměna	k1gFnSc1	výměna
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc3	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
difuze	difuze	k1gFnSc1	difuze
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prvoků	prvok	k1gMnPc2	prvok
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
probíhá	probíhat	k5eAaImIp3nS	probíhat
dýchání	dýchání	k1gNnSc1	dýchání
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
žahavců	žahavec	k1gMnPc2	žahavec
a	a	k8xC	a
ploštěnců	ploštěnec	k1gMnPc2	ploštěnec
nejsou	být	k5eNaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
žádné	žádný	k3yNgInPc1	žádný
specializované	specializovaný	k2eAgInPc1d1	specializovaný
mechanismy	mechanismus	k1gInPc1	mechanismus
a	a	k8xC	a
dýchání	dýchání	k1gNnSc1	dýchání
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
žížaly	žížala	k1gFnSc2	žížala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
specializované	specializovaný	k2eAgInPc1d1	specializovaný
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
velký	velký	k2eAgInSc4d1	velký
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgInPc7	tři
nejběžnějšími	běžný	k2eAgInPc7d3	nejběžnější
dýchacími	dýchací	k2eAgInPc7d1	dýchací
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
plíce	plíce	k1gFnSc2	plíce
<g/>
,	,	kIx,	,
žábry	žábry	k1gFnPc4	žábry
a	a	k8xC	a
vzdušnice	vzdušnice	k1gFnPc4	vzdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
orgány	orgán	k1gInPc1	orgán
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
specifickou	specifický	k2eAgFnSc4d1	specifická
orgánovou	orgánový	k2eAgFnSc4d1	orgánová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
dýchací	dýchací	k2eAgFnSc4d1	dýchací
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
různé	různý	k2eAgFnPc4d1	různá
dýchací	dýchací	k2eAgFnPc4d1	dýchací
soustavy	soustava	k1gFnPc4	soustava
<g/>
,	,	kIx,	,
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
žábry	žábry	k1gFnPc1	žábry
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc1	plíce
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
u	u	k7c2	u
organismů	organismus	k1gInPc2	organismus
suchozemských	suchozemský	k2eAgInPc2d1	suchozemský
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
mechanismus	mechanismus	k1gInSc1	mechanismus
funkce	funkce	k1gFnSc2	funkce
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
soustav	soustava	k1gFnPc2	soustava
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
ventilaci	ventilace	k1gFnSc4	ventilace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
je	být	k5eAaImIp3nS	být
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
vzduch	vzduch	k1gInSc1	vzduch
stále	stále	k6eAd1	stále
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
dýchacího	dýchací	k2eAgInSc2d1	dýchací
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
obratlovců	obratlovec	k1gMnPc2	obratlovec
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nádechem	nádech	k1gInSc7	nádech
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
nový	nový	k2eAgInSc1d1	nový
vzduch	vzduch	k1gInSc1	vzduch
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
výdechem	výdech	k1gInSc7	výdech
se	se	k3xPyFc4	se
použitý	použitý	k2eAgInSc4d1	použitý
vzduch	vzduch	k1gInSc4	vzduch
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
organismů	organismus	k1gInPc2	organismus
se	se	k3xPyFc4	se
však	však	k9	však
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
mechanismy	mechanismus	k1gInPc4	mechanismus
ventilace	ventilace	k1gFnSc2	ventilace
dýchacího	dýchací	k2eAgInSc2d1	dýchací
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
kyslík	kyslík	k1gInSc1	kyslík
buď	buď	k8xC	buď
systémech	systém	k1gInPc6	systém
"	"	kIx"	"
<g/>
trubek	trubka	k1gFnPc2	trubka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
rozpuštěn	rozpuštěn	k2eAgMnSc1d1	rozpuštěn
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krvi	krev	k1gFnSc6	krev
jsou	být	k5eAaImIp3nP	být
přítomné	přítomný	k2eAgInPc1d1	přítomný
dýchací	dýchací	k2eAgInPc1d1	dýchací
pigmenty	pigment	k1gInPc1	pigment
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hemocyanin	hemocyanin	k2eAgInSc1d1	hemocyanin
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
členovci	členovec	k1gMnPc1	členovec
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
(	(	kIx(	(
<g/>
obratlovci	obratlovec	k1gMnPc1	obratlovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInPc4d1	dýchací
pohyby	pohyb	k1gInPc4	pohyb
obecně	obecně	k6eAd1	obecně
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
dýchací	dýchací	k2eAgNnSc4d1	dýchací
svalstvo	svalstvo	k1gNnSc4	svalstvo
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
orgány	orgán	k1gInPc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
rozličné	rozličný	k2eAgInPc4d1	rozličný
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
dostat	dostat	k5eAaPmF	dostat
čerstvý	čerstvý	k2eAgInSc4d1	čerstvý
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
žáby	žába	k1gFnPc1	žába
dýchají	dýchat	k5eAaImIp3nP	dýchat
při	při	k7c6	při
pozitivním	pozitivní	k2eAgInSc6d1	pozitivní
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
vdechem	vdech	k1gInSc7	vdech
sníží	snížit	k5eAaPmIp3nS	snížit
dno	dno	k1gNnSc4	dno
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc4d1	ústní
<g/>
,	,	kIx,	,
natáhnou	natáhnout	k5eAaPmIp3nP	natáhnout
vzduch	vzduch	k1gInSc4	vzduch
do	do	k7c2	do
nozder	nozdra	k1gFnPc2	nozdra
<g/>
,	,	kIx,	,
stlačí	stlačit	k5eAaPmIp3nS	stlačit
ústní	ústní	k2eAgFnSc4d1	ústní
dutinu	dutina	k1gFnSc4	dutina
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
vtlačen	vtlačit	k5eAaPmNgInS	vtlačit
dolů	dolů	k6eAd1	dolů
do	do	k7c2	do
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
dýchání	dýchání	k1gNnSc1	dýchání
při	při	k7c6	při
negativním	negativní	k2eAgInSc6d1	negativní
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
nitrohrudní	nitrohrudní	k2eAgInSc4d1	nitrohrudní
tlak	tlak	k1gInSc4	tlak
při	při	k7c6	při
vdechu	vdech	k1gInSc6	vdech
i	i	k8xC	i
výdechu	výdech	k1gInSc3	výdech
negativní	negativní	k2eAgMnSc1d1	negativní
vůči	vůči	k7c3	vůči
atmosférickému	atmosférický	k2eAgInSc3d1	atmosférický
tlaku	tlak	k1gInSc3	tlak
(	(	kIx(	(
<g/>
při	při	k7c6	při
maximálním	maximální	k2eAgInSc6d1	maximální
výdechu	výdech	k1gInSc6	výdech
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
hodnoty	hodnota	k1gFnPc1	hodnota
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
-0,8	-0,8	k4	-0,8
kPa	kPa	k?	kPa
při	při	k7c6	při
vdechu	vdech	k1gInSc6	vdech
a	a	k8xC	a
-0,33	-0,33	k4	-0,33
<g/>
kPa	kPa	k?	kPa
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
způsobu	způsob	k1gInSc6	způsob
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
tažen	táhnout	k5eAaImNgInS	táhnout
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
tlačen	tlačen	k2eAgMnSc1d1	tlačen
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
max	max	kA	max
<g/>
.	.	kIx.	.
nádechem	nádech	k1gInSc7	nádech
a	a	k8xC	a
max	max	kA	max
<g/>
.	.	kIx.	.
výdechem	výdech	k1gInSc7	výdech
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vitální	vitální	k2eAgFnSc1d1	vitální
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInPc1d1	dýchací
svaly	sval	k1gInPc1	sval
mění	měnit	k5eAaImIp3nP	měnit
objem	objem	k1gInSc4	objem
hrudního	hrudní	k2eAgInSc2d1	hrudní
koše	koš	k1gInSc2	koš
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
roztahují	roztahovat	k5eAaImIp3nP	roztahovat
plíce	plíce	k1gFnPc1	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
vyššího	vysoký	k2eAgInSc2d2	vyšší
tlaku	tlak	k1gInSc2	tlak
do	do	k7c2	do
nižšího	nízký	k2eAgInSc2d2	nižší
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vzduch	vzduch	k1gInSc1	vzduch
nasáván	nasáván	k2eAgInSc1d1	nasáván
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
se	se	k3xPyFc4	se
objem	objem	k1gInSc1	objem
plic	plíce	k1gFnPc2	plíce
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInPc1d1	dýchací
svaly	sval	k1gInPc1	sval
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
inspirační	inspirační	k2eAgMnPc4d1	inspirační
(	(	kIx(	(
<g/>
vdechové	vdechový	k2eAgMnPc4d1	vdechový
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
exspirační	exspirační	k2eAgFnSc6d1	exspirační
(	(	kIx(	(
<g/>
výdechové	výdechový	k2eAgFnSc6d1	výdechová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
ještě	ještě	k9	ještě
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
a	a	k8xC	a
na	na	k7c6	na
pomocné	pomocný	k2eAgFnSc6d1	pomocná
(	(	kIx(	(
<g/>
pomocné	pomocný	k2eAgInPc1d1	pomocný
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgInPc1d1	aktivní
jen	jen	k9	jen
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
zátěži	zátěž	k1gFnSc6	zátěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vdechové	vdechový	k2eAgInPc4d1	vdechový
svaly	sval	k1gInPc4	sval
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
bránice	bránice	k1gFnSc1	bránice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
stahu	stah	k1gInSc6	stah
vyklenuje	vyklenovat	k5eAaImIp3nS	vyklenovat
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
podélnou	podélný	k2eAgFnSc4d1	podélná
osu	osa	k1gFnSc4	osa
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Inervována	inervovat	k5eAaImNgFnS	inervovat
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
bráničního	brániční	k2eAgInSc2d1	brániční
nervu	nerv	k1gInSc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
zevní	zevní	k2eAgInPc4d1	zevní
mezižeberní	mezižeberní	k2eAgInPc4d1	mezižeberní
svaly	sval	k1gInPc4	sval
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zdvihají	zdvihat	k5eAaImIp3nP	zdvihat
žebra	žebro	k1gNnPc1	žebro
<g/>
)	)	kIx)	)
a	a	k8xC	a
šikmé	šikmý	k2eAgInPc1d1	šikmý
svaly	sval	k1gInPc1	sval
(	(	kIx(	(
<g/>
musculi	muscout	k5eAaPmAgMnP	muscout
scaleni	scalen	k2eAgMnPc1d1	scalen
<g/>
,	,	kIx,	,
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
se	se	k3xPyFc4	se
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
intenzitě	intenzita	k1gFnSc6	intenzita
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zdvihač	zdvihač	k1gMnSc1	zdvihač
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
Musculus	Musculus	k1gMnSc1	Musculus
sternocleidomastoideus	sternocleidomastoideus	k1gMnSc1	sternocleidomastoideus
<g/>
)	)	kIx)	)
zvedá	zvedat	k5eAaImIp3nS	zvedat
částečně	částečně	k6eAd1	částečně
hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
Při	při	k7c6	při
nezvýšeném	zvýšený	k2eNgNnSc6d1	nezvýšené
dýchání	dýchání	k1gNnSc6	dýchání
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
mechanismem	mechanismus	k1gInSc7	mechanismus
výdechu	výdech	k1gInSc2	výdech
pasivní	pasivní	k2eAgFnSc2d1	pasivní
retrakce	retrakce	k1gFnSc2	retrakce
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
hrudníku	hrudník	k1gInSc2	hrudník
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
tíhovou	tíhový	k2eAgFnSc7d1	tíhová
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
potenciální	potenciální	k2eAgFnSc7d1	potenciální
elastickou	elastický	k2eAgFnSc7d1	elastická
sílou	síla	k1gFnSc7	síla
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
neúčastní	účastnit	k5eNaImIp3nP	účastnit
výdechu	výdech	k1gInSc3	výdech
žádné	žádný	k3yNgInPc4	žádný
svaly	sval	k1gInPc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
zátěži	zátěž	k1gFnSc6	zátěž
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
svaly	sval	k1gInPc1	sval
pomocné	pomocný	k2eAgInPc1d1	pomocný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svaly	sval	k1gInPc1	sval
tzv.	tzv.	kA	tzv.
břišního	břišní	k2eAgInSc2d1	břišní
lisu	lis	k1gInSc2	lis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
táhnou	táhnout	k5eAaImIp3nP	táhnout
bránici	bránice	k1gFnSc4	bránice
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Břišní	břišní	k2eAgInSc1d1	břišní
lis	lis	k1gInSc1	lis
tvoří	tvořit	k5eAaImIp3nS	tvořit
přímý	přímý	k2eAgInSc4d1	přímý
sval	sval	k1gInSc4	sval
břišní	břišní	k2eAgInSc4d1	břišní
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgInSc4d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
šikmý	šikmý	k2eAgInSc4d1	šikmý
sval	sval	k1gInSc4	sval
břišní	břišní	k2eAgInSc4d1	břišní
<g/>
,	,	kIx,	,
příčný	příčný	k2eAgInSc4d1	příčný
sval	sval	k1gInSc4	sval
břišní	břišní	k2eAgFnSc2d1	břišní
aj.	aj.	kA	aj.
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
mohou	moct	k5eAaImIp3nP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
mezižeberní	mezižeberní	k2eAgInPc4d1	mezižeberní
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
táhnou	táhnout	k5eAaImIp3nP	táhnout
žebra	žebro	k1gNnPc1	žebro
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plicních	plicní	k2eAgInPc6d1	plicní
sklípcích	sklípek	k1gInPc6	sklípek
kyslík	kyslík	k1gInSc1	kyslík
difunduje	difundovat	k5eAaImIp3nS	difundovat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
kyslík	kyslík	k1gInSc1	kyslík
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
odkysličené	odkysličený	k2eAgFnSc6d1	odkysličená
krvi	krev	k1gFnSc6	krev
nižší	nízký	k2eAgInSc1d2	nižší
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
toho	ten	k3xDgNnSc2	ten
přechází	přecházet	k5eAaImIp3nP	přecházet
přes	přes	k7c4	přes
tenký	tenký	k2eAgInSc4d1	tenký
vlhký	vlhký	k2eAgInSc4d1	vlhký
epitel	epitel	k1gInSc4	epitel
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
vyšší	vysoký	k2eAgInSc1d2	vyšší
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
uniká	unikat	k5eAaImIp3nS	unikat
přes	přes	k7c4	přes
epitel	epitel	k1gInSc4	epitel
ven	ven	k6eAd1	ven
z	z	k7c2	z
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
využívají	využívat	k5eAaPmIp3nP	využívat
suchozemští	suchozemský	k2eAgMnPc1d1	suchozemský
obratlovci	obratlovec	k1gMnPc1	obratlovec
dýchacích	dýchací	k2eAgInPc2d1	dýchací
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
jím	jíst	k5eAaImIp1nS	jíst
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
v	v	k7c6	v
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Hemoglobin	hemoglobin	k1gInSc1	hemoglobin
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
podjednotky	podjednotka	k1gFnPc4	podjednotka
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
kofaktor	kofaktor	k1gInSc4	kofaktor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hemová	hemová	k1gFnSc1	hemová
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
centru	centrum	k1gNnSc6	centrum
nese	nést	k5eAaImIp3nS	nést
atom	atom	k1gInSc1	atom
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
molekula	molekula	k1gFnSc1	molekula
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
čtyři	čtyři	k4xCgFnPc4	čtyři
molekuly	molekula	k1gFnPc4	molekula
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
ven	ven	k6eAd1	ven
kyslík	kyslík	k1gInSc4	kyslík
v	v	k7c6	v
tenkých	tenký	k2eAgFnPc6d1	tenká
kapilárách	kapilára	k1gFnPc6	kapilára
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
radikálně	radikálně	k6eAd1	radikálně
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
rychlost	rychlost	k1gFnSc4	rychlost
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
podle	podle	k7c2	podle
gradientu	gradient	k1gInSc2	gradient
svého	svůj	k3xOyFgInSc2	svůj
parciálního	parciální	k2eAgInSc2d1	parciální
tlaku	tlak	k1gInSc2	tlak
přestupuje	přestupovat	k5eAaImIp3nS	přestupovat
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInPc4d1	dýchací
svaly	sval	k1gInPc4	sval
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
inervovány	inervovat	k5eAaImNgInP	inervovat
motoneurony	motoneuron	k1gInPc7	motoneuron
umístěnými	umístěný	k2eAgInPc7d1	umístěný
v	v	k7c6	v
C1-C8	C1-C8	k1gFnSc6	C1-C8
a	a	k8xC	a
Th	Th	k1gFnSc6	Th
<g/>
1	[number]	k4	1
<g/>
-Th	-Th	k?	-Th
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
krčním	krční	k2eAgInSc6d1	krční
míšním	míšní	k2eAgInSc6d1	míšní
segmentu	segment	k1gInSc6	segment
a	a	k8xC	a
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
hrudním	hrudní	k2eAgInSc6d1	hrudní
míšním	míšní	k2eAgInSc6d1	míšní
segmentu	segment	k1gInSc6	segment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
motoneuronům	motoneuron	k1gMnPc3	motoneuron
je	být	k5eAaImIp3nS	být
nadřazené	nadřazený	k2eAgNnSc1d1	nadřazené
tzv.	tzv.	kA	tzv.
dýchací	dýchací	k2eAgNnSc1d1	dýchací
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
<g/>
:	:	kIx,	:
Jedna	jeden	k4xCgFnSc1	jeden
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
při	při	k7c6	při
vdechu	vdech	k1gInSc6	vdech
<g/>
,	,	kIx,	,
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
druhou	druhý	k4xOgFnSc4	druhý
skupinu	skupina	k1gFnSc4	skupina
aktivní	aktivní	k2eAgFnSc4d1	aktivní
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
dýchací	dýchací	k2eAgNnSc1d1	dýchací
centrum	centrum	k1gNnSc1	centrum
také	také	k9	také
reaguje	reagovat	k5eAaBmIp3nS	reagovat
zvýšením	zvýšení	k1gNnSc7	zvýšení
frekvence	frekvence	k1gFnSc2	frekvence
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
potřeb	potřeba	k1gFnPc2	potřeba
organismu	organismus	k1gInSc2	organismus
při	při	k7c6	při
tělesné	tělesný	k2eAgFnSc6d1	tělesná
zátěži	zátěž	k1gFnSc6	zátěž
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dýchacímu	dýchací	k2eAgNnSc3d1	dýchací
centru	centrum	k1gNnSc3	centrum
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
je	být	k5eAaImIp3nS	být
nadřazena	nadřazen	k2eAgFnSc1d1	nadřazena
retikulární	retikulární	k2eAgFnSc1d1	retikulární
formace	formace	k1gFnSc1	formace
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
moduluje	modulovat	k5eAaImIp3nS	modulovat
činnost	činnost	k1gFnSc4	činnost
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
centra	centrum	k1gNnSc2	centrum
na	na	k7c6	na
základě	základ	k1gInSc6	základ
aferentních	aferentní	k2eAgInPc2d1	aferentní
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
periferních	periferní	k2eAgInPc2d1	periferní
receptorů	receptor	k1gInPc2	receptor
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
různých	různý	k2eAgFnPc2d1	různá
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
také	také	k9	také
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
etáží	etáž	k1gFnPc2	etáž
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
činnosti	činnost	k1gFnSc2	činnost
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
dýchání	dýchání	k1gNnSc2	dýchání
zasahovat	zasahovat	k5eAaImF	zasahovat
i	i	k9	i
vědomě	vědomě	k6eAd1	vědomě
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zadržet	zadržet	k5eAaPmF	zadržet
dech	dech	k1gInSc4	dech
<g/>
,	,	kIx,	,
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
rychlost	rychlost	k1gFnSc4	rychlost
dýchání	dýchání	k1gNnSc2	dýchání
apod.	apod.	kA	apod.
Daleko	daleko	k6eAd1	daleko
významnější	významný	k2eAgFnSc1d2	významnější
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
jsou	být	k5eAaImIp3nP	být
automatické	automatický	k2eAgInPc1d1	automatický
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
řídí	řídit	k5eAaImIp3nP	řídit
dýchání	dýchání	k1gNnSc4	dýchání
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
chod	chod	k1gInSc1	chod
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
především	především	k9	především
autonomní	autonomní	k2eAgFnSc1d1	autonomní
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
např.	např.	kA	např.
kašlacím	kašlací	k2eAgInSc7d1	kašlací
a	a	k8xC	a
kýchacím	kýchací	k2eAgInSc7d1	kýchací
reflexem	reflex	k1gInSc7	reflex
<g/>
,	,	kIx,	,
polykáním	polykání	k1gNnSc7	polykání
<g/>
,	,	kIx,	,
zíváním	zívání	k1gNnSc7	zívání
<g/>
,	,	kIx,	,
mluvením	mluvení	k1gNnSc7	mluvení
<g/>
,	,	kIx,	,
zpíváním	zpívání	k1gNnSc7	zpívání
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
emočními	emoční	k2eAgInPc7d1	emoční
a	a	k8xC	a
psychickými	psychický	k2eAgInPc7d1	psychický
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Glomus	Glomus	k1gInSc1	Glomus
caroticum	caroticum	k1gInSc1	caroticum
a	a	k8xC	a
glomus	glomus	k1gInSc1	glomus
aorticum	aorticum	k1gInSc1	aorticum
<g/>
,	,	kIx,	,
periferní	periferní	k2eAgInPc1d1	periferní
chemoreceptory	chemoreceptor	k1gInPc1	chemoreceptor
v	v	k7c6	v
oblouku	oblouk	k1gInSc6	oblouk
aorty	aorta	k1gFnSc2	aorta
a	a	k8xC	a
při	při	k7c6	při
a.	a.	k?	a.
<g/>
carotis	carotis	k1gFnPc1	carotis
communis	communis	k1gFnPc2	communis
registrují	registrovat	k5eAaBmIp3nP	registrovat
parciální	parciální	k2eAgInSc4d1	parciální
tlak	tlak	k1gInSc4	tlak
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
arteriální	arteriální	k2eAgFnSc6d1	arteriální
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
citlivější	citlivý	k2eAgFnPc1d2	citlivější
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
vzestup	vzestup	k1gInSc4	vzestup
parciálního	parciální	k2eAgInSc2d1	parciální
tlaku	tlak	k1gInSc2	tlak
CO2	CO2	k1gFnSc2	CO2
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
pH	ph	kA	ph
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
receptory	receptor	k1gInPc1	receptor
informují	informovat	k5eAaBmIp3nP	informovat
výše	vysoce	k6eAd2	vysoce
nadřazená	nadřazený	k2eAgNnPc1d1	nadřazené
centra	centrum	k1gNnPc1	centrum
zvýšením	zvýšení	k1gNnSc7	zvýšení
své	svůj	k3xOyFgFnSc2	svůj
frekvence	frekvence	k1gFnSc2	frekvence
impulsů	impuls	k1gInPc2	impuls
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
při	při	k7c6	při
snížení	snížení	k1gNnSc6	snížení
PO2	PO2	k1gFnSc2	PO2
pod	pod	k7c7	pod
8	[number]	k4	8
<g/>
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
normální	normální	k2eAgFnSc1d1	normální
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
13,66	[number]	k4	13,66
kPa	kPa	k?	kPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
z	z	k7c2	z
glomus	glomus	k1gInSc4	glomus
caroticum	caroticum	k1gNnSc1	caroticum
a	a	k8xC	a
aorticum	aorticum	k1gNnSc1	aorticum
cestou	cestou	k7c2	cestou
n.	n.	k?	n.
<g/>
vagus	vagus	k1gInSc1	vagus
a	a	k8xC	a
n.	n.	k?	n.
<g/>
glossopharyngeus	glossopharyngeus	k1gInSc1	glossopharyngeus
zajistí	zajistit	k5eAaPmIp3nS	zajistit
zvýšení	zvýšení	k1gNnSc4	zvýšení
dýchací	dýchací	k2eAgFnSc2d1	dýchací
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajistí	zajistit	k5eAaPmIp3nP	zajistit
opětovnou	opětovný	k2eAgFnSc4d1	opětovná
normalizaci	normalizace	k1gFnSc4	normalizace
PO	Po	kA	Po
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1	centrální
chemosenzory	chemosenzora	k1gFnPc1	chemosenzora
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
prodloužené	prodloužený	k2eAgFnSc2d1	prodloužená
míchy	mícha	k1gFnSc2	mícha
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
vzestup	vzestup	k1gInSc4	vzestup
CO2	CO2	k1gFnSc2	CO2
a	a	k8xC	a
pH	ph	kA	ph
v	v	k7c6	v
likvoru	likvor	k1gInSc6	likvor
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
jimi	on	k3xPp3gMnPc7	on
zprostředkované	zprostředkovaný	k2eAgNnSc1d1	zprostředkované
zvýšení	zvýšení	k1gNnSc1	zvýšení
dýchací	dýchací	k2eAgFnSc2d1	dýchací
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hladinu	hladina	k1gFnSc4	hladina
CO2	CO2	k1gFnPc3	CO2
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
v	v	k7c6	v
likvoru	likvor	k1gInSc6	likvor
sníží	snížit	k5eAaPmIp3nS	snížit
(	(	kIx(	(
<g/>
pH	ph	kA	ph
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
:	:	kIx,	:
Např.	např.	kA	např.
při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
PCO2	PCO2	k1gFnSc2	PCO2
z	z	k7c2	z
5	[number]	k4	5
kPa	kPa	k?	kPa
na	na	k7c4	na
9	[number]	k4	9
<g/>
kPa	kPa	k?	kPa
se	se	k3xPyFc4	se
minutový	minutový	k2eAgInSc1d1	minutový
dýchací	dýchací	k2eAgInSc1d1	dýchací
objem	objem	k1gInSc1	objem
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
desetinásobně	desetinásobně	k6eAd1	desetinásobně
<g/>
.	.	kIx.	.
</s>
<s>
Heringův-Breuerův	Heringův-Breuerův	k2eAgInSc1d1	Heringův-Breuerův
reflex	reflex	k1gInSc1	reflex
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
Breuerův-Heringův	Breuerův-Heringův	k2eAgInSc4d1	Breuerův-Heringův
reflex	reflex	k1gInSc4	reflex
<g/>
)	)	kIx)	)
snižuje	snižovat	k5eAaImIp3nS	snižovat
hloubku	hloubka	k1gFnSc4	hloubka
dechů	dech	k1gInPc2	dech
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
ventilaci	ventilace	k1gFnSc6	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
počátku	počátek	k1gInSc6	počátek
jsou	být	k5eAaImIp3nP	být
tahové	tahový	k2eAgInPc1d1	tahový
receptory	receptor	k1gInPc1	receptor
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
trachey	trachea	k1gFnSc2	trachea
a	a	k8xC	a
bronchů	bronchus	k1gInPc2	bronchus
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
dýchacích	dýchací	k2eAgInPc2d1	dýchací
svalů	sval	k1gInPc2	sval
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
zpětnými	zpětný	k2eAgFnPc7d1	zpětná
vazbami	vazba	k1gFnPc7	vazba
zprostředkovanými	zprostředkovaný	k2eAgFnPc7d1	zprostředkovaná
svalovými	svalový	k2eAgFnPc7d1	svalová
vřeténky	vřeténko	k1gNnPc7	vřeténko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
místní	místní	k2eAgInPc1d1	místní
míšními	míšní	k2eAgInPc7d1	míšní
reflexy	reflex	k1gInPc7	reflex
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
činnost	činnost	k1gFnSc4	činnost
dýchacích	dýchací	k2eAgInPc2d1	dýchací
svalů	sval	k1gInPc2	sval
odporům	odpor	k1gInPc3	odpor
v	v	k7c6	v
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
v	v	k7c6	v
hrudníku	hrudník	k1gInSc6	hrudník
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
anafylaktický	anafylaktický	k2eAgInSc1d1	anafylaktický
šok	šok	k1gInSc1	šok
-	-	kIx~	-
silná	silný	k2eAgFnSc1d1	silná
alergická	alergický	k2eAgFnSc1d1	alergická
reakce	reakce	k1gFnSc1	reakce
apnoe	apnoe	k1gFnSc1	apnoe
-	-	kIx~	-
zástava	zástava	k1gFnSc1	zástava
dechu	dech	k1gInSc2	dech
Aspirace	aspirace	k1gFnSc1	aspirace
(	(	kIx(	(
<g/>
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
Astma	astma	k1gFnSc1	astma
Dušení	dušení	k1gNnSc2	dušení
dyspnoe	dyspnoe	k1gFnSc1	dyspnoe
-	-	kIx~	-
dušnost	dušnost	k1gFnSc1	dušnost
hypoxie	hypoxie	k1gFnSc1	hypoxie
-	-	kIx~	-
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
zásobování	zásobování	k1gNnSc1	zásobování
tkání	tkáň	k1gFnPc2	tkáň
kyslíkem	kyslík	k1gInSc7	kyslík
myastenie	myastenie	k1gFnSc2	myastenie
-	-	kIx~	-
svalová	svalový	k2eAgFnSc1d1	svalová
ochablost	ochablost	k1gFnSc1	ochablost
a	a	k8xC	a
abnormální	abnormální	k2eAgFnSc1d1	abnormální
únava	únava	k1gFnSc1	únava
Otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
-	-	kIx~	-
CO2	CO2	k1gFnSc1	CO2
Otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
-	-	kIx~	-
CO	co	k8xS	co
Plynová	plynový	k2eAgFnSc1d1	plynová
embolie	embolie	k1gFnSc1	embolie
-	-	kIx~	-
embolie	embolie	k1gFnSc1	embolie
=	=	kIx~	=
náhlé	náhlý	k2eAgNnSc1d1	náhlé
zablokování	zablokování	k1gNnSc1	zablokování
krevní	krevní	k2eAgFnSc2d1	krevní
cévy	céva	k1gFnSc2	céva
pneumonie	pneumonie	k1gFnSc2	pneumonie
-	-	kIx~	-
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
Silný	silný	k2eAgInSc1d1	silný
akutní	akutní	k2eAgInSc1d1	akutní
respirační	respirační	k2eAgInSc1d1	respirační
syndrom	syndrom	k1gInSc1	syndrom
Syndrom	syndrom	k1gInSc1	syndrom
náhlého	náhlý	k2eAgNnSc2d1	náhlé
úmrtí	úmrtí	k1gNnSc2	úmrtí
kojenců	kojenec	k1gMnPc2	kojenec
plicní	plicní	k2eAgInSc4d1	plicní
edém	edém	k1gInSc4	edém
utopení	utopení	k1gNnSc2	utopení
Kossmaulovo	Kossmaulův	k2eAgNnSc4d1	Kossmaulův
dýchání	dýchání	k1gNnSc4	dýchání
Cheyne-Stokesove	Cheyne-Stokesov	k1gInSc5	Cheyne-Stokesov
dýchání	dýchání	k1gNnSc4	dýchání
Biotovo	Biotův	k2eAgNnSc1d1	Biotův
dýchání	dýchání	k1gNnSc1	dýchání
</s>
