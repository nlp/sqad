<s>
Schopnost	schopnost	k1gFnSc1	schopnost
akustického	akustický	k2eAgNnSc2d1	akustické
vnímání	vnímání	k1gNnSc2	vnímání
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
křečků	křeček	k1gMnPc2	křeček
velice	velice	k6eAd1	velice
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
také	také	k9	také
frekvence	frekvence	k1gFnPc4	frekvence
ze	z	k7c2	z
spektra	spektrum	k1gNnSc2	spektrum
ultrazvuku	ultrazvuk	k1gInSc2	ultrazvuk
<g/>
.	.	kIx.	.
</s>
