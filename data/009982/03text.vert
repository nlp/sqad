<p>
<s>
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc4d1	zelený
pigment	pigment	k1gInSc4	pigment
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
sinicích	sinice	k1gFnPc6	sinice
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
řasách	řasa	k1gFnPc6	řasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
energii	energie	k1gFnSc4	energie
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
sacharidů	sacharid	k1gInPc2	sacharid
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
představuje	představovat	k5eAaImIp3nS	představovat
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
transformátory	transformátor	k1gInPc1	transformátor
světelných	světelný	k2eAgNnPc2d1	světelné
kvant	kvantum	k1gNnPc2	kvantum
na	na	k7c4	na
biologicky	biologicky	k6eAd1	biologicky
zpracovatelnou	zpracovatelný	k2eAgFnSc4d1	zpracovatelná
formu	forma	k1gFnSc4	forma
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
ji	on	k3xPp3gFnSc4	on
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
makroergní	makroergní	k2eAgFnSc4d1	makroergní
chemickou	chemický	k2eAgFnSc4d1	chemická
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
uvedená	uvedený	k2eAgFnSc1d1	uvedená
reakce	reakce	k1gFnSc1	reakce
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
další	další	k2eAgFnPc4d1	další
biochemické	biochemický	k2eAgFnPc4d1	biochemická
a	a	k8xC	a
biologické	biologický	k2eAgFnPc4d1	biologická
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
fotosyntetickým	fotosyntetický	k2eAgInPc3d1	fotosyntetický
pigmentům	pigment	k1gInPc3	pigment
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fykobiliny	fykobilin	k1gInPc7	fykobilin
a	a	k8xC	a
karotenoidy	karotenoid	k1gInPc7	karotenoid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
energii	energie	k1gFnSc4	energie
z	z	k7c2	z
odlišné	odlišný	k2eAgFnSc2d1	odlišná
části	část	k1gFnSc2	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světelného	světelný	k2eAgNnSc2d1	světelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
červenou	červený	k2eAgFnSc4d1	červená
část	část	k1gFnSc4	část
světelného	světelný	k2eAgNnSc2d1	světelné
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
ostatní	ostatní	k2eAgMnSc1d1	ostatní
odráží	odrážet	k5eAaImIp3nS	odrážet
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
tak	tak	k6eAd1	tak
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
všem	všecek	k3xTgFnPc3	všecek
fotosyntetizujícím	fotosyntetizující	k2eAgFnPc3d1	fotosyntetizující
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
porfyriny	porfyrin	k1gInPc4	porfyrin
obsahující	obsahující	k2eAgInSc1d1	obsahující
hořčík	hořčík	k1gInSc1	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
chlorofyly	chlorofyl	k1gInPc1	chlorofyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
a	a	k8xC	a
f	f	k?	f
a	a	k8xC	a
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
bakteriochlorofyly	bakteriochlorofyl	k1gInPc1	bakteriochlorofyl
(	(	kIx(	(
<g/>
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
bakteriích	bakterie	k1gFnPc6	bakterie
mimo	mimo	k6eAd1	mimo
sinic	sinice	k1gFnPc2	sinice
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
a	a	k8xC	a
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Molekuly	molekula	k1gFnPc1	molekula
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
thylakoidních	thylakoidní	k2eAgFnPc6d1	thylakoidní
membránách	membrána	k1gFnPc6	membrána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
umístěné	umístěný	k2eAgFnPc4d1	umístěná
v	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
bílkovinami	bílkovina	k1gFnPc7	bílkovina
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
pigmenty	pigment	k1gInPc7	pigment
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
fotosystému	fotosystý	k2eAgInSc3d1	fotosystý
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
==	==	k?	==
</s>
</p>
<p>
<s>
Biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
začíná	začínat	k5eAaImIp3nS	začínat
připojením	připojení	k1gNnSc7	připojení
glutamátu	glutamát	k1gInSc2	glutamát
na	na	k7c4	na
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
glutamyl-tRNA	glutamylRNA	k?	glutamyl-tRNA
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
syntéze	syntéza	k1gFnSc6	syntéza
chlorofylů	chlorofyl	k1gInPc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Glutamyl-tRNA	GlutamylRNA	k?	Glutamyl-tRNA
se	se	k3xPyFc4	se
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
glutamyl-	glutamyl-	k?	glutamyl-
<g/>
1	[number]	k4	1
<g/>
-semialdehyd	emialdehyd	k1gInSc1	-semialdehyd
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
5	[number]	k4	5
<g/>
-aminolevulovou	minolevulový	k2eAgFnSc4d1	-aminolevulový
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
v	v	k7c4	v
porfobilinogen	porfobilinogen	k1gInSc4	porfobilinogen
<g/>
.	.	kIx.	.
</s>
<s>
Porfobilinogen	Porfobilinogen	k1gInSc1	Porfobilinogen
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
cyklický	cyklický	k2eAgMnSc1d1	cyklický
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pyrrolové	pyrrolový	k2eAgNnSc1d1	pyrrolový
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
porfobilinogeny	porfobilinogen	k1gInPc1	porfobilinogen
formují	formovat	k5eAaImIp3nP	formovat
protoporfyrin	protoporfyrin	k1gInSc4	protoporfyrin
IX	IX	kA	IX
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
porfyrin	porfyrin	k1gInSc4	porfyrin
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
enzymem	enzym	k1gInSc7	enzym
Fe	Fe	k1gFnSc2	Fe
chelatasou	chelatasa	k1gFnSc7	chelatasa
zabudováno	zabudován	k2eAgNnSc4d1	zabudováno
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
hem	hem	k1gInSc1	hem
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
cytochrom	cytochrom	k1gInSc4	cytochrom
či	či	k8xC	či
fytochrom	fytochrom	k1gInSc4	fytochrom
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Mg	mg	kA	mg
chelatasou	chelatasa	k1gFnSc7	chelatasa
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
hořčík	hořčík	k1gInSc1	hořčík
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
protochlorofylid	protochlorofylid	k1gInSc1	protochlorofylid
a.	a.	k?	a.
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
fotochemickou	fotochemický	k2eAgFnSc7d1	fotochemická
reakcí	reakce	k1gFnSc7	reakce
za	za	k7c2	za
spotřeby	spotřeba	k1gFnSc2	spotřeba
dvou	dva	k4xCgInPc2	dva
fotonů	foton	k1gInPc2	foton
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
NADPH	NADPH	kA	NADPH
změněn	změnit	k5eAaPmNgInS	změnit
reduktasou	reduktasa	k1gFnSc7	reduktasa
na	na	k7c4	na
chlorofylid	chlorofylid	k1gInSc4	chlorofylid
a.	a.	k?	a.
Připojením	připojení	k1gNnSc7	připojení
fytolu	fytol	k1gInSc2	fytol
pak	pak	k6eAd1	pak
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
oxidací	oxidace	k1gFnSc7	oxidace
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
b.	b.	k?	b.
</s>
</p>
<p>
<s>
Fotochemická	fotochemický	k2eAgFnSc1d1	fotochemická
reakce	reakce	k1gFnSc1	reakce
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
chlorofylidu	chlorofylid	k1gInSc2	chlorofylid
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
neschopnosti	neschopnost	k1gFnSc2	neschopnost
rostlin	rostlina	k1gFnPc2	rostlina
tvořit	tvořit	k5eAaImF	tvořit
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
a	a	k8xC	a
důvodem	důvod	k1gInSc7	důvod
blednutí	blednutí	k1gNnSc2	blednutí
etiolovaných	etiolovaný	k2eAgFnPc2d1	etiolovaný
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
týká	týkat	k5eAaImIp3nS	týkat
rostlin	rostlina	k1gFnPc2	rostlina
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
krytosemenné	krytosemenný	k2eAgFnPc1d1	krytosemenná
umí	umět	k5eAaImIp3nP	umět
tvořit	tvořit	k5eAaImF	tvořit
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Fyziologie	fyziologie	k1gFnSc1	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bakteriochlorofyl	bakteriochlorofyl	k1gInSc1	bakteriochlorofyl
–	–	k?	–
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
vyskytující	vyskytující	k2eAgInSc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
bakteriích	bakterie	k1gFnPc6	bakterie
</s>
</p>
<p>
<s>
Karotenoidy	karotenoid	k1gInPc1	karotenoid
a	a	k8xC	a
fykobiliny	fykobilin	k1gInPc1	fykobilin
–	–	k?	–
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
fotosyntetické	fotosyntetický	k2eAgInPc1d1	fotosyntetický
pigmenty	pigment	k1gInPc1	pigment
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
CoJeCo	CoJeCo	k1gMnSc1	CoJeCo
<g/>
,	,	kIx,	,
http://www.cojeco.cz	[url]	k1gMnSc1	http://www.cojeco.cz
</s>
</p>
