<s>
Baroda	Baroda	k1gFnSc1
</s>
<s>
Baroda	Baroda	k1gFnSc1
(	(	kIx(
<g/>
žlutě	žlutě	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
státu	stát	k1gInSc2
Baroda	Baroda	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
státě	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gNnSc6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Vadodará	Vadodarý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Baroda	Baroda	k1gMnSc1
byl	být	k5eAaImAgMnS
významný	významný	k2eAgInSc4d1
knížecí	knížecí	k2eAgInSc4d1
stát	stát	k1gInSc4
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
Bombaje	Bombaj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
několika	několik	k4yIc2
menších	malý	k2eAgFnPc2d2
enkláv	enkláva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
dohromady	dohromady	k6eAd1
rozlohu	rozloha	k1gFnSc4
21	#num#	k4
048	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
se	se	k3xPyFc4
dělil	dělit	k5eAaImAgInS
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
distrikty	distrikt	k1gInPc4
(	(	kIx(
<g/>
Kádi	káď	k1gFnSc6
<g/>
,	,	kIx,
Baroda	Barod	k1gMnSc4
<g/>
,	,	kIx,
Nasvári	Nasvári	k1gNnPc7
a	a	k8xC
Amreli	Amrel	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
ty	ten	k3xDgMnPc4
pak	pak	k6eAd1
na	na	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
okresy	okres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanikl	zaniknout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
dominia	dominion	k1gNnSc2
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tvořila	tvořit	k5eAaImAgFnS
ho	on	k3xPp3gNnSc4
převážně	převážně	k6eAd1
úrodná	úrodný	k2eAgFnSc1d1
nížina	nížina	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
protékaly	protékat	k5eAaImAgFnP
řeky	řeka	k1gFnPc1
Máhí	Máhí	k1gFnSc1
<g/>
,	,	kIx,
Narmada	Narmada	k1gFnSc1
a	a	k8xC
Táptí	Táptí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěstovaly	pěstovat	k5eAaImAgInP
se	se	k3xPyFc4
zde	zde	k6eAd1
hlavně	hlavně	k6eAd1
obilí	obilí	k1gNnSc1
(	(	kIx(
<g/>
proso	proso	k1gNnSc1
<g/>
,	,	kIx,
pšenice	pšenice	k1gFnSc1
a	a	k8xC
rýže	rýže	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bavlna	bavlna	k1gFnSc1
<g/>
,	,	kIx,
tabák	tabák	k1gInSc1
<g/>
,	,	kIx,
mák	mák	k1gInSc1
(	(	kIx(
<g/>
výroba	výroba	k1gFnSc1
opia	opium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
a	a	k8xC
olejniny	olejnina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
byl	být	k5eAaImAgInS
rozšířen	rozšířit	k5eAaPmNgInS
chov	chov	k1gInSc1
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Baroda	Baroda	k1gFnSc1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgInSc7
z	z	k7c2
domorodých	domorodý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
začal	začít	k5eAaPmAgInS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
budovat	budovat	k5eAaImF
železnici	železnice	k1gFnSc4
(	(	kIx(
<g/>
r.	r.	kA
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gFnSc1
hustota	hustota	k1gFnSc1
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
knížecích	knížecí	k2eAgMnPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1921	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
státu	stát	k1gInSc2
asi	asi	k9
1900	#num#	k4
km	km	kA
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
problémy	problém	k1gInPc1
ovšem	ovšem	k9
způsobovaly	způsobovat	k5eAaImAgInP
tři	tři	k4xCgInPc1
rozdílné	rozdílný	k2eAgInPc1d1
rozchody	rozchod	k1gInPc1
kol	kola	k1gFnPc2
(	(	kIx(
<g/>
široký	široký	k2eAgInSc1d1
indický	indický	k2eAgInSc1d1
<g/>
,	,	kIx,
evropský	evropský	k2eAgInSc1d1
a	a	k8xC
úzký	úzký	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
poměry	poměr	k1gInPc4
knížecích	knížecí	k2eAgMnPc2d1
států	stát	k1gInPc2
byla	být	k5eAaImAgFnS
na	na	k7c6
dobré	dobrý	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
i	i	k9
silniční	silniční	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Gáekvárů	Gáekvár	k1gMnPc2
byla	být	k5eAaImAgFnS
u	u	k7c2
moci	moc	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1720	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Mughalské	Mughalský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
jinde	jinde	k6eAd1
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
místní	místní	k2eAgMnSc1d1
maráthský	maráthský	k1gMnSc1
vůdce	vůdce	k1gMnSc1
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
a	a	k8xC
osamostatnil	osamostatnit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
1780	#num#	k4
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
první	první	k4xOgFnSc1
z	z	k7c2
několika	několik	k4yIc2
smluv	smlouva	k1gFnPc2
s	s	k7c7
Brity	Brit	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
uznali	uznat	k5eAaPmAgMnP
knížectví	knížectví	k1gNnSc4
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
hranicích	hranice	k1gFnPc6
a	a	k8xC
zabezpečovali	zabezpečovat	k5eAaImAgMnP
jeho	jeho	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
se	se	k3xPyFc4
barodský	barodský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
zavazoval	zavazovat	k5eAaImAgMnS
vydržovat	vydržovat	k5eAaImF
pro	pro	k7c4
policejní	policejní	k2eAgFnPc4d1
jednoty	jednota	k1gFnPc4
3	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
jezdectva	jezdectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1921	#num#	k4
měla	mít	k5eAaImAgFnS
Baroda	Baroda	k1gFnSc1
2	#num#	k4
126	#num#	k4
522	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
živících	živící	k2eAgMnPc2d1
se	se	k3xPyFc4
hlavně	hlavně	k6eAd1
zemědělstvím	zemědělství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgMnSc4
hinduisté	hinduista	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
89,8	89,8	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
8	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
džinisté	džinista	k1gMnPc1
2,1	2,1	k4
%	%	kIx~
a	a	k8xC
ostatní	ostatní	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
(	(	kIx(
<g/>
Pársové	Párs	k1gMnPc1
<g/>
,	,	kIx,
křesťané	křesťan	k1gMnPc1
<g/>
)	)	kIx)
0,1	0,1	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Armádu	armáda	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
cca	cca	kA
3	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
pravidelného	pravidelný	k2eAgInSc2d1
a	a	k8xC
cca	cca	kA
6	#num#	k4
250	#num#	k4
mužů	muž	k1gMnPc2
nepravidelného	pravidelný	k2eNgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
se	s	k7c7
42	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
státu	stát	k1gInSc2
byla	být	k5eAaImAgFnS
Baroda	Baroda	k1gFnSc1
s	s	k7c7
94	#num#	k4
712	#num#	k4
obyvateli	obyvatel	k1gMnPc7
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
palác	palác	k1gInSc1
Gáekvárů	Gáekvár	k1gInPc2
<g/>
,	,	kIx,
centrální	centrální	k2eAgNnSc1d1
tržiště	tržiště	k1gNnSc1
<g/>
,	,	kIx,
aréna	aréna	k1gFnSc1
pro	pro	k7c4
zápasy	zápas	k1gInPc4
zvířat	zvíře	k1gNnPc2
a	a	k8xC
zvěřinec	zvěřinec	k1gMnSc1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
nemocnice	nemocnice	k1gFnSc1
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
ústřední	ústřední	k2eAgNnSc1d1
vězení	vězení	k1gNnSc1
a	a	k8xC
pochopitelně	pochopitelně	k6eAd1
státní	státní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
a	a	k8xC
množství	množství	k1gNnSc1
chrámů	chrám	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Baroda	Barod	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Baroda	Baroda	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
112709-3	112709-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
234710200	#num#	k4
</s>
