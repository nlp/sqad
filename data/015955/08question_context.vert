<s>
Baroda	Baroda	k1gFnSc1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgInSc7
z	z	k7c2
domorodých	domorodý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
začal	začít	k5eAaPmAgInS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
budovat	budovat	k5eAaImF
železnici	železnice	k1gFnSc4
(	(	kIx(
<g/>
r.	r.	kA
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gFnSc1
hustota	hustota	k1gFnSc1
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
knížecích	knížecí	k2eAgMnPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>