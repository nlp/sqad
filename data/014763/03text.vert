<s>
Ice	Ice	k?
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
Crow	Crow	k1gFnSc2
</s>
<s>
Ice	Ice	k?
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
CrowInterpretCaptain	CrowInterpretCaptain	k1gMnSc1
BeefheartDruh	BeefheartDruh	k1gMnSc1
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydánozáří	albumVydánozáří	k1gNnSc4
1982	#num#	k4
<g/>
Nahránokvěten	Nahránokvěten	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
červen	červen	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
Warner	Warner	k1gInSc1
Brothers	Brothers	k1gInSc1
Studios	Studios	k?
<g/>
,	,	kIx,
North	North	k1gInSc1
Hollywood	Hollywood	k1gInSc1
<g/>
,	,	kIx,
KalifornieŽánryrock	KalifornieŽánryrock	k1gInSc1
<g/>
,	,	kIx,
spoken	spoken	k1gInSc1
wordDélka	wordDélka	k1gFnSc1
<g/>
37	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
VydavatelstvíVirginProducentDon	VydavatelstvíVirginProducentDona	k1gFnPc2
Van	vana	k1gFnPc2
VlietProfesionální	VlietProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Christgau	Christgaus	k1gInSc2
(	(	kIx(
<g/>
A-	A-	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mark	Mark	k1gMnSc1
Prindle	Prindle	k1gMnSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Piero	Piero	k1gNnSc1
Scaruffi	Scaruff	k1gFnSc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Captain	Captain	k2eAgInSc1d1
Beefheart	Beefheart	k1gInSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Doc	doc	kA
at	at	k?
the	the	k?
Radar	radar	k1gInSc1
Station	station	k1gInSc1
<g/>
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ice	Ice	k?
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
Crow	Crow	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Legendary	Legendara	k1gFnPc4
A	A	kA
<g/>
&	&	k?
<g/>
M	M	kA
Sessions	Sessions	k1gInSc1
<g/>
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ice	Ice	k?
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
Crow	Crow	k1gFnSc2
je	být	k5eAaImIp3nS
dvanácté	dvanáctý	k4xOgNnSc4
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
Captaina	Captain	k2eAgMnSc2d1
Beefhearta	Beefheart	k1gMnSc2
a	a	k8xC
skupiny	skupina	k1gFnSc2
Magic	Magice	k1gFnPc2
Band	banda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
září	září	k1gNnSc6
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgMnSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
Don	Don	k1gMnSc1
Van	vana	k1gFnPc2
Vliet	Vliet	k1gMnSc1
použil	použít	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
Captain	Captaina	k1gFnPc2
Beefheart	Beefhearta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vydání	vydání	k1gNnSc6
alba	album	k1gNnSc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
kariéře	kariéra	k1gFnSc6
jako	jako	k9
malíř	malíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Všechny	všechen	k3xTgFnPc4
skladby	skladba	k1gFnPc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
Don	Don	k1gMnSc1
Van	vana	k1gFnPc2
Vliet	Vliet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Ice	Ice	k1gFnSc1
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
Crow	Crow	k1gFnSc2
<g/>
"	"	kIx"
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gMnSc1
Host	host	k1gMnSc1
the	the	k?
Ghost	Ghost	k1gMnSc1
the	the	k?
Most	most	k1gInSc1
Holy-O	Holy-O	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Semi-Multicoloured	Semi-Multicoloured	k1gMnSc1
Caucasian	Caucasian	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Hey	Hey	k1gFnPc1
Garland	Garlando	k1gNnPc2
<g/>
,	,	kIx,
I	i	k9
Dig	Dig	k1gMnSc7
Your	Youra	k1gFnPc2
Tweed	tweed	k1gInSc1
Coat	Coat	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Evening	Evening	k1gInSc1
Bell	bell	k1gInSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Cardboard	Cardboard	k1gMnSc1
Cutout	Cutout	k1gMnSc1
Sundown	Sundown	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gFnSc1
Past	past	k1gFnSc1
Sure	Sure	k1gFnSc1
Is	Is	k1gFnSc1
Tense	tense	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Ink	Ink	k1gMnSc1
Mathematics	Mathematicsa	k1gFnPc2
<g/>
"	"	kIx"
–	–	k?
1	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gMnSc1
Witch	Witch	k1gMnSc1
Doctor	Doctor	k1gMnSc1
Life	Life	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
'	'	kIx"
<g/>
81	#num#	k4
<g/>
'	'	kIx"
Poop	Poop	k1gMnSc1
Hatch	Hatch	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gMnSc1
Thousandth	Thousandth	k1gMnSc1
and	and	k?
Tenth	Tenth	k1gMnSc1
Day	Day	k1gMnSc1
of	of	k?
the	the	k?
Human	Human	k1gInSc1
Totem	totem	k1gInSc1
Pole	pole	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Skeleton	skeleton	k1gInSc1
Makes	Makes	k1gMnSc1
Good	Good	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Light	Light	k2eAgMnSc1d1
Reflected	Reflected	k1gMnSc1
Off	Off	k1gFnSc2
The	The	k1gMnSc1
Oceans	Oceansa	k1gFnPc2
Of	Of	k1gMnSc1
The	The	k1gMnSc1
Moon	Moon	k1gMnSc1
<g/>
"	"	kIx"
-	-	kIx~
4	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
[	[	kIx(
<g/>
Bonus	bonus	k1gInSc1
Track	Track	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.robertchristgau.com/get_artist.php?name=Captain+Beefheart+and+the+Magic+Band	http://www.robertchristgau.com/get_artist.php?name=Captain+Beefheart+and+the+Magic+Band	k1gInSc1
<g/>
↑	↑	k?
http://markprindle.com/captain.htm#ice	http://markprindle.com/captain.htm#ice	k1gFnSc2
<g/>
↑	↑	k?
http://www.scaruffi.com/vol1/beefhear.html	http://www.scaruffi.com/vol1/beefhear.html	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Captain	Captain	k2eAgInSc1d1
Beefheart	Beefheart	k1gInSc1
and	and	k?
The	The	k1gFnSc2
Magic	Magice	k1gFnPc2
Band	band	k1gInSc1
Captain	Captain	k1gMnSc1
Beefheart	Beefhearta	k1gFnPc2
•	•	k?
Doug	Doug	k1gMnSc1
Moon	Moon	k1gMnSc1
•	•	k?
Zoot	Zoot	k1gMnSc1
Horn	Horn	k1gMnSc1
Rollo	Rollo	k1gNnSc1
•	•	k?
Rockette	Rockett	k1gInSc5
Morton	Morton	k1gInSc1
•	•	k?
John	John	k1gMnSc1
French	French	k1gMnSc1
(	(	kIx(
<g/>
Drumbo	Drumba	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Mascara	Mascara	k1gFnSc1
Snake	Snake	k1gFnSc1
•	•	k?
Feelers	Feelers	k1gInSc1
Rebo	Rebo	k1gMnSc1
•	•	k?
Gary	Gara	k1gFnSc2
Lucas	Lucas	k1gMnSc1
•	•	k?
Moris	Moris	k1gInSc1
Tepper	Tepper	k1gMnSc1
•	•	k?
Ed	Ed	k1gMnSc1
Marimba	Marimba	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Williams	Williamsa	k1gFnPc2
•	•	k?
Michael	Michael	k1gMnSc1
Traylor	Traylor	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Safe	safe	k1gInSc1
as	as	k9
Milk	Milk	k1gMnSc1
•	•	k?
Strictly	Strictly	k1gMnSc1
Personal	Personal	k1gMnSc1
•	•	k?
Trout	trout	k5eAaImF
Mask	Mask	k1gInSc1
Replica	Replic	k1gInSc2
•	•	k?
Lick	Lick	k1gMnSc1
My	my	k3xPp1nPc1
Decals	Decals	k1gInSc1
Off	Off	k1gFnPc1
<g/>
,	,	kIx,
Baby	baba	k1gFnPc1
•	•	k?
Mirror	Mirror	k1gMnSc1
Man	Man	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Spotlight	Spotlight	k1gMnSc1
Kid	Kid	k1gMnSc1
•	•	k?
Clear	Clear	k1gInSc1
Spot	spot	k1gInSc1
•	•	k?
Unconditionally	Unconditionalla	k1gFnSc2
Guaranteed	Guaranteed	k1gMnSc1
•	•	k?
Bluejeans	Bluejeans	k1gInSc1
&	&	k?
Moonbeams	Moonbeams	k1gInSc1
•	•	k?
Shiny	Shina	k1gFnSc2
Beast	Beast	k1gMnSc1
(	(	kIx(
<g/>
Bat	Bat	k1gMnSc1
Chain	Chain	k1gMnSc1
Puller	Puller	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Doc	doc	kA
at	at	k?
the	the	k?
Radar	radar	k1gInSc1
Station	station	k1gInSc1
•	•	k?
Ice	Ice	k1gFnSc1
Cream	Cream	k1gInSc1
for	forum	k1gNnPc2
Crow	Crow	k1gFnSc2
•	•	k?
Bat	Bat	k1gMnSc1
Chain	Chain	k1gMnSc1
Puller	Puller	k1gMnSc1
EP	EP	kA
</s>
<s>
The	The	k?
Legendary	Legendara	k1gFnPc4
A	A	kA
<g/>
&	&	k?
<g/>
M	M	kA
Sessions	Sessions	k1gInSc4
Živá	živý	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Bongo	bongo	k1gNnSc1
Fury	Fura	k1gFnSc2
•	•	k?
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Going	Going	k1gInSc1
to	ten	k3xDgNnSc1
Do	do	k7c2
What	Whata	k1gFnPc2
I	i	k8xC
Wanna	Wann	k1gInSc2
Do	do	k7c2
<g/>
:	:	kIx,
Live	Live	k1gFnSc1
at	at	k?
My	my	k3xPp1nPc1
Father	Fathra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Place	plac	k1gInSc6
1978	#num#	k4
Kompilace	kompilace	k1gFnSc1
</s>
<s>
Grow	Grow	k?
Fins	Fins	k1gInSc1
<g/>
:	:	kIx,
Rarities	Rarities	k1gInSc1
1965-1982	1965-1982	k4
Singly	singl	k1gInPc4
</s>
<s>
„	„	k?
<g/>
Diddy	Didda	k1gFnPc1
Wah	Wah	k1gMnSc2
Diddy	Didda	k1gFnSc2
<g/>
“	“	k?
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
Frank	Frank	k1gMnSc1
Zappa	Zapp	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
