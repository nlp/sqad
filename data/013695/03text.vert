<s>
Démétér	Démétér	k1gFnSc1
</s>
<s>
Deméter	Deméter	k1gInSc1
Symboly	symbol	k1gInPc1
</s>
<s>
pšeničný	pšeničný	k2eAgInSc1d1
nebo	nebo	k8xC
obilný	obilný	k2eAgInSc1d1
snop	snop	k1gInSc1
-	-	kIx~
pluh	pluh	k1gInSc1
-	-	kIx~
srp	srp	k1gInSc1
-	-	kIx~
Venušin	Venušin	k2eAgInSc1d1
pahorek	pahorek	k1gInSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Olymp	Olymp	k1gInSc1
Partner	partner	k1gMnSc1
</s>
<s>
Zeus	Zeus	k6eAd1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Kronos	Kronos	k1gMnSc1
a	a	k8xC
Rheia	Rheia	k1gFnSc1
Sourozenci	sourozenec	k1gMnSc3
</s>
<s>
Hestiá	Hestiá	k1gFnSc1
<g/>
,	,	kIx,
Héra	Héra	k1gFnSc1
<g/>
,	,	kIx,
Hádés	Hádés	k1gInSc1
<g/>
,	,	kIx,
Poseidóna	Poseidóna	k1gFnSc1
Zeus	Zeusa	k1gFnPc2
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
muž	muž	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cheirón	Cheirón	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Persefona	Persefona	k1gFnSc1
a	a	k8xC
Plútos	Plútos	k1gInSc1
Římský	římský	k2eAgInSc4d1
ekvivalent	ekvivalent	k1gInSc4
</s>
<s>
Ceres	ceres	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Démétér	Démétér	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Δ	Δ	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
dcerou	dcera	k1gFnSc7
Titána	Titán	k1gMnSc2
Krona	Kron	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
a	a	k8xC
sestry	sestra	k1gFnSc2
Rheie	Rheie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
bohyní	bohyně	k1gFnSc7
plodnosti	plodnost	k1gFnSc2
země	zem	k1gFnSc2
a	a	k8xC
rolnictví	rolnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
římským	římský	k2eAgInSc7d1
protějškem	protějšek	k1gInSc7
je	být	k5eAaImIp3nS
Ceres	ceres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Démétér	Démétér	k1gFnSc1
a	a	k8xC
Persefona	Persefona	k1gFnSc1
</s>
<s>
Její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Kronos	Kronos	k1gMnSc1
ji	on	k3xPp3gFnSc4
spolkl	spolknout	k5eAaPmAgMnS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
předtím	předtím	k6eAd1
její	její	k3xOp3gFnSc3
sourozence	sourozenka	k1gFnSc3
Hestii	Hestie	k1gFnSc4
<g/>
,	,	kIx,
Héru	Héra	k1gFnSc4
<g/>
,	,	kIx,
Háda	Hádes	k1gMnSc4
a	a	k8xC
Poseidóna	Poseidón	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgInS
ho	on	k3xPp3gMnSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
strach	strach	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
vzepřou	vzepřít	k5eAaPmIp3nP
a	a	k8xC
zbaví	zbavit	k5eAaPmIp3nP
ho	on	k3xPp3gMnSc4
moci	moct	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
božské	božský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
v	v	k7c6
něm	on	k3xPp3gMnSc6
přežívaly	přežívat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
vysvobodil	vysvobodit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gNnSc4
nejmladší	mladý	k2eAgMnSc1d3
bratr	bratr	k1gMnSc1
Zeus	Zeusa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
matka	matka	k1gFnSc1
Rheia	Rheia	k1gFnSc1
ho	on	k3xPp3gInSc4
porodila	porodit	k5eAaPmAgFnS
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
na	na	k7c6
Krétě	Kréta	k1gFnSc6
a	a	k8xC
Krona	Kron	k1gMnSc2
ošálila	ošálit	k5eAaPmAgFnS
kamenem	kámen	k1gInSc7
zabaleným	zabalený	k2eAgInSc7d1
v	v	k7c6
plenkách	plenka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
Zeus	Zeus	k1gInSc1
dospěl	dochvít	k5eAaPmAgInS
<g/>
,	,	kIx,
postavil	postavit	k5eAaPmAgMnS
se	se	k3xPyFc4
otci	otec	k1gMnSc3
<g/>
,	,	kIx,
donutil	donutit	k5eAaPmAgMnS
jej	on	k3xPp3gInSc4
vydat	vydat	k5eAaPmF
ze	z	k7c2
sebe	sebe	k3xPyFc4
všechny	všechen	k3xTgFnPc4
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
se	se	k3xPyFc4
nejvyšším	vysoký	k2eAgMnSc7d3
bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétér	Démétér	k1gFnPc2
vzal	vzít	k5eAaPmAgInS
k	k	k7c3
sobě	se	k3xPyFc3
na	na	k7c4
Olymp	Olymp	k1gInSc4
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
na	na	k7c4
starost	starost	k1gFnSc4
péči	péče	k1gFnSc4
o	o	k7c4
plodnost	plodnost	k1gFnSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
rolnictví	rolnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naučila	naučit	k5eAaPmAgFnS
lidi	člověk	k1gMnPc4
obdělávat	obdělávat	k5eAaImF
pole	pole	k1gNnPc4
<g/>
,	,	kIx,
usadit	usadit	k5eAaPmF
se	se	k3xPyFc4
místo	místo	k7c2
kočování	kočování	k1gNnSc2
na	na	k7c6
jednom	jeden	k4xCgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
dala	dát	k5eAaPmAgFnS
jim	on	k3xPp3gMnPc3
tak	tak	k9
nový	nový	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
a	a	k8xC
stanovila	stanovit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gInPc4
zákony	zákon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Diovi	Diův	k2eAgMnPc1d1
se	se	k3xPyFc4
Démétér	Démétér	k1gFnSc7
líbila	líbit	k5eAaImAgFnS
<g/>
,	,	kIx,
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
její	její	k3xOp3gFnSc4
přízeň	přízeň	k1gFnSc4
<g/>
,	,	kIx,
dokonce	dokonce	k9
zabil	zabít	k5eAaPmAgMnS
bleskem	blesk	k1gInSc7
thessalského	thessalský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Íasióna	Íasión	k1gMnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
Démétér	Démétér	k1gFnSc4
dala	dát	k5eAaPmAgFnS
syna	syn	k1gMnSc4
Plúta	Plút	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétér	Démétér	k1gFnPc2
nakonec	nakonec	k6eAd1
Diovi	Diův	k2eAgMnPc1d1
porodila	porodit	k5eAaPmAgFnS
dceru	dcera	k1gFnSc4
Persefonu	Persefona	k1gFnSc4
<g/>
;	;	kIx,
někdy	někdy	k6eAd1
bývá	bývat	k5eAaImIp3nS
nazývána	nazýván	k2eAgFnSc1d1
Kora	Kora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
jednou	jednou	k6eAd1
byla	být	k5eAaImAgFnS
Persefona	Persefona	k1gFnSc1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
družkami	družka	k1gFnPc7
<g/>
,	,	kIx,
otevřela	otevřít	k5eAaPmAgFnS
se	se	k3xPyFc4
před	před	k7c7
ní	on	k3xPp3gFnSc7
zem	zem	k1gFnSc4
<g/>
,	,	kIx,
vynořil	vynořit	k5eAaPmAgMnS
se	se	k3xPyFc4
bůh	bůh	k1gMnSc1
podsvětí	podsvětí	k1gNnSc2
Hádés	Hádésa	k1gFnPc2
a	a	k8xC
v	v	k7c6
mžiku	mžik	k1gInSc6
s	s	k7c7
ní	on	k3xPp3gFnSc7
zmizel	zmizet	k5eAaPmAgMnS
v	v	k7c6
hlubinách	hlubina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétér	Démétér	k1gFnPc2
hned	hned	k6eAd1
spěchala	spěchat	k5eAaImAgFnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nenašla	najít	k5eNaPmAgFnS
ani	ani	k8xC
stopu	stopa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloudila	bloudit	k5eAaImAgFnS
devět	devět	k4xCc4
dní	den	k1gInPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
od	od	k7c2
boha	bůh	k1gMnSc2
slunce	slunce	k1gNnSc2
Hélia	hélium	k1gNnSc2
dozvěděla	dozvědět	k5eAaPmAgFnS
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
se	se	k3xPyFc4
vypravila	vypravit	k5eAaPmAgFnS
za	za	k7c4
Diem	Diem	k1gInSc4
na	na	k7c4
Olymp	Olymp	k1gInSc4
a	a	k8xC
žádala	žádat	k5eAaImAgFnS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
Zeus	Zeus	k1gInSc1
však	však	k9
nemohl	moct	k5eNaImAgInS
Háda	Hádes	k1gMnSc4
donutit	donutit	k5eAaPmF
k	k	k7c3
vrácení	vrácení	k1gNnSc3
Persefony	Persefona	k1gFnSc2
matce	matka	k1gFnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
mezitím	mezitím	k6eAd1
vzal	vzít	k5eAaPmAgInS
za	za	k7c4
manželku	manželka	k1gFnSc4
a	a	k8xC
dal	dát	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
ochutnat	ochutnat	k5eAaPmF
jádra	jádro	k1gNnPc4
granátového	granátový	k2eAgNnSc2d1
jablka	jablko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tím	ten	k3xDgNnSc7
jí	jíst	k5eAaImIp3nS
zavřel	zavřít	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
zpátky	zpátky	k6eAd1
na	na	k7c4
zem	zem	k1gFnSc4
-	-	kIx~
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
něco	něco	k3yInSc4
v	v	k7c6
podsvětí	podsvětí	k1gNnSc6
snědl	snědnout	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgMnS
se	se	k3xPyFc4
už	už	k6eAd1
vrátit	vrátit	k5eAaPmF
nahoru	nahoru	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Démétér	Démétér	k1gFnSc1
z	z	k7c2
velkého	velký	k2eAgInSc2d1
žalu	žal	k1gInSc2
se	se	k3xPyFc4
uchýlila	uchýlit	k5eAaPmAgFnS
do	do	k7c2
ústraní	ústraň	k1gFnPc2
do	do	k7c2
svého	svůj	k3xOyFgInSc2
chrámu	chrám	k1gInSc2
v	v	k7c6
Eleusíně	Eleusína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seslala	seslat	k5eAaPmAgFnS
na	na	k7c6
zemi	zem	k1gFnSc6
neúrodu	neúroda	k1gFnSc4
<g/>
,	,	kIx,
zato	zato	k6eAd1
zase	zase	k9
lidé	člověk	k1gMnPc1
přestali	přestat	k5eAaPmAgMnP
bohům	bůh	k1gMnPc3
obětovat	obětovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
musel	muset	k5eAaImAgInS
Zeus	Zeus	k1gInSc1
zakročit	zakročit	k5eAaPmF
<g/>
:	:	kIx,
donutil	donutit	k5eAaPmAgMnS
Háda	Hádes	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
propustil	propustit	k5eAaPmAgInS
Persefonu	Persefona	k1gFnSc4
na	na	k7c4
svět	svět	k1gInSc4
vždy	vždy	k6eAd1
na	na	k7c4
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc4
roku	rok	k1gInSc2
a	a	k8xC
zbylou	zbylý	k2eAgFnSc4d1
třetinu	třetina	k1gFnSc4
bude	být	k5eAaImBp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
trávit	trávit	k5eAaImF
v	v	k7c6
podsvětní	podsvětní	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
podzim	podzim	k1gInSc4
je	být	k5eAaImIp3nS
Persefona	Persefona	k1gFnSc1
dole	dole	k6eAd1
-	-	kIx~
rolník	rolník	k1gMnSc1
osévá	osévat	k5eAaImIp3nS
pole	pole	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
Persefona	Persefona	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
matce	matka	k1gFnSc3
<g/>
,	,	kIx,
příroda	příroda	k1gFnSc1
se	se	k3xPyFc4
probouzí	probouzet	k5eAaImIp3nS
<g/>
,	,	kIx,
rozkvétá	rozkvétat	k5eAaImIp3nS
<g/>
,	,	kIx,
zraje	zrát	k5eAaImIp3nS
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
sklízejí	sklízet	k5eAaImIp3nP
úrodu	úroda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Prý	prý	k9
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
pěstovat	pěstovat	k5eAaImF
obilí	obilí	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Triptolemos	Triptolemos	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
otec	otec	k1gMnSc1
král	král	k1gMnSc1
eleusínský	eleusínský	k2eAgInSc4d1
Keleos	Keleos	k1gInSc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
poskytli	poskytnout	k5eAaPmAgMnP
přístřeší	přístřeší	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gInSc2
největšího	veliký	k2eAgInSc2d3
smutku	smutek	k1gInSc2
po	po	k7c6
dceři	dcera	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétér	Démétér	k1gFnSc1
Triptolemovi	Triptolem	k1gMnSc3
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
orat	orat	k5eAaImF
<g/>
,	,	kIx,
darovala	darovat	k5eAaPmAgFnS
mu	on	k3xPp3gNnSc3
obilná	obilný	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
a	a	k8xC
on	on	k3xPp3gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
stal	stát	k5eAaPmAgMnS
učitelem	učitel	k1gMnSc7
zemědělství	zemědělství	k1gNnSc2
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
bratru	bratru	k9
Démofoóntovi	Démofoóntův	k2eAgMnPc1d1
zase	zase	k9
přinesla	přinést	k5eAaPmAgFnS
věčnou	věčný	k2eAgFnSc4d1
slávu	sláva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
jí	jíst	k5eAaImIp3nS
král	král	k1gMnSc1
Keleos	Keleos	k1gMnSc1
postavil	postavit	k5eAaPmAgMnS
v	v	k7c6
Eleusíně	Eleusína	k1gFnSc6
velkolepý	velkolepý	k2eAgInSc4d1
chrám	chrám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
poctě	pocta	k1gFnSc3
Démétér	Démétér	k1gFnSc2
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
velké	velký	k2eAgFnPc1d1
náboženské	náboženský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
velké	velký	k2eAgFnPc1d1
mystérie	mystérie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvaly	trvalo	k1gFnPc7
devět	devět	k4xCc1
dní	den	k1gInPc2
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
září	září	k1gNnSc2
a	a	k8xC
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Deméter	Deméter	k1gInSc1
„	„	k?
<g/>
bohyně	bohyně	k1gFnSc1
světa	svět	k1gInSc2
<g/>
“	“	k?
</s>
<s>
v	v	k7c6
Římě	Řím	k1gInSc6
zvaná	zvaný	k2eAgFnSc1d1
Ceres	ceres	k1gInSc1
</s>
<s>
významy	význam	k1gInPc1
<g/>
:	:	kIx,
mateřství	mateřství	k1gNnSc1
-	-	kIx~
sklizeň	sklizeň	k1gFnSc1
-	-	kIx~
tajemství	tajemství	k1gNnSc1
</s>
<s>
symbol	symbol	k1gInSc1
<g/>
:	:	kIx,
pšeničný	pšeničný	k2eAgInSc1d1
nebo	nebo	k8xC
obilný	obilný	k2eAgInSc1d1
snop	snop	k1gInSc1
-	-	kIx~
pluh	pluh	k1gInSc1
-	-	kIx~
srp	srp	k1gInSc1
-	-	kIx~
Venušin	Venušin	k2eAgInSc1d1
pahorek	pahorek	k1gInSc1
</s>
<s>
moderní	moderní	k2eAgInSc1d1
archetyp	archetyp	k1gInSc1
<g/>
:	:	kIx,
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
svět	svět	k1gInSc1
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nS
jen	jen	k9
kolem	kolem	k7c2
jejích	její	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
Demeter	Demeter	k1gFnSc2
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
matka	matka	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
její	její	k3xOp3gInPc4
symboly	symbol	k1gInPc4
patří	patřit	k5eAaImIp3nS
převrácený	převrácený	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
znázornění	znázornění	k1gNnSc4
ženských	ženský	k2eAgFnPc2d1
genitálií	genitálie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobá	podobat	k5eAaImIp3nS
se	se	k3xPyFc4
písmenu	písmeno	k1gNnSc3
delta	delta	k1gNnSc1
řecké	řecký	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
představující	představující	k2eAgFnPc4d1
ženské	ženský	k2eAgFnPc4d1
genitálie	genitálie	k1gFnPc4
jako	jako	k8xS,k8xC
bránu	brána	k1gFnSc4
zrození	zrození	k1gNnSc2
<g/>
,	,	kIx,
smrti	smrt	k1gFnSc2
i	i	k8xC
sexuální	sexuální	k2eAgFnSc2d1
rozkoše	rozkoš	k1gFnSc2
,	,	kIx,
měly	mít	k5eAaImAgFnP
vstupní	vstupní	k2eAgFnPc1d1
dveře	dveře	k1gFnPc1
do	do	k7c2
hrobek	hrobka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
byl	být	k5eAaImAgInS
symbolicky	symbolicky	k6eAd1
znázorněn	znázorněn	k2eAgInSc1d1
návrat	návrat	k1gInSc1
do	do	k7c2
lůna	lůno	k1gNnSc2
Matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Démétér	Démétér	k1gFnSc7
souvisí	souviset	k5eAaImIp3nS
i	i	k9
zasvěcování	zasvěcování	k1gNnSc1
do	do	k7c2
eleusínských	eleusínský	k2eAgNnPc2d1
mystérií	mystérium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
rituálů	rituál	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
součástí	součást	k1gFnSc7
jejího	její	k3xOp3gInSc2
kultu	kult	k1gInSc2
<g/>
,	,	kIx,
převzalo	převzít	k5eAaPmAgNnS
pozdější	pozdní	k2eAgNnSc1d2
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
přijímání	přijímání	k1gNnSc1
chleba	chléb	k1gInSc2
(	(	kIx(
<g/>
hostie	hostie	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
symbolu	symbol	k1gInSc2
Kristova	Kristův	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
pití	pití	k1gNnSc2
vína	víno	k1gNnSc2
jako	jako	k8xS,k8xC
symbolu	symbol	k1gInSc2
krve	krev	k1gFnSc2
Spasitelovy	spasitelův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
eleusínského	eleusínský	k2eAgInSc2d1
kultu	kult	k1gInSc2
však	však	k9
zřejmě	zřejmě	k6eAd1
byly	být	k5eAaImAgInP
i	i	k9
sexuální	sexuální	k2eAgInPc1d1
rituály	rituál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Její	její	k3xOp3gInSc1
archetyp	archetyp	k1gInSc1
je	být	k5eAaImIp3nS
živý	živý	k2eAgMnSc1d1
i	i	k8xC
dnes	dnes	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
pečující	pečující	k2eAgFnSc2d1
a	a	k8xC
ochranitelské	ochranitelský	k2eAgFnSc2d1
mateřské	mateřský	k2eAgFnSc2d1
náruče	náruč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
matka	matka	k1gFnSc1
jejího	její	k3xOp3gInSc2
typu	typ	k1gInSc2
dětmi	dítě	k1gFnPc7
opuštěna	opustit	k5eAaPmNgFnS
<g/>
,	,	kIx,
cítí	cítit	k5eAaImIp3nS
se	se	k3xPyFc4
oloupena	oloupen	k2eAgFnSc1d1
a	a	k8xC
ztrácí	ztrácet	k5eAaImIp3nS
smysl	smysl	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Umělci	umělec	k1gMnPc1
zobrazovali	zobrazovat	k5eAaImAgMnP
Démétér	Démétér	k1gFnSc4
spíše	spíše	k9
s	s	k7c7
mateřskými	mateřský	k2eAgInPc7d1
než	než	k8xS
panovnickými	panovnický	k2eAgInPc7d1
rysy	rys	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
symboly	symbol	k1gInPc4
byly	být	k5eAaImAgInP
věnce	věnec	k1gInPc1
z	z	k7c2
klasů	klas	k1gInPc2
a	a	k8xC
košíky	košík	k1gInPc1
s	s	k7c7
plody	plod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
její	její	k3xOp3gFnSc7
sochou	socha	k1gFnSc7
je	být	k5eAaImIp3nS
Démétér	Démétér	k1gFnSc1
Knidská	Knidský	k2eAgFnSc1d1
<g/>
,	,	kIx,
snad	snad	k9
od	od	k7c2
Leochara	Leochar	k1gMnSc2
(	(	kIx(
<g/>
asi	asi	k9
ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
reliéf	reliéf	k1gInSc1
Triptolemos	Triptolemos	k1gInSc1
mezi	mezi	k7c7
bohyněmi	bohyně	k1gFnPc7
Démétrou	Démétér	k1gFnSc7
a	a	k8xC
Korou	Kora	k1gFnSc7
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
dílo	dílo	k1gNnSc4
Feidiovo	Feidiův	k2eAgNnSc4d1
(	(	kIx(
<g/>
z	z	k7c2
r.	r.	kA
450	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
archeologickém	archeologický	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Löwe	Löw	k1gInSc2
<g/>
,	,	kIx,
Heindrich	Heindrich	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Stoll	Stoll	k1gMnSc1
<g/>
,	,	kIx,
ABC	ABC	kA
Antiky	antika	k1gFnSc2
</s>
<s>
Publius	Publius	k1gMnSc1
Ovidius	Ovidius	k1gMnSc1
Naso	Naso	k1gMnSc1
<g/>
,	,	kIx,
Proměny	proměna	k1gFnPc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Mertlík	Mertlík	k1gInSc1
<g/>
,	,	kIx,
Starověké	starověký	k2eAgFnPc1d1
báje	báj	k1gFnPc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Bohové	bůh	k1gMnPc1
a	a	k8xC
hrdinové	hrdina	k1gMnPc1
antických	antický	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
Prvotní	prvotní	k2eAgFnSc1d1
tvořitelská	tvořitelský	k2eAgFnSc1d1
božstva	božstvo	k1gNnPc1
</s>
<s>
Achlys	Achlysa	k1gFnPc2
•	•	k?
Aión	Aióna	k1gFnPc2
/	/	kIx~
Chronos	Chronosa	k1gFnPc2
•	•	k?
Eurynomé	Eurynomý	k2eAgFnSc2d1
•	•	k?
Ofión	Ofión	k1gMnSc1
•	•	k?
Chaos	chaos	k1gInSc1
•	•	k?
Erebos	Erebos	k1gInSc1
+	+	kIx~
Nyx	Nyx	k1gFnSc1
>	>	kIx)
Aithér	Aithér	k1gInSc1
•	•	k?
Hémerá	Hémerý	k2eAgFnSc1d1
•	•	k?
Gaia	Gaia	k1gFnSc1
•	•	k?
Tartaros	Tartaros	k1gInSc1
•	•	k?
Erós	erós	k1gInSc1
/	/	kIx~
Fanes	Fanes	k1gInSc1
•	•	k?
Pýthón	Pýthón	k1gInSc1
•	•	k?
Úranos	Úranos	k1gInSc1
•	•	k?
Pontos	Pontos	k1gInSc1
•	•	k?
Týfón	Týfón	k1gInSc1
Pontos	Pontos	k1gInSc1
+	+	kIx~
Gaia	Gaia	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Néreus	Néreus	k1gInSc1
•	•	k?
Thaumás	Thaumás	k1gInSc1
•	•	k?
Forkýs	Forkýs	k1gInSc1
•	•	k?
Kétó	Kétó	k1gMnSc2
•	•	k?
Eurybia	Eurybius	k1gMnSc2
•	•	k?
Harpyje	Harpyje	k1gFnSc2
•	•	k?
Chrýsáór	Chrýsáór	k1gMnSc1
•	•	k?
Néreovny	Néreovna	k1gFnSc2
Forkidy	Forkida	k1gFnSc2
</s>
<s>
Echidna	Echidna	k1gFnSc1
(	(	kIx(
<g/>
Kerberos	Kerberos	k1gMnSc1
<g/>
,	,	kIx,
Orthos	Orthos	k1gMnSc1
<g/>
,	,	kIx,
Hydra	hydra	k1gFnSc1
<g/>
,	,	kIx,
Chiméra	chiméra	k1gFnSc1
<g/>
,	,	kIx,
Sfinx	sfinx	k1gInSc1
<g/>
,	,	kIx,
Nemejský	Nemejský	k2eAgInSc1d1
lev	lev	k1gInSc1
<g/>
,	,	kIx,
Skylla	Skylla	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ládón	Ládón	k1gInSc1
•	•	k?
Gorgony	Gorgo	k1gFnSc2
•	•	k?
Graie	Graie	k1gFnSc2
•	•	k?
Sirény	Siréna	k1gFnSc2
•	•	k?
Hesperidky	Hesperidka	k1gFnSc2
</s>
<s>
Úranos	Úranos	k1gMnSc1
+	+	kIx~
Gaia	Gaia	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Titáni	Titán	k1gMnPc1
<g/>
12	#num#	k4
</s>
<s>
Okeanos	Okeanos	k1gMnSc1
•	•	k?
Koios	Koios	k1gMnSc1
•	•	k?
Kríos	Kríos	k1gMnSc1
•	•	k?
Hyperión	Hyperión	k1gMnSc1
•	•	k?
Iapetos	Iapetos	k1gMnSc1
•	•	k?
Mnémosyné	Mnémosyná	k1gFnSc2
•	•	k?
Foibé	Foibý	k2eAgNnSc1d1
•	•	k?
Rheia	Rheius	k1gMnSc2
•	•	k?
Téthys	Téthys	k1gInSc1
•	•	k?
Theia	Theia	k1gFnSc1
•	•	k?
Themis	Themis	k1gFnSc1
•	•	k?
Kronos	Kronos	k1gMnSc1
•	•	k?
Ókeanovny	Ókeanovna	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
<g/>
,	,	kIx,
<g/>
také	také	k9
Olympané	Olympan	k1gMnPc1
</s>
<s>
Seléné	Seléné	k1gNnSc1
<g/>
,	,	kIx,
Éós	Éós	k1gMnSc1
<g/>
,	,	kIx,
Hélios	Hélios	k1gMnSc1
•	•	k?
Astraios	Astraios	k1gMnSc1
•	•	k?
Aithra	Aithra	k1gFnSc1
•	•	k?
Asteria	Asterium	k1gNnSc2
•	•	k?
Dióna	Dióno	k1gNnSc2
•	•	k?
Klymené	Klymený	k2eAgFnSc2d1
•	•	k?
Métis	Métis	k1gFnSc2
•	•	k?
Pallás	Pallás	k1gInSc1
•	•	k?
Persés	Persés	k1gInSc1
•	•	k?
Kallirhoé	Kallirhoá	k1gFnSc2
•	•	k?
Peithó	Peithó	k1gMnSc1
•	•	k?
Létó	Létó	k1gMnSc1
•	•	k?
Asteria	Asterium	k1gNnSc2
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
<g/>
,	,	kIx,
Iapetovi	Iapetův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
</s>
<s>
Prométheus	Prométheus	k1gMnSc1
•	•	k?
Epimétheus	Epimétheus	k1gMnSc1
•	•	k?
Atlás	Atlás	k1gInSc1
•	•	k?
Menoitios	Menoitios	k1gInSc1
•	•	k?
Plejády	Plejáda	k1gFnSc2
•	•	k?
Hyády	Hyáda	k1gFnSc2
</s>
<s>
Kyklópové	Kyklópové	k2eAgFnSc1d1
</s>
<s>
Brontés	Brontés	k1gInSc1
(	(	kIx(
<g/>
hrom	hrom	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Steropés	Steropés	k1gInSc1
(	(	kIx(
<g/>
blesk	blesk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Argés	Argés	k1gInSc1
(	(	kIx(
<g/>
jas	jas	k1gInSc1
<g/>
)	)	kIx)
Hekatoncheirové	Hekatoncheirové	k2eAgFnSc1d1
</s>
<s>
Briareós	Briareós	k1gInSc1
•	•	k?
Kottos	Kottos	k1gInSc1
•	•	k?
Gyés	Gyés	k1gInSc1
při	při	k7c6
kastraci	kastrace	k1gFnSc6
</s>
<s>
Afrodita	Afrodita	k1gFnSc1
•	•	k?
Erínye	Eríny	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc1
Fúrie	Fúrie	k1gFnPc1
(	(	kIx(
<g/>
Alléktó	Alléktó	k1gFnPc1
•	•	k?
Megaira	Megaira	k1gFnSc1
•	•	k?
Tísifoné	Tísifoné	k1gNnSc1
<g/>
)	)	kIx)
Giganti	gigant	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
krve	krev	k1gFnSc2
</s>
<s>
Alkyoneus	Alkyoneus	k1gMnSc1
•	•	k?
Athos	Athos	k1gMnSc1
•	•	k?
Klytios	Klytios	k1gMnSc1
•	•	k?
Enkelados	Enkelados	k1gMnSc1
•	•	k?
Echión	Echión	k1gMnSc1
>	>	kIx)
pro	pro	k7c4
pomstu	pomsta	k1gFnSc4
<g/>
:	:	kIx,
gigantomachie	gigantomachie	k1gFnSc1
</s>
<s>
Kronos	Kronos	k1gMnSc1
+	+	kIx~
Rheia	Rheia	k1gFnSc1
<g/>
:	:	kIx,
<g/>
6	#num#	k4
olympských	olympský	k2eAgFnPc2d1
bohůa	bohůa	k6eAd1
další	další	k2eAgMnPc1d1
Olympané	Olympan	k1gMnPc1
</s>
<s>
Hestiá	Hestiá	k1gFnSc1
•	•	k?
Démétér	Démétér	k1gFnSc1
•	•	k?
Héra	Héra	k1gFnSc1
•	•	k?
Hádés	Hádés	k1gInSc1
•	•	k?
Poseidón	Poseidón	k1gInSc1
•	•	k?
Zeus	Zeus	k1gInSc1
bozi	bůh	k1gMnPc1
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Hermés	Hermés	k6eAd1
•	•	k?
Apollón	Apollón	k1gMnSc1
<g/>
+	+	kIx~
<g/>
Artemis	Artemis	k1gFnSc1
•	•	k?
Athéna	Athéna	k1gFnSc1
•	•	k?
Héfaistos	Héfaistos	k1gInSc1
•	•	k?
Arés	Arés	k1gInSc1
•	•	k?
Hébé	Hébé	k1gFnSc2
•	•	k?
Eileithýia	Eileithýia	k1gFnSc1
•	•	k?
Pandia	Pandium	k1gNnSc2
<g/>
,	,	kIx,
Ersa	Ers	k2eAgFnSc1d1
•	•	k?
Persefona	Persefona	k1gFnSc1
•	•	k?
Acherón	Acherón	k1gInSc1
•	•	k?
Moiry	Moira	k1gFnSc2
•	•	k?
Hóry	Hóra	k1gFnSc2
Múzy	Múza	k1gFnSc2
</s>
<s>
Kalliopé	Kalliopé	k1gFnSc7
•	•	k?
Euterpé	Euterpý	k2eAgFnSc2d1
•	•	k?
Erató	Erató	k1gFnSc2
•	•	k?
Thaleia	Thaleia	k1gFnSc1
•	•	k?
Melpomené	Melpomený	k2eAgNnSc4d1
•	•	k?
Terpsichoré	Terpsichorý	k2eAgFnSc2d1
•	•	k?
Kleió	Kleió	k1gMnSc1
•	•	k?
Úrania	Úranium	k1gNnSc2
•	•	k?
Polyhymnia	Polyhymnia	k1gFnSc1
</s>
<s>
bozi	bůh	k1gMnPc1
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Anteros	Anterosa	k1gFnPc2
•	•	k?
Erós	Erós	k1gMnSc1
•	•	k?
Harmonia	harmonium	k1gNnSc2
•	•	k?
Himeros	Himerosa	k1gFnPc2
•	•	k?
Eunomia	Eunomius	k1gMnSc2
•	•	k?
Hermaphroditus	Hermaphroditus	k1gInSc1
•	•	k?
Peitho	Peit	k1gMnSc2
•	•	k?
Rhodos	Rhodos	k1gInSc1
•	•	k?
Tyché	Tychý	k2eAgFnPc1d1
polobozi	polobůh	k1gMnPc1
na	na	k7c6
Olympu	Olymp	k1gInSc6
</s>
<s>
Dionýsos	Dionýsos	k1gMnSc1
•	•	k?
Héraklés	Héraklés	k1gInSc1
</s>
<s>
rasy	rasa	k1gFnPc1
a	a	k8xC
bytosti	bytost	k1gFnPc1
</s>
<s>
Nymfy	nymfa	k1gFnPc1
•	•	k?
Pan	Pan	k1gMnSc1
•	•	k?
Satyr	satyr	k1gMnSc1
•	•	k?
Silénos	Silénos	k1gMnSc1
•	•	k?
Argos	Argos	k1gMnSc1
•	•	k?
Kentaur	kentaur	k1gMnSc1
•	•	k?
Mínotaurus	Mínotaurus	k1gMnSc1
•	•	k?
Charybda	Charybda	k1gFnSc1
•	•	k?
Fénix	fénix	k1gMnSc1
polobozi	polobůh	k1gMnPc1
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
:	:	kIx,
hrdinové	hrdina	k1gMnPc1
a	a	k8xC
oběti	oběť	k1gFnPc1
</s>
<s>
Héraklés	Héraklés	k6eAd1
•	•	k?
Théseus	Théseus	k1gMnSc1
•	•	k?
Kallistó	Kallistó	k1gMnSc1
•	•	k?
Kirké	Kirká	k1gFnSc2
•	•	k?
Antaios	Antaios	k1gMnSc1
•	•	k?
Glaukos	Glaukos	k1gMnSc1
•	•	k?
Iásón	Iásón	k1gMnSc1
•	•	k?
Perseus	Perseus	k1gMnSc1
události	událost	k1gFnSc2
a	a	k8xC
příběhy	příběh	k1gInPc4
</s>
<s>
Amazonomachie	Amazonomachie	k1gFnSc1
•	•	k?
Kalydónský	Kalydónský	k2eAgInSc4d1
lov	lov	k1gInSc4
•	•	k?
Trójská	trójský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Odysseia	Odysseia	k1gFnSc1
•	•	k?
Sedm	sedm	k4xCc1
proti	proti	k7c3
Thébám	Théby	k1gFnPc3
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118671421	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014024166	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
306391409	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014024166	#num#	k4
</s>
