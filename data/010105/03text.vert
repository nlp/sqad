<p>
<s>
Zámek	zámek	k1gInSc4	zámek
Habry	habr	k1gInPc7	habr
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
Žižkově	Žižkův	k2eAgNnSc6d1	Žižkovo
náměstí	náměstí	k1gNnSc6	náměstí
čp.	čp.	k?	čp.
1	[number]	k4	1
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Habry	habr	k1gInPc4	habr
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
kostelu	kostel	k1gInSc3	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
muzeum	muzeum	k1gNnSc1	muzeum
Habry	habr	k1gInPc1	habr
<g/>
,	,	kIx,	,
zřizovatelem	zřizovatel	k1gMnSc7	zřizovatel
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Rudolf	Rudolf	k1gMnSc1	Rudolf
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Habry	habr	k1gInPc1	habr
jsou	být	k5eAaImIp3nP	být
poprvé	poprvé	k6eAd1	poprvé
zmiňovány	zmiňovat	k5eAaImNgInP	zmiňovat
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1101	[number]	k4	1101
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zámek	zámek	k1gInSc1	zámek
nechal	nechat	k5eAaPmAgInS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1717-1720	[number]	k4	1717-1720
Adolf	Adolf	k1gMnSc1	Adolf
Felix	Felix	k1gMnSc1	Felix
Pötting-Persing	Pötting-Persing	k1gInSc4	Pötting-Persing
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
rodu	rod	k1gInSc2	rod
patřilo	patřit	k5eAaImAgNnS	patřit
panství	panství	k1gNnSc1	panství
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
správy	správa	k1gFnSc2	správa
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jej	on	k3xPp3gMnSc4	on
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opravách	oprava	k1gFnPc6	oprava
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
dvoupatrový	dvoupatrový	k2eAgInSc1d1	dvoupatrový
objekt	objekt	k1gInSc1	objekt
o	o	k7c4	o
patro	patro	k1gNnSc4	patro
snížen	snížen	k2eAgMnSc1d1	snížen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
následné	následný	k2eAgFnSc6d1	následná
novobarokní	novobarokní	k2eAgFnSc6d1	novobarokní
přestavbě	přestavba	k1gFnSc6	přestavba
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zámek	zámek	k1gInSc1	zámek
od	od	k7c2	od
města	město	k1gNnSc2	město
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
Petr	Petr	k1gMnSc1	Petr
Rudolf	Rudolf	k1gMnSc1	Rudolf
Vančura	Vančura	k1gMnSc1	Vančura
a	a	k8xC	a
po	po	k7c6	po
prvotních	prvotní	k2eAgFnPc6d1	prvotní
opravách	oprava	k1gFnPc6	oprava
zde	zde	k6eAd1	zde
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
otevřel	otevřít	k5eAaPmAgInS	otevřít
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
opravených	opravený	k2eAgFnPc6d1	opravená
místnostech	místnost	k1gFnPc6	místnost
zámku	zámek	k1gInSc2	zámek
pořádají	pořádat	k5eAaImIp3nP	pořádat
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Expozice	expozice	k1gFnSc1	expozice
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
prohlídkové	prohlídkový	k2eAgFnSc2d1	prohlídková
trasy	trasa	k1gFnSc2	trasa
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
místností	místnost	k1gFnPc2	místnost
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lovecký	lovecký	k2eAgInSc1d1	lovecký
salón	salón	k1gInSc1	salón
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
ložnice	ložnice	k1gFnSc1	ložnice
a	a	k8xC	a
sklepení	sklepení	k1gNnSc1	sklepení
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
původní	původní	k2eAgFnPc1d1	původní
věžní	věžní	k2eAgFnPc1d1	věžní
hodiny	hodina	k1gFnPc1	hodina
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
na	na	k7c4	na
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
na	na	k7c6	na
turistika	turistika	k1gFnSc1	turistika
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
letní	letní	k2eAgFnSc2d1	letní
sezóny	sezóna	k1gFnSc2	sezóna
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Habry	habr	k1gInPc1	habr
<g/>
,	,	kIx,	,
tepregionu	tepregion	k1gInSc2	tepregion
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
muzeu	muzeum	k1gNnSc6	muzeum
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
