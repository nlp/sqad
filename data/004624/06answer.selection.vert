<s>
Vštěpování	vštěpování	k1gNnSc1	vštěpování
informace	informace	k1gFnSc2	informace
do	do	k7c2	do
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
buďto	buďto	k8xC	buďto
záměrně	záměrně	k6eAd1	záměrně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
opakováním	opakování	k1gNnSc7	opakování
–	–	k?	–
memorováním	memorování	k1gNnSc7	memorování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bezděčně	bezděčně	k6eAd1	bezděčně
<g/>
.	.	kIx.	.
</s>
