<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Lainsitz	Lainsitz	k1gMnSc1	Lainsitz
nebo	nebo	k8xC	nebo
Luschnitz	Luschnitz	k1gMnSc1	Luschnitz
<g/>
,	,	kIx,	,
Lužnitz	Lužnitz	k1gMnSc1	Lužnitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
s	s	k7c7	s
horním	horní	k2eAgInSc7d1	horní
tokem	tok	k1gInSc7	tok
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravobřežní	pravobřežní	k2eAgInSc1d1	pravobřežní
přítok	přítok	k1gInSc1	přítok
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
208	[number]	k4	208
km	km	kA	km
a	a	k8xC	a
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
území	území	k1gNnSc4	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
4234,65	[number]	k4	4234,65
km2	km2	k4	km2
<g/>
;	;	kIx,	;
číslo	číslo	k1gNnSc1	číslo
hydrologického	hydrologický	k2eAgNnSc2d1	hydrologické
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
:	:	kIx,	:
Lužnice	Lužnice	k1gFnSc1	Lužnice
1-07-01-001	[number]	k4	1-07-01-001
1	[number]	k4	1
<g/>
;	;	kIx,	;
průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
činí	činit	k5eAaImIp3nS	činit
24,3	[number]	k4	24,3
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
jihočeským	jihočeský	k2eAgFnPc3d1	Jihočeská
řekám	řeka	k1gFnPc3	řeka
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
spád	spád	k1gInSc4	spád
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
projevuje	projevovat	k5eAaImIp3nS	projevovat
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
také	také	k9	také
reguluje	regulovat	k5eAaImIp3nS	regulovat
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnPc1	voda
mnoha	mnoho	k4c2	mnoho
rybníků	rybník	k1gInPc2	rybník
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
povodí	povodí	k1gNnSc6	povodí
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Rožmberku	Rožmberk	k1gInSc3	Rožmberk
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Jakuba	Jakub	k1gMnSc2	Jakub
Krčína	Krčín	k1gMnSc2	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
nebo	nebo	k8xC	nebo
napájecím	napájecí	k2eAgInSc7d1	napájecí
kanálem	kanál	k1gInSc7	kanál
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stoka	stoka	k1gFnSc1	stoka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
provozována	provozován	k2eAgFnSc1d1	provozována
voroplavba	voroplavba	k1gFnSc1	voroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
panovník	panovník	k1gMnSc1	panovník
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stanovil	stanovit	k5eAaPmAgInS	stanovit
předpisy	předpis	k1gInPc4	předpis
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
splavnění	splavnění	k1gNnSc4	splavnění
Lužnice	Lužnice	k1gFnSc2	Lužnice
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
to	ten	k3xDgNnSc1	ten
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
3.8	[number]	k4	3.8
<g/>
.1366	.1366	k4	.1366
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
protéká	protékat	k5eAaImIp3nS	protékat
Novohradskými	novohradský	k2eAgFnPc7d1	Novohradská
horami	hora	k1gFnPc7	hora
<g/>
,	,	kIx,	,
Vitorazskem	Vitorazsko	k1gNnSc7	Vitorazsko
<g/>
,	,	kIx,	,
Třeboňskou	třeboňský	k2eAgFnSc7d1	Třeboňská
pánví	pánev	k1gFnSc7	pánev
a	a	k8xC	a
Středočeskou	středočeský	k2eAgFnSc7d1	Středočeská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
Lužnice	Lužnice	k1gFnSc2	Lužnice
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
až	až	k9	až
k	k	k7c3	k
Veselí	veselí	k1gNnSc3	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc1	součást
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
přírodně	přírodně	k6eAd1	přírodně
nejcennějších	cenný	k2eAgNnPc2d3	nejcennější
území	území	k1gNnPc2	území
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
patří	patřit	k5eAaImIp3nS	patřit
u	u	k7c2	u
vodáků	vodák	k1gMnPc2	vodák
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
českým	český	k2eAgFnPc3d1	Česká
řekám	řeka	k1gFnPc3	řeka
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgFnSc4d1	rekreační
kanoistiku	kanoistika	k1gFnSc4	kanoistika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
"	"	kIx"	"
<g/>
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
vody	voda	k1gFnPc1	voda
<g/>
"	"	kIx"	"
od	od	k7c2	od
Tábora	Tábor	k1gInSc2	Tábor
k	k	k7c3	k
Bechyni	Bechyně	k1gFnSc3	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
straně	strana	k1gFnSc6	strana
Novohradských	novohradský	k2eAgFnPc2d1	Novohradská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
svahu	svah	k1gInSc6	svah
hory	hora	k1gFnSc2	hora
Eichelberg	Eichelberg	k1gMnSc1	Eichelberg
(	(	kIx(	(
<g/>
1054	[number]	k4	1054
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
970	[number]	k4	970
m.	m.	k?	m.
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Karlstift	Karlstifta	k1gFnPc2	Karlstifta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
2	[number]	k4	2
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
značená	značený	k2eAgFnSc1d1	značená
orientačními	orientační	k2eAgFnPc7d1	orientační
tabulemi	tabule	k1gFnPc7	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
východně	východně	k6eAd1	východně
od	od	k7c2	od
Pohoří	pohoří	k1gNnSc2	pohoří
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protéká	protékat	k5eAaImIp3nS	protékat
splavovací	splavovací	k2eAgFnSc7d1	splavovací
nádrží	nádrž	k1gFnSc7	nádrž
Kapelunk	Kapelunka	k1gFnPc2	Kapelunka
a	a	k8xC	a
po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
odstupu	odstup	k1gInSc6	odstup
sledovala	sledovat	k5eAaImAgFnS	sledovat
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
Popelnicí	popelnice	k1gFnSc7	popelnice
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
Weitra	Weitrum	k1gNnSc2	Weitrum
(	(	kIx(	(
<g/>
Vitoraz	Vitoraz	k1gFnSc1	Vitoraz
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Gmündem	Gmünd	k1gInSc7	Gmünd
(	(	kIx(	(
<g/>
Cmuntem	Cmunt	k1gInSc7	Cmunt
<g/>
)	)	kIx)	)
a	a	k8xC	a
Českými	český	k2eAgFnPc7d1	Česká
Velenicemi	Velenice	k1gFnPc7	Velenice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Gmündem	Gmünd	k1gInSc7	Gmünd
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c6	na
území	území	k1gNnSc6	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
několika	několik	k4yIc6	několik
kilometrech	kilometr	k1gInPc6	kilometr
se	se	k3xPyFc4	se
za	za	k7c2	za
obcí	obec	k1gFnPc2	obec
Breitensee	Breitense	k1gFnSc2	Breitense
obrací	obracet	k5eAaImIp3nS	obracet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
široké	široký	k2eAgFnSc2d1	široká
Třeboňské	třeboňský	k2eAgFnSc2d1	Třeboňská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Weitra	Weitra	k1gFnSc1	Weitra
(	(	kIx(	(
<g/>
Vitoraz	Vitoraz	k1gFnSc1	Vitoraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gmünd	Gmünd	k1gInSc1	Gmünd
<g/>
,	,	kIx,	,
České	český	k2eAgFnPc1d1	Česká
Velenice	Velenice	k1gFnPc1	Velenice
<g/>
,	,	kIx,	,
Suchdol	Suchdol	k1gInSc1	Suchdol
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Soběslav	Soběslav	k1gFnSc1	Soběslav
<g/>
,	,	kIx,	,
Planá	Planá	k1gFnSc1	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Bechyně	Bechyně	k1gFnSc1	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
významnějším	významný	k2eAgInSc7d2	významnější
přítokem	přítok	k1gInSc7	přítok
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
říčka	říčka	k1gFnSc1	říčka
Skřemelice	Skřemelice	k1gFnSc1	Skřemelice
(	(	kIx(	(
<g/>
Braunaubach	Braunaubach	k1gInSc1	Braunaubach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Lužnice	Lužnice	k1gFnSc2	Lužnice
v	v	k7c6	v
Gmündu	Gmünd	k1gInSc6	Gmünd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
nedaleko	nedaleko	k7c2	nedaleko
Klikova	Klikův	k2eAgNnSc2d1	Klikův
do	do	k7c2	do
Lužnice	Lužnice	k1gFnSc2	Lužnice
pravobřežně	pravobřežně	k6eAd1	pravobřežně
ústí	ústit	k5eAaImIp3nS	ústit
říčka	říčka	k1gFnSc1	říčka
Dračice	dračice	k1gFnSc1	dračice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jezem	jez	k1gInSc7	jez
Pilař	Pilař	k1gMnSc1	Pilař
z	z	k7c2	z
Lužnice	Lužnice	k1gFnSc2	Lužnice
doleva	doleva	k6eAd1	doleva
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
k	k	k7c3	k
Třeboni	Třeboň	k1gFnSc3	Třeboň
umělý	umělý	k2eAgInSc4d1	umělý
kanál	kanál	k1gInSc4	kanál
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stoka	stoka	k1gFnSc1	stoka
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
pod	pod	k7c7	pod
jezem	jez	k1gInSc7	jez
se	se	k3xPyFc4	se
pravobřežně	pravobřežně	k6eAd1	pravobřežně
vlévá	vlévat	k5eAaImIp3nS	vlévat
Koštěnický	Koštěnický	k2eAgInSc4d1	Koštěnický
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
za	za	k7c4	za
Majdalenou	Majdalený	k2eAgFnSc4d1	Majdalený
se	se	k3xPyFc4	se
koryto	koryto	k1gNnSc4	koryto
Lužnice	Lužnice	k1gFnSc2	Lužnice
dělí	dělit	k5eAaImIp3nS	dělit
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Novořecké	novořecký	k2eAgFnSc2d1	novořecká
bašty	bašta	k1gFnSc2	bašta
<g/>
)	)	kIx)	)
vlevo	vlevo	k6eAd1	vlevo
na	na	k7c4	na
Starou	starý	k2eAgFnSc4d1	stará
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
řeka	řeka	k1gFnSc1	řeka
?	?	kIx.	?
</s>
<s>
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
původní-hlavní	původnílavní	k2eAgInSc1d1	původní-hlavní
tok	tok	k1gInSc1	tok
Lužnice	Lužnice	k1gFnSc1	Lužnice
<g/>
,	,	kIx,	,
meandrující	meandrující	k2eAgFnSc1d1	meandrující
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
převážně	převážně	k6eAd1	převážně
zarostlé	zarostlý	k2eAgInPc1d1	zarostlý
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
průjezdné	průjezdný	k2eAgNnSc1d1	průjezdné
řečiště	řečiště	k1gNnPc1	řečiště
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
napájí	napájet	k5eAaImIp3nP	napájet
největší	veliký	k2eAgInSc4d3	veliký
rybník	rybník	k1gInSc4	rybník
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
k	k	k7c3	k
Veselí	veselí	k1gNnSc3	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
obcí	obec	k1gFnSc7	obec
Frahelž	Frahelž	k1gFnSc1	Frahelž
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Lužnice	Lužnice	k1gFnSc2	Lužnice
Miletínský-Tisý	Miletínský-Tisý	k2eAgInSc4d1	Miletínský-Tisý
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
zejména	zejména	k9	zejména
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
rybníků	rybník	k1gInPc2	rybník
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgMnSc1d1	malý
Tisý	Tisý	k1gMnSc1	Tisý
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
umělý	umělý	k2eAgInSc4d1	umělý
13,5	[number]	k4	13,5
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
kanál-tok	kanálok	k1gInSc4	kanál-tok
s	s	k7c7	s
přímými	přímý	k2eAgInPc7d1	přímý
úseky	úsek	k1gInPc7	úsek
(	(	kIx(	(
<g/>
zbudovaný	zbudovaný	k2eAgInSc4d1	zbudovaný
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Jakubem	Jakub	k1gMnSc7	Jakub
Krčínem	Krčín	k1gMnSc7	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
odvádění	odvádění	k1gNnSc3	odvádění
povodňové	povodňový	k2eAgFnSc2d1	povodňová
vody	voda	k1gFnSc2	voda
mimo	mimo	k7c4	mimo
Rožmberský	rožmberský	k2eAgInSc4d1	rožmberský
rybník	rybník	k1gInSc4	rybník
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Nežárky	Nežárka	k1gFnSc2	Nežárka
a	a	k8xC	a
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
případného	případný	k2eAgNnSc2d1	případné
protržení	protržení	k1gNnSc2	protržení
jeho	jeho	k3xOp3gFnSc2	jeho
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Veselím	veselí	k1gNnSc7	veselí
se	se	k3xPyFc4	se
do	do	k7c2	do
Lužnice	Lužnice	k1gFnSc2	Lužnice
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
vrací	vracet	k5eAaImIp3nS	vracet
voda	voda	k1gFnSc1	voda
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
405	[number]	k4	405
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pravobřežně	pravobřežně	k6eAd1	pravobřežně
vlévá	vlévat	k5eAaImIp3nS	vlévat
Nežárka	Nežárka	k1gFnSc1	Nežárka
vracející	vracející	k2eAgFnSc2d1	vracející
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
Nové	Nové	k2eAgFnSc2d1	Nové
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
vlévá	vlévat	k5eAaImIp3nS	vlévat
Bechyňský	bechyňský	k2eAgInSc1d1	bechyňský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
vody	voda	k1gFnPc4	voda
zejména	zejména	k9	zejména
ze	z	k7c2	z
známých	známý	k2eAgNnPc2d1	známé
Soběslavsko-veselských	soběslavskoeselský	k2eAgNnPc2d1	soběslavsko-veselský
blat	blata	k1gNnPc2	blata
<g/>
,	,	kIx,	,
např.	např.	kA	např.
od	od	k7c2	od
obcí	obec	k1gFnPc2	obec
<g/>
:	:	kIx,	:
Vyhnanice	Vyhnanice	k1gFnPc1	Vyhnanice
<g/>
,	,	kIx,	,
Hlavatce	hlavatec	k1gInPc1	hlavatec
<g/>
,	,	kIx,	,
Vlastiboř	Vlastiboř	k1gFnSc1	Vlastiboř
nebo	nebo	k8xC	nebo
také	také	k9	také
od	od	k7c2	od
Komárova	komárův	k2eAgNnSc2d1	komárův
a	a	k8xC	a
Borkovic	Borkovice	k1gFnPc2	Borkovice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
četných	četný	k2eAgInPc2d1	četný
menších	malý	k2eAgInPc2d2	menší
i	i	k8xC	i
malých	malý	k2eAgInPc2d1	malý
přítoků	přítok	k1gInPc2	přítok
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Táboru	Tábor	k1gInSc3	Tábor
přijímá	přijímat	k5eAaImIp3nS	přijímat
Lužnice	Lužnice	k1gFnSc1	Lužnice
např.	např.	kA	např.
před	před	k7c7	před
Soběslaví	Soběslav	k1gFnSc7	Soběslav
Dírenský	Dírenský	k2eAgInSc1d1	Dírenský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
od	od	k7c2	od
rybníku	rybník	k1gInSc6	rybník
Nadýmač	nadýmač	k1gMnSc1	nadýmač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
pak	pak	k6eAd1	pak
Černovický	Černovický	k2eAgInSc4d1	Černovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Křemešnické	Křemešnický	k2eAgFnSc6d1	Křemešnická
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
rovinatou	rovinatý	k2eAgFnSc7d1	rovinatá
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
napájí	napájet	k5eAaImIp3nP	napájet
řadu	řada	k1gFnSc4	řada
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Zleva	zleva	k6eAd1	zleva
(	(	kIx(	(
<g/>
392	[number]	k4	392
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
u	u	k7c2	u
Plané	Planá	k1gFnSc2	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
přitéká	přitékat	k5eAaImIp3nS	přitékat
Maršovský	Maršovský	k2eAgInSc4d1	Maršovský
potok	potok	k1gInSc4	potok
(	(	kIx(	(
<g/>
od	od	k7c2	od
obcí	obec	k1gFnPc2	obec
Ústrašice	Ústrašice	k1gFnSc2	Ústrašice
<g/>
,	,	kIx,	,
Obora	obora	k1gFnSc1	obora
<g/>
,	,	kIx,	,
Maršov	Maršov	k1gInSc1	Maršov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
městem	město	k1gNnSc7	město
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
přitéká	přitékat	k5eAaImIp3nS	přitékat
u	u	k7c2	u
jezu	jez	k1gInSc2	jez
Soukeník	soukeník	k1gMnSc1	soukeník
zleva	zleva	k6eAd1	zleva
Radimovický	Radimovický	k2eAgInSc4d1	Radimovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
potom	potom	k6eAd1	potom
zprava	zprava	k6eAd1	zprava
známý	známý	k2eAgInSc1d1	známý
Kozský	Kozský	k2eAgInSc1d1	Kozský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
vodu	voda	k1gFnSc4	voda
několika	několik	k4yIc2	několik
potoků	potok	k1gInPc2	potok
ze	z	k7c2	z
směru	směr	k1gInSc2	směr
od	od	k7c2	od
Turovce	Turovec	k1gInSc2	Turovec
a	a	k8xC	a
Kozího	kozí	k2eAgInSc2d1	kozí
Hrádku	Hrádok	k1gInSc2	Hrádok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Tismenický	Tismenický	k2eAgInSc1d1	Tismenický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
soutok	soutok	k1gInSc1	soutok
384	[number]	k4	384
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
přehrazený	přehrazený	k2eAgInSc1d1	přehrazený
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nádrž	nádrž	k1gFnSc1	nádrž
Jordán	Jordán	k1gInSc1	Jordán
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
50	[number]	k4	50
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
7	[number]	k4	7
km	km	kA	km
západně	západně	k6eAd1	západně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vlévá	vlévat	k5eAaImIp3nS	vlévat
<g/>
,	,	kIx,	,
také	také	k9	také
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
Vlásenický	Vlásenický	k2eAgInSc4d1	Vlásenický
potok	potok	k1gInSc4	potok
a	a	k8xC	a
pak	pak	k6eAd1	pak
nedaleko	daleko	k6eNd1	daleko
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
zprava	zprava	k6eAd1	zprava
se	se	k3xPyFc4	se
u	u	k7c2	u
Bredova	Bredův	k2eAgInSc2d1	Bredův
mlýna	mlýn	k1gInSc2	mlýn
vlévá	vlévat	k5eAaImIp3nS	vlévat
Pilský	Pilský	k2eAgInSc1d1	Pilský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
přitéká	přitékat	k5eAaImIp3nS	přitékat
od	od	k7c2	od
obcí	obec	k1gFnPc2	obec
Padařov	Padařov	k1gInSc1	Padařov
a	a	k8xC	a
Drhovice	Drhovice	k1gFnSc1	Drhovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Dobronicemi	Dobronice	k1gFnPc7	Dobronice
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
"	"	kIx"	"
<g/>
Papírnou	papírný	k2eAgFnSc4d1	papírný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zprava	zprava	k6eAd1	zprava
vlévá	vlévat	k5eAaImIp3nS	vlévat
Oltyňský	Oltyňský	k2eAgInSc4d1	Oltyňský
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
pak	pak	k6eAd1	pak
zleva	zleva	k6eAd1	zleva
Všechlapský	Všechlapský	k2eAgInSc4d1	Všechlapský
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bechyni	Bechyně	k1gFnSc6	Bechyně
se	se	k3xPyFc4	se
také	také	k9	také
zprava	zprava	k6eAd1	zprava
vlévá	vlévat	k5eAaImIp3nS	vlévat
říčka	říčka	k1gFnSc1	říčka
Smutná	Smutná	k1gFnSc1	Smutná
<g/>
,	,	kIx,	,
za	za	k7c7	za
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
připojuje	připojovat	k5eAaImIp3nS	připojovat
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
kaňonem	kaňon	k1gInSc7	kaňon
protékající	protékající	k2eAgInSc4d1	protékající
potok	potok	k1gInSc4	potok
Židova	Židův	k2eAgFnSc1d1	Židův
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
pramenící	pramenící	k2eAgFnSc1d1	pramenící
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Bzí	bzít	k5eAaImIp3nS	bzít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bechyně	Bechyně	k1gFnSc2	Bechyně
teče	téct	k5eAaImIp3nS	téct
nahnědlá	nahnědlý	k2eAgFnSc1d1	nahnědlá
voda	voda	k1gFnSc1	voda
Lužnice	Lužnice	k1gFnSc1	Lužnice
mezi	mezi	k7c7	mezi
stráněmi	stráň	k1gFnPc7	stráň
dál	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Vltavě	Vltava	k1gFnSc3	Vltava
<g/>
,	,	kIx,	,
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Rosín	Rosín	k1gInSc1	Rosín
zprava	zprava	k6eAd1	zprava
přitéká	přitékat	k5eAaImIp3nS	přitékat
Koloměřický	Koloměřický	k2eAgInSc4d1	Koloměřický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
obcí	obec	k1gFnSc7	obec
Koloděje	koloděj	k1gMnSc2	koloděj
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
a	a	k8xC	a
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
míří	mířit	k5eAaImIp3nS	mířit
obloukem	oblouk	k1gInSc7	oblouk
k	k	k7c3	k
Neznašovu	Neznašův	k2eAgInSc3d1	Neznašův
nedaleko	nedaleko	k7c2	nedaleko
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Koryta	koryto	k1gNnPc1	koryto
obou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
tady	tady	k6eAd1	tady
zásadně	zásadně	k6eAd1	zásadně
změnila	změnit	k5eAaPmAgFnS	změnit
jak	jak	k6eAd1	jak
v	v	k7c6	v
šedesátých	šedesátý	k4xOgMnPc6	šedesátý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
po	po	k7c6	po
napuštění	napuštění	k1gNnSc6	napuštění
přehradní	přehradní	k2eAgFnSc2d1	přehradní
nádrže	nádrž	k1gFnSc2	nádrž
Orlík	Orlík	k1gInSc1	Orlík
a	a	k8xC	a
vybudování	vybudování	k1gNnSc1	vybudování
přehradního	přehradní	k2eAgInSc2d1	přehradní
stupně	stupeň	k1gInSc2	stupeň
Kořensko	Kořensko	k1gNnSc1	Kořensko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
voda	voda	k1gFnSc1	voda
Lužnice	Lužnice	k1gFnSc1	Lužnice
(	(	kIx(	(
<g/>
v	v	k7c6	v
354	[number]	k4	354
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
zvolna	zvolna	k6eAd1	zvolna
mizí	mizet	k5eAaImIp3nS	mizet
ve	v	k7c6	v
vzduté	vzdutý	k2eAgFnSc6d1	vzdutá
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
(	(	kIx(	(
<g/>
soutoku	soutok	k1gInSc6	soutok
<g/>
)	)	kIx)	)
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
zleva	zleva	k6eAd1	zleva
-	-	kIx~	-
Tušť	Tušť	k1gFnSc1	Tušť
<g/>
,	,	kIx,	,
Suchdolský	Suchdolský	k2eAgInSc1d1	Suchdolský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Odlehčovač	Odlehčovač	k1gInSc1	Odlehčovač
<g/>
,	,	kIx,	,
Miletínský	Miletínský	k2eAgInSc1d1	Miletínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Ponědražský	Ponědražský	k2eAgInSc1d1	Ponědražský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bechyňský	bechyňský	k2eAgInSc1d1	bechyňský
potok	potok	k1gInSc1	potok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Mokerský	Mokerský	k2eAgInSc1d1	Mokerský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Nedvědický	Nedvědický	k2eAgInSc1d1	Nedvědický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Radimovský	Radimovský	k2eAgInSc1d1	Radimovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Želečský	Želečský	k2eAgInSc1d1	Želečský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Maršovský	Maršovský	k2eAgInSc1d1	Maršovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Borecký	borecký	k2eAgInSc1d1	borecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Radimovický	Radimovický	k2eAgInSc1d1	Radimovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Větrovský	větrovský	k2eAgInSc1d1	větrovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Slapský	slapský	k2eAgInSc1d1	slapský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Příběnický	Příběnický	k2eAgInSc1d1	Příběnický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Malšický	Malšický	k2eAgInSc1d1	Malšický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Vnučský	Vnučský	k2eAgInSc1d1	Vnučský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Dobřejický	Dobřejický	k2eAgInSc1d1	Dobřejický
<g />
.	.	kIx.	.
</s>
<s>
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Třebelický	Třebelický	k2eAgInSc1d1	Třebelický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Všechlapský	Všechlapský	k2eAgInSc1d1	Všechlapský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Černýšovický	Černýšovický	k2eAgInSc1d1	Černýšovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Sudoměřický	sudoměřický	k2eAgInSc1d1	sudoměřický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bežerovický	Bežerovický	k2eAgInSc1d1	Bežerovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kamenodvorský	Kamenodvorský	k2eAgInSc1d1	Kamenodvorský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Židova	Židův	k2eAgFnSc1d1	Židův
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Nuzický	Nuzický	k2eAgInSc1d1	Nuzický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
-	-	kIx~	-
Skřemelice	Skřemelice	k1gFnSc1	Skřemelice
(	(	kIx(	(
<g/>
Braunaubach	Braunaubach	k1gInSc1	Braunaubach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Halámecký	Halámecký	k2eAgInSc1d1	Halámecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Gamza	Gamza	k1gFnSc1	Gamza
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Dračice	dračice	k1gFnSc1	dračice
<g/>
,	,	kIx,	,
Žabinec	žabinec	k1gInSc1	žabinec
<g/>
,	,	kIx,	,
Koštěnický	Koštěnický	k2eAgInSc1d1	Koštěnický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Potěšilka	Potěšilka	k1gFnSc1	Potěšilka
<g/>
,	,	kIx,	,
Nežárka	Nežárka	k1gFnSc1	Nežárka
<g/>
,	,	kIx,	,
Doňovský	Doňovský	k2eAgInSc1d1	Doňovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Dírenský	Dírenský	k2eAgInSc1d1	Dírenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Andělská	andělský	k2eAgFnSc1d1	Andělská
stoka	stoka	k1gFnSc1	stoka
<g/>
,	,	kIx,	,
Černovický	Černovický	k2eAgInSc1d1	Černovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Habří	habří	k1gNnSc1	habří
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kozský	Kozský	k2eAgInSc1d1	Kozský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Košínský	Košínský	k2eAgInSc1d1	Košínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Raštský	Raštský	k2eAgInSc1d1	Raštský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Vlásenický	Vlásenický	k2eAgInSc1d1	Vlásenický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Pilský	Pilský	k2eAgInSc1d1	Pilský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kašovický	Kašovický	k2eAgInSc1d1	Kašovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Slavňovický	Slavňovický	k2eAgInSc1d1	Slavňovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Oltyňský	Oltyňský	k2eAgInSc1d1	Oltyňský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Dobronický	Dobronický	k2eAgInSc1d1	Dobronický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Smutná	Smutná	k1gFnSc1	Smutná
<g/>
,	,	kIx,	,
Hvožďanský	Hvožďanský	k2eAgInSc4d1	Hvožďanský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
Bilinský	Bilinský	k2eAgInSc4d1	Bilinský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
Hostecký	Hostecký	k2eAgInSc4d1	Hostecký
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
Sobní	Sobní	k2eAgInSc4d1	Sobní
potok	potok	k1gInSc4	potok
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
jez	jez	k1gInSc4	jez
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Gmünd	Gmünda	k1gFnPc2	Gmünda
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
mohutný	mohutný	k2eAgInSc1d1	mohutný
jez	jez	k1gInSc1	jez
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
přírodním	přírodní	k2eAgInSc6d1	přírodní
skalním	skalní	k2eAgInSc6d1	skalní
prahu	práh	k1gInSc6	práh
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc4d1	poslední
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
jez	jez	k1gInSc4	jez
Pilař	Pilař	k1gMnSc1	Pilař
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Majdalena	Majdalen	k2eAgFnSc1d1	Majdalena
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
nejnebezpečnější	bezpečný	k2eNgInSc1d3	nejnebezpečnější
jez	jez	k1gInSc1	jez
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nejvíce	hodně	k6eAd3	hodně
obětí	oběť	k1gFnSc7	oběť
jez	jez	k1gInSc1	jez
Dráchov	Dráchov	k1gInSc1	Dráchov
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dráchov	Dráchovo	k1gNnPc2	Dráchovo
(	(	kIx(	(
<g/>
před	před	k7c7	před
Soběslaví	Soběslav	k1gFnSc7	Soběslav
<g/>
)	)	kIx)	)
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
jez	jez	k1gInSc1	jez
jez	jez	k1gInSc1	jez
Čejnov	Čejnov	k1gInSc4	Čejnov
<g/>
,	,	kIx,	,
u	u	k7c2	u
chatové	chatový	k2eAgFnSc2d1	chatová
osady	osada	k1gFnSc2	osada
<g />
.	.	kIx.	.
</s>
<s>
mezi	mezi	k7c7	mezi
Dráchovem	Dráchov	k1gInSc7	Dráchov
a	a	k8xC	a
Soběslaví	Soběslav	k1gFnSc7	Soběslav
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
také	také	k9	také
maximální	maximální	k2eAgFnSc1d1	maximální
opatrnost	opatrnost	k1gFnSc1	opatrnost
jez	jez	k1gInSc1	jez
Steiniger	Steiniger	k1gInSc1	Steiniger
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Soběslavi	Soběslav	k1gFnSc2	Soběslav
<g/>
.	.	kIx.	.
jez	jez	k1gInSc1	jez
Roudná	Roudný	k2eAgFnSc1d1	Roudná
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Roudná	Roudný	k2eAgFnSc1d1	Roudná
<g/>
.	.	kIx.	.
jez	jíst	k5eAaImRp2nS	jíst
Soukeník	soukeník	k1gMnSc1	soukeník
u	u	k7c2	u
Sezimova	Sezimův	k2eAgNnSc2d1	Sezimovo
Ústí	ústí	k1gNnSc2	ústí
<g/>
,	,	kIx,	,
tábořiště	tábořiště	k1gNnPc1	tábořiště
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
náročný	náročný	k2eAgInSc1d1	náročný
pro	pro	k7c4	pro
sjezd	sjezd	k1gInSc4	sjezd
lodí	loď	k1gFnPc2	loď
jez	jez	k1gInSc1	jez
Stádlec	Stádlec	k1gInSc4	Stádlec
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
řetězovým	řetězový	k2eAgInSc7d1	řetězový
mostem	most	k1gInSc7	most
jez	jez	k1gInSc1	jez
před	před	k7c7	před
Bechyní	Bechyně	k1gFnSc7	Bechyně
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
obloukovým	obloukový	k2eAgInSc7d1	obloukový
mostem	most	k1gInSc7	most
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
360	[number]	k4	360
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
V	v	k7c6	v
Dráchově	Dráchův	k2eAgFnSc6d1	Dráchův
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
postaven	postaven	k2eAgInSc4d1	postaven
nový	nový	k2eAgInSc4d1	nový
most	most	k1gInSc4	most
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
do	do	k7c2	do
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
jezem	jez	k1gInSc7	jez
Steiniger	Steiniger	k1gInSc1	Steiniger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silniční	silniční	k2eAgInSc4d1	silniční
most	most	k1gInSc4	most
(	(	kIx(	(
<g/>
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Bechyňský	bechyňský	k2eAgMnSc1d1	bechyňský
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
generální	generální	k2eAgFnSc1d1	generální
oprava	oprava	k1gFnSc1	oprava
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
Soběslav-Bechyně	Soběslav-Bechyně	k1gFnSc2	Soběslav-Bechyně
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Soběslavských	soběslavský	k2eAgNnPc2d1	Soběslavské
Blat	Blata	k1gNnPc2	Blata
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přes	přes	k7c4	přes
obce	obec	k1gFnPc4	obec
Vesce	Vesce	k1gFnSc2	Vesce
<g/>
,	,	kIx,	,
Vlastiboř	Vlastiboř	k1gFnSc4	Vlastiboř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
Soběslaví	Soběslav	k1gFnSc7	Soběslav
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
Špačkova	Špačkův	k2eAgInSc2d1	Špačkův
mlýna	mlýn	k1gInSc2	mlýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
Lužnice	Lužnice	k1gFnPc4	Lužnice
lanový	lanový	k2eAgInSc1d1	lanový
most-lávka	mostávka	k1gFnSc1	most-lávka
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Soběslaví	Soběslav	k1gFnPc2	Soběslav
<g/>
,	,	kIx,	,
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Klenovice	Klenovice	k1gFnSc2	Klenovice
(	(	kIx(	(
<g/>
na	na	k7c4	na
E	E	kA	E
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Rybova	Rybův	k2eAgMnSc2d1	Rybův
Lhota	Lhota	k1gMnSc1	Lhota
<g/>
,	,	kIx,	,
Želeč	Želeč	k1gMnSc1	Želeč
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
Lužnici	Lužnice	k1gFnSc4	Lužnice
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
z	z	k7c2	z
r.	r.	kA	r.
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
nosnost	nosnost	k1gFnSc1	nosnost
140	[number]	k4	140
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
kovový	kovový	k2eAgInSc1d1	kovový
most	most	k1gInSc1	most
Ovčín	ovčín	k1gInSc1	ovčín
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
57	[number]	k4	57
<g/>
m	m	kA	m
dl	dl	k?	dl
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
<g/>
m	m	kA	m
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
)	)	kIx)	)
U	u	k7c2	u
obce	obec	k1gFnSc2	obec
Skalice	Skalice	k1gFnSc2	Skalice
(	(	kIx(	(
<g/>
u	u	k7c2	u
chatové	chatový	k2eAgFnSc2d1	chatová
oblasti	oblast	k1gFnSc2	oblast
u	u	k7c2	u
jezer	jezero	k1gNnPc2	jezero
bývalé	bývalý	k2eAgFnSc2d1	bývalá
pískovny	pískovna	k1gFnSc2	pískovna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
betonová	betonový	k2eAgFnSc1d1	betonová
lávka	lávka	k1gFnSc1	lávka
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
a	a	k8xC	a
cyklotrasu	cyklotrasa	k1gFnSc4	cyklotrasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plané	Planá	k1gFnSc6	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
silniční	silniční	k2eAgInSc1d1	silniční
most	most	k1gInSc1	most
od	od	k7c2	od
E55	E55	k1gFnSc2	E55
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
směr	směr	k1gInSc4	směr
Malšice	Malšic	k1gMnSc2	Malšic
<g/>
,	,	kIx,	,
Bechyně	Bechyně	k1gMnSc2	Bechyně
nebo	nebo	k8xC	nebo
vlevo	vlevo	k6eAd1	vlevo
směr	směr	k1gInSc1	směr
Želeč	Želeč	k1gInSc1	Želeč
<g/>
,	,	kIx,	,
Hlavatce	hlavatec	k1gInPc1	hlavatec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
celokovový	celokovový	k2eAgMnSc1d1	celokovový
<g/>
)	)	kIx)	)
budovaný	budovaný	k2eAgMnSc1d1	budovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901-1902	[number]	k4	1901-1902
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
první	první	k4xOgFnSc2	první
elektrifikované	elektrifikovaný	k2eAgFnSc2d1	elektrifikovaná
trati	trať	k1gFnSc2	trať
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
mezi	mezi	k7c7	mezi
Táborem	Tábor	k1gInSc7	Tábor
a	a	k8xC	a
Bechyní	Bechyně	k1gFnSc7	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
inženýra	inženýr	k1gMnSc2	inženýr
Františka	František	k1gMnSc2	František
Křižíka	Křižík	k1gMnSc2	Křižík
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
přes	přes	k7c4	přes
Lužnici	Lužnice	k1gFnSc4	Lužnice
obloukový	obloukový	k2eAgInSc4d1	obloukový
železobetonový	železobetonový	k2eAgInSc4d1	železobetonový
Švehlův	Švehlův	k2eAgInSc4d1	Švehlův
most	most	k1gInSc4	most
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
Tábor-Bechyně	Tábor-Bechyně	k1gFnSc2	Tábor-Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
u	u	k7c2	u
chatové	chatový	k2eAgFnSc2d1	chatová
oblasti	oblast	k1gFnSc2	oblast
"	"	kIx"	"
<g/>
Riviera	Riviera	k1gFnSc1	Riviera
<g/>
"	"	kIx"	"
a	a	k8xC	a
lesní	lesní	k2eAgFnSc1d1	lesní
restaurace	restaurace	k1gFnSc1	restaurace
"	"	kIx"	"
<g/>
Harachovka	Harachovka	k1gFnSc1	Harachovka
<g/>
"	"	kIx"	"
-	-	kIx~	-
oblouková	obloukový	k2eAgFnSc1d1	oblouková
Harrachova	Harrachov	k1gInSc2	Harrachov
lávka	lávka	k1gFnSc1	lávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
tu	tu	k6eAd1	tu
původní	původní	k2eAgInSc1d1	původní
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
strhla	strhnout	k5eAaPmAgFnS	strhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1940	[number]	k4	1940
velká	velký	k2eAgFnSc1d1	velká
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
kry	kra	k1gFnPc1	kra
<g/>
)	)	kIx)	)
-	-	kIx~	-
součást	součást	k1gFnSc1	součást
turistické	turistický	k2eAgFnSc2d1	turistická
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
"	"	kIx"	"
<g/>
Harrachovy	Harrachov	k1gInPc1	Harrachov
stezky	stezka	k1gFnSc2	stezka
<g/>
"	"	kIx"	"
budované	budovaný	k2eAgInPc1d1	budovaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
do	do	k7c2	do
Příběnic	Příběnice	k1gFnPc2	Příběnice
po	po	k7c6	po
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Stádlec	Stádlec	k1gInSc1	Stádlec
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Lužnici	Lužnice	k1gFnSc6	Lužnice
znovu	znovu	k6eAd1	znovu
složen	složit	k5eAaPmNgMnS	složit
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
otevřen	otevřen	k2eAgInSc1d1	otevřen
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
řetězový	řetězový	k2eAgInSc1d1	řetězový
Stádlecký	Stádlecký	k2eAgInSc1d1	Stádlecký
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
Podolský	podolský	k2eAgMnSc1d1	podolský
<g/>
)	)	kIx)	)
tyčil	tyčit	k5eAaImAgMnS	tyčit
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dobronicích	Dobronice	k1gFnPc6	Dobronice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kolodějích	Koloděje	k1gFnPc6	Koloděje
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
silniční	silniční	k2eAgFnSc7d1	silniční
most	most	k1gInSc4	most
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
Týn	Týn	k1gInSc4	Týn
nad	nad	k7c7	nad
Vltavou-Chrášťany	Vltavou-Chrášťan	k1gMnPc7	Vltavou-Chrášťan
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Bechyně	Bechyně	k1gFnSc2	Bechyně
Lužnici	Lužnice	k1gFnSc4	Lužnice
překlenuje	překlenovat	k5eAaImIp3nS	překlenovat
unikátní	unikátní	k2eAgInSc1d1	unikátní
obloukový	obloukový	k2eAgInSc1d1	obloukový
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
most	most	k1gInSc1	most
s	s	k7c7	s
kolejemi	kolej	k1gFnPc7	kolej
a	a	k8xC	a
vozovkou	vozovka	k1gFnSc7	vozovka
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc4d1	známý
pod	pod	k7c7	pod
poetickým	poetický	k2eAgInSc7d1	poetický
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Bechyňská	bechyňský	k2eAgFnSc1d1	Bechyňská
duha	duha	k1gFnSc1	duha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1928	[number]	k4	1928
k	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
samostatnosti	samostatnost	k1gFnSc2	samostatnost
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Foto	foto	k1gNnSc1	foto
obloukové	obloukový	k2eAgFnSc2d1	oblouková
lávky	lávka	k1gFnSc2	lávka
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Nové	Nové	k2eAgFnSc2d1	Nové
řeky	řeka	k1gFnSc2	řeka
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Stříbřec	Stříbřec	k1gMnSc1	Stříbřec
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
můžeme	moct	k5eAaImIp1nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
pomník	pomník	k1gInSc4	pomník
Emy	Ema	k1gFnSc2	Ema
Destinnové	Destinnová	k1gFnSc2	Destinnová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Stráži	stráž	k1gFnSc6	stráž
nad	nad	k7c7	nad
Nežárkou	Nežárka	k1gFnSc7	Nežárka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
(	(	kIx(	(
<g/>
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Kozského	Kozský	k2eAgInSc2d1	Kozský
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
druhého	druhý	k4xOgMnSc2	druhý
prezidenta	prezident	k1gMnSc2	prezident
ČSR	ČSR	kA	ČSR
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
-	-	kIx~	-
vilu	vila	k1gFnSc4	vila
<g/>
,	,	kIx,	,
postavenou	postavený	k2eAgFnSc4d1	postavená
v	v	k7c6	v
letech	let	k1gInPc6	let
1930-1931	[number]	k4	1930-1931
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
zahradu	zahrada	k1gFnSc4	zahrada
a	a	k8xC	a
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
hrobka	hrobka	k1gFnSc1	hrobka
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
přání	přání	k1gNnSc2	přání
vyprojektoval	vyprojektovat	k5eAaPmAgMnS	vyprojektovat
významný	významný	k2eAgMnSc1d1	významný
čs	čs	kA	čs
<g/>
.	.	kIx.	.
architekt	architekt	k1gMnSc1	architekt
Pavel	Pavel	k1gMnSc1	Pavel
Janák	Janák	k1gMnSc1	Janák
<g/>
.	.	kIx.	.
</s>
<s>
Hrob	hrob	k1gInSc1	hrob
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
návrší	návrš	k1gFnPc2	návrš
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
říká	říkat	k5eAaImIp3nS	říkat
Kazatelna	kazatelna	k1gFnSc1	kazatelna
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
zde	zde	k6eAd1	zde
kázal	kázat	k5eAaImAgMnS	kázat
mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
u	u	k7c2	u
Příběnic	Příběnice	k1gFnPc2	Příběnice
měla	mít	k5eAaImAgFnS	mít
stavět	stavět	k5eAaImF	stavět
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Lužnici	Lužnice	k1gFnSc6	Lužnice
<g/>
,	,	kIx,	,
rybách	ryba	k1gFnPc6	ryba
a	a	k8xC	a
vodě	voda	k1gFnSc6	voda
:	:	kIx,	:
v	v	k7c6	v
Lužnici	Lužnice	k1gFnSc6	Lužnice
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
chycený	chycený	k2eAgInSc4d1	chycený
v	v	k7c6	v
Dráchově	Dráchův	k2eAgInSc6d1	Dráchův
sumec	sumec	k1gMnSc1	sumec
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
52	[number]	k4	52
kg	kg	kA	kg
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
217	[number]	k4	217
cm	cm	kA	cm
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
chycen	chytit	k5eAaPmNgInS	chytit
do	do	k7c2	do
slupu	slup	k1gInSc2	slup
-	-	kIx~	-
lapadla	lapadlo	k1gNnPc1	lapadlo
ryb	ryba	k1gFnPc2	ryba
na	na	k7c6	na
jezu	jez	k1gInSc6	jez
<g/>
,	,	kIx,	,
sestaveného	sestavený	k2eAgMnSc4d1	sestavený
ze	z	k7c2	z
žlabu	žlab	k1gInSc2	žlab
a	a	k8xC	a
děrované	děrovaný	k2eAgFnSc2d1	děrovaná
bedny	bedna	k1gFnSc2	bedna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
udici	udice	k1gFnSc4	udice
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1953	[number]	k4	1953
chycený	chycený	k2eAgMnSc1d1	chycený
sumec	sumec	k1gMnSc1	sumec
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
38,5	[number]	k4	38,5
kg	kg	kA	kg
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
208	[number]	k4	208
cm	cm	kA	cm
<g/>
.	.	kIx.	.
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1822	[number]	k4	1822
trpěl	trpět	k5eAaImAgInS	trpět
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
takovým	takový	k3xDgInSc7	takový
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
že	že	k8xS	že
magistrát	magistrát	k1gInSc1	magistrát
žádal	žádat	k5eAaImAgInS	žádat
úřady	úřad	k1gInPc7	úřad
puštěním	puštění	k1gNnSc7	puštění
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
Lužnici	Lužnice	k1gFnSc6	Lužnice
<g/>
.	.	kIx.	.
21.7	[number]	k4	21.7
<g/>
.1432	.1432	k4	.1432
-	-	kIx~	-
při	při	k7c6	při
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
povodni	povodeň	k1gFnSc6	povodeň
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
k	k	k7c3	k
zatopení	zatopení	k1gNnSc3	zatopení
Staroměstského	staroměstský	k2eAgNnSc2d1	Staroměstské
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Povodně	povodeň	k1gFnPc1	povodeň
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velkou	velký	k2eAgFnSc4d1	velká
neúrodu	neúroda	k1gFnSc4	neúroda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obilí	obilí	k1gNnSc4	obilí
<g/>
.	.	kIx.	.
17.8	[number]	k4	17.8
<g/>
.1501	.1501	k4	.1501
-	-	kIx~	-
velká	velký	k2eAgFnSc1d1	velká
voda	voda	k1gFnSc1	voda
poškodila	poškodit	k5eAaPmAgFnS	poškodit
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
mnohé	mnohý	k2eAgInPc1d1	mnohý
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
20.8	[number]	k4	20.8
<g/>
.1544	.1544	k4	.1544
-	-	kIx~	-
velká	velký	k2eAgFnSc1d1	velká
povodeň	povodeň	k1gFnSc1	povodeň
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Čech	Čechy	k1gFnPc2	Čechy
dorazila	dorazit	k5eAaPmAgFnS	dorazit
až	až	k6eAd1	až
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
opět	opět	k6eAd1	opět
nutnost	nutnost	k1gFnSc4	nutnost
stavby	stavba	k1gFnSc2	stavba
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
rybníku	rybník	k1gInSc6	rybník
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
18.7	[number]	k4	18.7
<g/>
.1736	.1736	k4	.1736
-	-	kIx~	-
po	po	k7c6	po
28	[number]	k4	28
dnech	den	k1gInPc6	den
dešťů	dešť	k1gInPc2	dešť
se	se	k3xPyFc4	se
protrhly	protrhnout	k5eAaPmAgFnP	protrhnout
hráze	hráz	k1gFnPc1	hráz
rybníků	rybník	k1gInPc2	rybník
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
Tučap	Tučap	k1gMnSc1	Tučap
a	a	k8xC	a
na	na	k7c6	na
Dírensku	Dírensko	k1gNnSc6	Dírensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
povodňová	povodňový	k2eAgFnSc1d1	povodňová
vlna	vlna	k1gFnSc1	vlna
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
i	i	k8xC	i
Nežárky	Nežárka	k1gFnSc2	Nežárka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
asi	asi	k9	asi
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
velkou	velký	k2eAgFnSc7d1	velká
vodou	voda	k1gFnSc7	voda
město	město	k1gNnSc1	město
Soběslav	Soběslav	k1gMnSc1	Soběslav
<g/>
.	.	kIx.	.
25.7	[number]	k4	25.7
<g/>
.1740	.1740	k4	.1740
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Soběslavsku	Soběslavsko	k1gNnSc6	Soběslavsko
obdobná	obdobný	k2eAgFnSc1d1	obdobná
situace	situace	k1gFnSc1	situace
jako	jako	k8xC	jako
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1736	[number]	k4	1736
22.2	[number]	k4	22.2
<g/>
.1865	.1865	k4	.1865
-	-	kIx~	-
při	při	k7c6	při
zimní	zimní	k2eAgFnSc6d1	zimní
povodni	povodeň	k1gFnSc6	povodeň
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
rozlila	rozlít	k5eAaPmAgFnS	rozlít
z	z	k7c2	z
břehů	břeh	k1gInPc2	břeh
20.2	[number]	k4	20.2
<g/>
.1890	.1890	k4	.1890
-	-	kIx~	-
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
a	a	k8xC	a
tání	tání	k1gNnSc2	tání
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
povodní	povodeň	k1gFnPc2	povodeň
na	na	k7c6	na
Lužnici	Lužnice	k1gFnSc6	Lužnice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
Záznamy	záznam	k1gInPc7	záznam
o	o	k7c6	o
datech	datum	k1gNnPc6	datum
a	a	k8xC	a
výškách	výška	k1gFnPc6	výška
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
mlýnu	mlýn	k1gInSc3	mlýn
Veselý	veselý	k2eAgMnSc1d1	veselý
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
)	)	kIx)	)
1.9	[number]	k4	1.9
<g/>
.1890	.1890	k4	.1890
-	-	kIx~	-
po	po	k7c6	po
vydatných	vydatný	k2eAgInPc6d1	vydatný
deštích	dešť	k1gInPc6	dešť
se	se	k3xPyFc4	se
rozvodnila	rozvodnit	k5eAaPmAgFnS	rozvodnit
celá	celý	k2eAgFnSc1d1	celá
Lužnice	Lužnice	k1gFnSc1	Lužnice
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
místy	místy	k6eAd1	místy
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
až	až	k9	až
o	o	k7c4	o
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
zaplaveno	zaplaven	k2eAgNnSc1d1	zaplaveno
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
domů	dům	k1gInPc2	dům
a	a	k8xC	a
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
16.3	[number]	k4	16.3
<g/>
.1940	.1940	k4	.1940
-	-	kIx~	-
šla	jít	k5eAaImAgFnS	jít
řekou	řeka	k1gFnSc7	řeka
velká	velký	k2eAgFnSc1d1	velká
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
ledové	ledový	k2eAgFnSc2d1	ledová
kry	kra	k1gFnSc2	kra
strhly	strhnout	k5eAaPmAgFnP	strhnout
lávku	lávka	k1gFnSc4	lávka
u	u	k7c2	u
Harrachovky	Harrachovka	k1gFnSc2	Harrachovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
26.3	[number]	k4	26.3
<g/>
.1988	.1988	k4	.1988
-	-	kIx~	-
po	po	k7c6	po
prudkém	prudký	k2eAgNnSc6d1	prudké
tání	tání	k1gNnSc6	tání
a	a	k8xC	a
deštích	dešť	k1gInPc6	dešť
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Táborsku	Táborsko	k1gNnSc6	Táborsko
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
na	na	k7c4	na
3	[number]	k4	3
tisíce	tisíc	k4xCgInSc2	tisíc
hektarů	hektar	k1gInPc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
asi	asi	k9	asi
30	[number]	k4	30
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zej	zet	k5eAaImRp2nS	zet
<g/>
.	.	kIx.	.
</s>
<s>
Roudná	Roudné	k1gNnPc1	Roudné
<g/>
,	,	kIx,	,
Klenovice	Klenovice	k1gFnPc1	Klenovice
<g/>
,	,	kIx,	,
Bechyně	Bechyně	k1gFnPc1	Bechyně
<g/>
)	)	kIx)	)
13.8	[number]	k4	13.8
<g/>
.2002	.2002	k4	.2002
-	-	kIx~	-
po	po	k7c6	po
vydatných	vydatný	k2eAgInPc6d1	vydatný
deštích	dešť	k1gInPc6	dešť
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
zaznamenané	zaznamenaný	k2eAgFnSc3d1	zaznamenaná
povodni	povodeň	k1gFnSc3	povodeň
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
rozvodněná	rozvodněný	k2eAgFnSc1d1	rozvodněná
Lužnice	Lužnice	k1gFnSc1	Lužnice
<g/>
,	,	kIx,	,
Nežárka	Nežárka	k1gFnSc1	Nežárka
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
a	a	k8xC	a
přetékající	přetékající	k2eAgInPc1d1	přetékající
nebo	nebo	k8xC	nebo
protržené	protržený	k2eAgInPc1d1	protržený
rybníky	rybník	k1gInPc1	rybník
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
rozlily	rozlít	k5eAaPmAgFnP	rozlít
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
nejen	nejen	k6eAd1	nejen
tisíce	tisíc	k4xCgInSc2	tisíc
hektarů	hektar	k1gInPc2	hektar
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ulice	ulice	k1gFnPc1	ulice
a	a	k8xC	a
domy	dům	k1gInPc1	dům
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
až	až	k9	až
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hrozilo	hrozit	k5eAaImAgNnS	hrozit
mj.	mj.	kA	mj.
také	také	k9	také
protržení	protržení	k1gNnSc4	protržení
hrází	hráz	k1gFnPc2	hráz
rybníku	rybník	k1gInSc6	rybník
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
nebo	nebo	k8xC	nebo
nádrže	nádrž	k1gFnPc1	nádrž
Jordán	Jordán	k1gInSc1	Jordán
-	-	kIx~	-
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
následná	následný	k2eAgFnSc1d1	následná
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Škody	škoda	k1gFnPc1	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
(	(	kIx(	(
<g/>
zničené	zničený	k2eAgInPc1d1	zničený
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
nebyly	být	k5eNaImAgFnP	být
jen	jen	k9	jen
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
nedozírné	dozírný	k2eNgFnPc1d1	nedozírná
skoro	skoro	k6eAd1	skoro
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Povodeň	povodeň	k1gFnSc1	povodeň
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
těžce	těžce	k6eAd1	těžce
i	i	k9	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zatopení	zatopení	k1gNnSc3	zatopení
celých	celý	k2eAgFnPc2d1	celá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
i	i	k9	i
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
28.3	[number]	k4	28.3
<g/>
.2006	.2006	k4	.2006
-	-	kIx~	-
při	při	k7c6	při
prudkém	prudký	k2eAgNnSc6d1	prudké
tání	tání	k1gNnSc6	tání
velké	velký	k2eAgFnSc2d1	velká
vrstvy	vrstva	k1gFnSc2	vrstva
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
voda	voda	k1gFnSc1	voda
Lužnice	Lužnice	k1gFnSc2	Lužnice
a	a	k8xC	a
přítoků	přítok	k1gInPc2	přítok
a	a	k8xC	a
v	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
prudce	prudko	k6eAd1	prudko
stoupala	stoupat	k5eAaImAgFnS	stoupat
a	a	k8xC	a
30.3	[number]	k4	30.3
<g/>
.	.	kIx.	.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záplavám	záplava	k1gFnPc3	záplava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
-	-	kIx~	-
zatopeno	zatopen	k2eAgNnSc1d1	zatopeno
24	[number]	k4	24
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
270	[number]	k4	270
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
bylo	být	k5eAaImAgNnS	být
1200	[number]	k4	1200
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
voda	voda	k1gFnSc1	voda
na	na	k7c4	na
150	[number]	k4	150
ha	ha	kA	ha
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
silnice	silnice	k1gFnSc2	silnice
E	E	kA	E
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
Černovického	Černovický	k2eAgInSc2d1	Černovický
potoka	potok	k1gInSc2	potok
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
7	[number]	k4	7
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
70	[number]	k4	70
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
škody	škoda	k1gFnSc2	škoda
přesáhly	přesáhnout	k5eAaPmAgInP	přesáhnout
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Postiženy	postižen	k2eAgFnPc1d1	postižena
byly	být	k5eAaImAgFnP	být
obce	obec	k1gFnPc1	obec
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
Planá	Planá	k1gFnSc1	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
<g/>
,	,	kIx,	,
Bechyně	Bechyně	k1gFnSc1	Bechyně
i	i	k8xC	i
Tábor-Čelkovice	Tábor-Čelkovice	k1gFnSc1	Tábor-Čelkovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ČR	ČR	kA	ČR
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
pro	pro	k7c4	pro
Jihočeský	jihočeský	k2eAgInSc4d1	jihočeský
kraj	kraj	k1gInSc4	kraj
nouzový	nouzový	k2eAgInSc4d1	nouzový
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
10.4	[number]	k4	10.4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Povodeň	povodeň	k1gFnSc1	povodeň
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
