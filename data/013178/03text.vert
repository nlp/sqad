<p>
<s>
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
buk	buk	k1gInSc1	buk
u	u	k7c2	u
Libáně	Libán	k1gInSc6	Libán
býval	bývat	k5eAaImAgInS	bývat
největším	veliký	k2eAgInSc7d3	veliký
bukem	buk	k1gInSc7	buk
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
rozpadlého	rozpadlý	k2eAgInSc2d1	rozpadlý
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
–	–	k?	–
podle	podle	k7c2	podle
místních	místní	k2eAgMnPc2d1	místní
zahynul	zahynout	k5eAaPmAgMnS	zahynout
pod	pod	k7c7	pod
hromadami	hromada	k1gFnPc7	hromada
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
vyvezli	vyvézt	k5eAaPmAgMnP	vyvézt
družstevníci	družstevník	k1gMnPc1	družstevník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stromu	strom	k1gInSc3	strom
byl	být	k5eAaImAgInS	být
věnován	věnován	k2eAgInSc1d1	věnován
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
Paměť	paměť	k1gFnSc4	paměť
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
dílu	díl	k1gInSc2	díl
č.	č.	k?	č.
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
Žižkovy	Žižkův	k2eAgInPc1d1	Žižkův
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
</p>
