<p>
<s>
Polák	Polák	k1gMnSc1	Polák
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Aythya	Aythya	k1gMnSc1	Aythya
nyroca	nyroca	k1gMnSc1	nyroca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
potápivá	potápivý	k2eAgFnSc1d1	potápivá
kachna	kachna	k1gFnSc1	kachna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgInSc1d3	nejmenší
polák	polák	k1gInSc1	polák
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
přibližně	přibližně	k6eAd1	přibližně
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
410	[number]	k4	410
<g/>
–	–	k?	–
<g/>
650	[number]	k4	650
g	g	kA	g
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
67	[number]	k4	67
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
kaštanově	kaštanově	k6eAd1	kaštanově
hnědý	hnědý	k2eAgInSc1d1	hnědý
s	s	k7c7	s
tmavějším	tmavý	k2eAgInSc7d2	tmavší
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
okraji	okraj	k1gInPc7	okraj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břich	k1gInSc6	břich
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
snadno	snadno	k6eAd1	snadno
rozeznat	rozeznat	k5eAaPmF	rozeznat
od	od	k7c2	od
podobných	podobný	k2eAgFnPc2d1	podobná
samic	samice	k1gFnPc2	samice
poláka	polák	k1gMnSc2	polák
chocholačky	chocholačka	k1gFnSc2	chocholačka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
samci	samec	k1gMnSc3	samec
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
jednotvárnější	jednotvárný	k2eAgNnSc4d2	jednotvárnější
zbarvení	zbarvení	k1gNnSc4	zbarvení
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc4d1	tmavá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Polák	Polák	k1gMnSc1	Polák
malý	malý	k1gMnSc1	malý
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
stahuje	stahovat	k5eAaImIp3nS	stahovat
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
ke	k	k7c3	k
Kaspickému	kaspický	k2eAgNnSc3d1	Kaspické
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
početnost	početnost	k1gFnSc1	početnost
poláka	polák	k1gMnSc2	polák
malého	malý	k2eAgMnSc2d1	malý
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
silně	silně	k6eAd1	silně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgMnPc2d3	nejvzácnější
českých	český	k2eAgMnPc2d1	český
poláků	polák	k1gMnPc2	polák
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
maximálně	maximálně	k6eAd1	maximálně
3	[number]	k4	3
páry	pár	k1gInPc4	pár
a	a	k8xC	a
zimuje	zimovat	k5eAaImIp3nS	zimovat
1	[number]	k4	1
-	-	kIx~	-
5	[number]	k4	5
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polák	Polák	k1gMnSc1	Polák
malý	malý	k1gMnSc1	malý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
párech	pár	k1gInPc6	pár
nebo	nebo	k8xC	nebo
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
hejnech	hejno	k1gNnPc6	hejno
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
potápivých	potápivý	k2eAgFnPc2d1	potápivá
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
poláků	polák	k1gInPc2	polák
a	a	k8xC	a
zrzohlávek	zrzohlávka	k1gFnPc2	zrzohlávka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
<g/>
,	,	kIx,	,
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
provádí	provádět	k5eAaImIp3nS	provádět
pro	pro	k7c4	pro
poláky	polák	k1gMnPc4	polák
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
výskok	výskok	k1gInSc4	výskok
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
na	na	k7c4	na
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
jen	jen	k9	jen
asi	asi	k9	asi
do	do	k7c2	do
metrové	metrový	k2eAgFnSc2d1	metrová
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
převážně	převážně	k6eAd1	převážně
vodní	vodní	k2eAgFnSc2d1	vodní
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
drobné	drobný	k2eAgMnPc4d1	drobný
vodní	vodní	k2eAgMnPc4d1	vodní
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
a	a	k8xC	a
malé	malý	k2eAgFnPc4d1	malá
rybky	rybka	k1gFnPc4	rybka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hlubších	hluboký	k2eAgNnPc2d2	hlubší
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
močálu	močál	k1gInSc2	močál
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
porostu	porost	k1gInSc6	porost
a	a	k8xC	a
během	během	k7c2	během
května	květen	k1gInSc2	květen
až	až	k8xS	až
června	červen	k1gInSc2	červen
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
klade	klást	k5eAaImIp3nS	klást
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
světle	světle	k6eAd1	světle
žlutých	žlutý	k2eAgNnPc2d1	žluté
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
střídavě	střídavě	k6eAd1	střídavě
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
28	[number]	k4	28
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
mláďata	mládě	k1gNnPc4	mládě
pečuje	pečovat	k5eAaImIp3nS	pečovat
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ferruginous	Ferruginous	k1gMnSc1	Ferruginous
Duck	Duck	k1gMnSc1	Duck
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DUNGEL	DUNGEL	kA	DUNGEL
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
249	[number]	k4	249
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
927	[number]	k4	927
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Potápivé	Potápivý	k2eAgFnSc2d1	Potápivá
kachny	kachna	k1gFnSc2	kachna
<g/>
:	:	kIx,	:
zrzohlávka	zrzohlávka	k1gFnSc1	zrzohlávka
<g/>
,	,	kIx,	,
poláci	polák	k1gMnPc1	polák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
polák	polák	k1gMnSc1	polák
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
polák	polák	k1gMnSc1	polák
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Aythya	Aythyus	k1gMnSc2	Aythyus
nyroca	nyrocus	k1gMnSc2	nyrocus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
http://drubez.chovzvirat.com/druhy/polak-maly.html	[url]	k1gMnSc1	http://drubez.chovzvirat.com/druhy/polak-maly.html
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
zlínské	zlínský	k2eAgFnSc2d1	zlínská
ZOO	zoo	k1gFnSc2	zoo
</s>
</p>
