<s>
Uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
přisvojuje	přisvojovat	k5eAaImIp3nS	přisvojovat
cizí	cizí	k2eAgNnPc4d1	cizí
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
požitky	požitek	k1gInPc4	požitek
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
násilím	násilí	k1gNnSc7	násilí
nebo	nebo	k8xC	nebo
svévolným	svévolný	k2eAgNnSc7d1	svévolné
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
