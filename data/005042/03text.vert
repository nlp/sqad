<s>
Uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
přisvojuje	přisvojovat	k5eAaImIp3nS	přisvojovat
cizí	cizí	k2eAgNnPc4d1	cizí
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
požitky	požitek	k1gInPc4	požitek
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
násilím	násilí	k1gNnSc7	násilí
nebo	nebo	k8xC	nebo
svévolným	svévolný	k2eAgNnSc7d1	svévolné
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
označuje	označovat	k5eAaImIp3nS	označovat
uchvatitele	uchvatitel	k1gMnSc4	uchvatitel
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dějin	dějiny	k1gFnPc2	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
aspiranty	aspirant	k1gMnPc4	aspirant
římského	římský	k2eAgInSc2d1	římský
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
snahách	snaha	k1gFnPc6	snaha
neuspěli	uspět	k5eNaPmAgMnP	uspět
–	–	k?	–
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
,	,	kIx,	,
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
<g/>
,	,	kIx,	,
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
atd.	atd.	kA	atd.
Protože	protože	k8xS	protože
však	však	k9	však
jako	jako	k9	jako
uzurpátoři	uzurpátor	k1gMnPc1	uzurpátor
začínali	začínat	k5eAaImAgMnP	začínat
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
"	"	kIx"	"
<g/>
legální	legální	k2eAgMnPc1d1	legální
<g/>
"	"	kIx"	"
císaři	císař	k1gMnPc1	císař
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Decius	Decius	k1gMnSc1	Decius
či	či	k8xC	či
Licinius	Licinius	k1gMnSc1	Licinius
Valerianus	Valerianus	k1gMnSc1	Valerianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
uzurpaci	uzurpace	k1gFnSc4	uzurpace
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
otázka	otázka	k1gFnSc1	otázka
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
uznán	uznat	k5eAaPmNgMnS	uznat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
říši	říš	k1gFnSc6	říš
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
podstatné	podstatný	k2eAgFnSc6d1	podstatná
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
legálního	legální	k2eAgMnSc4d1	legální
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
získal	získat	k5eAaPmAgMnS	získat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xC	jako
uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
proti	proti	k7c3	proti
jinému	jiný	k2eAgMnSc3d1	jiný
"	"	kIx"	"
<g/>
legálnímu	legální	k2eAgInSc3d1	legální
<g/>
"	"	kIx"	"
císaři	císař	k1gMnSc6	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
Římané	Říman	k1gMnPc1	Říman
označovali	označovat	k5eAaImAgMnP	označovat
uzurpátory	uzurpátor	k1gMnPc4	uzurpátor
termínem	termín	k1gInSc7	termín
tyrannus	tyrannus	k1gMnSc1	tyrannus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
zavádějícím	zavádějící	k2eAgInSc7d1	zavádějící
dojmem	dojem	k1gInSc7	dojem
–	–	k?	–
také	také	k9	také
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
vhodnějším	vhodný	k2eAgInSc7d2	vhodnější
výrazem	výraz	k1gInSc7	výraz
uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
roajalistická	roajalistický	k2eAgFnSc1d1	roajalistická
opozice	opozice	k1gFnSc1	opozice
užívala	užívat	k5eAaImAgFnS	užívat
termín	termín	k1gInSc4	termín
uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
pro	pro	k7c4	pro
Napoleona	Napoleon	k1gMnSc4	Napoleon
Bonaparta	Bonapart	k1gMnSc4	Bonapart
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
názoru	názor	k1gInSc2	názor
svévolně	svévolně	k6eAd1	svévolně
uchvátil	uchvátit	k5eAaPmAgInS	uchvátit
vládu	vláda	k1gFnSc4	vláda
náležející	náležející	k2eAgFnSc4d1	náležející
Bourbonům	bourbon	k1gInPc3	bourbon
<g/>
.	.	kIx.	.
</s>
<s>
Sporadicky	sporadicky	k6eAd1	sporadicky
byl	být	k5eAaImAgMnS	být
uzurpátorem	uzurpátor	k1gMnSc7	uzurpátor
nazýván	nazývat	k5eAaImNgMnS	nazývat
i	i	k9	i
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Egon	Egon	k1gMnSc1	Egon
Flaig	Flaig	k1gMnSc1	Flaig
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
Kaiser	Kaiser	k1gMnSc1	Kaiser
herausfordern	herausfordern	k1gMnSc1	herausfordern
–	–	k?	–
Die	Die	k1gFnSc1	Die
Usurpation	Usurpation	k1gInSc1	Usurpation
im	im	k?	im
Römischen	Römischen	k1gInSc1	Römischen
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
1992	[number]	k4	1992
</s>
