<s>
Pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
gameta	gameta	k1gFnSc1	gameta
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
γ	γ	k?	γ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
umožnit	umožnit	k5eAaPmF	umožnit
vznik	vznik	k1gInSc4	vznik
nového	nový	k2eAgMnSc2d1	nový
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Gamety	gameta	k1gFnPc1	gameta
vznikají	vznikat	k5eAaImIp3nP	vznikat
procesem	proces	k1gInSc7	proces
redukčního	redukční	k2eAgNnSc2d1	redukční
dělení	dělení	k1gNnSc2	dělení
gametocytů	gametocyt	k1gInPc2	gametocyt
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
počet	počet	k1gInSc1	počet
chromozómových	chromozómový	k2eAgFnPc2d1	chromozómová
sad	sada	k1gFnPc2	sada
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
tedy	tedy	k9	tedy
nejčastěji	často	k6eAd3	často
haploidní	haploidní	k2eAgFnPc1d1	haploidní
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
sadu	sada	k1gFnSc4	sada
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
gamety	gamet	k1gInPc4	gamet
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
sady	sada	k1gFnSc2	sada
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nového	nový	k2eAgMnSc2d1	nový
jedince	jedinec	k1gMnSc2	jedinec
potřebné	potřebný	k2eAgNnSc1d1	potřebné
oplození	oplození	k1gNnSc1	oplození
<g/>
,	,	kIx,	,
splynutí	splynutí	k1gNnSc1	splynutí
dvou	dva	k4xCgFnPc2	dva
gamet	gameta	k1gFnPc2	gameta
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
diploidní	diploidní	k2eAgFnSc1d1	diploidní
zygota	zygota	k1gFnSc1	zygota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
i	i	k9	i
neoplozená	oplozený	k2eNgFnSc1d1	neoplozená
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
parthenogeneze	parthenogeneze	k1gFnSc2	parthenogeneze
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
nebo	nebo	k8xC	nebo
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
androgeneze	androgeneze	k1gFnPc1	androgeneze
<g/>
)	)	kIx)	)
Gamety	gameta	k1gFnPc1	gameta
bývají	bývat	k5eAaImIp3nP	bývat
rozlišené	rozlišený	k2eAgInPc4d1	rozlišený
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
:	:	kIx,	:
samičí	samičí	k2eAgFnSc1d1	samičí
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
buňka	buňka	k1gFnSc1	buňka
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
vajíčko	vajíčko	k1gNnSc1	vajíčko
(	(	kIx(	(
<g/>
ovum	ovum	k1gInSc1	ovum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samčí	samčí	k2eAgFnPc1d1	samčí
gamety	gameta	k1gFnPc1	gameta
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opatřené	opatřený	k2eAgFnPc1d1	opatřená
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
bičíky	bičík	k1gInPc4	bičík
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
dostávat	dostávat	k5eAaImF	dostávat
k	k	k7c3	k
vajíčku	vajíčko	k1gNnSc3	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Bičíkaté	bičíkatý	k2eAgFnPc1d1	bičíkatá
samčí	samčí	k2eAgFnPc1d1	samčí
gamety	gameta	k1gFnPc1	gameta
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
spermie	spermie	k1gFnPc1	spermie
<g/>
,	,	kIx,	,
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
je	on	k3xPp3gInPc4	on
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
mechorosty	mechorost	k1gInPc1	mechorost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
samičí	samičí	k2eAgFnSc7d1	samičí
gametou	gameta	k1gFnSc7	gameta
vaječná	vaječný	k2eAgFnSc1d1	vaječná
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
oosféra	oosféra	k1gFnSc1	oosféra
<g/>
)	)	kIx)	)
a	a	k8xC	a
samčími	samčí	k2eAgFnPc7d1	samčí
gametami	gameta	k1gFnPc7	gameta
jsou	být	k5eAaImIp3nP	být
spermatické	spermatický	k2eAgFnPc1d1	spermatická
buňky	buňka	k1gFnPc1	buňka
pylové	pylový	k2eAgFnSc2d1	pylová
láčky	láčka	k1gFnSc2	láčka
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčko	vajíčko	k1gNnSc4	vajíčko
a	a	k8xC	a
pylové	pylový	k2eAgNnSc4d1	pylové
zrno	zrno	k1gNnSc4	zrno
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
výtrusům	výtrus	k1gInPc3	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
jak	jak	k6eAd1	jak
samčí	samčí	k2eAgNnSc1d1	samčí
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
samičí	samičí	k2eAgInPc1d1	samičí
gamety	gamet	k1gInPc1	gamet
(	(	kIx(	(
<g/>
hermafroditi	hermafrodit	k1gMnPc1	hermafrodit
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jednodomé	jednodomý	k2eAgFnPc1d1	jednodomá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
mají	mít	k5eAaImIp3nP	mít
pohlaví	pohlaví	k1gNnSc2	pohlaví
oddělené	oddělený	k2eAgFnSc2d1	oddělená
a	a	k8xC	a
jedinec	jedinec	k1gMnSc1	jedinec
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
samčí	samčí	k2eAgMnPc1d1	samčí
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
samičí	samičí	k2eAgFnPc4d1	samičí
gamety	gameta	k1gFnPc4	gameta
<g/>
.	.	kIx.	.
</s>
<s>
Živočichové	živočich	k1gMnPc1	živočich
s	s	k7c7	s
odděleným	oddělený	k2eAgNnSc7d1	oddělené
pohlavím	pohlaví	k1gNnSc7	pohlaví
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
pak	pak	k6eAd1	pak
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
dvoudomé	dvoudomý	k2eAgFnPc1d1	dvoudomá
<g/>
.	.	kIx.	.
</s>
<s>
Gamety	gamet	k1gInPc1	gamet
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
gonádách	gonáda	k1gFnPc6	gonáda
<g/>
,	,	kIx,	,
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
pohlavně	pohlavně	k6eAd1	pohlavně
se	se	k3xPyFc4	se
rozmnožujících	rozmnožující	k2eAgFnPc2d1	rozmnožující
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
gametangiích	gametangium	k1gNnPc6	gametangium
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgNnPc1d1	samičí
gametangia	gametangium	k1gNnPc1	gametangium
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
nazývána	nazýván	k2eAgNnPc1d1	nazýváno
archegonia	archegonium	k1gNnPc1	archegonium
(	(	kIx(	(
<g/>
zárodečníky	zárodečník	k1gInPc1	zárodečník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samčí	samčí	k2eAgNnPc1d1	samčí
antheridia	antheridium	k1gNnPc1	antheridium
(	(	kIx(	(
<g/>
pelatky	pelatka	k1gFnSc2	pelatka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
