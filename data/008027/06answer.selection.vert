<s>
Pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
gameta	gameta	k1gFnSc1	gameta
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
γ	γ	k?	γ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
umožnit	umožnit	k5eAaPmF	umožnit
vznik	vznik	k1gInSc4	vznik
nového	nový	k2eAgMnSc2d1	nový
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
