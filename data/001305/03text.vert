<s>
Kladníky	Kladník	k1gMnPc7	Kladník
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc1	obec
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
346	[number]	k4	346
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
asi	asi	k9	asi
160	[number]	k4	160
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
BAĎURA	Baďura	k1gMnSc1	Baďura
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Místopis	místopis	k1gInSc1	místopis
<g/>
.	.	kIx.	.
</s>
<s>
Lipenský	lipenský	k2eAgInSc1d1	lipenský
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Musejní	musejní	k2eAgInSc1d1	musejní
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
418	[number]	k4	418
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kladníky	Kladník	k1gInPc7	Kladník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
