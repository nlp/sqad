<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
;	;	kIx,	;
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
a	a	k8xC	a
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
