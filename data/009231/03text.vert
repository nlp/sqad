<p>
<s>
Zambijská	zambijský	k2eAgFnSc1d1	zambijská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
kolem	kolem	k7c2	kolem
14	[number]	k4	14
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
a	a	k8xC	a
Angola	Angola	k1gFnSc1	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
752	[number]	k4	752
614	[number]	k4	614
km	km	kA	km
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předkoloniální	Předkoloniální	k2eAgNnSc4d1	Předkoloniální
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Zambie	Zambie	k1gFnSc2	Zambie
žili	žít	k5eAaImAgMnP	žít
předchůdci	předchůdce	k1gMnPc1	předchůdce
člověka	člověk	k1gMnSc4	člověk
už	už	k6eAd1	už
před	před	k7c7	před
několika	několik	k4yIc7	několik
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Zambie	Zambie	k1gFnSc2	Zambie
byli	být	k5eAaImAgMnP	být
Khoinové	Khoin	k1gMnPc1	Khoin
a	a	k8xC	a
Sanové	Sanus	k1gMnPc1	Sanus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kmeny	kmen	k1gInPc1	kmen
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
vytlačeny	vytlačen	k2eAgInPc1d1	vytlačen
expandujícími	expandující	k2eAgInPc7d1	expandující
bantuskými	bantuský	k2eAgInPc7d1	bantuský
kmeny	kmen	k1gInPc7	kmen
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
předků	předek	k1gInPc2	předek
dnešních	dnešní	k2eAgMnPc2d1	dnešní
obyvatel	obyvatel	k1gMnPc2	obyvatel
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
velkou	velký	k2eAgFnSc7d1	velká
vlnou	vlna	k1gFnSc7	vlna
bantuských	bantuský	k2eAgMnPc2d1	bantuský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
lundská	lundský	k2eAgFnSc1d1	lundský
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
Barotsů	Barots	k1gMnPc2	Barots
(	(	kIx(	(
<g/>
správněji	správně	k6eAd2	správně
česky	česky	k6eAd1	česky
Loziů	Lozi	k1gInPc2	Lozi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zformované	zformovaný	k2eAgFnSc2d1	zformovaná
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
navštívil	navštívit	k5eAaPmAgMnS	navštívit
poprvé	poprvé	k6eAd1	poprvé
Zambii	Zambie	k1gFnSc4	Zambie
skotský	skotský	k2eAgMnSc1d1	skotský
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
David	David	k1gMnSc1	David
Livingstone	Livingston	k1gInSc5	Livingston
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Zambie	Zambie	k1gFnSc2	Zambie
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
český	český	k2eAgMnSc1d1	český
rodák	rodák	k1gMnSc1	rodák
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
částech	část	k1gFnPc6	část
Zambie	Zambie	k1gFnSc2	Zambie
cestoval	cestovat	k5eAaImAgInS	cestovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
a	a	k8xC	a
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnSc2	první
odborné	odborný	k2eAgFnSc2d1	odborná
etnografické	etnografický	k2eAgFnSc2d1	etnografická
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
říši	říš	k1gFnSc6	říš
Barotsů	Barots	k1gInPc2	Barots
(	(	kIx(	(
<g/>
Loziů	Lozi	k1gInPc2	Lozi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pouze	pouze	k6eAd1	pouze
německy	německy	k6eAd1	německy
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c4	v
Zambii	Zambie	k1gFnSc4	Zambie
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnPc1	první
podrobné	podrobný	k2eAgFnPc1d1	podrobná
mapy	mapa	k1gFnPc1	mapa
okolí	okolí	k1gNnSc2	okolí
Viktoriiných	Viktoriin	k2eAgInPc2d1	Viktoriin
vodopádů	vodopád	k1gInPc2	vodopád
a	a	k8xC	a
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Zambie	Zambie	k1gFnSc2	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
<g/>
,	,	kIx,	,
Botswany	Botswana	k1gFnSc2	Botswana
a	a	k8xC	a
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgNnSc4d1	koloniální
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
Barotsů	Barots	k1gMnPc2	Barots
obchodní	obchodní	k2eAgInSc4d1	obchodní
monopol	monopol	k1gInSc4	monopol
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
protektorát	protektorát	k1gInSc1	protektorát
nad	nad	k7c7	nad
Barotselandem	Barotseland	k1gInSc7	Barotseland
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
oblastmi	oblast	k1gFnPc7	oblast
spojen	spojit	k5eAaPmNgInS	spojit
do	do	k7c2	do
protektorátu	protektorát	k1gInSc2	protektorát
Severní	severní	k2eAgFnSc2d1	severní
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Objevené	objevený	k2eAgFnPc1d1	objevená
zásoby	zásoba	k1gFnPc1	zásoba
mědi	měď	k1gFnSc2	měď
přilákaly	přilákat	k5eAaPmAgInP	přilákat
množství	množství	k1gNnSc4	množství
evropských	evropský	k2eAgMnPc2d1	evropský
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
Severní	severní	k2eAgFnSc1d1	severní
Rhodesie	Rhodesie	k1gFnSc1	Rhodesie
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Rhodesií	Rhodesie	k1gFnSc7	Rhodesie
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ňaskem	Ňasko	k1gNnSc7	Ňasko
(	(	kIx(	(
<g/>
Malawi	Malawi	k1gNnSc7	Malawi
<g/>
)	)	kIx)	)
do	do	k7c2	do
Federace	federace	k1gFnSc2	federace
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
a	a	k8xC	a
Ňaska	Ňasko	k1gNnSc2	Ňasko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
zrušena	zrušit	k5eAaPmNgNnP	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Zambijské	zambijský	k2eAgFnSc2d1	zambijská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Kenneth	Kenneth	k1gInSc1	Kenneth
Kaunda	Kaund	k1gMnSc2	Kaund
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
postupně	postupně	k6eAd1	postupně
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
autoritativní	autoritativní	k2eAgInSc4d1	autoritativní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
podniky	podnik	k1gInPc1	podnik
znárodněny	znárodněn	k2eAgInPc1d1	znárodněn
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
a	a	k8xC	a
bouře	bouře	k1gFnSc1	bouře
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Kaundu	Kaunda	k1gFnSc4	Kaunda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
legalizoval	legalizovat	k5eAaBmAgMnS	legalizovat
opoziční	opoziční	k2eAgFnPc4d1	opoziční
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
svobodných	svobodný	k2eAgFnPc6d1	svobodná
pluralitních	pluralitní	k2eAgFnPc6d1	pluralitní
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
a	a	k8xC	a
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
představitel	představitel	k1gMnSc1	představitel
Hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
pluralitní	pluralitní	k2eAgFnSc4d1	pluralitní
demokracii	demokracie	k1gFnSc4	demokracie
Frederick	Frederick	k1gMnSc1	Frederick
Chiluba	Chiluba	k1gMnSc1	Chiluba
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
v	v	k7c6	v
zásobování	zásobování	k1gNnSc6	zásobování
potravinami	potravina	k1gFnPc7	potravina
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
přes	přes	k7c4	přes
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
hladomorům	hladomor	k1gInPc3	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
relativní	relativní	k2eAgInSc4d1	relativní
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
Levy	Levy	k?	Levy
Mwanawasa	Mwanawas	k1gMnSc2	Mwanawas
(	(	kIx(	(
<g/>
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
vládní	vládní	k2eAgFnSc1d1	vládní
strana	strana	k1gFnSc1	strana
i	i	k8xC	i
opozice	opozice	k1gFnSc1	opozice
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c4	na
vypsání	vypsání	k1gNnSc4	vypsání
nových	nový	k2eAgFnPc2d1	nová
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
říjen	říjen	k1gInSc4	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnPc4	geografie
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Zambie	Zambie	k1gFnSc1	Zambie
je	být	k5eAaImIp3nS	být
Lusaka	Lusak	k1gMnSc4	Lusak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
Kitwe	Kitwe	k1gFnSc1	Kitwe
<g/>
,	,	kIx,	,
Kabwe	Kabwe	k1gFnSc1	Kabwe
nebo	nebo	k8xC	nebo
Livingstone	Livingston	k1gInSc5	Livingston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Angolou	Angola	k1gFnSc7	Angola
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc7	Malawi
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gFnSc7	Zimbabwe
<g/>
,	,	kIx,	,
Mosambikem	Mosambik	k1gInSc7	Mosambik
<g/>
,	,	kIx,	,
Tanzanií	Tanzanie	k1gFnSc7	Tanzanie
a	a	k8xC	a
Namibií	Namibie	k1gFnSc7	Namibie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
lehce	lehko	k6eAd1	lehko
zvlněnou	zvlněný	k2eAgFnSc7d1	zvlněná
náhorní	náhorní	k2eAgFnSc7d1	náhorní
plošinou	plošina	k1gFnSc7	plošina
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
900	[number]	k4	900
–	–	k?	–
1	[number]	k4	1
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
prolomena	prolomit	k5eAaPmNgFnS	prolomit
riftovým	riftův	k2eAgNnSc7d1	riftův
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Luangwa	Luangw	k1gInSc2	Luangw
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgNnSc1d3	nejnižší
místo	místo	k1gNnSc1	místo
země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mosambikem	Mosambik	k1gInSc7	Mosambik
(	(	kIx(	(
<g/>
329	[number]	k4	329
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Manfinga	Manfinga	k1gFnSc1	Manfinga
Hills	Hills	k1gInSc1	Hills
(	(	kIx(	(
<g/>
2	[number]	k4	2
164	[number]	k4	164
m	m	kA	m
n.	n.	k?	n.
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zambijská	zambijský	k2eAgFnSc1d1	zambijská
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
provincii	provincie	k1gFnSc6	provincie
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Isoka	Isoek	k1gInSc2	Isoek
<g/>
,	,	kIx,	,
malawijská	malawijský	k2eAgFnSc1d1	malawijská
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chitipa	Chitipa	k1gFnSc1	Chitipa
(	(	kIx(	(
<g/>
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
:	:	kIx,	:
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Zambie	Zambie	k1gFnSc2	Zambie
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
bažiny	bažina	k1gFnPc4	bažina
-	-	kIx~	-
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezer	jezero	k1gNnPc2	jezero
Mweru	Mwer	k1gInSc2	Mwer
a	a	k8xC	a
Bangweulu	Bangweul	k1gInSc2	Bangweul
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
území	území	k1gNnSc2	území
Zambie	Zambie	k1gFnSc2	Zambie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
několik	několik	k4yIc4	několik
velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
část	část	k1gFnSc1	část
jezera	jezero	k1gNnSc2	jezero
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
část	část	k1gFnSc1	část
jezera	jezero	k1gNnSc2	jezero
Mweru	Mwer	k1gInSc2	Mwer
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vodopádů	vodopád	k1gInPc2	vodopád
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
vodopády	vodopád	k1gInPc4	vodopád
Ngonye	Ngonye	k1gNnSc2	Ngonye
či	či	k8xC	či
známé	známý	k2eAgInPc4d1	známý
Viktoriiny	Viktoriin	k2eAgInPc4d1	Viktoriin
vodopády	vodopád	k1gInPc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
pokryv	pokryv	k1gInSc1	pokryv
tvoří	tvořit	k5eAaImIp3nS	tvořit
savany	savana	k1gFnPc4	savana
a	a	k8xC	a
řídké	řídký	k2eAgInPc4d1	řídký
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
roční	roční	k2eAgFnPc1d1	roční
období	období	k1gNnSc3	období
(	(	kIx(	(
<g/>
období	období	k1gNnSc3	období
dešťů	dešť	k1gInPc2	dešť
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
chladné	chladný	k2eAgNnSc1d1	chladné
suché	suchý	k2eAgNnSc1d1	suché
období	období	k1gNnSc1	období
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
horké	horký	k2eAgNnSc1d1	horké
suché	suchý	k2eAgNnSc1d1	suché
období	období	k1gNnSc1	období
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
16	[number]	k4	16
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
do	do	k7c2	do
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
údaje	údaj	k1gInPc1	údaj
měřeny	měřen	k2eAgInPc1d1	měřen
v	v	k7c6	v
Lusace	Lusace	k1gFnSc1	Lusace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
700	[number]	k4	700
mm	mm	kA	mm
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
do	do	k7c2	do
1	[number]	k4	1
300	[number]	k4	300
mm	mm	kA	mm
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
;	;	kIx,	;
nejméně	málo	k6eAd3	málo
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
400	[number]	k4	400
mm	mm	kA	mm
<g/>
,	,	kIx,	,
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Botswanou	Botswana	k1gFnSc7	Botswana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
typická	typický	k2eAgNnPc4d1	typické
zvířata	zvíře	k1gNnPc4	zvíře
Zambie	Zambie	k1gFnSc2	Zambie
patří	patřit	k5eAaImIp3nS	patřit
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
bradavičnaté	bradavičnatý	k2eAgInPc1d1	bradavičnatý
<g/>
,	,	kIx,	,
lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
,	,	kIx,	,
hyena	hyena	k1gFnSc1	hyena
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
nebo	nebo	k8xC	nebo
pes	pes	k1gMnSc1	pes
ušatý	ušatý	k2eAgMnSc1d1	ušatý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgFnPc2d1	žijící
rostlin	rostlina	k1gFnPc2	rostlina
zde	zde	k6eAd1	zde
rostou	růst	k5eAaImIp3nP	růst
například	například	k6eAd1	například
baobaby	baobab	k1gInPc4	baobab
<g/>
,	,	kIx,	,
opuncie	opuncie	k1gFnPc4	opuncie
nebo	nebo	k8xC	nebo
pryšce	pryšec	k1gInPc4	pryšec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
světově	světově	k6eAd1	světově
významná	významný	k2eAgNnPc4d1	významné
ložiska	ložisko	k1gNnPc4	ložisko
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
Zambia	Zambia	k1gFnSc1	Zambia
–	–	k?	–
One	One	k1gFnSc1	One
Nation	Nation	k1gInSc1	Nation
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
Zambie	Zambie	k1gFnSc1	Zambie
–	–	k?	–
jeden	jeden	k4xCgInSc1	jeden
národ	národ	k1gInSc1	národ
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
spojit	spojit	k5eAaPmF	spojit
multikulturní	multikulturní	k2eAgFnSc4d1	multikulturní
zambijskou	zambijský	k2eAgFnSc4d1	zambijská
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hovoří	hovořit	k5eAaImIp3nS	hovořit
přes	přes	k7c4	přes
70	[number]	k4	70
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnSc1	obyvatel
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
13	[number]	k4	13
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
domorodými	domorodý	k2eAgInPc7d1	domorodý
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
bemba	bemba	k1gMnSc1	bemba
(	(	kIx(	(
<g/>
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyanja	nyanja	k1gMnSc1	nyanja
(	(	kIx(	(
<g/>
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tonga	tonga	k1gFnSc1	tonga
(	(	kIx(	(
<g/>
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
však	však	k9	však
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Míšením	míšení	k1gNnSc7	míšení
domorodých	domorodý	k2eAgInPc2d1	domorodý
jazyků	jazyk	k1gInPc2	jazyk
s	s	k7c7	s
angličtinou	angličtina	k1gFnSc7	angličtina
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgInPc1d1	nový
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hornický	hornický	k2eAgInSc4d1	hornický
jazyk	jazyk	k1gInSc4	jazyk
Copperbeltu	Copperbelt	k1gInSc2	Copperbelt
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
79	[number]	k4	79
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Porodnost	porodnost	k1gFnSc1	porodnost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
4,1	[number]	k4	4,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
2,1	[number]	k4	2,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
emigrace	emigrace	k1gFnSc1	emigrace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
0,016	[number]	k4	0,016
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
1,9	[number]	k4	1,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
8,9	[number]	k4	8,9
%	%	kIx~	%
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc6	jeden
ženě	žena	k1gFnSc6	žena
se	se	k3xPyFc4	se
za	za	k7c4	za
život	život	k1gInSc4	život
průměrně	průměrně	k6eAd1	průměrně
narodí	narodit	k5eAaPmIp3nS	narodit
5,4	[number]	k4	5,4
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
dožití	dožití	k1gNnSc4	dožití
je	být	k5eAaImIp3nS	být
37,4	[number]	k4	37,4
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
37,1	[number]	k4	37,1
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
37,7	[number]	k4	37,7
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
98,7	[number]	k4	98,7
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
negroidní	negroidní	k2eAgFnSc2d1	negroidní
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
1,1	[number]	k4	1,1
%	%	kIx~	%
europoidní	europoidní	k2eAgFnSc2d1	europoidní
a	a	k8xC	a
0,2	[number]	k4	0,2
%	%	kIx~	%
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náboženství	náboženství	k1gNnSc2	náboženství
převažuje	převažovat	k5eAaImIp3nS	převažovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
je	být	k5eAaImIp3nS	být
také	také	k9	také
islám	islám	k1gInSc1	islám
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
;	;	kIx,	;
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
animisté	animista	k1gMnPc1	animista
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
k	k	k7c3	k
animismu	animismus	k1gInSc3	animismus
hlásí	hlásit	k5eAaImIp3nS	hlásit
i	i	k9	i
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecně	obecně	k6eAd1	obecně
===	===	k?	===
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc4d1	typický
rozvojový	rozvojový	k2eAgInSc4d1	rozvojový
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
přepočítaný	přepočítaný	k2eAgMnSc1d1	přepočítaný
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
Zambie	Zambie	k1gFnPc1	Zambie
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
8,5	[number]	k4	8,5
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
byl	být	k5eAaImAgInS	být
tedy	tedy	k8xC	tedy
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
870	[number]	k4	870
USD	USD	kA	USD
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
činil	činit	k5eAaImAgInS	činit
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
ceny	cena	k1gFnSc2	cena
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
primér	primér	k1gInSc1	primér
–	–	k?	–
24	[number]	k4	24
%	%	kIx~	%
</s>
</p>
<p>
<s>
sekundér	sekundér	k1gInSc1	sekundér
–	–	k?	–
25	[number]	k4	25
%	%	kIx~	%
</s>
</p>
<p>
<s>
terciér	terciér	k1gInSc1	terciér
–	–	k?	–
51	[number]	k4	51
%	%	kIx~	%
<g/>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
primér	primér	k1gInSc1	primér
–	–	k?	–
85	[number]	k4	85
%	%	kIx~	%
</s>
</p>
<p>
<s>
sekundér	sekundér	k1gInSc1	sekundér
–	–	k?	–
6	[number]	k4	6
%	%	kIx~	%
</s>
</p>
<p>
<s>
terciér-	terciér-	k?	terciér-
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
zambijskou	zambijský	k2eAgFnSc7d1	zambijská
kwachou	kwacha	k1gFnSc7	kwacha
(	(	kIx(	(
<g/>
=	=	kIx~	=
100	[number]	k4	100
ngwee	ngwee	k1gNnPc2	ngwee
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
inflace	inflace	k1gFnSc1	inflace
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
21,5	[number]	k4	21,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Směnný	směnný	k2eAgInSc1d1	směnný
kurs	kurs	k1gInSc1	kurs
zambijské	zambijský	k2eAgFnSc2d1	zambijská
kwachy	kwacha	k1gFnSc2	kwacha
vůči	vůči	k7c3	vůči
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
USD	USD	kA	USD
=	=	kIx~	=
3	[number]	k4	3
848	[number]	k4	848
ZMK	ZMK	kA	ZMK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Zambijský	zambijský	k2eAgInSc1d1	zambijský
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
především	především	k9	především
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
Zambie	Zambie	k1gFnSc2	Zambie
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
měděném	měděný	k2eAgInSc6d1	měděný
pásu	pás	k1gInSc6	pás
(	(	kIx(	(
<g/>
Copperbelt	Copperbelt	k1gMnSc1	Copperbelt
<g/>
)	)	kIx)	)
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
měděného	měděný	k2eAgInSc2d1	měděný
pásu	pás	k1gInSc2	pás
je	být	k5eAaImIp3nS	být
soustředěn	soustředěn	k2eAgInSc1d1	soustředěn
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
prezidentem	prezident	k1gMnSc7	prezident
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
následovat	následovat	k5eAaImF	následovat
geologický	geologický	k2eAgInSc1d1	geologický
průzkum	průzkum	k1gInSc1	průzkum
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Růst	růst	k1gInSc1	růst
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
činí	činit	k5eAaImIp3nS	činit
5,1	[number]	k4	5,1
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
99	[number]	k4	99
%	%	kIx~	%
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
vyvážena	vyvážet	k5eAaImNgFnS	vyvážet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
extenzivního	extenzivní	k2eAgInSc2d1	extenzivní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
na	na	k7c6	na
nízkém	nízký	k2eAgInSc6d1	nízký
stupni	stupeň	k1gInSc6	stupeň
modernizace	modernizace	k1gFnSc2	modernizace
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
samozásobitelské	samozásobitelský	k2eAgFnSc2d1	samozásobitelská
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
jihoafrickém	jihoafrický	k2eAgInSc6d1	jihoafrický
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
využívány	využívat	k5eAaPmNgInP	využívat
k	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
–	–	k?	–
pouze	pouze	k6eAd1	pouze
460	[number]	k4	460
km	km	kA	km
<g/>
2	[number]	k4	2
půdy	půda	k1gFnSc2	půda
je	on	k3xPp3gNnSc4	on
zavlažováno	zavlažován	k2eAgNnSc4d1	zavlažováno
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc4	údaj
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
produkuje	produkovat	k5eAaImIp3nS	produkovat
málo	málo	k6eAd1	málo
významné	významný	k2eAgNnSc1d1	významné
množství	množství	k1gNnSc1	množství
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
zaostalá	zaostalý	k2eAgFnSc1d1	zaostalá
–	–	k?	–
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
nevýznamné	významný	k2eNgNnSc1d1	nevýznamné
množství	množství	k1gNnSc1	množství
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
67	[number]	k4	67
000	[number]	k4	000
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
prašných	prašný	k2eAgMnPc2d1	prašný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
km	km	kA	km
úzkorozchodné	úzkorozchodný	k2eAgFnSc2d1	úzkorozchodná
(	(	kIx(	(
<g/>
1	[number]	k4	1
067	[number]	k4	067
mm	mm	kA	mm
<g/>
)	)	kIx)	)
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
km	km	kA	km
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
1	[number]	k4	1
700	[number]	k4	700
km	km	kA	km
ropovodů	ropovod	k1gInPc2	ropovod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Zambie	Zambie	k1gFnSc2	Zambie
je	být	k5eAaImIp3nS	být
111	[number]	k4	111
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
se	s	k7c7	s
zpevněnou	zpevněný	k2eAgFnSc7d1	zpevněná
dráhou	dráha	k1gFnSc7	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
zambijské	zambijský	k2eAgNnSc1d1	zambijské
letiště	letiště	k1gNnSc1	letiště
má	mít	k5eAaImIp3nS	mít
dráhu	dráha	k1gFnSc4	dráha
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
3	[number]	k4	3
km	km	kA	km
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
3	[number]	k4	3
letiště	letiště	k1gNnSc2	letiště
mají	mít	k5eAaImIp3nP	mít
dráhu	dráha	k1gFnSc4	dráha
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
mezi	mezi	k7c4	mezi
2,5	[number]	k4	2,5
–	–	k?	–
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
se	se	k3xPyFc4	se
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vývoz	vývoz	k1gInSc1	vývoz
====	====	k?	====
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
exportérem	exportér	k1gMnSc7	exportér
mědi	měď	k1gFnSc2	měď
(	(	kIx(	(
<g/>
55	[number]	k4	55
%	%	kIx~	%
zambijského	zambijský	k2eAgInSc2d1	zambijský
vývozu	vývoz	k1gInSc2	vývoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
kobalt	kobalt	k1gInSc4	kobalt
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc4	tabák
<g/>
,	,	kIx,	,
bavlnu	bavlna	k1gFnSc4	bavlna
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
směřuje	směřovat	k5eAaImIp3nS	směřovat
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
25,2	[number]	k4	25,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
24,5	[number]	k4	24,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
9,4	[number]	k4	9,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Malawi	Malawi	k1gNnSc4	Malawi
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
objem	objem	k1gInSc1	objem
vývozu	vývoz	k1gInSc2	vývoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
činil	činit	k5eAaImAgInS	činit
876	[number]	k4	876
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dovoz	dovoz	k1gInSc1	dovoz
====	====	k?	====
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnPc1	Zambie
dováží	dovážet	k5eAaImIp3nP	dovážet
hlavně	hlavně	k9	hlavně
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgInPc1d1	ropný
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc1	potravina
a	a	k8xC	a
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
67,1	[number]	k4	67,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
9,8	[number]	k4	9,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
činil	činit	k5eAaImAgInS	činit
objem	objem	k1gInSc1	objem
dovozu	dovoz	k1gInSc2	dovoz
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turismus	turismus	k1gInSc4	turismus
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vodopády	vodopád	k1gInPc4	vodopád
====	====	k?	====
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
zambijské	zambijský	k2eAgFnPc4d1	zambijská
zajímavosti	zajímavost	k1gFnPc4	zajímavost
patří	patřit	k5eAaImIp3nP	patřit
známé	známý	k2eAgInPc1d1	známý
Viktoriiny	Viktoriin	k2eAgInPc1d1	Viktoriin
vodopády	vodopád	k1gInPc1	vodopád
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Livingstone	Livingston	k1gInSc5	Livingston
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
Zimbabwe	Zimbabwe	k1gFnSc7	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Vodopády	vodopád	k1gInPc1	vodopád
jsou	být	k5eAaImIp3nP	být
1	[number]	k4	1
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
necelých	celý	k2eNgInPc2d1	necelý
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
hluboké	hluboký	k2eAgNnSc1d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
ze	z	k7c2	z
zambijské	zambijský	k2eAgFnSc2d1	zambijská
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
USD	USD	kA	USD
(	(	kIx(	(
<g/>
ze	z	k7c2	z
Zimbabwské	zimbabwský	k2eAgFnSc2d1	Zimbabwská
20	[number]	k4	20
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viktoriiny	Viktoriin	k2eAgInPc1d1	Viktoriin
vodopády	vodopád	k1gInPc1	vodopád
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
svět	svět	k1gInSc4	svět
objevil	objevit	k5eAaPmAgMnS	objevit
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1855	[number]	k4	1855
David	David	k1gMnSc1	David
Livingstone	Livingston	k1gInSc5	Livingston
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
britské	britský	k2eAgFnSc2d1	britská
panovnice	panovnice	k1gFnSc2	panovnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domorodém	domorodý	k2eAgInSc6d1	domorodý
jazyku	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
vodopády	vodopád	k1gInPc7	vodopád
nazývají	nazývat	k5eAaImIp3nP	nazývat
Mosi-oa-tunya	Mosiaunya	k1gMnSc1	Mosi-oa-tunya
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Hromový	hromový	k2eAgInSc1d1	hromový
kouř	kouř	k1gInSc1	kouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Viktoriiných	Viktoriin	k2eAgInPc2d1	Viktoriin
vodopádů	vodopád	k1gInPc2	vodopád
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgMnPc7d1	významný
ještě	ještě	k6eAd1	ještě
vodopády	vodopád	k1gInPc7	vodopád
Ngonye	Ngony	k1gFnSc2	Ngony
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Příroda	příroda	k1gFnSc1	příroda
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
například	například	k6eAd1	například
South	South	k1gInSc4	South
Luangwa	Luangwa	k1gFnSc1	Luangwa
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpočetnějších	početní	k2eAgFnPc2d3	nejpočetnější
populací	populace	k1gFnPc2	populace
slonů	slon	k1gMnPc2	slon
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
parky	park	k1gInPc1	park
Kafue	Kafu	k1gFnSc2	Kafu
a	a	k8xC	a
Sioma	Siomum	k1gNnSc2	Siomum
Ngwez	Ngweza	k1gFnPc2	Ngweza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vůbec	vůbec	k9	vůbec
–	–	k?	–
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
divoké	divoký	k2eAgFnSc6d1	divoká
přírodě	příroda	k1gFnSc6	příroda
lze	lze	k6eAd1	lze
obdivovat	obdivovat	k5eAaImF	obdivovat
lvy	lev	k1gMnPc4	lev
<g/>
,	,	kIx,	,
hrochy	hroch	k1gMnPc4	hroch
<g/>
,	,	kIx,	,
buvoly	buvol	k1gMnPc4	buvol
<g/>
,	,	kIx,	,
zebry	zebra	k1gFnSc2	zebra
a	a	k8xC	a
žirafy	žirafa	k1gFnSc2	žirafa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
území	území	k1gNnSc2	území
Zambie	Zambie	k1gFnSc2	Zambie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
část	část	k1gFnSc1	část
jezera	jezero	k1gNnSc2	jezero
Tanganika	Tanganika	k1gFnSc1	Tanganika
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Víza	vízo	k1gNnSc2	vízo
====	====	k?	====
</s>
</p>
<p>
<s>
Zambijské	zambijský	k2eAgNnSc1d1	zambijské
vízum	vízum	k1gNnSc1	vízum
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
kterémkoliv	kterýkoliv	k3yIgInSc6	kterýkoliv
zastupitelském	zastupitelský	k2eAgInSc6d1	zastupitelský
úřadě	úřad	k1gInSc6	úřad
Zambie	Zambie	k1gFnSc2	Zambie
či	či	k8xC	či
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
kterémkoliv	kterýkoliv	k3yIgInSc6	kterýkoliv
hraničním	hraniční	k2eAgInSc6d1	hraniční
přechodu	přechod	k1gInSc6	přechod
do	do	k7c2	do
Zambie	Zambie	k1gFnSc2	Zambie
<g/>
.	.	kIx.	.
</s>
<s>
Poplatek	poplatek	k1gInSc1	poplatek
za	za	k7c4	za
udělení	udělení	k1gNnSc4	udělení
tranzitního	tranzitní	k2eAgNnSc2d1	tranzitní
víza	vízo	k1gNnSc2	vízo
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
USD	USD	kA	USD
<g/>
,	,	kIx,	,
20	[number]	k4	20
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
<g/>
,	,	kIx,	,
30	[number]	k4	30
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
vstupy	vstup	k1gInPc4	vstup
a	a	k8xC	a
40	[number]	k4	40
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vstupů	vstup	k1gInPc2	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Víza	vízo	k1gNnPc4	vízo
lze	lze	k6eAd1	lze
udělit	udělit	k5eAaPmF	udělit
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
90	[number]	k4	90
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
platný	platný	k2eAgInSc1d1	platný
alespoň	alespoň	k9	alespoň
90	[number]	k4	90
dnů	den	k1gInPc2	den
od	od	k7c2	od
data	datum	k1gNnSc2	datum
vydání	vydání	k1gNnSc2	vydání
víza	vízo	k1gNnSc2	vízo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
o	o	k7c4	o
zambijské	zambijský	k2eAgNnSc4d1	zambijské
vízum	vízum	k1gNnSc4	vízum
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyplnit	vyplnit	k5eAaPmF	vyplnit
formulář	formulář	k1gInSc4	formulář
a	a	k8xC	a
přiložit	přiložit	k5eAaPmF	přiložit
dvě	dva	k4xCgFnPc4	dva
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc4	problém
==	==	k?	==
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
je	být	k5eAaImIp3nS	být
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Obrovským	obrovský	k2eAgInSc7d1	obrovský
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
AIDS	aids	k1gInSc1	aids
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
téměř	téměř	k6eAd1	téměř
14	[number]	k4	14
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
nakaženo	nakazit	k5eAaPmNgNnS	nakazit
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
počet	počet	k1gInSc1	počet
sirotků	sirotek	k1gMnPc2	sirotek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ztratili	ztratit	k5eAaPmAgMnP	ztratit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
již	již	k6eAd1	již
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
milionu	milion	k4xCgInSc3	milion
<g/>
.	.	kIx.	.
</s>
<s>
Průměrnou	průměrný	k2eAgFnSc7d1	průměrná
délkou	délka	k1gFnSc7	délka
života	život	k1gInSc2	život
37,35	[number]	k4	37,35
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Zambie	Zambie	k1gFnSc1	Zambie
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
příčku	příčka	k1gFnSc4	příčka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
příliš	příliš	k6eAd1	příliš
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
také	také	k9	také
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
kriminalita	kriminalita	k1gFnSc1	kriminalita
zde	zde	k6eAd1	zde
nehraje	hrát	k5eNaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nemá	mít	k5eNaImIp3nS	mít
Zambie	Zambie	k1gFnSc1	Zambie
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
žádné	žádný	k3yNgInPc1	žádný
konflikty	konflikt	k1gInPc1	konflikt
a	a	k8xC	a
také	také	k9	také
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Dárci	dárce	k1gMnPc1	dárce
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
pomoci	pomoc	k1gFnSc2	pomoc
vesměs	vesměs	k6eAd1	vesměs
velice	velice	k6eAd1	velice
kladně	kladně	k6eAd1	kladně
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
proinvestiční	proinvestiční	k2eAgFnSc4d1	proinvestiční
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
je	být	k5eAaImIp3nS	být
i	i	k9	i
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zemi	zem	k1gFnSc4	zem
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
priority	priorita	k1gFnPc4	priorita
své	svůj	k3xOyFgFnSc2	svůj
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
sektor	sektor	k1gInSc4	sektor
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
10	[number]	k4	10
provincií	provincie	k1gFnPc2	provincie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
Copperbelt	Copperbelta	k1gFnPc2	Copperbelta
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
Luapula	Luapula	k1gFnSc1	Luapula
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
Lusaka	Lusak	k1gMnSc2	Lusak
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnPc1d1	západní
provincie	provincie	k1gFnPc1	provincie
</s>
</p>
<p>
<s>
Muchinga	Muchinga	k1gFnSc1	Muchinga
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
–	–	k?	–
Kenneth	Kenneth	k1gMnSc1	Kenneth
David	David	k1gMnSc1	David
Kaunda	Kaund	k1gMnSc2	Kaund
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
ZANC	ZANC	kA	ZANC
<g/>
,	,	kIx,	,
UNIP	UNIP	kA	UNIP
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
–	–	k?	–
Frederick	Frederick	k1gMnSc1	Frederick
Jacob	Jacoba	k1gFnPc2	Jacoba
Titus	Titus	k1gMnSc1	Titus
Chiluba	Chiluba	k1gMnSc1	Chiluba
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
MMD	MMD	kA	MMD
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
Levy	Levy	k?	Levy
Patrick	Patrick	k1gMnSc1	Patrick
Mwanawasa	Mwanawas	k1gMnSc2	Mwanawas
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
MMD	MMD	kA	MMD
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
Rupiah	Rupiah	k1gMnSc1	Rupiah
Bwezani	Bwezaň	k1gFnSc3	Bwezaň
Banda	bando	k1gNnSc2	bando
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
pro	pro	k7c4	pro
L.	L.	kA	L.
P.	P.	kA	P.
Mwanawasu	Mwanawas	k1gInSc3	Mwanawas
<g/>
;	;	kIx,	;
UNIP	UNIP	kA	UNIP
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
Rupiah	Rupiah	k1gMnSc1	Rupiah
Bwezani	Bwezaň	k1gFnSc3	Bwezaň
Banda	bando	k1gNnSc2	bando
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UNIP	UNIP	kA	UNIP
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
-	-	kIx~	-
Rupiah	Rupiah	k1gInSc1	Rupiah
Bwezani	Bwezaň	k1gFnSc3	Bwezaň
Banda	bando	k1gNnSc2	bando
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UNIP	UNIP	kA	UNIP
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Sata	Sata	k1gMnSc1	Sata
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PF	PF	kA	PF
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
-	-	kIx~	-
Guy	Guy	k1gMnSc1	Guy
Scott	Scott	k1gMnSc1	Scott
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PF	PF	kA	PF
</s>
</p>
<p>
<s>
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
-	-	kIx~	-
Edgar	Edgar	k1gMnSc1	Edgar
Lungu	Lung	k1gMnSc3	Lung
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PF	PF	kA	PF
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
OLŠA	OLŠA	kA	OLŠA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
HULEC	HULEC	kA	HULEC
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnSc2	Zambie
a	a	k8xC	a
Malawi	Malawi	k1gNnSc2	Malawi
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
952	[number]	k4	952
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zambie	Zambie	k1gFnSc2	Zambie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Zambie	Zambie	k1gFnSc2	Zambie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Zambie	Zambie	k1gFnSc2	Zambie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Togo	Togo	k1gNnSc1	Togo
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Cestopis	cestopis	k1gInSc1	cestopis
na	na	k7c6	na
webu	web	k1gInSc6	web
Cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ze	z	k7c2	z
Zambie	Zambie	k1gFnSc2	Zambie
</s>
</p>
<p>
<s>
Výprava	výprava	k1gFnSc1	výprava
za	za	k7c7	za
zatměním	zatmění	k1gNnSc7	zatmění
slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Bedekr	bedekr	k1gInSc1	bedekr
-	-	kIx~	-
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Idnes	Idnes	k1gInSc1	Idnes
-	-	kIx~	-
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
Zambii	Zambie	k1gFnSc4	Zambie
</s>
</p>
<p>
<s>
Zambia	Zambia	k1gFnSc1	Zambia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Zambia	Zambius	k1gMnSc2	Zambius
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Zambia	Zambia	k1gFnSc1	Zambia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-11	[number]	k4	2011-04-11
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Zambia	Zambia	k1gFnSc1	Zambia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-04-11	[number]	k4	2011-04-11
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Harare	Harar	k1gMnSc5	Harar
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Zambie	Zambie	k1gFnSc1	Zambie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-06-02	[number]	k4	2011-06-02
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOBSON	HOBSON	kA	HOBSON
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zambia	Zambia	k1gFnSc1	Zambia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
