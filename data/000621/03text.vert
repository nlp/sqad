<s>
Vok	Vok	k?	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
byl	být	k5eAaImAgMnS	být
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
literatura	literatura	k1gFnSc1	literatura
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
praotcem	praotec	k1gMnSc7	praotec
rodu	rod	k1gInSc2	rod
Kravařů	kravař	k1gMnPc2	kravař
je	být	k5eAaImIp3nS	být
Drslav	Drslav	k1gFnSc4	Drslav
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Benešovců	Benešovec	k1gMnPc2	Benešovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Voka	Vokus	k1gMnSc4	Vokus
<g/>
.	.	kIx.	.
</s>
<s>
Novodobí	novodobý	k2eAgMnPc1d1	novodobý
autoři	autor	k1gMnPc1	autor
historických	historický	k2eAgFnPc2d1	historická
publikací	publikace	k1gFnPc2	publikace
však	však	k9	však
píší	psát	k5eAaImIp3nP	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vok	Vok	k1gMnSc1	Vok
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Drslavův	Drslavův	k2eAgMnSc1d1	Drslavův
a	a	k8xC	a
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Benešova	Benešov	k1gInSc2	Benešov
a	a	k8xC	a
Kravař	Kravaře	k1gInPc2	Kravaře
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
též	též	k9	též
mylně	mylně	k6eAd1	mylně
slučuje	slučovat	k5eAaImIp3nS	slučovat
Voka	Voka	k1gFnSc1	Voka
z	z	k7c2	z
Benešova	Benešov	k1gInSc2	Benešov
a	a	k8xC	a
Kravař	kravař	k1gMnSc1	kravař
a	a	k8xC	a
Vok	Vok	k1gMnSc1	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
historickou	historický	k2eAgFnSc4d1	historická
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
i	i	k8xC	i
noví	nový	k2eAgMnPc1d1	nový
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
však	však	k9	však
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
otce	otec	k1gMnSc2	otec
Voka	Vokum	k1gNnSc2	Vokum
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
nelze	lze	k6eNd1	lze
bezpečně	bezpečně	k6eAd1	bezpečně
historicky	historicky	k6eAd1	historicky
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rodiče	rodič	k1gMnSc4	rodič
tohoto	tento	k3xDgMnSc4	tento
šlechtice	šlechtic	k1gMnSc4	šlechtic
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
že	že	k8xS	že
otcem	otec	k1gMnSc7	otec
Voka	Vokum	k1gNnSc2	Vokum
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
(	(	kIx(	(
<g/>
1300	[number]	k4	1300
<g/>
-	-	kIx~	-
<g/>
1329	[number]	k4	1329
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Vok	Vok	k1gMnPc4	Vok
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Benešova	Benešov	k1gInSc2	Benešov
(	(	kIx(	(
<g/>
1274	[number]	k4	1274
<g/>
-	-	kIx~	-
<g/>
1283	[number]	k4	1283
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Benešovců	Benešovec	k1gInPc2	Benešovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
Vok	Vok	k1gFnSc1	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
prvním	první	k4xOgMnSc6	první
historicky	historicky	k6eAd1	historicky
doložitelným	doložitelný	k2eAgInSc7d1	doložitelný
členem	člen	k1gInSc7	člen
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1300	[number]	k4	1300
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
krakovsko-sandoměřský	krakovskoandoměřský	k2eAgMnSc1d1	krakovsko-sandoměřský
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1303	[number]	k4	1303
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
královské	královský	k2eAgFnSc6d1	královská
listině	listina	k1gFnSc6	listina
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
místy	místo	k1gNnPc7	místo
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Kravař	kravař	k1gMnSc1	kravař
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Ekkonem	Ekkon	k1gMnSc7	Ekkon
<g/>
,	,	kIx,	,
mistrem	mistr	k1gMnSc7	mistr
templářského	templářský	k2eAgInSc2d1	templářský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
pronájem	pronájem	k1gInSc4	pronájem
městečka	městečko	k1gNnSc2	městečko
Vsetín	Vsetín	k1gInSc1	Vsetín
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
Freundsbergem	Freundsberg	k1gInSc7	Freundsberg
na	na	k7c4	na
31	[number]	k4	31
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vok	Vok	k1gFnSc7	Vok
u	u	k7c2	u
Kravař	Kravaře	k1gInPc2	Kravaře
jeho	jeho	k3xOp3gMnPc7	jeho
straníkem	straník	k1gMnSc7	straník
a	a	k8xC	a
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
mu	on	k3xPp3gMnSc3	on
svoje	své	k1gNnSc1	své
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1312	[number]	k4	1312
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
odbojným	odbojný	k2eAgMnPc3d1	odbojný
šlechticům	šlechtic	k1gMnPc3	šlechtic
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
konfiskován	konfiskován	k2eAgInSc4d1	konfiskován
majetek	majetek	k1gInSc4	majetek
Friduše	Friduše	k1gFnSc2	Friduše
z	z	k7c2	z
Linavy	Linava	k1gFnSc2	Linava
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
který	který	k3yIgInSc4	který
patřil	patřit	k5eAaImAgInS	patřit
hrad	hrad	k1gInSc1	hrad
Plumlov	Plumlovo	k1gNnPc2	Plumlovo
<g/>
,	,	kIx,	,
Drahuš	Drahuš	k1gMnSc1	Drahuš
a	a	k8xC	a
Helfštejn	Helfštejn	k1gMnSc1	Helfštejn
a	a	k8xC	a
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
náležející	náležející	k2eAgNnSc1d1	náležející
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
majetek	majetek	k1gInSc1	majetek
získal	získat	k5eAaPmAgInS	získat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1325	[number]	k4	1325
právě	právě	k6eAd1	právě
Vok	Vok	k1gMnPc1	Vok
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Vok	Vok	k?	Vok
získal	získat	k5eAaPmAgInS	získat
též	též	k9	též
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
majetek	majetek	k1gInSc1	majetek
kolem	kolem	k7c2	kolem
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1316	[number]	k4	1316
je	být	k5eAaImIp3nS	být
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Kravař	kravař	k1gMnSc1	kravař
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
podkomoří	podkomoří	k1gMnSc1	podkomoří
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
zásluhy	zásluha	k1gFnSc2	zásluha
také	také	k9	také
získal	získat	k5eAaPmAgMnS	získat
město	město	k1gNnSc4	město
Fulnek	Fulnek	k1gInSc1	Fulnek
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
koupil	koupit	k5eAaPmAgMnS	koupit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
dům	dům	k1gInSc4	dům
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1324	[number]	k4	1324
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komorník	komorník	k1gMnSc1	komorník
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
cúdy	cúda	k1gFnSc2	cúda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Vokovi	Voka	k1gMnSc6	Voka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1327	[number]	k4	1327
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc1	období
zřejmě	zřejmě	k6eAd1	zřejmě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
Vokova	Vokův	k2eAgInSc2d1	Vokův
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jako	jako	k9	jako
nebožtík	nebožtík	k1gMnSc1	nebožtík
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1329	[number]	k4	1329
<g/>
.	.	kIx.	.
</s>
<s>
Vok	Vok	k?	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
byl	být	k5eAaImAgInS	být
minimálně	minimálně	k6eAd1	minimálně
dvakrát	dvakrát	k6eAd1	dvakrát
ženat	ženat	k2eAgMnSc1d1	ženat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
manželkách	manželka	k1gFnPc6	manželka
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
žádné	žádný	k3yNgFnPc1	žádný
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
byl	být	k5eAaImAgInS	být
Vok	Vok	k1gMnSc7	Vok
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
plumlovsko-strážnické	plumlovskotrážnický	k2eAgFnSc2d1	plumlovsko-strážnický
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
Beneš	Beneš	k1gMnSc1	Beneš
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
duchovního	duchovní	k1gMnSc2	duchovní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1337	[number]	k4	1337
jako	jako	k8xS	jako
kanovník	kanovník	k1gMnSc1	kanovník
pražský	pražský	k2eAgMnSc1d1	pražský
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1355	[number]	k4	1355
jako	jako	k8xC	jako
kanovník	kanovník	k1gMnSc1	kanovník
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
a	a	k8xC	a
boleslavský	boleslavský	k2eAgMnSc1d1	boleslavský
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
Ješek	Ješek	k6eAd1	Ješek
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komorník	komorník	k1gMnSc1	komorník
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
cúdy	cúda	k1gFnSc2	cúda
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1329	[number]	k4	1329
<g/>
-	-	kIx~	-
<g/>
1368	[number]	k4	1368
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladším	mladý	k2eAgMnSc7d3	nejmladší
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
Drslav	Drslav	k1gMnSc4	Drslav
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
fulnecko-jičínské	fulneckoičínský	k2eAgFnSc2d1	fulnecko-jičínský
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
BALETKA	baletka	k1gFnSc1	baletka
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
až	až	k9	až
na	na	k7c4	na
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
488	[number]	k4	488
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
682	[number]	k4	682
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
