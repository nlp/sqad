<s>
Vlak	vlak	k1gInSc1	vlak
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc1	několik
pevně	pevně	k6eAd1	pevně
spojených	spojený	k2eAgNnPc2d1	spojené
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc3d1	jiná
pevné	pevný	k2eAgFnSc3d1	pevná
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
