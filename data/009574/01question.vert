<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
vozidlu	vozidlo	k1gNnSc3	vozidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
<g/>
?	?	kIx.	?
</s>
