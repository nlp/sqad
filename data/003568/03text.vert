<s>
International	Internationat	k5eAaImAgInS	Internationat
Business	business	k1gInSc1	business
Machines	Machines	k1gMnSc1	Machines
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
IBM	IBM	kA	IBM
<g/>
)	)	kIx)	)
–	–	k?	–
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Big	Big	k1gFnSc1	Big
Blue	Blue	k1gFnSc1	Blue
neboli	neboli	k8xC	neboli
Velká	velká	k1gFnSc1	velká
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc1d1	fungující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
–	–	k?	–
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
světová	světový	k2eAgFnSc1d1	světová
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
činnosti	činnost	k1gFnPc4	činnost
společnosti	společnost	k1gFnSc2	společnost
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
počítačového	počítačový	k2eAgInSc2d1	počítačový
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
hardwaru	hardware	k1gInSc2	hardware
a	a	k8xC	a
desítky	desítka	k1gFnSc2	desítka
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
IBM	IBM	kA	IBM
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c4	v
Armonk	Armonk	k1gInSc4	Armonk
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vykázala	vykázat	k5eAaPmAgFnS	vykázat
zisk	zisk	k1gInSc1	zisk
41,7	[number]	k4	41,7
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
výnosů	výnos	k1gInPc2	výnos
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
98,8	[number]	k4	98,8
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
120,4	[number]	k4	120,4
mld	mld	k?	mld
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
355	[number]	k4	355
766	[number]	k4	766
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
společnosti	společnost	k1gFnPc4	společnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
akcie	akcie	k1gFnPc1	akcie
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
obchodování	obchodování	k1gNnSc3	obchodování
na	na	k7c6	na
NYSE	NYSE	kA	NYSE
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
<g/>
:	:	kIx,	:
IBM	IBM	kA	IBM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
byly	být	k5eAaImAgFnP	být
firmou	firma	k1gFnSc7	firma
IBM	IBM	kA	IBM
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
(	(	kIx(	(
<g/>
PC	PC	kA	PC
–	–	k?	–
Personal	Personal	k1gFnSc1	Personal
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
firem	firma	k1gFnPc2	firma
Commodore	Commodor	k1gMnSc5	Commodor
<g/>
,	,	kIx,	,
Atari	Atar	k1gMnSc5	Atar
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
staly	stát	k5eAaPmAgInP	stát
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
typem	typ	k1gInSc7	typ
mikropočítače	mikropočítač	k1gInSc2	mikropočítač
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
zcela	zcela	k6eAd1	zcela
volně	volně	k6eAd1	volně
technickou	technický	k2eAgFnSc4d1	technická
dokumentaci	dokumentace	k1gFnSc4	dokumentace
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozvoji	rozvoj	k1gInSc3	rozvoj
výroby	výroba	k1gFnSc2	výroba
laciných	laciný	k2eAgInPc2d1	laciný
klonů	klon	k1gInPc2	klon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byly	být	k5eAaImAgInP	být
identicky	identicky	k6eAd1	identicky
funkční	funkční	k2eAgInPc1d1	funkční
počítače	počítač	k1gInPc1	počítač
jiných	jiný	k2eAgMnPc2d1	jiný
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
chtěla	chtít	k5eAaImAgFnS	chtít
licencovat	licencovat	k5eAaBmF	licencovat
BIOS	BIOS	kA	BIOS
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
záhy	záhy	k6eAd1	záhy
ztratila	ztratit	k5eAaPmAgFnS	ztratit
díky	díky	k7c3	díky
legálně	legálně	k6eAd1	legálně
provedenému	provedený	k2eAgNnSc3d1	provedené
reverznímu	reverzní	k2eAgNnSc3d1	reverzní
inženýrství	inženýrství	k1gNnSc3	inženýrství
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
do	do	k7c2	do
PC	PC	kA	PC
umístěn	umístěn	k2eAgInSc1d1	umístěn
BIOS	BIOS	kA	BIOS
levnějších	levný	k2eAgMnPc2d2	levnější
výrobců	výrobce	k1gMnPc2	výrobce
beze	beze	k7c2	beze
ztráty	ztráta	k1gFnSc2	ztráta
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
možnosti	možnost	k1gFnSc2	možnost
bezproblémového	bezproblémový	k2eAgNnSc2d1	bezproblémové
provozování	provozování	k1gNnSc2	provozování
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
počítačů	počítač	k1gMnPc2	počítač
PC	PC	kA	PC
byl	být	k5eAaImAgMnS	být
způsoben	způsoben	k2eAgMnSc1d1	způsoben
jejich	jejich	k3xOp3gFnSc7	jejich
nízkou	nízký	k2eAgFnSc7d1	nízká
cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
PC	PC	kA	PC
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
až	až	k9	až
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
nedokonalé	dokonalý	k2eNgInPc4d1	nedokonalý
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
bez	bez	k7c2	bez
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
též	též	k9	též
nedokonalé	dokonalý	k2eNgInPc1d1	nedokonalý
uživatelské	uživatelský	k2eAgInPc1d1	uživatelský
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
přestala	přestat	k5eAaPmAgFnS	přestat
PC	PC	kA	PC
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
efektivně	efektivně	k6eAd1	efektivně
konkurovat	konkurovat	k5eAaImF	konkurovat
asijským	asijský	k2eAgMnPc3d1	asijský
výrobcům	výrobce	k1gMnPc3	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
prodání	prodání	k1gNnSc4	prodání
divize	divize	k1gFnSc2	divize
výroby	výroba	k1gFnSc2	výroba
notebooků	notebook	k1gInPc2	notebook
ThinkPad	ThinkPad	k1gInSc1	ThinkPad
čínské	čínský	k2eAgFnSc2d1	čínská
firmě	firma	k1gFnSc6	firma
Lenovo	Lenovo	k1gNnSc1	Lenovo
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
především	především	k9	především
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
serverového	serverový	k2eAgNnSc2d1	serverové
řešení	řešení	k1gNnSc2	řešení
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojených	spojený	k2eAgFnPc2d1	spojená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
přináší	přinášet	k5eAaImIp3nS	přinášet
vyšší	vysoký	k2eAgInPc4d2	vyšší
zisky	zisk	k1gInPc4	zisk
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vyšší	vysoký	k2eAgFnSc3d2	vyšší
přidané	přidaný	k2eAgFnSc3d1	přidaná
hodnotě	hodnota	k1gFnSc3	hodnota
outsourcování	outsourcování	k1gNnSc2	outsourcování
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
komplexních	komplexní	k2eAgNnPc2d1	komplexní
řešení	řešení	k1gNnPc2	řešení
pro	pro	k7c4	pro
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
vytvořením	vytvoření	k1gNnSc7	vytvoření
prvních	první	k4xOgInPc2	první
elektronických	elektronický	k2eAgMnPc2d1	elektronický
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Computing	Computing	k1gInSc4	Computing
Tabulating	Tabulating	k1gInSc1	Tabulating
Recording	Recording	k1gInSc1	Recording
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
předmětů	předmět	k1gInPc2	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
jako	jako	k8xC	jako
vah	váha	k1gFnPc2	váha
<g/>
,	,	kIx,	,
automatických	automatický	k2eAgInPc2d1	automatický
kráječů	kráječ	k1gInPc2	kráječ
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
hodin	hodina	k1gFnPc2	hodina
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
pracovní	pracovní	k2eAgFnSc2d1	pracovní
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
rozličného	rozličný	k2eAgNnSc2d1	rozličné
dalšího	další	k2eAgNnSc2d1	další
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
International	International	k1gFnSc4	International
Business	business	k1gInSc1	business
Machines	Machines	k1gMnSc1	Machines
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
děrných	děrný	k2eAgInPc2d1	děrný
štítků	štítek	k1gInPc2	štítek
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
zužitkovala	zužitkovat	k5eAaPmAgFnS	zužitkovat
svou	svůj	k3xOyFgFnSc4	svůj
akvizici	akvizice	k1gFnSc4	akvizice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
–	–	k?	–
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Dehomag	Dehomaga	k1gFnPc2	Dehomaga
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gFnPc2	Deutsche
Hollerith	Holleritha	k1gFnPc2	Holleritha
Maschinen	Maschinen	k2eAgInSc4d1	Maschinen
Gesellschaft	Gesellschaft	k1gInSc4	Gesellschaft
<g/>
)	)	kIx)	)
Hermanna	Hermann	k1gMnSc2	Hermann
Holleritha	Hollerith	k1gMnSc2	Hollerith
<g/>
,	,	kIx,	,
pionýra	pionýr	k1gMnSc2	pionýr
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Automaty	automat	k1gInPc1	automat
pracující	pracující	k2eAgInPc1d1	pracující
s	s	k7c7	s
děrnými	děrný	k2eAgInPc7d1	děrný
štítky	štítek	k1gInPc7	štítek
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaImNgInP	využívat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
–	–	k?	–
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
a	a	k8xC	a
nejlukrativnější	lukrativní	k2eAgFnPc4d3	nejlukrativnější
zakázky	zakázka	k1gFnPc4	zakázka
IBM	IBM	kA	IBM
tehdy	tehdy	k6eAd1	tehdy
patřilo	patřit	k5eAaImAgNnS	patřit
zpracování	zpracování	k1gNnSc4	zpracování
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
korporace	korporace	k1gFnSc1	korporace
získala	získat	k5eAaPmAgFnS	získat
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
registračních	registrační	k2eAgInPc2d1	registrační
automatů	automat	k1gInPc2	automat
a	a	k8xC	a
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
děrných	děrný	k2eAgInPc2d1	děrný
štítků	štítek	k1gInPc2	štítek
v	v	k7c6	v
několika	několik	k4yIc6	několik
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
několik	několik	k4yIc4	několik
poboček	pobočka	k1gFnPc2	pobočka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
výrobních	výrobní	k2eAgInPc2d1	výrobní
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
<g/>
,	,	kIx,	,
Ženevě	Ženeva	k1gFnSc6	Ženeva
nebo	nebo	k8xC	nebo
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
bylo	být	k5eAaImAgNnS	být
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
stalo	stát	k5eAaPmAgNnS	stát
druhým	druhý	k4xOgInSc7	druhý
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
trhem	trh	k1gInSc7	trh
pro	pro	k7c4	pro
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
americkém	americký	k2eAgNnSc6d1	americké
<g/>
.	.	kIx.	.
</s>
<s>
Registrační	registrační	k2eAgInPc4d1	registrační
automaty	automat	k1gInPc4	automat
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
modely	model	k1gInPc1	model
Hollerith	Holleritha	k1gFnPc2	Holleritha
<g/>
,	,	kIx,	,
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
Dehomag	Dehomag	k1gInSc1	Dehomag
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
78	[number]	k4	78
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
a	a	k8xC	a
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
dodávala	dodávat	k5eAaImAgFnS	dodávat
kompletní	kompletní	k2eAgInSc4d1	kompletní
servis	servis	k1gInSc4	servis
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
instalace	instalace	k1gFnSc1	instalace
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
i	i	k9	i
"	"	kIx"	"
<g/>
workflow	workflow	k?	workflow
<g/>
"	"	kIx"	"
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
zvládnutí	zvládnutí	k1gNnSc3	zvládnutí
zpracování	zpracování	k1gNnSc1	zpracování
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Registrační	registrační	k2eAgInPc1d1	registrační
stroje	stroj	k1gInPc1	stroj
IBM	IBM	kA	IBM
byly	být	k5eAaImAgInP	být
licencovány	licencován	k2eAgInPc1d1	licencován
(	(	kIx(	(
<g/>
nebyly	být	k5eNaImAgInP	být
prodávány	prodáván	k2eAgInPc1d1	prodáván
–	–	k?	–
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
majetkem	majetek	k1gInSc7	majetek
IBM	IBM	kA	IBM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obnášely	obnášet	k5eAaImAgFnP	obnášet
servis	servis	k1gInSc4	servis
od	od	k7c2	od
IBM	IBM	kA	IBM
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgInP	upravit
"	"	kIx"	"
<g/>
na	na	k7c4	na
klíč	klíč	k1gInSc4	klíč
<g/>
"	"	kIx"	"
požadavkům	požadavek	k1gInPc3	požadavek
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dokumentaci	dokumentace	k1gFnSc4	dokumentace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Haftlingskartei	Haftlingskartei	k1gNnPc2	Haftlingskartei
–	–	k?	–
karty	karta	k1gFnSc2	karta
pro	pro	k7c4	pro
vězně	vězně	k6eAd1	vězně
internované	internovaný	k2eAgFnPc4d1	internovaná
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
dokumentace	dokumentace	k1gFnSc1	dokumentace
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
transportů	transport	k1gInPc2	transport
<g/>
,	,	kIx,	,
formulářové	formulářový	k2eAgFnPc1d1	formulářová
karty	karta	k1gFnPc1	karta
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
či	či	k8xC	či
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
rasového	rasový	k2eAgNnSc2d1	rasové
oddělení	oddělení	k1gNnSc2	oddělení
SS	SS	kA	SS
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
J.	J.	kA	J.
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
zařídil	zařídit	k5eAaPmAgMnS	zařídit
finanční	finanční	k2eAgFnSc4d1	finanční
injekci	injekce	k1gFnSc4	injekce
Dehomagu	Dehomag	k1gInSc2	Dehomag
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
až	až	k9	až
k	k	k7c3	k
zakoupení	zakoupení	k1gNnSc3	zakoupení
pozemků	pozemek	k1gInPc2	pozemek
u	u	k7c2	u
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
zřízení	zřízení	k1gNnSc4	zřízení
první	první	k4xOgFnSc2	první
továrny	továrna	k1gFnSc2	továrna
IBM	IBM	kA	IBM
v	v	k7c4	v
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Dehomag	Dehomag	k1gInSc1	Dehomag
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nabízel	nabízet	k5eAaImAgInS	nabízet
nacistickému	nacistický	k2eAgInSc3d1	nacistický
režimu	režim	k1gInSc3	režim
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
identifikaci	identifikace	k1gFnSc6	identifikace
"	"	kIx"	"
<g/>
rasově	rasově	k6eAd1	rasově
nežádoucích	žádoucí	k2eNgMnPc2d1	nežádoucí
<g/>
"	"	kIx"	"
obyvatel	obyvatel	k1gMnSc1	obyvatel
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1937	[number]	k4	1937
se	se	k3xPyFc4	se
ředitel	ředitel	k1gMnSc1	ředitel
Watson	Watson	k1gMnSc1	Watson
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
osobně	osobně	k6eAd1	osobně
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
stolu	stol	k1gInSc2	stol
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
speciální	speciální	k2eAgFnPc4d1	speciální
smlouvy	smlouva	k1gFnPc4	smlouva
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
licencovaných	licencovaný	k2eAgInPc2d1	licencovaný
registračních	registrační	k2eAgInPc2d1	registrační
strojů	stroj	k1gInPc2	stroj
od	od	k7c2	od
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
o	o	k7c6	o
poměrech	poměr	k1gInPc6	poměr
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
(	(	kIx(	(
<g/>
stupňující	stupňující	k2eAgFnSc2d1	stupňující
se	se	k3xPyFc4	se
rasové	rasový	k2eAgFnSc3d1	rasová
diskriminaci	diskriminace	k1gFnSc3	diskriminace
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
existujících	existující	k2eAgInPc6d1	existující
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
pro	pro	k7c4	pro
odpůrce	odpůrce	k1gMnPc4	odpůrce
Hitlerova	Hitlerův	k2eAgInSc2d1	Hitlerův
represívního	represívní	k2eAgInSc2d1	represívní
režimu	režim	k1gInSc2	režim
<g/>
)	)	kIx)	)
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
k	k	k7c3	k
jakému	jaký	k3yIgInSc3	jaký
účelu	účel	k1gInSc3	účel
budou	být	k5eAaImBp3nP	být
registrační	registrační	k2eAgInPc1d1	registrační
stroje	stroj	k1gInPc1	stroj
jeho	jeho	k3xOp3gFnSc2	jeho
firmy	firma	k1gFnSc2	firma
použity	použít	k5eAaPmNgInP	použít
<g/>
,	,	kIx,	,
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
a	a	k8xC	a
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
pobočkách	pobočka	k1gFnPc6	pobočka
IBM	IBM	kA	IBM
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
nechal	nechat	k5eAaPmAgMnS	nechat
své	svůj	k3xOyFgFnPc4	svůj
evropské	evropský	k2eAgFnPc4d1	Evropská
pobočky	pobočka	k1gFnPc4	pobočka
porušovat	porušovat	k5eAaImF	porušovat
tehdy	tehdy	k6eAd1	tehdy
vydaný	vydaný	k2eAgInSc4d1	vydaný
americký	americký	k2eAgInSc4d1	americký
výnos	výnos	k1gInSc4	výnos
Trading	Trading	k1gInSc1	Trading
With	Witha	k1gFnPc2	Witha
Enemy	Enema	k1gFnSc2	Enema
Act	Act	k1gFnSc2	Act
(	(	kIx(	(
<g/>
of	of	k?	of
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zakazující	zakazující	k2eAgNnSc1d1	zakazující
obchodování	obchodování	k1gNnSc1	obchodování
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
IBM	IBM	kA	IBM
zásadně	zásadně	k6eAd1	zásadně
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
a	a	k8xC	a
po	po	k7c6	po
technologické	technologický	k2eAgFnSc6d1	technologická
stránce	stránka	k1gFnSc6	stránka
byla	být	k5eAaImAgFnS	být
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
prvkem	prvek	k1gInSc7	prvek
k	k	k7c3	k
umožnění	umožnění	k1gNnSc3	umožnění
nacistické	nacistický	k2eAgFnSc2d1	nacistická
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
–	–	k?	–
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
by	by	kYmCp3nP	by
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zpracovat	zpracovat	k5eAaPmF	zpracovat
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
stovek	stovka	k1gFnPc2	stovka
tisíců	tisíc	k4xCgInPc2	tisíc
až	až	k9	až
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ortel	ortel	k1gInSc1	ortel
soudní	soudní	k2eAgFnSc2d1	soudní
žaloby	žaloba	k1gFnSc2	žaloba
<g/>
,	,	kIx,	,
podané	podaný	k2eAgFnSc2d1	podaná
proti	proti	k7c3	proti
IBM	IBM	kA	IBM
hromadně	hromadně	k6eAd1	hromadně
z	z	k7c2	z
40	[number]	k4	40
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
sálových	sálový	k2eAgFnPc2d1	sálová
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
nově	nově	k6eAd1	nově
definovaly	definovat	k5eAaBmAgFnP	definovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
portfolio	portfolio	k1gNnSc4	portfolio
na	na	k7c4	na
počítačové	počítačový	k2eAgFnPc4d1	počítačová
komponenty	komponenta	k1gFnPc4	komponenta
a	a	k8xC	a
datová	datový	k2eAgNnPc1d1	datové
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
několika	několik	k4yIc6	několik
ohledech	ohled	k1gInPc6	ohled
posunula	posunout	k5eAaPmAgFnS	posunout
technologické	technologický	k2eAgFnPc4d1	technologická
hranice	hranice	k1gFnPc4	hranice
(	(	kIx(	(
<g/>
miniaturizace	miniaturizace	k1gFnPc4	miniaturizace
<g/>
,	,	kIx,	,
zvyšování	zvyšování	k1gNnSc4	zvyšování
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
IBM	IBM	kA	IBM
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
fungující	fungující	k2eAgFnSc4d1	fungující
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
paměť	paměť	k1gFnSc4	paměť
SRAM	SRAM	kA	SRAM
technologií	technologie	k1gFnSc7	technologie
22	[number]	k4	22
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
mj.	mj.	kA	mj.
na	na	k7c4	na
cloud	cloud	k1gInSc4	cloud
computing	computing	k1gInSc1	computing
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pobočky	pobočka	k1gFnSc2	pobočka
IBM	IBM	kA	IBM
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Chodově	Chodov	k1gInSc6	Chodov
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
Branislav	Branislav	k1gMnSc1	Branislav
Šebo	Šebo	k1gMnSc1	Šebo
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgInSc4d1	kompatibilní
IBM	IBM	kA	IBM
a	a	k8xC	a
holocaust	holocaust	k1gInSc4	holocaust
FUD	FUD	kA	FUD
<g/>
#	#	kIx~	#
<g/>
Historie	historie	k1gFnSc1	historie
Lenovo	Lenův	k2eAgNnSc1d1	Lenovo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
IBM	IBM	kA	IBM
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
IBM	IBM	kA	IBM
</s>
