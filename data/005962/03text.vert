<s>
Runy	Runa	k1gFnPc1	Runa
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
hypotézy	hypotéza	k1gFnSc2	hypotéza
odvozené	odvozený	k2eAgFnPc1d1	odvozená
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
etruského	etruský	k2eAgNnSc2d1	etruské
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
znakové	znakový	k2eAgFnPc1d1	znaková
sady	sada	k1gFnPc1	sada
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
rané	raný	k2eAgFnSc2d1	raná
doby	doba	k1gFnSc2	doba
dějinné	dějinný	k2eAgFnSc2d1	dějinná
(	(	kIx(	(
<g/>
též	též	k9	též
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
protohistorie	protohistorie	k1gFnSc2	protohistorie
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
runové	runový	k2eAgInPc1d1	runový
nápisy	nápis	k1gInPc1	nápis
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
150	[number]	k4	150
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
runová	runový	k2eAgFnSc1d1	runová
abeceda	abeceda	k1gFnSc1	abeceda
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přijímáním	přijímání	k1gNnSc7	přijímání
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
roku	rok	k1gInSc2	rok
750	[number]	k4	750
a	a	k8xC	a
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
až	až	k9	až
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
používání	používání	k1gNnSc1	používání
runové	runový	k2eAgFnSc2d1	runová
abecedy	abeceda	k1gFnSc2	abeceda
ke	k	k7c3	k
speciálním	speciální	k2eAgInPc3d1	speciální
účelům	účel	k1gInPc3	účel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
k	k	k7c3	k
dekoracím	dekorace	k1gFnPc3	dekorace
<g/>
)	)	kIx)	)
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
na	na	k7c6	na
švédském	švédský	k2eAgInSc6d1	švédský
venkově	venkov	k1gInSc6	venkov
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
runových	runový	k2eAgInPc2d1	runový
zápisů	zápis	k1gInPc2	zápis
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
znaků	znak	k1gInPc2	znak
staršího	starý	k2eAgInSc2d2	starší
futharku	futhark	k1gInSc2	futhark
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
blízce	blízce	k6eAd1	blízce
podobno	podoben	k2eAgNnSc4d1	podobno
znakům	znak	k1gInPc3	znak
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
runy	runa	k1gFnPc1	runa
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
podobají	podobat	k5eAaImIp3nP	podobat
znakům	znak	k1gInPc3	znak
severoitalské	severoitalský	k2eAgFnSc6d1	severoitalská
<g/>
,	,	kIx,	,
rétorománské	rétorománský	k2eAgFnSc6d1	rétorománská
<g/>
,	,	kIx,	,
lepontské	lepontský	k2eAgFnSc6d1	lepontský
či	či	k8xC	či
benátské	benátský	k2eAgFnSc6d1	Benátská
abecedě	abeceda	k1gFnSc6	abeceda
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
dosti	dosti	k6eAd1	dosti
podobné	podobný	k2eAgFnPc1d1	podobná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
následovníky	následovník	k1gMnPc7	následovník
staré	starý	k2eAgFnSc2d1	stará
italské	italský	k2eAgFnSc2d1	italská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
souvislost	souvislost	k1gFnSc1	souvislost
není	být	k5eNaImIp3nS	být
věrohodně	věrohodně	k6eAd1	věrohodně
doložitelná	doložitelný	k2eAgFnSc1d1	doložitelná
a	a	k8xC	a
prvotní	prvotní	k2eAgFnSc1d1	prvotní
hypotéza	hypotéza	k1gFnSc1	hypotéza
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poukazovala	poukazovat	k5eAaImAgFnS	poukazovat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
zjednodušení	zjednodušení	k1gNnSc2	zjednodušení
znaků	znak	k1gInPc2	znak
uvedených	uvedený	k2eAgFnPc2d1	uvedená
abeced	abeceda	k1gFnPc2	abeceda
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jejich	jejich	k3xOp3gFnSc4	jejich
úpravu	úprava	k1gFnSc4	úprava
pro	pro	k7c4	pro
ulehčení	ulehčení	k1gNnSc4	ulehčení
záznamu	záznam	k1gInSc2	záznam
rytím	rytí	k1gNnSc7	rytí
a	a	k8xC	a
tesáním	tesání	k1gNnSc7	tesání
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
(	(	kIx(	(
<g/>
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
protiargumentem	protiargument	k1gInSc7	protiargument
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
tvary	tvar	k1gInPc1	tvar
run	runa	k1gFnPc2	runa
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
rytí	rytí	k1gNnSc4	rytí
<g/>
/	/	kIx~	/
<g/>
tesání	tesání	k1gNnSc1	tesání
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
jejich	jejich	k3xOp3gInPc1	jejich
uvažované	uvažovaný	k2eAgInPc1d1	uvažovaný
vzory	vzor	k1gInPc1	vzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Runy	Runa	k1gFnPc1	Runa
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
nejspíše	nejspíše	k9	nejspíše
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
nezávisle	závisle	k6eNd1	závisle
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jen	jen	k9	jen
se	s	k7c7	s
základním	základní	k2eAgInSc7d1	základní
vztahem	vztah	k1gInSc7	vztah
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc2d1	původní
germánskými	germánský	k2eAgFnPc7d1	germánská
písmo	písmo	k1gNnSc4	písmo
(	(	kIx(	(
<g/>
z	z	k7c2	z
cca	cca	kA	cca
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Runová	runový	k2eAgFnSc1d1	runová
abeceda	abeceda	k1gFnSc1	abeceda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
futhark	futhark	k1gInSc1	futhark
<g/>
/	/	kIx~	/
<g/>
futhork	futhork	k1gInSc1	futhork
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
runa	runa	k1gFnSc1	runa
Ansuz	Ansuza	k1gFnPc2	Ansuza
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
jako	jako	k9	jako
A	a	k9	a
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
O	o	k7c4	o
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
prvních	první	k4xOgNnPc2	první
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
a-be-ce-da	aeea	k1gFnSc1	a-be-ce-da
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
3	[number]	k4	3
sady	sada	k1gFnSc2	sada
(	(	kIx(	(
<g/>
skupiny	skupina	k1gFnSc2	skupina
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
potom	potom	k6eAd1	potom
aettir	aettir	k1gInSc4	aettir
(	(	kIx(	(
<g/>
pl.	pl.	k?	pl.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
aett	aett	k1gInSc1	aett
sg.	sg.	k?	sg.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
=	=	kIx~	=
"	"	kIx"	"
<g/>
rodiny	rodina	k1gFnSc2	rodina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
futharku	futhark	k1gInSc2	futhark
<g/>
,	,	kIx,	,
drobně	drobně	k6eAd1	drobně
se	se	k3xPyFc4	se
lišících	lišící	k2eAgFnPc2d1	lišící
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gInSc2	jejich
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
futhark	futhark	k1gInSc1	futhark
(	(	kIx(	(
<g/>
germánský	germánský	k2eAgInSc1d1	germánský
futhark	futhark	k1gInSc1	futhark
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
24	[number]	k4	24
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
(	(	kIx(	(
<g/>
anglosaský	anglosaský	k2eAgInSc1d1	anglosaský
futhork	futhork	k1gInSc1	futhork
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
(	(	kIx(	(
<g/>
vikinský	vikinský	k2eAgInSc1d1	vikinský
futhork	futhork	k1gInSc1	futhork
<g/>
)	)	kIx)	)
jenom	jenom	k9	jenom
16	[number]	k4	16
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Runy	Runa	k1gFnPc1	Runa
sloužily	sloužit	k5eAaImAgFnP	sloužit
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
k	k	k7c3	k
magickým	magický	k2eAgInPc3d1	magický
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
věštba	věštba	k1gFnSc1	věštba
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc1d1	různá
formule	formule	k1gFnPc1	formule
a	a	k8xC	a
talismany	talisman	k1gInPc1	talisman
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
umíme	umět	k5eAaImIp1nP	umět
runové	runový	k2eAgInPc4d1	runový
znaky	znak	k1gInPc4	znak
rozluštit	rozluštit	k5eAaPmF	rozluštit
<g/>
,	,	kIx,	,
smysl	smysl	k1gInSc4	smysl
některých	některý	k3yIgInPc2	některý
textů	text	k1gInPc2	text
nám	my	k3xPp1nPc3	my
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
utajen	utajit	k5eAaPmNgInS	utajit
<g/>
.	.	kIx.	.
</s>
<s>
Runy	Runa	k1gFnPc1	Runa
nerozeznávají	rozeznávat	k5eNaImIp3nP	rozeznávat
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Runy	Runa	k1gFnPc1	Runa
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
ryly	rýt	k5eAaImAgFnP	rýt
do	do	k7c2	do
kamenů	kámen	k1gInPc2	kámen
či	či	k8xC	či
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rituálního	rituální	k2eAgNnSc2d1	rituální
nebo	nebo	k8xC	nebo
magického	magický	k2eAgNnSc2d1	magické
použití	použití	k1gNnSc2	použití
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
dodržovány	dodržovat	k5eAaImNgInP	dodržovat
postupy	postup	k1gInPc1	postup
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Runatál	Runatála	k1gFnPc2	Runatála
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
ságy	sága	k1gFnSc2	sága
Hávamál	Hávamál	k1gInSc1	Hávamál
(	(	kIx(	(
<g/>
Výroky	výrok	k1gInPc1	výrok
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
barvení	barvení	k1gNnSc1	barvení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
provádělo	provádět	k5eAaImAgNnS	provádět
též	též	k9	též
i	i	k9	i
krví	krev	k1gFnSc7	krev
pisatele	pisatel	k1gMnSc2	pisatel
<g/>
.	.	kIx.	.
</s>
<s>
Runám	Runa	k1gFnPc3	Runa
se	se	k3xPyFc4	se
přičítá	přičítat	k5eAaImIp3nS	přičítat
božský	božský	k2eAgInSc4d1	božský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ságy	sága	k1gFnPc4	sága
praví	pravit	k5eAaBmIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bůh	bůh	k1gMnSc1	bůh
Ódin	Ódin	k1gInSc4	Ódin
obětoval	obětovat	k5eAaBmAgMnS	obětovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vědění	vědění	k1gNnSc2	vědění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
historie	historie	k1gFnSc1	historie
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
děl	dělo	k1gNnPc2	dělo
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
epické	epický	k2eAgFnSc6d1	epická
básni	báseň	k1gFnSc6	báseň
Runatál	Runatála	k1gFnPc2	Runatála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
díla	dílo	k1gNnSc2	dílo
Havamálu	Havamál	k1gInSc2	Havamál
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
úryvek	úryvek	k1gInSc1	úryvek
popisuje	popisovat	k5eAaImIp3nS	popisovat
Odinovu	Odinův	k2eAgFnSc4d1	Odinova
oběť	oběť	k1gFnSc4	oběť
<g/>
:	:	kIx,	:
Podělil	podělit	k5eAaPmAgMnS	podělit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
vědění	vědění	k1gNnSc4	vědění
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
<g/>
,	,	kIx,	,
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
,	,	kIx,	,
obry	obr	k1gMnPc4	obr
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
detailně	detailně	k6eAd1	detailně
popsáno	popsat	k5eAaPmNgNnS	popsat
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
úryvku	úryvek	k1gInSc6	úryvek
z	z	k7c2	z
Runatálu	Runatál	k1gInSc2	Runatál
<g/>
:	:	kIx,	:
V	v	k7c6	v
Runatálu	Runatál	k1gInSc6	Runatál
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
obsažena	obsažen	k2eAgFnSc1d1	obsažena
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgNnSc1d1	popisující
umění	umění	k1gNnSc1	umění
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
runami	runa	k1gFnPc7	runa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vyjmenovány	vyjmenovat	k5eAaPmNgInP	vyjmenovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
úkony	úkon	k1gInPc1	úkon
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
musí	muset	k5eAaImIp3nS	muset
schopný	schopný	k2eAgMnSc1d1	schopný
runopisec	runopisec	k1gMnSc1	runopisec
ovládat	ovládat	k5eAaImF	ovládat
<g/>
:	:	kIx,	:
Runoví	runový	k2eAgMnPc1d1	runový
mistři	mistr	k1gMnPc1	mistr
(	(	kIx(	(
<g/>
runici	runik	k1gMnPc1	runik
<g/>
)	)	kIx)	)
i	i	k9	i
mistryně	mistryně	k1gFnSc1	mistryně
(	(	kIx(	(
<g/>
alruny	alruna	k1gFnPc1	alruna
<g/>
;	;	kIx,	;
na	na	k7c6	na
Severu	sever	k1gInSc6	sever
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
lepší	dobrý	k2eAgNnSc1d2	lepší
postavení	postavení	k1gNnSc1	postavení
žen	žena	k1gFnPc2	žena
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
<g/>
,	,	kIx,	,
především	především	k9	především
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
preferovány	preferován	k2eAgFnPc1d1	preferována
v	v	k7c6	v
úkonech	úkon	k1gInPc6	úkon
týkajících	týkající	k2eAgInPc6d1	týkající
se	se	k3xPyFc4	se
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
věštění	věštění	k1gNnSc2	věštění
a	a	k8xC	a
léčení	léčení	k1gNnSc2	léčení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
obecně	obecně	k6eAd1	obecně
runopisci	runopisec	k1gMnPc1	runopisec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
ovládnout	ovládnout	k5eAaPmF	ovládnout
runy	runa	k1gFnPc4	runa
<g/>
.	.	kIx.	.
</s>
<s>
Četli	číst	k5eAaImAgMnP	číst
runy	run	k1gInPc4	run
složené	složený	k2eAgInPc4d1	složený
z	z	k7c2	z
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
tesali	tesat	k5eAaImAgMnP	tesat
do	do	k7c2	do
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
vyřezávali	vyřezávat	k5eAaImAgMnP	vyřezávat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
a	a	k8xC	a
při	při	k7c6	při
zapisování	zapisování	k1gNnSc6	zapisování
skaldských	skaldský	k2eAgInPc2d1	skaldský
veršů	verš	k1gInPc2	verš
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
při	při	k7c6	při
věštbách	věštba	k1gFnPc6	věštba
(	(	kIx(	(
<g/>
runové	runový	k2eAgInPc1d1	runový
kalendáře	kalendář	k1gInPc1	kalendář
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaříkadlech	zaříkadlo	k1gNnPc6	zaříkadlo
<g/>
.	.	kIx.	.
</s>
<s>
Germánský	germánský	k2eAgInSc1d1	germánský
futhark	futhark	k1gInSc1	futhark
prostý	prostý	k2eAgInSc1d1	prostý
Anglosaský	anglosaský	k2eAgInSc1d1	anglosaský
futhork	futhork	k1gInSc1	futhork
Vikinský	vikinský	k2eAgInSc4d1	vikinský
futhork	futhork	k1gInSc4	futhork
</s>
