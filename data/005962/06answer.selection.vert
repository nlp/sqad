<s>
První	první	k4xOgInPc1	první
runové	runový	k2eAgInPc1d1	runový
nápisy	nápis	k1gInPc1	nápis
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
150	[number]	k4	150
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
runová	runový	k2eAgFnSc1d1	runová
abeceda	abeceda	k1gFnSc1	abeceda
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přijímáním	přijímání	k1gNnSc7	přijímání
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
roku	rok	k1gInSc2	rok
750	[number]	k4	750
a	a	k8xC	a
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
až	až	k9	až
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
</s>
