<s>
Tykev	tykev	k1gFnSc1	tykev
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
tykev	tykev	k1gFnSc1	tykev
turek	turek	k1gMnSc1	turek
nebo	nebo	k8xC	nebo
dýně	dýně	k1gFnSc1	dýně
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
tykev	tykev	k1gFnSc1	tykev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
dýně	dýně	k1gFnSc2	dýně
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaná	dochovaný	k2eAgNnPc1d1	dochované
semena	semeno	k1gNnPc1	semeno
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
kulturní	kulturní	k2eAgFnPc4d1	kulturní
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoletou	jednoletý	k2eAgFnSc4d1	jednoletá
rostlinu	rostlina	k1gFnSc4	rostlina
keříčkového	keříčkový	k2eAgInSc2d1	keříčkový
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc4d1	žlutá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velkým	velký	k2eAgInPc3d1	velký
rozdílům	rozdíl	k1gInPc3	rozdíl
u	u	k7c2	u
bobulí	bobule	k1gFnPc2	bobule
této	tento	k3xDgFnSc2	tento
dýně	dýně	k1gFnSc2	dýně
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
několik	několik	k4yIc4	několik
variet	varieta	k1gFnPc2	varieta
<g/>
:	:	kIx,	:
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
,	,	kIx,	,
varieta	varieta	k1gFnSc1	varieta
giromontiina	giromontiin	k2eAgFnSc1d1	giromontiin
-	-	kIx~	-
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
kabačky	kabačka	k1gFnSc2	kabačka
a	a	k8xC	a
cukety	cuketa	k1gFnSc2	cuketa
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
,	,	kIx,	,
varieta	varieta	k1gFnSc1	varieta
patissonina	patissonina	k1gFnSc1	patissonina
-	-	kIx~	-
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
patizony	patizon	k1gMnPc4	patizon
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
,	,	kIx,	,
varieta	varieta	k1gFnSc1	varieta
oleifera	oleifera	k1gFnSc1	oleifera
-	-	kIx~	-
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
tykev	tykev	k1gFnSc1	tykev
(	(	kIx(	(
<g/>
dýně	dýně	k1gFnSc1	dýně
<g/>
)	)	kIx)	)
olejná	olejný	k2eAgFnSc1d1	olejná
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
typy	typ	k1gInPc4	typ
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
dýně	dýně	k1gFnSc2	dýně
patří	patřit	k5eAaImIp3nS	patřit
dýně	dýně	k1gFnSc1	dýně
špagetová	špagetový	k2eAgFnSc1d1	špagetová
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
nedělené	dělený	k2eNgInPc1d1	nedělený
nebo	nebo	k8xC	nebo
dlanitě	dlanitě	k6eAd1	dlanitě
laločné	laločný	k2eAgNnSc1d1	laločný
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
bobule	bobule	k1gFnSc1	bobule
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
důležité	důležitý	k2eAgFnPc1d1	důležitá
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
karoten	karoten	k1gInSc1	karoten
<g/>
.	.	kIx.	.
</s>
<s>
Dýně	dýně	k1gFnSc1	dýně
je	být	k5eAaImIp3nS	být
neodmyslitelným	odmyslitelný	k2eNgInSc7d1	neodmyslitelný
symbolem	symbol	k1gInSc7	symbol
amerického	americký	k2eAgInSc2d1	americký
svátku	svátek	k1gInSc2	svátek
Halloween	Hallowena	k1gFnPc2	Hallowena
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
dýních	dýně	k1gFnPc6	dýně
<g/>
.	.	kIx.	.
</s>
