<p>
<s>
Lanthan	lanthan	k1gInSc1	lanthan
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
La	la	k1gNnSc2	la
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Lanthanum	Lanthanum	k1gInSc1	Lanthanum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
kovů	kov	k1gInPc2	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
metalurgickém	metalurgický	k2eAgInSc6d1	metalurgický
průmyslu	průmysl	k1gInSc6	průmysl
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
anebo	anebo	k8xC	anebo
jejich	jejich	k3xOp3gFnSc4	jejich
deoxidaci	deoxidace	k1gFnSc4	deoxidace
a	a	k8xC	a
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
některých	některý	k3yIgNnPc2	některý
speciálních	speciální	k2eAgNnPc2d1	speciální
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Lanthan	lanthan	k1gInSc1	lanthan
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
elektropozitivní	elektropozitivní	k2eAgInSc1d1	elektropozitivní
přechodný	přechodný	k2eAgInSc1d1	přechodný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
6,00	[number]	k4	6,00
K.	K.	kA	K.
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
lanthan	lanthan	k1gInSc1	lanthan
značně	značně	k6eAd1	značně
reaktivním	reaktivní	k2eAgInSc7d1	reaktivní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejreaktivnější	reaktivní	k2eAgNnSc4d3	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
velmi	velmi	k6eAd1	velmi
stabilního	stabilní	k2eAgInSc2d1	stabilní
oxidu	oxid	k1gInSc2	oxid
lanthanitého	lanthanitý	k2eAgInSc2d1	lanthanitý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
lanthan	lanthan	k1gInSc1	lanthan
zvolna	zvolna	k6eAd1	zvolna
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
také	také	k9	také
přímo	přímo	k6eAd1	přímo
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
běžnými	běžný	k2eAgInPc7d1	běžný
nekovovými	kovový	k2eNgInPc7d1	nekovový
prvky	prvek	k1gInPc7	prvek
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
bor	bor	k1gInSc4	bor
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
a	a	k8xC	a
halogeny	halogen	k1gInPc1	halogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
La	la	k1gNnPc2	la
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgFnPc7	svůj
chemickými	chemický	k2eAgFnPc7d1	chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
podobá	podobat	k5eAaImIp3nS	podobat
hliníku	hliník	k1gInSc3	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc1	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nerozpustnost	nerozpustnost	k1gFnSc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nerozpustnou	rozpustný	k2eNgFnSc7d1	nerozpustná
sloučeninou	sloučenina	k1gFnSc7	sloučenina
je	být	k5eAaImIp3nS	být
šťavelan	šťavelan	k1gInSc1	šťavelan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
gravimetrickému	gravimetrický	k2eAgNnSc3d1	gravimetrické
stanovení	stanovení	k1gNnSc3	stanovení
lanthanu	lanthan	k1gInSc2	lanthan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lanthan	lanthan	k1gInSc1	lanthan
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Carlem	Carl	k1gMnSc7	Carl
Mosanderem	Mosander	k1gMnSc7	Mosander
<g/>
,	,	kIx,	,
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
byl	být	k5eAaImAgInS	být
izolován	izolován	k2eAgInSc1d1	izolován
až	až	k9	až
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Lanthan	lanthan	k1gInSc1	lanthan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
asi	asi	k9	asi
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
kolem	kolem	k7c2	kolem
1,2	[number]	k4	1,2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
lanthanu	lanthan	k1gInSc2	lanthan
na	na	k7c4	na
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
lanthan	lanthan	k1gInSc1	lanthan
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
monazitové	monazitový	k2eAgInPc4d1	monazitový
písky	písek	k1gInPc4	písek
a	a	k8xC	a
xenotim	xenotim	k1gInSc4	xenotim
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
–	–	k?	–
směsné	směsný	k2eAgInPc1d1	směsný
flourouhličitany	flourouhličitan	k1gInPc1	flourouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc4d1	fosfátová
suroviny	surovina	k1gFnPc4	surovina
-	-	kIx~	-
apatity	apatit	k1gInPc4	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
–	–	k?	–
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
redukcí	redukce	k1gFnSc7	redukce
solí	sůl	k1gFnPc2	sůl
kovovým	kovový	k2eAgInSc7d1	kovový
vápníkem	vápník	k1gInSc7	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Redukci	redukce	k1gFnSc4	redukce
fluoridu	fluorid	k1gInSc2	fluorid
lanthanitého	lanthanitý	k2eAgInSc2d1	lanthanitý
popisuje	popisovat	k5eAaImIp3nS	popisovat
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
LaF	LaF	k1gFnSc1	LaF
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Ca	ca	kA	ca
→	→	k?	→
2	[number]	k4	2
La	la	k1gNnSc2	la
+	+	kIx~	+
3	[number]	k4	3
CaF	CaF	k1gFnPc2	CaF
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgNnSc3d1	vysoké
zastoupení	zastoupení	k1gNnSc3	zastoupení
lanthanu	lanthan	k1gInSc2	lanthan
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
je	být	k5eAaImIp3nS	být
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
relativně	relativně	k6eAd1	relativně
nadbytek	nadbytek	k1gInSc4	nadbytek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzniká	vznikat	k5eAaImIp3nS	vznikat
částečně	částečně	k6eAd1	částečně
jako	jako	k8xS	jako
přebytek	přebytek	k1gInSc1	přebytek
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vysoce	vysoce	k6eAd1	vysoce
žádaných	žádaný	k2eAgInPc2d1	žádaný
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
europia	europium	k1gNnSc2	europium
nebo	nebo	k8xC	nebo
samaria	samarium	k1gNnSc2	samarium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
využití	využití	k1gNnSc1	využití
nalézá	nalézat	k5eAaImIp3nS	nalézat
lanthan	lanthan	k1gInSc4	lanthan
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vysoká	vysoký	k2eAgFnSc1d1	vysoká
afinita	afinita	k1gFnSc1	afinita
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
při	při	k7c6	při
odkysličování	odkysličování	k1gNnSc6	odkysličování
roztavených	roztavený	k2eAgInPc2d1	roztavený
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
malé	malý	k2eAgInPc4d1	malý
přídavky	přídavek	k1gInPc4	přídavek
lanthanu	lanthan	k1gInSc2	lanthan
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výsledné	výsledný	k2eAgFnPc4d1	výsledná
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
litina	litina	k1gFnSc1	litina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
lanthanu	lanthan	k1gInSc2	lanthan
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
tvárnost	tvárnost	k1gFnSc4	tvárnost
a	a	k8xC	a
kujnost	kujnost	k1gFnSc4	kujnost
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
nárazu	náraz	k1gInSc3	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
molybdenu	molybden	k1gInSc2	molybden
snižuje	snižovat	k5eAaImIp3nS	snižovat
přídavek	přídavek	k1gInSc1	přídavek
lanthanu	lanthan	k1gInSc2	lanthan
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
náhlým	náhlý	k2eAgFnPc3d1	náhlá
teplotním	teplotní	k2eAgFnPc3d1	teplotní
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgNnSc1d1	významné
uplatnění	uplatnění	k1gNnPc1	uplatnění
nalézají	nalézat	k5eAaImIp3nP	nalézat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
lanthanu	lanthan	k1gInSc2	lanthan
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
oxid	oxid	k1gInSc1	oxid
lanthanitý	lanthanitý	k2eAgInSc1d1	lanthanitý
La	la	k1gNnSc3	la
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
získává	získávat	k5eAaImIp3nS	získávat
vysoký	vysoký	k2eAgInSc1d1	vysoký
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
nízký	nízký	k2eAgInSc1d1	nízký
světelný	světelný	k2eAgInSc1d1	světelný
rozptyl	rozptyl	k1gInSc1	rozptyl
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
často	často	k6eAd1	často
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
v	v	k7c6	v
objektivech	objektiv	k1gInPc6	objektiv
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
nebo	nebo	k8xC	nebo
dalekohledech	dalekohled	k1gInPc6	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
lanthanu	lanthan	k1gInSc2	lanthan
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
optické	optický	k2eAgInPc1d1	optický
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
propouštějící	propouštějící	k2eAgNnSc1d1	propouštějící
pouze	pouze	k6eAd1	pouze
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalyzátory	katalyzátor	k1gInPc1	katalyzátor
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
lanthanu	lanthan	k1gInSc2	lanthan
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
petrochemii	petrochemie	k1gFnSc6	petrochemie
při	při	k7c6	při
krakování	krakování	k1gNnSc6	krakování
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brusné	brusný	k2eAgInPc1d1	brusný
a	a	k8xC	a
lešticí	lešticí	k2eAgInPc1d1	lešticí
práškové	práškový	k2eAgInPc1d1	práškový
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
optických	optický	k2eAgFnPc2d1	optická
součástek	součástka	k1gFnPc2	součástka
(	(	kIx(	(
<g/>
přesné	přesný	k2eAgFnSc2d1	přesná
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
do	do	k7c2	do
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
často	často	k6eAd1	často
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
sloučenin	sloučenina	k1gFnPc2	sloučenina
lanthanu	lanthan	k1gInSc2	lanthan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
lanthanitých	lanthanitý	k2eAgInPc2d1	lanthanitý
iontů	ion	k1gInPc2	ion
do	do	k7c2	do
analyzovaných	analyzovaný	k2eAgInPc2d1	analyzovaný
roztoků	roztok	k1gInPc2	roztok
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
spektrální	spektrální	k2eAgInSc4d1	spektrální
iontový	iontový	k2eAgInSc4d1	iontový
pufr	pufr	k1gInSc4	pufr
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
atomové	atomový	k2eAgFnSc6d1	atomová
absorpční	absorpční	k2eAgFnSc6d1	absorpční
spektrometrii	spektrometrie	k1gFnSc6	spektrometrie
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
interferencí	interference	k1gFnPc2	interference
<g/>
,	,	kIx,	,
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
přítomností	přítomnost	k1gFnPc2	přítomnost
vysokých	vysoký	k2eAgInPc2d1	vysoký
množství	množství	k1gNnSc4	množství
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lanthan	lanthan	k1gInSc1	lanthan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lanthan	lanthan	k1gInSc1	lanthan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
