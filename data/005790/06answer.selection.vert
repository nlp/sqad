<s>
Ucho	ucho	k1gNnSc1	ucho
mladého	mladý	k2eAgMnSc2d1	mladý
člověka	člověk	k1gMnSc2	člověk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
zvuk	zvuk	k1gInSc4	zvuk
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
frekvencí	frekvence	k1gFnPc2	frekvence
20	[number]	k4	20
–	–	k?	–
20	[number]	k4	20
000	[number]	k4	000
hertzů	hertz	k1gInPc2	hertz
<g/>
,	,	kIx,	,
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
obvykle	obvykle	k6eAd1	obvykle
slyší	slyšet	k5eAaImIp3nP	slyšet
jen	jen	k9	jen
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
frekvencí	frekvence	k1gFnSc7	frekvence
50	[number]	k4	50
–	–	k?	–
8	[number]	k4	8
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
