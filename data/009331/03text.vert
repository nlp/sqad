<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Pardubitz	Pardubitz	k1gInSc1	Pardubitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
univerzitní	univerzitní	k2eAgFnPc1d1	univerzitní
a	a	k8xC	a
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
metropole	metropole	k1gFnSc1	metropole
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
správní	správní	k2eAgFnSc7d1	správní
<g/>
,	,	kIx,	,
obytnou	obytný	k2eAgFnSc7d1	obytná
<g/>
,	,	kIx,	,
obslužnou	obslužný	k2eAgFnSc7d1	obslužná
a	a	k8xC	a
výrobní	výrobní	k2eAgFnSc7d1	výrobní
funkcí	funkce	k1gFnSc7	funkce
pardubicko-hradecké	pardubickoradecký	k2eAgFnSc2d1	pardubicko-hradecký
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Polabí	Polabí	k1gNnSc2	Polabí
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Labe	Labe	k1gNnSc1	Labe
a	a	k8xC	a
Chrudimky	Chrudimka	k1gFnPc1	Chrudimka
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
20	[number]	k4	20
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
220	[number]	k4	220
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
91	[number]	k4	91
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
desátým	desátý	k4xOgNnSc7	desátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
i	i	k8xC	i
okresu	okres	k1gInSc2	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Výměra	výměra	k1gFnSc1	výměra
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
82,7	[number]	k4	82,7
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
8	[number]	k4	8
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
20	[number]	k4	20
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
Pardubic	Pardubice	k1gInPc2	Pardubice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
<g/>
,	,	kIx,	,
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1340	[number]	k4	1340
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
připomínány	připomínán	k2eAgFnPc1d1	připomínána
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
prodělaly	prodělat	k5eAaPmAgFnP	prodělat
za	za	k7c4	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
přišel	přijít	k5eAaPmAgInS	přijít
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
města	město	k1gNnSc2	město
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
jako	jako	k8xS	jako
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
mlýnské	mlýnský	k2eAgInPc4d1	mlýnský
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
Fantova	Fantův	k2eAgFnSc1d1	Fantova
rafinérie	rafinérie	k1gFnSc1	rafinérie
minerálních	minerální	k2eAgInPc2d1	minerální
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivní	masivní	k2eAgFnSc3d1	masivní
výstavbě	výstavba	k1gFnSc3	výstavba
nových	nový	k2eAgFnPc2d1	nová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
především	především	k9	především
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
Univerzita	univerzita	k1gFnSc1	univerzita
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
pobočka	pobočka	k1gFnSc1	pobočka
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
průmysl	průmysl	k1gInSc4	průmysl
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
proslulé	proslulý	k2eAgFnPc1d1	proslulá
výrobou	výroba	k1gFnSc7	výroba
perníku	perník	k1gInSc2	perník
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
sportovní	sportovní	k2eAgFnPc1d1	sportovní
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
slavné	slavný	k2eAgInPc4d1	slavný
koňské	koňský	k2eAgInPc4d1	koňský
dostihy	dostih	k1gInPc4	dostih
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
,	,	kIx,	,
motocyklový	motocyklový	k2eAgInSc1d1	motocyklový
závod	závod	k1gInSc1	závod
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
šachu	šach	k1gInSc2	šach
Czech	Czecha	k1gFnPc2	Czecha
Open	Open	k1gInSc1	Open
či	či	k8xC	či
tenisová	tenisový	k2eAgFnSc1d1	tenisová
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
juniorka	juniorka	k1gFnSc1	juniorka
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
a	a	k8xC	a
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
tu	tu	k6eAd1	tu
sídlí	sídlet	k5eAaImIp3nS	sídlet
Východočeské	východočeský	k2eAgNnSc4d1	Východočeské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
profesionální	profesionální	k2eAgFnSc2d1	profesionální
Komorní	komorní	k2eAgFnSc2d1	komorní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
doložená	doložená	k1gFnSc1	doložená
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
<g/>
,	,	kIx,	,
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Pordoby	Pordoba	k1gFnSc2	Pordoba
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
tedy	tedy	k9	tedy
dříve	dříve	k6eAd2	dříve
znělo	znět	k5eAaImAgNnS	znět
Pordobice	Pordobice	k1gFnSc2	Pordobice
(	(	kIx(	(
<g/>
1318	[number]	k4	1318
"	"	kIx"	"
<g/>
de	de	k?	de
Pordobitz	Pordobitz	k1gInSc1	Pordobitz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
ves	ves	k1gFnSc1	ves
lidí	člověk	k1gMnPc2	člověk
Pordobových	Pordobových	k2eAgFnSc1d1	Pordobových
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pordobice	Pordobice	k1gFnPc1	Pordobice
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
čtvrti	čtvrt	k1gFnSc2	čtvrt
Pardubičky	Pardubička	k1gFnSc2	Pardubička
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
do	do	k7c2	do
Polabí	Polabí	k1gNnSc2	Polabí
přinesli	přinést	k5eAaPmAgMnP	přinést
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
mniši	mnich	k1gMnPc1	mnich
řádu	řád	k1gInSc2	řád
cyriaků	cyriak	k1gMnPc2	cyriak
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
polského	polský	k2eAgNnSc2d1	polské
místního	místní	k2eAgNnSc2d1	místní
jména	jméno	k1gNnSc2	jméno
Porydęby	Porydęba	k1gFnSc2	Porydęba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spravovali	spravovat	k5eAaImAgMnP	spravovat
zdejší	zdejší	k2eAgFnSc4d1	zdejší
kapli	kaple	k1gFnSc4	kaple
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hláskovou	hláskový	k2eAgFnSc4d1	hlásková
změnu	změna	k1gFnSc4	změna
Pordob-	Pordob-	k1gFnSc2	Pordob-
na	na	k7c4	na
Pardub-	Pardub-	k1gFnSc4	Pardub-
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
snahou	snaha	k1gFnSc7	snaha
po	po	k7c6	po
disimilaci	disimilace	k1gFnSc6	disimilace
dvou	dva	k4xCgMnPc2	dva
-o-	-	k?	-o-
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
mylnou	mylný	k2eAgFnSc7d1	mylná
asociací	asociace	k1gFnSc7	asociace
se	s	k7c7	s
spojením	spojení	k1gNnSc7	spojení
"	"	kIx"	"
<g/>
pár	pár	k4xCyI	pár
dubů	dub	k1gInPc2	dub
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
osobním	osobní	k2eAgNnSc7d1	osobní
jménem	jméno	k1gNnSc7	jméno
Pardus	pardus	k1gInSc1	pardus
<g/>
;	;	kIx,	;
výklad	výklad	k1gInSc1	výklad
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
podoba	podoba	k1gFnSc1	podoba
názvu	název	k1gInSc2	název
města	město	k1gNnSc2	město
zní	znět	k5eAaImIp3nS	znět
Pardubitz	Pardubitz	k1gInSc1	Pardubitz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
Pardubic	Pardubice	k1gInPc2	Pardubice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
zdejší	zdejší	k2eAgInSc4d1	zdejší
klášter	klášter	k1gInSc4	klášter
cyriaků	cyriak	k1gMnPc2	cyriak
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pánové	pán	k1gMnPc1	pán
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
Pernštejnové	Pernštejnová	k1gFnSc2	Pernštejnová
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
vlastníkem	vlastník	k1gMnSc7	vlastník
Pardubic	Pardubice	k1gInPc2	Pardubice
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Púta	Pút	k2eAgMnSc4d1	Pút
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Pardubice	Pardubice	k1gInPc1	Pardubice
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
Arnoštem	Arnošt	k1gMnSc7	Arnošt
z	z	k7c2	z
Hostýně	Hostýn	k1gInSc6	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
přední	přední	k2eAgFnSc4d1	přední
bílou	bílý	k2eAgFnSc4d1	bílá
(	(	kIx(	(
<g/>
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
<g/>
)	)	kIx)	)
polovinu	polovina	k1gFnSc4	polovina
koně	kůň	k1gMnSc2	kůň
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
uzdou	uzda	k1gFnSc7	uzda
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
štítu	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Arnoštova	Arnoštův	k2eAgFnSc1d1	Arnoštova
závěť	závěť	k1gFnSc1	závěť
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
dokladem	doklad	k1gInSc7	doklad
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
Pardubic	Pardubice	k1gInPc2	Pardubice
jako	jako	k8xS	jako
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
jako	jako	k9	jako
poddanského	poddanský	k2eAgNnSc2d1	poddanské
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Povýšení	povýšení	k1gNnSc1	povýšení
Pardubic	Pardubice	k1gInPc2	Pardubice
na	na	k7c4	na
městečko	městečko	k1gNnSc4	městečko
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
letech	let	k1gInPc6	let
1332	[number]	k4	1332
<g/>
–	–	k?	–
<g/>
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1390	[number]	k4	1390
byla	být	k5eAaImAgFnS	být
postoupena	postoupen	k2eAgFnSc1d1	postoupena
část	část	k1gFnSc1	část
panství	panství	k1gNnSc2	panství
Hanušovi	Hanuš	k1gMnSc6	Hanuš
z	z	k7c2	z
Milheimu	Milheim	k1gInSc2	Milheim
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
majitelem	majitel	k1gMnSc7	majitel
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
stal	stát	k5eAaPmAgMnS	stát
Viktorín	Viktorín	k1gMnSc1	Viktorín
Boček	Boček	k1gMnSc1	Boček
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
Pardubice	Pardubice	k1gInPc1	Pardubice
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
získal	získat	k5eAaPmAgInS	získat
úchvatem	úchvat	k1gInSc7	úchvat
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Jan	Jan	k1gMnSc1	Jan
Hlaváč	Hlaváč	k1gMnSc1	Hlaváč
z	z	k7c2	z
Mitrova	Mitrov	k1gInSc2	Mitrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1491	[number]	k4	1491
Pardubice	Pardubice	k1gInPc1	Pardubice
koupil	koupit	k5eAaPmAgMnS	koupit
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
nejmocnější	mocný	k2eAgMnSc1d3	nejmocnější
šlechtic	šlechtic	k1gMnSc1	šlechtic
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zvelebování	zvelebování	k1gNnSc2	zvelebování
města	město	k1gNnSc2	město
a	a	k8xC	a
panství	panství	k1gNnSc2	panství
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
i	i	k9	i
do	do	k7c2	do
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgFnSc2d1	gotická
přestavby	přestavba	k1gFnSc2	přestavba
zdejšího	zdejší	k2eAgInSc2d1	zdejší
vodního	vodní	k2eAgInSc2d1	vodní
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
synové	syn	k1gMnPc1	syn
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
pak	pak	k6eAd1	pak
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
renesančním	renesanční	k2eAgInSc6d1	renesanční
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
historický	historický	k2eAgInSc1d1	historický
unikát	unikát	k1gInSc1	unikát
–	–	k?	–
přechod	přechod	k1gInSc4	přechod
obranného	obranný	k2eAgInSc2d1	obranný
vodního	vodní	k2eAgInSc2d1	vodní
hradu	hrad	k1gInSc2	hrad
na	na	k7c4	na
rozlehlý	rozlehlý	k2eAgInSc4d1	rozlehlý
a	a	k8xC	a
pohodlný	pohodlný	k2eAgInSc4d1	pohodlný
pardubický	pardubický	k2eAgInSc4d1	pardubický
zámek	zámek	k1gInSc4	zámek
–	–	k?	–
lze	lze	k6eAd1	lze
obdivovat	obdivovat	k5eAaImF	obdivovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
i	i	k9	i
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
historické	historický	k2eAgNnSc4d1	historické
městské	městský	k2eAgNnSc4d1	Městské
jádro	jádro	k1gNnSc4	jádro
s	s	k7c7	s
renesančním	renesanční	k2eAgNnSc7d1	renesanční
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
malebnými	malebný	k2eAgFnPc7d1	malebná
uličkami	ulička	k1gFnPc7	ulička
a	a	k8xC	a
dominantou	dominanta	k1gFnSc7	dominanta
Pardubic	Pardubice	k1gInPc2	Pardubice
–	–	k?	–
Zelenou	zelený	k2eAgFnSc7d1	zelená
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
slavné	slavný	k2eAgFnSc6d1	slavná
éře	éra	k1gFnSc6	éra
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zadlužené	zadlužený	k2eAgFnPc4d1	zadlužená
panství	panství	k1gNnPc2	panství
prodali	prodat	k5eAaPmAgMnP	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
arciknížeti	arcikníže	k1gNnSc6wR	arcikníže
Maxmiliánu	Maxmiliána	k1gFnSc4	Maxmiliána
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
město	město	k1gNnSc1	město
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
zbídačila	zbídačit	k5eAaPmAgFnS	zbídačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Industrializace	industrializace	k1gFnSc2	industrializace
===	===	k?	===
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k6eAd1	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1845	[number]	k4	1845
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
severní	severní	k2eAgFnSc2d1	severní
státní	státní	k2eAgFnSc2d1	státní
dráhy	dráha	k1gFnSc2	dráha
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc1	století
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
města	město	k1gNnSc2	město
postupně	postupně	k6eAd1	postupně
rozvětveny	rozvětven	k2eAgFnPc4d1	rozvětvena
další	další	k2eAgFnPc4d1	další
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
jako	jako	k8xC	jako
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
mlýnských	mlýnský	k2eAgInPc2d1	mlýnský
strojů	stroj	k1gInPc2	stroj
Josefa	Josef	k1gMnSc2	Josef
Prokopa	Prokop	k1gMnSc2	Prokop
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Fantova	Fantův	k2eAgFnSc1d1	Fantova
rafinérie	rafinérie	k1gFnSc1	rafinérie
minerálních	minerální	k2eAgInPc2d1	minerální
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
země	zem	k1gFnSc2	zem
tehdy	tehdy	k6eAd1	tehdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
rozvoji	rozvoj	k1gInSc3	rozvoj
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1874	[number]	k4	1874
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
běžel	běžet	k5eAaImAgMnS	běžet
světoznámý	světoznámý	k2eAgInSc4d1	světoznámý
dostih	dostih	k1gInSc4	dostih
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hotel	hotel	k1gInSc1	hotel
Veselka	Veselka	k1gMnSc1	Veselka
(	(	kIx(	(
<g/>
zbořen	zbořen	k2eAgMnSc1d1	zbořen
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
v	v	k7c6	v
Bubeníkových	Bubeníkových	k2eAgInPc6d1	Bubeníkových
sadech	sad	k1gInPc6	sad
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
kolébkou	kolébka	k1gFnSc7	kolébka
českého	český	k2eAgNnSc2d1	české
letectví	letectví	k1gNnSc2	letectví
<g/>
,	,	kIx,	,
když	když	k8xS	když
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
inženýr	inženýr	k1gMnSc1	inženýr
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
první	první	k4xOgInSc4	první
dálkový	dálkový	k2eAgInSc4d1	dálkový
let	let	k1gInSc4	let
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Chuchle	Chuchle	k1gFnSc2	Chuchle
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
dostihovém	dostihový	k2eAgNnSc6d1	dostihové
závodišti	závodiště	k1gNnSc6	závodiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozmachu	rozmach	k1gInSc3	rozmach
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
firmy	firma	k1gFnPc1	firma
Explosia	Explosia	k1gFnSc1	Explosia
a	a	k8xC	a
Synthesia	Synthesia	k1gFnSc1	Synthesia
<g/>
,	,	kIx,	,
také	také	k9	také
Telegrafia	Telegrafia	k1gFnSc1	Telegrafia
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
konala	konat	k5eAaImAgFnS	konat
celostátní	celostátní	k2eAgFnSc1d1	celostátní
Výstava	výstava	k1gFnSc1	výstava
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
i	i	k9	i
hotel	hotel	k1gInSc1	hotel
Grand	grand	k1gMnSc1	grand
a	a	k8xC	a
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
místních	místní	k2eAgMnPc2d1	místní
odbojářů	odbojář	k1gMnPc2	odbojář
skrývali	skrývat	k5eAaImAgMnP	skrývat
členové	člen	k1gMnPc1	člen
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
skupiny	skupina	k1gFnSc2	skupina
Silver	Silver	k1gMnSc1	Silver
A	A	kA	A
Alfréd	Alfréd	k1gMnSc1	Alfréd
Bartoš	Bartoš	k1gMnSc1	Bartoš
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Valčík	valčík	k1gInSc1	valčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
byly	být	k5eAaImAgInP	být
Fantova	Fantův	k2eAgFnSc1d1	Fantova
rafinerie	rafinerie	k1gFnSc1	rafinerie
a	a	k8xC	a
pardubické	pardubický	k2eAgNnSc1d1	pardubické
letiště	letiště	k1gNnSc1	letiště
terčem	terč	k1gInSc7	terč
tří	tři	k4xCgInPc2	tři
náletů	nálet	k1gInPc2	nálet
britského	britský	k2eAgNnSc2d1	Britské
a	a	k8xC	a
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnPc4d3	veliký
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgInSc1	druhý
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
870	[number]	k4	870
tun	tuna	k1gFnPc2	tuna
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
i	i	k9	i
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
;	;	kIx,	;
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
263	[number]	k4	263
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
19	[number]	k4	19
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
budov	budova	k1gFnPc2	budova
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
nebo	nebo	k8xC	nebo
poškozeno	poškozen	k2eAgNnSc1d1	poškozeno
včetně	včetně	k7c2	včetně
staré	starý	k2eAgFnSc2d1	stará
budovy	budova	k1gFnSc2	budova
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečný	poválečný	k2eAgInSc4d1	poválečný
rozvoj	rozvoj	k1gInSc4	rozvoj
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
nových	nový	k2eAgFnPc2d1	nová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
sídliště	sídliště	k1gNnSc1	sídliště
Dukla	Dukla	k1gFnSc1	Dukla
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dřívější	dřívější	k2eAgFnSc2d1	dřívější
Válečné	válečný	k2eAgFnSc2d1	válečná
nemocnice	nemocnice	k1gFnSc2	nemocnice
Pardubice	Pardubice	k1gInPc1	Pardubice
a	a	k8xC	a
sídliště	sídliště	k1gNnSc4	sídliště
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
sídliště	sídliště	k1gNnSc4	sídliště
Višňovka	višňovka	k1gFnSc1	višňovka
a	a	k8xC	a
sídliště	sídliště	k1gNnSc1	sídliště
Na	na	k7c6	na
drážce	drážka	k1gFnSc6	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zavedena	zaveden	k2eAgFnSc1d1	zavedena
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
nové	nový	k2eAgNnSc1d1	nové
vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
stavět	stavět	k5eAaImF	stavět
sídliště	sídliště	k1gNnSc4	sídliště
Polabiny	Polabina	k1gFnSc2	Polabina
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
ještě	ještě	k6eAd1	ještě
sídliště	sídliště	k1gNnSc4	sídliště
Dubina	dubina	k1gFnSc1	dubina
<g/>
,	,	kIx,	,
Karlovina	Karlovina	k1gFnSc1	Karlovina
<g/>
,	,	kIx,	,
Závodu	závod	k1gInSc6	závod
Míru	mír	k1gInSc2	mír
a	a	k8xC	a
Pardubice-sever	Pardubiceevra	k1gFnPc2	Pardubice-sevra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
leží	ležet	k5eAaImIp3nP	ležet
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
pod	pod	k7c7	pod
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Chrudimkou	Chrudimka	k1gFnSc7	Chrudimka
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
15	[number]	k4	15
3⁄	3⁄	k?	3⁄
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
50	[number]	k4	50
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
98	[number]	k4	98
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
10	[number]	k4	10
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Chrudimi	Chrudim	k1gFnSc2	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Fytogeograficky	fytogeograficky	k6eAd1	fytogeograficky
patří	patřit	k5eAaImIp3nS	patřit
Pardubicko	Pardubicko	k1gNnSc4	Pardubicko
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
opadavých	opadavý	k2eAgInPc2d1	opadavý
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
palearktické	palearktický	k2eAgFnSc2d1	palearktická
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
Polabské	polabský	k2eAgFnSc6d1	Polabská
nížině	nížina	k1gFnSc6	nížina
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
211	[number]	k4	211
až	až	k9	až
258	[number]	k4	258
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
Stropinský	Stropinský	k2eAgInSc1d1	Stropinský
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Hostovice	Hostovice	k1gFnPc1	Hostovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pardubic	Pardubice	k1gInPc2	Pardubice
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stoupal	stoupat	k5eAaImAgMnS	stoupat
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
růst	růst	k1gInSc4	růst
po	po	k7c4	po
napojení	napojení	k1gNnSc4	napojení
města	město	k1gNnSc2	město
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
96	[number]	k4	96
036	[number]	k4	036
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
Pardubice	Pardubice	k1gInPc1	Pardubice
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
však	však	k9	však
počet	počet	k1gInSc1	počet
trvale	trvale	k6eAd1	trvale
žijících	žijící	k2eAgMnPc2d1	žijící
obyvatel	obyvatel	k1gMnPc2	obyvatel
stále	stále	k6eAd1	stále
mírně	mírně	k6eAd1	mírně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
porovnání	porovnání	k1gNnSc2	porovnání
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
okresu	okres	k1gInSc2	okres
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
soustřeďování	soustřeďování	k1gNnSc1	soustřeďování
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgMnSc4	který
podíl	podíl	k1gInSc4	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
vůči	vůči	k7c3	vůči
celému	celý	k2eAgInSc3d1	celý
regionu	region	k1gInSc3	region
mírně	mírně	k6eAd1	mírně
klesá	klesat	k5eAaImIp3nS	klesat
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dostupnějšího	dostupný	k2eAgNnSc2d2	dostupnější
bydlení	bydlení	k1gNnSc2	bydlení
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
obcích	obec	k1gFnPc6	obec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
hradecko-pardubická	hradeckoardubický	k2eAgFnSc1d1	hradecko-pardubický
aglomerace	aglomerace	k1gFnSc1	aglomerace
měla	mít	k5eAaImAgFnS	mít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
celkem	celkem	k6eAd1	celkem
335	[number]	k4	335
118	[number]	k4	118
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlastního	vlastní	k2eAgNnSc2d1	vlastní
města	město	k1gNnSc2	město
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
za	za	k7c7	za
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
743	[number]	k4	743
domech	dům	k1gInPc6	dům
25	[number]	k4	25
162	[number]	k4	162
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
11	[number]	k4	11
802	[number]	k4	802
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
23	[number]	k4	23
722	[number]	k4	722
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
551	[number]	k4	551
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
62	[number]	k4	62
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
židovské	židovská	k1gFnSc2	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
15	[number]	k4	15
823	[number]	k4	823
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
171	[number]	k4	171
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
1	[number]	k4	1
874	[number]	k4	874
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
554	[number]	k4	554
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgNnSc4d1	další
prvorepublikové	prvorepublikový	k2eAgNnSc4d1	prvorepublikové
sčítání	sčítání	k1gNnSc4	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
přineslo	přinést	k5eAaPmAgNnS	přinést
následující	následující	k2eAgInPc4d1	následující
výsledky	výsledek	k1gInPc4	výsledek
<g/>
:	:	kIx,	:
v	v	k7c4	v
2	[number]	k4	2
649	[number]	k4	649
domech	dům	k1gInPc6	dům
žilo	žít	k5eAaImAgNnS	žít
28	[number]	k4	28
846	[number]	k4	846
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
27	[number]	k4	27
703	[number]	k4	703
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
647	[number]	k4	647
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
604	[number]	k4	604
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
579	[number]	k4	579
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
5	[number]	k4	5
225	[number]	k4	225
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
518	[number]	k4	518
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
a	a	k8xC	a
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
Pardubice	Pardubice	k1gInPc4	Pardubice
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
Zeleného	Zeleného	k2eAgNnSc2d1	Zeleného
Předměstí	předměstí	k1gNnSc2	předměstí
a	a	k8xC	a
Bílého	bílý	k2eAgNnSc2d1	bílé
Předměstí	předměstí	k1gNnSc2	předměstí
(	(	kIx(	(
<g/>
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1930	[number]	k4	1930
žilo	žít	k5eAaImAgNnS	žít
43	[number]	k4	43
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
však	však	k9	však
tvořily	tvořit	k5eAaImAgFnP	tvořit
i	i	k9	i
Nové	Nové	k2eAgMnPc4d1	Nové
Jesenčany	Jesenčan	k1gMnPc4	Jesenčan
<g/>
,	,	kIx,	,
Pardubičky	Pardubička	k1gFnSc2	Pardubička
a	a	k8xC	a
Studánka	studánka	k1gFnSc1	studánka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgFnP	připojit
Pardubičky	Pardubička	k1gFnPc1	Pardubička
a	a	k8xC	a
Studánka	studánka	k1gFnSc1	studánka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Doubravice	Doubravice	k1gFnSc1	Doubravice
<g/>
,	,	kIx,	,
Cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
Ohrazenice	Ohrazenice	k1gFnSc1	Ohrazenice
<g/>
,	,	kIx,	,
Polabiny	Polabina	k1gFnPc1	Polabina
<g/>
,	,	kIx,	,
Semtín	Semtín	k1gInSc1	Semtín
<g/>
,	,	kIx,	,
Popkovice	Popkovice	k1gFnPc1	Popkovice
a	a	k8xC	a
Rosice	Rosice	k1gFnPc1	Rosice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
Trnová	trnový	k2eAgFnSc1d1	Trnová
<g/>
,	,	kIx,	,
Svítkov	Svítkov	k1gInSc1	Svítkov
a	a	k8xC	a
Srnojedy	Srnojeda	k1gMnSc2	Srnojeda
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
Dražkovice	Dražkovice	k1gFnSc1	Dražkovice
<g/>
,	,	kIx,	,
Nemošice	Nemošice	k1gFnSc1	Nemošice
<g/>
,	,	kIx,	,
Mnětice	Mnětice	k1gFnSc1	Mnětice
<g/>
,	,	kIx,	,
Drozdice	drozdice	k1gFnSc1	drozdice
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
<g/>
,	,	kIx,	,
Spojil	spojit	k5eAaPmAgMnS	spojit
a	a	k8xC	a
Staré	Staré	k2eAgMnPc4d1	Staré
Čívice	Čívic	k1gMnPc4	Čívic
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
Lány	lán	k1gInPc4	lán
na	na	k7c6	na
Důlku	důlek	k1gInSc6	důlek
a	a	k8xC	a
Opočínek	Opočínek	k1gMnSc1	Opočínek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
Spojil	spojit	k5eAaPmAgMnS	spojit
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nS	tvořit
enklávu	enkláva	k1gFnSc4	enkláva
uvnitř	uvnitř	k7c2	uvnitř
pardubického	pardubický	k2eAgNnSc2d1	pardubické
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
Srnojedy	Srnojed	k1gMnPc7	Srnojed
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
připojily	připojit	k5eAaPmAgFnP	připojit
Hostovice	Hostovice	k1gFnPc1	Hostovice
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Správní	správní	k2eAgNnSc1d1	správní
území	území	k1gNnSc1	území
Pardubic	Pardubice	k1gInPc2	Pardubice
se	se	k3xPyFc4	se
od	od	k7c2	od
připojení	připojení	k1gNnSc2	připojení
Hostovic	Hostovice	k1gFnPc2	Hostovice
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
27	[number]	k4	27
evidenčních	evidenční	k2eAgFnPc2d1	evidenční
částí	část	k1gFnPc2	část
ležících	ležící	k2eAgFnPc2d1	ležící
na	na	k7c6	na
20	[number]	k4	20
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
82,655	[number]	k4	82,655
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
zhruba	zhruba	k6eAd1	zhruba
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
evidenčním	evidenční	k2eAgFnPc3d1	evidenční
částem	část	k1gFnPc3	část
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
k.	k.	k?	k.
ú.	ú.	k?	ú.
Pardubice	Pardubice	k1gInPc4	Pardubice
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
části	část	k1gFnSc2	část
Pardubice-Staré	Pardubice-Starý	k2eAgNnSc1d1	Pardubice-Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgNnSc1d1	bílé
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgNnSc1d1	Zelené
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Cihelna	cihelna	k1gFnSc1	cihelna
a	a	k8xC	a
Polabiny	Polabina	k1gFnPc1	Polabina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
(	(	kIx(	(
<g/>
části	část	k1gFnPc4	část
Černá	černat	k5eAaImIp3nS	černat
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
a	a	k8xC	a
Žižín	Žižín	k1gInSc4	Žižín
<g/>
)	)	kIx)	)
a	a	k8xC	a
Semtín	Semtín	k1gMnSc1	Semtín
(	(	kIx(	(
<g/>
části	část	k1gFnSc6	část
Semtín	Semtína	k1gFnPc2	Semtína
a	a	k8xC	a
Doubravice	Doubravice	k1gFnSc2	Doubravice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
připojení	připojení	k1gNnSc3	připojení
obcí	obec	k1gFnPc2	obec
podél	podél	k7c2	podél
železnice	železnice	k1gFnSc2	železnice
je	být	k5eAaImIp3nS	být
protáhlé	protáhlý	k2eAgNnSc1d1	protáhlé
východo-západním	východoápadní	k2eAgInSc7d1	východo-západní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
nejvzdálenější	vzdálený	k2eAgInPc4d3	nejvzdálenější
body	bod	k1gInPc4	bod
dělí	dělit	k5eAaImIp3nS	dělit
19	[number]	k4	19
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgNnSc4d3	veliký
české	český	k2eAgNnSc4d1	české
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
domy	dům	k1gInPc1	dům
nemají	mít	k5eNaImIp3nP	mít
přiděleno	přidělen	k2eAgNnSc4d1	přiděleno
orientační	orientační	k2eAgNnSc4d1	orientační
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
sedmdesátitisícové	sedmdesátitisícový	k2eAgNnSc1d1	sedmdesátitisícové
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
mají	mít	k5eAaImIp3nP	mít
Pardubice	Pardubice	k1gInPc1	Pardubice
osm	osm	k4xCc1	osm
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
prvních	první	k4xOgNnPc2	první
sedmi	sedm	k4xCc2	sedm
obvodů	obvod	k1gInPc2	obvod
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
coby	coby	k?	coby
opatření	opatření	k1gNnSc4	opatření
proti	proti	k7c3	proti
porevolučnímu	porevoluční	k2eAgNnSc3d1	porevoluční
osamostatňování	osamostatňování	k1gNnSc3	osamostatňování
okrajových	okrajový	k2eAgFnPc2d1	okrajová
částí	část	k1gFnPc2	část
Pardubic	Pardubice	k1gInPc2	Pardubice
jako	jako	k8xC	jako
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
Spojil	spojit	k5eAaPmAgMnS	spojit
a	a	k8xC	a
Srnojedy	Srnojed	k1gMnPc4	Srnojed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
ihned	ihned	k6eAd1	ihned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
právě	právě	k6eAd1	právě
periferní	periferní	k2eAgInPc1d1	periferní
obvody	obvod	k1gInPc1	obvod
Pardubice	Pardubice	k1gInPc4	Pardubice
IV	IV	kA	IV
<g/>
,	,	kIx,	,
VI	VI	kA	VI
a	a	k8xC	a
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
obvod	obvod	k1gInSc1	obvod
Pardubice	Pardubice	k1gInPc1	Pardubice
II	II	kA	II
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
zbylé	zbylý	k2eAgInPc4d1	zbylý
tři	tři	k4xCgInPc4	tři
obvody	obvod	k1gInPc4	obvod
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc4	Pardubice
I	I	kA	I
<g/>
,	,	kIx,	,
III	III	kA	III
a	a	k8xC	a
IV	IV	kA	IV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připojením	připojení	k1gNnSc7	připojení
Hostovic	Hostovice	k1gFnPc2	Hostovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
osmý	osmý	k4xOgInSc4	osmý
městský	městský	k2eAgInSc4d1	městský
obvod	obvod	k1gInSc4	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
I	i	k9	i
–	–	k?	–
Bílé	bílý	k2eAgNnSc1d1	bílé
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pardubice-Staré	Pardubice-Starý	k2eAgNnSc1d1	Pardubice-Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgNnSc1d1	Zelené
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
II	II	kA	II
–	–	k?	–
Polabiny	Polabina	k1gFnSc2	Polabina
<g/>
,	,	kIx,	,
Cihelna	cihelna	k1gFnSc1	cihelna
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
III	III	kA	III
–	–	k?	–
Bílé	bílý	k2eAgNnSc1d1	bílé
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Studánka	studánka	k1gFnSc1	studánka
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
IV	Iva	k1gFnPc2	Iva
–	–	k?	–
Bílé	bílé	k1gNnSc4	bílé
Předměstí	předměstí	k1gNnPc2	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
<g/>
,	,	kIx,	,
Drozdice	drozdice	k1gFnPc4	drozdice
<g/>
,	,	kIx,	,
Mnětice	Mnětice	k1gFnPc4	Mnětice
<g/>
,	,	kIx,	,
Nemošice	Nemošice	k1gFnPc4	Nemošice
<g/>
,	,	kIx,	,
Pardubičky	Pardubička	k1gFnPc4	Pardubička
<g/>
,	,	kIx,	,
Staročernsko	Staročernsko	k1gNnSc1	Staročernsko
<g/>
,	,	kIx,	,
Studánka	studánka	k1gFnSc1	studánka
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žižín	Žižín	k1gMnSc1	Žižín
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
V	v	k7c4	v
–	–	k?	–
Dražkovice	Dražkovice	k1gFnPc4	Dražkovice
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgMnPc4d1	Nové
Jesenčany	Jesenčan	k1gMnPc4	Jesenčan
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgNnSc1d1	Zelené
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
VI	VI	kA	VI
–	–	k?	–
Lány	lán	k1gInPc1	lán
na	na	k7c6	na
Důlku	důlek	k1gInSc6	důlek
<g/>
,	,	kIx,	,
Opočínek	Opočínek	k1gInSc1	Opočínek
<g/>
,	,	kIx,	,
Popkovice	Popkovice	k1gFnPc1	Popkovice
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnPc1d1	Staré
Čívice	Čívice	k1gFnPc1	Čívice
<g/>
,	,	kIx,	,
Svítkov	Svítkov	k1gInSc1	Svítkov
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgNnSc1d1	Zelené
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
VII	VII	kA	VII
–	–	k?	–
Doubravice	Doubravice	k1gFnSc1	Doubravice
<g/>
,	,	kIx,	,
Ohrazenice	Ohrazenice	k1gFnSc1	Ohrazenice
<g/>
,	,	kIx,	,
Rosice	Rosice	k1gFnPc1	Rosice
<g/>
,	,	kIx,	,
Semtín	Semtín	k1gInSc1	Semtín
<g/>
,	,	kIx,	,
Trnová	trnový	k2eAgFnSc1d1	Trnová
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
VIII	VIII	kA	VIII
–	–	k?	–
HostoviceExistence	HostoviceExistence	k1gFnSc1	HostoviceExistence
obvodů	obvod	k1gInPc2	obvod
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jako	jako	k8xS	jako
zbytečně	zbytečně	k6eAd1	zbytečně
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
a	a	k8xC	a
drahá	drahý	k2eAgFnSc1d1	drahá
(	(	kIx(	(
<g/>
např.	např.	kA	např.
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
nebo	nebo	k8xC	nebo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
žádné	žádný	k3yNgMnPc4	žádný
nemají	mít	k5eNaImIp3nP	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2012	[number]	k4	2012
se	se	k3xPyFc4	se
primátorka	primátorka	k1gFnSc1	primátorka
Fraňková	Fraňková	k1gFnSc1	Fraňková
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
úsporných	úsporný	k2eAgNnPc2d1	úsporné
opatření	opatření	k1gNnPc2	opatření
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
čtyř	čtyři	k4xCgInPc2	čtyři
obvodů	obvod	k1gInPc2	obvod
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
a	a	k8xC	a
V	V	kA	V
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
spojení	spojení	k1gNnSc1	spojení
nejvýchodnějších	východní	k2eAgFnPc2d3	nejvýchodnější
Hostovic	Hostovice	k1gFnPc2	Hostovice
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
VIII	VIII	kA	VIII
<g/>
)	)	kIx)	)
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
setkávala	setkávat	k5eAaImAgFnS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
starostů	starosta	k1gMnPc2	starosta
dotyčných	dotyčný	k2eAgFnPc2d1	dotyčná
částí	část	k1gFnPc2	část
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
stran	strana	k1gFnPc2	strana
na	na	k7c6	na
magistrátu	magistrát	k1gInSc6	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkých	velký	k2eAgInPc6d1	velký
sporech	spor	k1gInPc6	spor
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
referendum	referendum	k1gNnSc1	referendum
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
"	"	kIx"	"
<g/>
Souhlasíte	souhlasit	k5eAaImIp2nP	souhlasit
s	s	k7c7	s
členěním	členění	k1gNnSc7	členění
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
na	na	k7c4	na
městské	městský	k2eAgInPc4d1	městský
obvody	obvod	k1gInPc4	obvod
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c4	na
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
21	[number]	k4	21
%	%	kIx~	%
<g/>
,	,	kIx,	,
referendum	referendum	k1gNnSc1	referendum
tedy	tedy	k9	tedy
nebylo	být	k5eNaImAgNnS	být
závazné	závazný	k2eAgNnSc1d1	závazné
<g/>
;	;	kIx,	;
60	[number]	k4	60
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
členění	členění	k1gNnSc4	členění
na	na	k7c4	na
obvody	obvod	k1gInPc4	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Instituce	instituce	k1gFnSc2	instituce
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
sídlí	sídlet	k5eAaImIp3nS	sídlet
Univerzita	univerzita	k1gFnSc1	univerzita
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
pobočka	pobočka	k1gFnSc1	pobočka
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
tu	tu	k6eAd1	tu
středisko	středisko	k1gNnSc1	středisko
Agentury	agentura	k1gFnSc2	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
Věznice	věznice	k1gFnSc1	věznice
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	let	k1gInPc6	let
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
jako	jako	k8xC	jako
zemská	zemský	k2eAgFnSc1d1	zemská
donucovací	donucovací	k2eAgFnSc1d1	donucovací
pracovna	pracovna	k1gFnSc1	pracovna
pro	pro	k7c4	pro
500	[number]	k4	500
káranců	káranec	k1gMnPc2	káranec
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velice	velice	k6eAd1	velice
moderní	moderní	k2eAgFnPc1d1	moderní
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
plány	plán	k1gInPc4	plán
a	a	k8xC	a
rozpočty	rozpočet	k1gInPc4	rozpočet
vystaveny	vystaven	k2eAgInPc4d1	vystaven
na	na	k7c6	na
Zemské	zemský	k2eAgFnSc6d1	zemská
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sloužila	sloužit	k5eAaImAgFnS	sloužit
věznice	věznice	k1gFnSc1	věznice
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
v	v	k7c6	v
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
nápravné	nápravný	k2eAgFnSc3d1	nápravná
skupině	skupina	k1gFnSc3	skupina
a	a	k8xC	a
mladistvé	mladistvý	k2eAgInPc1d1	mladistvý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zabezpečovala	zabezpečovat	k5eAaImAgFnS	zabezpečovat
zejména	zejména	k9	zejména
výkon	výkon	k1gInSc4	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
odsouzených	odsouzený	k2eAgFnPc2d1	odsouzená
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
mladistvých	mladistvý	k2eAgFnPc2d1	mladistvá
žen	žena	k1gFnPc2	žena
zařazených	zařazený	k2eAgFnPc2d1	zařazená
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
věznic	věznice	k1gFnPc2	věznice
(	(	kIx(	(
<g/>
A	a	k9	a
dohled	dohled	k1gInSc1	dohled
<g/>
,	,	kIx,	,
B	B	kA	B
dozor	dozor	k1gInSc1	dozor
<g/>
,	,	kIx,	,
C	C	kA	C
ostraha	ostraha	k1gFnSc1	ostraha
<g/>
,	,	kIx,	,
D	D	kA	D
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
ostraha	ostraha	k1gFnSc1	ostraha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
věznic	věznice	k1gFnPc2	věznice
<g/>
;	;	kIx,	;
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
výhradně	výhradně	k6eAd1	výhradně
mužská	mužský	k2eAgFnSc1d1	mužská
věznice	věznice	k1gFnSc1	věznice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jsou	být	k5eAaImIp3nP	být
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
zdejšími	zdejší	k2eAgMnPc7d1	zdejší
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
průmysl	průmysl	k1gInSc4	průmysl
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemický	chemický	k2eAgInSc1d1	chemický
===	===	k?	===
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tradici	tradice	k1gFnSc4	tradice
chemické	chemický	k2eAgFnSc2d1	chemická
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
především	především	k6eAd1	především
následující	následující	k2eAgFnPc1d1	následující
dvě	dva	k4xCgFnPc1	dva
společnosti	společnost	k1gFnPc1	společnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Paramo	Parama	k1gFnSc5	Parama
====	====	k?	====
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Paramo	Parama	k1gFnSc5	Parama
(	(	kIx(	(
<g/>
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
rafinérie	rafinérie	k1gFnSc1	rafinérie
minerálních	minerální	k2eAgInPc2d1	minerální
olejů	olej	k1gInPc2	olej
<g/>
)	)	kIx)	)
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
české	český	k2eAgFnPc4d1	Česká
rafinerie	rafinerie	k1gFnPc4	rafinerie
<g/>
;	;	kIx,	;
vyráběla	vyrábět	k5eAaImAgNnP	vyrábět
paliva	palivo	k1gNnPc1	palivo
<g/>
,	,	kIx,	,
maziva	mazivo	k1gNnPc1	mazivo
<g/>
,	,	kIx,	,
asfalty	asfalt	k1gInPc1	asfalt
a	a	k8xC	a
asfaltové	asfaltový	k2eAgInPc1d1	asfaltový
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
majitel	majitel	k1gMnSc1	majitel
krámku	krámek	k1gInSc2	krámek
David	David	k1gMnSc1	David
Fanto	Fanta	k1gMnSc5	Fanta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prodával	prodávat	k5eAaImAgInS	prodávat
petrolej	petrolej	k1gInSc4	petrolej
na	na	k7c4	na
litry	litr	k1gInPc4	litr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nevedl	vést	k5eNaImAgMnS	vést
si	se	k3xPyFc3	se
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zřídit	zřídit	k5eAaPmF	zřídit
vlastní	vlastní	k2eAgInSc1d1	vlastní
závod	závod	k1gInSc1	závod
na	na	k7c4	na
destilaci	destilace	k1gFnSc4	destilace
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
rafinaci	rafinace	k1gFnSc4	rafinace
petroleje	petrolej	k1gInSc2	petrolej
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
nalezl	nalézt	k5eAaBmAgMnS	nalézt
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nejen	nejen	k6eAd1	nejen
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Usazování	usazování	k1gNnSc4	usazování
prvních	první	k4xOgInPc2	první
kotlů	kotel	k1gInPc2	kotel
a	a	k8xC	a
kladení	kladení	k1gNnSc1	kladení
potrubí	potrubí	k1gNnSc2	potrubí
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oleje	olej	k1gInPc1	olej
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
vřetenové	vřetenový	k2eAgFnPc1d1	vřetenová
<g/>
,	,	kIx,	,
vazelínové	vazelínový	k2eAgFnPc1d1	vazelínový
<g/>
,	,	kIx,	,
válcové	válcový	k2eAgFnPc1d1	válcová
i	i	k8xC	i
těžké	těžký	k2eAgFnPc1d1	těžká
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
až	až	k9	až
dosud	dosud	k6eAd1	dosud
všeobecně	všeobecně	k6eAd1	všeobecně
uznávané	uznávaný	k2eAgInPc1d1	uznávaný
americké	americký	k2eAgInPc1d1	americký
výrobky	výrobek	k1gInPc1	výrobek
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvojnásobném	dvojnásobný	k2eAgNnSc6d1	dvojnásobné
bombardování	bombardování	k1gNnSc1	bombardování
americkým	americký	k2eAgNnSc7d1	americké
letectvem	letectvo	k1gNnSc7	letectvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
sotva	sotva	k8xS	sotva
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
rafinerie	rafinerie	k1gFnSc1	rafinerie
začala	začít	k5eAaPmAgFnS	začít
obnovovat	obnovovat	k5eAaImF	obnovovat
jako	jako	k9	jako
národní	národní	k2eAgInSc4d1	národní
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
patřilo	patřit	k5eAaImAgNnS	patřit
Paramo	Parama	k1gFnSc5	Parama
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
a	a	k8xC	a
nejznámější	známý	k2eAgFnPc4d3	nejznámější
petrochemické	petrochemický	k2eAgFnPc4d1	petrochemická
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
majitelé	majitel	k1gMnPc1	majitel
provedli	provést	k5eAaPmAgMnP	provést
částečné	částečný	k2eAgNnSc4d1	částečné
utlumení	utlumení	k1gNnSc4	utlumení
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
trvale	trvale	k6eAd1	trvale
odstaveny	odstaven	k2eAgFnPc1d1	odstavena
výrobní	výrobní	k2eAgFnPc1d1	výrobní
jednotky	jednotka	k1gFnPc1	jednotka
zpracovávající	zpracovávající	k2eAgFnSc4d1	zpracovávající
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
významným	významný	k2eAgMnSc7d1	významný
výrobcem	výrobce	k1gMnSc7	výrobce
asfaltů	asfalt	k1gInPc2	asfalt
<g/>
,	,	kIx,	,
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
rafinérských	rafinérský	k2eAgInPc2d1	rafinérský
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Synthesia	Synthesius	k1gMnSc4	Synthesius
a	a	k8xC	a
Explosia	Explosius	k1gMnSc4	Explosius
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Semtíně	Semtín	k1gInSc6	Semtín
založena	založit	k5eAaPmNgFnS	založit
Československá	československý	k2eAgFnSc1d1	Československá
akciová	akciový	k2eAgFnSc1d1	akciová
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
látky	látka	k1gFnPc4	látka
výbušné	výbušný	k2eAgFnPc4d1	výbušná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sesterská	sesterský	k2eAgFnSc1d1	sesterská
společnost	společnost	k1gFnSc1	společnost
Synthesia	Synthesium	k1gNnSc2	Synthesium
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
podnik	podnik	k1gInSc1	podnik
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
na	na	k7c4	na
Explosia	Explosius	k1gMnSc4	Explosius
a.s.	a.s.	k?	a.s.
Roku	rok	k1gInSc6	rok
1946	[number]	k4	1946
sloučením	sloučení	k1gNnSc7	sloučení
znárodněné	znárodněný	k2eAgFnSc2d1	znárodněná
Synthesie	Synthesie	k1gFnSc2	Synthesie
a	a	k8xC	a
Explosie	Explosie	k1gFnSc2	Explosie
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Synthesia	Synthesium	k1gNnPc4	Synthesium
<g/>
,	,	kIx,	,
národní	národní	k2eAgInPc4d1	národní
podnik	podnik	k1gInSc4	podnik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaný	přejmenovaný	k2eAgMnSc1d1	přejmenovaný
na	na	k7c4	na
Východočeské	východočeský	k2eAgInPc4d1	východočeský
chemické	chemický	k2eAgInPc4d1	chemický
závody	závod	k1gInPc4	závod
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Bouřlivým	bouřlivý	k2eAgInSc7d1	bouřlivý
rozvojem	rozvoj	k1gInSc7	rozvoj
prošel	projít	k5eAaPmAgInS	projít
především	především	k9	především
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synthesia	Synthesia	k1gFnSc1	Synthesia
a.s.	a.s.	k?	a.s.
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgMnPc4d1	přední
české	český	k2eAgMnPc4d1	český
výrobce	výrobce	k1gMnPc4	výrobce
celulosy	celulosa	k1gFnSc2	celulosa
<g/>
,	,	kIx,	,
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
zde	zde	k6eAd1	zde
na	na	k7c4	na
2300	[number]	k4	2300
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
tržby	tržba	k1gFnSc2	tržba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
činily	činit	k5eAaImAgInP	činit
3	[number]	k4	3
570	[number]	k4	570
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Explosia	Explosia	k1gFnSc1	Explosia
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
přední	přední	k2eAgNnSc4d1	přední
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trhavin	trhavina	k1gFnPc2	trhavina
a	a	k8xC	a
střelivin	střelivina	k1gFnPc2	střelivina
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
významným	významný	k2eAgMnSc7d1	významný
exportérem	exportér	k1gMnSc7	exportér
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
plastická	plastický	k2eAgFnSc1d1	plastická
trhavina	trhavina	k1gFnSc1	trhavina
Semtex	semtex	k1gInSc1	semtex
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kombinací	kombinace	k1gFnSc7	kombinace
slov	slovo	k1gNnPc2	slovo
Semtín	Semtín	k1gInSc4	Semtín
a	a	k8xC	a
Explosive	Explosiev	k1gFnPc4	Explosiev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elektrotechnický	elektrotechnický	k2eAgMnSc1d1	elektrotechnický
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
rozvíjen	rozvíjet	k5eAaImNgInS	rozvíjet
především	především	k6eAd1	především
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgFnSc6d1	vybudovaná
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
poblíž	poblíž	k7c2	poblíž
Starých	Starých	k2eAgInPc2d1	Starých
Čivic	Čivice	k1gInPc2	Čivice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pardubického	pardubický	k2eAgInSc2d1	pardubický
závodu	závod	k1gInSc2	závod
Tesla	Tesla	k1gFnSc1	Tesla
pocházely	pocházet	k5eAaImAgFnP	pocházet
pasivní	pasivní	k2eAgInPc4d1	pasivní
radary	radar	k1gInPc4	radar
Ramona	Ramona	k1gFnSc1	Ramona
a	a	k8xC	a
KRTP-86	KRTP-86	k1gFnSc1	KRTP-86
Tamara	Tamara	k1gFnSc1	Tamara
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
založena	založit	k5eAaPmNgFnS	založit
ERA	ERA	kA	ERA
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
další	další	k2eAgFnSc4d1	další
generaci	generace	k1gFnSc4	generace
pasivního	pasivní	k2eAgInSc2d1	pasivní
radiolokátoru	radiolokátor	k1gInSc2	radiolokátor
Věra	Věra	k1gFnSc1	Věra
<g/>
.	.	kIx.	.
</s>
<s>
ERA	ERA	kA	ERA
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
dodává	dodávat	k5eAaImIp3nS	dodávat
pasivní	pasivní	k2eAgInSc4d1	pasivní
radiolokační	radiolokační	k2eAgInSc4d1	radiolokační
systémy	systém	k1gInPc4	systém
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
letového	letový	k2eAgInSc2d1	letový
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
sledování	sledování	k1gNnSc2	sledování
pohybu	pohyb	k1gInSc2	pohyb
letadel	letadlo	k1gNnPc2	letadlo
jak	jak	k8xS	jak
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
letištních	letištní	k2eAgFnPc6d1	letištní
plochách	plocha	k1gFnPc6	plocha
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sledovat	sledovat	k5eAaImF	sledovat
i	i	k9	i
pohyb	pohyb	k1gInSc4	pohyb
jiných	jiný	k2eAgNnPc2d1	jiné
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
její	její	k3xOp3gInPc1	její
výrobky	výrobek	k1gInPc1	výrobek
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
všech	všecek	k3xTgInPc2	všecek
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
patří	patřit	k5eAaImIp3nS	patřit
zbrojařskému	zbrojařský	k2eAgNnSc3d1	zbrojařské
koncernu	koncern	k1gInSc2	koncern
Omnipol	omnipol	k1gInSc1	omnipol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
má	mít	k5eAaImIp3nS	mít
začít	začít	k5eAaPmF	začít
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
administrativní	administrativní	k2eAgFnSc2d1	administrativní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Foxconn	Foxconn	k1gInSc1	Foxconn
je	být	k5eAaImIp3nS	být
tchajwanská	tchajwanský	k2eAgFnSc1d1	tchajwanská
globální	globální	k2eAgFnSc1d1	globální
firma	firma	k1gFnSc1	firma
<g/>
;	;	kIx,	;
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
spotřební	spotřební	k2eAgFnSc4d1	spotřební
elektroniku	elektronika	k1gFnSc4	elektronika
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgFnSc4d1	komunikační
a	a	k8xC	a
elektronická	elektronický	k2eAgNnPc1d1	elektronické
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
součásti	součást	k1gFnPc4	součást
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
kromě	kromě	k7c2	kromě
čipů	čip	k1gInPc2	čip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2000	[number]	k4	2000
montovnu	montovna	k1gFnSc4	montovna
počítačů	počítač	k1gInPc2	počítač
s	s	k7c7	s
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
<g/>
Továrna	továrna	k1gFnSc1	továrna
společnosti	společnost	k1gFnSc2	společnost
Panasonic	Panasonic	kA	Panasonic
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
audiosystémy	audiosystém	k1gInPc4	audiosystém
do	do	k7c2	do
automobilů	automobil	k1gInPc2	automobil
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
i	i	k8xC	i
mimoevropský	mimoevropský	k2eAgInSc4d1	mimoevropský
trh	trh	k1gInSc4	trh
<g/>
;	;	kIx,	;
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potravinářství	potravinářství	k1gNnSc3	potravinářství
<g/>
:	:	kIx,	:
perník	perník	k1gInSc4	perník
a	a	k8xC	a
pivo	pivo	k1gNnSc4	pivo
===	===	k?	===
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proslulé	proslulý	k2eAgNnSc4d1	proslulé
perníkem	perník	k1gInSc7	perník
<g/>
;	;	kIx,	;
tradiční	tradiční	k2eAgNnSc1d1	tradiční
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
pardubický	pardubický	k2eAgInSc1d1	pardubický
perník	perník	k1gInSc1	perník
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc1d1	chráněné
označení	označení	k1gNnSc1	označení
původu	původ	k1gInSc2	původ
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
výrobce	výrobce	k1gMnSc1	výrobce
náhražky	náhražka	k1gFnSc2	náhražka
kávy	káva	k1gFnSc2	káva
Kávoviny	kávovina	k1gFnSc2	kávovina
nebo	nebo	k8xC	nebo
pekárna	pekárna	k1gFnSc1	pekárna
Odkolek	odkolek	k1gInSc1	odkolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozvoj	rozvoj	k1gInSc1	rozvoj
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Viléma	Vilém	k1gMnSc2	Vilém
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
Pivovar	pivovar	k1gInSc1	pivovar
Pardubice	Pardubice	k1gInPc1	Pardubice
transformován	transformován	k2eAgInSc1d1	transformován
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
zprivatizován	zprivatizován	k2eAgInSc1d1	zprivatizován
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Pernštejn	Pernštejn	k1gInSc4	Pernštejn
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
limonády	limonáda	k1gFnSc2	limonáda
<g/>
;	;	kIx,	;
originální	originální	k2eAgFnSc1d1	originální
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
<g/>
°	°	k?	°
tmavé	tmavý	k2eAgNnSc1d1	tmavé
pivo	pivo	k1gNnSc1	pivo
Porter	porter	k1gInSc4	porter
vlastní	vlastní	k2eAgFnSc2d1	vlastní
receptury	receptura	k1gFnSc2	receptura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
malý	malý	k2eAgInSc1d1	malý
nezávislý	závislý	k2eNgInSc1d1	nezávislý
pivovar	pivovar	k1gInSc1	pivovar
nepatřící	patřící	k2eNgInSc1d1	nepatřící
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
ho	on	k3xPp3gNnSc2	on
několik	několik	k4yIc1	několik
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
regionu	region	k1gInSc2	region
<g/>
;	;	kIx,	;
výstav	výstav	k1gInSc1	výstav
je	být	k5eAaImIp3nS	být
57	[number]	k4	57
tisíc	tisíc	k4xCgInPc2	tisíc
hektolitrů	hektolitr	k1gInPc2	hektolitr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
Pivovar	pivovar	k1gInSc4	pivovar
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
expanzi	expanze	k1gFnSc3	expanze
mimo	mimo	k7c4	mimo
region	region	k1gInSc4	region
to	ten	k3xDgNnSc1	ten
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
Pardubický	pardubický	k2eAgInSc4d1	pardubický
pivovar	pivovar	k1gInSc4	pivovar
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
MHD	MHD	kA	MHD
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
trolejbusy	trolejbus	k1gInPc1	trolejbus
vyjely	vyjet	k5eAaPmAgInP	vyjet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
poměrně	poměrně	k6eAd1	poměrně
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tratě	trať	k1gFnSc2	trať
na	na	k7c4	na
Jesničánky	Jesničánka	k1gFnPc4	Jesničánka
<g/>
,	,	kIx,	,
Slovany	Slovan	k1gInPc4	Slovan
<g/>
,	,	kIx,	,
do	do	k7c2	do
Ohrazenic	Ohrazenice	k1gFnPc2	Ohrazenice
<g/>
,	,	kIx,	,
Židova	Židův	k2eAgFnSc1d1	Židův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ale	ale	k9	ale
nastala	nastat	k5eAaPmAgFnS	nastat
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
nová	nový	k2eAgFnSc1d1	nová
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Polabiny	Polabina	k1gFnPc4	Polabina
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
úseků	úsek	k1gInPc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
křižovatky	křižovatka	k1gFnPc1	křižovatka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
před	před	k7c7	před
hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
výhradně	výhradně	k6eAd1	výhradně
nízkopodlažními	nízkopodlažní	k2eAgInPc7d1	nízkopodlažní
vozy	vůz	k1gInPc7	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Velkokapacitní	velkokapacitní	k2eAgInPc1d1	velkokapacitní
trolejbusy	trolejbus	k1gInPc1	trolejbus
Škoda	škoda	k1gFnSc1	škoda
Solaris	Solaris	k1gInSc1	Solaris
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
vítězích	vítěz	k1gMnPc6	vítěz
Velké	velká	k1gFnSc2	velká
Pardubické	pardubický	k2eAgInPc1d1	pardubický
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
autobusy	autobus	k1gInPc1	autobus
mají	mít	k5eAaImIp3nP	mít
pohon	pohon	k1gInSc4	pohon
na	na	k7c4	na
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
areálu	areál	k1gInSc6	areál
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
stanici	stanice	k1gFnSc4	stanice
se	s	k7c7	s
zemním	zemní	k2eAgInSc7d1	zemní
plynem	plyn	k1gInSc7	plyn
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
i	i	k9	i
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
přepraví	přepravit	k5eAaPmIp3nS	přepravit
každoročně	každoročně	k6eAd1	každoročně
okolo	okolo	k7c2	okolo
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc1	linka
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
Opočínku	Opočínek	k1gInSc2	Opočínek
<g/>
,	,	kIx,	,
Lánů	lán	k1gInPc2	lán
na	na	k7c6	na
Důlku	důlek	k1gInSc6	důlek
<g/>
,	,	kIx,	,
Lázní	lázeň	k1gFnPc2	lázeň
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
<g/>
,	,	kIx,	,
Němčic	Němčice	k1gFnPc2	Němčice
<g/>
,	,	kIx,	,
Sezemic	Sezemice	k1gFnPc2	Sezemice
<g/>
,	,	kIx,	,
Spojila	spojit	k5eAaPmAgFnS	spojit
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgInPc4d1	Černé
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
<g/>
,	,	kIx,	,
Starého	Starého	k2eAgNnSc2d1	Starého
Mateřova	Mateřův	k2eAgNnSc2d1	Mateřův
a	a	k8xC	a
Dražkovic	Dražkovice	k1gFnPc2	Dražkovice
<g/>
;	;	kIx,	;
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
linka	linka	k1gFnSc1	linka
do	do	k7c2	do
Chrudimi	Chrudim	k1gFnSc2	Chrudim
a	a	k8xC	a
prodloužení	prodloužení	k1gNnSc2	prodloužení
trolejí	trolej	k1gFnPc2	trolej
do	do	k7c2	do
Černé	Černá	k1gFnSc2	Černá
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
a	a	k8xC	a
do	do	k7c2	do
Ohrazenic	Ohrazenice	k1gFnPc2	Ohrazenice
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
budoucím	budoucí	k2eAgNnSc6d1	budoucí
propojení	propojení	k1gNnSc6	propojení
Pardubic	Pardubice	k1gInPc2	Pardubice
s	s	k7c7	s
Hradcem	Hradec	k1gInSc7	Hradec
linkami	linka	k1gFnPc7	linka
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Víkendové	víkendový	k2eAgInPc1d1	víkendový
spoje	spoj	k1gInPc1	spoj
linky	linka	k1gFnSc2	linka
15	[number]	k4	15
zajížděly	zajíždět	k5eAaImAgFnP	zajíždět
i	i	k9	i
do	do	k7c2	do
Přelouče	Přelouč	k1gFnSc2	Přelouč
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
zrušeny	zrušen	k2eAgInPc1d1	zrušen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jsou	být	k5eAaImIp3nP	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
104	[number]	k4	104
km	km	kA	km
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c6	na
koridorové	koridorový	k2eAgFnSc6d1	koridorová
trati	trať	k1gFnSc6	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
vycházejí	vycházet	k5eAaImIp3nP	vycházet
odtud	odtud	k6eAd1	odtud
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Pardubice	Pardubice	k1gInPc4	Pardubice
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
mnoho	mnoho	k4c1	mnoho
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
EuroCity	EuroCita	k1gFnSc2	EuroCita
či	či	k8xC	či
Railjet	Railjeta	k1gFnPc2	Railjeta
do	do	k7c2	do
měst	město	k1gNnPc2	město
jako	jako	k8xS	jako
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
expresy	expres	k1gInPc1	expres
a	a	k8xC	a
vnitrostátní	vnitrostátní	k2eAgInPc1d1	vnitrostátní
vlaky	vlak	k1gInPc1	vlak
InterCity	InterCit	k2eAgInPc1d1	InterCit
<g/>
.	.	kIx.	.
</s>
<s>
Zastavují	zastavovat	k5eAaImIp3nP	zastavovat
tu	tu	k6eAd1	tu
vlaky	vlak	k1gInPc1	vlak
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
i	i	k9	i
vlaky	vlak	k1gInPc1	vlak
soukromých	soukromý	k2eAgMnPc2d1	soukromý
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pardubického	pardubický	k2eAgNnSc2d1	pardubické
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
a	a	k8xC	a
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
moderní	moderní	k2eAgFnSc2d1	moderní
příměstské	příměstský	k2eAgFnSc2d1	příměstská
soupravy	souprava	k1gFnSc2	souprava
CityElefant	CityElefanta	k1gFnPc2	CityElefanta
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
Jaroměře	Jaroměř	k1gFnSc2	Jaroměř
elektrické	elektrický	k2eAgFnSc2d1	elektrická
jednotky	jednotka	k1gFnSc2	jednotka
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
RegioPanter	RegioPanter	k1gMnSc1	RegioPanter
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jezdí	jezdit	k5eAaImIp3nS	jezdit
několikrát	několikrát	k6eAd1	několikrát
za	za	k7c4	za
den	den	k1gInSc4	den
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
do	do	k7c2	do
Chrudimi	Chrudim	k1gFnSc2	Chrudim
a	a	k8xC	a
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
také	také	k6eAd1	také
moderní	moderní	k2eAgFnPc1d1	moderní
motorové	motorový	k2eAgFnPc1d1	motorová
soupravy	souprava	k1gFnPc1	souprava
RegioShark	RegioShark	k1gInSc4	RegioShark
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Pardubice	Pardubice	k1gInPc4	Pardubice
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
s	s	k7c7	s
odbavovací	odbavovací	k2eAgFnSc7d1	odbavovací
budovou	budova	k1gFnSc7	budova
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Karla	Karel	k1gMnSc2	Karel
Řepy	Řepa	k1gMnSc2	Řepa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
staveb	stavba	k1gFnPc2	stavba
poválečného	poválečný	k2eAgInSc2d1	poválečný
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
s	s	k7c7	s
prostornou	prostorný	k2eAgFnSc7d1	prostorná
odbavovací	odbavovací	k2eAgFnSc7d1	odbavovací
halou	hala	k1gFnSc7	hala
<g/>
,	,	kIx,	,
hotelem	hotel	k1gInSc7	hotel
a	a	k8xC	a
podzemním	podzemní	k2eAgNnSc7d1	podzemní
kinem	kino	k1gNnSc7	kino
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
nefunguje	fungovat	k5eNaImIp3nS	fungovat
a	a	k8xC	a
hotel	hotel	k1gInSc1	hotel
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
obsazen	obsadit	k5eAaPmNgMnS	obsadit
kancelářemi	kancelář	k1gFnPc7	kancelář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
plánují	plánovat	k5eAaImIp3nP	plánovat
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
nádražní	nádražní	k2eAgFnSc2d1	nádražní
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
investorem	investor	k1gMnSc7	investor
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
náměstí	náměstí	k1gNnSc2	náměstí
Jana	Jan	k1gMnSc2	Jan
Pernera	Perner	k1gMnSc2	Perner
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
ostrovní	ostrovní	k2eAgNnPc4d1	ostrovní
nástupiště	nástupiště	k1gNnPc4	nástupiště
s	s	k7c7	s
průjezdnými	průjezdný	k2eAgFnPc7d1	průjezdná
kolejemi	kolej	k1gFnPc7	kolej
(	(	kIx(	(
<g/>
6	[number]	k4	6
nástupištních	nástupištní	k2eAgFnPc2d1	nástupištní
hran	hrana	k1gFnPc2	hrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednostranné	jednostranný	k2eAgFnPc4d1	jednostranná
I.	I.	kA	I.
nástupiště	nástupiště	k1gNnSc1	nástupiště
u	u	k7c2	u
výpravní	výpravní	k2eAgFnSc2d1	výpravní
koleje	kolej	k1gFnSc2	kolej
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
nástupištními	nástupištní	k2eAgFnPc7d1	nástupištní
hranami	hrana	k1gFnPc7	hrana
u	u	k7c2	u
kusých	kusý	k2eAgFnPc2d1	kusá
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
až	až	k9	až
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nástupiště	nástupiště	k1gNnSc4	nástupiště
s	s	k7c7	s
přestupovým	přestupový	k2eAgInSc7d1	přestupový
tunelem	tunel	k1gInSc7	tunel
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
modernizovány	modernizován	k2eAgInPc1d1	modernizován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
stanicí	stanice	k1gFnSc7	stanice
je	být	k5eAaImIp3nS	být
Pardubice-Rosice	Pardubice-Rosice	k1gFnSc1	Pardubice-Rosice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
031	[number]	k4	031
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
budova	budova	k1gFnSc1	budova
prošla	projít	k5eAaPmAgFnS	projít
opravou	oprava	k1gFnSc7	oprava
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
šest	šest	k4xCc4	šest
železničních	železniční	k2eAgFnPc2d1	železniční
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
:	:	kIx,	:
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
Pardubice-Svítkov	Pardubice-Svítkov	k1gInSc1	Pardubice-Svítkov
a	a	k8xC	a
Pardubice-Opočínek	Pardubice-Opočínek	k1gInSc1	Pardubice-Opočínek
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
Pardubice-Pardubičky	Pardubice-Pardubička	k1gFnSc2	Pardubice-Pardubička
a	a	k8xC	a
Pardubice-Černá	Pardubice-Černý	k2eAgFnSc1d1	Pardubice-Černý
za	za	k7c4	za
Bory	bor	k1gInPc4	bor
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Pardubice	Pardubice	k1gInPc4	Pardubice
závodiště	závodiště	k1gNnSc2	závodiště
a	a	k8xC	a
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Pardubice-Semtín	Pardubice-Semtína	k1gFnPc2	Pardubice-Semtína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
Pardubic	Pardubice	k1gInPc2	Pardubice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vojenské	vojenský	k2eAgFnPc4d1	vojenská
a	a	k8xC	a
civilní	civilní	k2eAgFnPc4d1	civilní
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
civilní	civilní	k2eAgFnSc6d1	civilní
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
odbaven	odbavit	k5eAaPmNgInS	odbavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
184	[number]	k4	184
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
nový	nový	k2eAgInSc1d1	nový
terminál	terminál	k1gInSc1	terminál
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
kapacitou	kapacita	k1gFnSc7	kapacita
a	a	k8xC	a
moderním	moderní	k2eAgNnSc7d1	moderní
zázemím	zázemí	k1gNnSc7	zázemí
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
dle	dle	k7c2	dle
pardubického	pardubický	k2eAgMnSc2d1	pardubický
průkopníka	průkopník	k1gMnSc2	průkopník
aviatiky	aviatika	k1gFnSc2	aviatika
Jana	Jan	k1gMnSc2	Jan
Kašpara	Kašpar	k1gMnSc2	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
linka	linka	k1gFnSc1	linka
společnosti	společnost	k1gFnSc2	společnost
Ryanair	Ryanaira	k1gFnPc2	Ryanaira
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
koná	konat	k5eAaImIp3nS	konat
Aviatická	aviatický	k2eAgFnSc1d1	Aviatická
pouť	pouť	k1gFnSc1	pouť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
láká	lákat	k5eAaImIp3nS	lákat
mnohé	mnohý	k2eAgMnPc4d1	mnohý
letecké	letecký	k2eAgMnPc4d1	letecký
nadšence	nadšenec	k1gMnPc4	nadšenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Pardubice	Pardubice	k1gInPc4	Pardubice
protíná	protínat	k5eAaImIp3nS	protínat
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
z	z	k7c2	z
Chrudimi	Chrudim	k1gFnSc2	Chrudim
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
zmodernizovaná	zmodernizovaný	k2eAgFnSc1d1	zmodernizovaná
na	na	k7c4	na
čtyřproudovou	čtyřproudový	k2eAgFnSc4d1	čtyřproudová
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
<g/>
;	;	kIx,	;
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Opatovic	Opatovice	k1gFnPc2	Opatovice
ji	on	k3xPp3gFnSc4	on
úsek	úsek	k1gInSc1	úsek
budoucí	budoucí	k2eAgFnSc1d1	budoucí
dálnice	dálnice	k1gFnSc1	dálnice
D35	D35	k1gFnSc2	D35
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
D11	D11	k1gMnSc2	D11
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
exit	exit	k1gInSc1	exit
68	[number]	k4	68
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
na	na	k7c6	na
Chýšť	Chýšť	k1gFnSc6	Chýšť
Lázně	lázeň	k1gFnSc2	lázeň
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
prochází	procházet	k5eAaImIp3nS	procházet
širším	široký	k2eAgNnSc7d2	širší
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
do	do	k7c2	do
Sezemic	Sezemice	k1gFnPc2	Sezemice
<g/>
,	,	kIx,	,
Holic	Holice	k1gFnPc2	Holice
<g/>
,	,	kIx,	,
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
Mýta	mýto	k1gNnSc2	mýto
a	a	k8xC	a
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jako	jako	k8xS	jako
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
město	město	k1gNnSc4	město
generují	generovat	k5eAaImIp3nP	generovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
politici	politik	k1gMnPc1	politik
navázali	navázat	k5eAaPmAgMnP	navázat
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
uvnitř	uvnitř	k7c2	uvnitř
Pardubic	Pardubice	k1gInPc2	Pardubice
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
obchvaty	obchvat	k1gInPc1	obchvat
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
a	a	k8xC	a
severovýchodu	severovýchod	k1gInSc2	severovýchod
se	se	k3xPyFc4	se
ale	ale	k9	ale
nečekají	čekat	k5eNaImIp3nP	čekat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2025	[number]	k4	2025
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nekvalitně	kvalitně	k6eNd1	kvalitně
navržený	navržený	k2eAgInSc1d1	navržený
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
systém	systém	k1gInSc1	systém
dopravy	doprava	k1gFnSc2	doprava
vede	vést	k5eAaImIp3nS	vést
veškerou	veškerý	k3xTgFnSc4	veškerý
dopravu	doprava	k1gFnSc4	doprava
včetně	včetně	k7c2	včetně
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
mají	mít	k5eAaImIp3nP	mít
Pardubice	Pardubice	k1gInPc1	Pardubice
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nejvytíženější	vytížený	k2eAgInSc4d3	nejvytíženější
dopravní	dopravní	k2eAgInSc4d1	dopravní
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
nadjezd	nadjezd	k1gInSc4	nadjezd
a	a	k8xC	a
křižovatku	křižovatka	k1gFnSc4	křižovatka
u	u	k7c2	u
Parama	Parama	k?	Parama
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
denně	denně	k6eAd1	denně
projede	projet	k5eAaPmIp3nS	projet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
tisíc	tisíc	k4xCgInSc4	tisíc
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
a	a	k8xC	a
modernizací	modernizace	k1gFnSc7	modernizace
až	až	k6eAd1	až
ke	k	k7c3	k
křižovatce	křižovatka	k1gFnSc3	křižovatka
U	u	k7c2	u
Trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
plynule	plynule	k6eAd1	plynule
napojí	napojit	k5eAaPmIp3nS	napojit
na	na	k7c4	na
čtyřproudovou	čtyřproudový	k2eAgFnSc4d1	čtyřproudová
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
bude	být	k5eAaImBp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
chytrý	chytrý	k2eAgInSc1d1	chytrý
systém	systém	k1gInSc1	systém
dopravního	dopravní	k2eAgNnSc2d1	dopravní
světelného	světelný	k2eAgNnSc2d1	světelné
značení	značení	k1gNnSc2	značení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
dopravu	doprava	k1gFnSc4	doprava
dle	dle	k7c2	dle
její	její	k3xOp3gFnSc2	její
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šlo	jít	k5eAaImAgNnS	jít
plynule	plynule	k6eAd1	plynule
projet	projet	k5eAaPmF	projet
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
informační	informační	k2eAgFnPc4d1	informační
tabule	tabule	k1gFnPc4	tabule
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
řidičům	řidič	k1gMnPc3	řidič
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
nejrychleji	rychle	k6eAd3	rychle
projet	projet	k5eAaPmF	projet
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
informovat	informovat	k5eAaBmF	informovat
o	o	k7c6	o
případných	případný	k2eAgNnPc6d1	případné
dopravních	dopravní	k2eAgNnPc6d1	dopravní
omezeních	omezení	k1gNnPc6	omezení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
funguje	fungovat	k5eAaImIp3nS	fungovat
nově	nově	k6eAd1	nově
opravená	opravený	k2eAgFnSc1d1	opravená
výletní	výletní	k2eAgFnSc1d1	výletní
loď	loď	k1gFnSc1	loď
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g/>
,	,	kIx,	,
plující	plující	k2eAgMnSc1d1	plující
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
do	do	k7c2	do
Kunětic	Kunětice	k1gFnPc2	Kunětice
a	a	k8xC	a
dolů	dol	k1gInPc2	dol
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
do	do	k7c2	do
Srnojed	Srnojed	k1gMnSc1	Srnojed
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
za	za	k7c4	za
opravu	oprava	k1gFnSc4	oprava
lodi	loď	k1gFnSc2	loď
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
funguje	fungovat	k5eAaImIp3nS	fungovat
kromě	kromě	k7c2	kromě
vyhlídkových	vyhlídkový	k2eAgFnPc2d1	vyhlídková
plaveb	plavba	k1gFnPc2	plavba
i	i	k8xC	i
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
okraje	okraj	k1gInPc4	okraj
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	se	k3xPyFc4	se
také	také	k9	také
chystá	chystat	k5eAaImIp3nS	chystat
nákladní	nákladní	k2eAgFnSc1d1	nákladní
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
splavnění	splavnění	k1gNnSc6	splavnění
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ulehčit	ulehčit	k5eAaPmF	ulehčit
silniční	silniční	k2eAgFnSc3d1	silniční
dopravě	doprava	k1gFnSc3	doprava
a	a	k8xC	a
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xS	jako
doplněk	doplněk	k1gInSc1	doplněk
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
každý	každý	k3xTgInSc4	každý
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
slavná	slavný	k2eAgFnSc1d1	slavná
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
koňský	koňský	k2eAgInSc4d1	koňský
dostih	dostih	k1gInSc4	dostih
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
účastníkem	účastník	k1gMnSc7	účastník
dostihu	dostih	k1gInSc2	dostih
je	být	k5eAaImIp3nS	být
žokej	žokej	k1gMnSc1	žokej
Josef	Josef	k1gMnSc1	Josef
Váňa	Váňa	k1gMnSc1	Váňa
s	s	k7c7	s
osmi	osm	k4xCc7	osm
vítězstvími	vítězství	k1gNnPc7	vítězství
v	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
závodní	závodní	k2eAgFnSc1d1	závodní
dráha	dráha	k1gFnSc1	dráha
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
a	a	k8xC	a
nezvyklé	zvyklý	k2eNgNnSc4d1	nezvyklé
uspořádání	uspořádání	k1gNnSc4	uspořádání
překážek	překážka	k1gFnPc2	překážka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
a	a	k8xC	a
obtížná	obtížný	k2eAgFnSc1d1	obtížná
závodní	závodní	k2eAgFnSc1d1	závodní
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
steeplechase	steeplechase	k1gFnSc1	steeplechase
se	se	k3xPyFc4	se
běžela	běžet	k5eAaImAgFnS	běžet
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1874	[number]	k4	1874
o	o	k7c4	o
8000	[number]	k4	8000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
startu	start	k1gInSc6	start
stálo	stát	k5eAaImAgNnS	stát
14	[number]	k4	14
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
plnokrevný	plnokrevný	k2eAgMnSc1d1	plnokrevný
hřebec	hřebec	k1gMnSc1	hřebec
Fantome	fantom	k1gInSc5	fantom
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
žokejem	žokej	k1gMnSc7	žokej
Georgem	Georg	k1gMnSc7	Georg
Sayersem	Sayers	k1gInSc7	Sayers
<g/>
;	;	kIx,	;
dostih	dostih	k1gInSc1	dostih
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
pouhých	pouhý	k2eAgMnPc2d1	pouhý
7	[number]	k4	7
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
dostihu	dostih	k1gInSc2	dostih
a	a	k8xC	a
výsledek	výsledek	k1gInSc4	výsledek
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
ohlas	ohlas	k1gInSc4	ohlas
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Velká	velká	k1gFnSc1	velká
běžela	běžet	k5eAaImAgFnS	běžet
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1876	[number]	k4	1876
a	a	k8xC	a
1908	[number]	k4	1908
kvůli	kvůli	k7c3	kvůli
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
kvůli	kvůli	k7c3	kvůli
politickému	politický	k2eAgNnSc3d1	politické
napětí	napětí	k1gNnSc3	napětí
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
byl	být	k5eAaImAgInS	být
dostih	dostih	k1gInSc1	dostih
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
proti	proti	k7c3	proti
převaze	převaha	k1gFnSc3	převaha
německých	německý	k2eAgMnPc2d1	německý
koní	kůň	k1gMnPc2	kůň
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
klisnou	klisna	k1gFnSc7	klisna
Normou	Norma	k1gFnSc7	Norma
hraběnka	hraběnka	k1gFnSc1	hraběnka
Lata	lata	k1gFnSc1	lata
Brandisová	Brandisový	k2eAgFnSc1d1	Brandisová
<g/>
,	,	kIx,	,
až	až	k9	až
doposud	doposud	k6eAd1	doposud
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
měří	měřit	k5eAaImIp3nS	měřit
6900	[number]	k4	6900
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
31	[number]	k4	31
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
ji	on	k3xPp3gFnSc4	on
běží	běžet	k5eAaImIp3nP	běžet
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Startuje	startovat	k5eAaBmIp3nS	startovat
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
koní	kůň	k1gMnPc2	kůň
s	s	k7c7	s
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
žokeji	žokej	k1gMnPc7	žokej
a	a	k8xC	a
žokejkami	žokejka	k1gFnPc7	žokejka
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgMnS	vžít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
celého	celý	k2eAgInSc2d1	celý
dostihového	dostihový	k2eAgInSc2d1	dostihový
víkendu	víkend	k1gInSc2	víkend
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
osm	osm	k4xCc1	osm
dostihů	dostih	k1gInPc2	dostih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
dostihem	dostih	k1gInSc7	dostih
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
dostihem	dostih	k1gInSc7	dostih
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
pardubické	pardubický	k2eAgFnSc2d1	pardubická
MHD	MHD	kA	MHD
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c6	po
vítězných	vítězný	k2eAgMnPc6d1	vítězný
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
tradici	tradice	k1gFnSc4	tradice
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
např.	např.	kA	např.
když	když	k8xS	když
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
již	již	k6eAd1	již
za	za	k7c7	za
sebou	se	k3xPyFc7	se
měla	mít	k5eAaImAgFnS	mít
padesát	padesát	k4xCc4	padesát
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
===	===	k?	===
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgInSc4d1	každoroční
závod	závod	k1gInSc4	závod
motocyklů	motocykl	k1gInPc2	motocykl
na	na	k7c6	na
ploché	plochý	k2eAgFnSc6d1	plochá
dráze	dráha	k1gFnSc6	dráha
ve	v	k7c6	v
Svítkově	svítkově	k6eAd1	svítkově
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejstarší	starý	k2eAgInSc1d3	nejstarší
plochodrážní	plochodrážní	k2eAgInSc1d1	plochodrážní
závod	závod	k1gInSc1	závod
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
jela	jet	k5eAaImAgFnS	jet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
první	první	k4xOgFnSc4	první
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
před	před	k7c7	před
Velkou	velký	k2eAgFnSc7d1	velká
Pardubickou	pardubický	k2eAgFnSc7d1	pardubická
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tradiční	tradiční	k2eAgInSc4d1	tradiční
týden	týden	k1gInSc4	týden
městských	městský	k2eAgFnPc2d1	městská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
láká	lákat	k5eAaImIp3nS	lákat
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
jezdce	jezdec	k1gInPc4	jezdec
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
nebo	nebo	k8xC	nebo
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závod	závod	k1gInSc4	závod
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
tisíce	tisíc	k4xCgInPc4	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Dánů	Dán	k1gMnPc2	Dán
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Trofej	trofej	k1gInSc1	trofej
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
známý	známý	k2eAgMnSc1d1	známý
pardubický	pardubický	k2eAgMnSc1d1	pardubický
klenotník	klenotník	k1gMnSc1	klenotník
pan	pan	k1gMnSc1	pan
Lejhanec	Lejhanec	k1gMnSc1	Lejhanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hokej	hokej	k1gInSc4	hokej
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
sídlí	sídlet	k5eAaImIp3nS	sídlet
lední	lední	k2eAgInSc4d1	lední
hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
HC	HC	kA	HC
Dynamo	dynamo	k1gNnSc1	dynamo
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgInPc1d1	hrající
v	v	k7c6	v
ČSOB	ČSOB	kA	ČSOB
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
ARENĚ	ARENĚ	kA	ARENĚ
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
<g/>
,	,	kIx,	,
modernizované	modernizovaný	k2eAgFnSc6d1	modernizovaná
a	a	k8xC	a
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1	pardubický
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
patří	patřit	k5eAaImIp3nS	patřit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
;	;	kIx,	;
třikrát	třikrát	k6eAd1	třikrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
Československou	československý	k2eAgFnSc4d1	Československá
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
a	a	k8xC	a
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
českou	český	k2eAgFnSc4d1	Česká
extraligu	extraliga	k1gFnSc4	extraliga
v	v	k7c6	v
sezonách	sezona	k1gFnPc6	sezona
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kanadském	kanadský	k2eAgInSc6d1	kanadský
hokeji	hokej	k1gInSc6	hokej
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
na	na	k7c6	na
Matičním	matiční	k2eAgNnSc6d1	matiční
jezeře	jezero	k1gNnSc6	jezero
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
sportovní	sportovní	k2eAgFnSc7d1	sportovní
společností	společnost	k1gFnSc7	společnost
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
první	první	k4xOgInSc1	první
klub	klub	k1gInSc1	klub
LTC	LTC	kA	LTC
Pardubice	Pardubice	k1gInPc1	Pardubice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nesestoupil	sestoupit	k5eNaPmAgMnS	sestoupit
z	z	k7c2	z
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
československé	československý	k2eAgFnSc2d1	Československá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
české	český	k2eAgFnSc2d1	Česká
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc1	první
extraligový	extraligový	k2eAgInSc1d1	extraligový
zápas	zápas	k1gInSc1	zápas
pod	pod	k7c7	pod
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
nebem	nebe	k1gNnSc7	nebe
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
fanoušci	fanoušek	k1gMnPc1	fanoušek
hokeje	hokej	k1gInSc2	hokej
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
fandící	fandící	k2eAgFnSc4d1	fandící
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejnavštěvovanějším	navštěvovaný	k2eAgFnPc3d3	nejnavštěvovanější
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
vychovaly	vychovat	k5eAaPmAgInP	vychovat
spoustu	spousta	k1gFnSc4	spousta
nadějných	nadějný	k2eAgMnPc2d1	nadějný
hokejistů	hokejista	k1gMnPc2	hokejista
př	př	kA	př
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Martinec	Martinec	k1gMnSc1	Martinec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Československo	Československo	k1gNnSc4	Československo
a	a	k8xC	a
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Basketbal	basketbal	k1gInSc1	basketbal
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
hrajících	hrající	k2eAgMnPc2d1	hrající
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
národní	národní	k2eAgFnSc4d1	národní
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
sídlo	sídlo	k1gNnSc4	sídlo
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
BK	BK	kA	BK
Synthesia	Synthesia	k1gFnSc1	Synthesia
/	/	kIx~	/
JIP	JIP	kA	JIP
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c4	v
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
českou	český	k2eAgFnSc4d1	Česká
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
pravidelně	pravidelně	k6eAd1	pravidelně
hraje	hrát	k5eAaImIp3nS	hrát
mezi	mezi	k7c7	mezi
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
pěti	pět	k4xCc7	pět
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
<g/>
;	;	kIx,	;
skvělé	skvělý	k2eAgInPc4d1	skvělý
výsledky	výsledek	k1gInPc4	výsledek
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
mládež	mládež	k1gFnSc4	mládež
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Procházky	Procházka	k1gMnSc2	Procházka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
je	být	k5eAaImIp3nS	být
Vrcholové	vrcholový	k2eAgNnSc1d1	vrcholové
sportovní	sportovní	k2eAgNnSc1d1	sportovní
centrum	centrum	k1gNnSc1	centrum
mládeže	mládež	k1gFnSc2	mládež
pod	pod	k7c7	pod
Českou	český	k2eAgFnSc7d1	Česká
basketbalovou	basketbalový	k2eAgFnSc7d1	basketbalová
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Pardubice	Pardubice	k1gInPc4	Pardubice
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
Jiří	Jiří	k1gMnSc1	Jiří
Welsch	Welsch	k1gMnSc1	Welsch
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Šindelář	Šindelář	k1gMnSc1	Šindelář
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
skvělí	skvělý	k2eAgMnPc1d1	skvělý
hráči	hráč	k1gMnPc1	hráč
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
historie	historie	k1gFnSc2	historie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šachy	šach	k1gInPc4	šach
===	===	k?	===
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
největší	veliký	k2eAgInSc4d3	veliký
šachový	šachový	k2eAgInSc4d1	šachový
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Czech	Czech	k1gMnSc1	Czech
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
mimo	mimo	k6eAd1	mimo
šachů	šach	k1gInPc2	šach
také	také	k9	také
scrabble	scrabble	k6eAd1	scrabble
<g/>
,	,	kIx,	,
bridž	bridž	k1gInSc1	bridž
<g/>
,	,	kIx,	,
poker	poker	k1gInSc1	poker
<g/>
,	,	kIx,	,
mariáš	mariáš	k1gInSc1	mariáš
a	a	k8xC	a
mankala	mankat	k5eAaPmAgFnS	mankat
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
hlavních	hlavní	k2eAgMnPc2d1	hlavní
a	a	k8xC	a
dvaceti	dvacet	k4xCc2	dvacet
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
festivalem	festival	k1gInSc7	festival
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
převzali	převzít	k5eAaPmAgMnP	převzít
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
hejtman	hejtman	k1gMnSc1	hejtman
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
Radko	Radka	k1gFnSc5	Radka
Martínek	Martínek	k1gMnSc1	Martínek
a	a	k8xC	a
primátorka	primátorka	k1gFnSc1	primátorka
Pardubic	Pardubice	k1gInPc2	Pardubice
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
Fraňková	Fraňková	k1gFnSc1	Fraňková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tenis	tenis	k1gInSc1	tenis
<g/>
:	:	kIx,	:
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
juniorka	juniorka	k1gFnSc1	juniorka
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
staršího	starý	k2eAgInSc2d2	starší
dorostu	dorost	k1gInSc2	dorost
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
,	,	kIx,	,
neoficiálně	neoficiálně	k6eAd1	neoficiálně
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
juniorka	juniorka	k1gFnSc1	juniorka
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
Dorostenecké	dorostenecký	k2eAgNnSc4d1	dorostenecké
mistrovství	mistrovství	k1gNnSc4	mistrovství
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
juniorský	juniorský	k2eAgInSc1d1	juniorský
turnaj	turnaj	k1gInSc1	turnaj
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
na	na	k7c6	na
kurtech	kurt	k1gInPc6	kurt
klubu	klub	k1gInSc2	klub
LTC	LTC	kA	LTC
Pardubice	Pardubice	k1gInPc1	Pardubice
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
obdobu	obdoba	k1gFnSc4	obdoba
prestižního	prestižní	k2eAgInSc2d1	prestižní
floridského	floridský	k2eAgInSc2d1	floridský
Orange	Orang	k1gInSc2	Orang
Bowlu	Bowl	k1gInSc2	Bowl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vítězi	vítěz	k1gMnPc7	vítěz
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
grandslamových	grandslamový	k2eAgMnPc2d1	grandslamový
šampiónů	šampión	k1gMnPc2	šampión
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
světové	světový	k2eAgFnPc1d1	světová
jedničky	jednička	k1gFnPc1	jednička
–	–	k?	–
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
mužské	mužský	k2eAgFnSc2d1	mužská
dvouhry	dvouhra	k1gFnSc2	dvouhra
z	z	k7c2	z
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1988	[number]	k4	1988
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
tenista	tenista	k1gMnSc1	tenista
Miloslav	Miloslav	k1gMnSc1	Miloslav
Mečíř	mečíř	k1gMnSc1	mečíř
<g/>
.	.	kIx.	.
</s>
<s>
Pardubickou	pardubický	k2eAgFnSc4d1	pardubická
juniorku	juniorka	k1gFnSc4	juniorka
také	také	k9	také
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všichni	všechen	k3xTgMnPc1	všechen
čeští	český	k2eAgMnPc1d1	český
wimbledonští	wimbledonský	k2eAgMnPc1d1	wimbledonský
vítězové	vítěz	k1gMnPc1	vítěz
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
Jan	Jan	k1gMnSc1	Jan
Kodeš	Kodeš	k1gMnSc1	Kodeš
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Novotná	Novotná	k1gFnSc1	Novotná
a	a	k8xC	a
Petra	Petra	k1gFnSc1	Petra
Kvitová	Kvitová	k1gFnSc1	Kvitová
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
poražení	poražený	k2eAgMnPc1d1	poražený
wimbledonští	wimbledonský	k2eAgMnPc1d1	wimbledonský
finalisté	finalista	k1gMnPc1	finalista
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
Mandlíková	Mandlíková	k1gFnSc1	Mandlíková
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Drobný	drobný	k2eAgMnSc1d1	drobný
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
jako	jako	k9	jako
občan	občan	k1gMnSc1	občan
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
získala	získat	k5eAaPmAgFnS	získat
všechny	všechen	k3xTgInPc4	všechen
singlové	singlový	k2eAgInPc4d1	singlový
tituly	titul	k1gInPc4	titul
jako	jako	k8xS	jako
hráčka	hráčka	k1gFnSc1	hráčka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
se	se	k3xPyFc4	se
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
vrátila	vrátit	k5eAaPmAgFnS	vrátit
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
nově	nově	k6eAd1	nově
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
hraje	hrát	k5eAaImIp3nS	hrát
tým	tým	k1gInSc1	tým
FK	FK	kA	FK
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
sloučením	sloučení	k1gNnSc7	sloučení
několika	několik	k4yIc2	několik
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
však	však	k9	však
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
působily	působit	k5eAaImAgInP	působit
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
letech	let	k1gInPc6	let
či	či	k8xC	či
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Spojily	spojit	k5eAaPmAgInP	spojit
se	s	k7c7	s
kluby	klub	k1gInPc7	klub
FK	FK	kA	FK
Junior	junior	k1gMnSc1	junior
(	(	kIx(	(
<g/>
dorost	dorost	k1gInSc1	dorost
a	a	k8xC	a
žáci	žák	k1gMnPc1	žák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MFK	MFK	kA	MFK
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
přípravky	přípravek	k1gInPc1	přípravek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tesla	Tesla	k1gFnSc1	Tesla
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
oddíly	oddíl	k1gInPc1	oddíl
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Fotbal	fotbal	k1gInSc1	fotbal
Pardubice	Pardubice	k1gInPc1	Pardubice
a.s.	a.s.	k?	a.s.
vybudovaly	vybudovat	k5eAaPmAgFnP	vybudovat
silný	silný	k2eAgInSc4d1	silný
tým	tým	k1gInSc4	tým
s	s	k7c7	s
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
mládežnickou	mládežnický	k2eAgFnSc7d1	mládežnická
základnou	základna	k1gFnSc7	základna
<g/>
.	.	kIx.	.
</s>
<s>
FK	FK	kA	FK
Pardubice	Pardubice	k1gInPc4	Pardubice
hraje	hrát	k5eAaImIp3nS	hrát
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
FNL	FNL	kA	FNL
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Pod	pod	k7c7	pod
Vinicí	vinice	k1gFnSc7	vinice
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
starý	starý	k2eAgInSc4d1	starý
Letní	letní	k2eAgInSc4d1	letní
stadion	stadion	k1gInSc4	stadion
za	za	k7c7	za
zimním	zimní	k2eAgInSc7d1	zimní
stadionem	stadion	k1gInSc7	stadion
přestal	přestat	k5eAaPmAgInS	přestat
již	již	k6eAd1	již
požadavkům	požadavek	k1gInPc3	požadavek
soutěže	soutěž	k1gFnSc2	soutěž
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc1	mládež
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
soustředí	soustředit	k5eAaPmIp3nS	soustředit
ve	v	k7c6	v
sportovním	sportovní	k2eAgInSc6d1	sportovní
areálu	areál	k1gInSc6	areál
Ohrazenice	Ohrazenice	k1gFnSc2	Ohrazenice
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
FNL	FNL	kA	FNL
a	a	k8xC	a
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
dorostenecké	dorostenecký	k2eAgInPc1d1	dorostenecký
oddíly	oddíl	k1gInPc1	oddíl
(	(	kIx(	(
<g/>
oddíl	oddíl	k1gInSc1	oddíl
FK	FK	kA	FK
Pardubice	Pardubice	k1gInPc1	Pardubice
U-19	U-19	k1gFnSc2	U-19
hraje	hrát	k5eAaImIp3nS	hrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
dorosteneckou	dorostenecký	k2eAgFnSc4d1	dorostenecká
ligu	liga	k1gFnSc4	liga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
žákovských	žákovský	k2eAgInPc2d1	žákovský
celků	celek	k1gInPc2	celek
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
přípravek	přípravek	k1gInSc1	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Strategií	strategie	k1gFnSc7	strategie
klubu	klub	k1gInSc2	klub
je	být	k5eAaImIp3nS	být
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
využívat	využívat	k5eAaPmF	využívat
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
odchovanci	odchovanec	k1gMnPc1	odchovanec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
východočeského	východočeský	k2eAgInSc2d1	východočeský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
A-tým	Aý	k1gMnSc7	A-tý
FK	FK	kA	FK
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
družstvo	družstvo	k1gNnSc4	družstvo
dospělých	dospělí	k1gMnPc2	dospělí
Tesly	Tesla	k1gFnSc2	Tesla
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
postoupil	postoupit	k5eAaPmAgInS	postoupit
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
z	z	k7c2	z
divize	divize	k1gFnSc2	divize
do	do	k7c2	do
ČFL	ČFL	kA	ČFL
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
FNL	FNL	kA	FNL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
sídlí	sídlet	k5eAaImIp3nS	sídlet
Univerzita	univerzita	k1gFnSc1	univerzita
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
sedm	sedm	k4xCc4	sedm
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
ústav	ústav	k1gInSc4	ústav
a	a	k8xC	a
kde	kde	k6eAd1	kde
studuje	studovat	k5eAaImIp3nS	studovat
asi	asi	k9	asi
deset	deset	k4xCc1	deset
tisíc	tisíc	k4xCgInSc1	tisíc
studentů	student	k1gMnPc2	student
a	a	k8xC	a
studentek	studentka	k1gFnPc2	studentka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
je	být	k5eAaImIp3nS	být
také	také	k9	také
celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
chemická	chemický	k2eAgFnSc1d1	chemická
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dále	daleko	k6eAd2	daleko
4	[number]	k4	4
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
:	:	kIx,	:
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Dašická	Dašická	k1gFnSc1	Dašická
<g/>
,	,	kIx,	,
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Mozartova	Mozartův	k2eAgNnSc2d1	Mozartovo
<g/>
,	,	kIx,	,
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
Anglické	anglický	k2eAgNnSc1d1	anglické
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
parků	park	k1gInPc2	park
a	a	k8xC	a
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
parky	park	k1gInPc4	park
Pardubic	Pardubice	k1gInPc2	Pardubice
patří	patřit	k5eAaImIp3nP	patřit
Tyršovy	Tyršův	k2eAgFnPc1d1	Tyršova
sady	sada	k1gFnPc1	sada
a	a	k8xC	a
také	také	k9	také
park	park	k1gInSc1	park
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Špici	špice	k1gFnSc6	špice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vymezený	vymezený	k2eAgInSc1d1	vymezený
soutokem	soutok	k1gInSc7	soutok
řek	řeka	k1gFnPc2	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Chrudimka	Chrudimek	k1gMnSc4	Chrudimek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
opraven	opravna	k1gFnPc2	opravna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgNnPc1	čtyři
maloplošná	maloplošný	k2eAgNnPc1d1	maloplošné
zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Labiště	Labiště	k1gNnSc2	Labiště
pod	pod	k7c7	pod
Opočínkem	Opočínek	k1gInSc7	Opočínek
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Mělické	Mělická	k1gFnSc2	Mělická
labiště	labiště	k1gNnSc1	labiště
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Nemošická	Nemošická	k1gFnSc1	Nemošická
stráň	stráň	k1gFnSc1	stráň
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
U	u	k7c2	u
Pohránovského	Pohránovský	k2eAgInSc2d1	Pohránovský
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
–	–	k?	–
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
U	u	k7c2	u
Pohránovského	Pohránovský	k2eAgInSc2d1	Pohránovský
rybníka	rybník	k1gInSc2	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
tok	tok	k1gInSc1	tok
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
i	i	k8xC	i
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
památné	památný	k2eAgFnPc1d1	památná
stromořadí	stromořadí	k1gNnSc1	stromořadí
(	(	kIx(	(
<g/>
stromořadí	stromořadí	k1gNnSc1	stromořadí
34	[number]	k4	34
ks	ks	kA	ks
dubů	dub	k1gInPc2	dub
podél	podél	k7c2	podél
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
stromořadí	stromořadí	k1gNnSc2	stromořadí
22	[number]	k4	22
ks	ks	kA	ks
dubů	dub	k1gInPc2	dub
letních	letní	k2eAgFnPc2d1	letní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
4	[number]	k4	4
jerlíny	jerlín	k1gInPc4	jerlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Památné	památný	k2eAgInPc1d1	památný
jerlíny	jerlín	k1gInPc1	jerlín
japonské	japonský	k2eAgInPc1d1	japonský
na	na	k7c6	na
Wernerově	Wernerův	k2eAgNnSc6d1	Wernerovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
</s>
</p>
<p>
<s>
Památné	památný	k2eAgInPc1d1	památný
duby	dub	k1gInPc1	dub
u	u	k7c2	u
sídliště	sídliště	k1gNnSc2	sídliště
Závodu	závod	k1gInSc2	závod
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
</s>
</p>
<p>
<s>
Stromořadí	stromořadí	k1gNnSc1	stromořadí
22	[number]	k4	22
ks	ks	kA	ks
dubů	dub	k1gInPc2	dub
letních	letní	k2eAgInPc2d1	letní
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Závodu	závod	k1gInSc2	závod
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc4	čtyři
divadla	divadlo	k1gNnPc4	divadlo
<g/>
:	:	kIx,	:
největší	veliký	k2eAgNnSc4d3	veliký
a	a	k8xC	a
nejstarší	starý	k2eAgNnSc4d3	nejstarší
Východočeské	východočeský	k2eAgNnSc4d1	Východočeské
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Divadlo	divadlo	k1gNnSc1	divadlo
Exil	exil	k1gInSc1	exil
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
29	[number]	k4	29
a	a	k8xC	a
Loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
Radost	radost	k1gFnSc1	radost
DK	DK	kA	DK
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
profesionální	profesionální	k2eAgMnSc1d1	profesionální
Komorní	komorní	k1gMnSc1	komorní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
Barocco	Barocca	k1gMnSc5	Barocca
sempre	sempr	k1gMnSc5	sempr
giovane	giovan	k1gMnSc5	giovan
či	či	k8xC	či
amatérský	amatérský	k2eAgInSc1d1	amatérský
Pardubický	pardubický	k2eAgInSc1d1	pardubický
komorní	komorní	k2eAgInSc1d1	komorní
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Sborovému	sborový	k2eAgInSc3d1	sborový
zpěvu	zpěv	k1gInSc3	zpěv
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
Vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
umělecký	umělecký	k2eAgInSc4d1	umělecký
soubor	soubor	k1gInSc4	soubor
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Pardubický	pardubický	k2eAgInSc1d1	pardubický
dětský	dětský	k2eAgInSc4d1	dětský
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgInSc4d1	komorní
sbor	sbor	k1gInSc4	sbor
ORFEUS	Orfeus	k1gMnSc1	Orfeus
<g/>
,	,	kIx,	,
Continuo	Continuo	k1gMnSc1	Continuo
<g/>
,	,	kIx,	,
IUVENTUS	IUVENTUS	kA	IUVENTUS
CANTANS	CANTANS	kA	CANTANS
<g/>
,	,	kIx,	,
Chlapecký	chlapecký	k2eAgInSc1d1	chlapecký
sbor	sbor	k1gInSc1	sbor
BONIFANTES	BONIFANTES	kA	BONIFANTES
či	či	k8xC	či
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
Spojené	spojený	k2eAgInPc1d1	spojený
sbory	sbor	k1gInPc1	sbor
Pernštýn	Pernštýn	k1gInSc1	Pernštýn
–	–	k?	–
Ludmila	Ludmila	k1gFnSc1	Ludmila
–	–	k?	–
Suk	Suk	k1gMnSc1	Suk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
působily	působit	k5eAaImAgFnP	působit
či	či	k8xC	či
působí	působit	k5eAaImIp3nP	působit
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
trampská	trampský	k2eAgFnSc1d1	trampská
Stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
folková	folkový	k2eAgNnPc4d1	folkové
Pouta	pouto	k1gNnPc4	pouto
(	(	kIx(	(
<g/>
nástupce	nástupce	k1gMnSc2	nástupce
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
skupiny	skupina	k1gFnSc2	skupina
Poupata	poupě	k1gNnPc1	poupě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
folkový	folkový	k2eAgInSc4d1	folkový
Marien	Marien	k1gInSc4	Marien
či	či	k8xC	či
pop	pop	k1gInSc4	pop
punková	punkový	k2eAgNnPc1d1	punkové
Vypsaná	vypsaný	k2eAgNnPc1d1	vypsané
fiXa	fixum	k1gNnPc1	fixum
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
tradice	tradice	k1gFnPc1	tradice
udržují	udržovat	k5eAaImIp3nP	udržovat
folklorní	folklorní	k2eAgInPc1d1	folklorní
soubory	soubor	k1gInPc1	soubor
Lipka	lipka	k1gFnSc1	lipka
Pardubice	Pardubice	k1gInPc1	Pardubice
a	a	k8xC	a
Baldrián	baldrián	k1gInSc1	baldrián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
(	(	kIx(	(
<g/>
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
v	v	k7c6	v
domě	dům	k1gInSc6	dům
u	u	k7c2	u
Jonáše	Jonáš	k1gMnSc2	Jonáš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
Krajská	krajský	k2eAgFnSc1d1	krajská
knihovna	knihovna	k1gFnSc1	knihovna
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
kraje	kraj	k1gInSc2	kraj
Okresní	okresní	k2eAgFnSc1d1	okresní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
sídlí	sídlet	k5eAaImIp3nS	sídlet
Východočeské	východočeský	k2eAgNnSc1d1	Východočeské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
odsud	odsud	k6eAd1	odsud
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Dukla	Dukla	k1gFnSc1	Dukla
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
barona	baron	k1gMnSc2	baron
Artura	Artur	k1gMnSc2	Artur
Krause	Kraus	k1gMnSc2	Kraus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgFnSc3d1	významná
události	událost	k1gFnSc3	událost
===	===	k?	===
</s>
</p>
<p>
<s>
Pernštýnská	Pernštýnský	k2eAgFnSc1d1	Pernštýnská
noc	noc	k1gFnSc1	noc
–	–	k?	–
městské	městský	k2eAgFnPc4d1	městská
slavnosti	slavnost	k1gFnPc4	slavnost
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
smíchu	smích	k1gInSc2	smích
–	–	k?	–
divadelní	divadelní	k2eAgInSc1d1	divadelní
festival	festival	k1gInSc1	festival
v	v	k7c6	v
VČ	VČ	kA	VČ
Divadlo	divadlo	k1gNnSc1	divadlo
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komorní	komorní	k1gMnSc1	komorní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Pardubice	Pardubice	k1gInPc4	Pardubice
a	a	k8xC	a
Barocco	Barocca	k1gMnSc5	Barocca
sempre	sempr	k1gMnSc5	sempr
giovane	giovan	k1gMnSc5	giovan
–	–	k?	–
abonentní	abonentní	k2eAgInPc1d1	abonentní
cykly	cyklus	k1gInPc1	cyklus
koncertů	koncert	k1gInPc2	koncert
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Le	Le	k1gFnPc2	Le
Quattro	Quattro	k1gNnSc1	Quattro
Stagioni	Stagioň	k1gFnSc5	Stagioň
</s>
</p>
<p>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
festival	festival	k1gInSc1	festival
dětských	dětský	k2eAgInPc2d1	dětský
a	a	k8xC	a
mládežnických	mládežnický	k2eAgInPc2d1	mládežnický
pěveckých	pěvecký	k2eAgInPc2d1	pěvecký
sborů	sbor	k1gInPc2	sbor
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
akademických	akademický	k2eAgMnPc2d1	akademický
sborů	sbor	k1gInPc2	sbor
IFAS	IFAS	kA	IFAS
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
folklorní	folklorní	k2eAgInSc1d1	folklorní
festival	festival	k1gInSc1	festival
Podzimní	podzimní	k2eAgFnSc2d1	podzimní
folklorní	folklorní	k2eAgFnSc2d1	folklorní
slavnosti	slavnost	k1gFnSc2	slavnost
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Retroměstečko	Retroměstečko	k1gNnSc1	Retroměstečko
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
setkání	setkání	k1gNnSc1	setkání
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
hasičské	hasičský	k2eAgFnSc2d1	hasičská
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnSc2d1	historická
<g/>
,	,	kIx,	,
veteránské	veteránský	k2eAgFnSc2d1	veteránská
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc2d1	vojenská
a	a	k8xC	a
branně-bezpečnostní	branněezpečnostní	k2eAgFnSc2d1	branně-bezpečnostní
techniky	technika	k1gFnSc2	technika
se	s	k7c7	s
statickými	statický	k2eAgFnPc7d1	statická
a	a	k8xC	a
dynamickými	dynamický	k2eAgFnPc7d1	dynamická
ukázkami	ukázka	k1gFnPc7	ukázka
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Plavení	plavení	k1gNnSc4	plavení
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
sobota	sobota	k1gFnSc1	sobota
a	a	k8xC	a
neděle	neděle	k1gFnSc2	neděle
hlavní	hlavní	k2eAgInSc1d1	hlavní
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgInPc1d1	hudební
kluby	klub	k1gInPc1	klub
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
několik	několik	k4yIc4	několik
hudebních	hudební	k2eAgInPc2d1	hudební
klubů	klub	k1gInPc2	klub
s	s	k7c7	s
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
či	či	k8xC	či
méně	málo	k6eAd2	málo
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
produkcí	produkce	k1gFnSc7	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Rockové	rockový	k2eAgInPc1d1	rockový
kluby	klub	k1gInPc1	klub
Žlutý	žlutý	k2eAgMnSc1d1	žlutý
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
Ponorka	ponorka	k1gFnSc1	ponorka
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Divadlem	divadlo	k1gNnSc7	divadlo
29	[number]	k4	29
zaměřující	zaměřující	k2eAgNnPc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
alternativní	alternativní	k2eAgFnSc4d1	alternativní
scénu	scéna	k1gFnSc4	scéna
jsou	být	k5eAaImIp3nP	být
tradičními	tradiční	k2eAgNnPc7d1	tradiční
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jít	jít	k5eAaImF	jít
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
za	za	k7c7	za
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Pardubic	Pardubice	k1gInPc2	Pardubice
je	být	k5eAaImIp3nS	být
i	i	k9	i
množství	množství	k1gNnSc1	množství
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
podniky	podnik	k1gInPc1	podnik
Patapuf	Patapuf	k1gInSc4	Patapuf
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Prostě	prostě	k9	prostě
bar	bar	k1gInSc1	bar
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozšířeném	rozšířený	k2eAgNnSc6d1	rozšířené
centru	centrum	k1gNnSc6	centrum
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
Music	Musice	k1gFnPc2	Musice
Hall	Halla	k1gFnPc2	Halla
Hobe	Hob	k1gFnSc2	Hob
s	s	k7c7	s
pestrou	pestrý	k2eAgFnSc7d1	pestrá
dramaturgií	dramaturgie	k1gFnSc7	dramaturgie
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
polabinský	polabinský	k2eAgInSc1d1	polabinský
Klec	klec	k1gFnSc1	klec
Music	Music	k1gMnSc1	Music
Club	club	k1gInSc1	club
a	a	k8xC	a
Dýdy	Dýd	k2eAgFnPc1d1	Dýd
Baba	baba	k1gFnSc1	baba
u	u	k7c2	u
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
kolejí	kolej	k1gFnPc2	kolej
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navštěvovány	navštěvovat	k5eAaImNgFnP	navštěvovat
mladšími	mladý	k2eAgMnPc7d2	mladší
posluchači	posluchač	k1gMnPc7	posluchač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmová	filmový	k2eAgNnPc1d1	filmové
natáčení	natáčení	k1gNnPc1	natáčení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	se	k3xPyFc4	se
natáčely	natáčet	k5eAaImAgInP	natáčet
např.	např.	kA	např.
filmy	film	k1gInPc1	film
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
pardubické	pardubický	k2eAgNnSc1d1	pardubické
krematorium	krematorium	k1gNnSc1	krematorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dívka	dívka	k1gFnSc1	dívka
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Operace	operace	k1gFnSc1	operace
Silver	Silver	k1gInSc1	Silver
A	A	kA	A
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
televizní	televizní	k2eAgFnSc1d1	televizní
série	série	k1gFnSc1	série
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
lest	lest	k1gFnSc1	lest
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pokračování	pokračování	k1gNnSc4	pokračování
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnPc1d1	národní
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pardubický	pardubický	k2eAgInSc1d1	pardubický
zámek	zámek	k1gInSc1	zámek
s	s	k7c7	s
opevněním	opevnění	k1gNnSc7	opevnění
</s>
</p>
<p>
<s>
Pietní	pietní	k2eAgNnSc1d1	pietní
území	území	k1gNnSc1	území
Zámeček	zámeček	k1gInSc1	zámeček
</s>
</p>
<p>
<s>
Pardubické	pardubický	k2eAgNnSc4d1	pardubické
krematorium	krematorium	k1gNnSc4	krematorium
</s>
</p>
<p>
<s>
Winternitzovy	Winternitzův	k2eAgFnPc1d1	Winternitzova
automatické	automatický	k2eAgFnPc1d1	automatická
mlýnyVybrané	mlýnyVybraný	k2eAgFnPc1d1	mlýnyVybraný
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
dům	dům	k1gInSc1	dům
č.	č.	k?	č.
p.	p.	k?	p.
49	[number]	k4	49
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Jonáše	Jonáš	k1gMnSc2	Jonáš
</s>
</p>
<p>
<s>
Wenerův	Wenerův	k2eAgInSc1d1	Wenerův
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
</s>
</p>
<p>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
brána	brána	k1gFnSc1	brána
</s>
</p>
<p>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Grandhotel	grandhotel	k1gInSc1	grandhotel
a	a	k8xC	a
okresní	okresní	k2eAgInSc1d1	okresní
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
</s>
</p>
<p>
<s>
Machoňova	Machoňův	k2eAgFnSc1d1	Machoňova
pasáž	pasáž	k1gFnSc1	pasáž
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Pardubice	Pardubice	k1gInPc1	Pardubice
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
</s>
</p>
<p>
<s>
Počápelský	Počápelský	k2eAgInSc1d1	Počápelský
vodní	vodní	k2eAgInSc1d1	vodní
kanál	kanál	k1gInSc1	kanál
</s>
</p>
<p>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
vila	vila	k1gFnSc1	vila
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
Viktora	Viktor	k1gMnSc2	Viktor
Kříže	Kříž	k1gMnSc2	Kříž
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
</s>
</p>
<p>
<s>
===	===	k?	===
Architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
umělci	umělec	k1gMnPc1	umělec
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Roderich	Roderich	k1gMnSc1	Roderich
Bass	Bass	k1gMnSc1	Bass
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Bíbl	Bíbl	k1gMnSc1	Bíbl
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Bok	boka	k1gFnPc2	boka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Brázda	Brázda	k1gMnSc1	Brázda
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Bulis	Bulis	k1gFnSc2	Bulis
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Crha	Crha	k1gMnSc1	Crha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vincenc	Vincenc	k1gMnSc1	Vincenc
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Exner	Exner	k1gMnSc1	Exner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Smil	smil	k1gInSc1	smil
Flaška	flaška	k1gFnSc1	flaška
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
–	–	k?	–
<g/>
1403	[number]	k4	1403
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
satirik	satirik	k1gMnSc1	satirik
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Gebauerová	Gebauerová	k1gFnSc1	Gebauerová
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Grus	Grusa	k1gFnPc2	Grusa
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Gruša	Gruša	k1gMnSc1	Gruša
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Haničinec	Haničinec	k1gMnSc1	Haničinec
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Hemský	Hemský	k1gMnSc1	Hemský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiljí	Jiljí	k1gMnSc1	Jiljí
Vratislav	Vratislav	k1gMnSc1	Vratislav
Jahn	Jahn	k1gMnSc1	Jahn
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Janecký	Janecký	k2eAgMnSc1d1	Janecký
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Ota	Ota	k1gMnSc1	Ota
Janeček	Janeček	k1gMnSc1	Janeček
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Jarolímek	Jarolímek	k1gMnSc1	Jarolímek
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Kabeš	Kabeš	k1gMnSc1	Kabeš
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
Korolová	Korolový	k2eAgFnSc1d1	Korolová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Artur	Artur	k1gMnSc1	Artur
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Lexa	Lexa	k1gMnSc1	Lexa
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
epytolog	epytolog	k1gMnSc1	epytolog
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Artur	Artur	k1gMnSc1	Artur
Longen	Longen	k1gInSc1	Longen
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Emanuel	Emanuel	k1gMnSc1	Emanuel
Macan	Macan	k1gMnSc1	Macan
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
esperantista	esperantista	k1gMnSc1	esperantista
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Martinec	Martinec	k1gMnSc1	Martinec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Mathesius	Mathesius	k1gMnSc1	Mathesius
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Maxová	Maxová	k1gFnSc1	Maxová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nadrchal	nadrchat	k5eAaPmAgMnS	nadrchat
(	(	kIx(	(
<g/>
*	*	kIx~	*
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ondráček	Ondráček	k1gMnSc1	Ondráček
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Pírka	pírko	k1gNnSc2	pírko
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
c.	c.	k?	c.
k.	k.	k?	k.
dvorní	dvorní	k2eAgMnSc1d1	dvorní
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
sportovní	sportovní	k2eAgFnSc2d1	sportovní
fotografie	fotografia	k1gFnSc2	fotografia
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pištora	Pištor	k1gMnSc2	Pištor
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Popelka	Popelka	k1gMnSc1	Popelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
aranžér	aranžér	k1gMnSc1	aranžér
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Prýl	Prýl	k1gMnSc1	Prýl
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Přeučil	přeučit	k5eAaPmAgMnS	přeučit
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Rákos	rákos	k1gInSc1	rákos
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Schwarz	Schwarz	k1gMnSc1	Schwarz
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Starý	Starý	k1gMnSc1	Starý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Sýkora	Sýkora	k1gMnSc1	Sýkora
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šejba	Šejba	k1gMnSc1	Šejba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Švehlík	Švehlík	k1gMnSc1	Švehlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Hanuš	Hanuš	k1gMnSc1	Hanuš
Thein	Thein	k2eAgMnSc1d1	Thein
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Toman	Toman	k1gMnSc1	Toman
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Trnka	Trnka	k1gMnSc1	Trnka
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Bratranci	bratranec	k1gMnPc1	bratranec
Veverkové	Veverková	k1gFnSc2	Veverková
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynálezci	vynálezce	k1gMnPc1	vynálezce
</s>
</p>
<p>
<s>
Božena	Božena	k1gFnSc1	Božena
Vikova-Kunětická	Vikova-Kunětický	k2eAgFnSc1d1	Vikova-Kunětický
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vokolek	Vokolka	k1gFnPc2	Vokolka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
</s>
</p>
<p>
<s>
Jolana	Jolana	k1gFnSc1	Jolana
Voldánová	Voldánová	k1gFnSc1	Voldánová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
František	františek	k1gInSc4	františek
Vyčichlo	vyčichnout	k5eAaPmAgNnS	vyčichnout
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Julián	Julián	k1gMnSc1	Julián
Záhorovský	Záhorovský	k2eAgMnSc1d1	Záhorovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Doetinchem	Doetinch	k1gInSc7	Doetinch
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
</s>
</p>
<p>
<s>
Merano	Merana	k1gFnSc5	Merana
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc5	Itálie
</s>
</p>
<p>
<s>
Rosignano	Rosignana	k1gFnSc5	Rosignana
Marittimo	Marittima	k1gFnSc5	Marittima
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc5	Itálie
</s>
</p>
<p>
<s>
Pernik	Pernik	k1gInSc1	Pernik
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
</s>
</p>
<p>
<s>
Schönebeck	Schönebeck	k1gInSc1	Schönebeck
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Selb	Selb	k1gInSc1	Selb
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Skellefteå	Skellefteå	k?	Skellefteå
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
</s>
</p>
<p>
<s>
===	===	k?	===
Spřátelená	spřátelený	k2eAgNnPc1d1	spřátelené
města	město	k1gNnPc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Jerez	Jerez	k1gInSc1	Jerez
de	de	k?	de
la	la	k1gNnSc2	la
Frontera	Fronter	k1gMnSc2	Fronter
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Golega	Golega	k1gFnSc1	Golega
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
</s>
</p>
<p>
<s>
East	East	k2eAgInSc1d1	East
Lothian	Lothian	k1gInSc1	Lothian
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Weregem	Wereg	k1gInSc7	Wereg
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
</s>
</p>
<p>
<s>
Sežana	Sežana	k1gFnSc1	Sežana
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
Tatry	Tatra	k1gFnPc1	Tatra
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Letem	letem	k6eAd1	letem
českým	český	k2eAgInSc7d1	český
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
Půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
fotografických	fotografický	k2eAgMnPc2d1	fotografický
pohledů	pohled	k1gInPc2	pohled
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Tištěno	tištěn	k2eAgNnSc1d1	tištěno
v	v	k7c6	v
Knihtiskárně	knihtiskárna	k1gFnSc6	knihtiskárna
Jos	Jos	k1gFnSc2	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímka	Vilímek	k1gMnSc2	Vilímek
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
1	[number]	k4	1
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1898	[number]	k4	1898
</s>
</p>
<p>
<s>
FIEDLER	Fiedler	k1gMnSc1	Fiedler
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgFnPc1d1	židovská
památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sefer	Sefer	k1gMnSc1	Sefer
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900895	[number]	k4	900895
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Patrocinia	Patrocinium	k1gNnPc4	Patrocinium
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předhusitské	předhusitský	k2eAgFnSc2d1	předhusitská
a	a	k8xC	a
barokní	barokní	k2eAgFnSc2d1	barokní
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Pražské	pražský	k2eAgNnSc1d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
1344	[number]	k4	1344
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
164	[number]	k4	164
<g/>
–	–	k?	–
<g/>
179	[number]	k4	179
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
J.	J.	kA	J.
<g/>
,	,	kIx,	,
O	o	k7c6	o
starých	starý	k2eAgFnPc6d1	stará
památkách	památka	k1gFnPc6	památka
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
děkanského	děkanský	k2eAgInSc2d1	děkanský
chrámu	chrám	k1gInSc2	chrám
Sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
1908	[number]	k4	1908
</s>
</p>
<p>
<s>
Hlobil	Hlobit	k5eAaPmAgMnS	Hlobit
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Petrů	Petrů	k2eAgFnSc1d1	Petrů
E.	E.	kA	E.
<g/>
:	:	kIx,	:
Humanismus	humanismus	k1gInSc1	humanismus
a	a	k8xC	a
raná	raný	k2eAgFnSc1d1	raná
renesance	renesance	k1gFnSc1	renesance
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Hoferica	Hoferica	k1gFnSc1	Hoferica
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
mobiliář	mobiliář	k1gInSc4	mobiliář
a	a	k8xC	a
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
objevům	objev	k1gInPc3	objev
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
nepublik	nepublik	k1gMnSc1	nepublik
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc1	práce
</s>
</p>
<p>
<s>
Hrubý	Hrubý	k1gMnSc1	Hrubý
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc1	umění
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
klasicismu	klasicismus	k1gInSc2	klasicismus
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
nepub	nepub	k1gInSc1	nepub
<g/>
.	.	kIx.	.
text	text	k1gInSc1	text
pro	pro	k7c4	pro
Dějiny	dějiny	k1gFnPc4	dějiny
Pardubic	Pardubice	k1gInPc2	Pardubice
II	II	kA	II
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
pardubickém	pardubický	k2eAgInSc6d1	pardubický
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
21	[number]	k4	21
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
Langerová	Langerová	k1gFnSc1	Langerová
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Pardubice	Pardubice	k1gInPc1	Pardubice
Region	region	k1gInSc1	region
–	–	k?	–
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Bezirk	Bezirk	k1gInSc1	Bezirk
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86408-12-4	[number]	k4	80-86408-12-4
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Panoch	Panoch	k1gMnSc1	Panoch
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Řepa	Řepa	k1gMnSc1	Řepa
–	–	k?	–
pardubický	pardubický	k2eAgMnSc1d1	pardubický
architekt	architekt	k1gMnSc1	architekt
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nejistot	nejistota	k1gFnPc2	nejistota
<g/>
,	,	kIx,	,
Helios	Heliosa	k1gFnPc2	Heliosa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85211-15-7	[number]	k4	80-85211-15-7
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Panoch	Panoch	k1gMnSc1	Panoch
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
:	:	kIx,	:
Slavné	slavný	k2eAgFnSc2d1	slavná
vily	vila	k1gFnSc2	vila
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Foibos	Foibos	k1gMnSc1	Foibos
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87073-12-4	[number]	k4	978-80-87073-12-4
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Sakař	sakař	k1gMnSc1	sakař
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc4	dějiny
Pardubic	Pardubice	k1gInPc2	Pardubice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
5	[number]	k4	5
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
:	:	kIx,	:
nákladem	náklad	k1gInSc7	náklad
města	město	k1gNnSc2	město
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hradubice	Hradubice	k1gFnSc1	Hradubice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Pardubice	Pardubice	k1gInPc4	Pardubice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pardubice	Pardubice	k1gInPc1	Pardubice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Pardubice	Pardubice	k1gInPc1	Pardubice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Pardubice	Pardubice	k1gInPc1	Pardubice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Pardubice	Pardubice	k1gInPc1	Pardubice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Infocentrum	infocentrum	k1gNnSc1	infocentrum
Pardubice	Pardubice	k1gInPc1	Pardubice
</s>
</p>
