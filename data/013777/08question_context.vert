<s>
Tejovití	Tejovití	k1gMnPc1
(	(	kIx(
<g/>
Teiidae	Teiidae	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
ještěrů	ještěr	k1gMnPc2
<g/>
,	,	kIx,
rozšířená	rozšířený	k2eAgFnSc1d1
především	především	k9
v	v	k7c6
neotropické	neotropický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
zasahují	zasahovat	k5eAaImIp3nP
i	i	k9
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>