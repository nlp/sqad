<s>
Tejovití	Tejovití	k1gMnPc1
</s>
<s>
Tejovití	Tejovití	k1gMnPc1
Teju	Tejus	k1gInSc2
žakruarú	žakruarú	k?
(	(	kIx(
<g/>
Tupinambis	Tupinambis	k1gInSc1
teguixin	teguixin	k1gInSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
Lepidosauria	Lepidosaurium	k1gNnPc4
Řád	řád	k1gInSc1
</s>
<s>
šupinatí	šupinatit	k5eAaImIp3nP
(	(	kIx(
<g/>
Squamata	Squama	k1gNnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
ještěři	ještěr	k1gMnPc1
(	(	kIx(
<g/>
Sauria	Saurium	k1gNnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
tejovití	tejovití	k1gMnPc1
(	(	kIx(
<g/>
Teiidae	Teiidae	k1gInSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tejovití	Tejovití	k1gMnPc1
(	(	kIx(
<g/>
Teiidae	Teiidae	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
ještěrů	ještěr	k1gMnPc2
<g/>
,	,	kIx,
rozšířená	rozšířený	k2eAgFnSc1d1
především	především	k9
v	v	k7c6
neotropické	neotropický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
zasahují	zasahovat	k5eAaImIp3nP
i	i	k9
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tejovití	Tejovitý	k2eAgMnPc1d1
ještěři	ještěr	k1gMnPc1
jsou	být	k5eAaImIp3nP
neobyčejně	obyčejně	k6eNd1
rozmanití	rozmanitý	k2eAgMnPc1d1
ve	v	k7c6
velikosti	velikost	k1gFnSc6
i	i	k8xC
tvaru	tvar	k1gInSc6
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
mají	mít	k5eAaImIp3nP
dlouhý	dlouhý	k2eAgInSc4d1
lámavý	lámavý	k2eAgInSc4d1
ocas	ocas	k1gInSc4
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
vyvinuté	vyvinutý	k2eAgFnPc4d1
končetiny	končetina	k1gFnPc4
a	a	k8xC
rozeklaný	rozeklaný	k2eAgInSc4d1
<g/>
,	,	kIx,
zatažitelný	zatažitelný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typická	typický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gFnPc4
mohutná	mohutný	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
<g/>
,	,	kIx,
krytá	krytý	k2eAgFnSc1d1
nápadnými	nápadný	k2eAgInPc7d1
štítky	štítek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pozemní	pozemní	k2eAgMnPc4d1
ještěry	ještěr	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
najdeme	najít	k5eAaPmIp1nP
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
i	i	k9
šplhavé	šplhavý	k2eAgInPc4d1
či	či	k8xC
obojživelné	obojživelný	k2eAgInPc4d1
druhy	druh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
Tejovití	Tejovitý	k2eAgMnPc1d1
ještěři	ještěr	k1gMnPc1
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
rozmanití	rozmanitý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
zajímavým	zajímavý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
biologické	biologický	k2eAgFnSc2d1
konvergence	konvergence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInPc1d2
druhy	druh	k1gInPc1
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
zástupci	zástupce	k1gMnPc1
rodů	rod	k1gInPc2
Tupinambis	Tupinambis	k1gInSc1
a	a	k8xC
Callopistes	Callopistes	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nápadně	nápadně	k6eAd1
podobají	podobat	k5eAaImIp3nP
starosvětským	starosvětský	k2eAgMnPc3d1
varanům	varan	k1gMnPc3
<g/>
,	,	kIx,
menší	malý	k2eAgInPc1d2
druhy	druh	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
rody	rod	k1gInPc1
Cnemidophorus	Cnemidophorus	k1gInSc1
<g/>
,	,	kIx,
Ameiva	Ameiva	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
zase	zase	k9
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
zastupují	zastupovat	k5eAaImIp3nP
ještěrky	ještěrka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupcem	zástupce	k1gMnSc7
obojživelných	obojživelný	k2eAgInPc2d1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
Crocodilurus	Crocodilurus	k1gInSc1
nebo	nebo	k8xC
známá	známý	k2eAgFnSc1d1
dracéna	dracéna	k1gFnSc1
guyanská	guyanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dracaena	Dracaen	k2eAgFnSc1d1
guianensis	guianensis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
největším	veliký	k2eAgInSc7d3
druhem	druh	k1gInSc7
čeledi	čeleď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorůstá	dorůstat	k5eAaImIp3nS
délky	délka	k1gFnPc4
až	až	k9
130	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštností	zvláštnost	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
druh	druh	k1gInSc1
Echinosaura	Echinosaur	k1gMnSc2
horrida	horrid	k1gMnSc2
z	z	k7c2
pouští	poušť	k1gFnPc2
Peru	Peru	k1gNnSc2
a	a	k8xC
Ekvádoru	Ekvádor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
ostnitý	ostnitý	k2eAgMnSc1d1
pouštní	pouštní	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
,	,	kIx,
živící	živící	k2eAgMnSc1d1
se	se	k3xPyFc4
mravenci	mravenec	k1gMnPc1
<g/>
,	,	kIx,
vzdáleně	vzdáleně	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
australského	australský	k2eAgMnSc4d1
molocha	moloch	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Tejové	Tejus	k1gMnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
Americe	Amerika	k1gFnSc6
od	od	k7c2
jižní	jižní	k2eAgFnSc2d1
Kanady	Kanada	k1gFnSc2
(	(	kIx(
<g/>
druh	druh	k1gMnSc1
Cnemidophorus	Cnemidophorus	k1gMnSc1
sexlineatus	sexlineatus	k1gMnSc1
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
k	k	k7c3
Velkým	velký	k2eAgNnPc3d1
jezerům	jezero	k1gNnPc3
<g/>
)	)	kIx)
do	do	k7c2
Patagonie	Patagonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
některých	některý	k3yIgInPc6
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
Antilách	Antily	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
druhů	druh	k1gInPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Amazonie	Amazonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obývají	obývat	k5eAaImIp3nP
různá	různý	k2eAgNnPc4d1
prostředí	prostředí	k1gNnPc4
od	od	k7c2
pouští	poušť	k1gFnPc2
a	a	k8xC
polopouští	polopoušť	k1gFnPc2
přes	přes	k7c4
savany	savana	k1gFnPc4
až	až	k9
po	po	k7c4
deštné	deštný	k2eAgInPc4d1
pralesy	prales	k1gInPc4
<g/>
,	,	kIx,
bažiny	bažina	k1gFnPc4
a	a	k8xC
břehy	břeh	k1gInPc4
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
tejovití	tejovitý	k2eAgMnPc1d1
ještěři	ještěr	k1gMnPc1
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
zástupci	zástupce	k1gMnPc1
rodu	rod	k1gInSc2
Ameiva	Ameiva	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
synantropní	synantropný	k2eAgMnPc1d1
a	a	k8xC
adaptovali	adaptovat	k5eAaBmAgMnP
se	se	k3xPyFc4
na	na	k7c4
prostředí	prostředí	k1gNnSc4
plantáží	plantáž	k1gFnPc2
<g/>
,	,	kIx,
městských	městský	k2eAgInPc2d1
parků	park	k1gInPc2
či	či	k8xC
zahrad	zahrada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Tejové	Tejus	k1gMnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
masožraví	masožravý	k2eAgMnPc1d1
nebo	nebo	k8xC
všežraví	všežravý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drobné	drobný	k2eAgInPc1d1
druhy	druh	k1gInPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
především	především	k9
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
často	často	k6eAd1
loví	lovit	k5eAaImIp3nS
ve	v	k7c6
spadaném	spadaný	k2eAgNnSc6d1
listí	listí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInPc1d2
druhy	druh	k1gInPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
drobnými	drobný	k2eAgMnPc7d1
obratlovci	obratlovec	k1gMnPc7
<g/>
,	,	kIx,
ptačími	ptačí	k2eAgNnPc7d1
vejci	vejce	k1gNnPc7
a	a	k8xC
často	často	k6eAd1
také	také	k9
ovocem	ovoce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
velcí	velký	k2eAgMnPc1d1
tejové	tejus	k1gMnPc1
rodu	rod	k1gInSc2
Tupinambis	Tupinambis	k1gInSc1
jsou	být	k5eAaImIp3nP
známí	známý	k1gMnPc1
jako	jako	k8xC,k8xS
škůdci	škůdce	k1gMnPc1
drůbeže	drůbež	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
rádi	rád	k2eAgMnPc1d1
loví	lovit	k5eAaImIp3nP
domácí	domácí	k2eAgNnPc4d1
kuřata	kuře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dracéna	dracéna	k1gFnSc1
guyanská	guyanský	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
potravní	potravní	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
<g/>
,	,	kIx,
živí	živit	k5eAaImIp3nS
se	s	k7c7
především	především	k6eAd1
vodními	vodní	k2eAgMnPc7d1
plži	plž	k1gMnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
ulity	ulita	k1gFnPc1
drtí	drtit	k5eAaImIp3nP
silnými	silný	k2eAgFnPc7d1
čelistmi	čelist	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Všichni	všechen	k3xTgMnPc1
tejové	tejus	k1gMnPc1
jsou	být	k5eAaImIp3nP
vejcorodí	vejcorodý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc1
vejce	vejce	k1gNnPc1
mívají	mívat	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
kožovitou	kožovitý	k2eAgFnSc4d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
vápenitou	vápenitý	k2eAgFnSc4d1
skořápku	skořápka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
vajec	vejce	k1gNnPc2
ve	v	k7c6
snůšce	snůška	k1gFnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
různý	různý	k2eAgInSc1d1
<g/>
,	,	kIx,
od	od	k7c2
2	#num#	k4
do	do	k7c2
80	#num#	k4
<g/>
,	,	kIx,
velké	velký	k2eAgInPc1d1
druhy	druh	k1gInPc1
kladou	klást	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
více	hodně	k6eAd2
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
kladou	klást	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
do	do	k7c2
společných	společný	k2eAgNnPc2d1
líhnišť	líhniště	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
může	moct	k5eAaImIp3nS
nashromáždit	nashromáždit	k5eAaPmF
i	i	k9
několik	několik	k4yIc4
stovek	stovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavý	zajímavý	k2eAgInSc1d1
způsob	způsob	k1gInSc1
rozmnožování	rozmnožování	k1gNnSc2
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
u	u	k7c2
velkých	velký	k2eAgInPc2d1
tejů	tej	k1gInPc2
rodu	rod	k1gInSc2
Tupinambis	Tupinambis	k1gFnPc2
<g/>
,	,	kIx,
dracény	dracéna	k1gFnSc2
a	a	k8xC
některých	některý	k3yIgInPc2
dalších	další	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
ještěři	ještěr	k1gMnPc1
kladou	klást	k5eAaImIp3nP
vejce	vejce	k1gNnSc4
do	do	k7c2
termitišť	termitiště	k1gNnPc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
stromových	stromový	k2eAgInPc2d1
termitů	termit	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mají	mít	k5eAaImIp3nP
optimální	optimální	k2eAgFnSc4d1
vlhkost	vlhkost	k1gFnSc4
i	i	k8xC
teplotu	teplota	k1gFnSc4
pro	pro	k7c4
vývin	vývin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
po	po	k7c6
vylíhnutí	vylíhnutí	k1gNnSc6
nakrmí	nakrmit	k5eAaPmIp3nP
termity	termit	k1gInPc4
a	a	k8xC
teprve	teprve	k6eAd1
potom	potom	k6eAd1
se	se	k3xPyFc4
prohrabou	prohrabat	k5eAaPmIp3nP
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
bičochvostů	bičochvosta	k1gMnPc2
(	(	kIx(
<g/>
Cnemidophorus	Cnemidophorus	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
zaznamenána	zaznamenat	k5eAaPmNgFnS
partenogeneze	partenogeneze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lidské	lidský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Jihoameričtí	jihoamerický	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
loví	lovit	k5eAaImIp3nP
větší	veliký	k2eAgInPc4d2
druhy	druh	k1gInPc4
tejovitých	tejovitý	k2eAgMnPc2d1
ještěrů	ještěr	k1gMnPc2
pro	pro	k7c4
maso	maso	k1gNnSc4
a	a	k8xC
kůži	kůže	k1gFnSc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
leguány	leguán	k1gMnPc4
zelené	zelená	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mytologii	mytologie	k1gFnSc6
kmene	kmen	k1gInSc2
Guaraní	Guaraní	k1gNnSc2
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
Teju	Tejus	k1gInSc2
Jagua	Jaguum	k1gNnSc2
<g/>
,	,	kIx,
božstvo	božstvo	k1gNnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
ještěra	ještěr	k1gMnSc2
teju	teju	k6eAd1
se	s	k7c7
sedmi	sedm	k4xCc7
hlavami	hlava	k1gFnPc7
a	a	k8xC
ohnivýma	ohnivý	k2eAgNnPc7d1
očima	oko	k1gNnPc7
(	(	kIx(
<g/>
podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
teju	teju	k6eAd1
s	s	k7c7
hlavou	hlava	k1gFnSc7
psa	pes	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
střeží	střežit	k5eAaImIp3nS
podzemní	podzemní	k2eAgInSc1d1
svět	svět	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
vládcem	vládce	k1gMnSc7
pokladů	poklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
ze	z	k7c2
sedmi	sedm	k4xCc2
bratrů	bratr	k1gMnPc2
<g/>
,	,	kIx,
synů	syn	k1gMnPc2
lidské	lidský	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
Kerany	Kerana	k1gFnSc2
a	a	k8xC
zlého	zlý	k2eAgMnSc2d1
dcuha	dcuh	k1gMnSc2
Taú	Taú	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
následkem	následkem	k7c2
kletby	kletba	k1gFnSc2
bohyně	bohyně	k1gFnSc2
Měsíce	měsíc	k1gInSc2
Arasy	Arasa	k1gFnSc2
narodili	narodit	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
monstra	monstrum	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
teráriu	terárium	k1gNnSc6
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
tejové	tejus	k1gMnPc1
jsou	být	k5eAaImIp3nP
chováni	chovat	k5eAaImNgMnP
jako	jako	k9
terarijní	terarijní	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
efektivní	efektivní	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
často	často	k6eAd1
chována	chován	k2eAgFnSc1d1
dracéna	dracéna	k1gFnSc1
guyanská	guyanský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
ji	on	k3xPp3gFnSc4
můžeme	moct	k5eAaImIp1nP
spatřit	spatřit	k5eAaPmF
v	v	k7c6
ZOO	zoo	k1gNnSc6
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
i	i	k8xC
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímaví	zajímavý	k2eAgMnPc1d1
a	a	k8xC
velmi	velmi	k6eAd1
vytrvalí	vytrvalý	k2eAgMnPc1d1
chovanci	chovanec	k1gMnPc1
jsou	být	k5eAaImIp3nP
i	i	k9
zástupci	zástupce	k1gMnPc1
rodu	rod	k1gInSc2
Tupinambis	Tupinambis	k1gFnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
velikost	velikost	k1gFnSc4
příliš	příliš	k6eAd1
nehodí	hodit	k5eNaPmIp3nS
pro	pro	k7c4
běžná	běžný	k2eAgNnPc4d1
bytová	bytový	k2eAgNnPc4d1
terária	terárium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
"	"	kIx"
<g/>
inteligentní	inteligentní	k2eAgMnPc1d1
<g/>
"	"	kIx"
ještěři	ještěr	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
naučí	naučit	k5eAaPmIp3nS
znát	znát	k5eAaImF
svého	svůj	k3xOyFgMnSc4
chovatele	chovatel	k1gMnSc4
a	a	k8xC
snadno	snadno	k6eAd1
se	se	k3xPyFc4
ochočí	ochočit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
rozmnožují	rozmnožovat	k5eAaImIp3nP
jen	jen	k9
zřídka	zřídka	k6eAd1
a	a	k8xC
mají	mít	k5eAaImIp3nP
také	také	k9
sklon	sklon	k1gInSc4
k	k	k7c3
tučnění	tučnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInPc1d2
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
ameivy	ameiva	k1gFnSc2
(	(	kIx(
<g/>
Ameiva	Ameiva	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
bičochovsti	bičochovst	k1gMnPc1
(	(	kIx(
<g/>
Cnemidophorus	Cnemidophorus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
atraktivně	atraktivně	k6eAd1
zbarvení	zbarvený	k2eAgMnPc1d1
ještěři	ještěr	k1gMnPc1
<g/>
.	.	kIx.
dovážejí	dovážet	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
jen	jen	k9
zřídka	zřídka	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
plaší	plachý	k2eAgMnPc1d1
a	a	k8xC
značně	značně	k6eAd1
choulostiví	choulostivět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čihař	čihař	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Teraristika	teraristika	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Felix	Felix	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Ještěři	ještěr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Procházka	Procházka	k1gMnSc1
amazonským	amazonský	k2eAgInSc7d1
pralesem	prales	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
zvířat	zvíře	k1gNnPc2
VII	VII	kA
-	-	kIx~
Obojživelníci	obojživelník	k1gMnPc1
<g/>
,	,	kIx,
plazi	plaz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vergnerová	Vergnerová	k1gFnSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
-	-	kIx~
Vergner	Vergner	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Chov	chov	k1gInSc1
terarijních	terarijní	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ana	Ana	k?
B.	B.	kA
Quadros	Quadrosa	k1gFnPc2
<g/>
,	,	kIx,
Pablo	Pablo	k1gNnSc1
Chafrat	Chafrat	k1gInSc1
&	&	k?
Hussam	Hussam	k1gInSc1
Zaher	Zaher	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
new	new	k?
teiid	teiid	k1gInSc1
lizard	lizard	k1gInSc1
of	of	k?
the	the	k?
genus	genus	k1gMnSc1
Callopistes	Callopistes	k1gMnSc1
Gravenhorst	Gravenhorst	k1gMnSc1
<g/>
,	,	kIx,
1838	#num#	k4
(	(	kIx(
<g/>
Squamata	Squama	k1gNnPc4
<g/>
,	,	kIx,
Teiidae	Teiida	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
from	from	k1gMnSc1
the	the	k?
lower	lower	k1gMnSc1
Miocene	Miocen	k1gInSc5
of	of	k?
Argentina	Argentina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Vertebrate	Vertebrat	k1gInSc5
Paleontology	paleontolog	k1gMnPc4
e	e	k0
<g/>
1484754	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2724634.2018	2724634.2018	k4
<g/>
.1484754	.1484754	k4
</s>
<s>
J.	J.	kA
G.	G.	kA
Capano	Capana	k1gFnSc5
<g/>
,	,	kIx,
S.	S.	kA
Moritz	moritz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
L.	L.	kA
Cieri	Cieri	k1gNnSc4
<g/>
,	,	kIx,
L.	L.	kA
Reveret	Reveret	k1gInSc1
&	&	k?
E.	E.	kA
L.	L.	kA
Brainerd	Brainerd	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rib	Rib	k1gFnSc1
Motions	Motionsa	k1gFnPc2
Don	Don	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
Completely	Completela	k1gFnSc2
Hinge	Hing	k1gFnSc2
on	on	k3xPp3gMnSc1
Joint	Joint	k1gMnSc1
Design	design	k1gInSc1
<g/>
:	:	kIx,
Costal	Costal	k1gMnSc1
Joint	Joint	k1gMnSc1
Anatomy	anatom	k1gMnPc4
and	and	k?
Ventilatory	Ventilator	k1gInPc1
Kinematics	Kinematics	k1gInSc1
in	in	k?
a	a	k8xC
Teiid	Teiida	k1gFnPc2
Lizard	Lizard	k1gMnSc1
<g/>
,	,	kIx,
Salvator	Salvator	k1gMnSc1
merianae	meriana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrative	Integrativ	k1gInSc5
Organismal	Organismal	k1gMnSc1
Biology	biolog	k1gMnPc4
1	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
oby	oby	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1093/iob/oby004	https://doi.org/10.1093/iob/oby004	k4
</s>
<s>
Augé	Augé	k1gNnSc1
Marc	Marc	k1gInSc1
Louis	Louis	k1gMnSc1
&	&	k?
Brizuela	Brizuela	k1gFnSc1
Santiago	Santiago	k1gNnSc4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transient	Transient	k1gInSc1
presence	presence	k1gFnSc2
of	of	k?
a	a	k8xC
teiid	teiid	k1gInSc1
lizard	lizard	k1gMnSc1
in	in	k?
the	the	k?
European	European	k1gInSc1
Eocene	Eocen	k1gInSc5
suggests	suggests	k1gInSc1
transatlantic	transatlantice	k1gFnPc2
dispersal	dispersat	k5eAaPmAgInS,k5eAaImAgInS
and	and	k?
rapid	rapid	k1gInSc1
extinction	extinction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeobiodiversity	Palaeobiodiversit	k1gInPc7
and	and	k?
Palaeoenvironments	Palaeoenvironments	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1007/s12549-019-00414-2	https://doi.org/10.1007/s12549-019-00414-2	k4
</s>
<s>
Robert	Robert	k1gMnSc1
L.	L.	kA
Cieri	Cier	k1gFnPc1
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
T.	T.	kA
Hatch	Hatch	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
G.	G.	kA
Capano	Capana	k1gFnSc5
&	&	k?
Elizabeth	Elizabeth	k1gFnPc2
L.	L.	kA
Brainerd	Brainerd	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Locomotor	Locomotor	k1gInSc1
rib	rib	k?
kinematics	kinematics	k1gInSc1
in	in	k?
two	two	k?
species	species	k1gFnSc2
of	of	k?
lizards	lizards	k1gInSc1
and	and	k?
a	a	k8xC
new	new	k?
hypothesis	hypothesis	k1gFnSc1
for	forum	k1gNnPc2
the	the	k?
evolution	evolution	k1gInSc1
of	of	k?
aspiration	aspiration	k1gInSc1
breathing	breathing	k1gInSc1
in	in	k?
amniotes	amniotes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientifice	k1gInPc2
Reports	Reportsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
number	number	k1gMnSc1
<g/>
:	:	kIx,
7739	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1038/s41598-020-64140-y	https://doi.org/10.1038/s41598-020-64140-a	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tejovití	tejovitý	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Teiidae	Teiidae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Tejovití	Tejovitý	k2eAgMnPc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Živočichové	živočich	k1gMnPc1
</s>
