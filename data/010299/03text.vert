<p>
<s>
Samojedské	samojedský	k2eAgInPc1d1	samojedský
jazyky	jazyk	k1gInPc1	jazyk
tvoří	tvořit	k5eAaImIp3nP	tvořit
společně	společně	k6eAd1	společně
s	s	k7c7	s
ugrofinskými	ugrofinský	k2eAgInPc7d1	ugrofinský
jazyky	jazyk	k1gInPc7	jazyk
uralskou	uralský	k2eAgFnSc4d1	Uralská
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
jimi	on	k3xPp3gMnPc7	on
mluví	mluvit	k5eAaImIp3nS	mluvit
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Uralského	uralský	k2eAgNnSc2d1	uralský
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejsevernějších	severní	k2eAgFnPc6d3	nejsevernější
částech	část	k1gFnPc6	část
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
prasamojedského	prasamojedský	k2eAgInSc2d1	prasamojedský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
na	na	k7c4	na
samojedské	samojedský	k2eAgInPc4d1	samojedský
jazyky	jazyk	k1gInPc4	jazyk
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
termín	termín	k1gInSc1	termín
samojedský	samojedský	k2eAgInSc1d1	samojedský
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
ruského	ruský	k2eAgNnSc2d1	ruské
slova	slovo	k1gNnSc2	slovo
neznámého	známý	k2eNgNnSc2d1	neznámé
<g/>
,	,	kIx,	,
snad	snad	k9	snad
přímo	přímo	k6eAd1	přímo
samojedského	samojedský	k2eAgInSc2d1	samojedský
původu	původ	k1gInSc2	původ
samojed	samojed	k1gMnSc1	samojed
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
с	с	k?	с
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
označovalo	označovat	k5eAaImAgNnS	označovat
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lidové	lidový	k2eAgFnSc2d1	lidová
etymologie	etymologie	k1gFnSc2	etymologie
objasňován	objasňovat	k5eAaImNgMnS	objasňovat
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
jako	jako	k8xS	jako
Samo-jed	Samoed	k1gMnSc1	Samo-jed
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
samo-pojídač	samoojídač	k1gInSc1	samo-pojídač
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ruský	ruský	k2eAgMnSc1d1	ruský
etnolog	etnolog	k1gMnSc1	etnolog
G.	G.	kA	G.
N.	N.	kA	N.
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
raději	rád	k6eAd2	rád
výraz	výraz	k1gInSc1	výraz
samodijský	samodijský	k2eAgInSc1d1	samodijský
<g/>
,	,	kIx,	,
odkazující	odkazující	k2eAgInSc1d1	odkazující
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
skupinám	skupina	k1gFnPc3	skupina
Něnců	Něnce	k1gMnPc2	Něnce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
srovnávací	srovnávací	k2eAgFnSc6d1	srovnávací
jazykovědě	jazykověda	k1gFnSc6	jazykověda
ani	ani	k8xC	ani
antropologii	antropologie	k1gFnSc3	antropologie
neuchytil	uchytit	k5eNaPmAgMnS	uchytit
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
teorie	teorie	k1gFnPc1	teorie
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
kompozitní	kompozitní	k2eAgNnSc1d1	kompozitní
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
laponský	laponský	k2eAgInSc1d1	laponský
výraz	výraz	k1gInSc1	výraz
Same-edne	Samedne	k1gFnSc2	Same-edne
–	–	k?	–
'	'	kIx"	'
<g/>
země	zem	k1gFnPc1	zem
Sámů	Sámo	k1gMnPc2	Sámo
<g/>
'	'	kIx"	'
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
podobný	podobný	k2eAgInSc4d1	podobný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
k	k	k7c3	k
Samojedům	Samojed	k1gMnPc3	Samojed
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
podobná	podobný	k2eAgFnSc1d1	podobná
pravdě	pravda	k1gFnSc3	pravda
je	být	k5eAaImIp3nS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
o	o	k7c6	o
původu	původ	k1gInSc6	původ
etnonyma	etnonymum	k1gNnSc2	etnonymum
Samojed	Samojed	k1gMnSc1	Samojed
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
označení	označení	k1gNnSc2	označení
některých	některý	k3yIgFnPc2	některý
skupin	skupina	k1gFnPc2	skupina
Enců	Enc	k1gMnPc2	Enc
"	"	kIx"	"
<g/>
Somaté	Somatá	k1gFnPc1	Somatá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Samojedské	samojedský	k2eAgInPc1d1	samojedský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInPc1d1	severní
samojedské	samojedský	k2eAgInPc1d1	samojedský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Enečtina	Enečtina	k1gFnSc1	Enečtina
(	(	kIx(	(
<g/>
jenisejská	jenisejský	k2eAgFnSc1d1	jenisejský
samojedština	samojedština	k1gFnSc1	samojedština
<g/>
)	)	kIx)	)
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Něnečtina	Něnečtina	k1gFnSc1	Něnečtina
(	(	kIx(	(
<g/>
juračtina	juračtina	k1gFnSc1	juračtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nganasanština	Nganasanština	k1gFnSc1	Nganasanština
(	(	kIx(	(
<g/>
tavgijština	tavgijština	k1gFnSc1	tavgijština
<g/>
,	,	kIx,	,
tavgijská	tavgijský	k2eAgFnSc1d1	tavgijský
samojedština	samojedština	k1gFnSc1	samojedština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
samojedské	samojedský	k2eAgInPc1d1	samojedský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
†	†	k?	†
Kamasinština	Kamasinština	k1gFnSc1	Kamasinština
-	-	kIx~	-
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
†	†	k?	†
Matorština	Matorština	k1gFnSc1	Matorština
(	(	kIx(	(
<g/>
motorština	motorština	k1gFnSc1	motorština
<g/>
)	)	kIx)	)
–	–	k?	–
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
†	†	k?	†
Matorsko-tajgijsko-karagský	Matorskoajgijskoaragský	k2eAgInSc1d1	Matorsko-tajgijsko-karagský
jazyk	jazyk	k1gInSc1	jazyk
–	–	k?	–
vymřelý	vymřelý	k2eAgInSc1d1	vymřelý
</s>
</p>
<p>
<s>
†	†	k?	†
Koibalština	Koibalština	k1gFnSc1	Koibalština
–	–	k?	–
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Selkupština	Selkupština	k1gFnSc1	Selkupština
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Samoyedic	Samoyedic	k1gMnSc1	Samoyedic
languages	languages	k1gMnSc1	languages
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
