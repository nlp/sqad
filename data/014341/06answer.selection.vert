<s>
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
GEB	GEB	kA
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
465	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2656	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
amerického	americký	k2eAgMnSc2d1
akademika	akademik	k1gMnSc2
Douglase	Douglasa	k1gFnSc6
Hofstadtera	Hofstadter	k1gMnSc2
<g/>
,	,	kIx,
popsaná	popsaný	k2eAgFnSc1d1
autorem	autor	k1gMnSc7
jako	jako	k8xS,k8xC
<g/>
:	:	kIx,
„	„	k?
<g/>
metaforické	metaforický	k2eAgNnSc1d1
okno	okno	k1gNnSc1
do	do	k7c2
myslí	mysl	k1gFnPc2
a	a	k8xC
strojů	stroj	k1gInPc2
v	v	k7c6
duchu	duch	k1gMnSc6
Lewise	Lewise	k1gFnSc1
Carrolla	Carrolla	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>