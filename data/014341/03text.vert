<s>
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
</s>
<s>
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
Autor	autor	k1gMnSc1
</s>
<s>
Douglas	Douglas	k1gMnSc1
Hofstadter	Hofstadter	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
<g/>
:	:	kIx,
an	an	k?
Eternal	Eternal	k1gFnSc2
Golden	Goldna	k1gFnPc2
Braid	Braida	k1gFnPc2
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
popularizace	popularizace	k1gFnSc1
vědy	věda	k1gFnSc2
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Basic	Basic	kA
Books	Books	k1gInSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1979	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
I	i	k9
Am	Am	k1gFnSc1
a	a	k8xC
Strange	Strange	k1gFnSc1
Loop	Loop	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
GEB	GEB	kA
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
465	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2656	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
amerického	americký	k2eAgMnSc2d1
akademika	akademik	k1gMnSc2
Douglase	Douglasa	k1gFnSc6
Hofstadtera	Hofstadter	k1gMnSc2
<g/>
,	,	kIx,
popsaná	popsaný	k2eAgFnSc1d1
autorem	autor	k1gMnSc7
jako	jako	k8xS,k8xC
<g/>
:	:	kIx,
„	„	k?
<g/>
metaforické	metaforický	k2eAgNnSc1d1
okno	okno	k1gNnSc1
do	do	k7c2
myslí	mysl	k1gFnPc2
a	a	k8xC
strojů	stroj	k1gInPc2
v	v	k7c6
duchu	duch	k1gMnSc6
Lewise	Lewise	k1gFnSc1
Carrolla	Carrolla	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
vydání	vydání	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
kniha	kniha	k1gFnSc1
získala	získat	k5eAaPmAgFnS
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
povrchu	povrch	k1gInSc6
GEB	GEB	kA
zkoumá	zkoumat	k5eAaImIp3nS
logika	logika	k1gFnSc1
Kurta	Kurt	k1gMnSc2
Gödela	Gödel	k1gMnSc2
<g/>
,	,	kIx,
umělce	umělec	k1gMnSc2
M.	M.	kA
C.	C.	kA
Eschera	Escher	k1gMnSc4
a	a	k8xC
skladatele	skladatel	k1gMnSc4
Johanna	Johann	k1gMnSc4
Sebastiana	Sebastian	k1gMnSc4
Bacha	Bacha	k?
a	a	k8xC
diskutuje	diskutovat	k5eAaImIp3nS
o	o	k7c6
běžných	běžný	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
v	v	k7c4
jejich	jejich	k3xOp3gFnSc4
práci	práce	k1gFnSc4
a	a	k8xC
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
povrchem	povrch	k1gInSc7
kniha	kniha	k1gFnSc1
podrobně	podrobně	k6eAd1
a	a	k8xC
subtilně	subtilně	k6eAd1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
základních	základní	k2eAgInPc6d1
konceptech	koncept	k1gInPc6
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
symetrie	symetrie	k1gFnSc2
a	a	k8xC
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pomocí	pomocí	k7c2
příkladů	příklad	k1gInPc2
a	a	k8xC
analýz	analýza	k1gFnPc2
kniha	kniha	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
jak	jak	k6eAd1
sebe-reference	sebe-reference	k1gFnSc1
a	a	k8xC
formální	formální	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
umožňují	umožňovat	k5eAaImIp3nP
systémům	systém	k1gInPc3
získat	získat	k5eAaPmF
smysl	smysl	k1gInSc4
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
složeny	složit	k5eAaPmNgInP
z	z	k7c2
„	„	k?
<g/>
bezvýznamných	bezvýznamný	k2eAgFnPc2d1
<g/>
“	“	k?
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
smyslu	smysl	k1gInSc6
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
jak	jak	k6eAd1
zaznamenat	zaznamenat	k5eAaPmF
a	a	k8xC
vystihnout	vystihnout	k5eAaPmF
vědění	vědění	k1gNnSc4
<g/>
,	,	kIx,
o	o	k7c6
metodách	metoda	k1gFnPc6
a	a	k8xC
omezeních	omezení	k1gNnPc6
symbolické	symbolický	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
o	o	k7c6
základních	základní	k2eAgInPc6d1
principech	princip	k1gInPc6
„	„	k?
<g/>
smyslu	smysl	k1gInSc2
<g/>
“	“	k?
samotného	samotný	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
knihy	kniha	k1gFnSc2
</s>
<s>
GEB	GEB	kA
má	mít	k5eAaImIp3nS
formu	forma	k1gFnSc4
různých	různý	k2eAgInPc2d1
proplétajících	proplétající	k2eAgInPc2d1
se	se	k3xPyFc4
vyprávění	vyprávění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc4d1
kapitoly	kapitola	k1gFnPc4
střídají	střídat	k5eAaImIp3nP
dialogy	dialog	k1gInPc1
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
imaginárními	imaginární	k2eAgInPc7d1
charaktery	charakter	k1gInPc7
inspirovanými	inspirovaný	k2eAgInPc7d1
knihou	kniha	k1gFnSc7
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
řekla	říct	k5eAaPmAgFnS
želva	želva	k1gFnSc1
Achillovi	Achilles	k1gMnSc3
od	od	k7c2
Lewise	Lewise	k1gFnSc2
Carrolla	Carroll	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
Achilles	Achilles	k1gMnSc1
a	a	k8xC
Želvák	želvák	k1gMnSc1
řeší	řešit	k5eAaImIp3nS
paradox	paradox	k1gInSc4
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
modus	modus	k1gInSc4
ponens	ponensa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
na	na	k7c6
tomto	tento	k3xDgInSc6
dialogu	dialog	k1gInSc6
založil	založit	k5eAaPmAgInS
i	i	k9
další	další	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
a	a	k8xC
přidává	přidávat	k5eAaImIp3nS
postupně	postupně	k6eAd1
postavy	postava	k1gFnPc1
kraba	krab	k1gMnSc2
<g/>
,	,	kIx,
džina	džin	k1gMnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vyprávění	vyprávění	k1gNnPc1
často	často	k6eAd1
přejdou	přejít	k5eAaPmIp3nP
do	do	k7c2
sebereference	sebereference	k1gFnSc2
a	a	k8xC
metafikce	metafikce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
díle	dílo	k1gNnSc6
je	být	k5eAaImIp3nS
také	také	k9
často	často	k6eAd1
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
hra	hra	k1gFnSc1
se	s	k7c7
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovní	slovní	k2eAgFnPc4d1
hříčky	hříčka	k1gFnPc4
jsou	být	k5eAaImIp3nP
místy	místy	k6eAd1
použity	použít	k5eAaPmNgInP
k	k	k7c3
propojení	propojení	k1gNnSc3
myšlenek	myšlenka	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nevyhnutelně	vyhnutelně	k6eNd1
působí	působit	k5eAaImIp3nS
ohromujícím	ohromující	k2eAgInSc7d1
dojmem	dojem	k1gInSc7
při	při	k7c6
pokusu	pokus	k1gInSc6
tvořit	tvořit	k5eAaImF
prohlášení	prohlášení	k1gNnSc4
o	o	k7c4
sobě	se	k3xPyFc3
sama	sám	k3xTgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgFnPc4
slovní	slovní	k2eAgFnPc4d1
hříčky	hříčka	k1gFnPc4
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
tuto	tento	k3xDgFnSc4
knihu	kniha	k1gFnSc4
přeložit	přeložit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc1
dialog	dialog	k1gInSc1
je	být	k5eAaImIp3nS
napsán	napsat	k5eAaPmNgInS,k5eAaBmNgInS
formou	forma	k1gFnSc7
krabího	krabí	k2eAgInSc2d1
kánonu	kánon	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
každá	každý	k3xTgFnSc1
řádka	řádka	k1gFnSc1
před	před	k7c7
středem	střed	k1gInSc7
dialogu	dialog	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
stejné	stejný	k2eAgFnSc3d1
řádce	řádka	k1gFnSc3
od	od	k7c2
středu	střed	k1gInSc2
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konverzace	konverzace	k1gFnSc1
přesto	přesto	k8xC
dává	dávat	k5eAaImIp3nS
smysl	smysl	k1gInSc4
díky	díky	k7c3
použití	použití	k1gNnSc3
běžných	běžný	k2eAgFnPc2d1
frází	fráze	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
při	při	k7c6
pozdravu	pozdrav	k1gInSc6
na	na	k7c4
přivítanou	přivítaná	k1gFnSc4
i	i	k9
při	při	k7c6
odchodu	odchod	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
díky	díky	k7c3
umístění	umístění	k1gNnSc3
řádků	řádek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
při	při	k7c6
podrobném	podrobný	k2eAgNnSc6d1
zkoumání	zkoumání	k1gNnSc6
slouží	sloužit	k5eAaImIp3nS
i	i	k9
jako	jako	k9
odpovědi	odpověď	k1gFnPc1
na	na	k7c4
otázky	otázka	k1gFnPc4
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
řádku	řádek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Témata	téma	k1gNnPc1
knihy	kniha	k1gFnSc2
</s>
<s>
GEB	GEB	kA
obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
případů	případ	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
objekty	objekt	k1gInPc4
a	a	k8xC
myšlenky	myšlenka	k1gFnPc4
mluví	mluvit	k5eAaImIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
odkazují	odkazovat	k5eAaImIp3nP
na	na	k7c4
sebe	sebe	k3xPyFc4
sama	sám	k3xTgMnSc4
(	(	kIx(
<g/>
rekurze	rekurze	k1gFnPc1
<g/>
,	,	kIx,
sebe-reference	sebe-referenec	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
takzvaná	takzvaný	k2eAgFnSc1d1
typografická	typografický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Typographical	Typographical	k1gFnSc1
Number	Numbra	k1gFnPc2
Theory	Theora	k1gFnSc2
<g/>
,	,	kIx,
TNT	TNT	kA
<g/>
)	)	kIx)
ilustruje	ilustrovat	k5eAaBmIp3nS
Gödelovy	Gödelův	k2eAgFnPc4d1
věty	věta	k1gFnPc4
o	o	k7c6
neúplnosti	neúplnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
fonograf	fonograf	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zničí	zničit	k5eAaPmIp3nS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
při	při	k7c6
přehrávání	přehrávání	k1gNnSc6
skladby	skladba	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Nemohu	moct	k5eNaImIp1nS
být	být	k5eAaImF
hrána	hrát	k5eAaImNgFnS
na	na	k7c6
přehrávači	přehrávač	k1gInSc6
X	X	kA
<g/>
;	;	kIx,
zkoumání	zkoumání	k1gNnSc4
hudební	hudební	k2eAgFnSc2d1
formy	forma	k1gFnSc2
kánonu	kánon	k1gInSc2
nebo	nebo	k8xC
zamyšlení	zamyšlení	k1gNnSc1
se	se	k3xPyFc4
nad	nad	k7c7
Escherovým	Escherův	k2eAgMnSc7d1
litografem	litograf	k1gMnSc7
dvou	dva	k4xCgInPc2
rukou	ruka	k1gFnPc2
kreslících	kreslící	k2eAgInPc2d1
samy	sám	k3xTgFnPc1
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
popsání	popsání	k1gNnSc3
takových	takový	k3xDgInPc2
sebe-odkazujících	sebe-odkazující	k2eAgInPc2d1
se	se	k3xPyFc4
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
vymyslel	vymyslet	k5eAaPmAgMnS
Hofstadter	Hofstadter	k1gMnSc1
termín	termín	k1gInSc4
divná	divný	k2eAgFnSc1d1
smyčka	smyčka	k1gFnSc1
<g/>
,	,	kIx,
koncept	koncept	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zkoumá	zkoumat	k5eAaImIp3nS
v	v	k7c6
ještě	ještě	k6eAd1
větší	veliký	k2eAgFnSc6d2
hloubce	hloubka	k1gFnSc6
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
následující	následující	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Jsem	být	k5eAaImIp1nS
divná	divný	k2eAgFnSc1d1
smyčka	smyčka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
únik	únik	k1gInSc4
z	z	k7c2
mnoha	mnoho	k4c2
logických	logický	k2eAgNnPc2d1
protiřečení	protiřečení	k1gNnPc2
způsobených	způsobený	k2eAgFnPc2d1
sebe-odkazujícími	sebe-odkazující	k2eAgFnPc7d1
se	se	k3xPyFc4
objekty	objekt	k1gInPc7
autor	autor	k1gMnSc1
používá	používat	k5eAaImIp3nS
Zenové	zenový	k2eAgInPc4d1
kóany	kóan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
ukázat	ukázat	k5eAaPmF
čtenářům	čtenář	k1gMnPc3
jak	jak	k6eAd1
vnímat	vnímat	k5eAaImF
realitu	realita	k1gFnSc4
za	za	k7c7
hranicemi	hranice	k1gFnPc7
běžných	běžný	k2eAgFnPc2d1
představ	představa	k1gFnPc2
jejich	jejich	k3xOp3gFnPc2
vlastních	vlastní	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
a	a	k8xC
jak	jak	k6eAd1
uchopit	uchopit	k5eAaPmF
paradoxní	paradoxní	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
odmítnutím	odmítnutí	k1gNnSc7
premisy	premisa	k1gFnSc2
-	-	kIx~
strategie	strategie	k1gFnSc2
zvaná	zvaný	k2eAgFnSc1d1
Mu-negace	Mu-negace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc4d1
téma	téma	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
GEB	GEB	kA
rozebírá	rozebírat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
dynamické	dynamický	k2eAgInPc1d1
zásobníky	zásobník	k1gInPc1
–	–	k?
jeden	jeden	k4xCgInSc4
dialog	dialog	k1gInSc4
popisuje	popisovat	k5eAaImIp3nS
dobrodružství	dobrodružství	k1gNnSc4
Achilla	Achilles	k1gMnSc2
a	a	k8xC
želvy	želva	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
„	„	k?
<g/>
přidávají	přidávat	k5eAaImIp3nP
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
odebírají	odebírat	k5eAaImIp3nP
<g/>
“	“	k?
toniky	tonik	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstup	vstup	k1gInSc1
do	do	k7c2
obrázku	obrázek	k1gInSc2
v	v	k7c6
knize	kniha	k1gFnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
„	„	k?
<g/>
přidání	přidání	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
vstupem	vstup	k1gInSc7
do	do	k7c2
obrázku	obrázek	k1gInSc2
v	v	k7c6
knize	kniha	k1gFnSc6
v	v	k7c6
obrázku	obrázek	k1gInSc6
v	v	k7c6
knize	kniha	k1gFnSc6
vznikne	vzniknout	k5eAaPmIp3nS
dvojité	dvojitý	k2eAgNnSc1d1
„	„	k?
<g/>
přidání	přidání	k1gNnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
odebírání	odebírání	k1gNnSc1
<g/>
“	“	k?
odpovídá	odpovídat	k5eAaImIp3nS
odchodu	odchod	k1gInSc2
z	z	k7c2
předchozí	předchozí	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
reality	realita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Želvák	želvák	k1gMnSc1
vtipně	vtipně	k6eAd1
poznamenává	poznamenávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
„	„	k?
<g/>
odebírání	odebírání	k1gNnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
odchod	odchod	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
aktuální	aktuální	k2eAgFnSc6d1
realitě	realita	k1gFnSc6
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
o	o	k7c6
něm	on	k3xPp3gMnSc6
nikdo	nikdo	k3yNnSc1
neslyšel	slyšet	k5eNaImAgMnS
<g/>
;	;	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
vyvstává	vyvstávat	k5eAaImIp3nS
otázka	otázka	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Přestal	přestat	k5eAaPmAgMnS
přítel	přítel	k1gMnSc1
jednoduše	jednoduše	k6eAd1
existovat	existovat	k5eAaImF
nebo	nebo	k8xC
dosáhl	dosáhnout	k5eAaPmAgMnS
vyššího	vysoký	k2eAgInSc2d2
stavu	stav	k1gInSc2
reality	realita	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
A	a	k9
dále	daleko	k6eAd2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
čtenář	čtenář	k1gMnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
přidán	přidán	k2eAgInSc1d1
<g/>
“	“	k?
do	do	k7c2
světa	svět	k1gInSc2
Želváka	želvák	k1gMnSc2
a	a	k8xC
Achilla	Achilles	k1gMnSc2
<g/>
,	,	kIx,
nepovznesl	povznést	k5eNaPmAgMnS
se	se	k3xPyFc4
tento	tento	k3xDgMnSc1
přítel	přítel	k1gMnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
samé	samý	k3xTgFnSc2
úrovně	úroveň	k1gFnSc2
reality	realita	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
současně	současně	k6eAd1
přebývá	přebývat	k5eAaImIp3nS
čtenář	čtenář	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
rozebírají	rozebírat	k5eAaImIp3nP
základní	základní	k2eAgNnPc4d1
dogmata	dogma	k1gNnPc4
logiky	logika	k1gFnSc2
<g/>
,	,	kIx,
sebe-odkazující	sebe-odkazující	k2eAgInPc1d1
se	se	k3xPyFc4
příkazy	příkaz	k1gInPc7
<g/>
,	,	kIx,
netypované	typovaný	k2eNgNnSc1d1
systém	systém	k1gInSc4
a	a	k8xC
dokonce	dokonce	k9
programování	programování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jedna	jeden	k4xCgFnSc1
hádanka	hádanka	k1gFnSc1
v	v	k7c6
dialogu	dialog	k1gInSc6
Árie	árie	k1gFnSc2
s	s	k7c7
rozmanitými	rozmanitý	k2eAgFnPc7d1
variantami	varianta	k1gFnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Aria	Aria	k1gFnSc1
with	witha	k1gFnPc2
Diverse	diverse	k1gFnSc1
Variations	Variations	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
spekulací	spekulace	k1gFnSc7
o	o	k7c6
autorovi	autor	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
píše	psát	k5eAaImIp3nS
knihu	kniha	k1gFnSc4
a	a	k8xC
rozhodne	rozhodnout	k5eAaPmIp3nS
se	se	k3xPyFc4
ukončit	ukončit	k5eAaPmF
knihu	kniha	k1gFnSc4
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
přestal	přestat	k5eAaPmAgMnS
psát	psát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgMnSc1
autor	autor	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
náhle	náhle	k6eAd1
(	(	kIx(
<g/>
„	„	k?
<g/>
náhle	náhle	k6eAd1
<g/>
“	“	k?
z	z	k7c2
hlediska	hledisko	k1gNnSc2
zápletky	zápletka	k1gFnSc2
<g/>
)	)	kIx)
překvapit	překvapit	k5eAaPmF
čtenáře	čtenář	k1gMnPc4
zakončením	zakončení	k1gNnSc7
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
zřejmému	zřejmý	k2eAgInSc3d1
fyzickému	fyzický	k2eAgInSc3d1
faktu	fakt	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
několik	několik	k4yIc1
málo	málo	k4c1
stránek	stránka	k1gFnPc2
zbývá	zbývat	k5eAaImIp3nS
do	do	k7c2
konce	konec	k1gInSc2
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgMnSc1
autor	autor	k1gMnSc1
může	moct	k5eAaImIp3nS
„	„	k?
<g/>
zabalit	zabalit	k5eAaPmF
<g/>
“	“	k?
hlavní	hlavní	k2eAgFnPc4d1
pointy	pointa	k1gFnPc4
a	a	k8xC
pak	pak	k6eAd1
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
psaní	psaní	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
nechá	nechat	k5eAaPmIp3nS
čtenáři	čtenář	k1gMnSc3
vodítka	vodítko	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
příběh	příběh	k1gInSc1
již	již	k6eAd1
vlastně	vlastně	k9
skončil	skončit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
bludné	bludný	k2eAgInPc4d1
nebo	nebo	k8xC
bezmyšlenkovité	bezmyšlenkovitý	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
nepravdy	nepravda	k1gFnPc4
nebo	nebo	k8xC
kontradikce	kontradikce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
čtení	čtení	k1gNnSc6
posledních	poslední	k2eAgFnPc2d1
částí	část	k1gFnPc2
„	„	k?
<g/>
Árie	árie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
neboli	neboli	k8xC
knihy	kniha	k1gFnPc1
GEB	GEB	kA
jako	jako	k8xC,k8xS
celku	celek	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
povšimnout	povšimnout	k5eAaPmF
jistých	jistý	k2eAgFnPc2d1
zvláštností	zvláštnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
kniha	kniha	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
narážkou	narážka	k1gFnSc7
na	na	k7c4
autora	autor	k1gMnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
konec	konec	k1gInSc1
závěrečného	závěrečný	k2eAgInSc2d1
dialogu	dialog	k1gInSc2
je	být	k5eAaImIp3nS
výzva	výzva	k1gFnSc1
Želváka	želvák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vysvětlováno	vysvětlovat	k5eAaImNgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
celá	celý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
autorovou	autorův	k2eAgFnSc7d1
odpovědí	odpověď	k1gFnSc7
na	na	k7c4
Želvákovu	želvákův	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
by	by	kYmCp3nS
ale	ale	k8xC
celá	celý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
formu	forma	k1gFnSc4
nekonečně	konečně	k6eNd1
vyvstávajícího	vyvstávající	k2eAgInSc2d1
kánonu	kánon	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
autor	autor	k1gMnSc1
cituje	citovat	k5eAaBmIp3nS
autora	autor	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
cituje	citovat	k5eAaBmIp3nS
autora	autor	k1gMnSc4
a	a	k8xC
tak	tak	k6eAd1
donekonečna	donekonečna	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Přestože	přestože	k8xS
Hofstadter	Hofstadter	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
myšlenka	myšlenka	k1gFnSc1
překladu	překlad	k1gInSc2
jeho	jeho	k3xOp3gFnSc4
knihu	kniha	k1gFnSc4
jej	on	k3xPp3gInSc4
při	při	k7c6
psaní	psaní	k1gNnSc6
této	tento	k3xDgFnSc2
knihy	kniha	k1gFnSc2
nikdy	nikdy	k6eAd1
nenapadla	napadnout	k5eNaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
s	s	k7c7
touto	tento	k3xDgFnSc7
myšlenkou	myšlenka	k1gFnSc7
přišel	přijít	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
vydavatel	vydavatel	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
velmi	velmi	k6eAd1
nadšen	nadchnout	k5eAaPmNgMnS
z	z	k7c2
představy	představa	k1gFnSc2
vidět	vidět	k5eAaImF
tuto	tento	k3xDgFnSc4
knihu	kniha	k1gFnSc4
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
zejména	zejména	k9
…	…	k?
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
by	by	kYmCp3nS
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
milión	milión	k4xCgInSc1
otázek	otázka	k1gFnPc2
ke	k	k7c3
zvážení	zvážení	k1gNnSc3
<g/>
“	“	k?
při	při	k7c6
překladu	překlad	k1gInSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
kniha	kniha	k1gFnSc1
nespočívá	spočívat	k5eNaImIp3nS
jen	jen	k9
na	na	k7c6
slovních	slovní	k2eAgFnPc6d1
hříčkách	hříčka	k1gFnPc6
ale	ale	k8xC
také	také	k9
na	na	k7c4
„	„	k?
<g/>
strukturálních	strukturální	k2eAgFnPc2d1
hříčkách	hříčka	k1gFnPc6
<g/>
“	“	k?
-	-	kIx~
kde	kde	k6eAd1
forma	forma	k1gFnSc1
a	a	k8xC
obsah	obsah	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
zrcadlí	zrcadlit	k5eAaImIp3nP
jedna	jeden	k4xCgFnSc1
druhou	druhý	k4xOgFnSc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
například	například	k6eAd1
rozhovor	rozhovor	k1gInSc4
formou	forma	k1gFnSc7
„	„	k?
<g/>
krabího	krabí	k2eAgInSc2d1
kánonu	kánon	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
čte	číst	k5eAaImIp3nS
téměř	téměř	k6eAd1
stejně	stejně	k6eAd1
popředu	popříst	k5eAaPmIp1nS
i	i	k9
pozpátku	pozpátku	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Autor	autor	k1gMnSc1
podává	podávat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
příklad	příklad	k1gInSc4
překladatelského	překladatelský	k2eAgInSc2d1
oříšku	oříšek	k1gInSc2
v	v	k7c6
odstavci	odstavec	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Pan	Pan	k1gMnSc1
Želvák	želvák	k1gMnSc1
potkává	potkávat	k5eAaImIp3nS
paní	paní	k1gFnSc4
Želvu	želva	k1gFnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Mr	Mr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tortoise	Tortoise	k1gFnSc1
<g/>
,	,	kIx,
Meet	Meet	k1gInSc1
Madame	madame	k1gFnSc2
Tortue	Tortu	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
překladatelé	překladatel	k1gMnPc1
„	„	k?
<g/>
okamžitě	okamžitě	k6eAd1
spadnou	spadnout	k5eAaPmIp3nP
střemhlav	střemhlav	k6eAd1
do	do	k7c2
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
ženským	ženský	k2eAgNnSc7d1
pohlavím	pohlaví	k1gNnSc7
francouzského	francouzský	k2eAgNnSc2d1
tortue	tortue	k1gNnSc2
a	a	k8xC
mou	můj	k3xOp1gFnSc7
mužskou	mužský	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
<g/>
,	,	kIx,
Želvákem	želvák	k1gMnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hofstadter	Hofstadter	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
překládat	překládat	k5eAaImF
francouzskou	francouzský	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
jako	jako	k8xS,k8xC
<g/>
:	:	kIx,
„	„	k?
<g/>
Madame	madame	k1gFnSc1
Tortue	Tortu	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
italskou	italský	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
<g/>
:	:	kIx,
„	„	k?
<g/>
signorina	signorina	k1gFnSc1
Tartaruga	Tartaruga	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
by	by	kYmCp3nP
další	další	k2eAgInPc1d1
překladatelské	překladatelský	k2eAgInPc1d1
oříšky	oříšek	k1gInPc1
mohly	moct	k5eAaImAgInP
zastřít	zastřít	k5eAaPmF
smysl	smysl	k1gInSc4
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
Hofstadter	Hofstadter	k1gMnSc1
puntičkářsky	puntičkářsky	k6eAd1
prošel	projít	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
přes	přes	k7c4
i	i	k9
ty	ten	k3xDgFnPc4
nejposlednější	poslední	k2eAgFnPc4d3
věty	věta	k1gFnPc4
a	a	k8xC
poznamenal	poznamenat	k5eAaPmAgMnS
vysvětlivky	vysvětlivka	k1gFnPc4
do	do	k7c2
speciálního	speciální	k2eAgInSc2d1
výtisku	výtisk	k1gInSc2
<g/>
,	,	kIx,
určeného	určený	k2eAgMnSc2d1
pro	pro	k7c4
překladatele	překladatel	k1gMnSc4
do	do	k7c2
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Překlad	překlad	k1gInSc1
také	také	k9
přidal	přidat	k5eAaPmAgInS
Hofstadter	Hofstadter	k1gInSc1
možnost	možnost	k1gFnSc4
jak	jak	k8xS,k8xC
přidat	přidat	k5eAaPmF
význam	význam	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
hříčky	hříčka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
není	být	k5eNaImIp3nS
podtitul	podtitul	k1gInSc1
překladem	překlad	k1gInSc7
anglického	anglický	k2eAgInSc2d1
an	an	k?
Eternal	Eternal	k1gFnSc1
Golden	Goldna	k1gFnPc2
Braid	Braida	k1gFnPc2
(	(	kIx(
<g/>
volně	volně	k6eAd1
přeloženo	přeložen	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
<g/>
:	:	kIx,
věčný	věčný	k2eAgInSc1d1
zlatý	zlatý	k2eAgInSc1d1
propletenec	propletenec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
zdánlivě	zdánlivě	k6eAd1
nepodobná	podobný	k2eNgFnSc1d1
(	(	kIx(
<g/>
a	a	k8xC
nesmyslná	smyslný	k2eNgFnSc1d1
<g/>
)	)	kIx)
fráze	fráze	k1gFnSc1
<g/>
:	:	kIx,
Jí	jíst	k5eAaImIp3nS
Yì	Yì	k1gMnSc1
Bì	Bì	k1gMnSc1
(	(	kIx(
<g/>
doslova	doslova	k6eAd1
„	„	k?
<g/>
sbírka	sbírka	k1gFnSc1
exotických	exotický	k2eAgFnPc2d1
potvor	potvora	k1gFnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
čínštině	čínština	k1gFnSc6
homonymum	homonymum	k1gNnSc4
ke	k	k7c3
GEB	GEB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
informace	informace	k1gFnPc1
o	o	k7c6
tomto	tento	k3xDgNnSc6
tématu	téma	k1gNnSc6
lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
autorově	autorův	k2eAgFnSc6d1
poslední	poslední	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Le	Le	k1gMnSc1
Ton	Ton	k1gMnSc1
beau	beau	k6eAd1
de	de	k?
Marot	marota	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
zejména	zejména	k9
překladu	překlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
knihy	kniha	k1gFnSc2
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
květnu	květen	k1gInSc6
2012	#num#	k4
za	za	k7c2
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
autorem	autor	k1gMnSc7
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
nakladatelství	nakladatelství	k1gNnSc4
Dokořán	dokořán	k6eAd1
a	a	k8xC
Argo	Argo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Lidé	člověk	k1gMnPc1
v	v	k7c6
GEBu	GEBum	k1gNnSc6
</s>
<s>
Johann	Johann	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
Bach	Bach	k1gMnSc1
</s>
<s>
M.	M.	kA
C.	C.	kA
Escher	Eschra	k1gFnPc2
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
</s>
<s>
Charles	Charles	k1gMnSc1
Babbage	Babbag	k1gFnSc2
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
</s>
<s>
Georg	Georg	k1gMnSc1
Cantor	Cantor	k1gMnSc1
</s>
<s>
Alonzo	Alonza	k1gFnSc5
Church	Church	k1gMnSc1
</s>
<s>
Leonardo	Leonardo	k1gMnSc5
Fibonacci	Fibonacc	k1gMnSc5
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliký	veliký	k2eAgInSc1d1
</s>
<s>
Douglas	Douglas	k1gMnSc1
R.	R.	kA
Hofstadter	Hofstadter	k1gMnSc1
</s>
<s>
Johann	Johann	k1gMnSc1
Philipp	Philipp	k1gMnSc1
Kirnberger	Kirnberger	k1gMnSc1
</s>
<s>
René	René	k1gMnSc5
Magritte	Magritt	k1gMnSc5
</s>
<s>
Marvin	Marvina	k1gFnPc2
Minsky	minsky	k6eAd1
</s>
<s>
Johann	Johann	k1gMnSc1
Joachim	Joachim	k1gMnSc1
Quantz	Quantz	k1gMnSc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Tarski	Tarsk	k1gFnSc2
</s>
<s>
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc4
</s>
<s>
Zhaozhou	Zhaozha	k1gFnSc7
</s>
<s>
Achilles	Achilles	k1gMnSc1
a	a	k8xC
želva	želva	k1gFnSc1
</s>
<s>
Oblasti	oblast	k1gFnPc1
záběru	záběr	k1gInSc2
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
</s>
<s>
Bongardův	Bongardův	k2eAgInSc1d1
problém	problém	k1gInSc1
</s>
<s>
Mozek	mozek	k1gInSc1
<g/>
,	,	kIx,
mysl	mysl	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
poznání	poznání	k1gNnSc1
</s>
<s>
Formální	formální	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc1
vyčíslitelnosti	vyčíslitelnost	k1gFnSc2
</s>
<s>
Svobodná	svobodný	k2eAgFnSc1d1
vůle	vůle	k1gFnSc1
versus	versus	k7c1
Předurčení	předurčení	k1gNnSc1
</s>
<s>
Fuga	fuga	k1gFnSc1
<g/>
,	,	kIx,
kontrapunkt	kontrapunkt	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
hudební	hudební	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Genetika	genetika	k1gFnSc1
</s>
<s>
Holismus	holismus	k1gInSc1
versus	versus	k7c1
redukcionismus	redukcionismus	k1gInSc4
</s>
<s>
Isomorfismus	isomorfismus	k1gInSc1
a	a	k8xC
sémantika	sémantika	k1gFnSc1
</s>
<s>
Souvislé	souvislý	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
významů	význam	k1gInPc2
<g/>
,	,	kIx,
kontrapunkt	kontrapunkt	k1gInSc1
<g/>
,	,	kIx,
sémiotika	sémiotika	k1gFnSc1
<g/>
,	,	kIx,
kódy	kód	k1gInPc1
</s>
<s>
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Lisp	Lisp	k1gInSc1
</s>
<s>
Logika	logika	k1gFnSc1
<g/>
,	,	kIx,
teorie	teorie	k1gFnPc1
čísel	číslo	k1gNnPc2
</s>
<s>
Metamatematika	Metamatematika	k1gFnSc1
</s>
<s>
Molekulární	molekulární	k2eAgFnSc1d1
biologie	biologie	k1gFnSc1
</s>
<s>
Paradoxy	paradox	k1gInPc1
</s>
<s>
Sebereference	Sebereference	k1gFnPc1
<g/>
,	,	kIx,
rekurze	rekurze	k1gFnPc1
<g/>
,	,	kIx,
divné	divný	k2eAgFnPc1d1
smyčky	smyčka	k1gFnPc1
</s>
<s>
Sebe-organizující	Sebe-organizující	k2eAgMnSc1d1
se	se	k3xPyFc4
vznik	vznik	k1gInSc1
smyslu	smysl	k1gInSc2
identity	identita	k1gFnSc2
<g/>
:	:	kIx,
vědomí	vědomí	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Jsem	být	k5eAaImIp1nS
pravda	pravda	k1gFnSc1
a	a	k8xC
říkám	říkat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
nemohu	moct	k5eNaImIp1nS
být	být	k5eAaImF
dokázána	dokázat	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
systému	systém	k1gInSc2
do	do	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
náležím	náležet	k5eAaImIp1nS
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Jsem	být	k5eAaImIp1nS
pravdivý	pravdivý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
moje	můj	k3xOp1gFnSc1
pravda	pravda	k1gFnSc1
přesahuje	přesahovat	k5eAaImIp3nS
tento	tento	k3xDgInSc4
vesmír	vesmír	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Symetrie	symetrie	k1gFnSc1
</s>
<s>
Syntax	syntax	k1gFnSc1
versus	versus	k7c1
sémantika	sémantik	k1gMnSc2
</s>
<s>
Překlad	překlad	k1gInSc1
</s>
<s>
Typografie	typografie	k1gFnSc1
a	a	k8xC
syntax	syntax	k1gFnSc1
</s>
<s>
Zen	zen	k2eAgInSc1d1
Buddhismus	buddhismus	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Informatika	informatika	k1gFnSc1
(	(	kIx(
<g/>
počítačová	počítačový	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Fraktály	fraktál	k1gInPc1
</s>
<s>
Argument	argument	k1gInSc1
čínského	čínský	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
</s>
<s>
Hofstadterův	Hofstadterův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
seminář	seminář	k1gInSc1
o	o	k7c6
této	tento	k3xDgFnSc6
knize	kniha	k1gFnSc6
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
informatiky	informatika	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Odkaz	odkaz	k1gInSc1
na	na	k7c4
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
na	na	k7c6
stránce	stránka	k1gFnSc6
nakladatelství	nakladatelství	k1gNnSc2
Dokořán	dokořán	k6eAd1
<g/>
,	,	kIx,
zde	zde	k6eAd1
i	i	k9
ukázky	ukázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4451050-0	4451050-0	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
1146708184900842583	#num#	k4
</s>
