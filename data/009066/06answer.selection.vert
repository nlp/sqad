<s>
Fosfor	fosfor	k1gInSc1	fosfor
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
P	P	kA	P
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Phosphorus	Phosphorus	k1gInSc1	Phosphorus
<g/>
;	;	kIx,	;
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
kostík	kostík	k1gInSc1	kostík
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgInS	ujmout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc4d1	nekovový
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
i	i	k9	i
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
