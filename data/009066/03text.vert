<p>
<s>
Fosfor	fosfor	k1gInSc1	fosfor
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
P	P	kA	P
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Phosphorus	Phosphorus	k1gInSc1	Phosphorus
<g/>
;	;	kIx,	;
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
kostík	kostík	k1gInSc1	kostík
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgInS	ujmout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc4d1	nekovový
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
i	i	k9	i
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc1d1	nekovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
P	P	kA	P
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
se	se	k3xPyFc4	se
fosfor	fosfor	k1gInSc1	fosfor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
P	P	kA	P
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
fosfidy	fosfida	k1gFnPc4	fosfida
a	a	k8xC	a
fosforitany	fosforitan	k1gInPc4	fosforitan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
i	i	k8xC	i
P	P	kA	P
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
12	[number]	k4	12
alotropních	alotropní	k2eAgFnPc2d1	alotropní
modifikací	modifikace	k1gFnPc2	modifikace
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
fialový	fialový	k2eAgInSc4d1	fialový
nebo	nebo	k8xC	nebo
černý	černý	k2eAgInSc4d1	černý
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
P4	P4	k1gFnPc2	P4
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gFnPc4	jeho
vysoké	vysoký	k2eAgFnPc4d1	vysoká
reaktivity	reaktivita	k1gFnPc4	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
měkká	měkký	k2eAgFnSc1d1	měkká
látka	látka	k1gFnSc1	látka
nažloutlé	nažloutlý	k2eAgFnSc2d1	nažloutlá
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
žlutý	žlutý	k2eAgInSc4d1	žlutý
fosfor	fosfor	k1gInSc4	fosfor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
a	a	k8xC	a
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
samovznětlivý	samovznětlivý	k2eAgInSc1d1	samovznětlivý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
jeho	jeho	k3xOp3gFnSc2	jeho
páry	pára	k1gFnSc2	pára
světélkují	světélkovat	k5eAaImIp3nP	světélkovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
oxidaci	oxidace	k1gFnSc6	oxidace
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
vydávají	vydávat	k5eAaPmIp3nP	vydávat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chemiluminiscence	chemiluminiscence	k1gFnSc1	chemiluminiscence
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Fosforescence	fosforescence	k1gFnSc1	fosforescence
je	být	k5eAaImIp3nS	být
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
u	u	k7c2	u
fosforu	fosfor	k1gInSc2	fosfor
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
fosforu	fosfor	k1gInSc2	fosfor
nazván	nazván	k2eAgInSc1d1	nazván
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dlouhodobější	dlouhodobý	k2eAgNnPc4d2	dlouhodobější
uchovávání	uchovávání	k1gNnPc4	uchovávání
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ponořen	ponořen	k2eAgMnSc1d1	ponořen
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
jeho	jeho	k3xOp3gNnSc3	jeho
samovolnému	samovolný	k2eAgNnSc3d1	samovolné
vzplanutí	vzplanutí	k1gNnSc3	vzplanutí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
sirouhlíku	sirouhlík	k1gInSc6	sirouhlík
CS	CS	kA	CS
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgFnSc1d1	reaktivní
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
za	za	k7c2	za
pokojové	pokojový	k2eAgFnSc2d1	pokojová
teploty	teplota	k1gFnSc2	teplota
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplém	teplé	k1gNnSc6	teplé
roztoku	roztok	k1gInSc2	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
KOH	KOH	kA	KOH
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
dihydrogenfosforečnanu	dihydrogenfosforečnan	k1gInSc2	dihydrogenfosforečnan
draselného	draselný	k2eAgMnSc2d1	draselný
KH2PO4	KH2PO4	k1gMnSc2	KH2PO4
a	a	k8xC	a
fosforovodíku	fosforovodík	k1gInSc2	fosforovodík
neboli	neboli	k8xC	neboli
fosfanu	fosfan	k1gInSc2	fosfan
PH	Ph	kA	Ph
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
redukují	redukovat	k5eAaBmIp3nP	redukovat
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
ušlechtilé	ušlechtilý	k2eAgInPc1d1	ušlechtilý
kovy	kov	k1gInPc1	kov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
fosfor	fosfor	k1gInSc4	fosfor
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
fosfidy	fosfida	k1gFnPc1	fosfida
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Červený	červený	k2eAgInSc4d1	červený
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
vzniká	vznikat	k5eAaImIp3nS	vznikat
zahřátím	zahřátí	k1gNnSc7	zahřátí
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
inertním	inertní	k2eAgNnSc6d1	inertní
prostředí	prostředí	k1gNnSc6	prostředí
na	na	k7c4	na
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přeměna	přeměna	k1gFnSc1	přeměna
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
působením	působení	k1gNnSc7	působení
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
nesvětélkuje	světélkovat	k5eNaImIp3nS	světélkovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
neomezeně	omezeně	k6eNd1	omezeně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
ani	ani	k8xC	ani
nepolárních	polární	k2eNgNnPc6d1	nepolární
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
ani	ani	k8xC	ani
v	v	k7c6	v
sirouhlíku	sirouhlík	k1gInSc6	sirouhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnPc2	tání
597	[number]	k4	597
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
a	a	k8xC	a
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
až	až	k9	až
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivnější	reaktivní	k2eAgInSc1d2	reaktivnější
než	než	k8xS	než
červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jemně	jemně	k6eAd1	jemně
rozptýlenou	rozptýlený	k2eAgFnSc7d1	rozptýlená
formou	forma	k1gFnSc7	forma
červeného	červený	k2eAgInSc2d1	červený
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fosfor	fosfor	k1gInSc1	fosfor
vzniká	vznikat	k5eAaImIp3nS	vznikat
varem	var	k1gInSc7	var
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
s	s	k7c7	s
bromidem	bromid	k1gInSc7	bromid
fosforitým	fosforitý	k2eAgInSc7d1	fosforitý
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
hydroxidů	hydroxid	k1gInPc2	hydroxid
a	a	k8xC	a
vytěsňuje	vytěsňovat	k5eAaImIp3nS	vytěsňovat
některé	některý	k3yIgInPc4	některý
kovy	kov	k1gInPc4	kov
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
jejich	jejich	k3xOp3gFnPc2	jejich
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
má	mít	k5eAaImIp3nS	mít
polymerní	polymerní	k2eAgFnSc4d1	polymerní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
modifikacích	modifikace	k1gFnPc6	modifikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fialový	fialový	k2eAgInSc4d1	fialový
fosfor	fosfor	k1gInSc4	fosfor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fialový	fialový	k2eAgInSc1d1	fialový
fosfor	fosfor	k1gInSc1	fosfor
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgInSc1d2	vyšší
než	než	k8xS	než
čistý	čistý	k2eAgInSc1d1	čistý
červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
krystalizací	krystalizace	k1gFnSc7	krystalizace
z	z	k7c2	z
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fialový	fialový	k2eAgInSc4d1	fialový
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Fialový	fialový	k2eAgInSc1d1	fialový
fosfor	fosfor	k1gInSc1	fosfor
vzniká	vznikat	k5eAaImIp3nS	vznikat
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
zahříváním	zahřívání	k1gNnSc7	zahřívání
červeného	červený	k2eAgInSc2d1	červený
fosforu	fosfor	k1gInSc2	fosfor
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
550	[number]	k4	550
°	°	k?	°
<g/>
C	C	kA	C
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
Hittorfův	Hittorfův	k2eAgInSc1d1	Hittorfův
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
objevitele	objevitel	k1gMnSc2	objevitel
Johanna	Johann	k1gMnSc4	Johann
Wilhelma	Wilhelm	k1gMnSc4	Wilhelm
Hittorfa	Hittorf	k1gMnSc4	Hittorf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gNnSc4	on
poprvé	poprvé	k6eAd1	poprvé
připravil	připravit	k5eAaPmAgMnS	připravit
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
řetězci	řetězec	k1gInPc7	řetězec
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgFnP	propojit
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
roviny	rovina	k1gFnPc4	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Černý	černý	k2eAgInSc1d1	černý
fosfor	fosfor	k1gInSc1	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
svými	svůj	k3xOyFgFnPc7	svůj
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kovový	kovový	k2eAgInSc4d1	kovový
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tepelně	tepelně	k6eAd1	tepelně
i	i	k9	i
elektricky	elektricky	k6eAd1	elektricky
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vrstvenou	vrstvený	k2eAgFnSc4d1	vrstvená
polymerní	polymerní	k2eAgFnSc4d1	polymerní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
chemickými	chemický	k2eAgInPc7d1	chemický
vlastnosti	vlastnost	k1gFnPc4	vlastnost
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
červenému	červený	k2eAgInSc3d1	červený
fosforu	fosfor	k1gInSc3	fosfor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
vlhkém	vlhký	k2eAgInSc6d1	vlhký
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
zahříváním	zahřívání	k1gNnSc7	zahřívání
červeného	červený	k2eAgInSc2d1	červený
fosforu	fosfor	k1gInSc2	fosfor
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
přes	přes	k7c4	přes
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
12	[number]	k4	12
000	[number]	k4	000
atmosfér	atmosféra	k1gFnPc2	atmosféra
nebo	nebo	k8xC	nebo
pohodlněji	pohodlně	k6eAd2	pohodlně
zahříváním	zahřívání	k1gNnSc7	zahřívání
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
380	[number]	k4	380
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc2	přítomnost
jemně	jemně	k6eAd1	jemně
rozptýlené	rozptýlený	k2eAgFnSc2d1	rozptýlená
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
modifikací	modifikace	k1gFnPc2	modifikace
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc4d1	černý
fosfor	fosfor	k1gInSc4	fosfor
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
550	[number]	k4	550
°	°	k?	°
<g/>
C	C	kA	C
termodynamicky	termodynamicky	k6eAd1	termodynamicky
nejstabilnější	stabilní	k2eAgFnSc1d3	nejstabilnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Modrý	modrý	k2eAgInSc4d1	modrý
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
modifikací	modifikace	k1gFnSc7	modifikace
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
mírně	mírně	k6eAd1	mírně
zvlněnou	zvlněný	k2eAgFnSc7d1	zvlněná
monovrstvou	monovrstva	k1gFnSc7	monovrstva
s	s	k7c7	s
šestiúhelníkovou	šestiúhelníkový	k2eAgFnSc7d1	šestiúhelníková
mříží	mříž	k1gFnSc7	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
polovodič	polovodič	k1gInSc4	polovodič
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
černého	černý	k2eAgInSc2d1	černý
fosforu	fosfor	k1gInSc2	fosfor
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
šířku	šířka	k1gFnSc4	šířka
pásma	pásmo	k1gNnSc2	pásmo
–	–	k?	–
2	[number]	k4	2
elektronvolty	elektronvolt	k1gInPc1	elektronvolt
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
7	[number]	k4	7
<g/>
krát	krát	k6eAd1	krát
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
fosfor	fosfor	k1gInSc4	fosfor
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
modrého	modrý	k2eAgInSc2d1	modrý
fosforu	fosfor	k1gInSc2	fosfor
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c4	na
Michigan	Michigan	k1gInSc4	Michigan
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
formu	forma	k1gFnSc4	forma
však	však	k9	však
analyticky	analyticky	k6eAd1	analyticky
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
až	až	k9	až
v	v	k7c6	v
r.	r.	kA	r.
2018	[number]	k4	2018
na	na	k7c6	na
Hemholtzově	Hemholtzův	k2eAgNnSc6d1	Hemholtzův
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
poprvé	poprvé	k6eAd1	poprvé
připraven	připravit	k5eAaPmNgInS	připravit
již	již	k9	již
v	v	k7c6	v
r.	r.	kA	r.
2016	[number]	k4	2016
napařením	napaření	k1gNnSc7	napaření
na	na	k7c4	na
zlatý	zlatý	k2eAgInSc4d1	zlatý
substrát	substrát	k1gInSc4	substrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgInS	být
fosfor	fosfor	k1gInSc4	fosfor
poprvé	poprvé	k6eAd1	poprvé
izolován	izolovat	k5eAaBmNgMnS	izolovat
německým	německý	k2eAgMnSc7d1	německý
alchymistou	alchymista	k1gMnSc7	alchymista
Heningem	Hening	k1gInSc7	Hening
Brandem	Brand	k1gMnSc7	Brand
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
alchymisté	alchymista	k1gMnPc1	alchymista
<g/>
,	,	kIx,	,
najít	najít	k5eAaPmF	najít
kámen	kámen	k1gInSc4	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
rozkládat	rozkládat	k5eAaImF	rozkládat
lidskou	lidský	k2eAgFnSc4d1	lidská
moč	moč	k1gFnSc4	moč
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
zahustil	zahustit	k5eAaPmAgMnS	zahustit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
destiloval	destilovat	k5eAaImAgInS	destilovat
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc4	pár
nechal	nechat	k5eAaPmAgInS	nechat
zkondenzovat	zkondenzovat	k5eAaPmF	zkondenzovat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
voskovitou	voskovitý	k2eAgFnSc4d1	voskovitá
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
světélkovala	světélkovat	k5eAaImAgFnS	světélkovat
<g/>
.	.	kIx.	.
</s>
<s>
Brand	Brand	k1gMnSc1	Brand
nazval	nazvat	k5eAaPmAgMnS	nazvat
tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
phosphorus	phosphorus	k1gInSc1	phosphorus
<g/>
:	:	kIx,	:
phos	phos	k1gInSc1	phos
–	–	k?	–
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
phoros	phorosa	k1gFnPc2	phorosa
–	–	k?	–
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
světlonoš	světlonoš	k1gMnSc1	světlonoš
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
fosfor	fosfor	k1gInSc4	fosfor
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Boyle	Boyle	k1gFnSc2	Boyle
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1680	[number]	k4	1680
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
připravil	připravit	k5eAaPmAgInS	připravit
oxid	oxid	k1gInSc1	oxid
fosforečný	fosforečný	k2eAgInSc1d1	fosforečný
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
fosforečnou	fosforečný	k2eAgFnSc4d1	fosforečná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
ho	on	k3xPp3gMnSc4	on
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
teprve	teprve	k6eAd1	teprve
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisier	k1gMnSc1	Lavoisier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
fosforu	fosfor	k1gInSc2	fosfor
(	(	kIx(	(
<g/>
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
a	a	k8xC	a
pochybný	pochybný	k2eAgInSc1d1	pochybný
nález	nález	k1gInSc1	nález
minerálu	minerál	k1gInSc2	minerál
fosforu	fosfor	k1gInSc2	fosfor
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
z	z	k7c2	z
meteoritu	meteorit	k1gInSc2	meteorit
nalezeném	nalezený	k2eAgNnSc6d1	nalezené
v	v	k7c6	v
Townshipské	Townshipský	k2eAgFnSc6d1	Townshipský
salině	salina	k1gFnSc6	salina
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
fosfor	fosfor	k1gInSc1	fosfor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
11	[number]	k4	11
<g/>
.	.	kIx.	.
prvkem	prvek	k1gInSc7	prvek
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
koncentrace	koncentrace	k1gFnSc2	koncentrace
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
1,2	[number]	k4	1,2
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,07	[number]	k4	0,07
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
fosforu	fosfor	k1gInSc2	fosfor
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
minerálem	minerál	k1gInSc7	minerál
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
fosforu	fosfor	k1gInSc2	fosfor
je	být	k5eAaImIp3nS	být
směsný	směsný	k2eAgInSc1d1	směsný
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
–	–	k?	–
apatit	apatit	k1gInSc1	apatit
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
složení	složení	k1gNnSc4	složení
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Ca	ca	kA	ca
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
X	X	kA	X
(	(	kIx(	(
<g/>
X	X	kA	X
=	=	kIx~	=
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Apatit	apatit	k1gInSc1	apatit
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
především	především	k9	především
jeho	jeho	k3xOp3gFnPc2	jeho
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
oblasti	oblast	k1gFnPc1	oblast
těžby	těžba	k1gFnSc2	těžba
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
poloostrov	poloostrov	k1gInSc1	poloostrov
Kola	kolo	k1gNnSc2	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
minerály	minerál	k1gInPc7	minerál
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
fosforu	fosfor	k1gInSc2	fosfor
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
fosforit	fosforit	k1gInSc4	fosforit
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
fluoroapatit	fluoroapatit	k5eAaPmF	fluoroapatit
Ca	ca	kA	ca
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
a	a	k8xC	a
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc4d1	významný
wavellit	wavellit	k1gInSc4	wavellit
3	[number]	k4	3
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3.2	[number]	k4	3.2
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
12	[number]	k4	12
H2O	H2O	k1gFnPc2	H2O
a	a	k8xC	a
vivianit	vivianit	k1gInSc1	vivianit
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2.8	[number]	k4	2.8
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
fosfor	fosfor	k1gInSc1	fosfor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
a	a	k8xC	a
zubech	zub	k1gInPc6	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
důležitých	důležitý	k2eAgFnPc2d1	důležitá
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
jako	jako	k9	jako
DNA	DNA	kA	DNA
a	a	k8xC	a
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
energetických	energetický	k2eAgInPc2d1	energetický
přenašečů	přenašeč	k1gInPc2	přenašeč
(	(	kIx(	(
<g/>
ADP	ADP	kA	ADP
<g/>
,	,	kIx,	,
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
membráně	membrána	k1gFnSc6	membrána
(	(	kIx(	(
<g/>
fosfolipidech	fosfolipid	k1gInPc6	fosfolipid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostlinami	rostlina	k1gFnPc7	rostlina
je	být	k5eAaImIp3nS	být
přijímán	přijímán	k2eAgMnSc1d1	přijímán
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aniontu	anion	k1gInSc2	anion
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
se	se	k3xPyFc4	se
neredukuje	redukovat	k5eNaBmIp3nS	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
zápornému	záporný	k2eAgInSc3d1	záporný
náboji	náboj	k1gInSc3	náboj
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
je	být	k5eAaImIp3nS	být
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc3d1	vysoká
intrabuněčné	intrabuněčný	k2eAgFnSc3d1	intrabuněčný
koncentraci	koncentrace	k1gFnSc3	koncentrace
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
příjem	příjem	k1gInSc4	příjem
energeticky	energeticky	k6eAd1	energeticky
velmi	velmi	k6eAd1	velmi
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
a	a	k8xC	a
vysokoafinními	vysokoafinní	k2eAgInPc7d1	vysokoafinní
transportéry	transportér	k1gInPc7	transportér
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příjmu	příjem	k1gInSc6	příjem
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
mykorhiza	mykorhiza	k1gFnSc1	mykorhiza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
volný	volný	k2eAgInSc1d1	volný
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
fosfátový	fosfátový	k2eAgInSc1d1	fosfátový
aniont	aniont	k1gInSc1	aniont
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
PO	po	k7c4	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
i	i	k8xC	i
vázaný	vázaný	k2eAgMnSc1d1	vázaný
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgMnSc1d1	volný
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
skladován	skladovat	k5eAaImNgInS	skladovat
ve	v	k7c6	v
vakuole	vakuola	k1gFnSc6	vakuola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
elementárního	elementární	k2eAgInSc2d1	elementární
fosforu	fosfor	k1gInSc2	fosfor
je	být	k5eAaImIp3nS	být
redukce	redukce	k1gFnSc1	redukce
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
koksem	koks	k1gInSc7	koks
(	(	kIx(	(
<g/>
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
)	)	kIx)	)
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
křemenného	křemenný	k2eAgInSc2d1	křemenný
písku	písek	k1gInSc2	písek
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
3	[number]	k4	3
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
→	→	k?	→
3	[number]	k4	3
CaSiO	CaSiO	k1gFnSc1	CaSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
P2O5	P2O5	k1gFnSc1	P2O5
</s>
</p>
<p>
<s>
P2O5	P2O5	k4	P2O5
+	+	kIx~	+
5	[number]	k4	5
C	C	kA	C
→	→	k?	→
5	[number]	k4	5
CO	co	k6eAd1	co
+	+	kIx~	+
2	[number]	k4	2
PSouhrnně	PSouhrnně	k1gFnPc2	PSouhrnně
</s>
</p>
<p>
<s>
2	[number]	k4	2
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
6	[number]	k4	6
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
+	+	kIx~	+
10	[number]	k4	10
C	C	kA	C
→	→	k?	→
P4	P4	k1gMnSc1	P4
+	+	kIx~	+
6	[number]	k4	6
CaSiO	CaSiO	k1gFnSc1	CaSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
10	[number]	k4	10
COFosfor	COFosfora	k1gFnPc2	COFosfora
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1300	[number]	k4	1300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
těká	těkat	k5eAaImIp3nS	těkat
jako	jako	k9	jako
molekula	molekula	k1gFnSc1	molekula
P4	P4	k1gFnSc1	P4
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zachycován	zachycovat	k5eAaImNgInS	zachycovat
po	po	k7c6	po
kondenzaci	kondenzace	k1gFnSc6	kondenzace
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
jako	jako	k9	jako
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
inertní	inertní	k2eAgFnSc6d1	inertní
atmosféře	atmosféra	k1gFnSc6	atmosféra
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
modifikace	modifikace	k1gFnSc2	modifikace
červeného	červený	k2eAgInSc2d1	červený
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vrstevnatou	vrstevnatý	k2eAgFnSc4d1	vrstevnatá
strukturu	struktura	k1gFnSc4	struktura
Pn	Pn	k1gFnSc2	Pn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
metoda	metoda	k1gFnSc1	metoda
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
Pelletierova	Pelletierův	k2eAgFnSc1d1	Pelletierův
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
převádí	převádět	k5eAaImIp3nS	převádět
v	v	k7c4	v
prostředí	prostředí	k1gNnSc4	prostředí
mírně	mírně	k6eAd1	mírně
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
na	na	k7c4	na
dihydrogenfosforečnan	dihydrogenfosforečnan	k1gInSc4	dihydrogenfosforečnan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
sádra	sádra	k1gFnSc1	sádra
a	a	k8xC	a
dihydrogenfosforečnan	dihydrogenfosforečnan	k1gInSc1	dihydrogenfosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
redukován	redukovat	k5eAaBmNgInS	redukovat
koksem	koks	k1gInSc7	koks
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
šamotových	šamotový	k2eAgFnPc6d1	šamotová
pecích	pec	k1gFnPc6	pec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
H2SO4	H2SO4	k1gFnPc2	H2SO4
+	+	kIx~	+
4	[number]	k4	4
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4.2	[number]	k4	4.2
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
PO	po	k7c4	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
3	[number]	k4	3
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
PO	po	k7c4	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
10	[number]	k4	10
C	C	kA	C
→	→	k?	→
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
10	[number]	k4	10
CO	co	k6eAd1	co
+	+	kIx~	+
4	[number]	k4	4
P	P	kA	P
+	+	kIx~	+
6	[number]	k4	6
H2O	H2O	k1gFnPc2	H2O
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
fosforu	fosfor	k1gInSc2	fosfor
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
alotropní	alotropní	k2eAgFnSc6d1	alotropní
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
fosfor	fosfor	k1gInSc1	fosfor
právě	právě	k6eAd1	právě
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Toxických	toxický	k2eAgFnPc2d1	toxická
vlastností	vlastnost	k1gFnPc2	vlastnost
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
nástrah	nástraha	k1gFnPc2	nástraha
na	na	k7c4	na
krysy	krysa	k1gFnPc4	krysa
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
farmaceutických	farmaceutický	k2eAgInPc2d1	farmaceutický
preparátů	preparát	k1gInPc2	preparát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
samovznícení	samovznícení	k1gNnSc2	samovznícení
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
při	při	k7c6	při
styku	styk	k1gInSc6	styk
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
využívalo	využívat	k5eAaPmAgNnS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
samozápalných	samozápalný	k2eAgFnPc2d1	samozápalná
leteckých	letecký	k2eAgFnPc2d1	letecká
pum	puma	k1gFnPc2	puma
a	a	k8xC	a
dělostřeleckých	dělostřelecký	k2eAgInPc2d1	dělostřelecký
granátů	granát	k1gInPc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
Zákeřnost	zákeřnost	k1gFnSc1	zákeřnost
těchto	tento	k3xDgFnPc2	tento
zbraní	zbraň	k1gFnPc2	zbraň
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hořící	hořící	k2eAgInSc1d1	hořící
fosfor	fosfor	k1gInSc1	fosfor
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
mimořádně	mimořádně	k6eAd1	mimořádně
těžké	těžký	k2eAgNnSc1d1	těžké
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
hojitelné	hojitelný	k2eAgFnPc4d1	hojitelný
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jej	on	k3xPp3gInSc4	on
uhasit	uhasit	k5eAaPmF	uhasit
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgInSc1d1	jediný
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
ponoření	ponoření	k1gNnSc4	ponoření
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gInSc4	on
červený	červený	k2eAgInSc4d1	červený
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
jako	jako	k8xS	jako
zbraň	zbraň	k1gFnSc4	zbraň
====	====	k?	====
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
nekonvenční	konvenční	k2eNgFnSc4d1	nekonvenční
zbraň	zbraň	k1gFnSc4	zbraň
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xS	jako
chemická	chemický	k2eAgFnSc1d1	chemická
zbraň	zbraň	k1gFnSc1	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
feniánů	fenián	k1gInPc2	fenián
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
známy	znám	k2eAgInPc1d1	znám
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
jeho	jeho	k3xOp3gNnSc2	jeho
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
Korejské	korejský	k2eAgFnSc3d1	Korejská
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
válce	válec	k1gInSc2	válec
v	v	k7c6	v
Čečně	Čečna	k1gFnSc6	Čečna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
užil	užít	k5eAaPmAgMnS	užít
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
při	při	k7c6	při
plynovém	plynový	k2eAgInSc6d1	plynový
útoku	útok	k1gInSc6	útok
v	v	k7c4	v
Halabja	Halabjus	k1gMnSc4	Halabjus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
použili	použít	k5eAaPmAgMnP	použít
Američané	Američan	k1gMnPc1	Američan
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
jak	jak	k8xC	jak
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
proti	proti	k7c3	proti
sunnitským	sunnitský	k2eAgMnPc3d1	sunnitský
povstalcům	povstalec	k1gMnPc3	povstalec
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Fallúdžu	Fallúdžu	k1gFnSc6	Fallúdžu
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
byl	být	k5eAaImAgInS	být
též	též	k9	též
použit	použít	k5eAaPmNgInS	použít
Izraelem	Izrael	k1gInSc7	Izrael
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
Lité	litý	k2eAgNnSc1d1	Lité
olovo	olovo	k1gNnSc1	olovo
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
některých	některý	k3yIgMnPc2	některý
svědků	svědek	k1gMnPc2	svědek
použili	použít	k5eAaPmAgMnP	použít
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
Rusové	Rus	k1gMnPc1	Rus
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Použití	použití	k1gNnSc1	použití
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
záležitost	záležitost	k1gFnSc1	záležitost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
obydlených	obydlený	k2eAgFnPc6d1	obydlená
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
civilním	civilní	k2eAgNnSc7d1	civilní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
dokumentů	dokument	k1gInPc2	dokument
upravujících	upravující	k2eAgInPc2d1	upravující
použití	použití	k1gNnSc4	použití
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
závazné	závazný	k2eAgFnPc1d1	závazná
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
i	i	k9	i
více	hodně	k6eAd2	hodně
způsobů	způsob	k1gInPc2	způsob
užití	užití	k1gNnSc2	užití
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
<g/>
,	,	kIx,	,
nebývá	bývat	k5eNaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
chemická	chemický	k2eAgFnSc1d1	chemická
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
hořlavina	hořlavina	k1gFnSc1	hořlavina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
osvícení	osvícení	k1gNnSc3	osvícení
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
cílů	cíl	k1gInPc2	cíl
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
k	k	k7c3	k
zahalení	zahalení	k1gNnSc3	zahalení
bojiště	bojiště	k1gNnSc2	bojiště
kouřem	kouř	k1gInSc7	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
upalování	upalování	k1gNnSc2	upalování
<g/>
)	)	kIx)	)
živé	živý	k2eAgFnPc1d1	živá
síly	síla	k1gFnPc1	síla
nepřítele	nepřítel	k1gMnSc2	nepřítel
nebo	nebo	k8xC	nebo
k	k	k7c3	k
vyhnání	vyhnání	k1gNnSc3	vyhnání
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
bojovníků	bojovník	k1gMnPc2	bojovník
z	z	k7c2	z
bezpečných	bezpečný	k2eAgInPc2d1	bezpečný
úkrytů	úkryt	k1gInPc2	úkryt
a	a	k8xC	a
případně	případně	k6eAd1	případně
též	též	k9	též
k	k	k7c3	k
podlomení	podlomení	k1gNnSc3	podlomení
jejich	jejich	k3xOp3gFnSc2	jejich
bojové	bojový	k2eAgFnSc2d1	bojová
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
možnosti	možnost	k1gFnPc1	možnost
použití	použití	k1gNnSc2	použití
vedou	vést	k5eAaImIp3nP	vést
často	často	k6eAd1	často
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
bojující	bojující	k2eAgFnPc1d1	bojující
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
sympatizanti	sympatizant	k1gMnPc1	sympatizant
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
případy	případ	k1gInPc1	případ
nasazení	nasazení	k1gNnSc2	nasazení
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
zcela	zcela	k6eAd1	zcela
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Účinky	účinek	k1gInPc4	účinek
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
====	====	k?	====
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
"	"	kIx"	"
<g/>
fosforové	fosforový	k2eAgFnPc4d1	fosforová
<g/>
"	"	kIx"	"
bomby	bomba	k1gFnPc4	bomba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
efektem	efekt	k1gInSc7	efekt
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vážné	vážný	k2eAgInPc1d1	vážný
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
<g/>
,	,	kIx,	,
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
;	;	kIx,	;
devastující	devastující	k2eAgInPc4d1	devastující
účinky	účinek	k1gInPc4	účinek
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aerosolu	aerosol	k1gInSc2	aerosol
nebo	nebo	k8xC	nebo
hustého	hustý	k2eAgInSc2d1	hustý
dýmu	dým	k1gInSc2	dým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
jeho	on	k3xPp3gNnSc2	on
hoření	hoření	k1gNnSc2	hoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Červený	červený	k2eAgInSc4d1	červený
fosfor	fosfor	k1gInSc4	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
samovznětlivý	samovznětlivý	k2eAgMnSc1d1	samovznětlivý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc4d1	červený
fosfor	fosfor	k1gInSc4	fosfor
schopen	schopen	k2eAgInSc4d1	schopen
vzplanout	vzplanout	k5eAaPmF	vzplanout
při	při	k7c6	při
silnějším	silný	k2eAgNnSc6d2	silnější
lokálním	lokální	k2eAgNnSc6d1	lokální
zahřátí	zahřátí	k1gNnSc6	zahřátí
<g/>
,	,	kIx,	,
vyvolaném	vyvolaný	k2eAgNnSc6d1	vyvolané
např.	např.	kA	např.
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
třením	tření	k1gNnSc7	tření
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
dodnes	dodnes	k6eAd1	dodnes
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
běžných	běžný	k2eAgFnPc2d1	běžná
kuchyňských	kuchyňský	k2eAgFnPc2d1	kuchyňská
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
uplatní	uplatnit	k5eAaPmIp3nP	uplatnit
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
různých	různý	k2eAgFnPc2d1	různá
pyrotechnických	pyrotechnický	k2eAgFnPc2d1	pyrotechnická
potřeb	potřeba	k1gFnPc2	potřeba
–	–	k?	–
zápalky	zápalka	k1gFnSc2	zápalka
<g/>
,	,	kIx,	,
roznětky	roznětka	k1gFnSc2	roznětka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc7d1	výchozí
sloučeninou	sloučenina	k1gFnSc7	sloučenina
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
sloučenin	sloučenina	k1gFnPc2	sloučenina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Černý	černý	k2eAgInSc1d1	černý
fosfor	fosfor	k1gInSc1	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
kovovým	kovový	k2eAgFnPc3d1	kovová
vlastnostem	vlastnost	k1gFnPc3	vlastnost
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
polovodičů	polovodič	k1gInPc2	polovodič
typu	typ	k1gInSc2	typ
N	N	kA	N
(	(	kIx(	(
<g/>
negativních	negativní	k2eAgInPc2d1	negativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slitiny	slitina	k1gFnSc2	slitina
===	===	k?	===
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
fosfor	fosfor	k1gInSc1	fosfor
se	se	k3xPyFc4	se
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
kovů	kov	k1gInPc2	kov
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
jejich	jejich	k3xOp3gFnPc2	jejich
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
značně	značně	k6eAd1	značně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
křehkost	křehkost	k1gFnSc4	křehkost
<g/>
)	)	kIx)	)
výsledného	výsledný	k2eAgInSc2d1	výsledný
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
působí	působit	k5eAaImIp3nS	působit
fosfor	fosfor	k1gInSc1	fosfor
lepší	dobrý	k2eAgFnSc1d2	lepší
tekutost	tekutost	k1gFnSc1	tekutost
(	(	kIx(	(
<g/>
zabíhavost	zabíhavost	k1gFnSc1	zabíhavost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
u	u	k7c2	u
slitin	slitina	k1gFnPc2	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
u	u	k7c2	u
šedé	šedý	k2eAgFnSc2d1	šedá
litiny	litina	k1gFnSc2	litina
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
legování	legování	k1gNnSc1	legování
fosforu	fosfor	k1gInSc2	fosfor
do	do	k7c2	do
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
pájek	pájka	k1gFnPc2	pájka
a	a	k8xC	a
bronzů	bronz	k1gInPc2	bronz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některých	některý	k3yIgFnPc2	některý
speciálních	speciální	k2eAgFnPc2d1	speciální
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
fosforu	fosfor	k1gInSc2	fosfor
===	===	k?	===
</s>
</p>
<p>
<s>
Fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
stravě	strava	k1gFnSc6	strava
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
spíše	spíše	k9	spíše
s	s	k7c7	s
nadbytkem	nadbytek	k1gInSc7	nadbytek
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
kolových	kolový	k2eAgInPc6d1	kolový
nápojích	nápoj	k1gInPc6	nápoj
<g/>
,	,	kIx,	,
tavených	tavený	k2eAgInPc6d1	tavený
sýrech	sýr	k1gInPc6	sýr
a	a	k8xC	a
uzeninách	uzenina	k1gFnPc6	uzenina
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
1200	[number]	k4	1200
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
využití	využití	k1gNnPc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Fosforečnany	fosforečnan	k1gInPc1	fosforečnan
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
fosfáty	fosfát	k1gInPc1	fosfát
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
důležitá	důležitý	k2eAgNnPc1d1	důležité
rostlinná	rostlinný	k2eAgNnPc1d1	rostlinné
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
vápenátého	vápenátý	k2eAgInSc2d1	vápenátý
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
hydrogenfosforečnan	hydrogenfosforečnan	k1gInSc1	hydrogenfosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
se	se	k3xPyFc4	se
vsakuje	vsakovat	k5eAaImIp3nS	vsakovat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
a	a	k8xC	a
dihydrogenfosforečnan	dihydrogenfosforečnan	k1gInSc1	dihydrogenfosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
superfosfát	superfosfát	k1gInSc1	superfosfát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
a	a	k8xC	a
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepoužívají	používat	k5eNaImIp3nP	používat
čistě	čistě	k6eAd1	čistě
fosforečnanová	fosforečnanový	k2eAgNnPc1d1	fosforečnanový
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kombinovaná	kombinovaný	k2eAgNnPc1d1	kombinované
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
směsí	směs	k1gFnSc7	směs
sloučenin	sloučenina	k1gFnPc2	sloučenina
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
<g/>
,	,	kIx,	,
draselných	draselný	k2eAgFnPc2d1	draselná
<g/>
,	,	kIx,	,
sodných	sodný	k2eAgFnPc2d1	sodná
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rostliny	rostlina	k1gFnPc1	rostlina
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vápenaté	vápenatý	k2eAgInPc1d1	vápenatý
a	a	k8xC	a
sodné	sodný	k2eAgInPc1d1	sodný
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
zubních	zubní	k2eAgFnPc2d1	zubní
past	pasta	k1gFnPc2	pasta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fosforečná	fosforečný	k2eAgFnSc1d1	fosforečná
a	a	k8xC	a
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
odrezovacích	odrezovací	k2eAgInPc2d1	odrezovací
roztoků	roztok	k1gInPc2	roztok
pro	pro	k7c4	pro
odstraňování	odstraňování	k1gNnSc4	odstraňování
korozních	korozní	k2eAgInPc2d1	korozní
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
železných	železný	k2eAgFnPc2d1	železná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
železitým	železitý	k2eAgInSc7d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
také	také	k6eAd1	také
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
manganu	mangan	k1gInSc2	mangan
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nerozpustné	rozpustný	k2eNgInPc1d1	nerozpustný
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
chemicky	chemicky	k6eAd1	chemicky
vázané	vázaný	k2eAgInPc1d1	vázaný
do	do	k7c2	do
krystalové	krystalový	k2eAgFnSc2d1	krystalová
mřížky	mřížka	k1gFnSc2	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fosfátování	fosfátování	k1gNnSc1	fosfátování
<g/>
.	.	kIx.	.
</s>
<s>
Fosfátovaný	fosfátovaný	k2eAgInSc1d1	fosfátovaný
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
nátěry	nátěr	k1gInPc4	nátěr
<g/>
.	.	kIx.	.
</s>
<s>
Fosfátovaný	fosfátovaný	k2eAgInSc1d1	fosfátovaný
povrch	povrch	k1gInSc1	povrch
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dobré	dobrý	k2eAgFnPc4d1	dobrá
kluzné	kluzný	k2eAgFnPc4d1	kluzná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
fosfátují	fosfátovat	k5eAaImIp3nP	fosfátovat
polotovary	polotovar	k1gInPc1	polotovar
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
tváření	tváření	k1gNnSc3	tváření
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ocelové	ocelový	k2eAgInPc1d1	ocelový
hlubokotažné	hlubokotažný	k2eAgInPc1d1	hlubokotažný
plechy	plech	k1gInPc1	plech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sodné	sodný	k2eAgFnPc1d1	sodná
soli	sůl	k1gFnPc1	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgInPc1d1	fosforečný
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
prášků	prášek	k1gInPc2	prášek
na	na	k7c4	na
praní	praní	k1gNnSc4	praní
nebo	nebo	k8xC	nebo
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
mytí	mytí	k1gNnSc4	mytí
nádobí	nádobí	k1gNnSc2	nádobí
v	v	k7c6	v
automatických	automatický	k2eAgFnPc6d1	automatická
myčkách	myčka	k1gFnPc6	myčka
pro	pro	k7c4	pro
změkčení	změkčení	k1gNnSc4	změkčení
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
3	[number]	k4	3
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sýrů	sýr	k1gInPc2	sýr
a	a	k8xC	a
nakládání	nakládání	k1gNnSc4	nakládání
šunky	šunka	k1gFnSc2	šunka
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
HPO	HPO	kA	HPO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
antikorozivní	antikorozivní	k2eAgInPc4d1	antikorozivní
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
cirkulačních	cirkulační	k2eAgFnPc2d1	cirkulační
vod	voda	k1gFnPc2	voda
pro	pro	k7c4	pro
vytápění	vytápění	k1gNnSc4	vytápění
(	(	kIx(	(
<g/>
ústřední	ústřední	k2eAgNnSc4d1	ústřední
topení	topení	k1gNnSc4	topení
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
vyhřívací	vyhřívací	k2eAgInPc4d1	vyhřívací
okruhy	okruh	k1gInPc4	okruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fosforečnany	fosforečnan	k1gInPc1	fosforečnan
amonné	amonný	k2eAgInPc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
HPO	HPO	kA	HPO
<g/>
4	[number]	k4	4
a	a	k8xC	a
NH4H2PO4	NH4H2PO4	k1gMnSc1	NH4H2PO4
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
účinná	účinný	k2eAgNnPc1d1	účinné
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
samozhášecí	samozhášecí	k2eAgFnSc1d1	samozhášecí
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
celulózy	celulóza	k1gFnSc2	celulóza
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zmenšit	zmenšit	k5eAaPmF	zmenšit
hořlavost	hořlavost	k1gFnSc4	hořlavost
výsledných	výsledný	k2eAgInPc2d1	výsledný
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
divadelní	divadelní	k2eAgFnPc4d1	divadelní
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Fosfor	fosfor	k1gInSc1	fosfor
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
řadě	řada	k1gFnSc6	řada
různých	různý	k2eAgFnPc2d1	různá
anorganických	anorganický	k2eAgFnPc2d1	anorganická
i	i	k8xC	i
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řady	řada	k1gFnSc2	řada
anorganických	anorganický	k2eAgFnPc2d1	anorganická
sloučenin	sloučenina	k1gFnPc2	sloučenina
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
trihydrogenfosforečná	trihydrogenfosforečný	k2eAgFnSc1d1	trihydrogenfosforečná
H3PO4	H3PO4	k1gFnSc1	H3PO4
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
trojsytná	trojsytný	k2eAgFnSc1d1	trojsytný
minerální	minerální	k2eAgFnSc1d1	minerální
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
elementárního	elementární	k2eAgInSc2d1	elementární
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kyselina	kyselina	k1gFnSc1	kyselina
fosforečná	fosforečný	k2eAgFnSc1d1	fosforečná
je	být	k5eAaImIp3nS	být
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
viskosní	viskosní	k2eAgFnSc1d1	viskosní
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
řady	řada	k1gFnSc2	řada
solí	sůl	k1gFnPc2	sůl
–	–	k?	–
fosforečnany	fosforečnan	k1gInPc4	fosforečnan
(	(	kIx(	(
<g/>
PO	po	k7c6	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
hydrogenfosforečnany	hydrogenfosforečnan	k1gInPc1	hydrogenfosforečnan
(	(	kIx(	(
<g/>
HPO	HPO	kA	HPO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
a	a	k8xC	a
dihydrogenfosforečnany	dihydrogenfosforečnana	k1gFnSc2	dihydrogenfosforečnana
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
PO	po	k7c4	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
kyseliny	kyselina	k1gFnPc1	kyselina
fosforu	fosfor	k1gInSc2	fosfor
jsou	být	k5eAaImIp3nP	být
kyseliny	kyselina	k1gFnSc2	kyselina
fosforitá	fosforitý	k2eAgFnSc1d1	fosforitý
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
PO	Po	kA	Po
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
fosforičitá	fosforičitý	k2eAgFnSc1d1	fosforičitý
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
fosforná	fosforný	k2eAgFnSc1d1	fosforný
H3PO2	H3PO2	k1gFnSc1	H3PO2
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
metafosforečná	metafosforečný	k2eAgFnSc1d1	metafosforečný
HPO	HPO	kA	HPO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kyseliny	kyselina	k1gFnPc1	kyselina
fosforu	fosfor	k1gInSc2	fosfor
dokáží	dokázat	k5eAaPmIp3nP	dokázat
polymerovat	polymerovat	k5eAaBmF	polymerovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
fosforečný	fosforečný	k2eAgMnSc1d1	fosforečný
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
molekul	molekula	k1gFnPc2	molekula
P4O10	P4O10	k1gFnSc2	P4O10
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
za	za	k7c2	za
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vznikají	vznikat	k5eAaImIp3nP	vznikat
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
fosforečných	fosforečný	k2eAgFnPc2d1	fosforečná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
sušení	sušení	k1gNnSc4	sušení
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
i	i	k8xC	i
stopy	stopa	k1gFnPc4	stopa
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
fosforitý	fosforitý	k2eAgInSc1d1	fosforitý
má	mít	k5eAaImIp3nS	mít
vzorec	vzorec	k1gInSc1	vzorec
P	P	kA	P
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jedovatou	jedovatý	k2eAgFnSc4d1	jedovatá
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
fosforité	fosforitý	k2eAgFnSc2d1	fosforitý
a	a	k8xC	a
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
fosfanu	fosfan	k1gInSc2	fosfan
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
trihydrogenfosforečné	trihydrogenfosforečný	k2eAgFnSc2d1	trihydrogenfosforečná
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
fosforitý	fosforitý	k2eAgMnSc1d1	fosforitý
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
spalováním	spalování	k1gNnSc7	spalování
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
za	za	k7c2	za
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
fosforičitý	fosforičitý	k2eAgInSc1d1	fosforičitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
také	také	k9	také
dimer	dimer	k1gInSc1	dimer
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
bezbarvé	bezbarvý	k2eAgFnPc4d1	bezbarvá
,	,	kIx,	,
silně	silně	k6eAd1	silně
lesklé	lesklý	k2eAgInPc1d1	lesklý
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
značného	značný	k2eAgInSc2d1	značný
vývoje	vývoj	k1gInSc2	vývoj
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
fosforité	fosforitý	k2eAgFnSc2d1	fosforitý
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
trihydrogenfosforečné	trihydrogenfosforečný	k2eAgFnSc2d1	trihydrogenfosforečná
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
oxidu	oxid	k1gInSc2	oxid
fosforitého	fosforitý	k2eAgInSc2d1	fosforitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chloridy	chlorid	k1gInPc1	chlorid
fosforu	fosfor	k1gInSc2	fosfor
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
PCl	PCl	k?	PCl
<g/>
2	[number]	k4	2
chlorid	chlorid	k1gInSc1	chlorid
fosfornatý	fosfornatý	k2eAgInSc1d1	fosfornatý
je	být	k5eAaImIp3nS	být
nesnadno	snadno	k6eNd1	snadno
získatelná	získatelný	k2eAgFnSc1d1	získatelná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
elektrického	elektrický	k2eAgInSc2d1	elektrický
výboje	výboj	k1gInSc2	výboj
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
PCl	PCl	k1gFnSc2	PCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
páchnoucí	páchnoucí	k2eAgFnSc1d1	páchnoucí
po	po	k7c6	po
fosforu	fosfor	k1gInSc6	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
PCl	PCl	k?	PCl
<g/>
3	[number]	k4	3
chlorid	chlorid	k1gInSc1	chlorid
fosforitý	fosforitý	k2eAgInSc1d1	fosforitý
vzniká	vznikat	k5eAaImIp3nS	vznikat
spalováním	spalování	k1gNnSc7	spalování
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
PCl	PCl	k?	PCl
<g/>
5	[number]	k4	5
chlorid	chlorid	k1gInSc1	chlorid
fosforečný	fosforečný	k2eAgInSc1d1	fosforečný
vzniká	vznikat	k5eAaImIp3nS	vznikat
spalováním	spalování	k1gNnSc7	spalování
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
aniž	aniž	k8xC	aniž
taje	tát	k5eAaImIp3nS	tát
<g/>
.	.	kIx.	.
</s>
<s>
PCl	PCl	k?	PCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
PCl	PCl	k1gFnSc1	PCl
<g/>
5	[number]	k4	5
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
svých	svůj	k3xOyFgFnPc2	svůj
trojsytných	trojsytný	k2eAgFnPc2d1	trojsytný
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
fosforečný	fosforečný	k2eAgInSc1d1	fosforečný
P3N5	P3N5	k1gFnSc4	P3N5
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
amoniaku	amoniak	k1gInSc2	amoniak
na	na	k7c4	na
sulfid	sulfid	k1gInSc4	sulfid
fosforečný	fosforečný	k2eAgInSc4d1	fosforečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
a	a	k8xC	a
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Fosfan	Fosfan	k1gMnSc1	Fosfan
PH3	PH3	k1gMnSc1	PH3
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
plyn	plyn	k1gInSc4	plyn
se	s	k7c7	s
zápachem	zápach	k1gInSc7	zápach
po	po	k7c6	po
česneku	česnek	k1gInSc6	česnek
<g/>
,	,	kIx,	,
difosfan	difosfan	k1gInSc1	difosfan
P2H4	P2H4	k1gFnSc2	P2H4
je	být	k5eAaImIp3nS	být
samozápalná	samozápalný	k2eAgFnSc1d1	samozápalná
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Fosfan	Fosfan	k1gMnSc1	Fosfan
lze	lze	k6eAd1	lze
nejlépe	dobře	k6eAd3	dobře
připravit	připravit	k5eAaPmF	připravit
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
jodid	jodid	k1gInSc4	jodid
fosfonia	fosfonium	k1gNnSc2	fosfonium
[	[	kIx(	[
<g/>
PH	Ph	kA	Ph
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
I	i	k9	i
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fosfor	fosfor	k1gInSc4	fosfor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
fosfor	fosfor	k1gInSc1	fosfor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
eMedicine	eMedicinout	k5eAaPmIp3nS	eMedicinout
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
Article	Articl	k1gMnSc2	Articl
on	on	k3xPp3gMnSc1	on
White	Whit	k1gInSc5	Whit
Phosphorus	Phosphorus	k1gInSc4	Phosphorus
as	as	k1gNnSc3	as
used	useda	k1gFnPc2	useda
as	as	k1gNnPc2	as
weapon	weapon	k1gMnSc1	weapon
</s>
</p>
<p>
<s>
Website	Websit	k1gInSc5	Websit
of	of	k?	of
the	the	k?	the
Technische	Technische	k1gFnSc3	Technische
Universität	Universität	k2eAgInSc1d1	Universität
Darmstadt	Darmstadt	k1gInSc1	Darmstadt
and	and	k?	and
the	the	k?	the
CEEP	CEEP	kA	CEEP
about	about	k1gMnSc1	about
Phosphorus	Phosphorus	k1gMnSc1	Phosphorus
Recovery	Recovera	k1gFnSc2	Recovera
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fosfor-	Fosfor-	k?	Fosfor-
<g/>
32	[number]	k4	32
</s>
</p>
