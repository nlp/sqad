<s>
Barevné	barevný	k2eAgNnSc1d1	barevné
spektrum	spektrum	k1gNnSc1	spektrum
je	být	k5eAaImIp3nS	být
lidským	lidský	k2eAgNnSc7d1	lidské
okem	oke	k1gNnSc7	oke
viditelná	viditelný	k2eAgFnSc1d1	viditelná
část	část	k1gFnSc1	část
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
o	o	k7c6	o
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
380	[number]	k4	380
až	až	k8xS	až
750	[number]	k4	750
nm	nm	k?	nm
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
frekvenci	frekvence	k1gFnSc4	frekvence
790	[number]	k4	790
-	-	kIx~	-
400	[number]	k4	400
THz	THz	k1gFnPc2	THz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
prostředích	prostředí	k1gNnPc6	prostředí
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozsah	rozsah	k1gInSc1	rozsah
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
na	na	k7c4	na
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
555	[number]	k4	555
nm	nm	k?	nm
(	(	kIx(	(
<g/>
540	[number]	k4	540
THz	THz	k1gFnPc2	THz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgNnSc1d1	barevné
spektrum	spektrum	k1gNnSc1	spektrum
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
známé	známý	k2eAgFnPc4d1	známá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
to	ten	k3xDgNnSc1	ten
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
součinností	součinnost	k1gFnSc7	součinnost
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
mezi	mezi	k7c7	mezi
červenou	červený	k2eAgFnSc7d1	červená
a	a	k8xC	a
fialovou	fialový	k2eAgFnSc7d1	fialová
(	(	kIx(	(
<g/>
purpurová	purpurový	k2eAgNnPc4d1	purpurové
<g/>
)	)	kIx)	)
a	a	k8xC	a
nesaturované	saturovaný	k2eNgFnPc1d1	nesaturovaná
barvy	barva	k1gFnPc1	barva
jako	jako	k8xC	jako
růžová	růžový	k2eAgFnSc1d1	růžová
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
složeny	složit	k5eAaPmNgFnP	složit
ze	z	k7c2	z
směsice	směsice	k1gFnSc2	směsice
různých	různý	k2eAgFnPc2d1	různá
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
