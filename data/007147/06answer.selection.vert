<s>
Oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
na	na	k7c4	na
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
555	[number]	k4	555
nm	nm	k?	nm
(	(	kIx(	(
<g/>
540	[number]	k4	540
THz	THz	k1gFnPc2	THz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
