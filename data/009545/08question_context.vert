<s>
Vostok	Vostok	k1gInSc1	Vostok
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
asi	asi	k9	asi
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pod	pod	k7c7	pod
antarktickým	antarktický	k2eAgInSc7d1	antarktický
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
