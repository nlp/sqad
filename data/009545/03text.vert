<p>
<s>
Vostok	Vostok	k1gInSc1	Vostok
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
asi	asi	k9	asi
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pod	pod	k7c7	pod
antarktickým	antarktický	k2eAgInSc7d1	antarktický
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
77	[number]	k4	77
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
na	na	k7c4	na
105	[number]	k4	105
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ruskou	ruský	k2eAgFnSc7d1	ruská
polární	polární	k2eAgFnSc7d1	polární
základnou	základna	k1gFnSc7	základna
Vostok	Vostok	k1gInSc1	Vostok
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4000	[number]	k4	4000
m	m	kA	m
pod	pod	k7c7	pod
antarktickým	antarktický	k2eAgInSc7d1	antarktický
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
250	[number]	k4	250
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
měří	měřit	k5eAaImIp3nS	měřit
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
veliké	veliký	k2eAgNnSc1d1	veliké
asi	asi	k9	asi
jako	jako	k8xS	jako
jezero	jezero	k1gNnSc1	jezero
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jezero	jezero	k1gNnSc1	jezero
hloubky	hloubka	k1gFnSc2	hloubka
až	až	k9	až
800	[number]	k4	800
m.	m.	k?	m.
Jezero	jezero	k1gNnSc1	jezero
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
000	[number]	k4	000
km2	km2	k4	km2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
2800	[number]	k4	2800
km3	km3	k4	km3
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
ledovce	ledovec	k1gInSc2	ledovec
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
3750	[number]	k4	3750
až	až	k9	až
4200	[number]	k4	4200
m	m	kA	m
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc4	povrch
ledovce	ledovec	k1gInSc2	ledovec
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
3488	[number]	k4	3488
m.	m.	k?	m.
Oblast	oblast	k1gFnSc1	oblast
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejnehostinnějších	hostinný	k2eNgNnPc2d3	hostinný
<g/>
,	,	kIx,	,
nejhůře	zle	k6eAd3	zle
dostupných	dostupný	k2eAgNnPc2d1	dostupné
a	a	k8xC	a
nejchladnějších	chladný	k2eAgNnPc2d3	nejchladnější
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
–	–	k?	–
<g/>
89,2	[number]	k4	89,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
jižnímu	jižní	k2eAgNnSc3d1	jižní
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
pólu	pólo	k1gNnSc3	pólo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Polární	polární	k2eAgFnSc1d1	polární
stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ruští	ruský	k2eAgMnPc1d1	ruský
a	a	k8xC	a
britští	britský	k2eAgMnPc1d1	britský
vědci	vědec	k1gMnPc1	vědec
jezero	jezero	k1gNnSc4	jezero
objevili	objevit	k5eAaPmAgMnP	objevit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
3-4	[number]	k4	3-4
km	km	kA	km
pod	pod	k7c7	pod
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nedotčené	dotčený	k2eNgFnSc2d1	nedotčená
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
už	už	k6eAd1	už
asi	asi	k9	asi
35	[number]	k4	35
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stará	starý	k2eAgFnSc1d1	stará
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
asi	asi	k9	asi
milión	milión	k4xCgInSc4	milión
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Vostok	Vostok	k1gInSc1	Vostok
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
objeveným	objevený	k2eAgNnSc7d1	objevené
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
-3	-3	k4	-3
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
2,83	[number]	k4	2,83
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
-2,53	-2,53	k4	-2,53
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
tlaku	tlak	k1gInSc3	tlak
se	se	k3xPyFc4	se
i	i	k9	i
za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
teploty	teplota	k1gFnSc2	teplota
udržuje	udržovat	k5eAaImIp3nS	udržovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Geotermální	geotermální	k2eAgNnSc1d1	geotermální
teplo	teplo	k1gNnSc1	teplo
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mikroby	mikrob	k1gInPc1	mikrob
a	a	k8xC	a
bakterie	bakterie	k1gFnPc1	bakterie
==	==	k?	==
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spodní	spodní	k2eAgFnSc4d1	spodní
vrstvu	vrstva	k1gFnSc4	vrstva
ledového	ledový	k2eAgInSc2d1	ledový
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc2d1	obsahující
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
nahromaděná	nahromaděný	k2eAgFnSc1d1	nahromaděná
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	on	k3xPp3gMnPc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
k	k	k7c3	k
předpokladu	předpoklad	k1gInSc3	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
společnost	společnost	k1gFnSc1	společnost
mikrobů	mikrob	k1gInPc2	mikrob
žila	žít	k5eAaImAgFnS	žít
také	také	k9	také
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
doopravdy	doopravdy	k6eAd1	doopravdy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
zde	zde	k6eAd1	zde
otázky	otázka	k1gFnPc4	otázka
o	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
vzniku	vznik	k1gInSc2	vznik
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
se	se	k3xPyFc4	se
také	také	k9	také
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
existovat	existovat	k5eAaImF	existovat
život	život	k1gInSc4	život
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
mikrobů	mikrob	k1gInPc2	mikrob
v	v	k7c6	v
antarktickém	antarktický	k2eAgInSc6d1	antarktický
ledovém	ledový	k2eAgInSc6d1	ledový
štítu	štít	k1gInSc6	štít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1999	[number]	k4	1999
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
ohlásily	ohlásit	k5eAaPmAgInP	ohlásit
dva	dva	k4xCgInPc1	dva
nezávislé	závislý	k2eNgInPc1d1	nezávislý
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
vede	vést	k5eAaImIp3nS	vést
John	John	k1gMnSc1	John
Priscu	Priscus	k1gInSc2	Priscus
<g/>
,	,	kIx,	,
ekolog	ekolog	k1gMnSc1	ekolog
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
státu	stát	k1gInSc2	stát
Montana	Montana	k1gFnSc1	Montana
v	v	k7c6	v
Bozemane	Bozeman	k1gMnSc5	Bozeman
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
David	David	k1gMnSc1	David
Karl	Karl	k1gMnSc1	Karl
<g/>
,	,	kIx,	,
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
na	na	k7c6	na
Havajské	havajský	k2eAgFnSc6d1	Havajská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Honolulu	Honolulu	k1gNnSc6	Honolulu
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
oba	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
(	(	kIx(	(
<g/>
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
výzkumníci	výzkumník	k1gMnPc1	výzkumník
<g/>
)	)	kIx)	)
usilovně	usilovně	k6eAd1	usilovně
pracovaly	pracovat	k5eAaImAgInP	pracovat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
nové	nový	k2eAgInPc1d1	nový
výsledky	výsledek	k1gInPc1	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
informace	informace	k1gFnPc1	informace
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikrobi	mikrob	k1gMnPc1	mikrob
jsou	být	k5eAaImIp3nP	být
fyziologicky	fyziologicky	k6eAd1	fyziologicky
rozmanití	rozmanitý	k2eAgMnPc1d1	rozmanitý
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
též	též	k9	též
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezero	jezero	k1gNnSc1	jezero
Vostok	Vostok	k1gInSc1	Vostok
skrývá	skrývat	k5eAaImIp3nS	skrývat
život	život	k1gInSc4	život
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Prisca	Prisc	k1gInSc2	Prisc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
okolo	okolo	k7c2	okolo
10	[number]	k4	10
000	[number]	k4	000
mikrobiálních	mikrobiální	k2eAgFnPc2d1	mikrobiální
buněk	buňka	k1gFnPc2	buňka
na	na	k7c4	na
mililitr	mililitr	k1gInSc4	mililitr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
asi	asi	k9	asi
stokrát	stokrát	k6eAd1	stokrát
méně	málo	k6eAd2	málo
než	než	k8xS	než
typický	typický	k2eAgInSc1d1	typický
počet	počet	k1gInSc1	počet
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Davida	David	k1gMnSc2	David
Karla	Karel	k1gMnSc2	Karel
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
také	také	k9	také
nové	nový	k2eAgFnPc4d1	nová
analýzy	analýza	k1gFnPc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
živá	živý	k2eAgFnSc1d1	živá
mikrobiální	mikrobiální	k2eAgFnSc1d1	mikrobiální
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
její	její	k3xOp3gFnSc1	její
biomasa	biomasa	k1gFnSc1	biomasa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
vědci	vědec	k1gMnPc1	vědec
však	však	k9	však
o	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
výsledcích	výsledek	k1gInPc6	výsledek
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mikrobi	mikrob	k1gMnPc1	mikrob
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
také	také	k9	také
třeba	třeba	k6eAd1	třeba
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
výzkumníků	výzkumník	k1gMnPc2	výzkumník
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
vrstvy	vrstva	k1gFnSc2	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
komunita	komunita	k1gFnSc1	komunita
je	být	k5eAaImIp3nS	být
rozhodnutá	rozhodnutý	k2eAgFnSc1d1	rozhodnutá
tento	tento	k3xDgInSc4	tento
spor	spor	k1gInSc1	spor
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
dohodnutá	dohodnutý	k2eAgFnSc1d1	dohodnutá
na	na	k7c6	na
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
a	a	k8xC	a
evropští	evropský	k2eAgMnPc1d1	evropský
vědci	vědec	k1gMnPc1	vědec
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
opatrný	opatrný	k2eAgInSc4d1	opatrný
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
je	být	k5eAaImIp3nS	být
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
glaciolog	glaciolog	k1gMnSc1	glaciolog
Martin	Martin	k1gMnSc1	Martin
Siegert	Siegert	k1gMnSc1	Siegert
z	z	k7c2	z
Bristolské	bristolský	k2eAgFnSc2d1	Bristolská
univerzity	univerzita	k1gFnSc2	univerzita
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
odebrat	odebrat	k5eAaPmF	odebrat
nejprve	nejprve	k6eAd1	nejprve
vzorky	vzorek	k1gInPc4	vzorek
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Ellsworth	Ellswortha	k1gFnPc2	Ellswortha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
subglaciální	subglaciální	k2eAgNnSc1d1	subglaciální
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
výzkum	výzkum	k1gInSc1	výzkum
Vostoku	Vostok	k1gInSc2	Vostok
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaPmF	stát
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
Ellsworth	Ellsworth	k1gMnSc1	Ellsworth
by	by	kYmCp3nS	by
odhalil	odhalit	k5eAaPmAgMnS	odhalit
svoje	svůj	k3xOyFgNnSc4	svůj
tajemství	tajemství	k1gNnSc4	tajemství
asi	asi	k9	asi
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
milióny	milión	k4xCgInPc4	milión
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
se	se	k3xPyFc4	se
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
sedimenty	sediment	k1gInPc1	sediment
v	v	k7c6	v
sobě	se	k3xPyFc3	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
skutečně	skutečně	k6eAd1	skutečně
tak	tak	k6eAd1	tak
fascinující	fascinující	k2eAgInSc4d1	fascinující
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
výzkum	výzkum	k1gInSc4	výzkum
Vostoku	Vostok	k1gInSc2	Vostok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
vědci	vědec	k1gMnPc1	vědec
nedávno	nedávno	k6eAd1	nedávno
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
plán	plán	k1gInSc4	plán
provrtat	provrtat	k5eAaPmF	provrtat
se	se	k3xPyFc4	se
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
léta	léto	k1gNnSc2	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Priscu	Priscu	k6eAd1	Priscu
drží	držet	k5eAaImIp3nS	držet
odvážným	odvážný	k2eAgMnPc3d1	odvážný
Rusům	Rus	k1gMnPc3	Rus
palce	palec	k1gInPc4	palec
<g/>
,	,	kIx,	,
upřednostnil	upřednostnit	k5eAaPmAgMnS	upřednostnit
by	by	kYmCp3nS	by
však	však	k9	však
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
akci	akce	k1gFnSc4	akce
s	s	k7c7	s
lepším	dobrý	k2eAgInSc7d2	lepší
environmentálním	environmentální	k2eAgInSc7d1	environmentální
<g/>
,	,	kIx,	,
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
a	a	k8xC	a
vědeckým	vědecký	k2eAgInSc7d1	vědecký
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
ducha	duch	k1gMnSc4	duch
výzkumů	výzkum	k1gInPc2	výzkum
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
přínos	přínos	k1gInSc1	přínos
antarktického	antarktický	k2eAgNnSc2d1	antarktické
jezera	jezero	k1gNnSc2	jezero
by	by	kYmCp3nS	by
ale	ale	k9	ale
mohl	moct	k5eAaImAgInS	moct
paradoxně	paradoxně	k6eAd1	paradoxně
být	být	k5eAaImF	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kosmického	kosmický	k2eAgInSc2d1	kosmický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
sond	sonda	k1gFnPc2	sonda
totiž	totiž	k9	totiž
vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Jupiterově	Jupiterův	k2eAgInSc6d1	Jupiterův
měsíci	měsíc	k1gInSc6	měsíc
Europě	Europa	k1gFnSc6	Europa
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
ledovým	ledový	k2eAgInSc7d1	ledový
příkrovem	příkrov	k1gInSc7	příkrov
celé	celá	k1gFnSc2	celá
tekuté	tekutý	k2eAgInPc1d1	tekutý
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
snímky	snímek	k1gInPc1	snímek
prasklin	prasklina	k1gFnPc2	prasklina
v	v	k7c6	v
ledu	led	k1gInSc6	led
získané	získaný	k2eAgFnSc2d1	získaná
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
tepla	teplo	k1gNnSc2	teplo
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Europy	Europa	k1gFnSc2	Europa
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
slapové	slapový	k2eAgFnPc4d1	slapová
síly	síla	k1gFnPc4	síla
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
mocnou	mocný	k2eAgFnSc7d1	mocná
gravitací	gravitace	k1gFnSc7	gravitace
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Vostok	Vostok	k1gInSc4	Vostok
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
prototyp	prototyp	k1gInSc4	prototyp
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
a	a	k8xC	a
testování	testování	k1gNnSc4	testování
kosmické	kosmický	k2eAgFnSc2d1	kosmická
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
provést	provést	k5eAaPmF	provést
průzkum	průzkum	k1gInSc4	průzkum
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
navíc	navíc	k6eAd1	navíc
doufají	doufat	k5eAaImIp3nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
skrývat	skrývat	k5eAaImF	skrývat
život	život	k1gInSc4	život
pod	pod	k7c4	pod
kilometry	kilometr	k1gInPc4	kilometr
ledu	led	k1gInSc2	led
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
tomu	ten	k3xDgNnSc3	ten
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
i	i	k9	i
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vostok	Vostok	k1gInSc1	Vostok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jezero	jezero	k1gNnSc1	jezero
Vostok	Vostok	k1gInSc1	Vostok
</s>
</p>
<p>
<s>
Rusové	Rus	k1gMnPc1	Rus
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
provrtat	provrtat	k5eAaPmF	provrtat
do	do	k7c2	do
nejtajemnějšího	tajemný	k2eAgNnSc2d3	nejtajemnější
jezera	jezero	k1gNnSc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
–	–	k?	–
Matouš	Matouš	k1gMnSc1	Matouš
Lázňovský	Lázňovský	k1gMnSc1	Lázňovský
<g/>
,	,	kIx,	,
Technet	Technet	k1gMnSc1	Technet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
;	;	kIx,	;
článek	článek	k1gInSc4	článek
obsahující	obsahující	k2eAgInSc4d1	obsahující
přehled	přehled	k1gInSc4	přehled
pokusů	pokus	k1gInPc2	pokus
o	o	k7c6	o
navrtání	navrtání	k1gNnSc6	navrtání
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
The	The	k?	The
Lost	Lost	k2eAgMnSc1d1	Lost
World	World	k1gMnSc1	World
of	of	k?	of
Lake	Lake	k1gInSc1	Lake
Vostok	Vostok	k1gInSc1	Vostok
<g/>
,	,	kIx,	,
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
se	s	k7c7	s
slovenskými	slovenský	k2eAgInPc7d1	slovenský
titulky	titulek	k1gInPc7	titulek
<g/>
)	)	kIx)	)
</s>
</p>
