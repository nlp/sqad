<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Vatikánu	Vatikán	k1gInSc2	Vatikán
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
svislé	svislý	k2eAgInPc4d1	svislý
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc4d1	žlutý
a	a	k8xC	a
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k1gMnSc2	bílý
jsou	být	k5eAaImIp3nP	být
překřížené	překřížený	k2eAgInPc4d1	překřížený
klíče	klíč	k1gInPc4	klíč
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
tiára	tiára	k1gFnSc1	tiára
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Klíče	klíč	k1gInPc1	klíč
jsou	být	k5eAaImIp3nP	být
obrácené	obrácený	k2eAgMnPc4d1	obrácený
ven	ven	k6eAd1	ven
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
evangelium	evangelium	k1gNnSc4	evangelium
sv.	sv.	kA	sv.
Matouše	Matouš	k1gMnSc2	Matouš
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
tobě	ty	k3xPp2nSc3	ty
dám	dát	k5eAaPmIp1nS	dát
klíče	klíč	k1gInPc4	klíč
od	od	k7c2	od
království	království	k1gNnSc2	království
nebeského	nebeský	k2eAgNnSc2d1	nebeské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
barvy	barva	k1gFnPc1	barva
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
svěřenou	svěřený	k2eAgFnSc4d1	svěřená
moc	moc	k1gFnSc4	moc
pouštět	pouštět	k5eAaImF	pouštět
a	a	k8xC	a
otvírat	otvírat	k5eAaImF	otvírat
(	(	kIx(	(
<g/>
stříbrná	stříbrná	k1gFnSc1	stříbrná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
i	i	k8xC	i
zadržovat	zadržovat	k5eAaImF	zadržovat
a	a	k8xC	a
zavírat	zavírat	k5eAaImF	zavírat
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
červená	červený	k2eAgFnSc1d1	červená
šňůra	šňůra	k1gFnSc1	šňůra
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
jsou	být	k5eAaImIp3nP	být
klíče	klíč	k1gInPc1	klíč
svazány	svazat	k5eAaPmNgInP	svazat
<g/>
,	,	kIx,	,
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
moci	moc	k1gFnPc1	moc
spolu	spolu	k6eAd1	spolu
souvisí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Žluto-bílá	Žlutoílý	k2eAgFnSc1d1	Žluto-bílý
tiára	tiára	k1gFnSc1	tiára
–	–	k?	–
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
papežská	papežský	k2eAgFnSc1d1	Papežská
koruna	koruna	k1gFnSc1	koruna
–	–	k?	–
představuje	představovat	k5eAaImIp3nS	představovat
trojitý	trojitý	k2eAgInSc4d1	trojitý
papežský	papežský	k2eAgInSc4d1	papežský
úřad	úřad	k1gInSc4	úřad
<g/>
:	:	kIx,	:
učitelský	učitelský	k2eAgInSc4d1	učitelský
<g/>
,	,	kIx,	,
kněžský	kněžský	k2eAgInSc4d1	kněžský
a	a	k8xC	a
pastýřský	pastýřský	k2eAgInSc4d1	pastýřský
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
papežské	papežský	k2eAgFnSc3d1	Papežská
kokardě	kokarda	k1gFnSc3	kokarda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
byla	být	k5eAaImAgFnS	být
červená	červená	k1gFnSc1	červená
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
po	po	k7c6	po
francouzské	francouzský	k2eAgFnSc3d1	francouzská
okupaci	okupace	k1gFnSc3	okupace
Říma	Řím	k1gInSc2	Řím
nahrazená	nahrazený	k2eAgFnSc1d1	nahrazená
bílou	bílý	k2eAgFnSc7d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
oficiálně	oficiálně	k6eAd1	oficiálně
potvrzené	potvrzený	k2eAgFnPc1d1	potvrzená
pro	pro	k7c4	pro
vlajky	vlajka	k1gFnPc4	vlajka
církevního	církevní	k2eAgInSc2d1	církevní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Odvozují	odvozovat	k5eAaImIp3nP	odvozovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
svatopetrských	svatopetrský	k2eAgInPc2d1	svatopetrský
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
i	i	k9	i
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
království	království	k1gNnSc2	království
jeruzalémského	jeruzalémský	k2eAgNnSc2d1	Jeruzalémské
<g/>
,	,	kIx,	,
založeného	založený	k2eAgNnSc2d1	založené
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
výprav	výprava	k1gFnPc2	výprava
(	(	kIx(	(
<g/>
1099	[number]	k4	1099
až	až	k9	až
1187	[number]	k4	1187
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zlatý	zlatý	k2eAgInSc4d1	zlatý
kříž	kříž	k1gInSc4	kříž
na	na	k7c6	na
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeruzalémský	jeruzalémský	k2eAgMnSc1d1	jeruzalémský
král	král	k1gMnSc1	král
Balduin	Balduin	k1gMnSc1	Balduin
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1141	[number]	k4	1141
<g/>
–	–	k?	–
<g/>
1162	[number]	k4	1162
<g/>
)	)	kIx)	)
používal	používat	k5eAaImAgInS	používat
bílou	bílý	k2eAgFnSc4d1	bílá
zástavu	zástava	k1gFnSc4	zástava
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
si	se	k3xPyFc3	se
přidal	přidat	k5eAaPmAgMnS	přidat
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Amaury	Amaura	k1gFnSc2	Amaura
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1162	[number]	k4	1162
<g/>
–	–	k?	–
<g/>
1173	[number]	k4	1173
<g/>
)	)	kIx)	)
žlutý	žlutý	k2eAgInSc1d1	žlutý
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
čtvercový	čtvercový	k2eAgMnSc1d1	čtvercový
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
zakotvená	zakotvený	k2eAgFnSc1d1	zakotvená
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
zákoně	zákon	k1gInSc6	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1825	[number]	k4	1825
až	až	k9	až
1878	[number]	k4	1878
bývala	bývat	k5eAaImAgFnS	bývat
vlajkou	vlajka	k1gFnSc7	vlajka
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
