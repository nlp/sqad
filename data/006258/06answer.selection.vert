<s>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc1d1	plný
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
/	/	kIx~	/
<g/>
Radio	radio	k1gNnSc1	radio
Liberty	Libert	k1gInPc1	Libert
<g/>
,	,	kIx,	,
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
organizace	organizace	k1gFnSc1	organizace
založená	založený	k2eAgFnSc1d1	založená
Kongresem	kongres	k1gInSc7	kongres
USA	USA	kA	USA
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
objektivních	objektivní	k2eAgFnPc2d1	objektivní
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
diktátorských	diktátorský	k2eAgInPc6d1	diktátorský
režimech	režim	k1gInPc6	režim
<g/>
.	.	kIx.	.
</s>
