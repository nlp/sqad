<s>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc1d1	plný
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
/	/	kIx~	/
<g/>
Radio	radio	k1gNnSc1	radio
Liberty	Libert	k1gInPc1	Libert
<g/>
,	,	kIx,	,
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
organizace	organizace	k1gFnSc1	organizace
založená	založený	k2eAgFnSc1d1	založená
Kongresem	kongres	k1gInSc7	kongres
USA	USA	kA	USA
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
objektivních	objektivní	k2eAgFnPc2d1	objektivní
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
diktátorských	diktátorský	k2eAgInPc6d1	diktátorský
režimech	režim	k1gInPc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
RFE	RFE	kA	RFE
sám	sám	k3xTgInSc1	sám
ovšem	ovšem	k9	ovšem
spadal	spadat	k5eAaPmAgInS	spadat
pod	pod	k7c4	pod
kompetenci	kompetence	k1gFnSc4	kompetence
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Committee	Committe	k1gInSc2	Committe
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Free	Free	k1gFnPc2	Free
Europe	Europ	k1gInSc5	Europ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
projevu	projev	k1gInSc6	projev
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
Westminster	Westminster	k1gInSc1	Westminster
College	Colleg	k1gInSc2	Colleg
<g/>
)	)	kIx)	)
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Fultonu	Fulton	k1gInSc6	Fulton
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
satelity	satelit	k1gInPc4	satelit
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
možností	možnost	k1gFnSc7	možnost
jak	jak	k8xS	jak
proniknout	proniknout	k5eAaPmF	proniknout
touto	tento	k3xDgFnSc7	tento
"	"	kIx"	"
<g/>
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
<g/>
"	"	kIx"	"
a	a	k8xC	a
svobodně	svobodně	k6eAd1	svobodně
informovat	informovat	k5eAaBmF	informovat
tamější	tamější	k2eAgNnSc4d1	tamější
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
byly	být	k5eAaImAgFnP	být
rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
televize	televize	k1gFnSc1	televize
v	v	k7c6	v
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1949	[number]	k4	1949
založil	založit	k5eAaPmAgMnS	založit
zastupující	zastupující	k2eAgMnSc1d1	zastupující
ředitel	ředitel	k1gMnSc1	ředitel
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
Welsh	Welsh	k1gMnSc1	Welsh
Dulles	Dulles	k1gMnSc1	Dulles
<g/>
,	,	kIx,	,
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
organizaci	organizace	k1gFnSc4	organizace
"	"	kIx"	"
<g/>
Národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
pro	pro	k7c4	pro
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Committee	Committe	k1gInSc2	Committe
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Free	Free	k1gFnPc2	Free
Europe	Europ	k1gMnSc5	Europ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
podobné	podobný	k2eAgFnSc2d1	podobná
organizace	organizace	k1gFnSc2	organizace
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Rada	rada	k1gFnSc1	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Council	Council	k1gInSc1	Council
of	of	k?	of
Free	Free	k1gInSc1	Free
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
konzultuje	konzultovat	k5eAaImIp3nS	konzultovat
Dulles	Dulles	k1gInSc4	Dulles
vedení	vedení	k1gNnSc2	vedení
rady	rada	k1gFnSc2	rada
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Petra	Petr	k1gMnSc2	Petr
Zenkla	Zenkla	k1gMnSc2	Zenkla
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Jana	Jan	k1gMnSc2	Jan
Bervidy	Bervida	k1gFnSc2	Bervida
<g/>
,	,	kIx,	,
Julia	Julius	k1gMnSc2	Julius
Firta	Firt	k1gMnSc2	Firt
a	a	k8xC	a
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Poláky	Polák	k1gMnPc4	Polák
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
Jan	Jan	k1gMnSc1	Jan
Nowak-Jezioraňski	Nowak-Jezioraňsk	k1gFnSc2	Nowak-Jezioraňsk
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
Dulles	Dulles	k1gMnSc1	Dulles
sdělil	sdělit	k5eAaPmAgMnS	sdělit
exulantům	exulant	k1gMnPc3	exulant
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
John	John	k1gMnSc1	John
Foster	Foster	k1gMnSc1	Foster
Dulles	Dulles	k1gMnSc1	Dulles
<g/>
)	)	kIx)	)
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
vybudování	vybudování	k1gNnSc4	vybudování
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
vysílače	vysílač	k1gInSc2	vysílač
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
na	na	k7c6	na
území	území	k1gNnSc6	území
SRN	SRN	kA	SRN
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
tvrzení	tvrzení	k1gNnSc2	tvrzení
Jožky	Jožka	k1gMnSc2	Jožka
Pejskara	Pejskar	k1gMnSc2	Pejskar
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
stanice	stanice	k1gFnSc2	stanice
Radio	radio	k1gNnSc1	radio
Free	Fre	k1gInSc2	Fre
Europe	Europ	k1gInSc5	Europ
vynálezem	vynález	k1gInSc7	vynález
Julia	Julius	k1gMnSc2	Julius
Firta	Firt	k1gMnSc2	Firt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
zastupujícím	zastupující	k2eAgMnSc7d1	zastupující
ředitelem	ředitel	k1gMnSc7	ředitel
"	"	kIx"	"
<g/>
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
RFE	RFE	kA	RFE
<g/>
,	,	kIx,	,
redakce	redakce	k1gFnSc1	redakce
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
1955	[number]	k4	1955
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
ředitelem	ředitel	k1gMnSc7	ředitel
československé	československý	k2eAgFnSc2d1	Československá
sekce	sekce	k1gFnSc2	sekce
RFE	RFE	kA	RFE
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
RFE	RFE	kA	RFE
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1951	[number]	k4	1951
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
Volá	volat	k5eAaImIp3nS	volat
hlas	hlas	k1gInSc1	hlas
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
...	...	k?	...
Několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
ozval	ozvat	k5eAaPmAgInS	ozvat
zvuk	zvuk	k1gInSc1	zvuk
rušiček	rušička	k1gFnPc2	rušička
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc3	jejichž
typickému	typický	k2eAgInSc3d1	typický
zvuku	zvuk	k1gInSc3	zvuk
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
moskevské	moskevský	k2eAgFnPc4d1	Moskevská
boogie	boogie	k1gFnPc4	boogie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
ředitelem	ředitel	k1gMnSc7	ředitel
českého	český	k2eAgNnSc2d1	české
oddělení	oddělení	k1gNnSc2	oddělení
RFE	RFE	kA	RFE
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
-	-	kIx~	-
1961	[number]	k4	1961
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oněch	onen	k3xDgNnPc2	onen
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
RFE	RFE	kA	RFE
už	už	k9	už
vysílala	vysílat	k5eAaImAgFnS	vysílat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deseti	deset	k4xCc6	deset
jazycích	jazyk	k1gInPc6	jazyk
světa	svět	k1gInSc2	svět
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
vlnách	vlna	k1gFnPc6	vlna
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
objemu	objem	k1gInSc6	objem
přes	přes	k7c4	přes
500	[number]	k4	500
hodin	hodina	k1gFnPc2	hodina
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
RFE	RFE	kA	RFE
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
<g/>
,	,	kIx,	,
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
za	za	k7c7	za
"	"	kIx"	"
<g/>
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějšími	populární	k2eAgMnPc7d3	nejpopulárnější
československými	československý	k2eAgMnPc7d1	československý
hlasateli	hlasatel	k1gMnPc7	hlasatel
a	a	k8xC	a
žurnalisty	žurnalista	k1gMnSc2	žurnalista
RFE	RFE	kA	RFE
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
-	-	kIx~	-
1968	[number]	k4	1968
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Rozina	Rozina	k1gFnSc1	Rozina
Jadrná-Pokorná	Jadrná-Pokorný	k2eAgFnSc1d1	Jadrná-Pokorná
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgInPc1d1	hudební
pořady	pořad	k1gInPc1	pořad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
(	(	kIx(	(
<g/>
fejetony	fejeton	k1gInPc1	fejeton
a	a	k8xC	a
úvahy	úvaha	k1gFnPc1	úvaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jožka	Jožka	k1gMnSc1	Jožka
Pejskar	Pejskar	k1gMnSc1	Pejskar
(	(	kIx(	(
<g/>
píše	psát	k5eAaImIp3nS	psát
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
"	"	kIx"	"
<g/>
Jožka	Jožka	k1gFnSc1	Jožka
Pero	pero	k1gNnSc1	pero
<g/>
"	"	kIx"	"
politické	politický	k2eAgInPc1d1	politický
komentáře	komentář	k1gInPc1	komentář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
zakládá	zakládat	k5eAaImIp3nS	zakládat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
měsíčník	měsíčník	k1gInSc1	měsíčník
"	"	kIx"	"
<g/>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
<g/>
"	"	kIx"	"
jehož	jehož	k3xOyRp3gFnSc7	jehož
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mirek	Mirek	k1gMnSc1	Mirek
Podivínský	podivínský	k2eAgMnSc1d1	podivínský
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Martin	Martin	k1gMnSc1	Martin
Zemek	zemek	k1gMnSc1	zemek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pecháček	Pecháček	k1gMnSc1	Pecháček
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Paleček	Paleček	k1gMnSc1	Paleček
<g/>
,	,	kIx,	,
organizátoři	organizátor	k1gMnPc1	organizátor
"	"	kIx"	"
<g/>
letákových	letákový	k2eAgFnPc2d1	letáková
akcí	akce	k1gFnPc2	akce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
balóny	balón	k1gInPc4	balón
<g/>
)	)	kIx)	)
na	na	k7c6	na
území	území	k1gNnSc6	území
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Liberty	Libert	k1gInPc7	Libert
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc1	radio
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
pro	pro	k7c4	pro
národy	národ	k1gInPc4	národ
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Osvobození	osvobození	k1gNnSc2	osvobození
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc1	radio
Osvobožděnija	Osvobožděnij	k1gInSc2	Osvobožděnij
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Sergej	Sergej	k1gMnSc1	Sergej
Dubrovskij	Dubrovskij	k1gMnSc1	Dubrovskij
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
Rus	Rus	k1gMnSc1	Rus
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
emigrantského	emigrantský	k2eAgInSc2d1	emigrantský
spolku	spolek	k1gInSc2	spolek
"	"	kIx"	"
<g/>
Americká	americký	k2eAgFnSc1d1	americká
rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
bolševizmu	bolševizmus	k1gInSc2	bolševizmus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Amerikaňskij	Amerikaňskij	k1gMnSc3	Amerikaňskij
komitět	komitět	k5eAaImF	komitět
po	po	k7c6	po
osvobožděniju	osvobožděnít	k5eAaPmIp1nS	osvobožděnít
ot	ot	k1gMnSc1	ot
boševizma	boševizmum	k1gNnSc2	boševizmum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1959	[number]	k4	1959
ruší	rušit	k5eAaImIp3nS	rušit
ruské	ruský	k2eAgNnSc1d1	ruské
vedení	vedení	k1gNnSc1	vedení
vysílače	vysílač	k1gInSc2	vysílač
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Osvobození	osvobození	k1gNnPc2	osvobození
<g/>
"	"	kIx"	"
a	a	k8xC	a
přejímá	přejímat	k5eAaImIp3nS	přejímat
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc1	radio
Liberty	Libert	k1gInPc7	Libert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
přepadení	přepadení	k1gNnSc4	přepadení
ČSSR	ČSSR	kA	ČSSR
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
okolo	okolo	k7c2	okolo
440	[number]	k4	440
000	[number]	k4	000
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
československé	československý	k2eAgFnSc2d1	Československá
sekce	sekce	k1gFnSc2	sekce
RFE	RFE	kA	RFE
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pecháček	Pecháček	k1gMnSc1	Pecháček
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
-	-	kIx~	-
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
novinářích	novinář	k1gMnPc6	novinář
a	a	k8xC	a
publicistech	publicista	k1gMnPc6	publicista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
opustili	opustit	k5eAaPmAgMnP	opustit
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
perspektivní	perspektivní	k2eAgNnPc4d1	perspektivní
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
pracovní	pracovní	k2eAgFnSc4d1	pracovní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
RFE	RFE	kA	RFE
<g/>
.	.	kIx.	.
</s>
<s>
Pecháček	Pecháček	k1gMnSc1	Pecháček
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
strukturu	struktura	k1gFnSc4	struktura
vysílání	vysílání	k1gNnSc2	vysílání
předělal	předělat	k5eAaPmAgMnS	předělat
a	a	k8xC	a
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
ji	on	k3xPp3gFnSc4	on
aktuálním	aktuální	k2eAgMnPc3d1	aktuální
poměrům	poměr	k1gInPc3	poměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
RFE	RFE	kA	RFE
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
nové	nový	k2eAgFnPc1d1	nová
tváře	tvář	k1gFnPc1	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
žurnalisty	žurnalist	k1gMnPc4	žurnalist
a	a	k8xC	a
umělce	umělec	k1gMnSc2	umělec
vysokých	vysoký	k2eAgFnPc2d1	vysoká
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
Sláva	Sláva	k1gMnSc1	Sláva
Volný	volný	k2eAgMnSc1d1	volný
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Jezdinský	Jezdinský	k2eAgMnSc1d1	Jezdinský
<g/>
,	,	kIx,	,
Lída	Lída	k1gFnSc1	Lída
Rakušanová	Rakušanová	k1gFnSc1	Rakušanová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Valeská	Valeský	k2eAgFnSc1d1	Valeská
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Diviš	Diviš	k1gMnSc1	Diviš
a	a	k8xC	a
písničkář	písničkář	k1gMnSc1	písničkář
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
440	[number]	k4	440
000	[number]	k4	000
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
emigrantů	emigrant	k1gMnPc2	emigrant
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
220	[number]	k4	220
000	[number]	k4	000
usadilo	usadit	k5eAaPmAgNnS	usadit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
-	-	kIx~	-
1971	[number]	k4	1971
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
následně	následně	k6eAd1	následně
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
zámoří	zámoří	k1gNnSc2	zámoří
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Pecháček	Pecháček	k1gMnSc1	Pecháček
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
CIA	CIA	kA	CIA
přechodně	přechodně	k6eAd1	přechodně
otevřít	otevřít	k5eAaPmF	otevřít
pobočku	pobočka	k1gFnSc4	pobočka
československého	československý	k2eAgNnSc2d1	Československé
vysílání	vysílání	k1gNnSc2	vysílání
RFE	RFE	kA	RFE
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
"	"	kIx"	"
<g/>
Studia	studio	k1gNnSc2	studio
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Tibor	Tibor	k1gMnSc1	Tibor
Zlocha	zloch	k1gMnSc2	zloch
<g/>
.	.	kIx.	.
</s>
<s>
Volnými	volný	k2eAgMnPc7d1	volný
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
RFE	RFE	kA	RFE
"	"	kIx"	"
<g/>
Studio	studio	k1gNnSc1	studio
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Jonáš	Jonáš	k1gMnSc1	Jonáš
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
týdeníku	týdeník	k1gInSc2	týdeník
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
Čechů	Čech	k1gMnPc2	Čech
"	"	kIx"	"
<g/>
Vídeňské	vídeňský	k2eAgInPc1d1	vídeňský
svobodné	svobodný	k2eAgInPc1d1	svobodný
listy	list	k1gInPc1	list
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Berwid-Buquoy	Berwid-Buquoa	k1gFnSc2	Berwid-Buquoa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kameníček	kameníček	k1gInSc1	kameníček
a	a	k8xC	a
student	student	k1gMnSc1	student
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
světového	světový	k2eAgInSc2d1	světový
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
Hochschule	Hochschule	k1gFnSc1	Hochschule
für	für	k?	für
Welthandel	Welthandlo	k1gNnPc2	Welthandlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
Čechura	Čechura	k1gMnSc1	Čechura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1965	[number]	k4	1965
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
RFE	RFE	kA	RFE
polský	polský	k2eAgMnSc1d1	polský
historik	historik	k1gMnSc1	historik
Andrzej	Andrzej	k1gMnSc1	Andrzej
Czechowicz	Czechowicz	k1gMnSc1	Czechowicz
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1971	[number]	k4	1971
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
před	před	k7c7	před
televizními	televizní	k2eAgFnPc7d1	televizní
kamerami	kamera	k1gFnPc7	kamera
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Polska	Polsko	k1gNnSc2	Polsko
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kapitána	kapitán	k1gMnSc4	kapitán
polské	polský	k2eAgFnSc2d1	polská
SB	sb	kA	sb
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
nasazen	nasadit	k5eAaPmNgMnS	nasadit
jako	jako	k9	jako
vyzvědač	vyzvědač	k1gMnSc1	vyzvědač
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
RFE	RFE	kA	RFE
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
RFE	RFE	kA	RFE
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
agentů	agent	k1gMnPc2	agent
československé	československý	k2eAgFnSc2d1	Československá
rozvědky	rozvědka	k1gFnSc2	rozvědka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
československý	československý	k2eAgMnSc1d1	československý
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Rudolf	Rudolf	k1gMnSc1	Rudolf
Barák	Barák	k1gMnSc1	Barák
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
neprozřetelně	prozřetelně	k6eNd1	prozřetelně
chlubil	chlubit	k5eAaImAgInS	chlubit
<g/>
,	,	kIx,	,
že	že	k8xS	že
...	...	k?	...
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
povídá	povídat	k5eAaImIp3nS	povídat
v	v	k7c6	v
kantýně	kantýna	k1gFnSc6	kantýna
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
provedla	provést	k5eAaPmAgFnS	provést
americká	americký	k2eAgFnSc1d1	americká
kontrašpionáž	kontrašpionáž	k1gFnSc1	kontrašpionáž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
sdělení	sdělení	k1gNnSc2	sdělení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
a	a	k8xC	a
několik	několik	k4yIc1	několik
československých	československý	k2eAgMnPc2d1	československý
agentů	agent	k1gMnPc2	agent
bylo	být	k5eAaImAgNnS	být
propuštěno	propustit	k5eAaPmNgNnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Teroristický	teroristický	k2eAgInSc1d1	teroristický
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
RFE	RFE	kA	RFE
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
proveden	provést	k5eAaPmNgInS	provést
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
informacím	informace	k1gFnPc3	informace
získaným	získaný	k2eAgMnSc7d1	získaný
z	z	k7c2	z
archivů	archiv	k1gInPc2	archiv
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
akci	akce	k1gFnSc4	akce
objednala	objednat	k5eAaPmAgFnS	objednat
rumunská	rumunský	k2eAgFnSc1d1	rumunská
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
Securitate	Securitat	k1gInSc5	Securitat
u	u	k7c2	u
Johannese	Johannese	k1gFnSc2	Johannese
Weinricha	Weinrich	k1gMnSc2	Weinrich
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc2	člen
teroristické	teroristický	k2eAgFnSc2d1	teroristická
skupiny	skupina	k1gFnSc2	skupina
I.	I.	kA	I.
R.	R.	kA	R.
Sáncheze	Sáncheze	k1gFnSc1	Sáncheze
(	(	kIx(	(
<g/>
Carlose	Carlosa	k1gFnSc6	Carlosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
celou	celá	k1gFnSc4	celá
akci	akce	k1gFnSc4	akce
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
a	a	k8xC	a
provedl	provést	k5eAaPmAgMnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnSc1	exploze
zdemolovala	zdemolovat	k5eAaPmAgFnS	zdemolovat
československou	československý	k2eAgFnSc4d1	Československá
redakci	redakce	k1gFnSc4	redakce
a	a	k8xC	a
zranila	zranit	k5eAaPmAgFnS	zranit
tři	tři	k4xCgInPc4	tři
její	její	k3xOp3gMnPc4	její
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dokumentů	dokument	k1gInPc2	dokument
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zásah	zásah	k1gInSc1	zásah
československé	československý	k2eAgFnSc2d1	Československá
redakce	redakce	k1gFnSc2	redakce
bylo	být	k5eAaImAgNnS	být
nedopatření	nedopatření	k1gNnSc1	nedopatření
<g/>
.	.	kIx.	.
</s>
<s>
Sáncheze	Sánchézt	k5eAaPmIp3nS	Sánchézt
se	se	k3xPyFc4	se
vypátrat	vypátrat	k5eAaPmF	vypátrat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sovětská	sovětský	k2eAgFnSc1d1	sovětská
státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
jméno	jméno	k1gNnSc4	jméno
pachatele	pachatel	k1gMnSc2	pachatel
znala	znát	k5eAaImAgFnS	znát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
hlasatel	hlasatel	k1gMnSc1	hlasatel
československé	československý	k2eAgFnSc2d1	Československá
sekce	sekce	k1gFnSc2	sekce
RFE	RFE	kA	RFE
Pavel	Pavel	k1gMnSc1	Pavel
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
RFE	RFE	kA	RFE
propuštěn	propustit	k5eAaPmNgInS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
televizi	televize	k1gFnSc6	televize
jako	jako	k9	jako
kapitán	kapitán	k1gMnSc1	kapitán
Pavel	Pavel	k1gMnSc1	Pavel
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
československá	československý	k2eAgFnSc1d1	Československá
StB	StB	k1gFnSc1	StB
svého	své	k1gNnSc2	své
"	"	kIx"	"
<g/>
Czechowicze	Czechowicze	k1gFnSc1	Czechowicze
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Minařík	Minařík	k1gMnSc1	Minařík
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
předložil	předložit	k5eAaPmAgMnS	předložit
svým	svůj	k3xOyFgMnPc3	svůj
nadřízeným	nadřízený	k1gMnPc3	nadřízený
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
centrále	centrála	k1gFnSc6	centrála
StB	StB	k1gFnSc6	StB
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgInPc4	tři
návrhy	návrh	k1gInPc4	návrh
na	na	k7c6	na
provedení	provedení	k1gNnSc6	provedení
pumového	pumový	k2eAgInSc2d1	pumový
atentátu	atentát	k1gInSc2	atentát
na	na	k7c6	na
čs	čs	kA	čs
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
RFE	RFE	kA	RFE
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
Minařík	Minařík	k1gMnSc1	Minařík
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
návrhy	návrh	k1gInPc7	návrh
na	na	k7c4	na
provedení	provedení	k1gNnSc4	provedení
atentátu	atentát	k1gInSc2	atentát
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
ale	ale	k8xC	ale
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1994	[number]	k4	1994
rozsudek	rozsudek	k1gInSc1	rozsudek
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
případ	případ	k1gInSc1	případ
vrátil	vrátit	k5eAaPmAgInS	vrátit
státnímu	státní	k2eAgMnSc3d1	státní
zástupci	zástupce	k1gMnSc3	zástupce
k	k	k7c3	k
došetření	došetření	k1gNnSc3	došetření
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pak	pak	k6eAd1	pak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1994	[number]	k4	1994
trestní	trestní	k2eAgNnSc4d1	trestní
stíhání	stíhání	k1gNnSc4	stíhání
zastavil	zastavit	k5eAaPmAgMnS	zastavit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Minařík	Minařík	k1gMnSc1	Minařík
sice	sice	k8xC	sice
nepochybně	pochybně	k6eNd1	pochybně
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
vyřazení	vyřazení	k1gNnSc4	vyřazení
vysílače	vysílač	k1gInSc2	vysílač
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
předkládal	předkládat	k5eAaImAgMnS	předkládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
jednání	jednání	k1gNnSc1	jednání
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
stádia	stádium	k1gNnSc2	stádium
přípravy	příprava	k1gFnSc2	příprava
k	k	k7c3	k
trestnému	trestný	k2eAgInSc3d1	trestný
činu	čin	k1gInSc3	čin
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Tibor	Tibor	k1gMnSc1	Tibor
Zlocha	zloch	k1gMnSc2	zloch
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
seznamech	seznam	k1gInPc6	seznam
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
I.	I.	kA	I.
správy	správa	k1gFnSc2	správa
StB	StB	k1gFnSc2	StB
(	(	kIx(	(
<g/>
reg.	reg.	k?	reg.
číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
<g/>
4043	[number]	k4	4043
<g/>
)	)	kIx)	)
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
StB	StB	k1gMnSc1	StB
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
V.	V.	kA	V.
odbor	odbor	k1gInSc1	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgMnS	být
Zlocha	zloch	k1gMnSc4	zloch
jako	jako	k9	jako
agent	agent	k1gMnSc1	agent
vyslán	vyslán	k2eAgMnSc1d1	vyslán
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podával	podávat	k5eAaImAgMnS	podávat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
tamních	tamní	k2eAgFnPc6d1	tamní
aktivitách	aktivita	k1gFnPc6	aktivita
českých	český	k2eAgFnPc2d1	Česká
emigrantských	emigrantský	k2eAgFnPc2d1	emigrantská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
na	na	k7c6	na
americkém	americký	k2eAgNnSc6d1	americké
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
ho	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
působení	působení	k1gNnSc2	působení
RFE	RFE	kA	RFE
k	k	k7c3	k
několika	několik	k4yIc2	několik
desítkám	desítka	k1gFnPc3	desítka
atentátů	atentát	k1gInPc2	atentát
<g/>
,	,	kIx,	,
únosů	únos	k1gInPc2	únos
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
zastrašení	zastrašení	k1gNnSc4	zastrašení
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
i	i	k8xC	i
externích	externí	k2eAgMnPc2d1	externí
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
této	tento	k3xDgFnSc2	tento
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
východoevropských	východoevropský	k2eAgFnPc2d1	východoevropská
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
"	"	kIx"	"
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
skončila	skončit	k5eAaPmAgFnS	skončit
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
svojí	svojit	k5eAaImIp3nS	svojit
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
vysílat	vysílat	k5eAaImF	vysílat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
původní	původní	k2eAgFnSc1d1	původní
česká	český	k2eAgFnSc1d1	Česká
redakce	redakce	k1gFnSc1	redakce
RFE	RFE	kA	RFE
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
Českému	český	k2eAgInSc3d1	český
rozhlasu	rozhlas	k1gInSc3	rozhlas
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
"	"	kIx"	"
<g/>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
6	[number]	k4	6
/	/	kIx~	/
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
ČRo	ČRo	k1gFnSc2	ČRo
6	[number]	k4	6
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
prezidentem	prezident	k1gMnSc7	prezident
RFE	RFE	kA	RFE
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
Mgr.	Mgr.	kA	Mgr.
Pavel	Pavel	k1gMnSc1	Pavel
Pecháček	Pecháček	k1gMnSc1	Pecháček
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pecháčka	Pecháček	k1gMnSc2	Pecháček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
transakci	transakce	k1gFnSc6	transakce
spolupracovaly	spolupracovat	k5eAaImAgInP	spolupracovat
rovněž	rovněž	k6eAd1	rovněž
české	český	k2eAgFnSc2d1	Česká
redakce	redakce	k1gFnSc2	redakce
vysílačů	vysílač	k1gInPc2	vysílač
"	"	kIx"	"
<g/>
BBC	BBC	kA	BBC
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Voice	Voice	k1gFnSc1	Voice
of	of	k?	of
America	America	k1gFnSc1	America
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Welle	Welle	k1gFnSc2	Welle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
ředitelem	ředitel	k1gMnSc7	ředitel
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Ing.	ing.	kA	ing.
Ivo	Ivo	k1gMnSc1	Ivo
Štern	Štern	k1gMnSc1	Štern
<g/>
.	.	kIx.	.
</s>
<s>
Úřadující	úřadující	k2eAgFnSc7d1	úřadující
prezidentkou	prezidentka	k1gFnSc7	prezidentka
-	-	kIx~	-
po	po	k7c4	po
odstoupení	odstoupení	k1gNnSc4	odstoupení
Mgr.	Mgr.	kA	Mgr.
Pavla	Pavel	k1gMnSc2	Pavel
Pecháčka	Pecháček	k1gMnSc2	Pecháček
-	-	kIx~	-
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Olga	Olga	k1gFnSc1	Olga
Valeská	Valeský	k2eAgFnSc1d1	Valeská
<g/>
.	.	kIx.	.
</s>
<s>
RFE	RFE	kA	RFE
/	/	kIx~	/
RL	RL	kA	RL
dnes	dnes	k6eAd1	dnes
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
28	[number]	k4	28
jazycích	jazyk	k1gInPc6	jazyk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
přes	přes	k7c4	přes
1000	[number]	k4	1000
hodin	hodina	k1gFnPc2	hodina
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Victor	Victor	k1gMnSc1	Victor
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
Gedmin	Gedmin	k1gInSc1	Gedmin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
a	a	k8xC	a
do	do	k7c2	do
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
Aspen	Aspna	k1gFnPc2	Aspna
Institutu	institut	k1gInSc2	institut
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Kent	Kent	k1gMnSc1	Kent
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
opoziční	opoziční	k2eAgMnSc1d1	opoziční
novinář	novinář	k1gMnSc1	novinář
Andrej	Andrej	k1gMnSc1	Andrej
Babickij	Babickij	k1gMnSc1	Babickij
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
práva	právo	k1gNnSc2	právo
Čečenců	Čečenec	k1gInPc2	Čečenec
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
pracovní	pracovní	k2eAgFnSc1d1	pracovní
pozice	pozice	k1gFnSc1	pozice
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
některých	některý	k3yIgMnPc2	některý
novinářů	novinář	k1gMnPc2	novinář
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
obhajobu	obhajoba	k1gFnSc4	obhajoba
proruského	proruský	k2eAgInSc2d1	proruský
separatismu	separatismus	k1gInSc2	separatismus
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
anexe	anexe	k1gFnSc2	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2002	[number]	k4	2002
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
americký	americký	k2eAgInSc1d1	americký
Kongres	kongres	k1gInSc1	kongres
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
financování	financování	k1gNnSc2	financování
českého	český	k2eAgNnSc2d1	české
vysílání	vysílání	k1gNnSc2	vysílání
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2002	[number]	k4	2002
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Neformálním	formální	k2eNgMnSc7d1	neformální
nástupcem	nástupce	k1gMnSc7	nástupce
českého	český	k2eAgNnSc2d1	české
vysílání	vysílání	k1gNnSc2	vysílání
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
stanice	stanice	k1gFnSc1	stanice
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
ČRo	ČRo	k1gFnSc2	ČRo
6	[number]	k4	6
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přešla	přejít	k5eAaPmAgFnS	přejít
i	i	k9	i
část	část	k1gFnSc1	část
redaktorů	redaktor	k1gMnPc2	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
bezpečnostním	bezpečnostní	k2eAgNnPc3d1	bezpečnostní
rizikům	riziko	k1gNnPc3	riziko
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
možnosti	možnost	k1gFnPc1	možnost
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
několikrát	několikrát	k6eAd1	několikrát
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
sídlo	sídlo	k1gNnSc4	sídlo
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
navržených	navržený	k2eAgFnPc2d1	navržená
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
proto	proto	k8xC	proto
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
pro	pro	k7c4	pro
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
stanici	stanice	k1gFnSc4	stanice
v	v	k7c6	v
Praze-Strašnicích	Praze-Strašnice	k1gFnPc6	Praze-Strašnice
na	na	k7c6	na
Hagiboru	Hagibor	k1gInSc6	Hagibor
speciální	speciální	k2eAgFnSc1d1	speciální
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
;	;	kIx,	;
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
budově	budova	k1gFnSc6	budova
a	a	k8xC	a
vysílání	vysílání	k1gNnSc2	vysílání
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
/	/	kIx~	/
<g/>
Rádio	rádio	k1gNnSc1	rádio
Svoboda	svoboda	k1gFnSc1	svoboda
na	na	k7c6	na
Hagiboru	Hagibor	k1gInSc6	Hagibor
v	v	k7c6	v
Praze-Strašnicích	Praze-Strašnice	k1gFnPc6	Praze-Strašnice
odhalen	odhalen	k2eAgInSc4d1	odhalen
památník	památník	k1gInSc4	památník
připomínající	připomínající	k2eAgInSc4d1	připomínající
židovskou	židovský	k2eAgFnSc4d1	židovská
historii	historie	k1gFnSc4	historie
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
odhalení	odhalení	k1gNnSc6	odhalení
pomníku	pomník	k1gInSc2	pomník
promluvili	promluvit	k5eAaPmAgMnP	promluvit
prezident	prezident	k1gMnSc1	prezident
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
Steve	Steve	k1gMnSc1	Steve
Korn	Korn	k1gMnSc1	Korn
<g/>
,	,	kIx,	,
pamětnice	pamětnice	k1gFnSc1	pamětnice
holocaustu	holocaust	k1gInSc2	holocaust
PhDr.	PhDr.	kA	PhDr.
Dagmar	Dagmar	k1gFnSc1	Dagmar
Lieblová	Lieblová	k1gFnSc1	Lieblová
z	z	k7c2	z
Terezínské	terezínský	k2eAgFnSc2d1	Terezínská
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Pražské	pražský	k2eAgFnSc2d1	Pražská
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
František	František	k1gMnSc1	František
Banyai	Banya	k1gFnSc2	Banya
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
USA	USA	kA	USA
v	v	k7c6	v
ČR	ČR	kA	ČR
Normana	Norman	k1gMnSc2	Norman
Eisena	Eisen	k1gMnSc2	Eisen
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Lindsay	Lindsaa	k1gMnSc2	Lindsaa
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
<g/>
.	.	kIx.	.
</s>
<s>
Czechowicz	Czechowicz	k1gMnSc1	Czechowicz
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sedm	sedm	k4xCc1	sedm
těžkých	těžký	k2eAgNnPc2d1	těžké
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Janovský-Drážďanský	Janovský-Drážďanský	k2eAgMnSc1d1	Janovský-Drážďanský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85776	[number]	k4	85776
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Vimperk	Vimperk	k1gInSc1	Vimperk
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc2	KMa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cibulkovy	Cibulkův	k2eAgInPc1d1	Cibulkův
seznamy	seznam	k1gInPc1	seznam
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
StB	StB	k1gFnSc2	StB
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
391	[number]	k4	391
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kapitán	kapitán	k1gMnSc1	kapitán
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Pejskar	Pejskar	k1gMnSc1	Pejskar
<g/>
,	,	kIx,	,
Jožka	Jožka	k1gMnSc1	Jožka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Poslední	poslední	k2eAgFnSc1d1	poslední
pocta	pocta	k1gFnSc1	pocta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
díly	díl	k1gInPc1	díl
I	I	kA	I
-	-	kIx~	-
III	III	kA	III
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Volá	volat	k5eAaImIp3nS	volat
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901117	[number]	k4	901117
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
BBC	BBC	kA	BBC
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
RFE	RFE	kA	RFE
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
6	[number]	k4	6
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
ČRo	ČRo	k1gFnPc1	ČRo
6	[number]	k4	6
-	-	kIx~	-
RSE	RSE	kA	RSE
<g/>
)	)	kIx)	)
Před	před	k7c7	před
60	[number]	k4	60
lety	léto	k1gNnPc7	léto
zaznělo	zaznět	k5eAaImAgNnS	zaznět
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgNnSc1	první
zkušební	zkušební	k2eAgNnSc1d1	zkušební
vysílání	vysílání	k1gNnSc1	vysílání
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
České	český	k2eAgFnSc2d1	Česká
noviny	novina	k1gFnSc2	novina
3.7	[number]	k4	3.7
<g/>
.2010	.2010	k4	.2010
<g/>
)	)	kIx)	)
Nově	nově	k6eAd1	nově
odhalený	odhalený	k2eAgInSc1d1	odhalený
památník	památník	k1gInSc1	památník
před	před	k7c7	před
RFE	RFE	kA	RFE
<g/>
/	/	kIx~	/
<g/>
RL	RL	kA	RL
připomíná	připomínat	k5eAaImIp3nS	připomínat
židovskou	židovský	k2eAgFnSc4d1	židovská
historii	historie	k1gFnSc4	historie
Hagiboru	Hagibor	k1gInSc2	Hagibor
(	(	kIx(	(
<g/>
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
29	[number]	k4	29
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
o	o	k7c4	o
Alexandru	Alexandra	k1gFnSc4	Alexandra
Heidlerovi	Heidlerův	k2eAgMnPc1d1	Heidlerův
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
faráři	farář	k1gMnSc3	farář
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ukázky	ukázka	k1gFnPc4	ukázka
z	z	k7c2	z
pořadů	pořad	k1gInPc2	pořad
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Heidlerem	Heidler	k1gMnSc7	Heidler
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
o	o	k7c6	o
legendární	legendární	k2eAgFnSc6d1	legendární
diskžokejce	diskžokejka	k1gFnSc6	diskžokejka
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
Rozině	Rozina	k1gFnSc3	Rozina
Jadrné-Pokorné	Jadrné-Pokorný	k2eAgFnSc6d1	Jadrné-Pokorný
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
ukázek	ukázka	k1gFnPc2	ukázka
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
Rub	rub	k1gInSc1	rub
a	a	k8xC	a
líc	líc	k1gInSc1	líc
nejen	nejen	k6eAd1	nejen
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
je	být	k5eAaImIp3nS	být
věnován	věnován	k2eAgInSc1d1	věnován
osobnosti	osobnost	k1gFnSc3	osobnost
redaktora	redaktor	k1gMnSc2	redaktor
Josefa	Josef	k1gMnSc2	Josef
Pejskara	Pejskar	k1gMnSc2	Pejskar
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
Svobodnou	svobodný	k2eAgFnSc4d1	svobodná
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
Z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
slavné	slavný	k2eAgFnSc2d1	slavná
přestavby	přestavba	k1gFnSc2	přestavba
zbyl	zbýt	k5eAaPmAgInS	zbýt
nedostatek	nedostatek	k1gInSc1	nedostatek
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
