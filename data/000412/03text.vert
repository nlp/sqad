<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
STR	str	kA	str
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
r.	r.	kA	r.
1905	[number]	k4	1905
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
Newtonovy	Newtonův	k2eAgFnPc4d1	Newtonova
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
teorii	teorie	k1gFnSc4	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
reprezentovanou	reprezentovaný	k2eAgFnSc4d1	reprezentovaná
Maxwellovými	Maxwellův	k2eAgFnPc7d1	Maxwellova
rovnicemi	rovnice	k1gFnPc7	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
speciální	speciální	k2eAgFnSc1d1	speciální
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
popisuje	popisovat	k5eAaImIp3nS	popisovat
pouze	pouze	k6eAd1	pouze
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
případ	případ	k1gInSc1	případ
Einsteinova	Einsteinův	k2eAgInSc2d1	Einsteinův
principu	princip	k1gInSc2	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vliv	vliv	k1gInSc4	vliv
gravitace	gravitace	k1gFnSc2	gravitace
lze	lze	k6eAd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Einstein	Einstein	k1gMnSc1	Einstein
publikoval	publikovat	k5eAaBmAgMnS	publikovat
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
zavedl	zavést	k5eAaPmAgInS	zavést
už	už	k6eAd1	už
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Překonal	překonat	k5eAaPmAgInS	překonat
starý	starý	k2eAgInSc1d1	starý
absolutní	absolutní	k2eAgInSc1d1	absolutní
pohled	pohled	k1gInSc1	pohled
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jiné	jiný	k2eAgMnPc4d1	jiný
rovnoměrným	rovnoměrný	k2eAgInSc7d1	rovnoměrný
přímočarým	přímočarý	k2eAgInSc7d1	přímočarý
pohybem	pohyb	k1gInSc7	pohyb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rovnocenná	rovnocenný	k2eAgFnSc1d1	rovnocenná
(	(	kIx(	(
<g/>
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
-	-	kIx~	-
relativní	relativní	k2eAgInSc1d1	relativní
<g/>
)	)	kIx)	)
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
tedy	tedy	k9	tedy
žádná	žádný	k3yNgFnSc1	žádný
absolutní	absolutní	k2eAgFnSc1d1	absolutní
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
jedinou	jediný	k2eAgFnSc7d1	jediná
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
poměřovány	poměřován	k2eAgFnPc4d1	poměřována
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
také	také	k9	také
sadu	sada	k1gFnSc4	sada
transformací	transformace	k1gFnPc2	transformace
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
Galileovy	Galileův	k2eAgFnPc4d1	Galileova
transformace	transformace	k1gFnPc4	transformace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
stanovil	stanovit	k5eAaPmAgInS	stanovit
5	[number]	k4	5
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Newton	Newton	k1gMnSc1	Newton
konstruoval	konstruovat	k5eAaImAgMnS	konstruovat
svou	svůj	k3xOyFgFnSc4	svůj
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
Galileiho	Galilei	k1gMnSc2	Galilei
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
zredukoval	zredukovat	k5eAaPmAgInS	zredukovat
počet	počet	k1gInSc1	počet
základních	základní	k2eAgInPc2d1	základní
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Newtonova	Newtonův	k2eAgFnSc1d1	Newtonova
klasická	klasický	k2eAgFnSc1d1	klasická
mechanika	mechanika	k1gFnSc1	mechanika
funguje	fungovat	k5eAaImIp3nS	fungovat
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
jevy	jev	k1gInPc4	jev
včetně	včetně	k7c2	včetně
pevných	pevný	k2eAgNnPc2d1	pevné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
částicovou	částicový	k2eAgFnSc4d1	částicová
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
fyzikové	fyzik	k1gMnPc1	fyzik
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
model	model	k1gInSc1	model
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xC	jako
příčného	příčný	k2eAgNnSc2d1	příčné
vlnění	vlnění	k1gNnSc2	vlnění
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gFnPc4	jeho
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgNnSc1d1	mechanické
vlnění	vlnění	k1gNnSc1	vlnění
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
v	v	k7c6	v
médiu	médium	k1gNnSc6	médium
<g/>
,	,	kIx,	,
a	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
bylo	být	k5eAaImAgNnS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
pro	pro	k7c4	pro
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hypotetické	hypotetický	k2eAgNnSc1d1	hypotetické
médium	médium	k1gNnSc1	médium
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaPmNgNnS	nazvat
"	"	kIx"	"
<g/>
světlonosný	světlonosný	k2eAgInSc1d1	světlonosný
ether	ether	k1gInSc1	ether
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
některé	některý	k3yIgInPc4	některý
vzájemně	vzájemně	k6eAd1	vzájemně
neslučitelné	slučitelný	k2eNgFnPc1d1	neslučitelná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
být	být	k5eAaImF	být
extrémně	extrémně	k6eAd1	extrémně
tuhý	tuhý	k2eAgInSc4d1	tuhý
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
téměř	téměř	k6eAd1	téměř
nehmotný	hmotný	k2eNgMnSc1d1	nehmotný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezpomaloval	zpomalovat	k5eNaImAgMnS	zpomalovat
Zemi	zem	k1gFnSc4	zem
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
pohybu	pohyb	k1gInSc6	pohyb
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
etheru	ether	k1gInSc2	ether
vzkřísila	vzkřísit	k5eAaPmAgFnS	vzkřísit
myšlenku	myšlenka	k1gFnSc4	myšlenka
absolutní	absolutní	k2eAgFnSc2d1	absolutní
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
etheru	ether	k1gInSc3	ether
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
magnetismus	magnetismus	k1gInSc1	magnetismus
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
různé	různý	k2eAgInPc4d1	různý
aspekty	aspekt	k1gInPc4	aspekt
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
ukazovaly	ukazovat	k5eAaImAgFnP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
urychlovaným	urychlovaný	k2eAgInSc7d1	urychlovaný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
šíří	šířit	k5eAaImIp3nS	šířit
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
analogické	analogický	k2eAgInPc1d1	analogický
klasickému	klasický	k2eAgNnSc3d1	klasické
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
vlnění	vlnění	k1gNnSc3	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
rychlost	rychlost	k1gFnSc4	rychlost
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
využít	využít	k5eAaPmF	využít
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
ke	k	k7c3	k
změření	změření	k1gNnSc3	změření
rychlosti	rychlost	k1gFnSc2	rychlost
Země	zem	k1gFnSc2	zem
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
etheru	ether	k1gInSc3	ether
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
pokusů	pokus	k1gInPc2	pokus
byl	být	k5eAaImAgInS	být
Michelson-Morleyův	Michelson-Morleyův	k2eAgInSc1d1	Michelson-Morleyův
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
řady	řada	k1gFnSc2	řada
těchto	tento	k3xDgInPc2	tento
experimentů	experiment	k1gInPc2	experiment
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
nezávisí	záviset	k5eNaImIp3nS	záviset
ani	ani	k8xC	ani
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neměnná	měnný	k2eNgFnSc1d1	neměnná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
si	se	k3xPyFc3	se
Hendrik	Hendrik	k1gMnSc1	Hendrik
Lorentz	Lorentz	k1gMnSc1	Lorentz
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
povšimli	povšimnout	k5eAaPmAgMnP	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jeden	jeden	k4xCgMnSc1	jeden
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
nemusel	muset	k5eNaImAgMnS	muset
pozorovat	pozorovat	k5eAaImF	pozorovat
žádné	žádný	k3yNgNnSc4	žádný
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiný	jiný	k2eAgMnSc1d1	jiný
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Lorentz	Lorentz	k1gMnSc1	Lorentz
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
teorii	teorie	k1gFnSc4	teorie
etheru	ether	k1gInSc2	ether
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
pohybující	pohybující	k2eAgMnPc1d1	pohybující
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nehybnému	hybný	k2eNgInSc3d1	nehybný
etheru	ether	k1gInSc3	ether
podléhají	podléhat	k5eAaImIp3nP	podléhat
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
zkracování	zkracování	k1gNnSc3	zkracování
(	(	kIx(	(
<g/>
Lorentz-Fitzgeraldově	Lorentz-Fitzgeraldův	k2eAgFnSc3d1	Lorentz-Fitzgeraldův
kontrakci	kontrakce	k1gFnSc3	kontrakce
<g/>
)	)	kIx)	)
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
rychlosti	rychlost	k1gFnSc3	rychlost
plynutí	plynutí	k1gNnSc2	plynutí
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
dilatace	dilatace	k1gFnSc1	dilatace
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sladit	sladit	k5eAaPmF	sladit
teorii	teorie	k1gFnSc4	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc4d1	klasická
Newtonovu	Newtonův	k2eAgFnSc4d1	Newtonova
fyziku	fyzika	k1gFnSc4	fyzika
prostým	prostý	k2eAgNnSc7d1	prosté
nahrazením	nahrazení	k1gNnSc7	nahrazení
Galileiho	Galilei	k1gMnSc2	Galilei
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
rychlostmi	rychlost	k1gFnPc7	rychlost
mnohem	mnohem	k6eAd1	mnohem
menšími	malý	k2eAgFnPc7d2	menší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
efekty	efekt	k1gInPc4	efekt
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
zanedbat	zanedbat	k5eAaPmF	zanedbat
a	a	k8xC	a
výsledné	výsledný	k2eAgInPc4d1	výsledný
zákony	zákon	k1gInPc4	zákon
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
do	do	k7c2	do
Galileiho	Galilei	k1gMnSc2	Galilei
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Lorenzova	Lorenzův	k2eAgFnSc1d1	Lorenzova
teorie	teorie	k1gFnSc1	teorie
etheru	ether	k1gInSc2	ether
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
,	,	kIx,	,
i	i	k9	i
Lorentzem	Lorentz	k1gMnSc7	Lorentz
samotným	samotný	k2eAgMnSc7d1	samotný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
podstatu	podstata	k1gFnSc4	podstata
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Lorentz	Lorentz	k1gMnSc1	Lorentz
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
rovnice	rovnice	k1gFnPc4	rovnice
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
,	,	kIx,	,
Einsteinovým	Einsteinův	k2eAgInSc7d1	Einsteinův
přínosem	přínos	k1gInSc7	přínos
bylo	být	k5eAaImAgNnS	být
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
a	a	k8xC	a
odvození	odvození	k1gNnSc1	odvození
těchto	tento	k3xDgFnPc2	tento
rovnic	rovnice	k1gFnPc2	rovnice
z	z	k7c2	z
principiální	principiální	k2eAgFnSc2d1	principiální
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepožaduje	požadovat	k5eNaImIp3nS	požadovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
etheru	ether	k1gInSc2	ether
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
chtěl	chtít	k5eAaImAgMnS	chtít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
neměnné	měnný	k2eNgNnSc1d1	neměnné
(	(	kIx(	(
<g/>
invariantní	invariantní	k2eAgInPc1d1	invariantní
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
zdánlivě	zdánlivě	k6eAd1	zdánlivě
složité	složitý	k2eAgFnPc1d1	složitá
Lorentz-Fitzgeraldovy	Lorentz-Fitzgeraldův	k2eAgFnPc1d1	Lorentz-Fitzgeraldův
transformace	transformace	k1gFnPc1	transformace
čistě	čistě	k6eAd1	čistě
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
z	z	k7c2	z
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
invariantů	invariant	k1gInPc2	invariant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
relativita	relativita	k1gFnSc1	relativita
<g/>
"	"	kIx"	"
lépe	dobře	k6eAd2	dobře
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
představu	představa	k1gFnSc4	představa
transformace	transformace	k1gFnSc2	transformace
zákonů	zákon	k1gInPc2	zákon
fyziky	fyzika	k1gFnSc2	fyzika
mezi	mezi	k7c7	mezi
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
vzájemně	vzájemně	k6eAd1	vzájemně
pohybujícími	pohybující	k2eAgMnPc7d1	pohybující
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zabývá	zabývat	k5eAaImIp3nS	zabývat
chováním	chování	k1gNnSc7	chování
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
a	a	k8xC	a
časy	čas	k1gInPc1	čas
událostí	událost	k1gFnPc2	událost
zaznamenané	zaznamenaný	k2eAgFnPc4d1	zaznamenaná
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
lze	lze	k6eAd1	lze
porovnat	porovnat	k5eAaPmF	porovnat
pomocí	pomocí	k7c2	pomocí
rovnic	rovnice	k1gFnPc2	rovnice
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
STR	str	kA	str
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
pohyb	pohyb	k1gInSc1	pohyb
není	být	k5eNaImIp3nS	být
rovnoměrný	rovnoměrný	k2eAgInSc1d1	rovnoměrný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
problém	problém	k1gInSc1	problém
paradoxu	paradox	k1gInSc2	paradox
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
STR	str	kA	str
správně	správně	k6eAd1	správně
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
chování	chování	k1gNnSc1	chování
zrychlovaných	zrychlovaný	k2eAgNnPc2d1	zrychlované
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
konstantního	konstantní	k2eAgNnSc2d1	konstantní
nebo	nebo	k8xC	nebo
nulového	nulový	k2eAgNnSc2d1	nulové
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
nebo	nebo	k8xC	nebo
těch	ten	k3xDgFnPc2	ten
v	v	k7c6	v
rotující	rotující	k2eAgFnSc6d1	rotující
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
přesně	přesně	k6eAd1	přesně
popsat	popsat	k5eAaPmF	popsat
takový	takový	k3xDgInSc4	takový
pohyb	pohyb	k1gInSc4	pohyb
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
gravitačních	gravitační	k2eAgNnPc6d1	gravitační
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
gravitačním	gravitační	k2eAgInSc7d1	gravitační
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
)	)	kIx)	)
Pozorování	pozorování	k1gNnSc2	pozorování
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
jevu	jev	k1gInSc2	jev
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgMnSc7	jeden
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
musí	muset	k5eAaImIp3nP	muset
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
jednotně	jednotně	k6eAd1	jednotně
odpovídat	odpovídat	k5eAaImF	odpovídat
povaze	povaha	k1gFnSc3	povaha
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	řečen	k2eAgNnSc1d1	řečeno
-	-	kIx~	-
povaha	povaha	k1gFnSc1	povaha
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
<g/>
-li	i	k?	-li
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
vyjádření	vyjádření	k1gNnSc1	vyjádření
<g/>
:	:	kIx,	:
Žádným	žádný	k3yNgInSc7	žádný
pokusem	pokus	k1gInSc7	pokus
nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rovnoměrným	rovnoměrný	k2eAgInSc7d1	rovnoměrný
přímočarým	přímočarý	k2eAgInSc7d1	přímočarý
pohybem	pohyb	k1gInSc7	pohyb
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgNnSc1d1	matematické
vyjádření	vyjádření	k1gNnSc1	vyjádření
libovolné	libovolný	k2eAgFnSc2d1	libovolná
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
stejné	stejný	k2eAgFnSc6d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Zkráceně	zkráceně	k6eAd1	zkráceně
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
probíhají	probíhat	k5eAaImIp3nP	probíhat
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
děje	děj	k1gInPc1	děj
stejně	stejně	k6eAd1	stejně
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
stejné	stejný	k2eAgInPc4d1	stejný
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
zákony	zákon	k1gInPc4	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
neměnnost	neměnnost	k1gFnSc1	neměnnost
c	c	k0	c
<g/>
)	)	kIx)	)
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
značená	značený	k2eAgFnSc1d1	značená
c	c	k0	c
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
stejná	stejný	k2eAgFnSc1d1	stejná
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
a	a	k8xC	a
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
objektu	objekt	k1gInSc2	objekt
vyzařujícího	vyzařující	k2eAgInSc2d1	vyzařující
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Zkráceně	zkráceně	k6eAd1	zkráceně
<g/>
:	:	kIx,	:
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledně	důsledně	k6eAd1	důsledně
matematické	matematický	k2eAgFnSc3d1	matematická
formulaci	formulace	k1gFnSc3	formulace
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnPc4	relativita
předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
čtyř-dimenzionálním	čtyřimenzionální	k2eAgInSc6d1	čtyř-dimenzionální
prostoročasu	prostoročas	k1gInSc6	prostoročas
M.	M.	kA	M.
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
body	bod	k1gInPc4	bod
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
jsou	být	k5eAaImIp3nP	být
událostmi	událost	k1gFnPc7	událost
<g/>
;	;	kIx,	;
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
popíšeme	popsat	k5eAaPmIp1nP	popsat
jako	jako	k9	jako
světočáry	světočára	k1gFnPc1	světočára
(	(	kIx(	(
<g/>
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
objekt	objekt	k1gInSc4	objekt
jako	jako	k8xS	jako
bodový	bodový	k2eAgInSc4d1	bodový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Světočáry	světočára	k1gFnPc1	světočára
popisují	popisovat	k5eAaImIp3nP	popisovat
pouze	pouze	k6eAd1	pouze
pohyb	pohyb	k1gInSc4	pohyb
objektu	objekt	k1gInSc2	objekt
<g/>
;	;	kIx,	;
objekt	objekt	k1gInSc1	objekt
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
jiné	jiný	k2eAgFnPc4d1	jiná
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
charakteristiky	charakteristika	k1gFnPc4	charakteristika
jako	jako	k8xC	jako
energie	energie	k1gFnPc4	energie
<g/>
,	,	kIx,	,
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Kromě	kromě	k7c2	kromě
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
objektů	objekt	k1gInPc2	objekt
mějme	mít	k5eAaImRp1nP	mít
navíc	navíc	k6eAd1	navíc
třídu	třída	k1gFnSc4	třída
inerciálních	inerciální	k2eAgMnPc2d1	inerciální
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
(	(	kIx(	(
<g/>
kterým	který	k3yQgInPc3	který
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
objektů	objekt	k1gInPc2	objekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
inerciální	inerciální	k2eAgMnSc1d1	inerciální
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgMnSc1d1	spojen
s	s	k7c7	s
inerciální	inerciální	k2eAgFnSc7d1	inerciální
vztažnou	vztažný	k2eAgFnSc7d1	vztažná
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
souřadnicový	souřadnicový	k2eAgInSc4d1	souřadnicový
systém	systém	k1gInSc4	systém
se	s	k7c7	s
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
události	událost	k1gFnPc4	událost
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
M.	M.	kA	M.
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
souřadnice	souřadnice	k1gFnPc4	souřadnice
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
objektu	objekt	k1gInSc2	objekt
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
například	například	k6eAd1	například
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
souřadnice	souřadnice	k1gFnPc4	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
E	E	kA	E
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
hybnost	hybnost	k1gFnSc4	hybnost
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
E_	E_	k1gMnSc6	E_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
B_	B_	k1gFnSc1	B_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
ap.	ap.	kA	ap.
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
jakékoliv	jakýkoliv	k3yIgMnPc4	jakýkoliv
dva	dva	k4xCgMnPc4	dva
inerciální	inerciální	k2eAgMnPc4d1	inerciální
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
transformace	transformace	k1gFnSc1	transformace
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
souřadnice	souřadnice	k1gFnPc4	souřadnice
ze	z	k7c2	z
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
jednoho	jeden	k4xCgMnSc4	jeden
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
do	do	k7c2	do
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
druhého	druhý	k4xOgMnSc4	druhý
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
transformace	transformace	k1gFnSc1	transformace
nestanovuje	stanovovat	k5eNaImIp3nS	stanovovat
pouze	pouze	k6eAd1	pouze
převod	převod	k1gInSc4	převod
prostoročasových	prostoročasový	k2eAgFnPc2d1	prostoročasová
souřadnic	souřadnice	k1gFnPc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
také	také	k9	také
převod	převod	k1gInSc1	převod
ostatních	ostatní	k2eAgFnPc2d1	ostatní
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
pravidla	pravidlo	k1gNnPc4	pravidlo
převodu	převod	k1gInSc2	převod
pro	pro	k7c4	pro
hybnost	hybnost	k1gFnSc4	hybnost
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
p	p	k?	p
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
E	E	kA	E
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
atd.	atd.	kA	atd.
(	(	kIx(	(
<g/>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
lze	lze	k6eAd1	lze
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
převodními	převodní	k2eAgNnPc7d1	převodní
pravidly	pravidlo	k1gNnPc7	pravidlo
efektivně	efektivně	k6eAd1	efektivně
pracovat	pracovat	k5eAaImF	pracovat
pomocí	pomocí	k7c2	pomocí
matematiky	matematika	k1gFnSc2	matematika
tenzorů	tenzor	k1gInPc2	tenzor
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
množstvím	množství	k1gNnSc7	množství
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
lze	lze	k6eAd1	lze
každý	každý	k3xTgInSc1	každý
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
zákon	zákon	k1gInSc1	zákon
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
souřadnicím	souřadnice	k1gFnPc3	souřadnice
některé	některý	k3yIgFnSc2	některý
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
rovnicí	rovnice	k1gFnSc7	rovnice
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
diferenciální	diferenciální	k2eAgFnSc7d1	diferenciální
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
různých	různý	k2eAgFnPc2d1	různá
souřadnic	souřadnice	k1gFnPc2	souřadnice
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
příklady	příklad	k1gInPc7	příklad
jsou	být	k5eAaImIp3nP	být
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
nebo	nebo	k8xC	nebo
Newtonovy	Newtonův	k2eAgInPc1d1	Newtonův
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
Princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
)	)	kIx)	)
Žádný	žádný	k3yNgInSc1	žádný
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
zákon	zákon	k1gInSc1	zákon
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
transformací	transformace	k1gFnSc7	transformace
souřadnic	souřadnice	k1gFnPc2	souřadnice
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pokud	pokud	k8xS	pokud
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
splňuje	splňovat	k5eAaImIp3nS	splňovat
matematické	matematický	k2eAgFnPc4d1	matematická
rovnice	rovnice	k1gFnPc4	rovnice
popisující	popisující	k2eAgInSc4d1	popisující
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
zákon	zákon	k1gInSc4	zákon
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nezbytně	zbytně	k6eNd1	zbytně
splněny	splnit	k5eAaPmNgFnP	splnit
tytéž	týž	k3xTgFnPc1	týž
rovnice	rovnice	k1gFnPc1	rovnice
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
v	v	k7c6	v
libovolné	libovolný	k2eAgFnSc6d1	libovolná
jiné	jiný	k2eAgFnSc6d1	jiná
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
neměnnost	neměnnost	k1gFnSc1	neměnnost
c	c	k0	c
<g/>
)	)	kIx)	)
Existuje	existovat	k5eAaImIp3nS	existovat
základní	základní	k2eAgFnSc1d1	základní
konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
<	<	kIx(	<
c	c	k0	c
<	<	kIx(	<
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
<	<	kIx(	<
<g/>
c	c	k0	c
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
s	s	k7c7	s
následující	následující	k2eAgFnSc7d1	následující
vlastností	vlastnost	k1gFnSc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
události	událost	k1gFnPc1	událost
mající	mající	k2eAgFnSc2d1	mající
souřadnice	souřadnice	k1gFnSc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
mající	mající	k2eAgFnPc4d1	mající
souřadnice	souřadnice	k1gFnPc4	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
c	c	k0	c
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
c	c	k0	c
<g/>
(	(	kIx(	(
<g/>
s-t	s	k?	s-t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
tehdy	tehdy	k6eAd1	tehdy
a	a	k8xC	a
jen	jen	k6eAd1	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
c	c	k0	c
(	(	kIx(	(
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-t	-t	k?	-t
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Neformálně	formálně	k6eNd1	formálně
řečeno	říct	k5eAaPmNgNnS	říct
-	-	kIx~	-
druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekty	objekt	k1gInPc4	objekt
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
c	c	k0	c
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
nezbytně	zbytně	k6eNd1	zbytně
nutně	nutně	k6eAd1	nutně
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
c	c	k0	c
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
lze	lze	k6eAd1	lze
matematicky	matematicky	k6eAd1	matematicky
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
postulátu	postulát	k1gInSc2	postulát
a	a	k8xC	a
Maxwellových	Maxwellův	k2eAgFnPc2d1	Maxwellova
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zároveň	zároveň	k6eAd1	zároveň
dostaneme	dostat	k5eAaPmIp1nP	dostat
vyjádření	vyjádření	k1gNnSc4	vyjádření
c	c	k0	c
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Maxwellovými	Maxwellův	k2eAgFnPc7d1	Maxwellova
rovnicemi	rovnice	k1gFnPc7	rovnice
řídí	řídit	k5eAaImIp3nS	řídit
šíření	šíření	k1gNnSc1	šíření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
běžně	běžně	k6eAd1	běžně
c	c	k0	c
jako	jako	k8xC	jako
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
povšimněme	povšimnout	k5eAaPmRp1nP	povšimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
formulace	formulace	k1gFnPc4	formulace
druhého	druhý	k4xOgInSc2	druhý
postulátu	postulát	k1gInSc2	postulát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
dána	dán	k2eAgFnSc1d1	dána
výše	výše	k1gFnSc1	výše
<g/>
,	,	kIx,	,
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
existenci	existence	k1gFnSc4	existence
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
ani	ani	k8xC	ani
Maxwellových	Maxwellův	k2eAgFnPc2d1	Maxwellova
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
postulátu	postulát	k1gInSc2	postulát
lze	lze	k6eAd1	lze
vyvodit	vyvodit	k5eAaPmF	vyvodit
jeho	jeho	k3xOp3gFnSc4	jeho
silnější	silný	k2eAgFnSc4d2	silnější
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
prostoročasový	prostoročasový	k2eAgInSc4d1	prostoročasový
interval	interval	k1gInSc4	interval
je	být	k5eAaImIp3nS	být
invariantní	invariantní	k2eAgInSc1d1	invariantní
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
notaci	notace	k1gFnSc6	notace
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
t	t	k?	t
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
s-t	s	k?	s-t
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-y_	-y_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-t	-t	k?	-t
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
pro	pro	k7c4	pro
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
dvě	dva	k4xCgFnPc4	dva
události	událost	k1gFnPc4	událost
A	A	kA	A
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
odvození	odvození	k1gNnSc3	odvození
transformačních	transformační	k2eAgInPc2d1	transformační
zákonů	zákon	k1gInPc2	zákon
mezi	mezi	k7c7	mezi
vztažnými	vztažný	k2eAgFnPc7d1	vztažná
soustavami	soustava	k1gFnPc7	soustava
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnPc1	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Postuláty	postulát	k1gInPc1	postulát
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
velmi	velmi	k6eAd1	velmi
úsporně	úsporně	k6eAd1	úsporně
užitím	užití	k1gNnSc7	užití
matematického	matematický	k2eAgInSc2d1	matematický
jazyka	jazyk	k1gInSc2	jazyk
pseudo-Riemanových	pseudo-Riemanův	k2eAgFnPc2d1	pseudo-Riemanův
variet	varieta	k1gFnPc2	varieta
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvrzením	tvrzení	k1gNnPc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtyř-dimenzionální	čtyřimenzionální	k2eAgInSc1d1	čtyř-dimenzionální
prostoročas	prostoročas	k1gInSc1	prostoročas
M	M	kA	M
je	být	k5eAaImIp3nS	být
pseudo-Riemanovou	pseudo-Riemanův	k2eAgFnSc7d1	pseudo-Riemanův
varietou	varieta	k1gFnSc7	varieta
vybavenou	vybavený	k2eAgFnSc7d1	vybavená
Lorentzovou	Lorentzový	k2eAgFnSc7d1	Lorentzový
metrikou	metrika	k1gFnSc7	metrika
g	g	kA	g
signatury	signatura	k1gFnSc2	signatura
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
3	[number]	k4	3
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
rovinnou	rovinný	k2eAgFnSc7d1	rovinná
Minkowského	Minkowského	k2eAgFnSc7d1	Minkowského
metrikou	metrika	k1gFnSc7	metrika
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
metriku	metrika	k1gFnSc4	metrika
je	být	k5eAaImIp3nS	být
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
jako	jako	k9	jako
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
změníme	změnit	k5eAaPmIp1nP	změnit
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
legitimně	legitimně	k6eAd1	legitimně
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
postulát	postulát	k1gInSc1	postulát
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákony	zákon	k1gInPc1	zákon
fyziky	fyzika	k1gFnSc2	fyzika
jsou	být	k5eAaImIp3nP	být
invariantní	invariantní	k2eAgInPc1d1	invariantní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
reprezentovány	reprezentovat	k5eAaImNgInP	reprezentovat
ve	v	k7c6	v
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
g	g	kA	g
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
Minkowského	Minkowského	k2eAgFnSc7d1	Minkowského
metrikou	metrika	k1gFnSc7	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
formulace	formulace	k1gFnSc2	formulace
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
porovnání	porovnání	k1gNnSc1	porovnání
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tytéž	týž	k3xTgInPc4	týž
dva	dva	k4xCgInPc4	dva
postuláty	postulát	k1gInPc4	postulát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vynechán	vynechán	k2eAgInSc4d1	vynechán
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
metrika	metrika	k1gFnSc1	metrika
byla	být	k5eAaImAgFnS	být
Minkowského	Minkowského	k2eAgFnSc7d1	Minkowského
metrikou	metrika	k1gFnSc7	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Galileiho	Galileize	k6eAd1	Galileize
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
limitním	limitní	k2eAgInSc7d1	limitní
případem	případ	k1gInSc7	případ
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
v	v	k7c6	v
nerelativistické	relativistický	k2eNgFnSc6d1	nerelativistická
limitě	limita	k1gFnSc6	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
první	první	k4xOgInSc1	první
postulát	postulát	k1gInSc1	postulát
nezměněn	změnit	k5eNaPmNgInS	změnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
události	událost	k1gFnPc1	událost
mající	mající	k2eAgFnSc2d1	mající
souřadnice	souřadnice	k1gFnSc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
souřadnice	souřadnice	k1gFnSc1	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
-	-	kIx~	-
t	t	k?	t
=	=	kIx~	=
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s-t	s	k?	s-t
<g/>
=	=	kIx~	=
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-t	-t	k?	-t
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
-	-	kIx~	-
t	t	k?	t
=	=	kIx~	=
:	:	kIx,	:
s	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s-t	s	k?	s-t
<g/>
=	=	kIx~	=
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-t	-t	k?	-t
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
teorie	teorie	k1gFnSc1	teorie
daná	daný	k2eAgFnSc1d1	daná
klasickou	klasický	k2eAgFnSc7d1	klasická
mechanikou	mechanika	k1gFnSc7	mechanika
a	a	k8xC	a
Newtonovou	Newtonův	k2eAgFnSc7d1	Newtonova
gravitační	gravitační	k2eAgFnSc7d1	gravitační
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Galileiho	Galilei	k1gMnSc2	Galilei
principem	princip	k1gInSc7	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
už	už	k6eAd1	už
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Obráceně	obráceně	k6eAd1	obráceně
<g/>
,	,	kIx,	,
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Galileiho	Galilei	k1gMnSc2	Galilei
principem	princip	k1gInSc7	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepředpokládáme	předpokládat	k5eNaImIp1nP	předpokládat
existenci	existence	k1gFnSc4	existence
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
etheru	ether	k1gInSc2	ether
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
překvapivém	překvapivý	k2eAgNnSc6d1	překvapivé
množství	množství	k1gNnSc6	množství
případů	případ	k1gInPc2	případ
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
známou	známý	k2eAgFnSc4d1	známá
rovnici	rovnice	k1gFnSc4	rovnice
E	E	kA	E
=	=	kIx~	=
mc2	mc2	k4	mc2
<g/>
)	)	kIx)	)
kombinací	kombinace	k1gFnPc2	kombinace
jejích	její	k3xOp3gInPc2	její
postulátů	postulát	k1gInPc2	postulát
s	s	k7c7	s
hypotézou	hypotéza	k1gFnSc7	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
v	v	k7c6	v
nerelativistické	relativistický	k2eNgFnSc6d1	nerelativistická
limitě	limita	k1gFnSc6	limita
jednoduše	jednoduše	k6eAd1	jednoduše
přejdou	přejít	k5eAaPmIp3nP	přejít
v	v	k7c4	v
zákony	zákon	k1gInPc4	zákon
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
gravitační	gravitační	k2eAgInPc1d1	gravitační
vlivy	vliv	k1gInPc1	vliv
zanedbatelné	zanedbatelný	k2eAgInPc1d1	zanedbatelný
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgNnSc1d1	slabé
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
obecnou	obecný	k2eAgFnSc7d1	obecná
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgInPc6d1	malý
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
,	,	kIx,	,
takových	takový	k3xDgFnPc2	takový
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
menších	malý	k2eAgMnPc2d2	menší
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
selhává	selhávat	k5eAaImIp3nS	selhávat
následkem	následkem	k7c2	následkem
efektů	efekt	k1gInPc2	efekt
kvantové	kvantový	k2eAgFnSc2d1	kvantová
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
makroskopických	makroskopický	k2eAgInPc6d1	makroskopický
rozměrech	rozměr	k1gInPc6	rozměr
a	a	k8xC	a
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
silných	silný	k2eAgFnPc2d1	silná
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
polí	pole	k1gFnPc2	pole
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
obecně	obecně	k6eAd1	obecně
přijímána	přijímat	k5eAaImNgFnS	přijímat
celou	celý	k2eAgFnSc7d1	celá
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
zdají	zdát	k5eAaImIp3nP	zdát
vyvracet	vyvracet	k5eAaImF	vyvracet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
nereprodukovatelným	reprodukovatelný	k2eNgInPc3d1	nereprodukovatelný
experimentálním	experimentální	k2eAgInPc3d1	experimentální
chybám	chyba	k1gFnPc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
je	být	k5eAaImIp3nS	být
věcí	věc	k1gFnSc7	věc
svobodné	svobodný	k2eAgFnSc2d1	svobodná
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
definovat	definovat	k5eAaBmF	definovat
jednotky	jednotka	k1gFnPc4	jednotka
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
učinit	učinit	k5eAaImF	učinit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
postulátů	postulát	k1gInPc2	postulát
relativity	relativita	k1gFnSc2	relativita
tautologickým	tautologický	k2eAgInSc7d1	tautologický
důsledkem	důsledek	k1gInSc7	důsledek
těchto	tento	k3xDgFnPc2	tento
definic	definice	k1gFnPc2	definice
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgMnSc4	ten
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
postuláty	postulát	k1gInPc4	postulát
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
kombinace	kombinace	k1gFnSc2	kombinace
plynou	plynout	k5eAaImIp3nP	plynout
důsledky	důsledek	k1gInPc4	důsledek
nezávislé	závislý	k2eNgInPc4d1	nezávislý
na	na	k7c6	na
výběru	výběr	k1gInSc6	výběr
definice	definice	k1gFnSc2	definice
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
definujeme	definovat	k5eAaBmIp1nP	definovat
jednotky	jednotka	k1gFnPc1	jednotka
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
času	čas	k1gInSc2	čas
pomocí	pomocí	k7c2	pomocí
vlastností	vlastnost	k1gFnPc2	vlastnost
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
třeba	třeba	k9	třeba
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
podle	podle	k7c2	podle
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
oscilace	oscilace	k1gFnSc2	oscilace
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
podle	podle	k7c2	podle
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
emisí	emise	k1gFnPc2	emise
atomu	atom	k1gInSc2	atom
kryptonu	krypton	k1gInSc2	krypton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
tautologickým	tautologický	k2eAgInSc7d1	tautologický
zákon	zákon	k1gInSc1	zákon
určující	určující	k2eAgInSc1d1	určující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotky	jednotka	k1gFnPc1	jednotka
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
neměnnost	neměnnost	k1gFnSc1	neměnnost
c	c	k0	c
není	být	k5eNaImIp3nS	být
triviální	triviální	k2eAgMnSc1d1	triviální
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
definujeme	definovat	k5eAaBmIp1nP	definovat
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
čas	čas	k1gInSc4	čas
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
c	c	k0	c
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
druhý	druhý	k4xOgInSc1	druhý
postulát	postulát	k1gInSc1	postulát
tautologickým	tautologický	k2eAgNnSc7d1	tautologické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgMnSc1	první
takový	takový	k3xDgMnSc1	takový
už	už	k9	už
není	být	k5eNaImIp3nS	být
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
určena	určit	k5eAaPmNgFnS	určit
pomocí	pomocí	k7c2	pomocí
jednotky	jednotka	k1gFnSc2	jednotka
času	čas	k1gInSc2	čas
a	a	k8xC	a
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
fixní	fixní	k2eAgFnSc2d1	fixní
hodnoty	hodnota	k1gFnSc2	hodnota
c	c	k0	c
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zde	zde	k6eAd1	zde
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
priori	priori	k6eAd1	priori
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
počet	počet	k1gInSc1	počet
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
kryptonu	krypton	k1gInSc2	krypton
v	v	k7c6	v
jednotce	jednotka	k1gFnSc6	jednotka
délky	délka	k1gFnSc2	délka
být	být	k5eAaImF	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
(	(	kIx(	(
<g/>
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
stejný	stejný	k2eAgInSc4d1	stejný
ani	ani	k8xC	ani
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgInPc4	některý
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
bizarní	bizarní	k2eAgInSc4d1	bizarní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
naměřená	naměřený	k2eAgFnSc1d1	naměřená
dvěma	dva	k4xCgMnPc7	dva
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
není	být	k5eNaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
relativních	relativní	k2eAgFnPc6d1	relativní
rychlostech	rychlost	k1gFnPc6	rychlost
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gFnPc7	jejich
vztažnými	vztažný	k2eAgFnPc7d1	vztažná
soustavami	soustava	k1gFnPc7	soustava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Lorentzovy	Lorentzův	k2eAgFnPc4d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
současné	současný	k2eAgFnPc1d1	současná
události	událost	k1gFnPc1	událost
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
současnými	současný	k2eAgFnPc7d1	současná
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
relativita	relativita	k1gFnSc1	relativita
současnosti	současnost	k1gFnSc2	současnost
nesoumístných	soumístný	k2eNgFnPc2d1	soumístný
událostí	událost	k1gFnPc2	událost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
objektu	objekt	k1gInSc2	objekt
změřená	změřený	k2eAgFnSc1d1	změřená
jedním	jeden	k4xCgMnSc7	jeden
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
téhož	týž	k3xTgInSc2	týž
objektu	objekt	k1gInSc2	objekt
jinými	jiný	k2eAgMnPc7d1	jiný
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Lorentzovy	Lorentzův	k2eAgFnPc4d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paradox	paradox	k1gInSc1	paradox
dvojčat	dvojče	k1gNnPc2	dvojče
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedno	jeden	k4xCgNnSc4	jeden
letí	letět	k5eAaImIp3nS	letět
pryč	pryč	k6eAd1	pryč
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
lodí	loď	k1gFnSc7	loď
rychlostí	rychlost	k1gFnSc7	rychlost
blízkou	blízký	k2eAgFnSc7d1	blízká
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhé	druhý	k4xOgNnSc4	druhý
dvojče	dvojče	k1gNnSc4	dvojče
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
stárlo	stárnout	k5eAaImAgNnS	stárnout
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
ono	onen	k3xDgNnSc1	onen
samo	sám	k3xTgNnSc1	sám
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
že	že	k8xS	že
první	první	k4xOgNnPc1	první
dvojče	dvojče	k1gNnSc1	dvojče
stárlo	stárnout	k5eAaImAgNnS	stárnout
pomaleji	pomale	k6eAd2	pomale
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zrevidování	zrevidování	k1gNnSc2	zrevidování
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
také	také	k9	také
nový	nový	k2eAgInSc4d1	nový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
koncept	koncept	k1gInSc4	koncept
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
důležitými	důležitý	k2eAgInPc7d1	důležitý
pojmy	pojem	k1gInPc7	pojem
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nutnost	nutnost	k1gFnSc1	nutnost
změny	změna	k1gFnSc2	změna
"	"	kIx"	"
<g/>
zákonů	zákon	k1gInPc2	zákon
zachování	zachování	k1gNnSc1	zachování
<g/>
"	"	kIx"	"
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
grupy	grupa	k1gFnSc2	grupa
transformací	transformace	k1gFnPc2	transformace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
symetrií	symetrie	k1gFnSc7	symetrie
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
hlubšího	hluboký	k2eAgInSc2d2	hlubší
teorému	teorém	k1gInSc2	teorém
Emmy	Emma	k1gFnSc2	Emma
Noetherové	Noetherová	k1gFnSc2	Noetherová
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Podobně	podobně	k6eAd1	podobně
spojila	spojit	k5eAaPmAgFnS	spojit
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
i	i	k9	i
čas	čas	k1gInSc4	čas
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dvě	dva	k4xCgNnPc1	dva
strany	strana	k1gFnPc1	strana
jedné	jeden	k4xCgFnSc2	jeden
mince	mince	k1gFnSc2	mince
zvané	zvaný	k2eAgFnSc2d1	zvaná
časoprostor	časoprostor	k1gInSc4	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
rovnocenných	rovnocenný	k2eAgFnPc2d1	rovnocenná
cest	cesta	k1gFnPc2	cesta
jak	jak	k8xC	jak
definovat	definovat	k5eAaBmF	definovat
hybnost	hybnost	k1gFnSc4	hybnost
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
STR	str	kA	str
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
používá	používat	k5eAaImIp3nS	používat
zákony	zákon	k1gInPc4	zákon
zachování	zachování	k1gNnSc2	zachování
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
zůstaly	zůstat	k5eAaPmAgInP	zůstat
platné	platný	k2eAgInPc1d1	platný
v	v	k7c6	v
STR	str	kA	str
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
platit	platit	k5eAaImF	platit
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
možné	možný	k2eAgFnSc6d1	možná
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
si	se	k3xPyFc3	se
však	však	k9	však
udělali	udělat	k5eAaPmAgMnP	udělat
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
experiment	experiment	k1gInSc4	experiment
s	s	k7c7	s
Newtonovými	Newtonův	k2eAgFnPc7d1	Newtonova
definicemi	definice	k1gFnPc7	definice
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
veličiny	veličina	k1gFnPc1	veličina
v	v	k7c6	v
STR	str	kA	str
nejsou	být	k5eNaImIp3nP	být
zachovávány	zachováván	k2eAgFnPc1d1	zachovávána
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
možnou	možný	k2eAgFnSc7d1	možná
záchranou	záchrana	k1gFnSc7	záchrana
je	být	k5eAaImIp3nS	být
udělat	udělat	k5eAaPmF	udělat
malé	malý	k2eAgFnPc4d1	malá
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
definicích	definice	k1gFnPc6	definice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nP	uplatnit
jen	jen	k9	jen
při	při	k7c6	při
relativistických	relativistický	k2eAgFnPc6d1	relativistická
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
nové	nový	k2eAgFnPc1d1	nová
definice	definice	k1gFnPc1	definice
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgFnP	přijmout
jako	jako	k9	jako
správné	správný	k2eAgNnSc4d1	správné
pro	pro	k7c4	pro
hybnost	hybnost	k1gFnSc4	hybnost
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
STR	str	kA	str
<g/>
:	:	kIx,	:
Mějme	mít	k5eAaImRp1nP	mít
objekt	objekt	k1gInSc4	objekt
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
m	m	kA	m
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
v.	v.	k?	v.
Jeho	jeho	k3xOp3gFnPc1	jeho
energie	energie	k1gFnPc1	energie
a	a	k8xC	a
hybnost	hybnost	k1gFnSc1	hybnost
jsou	být	k5eAaImIp3nP	být
dány	dán	k2eAgInPc1d1	dán
vztahy	vztah	k1gInPc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
γ	γ	k?	γ
m	m	kA	m
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
γ	γ	k?	γ
m	m	kA	m
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
mv	mv	k?	mv
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
γ	γ	k?	γ
(	(	kIx(	(
<g/>
Lorentzův	Lorentzův	k2eAgInSc1d1	Lorentzův
faktor	faktor	k1gInSc1	faktor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
-	-	kIx~	-
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnPc4	gammum
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-v	-v	k?	-v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
:	:	kIx,	:
a	a	k8xC	a
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
γ	γ	k?	γ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
STR	str	kA	str
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rovnic	rovnice	k1gFnPc2	rovnice
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
zhruba	zhruba	k6eAd1	zhruba
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
se	se	k3xPyFc4	se
chování	chování	k1gNnSc1	chování
tělesa	těleso	k1gNnSc2	těleso
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
chová	chovat	k5eAaImIp3nS	chovat
zcela	zcela	k6eAd1	zcela
Newtonovsky	newtonovsky	k6eAd1	newtonovsky
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
γ	γ	k?	γ
→	→	k?	→
∞	∞	k?	∞
se	se	k3xPyFc4	se
zvýrazňují	zvýrazňovat	k5eAaImIp3nP	zvýrazňovat
relativistické	relativistický	k2eAgInPc1d1	relativistický
jevy	jev	k1gInPc1	jev
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vztah	vztah	k1gInSc1	vztah
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vzorec	vzorec	k1gInSc1	vzorec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
p	p	k?	p
c	c	k0	c
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
m	m	kA	m
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
pc	pc	k?	pc
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rychlosti	rychlost	k1gFnPc4	rychlost
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
γ	γ	k?	γ
aproximuje	aproximovat	k5eAaBmIp3nS	aproximovat
užitím	užití	k1gNnSc7	užití
Taylorova	Taylorův	k2eAgInSc2d1	Taylorův
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
lze	lze	k6eAd1	lze
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
≈	≈	k?	≈
m	m	kA	m
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc4	matrix
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}}	}}	k?	}}
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
≈	≈	k?	≈
m	m	kA	m
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
mv	mv	k?	mv
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Kromě	kromě	k7c2	kromě
prvního	první	k4xOgInSc2	první
výrazu	výraz	k1gInSc2	výraz
ve	v	k7c6	v
vyjádření	vyjádření	k1gNnSc6	vyjádření
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
diskutováno	diskutovat	k5eAaImNgNnS	diskutovat
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
vzorce	vzorec	k1gInPc1	vzorec
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
standardními	standardní	k2eAgFnPc7d1	standardní
definicemi	definice	k1gFnPc7	definice
newtonovské	newtonovský	k2eAgFnSc2d1	newtonovská
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pohlédneme	pohlédnout	k5eAaPmIp1nP	pohlédnout
na	na	k7c4	na
předchozí	předchozí	k2eAgInPc4d1	předchozí
vzorce	vzorec	k1gInPc4	vzorec
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
(	(	kIx(	(
<g/>
v	v	k7c6	v
=	=	kIx~	=
0	[number]	k4	0
a	a	k8xC	a
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
nenulový	nulový	k2eNgInSc4d1	nenulový
zbytek	zbytek	k1gInSc4	zbytek
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
m	m	kA	m
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
klidová	klidový	k2eAgFnSc1d1	klidová
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Klidová	klidový	k2eAgFnSc1d1	klidová
energie	energie	k1gFnSc1	energie
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Newtonovou	Newtonův	k2eAgFnSc7d1	Newtonova
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
a	a	k8xC	a
protože	protože	k8xS	protože
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
rovnici	rovnice	k1gFnSc6	rovnice
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
pouze	pouze	k6eAd1	pouze
přeměny	přeměna	k1gFnPc4	přeměna
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vezmeme	vzít	k5eAaPmIp1nP	vzít
vzorec	vzorec	k1gInSc4	vzorec
tak	tak	k9	tak
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
napsaný	napsaný	k2eAgMnSc1d1	napsaný
<g/>
,	,	kIx,	,
vidíme	vidět	k5eAaImIp1nP	vidět
že	že	k8xS	že
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
další	další	k2eAgFnSc7d1	další
formou	forma	k1gFnSc7	forma
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc1	vzorec
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
důležitým	důležitý	k2eAgInSc7d1	důležitý
například	například	k6eAd1	například
když	když	k8xS	když
pracujeme	pracovat	k5eAaImIp1nP	pracovat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgNnPc2d1	různé
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
jednoho	jeden	k4xCgInSc2	jeden
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Změřením	změření	k1gNnSc7	změření
rozdílů	rozdíl	k1gInPc2	rozdíl
lze	lze	k6eAd1	lze
předpovídat	předpovídat	k5eAaImF	předpovídat
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jádra	jádro	k1gNnPc1	jádro
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
velkou	velký	k2eAgFnSc4d1	velká
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
jadernými	jaderný	k2eAgFnPc7d1	jaderná
reakcemi	reakce	k1gFnPc7	reakce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
informace	informace	k1gFnPc4	informace
důležité	důležitý	k2eAgFnPc4d1	důležitá
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
(	(	kIx(	(
<g/>
správněji	správně	k6eAd2	správně
<g/>
:	:	kIx,	:
jaderné	jaderný	k2eAgFnPc4d1	jaderná
bomby	bomba	k1gFnPc4	bomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
vzorce	vzorec	k1gInSc2	vzorec
pro	pro	k7c4	pro
historii	historie	k1gFnSc4	historie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
učinily	učinit	k5eAaImAgInP	učinit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
hmotnost	hmotnost	k1gFnSc1	hmotnost
tělesa	těleso	k1gNnPc4	těleso
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
definici	definice	k1gFnSc4	definice
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
v	v	k7c6	v
STR	str	kA	str
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
představy	představa	k1gFnPc1	představa
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
rovnice	rovnice	k1gFnSc1	rovnice
užívá	užívat	k5eAaImIp3nS	užívat
tzv.	tzv.	kA	tzv.
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
neměnnou	neměnný	k2eAgFnSc7d1	neměnná
veličinou	veličina	k1gFnSc7	veličina
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
inerciální	inerciální	k2eAgMnPc4d1	inerciální
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
<g/>
,	,	kIx,	,
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
definicí	definice	k1gFnSc7	definice
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
relativistická	relativistický	k2eAgFnSc1d1	relativistická
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
γ	γ	k?	γ
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Jelikož	jelikož	k8xS	jelikož
γ	γ	k?	γ
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
relativistická	relativistický	k2eAgFnSc1d1	relativistická
hmotnost	hmotnost	k1gFnSc1	hmotnost
roste	růst	k5eAaImIp3nS	růst
rovněž	rovněž	k9	rovněž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
účelů	účel	k1gInPc2	účel
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
napsat	napsat	k5eAaPmF	napsat
rovnice	rovnice	k1gFnPc1	rovnice
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
hybnost	hybnost	k1gFnSc4	hybnost
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
M	M	kA	M
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
Mc	Mc	k1gMnSc1	Mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
M	M	kA	M
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
Mv	Mv	k1gMnSc1	Mv
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
které	který	k3yQgNnSc1	který
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
rovna	roven	k2eAgFnSc1d1	rovna
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
relativistická	relativistický	k2eAgFnSc1d1	relativistická
a	a	k8xC	a
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
.	.	kIx.	.
</s>
<s>
Nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
nebo	nebo	k8xC	nebo
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
záležitost	záležitost	k1gFnSc1	záležitost
pohodlnějších	pohodlný	k2eAgInPc2d2	pohodlnější
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c1	mnoho
fyziků	fyzik	k1gMnPc2	fyzik
nemá	mít	k5eNaImIp3nS	mít
rádo	rád	k2eAgNnSc1d1	rádo
koncept	koncept	k1gInSc4	koncept
relativistické	relativistický	k2eAgFnSc2d1	relativistická
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
skalár	skalár	k1gInSc4	skalár
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
relativistická	relativistický	k2eAgFnSc1d1	relativistická
hmotnost	hmotnost	k1gFnSc1	hmotnost
podél	podél	k7c2	podél
jedné	jeden	k4xCgFnSc2	jeden
osy	osa	k1gFnSc2	osa
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
relativistickou	relativistický	k2eAgFnSc7d1	relativistická
hmotností	hmotnost	k1gFnSc7	hmotnost
podél	podél	k7c2	podél
jiné	jiný	k2eAgFnSc2d1	jiná
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
veličinou	veličina	k1gFnSc7	veličina
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fyziků	fyzik	k1gMnPc2	fyzik
proto	proto	k8xC	proto
jednoduše	jednoduše	k6eAd1	jednoduše
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
ačkoliv	ačkoliv	k8xS	ačkoliv
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
současné	současný	k2eAgFnPc1d1	současná
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
současné	současný	k2eAgFnSc3d1	současná
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
AB	AB	kA	AB
na	na	k7c6	na
diagramu	diagram	k1gInSc6	diagram
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
časový	časový	k2eAgInSc1d1	časový
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
máme	mít	k5eAaImIp1nP	mít
zde	zde	k6eAd1	zde
soustavu	soustava	k1gFnSc4	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
událost	událost	k1gFnSc1	událost
A	a	k9	a
a	a	k8xC	a
událost	událost	k1gFnSc4	událost
B	B	kA	B
nastávají	nastávat	k5eAaImIp3nP	nastávat
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
A	a	k9	a
předchází	předcházet	k5eAaImIp3nS	předcházet
B	B	kA	B
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soustavě	soustava	k1gFnSc6	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
A	a	k9	a
předchází	předcházet	k5eAaImIp3nS	předcházet
B	B	kA	B
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soustavách	soustava	k1gFnPc6	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Hypoteticky	hypoteticky	k6eAd1	hypoteticky
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přemísťování	přemísťování	k1gNnSc1	přemísťování
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
informace	informace	k1gFnPc4	informace
<g/>
)	)	kIx)	)
z	z	k7c2	z
A	A	kA	A
do	do	k7c2	do
B	B	kA	B
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
nastávat	nastávat	k5eAaImF	nastávat
příčinný	příčinný	k2eAgInSc4d1	příčinný
vztah	vztah	k1gInSc4	vztah
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
A	a	k9	a
je	být	k5eAaImIp3nS	být
příčina	příčina	k1gFnSc1	příčina
a	a	k8xC	a
B	B	kA	B
je	být	k5eAaImIp3nS	být
následek	následek	k1gInSc4	následek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
AC	AC	kA	AC
na	na	k7c6	na
diagramu	diagram	k1gInSc6	diagram
je	být	k5eAaImIp3nS	být
prostorový	prostorový	k2eAgInSc1d1	prostorový
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
máme	mít	k5eAaImIp1nP	mít
zde	zde	k6eAd1	zde
soustavu	soustava	k1gFnSc4	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
událost	událost	k1gFnSc1	událost
A	a	k8xC	a
a	a	k8xC	a
událost	událost	k1gFnSc1	událost
C	C	kA	C
současné	současný	k2eAgInPc4d1	současný
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
pouze	pouze	k6eAd1	pouze
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Leč	leč	k8xS	leč
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
soustavy	soustava	k1gFnPc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
A	a	k9	a
předchází	předcházet	k5eAaImIp3nP	předcházet
C	C	kA	C
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
<g/>
)	)	kIx)	)
a	a	k8xC	a
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
C	C	kA	C
předchází	předcházet	k5eAaImIp3nS	předcházet
A.	A.	kA	A.
Vyjma	vyjma	k7c4	vyjma
cestování	cestování	k1gNnSc4	cestování
nadsvětelnou	nadsvětelný	k2eAgFnSc7d1	nadsvětelná
rychlostí	rychlost	k1gFnSc7	rychlost
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pro	pro	k7c4	pro
žádné	žádný	k3yNgNnSc4	žádný
hmotné	hmotný	k2eAgNnSc4d1	hmotné
těleso	těleso	k1gNnSc4	těleso
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
informaci	informace	k1gFnSc4	informace
<g/>
)	)	kIx)	)
cestovat	cestovat	k5eAaImF	cestovat
z	z	k7c2	z
A	A	kA	A
do	do	k7c2	do
C	C	kA	C
nebo	nebo	k8xC	nebo
z	z	k7c2	z
C	C	kA	C
do	do	k7c2	do
A.	A.	kA	A.
Nemůže	moct	k5eNaImIp3nS	moct
zde	zde	k6eAd1	zde
existovat	existovat	k5eAaImF	existovat
žádná	žádný	k3yNgFnSc1	žádný
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
A	A	kA	A
a	a	k8xC	a
C	C	kA	C
STR	str	kA	str
užívá	užívat	k5eAaImIp3nS	užívat
'	'	kIx"	'
<g/>
rovinný	rovinný	k2eAgMnSc1d1	rovinný
<g/>
'	'	kIx"	'
4	[number]	k4	4
<g/>
rozměrný	rozměrný	k2eAgInSc4d1	rozměrný
Minkowského	Minkowského	k2eAgInSc4d1	Minkowského
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
prostoročas	prostoročas	k1gInSc1	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
standardnímu	standardní	k2eAgInSc3d1	standardní
3	[number]	k4	3
<g/>
rozměrnému	rozměrný	k2eAgInSc3d1	rozměrný
Euklidovskému	euklidovský	k2eAgInSc3d1	euklidovský
prostoru	prostor	k1gInSc3	prostor
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
velmi	velmi	k6eAd1	velmi
jednoduše	jednoduše	k6eAd1	jednoduše
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciál	diferenciál	k1gInSc1	diferenciál
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
(	(	kIx(	(
<g/>
ds	ds	k?	ds
<g/>
)	)	kIx)	)
v	v	k7c6	v
kartézském	kartézský	k2eAgMnSc6d1	kartézský
3	[number]	k4	3
<g/>
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
diferenciály	diferenciál	k1gInPc1	diferenciál
tří	tři	k4xCgFnPc2	tři
prostorových	prostorový	k2eAgFnPc2d1	prostorová
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
přidána	přidat	k5eAaPmNgFnS	přidat
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
dimenze	dimenze	k1gFnSc1	dimenze
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
c	c	k0	c
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rovnice	rovnice	k1gFnSc1	rovnice
pro	pro	k7c4	pro
diferenciál	diferenciál	k1gInSc4	diferenciál
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
situacích	situace	k1gFnPc6	situace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užitečné	užitečný	k2eAgFnPc4d1	užitečná
uvažovat	uvažovat	k5eAaImF	uvažovat
čas	čas	k1gInSc4	čas
jako	jako	k8xS	jako
imaginární	imaginární	k2eAgFnSc4d1	imaginární
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
}	}	kIx)	}
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
rovnici	rovnice	k1gFnSc6	rovnice
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
.	.	kIx.	.
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i.t	i.t	k?	i.t
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
a	a	k8xC	a
metrika	metrika	k1gFnSc1	metrika
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
zmenšíme	zmenšit	k5eAaPmIp1nP	zmenšit
počet	počet	k1gInSc4	počet
prostorových	prostorový	k2eAgFnPc2d1	prostorová
dimenzí	dimenze	k1gFnPc2	dimenze
na	na	k7c4	na
2	[number]	k4	2
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
svět	svět	k1gInSc4	svět
3	[number]	k4	3
<g/>
-rozměrným	ozměrný	k2eAgInSc7d1	-rozměrný
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Lze	lze	k6eAd1	lze
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nulové	nulový	k2eAgInPc4d1	nulový
(	(	kIx(	(
<g/>
světelné	světelný	k2eAgInPc4d1	světelný
<g/>
)	)	kIx)	)
geodetiky	geodetik	k1gMnPc4	geodetik
leží	ležet	k5eAaImIp3nS	ležet
podél	podél	k7c2	podél
dvojkuželu	dvojkužel	k1gInSc2	dvojkužel
<g/>
:	:	kIx,	:
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgInP	definovat
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
což	což	k3yRnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
rovnici	rovnice	k1gFnSc4	rovnice
kružnice	kružnice	k1gFnSc2	kružnice
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
r	r	kA	r
=	=	kIx~	=
c	c	k0	c
·	·	k?	·
dt	dt	k?	dt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
předchozí	předchozí	k2eAgMnSc1d1	předchozí
rozšíříme	rozšířit	k5eAaPmIp1nP	rozšířit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
prostorových	prostorový	k2eAgFnPc2d1	prostorová
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
koncové	koncový	k2eAgInPc1d1	koncový
body	bod	k1gInPc1	bod
nulových	nulový	k2eAgInPc2d1	nulový
geodetik	geodetik	k1gMnSc1	geodetik
budou	být	k5eAaImBp3nP	být
soustřednými	soustředný	k2eAgFnPc7d1	soustředná
kulovými	kulový	k2eAgFnPc7d1	kulová
plochami	plocha	k1gFnPc7	plocha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poloměr	poloměr	k1gInSc4	poloměr
=	=	kIx~	=
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
=	=	kIx~	=
c	c	k0	c
·	·	k?	·
±	±	k?	±
<g/>
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
μ	μ	k?	μ
,	,	kIx,	,
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
dx	dx	k?	dx
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
dx	dx	k?	dx
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
nu	nu	k9	nu
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
μ	μ	k?	μ
,	,	kIx,	,
ν	ν	k?	ν
=	=	kIx~	=
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
3	[number]	k4	3
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
{	{	kIx(	{
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
c	c	k0	c
t	t	k?	t
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
,2	,2	k4	,2
<g/>
,3	,3	k4	,3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
ct	ct	k?	ct
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prázdný	prázdný	k2eAgInSc1d1	prázdný
dvojkužel	dvojkužet	k5eAaPmAgInS	dvojkužet
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
"	"	kIx"	"
<g/>
linie	linie	k1gFnSc1	linie
pohledu	pohled	k1gInSc2	pohled
<g/>
"	"	kIx"	"
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
díváme	dívat	k5eAaImIp1nP	dívat
na	na	k7c4	na
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
řekneme	říct	k5eAaPmIp1nP	říct
"	"	kIx"	"
<g/>
Světlo	světlo	k1gNnSc1	světlo
této	tento	k3xDgFnSc2	tento
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
mi	já	k3xPp1nSc3	já
právě	právě	k6eAd1	právě
dopadá	dopadat	k5eAaImIp3nS	dopadat
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
právě	právě	k9	právě
díváme	dívat	k5eAaImIp1nP	dívat
podél	podél	k7c2	podél
linie	linie	k1gFnSc2	linie
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
podél	podél	k6eAd1	podél
nulové	nulový	k2eAgMnPc4d1	nulový
geodetiky	geodetik	k1gMnPc4	geodetik
<g/>
.	.	kIx.	.
</s>
<s>
Díváme	dívat	k5eAaImIp1nP	dívat
se	se	k3xPyFc4	se
na	na	k7c4	na
událost	událost	k1gFnSc4	událost
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
d	d	k?	d
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
sekund	sekunda	k1gFnPc2	sekunda
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
světelný	světelný	k2eAgInSc1d1	světelný
kužel	kužel	k1gInSc1	kužel
znám	znát	k5eAaImIp1nS	znát
také	také	k9	také
jako	jako	k9	jako
'	'	kIx"	'
<g/>
světelný	světelný	k2eAgInSc4d1	světelný
kužel	kužel	k1gInSc4	kužel
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bod	bod	k1gInSc1	bod
v	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
dolním	dolní	k2eAgInSc6d1	dolní
rohu	roh	k1gInSc6	roh
obrázku	obrázek	k1gInSc2	obrázek
dole	dole	k6eAd1	dole
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
počátek	počátek	k1gInSc4	počátek
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
a	a	k8xC	a
čára	čára	k1gFnSc1	čára
ke	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
nulovou	nulový	k2eAgFnSc4d1	nulová
geodetiku	geodetik	k1gMnSc6	geodetik
"	"	kIx"	"
<g/>
linie	linie	k1gFnSc2	linie
pohledu	pohled	k1gInSc2	pohled
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Kužel	Kužel	k1gMnSc1	Kužel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
-	-	kIx~	-
<g/>
t	t	k?	t
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
bod	bod	k1gInSc1	bod
"	"	kIx"	"
<g/>
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kužel	kužel	k1gInSc1	kužel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
+	+	kIx~	+
<g/>
t	t	k?	t
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bod	bod	k1gInSc1	bod
"	"	kIx"	"
<g/>
vysílá	vysílat	k5eAaImIp3nS	vysílat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mnoho	mnoho	k4c1	mnoho
veličin	veličina	k1gFnPc2	veličina
známých	známý	k1gMnPc2	známý
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
relativitě	relativita	k1gFnSc6	relativita
na	na	k7c6	na
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
závislý	závislý	k2eAgInSc1d1	závislý
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
formulovat	formulovat	k5eAaImF	formulovat
zákony	zákon	k1gInPc4	zákon
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
pomocí	pomoc	k1gFnPc2	pomoc
na	na	k7c6	na
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
nezávislého	závislý	k2eNgInSc2d1	nezávislý
tenzorového	tenzorový	k2eAgInSc2d1	tenzorový
zápisu	zápis	k1gInSc2	zápis
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
měřitelné	měřitelný	k2eAgFnPc4d1	měřitelná
veličiny	veličina	k1gFnPc4	veličina
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
jako	jako	k8xC	jako
tenzory	tenzor	k1gInPc4	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
metrický	metrický	k2eAgInSc1d1	metrický
tenzor	tenzor	k1gInSc1	tenzor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
STR	str	kA	str
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
a	a	k8xC	a
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
inerciální	inerciální	k2eAgFnPc4d1	inerciální
vztažné	vztažný	k2eAgFnPc4d1	vztažná
soustavy	soustava	k1gFnPc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
relativitě	relativita	k1gFnSc6	relativita
se	se	k3xPyFc4	se
k	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
zápisu	zápis	k1gInSc2	zápis
používá	používat	k5eAaImIp3nS	používat
Einsteinovo	Einsteinův	k2eAgNnSc1d1	Einsteinovo
sumační	sumační	k2eAgNnSc1d1	sumační
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
sčítání	sčítání	k1gNnSc4	sčítání
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
stejné	stejný	k2eAgInPc4d1	stejný
indexy	index	k1gInPc4	index
s	s	k7c7	s
opačnou	opačný	k2eAgFnSc7d1	opačná
polohou	poloha	k1gFnSc7	poloha
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
členu	člen	k1gInSc6	člen
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rovném	rovný	k2eAgInSc6d1	rovný
prostoročasu	prostoročas	k1gInSc6	prostoročas
derivováním	derivování	k1gNnSc7	derivování
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tenzorové	tenzorový	k2eAgFnPc1d1	tenzorová
veličiny	veličina	k1gFnPc1	veličina
podle	podle	k7c2	podle
parametru	parametr	k1gInSc2	parametr
vzniká	vznikat	k5eAaImIp3nS	vznikat
tenzorová	tenzorový	k2eAgFnSc1d1	tenzorová
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakých	jaký	k3yRgFnPc6	jaký
chceme	chtít	k5eAaImIp1nP	chtít
teorii	teorie	k1gFnSc4	teorie
formulovat	formulovat	k5eAaImF	formulovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
invariantní	invariantní	k2eAgFnSc1d1	invariantní
vůči	vůči	k7c3	vůči
změně	změna	k1gFnSc3	změna
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
pohybu	pohyb	k1gInSc2	pohyb
hmotného	hmotný	k2eAgInSc2d1	hmotný
<g />
.	.	kIx.	.
</s>
<s hack="1">
bodu	bod	k1gInSc2	bod
tzv.	tzv.	kA	tzv.
čtyřrychlost	čtyřrychlost	k1gFnSc1	čtyřrychlost
a	a	k8xC	a
čtyřzrychlení	čtyřzrychlený	k2eAgMnPc1d1	čtyřzrychlený
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
γ	γ	k?	γ
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
klasické	klasický	k2eAgFnSc2d1	klasická
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
STR	str	kA	str
tzv.	tzv.	kA	tzv.
čtyřhybnost	čtyřhybnost	k1gFnSc1	čtyřhybnost
a	a	k8xC	a
čtyřsíla	čtyřsíla	k1gFnSc1	čtyřsíla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zavádíme	zavádět	k5eAaImIp1nP	zavádět
vztahy	vztah	k1gInPc4	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
P	P	kA	P
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
E	E	kA	E
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
equiv	equivit	k5eAaPmRp2nS	equivit
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
Ec	Ec	k1gFnSc1	Ec
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
:	:	kIx,	:
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s hack="1">
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
veličiny	veličina	k1gFnPc1	veličina
mají	mít	k5eAaImIp3nP	mít
tenzorový	tenzorový	k2eAgInSc4d1	tenzorový
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
derivováním	derivování	k1gNnSc7	derivování
tenzorových	tenzorový	k2eAgFnPc2d1	tenzorová
veličin	veličina	k1gFnPc2	veličina
podle	podle	k7c2	podle
parametru	parametr	k1gInSc2	parametr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
násobením	násobení	k1gNnSc7	násobení
tenzorových	tenzorový	k2eAgFnPc2d1	tenzorová
veličin	veličina	k1gFnPc2	veličina
skalárem	skalár	k1gInSc7	skalár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
značí	značit	k5eAaImIp3nS	značit
složky	složka	k1gFnPc4	složka
klasické	klasický	k2eAgFnSc2d1	klasická
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u_	u_	k?	u_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
složky	složka	k1gFnPc4	složka
klasické	klasický	k2eAgFnSc2d1	klasická
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
E	E	kA	E
=	=	kIx~	=
mc2	mc2	k4	mc2
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
pak	pak	k6eAd1	pak
používáme	používat	k5eAaImIp1nP	používat
jednak	jednak	k8xC	jednak
čtyřproud	čtyřproud	k6eAd1	čtyřproud
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
equiv	equiva	k1gFnPc2	equiva
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
klidová	klidový	k2eAgFnSc1d1	klidová
hustota	hustota	k1gFnSc1	hustota
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
Bývá	bývat	k5eAaImIp3nS	bývat
rovněž	rovněž	k9	rovněž
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zobecnit	zobecnit	k5eAaPmF	zobecnit
relativisticky	relativisticky	k6eAd1	relativisticky
i	i	k9	i
vektorový	vektorový	k2eAgInSc1d1	vektorový
a	a	k8xC	a
skalární	skalární	k2eAgInSc1d1	skalární
potenciál	potenciál	k1gInSc1	potenciál
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
čtyřpotenciál	čtyřpotenciál	k1gInSc4	čtyřpotenciál
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
(	(	kIx(	(
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
tenzor	tenzor	k1gInSc4	tenzor
intenzity	intenzita	k1gFnSc2	intenzita
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
definovaný	definovaný	k2eAgInSc1d1	definovaný
jako	jako	k9	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
A_	A_	k1gFnSc6	A_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
&	&	k?	&
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
&	&	k?	&
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
\\	\\	k?	\\
<g/>
E_	E_	k1gFnSc6	E_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
B_	B_	k1gFnPc2	B_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
<g/>
-B_	-B_	k?	-B_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
&	&	k?	&
<g/>
-B_	-B_	k?	-B_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
B_	B_	k1gFnPc2	B_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
E_	E_	k1gFnSc1	E_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
&	&	k?	&
<g/>
B_	B_	k1gFnPc6	B_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
-B_	-B_	k?	-B_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
můžeme	moct	k5eAaImIp1nP	moct
<g />
.	.	kIx.	.
</s>
<s hack="1">
elektromagnetismus	elektromagnetismus	k1gInSc1	elektromagnetismus
zapsat	zapsat	k5eAaPmF	zapsat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
elegantním	elegantní	k2eAgInSc6d1	elegantní
tvaru	tvar	k1gInSc6	tvar
<g/>
:	:	kIx,	:
Zachování	zachování	k1gNnSc1	zachování
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
rovnicí	rovnice	k1gFnSc7	rovnice
kontinuity	kontinuita	k1gFnSc2	kontinuita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ekvivalent	ekvivalent	k1gInSc1	ekvivalent
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
Lorentzova	Lorentzův	k2eAgFnSc1d1	Lorentzova
čtyřsíla	čtyřsíla	k1gFnSc1	čtyřsíla
daná	daný	k2eAgFnSc1d1	daná
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
q	q	k?	q
:	:	kIx,	:
F	F	kA	F
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
qF	qF	k?	qF
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
ověřování	ověřování	k1gNnSc3	ověřování
postulátů	postulát	k1gInPc2	postulát
a	a	k8xC	a
důsledků	důsledek	k1gInPc2	důsledek
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
předcházelo	předcházet	k5eAaImAgNnS	předcházet
testování	testování	k1gNnSc1	testování
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
rozpor	rozpor	k1gInSc4	rozpor
mezi	mezi	k7c7	mezi
transformačním	transformační	k2eAgNnSc7d1	transformační
chováním	chování	k1gNnSc7	chování
mechanických	mechanický	k2eAgInPc2d1	mechanický
a	a	k8xC	a
elektromagnetických	elektromagnetický	k2eAgInPc2d1	elektromagnetický
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
teorie	teorie	k1gFnSc2	teorie
éteru	éter	k1gInSc2	éter
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgInPc4d1	negativní
výsledky	výsledek	k1gInPc4	výsledek
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
možno	možno	k6eAd1	možno
interpretovat	interpretovat	k5eAaBmF	interpretovat
i	i	k8xC	i
jako	jako	k9	jako
nepřímá	přímý	k2eNgNnPc4d1	nepřímé
potvrzení	potvrzení	k1gNnPc4	potvrzení
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
přímému	přímý	k2eAgNnSc3d1	přímé
potvrzení	potvrzení	k1gNnSc3	potvrzení
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
a	a	k8xC	a
provedeno	provést	k5eAaPmNgNnS	provést
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Michelsonův-Morleyův	Michelsonův-Morleyův	k2eAgInSc1d1	Michelsonův-Morleyův
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
testování	testování	k1gNnSc1	testování
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
hypotézy	hypotéza	k1gFnSc2	hypotéza
kontrakce	kontrakce	k1gFnSc2	kontrakce
délky	délka	k1gFnSc2	délka
pohybem	pohyb	k1gInSc7	pohyb
v	v	k7c6	v
éteru	éter	k1gInSc6	éter
Hammarův	Hammarův	k2eAgInSc1d1	Hammarův
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
testování	testování	k1gNnSc1	testování
hypotézy	hypotéza	k1gFnSc2	hypotéza
strhávání	strhávání	k1gNnSc1	strhávání
éteru	éter	k1gInSc2	éter
Troutonův-Nobleův	Troutonův-Nobleův	k2eAgInSc1d1	Troutonův-Nobleův
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
stáčení	stáčení	k1gNnSc1	stáčení
roviny	rovina	k1gFnSc2	rovina
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
éteru	éter	k1gInSc6	éter
Michelsonův-Morleyho	Michelsonův-Morley	k1gMnSc2	Michelsonův-Morley
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
ač	ač	k8xS	ač
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
testu	test	k1gInSc3	test
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
základním	základní	k2eAgInSc7d1	základní
testem	test	k1gInSc7	test
prokazujícím	prokazující	k2eAgInSc7d1	prokazující
nezávislost	nezávislost	k1gFnSc4	nezávislost
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
pohybu	pohyb	k1gInSc6	pohyb
vztažné	vztažný	k2eAgFnSc2d1	vztažná
<g />
.	.	kIx.	.
</s>
<s>
soustavy	soustava	k1gFnSc2	soustava
Kennedyho-Thorndikův	Kennedyho-Thorndikův	k2eAgInSc1d1	Kennedyho-Thorndikův
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
modifikace	modifikace	k1gFnSc1	modifikace
Michelsonův-Morleyho	Michelsonův-Morley	k1gMnSc2	Michelsonův-Morley
experiment	experiment	k1gInSc1	experiment
Ivesův-Stilwellův	Ivesův-Stilwellův	k2eAgInSc1d1	Ivesův-Stilwellův
experiment	experiment	k1gInSc1	experiment
-	-	kIx~	-
první	první	k4xOgFnSc7	první
přímé	přímý	k2eAgFnPc1d1	přímá
potvrzení	potvrzení	k1gNnSc4	potvrzení
dilatace	dilatace	k1gFnSc2	dilatace
času	čas	k1gInSc2	čas
Lidé	člověk	k1gMnPc1	člověk
<g/>
:	:	kIx,	:
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
|	|	kIx~	|
Hendrik	Hendrik	k1gMnSc1	Hendrik
Lorentz	Lorentz	k1gMnSc1	Lorentz
|	|	kIx~	|
Hermann	Hermann	k1gMnSc1	Hermann
Minkowski	Minkowsk	k1gFnSc2	Minkowsk
|	|	kIx~	|
Bernhard	Bernhard	k1gInSc1	Bernhard
Riemann	Riemann	k1gNnSc1	Riemann
|	|	kIx~	|
Henri	Henri	k1gNnSc4	Henri
Poincaré	Poincarý	k2eAgNnSc4d1	Poincaré
Relativita	relativita	k1gFnSc1	relativita
<g/>
:	:	kIx,	:
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
|	|	kIx~	|
Galileiho	Galilei	k1gMnSc2	Galilei
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
|	|	kIx~	|
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
|	|	kIx~	|
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
|	|	kIx~	|
inerciální	inerciální	k2eAgFnSc1d1	inerciální
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
|	|	kIx~	|
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
Fyzika	fyzika	k1gFnSc1	fyzika
<g/>
:	:	kIx,	:
Newtonovská	newtonovský	k2eAgFnSc1d1	newtonovská
mechanika	mechanika	k1gFnSc1	mechanika
|	|	kIx~	|
prostoročas	prostoročas	k1gInSc1	prostoročas
|	|	kIx~	|
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
Matematika	matematika	k1gFnSc1	matematika
<g/>
:	:	kIx,	:
Minkowského	Minkowského	k2eAgInSc1d1	Minkowského
prostor	prostor	k1gInSc1	prostor
|	|	kIx~	|
světočára	světočára	k1gFnSc1	světočára
|	|	kIx~	|
světelný	světelný	k2eAgInSc1d1	světelný
kužel	kužel	k1gInSc1	kužel
|	|	kIx~	|
geometrie	geometrie	k1gFnSc1	geometrie
|	|	kIx~	|
tenzor	tenzor	k1gInSc1	tenzor
</s>
