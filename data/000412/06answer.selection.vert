<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
STR	str	kA	str
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
r.	r.	kA	r.
1905	[number]	k4	1905
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
