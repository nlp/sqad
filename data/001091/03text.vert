<s>
Franz	Franz	k1gMnSc1	Franz
Peter	Peter	k1gMnSc1	Peter
Schubert	Schubert	k1gMnSc1	Schubert
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
Lichtental	Lichtental	k1gMnSc1	Lichtental
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1828	[number]	k4	1828
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
Wieden	Wiedno	k1gNnPc2	Wiedno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
období	období	k1gNnSc2	období
raného	raný	k2eAgInSc2d1	raný
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
schopnost	schopnost	k1gFnSc4	schopnost
vystižení	vystižení	k1gNnSc2	vystižení
lyrických	lyrický	k2eAgFnPc2d1	lyrická
a	a	k8xC	a
romantických	romantický	k2eAgFnPc2d1	romantická
nálad	nálada	k1gFnPc2	nálada
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
melodický	melodický	k2eAgInSc4d1	melodický
talent	talent	k1gInSc4	talent
<g/>
;	;	kIx,	;
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
hovořil	hovořit	k5eAaImAgMnS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
nejpoetičtějším	poetický	k2eAgInSc6d3	nejpoetičtější
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Schubertův	Schubertův	k2eAgInSc1d1	Schubertův
největší	veliký	k2eAgInSc1d3	veliký
význam	význam	k1gInSc1	význam
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
písňové	písňový	k2eAgFnSc6d1	písňová
a	a	k8xC	a
komorní	komorní	k2eAgFnSc6d1	komorní
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
kolem	kolem	k7c2	kolem
600	[number]	k4	600
písní	píseň	k1gFnPc2	píseň
i	i	k8xC	i
mnoho	mnoho	k4c1	mnoho
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
komorní	komorní	k2eAgNnPc4d1	komorní
obsazení	obsazení	k1gNnPc4	obsazení
a	a	k8xC	a
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
sedmi	sedm	k4xCc2	sedm
dokončených	dokončený	k2eAgFnPc2d1	dokončená
symfonií	symfonie	k1gFnPc2	symfonie
a	a	k8xC	a
slavné	slavný	k2eAgNnSc4d1	slavné
osmé	osmý	k4xOgNnSc4	osmý
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dnešního	dnešní	k2eAgInSc2d1	dnešní
seznamu	seznam	k1gInSc2	seznam
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmé	sedmý	k4xOgFnSc2	sedmý
<g/>
)	)	kIx)	)
Nedokončené	dokončený	k2eNgFnSc2d1	nedokončená
i	i	k8xC	i
jiné	jiný	k2eAgFnSc2d1	jiná
liturgické	liturgický	k2eAgFnSc2d1	liturgická
<g/>
,	,	kIx,	,
scénické	scénický	k2eAgFnSc2d1	scénická
a	a	k8xC	a
baletní	baletní	k2eAgFnSc2d1	baletní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
pracovitost	pracovitost	k1gFnSc4	pracovitost
si	se	k3xPyFc3	se
Schubert	Schubert	k1gMnSc1	Schubert
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
krátkého	krátký	k2eAgInSc2d1	krátký
života	život	k1gInSc2	život
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
schopen	schopen	k2eAgMnSc1d1	schopen
opatřit	opatřit	k5eAaPmF	opatřit
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
finanční	finanční	k2eAgNnSc4d1	finanční
zajištění	zajištění	k1gNnSc4	zajištění
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
nebyla	být	k5eNaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
širší	široký	k2eAgFnSc3d2	širší
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
však	však	k9	však
narostl	narůst	k5eAaPmAgMnS	narůst
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
sklonku	sklonek	k1gInSc6	sklonek
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
a	a	k8xC	a
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
ke	k	k7c3	k
stěžejním	stěžejní	k2eAgMnPc3d1	stěžejní
autorům	autor	k1gMnPc3	autor
romantického	romantický	k2eAgInSc2d1	romantický
hudebního	hudební	k2eAgInSc2d1	hudební
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Peter	Peter	k1gMnSc1	Peter
Schubert	Schubert	k1gMnSc1	Schubert
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
předměstské	předměstský	k2eAgFnSc6d1	předměstská
části	část	k1gFnSc6	část
Lichtental	Lichtental	k1gMnSc1	Lichtental
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
součásti	součást	k1gFnPc1	součást
obvodu	obvod	k1gInSc2	obvod
Alsergrund	Alsergrund	k1gInSc1	Alsergrund
<g/>
,	,	kIx,	,
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
učitelské	učitelský	k2eAgFnSc6d1	učitelská
rodině	rodina	k1gFnSc6	rodina
jako	jako	k9	jako
třinácté	třináctý	k4xOgInPc4	třináctý
z	z	k7c2	z
patnácti	patnáct	k4xCc2	patnáct
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
sourozenců	sourozenec	k1gMnPc2	sourozenec
však	však	k9	však
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
ještě	ještě	k9	ještě
před	před	k7c7	před
dovršením	dovršení	k1gNnSc7	dovršení
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc4	původ
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
školního	školní	k2eAgInSc2d1	školní
řídícího	řídící	k2eAgInSc2d1	řídící
Franze	Franze	k1gFnPc4	Franze
Theodora	Theodor	k1gMnSc2	Theodor
Schuberta	Schubert	k1gMnSc2	Schubert
<g/>
,	,	kIx,	,
i	i	k8xC	i
jeho	jeho	k3xOp3gFnPc4	jeho
matky	matka	k1gFnPc4	matka
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
rozené	rozený	k2eAgFnSc2d1	rozená
Vietzové	Vietzová	k1gFnSc2	Vietzová
sahá	sahat	k5eAaImIp3nS	sahat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
:	:	kIx,	:
otec	otec	k1gMnSc1	otec
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
vesničky	vesnička	k1gFnSc2	vesnička
Neudorf	Neudorf	k1gInSc1	Neudorf
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
-	-	kIx~	-
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Malá	malý	k2eAgFnSc1d1	malá
Morava	Morava	k1gFnSc1	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
šumperském	šumperský	k2eAgInSc6d1	šumperský
okrese	okres	k1gInSc6	okres
poblíž	poblíž	k7c2	poblíž
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
ze	z	k7c2	z
Zlatých	zlatý	k2eAgFnPc2d1	zlatá
Hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
se	se	k3xPyFc4	se
mladý	mladý	k2eAgInSc1d1	mladý
Franz	Franz	k1gInSc1	Franz
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
výborný	výborný	k2eAgInSc4d1	výborný
hlas	hlas	k1gInSc4	hlas
stal	stát	k5eAaPmAgMnS	stát
studentem	student	k1gMnSc7	student
císařského	císařský	k2eAgInSc2d1	císařský
konviktu	konvikt	k1gInSc2	konvikt
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
členem	člen	k1gMnSc7	člen
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
sboru	sbor	k1gInSc2	sbor
dvorní	dvorní	k2eAgFnSc2d1	dvorní
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konviktu	konvikt	k1gInSc6	konvikt
si	se	k3xPyFc3	se
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
své	svůj	k3xOyFgNnSc4	svůj
zatím	zatím	k6eAd1	zatím
spíše	spíše	k9	spíše
jen	jen	k9	jen
domácké	domácký	k2eAgNnSc4d1	domácké
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k8xC	i
lekcemi	lekce	k1gFnPc7	lekce
u	u	k7c2	u
proslulého	proslulý	k2eAgMnSc2d1	proslulý
skladatele	skladatel	k1gMnSc2	skladatel
Salieriho	Salieri	k1gMnSc2	Salieri
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
navázal	navázat	k5eAaPmAgInS	navázat
některá	některý	k3yIgNnPc1	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
celoživotních	celoživotní	k2eAgNnPc2d1	celoživotní
přátelství	přátelství	k1gNnPc2	přátelství
se	s	k7c7	s
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
oporou	opora	k1gFnSc7	opora
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgFnSc4d1	klavírní
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
ukončil	ukončit	k5eAaPmAgInS	ukončit
s	s	k7c7	s
čerstvě	čerstvě	k6eAd1	čerstvě
dopsanou	dopsaný	k2eAgFnSc7d1	dopsaná
symfonií	symfonie	k1gFnSc7	symfonie
č.	č.	k?	č.
1	[number]	k4	1
(	(	kIx(	(
<g/>
D	D	kA	D
82	[number]	k4	82
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schubert	Schubert	k1gMnSc1	Schubert
opustil	opustit	k5eAaPmAgMnS	opustit
konvikt	konvikt	k1gInSc4	konvikt
předčasně	předčasně	k6eAd1	předčasně
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
následkem	následkem	k7c2	následkem
neúspěchů	neúspěch	k1gInPc2	neúspěch
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
učitel	učitel	k1gMnSc1	učitel
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
konal	konat	k5eAaImAgInS	konat
s	s	k7c7	s
nechutí	nechuť	k1gFnSc7	nechuť
a	a	k8xC	a
nevalným	valný	k2eNgInSc7d1	nevalný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaImAgMnS	věnovat
komponování	komponování	k1gNnSc4	komponování
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
dvou	dva	k4xCgNnPc2	dva
učitelských	učitelský	k2eAgNnPc2d1	učitelské
let	léto	k1gNnPc2	léto
pochází	pocházet	k5eAaImIp3nS	pocházet
například	například	k6eAd1	například
první	první	k4xOgFnSc1	první
dokončená	dokončený	k2eAgFnSc1d1	dokončená
opera	opera	k1gFnSc1	opera
Des	des	k1gNnSc2	des
Teufels	Teufelsa	k1gFnPc2	Teufelsa
Lustschloss	Lustschlossa	k1gFnPc2	Lustschlossa
(	(	kIx(	(
<g/>
D	D	kA	D
84	[number]	k4	84
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgFnSc2	první
mše	mše	k1gFnSc2	mše
(	(	kIx(	(
<g/>
F	F	kA	F
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
D	D	kA	D
105	[number]	k4	105
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
některé	některý	k3yIgFnSc2	některý
kompozice	kompozice	k1gFnSc2	kompozice
sedmnáctiletého	sedmnáctiletý	k2eAgMnSc2d1	sedmnáctiletý
mladíka	mladík	k1gMnSc2	mladík
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mistrovské	mistrovský	k2eAgFnPc1d1	mistrovská
úrovně	úroveň	k1gFnPc1	úroveň
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
pódiích	pódium	k1gNnPc6	pódium
koncertních	koncertní	k2eAgFnPc2d1	koncertní
síní	síň	k1gFnPc2	síň
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
byl	být	k5eAaImAgMnS	být
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
společník	společník	k1gMnSc1	společník
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
měl	mít	k5eAaImAgInS	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spolužák	spolužák	k1gMnSc1	spolužák
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Schober	Schober	k1gMnSc1	Schober
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
ubytování	ubytování	k1gNnSc4	ubytování
v	v	k7c6	v
domě	dům	k1gInSc6	dům
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
byl	být	k5eAaImAgMnS	být
Schubert	Schubert	k1gMnSc1	Schubert
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
hostem	host	k1gMnSc7	host
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
zcela	zcela	k6eAd1	zcela
nemajetný	majetný	k2eNgMnSc1d1	nemajetný
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
tak	tak	k9	tak
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
pohostinnosti	pohostinnost	k1gFnSc3	pohostinnost
většinou	většinou	k6eAd1	většinou
nucen	nucen	k2eAgInSc1d1	nucen
se	se	k3xPyFc4	se
starat	starat	k5eAaImF	starat
o	o	k7c4	o
obživu	obživa	k1gFnSc4	obživa
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
věnovat	věnovat	k5eAaPmF	věnovat
skladatelské	skladatelský	k2eAgFnPc4d1	skladatelská
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
významnější	významný	k2eAgFnSc7d2	významnější
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Schubert	Schubert	k1gMnSc1	Schubert
nalezl	naleznout	k5eAaPmAgMnS	naleznout
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1818	[number]	k4	1818
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
angažován	angažovat	k5eAaBmNgInS	angažovat
u	u	k7c2	u
hraběte	hrabě	k1gMnSc2	hrabě
Johanna	Johann	k1gMnSc2	Johann
Esterházyho	Esterházy	k1gMnSc2	Esterházy
jako	jako	k8xC	jako
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Želiezovce	Želiezovec	k1gInSc2	Želiezovec
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
ovšem	ovšem	k9	ovšem
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Zselíz	Zselíz	k1gInSc1	Zselíz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
žákyně	žákyně	k1gFnSc2	žákyně
komtesy	komtesa	k1gFnSc2	komtesa
Caroliny	Carolina	k1gFnSc2	Carolina
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejně	stejně	k6eAd1	stejně
beznadějně	beznadějně	k6eAd1	beznadějně
jako	jako	k8xC	jako
bývaly	bývat	k5eAaImAgFnP	bývat
beznadějné	beznadějný	k2eAgFnPc1d1	beznadějná
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
ostatní	ostatní	k2eAgFnPc1d1	ostatní
milostné	milostný	k2eAgFnPc1d1	milostná
touhy	touha	k1gFnPc1	touha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Schubert	Schubert	k1gMnSc1	Schubert
mimořádně	mimořádně	k6eAd1	mimořádně
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
a	a	k8xC	a
pilný	pilný	k2eAgMnSc1d1	pilný
skladatel	skladatel	k1gMnSc1	skladatel
-	-	kIx~	-
seznam	seznam	k1gInSc1	seznam
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
čítá	čítat	k5eAaImIp3nS	čítat
bezmála	bezmála	k6eAd1	bezmála
tisíc	tisíc	k4xCgInSc4	tisíc
položek	položka	k1gFnPc2	položka
-	-	kIx~	-
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gNnSc3	on
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
příliš	příliš	k6eAd1	příliš
velkého	velký	k2eAgInSc2d1	velký
uměleckého	umělecký	k2eAgInSc2d1	umělecký
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
.	.	kIx.	.
</s>
<s>
Příčin	příčina	k1gFnPc2	příčina
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
nepraktické	praktický	k2eNgFnSc2d1	nepraktická
bohémskosti	bohémskost	k1gFnSc2	bohémskost
a	a	k8xC	a
neochoty	neochota	k1gFnSc2	neochota
samostatně	samostatně	k6eAd1	samostatně
koncertovat	koncertovat	k5eAaImF	koncertovat
až	až	k9	až
po	po	k7c4	po
zdráhání	zdráhání	k1gNnSc4	zdráhání
vydavatelů	vydavatel	k1gMnPc2	vydavatel
publikovat	publikovat	k5eAaBmF	publikovat
s	s	k7c7	s
nejistým	jistý	k2eNgInSc7d1	nejistý
komerčním	komerční	k2eAgInSc7d1	komerční
úspěchem	úspěch	k1gInSc7	úspěch
díla	dílo	k1gNnSc2	dílo
poměrně	poměrně	k6eAd1	poměrně
neznámého	známý	k2eNgMnSc2d1	neznámý
mladého	mladý	k2eAgMnSc2d1	mladý
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Schubertovo	Schubertův	k2eAgNnSc1d1	Schubertovo
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
skladatelské	skladatelský	k2eAgNnSc1d1	skladatelské
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skladatel	skladatel	k1gMnSc1	skladatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
oratorium	oratorium	k1gNnSc4	oratorium
Lazar	Lazar	k1gMnSc1	Lazar
(	(	kIx(	(
<g/>
D	D	kA	D
689	[number]	k4	689
<g/>
)	)	kIx)	)
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc6d1	klavírní
fantazii	fantazie	k1gFnSc6	fantazie
Poutník	poutník	k1gMnSc1	poutník
(	(	kIx(	(
<g/>
D	D	kA	D
760	[number]	k4	760
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rokem	rok	k1gInSc7	rok
1821	[number]	k4	1821
také	také	k9	také
započala	započnout	k5eAaPmAgFnS	započnout
tradice	tradice	k1gFnSc1	tradice
slavných	slavný	k2eAgFnPc2d1	slavná
schubertiád	schubertiáda	k1gFnPc2	schubertiáda
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
večírků	večírek	k1gInPc2	večírek
okruhu	okruh	k1gInSc2	okruh
Schubertových	Schubertových	k2eAgMnPc2d1	Schubertových
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgNnP	hrát
a	a	k8xC	a
zpívala	zpívat	k5eAaImAgNnP	zpívat
skladatelova	skladatelův	k2eAgNnPc1d1	skladatelovo
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
ovšem	ovšem	k9	ovšem
také	také	k9	také
Schubertův	Schubertův	k2eAgInSc1d1	Schubertův
život	život	k1gInSc1	život
dostal	dostat	k5eAaPmAgInS	dostat
tragický	tragický	k2eAgInSc1d1	tragický
rozměr	rozměr	k1gInSc1	rozměr
<g/>
:	:	kIx,	:
Lehkomyslný	lehkomyslný	k2eAgMnSc1d1	lehkomyslný
skladatel	skladatel	k1gMnSc1	skladatel
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
některých	některý	k3yIgMnPc2	některý
přátel	přítel	k1gMnPc2	přítel
začal	začít	k5eAaPmAgInS	začít
až	až	k9	až
příliš	příliš	k6eAd1	příliš
oddávat	oddávat	k5eAaImF	oddávat
bohémskému	bohémský	k2eAgNnSc3d1	bohémské
hýření	hýření	k1gNnSc3	hýření
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
se	se	k3xPyFc4	se
nakazil	nakazit	k5eAaPmAgInS	nakazit
syfilidou	syfilida	k1gFnSc7	syfilida
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
prakticky	prakticky	k6eAd1	prakticky
neléčitelnou	léčitelný	k2eNgFnSc7d1	neléčitelná
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zdraví	zdraví	k1gNnSc1	zdraví
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
s	s	k7c7	s
několika	několik	k4yIc7	několik
výkyvy	výkyv	k1gInPc7	výkyv
postupně	postupně	k6eAd1	postupně
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
a	a	k8xC	a
pokračující	pokračující	k2eAgFnSc1d1	pokračující
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
promítala	promítat	k5eAaImAgFnS	promítat
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
a	a	k8xC	a
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
díla	dílo	k1gNnPc4	dílo
lze	lze	k6eAd1	lze
počítat	počítat	k5eAaImF	počítat
symfonii	symfonie	k1gFnSc4	symfonie
h	h	k?	h
moll	moll	k1gNnSc2	moll
Nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
(	(	kIx(	(
<g/>
D	D	kA	D
759	[number]	k4	759
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
začal	začít	k5eAaPmAgMnS	začít
komponovat	komponovat	k5eAaImF	komponovat
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
<g/>
,	,	kIx,	,
cykly	cyklus	k1gInPc4	cyklus
písní	píseň	k1gFnPc2	píseň
Krásná	krásný	k2eAgFnSc1d1	krásná
mlynářka	mlynářka	k1gFnSc1	mlynářka
(	(	kIx(	(
<g/>
D	D	kA	D
795	[number]	k4	795
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
a	a	k8xC	a
Zimní	zimní	k2eAgFnSc1d1	zimní
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
D	D	kA	D
911	[number]	k4	911
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
č.	č.	k?	č.
14	[number]	k4	14
d	d	k?	d
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
D	D	kA	D
810	[number]	k4	810
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
Německou	německý	k2eAgFnSc4d1	německá
mši	mše	k1gFnSc4	mše
As	as	k9	as
dur	dur	k1gNnPc1	dur
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
(	(	kIx(	(
<g/>
D	D	kA	D
872	[number]	k4	872
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zeslaben	zeslabit	k5eAaPmNgMnS	zeslabit
chronickou	chronický	k2eAgFnSc7d1	chronická
nemocí	nemoc	k1gFnSc7	nemoc
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
příčinou	příčina	k1gFnSc7	příčina
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tyfoidní	tyfoidní	k2eAgFnSc1d1	tyfoidní
horečka	horečka	k1gFnSc1	horečka
nebo	nebo	k8xC	nebo
otrava	otrava	k1gFnSc1	otrava
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
podávala	podávat	k5eAaImAgFnS	podávat
jako	jako	k9	jako
lék	lék	k1gInSc4	lék
syfilitikům	syfilitik	k1gMnPc3	syfilitik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
přání	přání	k1gNnSc2	přání
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
nedaleko	nedaleko	k7c2	nedaleko
hrobu	hrob	k1gInSc2	hrob
svého	svůj	k3xOyFgInSc2	svůj
celoživotního	celoživotní	k2eAgInSc2d1	celoživotní
vzoru	vzor	k1gInSc2	vzor
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
předměstí	předměstí	k1gNnSc6	předměstí
Währing	Währing	k1gInSc1	Währing
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
hroby	hrob	k1gInPc1	hrob
obou	dva	k4xCgMnPc2	dva
hudebních	hudební	k2eAgMnPc2d1	hudební
géniů	génius	k1gMnPc2	génius
přeneseny	přenesen	k2eAgInPc1d1	přenesen
na	na	k7c4	na
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
Ústřední	ústřední	k2eAgInSc4d1	ústřední
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
tiskem	tisk	k1gInSc7	tisk
a	a	k8xC	a
opatřena	opatřit	k5eAaPmNgFnS	opatřit
opusovým	opusový	k2eAgNnSc7d1	opusové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
vydání	vydání	k1gNnSc1	vydání
celé	celý	k2eAgFnSc2d1	celá
Schubertovy	Schubertův	k2eAgFnSc2d1	Schubertova
hudební	hudební	k2eAgFnSc2d1	hudební
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Breitkopf	Breitkopf	k1gInSc1	Breitkopf
und	und	k?	und
Härtel	Härtela	k1gFnPc2	Härtela
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
katalogizaci	katalogizace	k1gFnSc4	katalogizace
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
používá	používat	k5eAaImIp3nS	používat
seznam	seznam	k1gInSc1	seznam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
hudební	hudební	k2eAgMnSc1d1	hudební
vědec	vědec	k1gMnSc1	vědec
Otto	Otto	k1gMnSc1	Otto
Erich	Erich	k1gMnSc1	Erich
Deutsch	Deutsch	k1gMnSc1	Deutsch
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
-	-	kIx~	-
Thematic	Thematice	k1gFnPc2	Thematice
Catalogue	Catalogue	k1gNnPc2	Catalogue
of	of	k?	of
all	all	k?	all
his	his	k1gNnPc2	his
works	works	k1gInSc1	works
in	in	k?	in
chronological	chronologicat	k5eAaPmAgInS	chronologicat
order	order	k1gInSc4	order
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Deutschova	Deutschův	k2eAgInSc2d1	Deutschův
seznamu	seznam	k1gInSc2	seznam
uvádějí	uvádět	k5eAaImIp3nP	uvádět
písmenem	písmeno	k1gNnSc7	písmeno
D	D	kA	D
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nekomerčním	komerční	k2eNgInSc6d1	nekomerční
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgInSc4d1	Musopen
postupně	postupně	k6eAd1	postupně
přibývají	přibývat	k5eAaImIp3nP	přibývat
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgInPc1d1	šiřitelný
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
public	publicum	k1gNnPc2	publicum
domain	domaina	k1gFnPc2	domaina
licencí	licence	k1gFnPc2	licence
<g/>
)	)	kIx)	)
Schubertovy	Schubertův	k2eAgFnSc2d1	Schubertova
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nahráli	nahrát	k5eAaPmAgMnP	nahrát
a	a	k8xC	a
zpracovali	zpracovat	k5eAaPmAgMnP	zpracovat
přední	přední	k2eAgMnPc1d1	přední
světoví	světový	k2eAgMnPc1d1	světový
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1947	[number]	k4	1947
skladatel	skladatel	k1gMnSc1	skladatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Ernst	Ernst	k1gMnSc1	Ernst
Krenek	Krenek	k1gMnSc1	Krenek
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
o	o	k7c6	o
stylu	styl	k1gInSc6	styl
Schuberta	Schubert	k1gMnSc2	Schubert
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
studem	stud	k1gInSc7	stud
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zprvu	zprvu	k6eAd1	zprvu
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
Schuberta	Schubert	k1gMnSc4	Schubert
tehdy	tehdy	k6eAd1	tehdy
dost	dost	k6eAd1	dost
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
múzou	múza	k1gFnSc7	múza
obdařený	obdařený	k2eAgMnSc1d1	obdařený
vynálezce	vynálezce	k1gMnSc1	vynálezce
potěšujících	potěšující	k2eAgFnPc2d1	potěšující
melodií	melodie	k1gFnPc2	melodie
<g/>
...	...	k?	...
avšak	avšak	k8xC	avšak
postrádající	postrádající	k2eAgFnSc4d1	postrádající
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
pátravou	pátravý	k2eAgFnSc4d1	pátravá
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
lišili	lišit	k5eAaImAgMnP	lišit
skuteční	skutečný	k2eAgMnPc1d1	skutečný
mistři	mistr	k1gMnPc1	mistr
jako	jako	k8xC	jako
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
nebo	nebo	k8xC	nebo
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Krenek	Krenek	k1gMnSc1	Krenek
dále	daleko	k6eAd2	daleko
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
úplně	úplně	k6eAd1	úplně
jiného	jiný	k2eAgInSc2d1	jiný
úsudku	úsudek	k1gInSc2	úsudek
po	po	k7c6	po
podrobném	podrobný	k2eAgNnSc6d1	podrobné
studiu	studio	k1gNnSc6	studio
Schubertových	Schubertových	k2eAgFnPc2d1	Schubertových
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
kolegy	kolega	k1gMnSc2	kolega
<g/>
,	,	kIx,	,
skladatele	skladatel	k1gMnSc2	skladatel
Eduarda	Eduard	k1gMnSc2	Eduard
Erdmanna	Erdmann	k1gMnSc2	Erdmann
<g/>
.	.	kIx.	.
</s>
<s>
Krenek	Krenek	k1gInSc1	Krenek
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
klavírní	klavírní	k2eAgFnPc4d1	klavírní
sonáty	sonáta	k1gFnPc4	sonáta
jako	jako	k8xC	jako
na	na	k7c4	na
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schubert	Schubert	k1gMnSc1	Schubert
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
bezstarostný	bezstarostný	k2eAgMnSc1d1	bezstarostný
kovář	kovář	k1gMnSc1	kovář
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
neznal	znát	k5eNaImAgMnS	znát
a	a	k8xC	a
nestaral	starat	k5eNaImAgMnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
řemeslo	řemeslo	k1gNnSc4	řemeslo
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
Schubertova	Schubertův	k2eAgFnSc1d1	Schubertova
klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
pak	pak	k6eAd1	pak
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
Krenkovi	Krenek	k1gMnSc3	Krenek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vystavil	vystavit	k5eAaPmAgMnS	vystavit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jeho	jeho	k3xOp3gFnPc2	jeho
technických	technický	k2eAgFnPc2d1	technická
fines	finesa	k1gFnPc2	finesa
a	a	k8xC	a
poodhalil	poodhalit	k5eAaPmAgMnS	poodhalit
Schuberta	Schubert	k1gMnSc4	Schubert
jako	jako	k9	jako
umělce	umělec	k1gMnSc4	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
smýšlením	smýšlení	k1gNnSc7	smýšlení
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
spokojenosti	spokojenost	k1gFnSc2	spokojenost
s	s	k7c7	s
pouhým	pouhý	k2eAgNnSc7d1	pouhé
litím	lití	k1gNnSc7	lití
svých	svůj	k3xOyFgFnPc2	svůj
mazlivých	mazlivý	k2eAgFnPc2d1	mazlivá
myšlenek	myšlenka	k1gFnPc2	myšlenka
do	do	k7c2	do
tradičních	tradiční	k2eAgFnPc2d1	tradiční
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
jako	jako	k9	jako
umělce	umělec	k1gMnPc4	umělec
s	s	k7c7	s
horlivou	horlivý	k2eAgFnSc7d1	horlivá
chutí	chuť	k1gFnSc7	chuť
experimentovat	experimentovat	k5eAaImF	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
v	v	k7c6	v
Schubertově	Schubertův	k2eAgNnSc6d1	Schubertovo
díle	dílo	k1gNnSc6	dílo
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
paletě	paleta	k1gFnSc6	paleta
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
operách	opera	k1gFnPc6	opera
<g/>
,	,	kIx,	,
liturgické	liturgický	k2eAgFnSc3d1	liturgická
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc3d1	komorní
a	a	k8xC	a
sólové	sólový	k2eAgFnSc3d1	sólová
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
symfonických	symfonický	k2eAgFnPc6d1	symfonická
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejvíce	hodně	k6eAd3	hodně
familiérně	familiérně	k6eAd1	familiérně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
dobrodružnost	dobrodružnost	k1gFnSc1	dobrodružnost
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k6eAd1	především
originálním	originální	k2eAgInSc7d1	originální
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
modulaci	modulace	k1gFnSc4	modulace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
například	například	k6eAd1	například
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
smyčcového	smyčcový	k2eAgInSc2d1	smyčcový
kvintetu	kvintet	k1gInSc2	kvintet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moduluje	modulovat	k5eAaImIp3nS	modulovat
z	z	k7c2	z
C	C	kA	C
dur	dur	k1gNnSc2	dur
přes	přes	k7c4	přes
E	E	kA	E
dur	dur	k1gNnSc4	dur
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Cis	cis	k1gNnSc4	cis
dur	dur	k1gNnSc2	dur
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
neobvyklé	obvyklý	k2eNgFnSc6d1	neobvyklá
instrumentaci	instrumentace	k1gFnSc6	instrumentace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
arpédžové	arpédžový	k2eAgFnSc6d1	arpédžový
sonátě	sonáta	k1gFnSc6	sonáta
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
netradiční	tradiční	k2eNgFnSc6d1	netradiční
notaci	notace	k1gFnSc6	notace
kvintetu	kvintet	k1gInSc2	kvintet
Pstruh	pstruh	k1gMnSc1	pstruh
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
klasickými	klasický	k2eAgFnPc7d1	klasická
sonátovými	sonátový	k2eAgFnPc7d1	sonátová
formami	forma	k1gFnPc7	forma
Beethovena	Beethoven	k1gMnSc2	Beethoven
a	a	k8xC	a
Mozarta	Mozart	k1gMnSc2	Mozart
(	(	kIx(	(
<g/>
jeho	on	k3xPp3gMnSc4	on
raná	raný	k2eAgNnPc1d1	rané
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
zvláště	zvláště	k9	zvláště
5	[number]	k4	5
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
mozartovská	mozartovský	k2eAgFnSc1d1	mozartovská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
formální	formální	k2eAgFnSc1d1	formální
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
chod	chod	k1gInSc1	chod
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
inklinují	inklinovat	k5eAaImIp3nP	inklinovat
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
vzbudit	vzbudit	k5eAaPmF	vzbudit
dojem	dojem	k1gInSc4	dojem
spíše	spíše	k9	spíše
melodického	melodický	k2eAgInSc2d1	melodický
vývoje	vývoj	k1gInSc2	vývoj
než	než	k8xS	než
harmonického	harmonický	k2eAgNnSc2d1	harmonické
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
klasické	klasický	k2eAgFnSc2d1	klasická
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
táhlé	táhlý	k2eAgFnPc4d1	táhlá
romantické	romantický	k2eAgFnPc4d1	romantická
melodie	melodie	k1gFnPc4	melodie
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
jakýsi	jakýsi	k3yIgInSc1	jakýsi
upovídaný	upovídaný	k2eAgInSc1d1	upovídaný
styl	styl	k1gInSc1	styl
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gMnSc1	jeho
9	[number]	k4	9
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc1	symfonie
byla	být	k5eAaImAgFnS	být
popsaná	popsaný	k2eAgFnSc1d1	popsaná
Robertem	Robert	k1gMnSc7	Robert
Schumannem	Schumann	k1gMnSc7	Schumann
jako	jako	k8xC	jako
běh	běh	k1gInSc4	běh
k	k	k7c3	k
"	"	kIx"	"
<g/>
nebeským	nebeský	k2eAgFnPc3d1	nebeská
délkám	délka	k1gFnPc3	délka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Schubertovy	Schubertův	k2eAgFnPc1d1	Schubertova
harmonické	harmonický	k2eAgFnPc1d1	harmonická
inovace	inovace	k1gFnPc1	inovace
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
harmonické	harmonický	k2eAgInPc4d1	harmonický
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
subdominantní	subdominantní	k2eAgFnSc6d1	subdominantní
tónině	tónina	k1gFnSc6	tónina
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
dominantní	dominantní	k2eAgFnSc6d1	dominantní
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
části	část	k1gFnPc1	část
Trout	trout	k5eAaImF	trout
Quintet	Quintet	k1gInSc4	Quintet
-	-	kIx~	-
Pstruh	pstruh	k1gMnSc1	pstruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schubert	Schubert	k1gMnSc1	Schubert
byl	být	k5eAaImAgMnS	být
svými	svůj	k3xOyFgInPc7	svůj
praktikami	praktika	k1gFnPc7	praktika
předchůdcem	předchůdce	k1gMnSc7	předchůdce
běžné	běžný	k2eAgFnSc2d1	běžná
romantické	romantický	k2eAgFnSc2d1	romantická
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
zvyšování	zvyšování	k1gNnSc2	zvyšování
napětí	napětí	k1gNnSc2	napětí
uprostřed	uprostřed	k7c2	uprostřed
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
odloženým	odložený	k2eAgNnSc7d1	odložené
na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Schubertův	Schubertův	k2eAgInSc1d1	Schubertův
kompoziční	kompoziční	k2eAgInSc1d1	kompoziční
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
krátkého	krátký	k2eAgInSc2d1	krátký
života	život	k1gInSc2	život
rychle	rychle	k6eAd1	rychle
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
potenciálních	potenciální	k2eAgNnPc2d1	potenciální
mistrovských	mistrovský	k2eAgNnPc2d1	mistrovské
děl	dělo	k1gNnPc2	dělo
způsobených	způsobený	k2eAgFnPc2d1	způsobená
jeho	jeho	k3xOp3gFnSc7	jeho
brzkou	brzký	k2eAgFnSc7d1	brzká
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
31	[number]	k4	31
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
nejlépe	dobře	k6eAd3	dobře
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
epitafem	epitaf	k1gInSc7	epitaf
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
náhrobním	náhrobní	k2eAgInSc6d1	náhrobní
kameni	kámen	k1gInSc6	kámen
napsaném	napsaný	k2eAgInSc6d1	napsaný
básníkem	básník	k1gMnSc7	básník
Franzem	Franz	k1gMnSc7	Franz
Grillparzerem	Grillparzer	k1gMnSc7	Grillparzer
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
asi	asi	k9	asi
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Tady	tady	k6eAd1	tady
hudba	hudba	k1gFnSc1	hudba
pohřbila	pohřbít	k5eAaPmAgFnS	pohřbít
poklad	poklad	k1gInSc4	poklad
<g/>
,	,	kIx,	,
však	však	k9	však
zvláště	zvláště	k6eAd1	zvláště
své	svůj	k3xOyFgFnPc4	svůj
naděje	naděje	k1gFnPc4	naděje
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Here	Here	k1gNnSc1	Here
music	musice	k1gFnPc2	musice
has	hasit	k5eAaImRp2nS	hasit
buried	buried	k1gInSc1	buried
a	a	k8xC	a
treasure	treasur	k1gMnSc5	treasur
<g/>
,	,	kIx,	,
but	but	k?	but
even	even	k1gMnSc1	even
fairer	fairer	k1gMnSc1	fairer
hopes	hopes	k1gMnSc1	hopes
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Písňová	písňový	k2eAgFnSc1d1	písňová
tvorba	tvorba	k1gFnSc1	tvorba
Schuberta	Schubert	k1gMnSc2	Schubert
provázela	provázet	k5eAaImAgFnS	provázet
celý	celý	k2eAgInSc4d1	celý
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
život	život	k1gInSc4	život
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
prosadil	prosadit	k5eAaPmAgMnS	prosadit
nejdříve	dříve	k6eAd3	dříve
a	a	k8xC	a
zapsal	zapsat	k5eAaPmAgMnS	zapsat
nejhlouběji	hluboko	k6eAd3	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
provozovali	provozovat	k5eAaImAgMnP	provozovat
amatérští	amatérský	k2eAgMnPc1d1	amatérský
hudebníci	hudebník	k1gMnPc1	hudebník
pro	pro	k7c4	pro
vídeňské	vídeňský	k2eAgFnPc4d1	Vídeňská
domácí	domácí	k2eAgFnPc4d1	domácí
společnosti	společnost	k1gFnPc4	společnost
i	i	k8xC	i
velké	velký	k2eAgFnPc4d1	velká
pěvecké	pěvecký	k2eAgFnPc4d1	pěvecká
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gInPc2	on
složil	složit	k5eAaPmAgMnS	složit
asi	asi	k9	asi
600	[number]	k4	600
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
písňové	písňový	k2eAgInPc4d1	písňový
opusy	opus	k1gInPc4	opus
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
:	:	kIx,	:
raná	raný	k2eAgNnPc1d1	rané
mistrovská	mistrovský	k2eAgNnPc1d1	mistrovské
díla	dílo	k1gNnPc1	dílo
Gretchen	Gretchna	k1gFnPc2	Gretchna
am	am	k?	am
Spinnrade	Spinnrad	k1gInSc5	Spinnrad
(	(	kIx(	(
<g/>
Markétka	Markétka	k1gFnSc1	Markétka
u	u	k7c2	u
přeslice	přeslice	k1gFnSc2	přeslice
<g/>
,	,	kIx,	,
D	D	kA	D
118	[number]	k4	118
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Erlkönig	Erlkönig	k1gMnSc1	Erlkönig
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
D	D	kA	D
328	[number]	k4	328
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
Goethův	Goethův	k2eAgInSc4d1	Goethův
text	text	k1gInSc4	text
<g/>
)	)	kIx)	)
písňový	písňový	k2eAgInSc4d1	písňový
cyklus	cyklus	k1gInSc4	cyklus
Die	Die	k1gFnSc2	Die
schöne	schönout	k5eAaPmIp3nS	schönout
Müllerin	Müllerin	k1gInSc1	Müllerin
(	(	kIx(	(
<g/>
Krásná	krásný	k2eAgFnSc1d1	krásná
mlynářka	mlynářka	k1gFnSc1	mlynářka
<g/>
,	,	kIx,	,
D	D	kA	D
795	[number]	k4	795
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Müller	Müller	k1gMnSc1	Müller
písňový	písňový	k2eAgInSc1d1	písňový
cyklus	cyklus	k1gInSc1	cyklus
Winterreise	Winterreise	k1gFnSc1	Winterreise
(	(	kIx(	(
<g/>
Zimní	zimní	k2eAgFnSc1d1	zimní
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
D	D	kA	D
911	[number]	k4	911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
opět	opět	k6eAd1	opět
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Müller	Müller	k1gMnSc1	Müller
Druhou	druhý	k4xOgFnSc7	druhý
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
oblastí	oblast	k1gFnSc7	oblast
Schubertova	Schubertův	k2eAgNnSc2d1	Schubertovo
působení	působení	k1gNnSc2	působení
je	být	k5eAaImIp3nS	být
komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
klavírní	klavírní	k2eAgFnSc1d1	klavírní
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Schubert	Schubert	k1gMnSc1	Schubert
složil	složit	k5eAaPmAgMnS	složit
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
záplavu	záplava	k1gFnSc4	záplava
klavírních	klavírní	k2eAgNnPc2d1	klavírní
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mnohá	mnohé	k1gNnPc1	mnohé
byla	být	k5eAaImAgNnP	být
jen	jen	k6eAd1	jen
příležitostnými	příležitostný	k2eAgFnPc7d1	příležitostná
tanečními	taneční	k2eAgFnPc7d1	taneční
skladbičkami	skladbička	k1gFnPc7	skladbička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nejhlubších	hluboký	k2eAgFnPc2d3	nejhlubší
a	a	k8xC	a
nejpromyšlenějších	promyšlený	k2eAgFnPc2d3	nejpromyšlenější
Schubertových	Schubertových	k2eAgFnPc2d1	Schubertových
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
komorní	komorní	k2eAgFnSc2d1	komorní
tvorby	tvorba	k1gFnSc2	tvorba
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
poslech	poslech	k1gInSc4	poslech
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Forellenquintett	Forellenquintett	k2eAgInSc1d1	Forellenquintett
(	(	kIx(	(
<g/>
Kvintet	kvintet	k1gInSc1	kvintet
A	a	k8xC	a
dur	dur	k1gNnSc1	dur
Pstruh	pstruh	k1gMnSc1	pstruh
<g/>
,	,	kIx,	,
D	D	kA	D
667	[number]	k4	667
<g/>
)	)	kIx)	)
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
d	d	k?	d
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
D	D	kA	D
810	[number]	k4	810
<g/>
)	)	kIx)	)
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Tod	Tod	k1gFnSc2	Tod
und	und	k?	und
das	das	k?	das
Mädchen	Mädchen	k1gInSc1	Mädchen
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnSc1d1	poslední
tři	tři	k4xCgInPc4	tři
<g />
.	.	kIx.	.
</s>
<s>
klavírní	klavírní	k2eAgFnPc1d1	klavírní
sonáty	sonáta	k1gFnPc1	sonáta
(	(	kIx(	(
<g/>
D	D	kA	D
958	[number]	k4	958
<g/>
,	,	kIx,	,
D	D	kA	D
959	[number]	k4	959
<g/>
,	,	kIx,	,
D	D	kA	D
960	[number]	k4	960
<g/>
,	,	kIx,	,
zkomponovány	zkomponován	k2eAgInPc1d1	zkomponován
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
Virtuózní	virtuózní	k2eAgFnSc2d1	virtuózní
fantazie	fantazie	k1gFnSc2	fantazie
Poutník	poutník	k1gMnSc1	poutník
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Wanderer	Wanderer	k1gInSc1	Wanderer
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
D	D	kA	D
760	[number]	k4	760
<g/>
)	)	kIx)	)
Klavírní	klavírní	k2eAgInSc1d1	klavírní
Valses	Valses	k1gInSc1	Valses
Sentimentales	Sentimentalesa	k1gFnPc2	Sentimentalesa
(	(	kIx(	(
<g/>
Sentimentální	sentimentální	k2eAgInPc1d1	sentimentální
valčíky	valčík	k1gInPc1	valčík
<g/>
,	,	kIx,	,
D	D	kA	D
779	[number]	k4	779
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
50	[number]	k4	50
<g/>
)	)	kIx)	)
Impromptus	Impromptus	k1gInSc1	Impromptus
a	a	k8xC	a
Moments	Moments	k1gInSc1	Moments
musicaux	musicaux	k1gInSc4	musicaux
různých	různý	k2eAgNnPc2d1	různé
katalogových	katalogový	k2eAgNnPc2d1	Katalogové
čísel	číslo	k1gNnPc2	číslo
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
;	;	kIx,	;
i	i	k9	i
když	když	k8xS	když
Schubert	Schubert	k1gMnSc1	Schubert
tyto	tento	k3xDgFnPc4	tento
skladby	skladba	k1gFnPc4	skladba
už	už	k6eAd1	už
názvem	název	k1gInSc7	název
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
příležitostné	příležitostný	k2eAgFnSc2d1	příležitostná
lehké	lehký	k2eAgFnSc2d1	lehká
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
Impromptus	Impromptus	k1gInSc1	Impromptus
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bohaté	bohatý	k2eAgMnPc4d1	bohatý
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
vypracované	vypracovaný	k2eAgFnSc2d1	vypracovaná
kompozice	kompozice	k1gFnSc2	kompozice
charakteru	charakter	k1gInSc2	charakter
sonátových	sonátový	k2eAgFnPc2d1	sonátová
vět	věta	k1gFnPc2	věta
Fantazie	fantazie	k1gFnSc1	fantazie
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
f	f	k?	f
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
D	D	kA	D
940	[number]	k4	940
<g/>
)	)	kIx)	)
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
a	a	k8xC	a
jevištní	jevištní	k2eAgFnSc2d1	jevištní
tvorby	tvorba	k1gFnSc2	tvorba
byl	být	k5eAaImAgMnS	být
Schubert	Schubert	k1gMnSc1	Schubert
jako	jako	k8xS	jako
tehdy	tehdy	k6eAd1	tehdy
nepříliš	příliš	k6eNd1	příliš
známý	známý	k2eAgMnSc1d1	známý
skladatel	skladatel	k1gMnSc1	skladatel
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
nevýhodě	nevýhoda	k1gFnSc6	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mnohá	mnohý	k2eAgNnPc4d1	mnohé
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
už	už	k6eAd1	už
ve	v	k7c4	v
chvíli	chvíle	k1gFnSc4	chvíle
zrodu	zrod	k1gInSc2	zrod
zabita	zabit	k2eAgFnSc1d1	zabita
podřadnými	podřadný	k2eAgNnPc7d1	podřadné
librety	libreto	k1gNnPc7	libreto
<g/>
,	,	kIx,	,
na	na	k7c4	na
která	který	k3yRgNnPc4	který
komponoval	komponovat	k5eAaImAgMnS	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
však	však	k9	však
najdeme	najít	k5eAaPmIp1nP	najít
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
<g/>
:	:	kIx,	:
Symfonie	symfonie	k1gFnSc1	symfonie
h	h	k?	h
moll	moll	k1gNnSc2	moll
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Unvollendete	Unvollendete	k1gFnSc1	Unvollendete
(	(	kIx(	(
<g/>
D	D	kA	D
759	[number]	k4	759
<g/>
)	)	kIx)	)
-	-	kIx~	-
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
počítána	počítat	k5eAaImNgFnS	počítat
jako	jako	k9	jako
sedmá	sedmý	k4xOgFnSc1	sedmý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jako	jako	k9	jako
osmá	osmý	k4xOgFnSc1	osmý
Schubertova	Schubertův	k2eAgFnSc1d1	Schubertova
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
autorovými	autorův	k2eAgFnPc7d1	autorova
symfoniemi	symfonie	k1gFnPc7	symfonie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
předposlední	předposlední	k2eAgMnSc1d1	předposlední
<g/>
.	.	kIx.	.
</s>
<s>
Schubert	Schubert	k1gMnSc1	Schubert
dokončil	dokončit	k5eAaPmAgMnS	dokončit
dvě	dva	k4xCgFnPc4	dva
věty	věta	k1gFnPc4	věta
této	tento	k3xDgFnSc2	tento
symfonie	symfonie	k1gFnSc2	symfonie
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgInS	zanechat
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
náčrt	náčrt	k1gInSc1	náčrt
věty	věta	k1gFnSc2	věta
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
Scherza	scherzo	k1gNnSc2	scherzo
<g/>
.	.	kIx.	.
</s>
<s>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Messe	Messe	k1gFnSc1	Messe
(	(	kIx(	(
<g/>
Německá	německý	k2eAgFnSc1d1	německá
mše	mše	k1gFnSc1	mše
As	as	k1gNnSc2	as
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
D	D	kA	D
872	[number]	k4	872
<g/>
)	)	kIx)	)
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
baletu	balet	k1gInSc3	balet
Rosamunde	Rosamund	k1gInSc5	Rosamund
(	(	kIx(	(
<g/>
Rosamunda	Rosamunda	k1gFnSc1	Rosamunda
<g/>
,	,	kIx,	,
D	D	kA	D
797	[number]	k4	797
<g/>
)	)	kIx)	)
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
oratorium	oratorium	k1gNnSc1	oratorium
Lazarus	Lazarus	k1gMnSc1	Lazarus
(	(	kIx(	(
<g/>
Lazar	Lazar	k1gMnSc1	Lazar
<g/>
,	,	kIx,	,
D	D	kA	D
689	[number]	k4	689
<g/>
)	)	kIx)	)
Mše	mše	k1gFnSc1	mše
č.	č.	k?	č.
2	[number]	k4	2
G	G	kA	G
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
D	D	kA	D
167	[number]	k4	167
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Kyrie	Kyrie	k1gNnPc6	Kyrie
-	-	kIx~	-
Gloria	Gloria	k1gFnSc1	Gloria
-	-	kIx~	-
Credo	Credo	k1gNnSc1	Credo
-	-	kIx~	-
Sanctus	Sanctus	k1gMnSc1	Sanctus
-	-	kIx~	-
Benedictus	Benedictus	k1gMnSc1	Benedictus
-	-	kIx~	-
Agnus	Agnus	k1gMnSc1	Agnus
Dei	Dei	k1gMnSc2	Dei
Impromptu	impromptu	k1gNnSc2	impromptu
B	B	kA	B
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
posth	posth	k1gMnSc1	posth
<g/>
.	.	kIx.	.
142	[number]	k4	142
(	(	kIx(	(
<g/>
D	D	kA	D
935	[number]	k4	935
<g/>
)	)	kIx)	)
č.	č.	k?	č.
3	[number]	k4	3
-	-	kIx~	-
Andante	andante	k1gNnSc1	andante
(	(	kIx(	(
<g/>
téma	téma	k1gNnSc1	téma
<g/>
)	)	kIx)	)
-	-	kIx~	-
Variace	variace	k1gFnSc1	variace
1	[number]	k4	1
-	-	kIx~	-
Variace	variace	k1gFnSc1	variace
2	[number]	k4	2
-	-	kIx~	-
Variace	variace	k1gFnSc1	variace
3	[number]	k4	3
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnSc1	variace
4	[number]	k4	4
-	-	kIx~	-
Variace	variace	k1gFnSc1	variace
5	[number]	k4	5
-	-	kIx~	-
Variace	variace	k1gFnSc1	variace
6	[number]	k4	6
Po	po	k7c6	po
Schubertovi	Schubert	k1gMnSc6	Schubert
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
pár	pár	k1gInSc1	pár
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
railjet	railjeta	k1gFnPc2	railjeta
(	(	kIx(	(
<g/>
74	[number]	k4	74
<g/>
/	/	kIx~	/
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
společností	společnost	k1gFnPc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
ÖBB	ÖBB	kA	ÖBB
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Wien	Wien	k1gNnSc1	Wien
-	-	kIx~	-
Bruck	Bruck	k1gInSc1	Bruck
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Mur	mur	k1gInSc1	mur
-	-	kIx~	-
Graz	Graz	k1gInSc1	Graz
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
