<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
stavba	stavba	k1gFnSc1	stavba
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
pevným	pevný	k2eAgInSc7d1	pevný
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
obvodovými	obvodový	k2eAgFnPc7d1	obvodová
stěnami	stěna	k1gFnPc7	stěna
a	a	k8xC	a
střešní	střešní	k2eAgFnSc7d1	střešní
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
?	?	kIx.	?
</s>
