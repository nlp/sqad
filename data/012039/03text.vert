<p>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
stavba	stavba	k1gFnSc1	stavba
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
pevným	pevný	k2eAgInSc7d1	pevný
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
prostorově	prostorově	k6eAd1	prostorově
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
a	a	k8xC	a
navenek	navenek	k6eAd1	navenek
převážně	převážně	k6eAd1	převážně
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
obvodovými	obvodový	k2eAgFnPc7d1	obvodová
stěnami	stěna	k1gFnPc7	stěna
a	a	k8xC	a
střešní	střešní	k2eAgFnSc7d1	střešní
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ohraničenými	ohraničený	k2eAgInPc7d1	ohraničený
užitkovými	užitkový	k2eAgInPc7d1	užitkový
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budovy	budova	k1gFnPc1	budova
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozličných	rozličný	k2eAgInPc2d1	rozličný
typů	typ	k1gInPc2	typ
–	–	k?	–
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
přístřešků	přístřešek	k1gInPc2	přístřešek
poskytujících	poskytující	k2eAgInPc2d1	poskytující
pouze	pouze	k6eAd1	pouze
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
složité	složitý	k2eAgInPc4d1	složitý
komplexy	komplex	k1gInPc4	komplex
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takto	takto	k6eAd1	takto
složitých	složitý	k2eAgFnPc6d1	složitá
budovách	budova	k1gFnPc6	budova
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
např.	např.	kA	např.
regulovat	regulovat	k5eAaImF	regulovat
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
klimatizace	klimatizace	k1gFnSc1	klimatizace
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
nebo	nebo	k8xC	nebo
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
aktivity	aktivita	k1gFnPc4	aktivita
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
budov	budova	k1gFnPc2	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
návrh	návrh	k1gInSc4	návrh
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnPc1d1	stará
jako	jako	k8xC	jako
lidstvo	lidstvo	k1gNnSc1	lidstvo
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Architekti	architekt	k1gMnPc1	architekt
dnes	dnes	k6eAd1	dnes
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
většinu	většina	k1gFnSc4	většina
větších	veliký	k2eAgFnPc2d2	veliký
budov	budova	k1gFnPc2	budova
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
týmu	tým	k1gInSc2	tým
složeného	složený	k2eAgInSc2d1	složený
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
specializovaných	specializovaný	k2eAgMnPc2d1	specializovaný
stavebních	stavební	k2eAgMnPc2d1	stavební
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
práci	práce	k1gFnSc4	práce
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsoby	způsob	k1gInPc1	způsob
dopravy	doprava	k1gFnSc2	doprava
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
výtahy	výtah	k1gInPc1	výtah
</s>
</p>
<p>
<s>
eskalátory	eskalátor	k1gInPc1	eskalátor
</s>
</p>
<p>
<s>
schodištěZpůsoby	schodištěZpůsoba	k1gFnPc1	schodištěZpůsoba
propojování	propojování	k1gNnSc2	propojování
budov	budova	k1gFnPc2	budova
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
chodby	chodba	k1gFnPc1	chodba
</s>
</p>
<p>
<s>
podzemní	podzemní	k2eAgNnSc4d1	podzemní
město	město	k1gNnSc4	město
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
autonomní	autonomní	k2eAgFnSc1d1	autonomní
budova	budova	k1gFnSc1	budova
</s>
</p>
<p>
<s>
ekologická	ekologický	k2eAgFnSc1d1	ekologická
budova	budova	k1gFnSc1	budova
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgFnSc1d1	přírodní
budova	budova	k1gFnSc1	budova
</s>
</p>
<p>
<s>
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
</s>
</p>
<p>
<s>
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
stavba	stavba	k1gFnSc1	stavba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
stavba	stavba	k1gFnSc1	stavba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
palác	palác	k1gInSc1	palác
</s>
</p>
<p>
<s>
panelový	panelový	k2eAgInSc1d1	panelový
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
budova	budova	k1gFnSc1	budova
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
architektura	architektura	k1gFnSc1	architektura
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
</s>
</p>
<p>
<s>
Automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
stavba	stavba	k1gFnSc1	stavba
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgFnSc4d1	tradiční
architekturu	architektura	k1gFnSc4	architektura
–	–	k?	–
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Emporis	Emporis	k1gFnSc1	Emporis
–	–	k?	–
Světová	světový	k2eAgFnSc1d1	světová
databáze	databáze	k1gFnSc1	databáze
budov	budova	k1gFnPc2	budova
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
