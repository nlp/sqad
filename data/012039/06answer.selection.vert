<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
stavba	stavba	k1gFnSc1	stavba
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
pevným	pevný	k2eAgInSc7d1	pevný
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
prostorově	prostorově	k6eAd1	prostorově
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
a	a	k8xC	a
navenek	navenek	k6eAd1	navenek
převážně	převážně	k6eAd1	převážně
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
obvodovými	obvodový	k2eAgFnPc7d1	obvodová
stěnami	stěna	k1gFnPc7	stěna
a	a	k8xC	a
střešní	střešní	k2eAgFnSc7d1	střešní
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ohraničenými	ohraničený	k2eAgInPc7d1	ohraničený
užitkovými	užitkový	k2eAgInPc7d1	užitkový
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
