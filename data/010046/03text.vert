<p>
<s>
Moby	Moba	k1gFnPc1	Moba
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Richard	Richard	k1gMnSc1	Richard
Melville	Melville	k1gNnSc1	Melville
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgInS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívku	přezdívka	k1gFnSc4	přezdívka
Moby	Moba	k1gFnSc2	Moba
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
rodiče	rodič	k1gMnPc1	rodič
dali	dát	k5eAaPmAgMnP	dát
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Moby	Moba	k1gFnPc1	Moba
Dick	Dicka	k1gFnPc2	Dicka
<g/>
)	)	kIx)	)
od	od	k7c2	od
Hermana	Herman	k1gMnSc2	Herman
Melvilla	Melvill	k1gMnSc2	Melvill
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
praprapraprastrýce	praprapraprastrýko	k6eAd1	praprapraprastrýko
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
,	,	kIx,	,
Barracuda	Barracuda	k1gMnSc1	Barracuda
<g/>
,	,	kIx,	,
Brainstorm	Brainstorm	k1gInSc1	Brainstorm
<g/>
,	,	kIx,	,
UHF	UHF	kA	UHF
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Brotherhood	Brotherhood	k1gInSc1	Brotherhood
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Cake	Cake	k1gInSc1	Cake
<g/>
,	,	kIx,	,
Lopez	Lopez	k1gInSc1	Lopez
<g/>
,	,	kIx,	,
Mindstorm	Mindstorm	k1gInSc1	Mindstorm
<g/>
,	,	kIx,	,
Schaumgummi	Schaumgum	k1gFnPc7	Schaumgum
<g/>
,	,	kIx,	,
Pippy	Pipp	k1gInPc1	Pipp
Baliunas	Baliunasa	k1gFnPc2	Baliunasa
a	a	k8xC	a
Moby	Moba	k1gFnSc2	Moba
&	&	k?	&
The	The	k1gMnSc1	The
Void	Void	k1gMnSc1	Void
Pacific	Pacific	k1gMnSc1	Pacific
Choir	Choir	k1gMnSc1	Choir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
světového	světový	k2eAgInSc2d1	světový
zájmu	zájem	k1gInSc2	zájem
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Play	play	k0	play
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
singlům	singl	k1gInPc3	singl
"	"	kIx"	"
<g/>
Why	Why	k1gFnSc1	Why
Does	Doesa	k1gFnPc2	Doesa
My	my	k3xPp1nPc1	my
Heart	Heart	k1gInSc1	Heart
Feel	Feel	k1gMnSc1	Feel
So	So	kA	So
Bad	Bad	k1gMnSc1	Bad
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Porcelain	Porcelain	k1gInSc1	Porcelain
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
byla	být	k5eAaImAgFnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
platinová	platinový	k2eAgFnSc1d1	platinová
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
devět	devět	k4xCc1	devět
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
navázal	navázat	k5eAaPmAgInS	navázat
alby	alba	k1gFnSc2	alba
18	[number]	k4	18
a	a	k8xC	a
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
nejnovější	nový	k2eAgNnSc1d3	nejnovější
album	album	k1gNnSc1	album
Everything	Everything	k1gInSc1	Everything
Was	Was	k1gMnSc1	Was
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
,	,	kIx,	,
and	and	k?	and
Nothing	Nothing	k1gInSc4	Nothing
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
získalo	získat	k5eAaPmAgNnS	získat
převážně	převážně	k6eAd1	převážně
kladné	kladný	k2eAgFnPc4d1	kladná
recenze	recenze	k1gFnPc4	recenze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Melville	Melville	k1gFnSc2	Melville
Hall	Hall	k1gMnSc1	Hall
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
McBrideová	McBrideová	k1gFnSc1	McBrideová
byla	být	k5eAaImAgFnS	být
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestřička	sestřička	k1gFnSc1	sestřička
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
otec	otec	k1gMnSc1	otec
James	James	k1gMnSc1	James
Frederick	Frederick	k1gMnSc1	Frederick
Hall	Hall	k1gMnSc1	Hall
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Richardovi	Richard	k1gMnSc3	Richard
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
pouhé	pouhý	k2eAgInPc1d1	pouhý
dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
Darienu	Darien	k1gInSc6	Darien
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Shrnutí	shrnutí	k1gNnSc3	shrnutí
kariérní	kariérní	k2eAgFnSc2d1	kariérní
biografie	biografie	k1gFnSc2	biografie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
kapel	kapela	k1gFnPc2	kapela
–	–	k?	–
Ultra	ultra	k2eAgInSc4d1	ultra
Vivid	Vivid	k1gInSc4	Vivid
Scene	Scen	k1gInSc5	Scen
<g/>
,	,	kIx,	,
punkové	punkový	k2eAgFnSc2d1	punková
The	The	k1gFnSc2	The
Vatican	Vaticana	k1gFnPc2	Vaticana
Commandos	Commandosa	k1gFnPc2	Commandosa
a	a	k8xC	a
anarchistické	anarchistický	k2eAgFnSc2d1	anarchistická
Flipper	Flipper	k1gInSc4	Flipper
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
stal	stát	k5eAaPmAgInS	stát
dýdžejem	dýdžej	k1gInSc7	dýdžej
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
popularita	popularita	k1gFnSc1	popularita
strmě	strmě	k6eAd1	strmě
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Go	Go	k1gFnSc1	Go
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
byly	být	k5eAaImAgFnP	být
také	také	k9	také
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Drop	drop	k1gMnSc1	drop
a	a	k8xC	a
Beat	beat	k1gInSc1	beat
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Next	Next	k2eAgMnSc1d1	Next
Is	Is	k1gMnSc1	Is
the	the	k?	the
E	E	kA	E
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
mu	on	k3xPp3gMnSc3	on
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Instinct	Instinct	k2eAgInSc4d1	Instinct
Records	Records	k1gInSc4	Records
vyšla	vyjít	k5eAaPmAgFnS	vyjít
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
–	–	k?	–
Moby	Mob	k1gMnPc7	Mob
<g/>
,	,	kIx,	,
Early	earl	k1gMnPc7	earl
Underground	underground	k1gInSc1	underground
a	a	k8xC	a
Ambient	Ambient	k1gInSc1	Ambient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
věhlasnějším	věhlasný	k2eAgFnPc3d2	věhlasnější
firmám	firma	k1gFnPc3	firma
–	–	k?	–
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vydával	vydávat	k5eAaImAgInS	vydávat
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
Mute	Mute	k?	Mute
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
pak	pak	k6eAd1	pak
u	u	k7c2	u
labelu	label	k1gInSc2	label
Elektra	Elektrum	k1gNnSc2	Elektrum
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gFnSc2	Is
Wrong	Wronga	k1gFnPc2	Wronga
mělo	mít	k5eAaImAgNnS	mít
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
ohlas	ohlas	k1gInSc4	ohlas
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
získalo	získat	k5eAaPmAgNnS	získat
od	od	k7c2	od
MTV	MTV	kA	MTV
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
a	a	k8xC	a
velice	velice	k6eAd1	velice
vřele	vřele	k6eAd1	vřele
ho	on	k3xPp3gMnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
jak	jak	k8xS	jak
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
kritici	kritik	k1gMnPc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
poprvé	poprvé	k6eAd1	poprvé
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
hudbu	hudba	k1gFnSc4	hudba
nejednoho	nejeden	k4xCyIgInSc2	nejeden
žánru	žánr	k1gInSc2	žánr
<g/>
:	:	kIx,	:
prvky	prvek	k1gInPc1	prvek
house	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
ambient	ambient	k1gInSc1	ambient
<g/>
,	,	kIx,	,
punk	punk	k1gInSc1	punk
i	i	k8xC	i
jungle	jungle	k1gInSc1	jungle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1996	[number]	k4	1996
vydal	vydat	k5eAaPmAgMnS	vydat
remixové	remixový	k2eAgNnSc1d1	remixové
album	album	k1gNnSc1	album
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
–	–	k?	–
DJ	DJ	kA	DJ
Mix	mix	k1gInSc1	mix
Album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
o	o	k7c4	o
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
sedmipísňovou	sedmipísňový	k2eAgFnSc4d1	sedmipísňový
nahrávku	nahrávka	k1gFnSc4	nahrávka
The	The	k1gFnSc2	The
End	End	k1gFnSc2	End
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
ne	ne	k9	ne
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Moby	Moba	k1gFnSc2	Moba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
než	než	k8xS	než
album	album	k1gNnSc1	album
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgMnSc1d1	následující
Animal	animal	k1gMnSc1	animal
Rights	Rightsa	k1gFnPc2	Rightsa
<g/>
.	.	kIx.	.
</s>
<s>
Techno	Techno	k6eAd1	Techno
nahradil	nahradit	k5eAaPmAgInS	nahradit
hard	hard	k6eAd1	hard
rock	rock	k1gInSc1	rock
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
Moby	Mob	k2eAgInPc1d1	Mob
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
ujal	ujmout	k5eAaPmAgMnS	ujmout
zpěvu	zpěv	k1gInSc3	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
písním	píseň	k1gFnPc3	píseň
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
patří	patřit	k5eAaImIp3nS	patřit
"	"	kIx"	"
<g/>
Come	Come	k1gInSc1	Come
On	on	k3xPp3gInSc1	on
<g/>
,	,	kIx,	,
Baby	baba	k1gFnPc4	baba
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
When	When	k1gInSc1	When
I	i	k9	i
Reach	Reach	k1gInSc1	Reach
for	forum	k1gNnPc2	forum
My	my	k3xPp1nPc1	my
Revolver	revolver	k1gInSc1	revolver
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
limitované	limitovaný	k2eAgFnSc6d1	limitovaná
edici	edice	k1gFnSc6	edice
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
Little	Little	k1gFnSc2	Little
Idiot	idiot	k1gMnSc1	idiot
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
symfonické	symfonický	k2eAgFnSc2d1	symfonická
a	a	k8xC	a
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
diskografie	diskografie	k1gFnSc2	diskografie
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	nejvíce	k6eAd1	nejvíce
kritizováno	kritizován	k2eAgNnSc4d1	kritizováno
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
I	i	k8xC	i
Like	Like	k1gInSc4	Like
to	ten	k3xDgNnSc1	ten
Score	Scor	k1gMnSc5	Scor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ústřední	ústřední	k2eAgFnSc1d1	ústřední
píseň	píseň	k1gFnSc1	píseň
k	k	k7c3	k
bondovce	bondovce	k?	bondovce
Zítřek	zítřek	k1gInSc1	zítřek
nikdy	nikdy	k6eAd1	nikdy
neumírá	umírat	k5eNaImIp3nS	umírat
nebo	nebo	k8xC	nebo
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Go	Go	k1gFnSc4	Go
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Městečko	městečko	k1gNnSc4	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc1	album
Play	play	k0	play
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
Moby	Moba	k1gFnPc1	Moba
pracoval	pracovat	k5eAaImAgInS	pracovat
celých	celý	k2eAgInPc2d1	celý
dvanáct	dvanáct	k4xCc4	dvanáct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
taneční	taneční	k2eAgFnPc4d1	taneční
písně	píseň	k1gFnPc4	píseň
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
černošské	černošský	k2eAgFnSc2d1	černošská
hudby	hudba	k1gFnSc2	hudba
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
hip	hip	k0	hip
hopu	hopus	k1gInSc2	hopus
i	i	k8xC	i
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
mnoha	mnoho	k4c2	mnoho
písní	píseň	k1gFnPc2	píseň
tvoří	tvořit	k5eAaImIp3nS	tvořit
nahrávky	nahrávka	k1gFnPc4	nahrávka
pořízené	pořízený	k2eAgFnSc2d1	pořízená
folkloristou	folklorista	k1gMnSc7	folklorista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Alanem	Alan	k1gMnSc7	Alan
Lomaxem	Lomax	k1gInSc7	Lomax
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
Johnem	John	k1gMnSc7	John
Alanem	Alan	k1gMnSc7	Alan
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
především	především	k9	především
v	v	k7c6	v
trestaneckých	trestanecký	k2eAgInPc6d1	trestanecký
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moba	k1gFnPc1	Moba
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
písních	píseň	k1gFnPc6	píseň
i	i	k8xC	i
sám	sám	k3xTgMnSc1	sám
zpívá	zpívat	k5eAaImIp3nS	zpívat
a	a	k8xC	a
bubnuje	bubnovat	k5eAaImIp3nS	bubnovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Go	Go	k1gFnSc1	Go
<g/>
"	"	kIx"	"
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
===	===	k?	===
</s>
</p>
<p>
<s>
Mobyho	Mobyze	k6eAd1	Mobyze
první	první	k4xOgNnSc1	první
sólové	sólový	k2eAgNnSc1d1	sólové
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
sledováno	sledovat	k5eAaImNgNnS	sledovat
i	i	k8xC	i
hudebníkovým	hudebníkův	k2eAgMnSc7d1	hudebníkův
budoucím	budoucí	k2eAgMnSc7d1	budoucí
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
manažerem	manažer	k1gMnSc7	manažer
Erickem	Erick	k1gInSc7	Erick
Härleem	Härleus	k1gMnSc7	Härleus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgMnS	popsat
na	na	k7c4	na
HitQuarters	HitQuarters	k1gInSc4	HitQuarters
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
úžasná	úžasný	k2eAgFnSc1d1	úžasná
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ta	ten	k3xDgFnSc1	ten
show	show	k1gFnSc1	show
byla	být	k5eAaImAgFnS	být
prošpikována	prošpikovat	k5eAaPmNgFnS	prošpikovat
technickými	technický	k2eAgFnPc7d1	technická
nehodami	nehoda	k1gFnPc7	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
zapůsobilo	zapůsobit	k5eAaPmAgNnS	zapůsobit
a	a	k8xC	a
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Moby	Moby	k1gInPc1	Moby
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
singly	singl	k1gInPc4	singl
vydal	vydat	k5eAaPmAgMnS	vydat
pod	pod	k7c4	pod
Instinct	Instinct	k2eAgInSc4d1	Instinct
Records	Records	k1gInSc4	Records
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
pseudonymy	pseudonym	k1gInPc7	pseudonym
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Barracuda	Barracuda	k1gFnSc1	Barracuda
<g/>
,	,	kIx,	,
Brainstorm	Brainstorm	k1gInSc1	Brainstorm
či	či	k8xC	či
UHF	UHF	kA	UHF
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
byl	být	k5eAaImAgInS	být
komerční	komerční	k2eAgInSc1d1	komerční
neúspěch	neúspěch	k1gInSc1	neúspěch
<g/>
;	;	kIx,	;
rapová	rapový	k2eAgFnSc1d1	rapová
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
vokály	vokál	k1gInPc7	vokál
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Macka	Macek	k1gMnSc2	Macek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Time	Tim	k1gInSc2	Tim
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Up	Up	k1gFnPc7	Up
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnPc1d1	představující
několik	několik	k4yIc4	několik
remixů	remix	k1gInPc2	remix
a	a	k8xC	a
stopy	stopa	k1gFnSc2	stopa
remixingu	remixing	k1gInSc2	remixing
<g/>
.	.	kIx.	.
</s>
<s>
Prodáno	prodán	k2eAgNnSc1d1	prodáno
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Moby	Moba	k1gFnSc2	Moba
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
Mobility	mobilita	k1gFnSc2	mobilita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k9	právě
strana	strana	k1gFnSc1	strana
B	B	kA	B
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Go	Go	k1gFnSc1	Go
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
Moby	Mobum	k1gNnPc7	Mobum
dokázal	dokázat	k5eAaPmAgInS	dokázat
poprvé	poprvé	k6eAd1	poprvé
opravdu	opravdu	k6eAd1	opravdu
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
.	.	kIx.	.
</s>
<s>
Mix	mix	k1gInSc1	mix
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Woodtick	Woodtick	k1gInSc1	Woodtick
Mix	mix	k1gInSc1	mix
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
používající	používající	k2eAgInPc4d1	používající
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
hudebního	hudební	k2eAgInSc2d1	hudební
motivu	motiv	k1gInSc2	motiv
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Městečko	městečko	k1gNnSc4	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1991	[number]	k4	1991
do	do	k7c2	do
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
deseti	deset	k4xCc2	deset
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
popových	popový	k2eAgFnPc2d1	popová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
tak	tak	k6eAd1	tak
Mobymu	Mobym	k1gInSc2	Mobym
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c4	v
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
televizní	televizní	k2eAgNnSc4d1	televizní
show	show	k1gNnSc4	show
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc1	Pops
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
eponymní	eponymní	k2eAgNnSc4d1	eponymní
album	album	k1gNnSc4	album
Moby	Moba	k1gFnSc2	Moba
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
singlů	singl	k1gInPc2	singl
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1993	[number]	k4	1993
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
Next	Next	k2eAgMnSc1d1	Next
Is	Is	k1gMnSc1	Is
the	the	k?	the
E	E	kA	E
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Thousand	Thousand	k1gInSc1	Thousand
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
tempem	tempo	k1gNnSc7	tempo
tisíc	tisíc	k4xCgInPc2	tisíc
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
a	a	k8xC	a
1992	[number]	k4	1992
remixoval	remixovat	k5eAaPmAgInS	remixovat
písně	píseň	k1gFnSc2	píseň
od	od	k7c2	od
The	The	k1gFnSc2	The
B-	B-	k1gFnSc2	B-
<g/>
52	[number]	k4	52
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
The	The	k1gFnSc3	The
Prodigy	Prodiga	k1gFnSc2	Prodiga
<g/>
,	,	kIx,	,
Orbital	orbital	k1gInSc4	orbital
<g/>
,	,	kIx,	,
Erasure	Erasur	k1gMnSc5	Erasur
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc4	Michael
Jacksona	Jackson	k1gMnSc4	Jackson
a	a	k8xC	a
Ten	ten	k3xDgMnSc1	ten
City	City	k1gFnSc4	City
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
rap	rap	k1gMnSc1	rap
pro	pro	k7c4	pro
nahrávku	nahrávka	k1gFnSc4	nahrávka
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
Curse	Curse	k1gFnPc4	Curse
<g/>
"	"	kIx"	"
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Recoil	Recoila	k1gFnPc2	Recoila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
Rights	Rights	k1gInSc1	Rights
a	a	k8xC	a
I	i	k9	i
Like	Like	k1gFnSc1	Like
to	ten	k3xDgNnSc4	ten
Score	Scor	k1gInSc5	Scor
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Moby	Moba	k1gFnSc2	Moba
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Mute	Mute	k?	Mute
Records	Records	k1gInSc1	Records
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Move	Mov	k1gInSc2	Mov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
opět	opět	k6eAd1	opět
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc1	Pops
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Následně	následně	k6eAd1	následně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
u	u	k7c2	u
Mute	Mute	k?	Mute
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kopie	kopie	k1gFnSc1	kopie
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
vyšly	vyjít	k5eAaPmAgInP	vyjít
se	se	k3xPyFc4	se
speciálním	speciální	k2eAgInSc7d1	speciální
bonusovým	bonusový	k2eAgInSc7d1	bonusový
instrumentálním	instrumentální	k2eAgInSc7d1	instrumentální
CD	CD	kA	CD
zvaném	zvaný	k2eAgInSc6d1	zvaný
Underwater	Underwatra	k1gFnPc2	Underwatra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
pět	pět	k4xCc1	pět
ambientních	ambientní	k2eAgFnPc2d1	ambientní
skladeb	skladba	k1gFnPc2	skladba
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
43	[number]	k4	43
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
získalo	získat	k5eAaPmAgNnS	získat
přízeň	přízeň	k1gFnSc4	přízeň
kritiky	kritika	k1gFnSc2	kritika
(	(	kIx(	(
<g/>
časopis	časopis	k1gInSc1	časopis
Spin	spina	k1gFnPc2	spina
jej	on	k3xPp3gMnSc4	on
nazvalo	nazvat	k5eAaBmAgNnS	nazvat
"	"	kIx"	"
<g/>
Albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
oslavilo	oslavit	k5eAaPmAgNnS	oslavit
několik	několik	k4yIc1	několik
komerčních	komerční	k2eAgInPc2d1	komerční
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Moby	Moby	k1gInPc4	Moby
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
nevídaný	vídaný	k2eNgInSc4d1	nevídaný
úspěch	úspěch	k1gInSc4	úspěch
dvojité	dvojitý	k2eAgNnSc1d1	dvojité
album	album	k1gNnSc1	album
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
–	–	k?	–
Mixed	Mixed	k1gMnSc1	Mixed
and	and	k?	and
Remixed	Remixed	k1gMnSc1	Remixed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zklamaný	zklamaný	k2eAgInSc1d1	zklamaný
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
od	od	k7c2	od
hudebních	hudební	k2eAgNnPc2d1	hudební
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c6	o
zahrnutí	zahrnutí	k1gNnSc6	zahrnutí
umělcovy	umělcův	k2eAgFnSc2d1	umělcova
nové	nový	k2eAgFnSc2d1	nová
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
odmítala	odmítat	k5eAaImAgFnS	odmítat
jej	on	k3xPp3gMnSc4	on
brát	brát	k5eAaImF	brát
příliš	příliš	k6eAd1	příliš
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Moby	Moba	k1gFnPc1	Moba
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydat	vydat	k5eAaPmF	vydat
punk	punk	k1gInSc4	punk
rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
Animal	animal	k1gMnSc1	animal
Rights	Rights	k1gInSc1	Rights
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
cover	cover	k1gInSc4	cover
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
When	When	k1gInSc1	When
I	i	k9	i
Reach	Reach	k1gInSc1	Reach
for	forum	k1gNnPc2	forum
My	my	k3xPp1nPc1	my
Revolver	revolver	k1gInSc4	revolver
<g/>
"	"	kIx"	"
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Mission	Mission	k1gInSc1	Mission
of	of	k?	of
Burma	Burm	k1gMnSc2	Burm
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
následováno	následován	k2eAgNnSc1d1	následováno
turné	turné	k1gNnSc1	turné
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Red	Red	k1gFnPc2	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
a	a	k8xC	a
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Come	Com	k1gMnSc2	Com
On	on	k3xPp3gMnSc1	on
Baby	baby	k1gNnPc6	baby
<g/>
"	"	kIx"	"
z	z	k7c2	z
Animal	animal	k1gMnSc1	animal
Rights	Rights	k1gInSc4	Rights
byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgMnSc1	třetí
Mobyho	Moby	k1gMnSc4	Moby
skladbou	skladba	k1gFnSc7	skladba
v	v	k7c4	v
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc1	Pops
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
svým	svůj	k3xOyFgMnSc7	svůj
agresivním	agresivní	k2eAgMnSc7d1	agresivní
vizuálem	vizuál	k1gMnSc7	vizuál
i	i	k8xC	i
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ironií	ironie	k1gFnSc7	ironie
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
Moby	Moba	k1gFnSc2	Moba
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
elektronická	elektronický	k2eAgFnSc1d1	elektronická
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
začal	začít	k5eAaPmAgMnS	začít
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
získávat	získávat	k5eAaImF	získávat
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
u	u	k7c2	u
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
The	The	k1gMnPc2	The
Chemical	Chemical	k1gMnPc2	Chemical
Brothers	Brothersa	k1gFnPc2	Brothersa
a	a	k8xC	a
The	The	k1gFnPc2	The
Prodigy	Prodiga	k1gFnSc2	Prodiga
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Moby	Moby	k1gInPc1	Moby
přispěl	přispět	k5eAaPmAgMnS	přispět
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Republican	Republican	k1gInSc1	Republican
Party	parta	k1gFnSc2	parta
<g/>
"	"	kIx"	"
do	do	k7c2	do
benefičního	benefiční	k2eAgNnSc2d1	benefiční
alba	album	k1gNnSc2	album
Offbeat	Offbeat	k1gInSc1	Offbeat
<g/>
:	:	kIx,	:
A	a	k9	a
Red	Red	k1gFnSc1	Red
Hot	hot	k0	hot
Soundtrip	Soundtrip	k1gInSc4	Soundtrip
<g/>
,	,	kIx,	,
produkovaného	produkovaný	k2eAgNnSc2d1	produkované
neziskovou	ziskový	k2eNgFnSc7d1	nezisková
společností	společnost	k1gFnSc7	společnost
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
manažera	manažer	k1gMnSc2	manažer
Ericka	Ericko	k1gNnSc2	Ericko
Härleho	Härle	k1gMnSc2	Härle
album	album	k1gNnSc1	album
téměř	téměř	k6eAd1	téměř
zničilo	zničit	k5eAaPmAgNnS	zničit
Mobyho	Moby	k1gMnSc4	Moby
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nový	nový	k2eAgInSc1d1	nový
směr	směr	k1gInSc1	směr
nejenže	nejenže	k6eAd1	nejenže
nechal	nechat	k5eAaPmAgInS	nechat
chladnými	chladný	k2eAgInPc7d1	chladný
nezaujatá	zaujatý	k2eNgNnPc1d1	nezaujaté
média	médium	k1gNnPc1	médium
i	i	k9	i
značně	značně	k6eAd1	značně
prořídlou	prořídlý	k2eAgFnSc4d1	prořídlá
řadu	řada	k1gFnSc4	řada
stálých	stálý	k2eAgMnPc2d1	stálý
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
zmatek	zmatek	k1gInSc1	zmatek
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
za	za	k7c4	za
jaký	jaký	k3yRgInSc4	jaký
typ	typ	k1gInSc4	typ
umělce	umělec	k1gMnSc2	umělec
mají	mít	k5eAaImIp3nP	mít
Mobyho	Mobyha	k1gFnSc5	Mobyha
vlastně	vlastně	k9	vlastně
považovat	považovat	k5eAaImF	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Härle	Härle	k6eAd1	Härle
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mobymu	Mobym	k1gInSc2	Mobym
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přebít	přebít	k5eAaPmF	přebít
všechnu	všechen	k3xTgFnSc4	všechen
svou	svůj	k3xOyFgFnSc4	svůj
dobrou	dobrý	k2eAgFnSc4d1	dobrá
práci	práce	k1gFnSc4	práce
z	z	k7c2	z
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
teď	teď	k6eAd1	teď
zápasíme	zápasit	k5eAaImIp1nP	zápasit
o	o	k7c4	o
sebemenší	sebemenší	k2eAgNnSc4d1	sebemenší
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
očích	oko	k1gNnPc6	oko
mnoha	mnoho	k4c3	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
'	'	kIx"	'
<g/>
zastaralým	zastaralý	k2eAgMnSc7d1	zastaralý
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgInS	vydat
I	i	k8xC	i
Like	Like	k1gInSc1	Like
to	ten	k3xDgNnSc4	ten
Score	Scor	k1gInSc5	Scor
<g/>
,	,	kIx,	,
kolekci	kolekce	k1gFnSc4	kolekce
svých	svůj	k3xOyFgFnPc2	svůj
skladeb	skladba	k1gFnPc2	skladba
použitých	použitý	k2eAgFnPc2d1	použitá
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
nahrávkami	nahrávka	k1gFnPc7	nahrávka
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
James	James	k1gMnSc1	James
Bond	bond	k1gInSc1	bond
Theme	Them	k1gMnSc5	Them
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
použitá	použitý	k2eAgFnSc1d1	použitá
v	v	k7c6	v
bondovce	bondovce	k?	bondovce
Zítřek	zítřek	k1gInSc1	zítřek
nikdy	nikdy	k6eAd1	nikdy
neumírá	umírat	k5eNaImIp3nS	umírat
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
Dawn	Dawna	k1gFnPc2	Dawna
Fades	fadesa	k1gFnPc2	fadesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
cover	cover	k1gMnSc1	cover
verze	verze	k1gFnSc2	verze
originální	originální	k2eAgFnPc4d1	originální
skladby	skladba	k1gFnPc4	skladba
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Joy	Joy	k1gFnPc2	Joy
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
bez	bez	k7c2	bez
vokálů	vokál	k1gInPc2	vokál
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nelítostný	lítostný	k2eNgInSc1d1	nelítostný
souboj	souboj	k1gInSc1	souboj
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Manna	Mann	k1gMnSc2	Mann
<g/>
.	.	kIx.	.
<g/>
Moby	Moba	k1gMnSc2	Moba
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jako	jako	k9	jako
speciální	speciální	k2eAgMnSc1d1	speciální
host	host	k1gMnSc1	host
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
Space	Space	k1gMnSc1	Space
Ghost	Ghost	k1gMnSc1	Ghost
Coast	Coast	k1gMnSc1	Coast
to	ten	k3xDgNnSc1	ten
Coast	Coast	k1gInSc4	Coast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Space	Spaka	k1gFnSc3	Spaka
Ghost	Ghost	k1gInSc4	Ghost
posedlý	posedlý	k2eAgInSc4d1	posedlý
kletbou	kletba	k1gFnSc7	kletba
Kintavé	Kintavý	k2eAgFnPc1d1	Kintavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Play	play	k0	play
<g/>
,	,	kIx,	,
18	[number]	k4	18
a	a	k8xC	a
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
úspěch	úspěch	k1gInSc1	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vydal	vydat	k5eAaPmAgMnS	vydat
Moby	Mob	k2eAgFnPc4d1	Mob
album	album	k1gNnSc4	album
Play	play	k0	play
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
mělo	mít	k5eAaImAgNnS	mít
jen	jen	k9	jen
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
za	za	k7c4	za
rok	rok	k1gInSc4	rok
prodalo	prodat	k5eAaPmAgNnS	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
píseň	píseň	k1gFnSc1	píseň
v	v	k7c6	v
albu	album	k1gNnSc6	album
byla	být	k5eAaImAgNnP	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
licencována	licencován	k2eAgNnPc1d1	licencováno
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnPc4	reklama
a	a	k8xC	a
TV	TV	kA	TV
pořady	pořad	k1gInPc1	pořad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
nezávislé	závislý	k2eNgInPc4d1	nezávislý
filmy	film	k1gInPc4	film
a	a	k8xC	a
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
albem	album	k1gNnSc7	album
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc1	Pops
<g/>
"	"	kIx"	"
hned	hned	k6eAd1	hned
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mixy	mix	k1gInPc7	mix
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
nahrávky	nahrávka	k1gFnSc2	nahrávka
Alana	Alan	k1gMnSc2	Alan
Lomaxe	Lomaxe	k1gFnSc2	Lomaxe
Sounds	Soundsa	k1gFnPc2	Soundsa
of	of	k?	of
the	the	k?	the
South	South	k1gInSc1	South
<g/>
:	:	kIx,	:
A	a	k9	a
Musical	musical	k1gInSc1	musical
Journey	Journea	k1gFnSc2	Journea
From	From	k1gMnSc1	From
the	the	k?	the
Georgia	Georgia	k1gFnSc1	Georgia
Sea	Sea	k1gFnSc2	Sea
Islands	Islandsa	k1gFnPc2	Islandsa
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Mississippi	Mississippi	k1gNnSc1	Mississippi
Delta	delta	k1gFnSc1	delta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Natural	Natural	k?	Natural
Blues	blues	k1gInSc1	blues
<g/>
"	"	kIx"	"
Moby	Moba	k1gFnSc2	Moba
zmixoval	zmixovat	k5eAaPmAgMnS	zmixovat
skladbu	skladba	k1gFnSc4	skladba
z	z	k7c2	z
Lomaxovy	Lomaxův	k2eAgFnSc2d1	Lomaxův
kompilace	kompilace	k1gFnSc2	kompilace
Sounds	Sounds	k1gInSc1	Sounds
of	of	k?	of
the	the	k?	the
South	South	k1gMnSc1	South
"	"	kIx"	"
<g/>
Trouble	Trouble	k1gMnSc1	Trouble
So	So	kA	So
Hard	Hard	k1gMnSc1	Hard
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nazpívanou	nazpívaný	k2eAgFnSc4d1	nazpívaná
Verou	Verý	k2eAgFnSc4d1	Verý
Hallovou	Hallová	k1gFnSc4	Hallová
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
přispěl	přispět	k5eAaPmAgInS	přispět
Moby	Moba	k1gFnSc2	Moba
svou	svůj	k3xOyFgFnSc7	svůj
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Flower	Flowra	k1gFnPc2	Flowra
<g/>
"	"	kIx"	"
do	do	k7c2	do
filmu	film	k1gInSc2	film
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2001	[number]	k4	2001
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
DVD	DVD	kA	DVD
Moby	Moby	k1gInPc1	Moby
<g/>
:	:	kIx,	:
PlaytheDVD	PlaytheDVD	k1gFnPc1	PlaytheDVD
<g/>
,	,	kIx,	,
produkované	produkovaný	k2eAgFnPc1d1	produkovaná
Mobyy	Mobya	k1gFnPc1	Mobya
a	a	k8xC	a
Jeffem	Jeff	k1gInSc7	Jeff
Rogerse	Rogerse	k1gFnSc2	Rogerse
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
různé	různý	k2eAgFnPc4d1	různá
sekce	sekce	k1gFnPc4	sekce
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Live	Liv	k1gMnSc2	Liv
on	on	k3xPp3gMnSc1	on
TV	TV	kA	TV
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
vidoklipů	vidoklip	k1gInPc2	vidoklip
z	z	k7c2	z
alba	album	k1gNnSc2	album
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
"	"	kIx"	"
<g/>
South	South	k1gInSc1	South
Side	Sid	k1gInSc2	Sid
<g/>
"	"	kIx"	"
s	s	k7c7	s
Gwen	Gwen	k1gInSc1	Gwen
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Give	Give	k1gFnSc1	Give
An	An	k1gMnSc1	An
Idiot	idiot	k1gMnSc1	idiot
a	a	k8xC	a
Camcorder	Camcorder	k1gMnSc1	Camcorder
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mobymu	Mobym	k1gInSc2	Mobym
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
kamera	kamera	k1gFnSc1	kamera
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
nahrávku	nahrávka	k1gFnSc4	nahrávka
později	pozdě	k6eAd2	pozdě
sestříhala	sestříhat	k5eAaPmAgFnS	sestříhat
Tara	Tar	k2eAgFnSc1d1	Tara
Bethune-Leamenová	Bethune-Leamenová	k1gFnSc1	Bethune-Leamenová
<g/>
)	)	kIx)	)
a	a	k8xC	a
osmdesáti	osmdesát	k4xCc2	osmdesát
osmi	osm	k4xCc2	osm
minutový	minutový	k2eAgInSc1d1	minutový
"	"	kIx"	"
<g/>
Mega	mega	k1gNnSc1	mega
Mix	mix	k1gInSc1	mix
<g/>
"	"	kIx"	"
všech	všecek	k3xTgInPc2	všecek
remixů	remix	k1gInPc2	remix
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Mega	mega	k1gNnSc1	mega
Mix	mix	k1gInSc1	mix
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
vizuály	vizuál	k1gMnPc7	vizuál
vytvořenými	vytvořený	k2eAgMnPc7d1	vytvořený
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
v	v	k7c6	v
Crushi	Crush	k1gFnSc6	Crush
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
režisérky	režisérka	k1gFnSc2	režisérka
Kathi	Kath	k1gFnSc2	Kath
Prosserové	Prosserová	k1gFnSc2	Prosserová
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vydal	vydat	k5eAaPmAgInS	vydat
Moby	Moba	k1gFnSc2	Moba
album	album	k1gNnSc1	album
18	[number]	k4	18
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
získalo	získat	k5eAaPmAgNnS	získat
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
a	a	k8xC	a
platinová	platinový	k2eAgNnPc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc6	třicet
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
se	s	k7c7	s
zmíněnými	zmíněný	k2eAgInPc7d1	zmíněný
dvěma	dva	k4xCgInPc7	dva
alby	album	k1gNnPc7	album
<g/>
,	,	kIx,	,
Play	play	k0	play
a	a	k8xC	a
18	[number]	k4	18
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
přes	přes	k7c4	přes
500	[number]	k4	500
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
založil	založit	k5eAaPmAgMnS	založit
mobilní	mobilní	k2eAgMnSc1d1	mobilní
Area	Ares	k1gMnSc4	Ares
One	One	k1gFnSc2	One
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
představoval	představovat	k5eAaImAgInS	představovat
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
škálu	škála	k1gFnSc4	škála
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
OutKast	OutKast	k1gMnSc1	OutKast
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Order	Order	k1gMnSc1	Order
<g/>
,	,	kIx,	,
Incubus	Incubus	k1gMnSc1	Incubus
<g/>
,	,	kIx,	,
Nelly	Nella	k1gFnPc1	Nella
Furtado	Furtada	k1gFnSc5	Furtada
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Oakenfold	Oakenfold	k1gMnSc1	Oakenfold
<g/>
,	,	kIx,	,
i	i	k8xC	i
samotný	samotný	k2eAgInSc4d1	samotný
Moby	Moby	k1gInPc4	Moby
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Area	Ares	k1gMnSc2	Ares
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
,	,	kIx,	,
Moby	Moba	k1gFnSc2	Moba
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
Man	Man	k1gMnSc1	Man
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Busta	busta	k1gFnSc1	busta
Rhymes	Rhymes	k1gMnSc1	Rhymes
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Cox	Cox	k1gMnSc1	Cox
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
napsal	napsat	k5eAaBmAgInS	napsat
Moby	Mob	k2eAgFnPc1d1	Mob
se	se	k3xPyFc4	se
Sophie	Sophie	k1gFnPc1	Sophie
Ellis-Bextorovou	Ellis-Bextorový	k2eAgFnSc4d1	Ellis-Bextorový
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Is	Is	k1gMnSc1	Is
It	It	k1gMnSc1	It
Any	Any	k1gMnSc1	Any
Wonder	Wonder	k1gMnSc1	Wonder
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zremixoval	zremixovat	k5eAaPmAgMnS	zremixovat
počiny	počin	k1gInPc4	počin
Beastie	Beastie	k1gFnSc2	Beastie
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
Davida	David	k1gMnSc2	David
Bowieho	Bowie	k1gMnSc4	Bowie
<g/>
,	,	kIx,	,
Nase	Nas	k1gMnPc4	Nas
a	a	k8xC	a
Metalliky	Metallik	k1gMnPc4	Metallik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Early	earl	k1gMnPc4	earl
Mornin	Mornina	k1gFnPc2	Mornina
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Britney	Britnea	k1gFnSc2	Britnea
Spears	Spearsa	k1gFnPc2	Spearsa
In	In	k1gFnSc2	In
the	the	k?	the
Zone	Zon	k1gFnSc2	Zon
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Public	publicum	k1gNnPc2	publicum
Enemy	Enema	k1gFnSc2	Enema
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Make	Make	k1gInSc1	Make
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Fuck	Fuck	k1gMnSc1	Fuck
War	War	k1gMnSc1	War
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
ještě	ještě	k9	ještě
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Mobyho	Mobyze	k6eAd1	Mobyze
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Extreme	Extrem	k1gInSc5	Extrem
Ways	Waysa	k1gFnPc2	Waysa
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
ve	v	k7c6	v
filmech	film	k1gInPc6	film
o	o	k7c6	o
agentu	agens	k1gInSc6	agens
Jasonu	Jason	k1gMnSc3	Jason
Bourneovi	Bourneus	k1gMnSc3	Bourneus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
Moby	Moba	k1gFnSc2	Moba
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
Pyramid	pyramida	k1gFnPc2	pyramida
v	v	k7c4	v
Glastonbury	Glastonbur	k1gMnPc4	Glastonbur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
Last	Last	k2eAgInSc1d1	Last
Night	Night	k1gInSc1	Night
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
aktivita	aktivita	k1gFnSc1	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgMnS	vydat
album	album	k1gNnSc4	album
Hotel	hotel	k1gInSc1	hotel
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Pacha	Pacha	k1gMnSc1	Pacha
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spoléhalo	spoléhat	k5eAaImAgNnS	spoléhat
jen	jen	k9	jen
na	na	k7c4	na
vzorky	vzorek	k1gInPc4	vzorek
vokálů	vokál	k1gInPc2	vokál
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
zpěvy	zpěv	k1gInPc1	zpěv
a	a	k8xC	a
instrumentály	instrumentál	k1gInPc1	instrumentál
byly	být	k5eAaImAgInP	být
natočeny	natočit	k5eAaBmNgInP	natočit
živě	živě	k6eAd1	živě
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Mobym	Mobym	k1gInSc4	Mobym
a	a	k8xC	a
vokalistkou	vokalistka	k1gFnSc7	vokalistka
Laurou	Laura	k1gFnSc7	Laura
Dawn	Dawn	k1gNnSc4	Dawn
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
Mobyho	Mobyha	k1gFnSc5	Mobyha
největších	veliký	k2eAgInPc2d3	veliký
evropských	evropský	k2eAgInPc2d1	evropský
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lift	lift	k1gInSc1	lift
Me	Me	k1gFnSc2	Me
Up	Up	k1gFnSc2	Up
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Slipping	Slipping	k1gInSc1	Slipping
Away	Awaa	k1gFnSc2	Awaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
evropskou	evropský	k2eAgFnSc7d1	Evropská
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
stanice	stanice	k1gFnSc1	stanice
ITV	ITV	kA	ITV
použila	použít	k5eAaPmAgFnS	použít
speciálně	speciálně	k6eAd1	speciálně
zremixovanou	zremixovaný	k2eAgFnSc4d1	zremixovaná
verzi	verze	k1gFnSc4	verze
"	"	kIx"	"
<g/>
Lift	lift	k1gInSc1	lift
Me	Me	k1gMnSc2	Me
Up	Up	k1gMnSc2	Up
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
reportážní	reportážní	k2eAgFnSc4d1	reportážní
hudbu	hudba	k1gFnSc4	hudba
u	u	k7c2	u
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
ujmout	ujmout	k5eAaPmF	ujmout
se	se	k3xPyFc4	se
soundtracku	soundtrack	k1gInSc3	soundtrack
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Southland	Southland	k1gInSc4	Southland
Tales	Talesa	k1gFnPc2	Talesa
od	od	k7c2	od
Richarda	Richard	k1gMnSc2	Richard
Kellyho	Kelly	k1gMnSc2	Kelly
<g/>
,	,	kIx,	,
očekávaný	očekávaný	k2eAgInSc1d1	očekávaný
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
fanouškem	fanoušek	k1gMnSc7	fanoušek
Kellyho	Kelly	k1gMnSc4	Kelly
předchozího	předchozí	k2eAgInSc2d1	předchozí
filmu	film	k1gInSc2	film
Donnie	Donnie	k1gFnSc2	Donnie
Darko	Darko	k1gNnSc1	Darko
<g/>
.	.	kIx.	.
<g/>
Moby	Moba	k1gFnPc1	Moba
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
Laurou	Laura	k1gFnSc7	Laura
Dawnovou	Dawnová	k1gFnSc7	Dawnová
<g/>
,	,	kIx,	,
Daronem	Daron	k1gInSc7	Daron
Murphym	Murphym	k1gInSc1	Murphym
a	a	k8xC	a
Aaronem	Aaron	k1gInSc7	Aaron
A.	A.	kA	A.
Brooksem	Brooks	k1gInSc7	Brooks
založit	založit	k5eAaPmF	založit
rockovou	rockový	k2eAgFnSc4d1	rocková
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc2	The
Little	Little	k1gFnSc2	Little
Death	Deatha	k1gFnPc2	Deatha
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Little	Little	k1gFnSc1	Little
Death	Death	k1gInSc1	Death
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgInS	vydat
Last	Last	k2eAgInSc1d1	Last
Night	Night	k1gInSc1	Night
<g/>
,	,	kIx,	,
eklektické	eklektický	k2eAgNnSc1d1	eklektické
album	album	k1gNnSc1	album
elektrotaneční	elektrotaneční	k2eAgFnSc2d1	elektrotaneční
hudby	hudba	k1gFnSc2	hudba
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
nocí	noc	k1gFnSc7	noc
strávenou	strávený	k2eAgFnSc7d1	strávená
venku	venku	k6eAd1	venku
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Lower	Lowra	k1gFnPc2	Lowra
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
singly	singl	k1gInPc4	singl
z	z	k7c2	z
Last	Lasta	k1gFnPc2	Lasta
Night	Nighta	k1gFnPc2	Nighta
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Alice	Alice	k1gFnSc1	Alice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Disco	disco	k1gNnPc1	disco
Lies	Liesa	k1gFnPc2	Liesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Love	lov	k1gInSc5	lov
to	ten	k3xDgNnSc4	ten
Move	Move	k1gNnSc1	Move
in	in	k?	in
Here	Her	k1gInSc2	Her
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Ooh	Ooh	k1gFnSc1	Ooh
Yeah	Yeah	k1gMnSc1	Yeah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
zpracováváno	zpracovávat	k5eAaImNgNnS	zpracovávat
v	v	k7c4	v
Mobyho	Moby	k1gMnSc4	Moby
domovském	domovský	k2eAgNnSc6d1	domovské
studiu	studio	k1gNnSc6	studio
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
Manhattanu	Manhattan	k1gInSc6	Manhattan
a	a	k8xC	a
představovalo	představovat	k5eAaImAgNnS	představovat
množství	množství	k1gNnSc1	množství
hostujících	hostující	k2eAgMnPc2d1	hostující
zpěváků	zpěvák	k1gMnPc2	zpěvák
zahrnujících	zahrnující	k2eAgMnPc2d1	zahrnující
Wendy	Wenda	k1gFnSc2	Wenda
Starlandovou	Starlandový	k2eAgFnSc4d1	Starlandový
<g/>
,	,	kIx,	,
Grandmastera	Grandmaster	k1gMnSc4	Grandmaster
Caze	Caz	k1gInSc2	Caz
<g/>
,	,	kIx,	,
Sylviu	Sylvius	k1gMnSc3	Sylvius
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
KUDU	kudu	k1gMnSc1	kudu
<g/>
,	,	kIx,	,
British	British	k1gMnSc1	British
MC	MC	kA	MC
Aynzli	Aynzli	k1gMnSc1	Aynzli
a	a	k8xC	a
Nigerian	Nigerian	k1gMnSc1	Nigerian
419	[number]	k4	419
Squad	Squad	k1gInSc1	Squad
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
do	do	k7c2	do
2008	[number]	k4	2008
Moby	Moba	k1gFnSc2	Moba
provozoval	provozovat	k5eAaImAgInS	provozovat
řadu	řada	k1gFnSc4	řada
newyorských	newyorský	k2eAgFnPc2d1	newyorská
klubových	klubový	k2eAgFnPc2d1	klubová
akcí	akce	k1gFnPc2	akce
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Degenerates	Degeneratesa	k1gFnPc2	Degeneratesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Wait	Wait	k1gInSc1	Wait
for	forum	k1gNnPc2	forum
Me	Me	k1gFnSc2	Me
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Moby	Moba	k1gFnSc2	Moba
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
SuicideGirls	SuicideGirlsa	k1gFnPc2	SuicideGirlsa
uvedl	uvést	k5eAaPmAgMnS	uvést
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
přijít	přijít	k5eAaPmF	přijít
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
udělat	udělat	k5eAaPmF	udělat
opravdu	opravdu	k6eAd1	opravdu
emoční	emoční	k2eAgFnSc4d1	emoční
<g/>
,	,	kIx,	,
pěknou	pěkný	k2eAgFnSc4d1	pěkná
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Netuším	tušit	k5eNaImIp1nS	tušit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
uspěji	uspět	k5eAaPmIp1nS	uspět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mým	můj	k3xOp1gInSc7	můj
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
něco	něco	k3yInSc4	něco
velmi	velmi	k6eAd1	velmi
osobního	osobní	k2eAgMnSc2d1	osobní
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
melodického	melodický	k2eAgNnSc2d1	melodické
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
krásného	krásný	k2eAgNnSc2d1	krásné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
Moby	Moba	k1gFnSc2	Moba
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
vyjde	vyjít	k5eAaPmIp3nS	vyjít
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Album	album	k1gNnSc1	album
jsem	být	k5eAaImIp1nS	být
nahrál	nahrát	k5eAaPmAgInS	nahrát
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
studiu	studio	k1gNnSc6	studio
v	v	k7c4	v
Lower	Lower	k1gInSc4	Lower
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
'	'	kIx"	'
<g/>
studio	studio	k1gNnSc1	studio
<g/>
'	'	kIx"	'
vždy	vždy	k6eAd1	vždy
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
název	název	k1gInSc4	název
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
spoustu	spousta	k1gFnSc4	spousta
vybavení	vybavení	k1gNnSc6	vybavení
nacpaném	nacpaný	k2eAgNnSc6d1	nacpané
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
ložnici	ložnice	k1gFnSc6	ložnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
i	i	k8xC	i
malých	malý	k2eAgFnPc6d1	malá
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
jsem	být	k5eAaImIp1nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
udělat	udělat	k5eAaPmF	udělat
sám	sám	k3xTgInSc4	sám
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
říká	říkat	k5eAaImIp3nS	říkat
Moby	Moba	k1gFnPc4	Moba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
mou	můj	k3xOp1gFnSc7	můj
hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
bylo	být	k5eAaImAgNnS	být
vyslechnutí	vyslechnutí	k1gNnSc1	vyslechnutí
proslovu	proslov	k1gInSc2	proslov
Davida	David	k1gMnSc2	David
Lyncheho	Lynche	k1gMnSc2	Lynche
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
kreativitě	kreativita	k1gFnSc6	kreativita
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
parafrázuji	parafrázovat	k5eAaBmIp1nS	parafrázovat
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
že	že	k8xS	že
kreativita	kreativita	k1gFnSc1	kreativita
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
bez	bez	k7c2	bez
žádných	žádný	k3yNgInPc2	žádný
tržních	tržní	k2eAgInPc2d1	tržní
nátlaků	nátlak	k1gInPc2	nátlak
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
a	a	k8xC	a
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaPmIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kreativita	kreativita	k1gFnSc1	kreativita
až	až	k6eAd1	až
příliš	příliš	k6eAd1	příliš
mnoha	mnoho	k4c2	mnoho
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
nebo	nebo	k8xC	nebo
spisovatelů	spisovatel	k1gMnPc2	spisovatel
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dobře	dobře	k6eAd1	dobře
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
naloží	naložit	k5eAaPmIp3nS	naložit
v	v	k7c6	v
obchodnictví	obchodnictví	k1gNnSc6	obchodnictví
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velký	velký	k2eAgInSc1d1	velký
tržní	tržní	k2eAgInSc1d1	tržní
podíl	podíl	k1gInSc1	podíl
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
a	a	k8xC	a
kolik	kolik	k4yQc4	kolik
peněz	peníze	k1gInPc2	peníze
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
vynese	vynést	k5eAaPmIp3nS	vynést
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
této	tento	k3xDgFnSc2	tento
nahrávky	nahrávka	k1gFnSc2	nahrávka
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bych	by	kYmCp1nS	by
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
zabýval	zabývat	k5eAaImAgInS	zabývat
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bude	být	k5eAaImBp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
přijímán	přijímat	k5eAaImNgInS	přijímat
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
mírnější	mírný	k2eAgFnSc1d2	mírnější
<g/>
,	,	kIx,	,
melodičtější	melodický	k2eAgFnSc1d2	melodičtější
<g/>
,	,	kIx,	,
truchlivější	truchlivý	k2eAgFnSc1d2	truchlivější
a	a	k8xC	a
osobnější	osobní	k2eAgFnSc1d2	osobnější
než	než	k8xS	než
všechny	všechen	k3xTgFnPc4	všechen
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jsem	být	k5eAaImIp1nS	být
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Wait	Wait	k1gInSc1	Wait
for	forum	k1gNnPc2	forum
Me	Me	k1gFnSc2	Me
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moby	Moba	k1gFnPc1	Moba
a	a	k8xC	a
David	David	k1gMnSc1	David
Lynch	Lyncha	k1gFnPc2	Lyncha
diskutovali	diskutovat	k5eAaImAgMnP	diskutovat
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
na	na	k7c4	na
Lynchovu	Lynchův	k2eAgFnSc4d1	Lynchova
online	onlinout	k5eAaPmIp3nS	onlinout
kanálu	kanál	k1gInSc2	kanál
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
Foundation	Foundation	k1gInSc4	Foundation
Television	Television	k1gInSc4	Television
Beta	beta	k1gNnPc2	beta
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc4	video
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
singlu	singl	k1gInSc3	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Shot	shot	k1gInSc1	shot
in	in	k?	in
the	the	k?	the
Back	Back	k1gInSc1	Back
of	of	k?	of
the	the	k?	the
Head	Heada	k1gFnPc2	Heada
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
režírováno	režírovat	k5eAaImNgNnS	režírovat
Lynchem	Lynch	k1gInSc7	Lynch
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c4	na
Mobyho	Moby	k1gMnSc4	Moby
oficiálních	oficiální	k2eAgInPc2d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wait	Wait	k1gInSc4	Wait
For	forum	k1gNnPc2	forum
Me	Me	k1gMnSc2	Me
bylo	být	k5eAaImAgNnS	být
mixováno	mixovat	k5eAaImNgNnS	mixovat
Kenem	Ken	k1gInSc7	Ken
Thomasem	Thomas	k1gMnSc7	Thomas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předtím	předtím	k6eAd1	předtím
dělal	dělat	k5eAaImAgMnS	dělat
producenta	producent	k1gMnSc2	producent
některým	některý	k3yIgNnPc3	některý
albům	album	k1gNnPc3	album
skupiny	skupina	k1gFnSc2	skupina
Sigur	Sigur	k1gMnSc1	Sigur
Rós	Rós	k1gMnSc1	Rós
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moba	k1gFnPc1	Moba
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mixování	mixování	k1gNnSc1	mixování
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
pěkné	pěkný	k2eAgNnSc1d1	pěkné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc1d1	otevřený
vyzkoušení	vyzkoušení	k1gNnSc2	vyzkoušení
čehokoliv	cokoliv	k3yInSc2	cokoliv
–	–	k?	–
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
zaznamenávání	zaznamenávání	k1gNnSc4	zaznamenávání
starého	starý	k2eAgInSc2d1	starý
rozbitého	rozbitý	k2eAgInSc2d1	rozbitý
bakelitového	bakelitový	k2eAgInSc2d1	bakelitový
rádia	rádius	k1gInSc2	rádius
skrz	skrz	k7c4	skrz
jakési	jakýsi	k3yIgInPc4	jakýsi
staré	starý	k2eAgInPc4d1	starý
rozbité	rozbitý	k2eAgInPc4d1	rozbitý
efektové	efektový	k2eAgInPc4d1	efektový
pedály	pedál	k1gInPc4	pedál
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
znít	znít	k5eAaImF	znít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
zvuk	zvuk	k1gInSc1	zvuk
přítomný	přítomný	k2eAgInSc1d1	přítomný
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pětačtyřicetisekundové	pětačtyřicetisekundový	k2eAgFnSc2d1	pětačtyřicetisekundový
skladby	skladba	k1gFnSc2	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
'	'	kIx"	'
<g/>
Stock	Stock	k1gInSc1	Stock
Radio	radio	k1gNnSc1	radio
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jako	jako	k8xS	jako
'	'	kIx"	'
<g/>
geekové	geekové	k2eAgMnSc1d1	geekové
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
technicky	technicky	k6eAd1	technicky
mimo	mimo	k6eAd1	mimo
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
záznam	záznam	k1gInSc4	záznam
mixovali	mixovat	k5eAaImAgMnP	mixovat
čistě	čistě	k6eAd1	čistě
pomocí	pomocí	k7c2	pomocí
analogových	analogový	k2eAgNnPc2d1	analogové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
stereu	stereo	k1gNnSc6	stereo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
mixovala	mixovat	k5eAaImAgFnS	mixovat
hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
některé	některý	k3yIgFnPc4	některý
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
mohu	moct	k5eAaImIp1nS	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
znějí	znět	k5eAaImIp3nP	znět
se	se	k3xPyFc4	se
sluchátky	sluchátko	k1gNnPc7	sluchátko
fantasticky	fantasticky	k6eAd1	fantasticky
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
sníženy	snížen	k2eAgInPc4d1	snížen
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
oběti	oběť	k1gFnPc4	oběť
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
Moby	Moba	k1gFnSc2	Moba
daroval	darovat	k5eAaPmAgMnS	darovat
mezi	mezi	k7c7	mezi
75	[number]	k4	75
000	[number]	k4	000
a	a	k8xC	a
100	[number]	k4	100
000	[number]	k4	000
dolary	dolar	k1gInPc7	dolar
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
těmto	tento	k3xDgFnPc3	tento
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gNnSc3	on
posloužily	posloužit	k5eAaPmAgInP	posloužit
zisky	zisk	k1gInPc4	zisk
z	z	k7c2	z
nastávajících	nastávající	k2eAgNnPc2d1	nastávající
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
v	v	k7c6	v
San	San	k1gMnSc6	San
Diegu	Dieg	k1gMnSc6	Dieg
<g/>
,	,	kIx,	,
San	San	k1gMnSc6	San
Franciscu	Francisc	k1gMnSc6	Francisc
a	a	k8xC	a
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc4	Moby
dále	daleko	k6eAd2	daleko
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Falls	Falls	k1gInSc1	Falls
Festivalu	festival	k1gInSc2	festival
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
ostatních	ostatní	k2eAgFnPc2d1	ostatní
Sunset	Sunseta	k1gFnPc2	Sunseta
Sounds	Soundsa	k1gFnPc2	Soundsa
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
Moby	Moba	k1gFnSc2	Moba
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
UGC	UGC	kA	UGC
soutěž	soutěž	k1gFnSc4	soutěž
s	s	k7c7	s
Genero	Genero	k1gNnSc4	Genero
<g/>
.	.	kIx.	.
<g/>
TV	TV	kA	TV
<g/>
,	,	kIx,	,
vyzívajíc	vyzívat	k5eAaImSgFnS	vyzívat
fanoušky	fanoušek	k1gMnPc4	fanoušek
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
videoklipu	videoklip	k1gInSc2	videoklip
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
bude	být	k5eAaImBp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
<g />
.	.	kIx.	.
</s>
<s>
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
oficiální	oficiální	k2eAgInSc4d1	oficiální
videoklip	videoklip	k1gInSc4	videoklip
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgInSc4d1	nadcházející
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Wait	Wait	k1gInSc1	Wait
for	forum	k1gNnPc2	forum
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Moby	Moba	k1gFnSc2	Moba
vybral	vybrat	k5eAaPmAgMnS	vybrat
vítězný	vítězný	k2eAgInSc4d1	vítězný
videoklip	videoklip	k1gInSc4	videoklip
z	z	k7c2	z
500	[number]	k4	500
možných	možný	k2eAgMnPc2d1	možný
"	"	kIx"	"
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tvořivosti	tvořivost	k1gFnSc2	tvořivost
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnSc2	hodnota
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
pojetí	pojetí	k1gNnSc2	pojetí
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
obsaženého	obsažený	k2eAgInSc2d1	obsažený
humoru	humor	k1gInSc2	humor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
režíroval	režírovat	k5eAaImAgMnS	režírovat
Nimrod	Nimrod	k1gMnSc1	Nimrod
Shapira	Shapira	k1gMnSc1	Shapira
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
příběh	příběh	k1gInSc1	příběh
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pozvat	pozvat	k5eAaPmF	pozvat
Mobyho	Moby	k1gMnSc4	Moby
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
tak	tak	k9	tak
učinit	učinit	k5eAaPmF	učinit
knihou	kniha	k1gFnSc7	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
How	How	k1gFnSc2	How
to	ten	k3xDgNnSc4	ten
Summon	Summon	k1gNnSc4	Summon
Mobyː	Mobyː	k1gFnSc1	Mobyː
Guide	Guid	k1gInSc5	Guid
for	forum	k1gNnPc2	forum
Dummies	Dummies	k1gInSc1	Dummies
(	(	kIx(	(
<g/>
Jak	jak	k8xS	jak
přivolat	přivolat	k5eAaPmF	přivolat
Mobyho	Moby	k1gMnSc4	Moby
<g/>
:	:	kIx,	:
Návod	návod	k1gInSc1	návod
pro	pro	k7c4	pro
hlupáky	hlupák	k1gMnPc4	hlupák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
musí	muset	k5eAaImIp3nS	muset
podstoupit	podstoupit	k5eAaPmF	podstoupit
deset	deset	k4xCc4	deset
bizarních	bizarní	k2eAgInPc2d1	bizarní
a	a	k8xC	a
komických	komický	k2eAgInPc2d1	komický
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
poctou	pocta	k1gFnSc7	pocta
nějakému	nějaký	k3yIgInSc3	nějaký
jinému	jiný	k2eAgMnSc3d1	jiný
Mobyho	Moby	k1gMnSc4	Moby
videoklipu	videoklip	k1gInSc2	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
vyšel	vyjít	k5eAaPmAgInS	vyjít
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Destroyed	Destroyed	k1gInSc1	Destroyed
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
Moby	Moba	k1gFnSc2	Moba
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
dalším	další	k2eAgNnSc6d1	další
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jméno	jméno	k1gNnSc4	jméno
Destroyed	Destroyed	k1gInSc1	Destroyed
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
více	hodně	k6eAd2	hodně
akustické	akustický	k2eAgNnSc1d1	akustické
a	a	k8xC	a
méně	málo	k6eAd2	málo
elektronické	elektronický	k2eAgInPc4d1	elektronický
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1	vydáno
bylo	být	k5eAaImAgNnS	být
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
následováno	následován	k2eAgNnSc1d1	následováno
fotoalbem	fotoalbum	k1gNnSc7	fotoalbum
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
melodické	melodický	k2eAgFnPc1d1	melodická
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
a	a	k8xC	a
elektronické	elektronický	k2eAgFnPc1d1	elektronická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kdybych	kdyby	kYmCp1nS	kdyby
to	ten	k3xDgNnSc1	ten
měl	mít	k5eAaImAgInS	mít
shrnout	shrnout	k5eAaPmF	shrnout
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc4	ten
slovy	slovo	k1gNnPc7	slovo
'	'	kIx"	'
<g/>
porouchaná	porouchaný	k2eAgFnSc1d1	porouchaná
melodická	melodický	k2eAgFnSc1d1	melodická
elektronická	elektronický	k2eAgFnSc1d1	elektronická
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
prázdná	prázdné	k1gNnPc4	prázdné
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obal	obal	k1gInSc1	obal
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
na	na	k7c6	na
Letišti	letiště	k1gNnSc6	letiště
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
obrázek	obrázek	k1gInSc4	obrázek
s	s	k7c7	s
cedulí	cedule	k1gFnSc7	cedule
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
destroyed	destroyed	k1gInSc1	destroyed
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
fráze	fráze	k1gFnSc1	fráze
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
unclaimed	unclaimed	k1gMnSc1	unclaimed
baggage	baggage	k6eAd1	baggage
will	wilnout	k5eAaPmAgMnS	wilnout
be	be	k?	be
destroyed	destroyed	k1gMnSc1	destroyed
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Všechna	všechen	k3xTgNnPc1	všechen
nevyzvednutá	vyzvednutý	k2eNgNnPc1d1	nevyzvednuté
zavazadla	zavazadlo	k1gNnPc1	zavazadlo
budou	být	k5eAaImBp3nP	být
zničena	zničen	k2eAgNnPc1d1	zničeno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
patnácti	patnáct	k4xCc2	patnáct
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rockets	Rockets	k1gInSc1	Rockets
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
In	In	k1gMnSc1	In
NYC	NYC	kA	NYC
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
oznámením	oznámení	k1gNnSc7	oznámení
o	o	k7c6	o
albu	album	k1gNnSc6	album
přišlo	přijít	k5eAaPmAgNnS	přijít
vydání	vydání	k1gNnSc1	vydání
EP	EP	kA	EP
Be	Be	k1gMnSc2	Be
the	the	k?	the
One	One	k1gMnSc2	One
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
tři	tři	k4xCgFnPc4	tři
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
Destroyed	Destroyed	k1gMnSc1	Destroyed
<g/>
.	.	kIx.	.
</s>
<s>
EP	EP	kA	EP
bylo	být	k5eAaImAgNnS	být
zadarmo	zadarmo	k6eAd1	zadarmo
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
podepsali	podepsat	k5eAaPmAgMnP	podepsat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
poštovní	poštovní	k2eAgInSc4d1	poštovní
adresář	adresář	k1gInSc4	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
singl	singl	k1gInSc4	singl
dal	dát	k5eAaPmAgInS	dát
Moby	Moby	k1gInPc4	Moby
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
hlasování	hlasování	k1gNnSc2	hlasování
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
singl	singl	k1gInSc1	singl
má	mít	k5eAaImIp3nS	mít
vyjít	vyjít	k5eAaPmF	vyjít
jako	jako	k9	jako
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
"	"	kIx"	"
<g/>
Lie	Lie	k1gMnSc1	Lie
Down	Down	k1gMnSc1	Down
in	in	k?	in
Darkness	Darkness	k1gInSc1	Darkness
<g/>
"	"	kIx"	"
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Moby	Moba	k1gFnSc2	Moba
opět	opět	k6eAd1	opět
požádal	požádat	k5eAaPmAgMnS	požádat
fanoušky	fanoušek	k1gMnPc4	fanoušek
o	o	k7c4	o
výběr	výběr	k1gInSc4	výběr
třetího	třetí	k4xOgInSc2	třetí
singlu	singl	k1gInSc2	singl
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
bez	bez	k7c2	bez
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
oznámil	oznámit	k5eAaPmAgMnS	oznámit
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
Twitter	Twitter	k1gInSc4	Twitter
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
další	další	k2eAgInPc1d1	další
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
singly	singl	k1gInPc1	singl
After	After	k1gInSc1	After
a	a	k8xC	a
The	The	k1gMnSc1	The
Right	Right	k1gMnSc1	Right
Thing	Thing	k1gMnSc1	Thing
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgMnS	vydat
Destroyed	Destroyed	k1gMnSc1	Destroyed
Remixed	Remixed	k1gMnSc1	Remixed
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
CD	CD	kA	CD
limitované	limitovaný	k2eAgFnSc2d1	limitovaná
edice	edice	k1gFnSc2	edice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
sbírku	sbírka	k1gFnSc4	sbírka
remixů	remix	k1gInPc2	remix
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
také	také	k9	také
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
exkluzivní	exkluzivní	k2eAgInPc4d1	exkluzivní
remixy	remix	k1gInPc4	remix
od	od	k7c2	od
interpretů	interpret	k1gMnPc2	interpret
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
<g/>
,	,	kIx,	,
Holy	hola	k1gFnSc2	hola
Ghost	Ghost	k1gInSc1	Ghost
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
System	Syst	k1gInSc7	Syst
Divine	Divin	k1gInSc5	Divin
a	a	k8xC	a
představovalo	představovat	k5eAaImAgNnS	představovat
zbrusu	zbrusu	k6eAd1	zbrusu
nový	nový	k2eAgInSc4d1	nový
třicetiminutový	třicetiminutový	k2eAgInSc4d1	třicetiminutový
záznam	záznam	k1gInSc4	záznam
od	od	k7c2	od
Mobyho	Moby	k1gMnSc2	Moby
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Sides	Sides	k1gMnSc1	Sides
Gone	Gone	k1gInSc1	Gone
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počin	počin	k1gInSc1	počin
byl	být	k5eAaImAgInS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
eklektický	eklektický	k2eAgInSc1d1	eklektický
mix	mix	k1gInSc1	mix
od	od	k7c2	od
několika	několik	k4yIc2	několik
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
vzrušujících	vzrušující	k2eAgMnPc2d1	vzrušující
a	a	k8xC	a
nejzajímavějších	zajímavý	k2eAgMnPc2d3	nejzajímavější
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
DJů	DJů	k1gMnPc2	DJů
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Innocents	Innocents	k1gInSc1	Innocents
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
únorem	únor	k1gInSc7	únor
a	a	k8xC	a
srpnem	srpen	k1gInSc7	srpen
2013	[number]	k4	2013
Moby	Moba	k1gFnSc2	Moba
na	na	k7c4	na
Wanderlust	Wanderlust	k1gFnSc4	Wanderlust
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
též	též	k9	též
rozličné	rozličný	k2eAgFnPc4d1	rozličná
akce	akce	k1gFnPc4	akce
na	na	k7c6	na
Hawaii	Hawaie	k1gFnSc6	Hawaie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vermontu	Vermont	k1gInSc6	Vermont
<g/>
,	,	kIx,	,
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
Coloradu	Colorado	k1gNnSc6	Colorado
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
Státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
předvedl	předvést	k5eAaPmAgMnS	předvést
svou	svůj	k3xOyFgFnSc4	svůj
akustickou	akustický	k2eAgFnSc4d1	akustická
i	i	k8xC	i
DJskou	DJský	k2eAgFnSc4d1	DJská
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
jednou	jednou	k6eAd1	jednou
vystopil	vystopit	k5eAaPmAgMnS	vystopit
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
1	[number]	k4	1
World	Worlda	k1gFnPc2	Worlda
Music	Musice	k1gFnPc2	Musice
Festival	festival	k1gInSc1	festival
in	in	k?	in
Singapore	Singapor	k1gMnSc5	Singapor
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
na	na	k7c6	na
prestižním	prestižní	k2eAgInSc6d1	prestižní
Coachella	Coachello	k1gNnSc2	Coachello
Festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgInS	zahrát
dva	dva	k4xCgMnPc4	dva
75	[number]	k4	75
<g/>
minutové	minutový	k2eAgInPc1d1	minutový
DJ	DJ	kA	DJ
sety	set	k1gInPc1	set
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
představovaly	představovat	k5eAaImAgInP	představovat
vizuální	vizuální	k2eAgFnSc4d1	vizuální
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
NASA	NASA	kA	NASA
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
unikátních	unikátní	k2eAgInPc2d1	unikátní
obrázků	obrázek	k1gInPc2	obrázek
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
promítaných	promítaný	k2eAgFnPc2d1	promítaná
během	během	k7c2	během
představení	představení	k1gNnSc2	představení
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
květnové	květnový	k2eAgNnSc4d1	květnové
DJ	DJ	kA	DJ
představení	představení	k1gNnPc2	představení
na	na	k7c4	na
Movement	Movement	k1gInSc4	Movement
Detroit	Detroit	k1gInSc1	Detroit
Festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
Record	Record	k1gInSc4	Record
Store	Stor	k1gInSc5	Stor
Day	Day	k1gMnSc3	Day
2013	[number]	k4	2013
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgInS	vydat
sedmipalcovou	sedmipalcový	k2eAgFnSc4d1	sedmipalcová
nahrávku	nahrávka	k1gFnSc4	nahrávka
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
Lonely	Lonela	k1gFnSc2	Lonela
Night	Night	k1gInSc1	Night
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
představil	představit	k5eAaPmAgMnS	představit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vokalista	vokalista	k1gMnSc1	vokalista
skupiny	skupina	k1gFnSc2	skupina
Screaming	Screaming	k1gInSc1	Screaming
Trees	Trees	k1gMnSc1	Trees
Mark	Mark	k1gMnSc1	Mark
Lanegan	Lanegan	k1gMnSc1	Lanegan
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodné	doprovodný	k2eAgNnSc4d1	doprovodné
video	video	k1gNnSc4	video
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Colin	Colin	k1gMnSc1	Colin
Rich	Rich	k1gMnSc1	Rich
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
Moby	Mob	k1gMnPc7	Mob
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
skutečně	skutečně	k6eAd1	skutečně
nadšený	nadšený	k2eAgMnSc1d1	nadšený
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
hudební	hudební	k2eAgNnSc4d1	hudební
video	video	k1gNnSc4	video
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
takovým	takový	k3xDgMnSc7	takový
špičkovým	špičkový	k2eAgMnSc7d1	špičkový
režisérem	režisér	k1gMnSc7	režisér
<g/>
;	;	kIx,	;
cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokonale	dokonale	k6eAd1	dokonale
se	se	k3xPyFc4	se
ty	ten	k3xDgInPc1	ten
pomalé	pomalý	k2eAgInPc1d1	pomalý
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgInPc1d1	bohatý
a	a	k8xC	a
malátné	malátný	k2eAgInPc1d1	malátný
záběry	záběr	k1gInPc1	záběr
pouště	poušť	k1gFnSc2	poušť
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nahrávka	nahrávka	k1gFnSc1	nahrávka
následně	následně	k6eAd1	následně
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
remixy	remix	k1gInPc7	remix
od	od	k7c2	od
Photeka	Photeko	k1gNnSc2	Photeko
<g/>
,	,	kIx,	,
Gregora	Gregor	k1gMnSc2	Gregor
Theshera	Thesher	k1gMnSc2	Thesher
<g/>
,	,	kIx,	,
Freeschy	Freescha	k1gMnSc2	Freescha
a	a	k8xC	a
Mobyho	Moby	k1gMnSc2	Moby
samotného	samotný	k2eAgMnSc2d1	samotný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
Moby	Moba	k1gFnSc2	Moba
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydá	vydat	k5eAaPmIp3nS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Innocents	Innocentsa	k1gFnPc2	Innocentsa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
sepisováno	sepisovat	k5eAaImNgNnS	sepisovat
a	a	k8xC	a
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
v	v	k7c6	v
předešlých	předešlý	k2eAgInPc6d1	předešlý
osmnácti	osmnáct	k4xCc6	osmnáct
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nahrávané	nahrávaný	k2eAgNnSc1d1	nahrávané
v	v	k7c6	v
Mobyho	Moby	k1gMnSc2	Moby
bytě	byt	k1gInSc6	byt
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
několik	několik	k4yIc1	několik
hostujících	hostující	k2eAgMnPc2d1	hostující
vokalistů	vokalista	k1gMnPc2	vokalista
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
několika	několik	k4yIc2	několik
předchozích	předchozí	k2eAgNnPc2d1	předchozí
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Play	play	k0	play
<g/>
,	,	kIx,	,
18	[number]	k4	18
nebo	nebo	k8xC	nebo
Wait	Wait	k1gInSc4	Wait
For	forum	k1gNnPc2	forum
Me	Me	k1gFnSc7	Me
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
alba	album	k1gNnSc2	album
Destroyed	Destroyed	k1gInSc1	Destroyed
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
fotografie	fotografia	k1gFnPc4	fotografia
utvářející	utvářející	k2eAgFnSc4d1	utvářející
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
stránku	stránka	k1gFnSc4	stránka
alba	album	k1gNnSc2	album
pořízené	pořízený	k2eAgFnSc2d1	pořízená
samotným	samotný	k2eAgInSc7d1	samotný
Mobym	Mobym	k1gInSc4	Mobym
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
A	a	k8xC	a
Case	Case	k1gFnSc1	Case
for	forum	k1gNnPc2	forum
Shame	Sham	k1gMnSc5	Sham
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
i	i	k9	i
dříve	dříve	k6eAd2	dříve
vydaná	vydaný	k2eAgFnSc1d1	vydaná
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Lonely	Lonela	k1gFnSc2	Lonela
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Produkci	produkce	k1gFnSc4	produkce
alba	album	k1gNnSc2	album
provedl	provést	k5eAaPmAgMnS	provést
držitel	držitel	k1gMnSc1	držitel
Grammy	Gramma	k1gFnSc2	Gramma
Award	Award	k1gMnSc1	Award
Mark	Mark	k1gMnSc1	Mark
"	"	kIx"	"
<g/>
Spike	Spike	k1gInSc1	Spike
<g/>
"	"	kIx"	"
Stent	stent	k1gInSc1	stent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
castingu	casting	k1gInSc2	casting
k	k	k7c3	k
hudebnímu	hudební	k2eAgNnSc3d1	hudební
videu	video	k1gNnSc3	video
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
najít	najít	k5eAaPmF	najít
"	"	kIx"	"
<g/>
obézní	obézní	k2eAgMnPc4d1	obézní
rychlosportovní	rychlosportovní	k2eAgMnPc4d1	rychlosportovní
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
,	,	kIx,	,
nahé	nahý	k2eAgMnPc4d1	nahý
duchy	duch	k1gMnPc4	duch
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
S	s	k7c7	s
<g/>
&	&	k?	&
<g/>
M	M	kA	M
pokroucence	pokroucenec	k1gMnSc2	pokroucenec
zběhlého	zběhlý	k2eAgMnSc2d1	zběhlý
v	v	k7c6	v
rytmické	rytmický	k2eAgFnSc6d1	rytmická
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
uveřejněno	uveřejněn	k2eAgNnSc1d1	Uveřejněno
<g/>
,	,	kIx,	,
že	že	k8xS	že
dalším	další	k2eAgInSc7d1	další
singlem	singl	k1gInSc7	singl
<g />
.	.	kIx.	.
</s>
<s>
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
The	The	k1gFnSc7	The
Perfect	Perfect	k2eAgInSc4d1	Perfect
Life	Life	k1gInSc4	Life
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
představil	představit	k5eAaPmAgMnS	představit
Wayne	Wayn	k1gInSc5	Wayn
Coyne	Coyn	k1gInSc5	Coyn
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Moby	Moba	k1gFnSc2	Moba
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
DJ	DJ	kA	DJ
setem	set	k1gInSc7	set
v	v	k7c4	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
odletěl	odletět	k5eAaPmAgMnS	odletět
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
vystoupit	vystoupit	k5eAaPmF	vystoupit
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
show	show	k1gFnSc6	show
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
DJoval	DJoval	k1gFnSc4	DJoval
k	k	k7c3	k
desátému	desátý	k4xOgInSc3	desátý
výročí	výročí	k1gNnSc2	výročí
Decibel	decibel	k1gInSc4	decibel
Festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
show	show	k1gNnPc6	show
na	na	k7c6	na
Fonda	Fonda	k1gFnSc1	Fonda
Theatre	Theatr	k1gInSc5	Theatr
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
propagace	propagace	k1gFnSc2	propagace
alba	album	k1gNnSc2	album
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gMnPc6	jeho
jediná	jediný	k2eAgFnSc1d1	jediná
"	"	kIx"	"
<g/>
full	fullit	k5eAaPmRp2nS	fullit
live	live	k1gInSc1	live
<g/>
"	"	kIx"	"
vystoupení	vystoupení	k1gNnSc1	vystoupení
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
jako	jako	k8xC	jako
důvod	důvod	k1gInSc1	důvod
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jezdím	jezdit	k5eAaImIp1nS	jezdit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
neustále	neustále	k6eAd1	neustále
někde	někde	k6eAd1	někde
posedávat	posedávat	k5eAaImF	posedávat
–	–	k?	–
v	v	k7c6	v
autech	aut	k1gInPc6	aut
<g/>
,	,	kIx,	,
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
A	a	k8xC	a
když	když	k8xS	když
takto	takto	k6eAd1	takto
někde	někde	k6eAd1	někde
nestále	stále	k6eNd1	stále
posedávám	posedávat	k5eAaImIp1nS	posedávat
<g/>
,	,	kIx,	,
nemohu	moct	k5eNaImIp1nS	moct
trávit	trávit	k5eAaImF	trávit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
tvořením	tvoření	k1gNnSc7	tvoření
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
chci	chtít	k5eAaImIp1nS	chtít
v	v	k7c6	v
životě	život	k1gInSc6	život
dělat	dělat	k5eAaImF	dělat
–	–	k?	–
zůstat	zůstat	k5eAaPmF	zůstat
doma	doma	k6eAd1	doma
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc1	tři
vystoupení	vystoupení	k1gNnPc1	vystoupení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Připustil	připustit	k5eAaPmAgMnS	připustit
však	však	k9	však
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
cestování	cestování	k1gNnSc3	cestování
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
These	these	k1gFnSc1	these
Systems	Systemsa	k1gFnPc2	Systemsa
Are	ar	k1gInSc5	ar
Failing	Failing	k1gInSc1	Failing
a	a	k8xC	a
More	mor	k1gInSc5	mor
Fast	Fast	k1gMnSc1	Fast
Songs	Songsa	k1gFnPc2	Songsa
About	About	k1gMnSc1	About
the	the	k?	the
Apocalypse	Apocalypse	k1gFnSc1	Apocalypse
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
2016	[number]	k4	2016
Moby	Moba	k1gFnSc2	Moba
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
These	these	k1gFnSc2	these
Systems	Systems	k1gInSc1	Systems
Are	ar	k1gInSc5	ar
Failing	Failing	k1gInSc1	Failing
<g/>
.	.	kIx.	.
</s>
<s>
Vydáno	vydat	k5eAaPmNgNnS	vydat
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Moby	Moba	k1gFnSc2	Moba
&	&	k?	&
The	The	k1gMnSc1	The
Void	Void	k1gMnSc1	Void
Pacific	Pacific	k1gMnSc1	Pacific
Choir	Choir	k1gMnSc1	Choir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
oznámení	oznámení	k1gNnSc4	oznámení
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
též	též	k9	též
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Are	ar	k1gInSc5	ar
You	You	k1gFnPc3	You
Lost	Lost	k1gInSc1	Lost
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
Like	Lik	k1gMnSc2	Lik
Me	Me	k1gMnSc2	Me
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
skladbu	skladba	k1gFnSc4	skladba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
animátor	animátor	k1gMnSc1	animátor
Steve	Steve	k1gMnSc1	Steve
Cutts	Cutts	k1gInSc1	Cutts
a	a	k8xC	a
pojednávalo	pojednávat	k5eAaImAgNnS	pojednávat
o	o	k7c6	o
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
a	a	k8xC	a
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
Webby	Webb	k1gInPc4	Webb
Award	Awarda	k1gFnPc2	Awarda
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zpřístupněna	zpřístupnit	k5eAaPmNgFnS	zpřístupnit
první	první	k4xOgFnSc1	první
skladba	skladba	k1gFnSc1	skladba
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nejmenovaného	jmenovaný	k2eNgNnSc2d1	nejmenované
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Leave	Leav	k1gMnSc5	Leav
Me	Me	k1gMnSc5	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
jako	jako	k8xC	jako
oznámení	oznámení	k1gNnSc4	oznámení
o	o	k7c4	o
Circle	Circle	k1gInSc4	Circle
V	v	k7c6	v
Festivalu	festival	k1gInSc6	festival
–	–	k?	–
veganském	veganský	k2eAgInSc6d1	veganský
festivalu	festival	k1gInSc6	festival
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
Mobym	Mobym	k1gInSc1	Mobym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
jeho	on	k3xPp3gInSc4	on
jediný	jediný	k2eAgInSc4d1	jediný
živý	živý	k2eAgInSc4d1	živý
výstup	výstup	k1gInSc4	výstup
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgMnS	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
More	mor	k1gInSc5	mor
Fast	Fast	k1gMnSc1	Fast
Songs	Songsa	k1gFnPc2	Songsa
About	About	k1gMnSc1	About
the	the	k?	the
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
zadarmo	zadarmo	k6eAd1	zadarmo
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
současnostː	současnostː	k?	současnostː
Everything	Everything	k1gInSc1	Everything
Was	Was	k1gMnSc1	Was
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
,	,	kIx,	,
and	and	k?	and
Nothing	Nothing	k1gInSc1	Nothing
Hurt	Hurt	k1gMnSc1	Hurt
===	===	k?	===
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
Moby	Moba	k1gFnSc2	Moba
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
přijde	přijít	k5eAaPmIp3nS	přijít
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
inspirovaným	inspirovaný	k2eAgNnSc7d1	inspirované
trip	trip	k1gMnSc1	trip
hopem	hopem	k?	hopem
<g/>
,	,	kIx,	,
Everything	Everything	k1gInSc1	Everything
Was	Was	k1gFnPc2	Was
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
,	,	kIx,	,
and	and	k?	and
Nothing	Nothing	k1gInSc1	Nothing
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
vydal	vydat	k5eAaPmAgMnS	vydat
dva	dva	k4xCgInPc4	dva
hudební	hudební	k2eAgInPc4d1	hudební
klipy	klip	k1gInPc4	klip
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
i	i	k9	i
třetí	třetí	k4xOgFnPc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Wild	Wilda	k1gFnPc2	Wilda
Darkness	Darknessa	k1gFnPc2	Darknessa
<g/>
"	"	kIx"	"
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
twitteru	twitter	k1gInSc6	twitter
popsal	popsat	k5eAaPmAgMnS	popsat
následovněː	následovněː	k?	následovněː
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
<g/>
]	]	kIx)	]
existenciální	existenciální	k2eAgInSc1d1	existenciální
rozhovor	rozhovor	k1gInSc1	rozhovor
mezi	mezi	k7c7	mezi
mnou	já	k3xPp1nSc7	já
a	a	k8xC	a
evangelickým	evangelický	k2eAgInSc7d1	evangelický
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
mluvím	mluvit	k5eAaImIp1nS	mluvit
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
zmatení	zmatení	k1gNnSc1	zmatení
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
s	s	k7c7	s
touhou	touha	k1gFnSc7	touha
a	a	k8xC	a
nadějí	naděje	k1gFnSc7	naděje
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Moby	Moba	k1gFnPc1	Moba
bydlí	bydlet	k5eAaImIp3nP	bydlet
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Garyho	Gary	k1gMnSc2	Gary
Gygaxe	Gygaxe	k1gFnSc2	Gygaxe
o	o	k7c6	o
sobě	se	k3xPyFc3	se
Moby	Moby	k1gInPc4	Moby
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
bývalé	bývalý	k2eAgMnPc4d1	bývalý
hráče	hráč	k1gMnPc4	hráč
hry	hra	k1gFnSc2	hra
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Moby	Moba	k1gFnSc2	Moba
ukázal	ukázat	k5eAaPmAgInS	ukázat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
celebritami	celebrita	k1gFnPc7	celebrita
ve	v	k7c6	v
videu	video	k1gNnSc6	video
podporujícím	podporující	k2eAgNnSc6d1	podporující
Chelseu	Chelseus	k1gInSc2	Chelseus
Manningovou	Manningový	k2eAgFnSc7d1	Manningová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
osloven	oslovit	k5eAaPmNgMnS	oslovit
přáteli	přítel	k1gMnPc7	přítel
ze	z	k7c2	z
CIA	CIA	kA	CIA
a	a	k8xC	a
skrz	skrz	k7c4	skrz
sociální	sociální	k2eAgNnPc4d1	sociální
média	médium	k1gNnPc4	médium
má	mít	k5eAaImIp3nS	mít
šířit	šířit	k5eAaImF	šířit
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
tajné	tajný	k2eAgFnSc6d1	tajná
dohodě	dohoda	k1gFnSc6	dohoda
mezi	mezi	k7c7	mezi
Trumpem	Trump	k1gInSc7	Trump
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Veganství	Veganství	k1gNnPc4	Veganství
a	a	k8xC	a
zvířecí	zvířecí	k2eAgNnPc4d1	zvířecí
práva	právo	k1gNnPc4	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Moby	Moba	k1gFnPc4	Moba
je	být	k5eAaImIp3nS	být
zatvrzelý	zatvrzelý	k2eAgInSc1d1	zatvrzelý
vegan	vegan	k1gInSc1	vegan
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
boj	boj	k1gInSc4	boj
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
prvotinou	prvotina	k1gFnSc7	prvotina
jakožto	jakožto	k8xS	jakožto
spisovatele	spisovatel	k1gMnSc2	spisovatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
svazek	svazek	k1gInSc1	svazek
Gristle	Gristle	k1gFnSc2	Gristle
<g/>
:	:	kIx,	:
From	From	k1gMnSc1	From
Factory	Factor	k1gMnPc7	Factor
Farms	Farms	k1gInSc1	Farms
to	ten	k3xDgNnSc4	ten
Food	Food	k1gMnSc1	Food
Safety	Safeta	k1gFnSc2	Safeta
(	(	kIx(	(
<g/>
Thinking	Thinking	k1gInSc1	Thinking
Twice	Twice	k1gMnSc1	Twice
About	About	k1gMnSc1	About
the	the	k?	the
Meat	Meat	k1gMnSc1	Meat
We	We	k1gMnSc1	We
Eat	Eat	k1gMnSc1	Eat
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
esejí	esej	k1gFnPc2	esej
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
založil	založit	k5eAaPmAgMnS	založit
veganskou	veganský	k2eAgFnSc4d1	veganská
restauraci	restaurace	k1gFnSc4	restaurace
Little	Little	k1gFnSc2	Little
Pine	pin	k1gInSc5	pin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
ji	on	k3xPp3gFnSc4	on
on	on	k3xPp3gMnSc1	on
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
Moby	Moba	k1gFnSc2	Moba
podpořil	podpořit	k5eAaPmAgMnS	podpořit
kampaň	kampaň	k1gFnSc4	kampaň
#	#	kIx~	#
<g/>
TurnYourNoseUp	TurnYourNoseUp	k1gInSc4	TurnYourNoseUp
bojující	bojující	k2eAgMnPc4d1	bojující
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
velkochovů	velkochov	k1gInPc2	velkochov
<g/>
,	,	kIx,	,
spolupracující	spolupracující	k2eAgFnSc4d1	spolupracující
s	s	k7c7	s
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
organizací	organizace	k1gFnSc7	organizace
Farms	Farmsa	k1gFnPc2	Farmsa
Not	nota	k1gFnPc2	nota
Factories	Factories	k1gInSc1	Factories
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Farmy	farma	k1gFnSc2	farma
<g/>
,	,	kIx,	,
ne	ne	k9	ne
továrny	továrna	k1gFnSc2	továrna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Užívání	užívání	k1gNnSc1	užívání
drog	droga	k1gFnPc2	droga
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Psychology	psycholog	k1gMnPc7	psycholog
Today	Todaa	k1gMnSc2	Todaa
Moby	Moba	k1gMnSc2	Moba
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
devatenáctiletý	devatenáctiletý	k2eAgMnSc1d1	devatenáctiletý
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
LSD	LSD	kA	LSD
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
následně	následně	k6eAd1	následně
trpět	trpět	k5eAaImF	trpět
panickou	panický	k2eAgFnSc7d1	panická
poruchou	porucha	k1gFnSc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
již	již	k9	již
nadále	nadále	k6eAd1	nadále
nepřepadá	přepadat	k5eNaImIp3nS	přepadat
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
"	"	kIx"	"
<g/>
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
dá	dát	k5eAaPmIp3nS	dát
trochu	trochu	k6eAd1	trochu
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vystresovaný	vystresovaný	k2eAgMnSc1d1	vystresovaný
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
pokazí	pokazit	k5eAaPmIp3nS	pokazit
<g/>
,	,	kIx,	,
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
znovu	znovu	k6eAd1	znovu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
této	tento	k3xDgFnSc2	tento
věci	věc	k1gFnSc2	věc
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
otevřený	otevřený	k2eAgInSc1d1	otevřený
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
pomoci	pomoc	k1gFnSc2	pomoc
fanouškům	fanoušek	k1gMnPc3	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
podobnými	podobný	k2eAgFnPc7d1	podobná
panickými	panický	k2eAgFnPc7d1	panická
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
tázán	tázán	k2eAgInSc4d1	tázán
na	na	k7c4	na
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
odpovědělː	odpovědělː	k?	odpovědělː
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
libertarián	libertarián	k1gMnSc1	libertarián
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	lid	k1gInSc7	lid
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
dělat	dělat	k5eAaImF	dělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Přenechávám	přenechávat	k5eAaImIp1nS	přenechávat
každému	každý	k3xTgMnSc3	každý
dospělému	dospělý	k1gMnSc3	dospělý
člověku	člověk	k1gMnSc3	člověk
volnost	volnost	k1gFnSc4	volnost
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
se	se	k3xPyFc4	se
svobodně	svobodně	k6eAd1	svobodně
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
řeklː	řeklː	k?	řeklː
"	"	kIx"	"
<g/>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejbídnějších	bídný	k2eAgFnPc2d3	nejbídnější
věcí	věc	k1gFnPc2	věc
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
naprosto	naprosto	k6eAd1	naprosto
zničený	zničený	k2eAgMnSc1d1	zničený
z	z	k7c2	z
kocoviny	kocovina	k1gFnSc2	kocovina
a	a	k8xC	a
v	v	k7c4	v
osm	osm	k4xCc4	osm
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
postávat	postávat	k5eAaImF	postávat
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
drsné	drsný	k2eAgFnSc6d1	drsná
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
těžkým	těžký	k2eAgMnSc7d1	těžký
alkoholikem	alkoholik	k1gMnSc7	alkoholik
a	a	k8xC	a
pravidelným	pravidelný	k2eAgMnSc7d1	pravidelný
uživatelem	uživatel	k1gMnSc7	uživatel
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
na	na	k7c6	na
živém	živý	k2eAgNnSc6d1	živé
dubnovém	dubnový	k2eAgNnSc6d1	dubnové
konferenčním	konferenční	k2eAgNnSc6d1	konferenční
vystoupení	vystoupení	k1gNnSc6	vystoupení
2014	[number]	k4	2014
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
řekl	říct	k5eAaPmAgMnS	říct
na	na	k7c6	na
živém	živý	k2eAgNnSc6d1	živé
dubnovém	dubnový	k2eAgNnSc6d1	dubnové
konferenčním	konferenční	k2eAgNnSc6d1	konferenční
vystoupení	vystoupení	k1gNnSc6	vystoupení
2014	[number]	k4	2014
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
abstinentem	abstinent	k1gMnSc7	abstinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Duchovní	duchovní	k2eAgInSc1d1	duchovní
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Moby	Moba	k1gFnPc1	Moba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zdráhal	zdráhat	k5eAaImAgInS	zdráhat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgInSc2	samý
slovem	slovem	k6eAd1	slovem
"	"	kIx"	"
<g/>
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nejasnosti	nejasnost	k1gFnSc2	nejasnost
významu	význam	k1gInSc2	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
<g/>
;	;	kIx,	;
nazval	nazvat	k5eAaPmAgMnS	nazvat
se	se	k3xPyFc4	se
tak	tak	k9	tak
však	však	k9	však
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Moby	Moba	k1gFnSc2	Moba
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
evangeliiː	evangeliiː	k?	evangeliiː
"	"	kIx"	"
<g/>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
jsem	být	k5eAaImIp1nS	být
četl	číst	k5eAaImAgMnS	číst
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
mne	já	k3xPp1nSc4	já
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
způsobem	způsob	k1gInSc7	způsob
božský	božský	k2eAgMnSc1d1	božský
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
říkám	říkat	k5eAaImIp1nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
miluji	milovat	k5eAaImIp1nS	milovat
Krista	Krista	k1gFnSc1	Krista
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
tom	ten	k3xDgMnSc6	ten
nejjednodušším	jednoduchý	k2eAgMnSc6d3	nejjednodušší
<g/>
,	,	kIx,	,
nejnaivnějším	naivní	k2eAgMnSc6d3	nejnaivnější
a	a	k8xC	a
nejsubjektivnějším	subjektivní	k2eAgInSc6d3	subjektivní
smyslu	smysl	k1gInSc6	smysl
toho	ten	k3xDgNnSc2	ten
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Neříkám	říkat	k5eNaImIp1nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
bych	by	kYmCp1nS	by
nekritizoval	kritizovat	k5eNaImAgMnS	kritizovat
víru	víra	k1gFnSc4	víra
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Moby	Moba	k1gFnPc1	Moba
řeklː	řeklː	k?	řeklː
"	"	kIx"	"
<g/>
Opravdu	opravdu	k6eAd1	opravdu
jistě	jistě	k9	jistě
nemohu	moct	k5eNaImIp1nS	moct
vědět	vědět	k5eAaImF	vědět
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
sice	sice	k8xC	sice
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
subjektivní	subjektivní	k2eAgFnSc6d1	subjektivní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
miluji	milovat	k5eAaImIp1nS	milovat
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Vnímám	vnímat	k5eAaImIp1nS	vnímat
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvrdím	tvrdit	k5eAaImIp1nS	tvrdit
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
malý	malý	k2eAgInSc4d1	malý
a	a	k8xC	a
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
starý	starý	k2eAgInSc4d1	starý
tak	tak	k8xC	tak
jako	jako	k8xS	jako
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
žiji	žít	k5eAaImIp1nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
víru	vír	k1gInSc2	vír
beru	brát	k5eAaImIp1nS	brát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cítil	cítit	k5eAaImAgMnS	cítit
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
nepříjemně	příjemně	k6eNd1	příjemně
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgInS	mít
někomu	někdo	k3yInSc3	někdo
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
nutně	nutně	k6eAd1	nutně
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
audio	audio	k2eAgInSc6d1	audio
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
časopisem	časopis	k1gInSc7	časopis
Sojournersː	Sojournersː	k1gMnSc1	Sojournersː
"	"	kIx"	"
<g/>
Čtu	číst	k5eAaImIp1nS	číst
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
evangelia	evangelium	k1gNnPc1	evangelium
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
jejich	jejich	k3xOp3gNnSc7	jejich
božstvím	božství	k1gNnSc7	božství
<g/>
,	,	kIx,	,
cítíc	cítit	k5eAaImSgFnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
toto	tento	k3xDgNnSc4	tento
nemohli	moct	k5eNaImAgMnP	moct
rozřešit	rozřešit	k5eAaPmF	rozřešit
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nejsme	být	k5eNaImIp1nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
bystří	bystrý	k2eAgMnPc1d1	bystrý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
víře	víra	k1gFnSc6	víra
hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
Moby	Moba	k1gFnSc2	Moba
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Friends	Friends	k1gInSc4	Friends
of	of	k?	of
God	God	k1gFnSc2	God
<g/>
,	,	kIx,	,
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
o	o	k7c6	o
evangelikalismu	evangelikalismus	k1gInSc6	evangelikalismus
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
Státech	stát	k1gInPc6	stát
Alexandry	Alexandra	k1gFnSc2	Alexandra
Pelosiové	Pelosiový	k2eAgFnSc2d1	Pelosiová
<g/>
,	,	kIx,	,
napsalː	napsalː	k?	napsalː
"	"	kIx"	"
<g/>
Film	film	k1gInSc1	film
mi	já	k3xPp1nSc3	já
připomněl	připomnět	k5eAaPmAgInS	připomnět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
agenda	agenda	k1gFnSc1	agenda
pravdy	pravda	k1gFnSc2	pravda
evangelického	evangelický	k2eAgNnSc2d1	evangelické
křesťanství	křesťanství	k1gNnSc2	křesťanství
zcela	zcela	k6eAd1	zcela
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
učení	učení	k1gNnSc2	učení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Mother	Mothra	k1gFnPc2	Mothra
Jones	Jones	k1gMnSc1	Jones
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
Moby	Moba	k1gFnSc2	Moba
tázán	tázán	k2eAgInSc1d1	tázán
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
křesťana	křesťan	k1gMnSc4	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
odveden	odveden	k2eAgInSc1d1	odveden
<g/>
...	...	k?	...
od	od	k7c2	od
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
konvenčního	konvenční	k2eAgNnSc2d1	konvenční
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
že	že	k8xS	že
"	"	kIx"	"
<g/>
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Moby	Moba	k1gFnSc2	Moba
Mother	Mothra	k1gFnPc2	Mothra
Jones	Jones	k1gMnSc1	Jones
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
nějak	nějak	k6eAd1	nějak
označit	označit	k5eAaPmF	označit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
by	by	kYmCp3nS	by
nejspíš	nejspíš	k9	nejspíš
"	"	kIx"	"
<g/>
taoisto-křesťansko-agnostický	taoistořesťanskognostický	k2eAgMnSc1d1	taoisto-křesťansko-agnostický
kvantový	kvantový	k2eAgMnSc1d1	kvantový
mechanik	mechanik	k1gMnSc1	mechanik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
Mobyho	Moby	k1gMnSc2	Moby
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgMnSc1d1	viditelný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knize	kniha	k1gFnSc6	kniha
Porcelainː	Porcelainː	k1gMnSc1	Porcelainː
A	a	k8xC	a
Memoir	Memoir	k1gMnSc1	Memoir
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
představen	představit	k5eAaPmNgInS	představit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zbožný	zbožný	k2eAgMnSc1d1	zbožný
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
vegan	vegan	k1gMnSc1	vegan
<g/>
,	,	kIx,	,
a	a	k8xC	a
abstinent	abstinent	k1gMnSc1	abstinent
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Dobročinnost	dobročinnost	k1gFnSc4	dobročinnost
===	===	k?	===
</s>
</p>
<p>
<s>
Moby	Moba	k1gFnSc2	Moba
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
různých	různý	k2eAgFnPc2d1	různá
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
MoveOn	MoveOn	k1gNnSc1	MoveOn
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
Humane	Human	k1gMnSc5	Human
Society	societa	k1gFnPc4	societa
<g/>
,	,	kIx,	,
Farm	Farm	k1gInSc4	Farm
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
MobyGratis	MobyGratis	k1gFnSc2	MobyGratis
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
licencuje	licencovat	k5eAaBmIp3nS	licencovat
filmovou	filmový	k2eAgFnSc4d1	filmová
hudbu	hudba	k1gFnSc4	hudba
zadarmo	zadarmo	k6eAd1	zadarmo
pro	pro	k7c4	pro
neziskové	ziskový	k2eNgInPc4d1	neziskový
a	a	k8xC	a
nezávislé	závislý	k2eNgInPc4d1	nezávislý
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
z	z	k7c2	z
filmů	film	k1gInPc2	film
výnosy	výnos	k1gInPc1	výnos
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nadace	nadace	k1gFnSc2	nadace
The	The	k1gFnSc2	The
Humane	Human	k1gMnSc5	Human
Society	societa	k1gFnPc4	societa
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc7d1	kulturní
režisérkou	režisérka	k1gFnSc7	režisérka
MoveOn	MoveOno	k1gNnPc2	MoveOno
Laurou	Laura	k1gFnSc7	Laura
Dawnovou	Dawnový	k2eAgFnSc7d1	Dawnový
a	a	k8xC	a
s	s	k7c7	s
výkonným	výkonný	k2eAgMnSc7d1	výkonný
ředitelem	ředitel	k1gMnSc7	ředitel
MoveOn	MoveOna	k1gFnPc2	MoveOna
Eliem	Eli	k1gMnSc7	Eli
Pariserem	Pariser	k1gMnSc7	Pariser
Moby	Moba	k1gFnSc2	Moba
založil	založit	k5eAaPmAgMnS	založit
Bush	Bush	k1gMnSc1	Bush
in	in	k?	in
30	[number]	k4	30
Seconds	Seconds	k1gInSc1	Seconds
od	od	k7c2	od
MoveOn	MoveOna	k1gFnPc2	MoveOna
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Voter	Voter	k1gInSc1	Voter
Fund	fund	k1gInSc1	fund
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
Disco	disco	k1gNnSc2	disco
Lies	Liesa	k1gFnPc2	Liesa
z	z	k7c2	z
alba	album	k1gNnSc2	album
Last	Last	k1gMnSc1	Last
Night	Night	k1gMnSc1	Night
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
témata	téma	k1gNnPc4	téma
související	související	k2eAgNnSc4d1	související
s	s	k7c7	s
těžkým	těžký	k2eAgInSc7d1	těžký
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
s	s	k7c7	s
masovým	masový	k2eAgInSc7d1	masový
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
se	se	k3xPyFc4	se
také	také	k9	také
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
nonpartisánských	nonpartisánský	k2eAgFnPc2d1	nonpartisánský
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
Amend	Amenda	k1gFnPc2	Amenda
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
realizující	realizující	k2eAgInPc1d1	realizující
programy	program	k1gInPc1	program
prevence	prevence	k1gFnSc2	prevence
nemocí	nemoc	k1gFnPc2	nemoc
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
<g/>
Moby	Moba	k1gFnSc2	Moba
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Music	Musice	k1gFnPc2	Musice
and	and	k?	and
Neurologic	Neurologic	k1gMnSc1	Neurologic
Function	Function	k1gInSc1	Function
(	(	kIx(	(
<g/>
IMNF	IMNF	kA	IMNF
<g/>
)	)	kIx)	)
–	–	k?	–
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
vědeckému	vědecký	k2eAgNnSc3d1	vědecké
bádání	bádání	k1gNnSc3	bádání
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
klinické	klinický	k2eAgFnSc2d1	klinická
léčby	léčba	k1gFnSc2	léčba
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
lidí	člověk	k1gMnPc2	člověk
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
benefičních	benefiční	k2eAgInPc6d1	benefiční
koncertech	koncert	k1gInPc6	koncert
pořádaných	pořádaný	k2eAgInPc6d1	pořádaný
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
zvýšení	zvýšení	k1gNnSc4	zvýšení
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c4	o
muzikoterapii	muzikoterapie	k1gFnSc4	muzikoterapie
a	a	k8xC	a
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
IMNF	IMNF	kA	IMNF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
obdržel	obdržet	k5eAaPmAgInS	obdržet
cenu	cena	k1gFnSc4	cena
IMNF	IMNF	kA	IMNF
Music	Musice	k1gFnPc2	Musice
Has	hasit	k5eAaImRp2nS	hasit
Power	Power	k1gMnSc1	Power
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c4	o
muzikoterapii	muzikoterapie	k1gFnSc4	muzikoterapie
<g/>
,	,	kIx,	,
za	za	k7c4	za
obětavost	obětavost	k1gFnSc4	obětavost
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
jejich	jejich	k3xOp3gInSc2	jejich
programu	program	k1gInSc2	program
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
<g/>
Zastává	zastávat	k5eAaImIp3nS	zastávat
síťovou	síťový	k2eAgFnSc4d1	síťová
neutralitu	neutralita	k1gFnSc4	neutralita
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vypovídal	vypovídat	k5eAaPmAgMnS	vypovídat
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
problému	problém	k1gInSc6	problém
před	před	k7c7	před
výborem	výbor	k1gInSc7	výbor
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
založil	založit	k5eAaPmAgMnS	založit
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
zvanou	zvaný	k2eAgFnSc4d1	zvaná
MobyGratis	MobyGratis	k1gFnSc4	MobyGratis
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
nezávislé	závislý	k2eNgMnPc4d1	nezávislý
a	a	k8xC	a
neziskové	ziskový	k2eNgMnPc4d1	neziskový
filmaře	filmař	k1gMnPc4	filmař
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgMnPc4d1	filmový
studenty	student	k1gMnPc4	student
a	a	k8xC	a
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
hudbu	hudba	k1gFnSc4	hudba
zdarma	zdarma	k6eAd1	zdarma
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInSc4d1	vlastní
nezávislý	závislý	k2eNgInSc4d1	nezávislý
neziskový	ziskový	k2eNgInSc4d1	neziskový
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
video	video	k1gNnSc1	video
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
volnou	volný	k2eAgFnSc4d1	volná
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
Mobyho	Moby	k1gMnSc2	Moby
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
nicméně	nicméně	k8xC	nicméně
film	film	k1gInSc1	film
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
výtěžky	výtěžek	k1gInPc4	výtěžek
z	z	k7c2	z
komerčních	komerční	k2eAgInPc2d1	komerční
licenčních	licenční	k2eAgInPc2d1	licenční
poplatků	poplatek	k1gInPc2	poplatek
poskytnutých	poskytnutý	k2eAgInPc2d1	poskytnutý
skrz	skrz	k7c4	skrz
MobyGratis	MobyGratis	k1gInSc4	MobyGratis
jsou	být	k5eAaImIp3nP	být
předány	předán	k2eAgFnPc1d1	předána
The	The	k1gFnPc1	The
Humane	Human	k1gMnSc5	Human
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Moby	Moba	k1gFnSc2	Moba
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
Songs	Songs	k1gInSc4	Songs
for	forum	k1gNnPc2	forum
Tibet	Tibet	k1gInSc1	Tibet
–	–	k?	–
albu	alba	k1gFnSc4	alba
sloužícímu	sloužící	k1gMnSc3	sloužící
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
Tibetu	Tibet	k1gInSc2	Tibet
a	a	k8xC	a
současného	současný	k2eAgMnSc2d1	současný
dalajlámy	dalajláma	k1gMnSc2	dalajláma
Tändzina	Tändzin	k2eAgFnSc5d1	Tändzin
Gjamccho	Gjamccha	k1gFnSc5	Gjamccha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
Moby	Moba	k1gFnSc2	Moba
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
nadace	nadace	k1gFnSc2	nadace
Change	change	k1gFnSc2	change
Begins	Beginsa	k1gFnPc2	Beginsa
Within	Withina	k1gFnPc2	Withina
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
v	v	k7c4	v
Radio	radio	k1gNnSc4	radio
City	City	k1gFnSc2	City
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
transcendentální	transcendentální	k2eAgFnSc7d1	transcendentální
meditací	meditace	k1gFnSc7	meditace
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
využití	využití	k1gNnSc2	využití
meditace	meditace	k1gFnSc2	meditace
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
vysoce	vysoce	k6eAd1	vysoce
náchylným	náchylný	k2eAgMnPc3d1	náchylný
mladým	mladý	k2eAgMnPc3d1	mladý
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
pozvanými	pozvaný	k2eAgMnPc7d1	pozvaný
hosty	host	k1gMnPc7	host
a	a	k8xC	a
účinkujícími	účinkující	k1gMnPc7	účinkující
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
<g/>
,	,	kIx,	,
Donovan	Donovan	k1gMnSc1	Donovan
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Vedder	k1gInSc1	Vedder
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Harper	Harper	k1gInSc1	Harper
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Horn	Horn	k1gMnSc1	Horn
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Bettye	Bettyus	k1gMnSc5	Bettyus
LaVette	LaVett	k1gMnSc5	LaVett
<g/>
,	,	kIx,	,
Sheryl	Sheryl	k1gInSc4	Sheryl
Crow	Crow	k1gFnSc2	Crow
<g/>
,	,	kIx,	,
Angelo	Angela	k1gFnSc5	Angela
Badalamenti	Badalament	k1gMnPc1	Badalament
<g/>
,	,	kIx,	,
Russell	Russell	k1gMnSc1	Russell
Simmons	Simmons	k1gInSc1	Simmons
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Jerry	Jerro	k1gNnPc7	Jerro
Seinfeld	Seinfeld	k1gInSc1	Seinfeld
a	a	k8xC	a
Howard	Howard	k1gInSc1	Howard
Stern	sternum	k1gNnPc2	sternum
<g/>
.	.	kIx.	.
<g/>
Moby	Moba	k1gFnSc2	Moba
také	také	k9	také
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
podpořil	podpořit	k5eAaPmAgMnS	podpořit
nadaci	nadace	k1gFnSc4	nadace
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
(	(	kIx(	(
<g/>
DLF	DLF	kA	DLF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeleském	Angeleský	k2eAgNnSc6d1	Angeleský
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Ace	Ace	k1gFnSc6	Ace
Hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
The	The	k1gFnSc2	The
Music	Music	k1gMnSc1	Music
of	of	k?	of
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomohla	pomoct	k5eAaPmAgFnS	pomoct
získat	získat	k5eAaPmF	získat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
využití	využití	k1gNnSc3	využití
transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
meditace	meditace	k1gFnSc2	meditace
(	(	kIx(	(
<g/>
TM	TM	kA	TM
<g/>
)	)	kIx)	)
tisícům	tisíc	k4xCgInPc3	tisíc
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
mladých	mladý	k1gMnPc2	mladý
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
také	také	k9	také
oslavy	oslava	k1gFnPc1	oslava
desátého	desátý	k4xOgInSc2	desátý
výročí	výročí	k1gNnSc3	výročí
DLF	DLF	kA	DLF
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vystupující	vystupující	k2eAgMnPc4d1	vystupující
patřili	patřit	k5eAaImAgMnP	patřit
Angelo	Angela	k1gFnSc5	Angela
Badalamenti	Badalament	k1gMnPc1	Badalament
<g/>
,	,	kIx,	,
Chrysta	Chrysta	k1gMnSc1	Chrysta
Bellová	Bellová	k1gFnSc1	Bellová
<g/>
,	,	kIx,	,
Donovan	Donovan	k1gMnSc1	Donovan
<g/>
,	,	kIx,	,
Duran	Duran	k1gMnSc1	Duran
Duran	Duran	k1gMnSc1	Duran
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Julee	Julee	k1gFnSc1	Julee
Cruise	Cruise	k1gFnSc1	Cruise
<g/>
,	,	kIx,	,
Karen	Karen	k1gInSc1	Karen
O	O	kA	O
<g/>
,	,	kIx,	,
Kinny	Kinna	k1gFnPc1	Kinna
Landrum	Landrum	k1gNnSc1	Landrum
<g/>
,	,	kIx,	,
Lykke	Lykke	k1gFnSc1	Lykke
Li	li	k9	li
<g/>
,	,	kIx,	,
Rebekah	Rebekah	k1gMnSc1	Rebekah
Del	Del	k1gFnSc2	Del
Rio	Rio	k1gFnPc2	Rio
<g/>
,	,	kIx,	,
Rob	roba	k1gFnPc2	roba
Mathes	Mathesa	k1gFnPc2	Mathesa
<g/>
,	,	kIx,	,
Sky	Sky	k1gMnSc1	Sky
Ferreira	Ferreira	k1gMnSc1	Ferreira
<g/>
,	,	kIx,	,
Tennis	Tennis	k1gFnSc1	Tennis
<g/>
,	,	kIx,	,
Twin	Twin	k1gInSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
Zola	Zola	k1gMnSc1	Zola
Jesus	Jesus	k1gMnSc1	Jesus
a	a	k8xC	a
Wayne	Wayn	k1gInSc5	Wayn
Coyne	Coyn	k1gInSc5	Coyn
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
Drozd	Drozd	k1gMnSc1	Drozd
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnPc2	The
Flaming	Flaming	k1gInSc1	Flaming
Lips	Lipsa	k1gFnPc2	Lipsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
RIAA	RIAA	kA	RIAA
===	===	k?	===
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
Moby	Moba	k1gFnSc2	Moba
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
RIAA	RIAA	kA	RIAA
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
soudit	soudit	k5eAaImF	soudit
se	se	k3xPyFc4	se
s	s	k7c7	s
Jammie	Jammie	k1gFnSc1	Jammie
Thomas-Rassetovou	Thomas-Rassetový	k2eAgFnSc4d1	Thomas-Rassetový
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
ilegální	ilegální	k2eAgNnSc4d1	ilegální
stahování	stahování	k1gNnSc4	stahování
hudby	hudba	k1gFnSc2	hudba
z	z	k7c2	z
Kazaa	Kaza	k1gInSc2	Kaza
<g/>
.	.	kIx.	.
</s>
<s>
Nazval	nazvat	k5eAaPmAgMnS	nazvat
to	ten	k3xDgNnSc4	ten
naprostým	naprostý	k2eAgInSc7d1	naprostý
nesmyslem	nesmysl	k1gInSc7	nesmysl
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
RIAA	RIAA	kA	RIAA
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eseje	esej	k1gInPc4	esej
===	===	k?	===
</s>
</p>
<p>
<s>
Mnohá	mnohý	k2eAgFnSc1d1	mnohá
z	z	k7c2	z
Mobyho	Moby	k1gMnSc2	Moby
alb	alba	k1gFnPc2	alba
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
eseje	esej	k1gFnSc2	esej
sepsané	sepsaný	k2eAgFnSc2d1	sepsaná
jím	on	k3xPp3gMnSc7	on
samotným	samotný	k2eAgMnSc7d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
esej	esej	k1gFnSc4	esej
o	o	k7c6	o
přebytečné	přebytečný	k2eAgFnSc6d1	přebytečná
a	a	k8xC	a
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
spotřebě	spotřeba	k1gFnSc6	spotřeba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Používáme	používat	k5eAaImIp1nP	používat
toxická	toxický	k2eAgNnPc4d1	toxické
chlorová	chlorový	k2eAgNnPc4d1	chlorové
bělidla	bělidlo	k1gNnPc4	bělidlo
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
udrželi	udržet	k5eAaPmAgMnP	udržet
své	svůj	k3xOyFgInPc4	svůj
spodky	spodek	k1gInPc4	spodek
bílé	bílý	k2eAgInPc4d1	bílý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
církevních	církevní	k2eAgMnPc6d1	církevní
vládcích	vládce	k1gMnPc6	vládce
Spojených	spojený	k2eAgInPc2d1	spojený
Států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
nejde	jít	k5eNaImIp3nS	jít
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
pravice	pravice	k1gFnSc1	pravice
ven	ven	k6eAd1	ven
a	a	k8xC	a
nešíří	šířit	k5eNaImIp3nS	šířit
milosrdenství	milosrdenství	k1gNnSc1	milosrdenství
<g/>
,	,	kIx,	,
soucit	soucit	k1gInSc1	soucit
a	a	k8xC	a
nezištnost	nezištnost	k1gFnSc1	nezištnost
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
The	The	k1gFnSc6	The
End	End	k1gFnSc2	End
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
hovořil	hovořit	k5eAaImAgInS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
veganem	vegan	k1gInSc7	vegan
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dokázali	dokázat	k5eAaPmAgMnP	dokázat
byste	by	kYmCp2nP	by
se	se	k3xPyFc4	se
podívat	podívat	k5eAaPmF	podívat
zvířeti	zvíře	k1gNnSc3	zvíře
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
říct	říct	k5eAaPmF	říct
mu	on	k3xPp3gMnSc3	on
'	'	kIx"	'
<g/>
mé	můj	k3xOp1gFnSc2	můj
chutě	chuť	k1gFnSc2	chuť
jsou	být	k5eAaImIp3nP	být
důležitější	důležitý	k2eAgNnSc4d2	důležitější
než	než	k8xS	než
tvé	tvůj	k3xOp2gNnSc1	tvůj
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
'	'	kIx"	'
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
Animal	animal	k1gMnSc1	animal
Rights	Rights	k1gInSc4	Rights
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
pojetí	pojetí	k1gNnSc6	pojetí
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neupírali	upírat	k5eNaImAgMnP	upírat
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
homosexuálům	homosexuál	k1gMnPc3	homosexuál
a	a	k8xC	a
zvířatům	zvíře	k1gNnPc3	zvíře
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Před	před	k7c7	před
dávnými	dávný	k2eAgFnPc7d1	dávná
dobami	doba	k1gFnPc7	doba
měli	mít	k5eAaImAgMnP	mít
práva	právo	k1gNnPc4	právo
jen	jen	k8xS	jen
králové	králová	k1gFnPc4	králová
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
měli	mít	k5eAaImAgMnP	mít
práva	právo	k1gNnSc2	právo
i	i	k8xC	i
majetní	majetný	k2eAgMnPc1d1	majetný
bělošští	bělošský	k2eAgMnPc1d1	bělošský
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
všichni	všechen	k3xTgMnPc1	všechen
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
i	i	k9	i
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
mentálně	mentálně	k6eAd1	mentálně
postižení	postižený	k2eAgMnPc1d1	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
nejsme	být	k5eNaImIp1nP	být
ochotní	ochotný	k2eAgMnPc1d1	ochotný
poskytnout	poskytnout	k5eAaPmF	poskytnout
tato	tento	k3xDgNnPc4	tento
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
homosexuálům	homosexuál	k1gMnPc3	homosexuál
a	a	k8xC	a
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
kapitol	kapitola	k1gFnPc2	kapitola
Sound	Sound	k1gMnSc1	Sound
Unbound	Unbound	k1gMnSc1	Unbound
<g/>
:	:	kIx,	:
Sampling	Sampling	k1gInSc1	Sampling
Digital	Digital	kA	Digital
Music	Music	k1gMnSc1	Music
and	and	k?	and
Culture	Cultur	k1gMnSc5	Cultur
(	(	kIx(	(
<g/>
The	The	k1gMnSc3	The
MIT	MIT	kA	MIT
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
dělal	dělat	k5eAaImAgInS	dělat
Moby	Mob	k1gMnPc4	Mob
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Lucy	Luc	k1gMnPc7	Luc
Walkerovou	Walkerová	k1gFnSc4	Walkerová
<g/>
;	;	kIx,	;
kapitola	kapitola	k1gFnSc1	kapitola
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
upravena	upravit	k5eAaPmNgFnS	upravit
Paulem	Paul	k1gMnSc7	Paul
D.	D.	kA	D.
Millerem	Miller	k1gMnSc7	Miller
alias	alias	k9	alias
DJem	DJem	k1gInSc4	DJem
Spookym	Spookymum	k1gNnPc2	Spookymum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fotografování	fotografování	k1gNnSc2	fotografování
===	===	k?	===
</s>
</p>
<p>
<s>
Moby	Moby	k1gInPc1	Moby
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
obklopen	obklopit	k5eAaPmNgInS	obklopit
filmy	film	k1gInPc7	film
a	a	k8xC	a
temnými	temný	k2eAgFnPc7d1	temná
komorami	komora	k1gFnPc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
byl	být	k5eAaImAgMnS	být
fotografem	fotograf	k1gMnSc7	fotograf
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
Destroyed	Destroyed	k1gMnSc1	Destroyed
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
fotografie	fotografia	k1gFnPc4	fotografia
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
turné	turné	k1gNnPc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
jako	jako	k8xS	jako
Mobyho	Moby	k1gMnSc2	Moby
stejnojmenné	stejnojmenný	k2eAgNnSc1d1	stejnojmenné
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
představil	představit	k5eAaPmAgInS	představit
svou	svůj	k3xOyFgFnSc4	svůj
Innocents	Innocents	k1gInSc1	Innocents
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
sérii	série	k1gFnSc4	série
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Fremin	Fremin	k1gInSc4	Fremin
Gallery	Galler	k1gInPc4	Galler
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
galerií	galerie	k1gFnSc7	galerie
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
Moby	Moba	k1gFnSc2	Moba
vydal	vydat	k5eAaPmAgInS	vydat
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
čtyři	čtyři	k4xCgMnPc4	čtyři
sta	sto	k4xCgNnPc4	sto
šesti	šest	k4xCc6	šest
stránkovou	stránkový	k2eAgFnSc4d1	stránková
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Porcelainː	Porcelainː	k1gFnPc2	Porcelainː
A	a	k8xC	a
Memoir	Memoira	k1gFnPc2	Memoira
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Mobyho	Moby	k1gMnSc2	Moby
životě	život	k1gInSc6	život
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
sekcí	sekce	k1gFnPc2	sekce
rozčleněných	rozčleněný	k2eAgInPc2d1	rozčleněný
do	do	k7c2	do
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
prolog	prolog	k1gInSc4	prolog
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
a	a	k8xC	a
doslov	doslov	k1gInSc4	doslov
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
knihy	kniha	k1gFnSc2	kniha
tvoří	tvořit	k5eAaImIp3nS	tvořit
detailní	detailní	k2eAgNnSc4d1	detailní
vypravování	vypravování	k1gNnSc4	vypravování
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
situací	situace	k1gFnPc2	situace
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
je	být	k5eAaImIp3nS	být
také	také	k9	také
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
několika	několik	k4yIc7	několik
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Thousand	Thousand	k1gInSc1	Thousand
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pouze	pouze	k6eAd1	pouze
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
bubny	buben	k1gInPc1	buben
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
znějí	znět	k5eAaImIp3nP	znět
rychlostí	rychlost	k1gFnSc7	rychlost
1000	[number]	k4	1000
BPM	BPM	kA	BPM
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
šestnáct	šestnáct	k4xCc4	šestnáct
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Guinnessovy	Guinnessův	k2eAgFnSc2d1	Guinnessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mobyho	Mobyze	k6eAd1	Mobyze
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
kapelami	kapela	k1gFnPc7	kapela
jsou	být	k5eAaImIp3nP	být
Pantera	panter	k1gMnSc4	panter
<g/>
,	,	kIx,	,
Sepultura	Sepultura	k1gFnSc1	Sepultura
nebo	nebo	k8xC	nebo
Bad	Bad	k1gFnSc1	Bad
Brains	Brainsa	k1gFnPc2	Brainsa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
fanouškem	fanoušek	k1gMnSc7	fanoušek
Spice	Spice	k1gFnSc2	Spice
Girls	girl	k1gFnPc2	girl
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Spice	Spiec	k1gInPc1	Spiec
Up	Up	k1gFnSc1	Up
Your	Your	k1gInSc1	Your
Life	Life	k1gFnSc1	Life
<g/>
"	"	kIx"	"
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
členkou	členka	k1gFnSc7	členka
Emma	Emma	k1gFnSc1	Emma
Bunton	Bunton	k1gInSc1	Bunton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Moby	Moba	k1gFnSc2	Moba
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Ambient	Ambient	k1gMnSc1	Ambient
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Animal	animal	k1gMnSc1	animal
Rights	Rightsa	k1gFnPc2	Rightsa
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Play	play	k0	play
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
18	[number]	k4	18
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Hotel	hotel	k1gInSc1	hotel
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Last	Last	k1gMnSc1	Last
Night	Night	k1gMnSc1	Night
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Wait	Waita	k1gFnPc2	Waita
for	forum	k1gNnPc2	forum
Me	Me	k1gMnPc2	Me
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Destroyed	Destroyed	k1gMnSc1	Destroyed
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Innocents	Innocentsa	k1gFnPc2	Innocentsa
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Long	Long	k1gMnSc1	Long
Ambients	Ambients	k1gInSc4	Ambients
1	[number]	k4	1
<g/>
:	:	kIx,	:
Calm	Calma	k1gFnPc2	Calma
<g/>
.	.	kIx.	.
</s>
<s>
Sleep	Sleep	k1gMnSc1	Sleep
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
These	these	k1gFnSc1	these
Systems	Systemsa	k1gFnPc2	Systemsa
Are	ar	k1gInSc5	ar
Failing	Failing	k1gInSc1	Failing
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Void	Void	k1gMnSc1	Void
Pacific	Pacific	k1gMnSc1	Pacific
Choir	Choir	k1gMnSc1	Choir
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
-	-	kIx~	-
More	mor	k1gInSc5	mor
Fast	Fast	k1gMnSc1	Fast
Songs	Songsa	k1gFnPc2	Songsa
About	About	k1gMnSc1	About
the	the	k?	the
Apocalypse	Apocalypse	k1gFnSc1	Apocalypse
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Void	Void	k1gMnSc1	Void
Pacific	Pacific	k1gMnSc1	Pacific
Choir	Choir	k1gMnSc1	Choir
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
Everything	Everything	k1gInSc1	Everything
Was	Was	k1gFnSc2	Was
Beautiful	Beautifula	k1gFnPc2	Beautifula
<g/>
,	,	kIx,	,
and	and	k?	and
Nothing	Nothing	k1gInSc1	Nothing
Hurt	Hurt	k1gMnSc1	Hurt
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnPc1	kompilace
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Instinct	Instinct	k1gInSc1	Instinct
Dance	Danka	k1gFnSc3	Danka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Moby	Moby	k1gInPc1	Moby
<g/>
/	/	kIx~	/
<g/>
Barracuda	Barracuda	k1gFnSc1	Barracuda
<g/>
/	/	kIx~	/
<g/>
Brainstorm	Brainstorm	k1gInSc1	Brainstorm
<g/>
/	/	kIx~	/
<g/>
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Early	earl	k1gMnPc7	earl
Underground	underground	k1gInSc1	underground
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Rare	Rare	k1gInSc1	Rare
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Collected	Collected	k1gMnSc1	Collected
B-Sides	B-Sides	k1gMnSc1	B-Sides
1989-1993	[number]	k4	1989-1993
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Everything	Everything	k1gInSc1	Everything
Is	Is	k1gMnSc1	Is
Wrong	Wrong	k1gMnSc1	Wrong
–	–	k?	–
DJ	DJ	kA	DJ
Mix	mix	k1gInSc1	mix
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
remixové	remixový	k2eAgNnSc1d1	remixové
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
The	The	k1gMnSc1	The
End	End	k1gMnSc1	End
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
I	i	k9	i
Like	Like	k1gFnSc1	Like
to	ten	k3xDgNnSc4	ten
Score	Scor	k1gInSc5	Scor
(	(	kIx(	(
<g/>
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Songs	Songs	k1gInSc1	Songs
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Play	play	k0	play
<g/>
:	:	kIx,	:
The	The	k1gMnSc3	The
B	B	kA	B
Sides	Sides	k1gInSc1	Sides
(	(	kIx(	(
<g/>
béčkové	béčkový	k2eAgFnPc1d1	béčková
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Play	play	k0	play
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
18	[number]	k4	18
B	B	kA	B
Sides	Sidesa	k1gFnPc2	Sidesa
+	+	kIx~	+
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
remixy	remix	k1gInPc1	remix
<g/>
,	,	kIx,	,
koncert	koncert	k1gInSc1	koncert
a	a	k8xC	a
béčkové	béčkový	k2eAgFnPc1d1	béčková
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Moby	Moba	k1gFnSc2	Moba
<g/>
:	:	kIx,	:
Live	Live	k1gInSc1	Live
–	–	k?	–
Hotel	hotel	k1gInSc1	hotel
Tour	Tour	k1gInSc1	Tour
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
Live	Live	k1gFnSc6	Live
DVD	DVD	kA	DVD
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
remixované	remixovaný	k2eAgFnSc2d1	remixovaná
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Baby	baba	k1gFnSc2	baba
Monkey	Monkea	k1gFnSc2	Monkea
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Go	Go	k1gMnSc1	Go
–	–	k?	–
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Moby	Moba	k1gFnSc2	Moba
(	(	kIx(	(
<g/>
výběrová	výběrový	k2eAgFnSc1d1	výběrová
deska	deska	k1gFnSc1	deska
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Go	Go	k1gMnSc1	Go
–	–	k?	–
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Moby	Moba	k1gFnSc2	Moba
<g/>
:	:	kIx,	:
Remixed	Remixed	k1gInSc1	Remixed
(	(	kIx(	(
<g/>
remixové	remixový	k2eAgNnSc1d1	remixové
album	album	k1gNnSc1	album
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Videografie	Videografie	k1gFnSc2	Videografie
==	==	k?	==
</s>
</p>
<p>
<s>
Play	play	k0	play
<g/>
:	:	kIx,	:
The	The	k1gFnSc3	The
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
18	[number]	k4	18
B	B	kA	B
Sides	Sidesa	k1gFnPc2	Sidesa
+	+	kIx~	+
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Hotel	hotel	k1gInSc1	hotel
Tour	Tour	k1gInSc1	Tour
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Go	Go	k?	Go
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Moby	Moba	k1gFnSc2	Moba
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Go	Go	k?	Go
<g/>
:	:	kIx,	:
A	a	k9	a
Film	film	k1gInSc1	film
About	About	k1gInSc1	About
Moby	Moba	k1gMnSc2	Moba
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Moby	Moba	k1gFnSc2	Moba
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MELVILLE	MELVILLE	kA	MELVILLE
HALL	HALL	kA	HALL
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Porcelainː	Porcelainː	k?	Porcelainː
A	a	k9	a
Memoir	Memoir	k1gMnSc1	Memoir
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Faber	Faber	k1gInSc1	Faber
&	&	k?	&
Faber	Faber	k1gInSc1	Faber
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
409	[number]	k4	409
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
571	[number]	k4	571
<g/>
-	-	kIx~	-
<g/>
32148	[number]	k4	32148
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moby	Moba	k1gFnSc2	Moba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Moby	Moba	k1gFnSc2	Moba
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Fanouškovské	fanouškovský	k2eAgFnPc4d1	fanouškovská
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Biografie	biografie	k1gFnPc1	biografie
a	a	k8xC	a
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
stránek	stránka	k1gFnPc2	stránka
moby	moba	k1gFnSc2	moba
<g/>
.	.	kIx.	.
<g/>
borec	borec	k1gMnSc1	borec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
