<p>
<s>
Sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
(	(	kIx(	(
<g/>
SPN	SPN	kA	SPN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnPc4d1	přenosná
infekce	infekce	k1gFnPc4	infekce
(	(	kIx(	(
<g/>
SPI	spát	k5eAaImRp2nS	spát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nemoci	nemoc	k1gFnPc4	nemoc
obvykle	obvykle	k6eAd1	obvykle
přenášené	přenášený	k2eAgFnPc1d1	přenášená
mezi	mezi	k7c4	mezi
partnery	partner	k1gMnPc4	partner
nějakou	nějaký	k3yIgFnSc7	nějaký
formou	forma	k1gFnSc7	forma
sexuální	sexuální	k2eAgFnSc2d1	sexuální
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
přes	přes	k7c4	přes
vaginální	vaginální	k2eAgInSc4d1	vaginální
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
orální	orální	k2eAgInSc4d1	orální
sex	sex	k1gInSc4	sex
nebo	nebo	k8xC	nebo
anální	anální	k2eAgInSc4d1	anální
sex	sex	k1gInSc4	sex
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgFnP	nazývat
venerické	venerický	k2eAgFnPc1d1	venerická
(	(	kIx(	(
<g/>
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
<g/>
)	)	kIx)	)
nemoci	nemoc	k1gFnPc1	nemoc
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
představitelé	představitel	k1gMnPc1	představitel
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
zavedli	zavést	k5eAaPmAgMnP	zavést
nový	nový	k2eAgInSc4d1	nový
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
přehledu	přehled	k1gInSc2	přehled
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
problematice	problematika	k1gFnSc6	problematika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ale	ale	k9	ale
i	i	k9	i
když	když	k8xS	když
termíny	termín	k1gInPc1	termín
SPN	SPN	kA	SPN
a	a	k8xC	a
SPI	spát	k5eAaImRp2nS	spát
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používán	k2eAgInPc1d1	používán
jako	jako	k9	jako
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
,	,	kIx,	,
neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
označují	označovat	k5eAaImIp3nP	označovat
stejnou	stejný	k2eAgFnSc4d1	stejná
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
web	web	k1gInSc1	web
https://web.archive.org/web/20180815113345/http://www.etharc.org/:	[url]	k4	https://web.archive.org/web/20180815113345/http://www.etharc.org/:
"	"	kIx"	"
<g/>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
termíny	termín	k1gInPc1	termín
SPI	spát	k5eAaImRp2nS	spát
a	a	k8xC	a
SPN	SPN	kA	SPN
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
a	a	k8xC	a
často	často	k6eAd1	často
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pomůže	pomoct	k5eAaPmIp3nS	pomoct
nejdřív	dříve	k6eAd3	dříve
pochopit	pochopit	k5eAaPmF	pochopit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
infekcí	infekce	k1gFnSc7	infekce
a	a	k8xC	a
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
jednoduše	jednoduše	k6eAd1	jednoduše
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikrob	mikrob	k1gInSc1	mikrob
–	–	k?	–
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
baktérie	baktérie	k1gFnPc1	baktérie
nebo	nebo	k8xC	nebo
parazit	parazit	k1gMnSc1	parazit
–	–	k?	–
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
nemoc	nemoc	k1gFnSc4	nemoc
nebo	nebo	k8xC	nebo
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgMnSc1d1	přítomný
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
osoba	osoba	k1gFnSc1	osoba
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
mít	mít	k5eAaImF	mít
symptomy	symptom	k1gInPc4	symptom
nebo	nebo	k8xC	nebo
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
nebo	nebo	k8xC	nebo
baktérie	baktérie	k1gFnPc1	baktérie
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nP	muset
nutně	nutně	k6eAd1	nutně
cítit	cítit	k5eAaImF	cítit
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
infekce	infekce	k1gFnSc1	infekce
už	už	k6eAd1	už
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
u	u	k7c2	u
infikované	infikovaný	k2eAgFnSc2d1	infikovaná
osoby	osoba	k1gFnSc2	osoba
pocit	pocit	k1gInSc4	pocit
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
nevolnosti	nevolnost	k1gFnSc2	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
termín	termín	k1gInSc1	termín
SPI	spát	k5eAaImRp2nS	spát
označující	označující	k2eAgNnPc1d1	označující
infekce	infekce	k1gFnSc2	infekce
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
mikrobem	mikrob	k1gInSc7	mikrob
způsobujícím	způsobující	k2eAgMnSc7d1	způsobující
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
osoba	osoba	k1gFnSc1	osoba
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
symptomy	symptom	k1gInPc4	symptom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgInSc4d2	širší
termín	termín	k1gInSc4	termín
než	než	k8xS	než
termín	termín	k1gInSc4	termín
SPN	SPN	kA	SPN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
SPN	SPN	kA	SPN
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
infekce	infekce	k1gFnPc4	infekce
způsobující	způsobující	k2eAgInPc4d1	způsobující
symptomy	symptom	k1gInPc1	symptom
nebo	nebo	k8xC	nebo
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
lidé	člověk	k1gMnPc1	člověk
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
infikováni	infikován	k2eAgMnPc1d1	infikován
s	s	k7c7	s
SPI	spát	k5eAaImRp2nS	spát
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
neobjeví	objevit	k5eNaPmIp3nP	objevit
symptomy	symptom	k1gInPc1	symptom
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
termín	termín	k1gInSc1	termín
SPI	spát	k5eAaImRp2nS	spát
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
též	též	k6eAd1	též
vhodný	vhodný	k2eAgMnSc1d1	vhodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
SPI	spát	k5eAaImRp2nS	spát
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemůže	moct	k5eNaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
přítomné	přítomný	k2eAgInPc1d1	přítomný
nějaké	nějaký	k3yIgInPc1	nějaký
znaky	znak	k1gInPc1	znak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
osoba	osoba	k1gFnSc1	osoba
pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
herpesovou	herpesový	k2eAgFnSc4d1	herpesový
infekci	infekce	k1gFnSc4	infekce
(	(	kIx(	(
<g/>
opar	opar	k1gInSc4	opar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
puchýře	puchýř	k1gInSc2	puchýř
(	(	kIx(	(
<g/>
SPN	SPN	kA	SPN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
neobjeví	objevit	k5eNaPmIp3nS	objevit
(	(	kIx(	(
<g/>
SPI	spát	k5eAaImRp2nS	spát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
HIV	HIV	kA	HIV
infekci	infekce	k1gFnSc6	infekce
(	(	kIx(	(
<g/>
SPI	spát	k5eAaImRp2nS	spát
<g/>
)	)	kIx)	)
kdykoli	kdykoli	k6eAd1	kdykoli
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
neobjevili	objevit	k5eNaPmAgMnP	objevit
symptomy	symptom	k1gInPc4	symptom
AIDS	AIDS	kA	AIDS
(	(	kIx(	(
<g/>
SPN	SPN	kA	SPN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenos	přenos	k1gInSc1	přenos
(	(	kIx(	(
<g/>
transmise	transmise	k1gFnSc1	transmise
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc4	každý
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
osobou	osoba	k1gFnSc7	osoba
nebo	nebo	k8xC	nebo
s	s	k7c7	s
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
druhé	druhý	k4xOgFnSc2	druhý
osoby	osoba	k1gFnSc2	osoba
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
risk	risk	k1gInSc4	risk
přenosu	přenos	k1gInSc2	přenos
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
AIDS	aids	k1gInSc4	aids
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každá	každý	k3xTgFnSc1	každý
SPN	SPN	kA	SPN
značí	značit	k5eAaImIp3nS	značit
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc2d1	přenosná
nemoci	nemoc	k1gFnSc2	nemoc
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
určitými	určitý	k2eAgFnPc7d1	určitá
sexuálními	sexuální	k2eAgFnPc7d1	sexuální
aktivitami	aktivita	k1gFnPc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Baktérie	baktérie	k1gFnPc1	baktérie
<g/>
,	,	kIx,	,
plísně	plíseň	k1gFnPc1	plíseň
<g/>
,	,	kIx,	,
prvoci	prvok	k1gMnPc1	prvok
nebo	nebo	k8xC	nebo
viry	vir	k1gInPc4	vir
jsou	být	k5eAaImIp3nP	být
činitelé	činitel	k1gMnPc1	činitel
přenosu	přenos	k1gInSc2	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
nemoc	nemoc	k1gFnSc4	nemoc
ze	z	k7c2	z
sexuální	sexuální	k2eAgFnSc2d1	sexuální
aktivity	aktivita	k1gFnSc2	aktivita
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemoc	nemoc	k1gFnSc1	nemoc
nemá	mít	k5eNaImIp3nS	mít
a	a	k8xC	a
obráceně	obráceně	k6eAd1	obráceně
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
mající	mající	k2eAgFnSc2d1	mající
SPN	SPN	kA	SPN
ji	on	k3xPp3gFnSc4	on
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
kontaktu	kontakt	k1gInSc2	kontakt
(	(	kIx(	(
<g/>
sexuálního	sexuální	k2eAgMnSc2d1	sexuální
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgMnSc2d1	jiný
<g/>
)	)	kIx)	)
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgInS	mít
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
přenosu	přenos	k1gInSc2	přenos
rozličných	rozličný	k2eAgFnPc2d1	rozličná
nemocí	nemoc	k1gFnPc2	nemoc
různými	různý	k2eAgFnPc7d1	různá
sexuálními	sexuální	k2eAgFnPc7d1	sexuální
aktivitami	aktivita	k1gFnPc7	aktivita
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
všechny	všechen	k3xTgFnPc4	všechen
sexuální	sexuální	k2eAgFnPc4d1	sexuální
aktivity	aktivita	k1gFnPc4	aktivita
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
lidmi	člověk	k1gMnPc7	člověk
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
dvousměrnou	dvousměrný	k2eAgFnSc4d1	dvousměrná
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
SPN	SPN	kA	SPN
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
předání	předání	k1gNnSc1	předání
<g/>
"	"	kIx"	"
i	i	k9	i
"	"	kIx"	"
<g/>
příjem	příjem	k1gInSc1	příjem
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
risk	risk	k1gInSc1	risk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdravotní	zdravotní	k2eAgMnPc1d1	zdravotní
profesionálové	profesionál	k1gMnPc1	profesionál
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
např.	např.	kA	např.
použití	použití	k1gNnSc1	použití
kondomů	kondom	k1gInPc2	kondom
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
sexuální	sexuální	k2eAgFnSc6d1	sexuální
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
absolutní	absolutní	k2eAgFnSc4d1	absolutní
záruku	záruka	k1gFnSc4	záruka
<g/>
.	.	kIx.	.
</s>
<s>
Abstinence	abstinence	k1gFnSc1	abstinence
od	od	k7c2	od
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
aktivit	aktivita	k1gFnPc2	aktivita
zahrnující	zahrnující	k2eAgMnPc4d1	zahrnující
jiné	jiný	k2eAgMnPc4d1	jiný
lidi	člověk	k1gMnPc4	člověk
ochrání	ochránit	k5eAaPmIp3nS	ochránit
proti	proti	k7c3	proti
sexuálnímu	sexuální	k2eAgInSc3d1	sexuální
přenosu	přenos	k1gInSc3	přenos
těchto	tento	k3xDgFnPc2	tento
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
infekcí	infekce	k1gFnPc2	infekce
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
SPN	SPN	kA	SPN
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
též	též	k6eAd1	též
přenášeny	přenášet	k5eAaImNgInP	přenášet
jinými	jiný	k2eAgFnPc7d1	jiná
aktivitami	aktivita	k1gFnPc7	aktivita
jako	jako	k9	jako
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krevní	krevní	k2eAgFnSc1d1	krevní
transfuze	transfuze	k1gFnSc1	transfuze
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
krevní	krevní	k2eAgInPc1d1	krevní
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnPc1	sdílení
injekčních	injekční	k2eAgFnPc2d1	injekční
jehel	jehla	k1gFnPc2	jehla
<g/>
,	,	kIx,	,
úrazy	úraz	k1gInPc4	úraz
s	s	k7c7	s
propíchnutím	propíchnutí	k1gNnSc7	propíchnutí
jehlou	jehla	k1gFnSc7	jehla
(	(	kIx(	(
<g/>
když	když	k8xS	když
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
personál	personál	k1gInSc1	personál
je	být	k5eAaImIp3nS	být
neúmyslně	úmyslně	k6eNd1	úmyslně
píchnut	píchnout	k5eAaPmNgInS	píchnout
jehlou	jehla	k1gFnSc7	jehla
během	během	k7c2	během
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
procedury	procedura	k1gFnSc2	procedura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc4	sdílení
jehel	jehla	k1gFnPc2	jehla
pro	pro	k7c4	pro
tetování	tetování	k1gNnSc4	tetování
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
porod	porod	k1gInSc1	porod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prostředky	prostředek	k1gInPc1	prostředek
přenosu	přenos	k1gInSc2	přenos
značí	značit	k5eAaImIp3nP	značit
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
risk	risk	k1gInSc4	risk
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
skupiny	skupina	k1gFnPc4	skupina
osob	osoba	k1gFnPc2	osoba
jako	jako	k8xC	jako
zdravotníci	zdravotník	k1gMnPc1	zdravotník
<g/>
,	,	kIx,	,
hemofilici	hemofilik	k1gMnPc1	hemofilik
a	a	k8xC	a
narkomani	narkoman	k1gMnPc1	narkoman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současné	současný	k2eAgFnPc1d1	současná
epidemiologické	epidemiologický	k2eAgFnPc1d1	epidemiologická
studie	studie	k1gFnPc1	studie
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
komunitní	komunitní	k2eAgMnPc1d1	komunitní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
sexuálním	sexuální	k2eAgInSc7d1	sexuální
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
a	a	k8xC	a
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastnosti	vlastnost	k1gFnPc1	vlastnost
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
sítí	síť	k1gFnPc2	síť
jsou	být	k5eAaImIp3nP	být
podstatné	podstatný	k2eAgInPc1d1	podstatný
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
tříděné	tříděný	k2eAgNnSc1d1	tříděné
míchání	míchání	k1gNnSc1	míchání
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
partnerů	partner	k1gMnPc2	partner
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
prostitutky	prostitutka	k1gFnPc1	prostitutka
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
mít	mít	k5eAaImF	mít
mnoho	mnoho	k6eAd1	mnoho
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
prostituce	prostituce	k1gFnPc1	prostituce
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
cestovatelé	cestovatel	k1gMnPc1	cestovatel
jako	jako	k8xC	jako
řidiči	řidič	k1gMnPc1	řidič
kamionů	kamion	k1gInPc2	kamion
a	a	k8xC	a
námořníci	námořník	k1gMnPc1	námořník
mívají	mívat	k5eAaImIp3nP	mívat
víc	hodně	k6eAd2	hodně
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
partnerů	partner	k1gMnPc2	partner
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
prostitutky	prostitutka	k1gFnPc4	prostitutka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
jsou	být	k5eAaImIp3nP	být
potenciálně	potenciálně	k6eAd1	potenciálně
přenášeny	přenášen	k2eAgFnPc1d1	přenášena
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
formě	forma	k1gFnSc6	forma
sexuálního	sexuální	k2eAgInSc2d1	sexuální
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
do	do	k7c2	do
sexuálních	sexuální	k2eAgInPc2d1	sexuální
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
sexuální	sexuální	k2eAgInPc4d1	sexuální
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
být	být	k5eAaImF	být
asymptomatickým	asymptomatický	k2eAgInSc7d1	asymptomatický
(	(	kIx(	(
<g/>
bezpříznakovým	bezpříznakový	k2eAgInSc7d1	bezpříznakový
<g/>
)	)	kIx)	)
přenašečem	přenašeč	k1gInSc7	přenašeč
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
často	často	k6eAd1	často
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vážný	vážný	k2eAgInSc4d1	vážný
stav	stav	k1gInSc4	stav
pánevní	pánevní	k2eAgFnSc2d1	pánevní
zánětlivé	zánětlivý	k2eAgFnSc2d1	zánětlivá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgFnPc1d1	známá
stovky	stovka	k1gFnPc1	stovka
let	let	k1gInSc1	let
–	–	k?	–
angličtina	angličtina	k1gFnSc1	angličtina
má	mít	k5eAaImIp3nS	mít
zkratky	zkratka	k1gFnPc4	zkratka
pro	pro	k7c4	pro
2	[number]	k4	2
z	z	k7c2	z
nejčastějších	častý	k2eAgFnPc2d3	nejčastější
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
pox	pox	k?	pox
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
the	the	k?	the
clap	clap	k1gInSc1	clap
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kapavka	kapavka	k1gFnSc1	kapavka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
objevením	objevení	k1gNnSc7	objevení
moderních	moderní	k2eAgInPc2d1	moderní
léků	lék	k1gInPc2	lék
byly	být	k5eAaImAgFnP	být
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnPc1d1	přenosná
nemoci	nemoc	k1gFnPc1	nemoc
obecně	obecně	k6eAd1	obecně
neléčitelné	léčitelný	k2eNgFnPc1d1	neléčitelná
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
byla	být	k5eAaImAgFnS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
symptomů	symptom	k1gInPc2	symptom
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
klinika	klinika	k1gFnSc1	klinika
pro	pro	k7c4	pro
sexuální	sexuální	k2eAgFnPc4d1	sexuální
nemoci	nemoc	k1gFnPc4	nemoc
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1747	[number]	k4	1747
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
-	-	kIx~	-
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Dock	Dock	k1gMnSc1	Dock
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
efektivní	efektivní	k2eAgInSc1d1	efektivní
prostředek	prostředek	k1gInSc1	prostředek
léčby	léčba	k1gFnSc2	léčba
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc2d1	přenosná
nemoci	nemoc	k1gFnSc2	nemoc
byl	být	k5eAaImAgInS	být
salvarsan	salvarsan	k1gInSc1	salvarsan
-	-	kIx~	-
lék	lék	k1gInSc1	lék
na	na	k7c4	na
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objevem	objev	k1gInSc7	objev
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
stalo	stát	k5eAaPmAgNnS	stát
snadno	snadno	k6eAd1	snadno
léčitelnými	léčitelný	k2eAgInPc7d1	léčitelný
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
efektivními	efektivní	k2eAgFnPc7d1	efektivní
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
kampaněmi	kampaň	k1gFnPc7	kampaň
proti	proti	k7c3	proti
SPI	spát	k5eAaImRp2nS	spát
vedlo	vést	k5eAaImAgNnS	vést
během	běh	k1gInSc7	běh
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestaly	přestat	k5eAaPmAgFnP	přestat
být	být	k5eAaImF	být
vážným	vážný	k2eAgInSc7d1	vážný
medicínským	medicínský	k2eAgInSc7d1	medicínský
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
poznán	poznán	k2eAgInSc1d1	poznán
význam	význam	k1gInSc1	význam
sledování	sledování	k1gNnSc2	sledování
kontaktů	kontakt	k1gInPc2	kontakt
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
SPI	spát	k5eAaImRp2nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
partnerů	partner	k1gMnPc2	partner
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
testování	testování	k1gNnSc1	testování
na	na	k7c4	na
infekce	infekce	k1gFnPc4	infekce
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
a	a	k8xC	a
následně	následně	k6eAd1	následně
sledování	sledování	k1gNnSc1	sledování
jejích	její	k3xOp3gInPc2	její
kontaktů	kontakt	k1gInPc2	kontakt
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kliniky	klinika	k1gFnPc1	klinika
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
SPI	spát	k5eAaImRp2nS	spát
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
efektivními	efektivní	k2eAgFnPc7d1	efektivní
v	v	k7c4	v
potlačení	potlačení	k1gNnSc4	potlačení
infekcí	infekce	k1gFnPc2	infekce
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
genitální	genitální	k2eAgInSc1d1	genitální
herpes	herpes	k1gInSc1	herpes
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k8xC	i
AIDS	AIDS	kA	AIDS
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
jako	jako	k8xS	jako
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnPc4d1	přenosná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
moderní	moderní	k2eAgFnSc1d1	moderní
medicína	medicína	k1gFnSc1	medicína
nedovede	dovést	k5eNaPmIp3nS	dovést
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
AIDS	AIDS	kA	AIDS
má	mít	k5eAaImIp3nS	mít
asymptomatickou	asymptomatický	k2eAgFnSc4d1	asymptomatická
periodu	perioda	k1gFnSc4	perioda
umožňující	umožňující	k2eAgFnSc2d1	umožňující
nemoci	nemoc	k1gFnSc2	nemoc
rozšířit	rozšířit	k5eAaPmF	rozšířit
se	se	k3xPyFc4	se
na	na	k7c4	na
jiné	jiný	k2eAgMnPc4d1	jiný
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
následuje	následovat	k5eAaImIp3nS	následovat
symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
perioda	perioda	k1gFnSc1	perioda
vedoucí	vedoucí	k1gFnSc1	vedoucí
bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
skonu	skon	k1gInSc3	skon
<g/>
.	.	kIx.	.
</s>
<s>
Poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
AIDS	AIDS	kA	AIDS
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
globální	globální	k2eAgFnSc4d1	globální
pandemii	pandemie	k1gFnSc4	pandemie
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
veřejným	veřejný	k2eAgFnPc3d1	veřejná
informačním	informační	k2eAgFnPc3d1	informační
kampaním	kampaň	k1gFnPc3	kampaň
a	a	k8xC	a
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
léčby	léčba	k1gFnSc2	léčba
umožňující	umožňující	k2eAgNnSc4d1	umožňující
řízení	řízení	k1gNnSc4	řízení
AIDS	aids	k1gInSc1	aids
potlačením	potlačení	k1gNnSc7	potlačení
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
kontaktů	kontakt	k1gInPc2	kontakt
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
důležitý	důležitý	k2eAgInSc4d1	důležitý
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
nemoci	nemoc	k1gFnPc1	nemoc
neléčitelné	léčitelný	k2eNgFnPc1d1	neléčitelná
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
šíření	šíření	k1gNnSc2	šíření
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
je	být	k5eAaImIp3nS	být
nejspolehlivější	spolehlivý	k2eAgInSc4d3	nejspolehlivější
způsob	způsob	k1gInSc4	způsob
ochrany	ochrana	k1gFnSc2	ochrana
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
sexuálně	sexuálně	k6eAd1	sexuálně
přenosným	přenosný	k2eAgFnPc3d1	přenosná
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
,	,	kIx,	,
léčitelným	léčitelný	k2eAgFnPc3d1	léčitelná
i	i	k8xC	i
neléčitelným	léčitelný	k2eNgFnPc3d1	neléčitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typa	k1gFnPc1	typa
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
kauzativní	kauzativní	k2eAgInPc1d1	kauzativní
organismy	organismus	k1gInPc1	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
nemoci	nemoc	k1gFnPc4	nemoc
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
seznamu	seznam	k1gInSc2	seznam
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
sexuálně	sexuálně	k6eAd1	sexuálně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nemocí	nemoc	k1gFnPc2	nemoc
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
přenášeny	přenášet	k5eAaImNgInP	přenášet
i	i	k9	i
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
než	než	k8xS	než
sexuálně	sexuálně	k6eAd1	sexuálně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
HIV	HIV	kA	HIV
i	i	k8xC	i
AIDS	AIDS	kA	AIDS
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
přenášené	přenášený	k2eAgInPc1d1	přenášený
infikovanými	infikovaný	k2eAgFnPc7d1	infikovaná
jehlami	jehla	k1gFnPc7	jehla
u	u	k7c2	u
narkomanů	narkoman	k1gMnPc2	narkoman
a	a	k8xC	a
kandidóza	kandidóza	k1gFnSc1	kandidóza
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenesena	přenést	k5eAaPmNgFnS	přenést
sexuálně	sexuálně	k6eAd1	sexuálně
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
aktivitou	aktivita	k1gFnSc7	aktivita
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bakteriální	bakteriální	k2eAgMnPc4d1	bakteriální
===	===	k?	===
</s>
</p>
<p>
<s>
Syfilis	syfilis	k1gFnSc1	syfilis
(	(	kIx(	(
<g/>
Treponema	Treponema	k1gFnSc1	Treponema
pallidum	pallidum	k1gInSc1	pallidum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kapavka	kapavka	k1gFnSc1	kapavka
(	(	kIx(	(
<g/>
Neisseria	Neisserium	k1gNnPc1	Neisserium
gonorrhoeae	gonorrhoeae	k1gFnPc2	gonorrhoeae
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chlamydiová	Chlamydiový	k2eAgFnSc1d1	Chlamydiová
infekce	infekce	k1gFnSc1	infekce
(	(	kIx(	(
<g/>
Chlamydia	Chlamydium	k1gNnPc1	Chlamydium
trachomatis	trachomatis	k1gFnPc2	trachomatis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Měkký	měkký	k2eAgInSc1d1	měkký
vřed	vřed	k1gInSc1	vřed
(	(	kIx(	(
<g/>
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
ducreyi	ducrey	k1gFnSc2	ducrey
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Granuloma	Granuloma	k1gFnSc1	Granuloma
inguinale	inguinale	k6eAd1	inguinale
či	či	k8xC	či
donovanóza	donovanóza	k1gFnSc1	donovanóza
(	(	kIx(	(
<g/>
Klebsiella	Klebsiella	k1gFnSc1	Klebsiella
granulomatis	granulomatis	k1gFnSc1	granulomatis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lymfogranuloma	Lymfogranuloma	k1gFnSc1	Lymfogranuloma
venereum	venereum	k1gInSc1	venereum
(	(	kIx(	(
<g/>
LGV	LGV	kA	LGV
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Chlamydia	Chlamydium	k1gNnPc1	Chlamydium
trachomatis	trachomatis	k1gFnPc2	trachomatis
-	-	kIx~	-
serotypy	serotyp	k1gInPc1	serotyp
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chlamydie	chlamydie	k1gFnSc2	chlamydie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Negonokokální	Negonokokální	k2eAgFnSc1d1	Negonokokální
uretritida	uretritida	k1gFnSc1	uretritida
(	(	kIx(	(
<g/>
NGU	NGU	kA	NGU
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Ureaplasma	Ureaplasma	k1gNnSc1	Ureaplasma
urealyticum	urealyticum	k1gNnSc1	urealyticum
nebo	nebo	k8xC	nebo
Mycoplasma	Mycoplasma	k1gNnSc1	Mycoplasma
hominis	hominis	k1gFnSc2	hominis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Virové	virový	k2eAgMnPc4d1	virový
===	===	k?	===
</s>
</p>
<p>
<s>
Herpes	herpes	k1gInSc1	herpes
(	(	kIx(	(
<g/>
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gNnSc1	simplex
virus	virus	k1gInSc1	virus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
papilomavirus	papilomavirus	k1gInSc1	papilomavirus
(	(	kIx(	(
<g/>
Human	Human	k1gMnSc1	Human
papillomavirus	papillomavirus	k1gMnSc1	papillomavirus
-	-	kIx~	-
HPV	HPV	kA	HPV
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
HPV	HPV	kA	HPV
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
genitální	genitální	k2eAgFnPc1d1	genitální
bradavice	bradavice	k1gFnPc1	bradavice
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
HPV	HPV	kA	HPV
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
cervikální	cervikální	k2eAgFnPc1d1	cervikální
dysplazie	dysplazie	k1gFnPc1	dysplazie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
cervikální	cervikální	k2eAgFnSc3d1	cervikální
rakovině	rakovina	k1gFnSc3	rakovina
</s>
</p>
<p>
<s>
Herpes	herpes	k1gInSc1	herpes
virus	virus	k1gInSc1	virus
-	-	kIx~	-
virus	virus	k1gInSc1	virus
slinné	slinný	k2eAgFnSc2d1	slinná
žlázy	žláza	k1gFnSc2	žláza
(	(	kIx(	(
<g/>
Cytomegalovirus	Cytomegalovirus	k1gInSc1	Cytomegalovirus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
(	(	kIx(	(
<g/>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
A	a	k9	a
a	a	k8xC	a
Hepatitida	hepatitida	k1gFnSc1	hepatitida
E	E	kA	E
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgFnP	přenášet
fekálně-orální	fekálněrální	k2eAgFnSc7d1	fekálně-orální
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
ne	ne	k9	ne
sexuálně	sexuálně	k6eAd1	sexuálně
<g/>
;	;	kIx,	;
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
není	být	k5eNaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
</s>
</p>
<p>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
D	D	kA	D
</s>
</p>
<p>
<s>
===	===	k?	===
Parazitické	parazitický	k2eAgMnPc4d1	parazitický
===	===	k?	===
</s>
</p>
<p>
<s>
Veš	veš	k1gFnSc1	veš
muňka	muňka	k?	muňka
(	(	kIx(	(
<g/>
Phthirius	Phthirius	k1gMnSc1	Phthirius
pubis	pubis	k1gFnSc2	pubis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svrab	svrab	k1gInSc1	svrab
(	(	kIx(	(
<g/>
Sarcoptes	Sarcoptes	k1gInSc1	Sarcoptes
scabiei	scabie	k1gFnSc2	scabie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Plísňové	plísňový	k2eAgMnPc4d1	plísňový
===	===	k?	===
</s>
</p>
<p>
<s>
Kandidóza	Kandidóza	k1gFnSc1	Kandidóza
(	(	kIx(	(
<g/>
Candida	Candida	k1gFnSc1	Candida
albicans	albicans	k1gInSc1	albicans
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
striktně	striktně	k6eAd1	striktně
SPN	SPN	kA	SPN
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenesena	přenést	k5eAaPmNgFnS	přenést
sexuálním	sexuální	k2eAgInSc7d1	sexuální
kontaktem	kontakt	k1gInSc7	kontakt
<g/>
...	...	k?	...
<g/>
vulvitidy	vulvitida	k1gFnPc1	vulvitida
<g/>
,	,	kIx,	,
vulvovaginitidy	vulvovaginitis	k1gFnPc1	vulvovaginitis
<g/>
,	,	kIx,	,
balanitidy	balanitis	k1gFnPc1	balanitis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Protozoální	Protozoální	k2eAgFnPc4d1	Protozoální
(	(	kIx(	(
<g/>
z	z	k7c2	z
prvoků	prvok	k1gMnPc2	prvok
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Amebóza	Amebóza	k1gFnSc1	Amebóza
(	(	kIx(	(
<g/>
Entamoeba	Entamoeba	k1gMnSc1	Entamoeba
histolytica	histolytica	k1gMnSc1	histolytica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Giardióza	Giardióza	k1gFnSc1	Giardióza
(	(	kIx(	(
<g/>
Giardia	Giardium	k1gNnSc2	Giardium
lamblia	lamblium	k1gNnSc2	lamblium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trichomoniáza	Trichomoniáza	k1gFnSc1	Trichomoniáza
(	(	kIx(	(
<g/>
Trichomonas	Trichomonas	k1gInSc1	Trichomonas
vaginalis	vaginalis	k1gFnSc2	vaginalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vulvovaginálni	Vulvovaginálnit	k5eAaPmRp2nS	Vulvovaginálnit
poruchy	porucha	k1gFnPc1	porucha
</s>
</p>
<p>
<s>
Přenos	přenos	k1gInSc1	přenos
(	(	kIx(	(
<g/>
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hirsuties	Hirsuties	k1gInSc1	Hirsuties
papillaris	papillaris	k1gFnSc2	papillaris
genitalis	genitalis	k1gFnSc2	genitalis
-	-	kIx~	-
drobné	drobný	k2eAgInPc1d1	drobný
bílé	bílý	k2eAgInPc1d1	bílý
výstupky	výstupek	k1gInPc1	výstupek
žláz	žláza	k1gFnPc2	žláza
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
<g/>
)	)	kIx)	)
žaludu	žalud	k1gInSc2	žalud
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
nemoc	nemoc	k1gFnSc4	nemoc
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sexuálně	sexuálně	k6eAd1	sexuálně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
nemoc	nemoc	k1gFnSc1	nemoc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
záchodové	záchodový	k2eAgNnSc1d1	záchodové
prkénko	prkénko	k1gNnSc1	prkénko
aneb	aneb	k?	aneb
největší	veliký	k2eAgInPc4d3	veliký
mýty	mýtus	k1gInPc4	mýtus
o	o	k7c6	o
pohlavních	pohlavní	k2eAgFnPc6d1	pohlavní
chorobách	choroba	k1gFnPc6	choroba
</s>
</p>
