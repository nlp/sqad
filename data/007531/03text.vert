<s>
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Reduta	reduta	k1gFnSc1	reduta
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postavit	postavit	k5eAaPmF	postavit
novou	nový	k2eAgFnSc4d1	nová
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
dokončena	dokončen	k2eAgFnSc1d1	dokončena
stavba	stavba	k1gFnSc1	stavba
nového	nový	k2eAgNnSc2d1	nové
německého	německý	k2eAgNnSc2d1	německé
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Na	na	k7c6	na
hradbách	hradba	k1gFnPc6	hradba
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Mahenova	Mahenův	k2eAgNnSc2d1	Mahenovo
divadla	divadlo	k1gNnSc2	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
měnit	měnit	k5eAaImF	měnit
dispozice	dispozice	k1gFnPc4	dispozice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
dokončena	dokončit	k5eAaPmNgFnS	dokončit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
-	-	kIx~	-
od	od	k7c2	od
započetí	započetí	k1gNnSc2	započetí
stavby	stavba	k1gFnSc2	stavba
do	do	k7c2	do
otevření	otevření	k1gNnSc2	otevření
divadla	divadlo	k1gNnSc2	divadlo
za	za	k7c4	za
necelých	celý	k2eNgInPc2d1	necelý
17	[number]	k4	17
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výstavby	výstavba	k1gFnSc2	výstavba
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
nové	nový	k2eAgFnPc1d1	nová
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	se	k3xPyFc4	se
neblahými	blahý	k2eNgFnPc7d1	neblahá
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
získanými	získaný	k2eAgFnPc7d1	získaná
při	při	k7c6	při
požárech	požár	k1gInPc6	požár
tří	tři	k4xCgNnPc2	tři
evropských	evropský	k2eAgNnPc2d1	Evropské
divadel	divadlo	k1gNnPc2	divadlo
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
-	-	kIx~	-
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
divadla	divadlo	k1gNnSc2	divadlo
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Nice	Nice	k1gFnPc6	Nice
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
pak	pak	k6eAd1	pak
sotva	sotva	k6eAd1	sotva
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
nejhůře	zle	k6eAd3	zle
postiženého	postižený	k2eAgNnSc2d1	postižené
Okružního	okružní	k2eAgNnSc2d1	okružní
divadla	divadlo	k1gNnSc2	divadlo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
brněnské	brněnský	k2eAgNnSc1d1	brněnské
divadlo	divadlo	k1gNnSc1	divadlo
vyvarovalo	vyvarovat	k5eAaPmAgNnS	vyvarovat
podobných	podobný	k2eAgFnPc2d1	podobná
tragických	tragický	k2eAgFnPc2d1	tragická
eventualit	eventualita	k1gFnPc2	eventualita
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
mimo	mimo	k7c4	mimo
plány	plán	k1gInPc4	plán
pořízeny	pořízen	k2eAgInPc4d1	pořízen
další	další	k2eAgInPc4d1	další
nové	nový	k2eAgInPc4d1	nový
východy	východ	k1gInPc4	východ
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
dvě	dva	k4xCgNnPc4	dva
nová	nový	k2eAgNnPc4d1	nové
postranní	postranní	k2eAgNnPc4d1	postranní
schodiště	schodiště	k1gNnPc4	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pozdržení	pozdržení	k1gNnSc1	pozdržení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
odvážné	odvážný	k2eAgNnSc1d1	odvážné
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
smělé	smělý	k2eAgNnSc1d1	smělé
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
:	:	kIx,	:
nahradit	nahradit	k5eAaPmF	nahradit
navržené	navržený	k2eAgNnSc4d1	navržené
plynové	plynový	k2eAgNnSc4d1	plynové
osvětlení	osvětlení	k1gNnSc4	osvětlení
zcela	zcela	k6eAd1	zcela
novým	nový	k2eAgMnSc7d1	nový
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
starým	staré	k1gNnSc7	staré
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
vynálezem	vynález	k1gInSc7	vynález
Thomase	Thomas	k1gMnSc2	Thomas
Alvy	Alva	k1gMnSc2	Alva
Edisona	Edison	k1gMnSc2	Edison
-	-	kIx~	-
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
žárovkami	žárovka	k1gFnPc7	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
elektroinstalace	elektroinstalace	k1gFnSc2	elektroinstalace
provedli	provést	k5eAaPmAgMnP	provést
sám	sám	k3xTgInSc4	sám
legendární	legendární	k2eAgInSc4d1	legendární
Edison	Edison	k1gInSc4	Edison
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
laboratoř	laboratoř	k1gFnSc4	laboratoř
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
elektroinstalaci	elektroinstalace	k1gFnSc3	elektroinstalace
pak	pak	k6eAd1	pak
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
a	a	k8xC	a
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
divadla	divadlo	k1gNnSc2	divadlo
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1882	[number]	k4	1882
uvedením	uvedení	k1gNnSc7	uvedení
Beethovenovy	Beethovenův	k2eAgFnSc2d1	Beethovenova
předehry	předehra	k1gFnSc2	předehra
a	a	k8xC	a
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
Egmont	Egmonta	k1gFnPc2	Egmonta
(	(	kIx(	(
<g/>
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
divadlo	divadlo	k1gNnSc1	divadlo
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
hradbách	hradba	k1gFnPc6	hradba
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
prvním	první	k4xOgMnSc7	první
dramaturgem	dramaturg	k1gMnSc7	dramaturg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
brněnský	brněnský	k2eAgMnSc1d1	brněnský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahno	k1gNnPc2	Mahno
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
jméno	jméno	k1gNnSc4	jméno
dnes	dnes	k6eAd1	dnes
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
nového	nový	k2eAgNnSc2d1	nové
Janáčkova	Janáčkův	k2eAgNnSc2d1	Janáčkovo
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
sloužilo	sloužit	k5eAaImAgNnS	sloužit
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
převážně	převážně	k6eAd1	převážně
opernímu	operní	k2eAgInSc3d1	operní
souboru	soubor	k1gInSc3	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
divadle	divadlo	k1gNnSc6	divadlo
bylo	být	k5eAaImAgNnS	být
např.	např.	kA	např.
poprvé	poprvé	k6eAd1	poprvé
uvedeno	uvést	k5eAaPmNgNnS	uvést
šest	šest	k4xCc1	šest
oper	opera	k1gFnPc2	opera
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
a	a	k8xC	a
také	také	k9	také
světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
baletu	balet	k1gInSc2	balet
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
Sergeje	Sergej	k1gMnSc2	Sergej
Prokofjeva	Prokofjev	k1gMnSc2	Prokofjev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
je	být	k5eAaImIp3nS	být
Mahenovo	Mahenův	k2eAgNnSc4d1	Mahenovo
divadlo	divadlo	k1gNnSc4	divadlo
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
scénou	scéna	k1gFnSc7	scéna
činoherního	činoherní	k2eAgInSc2d1	činoherní
souboru	soubor	k1gInSc2	soubor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc1	svůj
představení	představení	k1gNnPc1	představení
zde	zde	k6eAd1	zde
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
soubory	soubor	k1gInPc1	soubor
NDB	NDB	kA	NDB
<g/>
.	.	kIx.	.
</s>
<s>
Činohra	činohra	k1gFnSc1	činohra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
divadelních	divadelní	k2eAgMnPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
profesionálně	profesionálně	k6eAd1	profesionálně
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
jeviště	jeviště	k1gNnSc2	jeviště
visí	viset	k5eAaImIp3nS	viset
lustr	lustr	k1gInSc4	lustr
původně	původně	k6eAd1	původně
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
pro	pro	k7c4	pro
posledního	poslední	k2eAgMnSc4d1	poslední
íránského	íránský	k2eAgMnSc4d1	íránský
šáha	šáh	k1gMnSc4	šáh
Muhammada	Muhammada	k1gFnSc1	Muhammada
Rezu	rez	k1gInSc2	rez
Pahlavího	Pahlaví	k2eAgInSc2d1	Pahlaví
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
zadní	zadní	k2eAgFnSc4d1	zadní
stranu	strana	k1gFnSc4	strana
divadla	divadlo	k1gNnSc2	divadlo
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
5	[number]	k4	5
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
kulisy	kulisa	k1gFnPc1	kulisa
a	a	k8xC	a
rekvizity	rekvizit	k1gInPc1	rekvizit
k	k	k7c3	k
některým	některý	k3yIgNnPc3	některý
představením	představení	k1gNnPc3	představení
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mahenova	Mahenův	k2eAgNnSc2d1	Mahenovo
divadla	divadlo	k1gNnSc2	divadlo
dodala	dodat	k5eAaPmAgFnS	dodat
proslulá	proslulý	k2eAgFnSc1d1	proslulá
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
firma	firma	k1gFnSc1	firma
Fellner	Fellner	k1gMnSc1	Fellner
a	a	k8xC	a
Helmer	Helmer	k1gMnSc1	Helmer
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
architekty	architekt	k1gMnPc7	architekt
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Fellnerem	Fellner	k1gMnSc7	Fellner
a	a	k8xC	a
Hermannem	Hermann	k1gMnSc7	Hermann
Helmerem	Helmer	k1gMnSc7	Helmer
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgNnPc2d1	jiné
evropských	evropský	k2eAgNnPc2d1	Evropské
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
stavbu	stavba	k1gFnSc4	stavba
však	však	k9	však
provedl	provést	k5eAaPmAgMnS	provést
brněnský	brněnský	k2eAgMnSc1d1	brněnský
městský	městský	k2eAgMnSc1d1	městský
stavitel	stavitel	k1gMnSc1	stavitel
Josef	Josef	k1gMnSc1	Josef
Arnold	Arnold	k1gMnSc1	Arnold
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
architekta	architekt	k1gMnSc2	architekt
J.	J.	kA	J.
Nebehostenyho	Nebehosteny	k1gMnSc2	Nebehosteny
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
ve	v	k7c6	v
eklektickém	eklektický	k2eAgInSc6d1	eklektický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
novorenesance	novorenesance	k1gFnSc2	novorenesance
<g/>
,	,	kIx,	,
neobaroka	neobaroek	k1gInSc2	neobaroek
a	a	k8xC	a
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
počet	počet	k1gInSc1	počet
sedadel	sedadlo	k1gNnPc2	sedadlo
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
úprav	úprava	k1gFnPc2	úprava
snížen	snížen	k2eAgInSc1d1	snížen
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
1195	[number]	k4	1195
na	na	k7c4	na
dnešních	dnešní	k2eAgInPc2d1	dnešní
pohodlných	pohodlný	k2eAgInPc2d1	pohodlný
572	[number]	k4	572
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
byla	být	k5eAaImAgFnS	být
pražskou	pražský	k2eAgFnSc7d1	Pražská
firmou	firma	k1gFnSc7	firma
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
Kolben-Daněk	Kolben-Daňka	k1gFnPc2	Kolben-Daňka
provedena	provést	k5eAaPmNgFnS	provést
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
těžké	těžký	k2eAgInPc1d1	těžký
jevištní	jevištní	k2eAgInPc1d1	jevištní
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
dělená	dělený	k2eAgFnSc1d1	dělená
podlaha	podlaha	k1gFnSc1	podlaha
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
a	a	k8xC	a
otáčivé	otáčivý	k2eAgNnSc1d1	otáčivé
jeviště	jeviště	k1gNnSc1	jeviště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
tehdy	tehdy	k6eAd1	tehdy
stalo	stát	k5eAaPmAgNnS	stát
nejmoderněji	moderně	k6eAd3	moderně
technicky	technicky	k6eAd1	technicky
vybaveným	vybavený	k2eAgNnSc7d1	vybavené
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
otevření	otevření	k1gNnSc2	otevření
divadla	divadlo	k1gNnSc2	divadlo
město	město	k1gNnSc1	město
čítající	čítající	k2eAgFnSc2d1	čítající
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
postavilo	postavit	k5eAaPmAgNnS	postavit
první	první	k4xOgNnSc1	první
divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
vybavené	vybavený	k2eAgNnSc1d1	vybavené
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
žárovkovým	žárovkový	k2eAgNnSc7d1	žárovkové
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
přitom	přitom	k6eAd1	přitom
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
plošně	plošně	k6eAd1	plošně
zavedena	zaveden	k2eAgFnSc1d1	zavedena
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jen	jen	k9	jen
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
divadla	divadlo	k1gNnSc2	divadlo
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
postavena	postaven	k2eAgFnSc1d1	postavena
malá	malý	k2eAgFnSc1d1	malá
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
projektu	projekt	k1gInSc2	projekt
elektrického	elektrický	k2eAgNnSc2d1	elektrické
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
T.	T.	kA	T.
A.	A.	kA	A.
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Brno	Brno	k1gNnSc4	Brno
až	až	k6eAd1	až
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zhlédl	zhlédnout	k5eAaPmAgMnS	zhlédnout
jím	jíst	k5eAaImIp1nS	jíst
navržené	navržený	k2eAgNnSc4d1	navržené
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
Edisonovy	Edisonův	k2eAgFnSc2d1	Edisonova
původní	původní	k2eAgFnSc2d1	původní
elektroinstalace	elektroinstalace	k1gFnSc2	elektroinstalace
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
expozici	expozice	k1gFnSc6	expozice
ve	v	k7c6	v
foyeru	foyer	k1gInSc6	foyer
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mahenovo	Mahenův	k2eAgNnSc4d1	Mahenovo
divadlo	divadlo	k1gNnSc4	divadlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
Historie	historie	k1gFnSc2	historie
Mahenova	Mahenův	k2eAgNnSc2d1	Mahenovo
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ndbrno	ndbrno	k1gNnSc1	ndbrno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Mahenovo	Mahenův	k2eAgNnSc4d1	Mahenovo
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
TACE	TACE	kA	TACE
</s>
