<s>
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
českého	český	k2eAgMnSc4d1
polyglota	polyglot	k1gMnSc4
Františka	František	k1gMnSc2
M.	M.	kA
Pelcla	Pelcl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Prof.	prof.	kA
ak.	ak.	k?
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
<g/>
h.c.	h.c.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1950	#num#	k4
(	(	kIx(
<g/>
70	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Postřelmov	Postřelmov	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.pelcl.cz	www.pelcl.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1950	#num#	k4
Postřelmov	Postřelmov	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
rektor	rektor	k1gMnSc1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
VŠUP	VŠUP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1950	#num#	k4
v	v	k7c6
obci	obec	k1gFnSc6
Postřelmov	Postřelmov	k1gInSc4
v	v	k7c6
okrese	okres	k1gInSc6
Šumperk	Šumperk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
Střední	střední	k1gMnSc1
uměleckoprůmyslovou	uměleckoprůmyslový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
architekturu	architektura	k1gFnSc4
na	na	k7c4
VŠUP	VŠUP	kA
a	a	k8xC
design	design	k1gInSc1
nábytku	nábytek	k1gInSc2
na	na	k7c4
Royal	Royal	k1gInSc4
College	Colleg	k1gFnSc2
of	of	k?
Art	Art	k1gMnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Založil	založit	k5eAaPmAgInS
uměleckou	umělecký	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Atika	Atika	k1gFnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
ateliér	ateliér	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
rektor	rektor	k1gMnSc1
VŠUP	VŠUP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
průmyslovým	průmyslový	k2eAgInSc7d1
designem	design	k1gInSc7
skla	sklo	k1gNnSc2
<g/>
,	,	kIx,
porcelánu	porcelán	k1gInSc2
a	a	k8xC
nábytku	nábytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Realizoval	realizovat	k5eAaBmAgInS
návrhy	návrh	k1gInPc4
interiéru	interiér	k1gInSc2
na	na	k7c6
pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
pro	pro	k7c4
Václava	Václav	k1gMnSc4
Havla	Havel	k1gMnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
interiéry	interiér	k1gInPc1
velvyslanectví	velvyslanectví	k1gNnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
Pretórii	Pretórie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
design	design	k1gInSc4
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Form	Form	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Czech	Czech	k1gInSc1
Grand	grand	k1gMnSc1
Design	design	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Design	design	k1gInSc1
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
Czech	Czech	k1gMnSc1
Grand	grand	k1gMnSc1
Design	design	k1gInSc1
-	-	kIx~
Síň	síň	k1gFnSc1
Slávy	Sláva	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
prof.	prof.	kA
ak.	ak.	k?
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
dr	dr	kA
<g/>
.	.	kIx.
<g/>
h.c.	h.c.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Osobnosti	osobnost	k1gFnPc4
Šumperska	Šumpersk	k1gInSc2
-	-	kIx~
Prof.	prof.	kA
<g/>
ak.	ak.	k?
<g/>
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rej	rej	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-03-19	2012-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
FARNÁ	FARNÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
<g/>
:	:	kIx,
Snobský	snobský	k2eAgInSc1d1
design	design	k1gInSc1
mě	já	k3xPp1nSc4
štve	štvát	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
20000075431	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7875	#num#	k4
8875	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
93030104	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
4140447	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
93030104	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
