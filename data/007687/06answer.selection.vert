<s>
Králova	Králův	k2eAgFnSc1d1	Králova
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Speech	speech	k1gInSc1	speech
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britsko-australsko-americké	britskoustralskomerický	k2eAgNnSc1d1	britsko-australsko-americký
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
natočené	natočený	k2eAgNnSc1d1	natočené
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
skutečných	skutečný	k2eAgFnPc2d1	skutečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Tom	Tom	k1gMnSc1	Tom
Hooper	Hooper	k1gMnSc1	Hooper
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
David	David	k1gMnSc1	David
Seidler	Seidler	k1gMnSc1	Seidler
<g/>
.	.	kIx.	.
</s>
