<s>
Králova	Králův	k2eAgFnSc1d1	Králova
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Speech	speech	k1gInSc1	speech
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britsko-australsko-americké	britskoustralskomerický	k2eAgNnSc1d1	britsko-australsko-americký
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
natočené	natočený	k2eAgNnSc1d1	natočené
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
skutečných	skutečný	k2eAgFnPc2d1	skutečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Tom	Tom	k1gMnSc1	Tom
Hooper	Hooper	k1gMnSc1	Hooper
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
David	David	k1gMnSc1	David
Seidler	Seidler	k1gMnSc1	Seidler
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
mezi	mezi	k7c7	mezi
britským	britský	k2eAgMnSc7d1	britský
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
logopedem	logoped	k1gMnSc7	logoped
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
People	People	k1gFnPc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choic	k1gMnPc4	Choic
Award	Award	k1gInSc1	Award
na	na	k7c6	na
Torontském	torontský	k2eAgInSc6d1	torontský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgFnPc4	čtyři
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
Colin	Colin	k1gMnSc1	Colin
Firth	Firth	k1gMnSc1	Firth
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
Rush	Rush	k1gInSc1	Rush
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Bonham	Bonham	k1gInSc1	Bonham
Carterová	Carterová	k1gFnSc1	Carterová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
princ	princ	k1gMnSc1	princ
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
<g/>
,	,	kIx,	,
trápí	trápit	k5eAaImIp3nS	trápit
s	s	k7c7	s
vážným	vážný	k2eAgInSc7d1	vážný
problémem	problém	k1gInSc7	problém
–	–	k?	–
koktavostí	koktavost	k1gFnPc2	koktavost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Alžběty	Alžběta	k1gFnSc2	Alžběta
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Lionelem	Lionel	k1gMnSc7	Lionel
Loguem	Logu	k1gMnSc7	Logu
<g/>
,	,	kIx,	,
australským	australský	k2eAgMnSc7d1	australský
logopedem	logoped	k1gMnSc7	logoped
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
neortodoxní	ortodoxní	k2eNgFnPc4d1	neortodoxní
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
princovy	princův	k2eAgFnPc4d1	princova
rezervy	rezerva	k1gFnPc4	rezerva
však	však	k8xC	však
Logueova	Logueův	k2eAgFnSc1d1	Logueův
metoda	metoda	k1gFnSc1	metoda
překvapivě	překvapivě	k6eAd1	překvapivě
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Albert	Albert	k1gMnSc1	Albert
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
krále	král	k1gMnSc4	král
Eduarda	Eduard	k1gMnSc2	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
překonat	překonat	k5eAaPmF	překonat
své	svůj	k3xOyFgFnPc4	svůj
obtíže	obtíž	k1gFnPc4	obtíž
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svět	svět	k1gInSc1	svět
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
logoped	logoped	k1gMnSc1	logoped
nemá	mít	k5eNaImIp3nS	mít
lékařské	lékařský	k2eAgNnSc4d1	lékařské
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ochotnického	ochotnický	k2eAgMnSc4d1	ochotnický
herce	herec	k1gMnSc4	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
koktavosti	koktavost	k1gFnSc6	koktavost
odnaučoval	odnaučovat	k5eAaImAgInS	odnaučovat
traumatizované	traumatizovaný	k2eAgMnPc4d1	traumatizovaný
vojáky	voják	k1gMnPc4	voják
vracející	vracející	k2eAgMnPc4d1	vracející
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tehdy	tehdy	k6eAd1	tehdy
praktikované	praktikovaný	k2eAgFnSc2d1	praktikovaná
léčby	léčba	k1gFnSc2	léčba
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
psychický	psychický	k2eAgInSc1d1	psychický
stav	stav	k1gInSc1	stav
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
pozitivními	pozitivní	k2eAgInPc7d1	pozitivní
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
k	k	k7c3	k
národu	národ	k1gInSc3	národ
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
přenášenou	přenášený	k2eAgFnSc7d1	přenášená
rozhlasem	rozhlas	k1gInSc7	rozhlas
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
dobře	dobře	k6eAd1	dobře
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
svému	svůj	k1gMnSc3	svůj
logopedovi	logoped	k1gMnSc3	logoped
poděkuje	poděkovat	k5eAaPmIp3nS	poděkovat
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
nazve	nazvat	k5eAaPmIp3nS	nazvat
jej	on	k3xPp3gMnSc4	on
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gMnSc1	Colin
Firth	Firth	k1gMnSc1	Firth
–	–	k?	–
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
Rush	Rusha	k1gFnPc2	Rusha
–	–	k?	–
logoped	logoped	k1gMnSc1	logoped
Lionel	Lionel	k1gMnSc1	Lionel
Logue	Logu	k1gFnSc2	Logu
Helena	Helena	k1gFnSc1	Helena
Bonham	Bonham	k1gInSc1	Bonham
Carterová	Carterová	k1gFnSc1	Carterová
–	–	k?	–
královna	královna	k1gFnSc1	královna
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Bowes-Lyon	Bowes-Lyon	k1gMnSc1	Bowes-Lyon
Guy	Guy	k1gMnSc1	Guy
Pearce	Pearce	k1gMnSc1	Pearce
–	–	k?	–
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Gambon	Gambon	k1gMnSc1	Gambon
–	–	k?	–
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
V.	V.	kA	V.
Timothy	Timotha	k1gFnPc1	Timotha
Spall	Spall	k1gMnSc1	Spall
–	–	k?	–
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
Jennifer	Jennifra	k1gFnPc2	Jennifra
Ehleová	Ehleový	k2eAgFnSc1d1	Ehleový
v	v	k7c4	v
Myrtle	Myrtle	k1gFnPc4	Myrtle
Logueová	Logueová	k1gFnSc1	Logueová
Derek	Derek	k6eAd1	Derek
Jacobi	Jacob	k1gFnSc2	Jacob
–	–	k?	–
Cosmo	Cosma	k1gFnSc5	Cosma
Gordon	Gordon	k1gMnSc1	Gordon
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
canterburský	canterburský	k2eAgMnSc1d1	canterburský
Anthony	Anthona	k1gFnPc4	Anthona
Andrews	Andrewsa	k1gFnPc2	Andrewsa
–	–	k?	–
Stanley	Stanlea	k1gFnSc2	Stanlea
Baldwin	Baldwina	k1gFnPc2	Baldwina
Eve	Eve	k1gFnPc2	Eve
Bestová	Bestový	k2eAgFnSc1d1	Bestový
–	–	k?	–
Wallis	Wallis	k1gFnSc1	Wallis
Simpson	Simpsona	k1gFnPc2	Simpsona
Freya	Freya	k1gFnSc1	Freya
Wilsonová	Wilsonová	k1gFnSc1	Wilsonová
–	–	k?	–
princezna	princezna	k1gFnSc1	princezna
Alžběta	Alžběta	k1gFnSc1	Alžběta
Ramona	Ramona	k1gFnSc1	Ramona
Marquezová	Marquezová	k1gFnSc1	Marquezová
–	–	k?	–
princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
Claire	Clair	k1gInSc5	Clair
Bloomová	Bloomový	k2eAgFnSc1d1	Bloomová
–	–	k?	–
královna	královna	k1gFnSc1	královna
Mary	Mary	k1gFnSc1	Mary
Oscar	Oscar	k1gInSc4	Oscar
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
:	:	kIx,	:
Colin	Colin	k2eAgInSc1d1	Colin
Firth	Firth	k1gInSc1	Firth
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
režie	režie	k1gFnSc2	režie
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Hooper	Hooper	k1gInSc4	Hooper
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Seidler	Seidler	k1gMnSc1	Seidler
</s>
