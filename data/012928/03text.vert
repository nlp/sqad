<p>
<s>
Toluen	toluen	k1gInSc1	toluen
(	(	kIx(	(
<g/>
chemicky	chemicky	k6eAd1	chemicky
methylbenzen	methylbenzen	k2eAgInSc1d1	methylbenzen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čirá	čirý	k2eAgFnSc1d1	čirá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
těkavá	těkavý	k2eAgFnSc1d1	těkavá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
páry	pár	k1gInPc4	pár
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
třaskavou	třaskavý	k2eAgFnSc4d1	třaskavá
směs	směs	k1gFnSc4	směs
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
aromatické	aromatický	k2eAgInPc4d1	aromatický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
zdraví	zdravit	k5eAaImIp3nS	zdravit
škodlivý	škodlivý	k2eAgInSc1d1	škodlivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorec	vzorec	k1gInSc1	vzorec
toluenu	toluen	k1gInSc2	toluen
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
možností	možnost	k1gFnPc2	možnost
zápisu	zápis	k1gInSc2	zápis
vzorce	vzorec	k1gInSc2	vzorec
toluenu	toluen	k1gInSc2	toluen
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
strukturní	strukturní	k2eAgFnSc1d1	strukturní
podoba	podoba	k1gFnSc1	podoba
vzorce	vzorec	k1gInSc2	vzorec
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
vpravo	vpravo	k6eAd1	vpravo
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možnost	možnost	k1gFnSc1	možnost
zápisu	zápis	k1gInSc2	zápis
vzorce	vzorec	k1gInSc2	vzorec
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
dvojných	dvojný	k2eAgFnPc2d1	dvojná
vazeb	vazba	k1gFnPc2	vazba
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
šestiúhelníku	šestiúhelník	k1gInSc2	šestiúhelník
dělá	dělat	k5eAaImIp3nS	dělat
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nezkráceném	zkrácený	k2eNgInSc6d1	nezkrácený
strukturním	strukturní	k2eAgInSc6d1	strukturní
vzorci	vzorec	k1gInSc6	vzorec
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
všechny	všechen	k3xTgInPc1	všechen
atomy	atom	k1gInPc1	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
i	i	k8xC	i
dvojné	dvojný	k2eAgFnSc2d1	dvojná
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
strukturních	strukturní	k2eAgInPc2d1	strukturní
vzorců	vzorec	k1gInPc2	vzorec
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
molekulový	molekulový	k2eAgMnSc1d1	molekulový
(	(	kIx(	(
<g/>
sumární	sumární	k2eAgMnSc1d1	sumární
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
7	[number]	k4	7
<g/>
H	H	kA	H
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzorec	vzorec	k1gInSc1	vzorec
racionální	racionální	k2eAgFnSc2d1	racionální
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Radikály	radikál	k1gInPc4	radikál
odvozené	odvozený	k2eAgInPc4d1	odvozený
od	od	k7c2	od
toluenu	toluen	k1gInSc2	toluen
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
toluenu	toluen	k1gInSc2	toluen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
radikály	radikál	k1gInPc4	radikál
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
odtrženým	odtržený	k2eAgInSc7d1	odtržený
vodíkem	vodík	k1gInSc7	vodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
odtržen	odtržen	k2eAgInSc4d1	odtržen
vodík	vodík	k1gInSc4	vodík
z	z	k7c2	z
methylové	methylový	k2eAgFnSc2d1	methylová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
benzyl	benzyl	k1gInSc1	benzyl
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
odtržen	odtrhnout	k5eAaPmNgInS	odtrhnout
z	z	k7c2	z
benzenového	benzenový	k2eAgNnSc2d1	benzenové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tolyl	tolyl	k1gInSc4	tolyl
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
vazby	vazba	k1gFnSc2	vazba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tol-	tol-	k?	tol-
<g/>
2	[number]	k4	2
<g/>
-yl	l	k?	-yl
<g/>
,	,	kIx,	,
tol-	tol-	k?	tol-
<g/>
3	[number]	k4	3
<g/>
-yl	l	k?	-yl
a	a	k8xC	a
tol-	tol-	k?	tol-
<g/>
4	[number]	k4	4
<g/>
-yl	l	k?	-yl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
toluenu	toluen	k1gInSc2	toluen
==	==	k?	==
</s>
</p>
<p>
<s>
Toluen	toluen	k1gInSc1	toluen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
petroleji	petrolej	k1gInSc6	petrolej
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc4	on
laboratorně	laboratorně	k6eAd1	laboratorně
připravit	připravit	k5eAaPmF	připravit
reakcí	reakce	k1gFnSc7	reakce
benzenu	benzen	k1gInSc2	benzen
s	s	k7c7	s
chlormethanem	chlormethan	k1gInSc7	chlormethan
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
se	se	k3xPyFc4	se
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
používá	používat	k5eAaImIp3nS	používat
chlorid	chlorid	k1gInSc4	chlorid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
(	(	kIx(	(
<g/>
AlCl	AlCl	k1gInSc4	AlCl
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc1	Cl
+	+	kIx~	+
C6H6	C6H6	k1gFnPc1	C6H6
→	→	k?	→
C6H5CH3	C6H5CH3	k1gMnSc1	C6H5CH3
+	+	kIx~	+
HCl	HCl	k1gMnSc1	HCl
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
toluenu	toluen	k1gInSc2	toluen
==	==	k?	==
</s>
</p>
<p>
<s>
Toluen	toluen	k1gInSc1	toluen
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
výhodným	výhodný	k2eAgFnPc3d1	výhodná
teplotám	teplota	k1gFnPc3	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
do	do	k7c2	do
teploměrů	teploměr	k1gInPc2	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
laků	lak	k1gInPc2	lak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
jiných	jiný	k2eAgFnPc2d1	jiná
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
trhaviny	trhavina	k1gFnSc2	trhavina
TNT	TNT	kA	TNT
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
trinitrotoluen	trinitrotoluen	k1gInSc1	trinitrotoluen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
také	také	k6eAd1	také
připravit	připravit	k5eAaPmF	připravit
benzen	benzen	k1gInSc4	benzen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
C6H5CH3	C6H5CH3	k4	C6H5CH3
+	+	kIx~	+
H2	H2	k1gMnSc1	H2
→	→	k?	→
C6H6	C6H6	k1gMnSc1	C6H6
+	+	kIx~	+
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
Toluen	toluen	k1gInSc1	toluen
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
také	také	k9	také
ve	v	k7c6	v
směsích	směs	k1gFnPc6	směs
s	s	k7c7	s
benzenem	benzen	k1gInSc7	benzen
a	a	k8xC	a
xylenem	xylen	k1gInSc7	xylen
jako	jako	k8xS	jako
příměs	příměs	k1gFnSc1	příměs
pro	pro	k7c4	pro
zvyšování	zvyšování	k1gNnSc4	zvyšování
oktanového	oktanový	k2eAgNnSc2d1	oktanové
čísla	číslo	k1gNnSc2	číslo
automobilových	automobilový	k2eAgInPc2d1	automobilový
benzinů	benzin	k1gInPc2	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
i	i	k9	i
součástí	součást	k1gFnSc7	součást
řady	řada	k1gFnSc2	řada
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
,	,	kIx,	,
leštidel	leštidlo	k1gNnPc2	leštidlo
<g/>
,	,	kIx,	,
lepidel	lepidlo	k1gNnPc2	lepidlo
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Toluen	toluen	k1gInSc1	toluen
dráždí	dráždit	k5eAaImIp3nS	dráždit
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
dýchací	dýchací	k2eAgFnPc4d1	dýchací
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tlumivý	tlumivý	k2eAgInSc4d1	tlumivý
účinek	účinek	k1gInSc4	účinek
na	na	k7c6	na
CNS	CNS	kA	CNS
a	a	k8xC	a
kardiovaskulární	kardiovaskulární	k2eAgInSc4d1	kardiovaskulární
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
metabolizuje	metabolizovat	k5eAaImIp3nS	metabolizovat
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
hippurovou	hippurový	k2eAgFnSc4d1	hippurová
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k9	také
na	na	k7c4	na
benzoylglukuronid	benzoylglukuronid	k1gInSc4	benzoylglukuronid
a	a	k8xC	a
na	na	k7c4	na
kresoly	kresol	k1gInPc4	kresol
konjugované	konjugovaný	k2eAgInPc4d1	konjugovaný
se	s	k7c7	s
sulfátem	sulfát	k1gInSc7	sulfát
a	a	k8xC	a
glukuronidem	glukuronid	k1gInSc7	glukuronid
<g/>
)	)	kIx)	)
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgFnSc1d1	akutní
toxicita	toxicita	k1gFnSc1	toxicita
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
orální	orální	k2eAgFnSc1d1	orální
LD50	LD50	k1gFnSc1	LD50
u	u	k7c2	u
potkanů	potkan	k1gMnPc2	potkan
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
600	[number]	k4	600
-	-	kIx~	-
7	[number]	k4	7
000	[number]	k4	000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
inhalační	inhalační	k2eAgFnSc1d1	inhalační
LC50	LC50	k1gFnSc1	LC50
8	[number]	k4	8
800	[number]	k4	800
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chronické	chronický	k2eAgFnSc6d1	chronická
expozici	expozice	k1gFnSc6	expozice
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
neurotoxické	urotoxický	k2eNgInPc1d1	neurotoxický
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
encefalopatie	encefalopatie	k1gFnPc1	encefalopatie
<g/>
,	,	kIx,	,
trvalé	trvalý	k2eAgInPc1d1	trvalý
poruchy	poruch	k1gInPc1	poruch
zraku	zrak	k1gInSc2	zrak
až	až	k9	až
slepota	slepota	k1gFnSc1	slepota
<g/>
,	,	kIx,	,
a	a	k8xC	a
porušení	porušení	k1gNnSc1	porušení
rovnováhy	rovnováha	k1gFnSc2	rovnováha
(	(	kIx(	(
<g/>
degenerace	degenerace	k1gFnSc1	degenerace
mozečku	mozeček	k1gInSc2	mozeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karcinogenita	karcinogenita	k1gFnSc1	karcinogenita
<g/>
,	,	kIx,	,
mutagenita	mutagenita	k1gFnSc1	mutagenita
<g/>
,	,	kIx,	,
teratogenita	teratogenit	k2eAgFnSc1d1	teratogenit
ani	ani	k8xC	ani
reprodukční	reprodukční	k2eAgFnSc1d1	reprodukční
toxicita	toxicita	k1gFnSc1	toxicita
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Narkotické	narkotický	k2eAgFnPc4d1	narkotická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Toluen	toluen	k1gInSc1	toluen
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
zneužíván	zneužíván	k2eAgInSc1d1	zneužíván
i	i	k9	i
narkomany	narkoman	k1gMnPc4	narkoman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
úmyslně	úmyslně	k6eAd1	úmyslně
vdechují	vdechovat	k5eAaImIp3nP	vdechovat
jeho	jeho	k3xOp3gInPc4	jeho
těkavé	těkavý	k2eAgInPc4d1	těkavý
výpary	výpar	k1gInPc4	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
umocnění	umocnění	k1gNnSc4	umocnění
účinků	účinek	k1gInPc2	účinek
jej	on	k3xPp3gMnSc4	on
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
v	v	k7c6	v
nevětraných	větraný	k2eNgFnPc6d1	nevětraná
místnostech	místnost	k1gFnPc6	místnost
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dekou	deka	k1gFnSc7	deka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
plastovém	plastový	k2eAgInSc6d1	plastový
sáčku	sáček	k1gInSc6	sáček
přetaženém	přetažený	k2eAgInSc6d1	přetažený
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
spojeny	spojen	k2eAgInPc1d1	spojen
mnohé	mnohý	k2eAgInPc1d1	mnohý
případy	případ	k1gInPc1	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
inhalací	inhalace	k1gFnSc7	inhalace
toluenu	toluen	k1gInSc2	toluen
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
dávkování	dávkování	k1gNnSc4	dávkování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
počátek	počátek	k1gInSc4	počátek
intoxikace	intoxikace	k1gFnSc2	intoxikace
jsou	být	k5eAaImIp3nP	být
příznačné	příznačný	k2eAgFnPc1d1	příznačná
úporné	úporný	k2eAgFnPc1d1	úporná
tupé	tupý	k2eAgFnPc1d1	tupá
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
podráždění	podráždění	k1gNnSc1	podráždění
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
až	až	k8xS	až
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
stav	stav	k1gInSc1	stav
podobný	podobný	k2eAgInSc1d1	podobný
opilosti	opilost	k1gFnSc6	opilost
provázený	provázený	k2eAgInSc4d1	provázený
pocity	pocit	k1gInPc4	pocit
euforie	euforie	k1gFnSc1	euforie
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
tělesné	tělesný	k2eAgFnPc1d1	tělesná
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
záchvěvy	záchvěv	k1gInPc1	záchvěv
horka	horko	k1gNnSc2	horko
a	a	k8xC	a
mravenčením	mravenčení	k1gNnSc7	mravenčení
<g/>
,	,	kIx,	,
postižený	postižený	k2eAgMnSc1d1	postižený
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
schopnost	schopnost	k1gFnSc4	schopnost
rozumně	rozumně	k6eAd1	rozumně
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
labilní	labilní	k2eAgMnSc1d1	labilní
<g/>
,	,	kIx,	,
obhroublý	obhroublý	k2eAgMnSc1d1	obhroublý
<g/>
;	;	kIx,	;
chůze	chůze	k1gFnSc1	chůze
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
vrávoravá	vrávoravý	k2eAgFnSc1d1	vrávoravá
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
přechodné	přechodný	k2eAgInPc1d1	přechodný
poruchy	poruch	k1gInPc1	poruch
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
stavy	stav	k1gInPc1	stav
zmatenosti	zmatenost	k1gFnSc2	zmatenost
<g/>
,	,	kIx,	,
dezorientace	dezorientace	k1gFnSc2	dezorientace
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnSc2	porucha
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
narušené	narušený	k2eAgNnSc1d1	narušené
vnímání	vnímání	k1gNnSc1	vnímání
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
sluchové	sluchový	k2eAgFnPc1d1	sluchová
a	a	k8xC	a
zrakové	zrakový	k2eAgFnPc1d1	zraková
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
citlivých	citlivý	k2eAgFnPc2d1	citlivá
osob	osoba	k1gFnPc2	osoba
až	až	k9	až
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
množství	množství	k1gNnSc1	množství
uživatelů	uživatel	k1gMnPc2	uživatel
nicméně	nicméně	k8xC	nicméně
umírá	umírat	k5eAaImIp3nS	umírat
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
užívání	užívání	k1gNnSc2	užívání
<g/>
,	,	kIx,	,
příčinou	příčina	k1gFnSc7	příčina
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
zadušení	zadušení	k1gNnPc2	zadušení
zvratky	zvratek	k1gInPc7	zvratek
<g/>
,	,	kIx,	,
zranění	zraněný	k2eAgMnPc1d1	zraněný
z	z	k7c2	z
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zástava	zástava	k1gFnSc1	zástava
dýchání	dýchání	k1gNnSc1	dýchání
či	či	k8xC	či
selhání	selhání	k1gNnSc1	selhání
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neúmyslného	úmyslný	k2eNgNnSc2d1	neúmyslné
předávkování	předávkování	k1gNnSc2	předávkování
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
užívání	užívání	k1gNnSc1	užívání
toluenu	toluen	k1gInSc2	toluen
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
závislosti	závislost	k1gFnSc2	závislost
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
časté	častý	k2eAgNnSc1d1	časté
vdechování	vdechování	k1gNnSc1	vdechování
navozuje	navozovat	k5eAaImIp3nS	navozovat
degenerativní	degenerativní	k2eAgFnPc4d1	degenerativní
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
srdci	srdce	k1gNnSc6	srdce
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc7d1	kostní
dření	dřeň	k1gFnSc7	dřeň
<g/>
,	,	kIx,	,
dýchacích	dýchací	k2eAgFnPc6d1	dýchací
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Toluen	toluen	k1gInSc1	toluen
je	být	k5eAaImIp3nS	být
jakožto	jakožto	k8xS	jakožto
velmi	velmi	k6eAd1	velmi
levná	levný	k2eAgFnSc1d1	levná
droga	droga	k1gFnSc1	droga
značně	značně	k6eAd1	značně
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
již	již	k6eAd1	již
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
není	být	k5eNaImIp3nS	být
volně	volně	k6eAd1	volně
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
živnostenský	živnostenský	k2eAgInSc4d1	živnostenský
list	list	k1gInSc4	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
negativní	negativní	k2eAgInPc1d1	negativní
efekty	efekt	k1gInPc1	efekt
===	===	k?	===
</s>
</p>
<p>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
užívání	užívání	k1gNnSc1	užívání
toluenu	toluen	k1gInSc2	toluen
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
toxickou	toxický	k2eAgFnSc7d1	toxická
encefalopatií	encefalopatie	k1gFnSc7	encefalopatie
vyznačující	vyznačující	k2eAgFnSc7d1	vyznačující
se	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
oslabením	oslabení	k1gNnSc7	oslabení
intelektu	intelekt	k1gInSc2	intelekt
<g/>
,	,	kIx,	,
úpadkem	úpadek	k1gInSc7	úpadek
osobnosti	osobnost	k1gFnSc2	osobnost
až	až	k8xS	až
demencí	demence	k1gFnPc2	demence
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
projevů	projev	k1gInPc2	projev
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
poruchy	porucha	k1gFnPc1	porucha
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
poškozené	poškozený	k2eAgFnSc2d1	poškozená
funkce	funkce	k1gFnSc2	funkce
mozečku	mozeček	k1gInSc2	mozeček
a	a	k8xC	a
pyramidových	pyramidový	k2eAgInPc2d1	pyramidový
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
přítomny	přítomen	k2eAgInPc1d1	přítomen
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
léze	léze	k1gFnPc1	léze
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
toluenem	toluen	k1gInSc7	toluen
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
systematicky	systematicky	k6eAd1	systematicky
monitorováno	monitorován	k2eAgNnSc1d1	monitorováno
Státním	státní	k2eAgInSc7d1	státní
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
ústavem	ústav	k1gInSc7	ústav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgFnP	být
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
roční	roční	k2eAgFnPc1d1	roční
koncentrace	koncentrace	k1gFnPc1	koncentrace
toluenu	toluen	k1gInSc2	toluen
naměřeny	naměřit	k5eAaBmNgFnP	naměřit
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
13,3	[number]	k4	13,3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
<g/>
m-	m-	k?	m-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
v	v	k7c6	v
Legerově	Legerův	k2eAgFnSc6d1	Legerova
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
10,87	[number]	k4	10,87
μ	μ	k?	μ
<g/>
.	.	kIx.	.
<g/>
m-	m-	k?	m-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostatních	ostatní	k2eAgNnPc6d1	ostatní
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
znečištění	znečištění	k1gNnSc1	znečištění
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
mezi	mezi	k7c7	mezi
0,66	[number]	k4	0,66
až	až	k9	až
7,37	[number]	k4	7,37
μ	μ	k?	μ
<g/>
.	.	kIx.	.
<g/>
m-	m-	k?	m-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Benzen	benzen	k1gInSc1	benzen
</s>
</p>
<p>
<s>
Trinitrotoluen	trinitrotoluen	k1gInSc1	trinitrotoluen
</s>
</p>
<p>
<s>
Xylen	xylen	k1gInSc1	xylen
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
toluen	toluen	k1gInSc1	toluen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Toluen	toluen	k1gInSc1	toluen
charakteristika	charakteristikon	k1gNnSc2	charakteristikon
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
IRZ	IRZ	kA	IRZ
</s>
</p>
