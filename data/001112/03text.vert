<s>
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1924	[number]	k4	1924
Náchod	Náchod	k1gInSc1	Náchod
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
česko-kanadský	českoanadský	k2eAgInSc1d1	česko-kanadský
spisovatel-prozaik	spisovatelrozaik	k1gInSc1	spisovatel-prozaik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
exilový	exilový	k2eAgMnSc1d1	exilový
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Zdenou	Zdena	k1gFnSc7	Zdena
Salivarovou	Salivarová	k1gFnSc7	Salivarová
zakladatel	zakladatel	k1gMnSc1	zakladatel
exilového	exilový	k2eAgNnSc2d1	exilové
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
českým	český	k2eAgMnPc3d1	český
poválečným	poválečný	k2eAgMnPc3d1	poválečný
prozaikům	prozaik	k1gMnPc3	prozaik
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dětství	dětství	k1gNnSc6	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
bankovní	bankovní	k2eAgMnSc1d1	bankovní
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
náčelník	náčelník	k1gMnSc1	náčelník
místní	místní	k2eAgFnSc2d1	místní
sokolské	sokolský	k2eAgFnSc2d1	Sokolská
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
jako	jako	k9	jako
správce	správce	k1gMnSc2	správce
sokolského	sokolský	k2eAgNnSc2d1	sokolské
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
1943	[number]	k4	1943
pracoval	pracovat	k5eAaImAgMnS	pracovat
"	"	kIx"	"
<g/>
totálně	totálně	k6eAd1	totálně
nasazen	nasadit	k5eAaPmNgInS	nasadit
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
náchodské	náchodský	k2eAgFnSc6d1	Náchodská
továrně	továrna	k1gFnSc6	továrna
Metallbauwerke	Metallbauwerke	k1gFnPc2	Metallbauwerke
Zimmermann	Zimmermann	k1gMnSc1	Zimmermann
und	und	k?	und
Schling	Schling	k1gInSc1	Schling
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
přádelně	přádelna	k1gFnSc6	přádelna
firmy	firma	k1gFnSc2	firma
Bartoň	Bartoň	k1gMnSc1	Bartoň
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
mohl	moct	k5eAaImAgMnS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
zase	zase	k9	zase
otevřeny	otevřen	k2eAgFnPc1d1	otevřena
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
krátce	krátce	k6eAd1	krátce
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
1946	[number]	k4	1946
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
-	-	kIx~	-
obor	obor	k1gInSc1	obor
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
umístěnku	umístěnka	k1gFnSc4	umístěnka
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
a	a	k8xC	a
Polici	police	k1gFnSc6	police
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc3d2	vyšší
sociální	sociální	k2eAgFnSc3d1	sociální
škole	škola	k1gFnSc3	škola
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
prací	prací	k2eAgMnSc1d1	prací
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
)	)	kIx)	)
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
1953	[number]	k4	1953
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
angloamerické	angloamerický	k2eAgFnSc2d1	angloamerická
redakce	redakce	k1gFnSc2	redakce
Státního	státní	k2eAgNnSc2d1	státní
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInSc1d2	pozdější
Odeon	odeon	k1gInSc1	odeon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1960	[number]	k4	1953-1960
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Březince	březinka	k1gFnSc6	březinka
1159	[number]	k4	1159
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
dvouměsíčníku	dvouměsíčník	k1gInSc2	dvouměsíčník
Světová	světový	k2eAgFnSc1d1	světová
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957-1958	[number]	k4	1957-1958
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc4	zástupce
šéfredaktora	šéfredaktor	k1gMnSc4	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
politickém	politický	k2eAgInSc6d1	politický
skandálu	skandál	k1gInSc6	skandál
následujícím	následující	k2eAgInSc6d1	následující
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gInSc2	jeho
prvního	první	k4xOgInSc2	první
románu	román	k1gInSc2	román
Zbabělci	zbabělec	k1gMnPc7	zbabělec
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
musel	muset	k5eAaImAgMnS	muset
redakci	redakce	k1gFnSc4	redakce
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
angloamerického	angloamerický	k2eAgNnSc2d1	angloamerické
oddělení	oddělení	k1gNnSc2	oddělení
SNKLU	SNKLU	kA	SNKLU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Zdenou	Zdena	k1gFnSc7	Zdena
Salivarovou	Salivarová	k1gFnSc7	Salivarová
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelem	spisovatel	k1gMnSc7	spisovatel
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
(	(	kIx(	(
<g/>
československá	československý	k2eAgFnSc1d1	Československá
specialita	specialita	k1gFnSc1	specialita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
satelitů	satelit	k1gInPc2	satelit
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
měsíčníku	měsíčník	k1gInSc2	měsíčník
Plamen	plamen	k1gInSc1	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
nejprve	nejprve	k6eAd1	nejprve
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
Cornellově	Cornellův	k2eAgFnSc6d1	Cornellova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Ithace	Ithaka	k1gFnSc6	Ithaka
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
stipendijní	stipendijní	k2eAgInSc1d1	stipendijní
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
příprav	příprava	k1gFnPc2	příprava
vydání	vydání	k1gNnSc2	vydání
knihy	kniha	k1gFnSc2	kniha
Tankový	tankový	k2eAgInSc4d1	tankový
prapor	prapor	k1gInSc4	prapor
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Zdenou	Zdena	k1gFnSc7	Zdena
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
profesura	profesura	k1gFnSc1	profesura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
na	na	k7c6	na
tamější	tamější	k2eAgFnSc6d1	tamější
univerzitě	univerzita	k1gFnSc6	univerzita
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
-	-	kIx~	-
vedl	vést	k5eAaImAgMnS	vést
kurz	kurz	k1gInSc4	kurz
současného	současný	k2eAgNnSc2d1	současné
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
kurzy	kurz	k1gInPc1	kurz
anglické	anglický	k2eAgFnSc2d1	anglická
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
kurzy	kurz	k1gInPc1	kurz
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
na	na	k7c6	na
Graduate	Graduat	k1gInSc5	Graduat
Centre	centr	k1gInSc5	centr
for	forum	k1gNnPc2	forum
Study	stud	k1gInPc1	stud
of	of	k?	of
Drama	dramo	k1gNnPc1	dramo
Torontské	torontský	k2eAgFnSc2d1	Torontská
univerzity	univerzita	k1gFnSc2	univerzita
také	také	k9	také
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
dějiny	dějiny	k1gFnPc4	dějiny
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
pohostinsky	pohostinsky	k6eAd1	pohostinsky
přednášel	přednášet	k5eAaImAgInS	přednášet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
severoamerických	severoamerický	k2eAgFnPc6d1	severoamerická
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Zdenou	Zdena	k1gFnSc7	Zdena
založil	založit	k5eAaPmAgMnS	založit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgNnPc4d1	přední
československá	československý	k2eAgNnPc4d1	Československé
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
exilová	exilový	k2eAgFnSc1d1	exilová
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vydával	vydávat	k5eAaPmAgInS	vydávat
české	český	k2eAgMnPc4d1	český
exilové	exilový	k2eAgMnPc4d1	exilový
autory	autor	k1gMnPc4	autor
a	a	k8xC	a
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zakázaná	zakázaný	k2eAgNnPc1d1	zakázané
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
celkem	celek	k1gInSc7	celek
227	[number]	k4	227
titulů	titul	k1gInPc2	titul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
zbaven	zbavit	k5eAaPmNgMnS	zbavit
československého	československý	k2eAgNnSc2d1	Československé
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
značného	značný	k2eAgInSc2d1	značný
ohlasu	ohlas	k1gInSc2	ohlas
zejména	zejména	k9	zejména
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
Oklahomské	Oklahomský	k2eAgFnSc6d1	Oklahomská
univerzitě	univerzita	k1gFnSc6	univerzita
udělena	udělen	k2eAgFnSc1d1	udělena
Neustadtská	Neustadtský	k2eAgFnSc1d1	Neustadtský
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
Zdenou	Zdena	k1gFnSc7	Zdena
Salivarovou	Salivarův	k2eAgFnSc7d1	Salivarův
poctěn	poctít	k5eAaPmNgInS	poctít
Řádem	řád	k1gInSc7	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Škvorečtí	Škvorecký	k2eAgMnPc1d1	Škvorecký
žili	žít	k5eAaImAgMnP	žít
společně	společně	k6eAd1	společně
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
mohli	moct	k5eAaImAgMnP	moct
začít	začít	k5eAaPmF	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
svou	svůj	k3xOyFgFnSc4	svůj
rodnou	rodný	k2eAgFnSc4d1	rodná
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
mj.	mj.	kA	mj.
i	i	k8xC	i
trávili	trávit	k5eAaImAgMnP	trávit
zimy	zima	k1gFnSc2	zima
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
-	-	kIx~	-
Společnost	společnost	k1gFnSc1	společnost
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
-	-	kIx~	-
založena	založit	k5eAaPmNgFnS	založit
(	(	kIx(	(
<g/>
Václavem	Václav	k1gMnSc7	Václav
Krištofem	Krištof	k1gMnSc7	Krištof
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
-	-	kIx~	-
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
gymnasium	gymnasium	k1gNnSc1	gymnasium
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
1998	[number]	k4	1998
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
2000	[number]	k4	2000
-	-	kIx~	-
Literární	literární	k2eAgFnSc2d1	literární
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
)	)	kIx)	)
škola	škola	k1gFnSc1	škola
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
uděluje	udělovat	k5eAaImIp3nS	udělovat
<g/>
:	:	kIx,	:
Cenu	cena	k1gFnSc4	cena
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgNnSc2d1	Škvorecké
její	její	k3xOp3gFnSc7	její
studentská	studentský	k2eAgFnSc1d1	studentská
část	část	k1gFnSc1	část
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Dannyho	Danny	k1gMnSc2	Danny
Smiřického	smiřický	k2eAgMnSc2d1	smiřický
Do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
poezií	poezie	k1gFnSc7	poezie
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
civilismu	civilismus	k1gInSc2	civilismus
a	a	k8xC	a
povídkami	povídka	k1gFnPc7	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Ohlas	ohlas	k1gInSc4	ohlas
však	však	k9	však
vzbudili	vzbudit	k5eAaPmAgMnP	vzbudit
až	až	k9	až
Zbabělci	zbabělec	k1gMnSc3	zbabělec
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
pentalogie	pentalogie	k1gFnSc2	pentalogie
s	s	k7c7	s
autorovým	autorův	k2eAgNnSc7d1	autorovo
alter-egem	altergo	k1gNnSc7	alter-ego
Dannym	Dannymum	k1gNnPc2	Dannymum
Smiřickým	smiřický	k2eAgMnSc7d1	smiřický
coby	coby	k?	coby
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
literární	literární	k2eAgInSc4d1	literární
počin	počin	k1gInSc4	počin
-	-	kIx~	-
podává	podávat	k5eAaImIp3nS	podávat
ucelené	ucelený	k2eAgNnSc4d1	ucelené
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c4	po
autorovy	autorův	k2eAgFnPc4d1	autorova
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
(	(	kIx(	(
<g/>
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Tankový	tankový	k2eAgInSc4d1	tankový
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
Prima	prima	k6eAd1	prima
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
,	,	kIx,	,
Mirákl	Mirákl	k1gInSc4	Mirákl
a	a	k8xC	a
Příběh	příběh	k1gInSc4	příběh
inženýra	inženýr	k1gMnSc2	inženýr
lidských	lidský	k2eAgFnPc2d1	lidská
duší	duše	k1gFnPc2	duše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
Škvoreckého	Škvorecký	k2eAgMnSc4d1	Škvorecký
překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
a	a	k8xC	a
doslovy	doslov	k1gInPc4	doslov
opatřoval	opatřovat	k5eAaImAgInS	opatřovat
díla	dílo	k1gNnPc1	dílo
předních	přední	k2eAgMnPc2d1	přední
moderních	moderní	k2eAgMnPc2d1	moderní
amerických	americký	k2eAgMnPc2d1	americký
autorů	autor	k1gMnPc2	autor
-	-	kIx~	-
Chandlera	Chandler	k1gMnSc2	Chandler
<g/>
,	,	kIx,	,
Hemingwaye	Hemingway	k1gMnSc2	Hemingway
<g/>
,	,	kIx,	,
Faulknera	Faulkner	k1gMnSc2	Faulkner
<g/>
,	,	kIx,	,
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc2	Jamese
aj.	aj.	kA	aj.
Jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
články	článek	k1gInPc1	článek
vyšly	vyjít	k5eAaPmAgInP	vyjít
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
v	v	k7c6	v
"	"	kIx"	"
<g/>
kulturním	kulturní	k2eAgInSc6d1	kulturní
oběžníku	oběžník	k1gInSc6	oběžník
<g/>
"	"	kIx"	"
SČM	SČM	kA	SČM
Slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
mladý	mladý	k2eAgInSc4d1	mladý
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
pak	pak	k6eAd1	pak
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
<g />
.	.	kIx.	.
</s>
<s>
dalších	další	k2eAgInPc6d1	další
časopisech	časopis	k1gInPc6	časopis
-	-	kIx~	-
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Krásná	krásný	k2eAgFnSc1d1	krásná
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
Květen	květen	k1gInSc1	květen
(	(	kIx(	(
<g/>
úryvek	úryvek	k1gInSc1	úryvek
ze	z	k7c2	z
Zbabělců	zbabělec	k1gMnPc2	zbabělec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plamen	plamen	k1gInSc1	plamen
(	(	kIx(	(
<g/>
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
Nápady	nápad	k1gInPc4	nápad
čtenáře	čtenář	k1gMnPc4	čtenář
detektivek	detektivka	k1gFnPc2	detektivka
<g/>
,	,	kIx,	,
kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
Tankového	tankový	k2eAgInSc2d1	tankový
praporu	prapor	k1gInSc2	prapor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
květ	květ	k1gInSc1	květ
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
Bassaxofon	Bassaxofon	k1gInSc1	Bassaxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
pohľady	pohľada	k1gFnPc1	pohľada
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Repertoár	repertoár	k1gInSc1	repertoár
malé	malý	k2eAgFnSc2d1	malá
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
Taneční	taneční	k2eAgFnSc1d1	taneční
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
Divadelní	divadelní	k2eAgFnPc1d1	divadelní
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgFnSc1d1	knižní
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Sešity	sešit	k1gInPc4	sešit
pro	pro	k7c4	pro
mladou	mladý	k2eAgFnSc4d1	mladá
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
Trn	trn	k1gInSc1	trn
<g/>
,	,	kIx,	,
Orientace	orientace	k1gFnSc1	orientace
<g/>
,	,	kIx,	,
Literární	literární	k2eAgInPc1d1	literární
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc1	pokračování
Dopisů	dopis	k1gInPc2	dopis
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Dopisů	dopis	k1gInPc2	dopis
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
překlady	překlad	k1gInPc4	překlad
<g/>
,	,	kIx,	,
medailony	medailon	k1gInPc4	medailon
a	a	k8xC	a
recenze	recenze	k1gFnPc4	recenze
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
angloamerické	angloamerický	k2eAgFnSc2d1	angloamerická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
1961	[number]	k4	1961
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2	rozsáhlejší
eseje	esej	k1gFnSc2	esej
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1970	[number]	k4	1970
psal	psát	k5eAaImAgInS	psát
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
eseje	esej	k1gFnSc2	esej
do	do	k7c2	do
exilových	exilový	k2eAgNnPc2d1	exilové
periodik	periodikum	k1gNnPc2	periodikum
<g/>
:	:	kIx,	:
Listy	lista	k1gFnSc2	lista
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
domov	domov	k1gInSc1	domov
(	(	kIx(	(
<g/>
Scarborough	Scarborough	k1gInSc1	Scarborough
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svědectví	svědectví	k1gNnPc1	svědectví
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telegram	telegram	k1gInSc1	telegram
(	(	kIx(	(
<g/>
Edmonton	Edmonton	k1gInSc1	Edmonton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
(	(	kIx(	(
<g/>
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
dvouměsíčníku	dvouměsíčník	k1gInSc2	dvouměsíčník
Západ	západ	k1gInSc1	západ
(	(	kIx(	(
<g/>
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jinojazyčných	jinojazyčný	k2eAgNnPc2d1	jinojazyčný
periodik	periodikum	k1gNnPc2	periodikum
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c4	v
Toronto	Toronto	k1gNnSc4	Toronto
Star	Star	kA	Star
<g/>
,	,	kIx,	,
World	World	k1gMnSc1	World
Literature	Literatur	k1gMnSc5	Literatur
(	(	kIx(	(
<g/>
Oklahoma	Oklahomum	k1gNnSc2	Oklahomum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Canadian	Canadian	k1gInSc1	Canadian
Forum	forum	k1gNnSc1	forum
(	(	kIx(	(
<g/>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Index	index	k1gInSc1	index
of	of	k?	of
Censorship	Censorship	k1gInSc1	Censorship
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Canadian	Canadian	k1gInSc1	Canadian
Fiction	Fiction	k1gInSc1	Fiction
Magazine	Magazin	k1gInSc5	Magazin
(	(	kIx(	(
<g/>
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Canadian	Canadian	k1gMnSc1	Canadian
Literature	Literatur	k1gMnSc5	Literatur
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Book	Book	k1gMnSc1	Book
Review	Review	k1gMnSc1	Review
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Idler	Idler	k1gMnSc1	Idler
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Review	Review	k1gFnSc1	Review
of	of	k?	of
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
Globe	globus	k1gInSc5	globus
and	and	k?	and
Mail	mail	k1gInSc1	mail
(	(	kIx(	(
<g/>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
Literárních	literární	k2eAgFnPc6d1	literární
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
Respektu	respekt	k1gInSc6	respekt
<g/>
,	,	kIx,	,
Divadelních	divadelní	k2eAgFnPc6d1	divadelní
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
Kritickém	kritický	k2eAgInSc6d1	kritický
sborníku	sborník	k1gInSc6	sborník
<g/>
,	,	kIx,	,
Iluminaci	iluminace	k1gFnSc6	iluminace
<g/>
,	,	kIx,	,
Revolver	revolver	k1gInSc1	revolver
Revui	revue	k1gFnSc4	revue
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
odborné	odborný	k2eAgInPc1d1	odborný
texty	text	k1gInPc1	text
o	o	k7c6	o
angloamerické	angloamerický	k2eAgFnSc6d1	angloamerická
literatuře	literatura	k1gFnSc6	literatura
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
strojopisného	strojopisný	k2eAgInSc2d1	strojopisný
sborníku	sborník	k1gInSc2	sborník
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
všude	všude	k6eAd1	všude
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hiršal	Hiršal	k1gMnSc1	Hiršal
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Josef	Josef	k1gMnSc1	Josef
Pepýt	Pepýt	k1gMnSc1	Pepýt
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
samizdatových	samizdatový	k2eAgInPc2d1	samizdatový
sborníků	sborník	k1gInPc2	sborník
Jakémusi	jakýsi	k3yIgMnSc3	jakýsi
Alexandru	Alexandr	k1gMnSc3	Alexandr
K.	K.	kA	K.
(	(	kIx(	(
<g/>
Petlice	petlice	k1gFnSc1	petlice
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hlasy	hlas	k1gInPc1	hlas
nad	nad	k7c7	nad
rukopisem	rukopis	k1gInSc7	rukopis
Českého	český	k2eAgInSc2d1	český
snáře	snář	k1gInSc2	snář
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Nápady	nápad	k1gInPc4	nápad
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
publika	publikum	k1gNnSc2	publikum
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
jeho	jeho	k3xOp3gInPc1	jeho
příspěvky	příspěvek	k1gInPc1	příspěvek
z	z	k7c2	z
Divadelních	divadelní	k2eAgFnPc2d1	divadelní
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
napsal	napsat	k5eAaPmAgInS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Dorůžkou	Dorůžka	k1gFnSc7	Dorůžka
a	a	k8xC	a
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Švábem	Šváb	k1gMnSc7	Šváb
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
Pražský	pražský	k2eAgInSc4d1	pražský
dixieland	dixieland	k1gInSc4	dixieland
zájezdovou	zájezdový	k2eAgFnSc4d1	zájezdová
revui	revue	k1gFnSc4	revue
Opravdu	opravdu	k6eAd1	opravdu
blues	blues	k1gFnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1962	[number]	k4	1962
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
text-appealech	textppeal	k1gInPc6	text-appeal
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Redutě	reduta	k1gFnSc6	reduta
a	a	k8xC	a
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Paravan	Paravan	k1gMnSc1	Paravan
texty	text	k1gInPc1	text
Ze	z	k7c2	z
života	život	k1gInSc2	život
lepší	dobrý	k2eAgFnSc2d2	lepší
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
Ze	z	k7c2	z
života	život	k1gInSc2	život
socialistické	socialistický	k2eAgFnSc2d1	socialistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
inscenace	inscenace	k1gFnSc1	inscenace
Střídání	střídání	k1gNnSc2	střídání
stráží	stráž	k1gFnPc2	stráž
v	v	k7c6	v
K.	K.	kA	K.
divadla	divadlo	k1gNnSc2	divadlo
Večerní	večerní	k2eAgNnSc1d1	večerní
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Evžen	Evžen	k1gMnSc1	Evžen
Sokolovský	sokolovský	k2eAgMnSc1d1	sokolovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc4	báseň
Nezoufejte	zoufat	k5eNaBmRp2nP	zoufat
<g/>
!	!	kIx.	!
</s>
<s>
uvedla	uvést	k5eAaPmAgFnS	uvést
pražská	pražský	k2eAgFnSc1d1	Pražská
Viola	Viola	k1gFnSc1	Viola
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Evalda	Evald	k1gMnSc2	Evald
Schorma	Schorm	k1gMnSc2	Schorm
<g/>
.	.	kIx.	.
</s>
<s>
Prózy	próza	k1gFnPc4	próza
Eine	Eine	k1gFnSc1	Eine
kleine	kleinout	k5eAaPmIp3nS	kleinout
Jazzmusik	Jazzmusik	k1gInSc4	Jazzmusik
<g/>
,	,	kIx,	,
Bebop	bebop	k1gInSc4	bebop
Richarda	Richard	k1gMnSc2	Richard
Kambaly	kambala	k1gFnSc2	kambala
<g/>
,	,	kIx,	,
Dobře	dobře	k6eAd1	dobře
prověřená	prověřený	k2eAgFnSc1d1	prověřená
Lizetka	Lizetka	k1gFnSc1	Lizetka
a	a	k8xC	a
Píseň	píseň	k1gFnSc1	píseň
zapomenutých	zapomenutý	k2eAgNnPc2d1	zapomenuté
let	léto	k1gNnPc2	léto
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
pořad	pořad	k1gInSc1	pořad
Hořkej	Hořkej	k?	Hořkej
jazz	jazz	k1gInSc1	jazz
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
S.	S.	kA	S.
<g/>
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc2	Neumann
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
adaptace	adaptace	k1gFnSc1	adaptace
Mirákl	Mirákl	k1gInSc4	Mirákl
pro	pro	k7c4	pro
studentské	studentský	k2eAgNnSc4d1	studentské
divadlo	divadlo	k1gNnSc4	divadlo
DISK	disk	k1gInSc1	disk
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
dramatizace	dramatizace	k1gFnSc1	dramatizace
J.	J.	kA	J.
Hančil	Hančil	k1gFnPc1	Hančil
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
Katarzyna	Katarzyna	k1gFnSc1	Katarzyna
Wrzosek	Wrzoska	k1gFnPc2	Wrzoska
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Seydler	Seydler	k1gMnSc1	Seydler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965-1968	[number]	k4	1965-1968
moderoval	moderovat	k5eAaBmAgInS	moderovat
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Dorůžkou	Dorůžka	k1gFnSc7	Dorůžka
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
hitparádu	hitparáda	k1gFnSc4	hitparáda
Šest	šest	k4xCc4	šest
na	na	k7c6	na
lenošce	lenoška	k1gFnSc6	lenoška
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
předmluvy	předmluva	k1gFnPc4	předmluva
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
či	či	k8xC	či
doslovy	doslov	k1gInPc4	doslov
ke	k	k7c3	k
knihám	kniha	k1gFnPc3	kniha
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
a	a	k8xC	a
Poezie	poezie	k1gFnSc2	poezie
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
redakci	redakce	k1gFnSc6	redakce
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Hlas	hlas	k1gInSc4	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
recenzoval	recenzovat	k5eAaImAgMnS	recenzovat
novinky	novinka	k1gFnPc4	novinka
beletristické	beletristický	k2eAgFnSc2d1	beletristická
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
anglicky	anglicky	k6eAd1	anglicky
psané	psaný	k2eAgFnSc2d1	psaná
hry	hra	k1gFnSc2	hra
The	The	k1gMnSc1	The
new	new	k?	new
men	men	k?	men
and	and	k?	and
women	womna	k1gFnPc2	womna
(	(	kIx(	(
<g/>
kanadské	kanadský	k2eAgFnSc2d1	kanadská
stanice	stanice	k1gFnSc2	stanice
CBC	CBC	kA	CBC
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
SRN	SRN	kA	SRN
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
próz	próza	k1gFnPc2	próza
či	či	k8xC	či
námětů	námět	k1gInPc2	námět
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
scénářů	scénář	k1gInPc2	scénář
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Farářův	farářův	k2eAgInSc1d1	farářův
konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Evald	Evald	k1gMnSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
(	(	kIx(	(
<g/>
spolupráce	spolupráce	k1gFnSc1	spolupráce
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Flirt	flirt	k1gInSc1	flirt
se	s	k7c7	s
slečnou	slečna	k1gFnSc7	slečna
Stříbrnou	stříbrná	k1gFnSc7	stříbrná
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Mahlerem	Mahler	k1gMnSc7	Mahler
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Gajer	Gajer	k1gMnSc1	Gajer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vít	Vít	k1gMnSc1	Vít
Olmer	Olmer	k1gMnSc1	Olmer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Československou	československý	k2eAgFnSc4d1	Československá
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
napsal	napsat	k5eAaBmAgInS	napsat
středometrážní	středometrážní	k2eAgFnSc4d1	středometrážní
Revui	revue	k1gFnSc4	revue
pro	pro	k7c4	pro
banjo	banjo	k1gNnSc4	banjo
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
Vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
poručíka	poručík	k1gMnSc2	poručík
Borůvky	Borůvka	k1gMnSc2	Borůvka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Pavel	Pavel	k1gMnSc1	Pavel
Blumenfeld	Blumenfeld	k1gMnSc1	Blumenfeld
<g/>
)	)	kIx)	)
a	a	k8xC	a
inscenaci	inscenace	k1gFnSc4	inscenace
Poe	Poe	k1gFnPc2	Poe
a	a	k8xC	a
vražda	vražda	k1gFnSc1	vražda
krásné	krásný	k2eAgFnSc2d1	krásná
dívky	dívka	k1gFnSc2	dívka
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Viktor	Viktor	k1gMnSc1	Viktor
Polesný	polesný	k1gMnSc1	polesný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Josef	Josef	k1gMnSc1	Josef
Benda	Benda	k1gMnSc1	Benda
napsal	napsat	k5eAaBmAgMnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Dorůžkou	Dorůžka	k1gFnSc7	Dorůžka
libreto	libreto	k1gNnSc4	libreto
pro	pro	k7c4	pro
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
k	k	k7c3	k
operetě	opereta	k1gFnSc3	opereta
Zmatek	zmatek	k1gInSc1	zmatek
kolem	kolem	k6eAd1	kolem
Lydie	Lydie	k1gFnSc2	Lydie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
ke	k	k7c3	k
spoluautorství	spoluautorství	k1gNnSc3	spoluautorství
knih	kniha	k1gFnPc2	kniha
vydaných	vydaný	k2eAgFnPc2d1	vydaná
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Jana	Jan	k1gMnSc2	Jan
Zábrany	zábrana	k1gFnSc2	zábrana
<g/>
:	:	kIx,	:
Vražda	vražda	k1gFnSc1	vražda
pro	pro	k7c4	pro
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
Vražda	vražda	k1gFnSc1	vražda
se	s	k7c7	s
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
<g/>
,	,	kIx,	,
Táňa	Táňa	k1gFnSc1	Táňa
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
pistolníci	pistolník	k1gMnPc1	pistolník
a	a	k8xC	a
k	k	k7c3	k
autorství	autorství	k1gNnSc3	autorství
překladu	překlad	k1gInSc2	překlad
knihy	kniha	k1gFnSc2	kniha
Warrena	Warren	k1gMnSc2	Warren
Millera	Miller	k1gMnSc2	Miller
Prezydent	Prezydent	k1gMnSc1	Prezydent
krokadýlů	krokadýl	k1gInPc2	krokadýl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
poslední	poslední	k2eAgNnSc4d1	poslední
prohlášení	prohlášení	k1gNnSc4	prohlášení
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
článcích	článek	k1gInPc6	článek
v	v	k7c4	v
Revolver	revolver	k1gInSc4	revolver
revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
Prezydent	Prezydent	k1gMnSc1	Prezydent
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Krokadýl	Krokadýl	k1gInSc1	Krokadýl
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
Čisté	čistý	k2eAgNnSc1d1	čisté
víno	víno	k1gNnSc1	víno
<g/>
;	;	kIx,	;
články	článek	k1gInPc1	článek
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
polemiku	polemika	k1gFnSc4	polemika
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
dodnes	dodnes	k6eAd1	dodnes
uspokojivě	uspokojivě	k6eAd1	uspokojivě
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Nezoufejte	zoufat	k5eNaBmRp2nP	zoufat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
civilismu	civilismus	k1gInSc2	civilismus
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnPc1d1	Nové
canterburské	canterburský	k2eAgFnPc1d1	canterburský
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Zbabělci	zbabělec	k1gMnPc1	zbabělec
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
-	-	kIx~	-
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
období	období	k1gNnSc6	období
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
autorově	autorův	k2eAgInSc6d1	autorův
rodném	rodný	k2eAgInSc6d1	rodný
Náchodě	Náchod	k1gInSc6	Náchod
očima	oko	k1gNnPc7	oko
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
jazzového	jazzový	k2eAgMnSc4d1	jazzový
hejska	hejsek	k1gMnSc4	hejsek
<g/>
"	"	kIx"	"
Dannyho	Danny	k1gMnSc4	Danny
Smiřického	smiřický	k2eAgMnSc4d1	smiřický
<g/>
,	,	kIx,	,
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
předstírané	předstíraný	k2eAgNnSc1d1	předstírané
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
místní	místní	k2eAgFnSc2d1	místní
honorace	honorace	k1gFnSc2	honorace
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
depatetizaci	depatetizace	k1gFnSc6	depatetizace
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
;	;	kIx,	;
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
nylonového	nylonový	k2eAgInSc2d1	nylonový
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
z	z	k7c2	z
období	období	k1gNnSc2	období
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnSc2	povídka
tenorsaxofonisty	tenorsaxofonista	k1gMnSc2	tenorsaxofonista
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
-	-	kIx~	-
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1971	[number]	k4	1971
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
1990	[number]	k4	1990
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
satirický	satirický	k2eAgInSc4d1	satirický
a	a	k8xC	a
groteskní	groteskní	k2eAgInSc4d1	groteskní
román	román	k1gInSc4	román
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
službě	služba	k1gFnSc6	služba
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
Sedmiramenný	sedmiramenný	k2eAgInSc1d1	sedmiramenný
svícen	svícen	k1gInSc1	svícen
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
<g />
.	.	kIx.	.
</s>
<s>
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
tematikou	tematika	k1gFnSc7	tematika
<g/>
)	)	kIx)	)
Babylónský	babylónský	k2eAgInSc1d1	babylónský
příběh	příběh	k1gInSc1	příběh
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
s	s	k7c7	s
častou	častý	k2eAgFnSc7d1	častá
tematikou	tematika	k1gFnSc7	tematika
jazzu	jazz	k1gInSc2	jazz
<g/>
)	)	kIx)	)
Legenda	legenda	k1gFnSc1	legenda
Emöke	Emöke	k1gFnSc1	Emöke
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
lyrická	lyrický	k2eAgFnSc1d1	lyrická
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
Williama	William	k1gMnSc2	William
Faulknera	Faulkner	k1gMnSc2	Faulkner
<g/>
)	)	kIx)	)
Tvář	tvář	k1gFnSc1	tvář
jazzu	jazz	k1gInSc2	jazz
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
s	s	k7c7	s
muzikologem	muzikolog	k1gMnSc7	muzikolog
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Dorůžkou	Dorůžka	k1gFnSc7	Dorůžka
<g/>
)	)	kIx)	)
Nápady	nápad	k1gInPc4	nápad
čtenáře	čtenář	k1gMnPc4	čtenář
detektivek	detektivka	k1gFnPc2	detektivka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
eseje	esej	k1gFnPc1	esej
o	o	k7c6	o
detektivní	detektivní	k2eAgFnSc6d1	detektivní
literatuře	literatura	k1gFnSc6	literatura
<g/>
)	)	kIx)	)
Ze	z	k7c2	z
života	život	k1gInSc2	život
lepší	dobrý	k2eAgFnSc2d2	lepší
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Jazzové	jazzový	k2eAgFnSc2d1	jazzová
inspirace	inspirace	k1gFnSc2	inspirace
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaBmNgNnS	napsat
s	s	k7c7	s
muzikologem	muzikolog	k1gMnSc7	muzikolog
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Dorůžkou	Dorůžka	k1gFnSc7	Dorůžka
<g/>
)	)	kIx)	)
Detektivní	detektivní	k2eAgFnSc2d1	detektivní
trilogie	trilogie	k1gFnSc2	trilogie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Smutek	smutek	k1gInSc1	smutek
poručíka	poručík	k1gMnSc2	poručík
Borůvky	Borůvka	k1gMnSc2	Borůvka
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
poručíka	poručík	k1gMnSc2	poručík
Borůvky	Borůvka	k1gMnSc2	Borůvka
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
1992	[number]	k4	1992
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Návrat	návrat	k1gInSc1	návrat
poručíka	poručík	k1gMnSc2	poručík
Borůvky	Borůvka	k1gMnSc2	Borůvka
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
1993	[number]	k4	1993
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Bassaxofon	Bassaxofon	k1gInSc1	Bassaxofon
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
O	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
-	-	kIx~	-
o	o	k7c6	o
nás	my	k3xPp1nPc6	my
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Farářův	farářův	k2eAgInSc1d1	farářův
konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
převedený	převedený	k2eAgInSc4d1	převedený
do	do	k7c2	do
beletristické	beletristický	k2eAgFnSc2d1	beletristická
formy	forma	k1gFnSc2	forma
<g/>
)	)	kIx)	)
Lvíče	lvíče	k1gNnSc1	lvíče
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
s	s	k7c7	s
detektivní	detektivní	k2eAgFnSc7d1	detektivní
zápletkou	zápletka	k1gFnSc7	zápletka
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
prostředí	prostředí	k1gNnSc2	prostředí
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Hořkej	Hořkej	k?	Hořkej
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1946-1967	[number]	k4	1946-1967
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
před	před	k7c7	před
normalizací	normalizace	k1gFnSc7	normalizace
<g/>
)	)	kIx)	)
Všichni	všechen	k3xTgMnPc1	všechen
ti	ten	k3xDgMnPc1	ten
bystří	bystrý	k2eAgMnPc1d1	bystrý
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Mirákl	Mirákl	k1gInSc1	Mirákl
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
1991	[number]	k4	1991
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Danny	Danno	k1gNnPc7	Danno
jako	jako	k9	jako
účastník	účastník	k1gMnSc1	účastník
politických	politický	k2eAgFnPc2d1	politická
událostí	událost	k1gFnPc2	událost
50	[number]	k4	50
<g/>
.	.	kIx.	.
i	i	k9	i
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
včetně	včetně	k7c2	včetně
motivu	motiv	k1gInSc2	motiv
deziluze	deziluze	k1gFnSc2	deziluze
z	z	k7c2	z
událostí	událost	k1gFnPc2	událost
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Hříchy	hřích	k1gInPc1	hřích
pro	pro	k7c4	pro
pátera	páter	k1gMnSc4	páter
Knoxe	Knoxe	k1gFnSc1	Knoxe
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Prima	prima	k2eAgFnSc1d1	prima
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
1990	[number]	k4	1990
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Danny	Danno	k1gNnPc7	Danno
jako	jako	k9	jako
septimán	septimán	k1gMnSc1	septimán
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
mládí	mládí	k1gNnSc2	mládí
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
inženýra	inženýr	k1gMnSc2	inženýr
lidských	lidský	k2eAgFnPc2d1	lidská
duší	duše	k1gFnPc2	duše
(	(	kIx(	(
<g/>
Engineer	Engineer	k1gInSc1	Engineer
of	of	k?	of
Human	Human	k1gInSc1	Human
Souls	Soulsa	k1gFnPc2	Soulsa
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1989	[number]	k4	1989
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
<g/>
,	,	kIx,	,
mnohovrstevný	mnohovrstevný	k2eAgInSc4d1	mnohovrstevný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
opět	opět	k6eAd1	opět
příběh	příběh	k1gInSc4	příběh
Dannyho	Danny	k1gMnSc2	Danny
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
jako	jako	k8xS	jako
univerzitního	univerzitní	k2eAgMnSc4d1	univerzitní
profesora	profesor	k1gMnSc4	profesor
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
děj	děj	k1gInSc1	děj
prokládán	prokládán	k2eAgInSc1d1	prokládán
mnoha	mnoho	k4c7	mnoho
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
a	a	k8xC	a
myšlenkovými	myšlenkový	k2eAgFnPc7d1	myšlenková
asociacemi	asociace	k1gFnPc7	asociace
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Mirákl	Mirákl	k1gInSc4	Mirákl
<g/>
)	)	kIx)	)
Samožerbuch	Samožerbuch	k1gMnSc1	Samožerbuch
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
-	-	kIx~	-
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
)	)	kIx)	)
Na	na	k7c6	na
brigádě	brigáda	k1gFnSc6	brigáda
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Brouskem	brousek	k1gInSc7	brousek
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
kritik	kritika	k1gFnPc2	kritika
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Bůh	bůh	k1gMnSc1	bůh
do	do	k7c2	do
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
hříchy	hřích	k1gInPc4	hřích
mládí	mládí	k1gNnSc2	mládí
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
A	a	k9	a
Tall	Tall	k1gInSc1	Tall
Tale	Tal	k1gMnSc2	Tal
of	of	k?	of
America	Americus	k1gMnSc2	Americus
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
)	)	kIx)	)
Dvě	dva	k4xCgFnPc1	dva
legendy	legenda	k1gFnPc1	legenda
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Scherzo	scherzo	k1gNnSc4	scherzo
capriccioso	capriccioso	k6eAd1	capriccioso
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Veselý	veselý	k2eAgInSc1d1	veselý
sen	sen	k1gInSc1	sen
o	o	k7c6	o
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
o	o	k7c6	o
působení	působení	k1gNnSc6	působení
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
Ze	z	k7c2	z
života	život	k1gInSc2	život
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
marginálie	marginálie	k1gFnPc1	marginálie
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Čítanka	čítanka	k1gFnSc1	čítanka
Josefa	Josef	k1gMnSc2	Josef
<g />
.	.	kIx.	.
</s>
<s>
Škvoreckého	Škvorecký	k2eAgInSc2d1	Škvorecký
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Hlas	hlas	k1gInSc1	hlas
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Píseň	píseň	k1gFnSc4	píseň
zapomenutých	zapomenutý	k2eAgNnPc2d1	zapomenuté
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Ožehavé	ožehavý	k2eAgNnSc1d1	ožehavé
téma	téma	k1gNnSc1	téma
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
mladý	mladý	k2eAgInSc1d1	mladý
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Dvě	dva	k4xCgFnPc1	dva
neznámé	známý	k2eNgFnPc1d1	neznámá
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Jaká	jaký	k3yRgFnSc1	jaký
vlastně	vlastně	k9	vlastně
byla	být	k5eAaImAgFnS	být
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Věk	věk	k1gInSc1	věk
nylonu	nylon	k1gInSc2	nylon
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Nevěsta	nevěsta	k1gFnSc1	nevěsta
z	z	k7c2	z
Texasu	Texas	k1gInSc2	Texas
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
složitě	složitě	k6eAd1	složitě
komponovaný	komponovaný	k2eAgInSc4d1	komponovaný
román	román	k1gInSc4	román
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
příběhy	příběh	k1gInPc1	příběh
českých	český	k2eAgMnPc2d1	český
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
)	)	kIx)	)
Vladimíra	Vladimír	k1gMnSc2	Vladimír
v	v	k7c6	v
Territorio	Territoria	k1gMnSc5	Territoria
libre	libr	k1gMnSc5	libr
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
neúspěšného	úspěšný	k2eNgMnSc2d1	neúspěšný
tenorsaxofonisty	tenorsaxofonista	k1gMnSc2	tenorsaxofonista
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
Líze	Líza	k1gFnSc6	Líza
a	a	k8xC	a
mladém	mladý	k1gMnSc6	mladý
Wertherovi	Werther	k1gMnSc6	Werther
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
rajského	rajský	k2eAgNnSc2d1	rajské
údolí	údolí	k1gNnSc2	údolí
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Dvě	dva	k4xCgFnPc1	dva
vraždy	vražda	k1gFnPc1	vražda
v	v	k7c6	v
mém	můj	k3xOp1gInSc6	můj
dvojím	dvojí	k4xRgInSc6	dvojí
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
beletristické	beletristický	k2eAgNnSc4d1	beletristické
zpracování	zpracování	k1gNnSc4	zpracování
obvinění	obvinění	k1gNnSc2	obvinění
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
StB	StB	k1gFnPc7	StB
<g/>
)	)	kIx)	)
Neuilly	Neuill	k1gInPc1	Neuill
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zde	zde	k6eAd1	zde
mnohé	mnohý	k2eAgFnPc1d1	mnohá
knižně	knižně	k6eAd1	knižně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
poprvé	poprvé	k6eAd1	poprvé
<g/>
)	)	kIx)	)
Nevysvětlitelný	vysvětlitelný	k2eNgInSc1d1	nevysvětlitelný
příběh	příběh	k1gInSc1	příběh
aneb	aneb	k?	aneb
Vyprávění	vyprávění	k1gNnSc1	vyprávění
Questa	Questa	k1gFnSc1	Questa
Firma	firma	k1gFnSc1	firma
Sicula	Sicula	k1gFnSc1	Sicula
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Krátké	Krátké	k2eAgNnSc1d1	Krátké
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
vraždou	vražda	k1gFnSc7	vražda
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Podivný	podivný	k2eAgInSc1d1	podivný
<g />
.	.	kIx.	.
</s>
<s>
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Providence	providence	k1gFnSc2	providence
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
na	na	k7c4	na
tuhle	tenhle	k3xDgFnSc4	tenhle
bolest	bolest	k1gFnSc4	bolest
nejsou	být	k5eNaImIp3nP	být
prášky	prášek	k1gInPc4	prášek
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Pulchra	Pulchra	k1gFnSc1	Pulchra
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
krásné	krásný	k2eAgFnSc6d1	krásná
planetě	planeta	k1gFnSc6	planeta
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Obyčejné	obyčejný	k2eAgInPc1d1	obyčejný
životy	život	k1gInPc1	život
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
třídních	třídní	k2eAgInPc6d1	třídní
srazech	sraz	k1gInPc6	sraz
Dannyho	Danny	k1gMnSc2	Danny
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
bývalých	bývalý	k2eAgMnPc2d1	bývalý
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
probíhá	probíhat	k5eAaImIp3nS	probíhat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
beletristické	beletristický	k2eAgNnSc4d1	beletristické
uzavření	uzavření	k1gNnSc4	uzavření
příběhu	příběh	k1gInSc2	příběh
Dannyho	Danny	k1gMnSc2	Danny
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
Kostelce	Kostelec	k1gInSc2	Kostelec
<g/>
)	)	kIx)	)
</s>
