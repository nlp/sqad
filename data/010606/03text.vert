<p>
<s>
Sarafán	sarafán	k1gInSc1	sarafán
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
с	с	k?	с
<g/>
,	,	kIx,	,
sarafan	sarafan	k1gMnSc1	sarafan
<g/>
;	;	kIx,	;
z	z	k7c2	z
perského	perský	k2eAgInSc2d1	perský
sе	sе	k?	sе
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
ruský	ruský	k2eAgInSc1d1	ruský
venkovský	venkovský	k2eAgInSc1d1	venkovský
národní	národní	k2eAgInSc1d1	národní
oděv	oděv	k1gInSc1	oděv
(	(	kIx(	(
<g/>
kroj	kroj	k1gInSc1	kroj
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
rukávů	rukáv	k1gInPc2	rukáv
<g/>
,	,	kIx,	,
oblékaný	oblékaný	k2eAgInSc1d1	oblékaný
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
dívkami	dívka	k1gFnPc7	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednodílný	jednodílný	k2eAgInSc4d1	jednodílný
šat	šat	k1gInSc4	šat
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
sukně	sukně	k1gFnPc4	sukně
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
střední	střední	k2eAgFnSc4d1	střední
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
opticky	opticky	k6eAd1	opticky
oddělena	oddělit	k5eAaPmNgNnP	oddělit
páskem	pásek	k1gMnSc7	pásek
nebo	nebo	k8xC	nebo
nošena	nošen	k2eAgFnSc1d1	nošena
s	s	k7c7	s
korzetem	korzet	k1gInSc7	korzet
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrch	vrch	k1gInSc1	vrch
šatů	šat	k1gInPc2	šat
spojují	spojovat	k5eAaImIp3nP	spojovat
pouze	pouze	k6eAd1	pouze
(	(	kIx(	(
<g/>
tenčí	tenký	k2eAgFnSc7d2	tenčí
či	či	k8xC	či
širší	široký	k2eAgFnSc7d2	širší
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
ramínka	ramínko	k1gNnPc1	ramínko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sarafánu	sarafán	k1gInSc3	sarafán
se	se	k3xPyFc4	se
nosily	nosit	k5eAaImAgFnP	nosit
nadýchané	nadýchaný	k2eAgFnPc1d1	nadýchaná
bílé	bílý	k2eAgFnPc1d1	bílá
nebo	nebo	k8xC	nebo
zdobené	zdobený	k2eAgFnPc1d1	zdobená
bavlněné	bavlněný	k2eAgFnPc1d1	bavlněná
halenky	halenka	k1gFnPc1	halenka
a	a	k8xC	a
případně	případně	k6eAd1	případně
krátké	krátký	k2eAgInPc4d1	krátký
korzety	korzet	k1gInPc4	korzet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
běžnějším	běžný	k2eAgInPc3d2	běžnější
sarafánům	sarafán	k1gInPc3	sarafán
ženy	žena	k1gFnSc2	žena
nosily	nosit	k5eAaImAgFnP	nosit
prosté	prostý	k2eAgFnPc1d1	prostá
zástěry	zástěra	k1gFnPc1	zástěra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
byly	být	k5eAaImAgFnP	být
prosté	prostý	k2eAgMnPc4d1	prostý
černé	černý	k1gMnPc4	černý
<g/>
,	,	kIx,	,
s	s	k7c7	s
květinovými	květinový	k2eAgInPc7d1	květinový
nebo	nebo	k8xC	nebo
kostkovanými	kostkovaný	k2eAgInPc7d1	kostkovaný
vzory	vzor	k1gInPc7	vzor
–	–	k?	–
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgNnSc4d1	každodenní
používání	používání	k1gNnSc4	používání
<g/>
;	;	kIx,	;
anebo	anebo	k8xC	anebo
propracovanější	propracovaný	k2eAgNnSc1d2	propracovanější
<g/>
,	,	kIx,	,
z	z	k7c2	z
lepších	dobrý	k2eAgFnPc2d2	lepší
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
brokátu	brokát	k1gInSc2	brokát
<g/>
,	,	kIx,	,
s	s	k7c7	s
lepším	dobrý	k2eAgNnSc7d2	lepší
zdobením	zdobení	k1gNnSc7	zdobení
<g/>
,	,	kIx,	,
prošíváním	prošívání	k1gNnSc7	prošívání
apod.	apod.	kA	apod.
Ty	ten	k3xDgInPc4	ten
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
vzácnější	vzácný	k2eAgFnPc4d2	vzácnější
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
nošen	nosit	k5eAaImNgInS	nosit
rolnickými	rolnický	k2eAgFnPc7d1	rolnická
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
dívkami	dívka	k1gFnPc7	dívka
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
Ruska	Rusko	k1gNnSc2	Rusko
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rusky	Ruska	k1gFnPc1	Ruska
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kokošníků	kokošník	k1gInPc2	kokošník
(	(	kIx(	(
<g/>
tradičních	tradiční	k2eAgInPc2d1	tradiční
ruských	ruský	k2eAgInPc2d1	ruský
čepců	čepec	k1gInPc2	čepec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
přestaly	přestat	k5eAaPmAgInP	přestat
coby	coby	k?	coby
dvorní	dvorní	k2eAgInPc4d1	dvorní
šaty	šat	k1gInPc4	šat
nosit	nosit	k5eAaImF	nosit
už	už	k6eAd1	už
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
během	během	k7c2	během
modernizace	modernizace	k1gFnSc2	modernizace
Ruska	Rusko	k1gNnSc2	Rusko
Petrem	Petr	k1gMnSc7	Petr
Velikým	veliký	k2eAgFnPc3d1	veliká
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
styl	styl	k1gInSc1	styl
odívání	odívání	k1gNnSc2	odívání
ruských	ruský	k2eAgMnPc2d1	ruský
aristokratů	aristokrat	k1gMnPc2	aristokrat
od	od	k7c2	od
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
Rusů	Rus	k1gMnPc2	Rus
podstatně	podstatně	k6eAd1	podstatně
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prosté	prostý	k2eAgInPc1d1	prostý
sarafány	sarafán	k1gInPc1	sarafán
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nP	nosit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
léta	léto	k1gNnSc2	léto
jako	jako	k8xS	jako
lehký	lehký	k2eAgInSc1d1	lehký
oděv	oděv	k1gInSc1	oděv
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
tradiční	tradiční	k2eAgFnSc2d1	tradiční
ruské	ruský	k2eAgFnSc2d1	ruská
blůzy	blůza	k1gFnSc2	blůza
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
viděny	vidět	k5eAaImNgFnP	vidět
během	během	k7c2	během
folklórních	folklórní	k2eAgNnPc2d1	folklórní
hudebních	hudební	k2eAgNnPc2d1	hudební
a	a	k8xC	a
tanečních	taneční	k2eAgNnPc2d1	taneční
představení	představení	k1gNnPc2	představení
a	a	k8xC	a
vyráběny	vyráběn	k2eAgInPc1d1	vyráběn
jako	jako	k8xS	jako
suvenýry	suvenýr	k1gInPc1	suvenýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sarafan	Sarafana	k1gFnPc2	Sarafana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kroj	kroj	k1gInSc1	kroj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sarafán	sarafán	k1gInSc1	sarafán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
sarafánů	sarafán	k1gInPc2	sarafán
</s>
</p>
