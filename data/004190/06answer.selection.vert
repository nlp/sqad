<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
za	za	k7c4	za
německé	německý	k2eAgFnPc4d1	německá
okupace	okupace	k1gFnPc4	okupace
Československa	Československo	k1gNnSc2	Československo
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
hrdinství	hrdinství	k1gNnSc4	hrdinství
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
schovávala	schovávat	k5eAaImAgFnS	schovávat
doma	doma	k6eAd1	doma
židovského	židovský	k2eAgMnSc2d1	židovský
uprchlíka	uprchlík	k1gMnSc2	uprchlík
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
