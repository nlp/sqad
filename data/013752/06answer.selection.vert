<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
International	International	k2eAgFnSc1d1
Agency	Agenca	k1gFnSc1
for	forum	k7c4
Research	Research	k1gInSc4
on	on	k7c6
Cancer	Cancer	k1gInSc6
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
IARC	IARC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>