<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
květen	květen	k1gInSc4
1965	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
36,96	36,96	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
33,49	33,49	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.iarc.fr	www.iarc.fr	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
International	International	k2eAgFnSc1d1
Agency	Agenca	k1gFnSc1
for	forum	k7c4
Research	Research	k1gInSc4
on	on	k7c6
Cancer	Cancer	k1gInSc6
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
IARC	IARC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
francouzském	francouzský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Lyon	Lyon	k1gInSc4
a	a	k8xC
koordinuje	koordinovat	k5eAaBmIp3nS
výzkum	výzkum	k1gInSc1
příčin	příčina	k1gFnPc2
rakoviny	rakovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vede	vést	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc4d1
databázi	databáze	k1gFnSc4
kategorizující	kategorizující	k2eAgInPc1d1
karcinogeny	karcinogen	k1gInPc1
a	a	k8xC
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
monografie	monografie	k1gFnPc4
věnované	věnovaný	k2eAgFnPc4d1
jednotlivým	jednotlivý	k2eAgNnPc3d1
činidlům	činidlo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
karcinogenů	karcinogen	k1gInPc2
dle	dle	k7c2
IARC	IARC	kA
</s>
<s>
Skupina	skupina	k1gFnSc1
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Prokázaný	prokázaný	k2eAgInSc1d1
karcinogen	karcinogen	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
</s>
<s>
2A	2A	k4
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
karcinogenní	karcinogenní	k2eAgInSc1d1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
</s>
<s>
2B	2B	k4
</s>
<s>
Podezřelý	podezřelý	k2eAgInSc1d1
karcinogen	karcinogen	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
</s>
<s>
3	#num#	k4
</s>
<s>
Neklasifikovaný	klasifikovaný	k2eNgInSc1d1
</s>
<s>
4	#num#	k4
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
není	být	k5eNaImIp3nS
karcinogenní	karcinogenní	k2eAgInSc1d1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Rakovina	rakovina	k1gFnSc1
</s>
<s>
Karcinogen	karcinogen	k1gInSc1
</s>
<s>
Nádor	nádor	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
IARC	IARC	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711171	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1001130-4	1001130-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0598	#num#	k4
0095	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79125097	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149533873	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79125097	#num#	k4
</s>
