<s>
Nurmijärvi	Nurmijärev	k1gFnSc3
</s>
<s>
Nurmijärvi	Nurmijärev	k1gFnSc3
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
Provincie	provincie	k1gFnSc2
</s>
<s>
Uusimaa	Uusimaa	k6eAd1
</s>
<s>
Nurmijärvi	Nurmijärev	k1gFnSc3
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
367,26	367,26	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
42	#num#	k4
709	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
116,3	116,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1605	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.nurmijarvi.fi/fr_FR/	www.nurmijarvi.fi/fr_FR/	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
v	v	k7c6
Nurmijärvi	Nurmijärev	k1gFnSc6
</s>
<s>
Nurmijärvi	Nurmijärvit	k5eAaPmRp2nS
[	[	kIx(
<g/>
ˈ	ˈ	k1gFnSc6
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Uusimaa	Uusima	k1gInSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Finského	finský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
,	,	kIx,
asi	asi	k9
37	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Helsinek	Helsinky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
žije	žít	k5eAaImIp3nS
přes	přes	k7c4
42	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
při	při	k7c6
hustotě	hustota	k1gFnSc6
118.03	118.03	k4
obyvalel	obyvalet	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
.	.	kIx.
1.2	1.2	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
jsou	být	k5eAaImIp3nP
švédsky	švédsky	k6eAd1
mluvící	mluvící	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
sousedí	sousedit	k5eAaImIp3nS
na	na	k7c6
západě	západ	k1gInSc6
s	s	k7c7
obcí	obec	k1gFnPc2
Vihti	Vihti	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
Hyvinkää	Hyvinkää	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
se	se	k3xPyFc4
Tuusula	Tuusula	k1gFnSc1
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
Vantaa	Vantaa	k1gFnSc1
a	a	k8xC
Espoo	Espoo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nurmijärvi	Nurmijärev	k1gFnSc6
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
obec	obec	k1gFnSc1
ve	v	k7c6
Finsku	Finsko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
nepoužívá	používat	k5eNaImIp3nS
název	název	k1gInSc1
„	„	k?
<g/>
město	město	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
definována	definován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
venkovská	venkovský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
nejjižnější	jižní	k2eAgFnSc2d3
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
Klaukkala	Klaukkal	k1gMnSc2
se	s	k7c7
17	#num#	k4
000	#num#	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c4
městských	městský	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nurmijärvi	Nurmijärev	k1gFnSc3
je	být	k5eAaImIp3nS
nejlépe	dobře	k6eAd3
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
rodiště	rodiště	k1gNnSc1
finského	finský	k2eAgMnSc2d1
národního	národní	k2eAgMnSc2d1
autora	autor	k1gMnSc2
Aleksis	Aleksis	k1gFnPc2
Kivi	kivi	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vesnice	vesnice	k1gFnPc1
ve	v	k7c6
Nurmijärvi	Nurmijärev	k1gFnSc6
</s>
<s>
Herunen	Herunen	k1gInSc1
•	•	k?
Klaukkala	Klaukkal	k1gMnSc2
•	•	k?
Lepsämä	Lepsämä	k1gMnSc2
•	•	k?
Nukari	Nukare	k1gFnSc4
•	•	k?
Nurmijärvi	Nurmijärev	k1gFnSc3
•	•	k?
Palojoki	Palojoki	k1gNnSc4
•	•	k?
Perttula	Perttulum	k1gNnSc2
•	•	k?
Rajamäki	Rajamäk	k1gMnSc3
•	•	k?
Röykkä	Röykkä	k1gMnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Rapla	Rapla	k?
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Suomen	Suomen	k1gInSc1
virallinen	virallinit	k5eAaPmNgInS,k5eAaImNgInS,k5eAaBmNgInS
tilasto	tilasto	k6eAd1
(	(	kIx(
<g/>
SVT	SVT	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Väestön	Väestön	k1gNnSc1
ennakkotilasto	ennakkotilasto	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nurmijärvi	Nurmijärev	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
1091070	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1099860-3	1099860-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
93080983	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129156017	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
93080983	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
