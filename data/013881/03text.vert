<s>
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
</s>
<s>
poslankyně	poslankyně	k1gFnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1996	#num#	k4
–	–	k?
1998	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
SPR-RSČ	SPR-RSČ	k?
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1968	#num#	k4
Profese	profese	k1gFnSc1
</s>
<s>
politička	politička	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1xF
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
politička	politička	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
90	#num#	k4c6
<g/>
.	.	kIx.
letech	rok	k1gInPc6
20	#num#	k4c2
<g/>
.	.	kIx.
století	století	k1gNnSc2
poslankyně	poslankyně	k1gFnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
za	za	k7c4
formaci	formace	k1gFnSc4
Sdružení	sdružení	k1gNnSc2
pro	pro	k7c4
republiku	republika	k1gFnSc4
-	-	kIx~
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
za	za	k7c4
SPR-RSČ	SPR-RSČ	k1gFnSc4
(	(	kIx(
<g/>
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Východočeský	východočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasedala	zasedat	k5eAaImAgFnS
v	v	k7c6
sněmovním	sněmovní	k2eAgInSc6d1
petičním	petiční	k2eAgInSc6d1
výboru	výbor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentu	parlament	k1gInSc6
setrvala	setrvat	k5eAaPmAgFnS
do	do	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1994	#num#	k4
kandidovala	kandidovat	k5eAaImAgFnS
neúspěšně	úspěšně	k6eNd1
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
Červený	červený	k2eAgInSc4d1
Kostelec	Kostelec	k1gInSc4
za	za	k7c4
SPR-RSČ	SPR-RSČ	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
31.5	31.5	k4
<g/>
.	.	kIx.
-	-	kIx~
1.6	1.6	k4
<g/>
.1996	.1996	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
<g/>
:	:	kIx,
8	#num#	k4
419	#num#	k4
Osoba	osoba	k1gFnSc1
<g/>
:	:	kIx,
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
komunalnipolitika	komunalnipolitika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
