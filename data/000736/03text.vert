<s>
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
University	universita	k1gFnPc1	universita
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gMnSc1	Universitas
Oxoniensis	Oxoniensis	k1gFnSc2	Oxoniensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc6d1	mluvící
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
městě	město	k1gNnSc6	město
Oxford	Oxford	k1gInSc4	Oxford
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejprestižnějších	prestižní	k2eAgFnPc2d3	nejprestižnější
univerzit	univerzita	k1gFnPc2	univerzita
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vysokých	vysoká	k1gFnPc2	vysoká
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
světě	svět	k1gInSc6	svět
pravidelně	pravidelně	k6eAd1	pravidelně
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
tradiční	tradiční	k2eAgFnSc7d1	tradiční
barvou	barva	k1gFnSc7	barva
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
federativního	federativní	k2eAgNnSc2d1	federativní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
38	[number]	k4	38
autonomních	autonomní	k2eAgFnPc2d1	autonomní
kolejí	kolej	k1gFnPc2	kolej
<g/>
,	,	kIx,	,
6	[number]	k4	6
permanent	permanent	k1gInSc1	permanent
halls	halls	k1gInSc4	halls
of	of	k?	of
residence	residence	k1gFnSc2	residence
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
instituce	instituce	k1gFnSc2	instituce
stojí	stát	k5eAaImIp3nS	stát
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
vicekancléř	vicekancléř	k1gMnSc1	vicekancléř
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kancléř	kancléř	k1gMnSc1	kancléř
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
symbolická	symbolický	k2eAgFnSc1d1	symbolická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
doživotně	doživotně	k6eAd1	doživotně
voleny	volen	k2eAgFnPc1d1	volena
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgNnSc1d1	samotné
vedení	vedení	k1gNnSc1	vedení
univerzity	univerzita	k1gFnSc2	univerzita
fakticky	fakticky	k6eAd1	fakticky
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
vicekancléře	vicekancléř	k1gMnPc4	vicekancléř
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
také	také	k9	také
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
univerzity	univerzita	k1gFnSc2	univerzita
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc4d1	přesný
rok	rok	k1gInSc4	rok
založení	založení	k1gNnSc2	založení
však	však	k9	však
není	být	k5eNaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
doložené	doložený	k2eAgInPc1d1	doložený
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
probíhající	probíhající	k2eAgFnSc6d1	probíhající
výuce	výuka	k1gFnSc6	výuka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1096	[number]	k4	1096
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
když	když	k8xS	když
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
zakázal	zakázat	k5eAaPmAgInS	zakázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1167	[number]	k4	1167
Angličanům	Angličan	k1gMnPc3	Angličan
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1201	[number]	k4	1201
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
univerzity	univerzita	k1gFnSc2	univerzita
magister	magistra	k1gFnPc2	magistra
scolarum	scolarum	k1gNnSc1	scolarum
Oxonie	Oxonie	k1gFnPc1	Oxonie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1214	[number]	k4	1214
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
titul	titul	k1gInSc4	titul
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
také	také	k6eAd1	také
vznikat	vznikat	k5eAaImF	vznikat
takzvané	takzvaný	k2eAgInPc1d1	takzvaný
colleges	colleges	k1gInSc1	colleges
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
omezovaly	omezovat	k5eAaImAgFnP	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
poskytování	poskytování	k1gNnSc6	poskytování
ubytování	ubytování	k1gNnSc2	ubytování
a	a	k8xC	a
stravy	strava	k1gFnSc2	strava
studentům	student	k1gMnPc3	student
a	a	k8xC	a
akademikům	akademik	k1gMnPc3	akademik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc1	jejich
působnost	působnost	k1gFnSc1	působnost
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
aktivity	aktivita	k1gFnPc4	aktivita
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgMnPc3d3	nejstarší
patří	patřit	k5eAaImIp3nS	patřit
University	universita	k1gFnPc4	universita
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
Balliol	Balliol	k1gInSc4	Balliol
College	College	k1gFnPc2	College
a	a	k8xC	a
Merton	Merton	k1gInSc1	Merton
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
výuka	výuka	k1gFnSc1	výuka
na	na	k7c6	na
Oxfordu	Oxford	k1gInSc6	Oxford
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
renesancí	renesance	k1gFnSc7	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
reformace	reformace	k1gFnSc2	reformace
a	a	k8xC	a
rozkolu	rozkol	k1gInSc2	rozkol
s	s	k7c7	s
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
mnoho	mnoho	k4c1	mnoho
učenců	učenec	k1gMnPc2	učenec
uteklo	utéct	k5eAaPmAgNnS	utéct
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
metoda	metoda	k1gFnSc1	metoda
výuky	výuka	k1gFnSc2	výuka
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
scholastiky	scholastika	k1gFnSc2	scholastika
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
renesančním	renesanční	k2eAgMnSc6d1	renesanční
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
univerzita	univerzita	k1gFnSc1	univerzita
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
roajalistů	roajalista	k1gMnPc2	roajalista
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
město	město	k1gNnSc1	město
Oxford	Oxford	k1gInSc4	Oxford
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
parlamentaristů	parlamentarista	k1gMnPc2	parlamentarista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
univerzita	univerzita	k1gFnSc1	univerzita
snaží	snažit	k5eAaImIp3nS	snažit
stát	stát	k5eAaImF	stát
stranou	strana	k1gFnSc7	strana
politických	politický	k2eAgInPc2d1	politický
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
čtyři	čtyři	k4xCgFnPc1	čtyři
ženské	ženská	k1gFnPc1	ženská
koleje	kolej	k1gFnPc1	kolej
<g/>
:	:	kIx,	:
Lady	lady	k1gFnSc1	lady
Margaret	Margareta	k1gFnPc2	Margareta
Hall	Halla	k1gFnPc2	Halla
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somerville	Somerville	k1gFnSc1	Somerville
College	College	k1gFnSc1	College
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
St	St	kA	St
Hugh	Hugh	k1gInSc1	Hugh
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
a	a	k8xC	a
St	St	kA	St
Hilda	Hilda	k1gFnSc1	Hilda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
ženy	žena	k1gFnPc1	žena
mohly	moct	k5eAaImAgFnP	moct
stát	stát	k5eAaPmF	stát
plnohodnotnými	plnohodnotný	k2eAgFnPc7d1	plnohodnotná
studentkami	studentka	k1gFnPc7	studentka
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
kolejí	kolej	k1gFnPc2	kolej
určených	určený	k2eAgFnPc2d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
byly	být	k5eAaImAgFnP	být
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
ženám	žena	k1gFnPc3	žena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
výhradně	výhradně	k6eAd1	výhradně
ženská	ženský	k2eAgFnSc1d1	ženská
kolej	kolej	k1gFnSc1	kolej
St.	st.	kA	st.
Hilda	Hilda	k1gFnSc1	Hilda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
otevřela	otevřít	k5eAaPmAgFnS	otevřít
své	svůj	k3xOyFgFnPc4	svůj
brány	brána	k1gFnPc4	brána
mužům	muž	k1gMnPc3	muž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
všechny	všechen	k3xTgFnPc1	všechen
koleje	kolej	k1gFnPc1	kolej
koedukační	koedukační	k2eAgFnPc1d1	koedukační
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
odešli	odejít	k5eAaPmAgMnP	odejít
po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
a	a	k8xC	a
měšťany	měšťan	k1gMnPc7	měšťan
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
roku	rok	k1gInSc2	rok
1209	[number]	k4	1209
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
do	do	k7c2	do
města	město	k1gNnSc2	město
Cambridge	Cambridge	k1gFnSc2	Cambridge
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
tam	tam	k6eAd1	tam
University	universita	k1gFnSc2	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
univerzitami	univerzita	k1gFnPc7	univerzita
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
panuje	panovat	k5eAaImIp3nS	panovat
rivalita	rivalita	k1gFnSc1	rivalita
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
obvykle	obvykle	k6eAd1	obvykle
přátelská	přátelský	k2eAgNnPc1d1	přátelské
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
univerzity	univerzita	k1gFnPc1	univerzita
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c1	mnoho
společných	společný	k2eAgInPc2d1	společný
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rysů	rys	k1gInPc2	rys
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
tedy	tedy	k9	tedy
často	často	k6eAd1	často
odkazováno	odkazovat	k5eAaImNgNnS	odkazovat
společně	společně	k6eAd1	společně
jako	jako	k8xC	jako
na	na	k7c6	na
Oxbridge	Oxbridge	k1gFnSc6	Oxbridge
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
38	[number]	k4	38
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
kolejí	kolej	k1gFnPc2	kolej
(	(	kIx(	(
<g/>
colleges	colleges	k1gInSc1	colleges
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
Permanent	permanent	k1gInSc1	permanent
Private	Privat	k1gInSc5	Privat
Halls	Halls	k1gInSc1	Halls
(	(	kIx(	(
<g/>
PPH	PPH	kA	PPH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
Oxfordskou	oxfordský	k2eAgFnSc4d1	Oxfordská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
je	být	k5eAaImIp3nS	být
sdružením	sdružení	k1gNnSc7	sdružení
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
autonomních	autonomní	k2eAgFnPc2d1	autonomní
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vedením	vedení	k1gNnSc7	vedení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vicekancléřem	vicekancléř	k1gMnSc7	vicekancléř
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
akademické	akademický	k2eAgInPc1d1	akademický
ústavy	ústav	k1gInPc1	ústav
jsou	být	k5eAaImIp3nP	být
centralizovány	centralizován	k2eAgInPc1d1	centralizován
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejsou	být	k5eNaImIp3nP	být
přidružené	přidružený	k2eAgFnPc1d1	přidružená
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
koleji	kolej	k1gFnSc3	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ústavy	ústav	k1gInPc1	ústav
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
prostředky	prostředek	k1gInPc4	prostředek
potřebné	potřebný	k2eAgInPc4d1	potřebný
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
osnovy	osnova	k1gFnPc4	osnova
a	a	k8xC	a
udělují	udělovat	k5eAaImIp3nP	udělovat
pokyny	pokyn	k1gInPc1	pokyn
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
,	,	kIx,	,
podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
výzkumech	výzkum	k1gInPc6	výzkum
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
semináře	seminář	k1gInPc4	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Koleje	kolej	k1gFnPc1	kolej
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
studenty	student	k1gMnPc4	student
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
konzultační	konzultační	k2eAgFnSc4d1	konzultační
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
akademického	akademický	k2eAgInSc2d1	akademický
ústavu	ústav	k1gInSc2	ústav
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
koleje	kolej	k1gFnPc1	kolej
více	hodně	k6eAd2	hodně
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Nuffield	Nuffield	k1gInSc1	Nuffield
College	Colleg	k1gInSc2	Colleg
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
výjimky	výjimka	k1gFnPc4	výjimka
a	a	k8xC	a
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
kolejí	kolej	k1gFnPc2	kolej
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
akademiky	akademik	k1gMnPc4	akademik
a	a	k8xC	a
studenty	student	k1gMnPc4	student
studující	studující	k1gFnSc2	studující
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
jako	jako	k8xC	jako
např.	např.	kA	např.
knihovny	knihovna	k1gFnPc1	knihovna
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgFnSc7d1	hlavní
univerzitou	univerzita	k1gFnSc7	univerzita
(	(	kIx(	(
<g/>
Bodleianská	Bodleianský	k2eAgFnSc1d1	Bodleianský
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ústavy	ústav	k1gInPc4	ústav
(	(	kIx(	(
<g/>
knihovny	knihovna	k1gFnPc4	knihovna
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ústavů	ústav	k1gInPc2	ústav
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Knihovna	knihovna	k1gFnSc1	knihovna
fakulty	fakulta	k1gFnSc2	fakulta
anglistiky	anglistika	k1gFnSc2	anglistika
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolejemi	kolej	k1gFnPc7	kolej
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
nabízí	nabízet	k5eAaImIp3nS	nabízet
svým	svůj	k3xOyFgMnPc3	svůj
studentům	student	k1gMnPc3	student
knihovnu	knihovna	k1gFnSc4	knihovna
s	s	k7c7	s
literaturou	literatura	k1gFnSc7	literatura
mnoha	mnoho	k4c2	mnoho
oborů	obor	k1gInPc2	obor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
trimestrů	trimestr	k1gInPc2	trimestr
<g/>
:	:	kIx,	:
Michaelmas	Michaelmasa	k1gFnPc2	Michaelmasa
(	(	kIx(	(
<g/>
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hilary	Hilar	k1gInPc1	Hilar
(	(	kIx(	(
<g/>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Trinity	Trinita	k1gFnSc2	Trinita
(	(	kIx(	(
<g/>
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgInPc2	tento
trimestrů	trimestr	k1gInPc2	trimestr
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
rada	rada	k1gFnSc1	rada
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
určí	určit	k5eAaPmIp3nS	určit
osm	osm	k4xCc4	osm
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Full	Full	k1gMnSc1	Full
terms	terms	k6eAd1	terms
<g/>
"	"	kIx"	"
kdy	kdy	k6eAd1	kdy
probíhá	probíhat	k5eAaImIp3nS	probíhat
výuka	výuka	k1gFnSc1	výuka
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
období	období	k1gNnPc1	období
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
než	než	k8xS	než
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
britských	britský	k2eAgFnPc2d1	britská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
intenzivně	intenzivně	k6eAd1	intenzivně
připravovat	připravovat	k5eAaImF	připravovat
i	i	k9	i
během	během	k7c2	během
prázdnin	prázdniny	k1gFnPc2	prázdniny
(	(	kIx(	(
<g/>
vánočních	vánoční	k2eAgFnPc2d1	vánoční
<g/>
,	,	kIx,	,
velikonočních	velikonoční	k2eAgFnPc2d1	velikonoční
a	a	k8xC	a
velkých	velký	k2eAgFnPc2d1	velká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
typem	typ	k1gInSc7	typ
výuky	výuka	k1gFnSc2	výuka
je	být	k5eAaImIp3nS	být
výuka	výuka	k1gFnSc1	výuka
formou	forma	k1gFnSc7	forma
konzultací	konzultace	k1gFnSc7	konzultace
(	(	kIx(	(
<g/>
tutoriálu	tutoriál	k1gInSc2	tutoriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
až	až	k8xS	až
čtyři	čtyři	k4xCgMnPc1	čtyři
studenti	student	k1gMnPc1	student
stráví	strávit	k5eAaPmIp3nP	strávit
hodinu	hodina	k1gFnSc4	hodina
s	s	k7c7	s
akademikem	akademik	k1gMnSc7	akademik
diskuzí	diskuze	k1gFnPc2	diskuze
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
celotýdenní	celotýdenní	k2eAgFnSc6d1	celotýdenní
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
eseji	esej	k1gInSc3	esej
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
většiny	většina	k1gFnSc2	většina
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
některých	některý	k3yIgFnPc2	některý
matematických	matematický	k2eAgFnPc2d1	matematická
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
či	či	k8xC	či
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
úlohách	úloha	k1gFnPc6	úloha
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
většiny	většina	k1gFnSc2	většina
matematických	matematický	k2eAgFnPc2d1	matematická
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
či	či	k8xC	či
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
konzultace	konzultace	k1gFnPc4	konzultace
týdně	týdně	k6eAd1	týdně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
jakéhokoli	jakýkoli	k3yIgMnSc2	jakýkoli
akademika	akademik	k1gMnSc2	akademik
na	na	k7c6	na
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vlastní	vlastní	k2eAgFnSc6d1	vlastní
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
a	a	k8xC	a
kolik	kolik	k4yRc4	kolik
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vyučujících	vyučující	k2eAgFnPc2d1	vyučující
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkem	doplněk	k1gInSc7	doplněk
ke	k	k7c3	k
konzultacím	konzultace	k1gFnPc3	konzultace
jsou	být	k5eAaImIp3nP	být
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
kurzy	kurz	k1gInPc1	kurz
a	a	k8xC	a
semináře	seminář	k1gInPc1	seminář
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Postgraduální	postgraduální	k2eAgMnPc1d1	postgraduální
studenti	student	k1gMnPc1	student
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
praktické	praktický	k2eAgFnSc6d1	praktická
výuce	výuka	k1gFnSc6	výuka
většinou	většinou	k6eAd1	většinou
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
kurzy	kurz	k1gInPc1	kurz
a	a	k8xC	a
semináře	seminář	k1gInPc1	seminář
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
individuální	individuální	k2eAgInSc4d1	individuální
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
organizaci	organizace	k1gFnSc4	organizace
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
udělování	udělování	k1gNnSc2	udělování
titulů	titul	k1gInPc2	titul
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
univerzita	univerzita	k1gFnSc1	univerzita
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
prvního	první	k4xOgInSc2	první
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
úspěšně	úspěšně	k6eAd1	úspěšně
složit	složit	k5eAaPmF	složit
dvě	dva	k4xCgFnPc4	dva
sady	sada	k1gFnPc4	sada
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sada	sada	k1gFnSc1	sada
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Honour	Honour	k1gMnSc1	Honour
Moderations	Moderationsa	k1gFnPc2	Moderationsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mods	Mods	k1gInSc1	Mods
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Preliminary	Preliminar	k1gInPc1	Preliminar
Moderations	Moderationsa	k1gFnPc2	Moderationsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Prelims	Prelims	k1gInSc1	Prelims
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
obyčejně	obyčejně	k6eAd1	obyčejně
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
(	(	kIx(	(
<g/>
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
studují	studovat	k5eAaImIp3nP	studovat
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnSc4d1	experimentální
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
,	,	kIx,	,
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
,	,	kIx,	,
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
fyziologii	fyziologie	k1gFnSc4	fyziologie
probíhají	probíhat	k5eAaImIp3nP	probíhat
zkoušky	zkouška	k1gFnPc1	zkouška
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
trimestrech	trimestr	k1gInPc6	trimestr
a	a	k8xC	a
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
studují	studovat	k5eAaImIp3nP	studovat
klasická	klasický	k2eAgNnPc4d1	klasické
studia	studio	k1gNnPc4	studio
po	po	k7c6	po
pěti	pět	k4xCc6	pět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
sada	sada	k1gFnSc1	sada
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Final	Final	k1gInSc1	Final
Honour	Honoura	k1gFnPc2	Honoura
School	Schoola	k1gFnPc2	Schoola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Finals	Finals	k1gInSc1	Finals
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
probíhají	probíhat	k5eAaImIp3nP	probíhat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
výkonu	výkon	k1gInSc2	výkon
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
je	být	k5eAaImIp3nS	být
absolventům	absolvent	k1gMnPc3	absolvent
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
prvního	první	k4xOgMnSc2	první
<g/>
,	,	kIx,	,
vyššího	vysoký	k2eAgNnSc2d2	vyšší
a	a	k8xC	a
nižšího	nízký	k2eAgNnSc2d2	nižší
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třetího	třetí	k4xOgInSc2	třetí
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
postgraduální	postgraduální	k2eAgNnSc4d1	postgraduální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Magisterské	magisterský	k2eAgInPc1d1	magisterský
a	a	k8xC	a
doktorské	doktorský	k2eAgInPc1d1	doktorský
tituly	titul	k1gInPc1	titul
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
vlastním	vlastní	k2eAgInSc6d1	vlastní
výzkumu	výzkum	k1gInSc6	výzkum
jsou	být	k5eAaImIp3nP	být
udělovány	udělován	k2eAgFnPc1d1	udělována
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
předměty	předmět	k1gInPc4	předmět
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
studia	studio	k1gNnSc2	studio
vyučované	vyučovaný	k2eAgFnPc1d1	vyučovaná
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tradice	tradice	k1gFnSc2	tradice
mohou	moct	k5eAaImIp3nP	moct
všichni	všechen	k3xTgMnPc1	všechen
absolventi	absolvent	k1gMnPc1	absolvent
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
imatrikulace	imatrikulace	k1gFnSc2	imatrikulace
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
dalšího	další	k2eAgNnSc2d1	další
studia	studio	k1gNnSc2	studio
za	za	k7c4	za
nepatrný	nepatrný	k2eAgInSc4d1	nepatrný
poplatek	poplatek	k1gInSc4	poplatek
zažádat	zažádat	k5eAaPmF	zažádat
o	o	k7c4	o
povýšení	povýšení	k1gNnSc4	povýšení
svého	svůj	k3xOyFgInSc2	svůj
titulu	titul	k1gInSc2	titul
na	na	k7c4	na
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
MA	MA	kA	MA
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Master	master	k1gMnSc1	master
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Encaenia	Encaenium	k1gNnSc2	Encaenium
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgFnSc1d1	každoroční
ceremonie	ceremonie	k1gFnSc1	ceremonie
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yQgFnSc2	který
významné	významný	k2eAgFnSc2d1	významná
světové	světový	k2eAgFnSc2d1	světová
osobnosti	osobnost	k1gFnSc2	osobnost
přebírají	přebírat	k5eAaImIp3nP	přebírat
její	její	k3xOp3gNnSc1	její
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
:	:	kIx,	:
titul	titul	k1gInSc1	titul
čestného	čestný	k2eAgMnSc2d1	čestný
doktora	doktor	k1gMnSc2	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
udělování	udělování	k1gNnSc2	udělování
titulu	titul	k1gInSc2	titul
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
laureáty	laureát	k1gMnPc7	laureát
Češi	Čech	k1gMnPc1	Čech
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
-	-	kIx~	-
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
-	-	kIx~	-
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
exilovou	exilový	k2eAgFnSc4d1	exilová
činnost	činnost	k1gFnSc4	činnost
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
-	-	kIx~	-
za	za	k7c4	za
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
-	-	kIx~	-
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
bohosloví	bohosloví	k1gNnSc2	bohosloví
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Divinity	Divinita	k1gFnSc2	Divinita
<g/>
,	,	kIx,	,
honoris	honoris	k1gFnSc1	honoris
causa	causa	k1gFnSc1	causa
<g/>
)	)	kIx)	)
Rektorem	rektor	k1gMnSc7	rektor
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
politik	politik	k1gMnSc1	politik
Chris	Chris	k1gFnSc2	Chris
Patten	Pattno	k1gNnPc2	Pattno
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
21	[number]	k4	21
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
elitářský	elitářský	k2eAgInSc4d1	elitářský
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovalo	vystudovat	k5eAaPmAgNnS	vystudovat
zde	zde	k6eAd1	zde
nejméně	málo	k6eAd3	málo
8	[number]	k4	8
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
47	[number]	k4	47
držitelů	držitel	k1gMnPc2	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
3	[number]	k4	3
držitelé	držitel	k1gMnPc1	držitel
Fieldsovy	Fieldsův	k2eAgFnSc2d1	Fieldsova
medaile	medaile	k1gFnSc2	medaile
<g/>
,	,	kIx,	,
26	[number]	k4	26
premiérů	premiér	k1gMnPc2	premiér
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
současného	současný	k2eAgMnSc2d1	současný
Davida	David	k1gMnSc2	David
Camerona	Cameron	k1gMnSc2	Cameron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
30	[number]	k4	30
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
premiérů	premiér	k1gMnPc2	premiér
<g/>
,	,	kIx,	,
12	[number]	k4	12
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
89	[number]	k4	89
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
<g/>
,	,	kIx,	,
18	[number]	k4	18
kardinálů	kardinál	k1gMnPc2	kardinál
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
vzdoropapež	vzdoropapež	k1gMnSc1	vzdoropapež
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
vědci	vědec	k1gMnPc1	vědec
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Stephen	Stephen	k2eAgInSc4d1	Stephen
Hawking	Hawking	k1gInSc4	Hawking
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc1	Dawkins
a	a	k8xC	a
držitelé	držitel	k1gMnPc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Anthony	Anthona	k1gFnSc2	Anthona
James	James	k1gMnSc1	James
Leggett	Leggett	k1gMnSc1	Leggett
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gInSc1	Berners-Lee
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
seznamem	seznam	k1gInSc7	seznam
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
spojeni	spojen	k2eAgMnPc1d1	spojen
s	s	k7c7	s
Oxfordem	Oxford	k1gInSc7	Oxford
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInSc4d3	nejznámější
Evelyn	Evelyn	k1gInSc4	Evelyn
Waugh	Waugha	k1gFnPc2	Waugha
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Austen	Austen	k2eAgMnSc1d1	Austen
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gFnSc1	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
<g/>
,	,	kIx,	,
Aldous	Aldous	k1gMnSc1	Aldous
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Pullman	Pullman	k1gMnSc1	Pullman
<g/>
,	,	kIx,	,
Vikram	Vikram	k1gInSc1	Vikram
Seth	Seth	k1gInSc1	Seth
<g/>
,	,	kIx,	,
Plum	Plum	k1gMnSc1	Plum
Sykes	Sykes	k1gMnSc1	Sykes
a	a	k8xC	a
Percy	Perca	k1gMnSc2	Perca
Bysshe	Byssh	k1gMnSc2	Byssh
Shelley	Shellea	k1gMnSc2	Shellea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
University	universita	k1gFnSc2	universita
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc4	Oxford
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc4	Press
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
</s>
