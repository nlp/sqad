<p>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
<g/>
:	:	kIx,	:
Ukrajinská	ukrajinský	k2eAgNnPc4d1	ukrajinské
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
USSR	USSR	kA	USSR
<g/>
;	;	kIx,	;
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
У	У	k?	У
Р	Р	k?	Р
С	С	k?	С
Р	Р	k?	Р
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
У	У	k?	У
С	С	k?	С
С	С	k?	С
Р	Р	k?	Р
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
patnácti	patnáct	k4xCc2	patnáct
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rusko-polské	ruskoolský	k2eAgFnSc2d1	rusko-polská
války	válka	k1gFnSc2	válka
přišla	přijít	k5eAaPmAgFnS	přijít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
o	o	k7c4	o
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
anexi	anexe	k1gFnSc6	anexe
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
Polska	Polsko	k1gNnSc2	Polsko
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
získala	získat	k5eAaPmAgFnS	získat
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
svá	svůj	k3xOyFgNnPc4	svůj
západní	západní	k2eAgNnPc4d1	západní
území	území	k1gNnPc4	území
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
USSR	USSR	kA	USSR
připojena	připojen	k2eAgFnSc1d1	připojena
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
/	/	kIx~	/
<g/>
Zakarpatská	zakarpatský	k2eAgFnSc1d1	Zakarpatská
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
získala	získat	k5eAaPmAgFnS	získat
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
Krym	Krym	k1gInSc1	Krym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
předtím	předtím	k6eAd1	předtím
ruský	ruský	k2eAgInSc1d1	ruský
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
administrativně	administrativně	k6eAd1	administrativně
(	(	kIx(	(
<g/>
19.2	[number]	k4	19.2
<g/>
.1954	.1954	k4	.1954
dar	dar	k1gInSc1	dar
Nikity	Nikita	k1gFnSc2	Nikita
Chruščova	Chruščův	k2eAgFnSc1d1	Chruščova
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhlášky	vyhláška	k1gFnSc2	vyhláška
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
zákona	zákon	k1gInSc2	zákon
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičleněn	přičleněn	k2eAgInSc1d1	přičleněn
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnSc2d1	ostatní
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
i	i	k8xC	i
symboliku	symbolika	k1gFnSc4	symbolika
a	a	k8xC	a
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
republik	republika	k1gFnPc2	republika
SSSR	SSSR	kA	SSSR
byla	být	k5eAaImAgFnS	být
též	též	k9	též
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
samostatně	samostatně	k6eAd1	samostatně
i	i	k8xC	i
v	v	k7c6	v
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zastoupení	zastoupení	k1gNnSc4	zastoupení
měla	mít	k5eAaImAgFnS	mít
ještě	ještě	k9	ještě
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Charkově	Charkov	k1gInSc6	Charkov
první	první	k4xOgMnSc1	první
Všeukrajinský	Všeukrajinský	k2eAgInSc1d1	Všeukrajinský
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
budoucím	budoucí	k2eAgNnSc6d1	budoucí
vytvoření	vytvoření	k1gNnSc6	vytvoření
federace	federace	k1gFnSc2	federace
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
federativní	federativní	k2eAgFnSc7d1	federativní
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
republikou	republika	k1gFnSc7	republika
(	(	kIx(	(
<g/>
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
území	území	k1gNnSc6	území
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
několik	několik	k4yIc4	několik
sovětských	sovětský	k2eAgInPc2d1	sovětský
i	i	k8xC	i
nesocialistických	socialistický	k2eNgInPc2d1	nesocialistický
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
:	:	kIx,	:
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
Doněcko-krivorožská	doněckorivorožský	k2eAgFnSc1d1	doněcko-krivorožský
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Oděská	oděský	k2eAgFnSc1d1	Oděská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Západoukrajinská	Západoukrajinský	k2eAgFnSc1d1	Západoukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
sovětské	sovětský	k2eAgFnPc1d1	sovětská
republiky	republika	k1gFnPc1	republika
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
sovětské	sovětský	k2eAgFnSc2d1	sovětská
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
v	v	k7c6	v
Charkově	Charkov	k1gInSc6	Charkov
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
federací	federace	k1gFnSc7	federace
sovětských	sovětský	k2eAgFnPc2d1	sovětská
národních	národní	k2eAgFnPc2d1	národní
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
republiku	republika	k1gFnSc4	republika
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1918	[number]	k4	1918
obsadila	obsadit	k5eAaPmAgFnS	obsadit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
byla	být	k5eAaImAgFnS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
USSR	USSR	kA	USSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
musel	muset	k5eAaImAgInS	muset
čelit	čelit	k5eAaImF	čelit
polské	polský	k2eAgFnSc3d1	polská
invazi	invaze	k1gFnSc3	invaze
a	a	k8xC	a
proto	proto	k8xC	proto
podepsal	podepsat	k5eAaPmAgMnS	podepsat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1920	[number]	k4	1920
s	s	k7c7	s
RSFSR	RSFSR	kA	RSFSR
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
utvoření	utvoření	k1gNnSc6	utvoření
vojenské	vojenský	k2eAgFnSc2d1	vojenská
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgInPc1	který
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
pět	pět	k4xCc4	pět
společných	společný	k2eAgInPc2d1	společný
lidových	lidový	k2eAgInPc2d1	lidový
komisariátů	komisariát	k1gInPc2	komisariát
(	(	kIx(	(
<g/>
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
chaotické	chaotický	k2eAgFnSc2d1	chaotická
válečné	válečný	k2eAgFnSc2d1	válečná
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
USSR	USSR	kA	USSR
krátce	krátce	k6eAd1	krátce
existovaly	existovat	k5eAaImAgInP	existovat
autonomní	autonomní	k2eAgNnSc4d1	autonomní
Besarabská	besarabský	k2eAgFnSc1d1	besarabská
SSR	SSR	kA	SSR
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
až	až	k8xS	až
září	září	k1gNnSc4	září
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
a	a	k8xC	a
Haličská	haličský	k2eAgFnSc1d1	Haličská
SSR	SSR	kA	SSR
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
až	až	k6eAd1	až
září	zářit	k5eAaImIp3nS	zářit
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
byly	být	k5eAaImAgInP	být
ukončeny	ukončit	k5eAaPmNgInP	ukončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1921	[number]	k4	1921
podepsáním	podepsání	k1gNnSc7	podepsání
Rižské	rižský	k2eAgFnSc2d1	Rižská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanovila	stanovit	k5eAaPmAgFnS	stanovit
definitivní	definitivní	k2eAgFnSc4d1	definitivní
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
republikami	republika	k1gFnPc7	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meziválečná	meziválečný	k2eAgFnSc1d1	meziválečná
doba	doba	k1gFnSc1	doba
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
podepsala	podepsat	k5eAaPmAgFnS	podepsat
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
Běloruskou	Běloruska	k1gFnSc7	Běloruska
SSR	SSR	kA	SSR
a	a	k8xC	a
Zakavkazskou	zakavkazský	k2eAgFnSc7d1	zakavkazská
SFSR	SFSR	kA	SFSR
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zbytek	zbytek	k1gInSc1	zbytek
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
ukrajinizace	ukrajinizace	k1gFnSc2	ukrajinizace
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
se	se	k3xPyFc4	se
ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
podporováno	podporován	k2eAgNnSc1d1	podporováno
ukrajinskojazyčné	ukrajinskojazyčný	k2eAgNnSc1d1	ukrajinskojazyčný
oficiální	oficiální	k2eAgNnSc1d1	oficiální
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
vedení	vedení	k1gNnSc1	vedení
razilo	razit	k5eAaImAgNnS	razit
heslo	heslo	k1gNnSc4	heslo
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc4d1	národní
obsah	obsah	k1gInSc4	obsah
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
formou	forma	k1gFnSc7	forma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
sovětské	sovětský	k2eAgFnSc2d1	sovětská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
však	však	k9	však
razantně	razantně	k6eAd1	razantně
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
"	"	kIx"	"
<g/>
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
úchylky	úchylka	k1gFnSc2	úchylka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následujícím	následující	k2eAgFnPc3d1	následující
čistkám	čistka	k1gFnPc3	čistka
padlo	padnout	k5eAaPmAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
nejen	nejen	k6eAd1	nejen
mnoho	mnoho	k4c4	mnoho
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
intelektuálů	intelektuál	k1gMnPc2	intelektuál
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
předních	přední	k2eAgFnPc2d1	přední
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofická	katastrofický	k2eAgFnSc1d1	katastrofická
stalinská	stalinský	k2eAgFnSc1d1	stalinská
politika	politika	k1gFnSc1	politika
násilné	násilný	k2eAgFnSc2d1	násilná
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
suchy	sucho	k1gNnPc7	sucho
a	a	k8xC	a
přednostním	přednostní	k2eAgNnSc7d1	přednostní
zásobováním	zásobování	k1gNnSc7	zásobování
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
letech	let	k1gInPc6	let
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
masový	masový	k2eAgInSc1d1	masový
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
padlo	padnout	k5eAaPmAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
2,6	[number]	k4	2,6
až	až	k9	až
3,5	[number]	k4	3,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stalinského	stalinský	k2eAgInSc2d1	stalinský
teroru	teror	k1gInSc2	teror
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
industrializace	industrializace	k1gFnSc2	industrializace
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
Paktu	pakt	k1gInSc6	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
územním	územní	k2eAgFnPc3d1	územní
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
anektoval	anektovat	k5eAaBmAgInS	anektovat
zpět	zpět	k6eAd1	zpět
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
a	a	k8xC	a
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
většina	většina	k1gFnSc1	většina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
získalo	získat	k5eAaPmAgNnS	získat
Polsko	Polsko	k1gNnSc4	Polsko
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
až	až	k9	až
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
jako	jako	k8xS	jako
Polsko-sovětská	polskoovětský	k2eAgFnSc1d1	polsko-sovětská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
USSR	USSR	kA	USSR
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
nuceno	nucen	k2eAgNnSc1d1	nuceno
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
ultimáta	ultimátum	k1gNnSc2	ultimátum
vydat	vydat	k5eAaPmF	vydat
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc2	svaz
Severní	severní	k2eAgFnSc4d1	severní
Bukovinu	Bukovina	k1gFnSc4	Bukovina
<g/>
,	,	kIx,	,
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
až	až	k9	až
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
Besarábie	Besarábie	k1gFnPc1	Besarábie
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
sovětské	sovětský	k2eAgFnSc3d1	sovětská
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
USSR	USSR	kA	USSR
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Moldavské	moldavský	k2eAgNnSc4d1	moldavské
ASSR	ASSR	kA	ASSR
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Podněstří	Podněstří	k1gFnSc2	Podněstří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
nově	nova	k1gFnSc3	nova
utvořené	utvořený	k2eAgFnSc2d1	utvořená
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
území	území	k1gNnSc2	území
USSR	USSR	kA	USSR
postupně	postupně	k6eAd1	postupně
obsadila	obsadit	k5eAaPmAgNnP	obsadit
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
silné	silný	k2eAgNnSc1d1	silné
partyzánské	partyzánský	k2eAgNnSc1d1	partyzánské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
měly	mít	k5eAaImAgInP	mít
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
ukrajinské	ukrajinský	k2eAgInPc4d1	ukrajinský
nacionalistické	nacionalistický	k2eAgInPc4d1	nacionalistický
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
viděly	vidět	k5eAaImAgInP	vidět
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
okupanty	okupant	k1gMnPc7	okupant
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
nezamýšlel	zamýšlet	k5eNaImAgMnS	zamýšlet
a	a	k8xC	a
ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
správně	správně	k6eAd1	správně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
západní	západní	k2eAgInSc4d1	západní
(	(	kIx(	(
<g/>
Distrikt	distrikt	k1gInSc4	distrikt
Halič	Halič	k1gFnSc4	Halič
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Generálního	generální	k2eAgInSc2d1	generální
gouvernementu	gouvernement	k1gInSc2	gouvernement
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
Říšský	říšský	k2eAgInSc1d1	říšský
komisariát	komisariát	k1gInSc1	komisariát
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
okupační	okupační	k2eAgInSc1d1	okupační
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
bylo	být	k5eAaImAgNnS	být
vyvražďováno	vyvražďován	k2eAgNnSc4d1	vyvražďován
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
funkcionáři	funkcionář	k1gMnPc1	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zemi	zem	k1gFnSc4	zem
nesmírné	smírný	k2eNgFnSc2d1	nesmírná
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
získala	získat	k5eAaPmAgFnS	získat
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
USSR	USSR	kA	USSR
znovu	znovu	k6eAd1	znovu
kontrolu	kontrola	k1gFnSc4	kontrola
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1943	[number]	k4	1943
a	a	k8xC	a
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
doba	doba	k1gFnSc1	doba
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
USSR	USSR	kA	USSR
připojena	připojit	k5eAaPmNgFnS	připojit
jako	jako	k8xS	jako
Zakarpatská	zakarpatský	k2eAgFnSc1d1	Zakarpatská
oblast	oblast	k1gFnSc1	oblast
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
oficiálně	oficiálně	k6eAd1	oficiálně
československá	československý	k2eAgFnSc1d1	Československá
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
USSR	USSR	kA	USSR
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
OSN	OSN	kA	OSN
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
získala	získat	k5eAaPmAgFnS	získat
samostatné	samostatný	k2eAgNnSc4d1	samostatné
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
postihl	postihnout	k5eAaPmAgInS	postihnout
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
takového	takový	k3xDgMnSc4	takový
rozsahu	rozsah	k1gInSc3	rozsah
jako	jako	k9	jako
hladomor	hladomor	k1gInSc1	hladomor
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
působily	působit	k5eAaImAgInP	působit
partyzánské	partyzánský	k2eAgInPc1d1	partyzánský
oddíly	oddíl	k1gInPc1	oddíl
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
bojující	bojující	k2eAgFnSc2d1	bojující
proti	proti	k7c3	proti
sovětské	sovětský	k2eAgFnSc3d1	sovětská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1954	[number]	k4	1954
při	při	k7c6	při
příležitostí	příležitost	k1gFnPc2	příležitost
třístého	třístý	k2eAgNnSc2d1	třístý
výročí	výročí	k1gNnSc2	výročí
Perejaslavské	Perejaslavský	k2eAgFnSc2d1	Perejaslavský
rady	rada	k1gFnSc2	rada
–	–	k?	–
příklonu	příklon	k1gInSc2	příklon
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
k	k	k7c3	k
Ruské	ruský	k2eAgFnSc3d1	ruská
říši	říš	k1gFnSc3	říš
–	–	k?	–
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
N.	N.	kA	N.
S.	S.	kA	S.
Chruščova	Chruščův	k2eAgMnSc4d1	Chruščův
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
USSR	USSR	kA	USSR
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
blízké	blízký	k2eAgInPc1d1	blízký
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
vztahy	vztah	k1gInPc1	vztah
Krymské	krymský	k2eAgFnSc2d1	Krymská
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgInPc6	padesátý
letech	let	k1gInPc6	let
bylo	být	k5eAaImAgNnS	být
pokračováno	pokračován	k2eAgNnSc1d1	pokračováno
v	v	k7c6	v
předválečné	předválečný	k2eAgFnSc6d1	předválečná
politice	politika	k1gFnSc6	politika
rusifikace	rusifikace	k1gFnSc2	rusifikace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zmírnila	zmírnit	k5eAaPmAgFnS	zmírnit
během	během	k7c2	během
období	období	k1gNnSc2	období
tání	tání	k1gNnSc2	tání
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
prudce	prudko	k6eAd1	prudko
změnil	změnit	k5eAaPmAgMnS	změnit
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
většina	většina	k1gFnSc1	většina
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
předních	přední	k2eAgMnPc2d1	přední
představitelů	představitel	k1gMnPc2	představitel
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
silná	silný	k2eAgFnSc1d1	silná
industrializace	industrializace	k1gFnSc1	industrializace
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc1	urbanizace
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
kaskáda	kaskáda	k1gFnSc1	kaskáda
přehrad	přehrada	k1gFnPc2	přehrada
na	na	k7c6	na
Dněpru	Dněpr	k1gInSc6	Dněpr
<g/>
.	.	kIx.	.
</s>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
v	v	k7c6	v
Černobylské	černobylský	k2eAgFnSc6d1	Černobylská
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
V.	V.	kA	V.
I.	I.	kA	I.
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
způsobila	způsobit	k5eAaPmAgFnS	způsobit
silné	silný	k2eAgNnSc4d1	silné
zamoření	zamoření	k1gNnSc4	zamoření
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
ukrajinsko-běloruském	ukrajinskoěloruský	k2eAgNnSc6d1	ukrajinsko-běloruský
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
byla	být	k5eAaImAgFnS	být
vysídlena	vysídlit	k5eAaPmNgFnS	vysídlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Disidentské	disidentský	k2eAgNnSc1d1	disidentské
hnutí	hnutí	k1gNnSc1	hnutí
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
disidentskou	disidentský	k2eAgFnSc7d1	disidentská
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
projevila	projevit	k5eAaPmAgFnS	projevit
byl	být	k5eAaImAgInS	být
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
dělnický	dělnický	k2eAgInSc1d1	dělnický
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
požadoval	požadovat	k5eAaImAgInS	požadovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
proces	proces	k1gInSc1	proces
se	s	k7c7	s
členy	člen	k1gInPc7	člen
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
vynesl	vynést	k5eAaPmAgInS	vynést
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
tresty	trest	k1gInPc4	trest
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
SSSR	SSSR	kA	SSSR
měla	mít	k5eAaImAgFnS	mít
každá	každý	k3xTgFnSc1	každý
svazová	svazový	k2eAgFnSc1d1	svazová
republika	republika	k1gFnSc1	republika
právo	právo	k1gNnSc4	právo
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
intelektuálů	intelektuál	k1gMnPc2	intelektuál
Ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
helsinský	helsinský	k2eAgInSc4d1	helsinský
výbor	výbor	k1gInSc4	výbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
požadoval	požadovat	k5eAaImAgMnS	požadovat
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
zavázal	zavázat	k5eAaPmAgMnS	zavázat
podepsáním	podepsání	k1gNnSc7	podepsání
Závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
aktu	akt	k1gInSc2	akt
Konference	konference	k1gFnSc1	konference
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
KGB	KGB	kA	KGB
zlikvidována	zlikvidován	k2eAgNnPc4d1	zlikvidováno
před	před	k7c7	před
Olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
neformální	formální	k2eNgFnPc1d1	neformální
politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc1d1	kulturní
organizace	organizace	k1gFnPc1	organizace
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
období	období	k1gNnSc6	období
perestojky	perestojka	k1gFnSc2	perestojka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
rehabilitováno	rehabilitován	k2eAgNnSc1d1	rehabilitováno
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
obětí	oběť	k1gFnPc2	oběť
stalinských	stalinský	k2eAgFnPc2d1	stalinská
čistek	čistka	k1gFnPc2	čistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
USSR	USSR	kA	USSR
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
poté	poté	k6eAd1	poté
zvolen	zvolen	k2eAgInSc4d1	zvolen
Leonid	Leonid	k1gInSc4	Leonid
Kravčuk	Kravčuk	k1gMnSc1	Kravčuk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
vydal	vydat	k5eAaPmAgInS	vydat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sovět	sovět	k1gInSc1	sovět
Deklaraci	deklarace	k1gFnSc4	deklarace
o	o	k7c6	o
suverenitě	suverenita	k1gFnSc6	suverenita
USSR	USSR	kA	USSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
moskevský	moskevský	k2eAgInSc4d1	moskevský
puč	puč	k1gInSc4	puč
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sovět	sovět	k1gInSc1	sovět
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	s	k7c7	s
90,3	[number]	k4	90,3
<g/>
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
samostatnost	samostatnost	k1gFnSc4	samostatnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
Referendu	referendum	k1gNnSc6	referendum
o	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
se	s	k7c7	s
70,2	[number]	k4	70,2
<g/>
%	%	kIx~	%
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Leonid	Leonida	k1gFnPc2	Leonida
Kravčuk	Kravčuk	k1gMnSc1	Kravčuk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
podepsal	podepsat	k5eAaPmAgMnS	podepsat
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
SSR	SSR	kA	SSR
a	a	k8xC	a
Ruské	ruský	k2eAgFnSc2d1	ruská
SFSR	SFSR	kA	SFSR
Bělověžské	Bělověžský	k2eAgFnSc2d1	Bělověžská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
oficiálně	oficiálně	k6eAd1	oficiálně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
Svaz	svaz	k1gInSc1	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
</s>
</p>
<p>
<s>
Hladomor	hladomor	k1gInSc1	hladomor
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
<g/>
;	;	kIx,	;
ukrajinci	ukrajinec	k1gInPc7	ukrajinec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
