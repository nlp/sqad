<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
fakultou	fakulta	k1gFnSc7	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
osmi	osm	k4xCc7	osm
katedrami	katedra	k1gFnPc7	katedra
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
výzkumným	výzkumný	k2eAgNnSc7d1	výzkumné
pracovištěm	pracoviště	k1gNnSc7	pracoviště
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
bakalářské	bakalářský	k2eAgInPc1d1	bakalářský
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
navazující	navazující	k2eAgInPc4d1	navazující
<g/>
)	)	kIx)	)
magisterské	magisterský	k2eAgInPc4d1	magisterský
a	a	k8xC	a
doktorské	doktorský	k2eAgInPc4d1	doktorský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
nebo	nebo	k8xC	nebo
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
a	a	k8xC	a
program	program	k1gInSc1	program
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
fakulta	fakulta	k1gFnSc1	fakulta
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
Komárově	komárův	k2eAgInSc6d1	komárův
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Sladkého	Sladkého	k2eAgFnSc1d1	Sladkého
13	[number]	k4	13
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestěhování	přestěhování	k1gNnSc3	přestěhování
hlavního	hlavní	k2eAgNnSc2d1	hlavní
působiště	působiště	k1gNnSc2	působiště
fakulty	fakulta	k1gFnSc2	fakulta
do	do	k7c2	do
nově	nově	k6eAd1	nově
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
Univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
kampusu	kampus	k1gInSc6	kampus
v	v	k7c6	v
Bohunicích	Bohunice	k1gFnPc6	Bohunice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Součásti	součást	k1gFnSc3	součást
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
tělocvična	tělocvična	k1gFnSc1	tělocvična
Pod	pod	k7c7	pod
Hradem	hrad	k1gInSc7	hrad
v	v	k7c6	v
Údolní	údolní	k2eAgFnSc6d1	údolní
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
atletiky	atletika	k1gFnSc2	atletika
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc2	plavání
a	a	k8xC	a
sportů	sport	k1gInPc2	sport
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
gymnastiky	gymnastika	k1gFnSc2	gymnastika
a	a	k8xC	a
úpolů	úpol	k1gInPc2	úpol
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
kineziologie	kineziologie	k1gFnSc2	kineziologie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
pedagogiky	pedagogika	k1gFnSc2	pedagogika
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
podpory	podpora	k1gFnSc2	podpora
zdraví	zdraví	k1gNnSc2	zdraví
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
managementu	management	k1gInSc2	management
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
sportovních	sportovní	k2eAgFnPc2d1	sportovní
her	hra	k1gFnPc2	hra
</s>
</p>
<p>
<s>
===	===	k?	===
Výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
pracoviště	pracoviště	k1gNnPc1	pracoviště
===	===	k?	===
</s>
</p>
<p>
<s>
Laboratoř	laboratoř	k1gFnSc1	laboratoř
kinantropologického	kinantropologický	k2eAgInSc2d1	kinantropologický
výzkumu	výzkum	k1gInSc2	výzkum
</s>
</p>
<p>
<s>
==	==	k?	==
Logo	logo	k1gNnSc1	logo
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
320	[number]	k4	320
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děkanát	děkanát	k1gInSc4	děkanát
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkana	děkan	k1gMnSc4	děkan
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
fakultního	fakultní	k2eAgInSc2d1	fakultní
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektor	rektor	k1gMnSc1	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc1d1	čtyřleté
a	a	k8xC	a
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
děkanem	děkan	k1gMnSc7	děkan
nejvýše	nejvýše	k6eAd1	nejvýše
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
proděkany	proděkan	k1gMnPc4	proděkan
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fakulta	fakulta	k1gFnSc1	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
fakulty	fakulta	k1gFnSc2	fakulta
</s>
</p>
<p>
<s>
Aktuality	aktualita	k1gFnPc1	aktualita
z	z	k7c2	z
FSpS	FSpS	k1gFnSc2	FSpS
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
portálu	portál	k1gInSc6	portál
MU	MU	kA	MU
</s>
</p>
