<s>
Monty	Mont	k1gInPc1	Mont
Pythonův	Pythonův	k2eAgInSc1d1	Pythonův
létající	létající	k2eAgInSc4d1	létající
cirkus	cirkus	k1gInSc4	cirkus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Monty	Monta	k1gMnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flying	Flying	k1gInSc1	Flying
Circus	Circus	k1gInSc1	Circus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
skečů	skeč	k1gInPc2	skeč
slavné	slavný	k2eAgFnSc2d1	slavná
britské	britský	k2eAgFnSc2d1	britská
parodistické	parodistický	k2eAgFnSc2d1	parodistická
a	a	k8xC	a
satirické	satirický	k2eAgFnSc2d1	satirická
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
45	[number]	k4	45
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
sérií	série	k1gFnPc2	série
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
díly	díl	k1gInPc1	díl
nebyly	být	k5eNaImAgInP	být
natočeny	natočen	k2eAgInPc1d1	natočen
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yQgInSc6	jaký
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Argo	Argo	k6eAd1	Argo
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
kniha	kniha	k1gFnSc1	kniha
Nic	nic	k6eAd1	nic
než	než	k8xS	než
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
přepsány	přepsán	k2eAgInPc4d1	přepsán
scénáře	scénář	k1gInPc4	scénář
všech	všecek	k3xTgInPc2	všecek
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
animovaných	animovaný	k2eAgFnPc2d1	animovaná
sekvencí	sekvence	k1gFnPc2	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
"	"	kIx"	"
<g/>
dvorní	dvorní	k2eAgMnSc1d1	dvorní
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
"	"	kIx"	"
Monty	Monta	k1gFnPc1	Monta
Pythonů	Python	k1gMnPc2	Python
Petr	Petr	k1gMnSc1	Petr
Palouš	Palouš	k1gMnSc1	Palouš
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
epizod	epizoda	k1gFnPc2	epizoda
jsou	být	k5eAaImIp3nP	být
převzaty	převzít	k5eAaPmNgInP	převzít
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Natáčena	natáčen	k2eAgFnSc1d1	natáčena
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Vysílána	vysílán	k2eAgFnSc1d1	vysílána
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1969	[number]	k4	1969
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
-	-	kIx~	-
Kdeže	kdeže	k9	kdeže
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
?	?	kIx.	?
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
-	-	kIx~	-
Sex	sex	k1gInSc1	sex
a	a	k8xC	a
násilí	násilí	k1gNnSc1	násilí
Tři	tři	k4xCgNnPc1	tři
-	-	kIx~	-
Jak	jak	k8xC	jak
poměrně	poměrně	k6eAd1	poměrně
zdaleka	zdaleka	k6eAd1	zdaleka
rozeznat	rozeznat	k5eAaPmF	rozeznat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
stromů	strom	k1gInPc2	strom
Čtyři	čtyři	k4xCgMnPc1	čtyři
-	-	kIx~	-
Čas	čas	k1gInSc4	čas
natáhnout	natáhnout	k5eAaPmF	natáhnout
sůvy	sůva	k1gFnPc4	sůva
Pět	pět	k4xCc4	pět
-	-	kIx~	-
Krize	krize	k1gFnSc1	krize
lidské	lidský	k2eAgFnSc2d1	lidská
identity	identita	k1gFnSc2	identita
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
Šest	šest	k4xCc1	šest
-	-	kIx~	-
BBC	BBC	kA	BBC
se	se	k3xPyFc4	se
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
budapešťskou	budapešťský	k2eAgFnSc4d1	Budapešťská
zinkovou	zinkový	k2eAgFnSc4d1	zinková
kolčavu	kolčava	k1gFnSc4	kolčava
Sedm	sedm	k4xCc1	sedm
-	-	kIx~	-
S	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
chcíp	chcíp	k1gInSc1	chcíp
pes	pes	k1gMnSc1	pes
Osm	osm	k4xCc1	osm
-	-	kIx~	-
Plnočelná	Plnočelný	k2eAgFnSc1d1	Plnočelný
nahota	nahota	k1gFnSc1	nahota
Devět	devět	k4xCc1	devět
-	-	kIx~	-
Mravenec	mravenec	k1gMnSc1	mravenec
<g/>
,	,	kIx,	,
úvod	úvod	k1gInSc1	úvod
Deset	deset	k4xCc4	deset
-	-	kIx~	-
Rolička	rolička	k1gFnSc1	rolička
ve	v	k7c6	v
skeči	skeč	k1gInSc6	skeč
Jedenáct	jedenáct	k4xCc1	jedenáct
-	-	kIx~	-
Královská	královský	k2eAgFnSc1d1	královská
filharmonie	filharmonie	k1gFnSc1	filharmonie
na	na	k7c6	na
záchodě	záchod	k1gInSc6	záchod
Dvanáct	dvanáct	k4xCc1	dvanáct
-	-	kIx~	-
Hradlo	hradlo	k1gNnSc1	hradlo
poblíž	poblíž	k7c2	poblíž
Hove	Hov	k1gInSc2	Hov
Třináct	třináct	k4xCc4	třináct
-	-	kIx~	-
Přestávka	přestávka	k1gFnSc1	přestávka
Natáčena	natáčen	k2eAgFnSc1d1	natáčena
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Vysílána	vysílán	k2eAgFnSc1d1	vysílána
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1970	[number]	k4	1970
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc1	čtrnáct
-	-	kIx~	-
Dinsdale	Dinsdal	k1gMnSc5	Dinsdal
Patnáct	patnáct	k4xCc1	patnáct
-	-	kIx~	-
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
Šestnáct	šestnáct	k4xCc1	šestnáct
-	-	kIx~	-
Show	show	k1gNnSc4	show
5	[number]	k4	5
Sedmnáct	sedmnáct	k4xCc1	sedmnáct
-	-	kIx~	-
Show	show	k1gNnSc3	show
Buzze	Buzze	k1gFnSc2	Buzze
Aldrina	Aldrino	k1gNnSc2	Aldrino
Osmnáct	osmnáct	k4xCc4	osmnáct
-	-	kIx~	-
Živě	živě	k6eAd1	živě
z	z	k7c2	z
bufetu	bufet	k1gInSc2	bufet
Grillomat	Grillomat	k1gInSc1	Grillomat
Devatenáct	devatenáct	k4xCc1	devatenáct
-	-	kIx~	-
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
Dvacet	dvacet	k4xCc4	dvacet
-	-	kIx~	-
Show	show	k1gFnSc1	show
Attily	Attila	k1gMnSc2	Attila
Huna	Hun	k1gMnSc2	Hun
Dvacet	dvacet	k4xCc4	dvacet
jedna	jeden	k4xCgFnSc1	jeden
-	-	kIx~	-
Archeologie	archeologie	k1gFnSc1	archeologie
dnes	dnes	k6eAd1	dnes
Dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgMnPc1	dva
-	-	kIx~	-
Jak	jak	k9	jak
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
rozeznat	rozeznat	k5eAaPmF	rozeznat
různé	různý	k2eAgFnSc2d1	různá
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
Dvacet	dvacet	k4xCc4	dvacet
tři	tři	k4xCgMnPc1	tři
-	-	kIx~	-
Scott	Scott	k1gMnSc1	Scott
z	z	k7c2	z
Antarktidy	Antarktida	k1gFnSc2	Antarktida
Dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgMnPc1	čtyři
-	-	kIx~	-
Jak	jak	k9	jak
nebýt	být	k5eNaImF	být
spatřen	spatřen	k2eAgInSc4d1	spatřen
Dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
-	-	kIx~	-
Prejt	prejt	k1gInSc1	prejt
Dvacet	dvacet	k4xCc1	dvacet
šest	šest	k4xCc1	šest
-	-	kIx~	-
Královská	královský	k2eAgFnSc1d1	královská
epizoda	epizoda	k1gFnSc1	epizoda
13	[number]	k4	13
Natáčena	natáčet	k5eAaImNgFnS	natáčet
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Vysílána	vysílán	k2eAgFnSc1d1	vysílána
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1972	[number]	k4	1972
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Dvacet	dvacet	k4xCc4	dvacet
sedm	sedm	k4xCc4	sedm
-	-	kIx~	-
Whickerův	Whickerův	k2eAgInSc1d1	Whickerův
svět	svět	k1gInSc1	svět
Dvacet	dvacet	k4xCc4	dvacet
osm	osm	k4xCc1	osm
-	-	kIx~	-
Ford	ford	k1gInSc1	ford
Popular	Populara	k1gFnPc2	Populara
manželů	manžel	k1gMnPc2	manžel
Norrisových	Norrisový	k2eAgMnPc2d1	Norrisový
Dvacet	dvacet	k4xCc1	dvacet
devět	devět	k4xCc1	devět
-	-	kIx~	-
Pořad	pořad	k1gInSc4	pořad
peníze	peníz	k1gInPc4	peníz
Třicet	třicet	k4xCc4	třicet
-	-	kIx~	-
Krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
zkáza	zkáza	k1gFnSc1	zkáza
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
a	a	k8xC	a
děs	děs	k1gInSc1	děs
Třicet	třicet	k4xCc1	třicet
jedna	jeden	k4xCgFnSc1	jeden
-	-	kIx~	-
Celoanglická	Celoanglický	k2eAgFnSc1d1	Celoanglický
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
shrnování	shrnování	k1gNnSc6	shrnování
Prousta	Proust	k1gMnSc2	Proust
Třicet	třicet	k4xCc1	třicet
dva	dva	k4xCgMnPc1	dva
-	-	kIx~	-
Válka	Válka	k1gMnSc1	Válka
proti	proti	k7c3	proti
pornografii	pornografie	k1gFnSc3	pornografie
Třicet	třicet	k4xCc1	třicet
tři	tři	k4xCgInPc4	tři
-	-	kIx~	-
Salátové	salátový	k2eAgInPc4d1	salátový
dny	den	k1gInPc4	den
Třicet	třicet	k4xCc1	třicet
čtyři	čtyři	k4xCgMnPc4	čtyři
-	-	kIx~	-
Cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
výprava	výprava	k1gFnSc1	výprava
Třicet	třicet	k4xCc1	třicet
pět	pět	k4xCc1	pět
-	-	kIx~	-
Nudista	nudista	k1gMnSc1	nudista
Třicet	třicet	k4xCc1	třicet
šest	šest	k4xCc4	šest
-	-	kIx~	-
Nemoc	nemoc	k1gFnSc1	nemoc
E.	E.	kA	E.
Henryho	Henry	k1gMnSc2	Henry
Thripshaw	Thripshaw	k1gMnSc2	Thripshaw
Třicet	třicet	k4xCc1	třicet
sedm	sedm	k4xCc4	sedm
-	-	kIx~	-
Dennis	Dennis	k1gFnSc1	Dennis
Moore	Moor	k1gInSc5	Moor
Třicet	třicet	k4xCc1	třicet
osm	osm	k4xCc4	osm
-	-	kIx~	-
Četba	četba	k1gFnSc1	četba
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
Třicet	třicet	k4xCc1	třicet
devět	devět	k4xCc4	devět
-	-	kIx~	-
Studio	studio	k1gNnSc1	studio
Sport	sport	k1gInSc1	sport
Natáčena	natáčen	k2eAgMnSc4d1	natáčen
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1974	[number]	k4	1974
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Vysílána	vysílán	k2eAgFnSc1d1	vysílána
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1974	[number]	k4	1974
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Nehraje	hrát	k5eNaImIp3nS	hrát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
John	John	k1gMnSc1	John
Cleese	Cleesa	k1gFnSc3	Cleesa
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
že	že	k8xS	že
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
originální	originální	k2eAgFnSc1d1	originální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
několika	několik	k4yIc6	několik
epizodách	epizoda	k1gFnPc6	epizoda
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
-	-	kIx~	-
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
balonů	balon	k1gInPc2	balon
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
jedna	jeden	k4xCgFnSc1	jeden
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Ellis	Ellis	k1gFnPc2	Ellis
Čtyřicet	čtyřicet	k4xCc4	čtyřicet
dva	dva	k4xCgMnPc1	dva
-	-	kIx~	-
Válka	Válka	k1gMnSc1	Válka
lehké	lehký	k2eAgFnSc2d1	lehká
zábavy	zábava	k1gFnSc2	zábava
Čtyřicet	čtyřicet	k4xCc4	čtyřicet
tři	tři	k4xCgMnPc1	tři
-	-	kIx~	-
Hamlet	Hamlet	k1gMnSc1	Hamlet
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
čtyři	čtyři	k4xCgNnPc1	čtyři
-	-	kIx~	-
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Neutron	neutron	k1gInSc1	neutron
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
pět	pět	k4xCc4	pět
-	-	kIx~	-
Stranicko-politické	stranickoolitický	k2eAgNnSc4d1	stranicko-politický
vysílání	vysílání	k1gNnSc4	vysílání
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
a	a	k8xC	a
1972	[number]	k4	1972
byly	být	k5eAaImAgFnP	být
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
a	a	k8xC	a
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
televizi	televize	k1gFnSc4	televize
(	(	kIx(	(
<g/>
WDR	WDR	kA	WDR
<g/>
,	,	kIx,	,
ORF	ORF	kA	ORF
<g/>
)	)	kIx)	)
natočeny	natočen	k2eAgFnPc4d1	natočena
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
Alfreda	Alfred	k1gMnSc2	Alfred
Biolka	Biolek	k1gMnSc2	Biolek
dvě	dva	k4xCgFnPc4	dva
speciální	speciální	k2eAgFnPc4d1	speciální
epizody	epizoda	k1gFnPc4	epizoda
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Monty	Monta	k1gFnSc2	Monta
Pythons	Pythons	k1gInSc1	Pythons
fliegender	fliegender	k1gMnSc1	fliegender
Zirkus	Zirkus	k1gMnSc1	Zirkus
(	(	kIx(	(
<g/>
premiérově	premiérově	k6eAd1	premiérově
vysílány	vysílat	k5eAaImNgInP	vysílat
na	na	k7c4	na
ARD	ARD	kA	ARD
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
znění	znění	k1gNnSc1	znění
první	první	k4xOgInSc4	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
německé	německý	k2eAgNnSc1d1	německé
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
německy	německy	k6eAd1	německy
neuměl	umět	k5eNaImAgMnS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
skečů	skeč	k1gInPc2	skeč
je	být	k5eAaImIp3nS	být
originálních	originální	k2eAgNnPc2d1	originální
<g/>
,	,	kIx,	,
scénáře	scénář	k1gInPc1	scénář
některých	některý	k3yIgFnPc2	některý
byly	být	k5eAaImAgFnP	být
převzaty	převzít	k5eAaPmNgFnP	převzít
ze	z	k7c2	z
satirické	satirický	k2eAgFnSc2d1	satirická
At	At	k1gFnSc2	At
Last	Last	k1gInSc1	Last
the	the	k?	the
1948	[number]	k4	1948
Show	show	k1gFnSc6	show
na	na	k7c4	na
ITV	ITV	kA	ITV
(	(	kIx(	(
<g/>
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
Graham	Graham	k1gMnSc1	Graham
Chapman	Chapman	k1gMnSc1	Chapman
a	a	k8xC	a
John	John	k1gMnSc1	John
Cleese	Cleese	k1gFnSc2	Cleese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
remakům	remak	k1gInPc3	remak
původních	původní	k2eAgFnPc2d1	původní
epizod	epizoda	k1gFnPc2	epizoda
patří	patřit	k5eAaImIp3nS	patřit
nový	nový	k2eAgInSc1d1	nový
Lumberjack	Lumberjack	k1gInSc1	Lumberjack
Song	song	k1gInSc1	song
a	a	k8xC	a
Ten	ten	k3xDgInSc1	ten
Seconds	Seconds	k1gInSc1	Seconds
of	of	k?	of
Sex	sex	k1gInSc1	sex
(	(	kIx(	(
<g/>
remake	remake	k1gInSc1	remake
původního	původní	k2eAgInSc2d1	původní
skeče	skeč	k1gInSc2	skeč
Nude	Nude	k1gFnSc1	Nude
Organist	Organist	k1gMnSc1	Organist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nečeká	čekat	k5eNaImIp3nS	čekat
španělskou	španělský	k2eAgFnSc4d1	španělská
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
teď	teď	k6eAd1	teď
něco	něco	k3yInSc4	něco
úplně	úplně	k6eAd1	úplně
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
magnetofonem	magnetofon	k1gInSc7	magnetofon
v	v	k7c6	v
nose	nos	k1gInSc6	nos
<g/>
!	!	kIx.	!
</s>
<s>
Mé	můj	k3xOp1gNnSc1	můj
vznášedlo	vznášedlo	k1gNnSc1	vznášedlo
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgNnSc1d1	plné
úhořů	úhoř	k1gMnPc2	úhoř
<g/>
.	.	kIx.	.
</s>
<s>
Wenn	Wenn	k1gInSc1	Wenn
ist	ist	k?	ist
das	das	k?	das
Nunstück	Nunstück	k1gInSc1	Nunstück
git	git	k?	git
und	und	k?	und
Slotermeyer	Slotermeyer	k1gInSc1	Slotermeyer
<g/>
?	?	kIx.	?
</s>
<s>
Ja	Ja	k?	Ja
<g/>
!	!	kIx.	!
</s>
<s>
...	...	k?	...
Beiherhund	Beiherhunda	k1gFnPc2	Beiherhunda
das	das	k?	das
Oder	Odra	k1gFnPc2	Odra
die	die	k?	die
Flipperwaldt	Flipperwaldt	k1gMnSc1	Flipperwaldt
gersput	gersput	k1gMnSc1	gersput
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
jako	jako	k8xS	jako
letecká	letecký	k2eAgFnSc1d1	letecká
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Dejte	dát	k5eAaPmRp2nP	dát
mi	já	k3xPp1nSc3	já
váš	váš	k3xOp2gInSc1	váš
lupinus	lupinus	k1gInSc1	lupinus
<g/>
!	!	kIx.	!
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zaměříme	zaměřit	k5eAaPmIp1nP	zaměřit
na	na	k7c4	na
fenomén	fenomén	k1gNnSc4	fenomén
jménem	jméno	k1gNnSc7	jméno
deja	dej	k1gInSc2	dej
vu	vu	k?	vu
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgFnPc1	samý
už	už	k6eAd1	už
dřív	dříve	k6eAd2	dříve
prožili	prožít	k5eAaPmAgMnP	prožít
<g/>
...	...	k?	...
<g/>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zaměříme	zaměřit	k5eAaPmIp1nP	zaměřit
na	na	k7c4	na
fenomén	fenomén	k1gNnSc4	fenomén
jménem	jméno	k1gNnSc7	jméno
deja	dej	k1gInSc2	dej
vu	vu	k?	vu
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgFnPc1	samý
už	už	k6eAd1	už
dřív	dříve	k6eAd2	dříve
prožili	prožít	k5eAaPmAgMnP	prožít
<g/>
...	...	k?	...
<g/>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zaměříme	zaměřit	k5eAaPmIp1nP	zaměřit
na	na	k7c4	na
fenomén	fenomén	k1gNnSc4	fenomén
jménem	jméno	k1gNnSc7	jméno
deja	dej	k1gInSc2	dej
vu	vu	k?	vu
<g/>
...	...	k?	...
Nesmíte	smět	k5eNaImIp2nP	smět
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
říct	říct	k5eAaPmF	říct
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
matrace	matrace	k1gFnSc1	matrace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
dá	dát	k5eAaPmIp3nS	dát
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
papírový	papírový	k2eAgInSc4d1	papírový
pytlík	pytlík	k1gInSc4	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
Omluvte	omluvit	k5eAaPmRp2nP	omluvit
prosím	prosit	k5eAaImIp1nS	prosit
mou	můj	k3xOp1gFnSc4	můj
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
<g/>
,	,	kIx,	,
hloupá	hloupý	k2eAgFnSc1d1	hloupá
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
<g/>
,	,	kIx,	,
nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
<g/>
,	,	kIx,	,
nechutná	chutný	k2eNgFnSc1d1	nechutná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
-	-	kIx~	-
(	(	kIx(	(
<g/>
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pauza	pauza	k1gFnSc1	pauza
<g/>
)	)	kIx)	)
-	-	kIx~	-
promiňte	prominout	k5eAaPmRp2nP	prominout
<g/>
,	,	kIx,	,
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
jsem	být	k5eAaImIp1nS	být
nepřišel	přijít	k5eNaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
vaše	váš	k3xOp2gFnSc1	váš
žena	žena	k1gFnSc1	žena
<g/>
?	?	kIx.	?
</s>
<s>
Ráda	rád	k2eAgFnSc1d1	ráda
fotečky	fotečka	k1gFnPc1	fotečka
<g/>
?	?	kIx.	?
</s>
<s>
Drc	drc	k1gInSc1	drc
drc	drc	k1gInSc1	drc
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
kolčavou	kolčava	k1gFnSc7	kolčava
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Conrad	Conrad	k1gInSc1	Conrad
Pooh	Pooh	k1gInSc1	Pooh
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
tančící	tančící	k2eAgInPc4d1	tančící
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
natočil	natočit	k5eAaBmAgMnS	natočit
svůj	svůj	k3xOyFgInSc4	svůj
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
film	film	k1gInSc4	film
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
o	o	k7c6	o
stárnoucí	stárnoucí	k2eAgFnSc6d1	stárnoucí
buzně	buzna	k1gFnSc6	buzna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jede	jet	k5eAaImIp3nS	jet
chcípnout	chcípnout	k5eAaPmF	chcípnout
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
libru	libra	k1gFnSc4	libra
nezkazím	zkazit	k5eNaPmIp1nS	zkazit
pointu	pointa	k1gFnSc4	pointa
tohoto	tento	k3xDgInSc2	tento
skeče	skeč	k1gInSc2	skeč
<g/>
!	!	kIx.	!
</s>
<s>
Vítejte	vítat	k5eAaImRp2nP	vítat
u	u	k7c2	u
hry	hra	k1gFnSc2	hra
Picni	picnout	k5eAaPmRp2nS	picnout
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
buznu	buzna	k1gFnSc4	buzna
<g/>
!	!	kIx.	!
</s>
