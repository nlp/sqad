<s>
Červená	červený	k2eAgFnSc1d1	červená
krvinka	krvinka	k1gFnSc1	krvinka
čili	čili	k8xC	čili
erytrocyt	erytrocyt	k1gInSc1	erytrocyt
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
erythros	erythrosa	k1gFnPc2	erythrosa
–	–	k?	–
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
kytos	kytos	k1gInSc1	kytos
–	–	k?	–
buňka	buňka	k1gFnSc1	buňka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
krevní	krevní	k2eAgFnSc1d1	krevní
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
přenášení	přenášení	k1gNnSc4	přenášení
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
nebo	nebo	k8xC	nebo
žaber	žábry	k1gFnPc2	žábry
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
tělních	tělní	k2eAgFnPc2d1	tělní
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
červené	červený	k2eAgNnSc4d1	červené
krevní	krevní	k2eAgNnSc4d1	krevní
barvivo	barvivo	k1gNnSc4	barvivo
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
železité	železitý	k2eAgFnSc2d1	železitá
složky	složka	k1gFnSc2	složka
(	(	kIx(	(
<g/>
Hemu	hem	k1gInSc2	hem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
váže	vázat	k5eAaImIp3nS	vázat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
z	z	k7c2	z
bílkovinné	bílkovinný	k2eAgFnSc2d1	bílkovinná
složky	složka	k1gFnSc2	složka
(	(	kIx(	(
<g/>
globinu	globina	k1gFnSc4	globina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
krvinka	krvinka	k1gFnSc1	krvinka
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
100-120	[number]	k4	100-120
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
již	již	k9	již
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
definujeme	definovat	k5eAaBmIp1nP	definovat
jako	jako	k9	jako
buňky	buňka	k1gFnPc1	buňka
obsahující	obsahující	k2eAgFnPc1d1	obsahující
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
a	a	k8xC	a
sloužící	sloužící	k1gFnSc4	sloužící
k	k	k7c3	k
zachytávání	zachytávání	k1gNnSc3	zachytávání
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
již	již	k6eAd1	již
v	v	k7c6	v
tkáni	tkáň	k1gFnSc6	tkáň
některých	některý	k3yIgInPc2	některý
helmintů	helmint	k1gInPc2	helmint
(	(	kIx(	(
<g/>
hlavatci	hlavatec	k1gInPc7	hlavatec
<g/>
,	,	kIx,	,
Priapulida	Priapulida	k1gFnSc1	Priapulida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ještě	ještě	k6eAd1	ještě
nemají	mít	k5eNaImIp3nP	mít
cévní	cévní	k2eAgFnSc4d1	cévní
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
měkkýšů	měkkýš	k1gMnPc2	měkkýš
obvykle	obvykle	k6eAd1	obvykle
červené	červený	k2eAgFnSc2d1	červená
krvinky	krvinka	k1gFnSc2	krvinka
chybí	chybit	k5eAaPmIp3nS	chybit
a	a	k8xC	a
krevní	krevní	k2eAgNnPc1d1	krevní
barviva	barvivo	k1gNnPc1	barvivo
jsou	být	k5eAaImIp3nP	být
rozpuštěna	rozpuštěn	k2eAgNnPc1d1	rozpuštěno
v	v	k7c6	v
hemolymfě	hemolymfa	k1gFnSc6	hemolymfa
<g/>
.	.	kIx.	.
</s>
<s>
Kroužkovci	kroužkovec	k1gMnPc1	kroužkovec
(	(	kIx(	(
<g/>
s	s	k7c7	s
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
cévní	cévní	k2eAgFnSc7d1	cévní
soustavou	soustava	k1gFnSc7	soustava
<g/>
)	)	kIx)	)
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
buď	buď	k8xC	buď
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
nebyly	být	k5eNaImAgFnP	být
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
nikdy	nikdy	k6eAd1	nikdy
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgMnPc1d2	nižší
strunatci	strunatec	k1gMnPc1	strunatec
obvykle	obvykle	k6eAd1	obvykle
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
také	také	k9	také
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
erytrocyty	erytrocyt	k1gInPc1	erytrocyt
oválné	oválný	k2eAgInPc1d1	oválný
s	s	k7c7	s
vyvinutým	vyvinutý	k2eAgNnSc7d1	vyvinuté
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
erytrocyty	erytrocyt	k1gInPc7	erytrocyt
bez	bez	k7c2	bez
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
bikonkávního	bikonkávní	k2eAgInSc2d1	bikonkávní
tvaru	tvar	k1gInSc2	tvar
–	–	k?	–
u	u	k7c2	u
velbloudovitých	velbloudovitý	k2eAgInPc2d1	velbloudovitý
však	však	k8xC	však
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgFnPc1d1	oválná
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
4	[number]	k4	4
mikrometrů	mikrometr	k1gInPc2	mikrometr
u	u	k7c2	u
kozy	koza	k1gFnSc2	koza
až	až	k9	až
po	po	k7c4	po
9,1	[number]	k4	9,1
mikrometru	mikrometr	k1gInSc2	mikrometr
u	u	k7c2	u
slona	slon	k1gMnSc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
mm3	mm3	k4	mm3
bývá	bývat	k5eAaImIp3nS	bývat
např.	např.	kA	např.
u	u	k7c2	u
člověka	člověk	k1gMnSc4	člověk
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
u	u	k7c2	u
holubů	holub	k1gMnPc2	holub
však	však	k9	však
pouhých	pouhý	k2eAgInPc2d1	pouhý
2	[number]	k4	2
400	[number]	k4	400
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
až	až	k6eAd1	až
6	[number]	k4	6
650	[number]	k4	650
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
lidských	lidský	k2eAgFnPc2d1	lidská
buněk	buňka	k1gFnPc2	buňka
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gInSc1	jejich
rozměr	rozměr	k1gInSc1	rozměr
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
7,4	[number]	k4	7,4
<g/>
×	×	k?	×
<g/>
2,1	[number]	k4	2,1
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
červená	červený	k2eAgFnSc1d1	červená
krvinka	krvinka	k1gFnSc1	krvinka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
265	[number]	k4	265
milionů	milion	k4xCgInPc2	milion
molekul	molekula	k1gFnPc2	molekula
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgFnPc4	čtyři
hemové	hemové	k2eAgFnPc4d1	hemové
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
mají	mít	k5eAaImIp3nP	mít
kolem	kolem	k7c2	kolem
4,8	[number]	k4	4,8
milionů	milion	k4xCgInPc2	milion
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c4	v
1	[number]	k4	1
mm3	mm3	k4	mm3
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
objemu	objem	k1gInSc6	objem
asi	asi	k9	asi
5,4	[number]	k4	5,4
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
trvale	trvale	k6eAd1	trvale
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
atmosferického	atmosferický	k2eAgInSc2d1	atmosferický
kyslíku	kyslík	k1gInSc2	kyslík
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
nížin	nížina	k1gFnPc2	nížina
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
množství	množství	k1gNnSc4	množství
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
až	až	k9	až
o	o	k7c4	o
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
složkou	složka	k1gFnSc7	složka
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
1	[number]	k4	1
mm3	mm3	k4	mm3
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
až	až	k9	až
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
a	a	k8xC	a
150	[number]	k4	150
až	až	k9	až
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
vázány	vázán	k2eAgInPc1d1	vázán
cca	cca	kA	cca
3,5	[number]	k4	3,5
g	g	kA	g
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zbývajících	zbývající	k2eAgFnPc6d1	zbývající
tkáních	tkáň	k1gFnPc6	tkáň
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
#	#	kIx~	#
<g/>
Erytropoéza	Erytropoéza	k1gFnSc1	Erytropoéza
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
nemají	mít	k5eNaImIp3nP	mít
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
nedovedou	dovést	k5eNaPmIp3nP	dovést
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc4	sám
dělit	dělit	k5eAaImF	dělit
a	a	k8xC	a
množit	množit	k5eAaImF	množit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
plochých	plochý	k2eAgFnPc2d1	plochá
kostí	kost	k1gFnPc2	kost
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
erytropoéza	erytropoéza	k1gFnSc1	erytropoéza
(	(	kIx(	(
<g/>
v	v	k7c6	v
embryu	embryo	k1gNnSc6	embryo
probíhá	probíhat	k5eAaImIp3nS	probíhat
erytropoéza	erytropoéza	k1gFnSc1	erytropoéza
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Erytropoéza	Erytropoéza	k1gFnSc1	Erytropoéza
je	být	k5eAaImIp3nS	být
stimulována	stimulovat	k5eAaImNgFnS	stimulovat
hormonem	hormon	k1gInSc7	hormon
erytropoetinem	erytropoetin	k1gInSc7	erytropoetin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
červené	červený	k2eAgFnSc2d1	červená
krvinky	krvinka	k1gFnSc2	krvinka
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
životnost	životnost	k1gFnSc1	životnost
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
erytroblastů	erytroblast	k1gInPc2	erytroblast
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
vypuzeno	vypuzen	k2eAgNnSc4d1	vypuzeno
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
membráně	membrána	k1gFnSc6	membrána
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
fosfatidylserin	fosfatidylserin	k1gInSc4	fosfatidylserin
a	a	k8xC	a
láká	lákat	k5eAaImIp3nS	lákat
makrofágy	makrofág	k1gInPc4	makrofág
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pomocí	pomocí	k7c2	pomocí
DNáz	DNáza	k1gFnPc2	DNáza
buněčné	buněčný	k2eAgNnSc1d1	buněčné
jádro	jádro	k1gNnSc1	jádro
stráví	strávit	k5eAaPmIp3nS	strávit
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
či	či	k8xC	či
poškozené	poškozený	k2eAgFnPc1d1	poškozená
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
obklopeny	obklopen	k2eAgInPc1d1	obklopen
fagocyty	fagocyt	k1gInPc1	fagocyt
<g/>
,	,	kIx,	,
odbourány	odbourán	k2eAgFnPc1d1	odbourána
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
odstraňovány	odstraňovat	k5eAaImNgFnP	odstraňovat
především	především	k9	především
ve	v	k7c6	v
slezině	slezina	k1gFnSc6	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Hem	hem	k1gInSc1	hem
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
je	být	k5eAaImIp3nS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
jako	jako	k9	jako
bilirubin	bilirubin	k1gInSc1	bilirubin
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
sportovci	sportovec	k1gMnPc1	sportovec
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dodáním	dodání	k1gNnSc7	dodání
erytropoetinu	erytropoetin	k2eAgInSc3d1	erytropoetin
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
produkci	produkce	k1gFnSc4	produkce
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
zvýšení	zvýšení	k1gNnSc2	zvýšení
počtu	počet	k1gInSc2	počet
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
je	být	k5eAaImIp3nS	být
autotransfúze	autotransfúze	k1gFnSc1	autotransfúze
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
sportovcům	sportovec	k1gMnPc3	sportovec
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
vpraveny	vpravit	k5eAaPmNgFnP	vpravit
jejich	jejich	k3xOp3gFnPc1	jejich
vlastní	vlastní	k2eAgFnPc1d1	vlastní
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
odebrané	odebraný	k2eAgMnPc4d1	odebraný
<g/>
,	,	kIx,	,
erytrocyty	erytrocyt	k1gInPc4	erytrocyt
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
skladovány	skladován	k2eAgInPc1d1	skladován
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
počet	počet	k1gInSc1	počet
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hustotu	hustota	k1gFnSc4	hustota
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
dopujícího	dopující	k2eAgMnSc2d1	dopující
sportovce	sportovec	k1gMnSc2	sportovec
<g/>
.	.	kIx.	.
</s>
