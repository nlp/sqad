<s>
Červená	červený	k2eAgFnSc1d1	červená
krvinka	krvinka	k1gFnSc1	krvinka
čili	čili	k8xC	čili
erytrocyt	erytrocyt	k1gInSc1	erytrocyt
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
erythros	erythrosa	k1gFnPc2	erythrosa
–	–	k?	–
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
kytos	kytos	k1gInSc1	kytos
–	–	k?	–
buňka	buňka	k1gFnSc1	buňka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
krevní	krevní	k2eAgFnSc1d1	krevní
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
