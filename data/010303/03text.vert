<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
míchaný	míchaný	k2eAgInSc4d1	míchaný
nápoj	nápoj	k1gInSc4	nápoj
z	z	k7c2	z
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
cukru	cukr	k1gInSc2	cukr
se	s	k7c7	s
smetanou	smetana	k1gFnSc7	smetana
navrch	navrch	k6eAd1	navrch
<g/>
.	.	kIx.	.
</s>
<s>
Narozeniny	narozeniny	k1gFnPc1	narozeniny
slaví	slavit	k5eAaImIp3nP	slavit
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
10	[number]	k4	10
g	g	kA	g
jemně	jemně	k6eAd1	jemně
mleté	mletý	k2eAgFnSc2d1	mletá
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
2	[number]	k4	2
dl	dl	k?	dl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
2-3	[number]	k4	2-3
lžíce	lžíce	k1gFnPc4	lžíce
irské	irský	k2eAgFnSc2d1	irská
whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
smetana	smetana	k1gFnSc1	smetana
ke	k	k7c3	k
šlehání	šlehání	k1gNnSc3	šlehání
podle	podle	k7c2	podle
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dochucení	dochucení	k1gNnSc6	dochucení
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
třtinový	třtinový	k2eAgMnSc1d1	třtinový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Irská	irský	k2eAgFnSc1d1	irská
káva	káva	k1gFnSc1	káva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
-	-	kIx~	-
recepty	recept	k1gInPc1	recept
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
i	i	k8xC	i
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
pojetí	pojetí	k1gNnSc1	pojetí
irské	irský	k2eAgFnSc2d1	irská
kávy	káva	k1gFnSc2	káva
</s>
</p>
<p>
<s>
Videorecept	Videorecept	k1gInSc1	Videorecept
na	na	k7c4	na
Vimeo	Vimeo	k1gNnSc4	Vimeo
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
káva	káva	k1gFnSc1	káva
aneb	aneb	k?	aneb
jak	jak	k6eAd1	jak
připravit	připravit	k5eAaPmF	připravit
pravou	pravý	k2eAgFnSc4d1	pravá
Irskou	irský	k2eAgFnSc4d1	irská
kávu	káva	k1gFnSc4	káva
-	-	kIx~	-
cafeeternity	cafeeternita	k1gFnSc2	cafeeternita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
