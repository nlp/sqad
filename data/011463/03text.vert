<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
President	president	k1gMnSc1	president
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
POTUS	POTUS	kA	POTUS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
osobou	osoba	k1gFnSc7	osoba
federální	federální	k2eAgFnSc2d1	federální
exekutivy	exekutiva	k1gFnSc2	exekutiva
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
mezi	mezi	k7c4	mezi
prezidenta	prezident	k1gMnSc4	prezident
(	(	kIx(	(
<g/>
či	či	k8xC	či
jinou	jiný	k2eAgFnSc4d1	jiná
formální	formální	k2eAgFnSc4d1	formální
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
silné	silný	k2eAgFnSc3d1	silná
pozici	pozice	k1gFnSc3	pozice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
systému	systém	k1gInSc6	systém
–	–	k?	–
tzv.	tzv.	kA	tzv.
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
prezidencialismu	prezidencialismus	k1gInSc6	prezidencialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
striktně	striktně	k6eAd1	striktně
oddělena	oddělit	k5eAaPmNgNnP	oddělit
od	od	k7c2	od
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kongres	kongres	k1gInSc1	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
všemocný	všemocný	k2eAgMnSc1d1	všemocný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
žádné	žádný	k3yNgInPc4	žádný
exekutivní	exekutivní	k2eAgInPc4d1	exekutivní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgMnS	vybavit
úplnou	úplný	k2eAgFnSc7d1	úplná
mocí	moc	k1gFnSc7	moc
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
zákonodárných	zákonodárný	k2eAgFnPc2d1	zákonodárná
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
ústavou	ústava	k1gFnSc7	ústava
daným	daný	k2eAgMnSc7d1	daný
držitelem	držitel	k1gMnSc7	držitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
členy	člen	k1gMnPc4	člen
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
úředníky	úředník	k1gMnPc4	úředník
(	(	kIx(	(
<g/>
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
až	až	k9	až
14	[number]	k4	14
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
není	být	k5eNaImIp3nS	být
odpovědný	odpovědný	k2eAgInSc4d1	odpovědný
Kongresu	kongres	k1gInSc3	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
systému	systém	k1gInSc6	systém
neexistuje	existovat	k5eNaImIp3nS	existovat
instituce	instituce	k1gFnSc1	instituce
hlasování	hlasování	k1gNnSc2	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
<g/>
,	,	kIx,	,
zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
orgán	orgán	k1gInSc1	orgán
nemůže	moct	k5eNaImIp3nS	moct
politickou	politický	k2eAgFnSc7d1	politická
cestou	cesta	k1gFnSc7	cesta
odstranit	odstranit	k5eAaPmF	odstranit
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
ani	ani	k8xC	ani
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
zase	zase	k9	zase
nemůže	moct	k5eNaImIp3nS	moct
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
prezident	prezident	k1gMnSc1	prezident
dopustil	dopustit	k5eAaPmAgMnS	dopustit
závažného	závažný	k2eAgInSc2d1	závažný
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
či	či	k8xC	či
skutku	skutek	k1gInSc2	skutek
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
by	by	kYmCp3nS	by
kompromitoval	kompromitovat	k5eAaBmAgInS	kompromitovat
svůj	svůj	k3xOyFgInSc4	svůj
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
ctít	ctít	k5eAaImF	ctít
a	a	k8xC	a
hájit	hájit	k5eAaImF	hájit
ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
podezření	podezření	k1gNnSc2	podezření
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
skutky	skutek	k1gInPc4	skutek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
Kongres	kongres	k1gInSc1	kongres
vyvolat	vyvolat	k5eAaPmF	vyvolat
tzv.	tzv.	kA	tzv.
impeachment	impeachment	k1gInSc4	impeachment
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
formální	formální	k2eAgInSc1d1	formální
proces	proces	k1gInSc1	proces
vyšetření	vyšetření	k1gNnSc2	vyšetření
těchto	tento	k3xDgInPc2	tento
skutků	skutek	k1gInPc2	skutek
či	či	k8xC	či
podezření	podezření	k1gNnSc4	podezření
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
důsledkem	důsledek	k1gInSc7	důsledek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
odvolání	odvolání	k1gNnSc1	odvolání
prezidenta	prezident	k1gMnSc2	prezident
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
odvolání	odvolání	k1gNnSc6	odvolání
pak	pak	k6eAd1	pak
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
Senát	senát	k1gInSc1	senát
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
k	k	k7c3	k
impeachmentu	impeachment	k1gInSc3	impeachment
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Andrewa	Andrewus	k1gMnSc2	Andrewus
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
a	a	k8xC	a
schylovalo	schylovat	k5eAaImAgNnS	schylovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
svému	svůj	k3xOyFgNnSc3	svůj
odvolání	odvolání	k1gNnSc3	odvolání
předešel	předejít	k5eAaPmAgMnS	předejít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
sám	sám	k3xTgInSc4	sám
resignoval	resignovat	k5eAaBmAgInS	resignovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstatou	podstata	k1gFnSc7	podstata
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
oddělení	oddělení	k1gNnSc1	oddělení
částí	část	k1gFnPc2	část
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
neodpovědnost	neodpovědnost	k1gFnSc1	neodpovědnost
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
vůči	vůči	k7c3	vůči
zákonodárné	zákonodárný	k2eAgFnSc3d1	zákonodárná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
realizována	realizován	k2eAgNnPc1d1	realizováno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
systému	systém	k1gInSc2	systém
tzv.	tzv.	kA	tzv.
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
protivah	protiváha	k1gFnPc2	protiváha
(	(	kIx(	(
<g/>
checks	checks	k1gInSc1	checks
and	and	k?	and
balances	balances	k1gInSc1	balances
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
omezování	omezování	k1gNnSc2	omezování
tří	tři	k4xCgFnPc2	tři
mocí	moc	k1gFnPc2	moc
(	(	kIx(	(
<g/>
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
,	,	kIx,	,
výkonné	výkonný	k2eAgFnSc2d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc2d1	soudní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velmocenskému	velmocenský	k2eAgNnSc3d1	velmocenské
postavení	postavení	k1gNnSc3	postavení
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vnímán	vnímán	k2eAgInSc1d1	vnímán
jako	jako	k8xC	jako
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
osoba	osoba	k1gFnSc1	osoba
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
volební	volební	k2eAgInSc1d1	volební
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
4	[number]	k4	4
roky	rok	k1gInPc4	rok
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
např.	např.	kA	např.
od	od	k7c2	od
Česka	Česko	k1gNnSc2	Česko
toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
není	být	k5eNaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
zákaz	zákaz	k1gInSc1	zákaz
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgInSc1d1	absolutní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
bylo	být	k5eAaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
jako	jako	k8xS	jako
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
tradici	tradice	k1gFnSc4	tradice
nastolenou	nastolený	k2eAgFnSc4d1	nastolená
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Georgem	Georg	k1gInSc7	Georg
Washingtonem	Washington	k1gInSc7	Washington
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
Franklin	Franklin	k2eAgInSc1d1	Franklin
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
a	a	k8xC	a
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
vícenásobného	vícenásobný	k2eAgNnSc2d1	vícenásobné
zvolení	zvolení	k1gNnSc2	zvolení
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
XX	XX	kA	XX
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
zakotvil	zakotvit	k5eAaPmAgMnS	zakotvit
zkrácení	zkrácení	k1gNnSc4	zkrácení
přechodného	přechodný	k2eAgNnSc2d1	přechodné
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
listopadovými	listopadový	k2eAgFnPc7d1	listopadová
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
slibu	slib	k1gInSc2	slib
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
skládali	skládat	k5eAaImAgMnP	skládat
přísahu	přísaha	k1gFnSc4	přísaha
všichni	všechen	k3xTgMnPc1	všechen
nově	nově	k6eAd1	nově
zvolení	zvolený	k2eAgMnPc1d1	zvolený
prezidenti	prezident	k1gMnPc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
je	být	k5eAaImIp3nS	být
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
slib	slib	k1gInSc1	slib
skládán	skládán	k2eAgInSc1d1	skládán
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc3	úřad
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
ujme	ujmout	k5eAaPmIp3nS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
viceprezident	viceprezident	k1gMnSc1	viceprezident
volený	volený	k2eAgMnSc1d1	volený
současně	současně	k6eAd1	současně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoce	pravomoc	k1gFnPc4	pravomoc
získá	získat	k5eAaPmIp3nS	získat
po	po	k7c6	po
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
přísaze	přísaha	k1gFnSc6	přísaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
nepřímé	přímý	k2eNgFnPc1d1	nepřímá
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
sborem	sbor	k1gInSc7	sbor
volitelů	volitel	k1gMnPc2	volitel
nominovaných	nominovaný	k2eAgFnPc2d1	nominovaná
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
Unie	unie	k1gFnSc2	unie
podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
na	na	k7c6	na
principu	princip	k1gInSc6	princip
"	"	kIx"	"
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
bere	brát	k5eAaImIp3nS	brát
vše	všechen	k3xTgNnSc4	všechen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
-li	i	k?	-li
v	v	k7c6	v
dotyčném	dotyčný	k2eAgInSc6d1	dotyčný
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
všechny	všechen	k3xTgMnPc4	všechen
jeho	jeho	k3xOp3gMnPc4	jeho
volitele	volitel	k1gMnPc4	volitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
volitelů	volitel	k1gMnPc2	volitel
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
součtem	součet	k1gInSc7	součet
počtu	počet	k1gInSc2	počet
zastupitelů	zastupitel	k1gMnPc2	zastupitel
dolní	dolní	k2eAgFnSc2d1	dolní
a	a	k8xC	a
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
2	[number]	k4	2
státy	stát	k1gInPc7	stát
Nebraska	Nebrasko	k1gNnSc2	Nebrasko
a	a	k8xC	a
Maine	Main	k1gMnSc5	Main
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
hlasy	hlas	k1gInPc4	hlas
podle	podle	k7c2	podle
vítězství	vítězství	k1gNnSc2	vítězství
kandidátů	kandidát	k1gMnPc2	kandidát
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
volebních	volební	k2eAgInPc6d1	volební
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc4	způsob
volby	volba	k1gFnSc2	volba
si	se	k3xPyFc3	se
určují	určovat	k5eAaImIp3nP	určovat
samy	sám	k3xTgInPc1	sám
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
získá	získat	k5eAaPmIp3nS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
538	[number]	k4	538
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
alespoň	alespoň	k9	alespoň
270	[number]	k4	270
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
538	[number]	k4	538
volitelů	volitel	k1gMnPc2	volitel
=	=	kIx~	=
435	[number]	k4	435
volitelů	volitel	k1gMnPc2	volitel
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
členů	člen	k1gMnPc2	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
)	)	kIx)	)
+	+	kIx~	+
3	[number]	k4	3
volitelé	volitel	k1gMnPc1	volitel
za	za	k7c4	za
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
+	+	kIx~	+
100	[number]	k4	100
volitelů	volitel	k1gMnPc2	volitel
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
členů	člen	k1gMnPc2	člen
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
2	[number]	k4	2
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
stát	stát	k1gInSc4	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volitelé	volitel	k1gMnPc1	volitel
za	za	k7c4	za
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
jsou	být	k5eAaImIp3nP	být
garantováni	garantován	k2eAgMnPc1d1	garantován
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
volitelů	volitel	k1gMnPc2	volitel
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
žijí	žít	k5eAaImIp3nP	žít
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
volitelů	volitel	k1gMnPc2	volitel
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
státu	stát	k1gInSc2	stát
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
čase	čas	k1gInSc6	čas
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
(	(	kIx(	(
<g/>
k	k	k7c3	k
aktualizaci	aktualizace	k1gFnSc3	aktualizace
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrát	vyhrát	k5eAaPmF	vyhrát
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
kandidát	kandidát	k1gMnSc1	kandidát
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tito	tento	k3xDgMnPc1	tento
výhodně	výhodně	k6eAd1	výhodně
rozmístěni	rozmístěn	k2eAgMnPc1d1	rozmístěn
ve	v	k7c6	v
volebních	volební	k2eAgInPc6d1	volební
obvodech	obvod	k1gInPc6	obvod
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
státech	stát	k1gInPc6	stát
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
gerrymandering	gerrymandering	k1gInSc1	gerrymandering
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
poznaly	poznat	k5eAaPmAgInP	poznat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
méně	málo	k6eAd2	málo
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
5	[number]	k4	5
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
protikandidát	protikandidát	k1gMnSc1	protikandidát
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hillary	Hillar	k1gInPc4	Hillar
Clintonová	Clintonová	k1gFnSc1	Clintonová
porazila	porazit	k5eAaPmAgFnS	porazit
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trumpa	k1gFnSc1	Trumpa
o	o	k7c4	o
víc	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
činí	činit	k5eAaImIp3nS	činit
největší	veliký	k2eAgInSc1d3	veliký
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volebním	volební	k2eAgInSc7d1	volební
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
první	první	k4xOgNnSc4	první
úterý	úterý	k1gNnSc4	úterý
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
pondělí	pondělí	k1gNnSc6	pondělí
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
volebního	volební	k2eAgInSc2d1	volební
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
volitelé	volitel	k1gMnPc1	volitel
v	v	k7c4	v
první	první	k4xOgNnSc4	první
pondělí	pondělí	k1gNnSc4	pondělí
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
středě	středa	k1gFnSc6	středa
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
státech	stát	k1gInPc6	stát
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
lístku	lístek	k1gInSc6	lístek
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
pro	pro	k7c4	pro
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
a	a	k8xC	a
součty	součet	k1gInPc4	součet
svých	svůj	k3xOyFgInPc2	svůj
hlasů	hlas	k1gInPc2	hlas
pošlou	poslat	k5eAaPmIp3nP	poslat
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
XII	XII	kA	XII
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
společná	společný	k2eAgFnSc1d1	společná
schůze	schůze	k1gFnSc1	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
viceprezident	viceprezident	k1gMnSc1	viceprezident
USA	USA	kA	USA
<g/>
)	)	kIx)	)
přečte	přečíst	k5eAaPmIp3nS	přečíst
nahlas	nahlas	k6eAd1	nahlas
hlasy	hlas	k1gInPc4	hlas
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
budoucí	budoucí	k2eAgMnSc1d1	budoucí
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nikdo	nikdo	k3yNnSc1	nikdo
nezískal	získat	k5eNaPmAgMnS	získat
nadpoloviční	nadpoloviční	k2eAgInSc4d1	nadpoloviční
počet	počet	k1gInSc4	počet
prezidentských	prezidentský	k2eAgInPc2d1	prezidentský
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
schůzi	schůze	k1gFnSc4	schůze
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
prezidentských	prezidentský	k2eAgMnPc2d1	prezidentský
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
udělit	udělit	k5eAaPmF	udělit
1	[number]	k4	1
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nikdo	nikdo	k3yNnSc1	nikdo
nezískal	získat	k5eNaPmAgInS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
viceprezidentských	viceprezidentský	k2eAgInPc2d1	viceprezidentský
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
volil	volit	k5eAaImAgMnS	volit
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Senát	senát	k1gInSc1	senát
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
viceprezidentských	viceprezidentský	k2eAgMnPc2d1	viceprezidentský
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
události	událost	k1gFnSc3	událost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
prezidenta	prezident	k1gMnSc2	prezident
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rovnosti	rovnost	k1gFnSc3	rovnost
počtu	počet	k1gInSc2	počet
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
také	také	k9	také
volila	volit	k5eAaImAgFnS	volit
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádný	žádný	k3yNgMnSc1	žádný
kandidát	kandidát	k1gMnSc1	kandidát
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
nezískal	získat	k5eNaPmAgMnS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volitelé	volitel	k1gMnPc1	volitel
sboru	sbor	k1gInSc2	sbor
dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
nejsou	být	k5eNaImIp3nP	být
vázáni	vázat	k5eAaImNgMnP	vázat
výsledkem	výsledek	k1gInSc7	výsledek
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
volit	volit	k5eAaImF	volit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgNnSc2	svůj
svobodného	svobodný	k2eAgNnSc2d1	svobodné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
jakéhokoli	jakýkoli	k3yIgMnSc4	jakýkoli
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
občana	občan	k1gMnSc4	občan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nekandidoval	kandidovat	k5eNaImAgMnS	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
události	událost	k1gFnSc3	událost
již	již	k9	již
také	také	k9	také
výjimečně	výjimečně	k6eAd1	výjimečně
došlo	dojít	k5eAaPmAgNnS	dojít
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
řídí	řídit	k5eAaImIp3nS	řídit
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
precedenčně	precedenčně	k6eAd1	precedenčně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
tvorby	tvorba	k1gFnSc2	tvorba
zákonů	zákon	k1gInPc2	zákon
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
volitele	volitel	k1gMnPc4	volitel
volit	volit	k5eAaImF	volit
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
využily	využít	k5eAaPmAgInP	využít
této	tento	k3xDgFnSc2	tento
legislativní	legislativní	k2eAgFnSc2d1	legislativní
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
21	[number]	k4	21
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
volitelé	volitel	k1gMnPc1	volitel
nejsou	být	k5eNaImIp3nP	být
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
ničím	nic	k3yNnSc7	nic
vázáni	vázat	k5eAaImNgMnP	vázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
dějinách	dějiny	k1gFnPc6	dějiny
kvůli	kvůli	k7c3	kvůli
výjimečnosti	výjimečnost	k1gFnSc3	výjimečnost
jevu	jev	k1gInSc2	jev
odlišného	odlišný	k2eAgNnSc2d1	odlišné
hlasování	hlasování	k1gNnSc2	hlasování
volitelů	volitel	k1gMnPc2	volitel
od	od	k7c2	od
volebních	volební	k2eAgInPc2d1	volební
výsledků	výsledek	k1gInPc2	výsledek
nedošlo	dojít	k5eNaPmAgNnS	dojít
nikdy	nikdy	k6eAd1	nikdy
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
celkového	celkový	k2eAgInSc2d1	celkový
výsledku	výsledek	k1gInSc2	výsledek
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
klání	klání	k1gNnSc2	klání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
zajímavosti	zajímavost	k1gFnSc2	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chronologický	chronologický	k2eAgInSc4d1	chronologický
seznam	seznam	k1gInSc4	seznam
===	===	k?	===
</s>
</p>
<p>
<s>
1789	[number]	k4	1789
–	–	k?	–
sídlo	sídlo	k1gNnSc4	sídlo
prezidenta	prezident	k1gMnSc2	prezident
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
1790	[number]	k4	1790
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
(	(	kIx(	(
<g/>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1793	[number]	k4	1793
–	–	k?	–
George	Georg	k1gMnSc4	Georg
Washington	Washington	k1gInSc1	Washington
pronesl	pronést	k5eAaPmAgInS	pronést
vůbec	vůbec	k9	vůbec
nejkratší	krátký	k2eAgInSc1d3	nejkratší
inaugurační	inaugurační	k2eAgInSc1d1	inaugurační
projev	projev	k1gInSc1	projev
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1800	[number]	k4	1800
–	–	k?	–
dva	dva	k4xCgMnPc1	dva
kandidáti	kandidát	k1gMnPc1	kandidát
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
předchůdkyně	předchůdkyně	k1gFnSc2	předchůdkyně
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
a	a	k8xC	a
Aaron	Aaron	k1gMnSc1	Aaron
Burr	Burr	k1gMnSc1	Burr
získali	získat	k5eAaPmAgMnP	získat
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
zvolila	zvolit	k5eAaPmAgFnS	zvolit
až	až	k9	až
při	při	k7c6	při
36	[number]	k4	36
<g/>
.	.	kIx.	.
hlasování	hlasování	k1gNnSc6	hlasování
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
prezidentem	prezident	k1gMnSc7	prezident
Jeffersona	Jefferson	k1gMnSc2	Jefferson
<g/>
,	,	kIx,	,
Burr	Burr	k1gMnSc1	Burr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Bouřlivák	bouřlivák	k1gMnSc1	bouřlivák
Burr	Burr	k1gMnSc1	Burr
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úřadu	úřad	k1gInSc6	úřad
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
prvního	první	k4xOgMnSc2	první
amerického	americký	k2eAgMnSc2d1	americký
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Alexandra	Alexandr	k1gMnSc2	Alexandr
Hamiltona	Hamilton	k1gMnSc2	Hamilton
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
11	[number]	k4	11
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1804	[number]	k4	1804
a	a	k8xC	a
smrtelně	smrtelně	k6eAd1	smrtelně
ho	on	k3xPp3gMnSc4	on
postřelil	postřelit	k5eAaPmAgMnS	postřelit
<g/>
.	.	kIx.	.
</s>
<s>
Hamilton	Hamilton	k1gInSc1	Hamilton
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1824	[number]	k4	1824
–	–	k?	–
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
žádný	žádný	k3yNgInSc1	žádný
ze	z	k7c2	z
4	[number]	k4	4
kandidátů	kandidát	k1gMnPc2	kandidát
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
nezískal	získat	k5eNaPmAgMnS	získat
většinu	většina	k1gFnSc4	většina
všech	všecek	k3xTgMnPc2	všecek
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Johna	John	k1gMnSc4	John
Quincy	Quinca	k1gFnSc2	Quinca
Adamse	Adams	k1gMnSc4	Adams
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
získal	získat	k5eAaPmAgInS	získat
o	o	k7c4	o
45	[number]	k4	45
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
méně	málo	k6eAd2	málo
než	než	k8xS	než
Andrew	Andrew	k1gMnSc1	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1825	[number]	k4	1825
–	–	k?	–
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
také	také	k9	také
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
jedinými	jediné	k1gNnPc7	jediné
opětovně	opětovně	k6eAd1	opětovně
nezvolenými	zvolený	k2eNgMnPc7d1	nezvolený
prezidenty	prezident	k1gMnPc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
dvojici	dvojice	k1gFnSc4	dvojice
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
představují	představovat	k5eAaImIp3nP	představovat
George	George	k1gNnPc1	George
Herbert	Herbert	k1gMnSc1	Herbert
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
-	-	kIx~	-
93	[number]	k4	93
<g/>
)	)	kIx)	)
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
George	Georg	k1gMnSc2	Georg
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1829	[number]	k4	1829
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
demokratický	demokratický	k2eAgMnSc1d1	demokratický
prezident	prezident	k1gMnSc1	prezident
Andrew	Andrew	k1gMnSc1	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1837	[number]	k4	1837
–	–	k?	–
Martin	Martin	k2eAgInSc1d1	Martin
Van	van	k1gInSc1	van
Buren	Burna	k1gFnPc2	Burna
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
již	již	k6eAd1	již
jako	jako	k8xC	jako
americký	americký	k2eAgMnSc1d1	americký
občan	občan	k1gMnSc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jako	jako	k8xS	jako
britští	britský	k2eAgMnPc1d1	britský
kolonisté	kolonista	k1gMnPc1	kolonista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1841	[number]	k4	1841
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
stal	stát	k5eAaPmAgMnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
strávil	strávit	k5eAaPmAgMnS	strávit
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1841	[number]	k4	1841
–	–	k?	–
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
John	John	k1gMnSc1	John
Tyler	Tyler	k1gMnSc1	Tyler
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
jako	jako	k8xS	jako
Whig	whig	k1gMnSc1	whig
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
předtím	předtím	k6eAd1	předtím
přijal	přijmout	k5eAaPmAgMnS	přijmout
nominaci	nominace	k1gFnSc4	nominace
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
s	s	k7c7	s
whigy	whig	k1gMnPc7	whig
rozešel	rozejít	k5eAaPmAgMnS	rozejít
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
"	"	kIx"	"
<g/>
bezpartijní	bezpartijní	k2eAgInPc4d1	bezpartijní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1849	[number]	k4	1849
–	–	k?	–
Prezident	prezident	k1gMnSc1	prezident
Zachary	Zachara	k1gFnSc2	Zachara
Taylor	Taylor	k1gMnSc1	Taylor
použil	použít	k5eAaPmAgMnS	použít
poprvé	poprvé	k6eAd1	poprvé
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
v	v	k7c6	v
pohřební	pohřební	k2eAgFnSc6d1	pohřební
řeči	řeč	k1gFnSc6	řeč
Dolley	Dolle	k2eAgFnPc1d1	Dolle
Madisonové	Madisonová	k1gFnPc1	Madisonová
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
exprezidenta	exprezident	k1gMnSc2	exprezident
Jamese	Jamese	k1gFnSc1	Jamese
Madisona	Madisona	k1gFnSc1	Madisona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1861	[number]	k4	1861
–	–	k?	–
prvním	první	k4xOgMnSc6	první
republikánským	republikánský	k2eAgMnSc7d1	republikánský
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
hercem	herec	k1gMnSc7	herec
Johnem	John	k1gMnSc7	John
Wilkesem	Wilkes	k1gMnSc7	Wilkes
Boothem	Booth	k1gInSc7	Booth
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1865	[number]	k4	1865
–	–	k?	–
po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Lincolna	Lincoln	k1gMnSc2	Lincoln
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
viceprezident	viceprezident	k1gMnSc1	viceprezident
Andrew	Andrew	k1gMnSc1	Andrew
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jedinými	jediný	k2eAgMnPc7d1	jediný
unionistickými	unionistický	k2eAgMnPc7d1	unionistický
prezidenty	prezident	k1gMnPc7	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
proces	proces	k1gInSc1	proces
odvolání	odvolání	k1gNnSc2	odvolání
tzv.	tzv.	kA	tzv.
impeachmentu	impeachment	k1gInSc2	impeachment
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1868	[number]	k4	1868
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
hlas	hlas	k1gInSc4	hlas
nebyl	být	k5eNaImAgInS	být
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
prezidenství	prezidenství	k1gNnSc2	prezidenství
nebyl	být	k5eNaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
žádný	žádný	k1gMnSc1	žádný
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1876	[number]	k4	1876
–	–	k?	–
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získal	získat	k5eAaPmAgMnS	získat
demokrat	demokrat	k1gMnSc1	demokrat
Samuel	Samuel	k1gMnSc1	Samuel
Tilden	Tildna	k1gFnPc2	Tildna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
udávaly	udávat	k5eAaImAgFnP	udávat
volební	volební	k2eAgFnPc1d1	volební
komise	komise	k1gFnPc1	komise
soupeřících	soupeřící	k2eAgFnPc2d1	soupeřící
stran	strana	k1gFnPc2	strana
odlišné	odlišný	k2eAgInPc1d1	odlišný
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
volební	volební	k2eAgFnSc2d1	volební
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
vítěze	vítěz	k1gMnPc4	vítěz
rozdílem	rozdíl	k1gInSc7	rozdíl
jednoho	jeden	k4xCgInSc2	jeden
hlasu	hlas	k1gInSc2	hlas
republikána	republikán	k1gMnSc2	republikán
Rutherforda	Rutherford	k1gMnSc2	Rutherford
Hayese	Hayese	k1gFnSc2	Hayese
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
ovšem	ovšem	k9	ovšem
volilo	volit	k5eAaImAgNnS	volit
o	o	k7c4	o
250	[number]	k4	250
000	[number]	k4	000
voličů	volič	k1gMnPc2	volič
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1877	[number]	k4	1877
–	–	k?	–
Rutherford	Rutherford	k1gMnSc1	Rutherford
Hayes	Hayes	k1gMnSc1	Hayes
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
složil	složit	k5eAaPmAgMnS	složit
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
salónku	salónek	k1gInSc6	salónek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tajně	tajně	k6eAd1	tajně
již	již	k6eAd1	již
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
4	[number]	k4	4
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
byla	být	k5eAaImAgFnS	být
neděle	neděle	k1gFnSc1	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
pak	pak	k6eAd1	pak
přísahu	přísaha	k1gFnSc4	přísaha
opakoval	opakovat	k5eAaImAgInS	opakovat
veřejně	veřejně	k6eAd1	veřejně
v	v	k7c6	v
Kapitolu	Kapitol	k1gInSc6	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
telefon	telefon	k1gInSc1	telefon
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1885	[number]	k4	1885
–	–	k?	–
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
Grover	Grovra	k1gFnPc2	Grovra
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
počtem	počet	k1gInSc7	počet
prezidentů	prezident	k1gMnPc2	prezident
(	(	kIx(	(
<g/>
n	n	k0	n
=	=	kIx~	=
45	[number]	k4	45
<g/>
)	)	kIx)	)
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úřadě	úřad	k1gInSc6	úřad
(	(	kIx(	(
<g/>
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
=	=	kIx~	=
44	[number]	k4	44
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
umřel	umřít	k5eAaPmAgMnS	umřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
nebyl	být	k5eNaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
nový	nový	k2eAgMnSc1d1	nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1886	[number]	k4	1886
–	–	k?	–
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
první	první	k4xOgFnSc7	první
dámou	dáma	k1gFnSc7	dáma
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
stala	stát	k5eAaPmAgFnS	stát
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
prezidenta	prezident	k1gMnSc2	prezident
Grovera	Grover	k1gMnSc2	Grover
Clevelanda	Clevelanda	k1gFnSc1	Clevelanda
Frances	Frances	k1gInSc1	Frances
Clevelandová	Clevelandový	k2eAgFnSc1d1	Clevelandová
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
<	<	kIx(	<
</s>
</p>
<p>
<s>
1901	[number]	k4	1901
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
umírá	umírat	k5eAaImIp3nS	umírat
poslední	poslední	k2eAgMnSc1d1	poslední
veterán	veterán	k1gMnSc1	veterán
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
zvolený	zvolený	k2eAgInSc4d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
William	William	k1gInSc4	William
McKinley	McKinlea	k1gFnSc2	McKinlea
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
střelnou	střelný	k2eAgFnSc7d1	střelná
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
26	[number]	k4	26
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vůbec	vůbec	k9	vůbec
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
42	[number]	k4	42
<g/>
letý	letý	k2eAgInSc1d1	letý
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1933	[number]	k4	1933
–	–	k?	–
nejdéle	dlouho	k6eAd3	dlouho
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
zvolený	zvolený	k2eAgInSc4d1	zvolený
Franklin	Franklin	k1gInSc4	Franklin
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
–	–	k?	–
nejmladším	mladý	k2eAgMnPc3d3	nejmladší
zvoleným	zvolený	k2eAgMnPc3d1	zvolený
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
římskokatolickým	římskokatolický	k2eAgMnSc7d1	římskokatolický
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
43	[number]	k4	43
let	léto	k1gNnPc2	léto
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
prezident	prezident	k1gMnSc1	prezident
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
procesu	proces	k1gInSc3	proces
impeachmentu	impeachment	k1gInSc2	impeachment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
porušit	porušit	k5eAaPmF	porušit
tradici	tradice	k1gFnSc4	tradice
inauguračních	inaugurační	k2eAgInPc2d1	inaugurační
ceremoniálů	ceremoniál	k1gInPc2	ceremoniál
a	a	k8xC	a
přenesl	přenést	k5eAaPmAgInS	přenést
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
uvedení	uvedení	k1gNnSc4	uvedení
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Kapitolu	Kapitol	k1gInSc2	Kapitol
na	na	k7c4	na
západní	západní	k2eAgInPc4d1	západní
schody	schod	k1gInPc4	schod
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
chtěl	chtít	k5eAaImAgMnS	chtít
uctít	uctít	k5eAaPmF	uctít
svůj	svůj	k3xOyFgInSc4	svůj
stát	stát	k1gInSc4	stát
Kalifornii	Kalifornie	k1gFnSc4	Kalifornie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
chtěl	chtít	k5eAaImAgMnS	chtít
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc4	počet
diváků	divák	k1gMnPc2	divák
na	na	k7c4	na
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
69	[number]	k4	69
letech	let	k1gInPc6	let
a	a	k8xC	a
po	po	k7c6	po
69	[number]	k4	69
dnech	den	k1gInPc6	den
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
terčem	terč	k1gInSc7	terč
neúspěšného	úspěšný	k2eNgInSc2d1	neúspěšný
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Střelcem	Střelec	k1gMnSc7	Střelec
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Hinckley	Hincklea	k1gFnSc2	Hincklea
<g/>
,	,	kIx,	,
fanoušek	fanoušek	k1gMnSc1	fanoušek
a	a	k8xC	a
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
herečky	herečka	k1gFnSc2	herečka
Jodie	Jodie	k1gFnSc2	Jodie
Fosterové	Fosterová	k1gFnSc2	Fosterová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Taxikář	taxikář	k1gMnSc1	taxikář
<g/>
.	.	kIx.	.
</s>
<s>
Nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vzbudit	vzbudit	k5eAaPmF	vzbudit
její	její	k3xOp3gFnSc4	její
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
činu	čin	k1gInSc3	čin
-	-	kIx~	-
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
kulka	kulka	k1gFnSc1	kulka
minula	minout	k5eAaImAgFnS	minout
srdce	srdce	k1gNnSc4	srdce
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
palec	palec	k1gInSc1	palec
a	a	k8xC	a
s	s	k7c7	s
průstřelem	průstřel	k1gInSc7	průstřel
hrudníku	hrudník	k1gInSc2	hrudník
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
započal	započnout	k5eAaPmAgInS	započnout
impeachment	impeachment	k1gInSc1	impeachment
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Clintonem	Clinton	k1gMnSc7	Clinton
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
vina	vina	k1gFnSc1	vina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
George	George	k1gFnPc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jehož	jenž	k3xRgNnSc2	jenž
znovuzvolení	znovuzvolení	k1gNnSc2	znovuzvolení
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
byly	být	k5eAaImAgInP	být
podány	podán	k2eAgInPc1d1	podán
dva	dva	k4xCgInPc1	dva
návrhy	návrh	k1gInPc1	návrh
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dalšího	další	k2eAgInSc2d1	další
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
obsahem	obsah	k1gInSc7	obsah
je	být	k5eAaImIp3nS	být
zrušení	zrušení	k1gNnSc1	zrušení
podmínky	podmínka	k1gFnSc2	podmínka
narození	narození	k1gNnSc2	narození
kandidáta	kandidát	k1gMnSc2	kandidát
ucházejícího	ucházející	k2eAgMnSc2d1	ucházející
se	se	k3xPyFc4	se
o	o	k7c4	o
mandát	mandát	k1gInSc4	mandát
prezidenta	prezident	k1gMnSc2	prezident
či	či	k8xC	či
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
tedy	tedy	k9	tedy
připouští	připouštět	k5eAaImIp3nP	připouštět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
pozic	pozice	k1gFnPc2	pozice
dostali	dostat	k5eAaPmAgMnP	dostat
naturalizovaní	naturalizovaný	k2eAgMnPc1d1	naturalizovaný
Američané	Američan	k1gMnPc1	Američan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
délkou	délka	k1gFnSc7	délka
amerického	americký	k2eAgNnSc2d1	americké
občanství	občanství	k1gNnSc2	občanství
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
ovšem	ovšem	k9	ovšem
nedošlo	dojít	k5eNaPmAgNnS	dojít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
akci	akce	k1gFnSc3	akce
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
afroamerického	afroamerický	k2eAgInSc2d1	afroamerický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgInS	obdržet
absolutně	absolutně	k6eAd1	absolutně
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gInPc2	volič
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
země	zem	k1gFnSc2	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
-	-	kIx~	-
přesně	přesně	k6eAd1	přesně
69	[number]	k4	69
456	[number]	k4	456
897	[number]	k4	897
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
inauguraci	inaugurace	k1gFnSc6	inaugurace
přísahal	přísahat	k5eAaImAgMnS	přísahat
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
bibli	bible	k1gFnSc4	bible
jako	jako	k8xC	jako
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zrušil	zrušit	k5eAaPmAgMnS	zrušit
otrokářství	otrokářství	k1gNnSc4	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedoslovnou	doslovný	k2eNgFnSc4d1	nedoslovná
formulaci	formulace	k1gFnSc4	formulace
slibu	slib	k1gInSc3	slib
přísahu	přísaha	k1gFnSc4	přísaha
opakoval	opakovat	k5eAaImAgMnS	opakovat
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgMnS	být
uveden	uveden	k2eAgMnSc1d1	uveden
nejstarší	starý	k2eAgMnSc1d3	nejstarší
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
70	[number]	k4	70
<g/>
letý	letý	k2eAgMnSc1d1	letý
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
všech	všecek	k3xTgNnPc2	všecek
prvně	prvně	k?	prvně
inaugurovaných	inaugurovaný	k2eAgMnPc2d1	inaugurovaný
prezidentů	prezident	k1gMnPc2	prezident
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
činil	činit	k5eAaImAgInS	činit
55	[number]	k4	55
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
prezidentském	prezidentský	k2eAgNnSc6d1	prezidentské
klání	klání	k1gNnSc6	klání
utkalo	utkat	k5eAaPmAgNnS	utkat
7	[number]	k4	7
důležitých	důležitý	k2eAgInPc2d1	důležitý
tzv.	tzv.	kA	tzv.
třetích	třetí	k4xOgNnPc2	třetí
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1892	[number]	k4	1892
–	–	k?	–
James	James	k1gMnSc1	James
Weaver	Weaver	k1gMnSc1	Weaver
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
4	[number]	k4	4
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
22	[number]	k4	22
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
–	–	k?	–
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
oslabil	oslabit	k5eAaPmAgInS	oslabit
republikánského	republikánský	k2eAgMnSc4d1	republikánský
kandidáta	kandidát	k1gMnSc4	kandidát
Williama	William	k1gMnSc4	William
Tafta	Taft	k1gMnSc4	Taft
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
6	[number]	k4	6
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
88	[number]	k4	88
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Taft	taft	k1gInSc1	taft
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
jen	jen	k9	jen
ve	v	k7c6	v
2	[number]	k4	2
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
8	[number]	k4	8
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
demokrat	demokrat	k1gMnSc1	demokrat
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
M.	M.	kA	M.
La	la	k1gNnSc1	la
Folette	Folett	k1gInSc5	Folett
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
1	[number]	k4	1
státě	stát	k1gInSc6	stát
(	(	kIx(	(
<g/>
13	[number]	k4	13
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
–	–	k?	–
Strom	strom	k1gInSc1	strom
Thurmond	Thurmond	k1gMnSc1	Thurmond
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
ve	v	k7c6	v
4	[number]	k4	4
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
39	[number]	k4	39
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
F.	F.	kA	F.
Byrd	Byrd	k1gMnSc1	Byrd
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vlastní	vlastní	k2eAgFnSc2d1	vlastní
volby	volba	k1gFnSc2	volba
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
15	[number]	k4	15
volitelů	volitel	k1gMnPc2	volitel
ve	v	k7c6	v
3	[number]	k4	3
státech	stát	k1gInPc6	stát
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
–	–	k?	–
George	Georg	k1gMnSc4	Georg
Wallace	Wallace	k1gFnSc2	Wallace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
5	[number]	k4	5
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
46	[number]	k4	46
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Ross	Ross	k1gInSc1	Ross
Perot	Perot	k1gInSc1	Perot
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nezískal	získat	k5eNaPmAgInS	získat
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
19	[number]	k4	19
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
</s>
</p>
<p>
<s>
Jediní	jediný	k2eAgMnPc1d1	jediný
2	[number]	k4	2
prezidenti	prezident	k1gMnPc1	prezident
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
znovuzvoleni	znovuzvolit	k5eAaPmNgMnP	znovuzvolit
jsou	být	k5eAaImIp3nP	být
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
<g/>
.	.	kIx.	.
</s>
<s>
JFK	JFK	kA	JFK
o	o	k7c6	o
nich	on	k3xPp3gInPc2	on
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Profily	profil	k1gInPc4	profil
odvahy	odvaha	k1gFnSc2	odvaha
jako	jako	k9	jako
o	o	k7c6	o
morálních	morální	k2eAgFnPc6d1	morální
autoritách	autorita	k1gFnPc6	autorita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
ze	z	k7c2	z
45	[number]	k4	45
prezidentů	prezident	k1gMnPc2	prezident
sloužilo	sloužit	k5eAaImAgNnS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
prezidenti	prezident	k1gMnPc1	prezident
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2017	[number]	k4	2017
měli	mít	k5eAaImAgMnP	mít
předchozí	předchozí	k2eAgFnSc4d1	předchozí
zkušenost	zkušenost	k1gFnSc4	zkušenost
se	s	k7c7	s
členstvím	členství	k1gNnSc7	členství
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
guvernérstvím	guvernérství	k1gNnSc7	guvernérství
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
generálové	generál	k1gMnPc1	generál
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
přerušil	přerušit	k5eAaPmAgMnS	přerušit
tuto	tento	k3xDgFnSc4	tento
227	[number]	k4	227
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc4d1	trvající
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nezastával	zastávat	k5eNaImAgMnS	zastávat
žádnou	žádný	k3yNgFnSc4	žádný
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
ani	ani	k8xC	ani
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stranická	stranický	k2eAgFnSc1d1	stranická
příslušnost	příslušnost	k1gFnSc1	příslušnost
prezidentů	prezident	k1gMnPc2	prezident
<g/>
:	:	kIx,	:
2	[number]	k4	2
federalisté	federalista	k1gMnPc1	federalista
<g/>
,	,	kIx,	,
4	[number]	k4	4
starorepublikáni	starorepublikán	k2eAgMnPc1d1	starorepublikán
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
whigové	whig	k1gMnPc1	whig
<g/>
,	,	kIx,	,
1	[number]	k4	1
unionista	unionista	k1gMnSc1	unionista
<g/>
,	,	kIx,	,
16	[number]	k4	16
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
19	[number]	k4	19
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
mandátu	mandát	k1gInSc2	mandát
u	u	k7c2	u
republikánských	republikánský	k2eAgMnPc2d1	republikánský
a	a	k8xC	a
demokratických	demokratický	k2eAgMnPc2d1	demokratický
prezidentů	prezident	k1gMnPc2	prezident
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
shodná	shodný	k2eAgFnSc1d1	shodná
–	–	k?	–
88	[number]	k4	88
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
prezidenty	prezident	k1gMnPc4	prezident
Tylera	Tylero	k1gNnSc2	Tylero
a	a	k8xC	a
A.	A.	kA	A.
Johnsona	Johnsona	k1gFnSc1	Johnsona
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
stranickou	stranický	k2eAgFnSc7d1	stranická
příslušností	příslušnost	k1gFnSc7	příslušnost
demokraty	demokrat	k1gMnPc4	demokrat
<g/>
,	,	kIx,	,
zvolené	zvolený	k2eAgNnSc4d1	zvolené
na	na	k7c6	na
kandidátkách	kandidátka	k1gFnPc6	kandidátka
jiných	jiný	k2eAgFnPc2d1	jiná
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mandátu	mandát	k1gInSc2	mandát
nebyli	být	k5eNaImAgMnP	být
demokraty	demokrat	k1gMnPc7	demokrat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Od	od	k7c2	od
prezidentů	prezident	k1gMnPc2	prezident
J.	J.	kA	J.
Madisona	Madisona	k1gFnSc1	Madisona
a	a	k8xC	a
J.	J.	kA	J.
Monroea	Monroea	k1gFnSc1	Monroea
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výměna	výměna	k1gFnSc1	výměna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgInPc4	dva
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
po	po	k7c4	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
prezidentů	prezident	k1gMnPc2	prezident
znovuzvoleni	znovuzvolit	k5eAaPmNgMnP	znovuzvolit
až	až	k6eAd1	až
po	po	k7c6	po
184	[number]	k4	184
letech	léto	k1gNnPc6	léto
W.	W.	kA	W.
J.	J.	kA	J.
Clinton	Clinton	k1gMnSc1	Clinton
a	a	k8xC	a
G.	G.	kA	G.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výměna	výměna	k1gFnSc1	výměna
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dosud	dosud	k6eAd1	dosud
8	[number]	k4	8
prezidentů	prezident	k1gMnPc2	prezident
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
(	(	kIx(	(
<g/>
W.	W.	kA	W.
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Taylor	Taylor	k1gInSc1	Taylor
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
W.	W.	kA	W.
Harding	Harding	k1gInSc1	Harding
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4	[number]	k4	4
byli	být	k5eAaImAgMnP	být
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
(	(	kIx(	(
<g/>
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Garfield	Garfield	k1gInSc1	Garfield
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
McKinley	McKinley	k1gInPc1	McKinley
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Kennedy	Kenned	k1gMnPc4	Kenned
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
90	[number]	k4	90
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
dožilo	dožít	k5eAaPmAgNnS	dožít
6	[number]	k4	6
prezidentů	prezident	k1gMnPc2	prezident
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Q.	Q.	kA	Q.
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
st.	st.	kA	st.
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
1787	[number]	k4	1787
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Herbert	Herbert	k1gMnSc1	Herbert
C.	C.	kA	C.
Hoover	Hoover	k1gMnSc1	Hoover
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g />
.	.	kIx.	.
</s>
<s>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Ronald	Ronald	k1gMnSc1	Ronald
W.	W.	kA	W.
Reagan	Reagan	k1gMnSc1	Reagan
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
R.	R.	kA	R.
Ford	ford	k1gInSc1	ford
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
George	Georg	k1gInSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
James	James	k1gMnSc1	James
E.	E.	kA	E.
Carter	Carter	k1gMnSc1	Carter
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Doposud	doposud	k6eAd1	doposud
jediným	jediný	k2eAgMnSc7d1	jediný
svobodným	svobodný	k2eAgMnSc7d1	svobodný
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
byl	být	k5eAaImAgMnS	být
James	James	k1gMnSc1	James
Buchanan	Buchanan	k1gMnSc1	Buchanan
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
první	první	k4xOgFnSc2	první
dámy	dáma	k1gFnSc2	dáma
plnila	plnit	k5eAaImAgFnS	plnit
jeho	jeho	k3xOp3gFnSc4	jeho
neteř	neteř	k1gFnSc4	neteř
Harriet	Harrieta	k1gFnPc2	Harrieta
Laneová	Laneová	k1gFnSc1	Laneová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
prezidentů	prezident	k1gMnPc2	prezident
nebyl	být	k5eNaImAgMnS	být
jedináček	jedináček	k1gMnSc1	jedináček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
prezident	prezident	k1gMnSc1	prezident
využívá	využívat	k5eAaImIp3nS	využívat
upravenou	upravený	k2eAgFnSc4d1	upravená
limuzínu	limuzína	k1gFnSc4	limuzína
Cadillac	cadillac	k1gInSc1	cadillac
DTS	DTS	kA	DTS
zvanou	zvaný	k2eAgFnSc4d1	zvaná
jako	jako	k8xS	jako
Cadillac	cadillac	k1gInSc4	cadillac
One	One	k1gFnSc2	One
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
jízdou	jízda	k1gFnSc7	jízda
byla	být	k5eAaImAgFnS	být
cesta	cesta	k1gFnSc1	cesta
na	na	k7c6	na
inauguraci	inaugurace	k1gFnSc6	inaugurace
20.1	[number]	k4	20.1
<g/>
.2001	.2001	k4	.2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
vzduchem	vzduch	k1gInSc7	vzduch
jsou	být	k5eAaImIp3nP	být
připravena	připravit	k5eAaPmNgFnS	připravit
dvě	dva	k4xCgNnPc1	dva
stejná	stejný	k2eAgNnPc1d1	stejné
letadla	letadlo	k1gNnPc1	letadlo
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
One	One	k1gFnSc1	One
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
automaticky	automaticky	k6eAd1	automaticky
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
One	One	k1gFnSc1	One
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kratší	krátký	k2eAgFnPc4d2	kratší
cesty	cesta	k1gFnPc4	cesta
využívá	využívat	k5eAaImIp3nS	využívat
helikoptéru	helikoptéra	k1gFnSc4	helikoptéra
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
tzv.	tzv.	kA	tzv.
Marine	Marin	k1gMnSc5	Marin
One	One	k1gMnSc5	One
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmínky	podmínka	k1gFnPc1	podmínka
volitelnosti	volitelnost	k1gFnPc1	volitelnost
==	==	k?	==
</s>
</p>
<p>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
zvolení	zvolení	k1gNnSc4	zvolení
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
upravuje	upravovat	k5eAaImIp3nS	upravovat
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Volitelnou	volitelný	k2eAgFnSc4d1	volitelná
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
občanství	občanství	k1gNnSc4	občanství
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
zrozením	zrození	k1gNnSc7	zrození
či	či	k8xC	či
měla	mít	k5eAaImAgFnS	mít
toto	tento	k3xDgNnSc4	tento
občanství	občanství	k1gNnSc4	občanství
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přijetí	přijetí	k1gNnSc2	přijetí
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
věku	věk	k1gInSc6	věk
třiceti	třicet	k4xCc7	třicet
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
bydlí	bydlet	k5eAaImIp3nS	bydlet
alespoň	alespoň	k9	alespoň
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
odd.	odd.	kA	odd.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
stejné	stejný	k2eAgFnPc4d1	stejná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
prezidenta	prezident	k1gMnSc2	prezident
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
na	na	k7c4	na
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
titulu	titul	k1gInSc2	titul
předsedou	předseda	k1gMnSc7	předseda
Senátu	senát	k1gInSc2	senát
USA	USA	kA	USA
bez	bez	k7c2	bez
hlasovacího	hlasovací	k2eAgNnSc2d1	hlasovací
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Hlasovat	hlasovat	k5eAaImF	hlasovat
může	moct	k5eAaImIp3nS	moct
jen	jen	k9	jen
při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
rukou	ruka	k1gFnPc2	ruka
skládají	skládat	k5eAaImIp3nP	skládat
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
slib	slib	k1gInSc4	slib
nově	nově	k6eAd1	nově
zvolení	zvolený	k2eAgMnPc1d1	zvolený
senátoři	senátor	k1gMnPc1	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
přísaha	přísaha	k1gFnSc1	přísaha
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
XX	XX	kA	XX
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
přísaha	přísaha	k1gFnSc1	přísaha
vykonána	vykonán	k2eAgFnSc1d1	vykonána
na	na	k7c6	na
Bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
<g/>
.	.	kIx.	.
</s>
<s>
Chester	Chester	k1gMnSc1	Chester
A.	A.	kA	A.
Arthur	Arthur	k1gMnSc1	Arthur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
a	a	k8xC	a
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
Bibli	bible	k1gFnSc6	bible
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
inauguraci	inaugurace	k1gFnSc4	inaugurace
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykonavatelem	vykonavatel	k1gMnSc7	vykonavatel
je	být	k5eAaImIp3nS	být
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
vykonal	vykonat	k5eAaPmAgMnS	vykonat
1	[number]	k4	1
<g/>
.	.	kIx.	.
inauguraci	inaugurace	k1gFnSc3	inaugurace
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
starosty	starosta	k1gMnSc2	starosta
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Impeachment	Impeachment	k1gInSc4	Impeachment
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
odvolat	odvolat	k5eAaPmF	odvolat
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
chráněn	chráněn	k2eAgInSc4d1	chráněn
imunitou	imunita	k1gFnSc7	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ústavy	ústava	k1gFnPc1	ústava
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
typech	typ	k1gInPc6	typ
provinění	provinění	k1gNnSc1	provinění
(	(	kIx(	(
<g/>
těžký	těžký	k2eAgInSc1d1	těžký
zločin	zločin	k1gInSc1	zločin
<g/>
,	,	kIx,	,
zpronevěra	zpronevěra	k1gFnSc1	zpronevěra
<g/>
,	,	kIx,	,
vlastizrada	vlastizrada	k1gFnSc1	vlastizrada
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
procesu	proces	k1gInSc2	proces
obžaloby	obžaloba	k1gFnSc2	obžaloba
(	(	kIx(	(
<g/>
impeachment	impeachment	k1gInSc1	impeachment
<g/>
)	)	kIx)	)
a	a	k8xC	a
soudním	soudní	k2eAgNnSc6d1	soudní
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
odvolání	odvolání	k1gNnSc4	odvolání
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vyšetření	vyšetření	k1gNnSc1	vyšetření
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
či	či	k8xC	či
její	její	k3xOp3gFnSc7	její
komisí	komise	k1gFnSc7	komise
</s>
</p>
<p>
<s>
obžaloba	obžaloba	k1gFnSc1	obžaloba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
může	moct	k5eAaImIp3nS	moct
vznést	vznést	k5eAaPmF	vznést
jen	jen	k9	jen
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obvinění	obvinění	k1gNnSc4	obvinění
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgFnSc2d1	nutná
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
soudním	soudní	k2eAgInSc7d1	soudní
tribunálem	tribunál	k1gInSc7	tribunál
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
předsedá	předsedat	k5eAaImIp3nS	předsedat
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
zbaven	zbavit	k5eAaPmNgInS	zbavit
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
pro	pro	k7c4	pro
vinu	vina	k1gFnSc4	vina
alespoň	alespoň	k9	alespoň
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
všech	všecek	k3xTgMnPc2	všecek
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
<g/>
Procesu	proces	k1gInSc2	proces
odvolání	odvolání	k1gNnSc2	odvolání
(	(	kIx(	(
<g/>
impeachmentu	impeachment	k1gInSc3	impeachment
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
podrobeni	podroben	k2eAgMnPc1d1	podroben
2	[number]	k4	2
prezidenti	prezident	k1gMnPc1	prezident
<g/>
:	:	kIx,	:
Andrew	Andrew	k1gMnSc1	Andrew
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
ministra	ministr	k1gMnSc2	ministr
války	válka	k1gFnSc2	válka
a	a	k8xC	a
sporu	spor	k1gInSc2	spor
se	s	k7c7	s
Senátem	senát	k1gInSc7	senát
bez	bez	k7c2	bez
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
obvinění	obvinění	k1gNnSc2	obvinění
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
po	po	k7c6	po
aféře	aféra	k1gFnSc6	aféra
s	s	k7c7	s
Monikou	Monika	k1gFnSc7	Monika
Lewinskou	Lewinská	k1gFnSc7	Lewinská
s	s	k7c7	s
obviněním	obvinění	k1gNnSc7	obvinění
z	z	k7c2	z
křivé	křivý	k2eAgFnSc2d1	křivá
přísahy	přísaha	k1gFnSc2	přísaha
a	a	k8xC	a
bránění	bránění	k1gNnSc2	bránění
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
se	se	k3xPyFc4	se
po	po	k7c4	po
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
maření	maření	k1gNnSc2	maření
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
zneužití	zneužití	k1gNnSc1	zneužití
pravomoci	pravomoc	k1gFnSc2	pravomoc
a	a	k8xC	a
pohrdání	pohrdání	k1gNnSc2	pohrdání
Kongresem	kongres	k1gInSc7	kongres
procesu	proces	k1gInSc2	proces
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
svým	svůj	k3xOyFgNnSc7	svůj
odstoupením	odstoupení	k1gNnSc7	odstoupení
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byl	být	k5eAaImAgInS	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
mj.	mj.	kA	mj.
upravuje	upravovat	k5eAaImIp3nS	upravovat
předání	předání	k1gNnSc1	předání
pravomoce	pravomoc	k1gFnSc2	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zbavení	zbavení	k1gNnSc2	zbavení
prezidenta	prezident	k1gMnSc2	prezident
úřadu	úřad	k1gInSc2	úřad
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
rezignace	rezignace	k1gFnSc2	rezignace
stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kdykoli	kdykoli	k6eAd1	kdykoli
prezident	prezident	k1gMnSc1	prezident
postoupí	postoupit	k5eAaPmIp3nS	postoupit
prozatímnímu	prozatímní	k2eAgMnSc3d1	prozatímní
předsedovi	předseda	k1gMnSc3	předseda
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
předsedovi	předseda	k1gMnSc3	předseda
Sněmovny	sněmovna	k1gFnPc1	sněmovna
reprezentantů	reprezentant	k1gInPc2	reprezentant
písemné	písemný	k2eAgNnSc4d1	písemné
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
vykonávat	vykonávat	k5eAaImF	vykonávat
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokud	dokud	k8xS	dokud
jim	on	k3xPp3gMnPc3	on
nepředá	předat	k5eNaPmIp3nS	předat
písemné	písemný	k2eAgNnSc4d1	písemné
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
takové	takový	k3xDgFnPc4	takový
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
povinnosti	povinnost	k1gFnSc2	povinnost
viceprezident	viceprezident	k1gMnSc1	viceprezident
jako	jako	k8xC	jako
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
využití	využití	k1gNnSc3	využití
došlo	dojít	k5eAaPmAgNnS	dojít
dosud	dosud	k6eAd1	dosud
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
Nixon	Nixon	k1gMnSc1	Nixon
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
michiganského	michiganský	k2eAgMnSc2d1	michiganský
kongresmana	kongresman	k1gMnSc2	kongresman
Geralda	Gerald	k1gMnSc2	Gerald
Forda	ford	k1gMnSc2	ford
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Spiro	Spiro	k1gNnSc1	Spiro
Agnewa	Agnewum	k1gNnSc2	Agnewum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
nový	nový	k2eAgInSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Ford	ford	k1gInSc4	ford
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
post	post	k1gInSc4	post
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
bývalého	bývalý	k2eAgMnSc2d1	bývalý
guvernéra	guvernér	k1gMnSc2	guvernér
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
Nelsona	Nelson	k1gMnSc4	Nelson
Rockefellera	Rockefeller	k1gMnSc4	Rockefeller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
G.	G.	kA	G.
Bush	Bush	k1gMnSc1	Bush
st.	st.	kA	st.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
-	-	kIx~	-
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
R.	R.	kA	R.
Cheney	Chenea	k1gMnSc2	Chenea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
-	-	kIx~	-
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
R.	R.	kA	R.
Cheney	Chenea	k1gMnSc2	Chenea
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1981	[number]	k4	1981
při	při	k7c6	při
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Reagana	Reagan	k1gMnSc4	Reagan
nebyl	být	k5eNaImAgMnS	být
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
uplatněn	uplatnit	k5eAaPmNgInS	uplatnit
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadla	letadlo	k1gNnSc2	letadlo
z	z	k7c2	z
Texasu	Texas	k1gInSc2	Texas
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgMnS	ujmout
funkce	funkce	k1gFnSc2	funkce
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byly	být	k5eAaImAgInP	být
právní	právní	k2eAgInPc1d1	právní
předpoklady	předpoklad	k1gInPc1	předpoklad
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
chirurgického	chirurgický	k2eAgInSc2d1	chirurgický
zákroku	zákrok	k1gInSc2	zákrok
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prezident	prezident	k1gMnSc1	prezident
nenabyl	nabýt	k5eNaPmAgMnS	nabýt
vědomí	vědomí	k1gNnSc4	vědomí
před	před	k7c7	před
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
dostavil	dostavit	k5eAaPmAgMnS	dostavit
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
4	[number]	k4	4
<g/>
.	.	kIx.	.
oddíl	oddíl	k1gInSc1	oddíl
dodatku	dodatek	k1gInSc2	dodatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prezident	prezident	k1gMnSc1	prezident
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
písemně	písemně	k6eAd1	písemně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
předání	předání	k1gNnSc4	předání
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
podle	podle	k7c2	podle
3	[number]	k4	3
<g/>
.	.	kIx.	.
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
možnosti	možnost	k1gFnSc2	možnost
však	však	k9	však
nevyužil	využít	k5eNaPmAgMnS	využít
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
celkovou	celkový	k2eAgFnSc4d1	celková
anestézii	anestézie	k1gFnSc4	anestézie
při	při	k7c6	při
operačním	operační	k2eAgInSc6d1	operační
výkonu	výkon	k1gInSc6	výkon
přišití	přišití	k1gNnSc2	přišití
utrženého	utržený	k2eAgInSc2d1	utržený
čtyřhlavého	čtyřhlavý	k2eAgInSc2d1	čtyřhlavý
stehenního	stehenní	k2eAgInSc2d1	stehenní
svalu	sval	k1gInSc2	sval
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemusel	muset	k5eNaImAgInS	muset
přenést	přenést	k5eAaPmF	přenést
své	svůj	k3xOyFgFnPc4	svůj
pravomoce	pravomoc	k1gFnPc4	pravomoc
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Gorea	Goreus	k1gMnSc4	Goreus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Prezidentská	prezidentský	k2eAgNnPc1d1	prezidentské
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc4	seznam
prezidentů	prezident	k1gMnPc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc4	seznam
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
prvních	první	k4xOgFnPc2	první
dam	dáma	k1gFnPc2	dáma
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Přísaha	přísaha	k1gFnSc1	přísaha
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
manželek	manželka	k1gFnPc2	manželka
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
</p>
<p>
<s>
Hlasy	hlas	k1gInPc1	hlas
přísahajících	přísahající	k2eAgMnPc2d1	přísahající
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
