<p>
<s>
Nimbus	nimbus	k1gInSc1	nimbus
Dam	dáma	k1gFnPc2	dáma
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Folsom	Folsom	k1gInSc4	Folsom
<g/>
,	,	kIx,	,
asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Sacramento	Sacramento	k1gNnSc4	Sacramento
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
přítok	přítok	k1gInSc1	přítok
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
řeka	řeka	k1gFnSc1	řeka
American	Americana	k1gFnPc2	Americana
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
700	[number]	k4	700
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytváží	vytvážet	k5eAaPmIp3nS	vytvážet
jezero	jezero	k1gNnSc4	jezero
Natoma	Natomum	k1gNnSc2	Natomum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
projektu	projekt	k1gInSc2	projekt
Central	Central	k1gMnSc2	Central
Valley	Vallea	k1gMnSc2	Vallea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
protipovodňovou	protipovodňový	k2eAgFnSc4d1	protipovodňová
ochranu	ochrana	k1gFnSc4	ochrana
Sacramenta	Sacramento	k1gNnSc2	Sacramento
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
zásobování	zásobování	k1gNnSc4	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nimbus	nimbus	k1gInSc4	nimbus
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
jako	jako	k8xC	jako
regulační	regulační	k2eAgFnSc1d1	regulační
nádrž	nádrž	k1gFnSc1	nádrž
pro	pro	k7c4	pro
přehradu	přehrada	k1gFnSc4	přehrada
Folsom	Folsom	k1gInSc1	Folsom
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
American	Americana	k1gFnPc2	Americana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výstavba	výstavba	k1gFnSc1	výstavba
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
hráze	hráz	k1gFnSc2	hráz
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
m	m	kA	m
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
333	[number]	k4	333
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nimbus	nimbus	k1gInSc1	nimbus
Dam	dáma	k1gFnPc2	dáma
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Bureau	Bureaum	k1gNnSc3	Bureaum
of	of	k?	of
Reclamation	Reclamation	k1gInSc1	Reclamation
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
