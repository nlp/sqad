<s>
Tento	tento	k3xDgInSc4
pojem	pojem	k1gInSc4
poprvé	poprvé	k6eAd1
použila	použít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
americká	americký	k2eAgFnSc1d1
odbornice	odbornice	k1gFnSc1
na	na	k7c4
kritickou	kritický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
rasy	rasa	k1gFnSc2
a	a	k8xC
obhájkyně	obhájkyně	k1gFnPc4
občanských	občanský	k2eAgNnPc2d1
a	a	k8xC
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
Kimberlé	Kimberlé	k1gFnSc1
Williams	Williams	k1gFnSc1
Crenshaw	Crenshaw	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>