<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
</s>
<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
intersekcionální	intersekcionální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
mnohonásobnou	mnohonásobný	k2eAgFnSc7d1
diskriminací	diskriminace	k1gFnSc7
<g/>
,	,	kIx,
studiem	studio	k1gNnSc7
systému	systém	k1gInSc2
společenských	společenský	k2eAgFnPc2d1
identit	identita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
paralelně	paralelně	k6eAd1
překrývají	překrývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
také	také	k9
zkoumá	zkoumat	k5eAaImIp3nS
korespondující	korespondující	k2eAgInPc4d1
systémy	systém	k1gInPc4
diskriminace	diskriminace	k1gFnSc2
<g/>
,	,	kIx,
útlaku	útlak	k1gInSc2
a	a	k8xC
dominance	dominance	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
gender	gender	k1gInSc4
<g/>
,	,	kIx,
etnicitu	etnicita	k1gFnSc4
<g/>
,	,	kIx,
třídu	třída	k1gFnSc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
a	a	k8xC
další	další	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc4
pojem	pojem	k1gInSc4
poprvé	poprvé	k6eAd1
použila	použít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
americká	americký	k2eAgFnSc1d1
odbornice	odbornice	k1gFnSc1
na	na	k7c4
kritickou	kritický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
rasy	rasa	k1gFnSc2
a	a	k8xC
obhájkyně	obhájkyně	k1gFnPc4
občanských	občanský	k2eAgNnPc2d1
a	a	k8xC
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
Kimberlé	Kimberlé	k1gFnSc1
Williams	Williams	k1gFnSc1
Crenshaw	Crenshaw	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pojem	pojem	k1gInSc1
intersekcionalita	intersekcionalita	k1gFnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
druhé	druhý	k4xOgFnSc2
vlny	vlna	k1gFnSc2
feminismu	feminismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
feminismu	feminismus	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
té	ten	k3xDgFnSc6
první	první	k4xOgFnSc1
orientovala	orientovat	k5eAaBmAgFnS
na	na	k7c4
pojmenování	pojmenování	k1gNnSc4
problémů	problém	k1gInPc2
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
oblastech	oblast	k1gFnPc6
sexuality	sexualita	k1gFnSc2
<g/>
,	,	kIx,
rodiny	rodina	k1gFnSc2
nebo	nebo	k8xC
pracovního	pracovní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
lidé	člověk	k1gMnPc1
poprvé	poprvé	k6eAd1
začínají	začínat	k5eAaImIp3nP
vnímat	vnímat	k5eAaImF
problematiku	problematika	k1gFnSc4
diskriminace	diskriminace	k1gFnSc2
v	v	k7c6
různých	různý	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
a	a	k8xC
právě	právě	k9
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
vzniká	vznikat	k5eAaImIp3nS
pojem	pojem	k1gInSc1
intersekcionalita	intersekcionalita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
definuje	definovat	k5eAaBmIp3nS
tyto	tento	k3xDgInPc4
aspekty	aspekt	k1gInPc4
diskriminace	diskriminace	k1gFnSc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kimberlé	Kimberlý	k2eAgFnPc1d1
Crenshaw	Crenshaw	k1gFnPc1
<g/>
,	,	kIx,
zakladatelka	zakladatelka	k1gFnSc1
termínu	termín	k1gInSc2
intersekcionalita	intersekcionalita	k1gFnSc1
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
oddělená	oddělený	k2eAgNnPc1d1
studia	studio	k1gNnPc1
rasismu	rasismus	k1gInSc2
a	a	k8xC
sexismu	sexismus	k1gInSc2
nejsou	být	k5eNaImIp3nP
dostatečná	dostatečný	k2eAgFnSc1d1
ve	v	k7c6
zkoumání	zkoumání	k1gNnSc6
zkušeností	zkušenost	k1gFnPc2
lidí	člověk	k1gMnPc2
setkávajících	setkávající	k2eAgMnPc2d1
se	se	k3xPyFc4
s	s	k7c7
průnikem	průnik	k1gInSc7
různých	různý	k2eAgFnPc2d1
diskriminací	diskriminace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Combahee	Combahee	k1gFnSc1
River	Rivra	k1gFnPc2
Collective	Collectiv	k1gInSc5
</s>
<s>
Organizace	organizace	k1gFnSc1
Combahee	Combahe	k1gFnSc2
River	Rivra	k1gFnPc2
Collective	Collectiv	k1gInSc5
byla	být	k5eAaImAgNnP
založená	založený	k2eAgNnPc1d1
černošskými	černošský	k2eAgFnPc7d1
lesbickými	lesbický	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
uskupení	uskupení	k1gNnSc1
bylo	být	k5eAaImAgNnS
aktivní	aktivní	k2eAgNnSc1d1
až	až	k9
do	do	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
rozpadu	rozpad	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
členky	členka	k1gFnPc1
zastávaly	zastávat	k5eAaImAgFnP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
bělošský	bělošský	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
se	se	k3xPyFc4
neztotožňoval	ztotožňovat	k5eNaImAgInS
s	s	k7c7
jejich	jejich	k3xOp3gInPc7
názory	názor	k1gInPc7
a	a	k8xC
nenaplňoval	naplňovat	k5eNaImAgMnS
jejich	jejich	k3xOp3gFnSc2
ideje	idea	k1gFnSc2
a	a	k8xC
očekávání	očekávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
povědomí	povědomí	k1gNnSc2
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
kvůli	kvůli	k7c3
dokumentu	dokument	k1gInSc3
Combahee	Combahe	k1gInSc2
River	River	k1gInSc4
Collectiv	Collectiv	k1gInSc1
Statement	Statement	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
můžeme	moct	k5eAaImIp1nP
poprvé	poprvé	k6eAd1
najít	najít	k5eAaPmF
zmínku	zmínka	k1gFnSc4
o	o	k7c6
intersekcionalitě	intersekcionalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokument	dokument	k1gInSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
problémem	problém	k1gInSc7
útisku	útisk	k1gInSc2
žen	žena	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
barvy	barva	k1gFnSc2
pleti	pleť	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
jejich	jejich	k3xOp3gFnSc1
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
a	a	k8xC
feminismus	feminismus	k1gInSc1
</s>
<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
feministickou	feministický	k2eAgFnSc7d1
a	a	k8xC
antirasovou	antirasův	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dotýká	dotýkat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k6eAd1
žen	žena	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
významně	významně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intersekcionální	Intersekcionální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
diskriminováni	diskriminován	k2eAgMnPc1d1
na	na	k7c6
základě	základ	k1gInSc6
několika	několik	k4yIc2
sociálních	sociální	k2eAgMnPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgMnPc2d1
faktorů	faktor	k1gInPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nerovnosti	nerovnost	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
kontextu	kontext	k1gInSc6
tří	tři	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
rasa	rasa	k1gFnSc1
či	či	k8xC
etnicita	etnicita	k1gFnSc1
<g/>
,	,	kIx,
třída	třída	k1gFnSc1
a	a	k8xC
také	také	k9
gender	gender	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
faktory	faktor	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
například	například	k6eAd1
věk	věk	k1gInSc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
rodinný	rodinný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
náboženské	náboženský	k2eAgNnSc4d1
vyznání	vyznání	k1gNnSc4
<g/>
,	,	kIx,
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc1d1
způsobilost	způsobilost	k1gFnSc1
nebo	nebo	k8xC
fáze	fáze	k1gFnSc1
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
mnohonásobnou	mnohonásobný	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
termínu	termín	k1gInSc3
intersekcionalita	intersekcionalita	k1gFnSc1
také	také	k9
odkazuje	odkazovat	k5eAaImIp3nS
i	i	k9
americká	americký	k2eAgFnSc1d1
socioložka	socioložka	k1gFnSc1
Patricia	Patricius	k1gMnSc2
Hill	Hill	k1gMnSc1
Collins	Collins	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
definuje	definovat	k5eAaBmIp3nS
tento	tento	k3xDgInSc4
pojem	pojem	k1gInSc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
konkrétní	konkrétní	k2eAgInPc1d1
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
protínající	protínající	k2eAgFnSc2d1
formy	forma	k1gFnSc2
útisku	útisk	k1gInSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
rase	rasa	k1gFnSc3
<g/>
,	,	kIx,
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
a	a	k8xC
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
Dále	daleko	k6eAd2
zmiňuje	zmiňovat	k5eAaImIp3nS
typické	typický	k2eAgInPc4d1
rysy	rys	k1gInPc4
intersekcionality	intersekcionalita	k1gFnSc2
–	–	k?
černošský	černošský	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
<g/>
,	,	kIx,
multirasový	multirasový	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
a	a	k8xC
feminismus	feminismus	k1gInSc1
třetího	třetí	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
a	a	k8xC
diskriminace	diskriminace	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
teorie	teorie	k1gFnSc2
intersekcionality	intersekcionalita	k1gFnSc2
zažívá	zažívat	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
diskriminaci	diskriminace	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
všech	všecek	k3xTgFnPc2
možných	možný	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastějšími	častý	k2eAgFnPc7d3
kombinacemi	kombinace	k1gFnPc7
utlačování	utlačování	k1gNnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
dvojnásobný	dvojnásobný	k2eAgInSc1d1
útlak	útlak	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pohlaví	pohlaví	k1gNnSc1
plus	plus	k1gInSc1
rasa	rasa	k1gFnSc1
a	a	k8xC
věk	věk	k1gInSc1
plus	plus	k1gNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
někdo	někdo	k3yInSc1
utiskován	utiskován	k2eAgMnSc1d1
pouze	pouze	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
jednoho	jeden	k4xCgInSc2
z	z	k7c2
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
intersekcionalitu	intersekcionalita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příklady	příklad	k1gInPc1
intersekcionality	intersekcionalita	k1gFnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
</s>
<s>
Typickým	typický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
takovéto	takovýto	k3xDgFnSc2
diskriminace	diskriminace	k1gFnSc2
je	být	k5eAaImIp3nS
černošská	černošský	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
žena	žena	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
utlačována	utlačovat	k5eAaImNgFnS
na	na	k7c6
základě	základ	k1gInSc6
své	svůj	k3xOyFgFnSc2
rasy	rasa	k1gFnSc2
a	a	k8xC
pohlaví	pohlaví	k1gNnSc2
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejtypičtějších	typický	k2eAgInPc2d3
příkladů	příklad	k1gInPc2
intersekcionality	intersekcionalita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
majoritním	majoritní	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
jsou	být	k5eAaImIp3nP
ženy	žena	k1gFnPc1
ve	v	k7c6
věku	věk	k1gInSc6
nad	nad	k7c4
50	#num#	k4
let	léto	k1gNnPc2
hledající	hledající	k2eAgNnSc4d1
zaměstnání	zaměstnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žena	žena	k1gFnSc1
jako	jako	k8xS,k8xC
taková	takový	k3xDgFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
naší	náš	k3xOp1gFnSc6
společnosti	společnost	k1gFnSc6
znevýhodňována	znevýhodňován	k2eAgFnSc1d1
kvůli	kvůli	k7c3
tradičním	tradiční	k2eAgFnPc3d1
rodinným	rodinný	k2eAgFnPc3d1
povinnostem	povinnost	k1gFnPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
starost	starost	k1gFnSc4
o	o	k7c4
děti	dítě	k1gFnPc4
či	či	k8xC
rodiče	rodič	k1gMnPc4
a	a	k8xC
také	také	k9
péče	péče	k1gFnSc2
o	o	k7c4
domácnost	domácnost	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ženy	žena	k1gFnPc1
mívají	mívat	k5eAaImIp3nP
za	za	k7c4
stejnou	stejný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
nižší	nízký	k2eAgInSc4d2
plat	plat	k1gInSc4
než	než	k8xS
muži	muž	k1gMnPc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
gender	gender	k1gMnSc1
pay	pay	k?
gap	gap	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
mladšími	mladý	k2eAgFnPc7d2
uchazečkami	uchazečka	k1gFnPc7
jsou	být	k5eAaImIp3nP
starší	starý	k2eAgFnPc1d2
ženy	žena	k1gFnPc1
diskriminovány	diskriminovat	k5eAaBmNgFnP
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgInSc3
pokročilejšímu	pokročilý	k2eAgInSc3d2
věku	věk	k1gInSc3
<g/>
,	,	kIx,
často	často	k6eAd1
mají	mít	k5eAaImIp3nP
nižší	nízký	k2eAgNnSc4d2
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
jim	on	k3xPp3gMnPc3
také	také	k9
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
znalost	znalost	k1gFnSc1
současných	současný	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
vymožeností	vymoženost	k1gFnPc2
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
potýkat	potýkat	k5eAaImF
s	s	k7c7
jazykovou	jazykový	k2eAgFnSc7d1
bariérou	bariéra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
také	také	k9
dalo	dát	k5eAaPmAgNnS
označit	označit	k5eAaPmF
za	za	k7c4
vícenásobnou	vícenásobný	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
–	–	k?
diskriminace	diskriminace	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
věku	věk	k1gInSc2
<g/>
,	,	kIx,
pohlaví	pohlaví	k1gNnSc2
a	a	k8xC
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DeFrancisco	DeFrancisco	k1gNnSc1
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
P.	P.	kA
<g/>
;	;	kIx,
Palczewski	Palczewsk	k1gMnSc3
<g/>
,	,	kIx,
Catherine	Catherin	k1gInSc5
H.	H.	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gender	Gender	k1gInSc1
in	in	k?
Communication	Communication	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thousand	Thousanda	k1gFnPc2
Oaks	Oaksa	k1gFnPc2
<g/>
,	,	kIx,
California	Californium	k1gNnSc2
<g/>
:	:	kIx,
Sage	Sage	k1gFnSc1
<g/>
.	.	kIx.
p.	p.	k?
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-1-4522-2009-31	978-1-4522-2009-31	k4
2	#num#	k4
CRENSHAW	CRENSHAW	kA
<g/>
,	,	kIx,
Kimberle	Kimberle	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demarginalizing	Demarginalizing	k1gInSc1
the	the	k?
Intersection	Intersection	k1gInSc1
of	of	k?
Race	Race	k1gInSc1
and	and	k?
Sex	sex	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Black	Blacka	k1gFnPc2
Feminist	Feminist	k1gMnSc1
Critique	Critiqu	k1gFnSc2
of	of	k?
Antidiscrimination	Antidiscrimination	k1gInSc1
Doctrine	Doctrin	k1gInSc5
<g/>
,	,	kIx,
Feminist	Feminist	k1gMnSc1
Theory	Theora	k1gFnSc2
and	and	k?
Antiracist	Antiracist	k1gInSc1
Politics	Politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
Chicago	Chicago	k1gNnSc1
Legal	Legal	k1gMnSc1
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RITZER	RITZER	kA
<g/>
,	,	kIx,
George	George	k1gFnSc1
<g/>
.	.	kIx.
<g/>
The	The	k1gMnSc1
Blackwell	Blackwell	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Sociology	sociolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Blackwell	Blackwell	k1gInSc1
Publishing	Publishing	k1gInSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4051	#num#	k4
<g/>
-	-	kIx~
<g/>
2433	#num#	k4
<g/>
-	-	kIx~
<g/>
1.1	1.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
The	The	k1gMnPc2
Combahee	Combahee	k1gInSc4
River	Rivra	k1gFnPc2
Collective	Collectiv	k1gInSc5
statement	statement	k1gInSc1
<g/>
:	:	kIx,
Black	Black	k1gInSc1
Feminist	Feminist	k1gInSc1
organizing	organizing	k1gInSc1
in	in	k?
the	the	k?
seventies	seventies	k1gMnSc1
and	and	k?
eighties	eighties	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Kitchen	Kitchen	k1gInSc1
Table	tablo	k1gNnSc6
<g/>
:	:	kIx,
Women	Women	k1gInSc1
of	of	k?
Color	Color	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
913750506	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Knudsen	Knudsen	k1gInSc1
<g/>
,	,	kIx,
Susanne	Susann	k1gInSc5
V.	V.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Intersectionality	Intersectionalita	k1gFnPc1
–	–	k?
a	a	k8xC
theoretical	theoreticat	k5eAaPmAgInS
inspiration	inspiration	k1gInSc1
in	in	k?
the	the	k?
analysis	analysis	k1gFnSc2
of	of	k?
minority	minorita	k1gFnSc2
cultures	cultures	k1gMnSc1
and	and	k?
identities	identities	k1gMnSc1
in	in	k?
textbooks	textbooks	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
in	in	k?
Bruillard	Bruillard	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Éric	Éric	k1gInSc1
<g/>
;	;	kIx,
Horsley	Horslea	k1gFnPc1
<g/>
,	,	kIx,
Mike	Mik	k1gInPc1
<g/>
;	;	kIx,
Aamotsbakken	Aamotsbakken	k1gInSc1
<g/>
,	,	kIx,
Bente	Bent	k1gMnSc5
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Caught	Caught	k1gMnSc1
in	in	k?
the	the	k?
Web	web	k1gInSc1
or	or	k?
Lost	Lost	k1gInSc1
in	in	k?
the	the	k?
Textbook	Textbook	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
th	th	k?
IARTEM	IARTEM	kA
conference	conference	k1gFnSc2
on	on	k3xPp3gInSc1
learning	learning	k1gInSc1
and	and	k?
educational	educationat	k5eAaImAgInS,k5eAaPmAgInS
media	medium	k1gNnPc4
<g/>
,	,	kIx,
held	held	k6eAd1
in	in	k?
Caen	Caen	k1gInSc1
in	in	k?
October	October	k1gInSc1
2005	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Utrecht	Utrecht	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Netherlands	Netherlands	k1gInSc1
<g/>
:	:	kIx,
International	International	k1gFnSc1
Association	Association	k1gInSc1
for	forum	k1gNnPc2
Research	Research	k1gMnSc1
on	on	k3xPp3gMnSc1
Textbooks	Textbooks	k1gInSc4
and	and	k?
Educational	Educational	k1gFnSc2
Media	medium	k1gNnSc2
(	(	kIx(
<g/>
IARTEM	IARTEM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
61	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
<g/>
,	,	kIx,
OCLC	OCLC	kA
799730084	#num#	k4
<g/>
,	,	kIx,
archived	archived	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
original	originat	k5eAaPmAgMnS,k5eAaImAgMnS
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
on	on	k3xPp3gInSc1
11	#num#	k4
December	December	k1gInSc1
2006	#num#	k4
<g/>
↑	↑	k?
DeFrancisco	DeFrancisco	k1gNnSc1
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
P.	P.	kA
<g/>
;	;	kIx,
Palczewski	Palczewsk	k1gMnSc3
<g/>
,	,	kIx,
Catherine	Catherin	k1gInSc5
<g/>
,	,	kIx,
H.	H.	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Communicating	Communicating	k1gInSc1
gender	gender	k1gInSc1
diversity	diversit	k1gInPc1
<g/>
:	:	kIx,
a	a	k8xC
critical	criticat	k5eAaPmAgMnS
approach	approach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
<g/>
:	:	kIx,
Sage	Sage	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781412925594	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CRENSHAW	CRENSHAW	kA
<g/>
,	,	kIx,
Kimberle	Kimberle	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demarginalizing	Demarginalizing	k1gInSc1
the	the	k?
Intersection	Intersection	k1gInSc1
of	of	k?
Race	Race	k1gInSc1
and	and	k?
Sex	sex	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Black	Blacka	k1gFnPc2
Feminist	Feminist	k1gMnSc1
Critique	Critiqu	k1gFnSc2
of	of	k?
Antidiscrimination	Antidiscrimination	k1gInSc1
Doctrine	Doctrin	k1gInSc5
<g/>
,	,	kIx,
Feminist	Feminist	k1gMnSc1
Theory	Theora	k1gFnSc2
and	and	k?
Antiracist	Antiracist	k1gInSc1
Politics	Politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
Chicago	Chicago	k1gNnSc1
Legal	Legal	k1gMnSc1
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RITZER	RITZER	kA
<g/>
,	,	kIx,
George	George	k1gFnSc1
<g/>
.	.	kIx.
<g/>
The	The	k1gMnSc1
Blackwell	Blackwell	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Sociology	sociolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Blackwell	Blackwell	k1gInSc1
Publishing	Publishing	k1gInSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4051	#num#	k4
<g/>
-	-	kIx~
<g/>
2433	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Combahee	Combahee	k1gInSc1
River	River	k1gInSc1
Collective	Collectiv	k1gInSc5
statement	statement	k1gInSc1
<g/>
:	:	kIx,
Black	Black	k1gInSc1
Feminist	Feminist	k1gInSc1
organizing	organizing	k1gInSc1
in	in	k?
the	the	k?
seventies	seventies	k1gMnSc1
and	and	k?
eighties	eighties	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Kitchen	Kitchen	k1gInSc1
Table	tablo	k1gNnSc6
<g/>
:	:	kIx,
Women	Women	k1gInSc1
of	of	k?
Color	Color	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
913750506	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sociologie	sociologie	k1gFnSc1
</s>
