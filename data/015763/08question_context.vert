<s>
Intersekcionalita	Intersekcionalita	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
intersekcionální	intersekcionální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
mnohonásobnou	mnohonásobný	k2eAgFnSc7d1
diskriminací	diskriminace	k1gFnSc7
<g/>
,	,	kIx,
studiem	studio	k1gNnSc7
systému	systém	k1gInSc2
společenských	společenský	k2eAgFnPc2d1
identit	identita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
paralelně	paralelně	k6eAd1
překrývají	překrývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>