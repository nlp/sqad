<s>
Bismarck	Bismarck	k1gMnSc1	Bismarck
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
po	po	k7c6	po
Fargu	Farg	k1gInSc6	Farg
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Severní	severní	k2eAgFnSc1d1	severní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
obecní	obecní	k2eAgNnSc1d1	obecní
město	město	k1gNnSc1	město
Burleigh	Burleigha	k1gFnPc2	Burleigha
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Bismarck	Bismarck	k1gMnSc1	Bismarck
byl	být	k5eAaImAgMnS	být
založen	založit	k5eAaPmNgMnS	založit
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Severní	severní	k2eAgFnSc2d1	severní
Dakoty	Dakota	k1gFnSc2	Dakota
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získalo	získat	k5eAaPmAgNnS	získat
suverenitu	suverenita	k1gFnSc4	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
71,0	[number]	k4	71,0
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
69,6	[number]	k4	69,6
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
1,4	[number]	k4	1,4
km2	km2	k4	km2
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Edwinton	Edwinton	k1gInSc1	Edwinton
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Edwina	Edwin	k1gMnSc2	Edwin
M.	M.	kA	M.
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
Severní	severní	k2eAgFnSc2d1	severní
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
Northern	Northern	k1gInSc1	Northern
Pacific	Pacifice	k1gFnPc2	Pacifice
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
německého	německý	k2eAgMnSc2d1	německý
kancléře	kancléř	k1gMnSc2	kancléř
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
"	"	kIx"	"
<g/>
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Northern	Northern	k1gMnSc1	Northern
Pacific	Pacific	k1gMnSc1	Pacific
Railway	Railwaa	k1gFnSc2	Railwaa
ho	on	k3xPp3gNnSc4	on
tak	tak	k6eAd1	tak
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěla	chtít	k5eAaImAgFnS	chtít
nalákat	nalákat	k5eAaPmF	nalákat
německé	německý	k2eAgMnPc4d1	německý
imigranty	imigrant	k1gMnPc4	imigrant
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
usadili	usadit	k5eAaPmAgMnP	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
populace	populace	k1gFnSc2	populace
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
nedalekých	daleký	k2eNgInPc6d1	nedaleký
Black	Blacko	k1gNnPc2	Blacko
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
61	[number]	k4	61
272	[number]	k4	272
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Bismarcku	Bismarck	k1gInSc6	Bismarck
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sídlilo	sídlit	k5eAaImAgNnS	sídlit
55	[number]	k4	55
532	[number]	k4	532
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
23	[number]	k4	23
185	[number]	k4	185
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
14	[number]	k4	14
444	[number]	k4	444
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
797,4	[number]	k4	797,4
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
92,4	[number]	k4	92,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
německý	německý	k2eAgMnSc1d1	německý
-	-	kIx~	-
57,9	[number]	k4	57,9
%	%	kIx~	%
norský	norský	k2eAgMnSc1d1	norský
-	-	kIx~	-
18,2	[number]	k4	18,2
%	%	kIx~	%
ruský	ruský	k2eAgMnSc1d1	ruský
-	-	kIx~	-
7,7	[number]	k4	7,7
%	%	kIx~	%
irský	irský	k2eAgMnSc1d1	irský
-	-	kIx~	-
7,2	[number]	k4	7,2
%	%	kIx~	%
anglický	anglický	k2eAgMnSc1d1	anglický
-	-	kIx~	-
5,0	[number]	k4	5,0
%	%	kIx~	%
švédský	švédský	k2eAgMnSc1d1	švédský
-	-	kIx~	-
4,3	[number]	k4	4,3
%	%	kIx~	%
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
23,5	[number]	k4	23,5
%	%	kIx~	%
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
11,1	[number]	k4	11,1
%	%	kIx~	%
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
29,1	[number]	k4	29,1
%	%	kIx~	%
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
22,4	[number]	k4	22,4
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
13,8	[number]	k4	13,8
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
36	[number]	k4	36
let	léto	k1gNnPc2	léto
</s>
