<p>
<s>
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
TS	ts	k0	ts
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teoretický	teoretický	k2eAgInSc4d1	teoretický
model	model	k1gInSc4	model
počítače	počítač	k1gInSc2	počítač
popsaný	popsaný	k2eAgInSc4d1	popsaný
matematikem	matematik	k1gMnSc7	matematik
Alanem	Alan	k1gMnSc7	Alan
Turingem	Turing	k1gInSc7	Turing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
procesorové	procesorový	k2eAgFnSc2d1	procesorová
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
konečným	konečný	k2eAgInSc7d1	konečný
automatem	automat	k1gInSc7	automat
<g/>
,	,	kIx,	,
programu	program	k1gInSc2	program
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pravidel	pravidlo	k1gNnPc2	pravidlo
přechodové	přechodový	k2eAgFnSc2d1	přechodová
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
pravostranně	pravostranně	k6eAd1	pravostranně
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
pásky	páska	k1gFnSc2	páska
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
mezivýsledků	mezivýsledek	k1gInPc2	mezivýsledek
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
modelování	modelování	k1gNnSc4	modelování
algoritmů	algoritmus	k1gInPc2	algoritmus
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
vyčíslitelnosti	vyčíslitelnost	k1gFnSc2	vyčíslitelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
způsobu	způsob	k1gInSc2	způsob
vyjádření	vyjádření	k1gNnSc2	vyjádření
Churchovy-Turingovy	Churchovy-Turingův	k2eAgFnSc2d1	Churchovy-Turingův
teze	teze	k1gFnSc2	teze
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
algoritmu	algoritmus	k1gInSc3	algoritmus
existuje	existovat	k5eAaImIp3nS	existovat
ekvivalentní	ekvivalentní	k2eAgInSc4d1	ekvivalentní
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
síly	síla	k1gFnSc2	síla
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
turingovská	turingovský	k2eAgFnSc1d1	turingovský
úplnost	úplnost	k1gFnSc1	úplnost
<g/>
:	:	kIx,	:
turingovsky	turingovsky	k6eAd1	turingovsky
úplné	úplný	k2eAgFnPc1d1	úplná
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
ty	ten	k3xDgInPc1	ten
programovací	programovací	k2eAgInPc1d1	programovací
jazyky	jazyk	k1gInPc1	jazyk
nebo	nebo	k8xC	nebo
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
sílu	síla	k1gFnSc4	síla
jako	jako	k8xC	jako
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
sedmice	sedmice	k?	sedmice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
Q	Q	kA	Q
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc2	sigma
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
stavů	stav	k1gInPc2	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnSc2	Gammum
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
abeceda	abeceda	k1gFnSc1	abeceda
symbolů	symbol	k1gInPc2	symbol
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc1	symbol
reprezentující	reprezentující	k2eAgInSc1d1	reprezentující
prázdný	prázdný	k2eAgInSc1d1	prázdný
symbol	symbol	k1gInSc1	symbol
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vstupní	vstupní	k2eAgFnSc2d1	vstupní
abecedy	abeceda	k1gFnSc2	abeceda
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
řetězce	řetězec	k1gInSc2	řetězec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc7	sigma
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
setminus	setminus	k1gInSc1	setminus
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
vstupních	vstupní	k2eAgInPc2d1	vstupní
symbolů	symbol	k1gInPc2	symbol
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgInSc1d1	počáteční
stav	stav	k1gInSc1	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
:	:	kIx,	:
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
přechodová	přechodový	k2eAgFnSc1d1	přechodová
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
posun	posun	k1gInSc1	posun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
vlevo	vlevo	k6eAd1	vlevo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
posun	posun	k1gInSc1	posun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
vpravo	vpravo	k6eAd1	vpravo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
koncových	koncový	k2eAgInPc2d1	koncový
stavů	stav	k1gInPc2	stav
</s>
</p>
<p>
<s>
==	==	k?	==
Konfigurace	konfigurace	k1gFnSc2	konfigurace
==	==	k?	==
</s>
</p>
<p>
<s>
Konfigurace	konfigurace	k1gFnSc1	konfigurace
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc1d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
q	q	k?	q
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
right	right	k2eAgInSc4d1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc4	rangle
}	}	kIx)	}
</s>
</p>
<p>
<s>
množiny	množina	k1gFnPc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∣	∣	k?	∣
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
yb	yb	k?	yb
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mid	mid	k?	mid
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
q	q	k?	q
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgInSc4d1	aktuální
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
souvislá	souvislý	k2eAgFnSc1d1	souvislá
část	část	k1gFnSc1	část
pásky	páska	k1gFnSc2	páska
obsahující	obsahující	k2eAgFnSc1d1	obsahující
všechny	všechen	k3xTgInPc4	všechen
neprázdné	prázdný	k2eNgInPc4d1	neprázdný
symboly	symbol	k1gInPc4	symbol
a	a	k8xC	a
n	n	k0	n
je	být	k5eAaImIp3nS	být
pozice	pozice	k1gFnPc4	pozice
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc4	číslo
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
množiny	množina	k1gFnPc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnSc2	Gammum
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
disjunktní	disjunktní	k2eAgInPc1d1	disjunktní
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
lze	lze	k6eAd1	lze
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
volbou	volba	k1gFnSc7	volba
prvků	prvek	k1gInPc2	prvek
množiny	množina	k1gFnPc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
též	též	k9	též
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
konfigurace	konfigurace	k1gFnSc1	konfigurace
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
řetězec	řetězec	k1gInSc1	řetězec
symbolů	symbol	k1gInPc2	symbol
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
stavu	stav	k1gInSc3	stav
pásky	pásek	k1gInPc4	pásek
a	a	k8xC	a
aktuální	aktuální	k2eAgInSc4d1	aktuální
stav	stav	k1gInSc4	stav
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vloží	vložit	k5eAaPmIp3nP	vložit
před	před	k7c4	před
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
například	například	k6eAd1	například
páska	páska	k1gFnSc1	páska
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1234	[number]	k4	1234
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
symbolu	symbol	k1gInSc6	symbol
2	[number]	k4	2
<g/>
,	,	kIx,	,
zapíše	zapsat	k5eAaPmIp3nS	zapsat
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
konfigurace	konfigurace	k1gFnSc1	konfigurace
jako	jako	k9	jako
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
234	[number]	k4	234
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
konfigurace	konfigurace	k1gFnSc1	konfigurace
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konfigurace	konfigurace	k1gFnSc1	konfigurace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
<g/>
wb	wb	k?	wb
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
kompaktním	kompaktní	k2eAgInSc6d1	kompaktní
zápisu	zápis	k1gInSc6	zápis
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
sw	sw	k?	sw
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočet	výpočet	k1gInSc1	výpočet
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
výpočtu	výpočet	k1gInSc2	výpočet
je	být	k5eAaImIp3nS	být
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
konfiguraci	konfigurace	k1gFnSc6	konfigurace
a	a	k8xC	a
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
je	být	k5eAaImIp3nS	být
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
vstupní	vstupní	k2eAgNnSc1d1	vstupní
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
krocích	krok	k1gInPc6	krok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgInSc1d1	aktuální
stav	stav	k1gInSc1	stav
zároveň	zároveň	k6eAd1	zároveň
stavem	stav	k1gInSc7	stav
koncovým	koncový	k2eAgInSc7d1	koncový
<g/>
,	,	kIx,	,
výpočet	výpočet	k1gInSc1	výpočet
končí	končit	k5eAaImIp3nS	končit
</s>
</p>
<p>
<s>
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
přečte	přečíst	k5eAaPmIp3nS	přečíst
jeden	jeden	k4xCgInSc1	jeden
vstupní	vstupní	k2eAgInSc1d1	vstupní
symbol	symbol	k1gInSc1	symbol
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
právě	právě	k9	právě
nachází	nacházet	k5eAaImIp3nS	nacházet
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přechodové	přechodový	k2eAgFnSc6d1	přechodová
funkci	funkce	k1gFnSc6	funkce
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgInSc4d1	aktuální
stav	stav	k1gInSc4	stav
a	a	k8xC	a
pro	pro	k7c4	pro
přečtený	přečtený	k2eAgInSc4d1	přečtený
symbol	symbol	k1gInSc4	symbol
definovaný	definovaný	k2eAgInSc4d1	definovaný
přechod	přechod	k1gInSc4	přechod
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
více	hodně	k6eAd2	hodně
možných	možný	k2eAgInPc2d1	možný
přechodů	přechod	k1gInPc2	přechod
u	u	k7c2	u
nedeterministických	deterministický	k2eNgInPc2d1	nedeterministický
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
jeden	jeden	k4xCgInSc1	jeden
náhodně	náhodně	k6eAd1	náhodně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
stav	stav	k1gInSc1	stav
</s>
</p>
<p>
<s>
na	na	k7c6	na
aktuální	aktuální	k2eAgFnSc6d1	aktuální
pozici	pozice	k1gFnSc6	pozice
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
příslušný	příslušný	k2eAgInSc1d1	příslušný
symbol	symbol	k1gInSc1	symbol
</s>
</p>
<p>
<s>
hlava	hlava	k1gFnSc1	hlava
se	s	k7c7	s
příslušným	příslušný	k2eAgInSc7d1	příslušný
způsobem	způsob	k1gInSc7	způsob
posune	posunout	k5eAaPmIp3nS	posunout
(	(	kIx(	(
<g/>
či	či	k8xC	či
neposune	posunout	k5eNaPmIp3nS	posunout
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Neformální	formální	k2eNgInSc1d1	neformální
popis	popis	k1gInSc1	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Neformálně	formálně	k6eNd1	formálně
řečeno	říct	k5eAaPmNgNnS	říct
je	být	k5eAaImIp3nS	být
TS	ts	k0	ts
primitivní	primitivní	k2eAgInSc4d1	primitivní
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
nejjednoduššími	jednoduchý	k2eAgFnPc7d3	nejjednodušší
instrukcemi	instrukce	k1gFnPc7	instrukce
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
zapisovací	zapisovací	k2eAgFnSc7d1	zapisovací
páskou	páska	k1gFnSc7	páska
<g/>
,	,	kIx,	,
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
TS	ts	k0	ts
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
co	co	k3yInSc1	co
do	do	k7c2	do
síly	síla	k1gFnSc2	síla
výpočtu	výpočet	k1gInSc2	výpočet
ekvivalentní	ekvivalentní	k2eAgMnSc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
modely	model	k1gInPc1	model
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
kvůli	kvůli	k7c3	kvůli
jednodušší	jednoduchý	k2eAgFnSc3d2	jednodušší
demonstraci	demonstrace	k1gFnSc3	demonstrace
daných	daný	k2eAgInPc2d1	daný
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
model	model	k1gInSc4	model
můžeme	moct	k5eAaImIp1nP	moct
považovat	považovat	k5eAaImF	považovat
ten	ten	k3xDgInSc4	ten
s	s	k7c7	s
jednostranně	jednostranně	k6eAd1	jednostranně
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
páskou	páska	k1gFnSc7	páska
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
TS	ts	k0	ts
poté	poté	k6eAd1	poté
postupuje	postupovat	k5eAaImIp3nS	postupovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
konečný	konečný	k2eAgInSc1d1	konečný
automat	automat	k1gInSc1	automat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
zásadním	zásadní	k2eAgInSc7d1	zásadní
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nekonečně	konečně	k6eNd1	konečně
velkou	velký	k2eAgFnSc4d1	velká
pásku	páska	k1gFnSc4	páska
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
může	moct	k5eAaImIp3nS	moct
zapisovat	zapisovat	k5eAaImF	zapisovat
a	a	k8xC	a
libovolně	libovolně	k6eAd1	libovolně
se	se	k3xPyFc4	se
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
definujeme	definovat	k5eAaBmIp1nP	definovat
stavy	stav	k1gInPc4	stav
a	a	k8xC	a
přechodové	přechodový	k2eAgFnPc4d1	přechodová
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
přijímající	přijímající	k2eAgInSc4d1	přijímající
a	a	k8xC	a
zamítající	zamítající	k2eAgInSc4d1	zamítající
stav	stav	k1gInSc4	stav
a	a	k8xC	a
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yIgFnSc7	který
TS	ts	k0	ts
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
na	na	k7c6	na
pásku	pásek	k1gInSc6	pásek
TS	ts	k0	ts
vložíme	vložit	k5eAaPmIp1nP	vložit
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
TS	ts	k0	ts
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
můžeme	moct	k5eAaImIp1nP	moct
po	po	k7c6	po
TS	ts	k0	ts
chtít	chtít	k5eAaImF	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nám	my	k3xPp1nPc3	my
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
symboly	symbol	k1gInPc7	symbol
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
můžeme	moct	k5eAaImIp1nP	moct
realizovat	realizovat	k5eAaBmF	realizovat
nějaký	nějaký	k3yIgInSc4	nějaký
výpočet	výpočet	k1gInSc4	výpočet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
násobení	násobený	k2eAgMnPc1d1	násobený
<g/>
.	.	kIx.	.
</s>
<s>
TS	ts	k0	ts
může	moct	k5eAaImIp3nS	moct
ukončit	ukončit	k5eAaPmF	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
výpočet	výpočet	k1gInSc4	výpočet
třemi	tři	k4xCgInPc7	tři
možnými	možný	k2eAgInPc7d1	možný
stavy	stav	k1gInPc7	stav
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
vstupní	vstupní	k2eAgNnSc1d1	vstupní
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
TS	ts	k0	ts
slovo	slovo	k1gNnSc4	slovo
přijme	přijmout	k5eAaPmIp3nS	přijmout
(	(	kIx(	(
<g/>
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
<g/>
,	,	kIx,	,
TS	ts	k0	ts
slovo	slovo	k1gNnSc4	slovo
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
TS	ts	k0	ts
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
TS	ts	k0	ts
bude	být	k5eAaImBp3nS	být
donekonečna	donekonečna	k6eAd1	donekonečna
přecházet	přecházet	k5eAaImF	přecházet
mezi	mezi	k7c7	mezi
nějakými	nějaký	k3yIgInPc7	nějaký
stavy	stav	k1gInPc7	stav
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
tedy	tedy	k9	tedy
neumí	umět	k5eNaImIp3nS	umět
vyřešit	vyřešit	k5eAaPmF	vyřešit
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příklady	příklad	k1gInPc1	příklad
a	a	k8xC	a
důkazy	důkaz	k1gInPc1	důkaz
budou	být	k5eAaImBp3nP	být
následovat	následovat	k5eAaImF	následovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
Napíšeme	napsat	k5eAaPmIp1nP	napsat
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
TS	ts	k0	ts
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
slovo	slovo	k1gNnSc1	slovo
napsané	napsaný	k2eAgNnSc1d1	napsané
na	na	k7c6	na
pásku	pásek	k1gInSc6	pásek
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
0	[number]	k4	0
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
řetězec	řetězec	k1gInSc1	řetězec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
n	n	k0	n
nul	nula	k1gFnPc2	nula
a	a	k8xC	a
následně	následně	k6eAd1	následně
n	n	k0	n
jedniček	jednička	k1gFnPc2	jednička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
konečný	konečný	k2eAgInSc1d1	konečný
automat	automat	k1gInSc1	automat
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
řešit	řešit	k5eAaImF	řešit
neumí	umět	k5eNaImIp3nS	umět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zásobníkový	zásobníkový	k2eAgInSc1d1	zásobníkový
automat	automat	k1gInSc1	automat
to	ten	k3xDgNnSc4	ten
již	již	k6eAd1	již
řešit	řešit	k5eAaImF	řešit
umí	umět	k5eAaImIp3nS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Ukážeme	ukázat	k5eAaPmIp1nP	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
vypadal	vypadat	k5eAaImAgInS	vypadat
TS	ts	k0	ts
M	M	kA	M
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
vždy	vždy	k6eAd1	vždy
vymaže	vymazat	k5eAaPmIp3nS	vymazat
počáteční	počáteční	k2eAgFnSc1d1	počáteční
0	[number]	k4	0
<g/>
,	,	kIx,	,
přesune	přesunout	k5eAaPmIp3nS	přesunout
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c4	na
konec	konec	k1gInSc4	konec
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
vymaže	vymazat	k5eAaPmIp3nS	vymazat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
poslední	poslední	k2eAgFnSc4d1	poslední
1	[number]	k4	1
a	a	k8xC	a
přesune	přesunout	k5eAaPmIp3nS	přesunout
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
slovo	slovo	k1gNnSc1	slovo
nevymaže	vymazat	k5eNaPmIp3nS	vymazat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nedostane	dostat	k5eNaPmIp3nS	dostat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
další	další	k2eAgInSc1d1	další
krok	krok	k1gInSc1	krok
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
slovo	slovo	k1gNnSc1	slovo
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formální	formální	k2eAgInSc1d1	formální
zápis	zápis	k1gInSc1	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnPc4	Gammum
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
počáteční	počáteční	k2eAgInSc1d1	počáteční
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Přechodová	přechodový	k2eAgFnSc1d1	přechodová
funkce	funkce	k1gFnSc1	funkce
by	by	kYmCp3nS	by
vypadala	vypadat	k5eAaImAgFnS	vypadat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vymažeme	vymazat	k5eAaPmIp1nP	vymazat
počáteční	počáteční	k2eAgFnSc4d1	počáteční
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
zahájíme	zahájit	k5eAaPmIp1nP	zahájit
přesun	přesun	k1gInSc4	přesun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přesouváme	přesouvat	k5eAaImIp1nP	přesouvat
hlavu	hlava	k1gFnSc4	hlava
doprava	doprava	k6eAd1	doprava
přes	přes	k7c4	přes
nuly	nula	k1gFnPc4	nula
<g/>
,	,	kIx,	,
pásku	páska	k1gFnSc4	páska
neměníme	měnit	k5eNaImIp1nP	měnit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
narazili	narazit	k5eAaPmAgMnP	narazit
jsme	být	k5eAaImIp1nP	být
na	na	k7c4	na
první	první	k4xOgFnSc4	první
jedničku	jednička	k1gFnSc4	jednička
<g/>
,	,	kIx,	,
měníme	měnit	k5eAaImIp1nP	měnit
stav	stav	k1gInSc4	stav
a	a	k8xC	a
pokračujeme	pokračovat	k5eAaImIp1nP	pokračovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přesouváme	přesouvat	k5eAaImIp1nP	přesouvat
hlavu	hlava	k1gFnSc4	hlava
doprava	doprava	k6eAd1	doprava
přes	přes	k7c4	přes
jedničky	jednička	k1gFnPc4	jednička
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
za	za	k7c7	za
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
vrátíme	vrátit	k5eAaPmIp1nP	vrátit
se	se	k3xPyFc4	se
před	před	k7c4	před
poslední	poslední	k2eAgInSc4d1	poslední
symbol	symbol	k1gInSc4	symbol
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vymažeme	vymazat	k5eAaPmIp1nP	vymazat
poslední	poslední	k2eAgFnSc4d1	poslední
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
přesuneme	přesunout	k5eAaPmIp1nP	přesunout
se	se	k3xPyFc4	se
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
znak	znak	k1gInSc4	znak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
kroku	krok	k1gInSc6	krok
6	[number]	k4	6
vymazali	vymazat	k5eAaPmAgMnP	vymazat
poslední	poslední	k2eAgInSc4d1	poslední
symbol	symbol	k1gInSc4	symbol
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
algoritmus	algoritmus	k1gInSc1	algoritmus
končí	končit	k5eAaImIp3nS	končit
přechodem	přechod	k1gInSc7	přechod
do	do	k7c2	do
koncového	koncový	k2eAgInSc2d1	koncový
stavu	stav	k1gInSc2	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jinak	jinak	k6eAd1	jinak
zahájíme	zahájit	k5eAaPmIp1nP	zahájit
přesun	přesun	k1gInSc4	přesun
doleva	doleva	k6eAd1	doleva
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
,1	,1	k4	,1
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přesouváme	přesouvat	k5eAaImIp1nP	přesouvat
hlavu	hlava	k1gFnSc4	hlava
doleva	doleva	k6eAd1	doleva
přes	přes	k7c4	přes
jedničky	jednička	k1gFnPc4	jednička
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
narazili	narazit	k5eAaPmAgMnP	narazit
jsme	být	k5eAaImIp1nP	být
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
měníme	měnit	k5eAaImIp1nP	měnit
stav	stav	k1gInSc4	stav
a	a	k8xC	a
pokračujeme	pokračovat	k5eAaImIp1nP	pokračovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
doleva	doleva	k6eAd1	doleva
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přesouváme	přesouvat	k5eAaImIp1nP	přesouvat
hlavu	hlava	k1gFnSc4	hlava
doleva	doleva	k6eAd1	doleva
přes	přes	k7c4	přes
nuly	nula	k1gFnPc4	nula
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dostali	dostat	k5eAaPmAgMnP	dostat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
před	před	k7c4	před
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
vrátíme	vrátit	k5eAaPmIp1nP	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
znak	znak	k1gInSc4	znak
a	a	k8xC	a
začínáme	začínat	k5eAaImIp1nP	začínat
zase	zase	k9	zase
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
znaky	znak	k1gInPc4	znak
kratšíNěkolik	kratšíNěkolika	k1gFnPc2	kratšíNěkolika
poznámek	poznámka	k1gFnPc2	poznámka
k	k	k7c3	k
algoritmu	algoritmus	k1gInSc3	algoritmus
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
libovolně	libovolně	k6eAd1	libovolně
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c4	po
pásce	pásec	k1gInPc4	pásec
tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
přečte	přečíst	k5eAaPmIp3nS	přečíst
symbol	symbol	k1gInSc4	symbol
A	a	k9	a
<g/>
,	,	kIx,	,
zapíše	zapsat	k5eAaPmIp3nS	zapsat
(	(	kIx(	(
<g/>
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
)	)	kIx)	)
symbol	symbol	k1gInSc4	symbol
A	a	k9	a
a	a	k8xC	a
posune	posunout	k5eAaPmIp3nS	posunout
se	se	k3xPyFc4	se
doleva	doleva	k6eAd1	doleva
nebo	nebo	k8xC	nebo
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
hlava	hlava	k1gFnSc1	hlava
může	moct	k5eAaImIp3nS	moct
posunout	posunout	k5eAaPmF	posunout
o	o	k7c4	o
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
cokoliv	cokoliv	k3yInSc4	cokoliv
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
kroků	krok	k1gInPc2	krok
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kroků	krok	k1gInPc2	krok
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
pásky	páska	k1gFnSc2	páska
mění	měnit	k5eAaImIp3nS	měnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
vymazání	vymazání	k1gNnSc2	vymazání
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zpracované	zpracovaný	k2eAgInPc1d1	zpracovaný
symboly	symbol	k1gInPc1	symbol
0	[number]	k4	0
a	a	k8xC	a
1	[number]	k4	1
"	"	kIx"	"
<g/>
škrtnout	škrtnout	k5eAaPmF	škrtnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nahradit	nahradit	k5eAaPmF	nahradit
speciálním	speciální	k2eAgInSc7d1	speciální
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
páskové	páskový	k2eAgFnSc2d1	pásková
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
posledním	poslední	k2eAgInSc7d1	poslední
symbolem	symbol	k1gInSc7	symbol
ze	z	k7c2	z
vstupního	vstupní	k2eAgNnSc2d1	vstupní
slova	slovo	k1gNnSc2	slovo
w	w	k?	w
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
prázdné	prázdný	k2eAgInPc4d1	prázdný
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
může	moct	k5eAaImIp3nS	moct
takto	takto	k6eAd1	takto
poznat	poznat	k5eAaPmF	poznat
konec	konec	k1gInSc4	konec
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
slova	slovo	k1gNnSc2	slovo
může	moct	k5eAaImIp3nS	moct
poznat	poznat	k5eAaPmF	poznat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
například	například	k6eAd1	například
přidat	přidat	k5eAaPmF	přidat
speciální	speciální	k2eAgInSc4d1	speciální
symbol	symbol	k1gInSc4	symbol
před	před	k7c4	před
začátek	začátek	k1gInSc4	začátek
slova	slovo	k1gNnSc2	slovo
w	w	k?	w
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
označí	označit	k5eAaPmIp3nS	označit
začátek	začátek	k1gInSc4	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
<g/>
Ukázka	ukázka	k1gFnSc1	ukázka
výpočtu	výpočet	k1gInSc2	výpočet
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Zapíšeme	zapsat	k5eAaPmIp1nP	zapsat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
posloupnost	posloupnost	k1gFnSc4	posloupnost
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgFnPc2d1	následující
konfigurací	konfigurace	k1gFnSc7	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
šipkou	šipka	k1gFnSc7	šipka
představující	představující	k2eAgInSc4d1	představující
jeden	jeden	k4xCgInSc4	jeden
krok	krok	k1gInSc4	krok
výpočtu	výpočet	k1gInSc2	výpočet
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
číslo	číslo	k1gNnSc1	číslo
použitého	použitý	k2eAgNnSc2d1	Použité
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0011	[number]	k4	0011
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
011	[number]	k4	011
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
011	[number]	k4	011
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
dospěl	dochvít	k5eAaPmAgInS	dochvít
do	do	k7c2	do
koncového	koncový	k2eAgInSc2d1	koncový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
výpočet	výpočet	k1gInSc1	výpočet
končí	končit	k5eAaImIp3nS	končit
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
výpočtu	výpočet	k1gInSc2	výpočet
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
001	[number]	k4	001
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Ale	ale	k9	ale
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
<g/>
,	,	kIx,	,
výpočet	výpočet	k1gInSc1	výpočet
končí	končit	k5eAaImIp3nS	končit
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stavu	stav	k1gInSc2	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
náš	náš	k3xOp1gInSc1	náš
stroj	stroj	k1gInSc1	stroj
dostane	dostat	k5eAaPmIp3nS	dostat
po	po	k7c6	po
vymazání	vymazání	k1gNnSc6	vymazání
poslední	poslední	k2eAgInSc1d1	poslední
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
nastat	nastat	k5eAaPmF	nastat
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
případy	případ	k1gInPc4	případ
−	−	k?	−
buď	budit	k5eAaImRp2nS	budit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
slovo	slovo	k1gNnSc1	slovo
vyprázdnilo	vyprázdnit	k5eAaPmAgNnS	vyprázdnit
(	(	kIx(	(
<g/>
instrukce	instrukce	k1gFnSc1	instrukce
7	[number]	k4	7
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ještě	ještě	k9	ještě
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
zbývá	zbývat	k5eAaImIp3nS	zbývat
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
končit	končit	k5eAaImF	končit
jedničkami	jednička	k1gFnPc7	jednička
(	(	kIx(	(
<g/>
instrukce	instrukce	k1gFnSc1	instrukce
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
výpočtu	výpočet	k1gInSc2	výpočet
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
0	[number]	k4	0
<g/>
101	[number]	k4	101
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0101	[number]	k4	0101
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
101	[number]	k4	101
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
101	[number]	k4	101
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
101	[number]	k4	101
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
01	[number]	k4	01
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
opět	opět	k6eAd1	opět
končí	končit	k5eAaImIp3nS	končit
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přechodová	přechodový	k2eAgFnSc1d1	přechodová
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korektním	korektní	k2eAgNnSc6d1	korektní
slově	slovo	k1gNnSc6	slovo
řetězec	řetězec	k1gInSc1	řetězec
jedniček	jednička	k1gFnPc2	jednička
zahájený	zahájený	k2eAgMnSc1d1	zahájený
za	za	k7c4	za
první	první	k4xOgFnSc7	první
nulou	nula	k1gFnSc7	nula
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
tedy	tedy	k9	tedy
stroj	stroj	k1gInSc1	stroj
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
najít	najít	k5eAaPmF	najít
jen	jen	k9	jen
symboly	symbol	k1gInPc4	symbol
1	[number]	k4	1
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Modifikace	modifikace	k1gFnSc1	modifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgFnPc2d1	různá
modifikací	modifikace	k1gFnPc2	modifikace
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
přijímají	přijímat	k5eAaImIp3nP	přijímat
stejnou	stejný	k2eAgFnSc4d1	stejná
třídu	třída	k1gFnSc4	třída
jazyků	jazyk	k1gInPc2	jazyk
jako	jako	k8xS	jako
základní	základní	k2eAgInSc4d1	základní
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TS	ts	k0	ts
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
provedení	provedení	k1gNnSc2	provedení
výpočtu	výpočet	k1gInSc2	výpočet
bez	bez	k7c2	bez
posunu	posun	k1gInSc2	posun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
</s>
</p>
<p>
<s>
přechodová	přechodový	k2eAgFnSc1d1	přechodová
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
zůstane	zůstat	k5eAaPmIp3nS	zůstat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
:	:	kIx,	:
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
Idea	idea	k1gFnSc1	idea
důkazu	důkaz	k1gInSc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výpočetně	výpočetně	k6eAd1	výpočetně
stejně	stejně	k6eAd1	stejně
silný	silný	k2eAgInSc1d1	silný
TS	ts	k0	ts
<g/>
:	:	kIx,	:
posun	posun	k1gInSc1	posun
N	N	kA	N
(	(	kIx(	(
<g/>
žádný	žádný	k3yNgInSc1	žádný
posun	posun	k1gInSc1	posun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
běžném	běžný	k2eAgInSc6d1	běžný
TS	ts	k0	ts
zařídit	zařídit	k5eAaPmF	zařídit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
přečte	přečíst	k5eAaPmIp3nS	přečíst
vstupní	vstupní	k2eAgInSc4d1	vstupní
symbol	symbol	k1gInSc4	symbol
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
posune	posunout	k5eAaPmIp3nS	posunout
se	se	k3xPyFc4	se
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
opět	opět	k6eAd1	opět
přečte	přečíst	k5eAaPmIp3nS	přečíst
symbol	symbol	k1gInSc1	symbol
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
zapíše	zapsat	k5eAaPmIp3nS	zapsat
tam	tam	k6eAd1	tam
stejný	stejný	k2eAgInSc1d1	stejný
symbol	symbol	k1gInSc1	symbol
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nezmění	změnit	k5eNaPmIp3nS	změnit
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
hlava	hlava	k1gFnSc1	hlava
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
)	)	kIx)	)
a	a	k8xC	a
posune	posunout	k5eAaPmIp3nS	posunout
se	se	k3xPyFc4	se
doleva	doleva	k6eAd1	doleva
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
jsme	být	k5eAaImIp1nP	být
dostali	dostat	k5eAaPmAgMnP	dostat
čtecí	čtecí	k2eAgFnSc4d1	čtecí
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
předchozího	předchozí	k2eAgNnSc2d1	předchozí
umístění	umístění	k1gNnSc2	umístění
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
symbol	symbol	k1gInSc1	symbol
nebyl	být	k5eNaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
máme	mít	k5eAaImIp1nP	mít
jako	jako	k9	jako
výchozí	výchozí	k2eAgFnSc4d1	výchozí
TS	ts	k0	ts
s	s	k7c7	s
levou	levý	k2eAgFnSc7d1	levá
zarážkou	zarážka	k1gFnSc7	zarážka
(	(	kIx(	(
<g/>
páska	páska	k1gFnSc1	páska
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
jen	jen	k9	jen
zprava	zprava	k6eAd1	zprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
posouvat	posouvat	k5eAaImF	posouvat
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
posunuli	posunout	k5eAaPmAgMnP	posunout
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
by	by	kYmCp3nS	by
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
symbolu	symbol	k1gInSc6	symbol
<g/>
,	,	kIx,	,
posunula	posunout	k5eAaPmAgFnS	posunout
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kroku	krok	k1gInSc6	krok
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
<g/>
TS	ts	k0	ts
s	s	k7c7	s
oboustranně	oboustranně	k6eAd1	oboustranně
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
páskou	páska	k1gFnSc7	páska
</s>
</p>
<p>
<s>
páska	páska	k1gFnSc1	páska
není	být	k5eNaImIp3nS	být
zleva	zleva	k6eAd1	zleva
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
</s>
</p>
<p>
<s>
možný	možný	k2eAgInSc1d1	možný
posun	posun	k1gInSc1	posun
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
doleva	doleva	k6eAd1	doleva
z	z	k7c2	z
libovolné	libovolný	k2eAgFnSc2d1	libovolná
konfigurace	konfigurace	k1gFnSc2	konfigurace
</s>
</p>
<p>
<s>
Idea	idea	k1gFnSc1	idea
důkazu	důkaz	k1gInSc2	důkaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
běžném	běžný	k2eAgInSc6d1	běžný
TS	ts	k0	ts
(	(	kIx(	(
<g/>
označme	označit	k5eAaPmRp1nP	označit
TS	ts	k0	ts
<g/>
)	)	kIx)	)
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
nasimulovat	nasimulovat	k5eAaPmF	nasimulovat
TS	ts	k0	ts
s	s	k7c7	s
oboustranně	oboustranně	k6eAd1	oboustranně
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
páskou	páska	k1gFnSc7	páska
(	(	kIx(	(
<g/>
označme	označit	k5eAaPmRp1nP	označit
TS	ts	k0	ts
<g/>
∞	∞	k?	∞
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
TS	ts	k0	ts
si	se	k3xPyFc3	se
zvolíme	zvolit	k5eAaPmIp1nP	zvolit
takový	takový	k3xDgInSc4	takový
symbol	symbol	k1gInSc4	symbol
D	D	kA	D
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnSc2	Gammum
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
symbol	symbol	k1gInSc1	symbol
nachází	nacházet	k5eAaImIp3nS	nacházet
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
vstupního	vstupní	k2eAgNnSc2d1	vstupní
slova	slovo	k1gNnSc2	slovo
w.	w.	k?	w.
Nyní	nyní	k6eAd1	nyní
nadefinujeme	nadefinovat	k5eAaPmIp1nP	nadefinovat
TS	ts	k0	ts
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
na	na	k7c4	na
symbol	symbol	k1gInSc4	symbol
D	D	kA	D
<g/>
,	,	kIx,	,
posune	posunout	k5eAaPmIp3nS	posunout
celý	celý	k2eAgInSc4d1	celý
obsah	obsah	k1gInSc4	obsah
pásky	páska	k1gFnSc2	páska
napravo	napravo	k6eAd1	napravo
od	od	k7c2	od
symbolu	symbol	k1gInSc2	symbol
D	D	kA	D
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
buňku	buňka	k1gFnSc4	buňka
doprava	doprava	k6eAd1	doprava
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nP	vrátit
čtecí	čtecí	k2eAgFnSc4d1	čtecí
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c4	na
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mezi	mezi	k7c7	mezi
obsahem	obsah	k1gInSc7	obsah
pásky	páska	k1gFnSc2	páska
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
D.	D.	kA	D.
Tím	ten	k3xDgNnSc7	ten
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
funkčnosti	funkčnost	k1gFnPc1	funkčnost
TS	ts	k0	ts
<g/>
∞	∞	k?	∞
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Zlomíme	zlomit	k5eAaPmIp1nP	zlomit
TS	ts	k0	ts
<g/>
∞	∞	k?	∞
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nám	my	k3xPp1nPc3	my
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dva	dva	k4xCgInPc4	dva
TS	ts	k0	ts
s	s	k7c7	s
zleva	zleva	k6eAd1	zleva
omezenou	omezený	k2eAgFnSc7d1	omezená
páskou	páska	k1gFnSc7	páska
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
pásky	páska	k1gFnPc1	páska
dáme	dát	k5eAaPmIp1nP	dát
pod	pod	k7c4	pod
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
nadefinujeme	nadefinovat	k5eAaPmIp1nP	nadefinovat
přechody	přechod	k1gInPc1	přechod
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
když	když	k8xS	když
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c4	na
levý	levý	k2eAgInSc4d1	levý
konec	konec	k1gInSc4	konec
horní	horní	k2eAgFnSc2d1	horní
pásky	páska	k1gFnSc2	páska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
posun	posun	k1gInSc4	posun
doleva	doleva	k6eAd1	doleva
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
pásku	páska	k1gFnSc4	páska
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
levý	levý	k2eAgInSc4d1	levý
okraj	okraj	k1gInSc4	okraj
spodní	spodní	k2eAgFnSc2d1	spodní
pásky	páska	k1gFnSc2	páska
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
chtít	chtít	k5eAaImF	chtít
jít	jít	k5eAaImF	jít
doprava	doprava	k6eAd1	doprava
(	(	kIx(	(
<g/>
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
stopě	stopa	k1gFnSc6	stopa
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
směry	směr	k1gInPc4	směr
prohozené	prohozený	k2eAgInPc4d1	prohozený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
pásku	páska	k1gFnSc4	páska
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Okraj	okraj	k1gInSc4	okraj
pásky	páska	k1gFnSc2	páska
poznáme	poznat	k5eAaPmIp1nP	poznat
podle	podle	k7c2	podle
pomocného	pomocný	k2eAgInSc2d1	pomocný
symbolu	symbol	k1gInSc2	symbol
D	D	kA	D
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
<g/>
n-páskový	náskový	k2eAgInSc4d1	n-páskový
TS	ts	k0	ts
</s>
</p>
<p>
<s>
čte	číst	k5eAaImIp3nS	číst
z	z	k7c2	z
a	a	k8xC	a
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
do	do	k7c2	do
více	hodně	k6eAd2	hodně
pásek	pásek	k1gInSc1	pásek
najednou	najednou	k6eAd1	najednou
</s>
</p>
<p>
<s>
jediná	jediný	k2eAgFnSc1d1	jediná
změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přechodové	přechodový	k2eAgFnSc6d1	přechodová
funkci	funkce	k1gFnSc6	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
:	:	kIx,	:
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nedeterministický	deterministický	k2eNgInSc1d1	nedeterministický
TS	ts	k0	ts
(	(	kIx(	(
<g/>
NTS	NTS	kA	NTS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
"	"	kIx"	"
<g/>
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnPc2	možnost
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
:	:	kIx,	:
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Symbol	symbol	k1gInSc4	symbol
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
značí	značit	k5eAaImIp3nS	značit
potenční	potenční	k2eAgFnSc4d1	potenční
množinu	množina	k1gFnSc4	množina
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Idea	idea	k1gFnSc1	idea
důkazu	důkaz	k1gInSc2	důkaz
<g/>
:	:	kIx,	:
NTS	NTS	kA	NTS
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Kořenem	kořen	k1gInSc7	kořen
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgFnSc1d1	počáteční
konfigurace	konfigurace	k1gFnSc1	konfigurace
NTS	NTS	kA	NTS
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
větev	větev	k1gFnSc1	větev
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
uzlu	uzel	k1gInSc2	uzel
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
může	moct	k5eAaImIp3nS	moct
NTS	NTS	kA	NTS
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
NTS	NTS	kA	NTS
přečíst	přečíst	k5eAaPmF	přečíst
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
zapsat	zapsat	k5eAaPmF	zapsat
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
nebo	nebo	k8xC	nebo
přečíst	přečíst	k5eAaPmF	přečíst
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
zapsat	zapsat	k5eAaPmF	zapsat
jedničku	jednička	k1gFnSc4	jednička
<g/>
,	,	kIx,	,
povedou	povést	k5eAaPmIp3nP	povést
z	z	k7c2	z
uzlu	uzel	k1gInSc6	uzel
dvě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
tyto	tento	k3xDgFnPc4	tento
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
NTS	NTS	kA	NTS
vybere	vybrat	k5eAaPmIp3nS	vybrat
vždy	vždy	k6eAd1	vždy
tu	ten	k3xDgFnSc4	ten
správnou	správný	k2eAgFnSc4d1	správná
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Deterministicky	deterministicky	k6eAd1	deterministicky
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
tento	tento	k3xDgInSc4	tento
stroj	stroj	k1gInSc4	stroj
naprogramovat	naprogramovat	k5eAaPmF	naprogramovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
projdeme	projít	k5eAaPmIp1nP	projít
všechny	všechen	k3xTgFnPc1	všechen
větve	větev	k1gFnPc1	větev
NTS	NTS	kA	NTS
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
může	moct	k5eAaImIp3nS	moct
NTS	NTS	kA	NTS
vybírat	vybírat	k5eAaImF	vybírat
a	a	k8xC	a
když	když	k8xS	když
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
akceptující	akceptující	k2eAgFnSc4d1	akceptující
větev	větev	k1gFnSc4	větev
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
akceptující	akceptující	k2eAgInSc1d1	akceptující
uzel	uzel	k1gInSc1	uzel
(	(	kIx(	(
<g/>
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc4	stroj
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
dané	daný	k2eAgNnSc4d1	dané
slovo	slovo	k1gNnSc4	slovo
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
větvích	větev	k1gFnPc6	větev
(	(	kIx(	(
<g/>
listech	list	k1gInPc6	list
<g/>
)	)	kIx)	)
zamítající	zamítající	k2eAgInSc1d1	zamítající
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
slovo	slovo	k1gNnSc1	slovo
zamítá	zamítat	k5eAaImIp3nS	zamítat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
větev	větev	k1gFnSc1	větev
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
a	a	k8xC	a
ostatní	ostatní	k2eAgFnSc1d1	ostatní
zamítající	zamítající	k2eAgFnSc1d1	zamítající
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
cyklí	cyklet	k5eAaImIp3nS	cyklet
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
budeme	být	k5eAaImBp1nP	být
strom	strom	k1gInSc4	strom
procházet	procházet	k5eAaImF	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
projdeme	projít	k5eAaPmIp1nP	projít
strom	strom	k1gInSc4	strom
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
nám	my	k3xPp1nPc3	my
zacyklení	zacyklení	k1gNnSc1	zacyklení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
hned	hned	k6eAd1	hned
první	první	k4xOgFnSc1	první
cesta	cesta	k1gFnSc1	cesta
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
cesty	cesta	k1gFnPc1	cesta
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgFnP	vést
do	do	k7c2	do
přijímajícího	přijímající	k2eAgInSc2d1	přijímající
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
by	by	kYmCp3nP	by
fungoval	fungovat	k5eAaImAgInS	fungovat
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
utopil	utopit	k5eAaPmAgMnS	utopit
hned	hned	k6eAd1	hned
v	v	k7c6	v
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
projdeme	projít	k5eAaPmIp1nP	projít
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
budeme	být	k5eAaImBp1nP	být
po	po	k7c6	po
částech	část	k1gFnPc6	část
procházet	procházet	k5eAaImF	procházet
všechny	všechen	k3xTgFnPc1	všechen
možné	možný	k2eAgFnPc1d1	možná
větve	větev	k1gFnPc1	větev
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
narazíme	narazit	k5eAaPmIp1nP	narazit
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
zamítající	zamítající	k2eAgInPc4d1	zamítající
a	a	k8xC	a
přijímající	přijímající	k2eAgInPc4d1	přijímající
stavy	stav	k1gInPc4	stav
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
nám	my	k3xPp1nPc3	my
ale	ale	k9	ale
stačí	stačit	k5eAaBmIp3nS	stačit
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
nám	my	k3xPp1nPc3	my
ale	ale	k9	ale
nezaručí	zaručit	k5eNaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyhneme	vyhnout	k5eAaPmIp1nP	vyhnout
cyklům	cyklus	k1gInPc3	cyklus
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
žádný	žádný	k3yNgInSc1	žádný
přijímající	přijímající	k2eAgInSc1d1	přijímající
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
<g/>
-li	i	k?	-li
již	již	k6eAd1	již
poslední	poslední	k2eAgFnSc4d1	poslední
větev	větev	k1gFnSc4	větev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
cesty	cesta	k1gFnPc1	cesta
vedly	vést	k5eAaImAgFnP	vést
do	do	k7c2	do
zamítajícího	zamítající	k2eAgInSc2d1	zamítající
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
NTS	NTS	kA	NTS
se	se	k3xPyFc4	se
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
TS	ts	k0	ts
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
NTS	NTS	kA	NTS
silnější	silný	k2eAgInSc1d2	silnější
nástroj	nástroj	k1gInSc1	nástroj
než	než	k8xS	než
TS	ts	k0	ts
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
přijímají	přijímat	k5eAaImIp3nP	přijímat
stejnou	stejný	k2eAgFnSc4d1	stejná
množinu	množina	k1gFnSc4	množina
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Subrutiny	subrutina	k1gFnSc2	subrutina
==	==	k?	==
</s>
</p>
<p>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
složitějších	složitý	k2eAgInPc2d2	složitější
TS	ts	k0	ts
není	být	k5eNaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
záležitost	záležitost	k1gFnSc1	záležitost
a	a	k8xC	a
subrutiny	subrutina	k1gFnPc1	subrutina
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
usnadnění	usnadnění	k1gNnSc3	usnadnění
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Subrutina	subrutina	k1gFnSc1	subrutina
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
počáteční	počáteční	k2eAgInSc4d1	počáteční
a	a	k8xC	a
koncový	koncový	k2eAgInSc4d1	koncový
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
řeší	řešit	k5eAaImIp3nS	řešit
nějaký	nějaký	k3yIgInSc1	nějaký
dílčí	dílčí	k2eAgInSc1d1	dílčí
problém	problém	k1gInSc1	problém
v	v	k7c6	v
TS	ts	k0	ts
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
programování	programování	k1gNnSc6	programování
má	mít	k5eAaImIp3nS	mít
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
nějaké	nějaký	k3yIgInPc4	nějaký
vstupní	vstupní	k2eAgInPc4d1	vstupní
a	a	k8xC	a
výstupní	výstupní	k2eAgInPc4d1	výstupní
stavy	stav	k1gInPc4	stav
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
z	z	k7c2	z
pravidla	pravidlo	k1gNnSc2	pravidlo
nějaký	nějaký	k3yIgInSc1	nějaký
dílčí	dílčí	k2eAgInSc1d1	dílčí
problém	problém	k1gInSc1	problém
celého	celý	k2eAgInSc2d1	celý
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
příkladě	příklad	k1gInSc6	příklad
kontroly	kontrola	k1gFnSc2	kontrola
0	[number]	k4	0
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
n	n	k0	n
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
subrutina	subrutina	k1gFnSc1	subrutina
například	například	k6eAd1	například
vrácení	vrácení	k1gNnSc4	vrácení
čtecí	čtecí	k2eAgFnSc2d1	čtecí
hlavy	hlava	k1gFnSc2	hlava
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
nebo	nebo	k8xC	nebo
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
už	už	k6eAd1	už
máme	mít	k5eAaImIp1nP	mít
označena	označit	k5eAaPmNgNnP	označit
všechna	všechen	k3xTgNnPc4	všechen
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zakódování	zakódování	k1gNnSc6	zakódování
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
můžeme	moct	k5eAaImIp1nP	moct
zakódovat	zakódovat	k5eAaPmF	zakódovat
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
TS	ts	k0	ts
zakódovat	zakódovat	k5eAaPmF	zakódovat
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
například	například	k6eAd1	například
očíslovat	očíslovat	k5eAaPmF	očíslovat
všechny	všechen	k3xTgInPc4	všechen
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
očíslovat	očíslovat	k5eAaPmF	očíslovat
všechny	všechen	k3xTgInPc4	všechen
symboly	symbol	k1gInPc4	symbol
z	z	k7c2	z
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
očíslovat	očíslovat	k5eAaPmF	očíslovat
všechny	všechen	k3xTgInPc4	všechen
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
čtecí	čtecí	k2eAgFnSc1d1	čtecí
hlava	hlava	k1gFnSc1	hlava
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
můžeme	moct	k5eAaImIp1nP	moct
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
q	q	k?	q
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
→	→	k?	→
<g/>
(	(	kIx(	(
<g/>
q	q	k?	q
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
<g/>
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dolní	dolní	k2eAgInPc1d1	dolní
indexy	index	k1gInPc1	index
označují	označovat	k5eAaImIp3nP	označovat
očíslování	očíslování	k1gNnSc4	očíslování
<g/>
,	,	kIx,	,
zakódovat	zakódovat	k5eAaPmF	zakódovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
1001000100000010	[number]	k4	1001000100000010
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
oddělovač	oddělovač	k1gInSc1	oddělovač
jsme	být	k5eAaImIp1nP	být
použili	použít	k5eAaPmAgMnP	použít
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
počet	počet	k1gInSc4	počet
nul	nula	k1gFnPc2	nula
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
rovná	rovnat	k5eAaImIp3nS	rovnat
očíslování	očíslování	k1gNnSc1	očíslování
daného	daný	k2eAgInSc2d1	daný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
přechodových	přechodový	k2eAgFnPc2d1	přechodová
funkcí	funkce	k1gFnPc2	funkce
můžeme	moct	k5eAaImIp1nP	moct
oddělit	oddělit	k5eAaPmF	oddělit
dvěma	dva	k4xCgFnPc7	dva
jedničkami	jednička	k1gFnPc7	jednička
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
přidali	přidat	k5eAaPmAgMnP	přidat
ještě	ještě	k9	ještě
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
q	q	k?	q
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
→	→	k?	→
<g/>
(	(	kIx(	(
<g/>
q	q	k?	q
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
bychom	by	kYmCp1nP	by
řetězec	řetězec	k1gInSc1	řetězec
0	[number]	k4	0
<g/>
1001000100000010110100010001000000100	[number]	k4	1001000100000010110100010001000000100
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
můžeme	moct	k5eAaImIp1nP	moct
zakódovat	zakódovat	k5eAaPmF	zakódovat
všechny	všechen	k3xTgFnPc4	všechen
přechodové	přechodový	k2eAgFnPc4d1	přechodová
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
už	už	k6eAd1	už
jen	jen	k9	jen
označit	označit	k5eAaPmF	označit
startovací	startovací	k2eAgInSc4d1	startovací
<g/>
,	,	kIx,	,
přijímající	přijímající	k2eAgInSc4d1	přijímající
a	a	k8xC	a
ukončovací	ukončovací	k2eAgInSc4d1	ukončovací
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
můžeme	moct	k5eAaImIp1nP	moct
udělat	udělat	k5eAaPmF	udělat
například	například	k6eAd1	například
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
startovací	startovací	k2eAgInSc1d1	startovací
stav	stav	k1gInSc1	stav
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
vždy	vždy	k6eAd1	vždy
index	index	k1gInSc4	index
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
přijímající	přijímající	k2eAgFnSc1d1	přijímající
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
zamítající	zamítající	k2eAgFnSc1d1	zamítající
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
formátu	formát	k1gInSc2	formát
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
zakódovat	zakódovat	k5eAaPmF	zakódovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
TS	ts	k0	ts
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
Turingových	Turingový	k2eAgInPc2d1	Turingový
strojů	stroj	k1gInPc2	stroj
je	být	k5eAaImIp3nS	být
spočetná	spočetný	k2eAgFnSc1d1	spočetná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Univerzální	univerzální	k2eAgFnPc1d1	univerzální
TS	ts	k0	ts
==	==	k?	==
</s>
</p>
<p>
<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
TS	ts	k0	ts
U	u	k7c2	u
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jako	jako	k8xS	jako
vstup	vstup	k1gInSc4	vstup
přijímá	přijímat	k5eAaImIp3nS	přijímat
kód	kód	k1gInSc4	kód
jiného	jiný	k2eAgInSc2d1	jiný
TS	ts	k0	ts
T	T	kA	T
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc4	jeho
Gödelovo	Gödelův	k2eAgNnSc1d1	Gödelovo
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
vstupní	vstupní	k2eAgNnSc1d1	vstupní
slovo	slovo	k1gNnSc1	slovo
stroje	stroj	k1gInSc2	stroj
T.	T.	kA	T.
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozkódovat	rozkódovat	k5eAaPmF	rozkódovat
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
funkci	funkce	k1gFnSc4	funkce
stroje	stroj	k1gInSc2	stroj
T	T	kA	T
a	a	k8xC	a
výpočet	výpočet	k1gInSc4	výpočet
tohoto	tento	k3xDgInSc2	tento
stroje	stroj	k1gInSc2	stroj
simulovat	simulovat	k5eAaImF	simulovat
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
TS	ts	k0	ts
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vypočítat	vypočítat	k5eAaPmF	vypočítat
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
částečně	částečně	k6eAd1	částečně
rekurzivní	rekurzivní	k2eAgFnSc4d1	rekurzivní
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
k	k	k7c3	k
univerzální	univerzální	k2eAgFnSc4d1	univerzální
částečně	částečně	k6eAd1	částečně
rekurzivní	rekurzivní	k2eAgFnSc4d1	rekurzivní
funkci	funkce	k1gFnSc4	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
libovolný	libovolný	k2eAgInSc4d1	libovolný
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetný	spočetný	k2eAgInSc4d1	spočetný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Univerzální	univerzální	k2eAgInSc1d1	univerzální
jazyk	jazyk	k1gInSc1	jazyk
Lu	Lu	k1gFnSc2	Lu
==	==	k?	==
</s>
</p>
<p>
<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
jazyk	jazyk	k1gInSc1	jazyk
Lu	Lu	k1gFnSc2	Lu
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvojice	dvojice	k1gFnPc4	dvojice
TS	ts	k0	ts
a	a	k8xC	a
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc4	tento
TS	ts	k0	ts
daný	daný	k2eAgInSc1d1	daný
řetězec	řetězec	k1gInSc1	řetězec
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
Lu	Lu	k1gFnSc1	Lu
=	=	kIx~	=
{	{	kIx(	{
<g/>
[	[	kIx(	[
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
]	]	kIx)	]
<g/>
|	|	kIx~	|
TS	ts	k0	ts
M	M	kA	M
přijímá	přijímat	k5eAaImIp3nS	přijímat
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetný	spočetný	k2eAgInSc1d1	spočetný
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
daná	daný	k2eAgFnSc1d1	daná
dvojice	dvojice	k1gFnSc1	dvojice
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
bychom	by	kYmCp1nP	by
sestrojit	sestrojit	k5eAaPmF	sestrojit
TS	ts	k0	ts
Tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
TS	ts	k0	ts
M	M	kA	M
přijímá	přijímat	k5eAaImIp3nS	přijímat
slovo	slovo	k1gNnSc4	slovo
w.	w.	k?	w.
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
budeme	být	k5eAaImBp1nP	být
potřebovat	potřebovat	k5eAaImF	potřebovat
Univerzální	univerzální	k2eAgFnSc4d1	univerzální
TS	ts	k0	ts
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
TS	ts	k0	ts
Tu	ten	k3xDgFnSc4	ten
bude	být	k5eAaImBp3nS	být
postupovat	postupovat	k5eAaImF	postupovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
vezme	vzít	k5eAaPmIp3nS	vzít
libovolný	libovolný	k2eAgInSc4d1	libovolný
(	(	kIx(	(
<g/>
zakódovaný	zakódovaný	k2eAgInSc4d1	zakódovaný
<g/>
)	)	kIx)	)
TS	ts	k0	ts
M	M	kA	M
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
a	a	k8xC	a
nasimuluje	nasimulovat	k5eAaPmIp3nS	nasimulovat
činnost	činnost	k1gFnSc4	činnost
TS	ts	k0	ts
M	M	kA	M
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
w.	w.	k?	w.
Neexistuje	existovat	k5eNaImIp3nS	existovat
jiná	jiný	k2eAgFnSc1d1	jiná
možnost	možnost	k1gFnSc1	možnost
jak	jak	k6eAd1	jak
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
TS	ts	k0	ts
M	M	kA	M
přijímá	přijímat	k5eAaImIp3nS	přijímat
slovo	slovo	k1gNnSc4	slovo
w	w	k?	w
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostě	prostě	k9	prostě
do	do	k7c2	do
TS	ts	k0	ts
M	M	kA	M
to	ten	k3xDgNnSc4	ten
slovo	slovo	k1gNnSc4	slovo
w	w	k?	w
dáme	dát	k5eAaPmIp1nP	dát
a	a	k8xC	a
počkáme	počkat	k5eAaPmIp1nP	počkat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nám	my	k3xPp1nPc3	my
TS	ts	k0	ts
M	M	kA	M
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
Universální	universální	k2eAgFnSc1d1	universální
TS	ts	k0	ts
Tu	ten	k3xDgFnSc4	ten
nasimuluje	nasimulovat	k5eAaPmIp3nS	nasimulovat
činnost	činnost	k1gFnSc1	činnost
TS	ts	k0	ts
M	M	kA	M
a	a	k8xC	a
pokud	pokud	k8xS	pokud
TS	ts	k0	ts
M	M	kA	M
slovo	slovo	k1gNnSc1	slovo
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
i	i	k8xC	i
Tu	ten	k3xDgFnSc4	ten
dvojici	dvojice	k1gFnSc4	dvojice
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
TS	ts	k0	ts
M	M	kA	M
slovo	slovo	k1gNnSc1	slovo
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
i	i	k8xC	i
Tu	ten	k3xDgFnSc4	ten
dvojici	dvojice	k1gFnSc4	dvojice
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
TS	ts	k0	ts
M	M	kA	M
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
w	w	k?	w
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
TS	ts	k0	ts
Tu	ten	k3xDgFnSc4	ten
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
TS	ts	k0	ts
M	M	kA	M
zacyklil	zacyklit	k5eAaPmAgMnS	zacyklit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zacyklí	zacyklit	k5eAaPmIp3nP	zacyklit
taktéž	taktéž	k?	taktéž
<g/>
.	.	kIx.	.
</s>
<s>
Náš	náš	k3xOp1gInSc1	náš
Tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
schopen	schopen	k2eAgMnSc1d1	schopen
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
a	a	k8xC	a
přijmout	přijmout	k5eAaPmF	přijmout
všechny	všechen	k3xTgFnPc4	všechen
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
Lu	Lu	k1gFnSc2	Lu
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
identifikovat	identifikovat	k5eAaBmF	identifikovat
všechny	všechen	k3xTgFnPc4	všechen
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
Lu	Lu	k1gFnSc2	Lu
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
TS	ts	k0	ts
M	M	kA	M
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
w	w	k?	w
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
takový	takový	k3xDgInSc4	takový
TS	ts	k0	ts
jistě	jistě	k9	jistě
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
TS	ts	k0	ts
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
vstup	vstup	k1gInSc4	vstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Redukce	redukce	k1gFnSc1	redukce
problémů	problém	k1gInPc2	problém
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc4	některý
problémy	problém	k1gInPc4	problém
(	(	kIx(	(
<g/>
Turingovy	Turingův	k2eAgInPc1d1	Turingův
stroje	stroj	k1gInPc1	stroj
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
redukovat	redukovat	k5eAaBmF	redukovat
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
důkazu	důkaz	k1gInSc2	důkaz
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
daný	daný	k2eAgInSc1d1	daný
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
rekurzivní	rekurzivní	k2eAgInSc1d1	rekurzivní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetný	spočetný	k2eAgInSc1d1	spočetný
jazyk	jazyk	k1gInSc1	jazyk
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetný	spočetný	k2eAgInSc4d1	spočetný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Modelová	modelový	k2eAgFnSc1d1	modelová
situace	situace	k1gFnSc1	situace
<g/>
:	:	kIx,	:
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
A	a	k9	a
je	být	k5eAaImIp3nS	být
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetný	spočetný	k2eAgInSc1d1	spočetný
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
důkaz	důkaz	k1gInSc1	důkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
B	B	kA	B
rekurzivní	rekurzivní	k2eAgFnSc1d1	rekurzivní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
B	B	kA	B
existoval	existovat	k5eAaImAgInS	existovat
rozhodovací	rozhodovací	k2eAgInSc1d1	rozhodovací
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
bychom	by	kYmCp1nP	by
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
schopni	schopen	k2eAgMnPc1d1	schopen
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
také	také	k9	také
problém	problém	k1gInSc4	problém
A	A	kA	A
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
z	z	k7c2	z
A	A	kA	A
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
rázem	rázem	k6eAd1	rázem
stal	stát	k5eAaPmAgInS	stát
rekurzivní	rekurzivní	k2eAgInSc1d1	rekurzivní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
ale	ale	k9	ale
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
A	a	k9	a
není	být	k5eNaImIp3nS	být
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ani	ani	k8xC	ani
B	B	kA	B
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
HALT	halt	k0	halt
===	===	k?	===
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
HALT	halt	k0	halt
představuje	představovat	k5eAaImIp3nS	představovat
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
dvojic	dvojice	k1gFnPc2	dvojice
TS	ts	k0	ts
a	a	k8xC	a
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
TS	ts	k0	ts
pro	pro	k7c4	pro
dané	daný	k2eAgNnSc4d1	dané
slovo	slovo	k1gNnSc4	slovo
zastaví	zastavit	k5eAaPmIp3nS	zastavit
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezacyklí	zacyklit	k5eNaPmIp3nS	zacyklit
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
rekurzivní	rekurzivní	k2eAgInSc1d1	rekurzivní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
byl	být	k5eAaImAgInS	být
rekurzivní	rekurzivní	k2eAgMnSc1d1	rekurzivní
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
bychom	by	kYmCp1nP	by
schopni	schopen	k2eAgMnPc1d1	schopen
sestavit	sestavit	k5eAaPmF	sestavit
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
Univerzální	univerzální	k2eAgInSc1d1	univerzální
jazyk	jazyk	k1gInSc1	jazyk
Lu	Lu	k1gFnSc2	Lu
<g/>
.	.	kIx.	.
</s>
<s>
TS	ts	k0	ts
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
Lu	Lu	k1gFnSc4	Lu
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vypadat	vypadat	k5eAaImF	vypadat
následovně	následovně	k6eAd1	následovně
(	(	kIx(	(
<g/>
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
vstup	vstup	k1gInSc4	vstup
TS	ts	k0	ts
M	M	kA	M
a	a	k8xC	a
w	w	k?	w
<g/>
,	,	kIx,	,
TSHALT	TSHALT	kA	TSHALT
je	být	k5eAaImIp3nS	být
TS	ts	k0	ts
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
jazyk	jazyk	k1gInSc4	jazyk
HALT	halt	k0	halt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
TSHALT	TSHALT	kA	TSHALT
zjisti	zjistit	k5eAaPmRp2nS	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
M	M	kA	M
zastaví	zastavit	k5eAaPmIp3nS	zastavit
pro	pro	k7c4	pro
w.	w.	k?	w.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
odmítni	odmítnout	k5eAaPmRp2nS	odmítnout
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
simuluj	simulovat	k5eAaImRp2nS	simulovat
TS	ts	k0	ts
M	M	kA	M
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
w	w	k?	w
a	a	k8xC	a
vrať	vrátit	k5eAaPmRp2nS	vrátit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vrátí	vrátit	k5eAaPmIp3nP	vrátit
TS	ts	k0	ts
M.	M.	kA	M.
<g/>
Tento	tento	k3xDgInSc1	tento
TS	ts	k0	ts
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgMnSc1d1	schopný
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
TS	ts	k0	ts
M	M	kA	M
přijímá	přijímat	k5eAaImIp3nS	přijímat
slovo	slovo	k1gNnSc4	slovo
w.	w.	k?	w.
Nejdříve	dříve	k6eAd3	dříve
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
daný	daný	k2eAgInSc1d1	daný
TS	ts	k0	ts
M	M	kA	M
pro	pro	k7c4	pro
slovo	slovo	k1gNnSc4	slovo
w	w	k?	w
zastaví	zastavit	k5eAaPmIp3nP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
rovnou	rovnou	k6eAd1	rovnou
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
simuluje	simulovat	k5eAaImIp3nS	simulovat
činnost	činnost	k1gFnSc4	činnost
M	M	kA	M
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
už	už	k6eAd1	už
musí	muset	k5eAaImIp3nS	muset
vrátit	vrátit	k5eAaPmF	vrátit
nějaký	nějaký	k3yIgInSc4	nějaký
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
zacyklit	zacyklit	k5eAaPmF	zacyklit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
TSHALT	TSHALT	kA	TSHALT
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
TS	ts	k0	ts
by	by	kYmCp3nS	by
tudíž	tudíž	k8xC	tudíž
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
Lu	Lu	k1gMnSc1	Lu
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
ale	ale	k9	ale
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
Lu	Lu	k1gFnSc4	Lu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
TSHALT	TSHALT	kA	TSHALT
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
šlo	jít	k5eAaImAgNnS	jít
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
HALT	halt	k0	halt
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
EMPTY	EMPTY	kA	EMPTY
===	===	k?	===
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
EMPTY	EMPTY	kA	EMPTY
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
TS	ts	k0	ts
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přijímají	přijímat	k5eAaImIp3nP	přijímat
prázdný	prázdný	k2eAgInSc4d1	prázdný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
rekurzivní	rekurzivní	k2eAgInSc1d1	rekurzivní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
Lu	Lu	k1gFnSc4	Lu
<g/>
.	.	kIx.	.
</s>
<s>
Představme	představit	k5eAaPmRp1nP	představit
si	se	k3xPyFc3	se
TSEMPTY	TSEMPTY	kA	TSEMPTY
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k9	zda
TS	ts	k0	ts
přijímá	přijímat	k5eAaImIp3nS	přijímat
prázdný	prázdný	k2eAgInSc1d1	prázdný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
TS	ts	k0	ts
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
Lu	Lu	k1gFnSc4	Lu
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
TS	ts	k0	ts
M	M	kA	M
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
w	w	k?	w
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
nový	nový	k2eAgInSc4d1	nový
TS	ts	k0	ts
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
krom	krom	k7c2	krom
w	w	k?	w
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
<g/>
,	,	kIx,	,
simuluje	simulovat	k5eAaImIp3nS	simulovat
činnost	činnost	k1gFnSc4	činnost
původního	původní	k2eAgInSc2d1	původní
TS	ts	k0	ts
M.	M.	kA	M.
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
TSEMPTY	TSEMPTY	kA	TSEMPTY
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
TS	ts	k0	ts
M1	M1	k1gFnSc3	M1
přijímá	přijímat	k5eAaImIp3nS	přijímat
prázdný	prázdný	k2eAgInSc1d1	prázdný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
TS	ts	k0	ts
M1	M1	k1gFnSc3	M1
přijímá	přijímat	k5eAaImIp3nS	přijímat
prázdný	prázdný	k2eAgInSc1d1	prázdný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
slovo	slovo	k1gNnSc1	slovo
w.	w.	k?	w.
Pokud	pokud	k8xS	pokud
TS	ts	k0	ts
M1	M1	k1gFnSc3	M1
přijímá	přijímat	k5eAaImIp3nS	přijímat
neprázdný	prázdný	k2eNgInSc1d1	neprázdný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
slovo	slovo	k1gNnSc1	slovo
w.	w.	k?	w.
Všechna	všechen	k3xTgNnPc1	všechen
slova	slovo	k1gNnPc1	slovo
krom	krom	k7c2	krom
w	w	k?	w
totiž	totiž	k9	totiž
musí	muset	k5eAaImIp3nS	muset
TS	ts	k0	ts
M1	M1	k1gMnSc1	M1
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
-li	i	k?	-li
TS	ts	k0	ts
M1	M1	k1gFnSc7	M1
neprázdný	prázdný	k2eNgInSc1d1	neprázdný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgNnSc1d1	jediné
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
přijímat	přijímat	k5eAaImF	přijímat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
w.	w.	k?	w.
Přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
-li	i	k?	-li
TS	ts	k0	ts
M1	M1	k1gMnPc7	M1
slovo	slovo	k1gNnSc1	slovo
w	w	k?	w
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
TS	ts	k0	ts
M	M	kA	M
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
slovo	slovo	k1gNnSc4	slovo
w	w	k?	w
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
TS	ts	k0	ts
M1	M1	k1gFnPc7	M1
a	a	k8xC	a
TS	ts	k0	ts
M	M	kA	M
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
ale	ale	k9	ale
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
Lu	Lu	k1gFnSc2	Lu
není	být	k5eNaImIp3nS	být
rozhodnutelný	rozhodnutelný	k2eAgMnSc1d1	rozhodnutelný
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
ani	ani	k8xC	ani
jazyk	jazyk	k1gInSc1	jazyk
EMPTY	EMPTY	kA	EMPTY
být	být	k5eAaImF	být
rozhodnutelný	rozhodnutelný	k2eAgInSc4d1	rozhodnutelný
<g/>
/	/	kIx~	/
<g/>
rekurzivní	rekurzivní	k2eAgFnPc1d1	rekurzivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
TS	ts	k0	ts
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vyřešit	vyřešit	k5eAaPmF	vyřešit
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
TS	ts	k0	ts
mocný	mocný	k2eAgInSc4d1	mocný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vyřešit	vyřešit	k5eAaPmF	vyřešit
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
můžeme	moct	k5eAaImIp1nP	moct
zakódovat	zakódovat	k5eAaPmF	zakódovat
do	do	k7c2	do
řetězce	řetězec	k1gInSc2	řetězec
skládajícího	skládající	k2eAgNnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedniček	jednička	k1gFnPc2	jednička
a	a	k8xC	a
nul	nula	k1gFnPc2	nula
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
kódují	kódovat	k5eAaBmIp3nP	kódovat
programy	program	k1gInPc1	program
na	na	k7c6	na
reálných	reálný	k2eAgInPc6d1	reálný
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
máme	mít	k5eAaImIp1nP	mít
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turingových	Turingový	k2eAgInPc2d1	Turingový
strojů	stroj	k1gInPc2	stroj
je	být	k5eAaImIp3nS	být
spočetně	spočetně	k6eAd1	spočetně
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uzávěr	uzávěr	k1gInSc1	uzávěr
nad	nad	k7c7	nad
abecedou	abeceda	k1gFnSc7	abeceda
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
*	*	kIx~	*
můžeme	moct	k5eAaImIp1nP	moct
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
posloupnosti	posloupnost	k1gFnSc2	posloupnost
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
,	,	kIx,	,
110	[number]	k4	110
<g/>
,	,	kIx,	,
111	[number]	k4	111
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
...	...	k?	...
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
zakódovaný	zakódovaný	k2eAgInSc1d1	zakódovaný
do	do	k7c2	do
jedniček	jednička	k1gFnPc2	jednička
a	a	k8xC	a
nul	nula	k1gFnPc2	nula
tudíž	tudíž	k8xC	tudíž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
posloupnosti	posloupnost	k1gFnSc6	posloupnost
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
=	=	kIx~	=
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nespočetně	spočetně	k6eNd1	spočetně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Použijeme	použít	k5eAaPmIp1nP	použít
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
diagonální	diagonální	k2eAgFnSc4d1	diagonální
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
vybraná	vybraný	k2eAgFnSc1d1	vybraná
podmnožina	podmnožina	k1gFnSc1	podmnožina
z	z	k7c2	z
uzávěru	uzávěr	k1gInSc2	uzávěr
nad	nad	k7c7	nad
abecedou	abeceda	k1gFnSc7	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
abecedu	abeceda	k1gFnSc4	abeceda
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
}	}	kIx)	}
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
-	-	kIx~	-
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgInSc4d1	daný
jazyk	jazyk	k1gInSc4	jazyk
slovo	slovo	k1gNnSc1	slovo
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
řádku	řádek	k1gInSc6	řádek
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
+	+	kIx~	+
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A	a	k9	a
<g/>
*	*	kIx~	*
=	=	kIx~	=
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
L1	L1	k4	L1
=	=	kIx~	=
-	-	kIx~	-
-	-	kIx~	-
+	+	kIx~	+
+	+	kIx~	+
-	-	kIx~	-
+	+	kIx~	+
...	...	k?	...
</s>
</p>
<p>
<s>
L2	L2	k4	L2
=	=	kIx~	=
+	+	kIx~	+
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
+	+	kIx~	+
...	...	k?	...
</s>
</p>
<p>
<s>
L3	L3	k4	L3
=	=	kIx~	=
+	+	kIx~	+
+	+	kIx~	+
+	+	kIx~	+
+	+	kIx~	+
+	+	kIx~	+
-	-	kIx~	-
...	...	k?	...
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Takže	takže	k9	takže
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
L1	L1	k1gFnSc1	L1
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
L1	L1	k1gFnSc1	L1
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
}	}	kIx)	}
atd.	atd.	kA	atd.
Nyní	nyní	k6eAd1	nyní
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jistě	jistě	k9	jistě
nenajdeme	najít	k5eNaPmIp1nP	najít
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
(	(	kIx(	(
<g/>
nekonečném	konečný	k2eNgInSc6d1	nekonečný
<g/>
)	)	kIx)	)
výčtu	výčet	k1gInSc6	výčet
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
jazyk	jazyk	k1gInSc4	jazyk
Ln	Ln	k1gFnSc2	Ln
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jazyk	jazyk	k1gInSc1	jazyk
Li	li	k9	li
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slovo	slovo	k1gNnSc4	slovo
z	z	k7c2	z
A	A	kA	A
<g/>
*	*	kIx~	*
na	na	k7c6	na
i-té	iý	k2eAgFnSc6d1	i-tý
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
Ln	Ln	k1gFnSc4	Ln
ho	on	k3xPp3gMnSc4	on
obsahovat	obsahovat	k5eAaImF	obsahovat
nebude	být	k5eNaImBp3nS	být
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jistě	jistě	k9	jistě
docílíme	docílit	k5eAaPmIp1nP	docílit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
jazyk	jazyk	k1gInSc1	jazyk
Ln	Ln	k1gMnPc2	Ln
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
lišit	lišit	k5eAaImF	lišit
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jednom	jeden	k4xCgInSc6	jeden
slově	slovo	k1gNnSc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
L1	L1	k1gMnSc1	L1
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
slovo	slovo	k1gNnSc4	slovo
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
slovo	slovo	k1gNnSc1	slovo
0	[number]	k4	0
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Ln	Ln	k1gMnPc1	Ln
ho	on	k3xPp3gNnSc4	on
obsahovat	obsahovat	k5eAaImF	obsahovat
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
L2	L2	k1gFnSc2	L2
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
slovo	slovo	k1gNnSc1	slovo
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Ln	Ln	k1gFnSc1	Ln
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
L3	L3	k1gFnSc2	L3
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Ln	Ln	k1gFnSc1	Ln
ho	on	k3xPp3gMnSc4	on
nebude	být	k5eNaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Ln	Ln	k?	Ln
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Ln	Ln	k1gMnPc2	Ln
nenajdeme	najít	k5eNaPmIp1nP	najít
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
výčtu	výčet	k1gInSc6	výčet
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
uvedeným	uvedený	k2eAgInSc7d1	uvedený
jazykem	jazyk	k1gInSc7	jazyk
Li	li	k8xS	li
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
lišit	lišit	k5eAaImF	lišit
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slovo	slovo	k1gNnSc4	slovo
z	z	k7c2	z
A	A	kA	A
<g/>
*	*	kIx~	*
na	na	k7c6	na
i-té	iý	k2eAgFnSc6d1	i-tý
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
jsme	být	k5eAaImIp1nP	být
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nespočetná	spočetný	k2eNgFnSc1d1	nespočetná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
nespočetně	spočetně	k6eNd1	spočetně
mnoho	mnoho	k4c4	mnoho
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
a	a	k8xC	a
spočetně	spočetně	k6eAd1	spočetně
mnoho	mnoho	k4c1	mnoho
Turingových	Turingový	k2eAgInPc2d1	Turingový
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
existovat	existovat	k5eAaImF	existovat
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
Turingovy	Turingův	k2eAgInPc1d1	Turingův
stroje	stroj	k1gInPc1	stroj
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Jazyků	jazyk	k1gMnPc2	jazyk
(	(	kIx(	(
<g/>
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkrátka	zkrátka	k6eAd1	zkrátka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Turingových	Turingový	k2eAgInPc2d1	Turingový
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
popis	popis	k1gInSc1	popis
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
</s>
</p>
