<p>
<s>
Claude	Claude	k6eAd1	Claude
Elwood	Elwood	k1gInSc1	Elwood
Shannon	Shannona	k1gFnPc2	Shannona
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Petoskey	Petoskea	k1gFnPc1	Petoskea
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
elektronik	elektronik	k1gMnSc1	elektronik
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
teorie	teorie	k1gFnSc2	teorie
informace	informace	k1gFnSc2	informace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
teorie	teorie	k1gFnSc2	teorie
návrhu	návrh	k1gInSc2	návrh
digitálních	digitální	k2eAgInPc2d1	digitální
elektrických	elektrický	k2eAgInPc2d1	elektrický
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
představil	představit	k5eAaPmAgInS	představit
lineární	lineární	k2eAgInSc1d1	lineární
model	model	k1gInSc1	model
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc4d1	zvaný
Shannonův	Shannonův	k2eAgInSc4d1	Shannonův
a	a	k8xC	a
Weaverův	Weaverův	k2eAgInSc4d1	Weaverův
model	model	k1gInSc4	model
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
byl	být	k5eAaImAgInS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
radioamatérem	radioamatér	k1gMnSc7	radioamatér
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
University	universita	k1gFnSc2	universita
of	of	k?	of
Michigan	Michigan	k1gInSc1	Michigan
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
MIT	MIT	kA	MIT
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
získal	získat	k5eAaPmAgInS	získat
magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc1	titul
za	za	k7c4	za
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
Booleovy	Booleův	k2eAgFnSc2d1	Booleova
algebry	algebra	k1gFnSc2	algebra
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
reléových	reléový	k2eAgFnPc2d1	reléová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Všiml	všimnout	k5eAaPmAgMnS	všimnout
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xS	jak
Booloeva	Booloeva	k1gFnSc1	Booloeva
algebra	algebra	k1gFnSc1	algebra
(	(	kIx(	(
<g/>
počítající	počítající	k2eAgFnSc1d1	počítající
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
o	o	k7c6	o
2	[number]	k4	2
prvcích	prvek	k1gInPc6	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
relé	relé	k1gNnPc1	relé
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
založil	založit	k5eAaPmAgInS	založit
nový	nový	k2eAgInSc4d1	nový
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
–	–	k?	–
teorii	teorie	k1gFnSc3	teorie
logických	logický	k2eAgFnPc2d1	logická
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
otevřel	otevřít	k5eAaPmAgMnS	otevřít
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
konečných	konečný	k2eAgInPc2d1	konečný
automatů	automat	k1gInPc2	automat
a	a	k8xC	a
následně	následně	k6eAd1	následně
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
číslicových	číslicový	k2eAgMnPc2d1	číslicový
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaPmAgMnS	napsat
dizertaci	dizertace	k1gFnSc4	dizertace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
podobný	podobný	k2eAgInSc4d1	podobný
postup	postup	k1gInSc4	postup
na	na	k7c4	na
genetiku	genetika	k1gFnSc4	genetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
Bellových	Bellův	k2eAgFnPc2d1	Bellova
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
15	[number]	k4	15
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zde	zde	k6eAd1	zde
teorii	teorie	k1gFnSc4	teorie
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc4	technolog
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Shannonova	Shannonův	k2eAgFnSc1d1	Shannonova
myš	myš	k1gFnSc1	myš
(	(	kIx(	(
<g/>
též	též	k9	též
myška	myška	k1gFnSc1	myška
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgInSc4	první
sestrojený	sestrojený	k2eAgInSc4d1	sestrojený
učící	učící	k2eAgInSc4d1	učící
se	se	k3xPyFc4	se
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
napojený	napojený	k2eAgInSc4d1	napojený
ocelový	ocelový	k2eAgInSc4d1	ocelový
vozíček	vozíček	k1gInSc4	vozíček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
projížděl	projíždět	k5eAaImAgInS	projíždět
bludištěm	bludiště	k1gNnSc7	bludiště
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
správné	správný	k2eAgFnPc4d1	správná
a	a	k8xC	a
nesprávné	správný	k2eNgFnPc4d1	nesprávná
odbočky	odbočka	k1gFnPc4	odbočka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
pokusu	pokus	k1gInSc6	pokus
projet	projet	k5eAaPmF	projet
bludištěm	bludiště	k1gNnSc7	bludiště
již	již	k6eAd1	již
náhodně	náhodně	k6eAd1	náhodně
netápal	tápat	k5eNaImAgMnS	tápat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dojel	dojet	k5eAaPmAgMnS	dojet
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
možnou	možný	k2eAgFnSc7d1	možná
cestou	cesta	k1gFnSc7	cesta
za	za	k7c4	za
15	[number]	k4	15
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
první	první	k4xOgFnSc1	první
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
učil	učit	k5eAaImAgMnS	učit
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
zabrala	zabrat	k5eAaPmAgFnS	zabrat
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
ukázka	ukázka	k1gFnSc1	ukázka
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
učícího	učící	k2eAgInSc2d1	učící
se	se	k3xPyFc4	se
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Shannon	Shannon	k1gMnSc1	Shannon
publikoval	publikovat	k5eAaBmAgMnS	publikovat
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Programming	Programming	k1gInSc1	Programming
a	a	k8xC	a
Computer	computer	k1gInSc1	computer
for	forum	k1gNnPc2	forum
Playing	Playing	k1gInSc4	Playing
Chess	Chessa	k1gFnPc2	Chessa
<g/>
.	.	kIx.	.
</s>
<s>
Popisoval	popisovat	k5eAaImAgMnS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
stroj	stroj	k1gInSc1	stroj
může	moct	k5eAaImIp3nS	moct
zahrát	zahrát	k5eAaPmF	zahrát
šachy	šach	k1gInPc4	šach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
A	a	k8xC	a
mathematical	mathematicat	k5eAaPmAgInS	mathematicat
theory	theora	k1gFnPc4	theora
of	of	k?	of
communication	communication	k1gInSc1	communication
–	–	k?	–
napsal	napsat	k5eAaBmAgInS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Warrenem	Warren	k1gMnSc7	Warren
Weaverem	Weaver	k1gMnSc7	Weaver
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historiků	historik	k1gMnPc2	historik
vědy	věda	k1gFnSc2	věda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
Magnu	Magen	k2eAgFnSc4d1	Magna
chartu	charta	k1gFnSc4	charta
informačního	informační	k2eAgInSc2d1	informační
věku	věk	k1gInSc2	věk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
otevřel	otevřít	k5eAaPmAgInS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vnímání	vnímání	k1gNnSc3	vnímání
informace	informace	k1gFnSc2	informace
jako	jako	k8xS	jako
předmětu	předmět	k1gInSc2	předmět
zájmu	zájem	k1gInSc2	zájem
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
informační	informační	k2eAgFnPc4d1	informační
a	a	k8xC	a
znalostní	znalostní	k2eAgFnPc4d1	znalostní
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
slovo	slovo	k1gNnSc1	slovo
bit	bit	k1gInSc1	bit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Shannon-Fanovo	Shannon-Fanův	k2eAgNnSc1d1	Shannon-Fanův
kódování	kódování	k1gNnSc1	kódování
</s>
</p>
<p>
<s>
Informační	informační	k2eAgFnSc1d1	informační
entropie	entropie	k1gFnSc1	entropie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Claude	Claud	k1gInSc5	Claud
Shannon	Shannon	k1gInSc4	Shannon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
