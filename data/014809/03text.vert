<s>
Lež	lež	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Lhář	lhář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
vědeckofantastickou	vědeckofantastický	k2eAgFnSc4d1
povídku	povídka	k1gFnSc4
Isaaca	Isaac	k1gInSc2
Asimova	Asimův	k2eAgInSc2d1
z	z	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
Lhář	lhář	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s>
Pinocchio	Pinocchio	k1gNnSc1
<g/>
,	,	kIx,
symbol	symbol	k1gInSc1
prolhanosti	prolhanost	k1gFnSc2
</s>
<s>
Lež	lež	k1gFnSc1
je	být	k5eAaImIp3nS
typ	typ	k1gInSc4
klamu	klam	k1gInSc2
mající	mající	k2eAgFnSc4d1
formu	forma	k1gFnSc4
nepravdivého	pravdivý	k2eNgInSc2d1
výroku	výrok	k1gInSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
s	s	k7c7
vědomým	vědomý	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
oklamat	oklamat	k5eAaPmF
druhé	druhý	k4xOgFnSc2
za	za	k7c2
účelem	účel	k1gInSc7
získání	získání	k1gNnSc2
nějaké	nějaký	k3yIgFnSc2
výhody	výhoda	k1gFnSc2
či	či	k8xC
vyhnutí	vyhnutí	k1gNnSc2
se	se	k3xPyFc4
trestu	trest	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pojetí	pojetí	k1gNnSc4
rozsahu	rozsah	k1gInSc2
pojmu	pojem	k1gInSc2
lži	lež	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
i	i	k8xC
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
–	–	k?
týž	týž	k3xTgInSc4
výrok	výrok	k1gInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
označen	označit	k5eAaPmNgInS
i	i	k9
při	při	k7c6
správné	správný	k2eAgFnSc6d1
interpretaci	interpretace	k1gFnSc6
za	za	k7c4
lživý	lživý	k2eAgInSc4d1
i	i	k8xC
pravdivý	pravdivý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnSc6
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
intuitivně	intuitivně	k6eAd1
rozlišují	rozlišovat	k5eAaImIp3nP
na	na	k7c4
„	„	k?
<g/>
velké	velká	k1gFnPc4
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
malé	malý	k2eAgFnSc2d1
<g/>
“	“	k?
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
škodu	škoda	k1gFnSc4
způsobují	způsobovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Lhaní	lhaní	k1gNnPc2
je	být	k5eAaImIp3nS
tvrzení	tvrzení	k1gNnSc1
něčeho	něco	k3yInSc2
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
mluvčí	mluvčí	k1gMnSc1
nebo	nebo	k8xC
pisatel	pisatel	k1gMnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nepravdivé	pravdivý	k2eNgNnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
o	o	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
nemá	mít	k5eNaImIp3nS
rozumný	rozumný	k2eAgInSc1d1
důvod	důvod	k1gInSc1
se	se	k3xPyFc4
domnívat	domnívat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
pravdivé	pravdivý	k2eAgNnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
to	ten	k3xDgNnSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
pravdu	pravda	k1gFnSc4
buď	buď	k8xC
sobě	se	k3xPyFc3
nebo	nebo	k8xC
jiným	jiný	k2eAgNnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
i	i	k9
o	o	k7c4
způsob	způsob	k1gInSc4
obranného	obranný	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
i	i	k9
o	o	k7c4
momentální	momentální	k2eAgInSc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
jedince	jedinec	k1gMnSc4
nepříjemnou	příjemný	k2eNgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ji	on	k3xPp3gFnSc4
nedokáže	dokázat	k5eNaPmIp3nS
aktuálně	aktuálně	k6eAd1
řešit	řešit	k5eAaImF
jiným	jiný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristický	charakteristický	k2eAgInSc1d1
je	být	k5eAaImIp3nS
úmysl	úmysl	k1gInSc1
a	a	k8xC
vědomí	vědomí	k1gNnSc1
nepravdivosti	nepravdivost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lhář	lhář	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
lže	lhát	k5eAaImIp3nS
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
lhala	lhát	k5eAaImAgFnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
nebo	nebo	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
tendenci	tendence	k1gFnSc4
lhát	lhát	k5eAaImF
opakovaně	opakovaně	k6eAd1
(	(	kIx(
<g/>
notorický	notorický	k2eAgMnSc1d1
či	či	k8xC
patologický	patologický	k2eAgMnSc1d1
lhář	lhář	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
lhaní	lhaní	k1gNnSc4
označit	označit	k5eAaPmF
za	za	k7c4
poruchu	porucha	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
důležitá	důležitý	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
a	a	k8xC
účel	účel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
jedince	jedinko	k6eAd1
ke	k	k7c3
lhaní	lhaní	k1gNnSc3
vede	vést	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motivace	motivace	k1gFnPc1
a	a	k8xC
její	její	k3xOp3gInPc1
důsledky	důsledek	k1gInPc1
mají	mít	k5eAaImIp3nP
význam	význam	k1gInSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
posouzení	posouzení	k1gNnSc2
závažnosti	závažnost	k1gFnSc2
lži	lež	k1gFnSc2
<g/>
,	,	kIx,
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
kvalifikovat	kvalifikovat	k5eAaBmF
jako	jako	k8xC,k8xS
chování	chování	k1gNnSc1
disociální	disociální	k2eAgFnPc1d1
(	(	kIx(
<g/>
jedinec	jedinec	k1gMnSc1
škodí	škodit	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
sobě	sebe	k3xPyFc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
asociální	asociální	k2eAgFnSc1d1
(	(	kIx(
<g/>
pomluva	pomluva	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
antisociální	antisociální	k2eAgFnSc1d1
(	(	kIx(
<g/>
způsobí	způsobit	k5eAaPmIp3nS
druhému	druhý	k4xOgInSc3
újmu	újma	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Různá	různý	k2eAgNnPc1d1
pojetí	pojetí	k1gNnPc1
lhaní	lhaní	k1gNnSc2
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
pravdy	pravda	k1gFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
projevují	projevovat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
úrovni	úroveň	k1gFnSc6
sémantiky	sémantika	k1gFnSc2
různých	různý	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ruském	ruský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
například	například	k6eAd1
existují	existovat	k5eAaImIp3nP
výrazy	výraz	k1gInPc1
л	л	k?
a	a	k8xC
в	в	k?
pro	pro	k7c4
lhaní	lhaní	k1gNnSc4
a	a	k8xC
výrazy	výraz	k1gInPc4
и	и	k?
a	a	k8xC
п	п	k?
pro	pro	k7c4
pravdu	pravda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
můžeme	moct	k5eAaImIp1nP
nuance	nuance	k1gFnPc1
těchto	tento	k3xDgInPc2
výrazů	výraz	k1gInPc2
vyjadřovat	vyjadřovat	k5eAaImF
pouze	pouze	k6eAd1
užitím	užití	k1gNnSc7
velkých	velká	k1gFnPc2
či	či	k8xC
malých	malý	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Důvody	důvod	k1gInPc1
lhaní	lhaní	k1gNnSc2
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1
lež	lež	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
získání	získání	k1gNnSc3
nějaké	nějaký	k3yIgFnSc2
výhody	výhoda	k1gFnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
někdy	někdy	k6eAd1
i	i	k9
pro	pro	k7c4
nějakou	nějaký	k3yIgFnSc4
skupinu	skupina	k1gFnSc4
nebo	nebo	k8xC
jinou	jiný	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
získaná	získaný	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
různý	různý	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
důvody	důvod	k1gInPc4
materiální	materiální	k2eAgInSc1d1
<g/>
,	,	kIx,
vyhnutí	vyhnutí	k1gNnSc1
se	se	k3xPyFc4
trestu	trest	k1gInSc3
<g/>
,	,	kIx,
zachování	zachování	k1gNnSc3
pověsti	pověst	k1gFnSc2
<g/>
,	,	kIx,
zlepšení	zlepšení	k1gNnSc2
pověsti	pověst	k1gFnSc2
či	či	k8xC
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
před	před	k7c7
nebezpečím	nebezpečí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ke	k	k7c3
lhaní	lhaní	k1gNnSc3
i	i	k8xC
jiné	jiný	k2eAgInPc4d1
důvody	důvod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
lhaní	lhaní	k1gNnSc3
navádí	navádět	k5eAaImIp3nS
sklon	sklon	k1gInSc1
k	k	k7c3
nápodobě	nápodoba	k1gFnSc3
(	(	kIx(
<g/>
lžeme	lhát	k5eAaImIp1nP
<g/>
,	,	kIx,
pokud	pokud	k8xS
lžou	lhát	k5eAaImIp3nP
i	i	k9
ostatní	ostatní	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lhaní	lhaní	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
zdrojem	zdroj	k1gInSc7
zábavnosti	zábavnost	k1gFnSc2
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
motivací	motivace	k1gFnSc7
snaha	snaha	k1gFnSc1
neublížit	ublížit	k5eNaPmF
pravdou	pravda	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
adresáta	adresát	k1gMnSc4
příliš	příliš	k6eAd1
těžká	těžký	k2eAgFnSc1d1
(	(	kIx(
<g/>
milosrdná	milosrdný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
není	být	k5eNaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
lež	lež	k1gFnSc4
</s>
<s>
Jazykové	jazykový	k2eAgFnPc1d1
figury	figura	k1gFnPc1
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1
jazykové	jazykový	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
vytvářejí	vytvářet	k5eAaImIp3nP
výroky	výrok	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
neodpovídají	odpovídat	k5eNaImIp3nP
skutečnosti	skutečnost	k1gFnPc4
a	a	k8xC
přesto	přesto	k8xC
nejsou	být	k5eNaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
lživé	lživý	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
Sarkasmus	sarkasmus	k1gInSc1
-	-	kIx~
záměrné	záměrný	k2eAgNnSc1d1
uvedení	uvedení	k1gNnSc1
antonymního	antonymní	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
<g/>
:	:	kIx,
Dneska	Dneska	k?
jsem	být	k5eAaImIp1nS
ve	v	k7c6
škole	škola	k1gFnSc6
rozbil	rozbít	k5eAaPmAgInS
okno	okno	k1gNnSc4
a	a	k8xC
učitel	učitel	k1gMnSc1
mě	já	k3xPp1nSc4
pochválil	pochválit	k5eAaPmAgMnS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ho	on	k3xPp3gInSc4
potrestal	potrestat	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ironie	ironie	k1gFnSc1
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
doprovázena	doprovázet	k5eAaImNgFnS
charakteristickou	charakteristický	k2eAgFnSc7d1
intonací	intonace	k1gFnSc7
a	a	k8xC
mimikou	mimika	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
posluchači	posluchač	k1gMnPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
pronášený	pronášený	k2eAgInSc1d1
výrok	výrok	k1gInSc1
neodpovídá	odpovídat	k5eNaImIp3nS
skutečnosti	skutečnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Nadsázka	nadsázka	k1gFnSc1
-	-	kIx~
uvedený	uvedený	k2eAgInSc1d1
údaj	údaj	k1gInSc1
je	být	k5eAaImIp3nS
přehnaný	přehnaný	k2eAgInSc1d1
<g/>
:	:	kIx,
V	v	k7c6
té	ten	k3xDgFnSc6
frontě	fronta	k1gFnSc6
stálo	stát	k5eAaImAgNnS
asi	asi	k9
tisíc	tisíc	k4xCgInSc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tam	tam	k6eAd1
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
určitě	určitě	k6eAd1
ne	ne	k9
tisíc	tisíc	k4xCgInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Metafora	metafora	k1gFnSc1
-	-	kIx~
uvedený	uvedený	k2eAgInSc1d1
výraz	výraz	k1gInSc1
je	být	k5eAaImIp3nS
přenesený	přenesený	k2eAgInSc1d1
na	na	k7c6
základě	základ	k1gInSc6
podobnosti	podobnost	k1gFnSc2
<g/>
,	,	kIx,
přirovnání	přirovnání	k1gNnSc1
<g/>
:	:	kIx,
Moje	můj	k3xOp1gFnSc1
sestra	sestra	k1gFnSc1
je	být	k5eAaImIp3nS
hrozná	hrozný	k2eAgFnSc1d1
slepice	slepice	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
sestra	sestra	k1gFnSc1
upovídaná	upovídaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
dotěrná	dotěrný	k2eAgFnSc1d1
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Metonymie	metonymie	k1gFnSc1
-	-	kIx~
uvedený	uvedený	k2eAgInSc1d1
výraz	výraz	k1gInSc1
je	být	k5eAaImIp3nS
přenesený	přenesený	k2eAgInSc1d1
na	na	k7c6
základě	základ	k1gInSc6
souvislosti	souvislost	k1gFnSc2
<g/>
:	:	kIx,
Snědl	sníst	k5eAaPmAgMnS,k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
dva	dva	k4xCgInPc4
talíře	talíř	k1gInPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
snědl	snědnout	k5eAaImAgMnS,k5eAaPmAgMnS
obsah	obsah	k1gInSc4
dvou	dva	k4xCgInPc2
talířů	talíř	k1gInPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
ustálené	ustálený	k2eAgInPc1d1
výroky	výrok	k1gInPc1
-	-	kIx~
mluvčí	mluvčí	k1gFnSc1
uvede	uvést	k5eAaPmIp3nS
běžně	běžně	k6eAd1
užívaný	užívaný	k2eAgInSc1d1
výraz	výraz	k1gInSc1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
jeho	jeho	k3xOp3gInSc1
význam	význam	k1gInSc1
myslí	myslet	k5eAaImIp3nS
doslovně	doslovně	k6eAd1
<g/>
:	:	kIx,
Přeji	přát	k5eAaImIp1nS
všechno	všechen	k3xTgNnSc1
nejlepší	dobrý	k2eAgNnSc1d3
k	k	k7c3
narozeninám	narozeniny	k1gFnPc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
mu	on	k3xPp3gMnSc3
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
oslavenec	oslavenec	k1gMnSc1
lhostejný	lhostejný	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
nepříjemný	příjemný	k2eNgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
přeje	přát	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
tě	ty	k3xPp2nSc4
zabiju	zabít	k5eAaPmIp1nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
tato	tento	k3xDgFnSc1
kategorie	kategorie	k1gFnSc1
je	být	k5eAaImIp3nS
nejspornější	sporný	k2eAgFnSc1d3
<g/>
,	,	kIx,
protože	protože	k8xS
některé	některý	k3yIgInPc4
obraty	obrat	k1gInPc4
může	moct	k5eAaImIp3nS
mluvčí	mluvčí	k1gFnSc1
podávat	podávat	k5eAaImF
jako	jako	k8xC,k8xS
ustálený	ustálený	k2eAgInSc4d1
výrok	výrok	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
posluchač	posluchač	k1gMnSc1
jej	on	k3xPp3gNnSc4
chápe	chápat	k5eAaImIp3nS
doslovně	doslovně	k6eAd1
-	-	kIx~
výzva	výzva	k1gFnSc1
<g/>
:	:	kIx,
Na	na	k7c4
mě	já	k3xPp1nSc4
se	se	k3xPyFc4
můžeš	moct	k5eAaImIp2nS
vždycky	vždycky	k6eAd1
obrátit	obrátit	k5eAaPmF
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
chápána	chápat	k5eAaImNgFnS
doslovně	doslovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Kontextové	kontextový	k2eAgFnPc1d1
nepravdy	nepravda	k1gFnPc1
</s>
<s>
Za	za	k7c4
lež	lež	k1gFnSc4
se	se	k3xPyFc4
nepovažuje	považovat	k5eNaImIp3nS
klamavé	klamavý	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
adresát	adresát	k1gMnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
klamem	klam	k1gInSc7
může	moct	k5eAaImIp3nS
počítat	počítat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takových	takový	k3xDgInPc2
kontextů	kontext	k1gInPc2
existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadelní	divadelní	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
,	,	kIx,
filmy	film	k1gInPc1
<g/>
,	,	kIx,
koncerty	koncert	k1gInPc1
apod.	apod.	kA
-	-	kIx~
herec	herec	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
nemá	mít	k5eNaImIp3nS
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
<g/>
,	,	kIx,
ani	ani	k8xC
se	se	k3xPyFc4
nemusí	muset	k5eNaImIp3nS
ztotožňovat	ztotožňovat	k5eAaImF
s	s	k7c7
charakterem	charakter	k1gInSc7
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
hraje	hrát	k5eAaImIp3nS
<g/>
;	;	kIx,
zpěvák	zpěvák	k1gMnSc1
může	moct	k5eAaImIp3nS
písní	píseň	k1gFnSc7
vyjadřovat	vyjadřovat	k5eAaImF
myšlenky	myšlenka	k1gFnPc4
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgMnPc7
nesouhlasí	souhlasit	k5eNaImIp3nP
(	(	kIx(
<g/>
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
autorem	autor	k1gMnSc7
písně	píseň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
a	a	k8xC
sport	sport	k1gInSc1
-	-	kIx~
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
her	hra	k1gFnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
klamavých	klamavý	k2eAgFnPc2d1
technik	technika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
poker	poker	k1gInSc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
blafování	blafování	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
uplatňuje	uplatňovat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
hrách	hra	k1gFnPc6
od	od	k7c2
šachu	šach	k1gInSc2
až	až	k9
po	po	k7c4
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškeré	veškerý	k3xTgInPc4
podvody	podvod	k1gInPc4
je	být	k5eAaImIp3nS
však	však	k9
třeba	třeba	k6eAd1
provádět	provádět	k5eAaImF
pouze	pouze	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
vymezených	vymezený	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kouzelnické	kouzelnický	k2eAgNnSc1d1
vystoupení	vystoupení	k1gNnSc1
-	-	kIx~
iluzionista	iluzionista	k1gMnSc1
klame	klamat	k5eAaImIp3nS
obecenstvo	obecenstvo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
diváci	divák	k1gMnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
klam	klam	k1gInSc4
připraveni	připravit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Tajemství	tajemství	k1gNnSc1
-	-	kIx~
pokud	pokud	k8xS
mluvčí	mluvčí	k1gMnSc1
jasně	jasně	k6eAd1
vymezí	vymezit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nějaká	nějaký	k3yIgFnSc1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc7
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nesmí	smět	k5eNaImIp3nS
prozradit	prozradit	k5eAaPmF
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
na	na	k7c4
něj	on	k3xPp3gNnSc4
plné	plný	k2eAgNnSc4d1
právo	právo	k1gNnSc4
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
uchýlit	uchýlit	k5eAaPmF
i	i	k9
k	k	k7c3
drobným	drobný	k2eAgFnPc3d1
lžím	lež	k1gFnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
k	k	k7c3
prozrazení	prozrazení	k1gNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
<g/>
:	:	kIx,
Kamarádka	kamarádka	k1gFnSc1
se	se	k3xPyFc4
svěří	svěřit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
těhotná	těhotný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
prozatím	prozatím	k6eAd1
nechce	chtít	k5eNaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
vědělo	vědět	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můj	můj	k1gMnSc1
známý	známý	k1gMnSc1
se	se	k3xPyFc4
mě	já	k3xPp1nSc4
dotáže	dotázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
jestli	jestli	k8xS
je	být	k5eAaImIp3nS
těhotná	těhotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
vyhnu	vyhnout	k5eAaPmIp1nS
přímé	přímý	k2eAgFnSc2d1
odpovědi	odpověď	k1gFnSc2
<g/>
,	,	kIx,
známý	známý	k2eAgInSc4d1
zřejmě	zřejmě	k6eAd1
pravdu	pravda	k1gFnSc4
stejně	stejně	k6eAd1
uhodne	uhodnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mám	mít	k5eAaImIp1nS
<g/>
-li	-li	k?
respektovat	respektovat	k5eAaImF
tajemství	tajemství	k1gNnSc4
<g/>
,	,	kIx,
nezbývá	zbývat	k5eNaImIp3nS
mi	já	k3xPp1nSc3
než	než	k8xS
zalhat	zalhat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
v	v	k7c6
této	tento	k3xDgFnSc6
kategorii	kategorie	k1gFnSc6
může	moct	k5eAaImIp3nS
velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
docházet	docházet	k5eAaImF
ke	k	k7c3
sporným	sporný	k2eAgInPc3d1
momentům	moment	k1gInPc3
<g/>
:	:	kIx,
</s>
<s>
Pokud	pokud	k8xS
obchodník	obchodník	k1gMnSc1
prodává	prodávat	k5eAaImIp3nS
zboží	zboží	k1gNnSc4
za	za	k7c4
přemrštěnou	přemrštěný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
omlouvat	omlouvat	k5eAaImF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
obchodník	obchodník	k1gMnSc1
to	ten	k3xDgNnSc4
tak	tak	k9
dělá	dělat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdo	někdo	k3yInSc1
ho	on	k3xPp3gNnSc4
ovšem	ovšem	k9
může	moct	k5eAaImIp3nS
označit	označit	k5eAaPmF
za	za	k7c4
podvodníka	podvodník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
muž	muž	k1gMnSc1
nepřizná	přiznat	k5eNaPmIp3nS
k	k	k7c3
nevěře	nevěra	k1gFnSc3
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
argumentovat	argumentovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc4
tak	tak	k6eAd1
dělají	dělat	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s>
Klamavá	klamavý	k2eAgFnSc1d1
reklama	reklama	k1gFnSc1
je	být	k5eAaImIp3nS
obhajována	obhajovat	k5eAaImNgFnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
reklama	reklama	k1gFnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
klamavá	klamavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Omyly	omyl	k1gInPc1
a	a	k8xC
schopnost	schopnost	k1gFnSc1
poznání	poznání	k1gNnSc2
pravdy	pravda	k1gFnSc2
</s>
<s>
Omyl	omyl	k1gInSc1
nelze	lze	k6eNd1
chápat	chápat	k5eAaImF
jako	jako	k8xS,k8xC
lež	lež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
mluvčí	mluvčí	k1gMnSc1
není	být	k5eNaImIp3nS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pronáší	pronášet	k5eAaImIp3nS
nepravdu	nepravda	k1gFnSc4
<g/>
,	,	kIx,
nelze	lze	k6eNd1
ho	on	k3xPp3gMnSc4
označit	označit	k5eAaPmF
za	za	k7c4
lháře	lhář	k1gMnSc4
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
výrok	výrok	k1gInSc1
zjevně	zjevně	k6eAd1
nesmyslný	smyslný	k2eNgMnSc1d1
-	-	kIx~
můžeme	moct	k5eAaImIp1nP
pak	pak	k6eAd1
pochybovat	pochybovat	k5eAaImF
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
intelektu	intelekt	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
pravdomluvnosti	pravdomluvnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
platí	platit	k5eAaImIp3nS
i	i	k9
pravý	pravý	k2eAgInSc1d1
opak	opak	k1gInSc1
-	-	kIx~
pokud	pokud	k8xS
mluvčí	mluvčí	k1gMnSc1
pronáší	pronášet	k5eAaImIp3nS
něco	něco	k3yInSc1
<g/>
,	,	kIx,
o	o	k7c6
čem	co	k3yRnSc6,k3yInSc6,k3yQnSc6
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
lež	lež	k1gFnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
lže	lhát	k5eAaImIp3nS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
později	pozdě	k6eAd2
ukáže	ukázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
říkal	říkat	k5eAaImAgMnS
pravdu	pravda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
vyhraněného	vyhraněný	k2eAgInSc2d1
skepticismu	skepticismus	k1gInSc2
lidé	člověk	k1gMnPc1
nemohou	moct	k5eNaImIp3nP
mít	mít	k5eAaImF
jistotu	jistota	k1gFnSc4
nikdy	nikdy	k6eAd1
v	v	k7c6
ničem	nic	k3yNnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
zejména	zejména	k9
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
je	být	k5eAaImIp3nS
a	a	k8xC
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
není	být	k5eNaImIp3nS
Pravdou	pravda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
malichernost	malichernost	k1gFnSc4
jako	jako	k8xC,k8xS
tvrzení	tvrzení	k1gNnSc4
„	„	k?
<g/>
Byl	být	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
tam	tam	k6eAd1
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
řekl	říct	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
anebo	anebo	k8xC
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
o	o	k7c4
hlubší	hluboký	k2eAgFnPc4d2
metafyzické	metafyzický	k2eAgFnPc4d1
<g/>
,	,	kIx,
duchovní	duchovní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
,	,	kIx,
otázky	otázka	k1gFnPc1
Boha	bůh	k1gMnSc2
atp.	atp.	kA
-	-	kIx~
nejistota	nejistota	k1gFnSc1
zůstává	zůstávat	k5eAaImIp3nS
tatáž	týž	k3xTgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
relativizováno	relativizován	k2eAgNnSc1d1
i	i	k8xC
celé	celý	k2eAgNnSc1d1
lhaní	lhaní	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovedeno	dovést	k5eAaPmNgNnS
do	do	k7c2
takového	takový	k3xDgInSc2
extrému	extrém	k1gInSc2
<g/>
,	,	kIx,
lže	lhát	k5eAaImIp3nS
se	se	k3xPyFc4
neustále	neustále	k6eAd1
<g/>
,	,	kIx,
vším	všecek	k3xTgNnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
lidé	člověk	k1gMnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
a	a	k8xC
prezentují	prezentovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
podstatou	podstata	k1gFnSc7
člověka	člověk	k1gMnSc2
neustále	neustále	k6eAd1
přejímat	přejímat	k5eAaImF
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
okolí	okolí	k1gNnSc2
podněty	podnět	k1gInPc4
<g/>
,	,	kIx,
fakta	faktum	k1gNnPc4
a	a	k8xC
pravdy	pravda	k1gFnPc4
a	a	k8xC
ty	ten	k3xDgFnPc4
poté	poté	k6eAd1
dále	daleko	k6eAd2
reprodukovat	reprodukovat	k5eAaBmF
<g/>
,	,	kIx,
buď	buď	k8xC
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
zakomponovat	zakomponovat	k5eAaPmF
do	do	k7c2
svých	svůj	k3xOyFgInPc2
názorů	názor	k1gInPc2
či	či	k8xC
je	být	k5eAaImIp3nS
jen	jen	k9
předávat	předávat	k5eAaImF
dál	daleko	k6eAd2
nevinným	vinný	k2eNgNnSc7d1
vyprávěním	vyprávění	k1gNnSc7
nebo	nebo	k8xC
dokonce	dokonce	k9
ve	v	k7c6
formě	forma	k1gFnSc6
fám	fáma	k1gFnPc2
a	a	k8xC
pomluv	pomluva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
však	však	k9
nemůže	moct	k5eNaImIp3nS
podařit	podařit	k5eAaPmF
vyjádřit	vyjádřit	k5eAaPmF
skutečnou	skutečný	k2eAgFnSc4d1
Pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
lidské	lidský	k2eAgInPc4d1
duševní	duševní	k2eAgInPc4d1
či	či	k8xC
fyziologické	fyziologický	k2eAgInPc4d1
kanály	kanál	k1gInPc4
či	či	k8xC
jen	jen	k9
technické	technický	k2eAgInPc4d1
prostředí	prostředí	k1gNnSc2
ji	on	k3xPp3gFnSc4
vždy	vždy	k6eAd1
nějak	nějak	k6eAd1
zdeformují	zdeformovat	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
extrémního	extrémní	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
by	by	kYmCp3nS
tedy	tedy	k9
člověk	člověk	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
ani	ani	k9
promluvit	promluvit	k5eAaPmF
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
nelhal	lhát	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Nepravé	pravý	k2eNgFnPc4d1
lži	lež	k1gFnPc4
u	u	k7c2
duševně	duševně	k6eAd1
nemocných	mocný	k2eNgFnPc2d1,k2eAgFnPc2d1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
předchozím	předchozí	k2eAgInSc6d1
případě	případ	k1gInSc6
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
si	se	k3xPyFc3
mluvčí	mluvčí	k1gMnSc1
není	být	k5eNaImIp3nS
vědom	vědom	k2eAgMnSc1d1
své	své	k1gNnSc4
lži	lež	k1gFnPc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
jej	on	k3xPp3gMnSc4
nelze	lze	k6eNd1
označit	označit	k5eAaPmF
za	za	k7c4
lháře	lhář	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
duševně	duševně	k6eAd1
zdraví	zdravý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
lžou	lhát	k5eAaImIp3nP
vědomě	vědomě	k6eAd1
a	a	k8xC
zpravidla	zpravidla	k6eAd1
záměrně	záměrně	k6eAd1
<g/>
,	,	kIx,
duševně	duševně	k6eAd1
nemocní	nemocný	k1gMnPc1
(	(	kIx(
<g/>
psychotici	psychotik	k1gMnPc1
a	a	k8xC
psychopati	psychopat	k1gMnPc1
včetně	včetně	k7c2
drogově	drogově	k6eAd1
závislých	závislý	k2eAgFnPc2d1
<g/>
,	,	kIx,
např.	např.	kA
těžkých	těžký	k2eAgMnPc2d1
alkoholiků	alkoholik	k1gMnPc2
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
lhát	lhát	k5eAaImF
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
uvědomovali	uvědomovat	k5eAaImAgMnP
<g/>
,	,	kIx,
nebo	nebo	k8xC
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
pro	pro	k7c4
to	ten	k3xDgNnSc4
vědomě	vědomě	k6eAd1
rozhodli	rozhodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
tedy	tedy	k9
o	o	k7c4
lži	lež	k1gFnPc4
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
druhy	druh	k1gInPc4
soustavného	soustavný	k2eAgNnSc2d1
<g/>
,	,	kIx,
patologického	patologický	k2eAgNnSc2d1
lhaní	lhaní	k1gNnSc2
či	či	k8xC
vymýšlení	vymýšlení	k1gNnSc1
si	se	k3xPyFc3
u	u	k7c2
duševně	duševně	k6eAd1
nemocných	nemocný	k1gMnPc2
patří	patřit	k5eAaImIp3nS
konfabulace	konfabulace	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
dotyčný	dotyčný	k2eAgInSc1d1
vyplňuje	vyplňovat	k5eAaImIp3nS
mezery	mezera	k1gFnPc1
v	v	k7c6
paměti	paměť	k1gFnSc6
(	(	kIx(
<g/>
jednou	jednou	k6eAd1
odpoví	odpovědět	k5eAaPmIp3nS
na	na	k7c4
tutéž	týž	k3xTgFnSc4
otázku	otázka	k1gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
podruhé	podruhé	k6eAd1
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fantazírování	fantazírování	k1gNnSc1
<g/>
,	,	kIx,
bájivá	bájivý	k2eAgFnSc1d1
lhavost	lhavost	k1gFnSc1
či	či	k8xC
Münchhausenův	Münchhausenův	k2eAgInSc1d1
syndrom	syndrom	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
bývají	bývat	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnPc1
na	na	k7c4
poruchy	porucha	k1gFnPc4
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zautomatizované	zautomatizovaný	k2eAgNnSc1d1
lhaní	lhaní	k1gNnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vyskytovat	vyskytovat	k5eAaImF
také	také	k9
u	u	k7c2
obsedantně-kompulzivních	obsedantně-kompulzivní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
za	za	k7c7
účelem	účel	k1gInSc7
vysvětlení	vysvětlení	k1gNnSc2
kompulzí	kompulzí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bájivá	bájivý	k2eAgFnSc1d1
lhavost	lhavost	k1gFnSc1
(	(	kIx(
<g/>
pseudologia	pseudologia	k1gFnSc1
phantastica	phantastica	k1gFnSc1
<g/>
,	,	kIx,
mytomanie	mytomanie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
porucha	porucha	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mýtoman	mýtoman	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
různých	různý	k2eAgMnPc2d1
podvodníků	podvodník	k1gMnPc2
<g/>
,	,	kIx,
nelže	lhát	k5eNaImIp3nS
ze	z	k7c2
zištnosti	zištnost	k1gFnSc2
nebo	nebo	k8xC
s	s	k7c7
úmyslem	úmysl	k1gInSc7
někomu	někdo	k3yInSc3
uškodit	uškodit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
hlavní	hlavní	k2eAgInSc1d1
motiv	motiv	k1gInSc1
je	být	k5eAaImIp3nS
získání	získání	k1gNnSc4
takové	takový	k3xDgFnSc2
pozornosti	pozornost	k1gFnSc2
a	a	k8xC
respektu	respekt	k1gInSc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
by	by	kYmCp3nP
podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
nikdy	nikdy	k6eAd1
nepoznal	poznat	k5eNaPmAgMnS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
spolehl	spolehnout	k5eAaPmAgMnS
na	na	k7c4
své	svůj	k3xOyFgFnPc4
„	„	k?
<g/>
obyčejné	obyčejný	k2eAgInPc1d1
a	a	k8xC
nezajímavé	zajímavý	k2eNgInPc1d1
<g/>
“	“	k?
já	já	k3xPp1nSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trpí	trpět	k5eAaImIp3nS
nedostatkem	nedostatek	k1gInSc7
sebevědomí	sebevědomí	k1gNnSc1
a	a	k8xC
vymýšlení	vymýšlení	k1gNnSc1
historek	historka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
hraje	hrát	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
roli	role	k1gFnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
stává	stávat	k5eAaImIp3nS
drogou	droga	k1gFnSc7
<g/>
,	,	kIx,
bez	bez	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
neobejde	obejde	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Nepravá	pravý	k2eNgFnSc1d1
lež	lež	k1gFnSc1
u	u	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
U	u	k7c2
dětí	dítě	k1gFnPc2
přibližně	přibližně	k6eAd1
do	do	k7c2
období	období	k1gNnSc2
5-7	5-7	k4
let	léto	k1gNnPc2
vystupuje	vystupovat	k5eAaImIp3nS
tzv.	tzv.	kA
nepravá	pravý	k2eNgFnSc1d1
lež	lež	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
neschopnost	neschopnost	k1gFnSc4
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
fantazií	fantazie	k1gFnSc7
<g/>
,	,	kIx,
sny	sen	k1gInPc7
a	a	k8xC
realitou	realita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
záměrné	záměrný	k2eAgNnSc4d1
lhaní	lhaní	k1gNnSc4
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
ani	ani	k8xC
o	o	k7c4
duševní	duševní	k2eAgFnSc4d1
poruchu	porucha	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
součást	součást	k1gFnSc1
přirozeného	přirozený	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
ojedinělých	ojedinělý	k2eAgInPc6d1
případech	případ	k1gInPc6
nepravá	pravý	k2eNgFnSc1d1
lež	lež	k1gFnSc1
může	moct	k5eAaImIp3nS
přetrvávat	přetrvávat	k5eAaImF
i	i	k9
do	do	k7c2
pozdějšího	pozdní	k2eAgInSc2d2
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Paradoxy	paradox	k1gInPc1
</s>
<s>
Paradoxní	paradoxní	k2eAgMnSc1d1
<g/>
,	,	kIx,
protimluvný	protimluvný	k2eAgInSc1d1
výrok	výrok	k1gInSc1
nelze	lze	k6eNd1
označit	označit	k5eAaPmF
za	za	k7c4
pravdivý	pravdivý	k2eAgInSc4d1
ani	ani	k8xC
za	za	k7c4
nepravdivý	pravdivý	k2eNgInSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
daném	daný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
nedává	dávat	k5eNaImIp3nS
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
pokud	pokud	k8xS
si	se	k3xPyFc3
někdo	někdo	k3yInSc1
například	například	k6eAd1
ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
projevu	projev	k1gInSc6
protiřečí	protiřečit	k5eAaImIp3nS
<g/>
,	,	kIx,
lze	lze	k6eAd1
chápat	chápat	k5eAaImF
jeho	jeho	k3xOp3gInPc4
výroky	výrok	k1gInPc4
spíše	spíše	k9
jako	jako	k9
nepravdivé	pravdivý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
paradoxů	paradox	k1gInPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
zmínku	zmínka	k1gFnSc4
úvaha	úvaha	k1gFnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kolik	kolik	k4yIc1,k4yQc1,k4yRc1
zrnek	zrnko	k1gNnPc2
písku	písek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
hromadu	hromada	k1gFnSc4
-	-	kIx~
kdy	kdy	k6eAd1
jeden	jeden	k4xCgMnSc1
pozorovatel	pozorovatel	k1gMnSc1
může	moct	k5eAaImIp3nS
už	už	k6eAd1
písek	písek	k1gInSc4
označit	označit	k5eAaPmF
za	za	k7c4
hromadu	hromada	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pro	pro	k7c4
druhého	druhý	k4xOgMnSc4
se	se	k3xPyFc4
ještě	ještě	k9
o	o	k7c4
hromadu	hromada	k1gFnSc4
nejedná	jednat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
jednoduchý	jednoduchý	k2eAgInSc1d1
paradox	paradox	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pojmy	pojem	k1gInPc1
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
běžně	běžně	k6eAd1
užíváme	užívat	k5eAaImIp1nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
nepřesné	přesný	k2eNgFnSc2d1
a	a	k8xC
vágní	vágní	k2eAgFnSc2d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
ke	k	k7c3
sporům	spor	k1gInPc3
např.	např.	kA
o	o	k7c4
pravdivost	pravdivost	k1gFnSc4
výroku	výrok	k1gInSc2
<g/>
:	:	kIx,
Už	už	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
ví	vědět	k5eAaImIp3nS
spousta	spousta	k1gFnSc1
lidí	člověk	k1gMnPc2
-	-	kIx~
výraz	výraz	k1gInSc1
spousta	spousta	k1gFnSc1
může	moct	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgInPc6d1
kontextech	kontext	k1gInPc6
označovat	označovat	k5eAaImF
zcela	zcela	k6eAd1
odlišné	odlišný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
typy	typ	k1gInPc1
lží	lež	k1gFnPc2
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1
lež	lež	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgInSc1d1
typ	typ	k1gInSc1
lži	lež	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
tvrzeno	tvrdit	k5eAaImNgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
odporuje	odporovat	k5eAaImIp3nS
mluvčím	mluvčí	k1gMnSc7
poznané	poznaný	k2eAgFnSc2d1
skutečnosti	skutečnost	k1gFnSc2
nebo	nebo	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
přesvědčení	přesvědčení	k1gNnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
vědomě	vědomě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bývá	bývat	k5eAaImIp3nS
oklamání	oklamání	k1gNnSc1
příjemce	příjemce	k1gMnSc2
zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Polopravda	polopravda	k1gFnSc1
</s>
<s>
Jako	jako	k8xC,k8xS
polopravdu	polopravda	k1gFnSc4
chápeme	chápat	k5eAaImIp1nP
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mluvčí	mluvčit	k5eAaImIp3nS
něco	něco	k3yInSc1
podstatného	podstatný	k2eAgNnSc2d1
vynechá	vynechat	k5eAaPmIp3nS
a	a	k8xC
tím	ten	k3xDgNnSc7
vědomě	vědomě	k6eAd1
ponechává	ponechávat	k5eAaImIp3nS
někoho	někdo	k3yInSc4
v	v	k7c6
mylných	mylný	k2eAgFnPc6d1
představách	představa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
také	také	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
neopravíme	opravit	k5eNaPmIp1nP
již	již	k6eAd1
dříve	dříve	k6eAd2
existující	existující	k2eAgInSc1d1
mylný	mylný	k2eAgInSc1d1
názor	názor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polopravda	polopravda	k1gFnSc1
často	často	k6eAd1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
mluvčí	mluvčí	k1gMnSc1
uklidní	uklidnit	k5eAaPmIp3nS
své	svůj	k3xOyFgNnSc4
svědomí	svědomí	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
nezalhal	zalhat	k5eNaPmAgMnS
a	a	k8xC
část	část	k1gFnSc4
pravdy	pravda	k1gFnSc2
prozradil	prozradit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
stále	stále	k6eAd1
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
lež	lež	k1gFnSc4
<g/>
:	:	kIx,
Ve	v	k7c6
škole	škola	k1gFnSc6
se	se	k3xPyFc4
pátrá	pátrat	k5eAaImIp3nS
po	po	k7c6
žácích	žák	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
o	o	k7c6
přestávkách	přestávka	k1gFnPc6
kouří	kouřit	k5eAaImIp3nP
na	na	k7c6
záchodě	záchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
řekne	říct	k5eAaPmIp3nS
synovi	syn	k1gMnSc3
<g/>
:	:	kIx,
„	„	k?
<g/>
Doufám	doufat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
nepatříš	patřit	k5eNaImIp2nS
k	k	k7c3
těm	ten	k3xDgMnPc3
klukům	kluk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
kouří	kouřit	k5eAaImIp3nP
na	na	k7c6
záchodě	záchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víš	vědět	k5eAaImIp2nS
přece	přece	k9
<g/>
,	,	kIx,
že	že	k8xS
kouření	kouření	k1gNnSc1
je	být	k5eAaImIp3nS
škodlivé	škodlivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
Syn	syn	k1gMnSc1
odpoví	odpovědět	k5eAaPmIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Ne	ne	k9
já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
na	na	k7c6
záchodě	záchod	k1gInSc6
nekouřil	kouřit	k5eNaImAgMnS
<g/>
,	,	kIx,
vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
kouření	kouření	k1gNnSc1
je	být	k5eAaImIp3nS
škodlivé	škodlivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
Syn	syn	k1gMnSc1
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
dobře	dobře	k6eAd1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jeho	jeho	k3xOp3gMnPc1
kamarádi	kamarád	k1gMnPc1
na	na	k7c6
záchodě	záchod	k1gInSc6
kouří	kouřit	k5eAaImIp3nS
a	a	k8xC
sám	sám	k3xTgMnSc1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
někdy	někdy	k6eAd1
kouří	kouřit	k5eAaImIp3nS
v	v	k7c6
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zamlčování	zamlčování	k1gNnSc1
</s>
<s>
Pokud	pokud	k8xS
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
lidé	člověk	k1gMnPc1
navzájem	navzájem	k6eAd1
svěřovat	svěřovat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mlčení	mlčení	k1gNnSc2
ekvivalentní	ekvivalentní	k2eAgFnSc7d1
lží	lež	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
manželství	manželství	k1gNnSc6
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
manželé	manžel	k1gMnPc1
nevěru	nevěra	k1gFnSc4
sdělí	sdělit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevěrník	nevěrník	k1gMnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tedy	tedy	k9
měl	mít	k5eAaImAgInS
přiznat	přiznat	k5eAaPmF
a	a	k8xC
nečekat	čekat	k5eNaImF
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
ho	on	k3xPp3gInSc4
partner	partner	k1gMnSc1
sám	sám	k3xTgMnSc1
nezeptá	zeptat	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mlžení	mlžení	k1gNnSc1
</s>
<s>
Hovorový	hovorový	k2eAgInSc1d1
výraz	výraz	k1gInSc1
mlžení	mlžení	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
zatajování	zatajování	k1gNnSc4
<g/>
,	,	kIx,
zalhávání	zalhávání	k1gNnSc4
skutečnosti	skutečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvčí	mluvčí	k1gMnSc1
záměrně	záměrně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
nadbytečné	nadbytečný	k2eAgFnPc4d1
a	a	k8xC
okrajové	okrajový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odvedl	odvést	k5eAaPmAgMnS
pozornost	pozornost	k1gFnSc4
od	od	k7c2
pronesené	pronesený	k2eAgFnSc2d1
nepravdy	nepravda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nepřímé	přímý	k2eNgInPc1d1
požadavky	požadavek	k1gInPc1
</s>
<s>
Člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
často	často	k6eAd1
ostýchá	ostýchat	k5eAaImIp3nS
někoho	někdo	k3yInSc4
přímo	přímo	k6eAd1
požádat	požádat	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
uchyluje	uchylovat	k5eAaImIp3nS
k	k	k7c3
neupřímným	upřímný	k2eNgFnPc3d1
praktikám	praktika	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dovedou	dovést	k5eAaPmIp3nP
adresáta	adresát	k1gMnSc4
k	k	k7c3
nabídce	nabídka	k1gFnSc3
<g/>
:	:	kIx,
Chci	chtít	k5eAaImIp1nS
si	se	k3xPyFc3
od	od	k7c2
kolegy	kolega	k1gMnSc2
půjčit	půjčit	k5eAaPmF
auto	auto	k1gNnSc4
na	na	k7c4
odstěhování	odstěhování	k1gNnSc4
skříně	skříň	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nemám	mít	k5eNaImIp1nS
odvahu	odvaha	k1gFnSc4
ho	on	k3xPp3gMnSc4
o	o	k7c4
to	ten	k3xDgNnSc4
požádat	požádat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
před	před	k7c7
ním	on	k3xPp3gMnSc7
pronáším	pronášet	k5eAaImIp1nS
výroky	výrok	k1gInPc4
jako	jako	k9
<g/>
:	:	kIx,
Už	už	k6eAd1
sis	sis	k?
dal	dát	k5eAaPmAgMnS
na	na	k7c4
auto	auto	k1gNnSc4
zimní	zimní	k2eAgFnSc2d1
pneumatiky	pneumatika	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
máš	mít	k5eAaImIp2nS
<g/>
,	,	kIx,
že	že	k8xS
máš	mít	k5eAaImIp2nS
auto	auto	k1gNnSc1
<g/>
,	,	kIx,
já	já	k3xPp1nSc1
teď	teď	k6eAd1
budu	být	k5eAaImBp1nS
stěhovat	stěhovat	k5eAaImF
skříň	skříň	k1gFnSc1
a	a	k8xC
vůbec	vůbec	k9
nevím	vědět	k5eNaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
udělám	udělat	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
"	"	kIx"
<g/>
velkou	velký	k2eAgFnSc4d1
lež	lež	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
neupřímnost	neupřímnost	k1gFnSc1
je	být	k5eAaImIp3nS
zřejmá	zřejmý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Milosrdná	milosrdný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
</s>
<s>
Milosrdná	milosrdný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
altruistické	altruistický	k2eAgNnSc4d1
lhaní	lhaní	k1gNnSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
angl.	angl.	k?
white	white	k5eAaPmIp2nP
lie	lie	k?
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
)	)	kIx)
nezpůsobí	způsobit	k5eNaPmIp3nS
neshodu	neshoda	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
vyjde	vyjít	k5eAaPmIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
přináší	přinášet	k5eAaImIp3nS
určitý	určitý	k2eAgInSc4d1
užitek	užitek	k1gInSc4
lháři	lhář	k1gMnPc7
nebo	nebo	k8xC
posluchači	posluchač	k1gMnPc7
(	(	kIx(
<g/>
nebo	nebo	k8xC
oběma	dva	k4xCgFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesný	přesný	k2eAgInSc1d1
význam	význam	k1gInSc1
termínu	termín	k1gInSc2
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
konkrétní	konkrétní	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nikomu	nikdo	k3yNnSc3
neublíží	ublížit	k5eNaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
řečeny	řečen	k2eAgFnPc1d1
bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgInSc2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
většinou	většinou	k6eAd1
mezi	mezi	k7c4
milosrdné	milosrdný	k2eAgFnPc4d1
lži	lež	k1gFnPc4
nepočítají	počítat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západních	západní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
sem	sem	k6eAd1
nejčastěji	často	k6eAd3
počítá	počítat	k5eAaImIp3nS
nepravda	nepravda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
ospravedlněna	ospravedlnit	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
chtěli	chtít	k5eAaImAgMnP
předejít	předejít	k5eAaPmF
rozčilení	rozčilení	k1gNnPc4
<g/>
,	,	kIx,
povzbudit	povzbudit	k5eAaPmF
někoho	někdo	k3yInSc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
získat	získat	k5eAaPmF
výhodu	výhoda	k1gFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
někomu	někdo	k3yInSc3
uškodilo	uškodit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
milosrdné	milosrdný	k2eAgFnPc4d1
lži	lež	k1gFnPc4
je	být	k5eAaImIp3nS
sestřička	sestřička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ujišťuje	ujišťovat	k5eAaImIp3nS
nemocného	mocný	k2eNgMnSc4d1,k2eAgMnSc4d1
pacienta	pacient	k1gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
tom	ten	k3xDgInSc6
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
se	s	k7c7
zdravím	zdraví	k1gNnSc7
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
starší	starý	k2eAgMnSc1d2
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ujišťuje	ujišťovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
manželku	manželka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
stejně	stejně	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
když	když	k8xS
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
vzal	vzít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
vyjde	vyjít	k5eAaPmIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
lži	lež	k1gFnSc2
ze	z	k7c2
zdvořilosti	zdvořilost	k1gFnSc2
prostě	prostě	k6eAd1
přehlížen	přehlížet	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milosrdně	milosrdně	k6eAd1
lžou	lhát	k5eAaImIp3nP
více	hodně	k6eAd2
ženy	žena	k1gFnPc1
než	než	k8xS
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
milosrdná	milosrdný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
má	mít	k5eAaImIp3nS
však	však	k9
své	svůj	k3xOyFgFnPc4
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
důkladně	důkladně	k6eAd1
zváženo	zvážen	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
důležitost	důležitost	k1gFnSc1
pravdy	pravda	k1gFnSc2
není	být	k5eNaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
případná	případný	k2eAgFnSc1d1
nepříjemnost	nepříjemnost	k1gFnSc1
jejího	její	k3xOp3gNnSc2
vyřčení	vyřčení	k1gNnSc2
a	a	k8xC
zda	zda	k8xS
by	by	kYmCp3nS
tato	tento	k3xDgFnSc1
případná	případný	k2eAgFnSc1d1
nepříjemnost	nepříjemnost	k1gFnSc1
byla	být	k5eAaImAgFnS
skutečně	skutečně	k6eAd1
zbytečná	zbytečný	k2eAgFnSc1d1
a	a	k8xC
nedůležitá	důležitý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
totiž	totiž	k9
můžeme	moct	k5eAaImIp1nP
odvracet	odvracet	k5eAaImF
zcela	zcela	k6eAd1
zbytečné	zbytečný	k2eAgInPc1d1
konflikty	konflikt	k1gInPc1
(	(	kIx(
<g/>
otázka	otázka	k1gFnSc1
„	„	k?
<g/>
jsem	být	k5eAaImIp1nS
tlustá	tlustý	k2eAgFnSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
nelze	lze	k6eNd1
zaměnit	zaměnit	k5eAaPmF
řešení	řešení	k1gNnSc4
důležitých	důležitý	k2eAgInPc2d1
problémů	problém	k1gInPc2
za	za	k7c4
své	svůj	k3xOyFgNnSc4
pohodlí	pohodlí	k1gNnSc4
(	(	kIx(
<g/>
lhát	lhát	k5eAaImF
příteli	přítel	k1gMnSc3
<g/>
,	,	kIx,
pokud	pokud	k8xS
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
žena	žena	k1gFnSc1
nevěrná	věrný	k2eNgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
psychosomatiky	psychosomatika	k1gFnSc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
velice	velice	k6eAd1
nežádoucí	žádoucí	k2eNgNnSc1d1
zhoršovat	zhoršovat	k5eAaImF
pacientův	pacientův	k2eAgInSc4d1
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
přímo	přímo	k6eAd1
vyřčenou	vyřčený	k2eAgFnSc7d1
nepříjemnou	příjemný	k2eNgFnSc7d1
pravdou	pravda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodnější	výhodný	k2eAgInPc1d2
je	být	k5eAaImIp3nS
vyčkat	vyčkat	k5eAaPmF
na	na	k7c4
zlepšení	zlepšení	k1gNnSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
stavu	stav	k1gInSc2
a	a	k8xC
pravdu	pravda	k1gFnSc4
mu	on	k3xPp3gMnSc3
pak	pak	k9
dávkovat	dávkovat	k5eAaImF
postupně	postupně	k6eAd1
<g/>
,	,	kIx,
zaobalenou	zaobalený	k2eAgFnSc4d1
do	do	k7c2
optimistického	optimistický	k2eAgNnSc2d1
povzbuzení	povzbuzení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milosrdné	milosrdný	k2eAgNnSc4d1
lhaní	lhaní	k1gNnPc4
smrtelně	smrtelně	k6eAd1
nemocným	nemocný	k2eAgMnPc3d1,k2eNgMnPc3d1
pacientům	pacient	k1gMnPc3
lze	lze	k6eAd1
proto	proto	k8xC
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
omluvit	omluvit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nikoli	nikoli	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
pacient	pacient	k1gMnSc1
výslovně	výslovně	k6eAd1
přeje	přát	k5eAaImIp3nS
dozvědět	dozvědět	k5eAaPmF
se	se	k3xPyFc4
pravdu	pravda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
naopak	naopak	k6eAd1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
mít	mít	k5eAaImF
pacient	pacient	k1gMnSc1
právo	právo	k1gNnSc4
nevědět	vědět	k5eNaImF
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
nepřeje	přát	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Se	s	k7c7
lhaním	lhaní	k1gNnSc7
souvisí	souviset	k5eAaImIp3nS
i	i	k9
výzkum	výzkum	k1gInSc4
placebo	placebo	k1gNnSc4
efektu	efekt	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
pacientovi	pacient	k1gMnSc3
podáván	podávat	k5eAaImNgInS
neúčinný	účinný	k2eNgInSc1d1
lék	lék	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
dětem	dítě	k1gFnPc3
</s>
<s>
Lež	lež	k1gFnSc1
dětem	dítě	k1gFnPc3
je	být	k5eAaImIp3nS
nepravdivé	pravdivý	k2eNgNnSc1d1
vysvětlení	vysvětlení	k1gNnSc1
určitého	určitý	k2eAgNnSc2d1
tématu	téma	k1gNnSc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
například	například	k6eAd1
sexu	sex	k1gInSc3
<g/>
)	)	kIx)
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
přijatelnější	přijatelný	k2eAgNnSc1d2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInSc4d3
příklad	příklad	k1gInSc4
je	být	k5eAaImIp3nS
Přinesl	přinést	k5eAaPmAgMnS
tě	ty	k3xPp2nSc4
čáp	čáp	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobnou	podobný	k2eAgFnSc7d1
lží	lež	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
výrok	výrok	k1gInSc4
Dárky	dárek	k1gInPc7
nosí	nosit	k5eAaImIp3nS
Ježíšek	ježíšek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
výzkumu	výzkum	k1gInSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lež	lež	k1gFnSc1
dětem	dítě	k1gFnPc3
je	být	k5eAaImIp3nS
chápána	chápán	k2eAgFnSc1d1
různými	různý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
velmi	velmi	k6eAd1
nejednotně	jednotně	k6eNd1
<g/>
,	,	kIx,
někdo	někdo	k3yInSc1
podobné	podobný	k2eAgInPc1d1
výroky	výrok	k1gInPc1
označuje	označovat	k5eAaImIp3nS
za	za	k7c2
pravdivé	pravdivý	k2eAgFnSc2d1
<g/>
,	,	kIx,
někdo	někdo	k3yInSc1
za	za	k7c2
zcela	zcela	k6eAd1
lživé	lživý	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obranná	obranný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
</s>
<s>
Obranná	obranný	k2eAgFnSc1d1
lež	lež	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
milosrdné	milosrdný	k2eAgFnPc4d1
lži	lež	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užíváme	užívat	k5eAaImIp1nP
ji	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
hrozí	hrozit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
odhalení	odhalení	k1gNnSc1
pravdy	pravda	k1gFnSc2
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
katastrofální	katastrofální	k2eAgInPc4d1
následky	následek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soused	soused	k1gMnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
sousedka	sousedka	k1gFnSc1
byla	být	k5eAaImAgFnS
svému	svůj	k3xOyFgMnSc3
muži	muž	k1gMnPc7
nevěrná	věrný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
je	být	k5eAaImIp3nS
vzteklý	vzteklý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soused	soused	k1gMnSc1
tedy	tedy	k9
sousedku	sousedka	k1gFnSc4
neprozradí	prozradit	k5eNaPmIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
manžel	manžel	k1gMnSc1
by	by	kYmCp3nS
jí	on	k3xPp3gFnSc3
mohl	moct	k5eAaImAgMnS
v	v	k7c4
zuřivosti	zuřivost	k1gFnPc4
vážně	vážně	k6eAd1
ublížit	ublížit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Sebeklam	sebeklam	k1gInSc1
</s>
<s>
Jako	jako	k8xC,k8xS
sebeklam	sebeklam	k1gInSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
případ	případ	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
člověk	člověk	k1gMnSc1
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
přesvědčí	přesvědčit	k5eAaPmIp3nS
o	o	k7c6
pravdivosti	pravdivost	k1gFnSc6
výroku	výrok	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
neodpovídá	odpovídat	k5eNaImIp3nS
skutečnosti	skutečnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tedy	tedy	k9
následně	následně	k6eAd1
tento	tento	k3xDgInSc1
výrok	výrok	k1gInSc1
pronáší	pronášet	k5eAaImIp3nS
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
už	už	k9
o	o	k7c4
lež	lež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebeklamům	sebeklam	k1gInPc3
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
podléhá	podléhat	k5eAaImIp3nS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
sebeklamy	sebeklam	k1gInPc1
jsou	být	k5eAaImIp3nP
však	však	k9
duševní	duševní	k2eAgFnSc7d1
nemocí	nemoc	k1gFnSc7
<g/>
:	:	kIx,
bludy	blud	k1gInPc1
<g/>
,	,	kIx,
paranoia	paranoia	k1gFnSc1
<g/>
,	,	kIx,
mentální	mentální	k2eAgFnSc1d1
anorexie	anorexie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lichotka	lichotka	k1gFnSc1
</s>
<s>
Lichotka	lichotka	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
ingraciace	ingraciace	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
přehnané	přehnaný	k2eAgNnSc1d1
chválení	chválení	k1gNnSc1
nějaké	nějaký	k3yIgFnSc2
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
s	s	k7c7
cílem	cíl	k1gInSc7
vloudit	vloudit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
její	její	k3xOp3gFnSc2
přízně	přízeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichotka	lichotka	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
pravdivá	pravdivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
častěji	často	k6eAd2
se	se	k3xPyFc4
při	při	k7c6
lichocení	lichocení	k1gNnSc6
užívají	užívat	k5eAaImIp3nP
částečné	částečný	k2eAgFnPc4d1
lži	lež	k1gFnPc4
<g/>
,	,	kIx,
nadsázky	nadsázka	k1gFnPc4
a	a	k8xC
přemrštěná	přemrštěný	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
některých	některý	k3yIgInPc2
uvedených	uvedený	k2eAgInPc2d1
typů	typ	k1gInPc2
lži	lež	k1gFnSc2
</s>
<s>
Manželka	manželka	k1gFnSc1
je	být	k5eAaImIp3nS
manželovi	manžel	k1gMnSc3
nevěrná	věrný	k2eNgFnSc1d1
se	s	k7c7
sousedem	soused	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manžel	manžel	k1gMnSc1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
zeptá	zeptat	k5eAaPmIp3nS
<g/>
,	,	kIx,
jestli	jestli	k8xS
ji	on	k3xPp3gFnSc4
soused	soused	k1gMnSc1
přitahuje	přitahovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželka	manželka	k1gFnSc1
odpoví	odpovědět	k5eAaPmIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ne	ne	k9
<g/>
,	,	kIx,
vůbec	vůbec	k9
mě	já	k3xPp1nSc4
nezajímá	zajímat	k5eNaImIp3nS
=	=	kIx~
lež	lež	k1gFnSc1
(	(	kIx(
<g/>
manželka	manželka	k1gFnSc1
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
může	moct	k5eAaImIp3nS
chápat	chápat	k5eAaImF
jako	jako	k9
milosrdnou	milosrdný	k2eAgFnSc4d1
nebo	nebo	k8xC
obrannou	obranný	k2eAgFnSc4d1
lež	lež	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
mi	já	k3xPp1nSc3
sympatický	sympatický	k2eAgInSc4d1
=	=	kIx~
polopravda	polopravda	k1gFnSc1
</s>
<s>
Dej	dát	k5eAaPmRp2nS
mi	já	k3xPp1nSc3
pokoj	pokojit	k5eAaImRp2nS,k5eAaPmRp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
=	=	kIx~
zamlčování	zamlčování	k1gNnSc1
<g/>
,	,	kIx,
vyhýbání	vyhýbání	k1gNnSc1
se	se	k3xPyFc4
odpovědi	odpověď	k1gFnPc4
</s>
<s>
Není	být	k5eNaImIp3nS
podstatné	podstatný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jestli	jestli	k8xS
mě	já	k3xPp1nSc4
přitahuje	přitahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc4
jsi	být	k5eAaImIp2nS
můj	můj	k3xOp1gMnSc1
muž	muž	k1gMnSc1
a	a	k8xC
na	na	k7c6
tom	ten	k3xDgNnSc6
nikdo	nikdo	k3yNnSc1
nic	nic	k3yNnSc1
nezmění	změnit	k5eNaPmIp3nS
=	=	kIx~
mlžení	mlžení	k1gNnSc1
</s>
<s>
Byla	být	k5eAaImAgFnS
jsem	být	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
nevěrná	věrný	k2eNgFnSc1d1
=	=	kIx~
pravda	pravda	k1gFnSc1
</s>
<s>
Tos	Tos	k?
mě	já	k3xPp1nSc4
podcenil	podcenit	k5eAaPmAgMnS
<g/>
,	,	kIx,
děláme	dělat	k5eAaImIp1nP
to	ten	k3xDgNnSc1
spolu	spolu	k6eAd1
každou	každý	k3xTgFnSc4
volnou	volný	k2eAgFnSc4d1
chvilku	chvilka	k1gFnSc4
=	=	kIx~
vylhávání	vylhávání	k1gNnSc1
<g/>
,	,	kIx,
záměrné	záměrný	k2eAgNnSc1d1
přehánění	přehánění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bude	být	k5eAaImBp3nS
pravděpodobně	pravděpodobně	k6eAd1
interpretováno	interpretovat	k5eAaBmNgNnS
jako	jako	k8xC,k8xS
ironický	ironický	k2eAgInSc1d1
výmysl	výmysl	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
pokud	pokud	k8xS
se	se	k3xPyFc4
věc	věc	k1gFnSc1
provalí	provalit	k5eAaPmIp3nS
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
vždycky	vždycky	k6eAd1
může	moct	k5eAaImIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
nelhala	lhát	k5eNaImAgFnS
</s>
<s>
Odhalení	odhalení	k1gNnSc1
lháře	lhář	k1gMnSc2
</s>
<s>
Člověk	člověk	k1gMnSc1
hovořící	hovořící	k2eAgFnSc4d1
nepravdu	nepravda	k1gFnSc4
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
prozradit	prozradit	k5eAaPmF
svým	svůj	k3xOyFgNnSc7
nevědomým	vědomý	k2eNgNnSc7d1
chováním	chování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičně	tradičně	k6eAd1
je	být	k5eAaImIp3nS
uváděno	uváděn	k2eAgNnSc4d1
např.	např.	kA
drbání	drbání	k1gNnSc4
nosu	nos	k1gInSc2
<g/>
,	,	kIx,
odvracení	odvracení	k1gNnSc4
pohledu	pohled	k1gInSc2
<g/>
,	,	kIx,
nedostatek	nedostatek	k1gInSc1
či	či	k8xC
naopak	naopak	k6eAd1
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
detailů	detail	k1gInPc2
<g/>
,	,	kIx,
neobvyklá	obvyklý	k2eNgFnSc1d1
gestikulace	gestikulace	k1gFnSc1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dosavadních	dosavadní	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
však	však	k9
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neexistují	existovat	k5eNaImIp3nP
žádná	žádný	k3yNgNnPc4
obecně	obecně	k6eAd1
platná	platný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
by	by	kYmCp3nP
vedla	vést	k5eAaImAgNnP
k	k	k7c3
odhalení	odhalení	k1gNnSc3
lháře	lhář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
Pinokiův	Pinokiův	k2eAgInSc4d1
efekt	efekt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
úplně	úplně	k6eAd1
každý	každý	k3xTgMnSc1
lhář	lhář	k1gMnSc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
prozradí	prozradit	k5eAaPmIp3nS
svým	svůj	k3xOyFgNnSc7
vystupováním	vystupování	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
ukazuje	ukazovat	k5eAaImIp3nS
jako	jako	k9
mylný	mylný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
určité	určitý	k2eAgInPc4d1
verbální	verbální	k2eAgInPc4d1
i	i	k8xC
neverbální	verbální	k2eNgInPc4d1
projevy	projev	k1gInPc4
signalizující	signalizující	k2eAgFnSc4d1
lež	lež	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
žádný	žádný	k3yNgInSc1
neplatí	platit	k5eNaImIp3nS
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
sledování	sledování	k1gNnSc3
tělesných	tělesný	k2eAgInPc2d1
projevů	projev	k1gInPc2
při	při	k7c6
výslechu	výslech	k1gInSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
odhalení	odhalení	k1gNnSc1
lži	lež	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
polygraf	polygraf	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
detektor	detektor	k1gInSc1
lži	lež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
faktory	faktor	k1gInPc1
<g/>
,	,	kIx,
ovlivňující	ovlivňující	k2eAgNnSc1d1
odhalení	odhalení	k1gNnSc1
lháře	lhář	k1gMnSc2
<g/>
:	:	kIx,
</s>
<s>
Lháře	lhář	k1gMnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
odhalit	odhalit	k5eAaPmF
tím	ten	k3xDgNnSc7
snadněji	snadno	k6eAd2
<g/>
,	,	kIx,
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
je	být	k5eAaImIp3nS
mi	já	k3xPp1nSc3
bližší	blízký	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
nejen	nejen	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
např.	např.	kA
rodiny	rodina	k1gFnSc2
nebo	nebo	k8xC
kamarádů	kamarád	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
úrovni	úroveň	k1gFnSc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
etnika	etnikum	k1gNnSc2
nebo	nebo	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odhalení	odhalení	k1gNnSc1
lháře	lhář	k1gMnSc2
přispívá	přispívat	k5eAaImIp3nS
zvýšená	zvýšený	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lháře	lhář	k1gMnPc4
snadněji	snadno	k6eAd2
odhalí	odhalit	k5eAaPmIp3nS
nezávislý	závislý	k2eNgMnSc1d1
pozorovatel	pozorovatel	k1gMnSc1
než	než	k8xS
tazatel	tazatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Lhář	lhář	k1gMnSc1
se	se	k3xPyFc4
spíše	spíše	k9
prozradí	prozradit	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
pod	pod	k7c7
tlakem	tlak	k1gInSc7
-	-	kIx~
časovým	časový	k2eAgInSc7d1
nebo	nebo	k8xC
hodnotovým	hodnotový	k2eAgInSc7d1
(	(	kIx(
<g/>
„	„	k?
<g/>
jde	jít	k5eAaImIp3nS
mu	on	k3xPp3gNnSc3
o	o	k7c6
hodně	hodně	k6eAd1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
pod	pod	k7c7
tlakem	tlak	k1gInSc7
i	i	k9
pravdomluvný	pravdomluvný	k2eAgMnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
u	u	k7c2
něj	on	k3xPp3gMnSc2
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
nezvyklému	zvyklý	k2eNgNnSc3d1
chování	chování	k1gNnSc3
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
lháře	lhář	k1gMnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
Othellova	Othellův	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
dokázáno	dokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
tazatel	tazatel	k1gMnSc1
částečně	částečně	k6eAd1
přenáší	přenášet	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
chování	chování	k1gNnSc4
na	na	k7c4
dotazovaného	dotazovaný	k2eAgMnSc4d1
<g/>
,	,	kIx,
takže	takže	k8xS
„	„	k?
<g/>
nezvyklé	zvyklý	k2eNgNnSc1d1
chování	chování	k1gNnSc1
<g/>
"	"	kIx"
může	moct	k5eAaImIp3nS
pocházet	pocházet	k5eAaImF
právě	právě	k9
od	od	k7c2
tazatele	tazatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Lháře	lhář	k1gMnSc2
odhalí	odhalit	k5eAaPmIp3nS
spíše	spíše	k9
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
s	s	k7c7
odhalováním	odhalování	k1gNnSc7
lhářů	lhář	k1gMnPc2
zkušenosti	zkušenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
se	se	k3xPyFc4
snadněji	snadno	k6eAd2
odhalí	odhalit	k5eAaPmIp3nS
z	z	k7c2
verbálních	verbální	k2eAgInPc2d1
projevů	projev	k1gInPc2
než	než	k8xS
neverbálních	verbální	k2eNgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Nebyla	být	k5eNaImAgFnS
prokázána	prokázat	k5eAaPmNgFnS
žádná	žádný	k3yNgFnSc1
souvislost	souvislost	k1gFnSc1
mezi	mezi	k7c7
pohlavím	pohlaví	k1gNnSc7
tazatele	tazatel	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
úspěšností	úspěšnost	k1gFnPc2
-	-	kIx~
ženy	žena	k1gFnPc1
odhalují	odhalovat	k5eAaImIp3nP
lháře	lhář	k1gMnSc4
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
úspěšností	úspěšnost	k1gFnSc7
jako	jako	k8xC,k8xS
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Šance	šance	k1gFnSc1
odhalit	odhalit	k5eAaPmF
lež	lež	k1gFnSc4
je	být	k5eAaImIp3nS
u	u	k7c2
běžného	běžný	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
poměrně	poměrně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
-	-	kIx~
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
označit	označit	k5eAaPmF
lháře	lhář	k1gMnSc4
<g/>
,	,	kIx,
dosahuje	dosahovat	k5eAaImIp3nS
úspěšnosti	úspěšnost	k1gFnSc2
jen	jen	k9
57	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
možnosti	možnost	k1gFnPc4
jsou	být	k5eAaImIp3nP
jen	jen	k9
dvě	dva	k4xCgFnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
50	#num#	k4
%	%	kIx~
představuje	představovat	k5eAaImIp3nS
míru	míra	k1gFnSc4
náhody	náhoda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náprava	náprava	k1gFnSc1
a	a	k8xC
výchova	výchova	k1gFnSc1
</s>
<s>
Lhaní	lhaní	k1gNnSc1
(	(	kIx(
<g/>
lež	lež	k1gFnSc1
<g/>
)	)	kIx)
nelze	lze	k6eNd1
například	například	k6eAd1
konkrétně	konkrétně	k6eAd1
ve	v	k7c6
školním	školní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
opomíjet	opomíjet	k5eAaImF
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
významným	významný	k2eAgInSc7d1
signálem	signál	k1gInSc7
možného	možný	k2eAgInSc2d1
dalšího	další	k2eAgInSc2d1
negativního	negativní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
hodnocení	hodnocení	k1gNnSc6
dětských	dětský	k2eAgFnPc2d1
lží	lež	k1gFnPc2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
často	často	k6eAd1
dítě	dítě	k1gNnSc1
lže	lhát	k5eAaImIp3nS
<g/>
,	,	kIx,
komu	kdo	k3yRnSc3,k3yQnSc3,k3yInSc3
lže	lhát	k5eAaImIp3nS
nejčastěji	často	k6eAd3
<g/>
,	,	kIx,
při	při	k7c6
jaké	jaký	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
příležitosti	příležitost	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
jaké	jaký	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
situaci	situace	k1gFnSc6
a	a	k8xC
především	především	k6eAd1
proč	proč	k6eAd1
<g/>
,	,	kIx,
za	za	k7c7
jakým	jaký	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
účelem	účel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepším	dobrý	k2eAgInSc7d3
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
prevence	prevence	k1gFnSc1
–	–	k?
předcházení	předcházení	k1gNnSc1
dospělým	dospělí	k1gMnPc3
<g/>
,	,	kIx,
nelhat	lhat	k5eNaImF,k5eNaPmF,k5eNaBmF
nikomu	nikdo	k3yNnSc3
<g/>
,	,	kIx,
říkat	říkat	k5eAaImF
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
odstraňovat	odstraňovat	k5eAaImF
nepřiměřený	přiměřený	k2eNgInSc4d1
strach	strach	k1gInSc4
z	z	k7c2
trestu	trest	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
tělesných	tělesný	k2eAgMnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lež	lež	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
základu	základ	k1gInSc2
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
založena	založen	k2eAgFnSc1d1
tzv.	tzv.	kA
autorská	autorský	k2eAgFnSc1d1
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
společenská	společenský	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
autor	autor	k1gMnSc1
i	i	k8xC
příjemce	příjemce	k1gMnSc2
zprávy	zpráva	k1gFnSc2
přijímají	přijímat	k5eAaImIp3nP
pro	pro	k7c4
hru	hra	k1gFnSc4
nevýznamnost	nevýznamnost	k1gFnSc4
pravdivosti	pravdivost	k1gFnSc2
tvrzení	tvrzení	k1gNnSc2
samého	samý	k3xTgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
na	na	k7c4
představy	představa	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nejsou	být	k5eNaImIp3nP
spojené	spojený	k2eAgFnPc1d1
se	se	k3xPyFc4
skutečností	skutečnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
všech	všecek	k3xTgInPc2
oborů	obor	k1gInPc2
kultury	kultura	k1gFnSc2
-	-	kIx~
bez	bez	k7c2
této	tento	k3xDgFnSc2
autorské	autorský	k2eAgFnSc2d1
licence	licence	k1gFnSc2
by	by	kYmCp3nS
umění	umění	k1gNnSc2
nemohlo	moct	k5eNaImAgNnS
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Jev	jev	k1gInSc1
lži	lež	k1gFnSc2
je	být	k5eAaImIp3nS
také	také	k9
často	často	k6eAd1
tématem	téma	k1gNnSc7
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Lhaní	lhaní	k1gNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
tvýho	tvýho	k?
obrannýho	obrannýho	k?
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
něj	on	k3xPp3gInSc2
jsi	být	k5eAaImIp2nS
zranitelnej	zranitelnej	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
neumíš	umět	k5eNaImIp2nS
lhát	lhát	k5eAaImF
<g/>
,	,	kIx,
nemůžeš	moct	k5eNaImIp2nS
před	před	k7c7
lidmi	člověk	k1gMnPc7
utajit	utajit	k5eAaPmF
svý	svý	k?
skutečný	skutečný	k2eAgInSc4d1
úmysly	úmysl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nezbytný	zbytný	k2eNgInSc1d1,k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vem	Vem	k?
si	se	k3xPyFc3
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
řek	řek	k1gMnSc1
<g/>
`	`	kIx"
Nelson	Nelson	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
se	s	k7c7
slepým	slepý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
díval	dívat	k5eAaImAgMnS
do	do	k7c2
dalekohledu	dalekohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
loď	loď	k1gFnSc1
na	na	k7c6
obzoru	obzor	k1gInSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
Dave	Dav	k1gInSc5
Lister	Lister	k1gMnSc1
učí	učit	k5eAaImIp3nS
lhát	lhát	k5eAaImF
Krytona	Krytona	k1gFnSc1
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Kamila	Kamila	k1gFnSc1
<g/>
“	“	k?
britského	britský	k2eAgInSc2d1
sitcomu	sitcom	k1gInSc2
Červený	Červený	k1gMnSc1
trpaslík	trpaslík	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lež	lež	k1gFnSc1
v	v	k7c6
mytologii	mytologie	k1gFnSc6
</s>
<s>
Lež	lež	k1gFnSc1
a	a	k8xC
klam	klam	k1gInSc1
měly	mít	k5eAaImAgInP
ve	v	k7c6
starověkém	starověký	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
svou	svůj	k3xOyFgFnSc4
bohyni	bohyně	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
se	se	k3xPyFc4
Apaté	Apatý	k2eAgInPc1d1
a	a	k8xC
byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
bohyně	bohyně	k1gFnSc2
temné	temný	k2eAgFnSc2d1
noci	noc	k1gFnSc2
Nyx	Nyx	k1gFnSc2
(	(	kIx(
<g/>
neměla	mít	k5eNaImAgFnS
otce	otec	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
římskou	římský	k2eAgFnSc7d1
obdobou	obdoba	k1gFnSc7
byla	být	k5eAaImAgFnS
bohyně	bohyně	k1gFnSc1
Fraus	fraus	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
v	v	k7c6
dramatu	drama	k1gNnSc6
</s>
<s>
Lež	lež	k1gFnSc1
<g/>
,	,	kIx,
klam	klam	k1gInSc1
a	a	k8xC
intriky	intrika	k1gFnPc1
jsou	být	k5eAaImIp3nP
častým	častý	k2eAgInSc7d1
motivem	motiv	k1gInSc7
Shakespearových	Shakespearových	k2eAgFnPc2d1
tragédií	tragédie	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Othella	Othello	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
</s>
<s>
Lež	lež	k1gFnSc4
a	a	k8xC
klam	klam	k1gInSc4
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
literatuře	literatura	k1gFnSc6
všech	všecek	k3xTgInPc2
žánrů	žánr	k1gInPc2
<g/>
,	,	kIx,
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
každá	každý	k3xTgFnSc1
známá	známý	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
lži	lež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
příklady	příklad	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Pohádky	pohádka	k1gFnPc1
–	–	k?
existují	existovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
pohádky	pohádka	k1gFnPc4
o	o	k7c6
potrestaných	potrestaný	k2eAgFnPc6d1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
odměněných	odměněný	k2eAgMnPc6d1
lhářích	lhář	k1gMnPc6
<g/>
;	;	kIx,
například	například	k6eAd1
pohádka	pohádka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
otec	otec	k1gMnSc1
slíbí	slíbit	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
dceru	dcera	k1gFnSc4
mládenci	mládenec	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
dokáže	dokázat	k5eAaPmIp3nS
vymyslet	vymyslet	k5eAaPmF
nejoriginálnější	originální	k2eAgFnSc4d3
lež	lež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Carlo	Carlo	k1gNnSc1
Collodi	Collod	k1gMnPc1
<g/>
:	:	kIx,
Pinocchio	Pinocchio	k6eAd1
–	–	k?
dřevěnému	dřevěný	k2eAgMnSc3d1
hrdinovi	hrdina	k1gMnSc3
Pinocchiovi	Pinocchius	k1gMnSc3
začne	začít	k5eAaPmIp3nS
nekontrolovaně	kontrolovaně	k6eNd1
růst	růst	k5eAaImF
nos	nos	k1gInSc4
pokaždé	pokaždé	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
lže	lhát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Gottfried	Gottfried	k1gMnSc1
August	August	k1gMnSc1
Bürger	Bürger	k1gMnSc1
<g/>
:	:	kIx,
Baron	baron	k1gMnSc1
Prášil	Prášil	k1gMnSc1
–	–	k?
příběh	příběh	k1gInSc1
německého	německý	k2eAgMnSc2d1
barona	baron	k1gMnSc2
von	von	k1gInSc1
Münchhausen	Münchhausen	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vypráví	vyprávět	k5eAaImIp3nS
svým	svůj	k3xOyFgMnPc3
přátelům	přítel	k1gMnPc3
prostince	prostinko	k6eAd1
přehnané	přehnaný	k2eAgFnPc4d1
historky	historka	k1gFnPc4
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
údajně	údajně	k6eAd1
hrdinského	hrdinský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Vysockij	Vysockij	k1gMnSc1
<g/>
:	:	kIx,
Pravda	pravda	k1gFnSc1
a	a	k8xC
lež	lež	k1gFnSc1
(	(	kIx(
<g/>
píseň	píseň	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
píseň	píseň	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
známá	známá	k1gFnSc1
především	především	k6eAd1
v	v	k7c6
podání	podání	k1gNnSc6
Jaromíra	Jaromír	k1gMnSc4
Nohavici	nohavice	k1gFnSc6
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
spíše	spíše	k9
uvěří	uvěřit	k5eAaPmIp3nP
lži	lež	k1gFnSc3
než	než	k8xS
pravdě	pravda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
William	William	k1gInSc1
Golding	Golding	k1gInSc1
<g/>
:	:	kIx,
Pán	pán	k1gMnSc1
much	moucha	k1gFnPc2
–	–	k?
skupina	skupina	k1gFnSc1
chlapců	chlapec	k1gMnPc2
uvízne	uvíznout	k5eAaPmIp3nS
na	na	k7c6
ostrově	ostrov	k1gInSc6
a	a	k8xC
snadno	snadno	k6eAd1
podlehne	podlehnout	k5eAaPmIp3nS
kolektivnímu	kolektivní	k2eAgInSc3d1
sebeklamu	sebeklam	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
hloubi	hloub	k1gFnSc6
ostrova	ostrov	k1gInSc2
žije	žít	k5eAaImIp3nS
nestvůra	nestvůra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
jejich	jejich	k3xOp3gInPc4
životy	život	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
George	Georg	k1gMnSc4
Orwell	Orwell	k1gMnSc1
<g/>
:	:	kIx,
1984	#num#	k4
–	–	k?
román	román	k1gInSc1
zobrazující	zobrazující	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
totalitním	totalitní	k2eAgInSc6d1
státě	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
hrdina	hrdina	k1gMnSc1
Winston	Winston	k1gInSc1
Smith	Smith	k1gMnSc1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
úřadě	úřad	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
přepisují	přepisovat	k5eAaImIp3nP
starší	starší	k1gMnPc1
vydání	vydání	k1gNnSc2
novin	novina	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
neustálému	neustálý	k2eAgNnSc3d1
překrucování	překrucování	k1gNnSc3
minulosti	minulost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Frank	Frank	k1gMnSc1
Herbert	Herbert	k1gMnSc1
<g/>
:	:	kIx,
Duna	duna	k1gFnSc1
–	–	k?
v	v	k7c6
celém	celý	k2eAgInSc6d1
románovém	románový	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
tzv.	tzv.	kA
mluvčí	mluvčí	k1gFnSc4
pravdy	pravda	k1gFnSc2
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
dokážou	dokázat	k5eAaPmIp3nP
za	za	k7c2
všech	všecek	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
rozpoznat	rozpoznat	k5eAaPmF
lež	lež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Terry	Terr	k1gInPc1
Pratchett	Pratchetta	k1gFnPc2
<g/>
:	:	kIx,
Pravda	pravda	k9
–	–	k?
fantasy	fantas	k1gInPc1
příběh	příběh	k1gInSc1
ze	z	k7c2
série	série	k1gFnSc2
Úžasná	úžasný	k2eAgFnSc1d1
Zeměplocha	Zeměploch	k1gMnSc2
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
novinové	novinový	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
(	(	kIx(
<g/>
pravdivé	pravdivý	k2eAgFnPc1d1
nebo	nebo	k8xC
smyšlené	smyšlený	k2eAgFnPc1d1
<g/>
)	)	kIx)
manipulují	manipulovat	k5eAaImIp3nP
s	s	k7c7
veřejným	veřejný	k2eAgNnSc7d1
míněním	mínění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Lež	lež	k1gFnSc1
ve	v	k7c6
filmu	film	k1gInSc6
</s>
<s>
Good	Good	k1gMnSc1
Bye	Bye	k1gMnSc1
<g/>
,	,	kIx,
Lenin	Lenin	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
–	–	k?
příklad	příklad	k1gInSc4
milosrdné	milosrdný	k2eAgFnSc2d1
lži	lež	k1gFnSc2
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
a	a	k8xC
sestra	sestra	k1gFnSc1
v	v	k7c6
(	(	kIx(
<g/>
bývalém	bývalý	k2eAgInSc6d1
<g/>
)	)	kIx)
Východním	východní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
udržují	udržovat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
vážně	vážně	k6eAd1
nemocnou	mocný	k2eNgFnSc4d1,k2eAgFnSc4d1
matku	matka	k1gFnSc4
v	v	k7c6
přesvědčení	přesvědčení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
nedošlo	dojít	k5eNaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
politického	politický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Lhář	lhář	k1gMnSc1
<g/>
,	,	kIx,
lhář	lhář	k1gMnSc1
(	(	kIx(
<g/>
Liar	Liar	k1gMnSc1
Liar	Liar	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
komedie	komedie	k1gFnSc2
o	o	k7c6
právníkovi	právník	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
celý	celý	k2eAgInSc4d1
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
nedokáže	dokázat	k5eNaPmIp3nS
zalhat	zalhat	k5eAaPmF
<g/>
,	,	kIx,
s	s	k7c7
Jimem	Jimum	k1gNnSc7
Carreym	Carreymum	k1gNnPc2
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Svět	svět	k1gInSc1
podle	podle	k7c2
Prota	Prot	k1gInSc2
(	(	kIx(
<g/>
K-Pax	K-Pax	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
film	film	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
odlišit	odlišit	k5eAaPmF
pravdu	pravda	k1gFnSc4
od	od	k7c2
výmyslu	výmysl	k1gInSc2
<g/>
,	,	kIx,
dokládá	dokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
pravdu	pravda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
(	(	kIx(
<g/>
Big	Big	k1gMnSc1
Fish	Fish	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
film	film	k1gInSc1
Tima	Timus	k1gMnSc2
Burtona	Burton	k1gMnSc2
o	o	k7c6
muži	muž	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
o	o	k7c6
svém	svůj	k3xOyFgInSc6
životě	život	k1gInSc6
vykládá	vykládat	k5eAaImIp3nS
fantastické	fantastický	k2eAgFnPc4d1
historky	historka	k1gFnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
ani	ani	k8xC
jeho	jeho	k3xOp3gMnSc1
vlastní	vlastní	k2eAgMnSc1d1
syn	syn	k1gMnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
a	a	k8xC
co	co	k9
výmysl	výmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
filmy	film	k1gInPc1
zasazují	zasazovat	k5eAaImIp3nP
hrdiny	hrdina	k1gMnPc4
do	do	k7c2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
jen	jen	k9
iluzorní	iluzorní	k2eAgInSc1d1
<g/>
,	,	kIx,
světa	svět	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
nedokážou	dokázat	k5eNaPmIp3nP
rozlišit	rozlišit	k5eAaPmF
skutečnost	skutečnost	k1gFnSc4
od	od	k7c2
klamu	klam	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
Matrix	Matrix	k1gInSc1
<g/>
,	,	kIx,
Ostrov	ostrov	k1gInSc1
<g/>
,	,	kIx,
Hra	hra	k1gFnSc1
<g/>
,	,	kIx,
Truman	Truman	k1gMnSc1
Show	show	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Podle	podle	k7c2
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HARTL	Hartl	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
HARTLOVÁ	Hartlová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologický	psychologický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
774	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Mojmír	Mojmír	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychopatologie	psychopatologie	k1gFnSc2
a	a	k8xC
psychiatrie	psychiatrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
317	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7367	#num#	k4
<g/>
-	-	kIx~
<g/>
154	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Charakteristika	charakteristika	k1gFnSc1
předškolního	předškolní	k2eAgNnSc2d1
období	období	k1gNnSc2
–	–	k?
Psychology	psycholog	k1gMnPc7
<g/>
.	.	kIx.
psychology	psycholog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
inthemind	inthemind	k1gInSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
45	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://phys.org/news/2020-05-women-told-white-lies-men.html	https://phys.org/news/2020-05-women-told-white-lies-men.html	k1gInSc1
-	-	kIx~
Women	Women	k2eAgInSc1d1
told	told	k1gInSc1
more	mor	k1gInSc5
white	white	k5eAaPmIp2nP
lies	liesa	k1gFnPc2
in	in	k?
evaluations	evaluations	k1gInSc1
than	than	k1gInSc1
men	men	k?
<g/>
:	:	kIx,
study	stud	k1gInPc1
<g/>
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
155	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
29	#num#	k4
a	a	k8xC
d.	d.	k?
↑	↑	k?
NAKONEČNÝ	NAKONEČNÝ	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1679	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
355	#num#	k4
<g/>
-	-	kIx~
<g/>
356	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Podle	podle	k7c2
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
66	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
73	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠKODA	Škoda	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FISCHER	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Slavomil	Slavomil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnSc1d1
pedagogika	pedagogika	k1gFnSc1
:	:	kIx,
edukace	edukace	k1gFnSc1
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
osob	osoba	k1gFnPc2
se	s	k7c7
somatickým	somatický	k2eAgNnSc7d1
<g/>
,	,	kIx,
psychickým	psychický	k2eAgNnSc7d1
a	a	k8xC
sociálním	sociální	k2eAgNnSc7d1
znevýhodněním	znevýhodnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7387	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠVARCOVÁ	Švarcová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrané	vybraný	k2eAgFnPc4d1
kapitoly	kapitola	k1gFnPc4
z	z	k7c2
etopedie	etopedie	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
patologie	patologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Gaudeamus	Gaudeamus	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7041	#num#	k4
<g/>
-	-	kIx~
<g/>
959	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MLEZIVA	mlezivo	k1gNnPc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
lži	lež	k1gFnSc2
:	:	kIx,
podvádění	podvádění	k1gNnSc1
a	a	k8xC
klamání	klamání	k1gNnSc1
s	s	k7c7
příklady	příklad	k1gInPc7
a	a	k8xC
obrana	obrana	k1gFnSc1
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
391	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PECK	PECK	kA
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
M.	M.	kA
Lidé	člověk	k1gMnPc1
lži	lež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
262	#num#	k4
<g/>
-	-	kIx~
<g/>
1313	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠKODA	Škoda	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FISCHER	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Slavomil	Slavomil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnSc1d1
pedagogika	pedagogika	k1gFnSc1
:	:	kIx,
edukace	edukace	k1gFnSc1
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
osob	osoba	k1gFnPc2
se	s	k7c7
somatickým	somatický	k2eAgNnSc7d1
<g/>
,	,	kIx,
psychickým	psychický	k2eAgNnSc7d1
a	a	k8xC
sociálním	sociální	k2eAgNnSc7d1
znevýhodněním	znevýhodnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7387	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠVARCOVÁ	Švarcová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrané	vybraný	k2eAgFnPc4d1
kapitoly	kapitola	k1gFnPc4
z	z	k7c2
etopedie	etopedie	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
patologie	patologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Gaudeamus	Gaudeamus	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7041	#num#	k4
<g/>
-	-	kIx~
<g/>
959	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lživé	lživý	k2eAgFnPc1d1
praktiky	praktika	k1gFnPc1
a	a	k8xC
skutečnosti	skutečnost	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Autorská	autorský	k2eAgFnSc1d1
licence	licence	k1gFnSc1
</s>
<s>
Eufemismus	eufemismus	k1gInSc1
</s>
<s>
Iluze	iluze	k1gFnSc1
</s>
<s>
Intrika	intrika	k1gFnSc1
</s>
<s>
Křivé	křivý	k2eAgNnSc1d1
svědectví	svědectví	k1gNnSc1
</s>
<s>
Logický	logický	k2eAgInSc1d1
klam	klam	k1gInSc1
</s>
<s>
Mystifikace	mystifikace	k1gFnSc1
</s>
<s>
Plagiát	plagiát	k1gInSc1
</s>
<s>
Podvod	podvod	k1gInSc1
</s>
<s>
Pomluva	pomluva	k1gFnSc1
</s>
<s>
Předsudek	předsudek	k1gInSc1
</s>
<s>
Přetvářka	přetvářka	k1gFnSc1
</s>
<s>
Tajemství	tajemství	k1gNnSc1
</s>
<s>
Zrada	zrada	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
články	článek	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Pravda	pravda	k9
</s>
<s>
Morálka	morálka	k1gFnSc1
</s>
<s>
Etika	etika	k1gFnSc1
</s>
<s>
Paradox	paradox	k1gInSc1
</s>
<s>
Paradox	paradox	k1gInSc1
lháře	lhář	k1gMnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
lež	lež	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Lež	lež	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
lež	lež	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vybíral	Vybíral	k1gMnSc1
<g/>
:	:	kIx,
Lži	lež	k1gFnPc1
<g/>
,	,	kIx,
polopravdy	polopravda	k1gFnPc1
a	a	k8xC
pravda	pravda	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4036510-4	4036510-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85138286	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85138286	#num#	k4
</s>
