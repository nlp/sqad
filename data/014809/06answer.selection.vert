<s>
Lež	lež	k1gFnSc1
je	být	k5eAaImIp3nS
typ	typ	k1gInSc4
klamu	klam	k1gInSc2
mající	mající	k2eAgFnSc4d1
formu	forma	k1gFnSc4
nepravdivého	pravdivý	k2eNgInSc2d1
výroku	výrok	k1gInSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
s	s	k7c7
vědomým	vědomý	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
oklamat	oklamat	k5eAaPmF
druhé	druhý	k4xOgFnSc2
za	za	k7c2
účelem	účel	k1gInSc7
získání	získání	k1gNnSc2
nějaké	nějaký	k3yIgFnSc2
výhody	výhoda	k1gFnSc2
či	či	k8xC
vyhnutí	vyhnutí	k1gNnSc2
se	se	k3xPyFc4
trestu	trest	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pojetí	pojetí	k1gNnSc4
rozsahu	rozsah	k1gInSc2
pojmu	pojem	k1gInSc2
lži	lež	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
i	i	k8xC
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
–	–	k?
týž	týž	k3xTgInSc4
výrok	výrok	k1gInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
označen	označit	k5eAaPmNgInS
i	i	k9
při	při	k7c6
správné	správný	k2eAgFnSc6d1
interpretaci	interpretace	k1gFnSc6
za	za	k7c4
lživý	lživý	k2eAgInSc4d1
i	i	k8xC
pravdivý	pravdivý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>