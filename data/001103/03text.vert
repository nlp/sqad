<s>
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Cháro	k1gNnPc4	Cháro
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1977	[number]	k4	1977
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejový	hokejový	k2eAgMnSc1d1	hokejový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
týmu	tým	k1gInSc2	tým
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Norris	Norris	k1gFnSc2	Norris
Trophy	Tropha	k1gFnSc2	Tropha
za	za	k7c4	za
sezonu	sezona	k1gFnSc4	sezona
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
dráhy	dráha	k1gFnSc2	dráha
jsou	být	k5eAaImIp3nP	být
nesmazatelně	smazatelně	k6eNd1	smazatelně
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
zápasníkem	zápasník	k1gMnSc7	zápasník
a	a	k8xC	a
reprezentantem	reprezentant	k1gMnSc7	reprezentant
Československa	Československo	k1gNnSc2	Československo
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Chárou	Chára	k1gMnSc7	Chára
<g/>
.	.	kIx.	.
</s>
<s>
Naučil	naučit	k5eAaPmAgMnS	naučit
jej	on	k3xPp3gMnSc4	on
gymnastické	gymnastický	k2eAgFnPc1d1	gymnastická
obratnosti	obratnost	k1gFnPc1	obratnost
<g/>
,	,	kIx,	,
zápasení	zápasení	k1gNnSc1	zápasení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
mu	on	k3xPp3gMnSc3	on
vštěpoval	vštěpovat	k5eAaBmAgMnS	vštěpovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
ne	ne	k9	ne
rváč	rváč	k1gMnSc1	rváč
<g/>
.	.	kIx.	.
</s>
<s>
Cháru	Chára	k1gFnSc4	Chára
draftoval	draftovat	k5eAaImAgMnS	draftovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
na	na	k7c4	na
56	[number]	k4	56
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
tým	tým	k1gInSc4	tým
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Islanders	Islandersa	k1gFnPc2	Islandersa
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
strávených	strávený	k2eAgInPc6d1	strávený
v	v	k7c4	v
organizaci	organizace	k1gFnSc4	organizace
Islanders	Islanders	k1gInSc1	Islanders
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senators	k1gInSc1	Senators
společně	společně	k6eAd1	společně
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Muckaltem	Muckalt	k1gInSc7	Muckalt
a	a	k8xC	a
výběrem	výběr	k1gInSc7	výběr
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
(	(	kIx(	(
<g/>
vybraným	vybraný	k2eAgMnSc7d1	vybraný
hráčem	hráč	k1gMnSc7	hráč
je	být	k5eAaImIp3nS	být
Jason	Jason	k1gMnSc1	Jason
Spezza	Spezz	k1gMnSc2	Spezz
<g/>
)	)	kIx)	)
za	za	k7c4	za
Alexeje	Alexej	k1gMnSc4	Alexej
Jašina	Jašin	k1gMnSc4	Jašin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmu	tým	k1gInSc6	tým
Senators	Senatorsa	k1gFnPc2	Senatorsa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
elitních	elitní	k2eAgInPc2d1	elitní
zadáku	zadák	k1gInSc2	zadák
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
Utkání	utkání	k1gNnSc4	utkání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
dovedností	dovednost	k1gFnPc2	dovednost
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
střelu	střela	k1gFnSc4	střela
za	za	k7c7	za
legendárním	legendární	k2eAgInSc7d1	legendární
bekem	bek	k1gInSc7	bek
Alem	alma	k1gFnPc2	alma
MacInnisem	MacInnis	k1gInSc7	MacInnis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
NHL	NHL	kA	NHL
v	v	k7c6	v
bodování	bodování	k1gNnSc6	bodování
plus	plus	k1gInSc1	plus
<g/>
/	/	kIx~	/
<g/>
minus	minus	k1gInSc1	minus
za	za	k7c7	za
Martinem	Martin	k1gMnSc7	Martin
St.	st.	kA	st.
Louisem	Louis	k1gMnSc7	Louis
a	a	k8xC	a
Markem	Marek	k1gMnSc7	Marek
Malíkem	Malík	k1gMnSc7	Malík
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Norris	Norris	k1gFnSc4	Norris
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
ale	ale	k8xC	ale
získal	získat	k5eAaPmAgMnS	získat
Scott	Scott	k1gMnSc1	Scott
Niedermayer	Niedermayer	k1gMnSc1	Niedermayer
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
výběru	výběr	k1gInSc6	výběr
hvězd	hvězda	k1gFnPc2	hvězda
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stávkové	stávkový	k2eAgFnSc6d1	stávková
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
švédský	švédský	k2eAgInSc4d1	švédský
tým	tým	k1gInSc4	tým
Färjestads	Färjestadsa	k1gFnPc2	Färjestadsa
BK	BK	kA	BK
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
dohodnout	dohodnout	k5eAaPmF	dohodnout
se	se	k3xPyFc4	se
Senators	Senators	k1gInSc1	Senators
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
kontraktu	kontrakt	k1gInSc6	kontrakt
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezony	sezona	k1gFnSc2	sezona
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
volným	volný	k2eAgMnSc7d1	volný
hráčem	hráč	k1gMnSc7	hráč
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pětiletou	pětiletý	k2eAgFnSc4d1	pětiletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
mu	on	k3xPp3gMnSc3	on
37	[number]	k4	37
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kapitánem	kapitán	k1gMnSc7	kapitán
po	po	k7c6	po
dosavadním	dosavadní	k2eAgInSc6d1	dosavadní
Joeu	Joeus	k1gInSc6	Joeus
Thorntonovi	Thornton	k1gMnSc3	Thornton
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
teprve	teprve	k6eAd1	teprve
třetím	třetí	k4xOgMnSc7	třetí
slovenským	slovenský	k2eAgMnSc7d1	slovenský
kapitánem	kapitán	k1gMnSc7	kapitán
v	v	k7c6	v
NHL	NHL	kA	NHL
po	po	k7c6	po
Peteru	Peter	k1gMnSc6	Peter
Šťastném	Šťastný	k1gMnSc6	Šťastný
a	a	k8xC	a
Stanu	stanout	k5eAaPmIp1nS	stanout
Mikitovi	Mikita	k1gMnSc3	Mikita
<g/>
.	.	kIx.	.
</s>
<s>
Chára	Chára	k1gMnSc1	Chára
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
Utkání	utkání	k1gNnSc4	utkání
hvězd	hvězda	k1gFnPc2	hvězda
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
vstřelení	vstřelení	k1gNnSc4	vstřelení
dvou	dva	k4xCgInPc2	dva
gólů	gól	k1gInPc2	gól
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
o	o	k7c4	o
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
střelu	střela	k1gFnSc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
střela	střela	k1gFnSc1	střela
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
100,4	[number]	k4	100,4
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Utkání	utkání	k1gNnSc4	utkání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
o	o	k7c4	o
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
střelu	střela	k1gFnSc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
puk	puk	k1gInSc1	puk
rychlosti	rychlost	k1gFnSc2	rychlost
211	[number]	k4	211
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
117,1	[number]	k4	117,1
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březno	k1gNnSc2	březno
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
Chára	Cháro	k1gNnPc4	Cháro
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Washington	Washington	k1gInSc1	Washington
Capitals	Capitals	k1gInSc1	Capitals
poranil	poranit	k5eAaPmAgInS	poranit
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
chyběl	chybět	k5eAaImAgInS	chybět
ale	ale	k9	ale
jen	jen	k9	jen
pět	pět	k4xCc1	pět
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
operaci	operace	k1gFnSc3	operace
odložil	odložit	k5eAaPmAgInS	odložit
na	na	k7c4	na
přestávku	přestávka	k1gFnSc4	přestávka
mezi	mezi	k7c7	mezi
sezonami	sezona	k1gFnPc7	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Bruins	Bruins	k6eAd1	Bruins
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
play-off	playff	k1gInSc4	play-off
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohráli	prohrát	k5eAaPmAgMnP	prohrát
s	s	k7c7	s
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
.	.	kIx.	.
</s>
<s>
Chára	Chára	k1gMnSc1	Chára
byl	být	k5eAaImAgMnS	být
podruhé	podruhé	k6eAd1	podruhé
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Norris	Norris	k1gFnSc4	Norris
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nechyběl	chybět	k5eNaImAgMnS	chybět
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
Utkání	utkání	k1gNnSc6	utkání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
pro	pro	k7c4	pro
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
střelu	střela	k1gFnSc4	střela
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
rychlostí	rychlost	k1gFnSc7	rychlost
169,7	[number]	k4	169,7
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
105,4	[number]	k4	105,4
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
)	)	kIx)	)
překonal	překonat	k5eAaPmAgMnS	překonat
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
rekord	rekord	k1gInSc1	rekord
Ala	ala	k1gFnSc1	ala
Iafrata	Iafrata	k1gFnSc1	Iafrata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
sezoně	sezona	k1gFnSc3	sezona
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
Norris	Norris	k1gFnSc4	Norris
Trophy	Tropha	k1gFnSc2	Tropha
když	když	k8xS	když
porazil	porazit	k5eAaPmAgInS	porazit
Mika	Mik	k1gMnSc4	Mik
Greena	Green	k1gMnSc4	Green
z	z	k7c2	z
Washington	Washington	k1gInSc1	Washington
Capitals	Capitalsa	k1gFnPc2	Capitalsa
a	a	k8xC	a
Nicklase	Nicklas	k1gInSc6	Nicklas
Lidströma	Lidströma	k1gFnSc1	Lidströma
z	z	k7c2	z
Detroit	Detroit	k1gInSc4	Detroit
Red	Red	k1gFnSc3	Red
Wings	Wings	k1gInSc4	Wings
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
překonal	překonat	k5eAaPmAgMnS	překonat
svůj	svůj	k3xOyFgInSc4	svůj
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
(	(	kIx(	(
<g/>
169,7	[number]	k4	169,7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
posunul	posunout	k5eAaPmAgMnS	posunout
hranici	hranice	k1gFnSc4	hranice
na	na	k7c4	na
175	[number]	k4	175
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
108,8	[number]	k4	108,8
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2014	[number]	k4	2014
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vlajkonošem	vlajkonoš	k1gMnSc7	vlajkonoš
slovenské	slovenský	k2eAgFnSc2d1	slovenská
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Utkání	utkání	k1gNnSc4	utkání
hvězd	hvězda	k1gFnPc2	hvězda
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2009	[number]	k4	2009
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
Týmu	tým	k1gInSc2	tým
hvězd	hvězda	k1gFnPc2	hvězda
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2008	[number]	k4	2008
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
Týmu	tým	k1gInSc2	tým
hvězd	hvězda	k1gFnPc2	hvězda
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
James	James	k1gMnSc1	James
Norris	Norris	k1gFnSc2	Norris
Memorial	Memorial	k1gMnSc1	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
o	o	k7c4	o
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
střelu	střela	k1gFnSc4	střela
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Utkání	utkání	k1gNnSc1	utkání
hvězd	hvězda	k1gFnPc2	hvězda
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
si	se	k3xPyFc3	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Nemšové	Nemšová	k1gFnSc6	Nemšová
vzal	vzít	k5eAaPmAgInS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Tatianu	Tatiana	k1gFnSc4	Tatiana
Biskupicovou	Biskupicová	k1gFnSc4	Biskupicová
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodilo	narodit	k5eAaPmAgNnS	narodit
jejich	jejich	k3xOp3gNnSc4	jejich
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Elliz	Elliza	k1gFnPc2	Elliza
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
je	být	k5eAaImIp3nS	být
čtyřnásobným	čtyřnásobný	k2eAgMnSc7d1	čtyřnásobný
mistrem	mistr	k1gMnSc7	mistr
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
řecko-římském	řecko-římský	k2eAgInSc6d1	řecko-římský
zápase	zápas	k1gInSc6	zápas
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
trenér	trenér	k1gMnSc1	trenér
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
ambasadorů	ambasador	k1gMnPc2	ambasador
charitativní	charitativní	k2eAgFnSc2d1	charitativní
organizace	organizace	k1gFnSc2	organizace
Right	Righta	k1gFnPc2	Righta
to	ten	k3xDgNnSc1	ten
Play	play	k0	play
(	(	kIx(	(
<g/>
Právo	právo	k1gNnSc4	právo
hrát	hrát	k5eAaImF	hrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
strávil	strávit	k5eAaPmAgMnS	strávit
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
Mosambiku	Mosambik	k1gInSc6	Mosambik
a	a	k8xC	a
s	s	k7c7	s
obráncem	obránce	k1gMnSc7	obránce
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flames	k1gMnSc1	Flames
Robynem	Robyn	k1gMnSc7	Robyn
Regehrem	Regehr	k1gMnSc7	Regehr
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
<s>
Domluví	domluvit	k5eAaPmIp3nS	domluvit
se	s	k7c7	s
šesti	šest	k4xCc7	šest
jazyky	jazyk	k1gInPc7	jazyk
<g/>
:	:	kIx,	:
slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c6	na
Mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
:	:	kIx,	:
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Cháro	k1gNnPc1	Cháro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chára	Chára	k1gMnSc1	Chára
<g/>
:	:	kIx,	:
Môjho	Môj	k1gMnSc4	Môj
syna	syn	k1gMnSc4	syn
odpisovali	odpisovat	k5eAaImAgMnP	odpisovat
(	(	kIx(	(
<g/>
Ernest	Ernest	k1gMnSc1	Ernest
Bokroš	Bokroš	k1gMnSc1	Bokroš
<g/>
)	)	kIx)	)
Oslava	oslava	k1gFnSc1	oslava
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cupat	k5eAaImIp1nS	cupat
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
22.7	[number]	k4	22.7
<g/>
.2011	.2011	k4	.2011
<g/>
.	.	kIx.	.
</s>
<s>
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Cháro	k1gNnPc1	Cháro
-	-	kIx~	-
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Cháro	k1gNnPc1	Cháro
-	-	kIx~	-
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Cháro	k1gNnPc1	Cháro
-	-	kIx~	-
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
