<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
starými	starý	k2eAgInPc7d1	starý
symboly	symbol	k1gInPc7	symbol
islámu	islám	k1gInSc2	islám
<g/>
:	:	kIx,	:
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
vlajce	vlajka	k1gFnSc6	vlajka
už	už	k6eAd1	už
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
