<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
:	:	kIx,	:
dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
turecké	turecký	k2eAgFnSc2d1	turecká
vlajky	vlajka	k1gFnSc2	vlajka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
starými	starý	k2eAgInPc7d1	starý
symboly	symbol	k1gInPc7	symbol
islámu	islám	k1gInSc2	islám
<g/>
:	:	kIx,	:
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
vlajce	vlajka	k1gFnSc6	vlajka
už	už	k6eAd1	už
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
se	se	k3xPyFc4	se
zvenku	zvenku	k6eAd1	zvenku
zhruba	zhruba	k6eAd1	zhruba
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
jedním	jeden	k4xCgInSc7	jeden
cípem	cíp	k1gInSc7	cíp
pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
spojnice	spojnice	k1gFnSc2	spojnice
dvou	dva	k4xCgInPc2	dva
rohů	roh	k1gInPc2	roh
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
emblém	emblém	k1gInSc1	emblém
je	být	k5eAaImIp3nS	být
posunutý	posunutý	k2eAgInSc1d1	posunutý
od	od	k7c2	od
středu	střed	k1gInSc2	střed
kousek	kousek	k6eAd1	kousek
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
naposledy	naposledy	k6eAd1	naposledy
schválená	schválený	k2eAgFnSc1d1	schválená
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
turecké	turecký	k2eAgFnSc2d1	turecká
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
vlajka	vlajka	k1gFnSc1	vlajka
tuniská	tuniský	k2eAgFnSc1d1	Tuniská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Turecka	Turecko	k1gNnSc2	Turecko
</s>
</p>
<p>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Turecka	Turecko	k1gNnSc2	Turecko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Turecká	turecký	k2eAgFnSc1d1	turecká
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
