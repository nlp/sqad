<s>
Anime	Animat	k5eAaPmIp3nS	Animat
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
zdaleka	zdaleka	k6eAd1	zdaleka
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
stylem	styl	k1gInSc7	styl
kresby	kresba	k1gFnSc2	kresba
postav	postav	k1gInSc1	postav
a	a	k8xC	a
pozadí	pozadí	k1gNnSc1	pozadí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
anime	animat	k5eAaPmIp3nS	animat
se	se	k3xPyFc4	se
však	však	k9	však
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
loutková	loutkový	k2eAgFnSc1d1	loutková
nebo	nebo	k8xC	nebo
3D	[number]	k4	3D
animace	animace	k1gFnPc4	animace
<g/>
.	.	kIx.	.
</s>
