<s>
Estádio	Estádio	k1gNnSc1	Estádio
José	Josá	k1gFnSc2	Josá
Alvalade	Alvalad	k1gInSc5	Alvalad
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
Lisabonu	Lisabon	k1gInSc2	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
hostil	hostit	k5eAaImAgInS	hostit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
několik	několik	k4yIc4	několik
utkání	utkání	k1gNnSc4	utkání
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
semifinálového	semifinálový	k2eAgNnSc2d1	semifinálové
střetnutí	střetnutí	k1gNnSc2	střetnutí
mezi	mezi	k7c7	mezi
domácím	domácí	k2eAgNnSc7d1	domácí
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
a	a	k8xC	a
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
portugalských	portugalský	k2eAgInPc2d1	portugalský
klubů	klub	k1gInPc2	klub
Sporting	Sporting	k1gInSc1	Sporting
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
52	[number]	k4	52
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
stadionů	stadion	k1gInPc2	stadion
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
UEFA	UEFA	kA	UEFA
ocenila	ocenit	k5eAaPmAgFnS	ocenit
pěti	pět	k4xCc7	pět
hvězdičkami	hvězdička	k1gFnPc7	hvězdička
<g/>
.	.	kIx.	.
</s>
