<s>
Hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
Rodentia	Rodentia	k1gFnSc1	Rodentia
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejpočetnější	početní	k2eAgInSc4d3	nejpočetnější
řád	řád	k1gInSc4	řád
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
třídy	třída	k1gFnSc2	třída
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
menší	malý	k2eAgMnPc4d2	menší
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc4	uspořádání
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgInPc1d1	přední
řezáky	řezák	k1gInPc1	řezák
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
v	v	k7c4	v
hlodáky	hlodák	k1gInPc4	hlodák
<g/>
;	;	kIx,	;
špičáky	špičák	k1gInPc4	špičák
chybí	chybět	k5eAaImIp3nP	chybět
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Hlodáky	hlodák	k1gInPc1	hlodák
mají	mít	k5eAaImIp3nP	mít
přední	přední	k2eAgFnSc4d1	přední
stranu	strana	k1gFnSc4	strana
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
sklovinu	sklovina	k1gFnSc4	sklovina
a	a	k8xC	a
zadní	zadní	k2eAgFnSc4d1	zadní
stranu	strana	k1gFnSc4	strana
mnohem	mnohem	k6eAd1	mnohem
měkčí	měkký	k2eAgFnSc7d2	měkčí
zubovinou	zubovina	k1gFnSc7	zubovina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
většímu	veliký	k2eAgNnSc3d2	veliký
obrušování	obrušování	k1gNnSc3	obrušování
měkčí	měkký	k2eAgFnSc2d2	měkčí
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
si	se	k3xPyFc3	se
zuby	zub	k1gInPc1	zub
udržují	udržovat	k5eAaImIp3nP	udržovat
stále	stále	k6eAd1	stále
ostrou	ostrý	k2eAgFnSc4d1	ostrá
hranu	hrana	k1gFnSc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
řád	řád	k1gInSc1	řád
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
2500	[number]	k4	2500
žijících	žijící	k2eAgMnPc2d1	žijící
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hlodavců	hlodavec	k1gMnPc2	hlodavec
je	být	k5eAaImIp3nS	být
všežravá	všežravý	k2eAgFnSc1d1	všežravá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
největším	veliký	k2eAgMnSc7d3	veliký
žijícím	žijící	k2eAgMnSc7d1	žijící
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
kapybara	kapybara	k1gFnSc1	kapybara
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc4	co
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
těmto	tento	k3xDgMnPc3	tento
živočichům	živočich	k1gMnPc3	živočich
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
tak	tak	k9	tak
velké	velký	k2eAgFnPc4d1	velká
úspěšnosti	úspěšnost	k1gFnPc4	úspěšnost
<g/>
?	?	kIx.	?
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přicházejí	přicházet	k5eAaImIp3nP	přicházet
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
hlavní	hlavní	k2eAgFnSc1d1	hlavní
evoluční	evoluční	k2eAgFnSc1d1	evoluční
diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
savců	savec	k1gMnPc2	savec
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
eocénu	eocén	k1gInSc6	eocén
(	(	kIx(	(
<g/>
před	před	k7c7	před
34	[number]	k4	34
až	až	k8xS	až
56	[number]	k4	56
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
čeledí	čeleď	k1gFnPc2	čeleď
hlodavců	hlodavec	k1gMnPc2	hlodavec
-	-	kIx~	-
Muridae	Muridae	k1gInSc1	Muridae
-	-	kIx~	-
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
(	(	kIx(	(
<g/>
před	před	k7c7	před
5	[number]	k4	5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ještě	ještě	k6eAd1	ještě
relativně	relativně	k6eAd1	relativně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
druhy	druh	k1gInPc7	druh
tato	tento	k3xDgFnSc1	tento
čeleď	čeleď	k1gFnSc1	čeleď
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
svoji	svůj	k3xOyFgFnSc4	svůj
genetickou	genetický	k2eAgFnSc4d1	genetická
diverzitu	diverzita	k1gFnSc4	diverzita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
také	také	k9	také
zůstala	zůstat	k5eAaPmAgFnS	zůstat
poměrně	poměrně	k6eAd1	poměrně
nespecializována	specializován	k2eNgFnSc1d1	specializován
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
jsou	být	k5eAaImIp3nP	být
hlodavci	hlodavec	k1gMnPc7	hlodavec
poměrně	poměrně	k6eAd1	poměrně
drobní	drobný	k2eAgMnPc1d1	drobný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
váží	vážit	k5eAaImIp3nP	vážit
méně	málo	k6eAd2	málo
než	než	k8xS	než
150	[number]	k4	150
g	g	kA	g
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
řada	řada	k1gFnSc1	řada
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
morče	morče	k1gNnSc1	morče
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
1800	[number]	k4	1800
<g/>
g	g	kA	g
a	a	k8xC	a
raritu	rarita	k1gFnSc4	rarita
tvoří	tvořit	k5eAaImIp3nS	tvořit
kapybara	kapybara	k1gFnSc1	kapybara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
váží	vážit	k5eAaImIp3nS	vážit
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
tělesná	tělesný	k2eAgFnSc1d1	tělesná
velikost	velikost	k1gFnSc1	velikost
jim	on	k3xPp3gMnPc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
příležitost	příležitost	k1gFnSc4	příležitost
využít	využít	k5eAaPmF	využít
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
mikrobiotopů	mikrobiotop	k1gMnPc2	mikrobiotop
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
třetí	třetí	k4xOgFnPc4	třetí
jsou	být	k5eAaImIp3nP	být
mnozí	mnohý	k2eAgMnPc1d1	mnohý
hlodavci	hlodavec	k1gMnPc1	hlodavec
mimořádně	mimořádně	k6eAd1	mimořádně
plodní	plodní	k2eAgMnPc1d1	plodní
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
vrhy	vrh	k1gInPc1	vrh
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
jsou	být	k5eAaImIp3nP	být
charakteristiky	charakteristika	k1gFnPc1	charakteristika
umožňující	umožňující	k2eAgFnPc1d1	umožňující
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
využít	využít	k5eAaPmF	využít
příznivých	příznivý	k2eAgFnPc2d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
evoluční	evoluční	k2eAgFnSc2d1	evoluční
přizpůsobivosti	přizpůsobivost	k1gFnSc2	přizpůsobivost
<g/>
,	,	kIx,	,
malých	malý	k2eAgInPc2d1	malý
tělesných	tělesný	k2eAgInPc2d1	tělesný
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
reprodukce	reprodukce	k1gFnSc2	reprodukce
umožnila	umožnit	k5eAaPmAgFnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poměrně	poměrně	k6eAd1	poměrně
neveliké	veliký	k2eNgFnPc1d1	neveliká
strukturální	strukturální	k2eAgFnPc1d1	strukturální
a	a	k8xC	a
funkční	funkční	k2eAgFnPc1d1	funkční
změny	změna	k1gFnPc1	změna
postačovaly	postačovat	k5eAaImAgFnP	postačovat
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
záplavy	záplava	k1gFnSc2	záplava
současných	současný	k2eAgInPc2d1	současný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
hlodavci	hlodavec	k1gMnPc1	hlodavec
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
před	před	k7c7	před
56	[number]	k4	56
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
paleocénu	paleocén	k1gInSc2	paleocén
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
fosílie	fosílie	k1gFnPc1	fosílie
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Laurasie	Laurasie	k1gFnSc2	Laurasie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
eocénu	eocén	k1gInSc2	eocén
se	se	k3xPyFc4	se
hlodavci	hlodavec	k1gMnPc1	hlodavec
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
spojením	spojení	k1gNnSc7	spojení
Amerik	Amerika	k1gFnPc2	Amerika
se	s	k7c7	s
hlodavci	hlodavec	k1gMnPc7	hlodavec
dostali	dostat	k5eAaPmAgMnP	dostat
také	také	k9	také
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
morčatovití	morčatovitý	k2eAgMnPc1d1	morčatovitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
hlodavcem	hlodavec	k1gMnSc7	hlodavec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
Josephoartigasia	Josephoartigasia	k1gFnSc1	Josephoartigasia
z	z	k7c2	z
období	období	k1gNnSc2	období
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
byla	být	k5eAaImAgFnS	být
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
vysoká	vysoká	k1gFnSc1	vysoká
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
vážila	vážit	k5eAaImAgFnS	vážit
až	až	k9	až
přes	přes	k7c4	přes
dvě	dva	k4xCgFnPc4	dva
tuny	tuna	k1gFnPc4	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
morčatovité	morčatovitý	k2eAgInPc4d1	morčatovitý
a	a	k8xC	a
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Hlodavci	hlodavec	k1gMnPc1	hlodavec
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
poznatků	poznatek	k1gInPc2	poznatek
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
33	[number]	k4	33
čeledí	čeleď	k1gFnPc2	čeleď
seskupených	seskupený	k2eAgMnPc2d1	seskupený
do	do	k7c2	do
pěti	pět	k4xCc2	pět
podřádů	podřád	k1gInPc2	podřád
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
členěných	členěný	k2eAgMnPc2d1	členěný
do	do	k7c2	do
dvou	dva	k4xCgMnPc2	dva
infrařádů	infrařád	k1gMnPc2	infrařád
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
nadčeledí	nadčeleď	k1gFnPc2	nadčeleď
<g/>
:	:	kIx,	:
řád	řád	k1gInSc1	řád
Rodentia	Rodentium	k1gNnSc2	Rodentium
-	-	kIx~	-
hlodavci	hlodavec	k1gMnPc1	hlodavec
podřád	podřád	k1gInSc1	podřád
Sciuromorpha	Sciuromorpha	k1gFnSc1	Sciuromorpha
-	-	kIx~	-
veverkočelistní	veverkočelistní	k2eAgFnSc1d1	veverkočelistní
čeleď	čeleď	k1gFnSc1	čeleď
Aplodontiidae	Aplodontiidae	k1gFnSc1	Aplodontiidae
-	-	kIx~	-
bobruškovití	bobruškovitý	k2eAgMnPc1d1	bobruškovitý
čeleď	čeleď	k1gFnSc1	čeleď
Sciuridae	Sciuridae	k1gNnSc4	Sciuridae
-	-	kIx~	-
veverkovití	veverkovitý	k2eAgMnPc1d1	veverkovitý
čeleď	čeleď	k1gFnSc1	čeleď
Gliridae	Gliridae	k1gNnPc7	Gliridae
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc1	synonymum
Myoxidae	Myoxida	k1gInSc2	Myoxida
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
plchovití	plchovitý	k2eAgMnPc1d1	plchovitý
podřád	podřád	k1gInSc4	podřád
Castorimorpha	Castorimorpha	k1gFnSc1	Castorimorpha
čeleď	čeleď	k1gFnSc1	čeleď
Castoridae	Castoridae	k1gFnSc1	Castoridae
-	-	kIx~	-
bobrovití	bobrovitý	k2eAgMnPc1d1	bobrovitý
čeleď	čeleď	k1gFnSc1	čeleď
Heteromyidae	Heteromyidae	k1gNnPc7	Heteromyidae
-	-	kIx~	-
pytloušovití	pytloušovití	k1gMnPc1	pytloušovití
čeleď	čeleď	k1gFnSc1	čeleď
Geomyidae	Geomyidae	k1gFnSc1	Geomyidae
-	-	kIx~	-
pytlonošovití	pytlonošovití	k1gMnPc1	pytlonošovití
podřád	podřád	k1gInSc1	podřád
Myomorpha	Myomorpha	k1gFnSc1	Myomorpha
-	-	kIx~	-
myšovci	myšovec	k1gMnSc3	myšovec
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
Dipodoidea	Dipodoideum	k1gNnSc2	Dipodoideum
čeleď	čeleď	k1gFnSc1	čeleď
Dipodidae	Dipodidae	k1gFnSc1	Dipodidae
-	-	kIx~	-
tarbíkovití	tarbíkovitý	k2eAgMnPc1d1	tarbíkovitý
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
Muroidea	Muroidea	k1gFnSc1	Muroidea
čeleď	čeleď	k1gFnSc1	čeleď
Platacanthomyidae	Platacanthomyidae	k1gFnSc1	Platacanthomyidae
(	(	kIx(	(
<g/>
ostnoplši	ostnoplsat	k5eAaPmIp1nSwK	ostnoplsat
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
Spalacidae	Spalacidae	k1gFnSc1	Spalacidae
-	-	kIx~	-
slepcovití	slepcovitý	k2eAgMnPc1d1	slepcovitý
čeleď	čeleď	k1gFnSc1	čeleď
Calomyscidae	Calomyscidae	k1gNnSc4	Calomyscidae
(	(	kIx(	(
<g/>
křečíci	křečík	k1gMnPc5	křečík
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
Cricetidae	Cricetidae	k1gFnSc1	Cricetidae
-	-	kIx~	-
křečci	křeček	k1gMnPc1	křeček
praví	pravit	k5eAaImIp3nP	pravit
<g />
.	.	kIx.	.
</s>
<s>
čeleď	čeleď	k1gFnSc1	čeleď
Nesomyidae	Nesomyidae	k1gFnPc2	Nesomyidae
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc1	myš
a	a	k8xC	a
křečci	křeček	k1gMnPc1	křeček
vyčlenění	vyčleněný	k2eAgMnPc1d1	vyčleněný
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Muridae	Murida	k1gFnSc2	Murida
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
Muridae	Muridae	k1gFnSc1	Muridae
-	-	kIx~	-
myšovití	myšovití	k1gMnPc1	myšovití
podřád	podřád	k1gInSc4	podřád
Anomaluromorpha	Anomaluromorpha	k1gFnSc1	Anomaluromorpha
čeleď	čeleď	k1gFnSc1	čeleď
Anomaluridae	Anomaluridae	k1gFnSc1	Anomaluridae
-	-	kIx~	-
šupinatkovití	šupinatkovitý	k2eAgMnPc1d1	šupinatkovitý
čeleď	čeleď	k1gFnSc1	čeleď
Pedetidae	Pedetidae	k1gNnPc7	Pedetidae
-	-	kIx~	-
noháčovití	noháčovití	k1gMnPc1	noháčovití
podřád	podřád	k1gInSc1	podřád
Hystricomorpha	Hystricomorpha	k1gFnSc1	Hystricomorpha
-	-	kIx~	-
dikobrazočelistní	dikobrazočelistní	k2eAgFnSc1d1	dikobrazočelistní
infrařád	infrařáda	k1gFnPc2	infrařáda
Ctenodactylomorphi	Ctenodactylomorph	k1gInSc3	Ctenodactylomorph
čeleď	čeleď	k1gFnSc4	čeleď
Ctenodactylidae	Ctenodactylidae	k1gNnSc2	Ctenodactylidae
-	-	kIx~	-
gundiovití	gundiovití	k1gMnPc1	gundiovití
infrařád	infrařáda	k1gFnPc2	infrařáda
Hystricognathi	Hystricognath	k1gFnSc2	Hystricognath
čeleď	čeleď	k1gFnSc1	čeleď
Bathyergidae	Bathyergidae	k1gFnSc1	Bathyergidae
-	-	kIx~	-
rypošovití	rypošovití	k1gMnPc1	rypošovití
čeleď	čeleď	k1gFnSc1	čeleď
Hystricidae	Hystricidae	k1gFnSc1	Hystricidae
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
dikobrazovití	dikobrazovitý	k2eAgMnPc1d1	dikobrazovitý
čeleď	čeleď	k1gFnSc1	čeleď
Petromuridae	Petromuridae	k1gFnPc7	Petromuridae
-	-	kIx~	-
skalní	skalní	k2eAgFnPc1d1	skalní
krysy	krysa	k1gFnPc1	krysa
čeleď	čeleď	k1gFnSc1	čeleď
Thryonomyidae	Thryonomyidae	k1gFnSc1	Thryonomyidae
-	-	kIx~	-
řekomyšovití	řekomyšovití	k1gMnPc1	řekomyšovití
čeleď	čeleď	k1gFnSc1	čeleď
Erethizontidae	Erethizontidae	k1gFnSc1	Erethizontidae
-	-	kIx~	-
urzonovití	urzonovitý	k2eAgMnPc1d1	urzonovitý
čeleď	čeleď	k1gFnSc1	čeleď
Chinchillidae	Chinchillidae	k1gNnSc4	Chinchillidae
-	-	kIx~	-
činčilovití	činčilovitý	k2eAgMnPc1d1	činčilovitý
čeleď	čeleď	k1gFnSc1	čeleď
Dinomyidae	Dinomyidae	k1gNnSc4	Dinomyidae
-	-	kIx~	-
pakaranovití	pakaranovitý	k2eAgMnPc1d1	pakaranovitý
čeleď	čeleď	k1gFnSc1	čeleď
Caviidae	Caviidae	k1gNnSc4	Caviidae
-	-	kIx~	-
morčatovití	morčatovitý	k2eAgMnPc1d1	morčatovitý
čeleď	čeleď	k1gFnSc1	čeleď
Dasyproctidae	Dasyproctidae	k1gNnPc7	Dasyproctidae
-	-	kIx~	-
agutiovití	agutiovití	k1gMnPc1	agutiovití
čeleď	čeleď	k1gFnSc1	čeleď
Cuniculidae	Cuniculidae	k1gFnSc1	Cuniculidae
-	-	kIx~	-
pakovití	pakovití	k1gMnPc1	pakovití
čeleď	čeleď	k1gFnSc1	čeleď
Ctenomyidae	Ctenomyidae	k1gFnSc1	Ctenomyidae
-	-	kIx~	-
tukotukovití	tukotukovitý	k2eAgMnPc1d1	tukotukovitý
čeleď	čeleď	k1gFnSc1	čeleď
Octodontidae	Octodontidae	k1gNnSc4	Octodontidae
-	-	kIx~	-
osmákovití	osmákovitý	k2eAgMnPc1d1	osmákovitý
čeleď	čeleď	k1gFnSc1	čeleď
Abrocomidae	Abrocomidae	k1gNnSc4	Abrocomidae
-	-	kIx~	-
činčilákovití	činčilákovitý	k2eAgMnPc1d1	činčilákovitý
čeleď	čeleď	k1gFnSc1	čeleď
Echimyidae	Echimyidae	k1gNnSc4	Echimyidae
-	-	kIx~	-
korovití	korovitý	k2eAgMnPc1d1	korovitý
čeleď	čeleď	k1gFnSc1	čeleď
Myocastoridae	Myocastoridae	k1gNnPc7	Myocastoridae
-	-	kIx~	-
nutriovití	nutriovití	k1gMnPc1	nutriovití
čeleď	čeleď	k1gFnSc1	čeleď
Capromyidae	Capromyidae	k1gFnSc1	Capromyidae
-	-	kIx~	-
hutiovití	hutiovití	k1gMnPc1	hutiovití
čeleď	čeleď	k1gFnSc1	čeleď
†	†	k?	†
<g/>
Heptaxodontidae	Heptaxodontidae	k1gInSc1	Heptaxodontidae
-	-	kIx~	-
velehutiovití	velehutiovití	k1gMnPc1	velehutiovití
Fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
hlodavců	hlodavec	k1gMnPc2	hlodavec
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gFnSc7	jejich
rychlou	rychlý	k2eAgFnSc7d1	rychlá
evolucí	evoluce	k1gFnSc7	evoluce
<g/>
,	,	kIx,	,
komplikující	komplikující	k2eAgFnSc1d1	komplikující
přesné	přesný	k2eAgNnSc4d1	přesné
stanovení	stanovení	k1gNnSc4	stanovení
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
morfologických	morfologický	k2eAgNnPc2d1	morfologické
dat	datum	k1gNnPc2	datum
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
angulárního	angulární	k2eAgInSc2d1	angulární
výběžku	výběžek	k1gInSc2	výběžek
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
(	(	kIx(	(
<g/>
mandibule	mandibula	k1gFnSc6	mandibula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
můžeme	moct	k5eAaImIp1nP	moct
hlodavce	hlodavec	k1gMnSc4	hlodavec
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
sciurognátní	sciurognátní	k2eAgFnSc4d1	sciurognátní
(	(	kIx(	(
<g/>
veverkočelistní	veverkočelistní	k2eAgFnSc4d1	veverkočelistní
<g/>
)	)	kIx)	)
a	a	k8xC	a
hystrikognátní	hystrikognátní	k2eAgFnSc1d1	hystrikognátní
(	(	kIx(	(
<g/>
dikobrazočelistní	dikobrazočelistní	k2eAgFnSc1d1	dikobrazočelistní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
morfologickým	morfologický	k2eAgInSc7d1	morfologický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
utvoření	utvoření	k1gNnSc1	utvoření
komplexu	komplex	k1gInSc2	komplex
žvýkacího	žvýkací	k2eAgInSc2d1	žvýkací
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
typy	typ	k1gInPc1	typ
-	-	kIx~	-
protogomorfní	protogomorfní	k2eAgNnPc1d1	protogomorfní
<g/>
,	,	kIx,	,
myomorfní	myomorfní	k2eAgNnPc1d1	myomorfní
<g/>
,	,	kIx,	,
sciuromorfní	sciuromorfní	k2eAgNnPc1d1	sciuromorfní
a	a	k8xC	a
hystrikomorfní	hystrikomorfní	k2eAgNnPc1d1	hystrikomorfní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
charakteristiky	charakteristika	k1gFnPc1	charakteristika
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
různě	různě	k6eAd1	různě
kombinovat	kombinovat	k5eAaImF	kombinovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rypošovití	rypošovití	k1gMnPc1	rypošovití
jsou	být	k5eAaImIp3nP	být
hystrikognáti	hystrikognát	k1gMnPc1	hystrikognát
s	s	k7c7	s
protrogomorfním	protrogomorfní	k2eAgNnSc7d1	protrogomorfní
uspořádáním	uspořádání	k1gNnSc7	uspořádání
žvýkacího	žvýkací	k2eAgInSc2d1	žvýkací
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hlodavci	hlodavec	k1gMnPc1	hlodavec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Rodentia	Rodentius	k1gMnSc4	Rodentius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
