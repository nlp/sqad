<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
má	mít	k5eAaImIp3nS	mít
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
list	list	k1gInSc1	list
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
panslavistických	panslavistický	k2eAgFnPc6d1	panslavistická
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
vlajka	vlajka	k1gFnSc1	vlajka
užívaná	užívaný	k2eAgFnSc1d1	užívaná
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc1d1	námořní
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
štítu	štít	k1gInSc2	štít
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
ale	ale	k8xC	ale
používali	používat	k5eAaImAgMnP	používat
vlajku	vlajka	k1gFnSc4	vlajka
v	v	k7c6	v
panslovanských	panslovanský	k2eAgFnPc6d1	panslovanská
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ještě	ještě	k9	ještě
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SFRJ	SFRJ	kA	SFRJ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
socialistické	socialistický	k2eAgNnSc1d1	socialistické
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
za	za	k7c4	za
vlajku	vlajka	k1gFnSc4	vlajka
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
trikolora	trikolora	k1gFnSc1	trikolora
červená-bílá-modrá	červenáíláodrý	k2eAgFnSc1d1	červená-bílá-modrý
s	s	k7c7	s
rudou	rudý	k2eAgFnSc7d1	rudá
jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nahradil	nahradit	k5eAaPmAgMnS	nahradit
hvězdu	hvězda	k1gFnSc4	hvězda
znak	znak	k1gInSc4	znak
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
upraven	upraven	k2eAgInSc1d1	upraven
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc4	jeho
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
prostředním	prostřední	k2eAgNnSc6d1	prostřední
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
by	by	kYmCp3nS	by
připomínalo	připomínat	k5eAaImAgNnS	připomínat
vlajku	vlajka	k1gFnSc4	vlajka
používanou	používaný	k2eAgFnSc4d1	používaná
ustašovci	ustašovec	k1gMnPc1	ustašovec
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
tak	tak	k9	tak
znak	znak	k1gInSc1	znak
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
do	do	k7c2	do
červeného	červené	k1gNnSc2	červené
i	i	k8xC	i
modrého	modré	k1gNnSc2	modré
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
přidána	přidat	k5eAaPmNgFnS	přidat
byla	být	k5eAaImAgFnS	být
i	i	k9	i
znaková	znakový	k2eAgFnSc1d1	znaková
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
chorvatských	chorvatský	k2eAgFnPc2d1	chorvatská
žup	župa	k1gFnPc2	župa
==	==	k?	==
</s>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
21	[number]	k4	21
žup	župa	k1gFnPc2	župa
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
žup	župa	k1gFnPc2	župa
je	být	k5eAaImIp3nS	být
i	i	k9	i
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
župy	župa	k1gFnPc1	župa
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnPc1d1	vlastní
vlajky	vlajka	k1gFnPc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
</s>
</p>
<p>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
