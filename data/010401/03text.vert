<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
392	[number]	k4	392
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Velkého	velký	k2eAgNnSc2d1	velké
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
do	do	k7c2	do
Tulešic	Tulešice	k1gInPc2	Tulešice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
45	[number]	k4	45
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
dvěma	dva	k4xCgInPc7	dva
kraji	kraj	k1gInPc7	kraj
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
okresy	okres	k1gInPc7	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vedení	vedení	k1gNnSc1	vedení
silnice	silnice	k1gFnSc2	silnice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
===	===	k?	===
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
602	[number]	k4	602
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petráveč	Petráveč	k1gMnSc1	Petráveč
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
Heřmanice	Heřmanice	k1gFnPc1	Heřmanice
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3922	[number]	k4	3922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tasov	Tasov	k1gInSc4	Tasov
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
390	[number]	k4	390
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3923	[number]	k4	3923
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3924	[number]	k4	3924
<g/>
,	,	kIx,	,
peáž	peáž	k1gFnSc1	peáž
s	s	k7c7	s
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
390	[number]	k4	390
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
===	===	k?	===
</s>
</p>
<p>
<s>
Čikov	Čikov	k1gInSc4	Čikov
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3926	[number]	k4	3926
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3995	[number]	k4	3995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jasenice	Jasenice	k1gFnSc1	Jasenice
</s>
</p>
<p>
<s>
Pucov	Pucov	k1gInSc4	Pucov
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3928	[number]	k4	3928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jinošov	Jinošov	k1gInSc4	Jinošov
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
399	[number]	k4	399
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3956	[number]	k4	3956
<g/>
,	,	kIx,	,
peáž	peáž	k1gFnSc1	peáž
s	s	k7c7	s
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
399	[number]	k4	399
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otradice	Otradice	k1gFnPc1	Otradice
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3994	[number]	k4	3994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kralice	Kralice	k1gFnPc1	Kralice
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Březník	Březník	k1gMnSc1	Březník
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39212	[number]	k4	39212
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kuroslepy	Kuroslepa	k1gFnPc1	Kuroslepa
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39213	[number]	k4	39213
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mohelno	Mohelna	k1gFnSc5	Mohelna
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39214	[number]	k4	39214
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3935	[number]	k4	3935
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39217	[number]	k4	39217
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39219	[number]	k4	39219
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dukovany	Dukovany	k1gInPc1	Dukovany
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
152	[number]	k4	152
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
15248	[number]	k4	15248
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Znojmo	Znojmo	k1gNnSc1	Znojmo
===	===	k?	===
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Dubňany	Dubňan	k1gMnPc7	Dubňan
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4131	[number]	k4	4131
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39220	[number]	k4	39220
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tulešice	Tulešice	k1gFnPc1	Tulešice
(	(	kIx(	(
<g/>
křiž	křížit	k5eAaImRp2nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
396	[number]	k4	396
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
39221	[number]	k4	39221
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
392	[number]	k4	392
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Geoportal	Geoportat	k5eAaPmAgMnS	Geoportat
</s>
</p>
