<s>
Barva	barva	k1gFnSc1	barva
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
panarabské	panarabský	k2eAgFnPc4d1	panarabská
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
se	se	k3xPyFc4	se
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
