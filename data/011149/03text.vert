<p>
<s>
Akvarel	akvarel	k1gInSc1	akvarel
je	být	k5eAaImIp3nS	být
malba	malba	k1gFnSc1	malba
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
barva	barva	k1gFnSc1	barva
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
vodová	vodový	k2eAgFnSc1d1	vodová
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maluje	malovat	k5eAaImIp3nS	malovat
se	se	k3xPyFc4	se
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
povrchy	povrch	k1gInPc4	povrch
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
papyrus	papyrus	k1gInSc1	papyrus
<g/>
,	,	kIx,	,
plast	plast	k1gInSc1	plast
nebo	nebo	k8xC	nebo
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
plátno	plátno	k1gNnSc1	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Vodová	vodový	k2eAgFnSc1d1	vodová
barva	barva	k1gFnSc1	barva
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
kryje	krýt	k5eAaImIp3nS	krýt
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
podklad	podklad	k1gInSc1	podklad
obvykle	obvykle	k6eAd1	obvykle
prosvítá	prosvítat	k5eAaImIp3nS	prosvítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
akvarelu	akvarel	k1gInSc2	akvarel
==	==	k?	==
</s>
</p>
<p>
<s>
Akvarelová	akvarelový	k2eAgFnSc1d1	akvarelová
malba	malba	k1gFnSc1	malba
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
starého	starý	k2eAgInSc2d1	starý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
význačné	význačný	k2eAgFnPc1d1	význačná
osobnosti	osobnost	k1gFnPc1	osobnost
byly	být	k5eAaImAgFnP	být
malovány	malovat	k5eAaImNgFnP	malovat
na	na	k7c4	na
svitky	svitek	k1gInPc4	svitek
papyru	papyr	k1gInSc2	papyr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podobné	podobný	k2eAgInPc1d1	podobný
znaky	znak	k1gInPc1	znak
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
středověkých	středověký	k2eAgInPc6d1	středověký
rukopisech	rukopis	k1gInPc6	rukopis
a	a	k8xC	a
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
renesance	renesance	k1gFnSc2	renesance
tvořil	tvořit	k5eAaImAgInS	tvořit
i	i	k9	i
význačný	význačný	k2eAgMnSc1d1	význačný
předchůdce	předchůdce	k1gMnSc1	předchůdce
akvarelu	akvarel	k1gInSc2	akvarel
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
rytec	rytec	k1gMnSc1	rytec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
<g/>
,	,	kIx,	,
umělecky	umělecky	k6eAd1	umělecky
plodného	plodný	k2eAgInSc2d1	plodný
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
–	–	k?	–
<g/>
1528	[number]	k4	1528
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgMnS	napsat
tři	tři	k4xCgFnPc4	tři
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
rytin	rytina	k1gFnPc2	rytina
a	a	k8xC	a
namaloval	namalovat	k5eAaPmAgMnS	namalovat
188	[number]	k4	188
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
86	[number]	k4	86
akvarelů	akvarel	k1gInPc2	akvarel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
akvarelu	akvarel	k1gInSc2	akvarel
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Dürerově	Dürerův	k2eAgFnSc6d1	Dürerova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
akvarelu	akvarel	k1gInSc3	akvarel
používalo	používat	k5eAaImAgNnS	používat
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xS	jako
doplňkového	doplňkový	k2eAgNnSc2d1	doplňkové
média	médium	k1gNnSc2	médium
pro	pro	k7c4	pro
navrhování	navrhování	k1gNnSc4	navrhování
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
mistři	mistr	k1gMnPc1	mistr
italské	italský	k2eAgFnSc2d1	italská
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
<g/>
,	,	kIx,	,
Rafael	Rafaela	k1gFnPc2	Rafaela
a	a	k8xC	a
Michelangelo	Michelangela	k1gFnSc5	Michelangela
i	i	k9	i
mistři	mistr	k1gMnPc1	mistr
vlámské	vlámský	k2eAgFnSc2d1	vlámská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zhotovili	zhotovit	k5eAaPmAgMnP	zhotovit
bezpočet	bezpočet	k1gInSc4	bezpočet
úchvatných	úchvatný	k2eAgFnPc2d1	úchvatná
kreseb	kresba	k1gFnPc2	kresba
perem	pero	k1gNnSc7	pero
a	a	k8xC	a
štětcem	štětec	k1gInSc7	štětec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
malovali	malovat	k5eAaImAgMnP	malovat
francouzští	francouzský	k2eAgMnPc1d1	francouzský
malíři	malíř	k1gMnPc1	malíř
Nicolas	Nicolas	k1gMnSc1	Nicolas
Poussin	Poussin	k1gMnSc1	Poussin
a	a	k8xC	a
Claude	Claud	k1gInSc5	Claud
Lorrain	Lorrain	k2eAgInSc1d1	Lorrain
kvašem	kvaš	k1gInSc7	kvaš
krajiny	krajina	k1gFnSc2	krajina
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
předzvěst	předzvěst	k1gFnSc4	předzvěst
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
akvarelu	akvarel	k1gInSc2	akvarel
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
britští	britský	k2eAgMnPc1d1	britský
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
akvarel	akvarel	k1gInSc4	akvarel
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
značný	značný	k2eAgInSc4d1	značný
nárůst	nárůst	k1gInSc4	nárůst
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
krajinářské	krajinářský	k2eAgFnSc2d1	krajinářská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodujícím	rozhodující	k2eAgMnSc7d1	rozhodující
činitelem	činitel	k1gMnSc7	činitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
akvarelu	akvarel	k1gInSc2	akvarel
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
akvarelovou	akvarelový	k2eAgFnSc4d1	akvarelová
malbu	malba	k1gFnSc4	malba
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
pastelu	pastel	k1gInSc2	pastel
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
olejomalby	olejomalba	k1gFnPc1	olejomalba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
kvalita	kvalita	k1gFnSc1	kvalita
obrazů	obraz	k1gInPc2	obraz
Williama	William	k1gMnSc2	William
Turnera	turner	k1gMnSc2	turner
<g/>
,	,	kIx,	,
Thomase	Thomas	k1gMnSc2	Thomas
Girtina	Girtin	k1gMnSc2	Girtin
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
jejich	jejich	k3xOp3gMnPc2	jejich
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turner	turner	k1gMnSc1	turner
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
mistrů	mistr	k1gMnPc2	mistr
anglického	anglický	k2eAgNnSc2d1	anglické
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
vynikajícího	vynikající	k2eAgMnSc2d1	vynikající
malíře	malíř	k1gMnSc2	malíř
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
věhlasného	věhlasný	k2eAgMnSc4d1	věhlasný
akvarelistu	akvarelista	k1gMnSc4	akvarelista
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
jej	on	k3xPp3gMnSc4	on
Monet	moneta	k1gFnPc2	moneta
<g/>
,	,	kIx,	,
Manet	manet	k1gInSc1	manet
<g/>
,	,	kIx,	,
Pissarro	Pissarro	k1gNnSc1	Pissarro
<g/>
,	,	kIx,	,
Degas	Degas	k1gInSc1	Degas
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
náznaky	náznak	k1gInPc7	náznak
impresionismu	impresionismus	k1gInSc2	impresionismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Parkes	Parkes	k1gMnSc1	Parkes
Bonington	Bonington	k1gInSc4	Bonington
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnSc1d1	přední
anglický	anglický	k2eAgMnSc1d1	anglický
akvarelista	akvarelista	k1gMnSc1	akvarelista
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
akvarelovou	akvarelový	k2eAgFnSc4d1	akvarelová
malbu	malba	k1gFnSc4	malba
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
umělci	umělec	k1gMnPc1	umělec
nadšeně	nadšeně	k6eAd1	nadšeně
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
Eugè	Eugè	k1gMnSc3	Eugè
Delacroix	Delacroix	k1gInSc4	Delacroix
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
během	během	k7c2	během
putování	putování	k1gNnSc2	putování
severní	severní	k2eAgFnSc7d1	severní
Afrikou	Afrika	k1gFnSc7	Afrika
maloval	malovat	k5eAaImAgMnS	malovat
akvarely	akvarel	k1gInPc4	akvarel
a	a	k8xC	a
dělal	dělat	k5eAaImAgInS	dělat
si	se	k3xPyFc3	se
zápisky	zápisek	k1gInPc4	zápisek
<g/>
.	.	kIx.	.
</s>
<s>
Dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
proslulý	proslulý	k2eAgInSc1d1	proslulý
deník	deník	k1gInSc1	deník
se	s	k7c7	s
skicami	skica	k1gFnPc7	skica
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
použil	použít	k5eAaPmAgMnS	použít
při	při	k7c6	při
malování	malování	k1gNnSc6	malování
obrazů	obraz	k1gInPc2	obraz
znázorňujících	znázorňující	k2eAgInPc2d1	znázorňující
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
scény	scéna	k1gFnPc4	scéna
arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
akvarelisty	akvarelista	k1gMnPc7	akvarelista
byli	být	k5eAaImAgMnP	být
Gustave	Gustav	k1gMnSc5	Gustav
Moreau	Moreaum	k1gNnSc3	Moreaum
a	a	k8xC	a
Eugéne	Eugén	k1gInSc5	Eugén
Lamisse	Lamisse	k1gFnSc1	Lamisse
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
založil	založit	k5eAaPmAgMnS	založit
Société	Sociétý	k2eAgNnSc4d1	Société
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aquarellistes	Aquarellistes	k1gMnSc1	Aquarellistes
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
Paul	Paul	k1gMnSc1	Paul
Cézanne	Cézann	k1gInSc5	Cézann
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
akvarel	akvarel	k1gInSc4	akvarel
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
impresionistického	impresionistický	k2eAgInSc2d1	impresionistický
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
proslulé	proslulý	k2eAgNnSc4d1	proslulé
španělské	španělský	k2eAgNnSc4d1	španělské
akvarelisty	akvarelista	k1gMnSc2	akvarelista
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Pérez	Pérez	k1gMnSc1	Pérez
Villamil	Villamil	k1gMnSc1	Villamil
a	a	k8xC	a
Marino	Marina	k1gFnSc5	Marina
Fortuny	Fortuna	k1gFnSc2	Fortuna
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
prvního	první	k4xOgNnSc2	první
španělského	španělský	k2eAgNnSc2d1	španělské
sdružení	sdružení	k1gNnSc2	sdružení
akvarelistů	akvarelista	k1gMnPc2	akvarelista
Agrupació	Agrupació	k1gMnSc1	Agrupació
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Acuarellistes	Acuarellistes	k1gMnSc1	Acuarellistes
de	de	k?	de
Catalunya	Catalunya	k1gMnSc1	Catalunya
<g/>
.	.	kIx.	.
</s>
<s>
Fortuny	Fortuna	k1gFnPc4	Fortuna
byl	být	k5eAaImAgInS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
propagátorem	propagátor	k1gMnSc7	propagátor
akvarelu	akvarel	k1gInSc2	akvarel
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
akvarelových	akvarelový	k2eAgFnPc2d1	akvarelová
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Akvarelové	akvarelový	k2eAgFnPc1d1	akvarelová
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
<g/>
,	,	kIx,	,
nerostných	nerostný	k2eAgInPc2d1	nerostný
nebo	nebo	k8xC	nebo
živočišných	živočišný	k2eAgInPc2d1	živočišný
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgInPc2d1	spojený
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
arabskou	arabský	k2eAgFnSc7d1	arabská
gumou	guma	k1gFnSc7	guma
<g/>
,	,	kIx,	,
glycerínem	glycerín	k1gInSc7	glycerín
<g/>
,	,	kIx,	,
medem	med	k1gInSc7	med
a	a	k8xC	a
konzervačními	konzervační	k2eAgFnPc7d1	konzervační
přísadami	přísada	k1gFnPc7	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Glycerín	glycerín	k1gInSc1	glycerín
a	a	k8xC	a
med	med	k1gInSc1	med
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
popraskání	popraskání	k1gNnSc4	popraskání
nátěru	nátěr	k1gInSc2	nátěr
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
vysychání	vysychání	k1gNnSc2	vysychání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akvarelové	akvarelový	k2eAgFnPc1d1	akvarelová
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
čtyřech	čtyři	k4xCgNnPc6	čtyři
provedeních	provedení	k1gNnPc6	provedení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
suché	suchý	k2eAgFnPc1d1	suchá
barvy	barva	k1gFnPc1	barva
v	v	k7c6	v
přihrádkách	přihrádka	k1gFnPc6	přihrádka
</s>
</p>
<p>
<s>
mokré	mokrý	k2eAgFnPc4d1	mokrá
barvy	barva	k1gFnPc4	barva
v	v	k7c6	v
přihrádkách	přihrádka	k1gFnPc6	přihrádka
</s>
</p>
<p>
<s>
polotekuté	polotekutý	k2eAgFnPc1d1	polotekutá
barvy	barva	k1gFnPc1	barva
v	v	k7c6	v
tubičkách	tubička	k1gFnPc6	tubička
</s>
</p>
<p>
<s>
tekuté	tekutý	k2eAgFnPc1d1	tekutá
barvy	barva	k1gFnPc1	barva
v	v	k7c6	v
lahvičkách	lahvička	k1gFnPc6	lahvička
-	-	kIx~	-
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
lahvičky	lahvička	k1gFnSc2	lahvička
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
nalít	nalít	k5eAaPmF	nalít
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
nádoby	nádoba	k1gFnSc2	nádoba
a	a	k8xC	a
naředit	naředit	k5eAaPmF	naředit
je	být	k5eAaImIp3nS	být
vodou	voda	k1gFnSc7	voda
</s>
</p>
<p>
<s>
==	==	k?	==
Úpravy	úprava	k1gFnPc1	úprava
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Arabská	arabský	k2eAgFnSc1d1	arabská
guma	guma	k1gFnSc1	guma
===	===	k?	===
</s>
</p>
<p>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
guma	guma	k1gFnSc1	guma
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
přísad	přísada	k1gFnPc2	přísada
akvarelových	akvarelový	k2eAgFnPc2d1	akvarelová
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
přidání	přidání	k1gNnSc1	přidání
média	médium	k1gNnSc2	médium
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
průhlednost	průhlednost	k1gFnSc4	průhlednost
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
tohoto	tento	k3xDgNnSc2	tento
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
malba	malba	k1gFnSc1	malba
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
po	po	k7c6	po
zaschnutí	zaschnutí	k1gNnSc6	zaschnutí
nepříjemně	příjemně	k6eNd1	příjemně
leskla	lesknout	k5eAaImAgFnS	lesknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Býčí	býčí	k2eAgFnSc1d1	býčí
žluč	žluč	k1gFnSc1	žluč
===	===	k?	===
</s>
</p>
<p>
<s>
Býčí	býčí	k2eAgFnSc1d1	býčí
žluč	žluč	k1gFnSc1	žluč
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
klouzavosti	klouzavost	k1gFnSc2	klouzavost
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
kapek	kapka	k1gFnPc2	kapka
přidáme	přidat	k5eAaPmIp1nP	přidat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ředíme	ředit	k5eAaImIp1nP	ředit
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
vhodné	vhodný	k2eAgNnSc1d1	vhodné
používat	používat	k5eAaImF	používat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
tohoto	tento	k3xDgNnSc2	tento
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
barvy	barva	k1gFnSc2	barva
by	by	kYmCp3nP	by
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
mdlé	mdlý	k2eAgFnPc1d1	mdlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Irizující	irizující	k2eAgNnSc1d1	irizující
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Irizující	irizující	k2eAgNnSc1d1	irizující
médium	médium	k1gNnSc1	médium
dodá	dodat	k5eAaPmIp3nS	dodat
malbě	malba	k1gFnSc3	malba
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Strukturální	strukturální	k2eAgNnSc1d1	strukturální
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Strukturální	strukturální	k2eAgNnSc1d1	strukturální
médium	médium	k1gNnSc1	médium
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
výraznější	výrazný	k2eAgFnSc4d2	výraznější
texturu	textura	k1gFnSc4	textura
lazury	lazura	k1gFnSc2	lazura
<g/>
.	.	kIx.	.
</s>
<s>
Přidáváme	přidávat	k5eAaImIp1nP	přidávat
jej	on	k3xPp3gMnSc4	on
buď	buď	k8xC	buď
do	do	k7c2	do
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akvarelový	akvarelový	k2eAgInSc1d1	akvarelový
štětec	štětec	k1gInSc1	štětec
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
akvarel	akvarel	k1gInSc4	akvarel
technika	technikum	k1gNnSc2	technikum
využívající	využívající	k2eAgNnSc4d1	využívající
jako	jako	k9	jako
ředidlo	ředidlo	k1gNnSc4	ředidlo
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
štětec	štětec	k1gInSc4	štětec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hodně	hodně	k6eAd1	hodně
vody	voda	k1gFnSc2	voda
nasaje	nasát	k5eAaPmIp3nS	nasát
a	a	k8xC	a
udrží	udržet	k5eAaPmIp3nS	udržet
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc1d1	vhodná
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
měkké	měkký	k2eAgInPc1d1	měkký
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInPc1d1	přírodní
štětce	štětec	k1gInPc1	štětec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
chlupů	chlup	k1gInPc2	chlup
sobolů	sobol	k1gMnPc2	sobol
<g/>
,	,	kIx,	,
volů	vůl	k1gMnPc2	vůl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
veverek	veverka	k1gFnPc2	veverka
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
štětce	štětec	k1gInPc1	štětec
ze	z	k7c2	z
syntetického	syntetický	k2eAgNnSc2d1	syntetické
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neudrží	udržet	k5eNaPmIp3nS	udržet
tolik	tolik	k6eAd1	tolik
barvy	barva	k1gFnPc4	barva
jako	jako	k8xC	jako
štětce	štětec	k1gInPc4	štětec
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
vlasu	vlas	k1gInSc2	vlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvary	tvar	k1gInPc1	tvar
štětců	štětec	k1gInPc2	štětec
===	===	k?	===
</s>
</p>
<p>
<s>
Kulatý	kulatý	k2eAgInSc4d1	kulatý
(	(	kIx(	(
<g/>
Round	round	k1gInSc4	round
<g/>
)	)	kIx)	)
-	-	kIx~	-
špičatý	špičatý	k2eAgInSc1d1	špičatý
hrot	hrot	k1gInSc1	hrot
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
štětiny	štětina	k1gFnPc4	štětina
pro	pro	k7c4	pro
detaily	detail	k1gInPc4	detail
</s>
</p>
<p>
<s>
Plochý	plochý	k2eAgInSc4d1	plochý
(	(	kIx(	(
<g/>
Flat	Flat	k1gInSc4	Flat
<g/>
)	)	kIx)	)
-	-	kIx~	-
rychlé	rychlý	k2eAgNnSc1d1	rychlé
a	a	k8xC	a
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
roztírání	roztírání	k1gNnSc1	roztírání
barvy	barva	k1gFnSc2	barva
</s>
</p>
<p>
<s>
Plochý	plochý	k2eAgInSc1d1	plochý
krátký	krátký	k2eAgInSc1d1	krátký
(	(	kIx(	(
<g/>
Bright	Bright	k1gInSc1	Bright
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
tuhými	tuhý	k2eAgFnPc7d1	tuhá
štětinami	štětina	k1gFnPc7	štětina
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgInSc1d1	vhodný
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
tenčích	tenký	k2eAgFnPc2d2	tenčí
vrstev	vrstva	k1gFnPc2	vrstva
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
<g/>
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
silnější	silný	k2eAgFnPc4d2	silnější
vrstvy	vrstva	k1gFnPc4	vrstva
malby	malba	k1gFnSc2	malba
jako	jako	k8xS	jako
například	například	k6eAd1	například
impasto	impasto	k6eAd1	impasto
</s>
</p>
<p>
<s>
Kočičí	kočičí	k2eAgInSc1d1	kočičí
jazýček	jazýček	k1gInSc1	jazýček
(	(	kIx(	(
<g/>
Filbert	Filbert	k1gInSc1	Filbert
<g/>
)	)	kIx)	)
-	-	kIx~	-
plochý	plochý	k2eAgInSc1d1	plochý
štětec	štětec	k1gInSc1	štětec
s	s	k7c7	s
klenutým	klenutý	k2eAgInSc7d1	klenutý
koncem	konec	k1gInSc7	konec
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nejen	nejen	k6eAd1	nejen
dobré	dobrý	k2eAgNnSc1d1	dobré
pokrytí	pokrytí	k1gNnSc1	pokrytí
větší	veliký	k2eAgFnSc2d2	veliký
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
i	i	k9	i
detailní	detailní	k2eAgFnSc4d1	detailní
práci	práce	k1gFnSc4	práce
</s>
</p>
<p>
<s>
Vějíř	vějíř	k1gInSc1	vějíř
(	(	kIx(	(
<g/>
Fan	Fana	k1gFnPc2	Fana
<g/>
)	)	kIx)	)
-	-	kIx~	-
natírání	natírání	k1gNnSc1	natírání
větších	veliký	k2eAgFnPc2d2	veliký
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Zkosený	zkosený	k2eAgInSc1d1	zkosený
(	(	kIx(	(
<g/>
Angle	Angl	k1gMnSc5	Angl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plochý	plochý	k2eAgInSc1d1	plochý
štětec	štětec	k1gInSc1	štětec
se	s	k7c7	s
zkoseným	zkosený	k2eAgInSc7d1	zkosený
koncem	konec	k1gInSc7	konec
<g/>
;	;	kIx,	;
plochy	plocha	k1gFnPc1	plocha
i	i	k8xC	i
detaily	detail	k1gInPc1	detail
</s>
</p>
<p>
<s>
Mop	mop	k1gInSc1	mop
-	-	kIx~	-
velký	velký	k2eAgInSc1d1	velký
štětec	štětec	k1gInSc1	štětec
se	s	k7c7	s
zakulaceným	zakulacený	k2eAgInSc7d1	zakulacený
okrajem	okraj	k1gInSc7	okraj
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
jemnou	jemný	k2eAgFnSc4d1	jemná
aplikaci	aplikace	k1gFnSc4	aplikace
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
lazurování	lazurování	k1gNnSc4	lazurování
přes	přes	k7c4	přes
stávající	stávající	k2eAgFnPc4d1	stávající
vrstvy	vrstva	k1gFnPc4	vrstva
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
rizika	riziko	k1gNnSc2	riziko
poškození	poškození	k1gNnSc2	poškození
podkladu	podklad	k1gInSc2	podklad
</s>
</p>
<p>
<s>
Rigger	Rigger	k1gInSc1	Rigger
-	-	kIx~	-
kulatý	kulatý	k2eAgInSc1d1	kulatý
štětec	štětec	k1gInSc1	štětec
s	s	k7c7	s
delším	dlouhý	k2eAgInSc7d2	delší
vlasem	vlas	k1gInSc7	vlas
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
lanoví	lanoví	k1gNnSc2	lanoví
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
lodí	loď	k1gFnPc2	loď
<g/>
;	;	kIx,	;
štětec	štětec	k1gInSc1	štětec
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
jemné	jemný	k2eAgFnPc4d1	jemná
linie	linie	k1gFnPc4	linie
</s>
</p>
<p>
<s>
==	==	k?	==
Akvarelový	akvarelový	k2eAgInSc4d1	akvarelový
papír	papír	k1gInSc4	papír
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
akvarel	akvarel	k1gInSc4	akvarel
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
nesmírné	smírný	k2eNgNnSc4d1	nesmírné
množství	množství	k1gNnSc4	množství
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nákupem	nákup	k1gInSc7	nákup
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvážit	zvážit	k5eAaPmF	zvážit
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
chceme	chtít	k5eAaImIp1nP	chtít
malovat	malovat	k5eAaImF	malovat
<g/>
.	.	kIx.	.
</s>
<s>
Akvarelové	akvarelový	k2eAgInPc1d1	akvarelový
papíry	papír	k1gInPc1	papír
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
gramáží	gramáž	k1gFnSc7	gramáž
<g/>
,	,	kIx,	,
absorpcí	absorpce	k1gFnSc7	absorpce
a	a	k8xC	a
texturou	textura	k1gFnSc7	textura
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gramáž	gramáž	k1gFnSc4	gramáž
===	===	k?	===
</s>
</p>
<p>
<s>
Gramáž	gramáž	k1gFnSc1	gramáž
udává	udávat	k5eAaImIp3nS	udávat
hmotnost	hmotnost	k1gFnSc4	hmotnost
papíru	papír	k1gInSc2	papír
na	na	k7c4	na
1	[number]	k4	1
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
gramáž	gramáž	k1gFnSc4	gramáž
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
silnější	silný	k2eAgMnSc1d2	silnější
je	být	k5eAaImIp3nS	být
papír	papír	k1gInSc1	papír
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
akvarel	akvarel	k1gInSc4	akvarel
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
papíry	papír	k1gInPc1	papír
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
gramáží	gramáž	k1gFnSc7	gramáž
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nevlní	vlnit	k5eNaImIp3nS	vlnit
jako	jako	k9	jako
například	například	k6eAd1	například
pro	pro	k7c4	pro
akvarel	akvarel	k1gInSc4	akvarel
nevhodné	vhodný	k2eNgFnSc2d1	nevhodná
klasické	klasický	k2eAgFnSc2d1	klasická
čtvrtky	čtvrtka	k1gFnSc2	čtvrtka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Absorpce	absorpce	k1gFnSc2	absorpce
===	===	k?	===
</s>
</p>
<p>
<s>
Absorpce	absorpce	k1gFnSc1	absorpce
povrchu	povrch	k1gInSc2	povrch
akvarelových	akvarelový	k2eAgInPc2d1	akvarelový
papírů	papír	k1gInPc2	papír
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
absorpce	absorpce	k1gFnSc2	absorpce
se	se	k3xPyFc4	se
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
již	již	k6eAd1	již
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
druhem	druh	k1gInSc7	druh
použitého	použitý	k2eAgNnSc2d1	Použité
klížidla	klížidlo	k1gNnSc2	klížidlo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
množstvím	množství	k1gNnSc7	množství
<g/>
.	.	kIx.	.
</s>
<s>
Neklížený	klížený	k2eNgInSc1d1	klížený
papír	papír	k1gInSc1	papír
tekutinu	tekutina	k1gFnSc4	tekutina
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
silně	silně	k6eAd1	silně
klížený	klížený	k2eAgInSc4d1	klížený
papír	papír	k1gInSc4	papír
kapalinu	kapalina	k1gFnSc4	kapalina
téměř	téměř	k6eAd1	téměř
odpuzuje	odpuzovat	k5eAaImIp3nS	odpuzovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Textura	textura	k1gFnSc1	textura
===	===	k?	===
</s>
</p>
<p>
<s>
Textura	textura	k1gFnSc1	textura
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
chování	chování	k1gNnSc4	chování
akvarelu	akvarel	k1gInSc2	akvarel
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
horka	horko	k1gNnSc2	horko
lisované	lisovaný	k2eAgInPc1d1	lisovaný
papíry	papír	k1gInPc1	papír
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgInPc1d1	hladký
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
lisované	lisovaný	k2eAgFnPc4d1	lisovaná
již	již	k9	již
mají	mít	k5eAaImIp3nP	mít
určitou	určitý	k2eAgFnSc4d1	určitá
texturu	textura	k1gFnSc4	textura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lavírování	lavírování	k1gNnSc2	lavírování
==	==	k?	==
</s>
</p>
<p>
<s>
Technika	technika	k1gFnSc1	technika
lavírování	lavírování	k1gNnSc2	lavírování
představuje	představovat	k5eAaImIp3nS	představovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
nejvýše	vysoce	k6eAd3	vysoce
dvěma	dva	k4xCgFnPc7	dva
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
rozředěním	rozředění	k1gNnSc7	rozředění
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
valérů	valér	k1gInPc2	valér
<g/>
.	.	kIx.	.
</s>
<s>
Optického	optický	k2eAgInSc2d1	optický
vjemu	vjem	k1gInSc2	vjem
různorodé	různorodý	k2eAgFnSc2d1	různorodá
sytosti	sytost	k1gFnSc2	sytost
barvy	barva	k1gFnSc2	barva
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nS	docílit
neúplným	úplný	k2eNgNnSc7d1	neúplné
překrytím	překrytí	k1gNnSc7	překrytí
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
papíru	papír	k1gInSc2	papír
barevným	barevný	k2eAgInSc7d1	barevný
pigmentem	pigment	k1gInSc7	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
velcí	velký	k2eAgMnPc1d1	velký
mistři	mistr	k1gMnPc1	mistr
jako	jako	k8xC	jako
Rembrandt	Rembrandt	k1gInSc1	Rembrandt
nebo	nebo	k8xC	nebo
Constable	Constable	k1gFnSc1	Constable
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
neopomíjeli	opomíjet	k5eNaImAgMnP	opomíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termínem	termín	k1gInSc7	termín
lazura	lazura	k1gFnSc1	lazura
se	se	k3xPyFc4	se
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
označuje	označovat	k5eAaImIp3nS	označovat
vrstva	vrstva	k1gFnSc1	vrstva
řídké	řídký	k2eAgFnSc2d1	řídká
<g/>
,	,	kIx,	,
průsvitné	průsvitný	k2eAgFnSc2d1	průsvitná
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
nanesená	nanesený	k2eAgFnSc1d1	nanesená
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
(	(	kIx(	(
<g/>
papír	papír	k1gInSc4	papír
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
přítomnou	přítomný	k2eAgFnSc4d1	přítomná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Lazurováním	lazurování	k1gNnSc7	lazurování
se	se	k3xPyFc4	se
dociluje	docilovat	k5eAaImIp3nS	docilovat
nových	nový	k2eAgInPc2d1	nový
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
změny	změna	k1gFnPc1	změna
valérů	valér	k1gInPc2	valér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mokrý	mokrý	k2eAgInSc4d1	mokrý
akvarel	akvarel	k1gInSc4	akvarel
==	==	k?	==
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
technika	technika	k1gFnSc1	technika
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
nanášení	nanášení	k1gNnSc6	nanášení
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
navlhčený	navlhčený	k2eAgInSc4d1	navlhčený
podklad	podklad	k1gInSc4	podklad
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
nezavadlou	zavadlý	k2eNgFnSc4d1	zavadlý
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
rozpíjejí	rozpíjet	k5eAaImIp3nP	rozpíjet
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SMITH	SMITH	kA	SMITH
<g/>
,	,	kIx,	,
Ray	Ray	k1gFnSc1	Ray
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
technik	technika	k1gFnPc2	technika
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7391	[number]	k4	7391
<g/>
-	-	kIx~	-
<g/>
482	[number]	k4	482
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
128	[number]	k4	128
<g/>
-	-	kIx~	-
<g/>
155	[number]	k4	155
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERRY	BERRY	kA	BERRY
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Naučte	naučit	k5eAaPmRp2nP	naučit
se	se	k3xPyFc4	se
malovat	malovat	k5eAaImF	malovat
-	-	kIx~	-
Vodovky	vodovka	k1gFnPc1	vodovka
-	-	kIx~	-
Tempery	tempera	k1gFnPc1	tempera
-	-	kIx~	-
Akvarel	akvarel	k1gInSc1	akvarel
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Zoner	Zoner	k1gInSc1	Zoner
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
176	[number]	k4	176
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7413	[number]	k4	7413
<g/>
-	-	kIx~	-
<g/>
189	[number]	k4	189
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akvarel	akvarel	k1gInSc4	akvarel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
akvarel	akvarel	k1gInSc1	akvarel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
