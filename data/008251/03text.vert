<p>
<s>
HC	HC	kA	HC
Dobříš	Dobříš	k1gFnSc1	Dobříš
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
Dobříš	Dobříš	k1gFnSc4	Dobříš
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Dobříš	dobřit	k5eAaImIp2nS	dobřit
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
po	po	k7c6	po
odhlášení	odhlášení	k1gNnSc6	odhlášení
z	z	k7c2	z
krajské	krajský	k2eAgFnSc2d1	krajská
soutěže	soutěž	k1gFnSc2	soutěž
klubové	klubový	k2eAgNnSc1d1	klubové
vedení	vedení	k1gNnSc1	vedení
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
mládežnické	mládežnický	k2eAgInPc4d1	mládežnický
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgMnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
po	po	k7c6	po
fúzi	fúze	k1gFnSc6	fúze
se	s	k7c7	s
Slavojem	Slavoj	k1gInSc7	Slavoj
Obecnice	obecnice	k1gFnSc2	obecnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
místní	místní	k2eAgInSc1d1	místní
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
<g/>
.	.	kIx.	.
<g/>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrával	odehrávat	k5eAaImAgInS	odehrávat
na	na	k7c6	na
zimním	zimní	k2eAgInSc6d1	zimní
stadionu	stadion	k1gInSc6	stadion
Dobříš	dobřit	k5eAaImIp2nS	dobřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
ZČ	ZČ	kA	ZČ
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
–	–	k?	–
archiv	archiv	k1gInSc1	archiv
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ročníků	ročník	k1gInPc2	ročník
dostupné	dostupný	k2eAgNnSc1d1	dostupné
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
avlh	avlh	k1gInSc1	avlh
<g/>
.	.	kIx.	.
<g/>
sweb	sweb	k1gInSc1	sweb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
