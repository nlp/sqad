<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hormon	hormon	k1gInSc1	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
například	například	k6eAd1	například
pelichání	pelichání	k1gNnSc1	pelichání
<g/>
?	?	kIx.	?
</s>
