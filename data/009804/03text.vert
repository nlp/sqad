<p>
<s>
Ligr	Ligr	k1gInSc1	Ligr
(	(	kIx(	(
<g/>
též	též	k9	též
psáno	psán	k2eAgNnSc1d1	psáno
liger	liger	k1gInSc4	liger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kříženec	kříženec	k1gMnSc1	kříženec
mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
lva	lev	k1gMnSc2	lev
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
značných	značný	k2eAgInPc2d1	značný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ligři	ligr	k1gMnPc1	ligr
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
Samaře	Samara	k1gFnSc6	Samara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
cirkusy	cirkus	k1gInPc7	cirkus
nejčastějšími	častý	k2eAgNnPc7d3	nejčastější
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
mezidruhovému	mezidruhový	k2eAgNnSc3d1	mezidruhové
křížení	křížení	k1gNnSc3	křížení
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liger	Liger	k1gInSc1	Liger
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
známou	známý	k2eAgFnSc7d1	známá
kočkovitou	kočkovitý	k2eAgFnSc7d1	kočkovitá
šelmou	šelma	k1gFnSc7	šelma
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
přes	přes	k7c4	přes
400	[number]	k4	400
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
plavou	plavý	k2eAgFnSc4d1	plavá
barvu	barva	k1gFnSc4	barva
jako	jako	k8xS	jako
srst	srst	k1gFnSc4	srst
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pruhy	pruh	k1gInPc4	pruh
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
tygr	tygr	k1gMnSc1	tygr
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hřívu	hříva	k1gFnSc4	hříva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
mohutná	mohutný	k2eAgFnSc1d1	mohutná
než	než	k8xS	než
hříva	hříva	k1gFnSc1	hříva
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
tygra	tygr	k1gMnSc2	tygr
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
lva	lev	k1gMnSc2	lev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tigon	tigon	k1gInSc1	tigon
<g/>
.	.	kIx.	.
</s>
<s>
Tigon	Tigon	k1gInSc1	Tigon
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
lva	lev	k1gMnSc2	lev
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
ligera	ligero	k1gNnSc2	ligero
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
liliger	liliger	k1gInSc1	liliger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tigon	Tigon	k1gMnSc1	Tigon
</s>
</p>
<p>
<s>
Liliger	Liliger	k1gMnSc1	Liliger
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
</s>
</p>
<p>
<s>
Tygr	tygr	k1gMnSc1	tygr
</s>
</p>
<p>
<s>
Hybrid	hybrid	k1gInSc1	hybrid
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
