<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
nástroji	nástroj	k1gInPc7	nástroj
má	mít	k5eAaImIp3nS	mít
hoboj	hoboj	k1gFnSc4	hoboj
také	také	k9	také
pronikavější	pronikavý	k2eAgInSc4d2	pronikavější
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
sudé	sudý	k2eAgFnPc4d1	sudá
vyšší	vysoký	k2eAgFnPc4d2	vyšší
harmonické	harmonický	k2eAgFnPc4d1	harmonická
frekvence	frekvence	k1gFnPc4	frekvence
(	(	kIx(	(
<g/>
klarinet	klarinet	k1gInSc4	klarinet
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
silnější	silný	k2eAgFnSc1d2	silnější
liché	lichý	k2eAgFnPc4d1	lichá
vyšší	vysoký	k2eAgFnPc4d2	vyšší
harmonické	harmonický	k2eAgFnPc4d1	harmonická
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
jemnější	jemný	k2eAgNnSc1d2	jemnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
