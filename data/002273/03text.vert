<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
5	[number]	k4	5
410	[number]	k4	410
728	[number]	k4	728
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OBSE	OBSE	kA	OBSE
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
<g/>
,	,	kIx,	,
MMF	MMF	kA	MMF
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
od	od	k7c2	od
30	[number]	k4	30
<g />
.	.	kIx.	.
</s>
<s>
<g/>
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EU	EU	kA	EU
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EEA	EEA	kA	EEA
<g/>
,	,	kIx,	,
Eurozóny	Eurozón	k1gInPc1	Eurozón
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Slovanů	Slovan	k1gMnPc2	Slovan
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
především	především	k9	především
Keltové	Kelt	k1gMnPc1	Kelt
a	a	k8xC	a
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Sámovy	Sámův	k2eAgFnSc2d1	Sámova
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
poté	poté	k6eAd1	poté
hrály	hrát	k5eAaImAgFnP	hrát
oblasti	oblast	k1gFnPc1	oblast
západního	západní	k2eAgNnSc2d1	západní
Slovenska	Slovensko	k1gNnSc2	Slovensko
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
především	především	k6eAd1	především
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
krystalizačních	krystalizační	k2eAgNnPc2d1	krystalizační
center	centrum	k1gNnPc2	centrum
Velké	velká	k1gFnSc2	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
tohoto	tento	k3xDgInSc2	tento
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
uherské	uherský	k2eAgFnSc2d1	uherská
dynastie	dynastie	k1gFnSc2	dynastie
Arpádovců	Arpádovec	k1gMnPc2	Arpádovec
se	se	k3xPyFc4	se
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
rodů	rod	k1gInPc2	rod
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nezískali	získat	k5eNaPmAgMnP	získat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
slovenské	slovenský	k2eAgNnSc4d1	slovenské
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něhož	jenž	k3xRgMnSc2	jenž
Ľudovít	Ľudovít	k1gMnSc2	Ľudovít
Štúr	Štúr	k1gMnSc1	Štúr
kodifikoval	kodifikovat	k5eAaBmAgMnS	kodifikovat
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
slovenštinu	slovenština	k1gFnSc4	slovenština
jako	jako	k8xC	jako
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
trvaly	trvat	k5eAaImAgFnP	trvat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
povznesení	povznesení	k1gNnSc2	povznesení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
potírány	potírat	k5eAaImNgFnP	potírat
maďarizační	maďarizační	k2eAgFnSc7d1	maďarizační
politikou	politika	k1gFnSc7	politika
uherské	uherský	k2eAgFnSc2d1	uherská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něhož	jenž	k3xRgNnSc2	jenž
získali	získat	k5eAaPmAgMnP	získat
Slováci	Slovák	k1gMnPc1	Slovák
statut	statuta	k1gNnPc2	statuta
státotvorného	státotvorný	k2eAgInSc2d1	státotvorný
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
satelitem	satelit	k1gInSc7	satelit
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
během	během	k7c2	během
listopadové	listopadový	k2eAgFnSc2d1	listopadová
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Neshody	neshoda	k1gFnPc1	neshoda
mezi	mezi	k7c7	mezi
českými	český	k2eAgFnPc7d1	Česká
a	a	k8xC	a
slovenskými	slovenský	k2eAgFnPc7d1	slovenská
politickými	politický	k2eAgFnPc7d1	politická
elitami	elita	k1gFnPc7	elita
vyústily	vyústit	k5eAaPmAgFnP	vyústit
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
v	v	k7c6	v
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
důsledkem	důsledek	k1gInSc7	důsledek
čehož	což	k3yRnSc2	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
jakožto	jakožto	k8xS	jakožto
suverénní	suverénní	k2eAgInSc1d1	suverénní
slovenský	slovenský	k2eAgInSc1d1	slovenský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
Slovensko	Slovensko	k1gNnSc4	Slovensko
součástí	součást	k1gFnPc2	součást
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zavedena	zaveden	k2eAgFnSc1d1	zavedena
jednotná	jednotný	k2eAgFnSc1d1	jednotná
evropská	evropský	k2eAgFnSc1d1	Evropská
měna	měna	k1gFnSc1	měna
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
s	s	k7c7	s
demokratickými	demokratický	k2eAgFnPc7d1	demokratická
institucemi	instituce	k1gFnPc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
převážně	převážně	k6eAd1	převážně
soběstačnou	soběstačný	k2eAgFnSc7d1	soběstačná
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
produkcí	produkce	k1gFnSc7	produkce
<g/>
,	,	kIx,	,
modernizovaným	modernizovaný	k2eAgInSc7d1	modernizovaný
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
rozvíjejícím	rozvíjející	k2eAgMnSc7d1	rozvíjející
se	se	k3xPyFc4	se
sektorem	sektor	k1gInSc7	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převažuje	převažovat	k5eAaImIp3nS	převažovat
jak	jak	k6eAd1	jak
v	v	k7c6	v
podílu	podíl	k1gInSc6	podíl
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pracovní	pracovní	k2eAgFnPc4d1	pracovní
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
ekonomika	ekonomika	k1gFnSc1	ekonomika
Slovenska	Slovensko	k1gNnSc2	Slovensko
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgNnPc2d1	rostoucí
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
geografickému	geografický	k2eAgInSc3d1	geografický
profilu	profil	k1gInSc3	profil
země	zem	k1gFnSc2	zem
řidčeji	řídce	k6eAd2	řídce
rozprostřena	rozprostřen	k2eAgFnSc1d1	rozprostřena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozšíření	rozšíření	k1gNnSc3	rozšíření
a	a	k8xC	a
modernizaci	modernizace	k1gFnSc3	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výroby	výroba	k1gFnSc2	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
převažují	převažovat	k5eAaImIp3nP	převažovat
jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
tepelnými	tepelný	k2eAgFnPc7d1	tepelná
a	a	k8xC	a
vodními	vodní	k2eAgFnPc7d1	vodní
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
do	do	k7c2	do
struktur	struktura	k1gFnPc2	struktura
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
misích	mise	k1gFnPc6	mise
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
profesionalizovaná	profesionalizovaný	k2eAgFnSc1d1	profesionalizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
Slovensko	Slovensko	k1gNnSc4	Slovensko
pevné	pevný	k2eAgInPc4d1	pevný
svazky	svazek	k1gInPc4	svazek
především	především	k9	především
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
nelepší	lepšit	k5eNaImIp3nP	lepšit
konfliktní	konfliktní	k2eAgInPc1d1	konfliktní
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
silná	silný	k2eAgFnSc1d1	silná
maďarská	maďarský	k2eAgFnSc1d1	maďarská
a	a	k8xC	a
romská	romský	k2eAgFnSc1d1	romská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kulturní	kulturní	k2eAgFnSc4d1	kulturní
i	i	k8xC	i
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
tradici	tradice	k1gFnSc4	tradice
jako	jako	k8xS	jako
i	i	k9	i
množství	množství	k1gNnSc1	množství
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
názvu	název	k1gInSc2	název
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
etnonyma	etnonymum	k1gNnPc4	etnonymum
Slováci	Slovák	k1gMnPc1	Slovák
je	on	k3xPp3gFnPc4	on
pevně	pevně	k6eAd1	pevně
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkové	předek	k1gMnPc1	předek
Slováků	Slovák	k1gMnPc2	Slovák
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
vymezeném	vymezený	k2eAgNnSc6d1	vymezené
území	území	k1gNnSc6	území
odděleném	oddělený	k2eAgNnSc6d1	oddělené
státní	státní	k2eAgNnSc4d1	státní
hranicí	hranice	k1gFnSc7	hranice
od	od	k7c2	od
jim	on	k3xPp3gMnPc3	on
jazykově	jazykově	k6eAd1	jazykově
blízkých	blízký	k2eAgMnPc2d1	blízký
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Moravanů	Moravan	k1gMnPc2	Moravan
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
německého	německý	k2eAgNnSc2d1	německé
a	a	k8xC	a
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
odlišovaly	odlišovat	k5eAaImAgInP	odlišovat
jiný	jiný	k2eAgInSc4d1	jiný
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
zárodků	zárodek	k1gInPc2	zárodek
slovenského	slovenský	k2eAgNnSc2d1	slovenské
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
pramenech	pramen	k1gInPc6	pramen
většinou	většinou	k6eAd1	většinou
byli	být	k5eAaImAgMnP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
nazýváni	nazývat	k5eAaImNgMnP	nazývat
Slavus	Slavus	k1gMnSc1	Slavus
či	či	k8xC	či
Sclavus	Sclavus	k1gMnSc1	Sclavus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
odlišeni	odlišen	k2eAgMnPc1d1	odlišen
od	od	k7c2	od
Chorvatů	Chorvat	k1gMnPc2	Chorvat
<g/>
,	,	kIx,	,
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
slovenský	slovenský	k2eAgInSc1d1	slovenský
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užíval	užívat	k5eAaImAgInS	užívat
například	například	k6eAd1	například
v	v	k7c6	v
názvech	název	k1gInPc6	název
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
u	u	k7c2	u
toponym	toponymum	k1gNnPc2	toponymum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
spolu	spolu	k6eAd1	spolu
stýkaly	stýkat	k5eAaImAgFnP	stýkat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
neslovanské	slovanský	k2eNgInPc4d1	neslovanský
národy	národ	k1gInPc4	národ
Uher	Uher	k1gMnSc1	Uher
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
populací	populace	k1gFnSc7	populace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
osídlení	osídlení	k1gNnPc1	osídlení
předchůdci	předchůdce	k1gMnPc7	předchůdce
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
doloženo	doložen	k2eAgNnSc4d1	doloženo
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
známé	známý	k2eAgNnSc4d1	známé
etnikum	etnikum	k1gNnSc4	etnikum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
osídlilo	osídlit	k5eAaPmAgNnS	osídlit
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
pronikli	proniknout	k5eAaPmAgMnP	proniknout
na	na	k7c4	na
slovenské	slovenský	k2eAgNnSc4d1	slovenské
území	území	k1gNnSc4	území
Germáni	Germán	k1gMnPc1	Germán
soupeřící	soupeřící	k2eAgMnPc1d1	soupeřící
s	s	k7c7	s
Římskou	římska	k1gFnSc7	římska
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
hranici	hranice	k1gFnSc4	hranice
zde	zde	k6eAd1	zde
tehdy	tehdy	k6eAd1	tehdy
tvořila	tvořit	k5eAaImAgFnS	tvořit
řeka	řeka	k1gFnSc1	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
"	"	kIx"	"
<g/>
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
<g/>
"	"	kIx"	"
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
bylo	být	k5eAaImAgNnS	být
Vanniovo	Vanniův	k2eAgNnSc1d1	Vanniovo
království	království	k1gNnSc1	království
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
osídlovali	osídlovat	k5eAaImAgMnP	osídlovat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
568	[number]	k4	568
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
nomádských	nomádský	k2eAgMnPc2d1	nomádský
Avarů	Avar	k1gMnPc2	Avar
a	a	k8xC	a
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Sámovy	Sámův	k2eAgFnSc2d1	Sámova
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
791	[number]	k4	791
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgInSc1d1	veliký
Avary	Avar	k1gMnPc4	Avar
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
Slované	Slovan	k1gMnPc1	Slovan
s	s	k7c7	s
již	již	k6eAd1	již
rozpadlými	rozpadlý	k2eAgInPc7d1	rozpadlý
kmenovými	kmenový	k2eAgInPc7d1	kmenový
vztahy	vztah	k1gInPc7	vztah
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Moravské	moravský	k2eAgNnSc4d1	Moravské
a	a	k8xC	a
Nitranské	nitranský	k2eAgNnSc4d1	Nitranské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
833	[number]	k4	833
následovala	následovat	k5eAaImAgFnS	následovat
integrace	integrace	k1gFnPc4	integrace
obou	dva	k4xCgInPc2	dva
knížectví	knížectví	k1gNnPc2	knížectví
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
v	v	k7c6	v
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
maďarských	maďarský	k2eAgInPc2d1	maďarský
kmenů	kmen	k1gInPc2	kmen
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
nově	nově	k6eAd1	nově
vytvořenému	vytvořený	k2eAgInSc3d1	vytvořený
uherskému	uherský	k2eAgInSc3d1	uherský
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
Horní	horní	k2eAgFnSc1d1	horní
Uhry	Uher	k1gMnPc7	Uher
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
součástí	součást	k1gFnPc2	součást
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
habsburského	habsburský	k2eAgNnSc2d1	habsburské
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
expanzi	expanze	k1gFnSc6	expanze
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uherské	uherský	k2eAgNnSc1d1	Uherské
území	území	k1gNnSc1	území
země	zem	k1gFnSc2	zem
dočasně	dočasně	k6eAd1	dočasně
zredukovalo	zredukovat	k5eAaPmAgNnS	zredukovat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
na	na	k7c4	na
hornatou	hornatý	k2eAgFnSc4d1	hornatá
část	část	k1gFnSc4	část
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc4d1	dnešní
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
spolkovou	spolkový	k2eAgFnSc4d1	spolková
zemi	zem	k1gFnSc4	zem
Hradsko	Hradsko	k1gNnSc1	Hradsko
(	(	kIx(	(
<g/>
Burgenland	Burgenland	k1gInSc1	Burgenland
<g/>
)	)	kIx)	)
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
stalo	stát	k5eAaPmAgNnS	stát
jádrem	jádro	k1gNnSc7	jádro
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
Královských	královský	k2eAgFnPc2d1	královská
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
(	(	kIx(	(
<g/>
1536	[number]	k4	1536
<g/>
–	–	k?	–
<g/>
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
i	i	k9	i
korunovačním	korunovační	k2eAgNnSc7d1	korunovační
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
1563	[number]	k4	1563
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
sněmu	sněm	k1gInSc2	sněm
Královského	královský	k2eAgNnSc2d1	královské
Uherska	Uhersko	k1gNnSc2	Uhersko
(	(	kIx(	(
<g/>
1542	[number]	k4	1542
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trnava	Trnava	k1gFnSc1	Trnava
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
ostřihomského	ostřihomský	k2eAgMnSc2d1	ostřihomský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
centrálním	centrální	k2eAgNnSc7d1	centrální
městem	město	k1gNnSc7	město
uherské	uherský	k2eAgFnSc2d1	uherská
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
moderního	moderní	k2eAgInSc2d1	moderní
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
národní	národní	k2eAgFnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
až	až	k9	až
1849	[number]	k4	1849
podporovala	podporovat	k5eAaImAgFnS	podporovat
část	část	k1gFnSc1	část
Slováků	Slovák	k1gMnPc2	Slovák
centrální	centrální	k2eAgFnSc4d1	centrální
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
proti	proti	k7c3	proti
uherským	uherský	k2eAgMnPc3d1	uherský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc4	jejich
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
odtržení	odtržení	k1gNnSc4	odtržení
Slovenska	Slovensko	k1gNnSc2	Slovensko
od	od	k7c2	od
Uher	Uhry	k1gFnPc2	Uhry
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
uherského	uherský	k2eAgNnSc2d1	Uherské
povstání	povstání	k1gNnSc2	povstání
však	však	k9	však
naopak	naopak	k6eAd1	naopak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
centralizaci	centralizace	k1gFnSc3	centralizace
státu	stát	k1gInSc2	stát
a	a	k8xC	a
požadavky	požadavek	k1gInPc4	požadavek
Slováků	Slovák	k1gMnPc2	Slovák
na	na	k7c4	na
federalizaci	federalizace	k1gFnSc4	federalizace
monarchie	monarchie	k1gFnSc2	monarchie
či	či	k8xC	či
národní	národní	k2eAgFnSc4d1	národní
autonomii	autonomie	k1gFnSc4	autonomie
nebyly	být	k5eNaImAgFnP	být
vyslyšeny	vyslyšen	k2eAgFnPc1d1	vyslyšena
<g/>
.	.	kIx.	.
</s>
<s>
Nejhoršímu	zlý	k2eAgInSc3d3	Nejhorší
tlaku	tlak	k1gInSc3	tlak
maďarizace	maďarizace	k1gFnSc2	maďarizace
čelili	čelit	k5eAaImAgMnP	čelit
Slováci	Slovák	k1gMnPc1	Slovák
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
rakousko-uherského	rakouskoherský	k2eAgInSc2d1	rakousko-uherský
vyrovnaní	vyrovnaný	k2eAgMnPc1d1	vyrovnaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
pak	pak	k6eAd1	pak
uherská	uherský	k2eAgFnSc1d1	uherská
vláda	vláda	k1gFnSc1	vláda
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
i	i	k9	i
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	s	k7c7	s
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
dnešním	dnešní	k2eAgNnSc7d1	dnešní
vytyčením	vytyčení	k1gNnSc7	vytyčení
hranic	hranice	k1gFnPc2	hranice
stalo	stát	k5eAaPmAgNnS	stát
jako	jako	k8xC	jako
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
země	země	k1gFnSc1	země
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
součástí	součást	k1gFnPc2	součást
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
vymanilo	vymanit	k5eAaPmAgNnS	vymanit
se	se	k3xPyFc4	se
tak	tak	k9	tak
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vnitřním	vnitřní	k2eAgInPc3d1	vnitřní
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
agresi	agrese	k1gFnSc6	agrese
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
Československo	Československo	k1gNnSc1	Československo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
získalo	získat	k5eAaPmAgNnS	získat
Slovensko	Slovensko	k1gNnSc1	Slovensko
formální	formální	k2eAgFnSc4d1	formální
samostatnost	samostatnost	k1gFnSc4	samostatnost
jako	jako	k8xS	jako
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
neoficiálně	neoficiálně	k6eAd1	neoficiálně
Slovenský	slovenský	k2eAgInSc1d1	slovenský
štát	štát	k1gInSc1	štát
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
satelitem	satelit	k1gInSc7	satelit
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
proti	proti	k7c3	proti
fašistickému	fašistický	k2eAgInSc3d1	fašistický
režimu	režim	k1gInSc3	režim
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(	(
<g/>
SNP	SNP	kA	SNP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
porazily	porazit	k5eAaPmAgFnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následných	následný	k2eAgFnPc6d1	následná
represích	represe	k1gFnPc6	represe
vůči	vůči	k7c3	vůči
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
jednotek	jednotka	k1gFnPc2	jednotka
SS	SS	kA	SS
a	a	k8xC	a
Heimatschutzu	Heimatschutz	k1gInSc2	Heimatschutz
podílely	podílet	k5eAaImAgInP	podílet
i	i	k9	i
Pohotovostní	pohotovostní	k2eAgInPc1d1	pohotovostní
oddíly	oddíl	k1gInPc1	oddíl
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slovenské	slovenský	k2eAgNnSc4d1	slovenské
území	území	k1gNnSc4	území
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
socialistický	socialistický	k2eAgInSc4d1	socialistický
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spadal	spadat	k5eAaImAgInS	spadat
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
přeměnu	přeměna	k1gFnSc4	přeměna
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
asymetrického	asymetrický	k2eAgInSc2d1	asymetrický
modelu	model	k1gInSc2	model
institucí	instituce	k1gFnPc2	instituce
například	například	k6eAd1	například
samostatného	samostatný	k2eAgInSc2d1	samostatný
slovenského	slovenský	k2eAgInSc2d1	slovenský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
otázky	otázka	k1gFnPc1	otázka
řešené	řešený	k2eAgFnPc1d1	řešená
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
poloviny	polovina	k1gFnSc2	polovina
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
zahájen	zahájit	k5eAaPmNgInS	zahájit
masivní	masivní	k2eAgInSc1d1	masivní
plán	plán	k1gInSc1	plán
industrializace	industrializace	k1gFnSc2	industrializace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
ustavena	ustaven	k2eAgFnSc1d1	ustavena
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
republikou	republika	k1gFnSc7	republika
(	(	kIx(	(
<g/>
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
tvořila	tvořit	k5eAaImAgFnS	tvořit
Československou	československý	k2eAgFnSc4d1	Československá
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
demokratizaci	demokratizace	k1gFnSc3	demokratizace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
ozývat	ozývat	k5eAaImF	ozývat
hlasy	hlas	k1gInPc1	hlas
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
ba	ba	k9	ba
i	i	k9	i
svrchovanosti	svrchovanost	k1gFnPc1	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
"	"	kIx"	"
<g/>
O	o	k7c6	o
názvu	název	k1gInSc6	název
<g/>
,	,	kIx,	,
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
pečeti	pečeť	k1gFnSc6	pečeť
a	a	k8xC	a
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
hymně	hymna	k1gFnSc6	hymna
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témž	týž	k3xTgNnSc6	týž
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
i	i	k9	i
název	název	k1gInSc1	název
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
pak	pak	k6eAd1	pak
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
stávající	stávající	k2eAgFnSc1d1	stávající
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
i	i	k8xC	i
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
samostatnými	samostatný	k2eAgInPc7d1	samostatný
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	s	k7c7	s
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
také	také	k9	také
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
jeho	jeho	k3xOp3gNnPc2	jeho
území	území	k1gNnPc2	území
čítá	čítat	k5eAaImIp3nS	čítat
49	[number]	k4	49
036	[number]	k4	036
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgFnPc3d1	severní
a	a	k8xC	a
středním	střední	k2eAgFnPc3d1	střední
oblastem	oblast	k1gFnPc3	oblast
Slovenska	Slovensko	k1gNnSc2	Slovensko
dominují	dominovat	k5eAaImIp3nP	dominovat
na	na	k7c4	na
především	především	k6eAd1	především
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
horské	horský	k2eAgInPc4d1	horský
masivy	masiv	k1gInPc4	masiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
–	–	k?	–
Vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Malé	Malé	k2eAgInPc1d1	Malé
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
Javorníky	Javorník	k1gInPc1	Javorník
a	a	k8xC	a
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInPc1d1	střední
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
a	a	k8xC	a
Nízkých	nízký	k2eAgFnPc2d1	nízká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
Karpaty	Karpaty	k1gInPc1	Karpaty
postupují	postupovat	k5eAaImIp3nP	postupovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
především	především	k9	především
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
rudohoří	rudohoří	k1gNnSc1	rudohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
oblastech	oblast	k1gFnPc6	oblast
převládají	převládat	k5eAaImIp3nP	převládat
spíše	spíše	k9	spíše
kopce	kopec	k1gInPc4	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
položené	položený	k2eAgFnPc1d1	položená
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
představují	představovat	k5eAaImIp3nP	představovat
Podunajská	podunajský	k2eAgFnSc1d1	Podunajská
a	a	k8xC	a
Východoslovenská	východoslovenský	k2eAgFnSc1d1	Východoslovenská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
2	[number]	k4	2
655	[number]	k4	655
m.	m.	k?	m.
Přes	přes	k7c4	přes
31,9	[number]	k4	31,9
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávajícími	přetrvávající	k2eAgInPc7d1	přetrvávající
ekologickými	ekologický	k2eAgInPc7d1	ekologický
problémy	problém	k1gInPc7	problém
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
ovzduší	ovzduší	k1gNnSc4	ovzduší
a	a	k8xC	a
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
lesní	lesní	k2eAgInSc1d1	lesní
porost	porost	k1gInSc1	porost
vážně	vážně	k6eAd1	vážně
narušen	narušen	k2eAgInSc1d1	narušen
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
kácením	kácení	k1gNnSc7	kácení
a	a	k8xC	a
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
pětiny	pětina	k1gFnPc4	pětina
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgFnPc4d1	chráněná
krajinné	krajinný	k2eAgFnPc4d1	krajinná
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
spravuje	spravovat	k5eAaImIp3nS	spravovat
Státní	státní	k2eAgFnSc1d1	státní
ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
chráněné	chráněný	k2eAgFnPc1d1	chráněná
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
ustavovány	ustavovat	k5eAaImNgFnP	ustavovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nachází	nacházet	k5eAaImIp3nS	nacházet
9	[number]	k4	9
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
a	a	k8xC	a
14	[number]	k4	14
chráněných	chráněný	k2eAgFnPc2d1	chráněná
krajinných	krajinný	k2eAgFnPc2d1	krajinná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Vysoké	vysoká	k1gFnPc4	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
větrná	větrný	k2eAgFnSc1d1	větrná
smršť	smršť	k1gFnSc1	smršť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
silně	silně	k6eAd1	silně
poškodila	poškodit	k5eAaPmAgFnS	poškodit
zdejší	zdejší	k2eAgInSc4d1	zdejší
lesní	lesní	k2eAgInSc4d1	lesní
porost	porost	k1gInSc4	porost
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
škody	škoda	k1gFnPc4	škoda
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
několika	několik	k4yIc2	několik
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
řek	řeka	k1gFnPc2	řeka
protékajících	protékající	k2eAgFnPc2d1	protékající
Slovenskem	Slovensko	k1gNnSc7	Slovensko
představuje	představovat	k5eAaImIp3nS	představovat
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Moravou	Morava	k1gFnSc7	Morava
tvoří	tvořit	k5eAaImIp3nP	tvořit
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
slovenským	slovenský	k2eAgInSc7d1	slovenský
vodním	vodní	k2eAgInSc7d1	vodní
tokem	tok	k1gInSc7	tok
je	být	k5eAaImIp3nS	být
Váh	Váh	k1gInSc1	Váh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
řeky	řeka	k1gFnPc4	řeka
patří	patřit	k5eAaImIp3nS	patřit
Hron	Hron	k1gInSc1	Hron
<g/>
,	,	kIx,	,
Hornád	Hornáda	k1gFnPc2	Hornáda
<g/>
,	,	kIx,	,
Bodrog	Bodroga	k1gFnPc2	Bodroga
a	a	k8xC	a
Poprad	Poprad	k1gInSc1	Poprad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jsou	být	k5eAaImIp3nP	být
častá	častý	k2eAgNnPc1d1	časté
rovněž	rovněž	k6eAd1	rovněž
horská	horský	k2eAgNnPc1d1	horské
jezera	jezero	k1gNnPc1	jezero
a	a	k8xC	a
minerální	minerální	k2eAgInPc1d1	minerální
i	i	k8xC	i
termální	termální	k2eAgInPc1d1	termální
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
černomořského	černomořský	k2eAgNnSc2d1	černomořské
úmoří	úmoří	k1gNnSc2	úmoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
rozličné	rozličný	k2eAgInPc1d1	rozličný
druhy	druh	k1gInPc1	druh
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
na	na	k7c4	na
29	[number]	k4	29
tisíc	tisíc	k4xCgInPc2	tisíc
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
druhů	druh	k1gInPc2	druh
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
běžných	běžný	k2eAgNnPc2d1	běžné
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
rysi	rys	k1gMnPc1	rys
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc1d1	divoká
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
svišti	svišť	k1gMnPc1	svišť
<g/>
,	,	kIx,	,
vydry	vydra	k1gFnPc1	vydra
<g/>
,	,	kIx,	,
kuny	kuna	k1gFnPc1	kuna
<g/>
,	,	kIx,	,
norci	norec	k1gMnPc1	norec
a	a	k8xC	a
kamzíci	kamzík	k1gMnPc1	kamzík
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
celostátně	celostátně	k6eAd1	celostátně
chránění	chráněný	k2eAgMnPc1d1	chráněný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
koroptve	koroptev	k1gFnPc1	koroptev
<g/>
,	,	kIx,	,
bažanti	bažant	k1gMnPc1	bažant
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc4d1	divoká
husy	husa	k1gFnPc4	husa
a	a	k8xC	a
kachny	kachna	k1gFnPc4	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
rostlinstva	rostlinstvo	k1gNnSc2	rostlinstvo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
převládají	převládat	k5eAaImIp3nP	převládat
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
duby	dub	k1gInPc4	dub
<g/>
,	,	kIx,	,
na	na	k7c6	na
úpatích	úpatí	k1gNnPc6	úpatí
hor	hora	k1gFnPc2	hora
buky	buk	k1gInPc1	buk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
smrky	smrk	k1gInPc4	smrk
a	a	k8xC	a
na	na	k7c6	na
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
místech	místo	k1gNnPc6	místo
převažuje	převažovat	k5eAaImIp3nS	převažovat
co	co	k9	co
do	do	k7c2	do
biomů	biom	k1gInPc2	biom
tajga	tajga	k1gFnSc1	tajga
a	a	k8xC	a
tundra	tundra	k1gFnSc1	tundra
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
převládají	převládat	k5eAaImIp3nP	převládat
studenější	studený	k2eAgNnPc1d2	studenější
léta	léto	k1gNnPc1	léto
a	a	k8xC	a
chladné	chladný	k2eAgFnPc1d1	chladná
vlhké	vlhký	k2eAgFnPc1d1	vlhká
zimy	zima	k1gFnPc1	zima
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
-4	-4	k4	-4
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
blíží	blížit	k5eAaImIp3nP	blížit
či	či	k8xC	či
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
hodnotu	hodnota	k1gFnSc4	hodnota
i	i	k9	i
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
při	při	k7c6	při
Dunaji	Dunaj	k1gInSc6	Dunaj
průměrné	průměrný	k2eAgFnPc1d1	průměrná
srážky	srážka	k1gFnPc1	srážka
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
570	[number]	k4	570
mm	mm	kA	mm
<g/>
,	,	kIx,	,
při	při	k7c6	při
návětrných	návětrný	k2eAgFnPc6d1	návětrná
stranách	strana	k1gFnPc6	strana
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
šplhají	šplhat	k5eAaImIp3nP	šplhat
až	až	k9	až
na	na	k7c4	na
1100	[number]	k4	1100
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor	hora	k1gFnPc2	hora
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
sníh	sníh	k1gInSc1	sníh
až	až	k9	až
do	do	k7c2	do
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
občanů	občan	k1gMnPc2	občan
na	na	k7c6	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
zvaný	zvaný	k2eAgInSc1d1	zvaný
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
zasedá	zasedat	k5eAaImIp3nS	zasedat
150	[number]	k4	150
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgFnSc2	každý
4	[number]	k4	4
roky	rok	k1gInPc4	rok
a	a	k8xC	a
volit	volit	k5eAaImF	volit
může	moct	k5eAaImIp3nS	moct
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
starší	starší	k1gMnSc1	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
slovenským	slovenský	k2eAgMnSc7d1	slovenský
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
vede	vést	k5eAaImIp3nS	vést
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
SMER	SMER	kA	SMER
-	-	kIx~	-
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracium	k1gNnSc2	demokracium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
justice	justice	k1gFnSc2	justice
zastává	zastávat	k5eAaImIp3nS	zastávat
významnou	významný	k2eAgFnSc4d1	významná
pozici	pozice	k1gFnSc4	pozice
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc4d1	tvořený
13	[number]	k4	13
soudci	soudce	k1gMnPc7	soudce
jmenovanými	jmenovaný	k2eAgMnPc7d1	jmenovaný
prezidentem	prezident	k1gMnSc7	prezident
na	na	k7c4	na
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
funguje	fungovat	k5eAaImIp3nS	fungovat
systém	systém	k1gInSc1	systém
krajských	krajský	k2eAgInPc2d1	krajský
<g/>
,	,	kIx,	,
okresních	okresní	k2eAgInPc2d1	okresní
a	a	k8xC	a
vojenských	vojenský	k2eAgInPc2d1	vojenský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
doplněných	doplněný	k2eAgInPc2d1	doplněný
Specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
trestním	trestní	k2eAgInSc7d1	trestní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
organizací	organizace	k1gFnPc2	organizace
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
a	a	k8xC	a
Nadace	nadace	k1gFnSc1	nadace
Bertelsmann	Bertelsmanna	k1gFnPc2	Bertelsmanna
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
konají	konat	k5eAaImIp3nP	konat
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
oddělení	oddělení	k1gNnSc1	oddělení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
státních	státní	k2eAgFnPc2d1	státní
mocí	moc	k1gFnPc2	moc
(	(	kIx(	(
<g/>
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc2d1	soudní
a	a	k8xC	a
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnPc1d1	poslední
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
SMER	SMER	kA	SMER
–	–	k?	–
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracia	k1gFnSc1	demokracia
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
volbám	volba	k1gFnPc3	volba
propadla	propadlo	k1gNnSc2	propadlo
a	a	k8xC	a
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
udržet	udržet	k5eAaPmF	udržet
původní	původní	k2eAgFnSc4d1	původní
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc6	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
28	[number]	k4	28
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
stana	staen	k2eAgFnSc1d1	stana
Sloboda	sloboda	k1gFnSc1	sloboda
a	a	k8xC	a
Solidarita	solidarita	k1gFnSc1	solidarita
s	s	k7c7	s
12	[number]	k4	12
%	%	kIx~	%
a	a	k8xC	a
na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
OBYČAJNÍ	OBYČAJNÍ	kA	OBYČAJNÍ
ĽUDIA	ĽUDIA	kA	ĽUDIA
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
osobnosti	osobnost	k1gFnPc1	osobnost
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
zastoupené	zastoupený	k2eAgFnPc4d1	zastoupená
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
patří	patřit	k5eAaImIp3nS	patřit
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národná	národný	k2eAgFnSc1d1	národná
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
8,6	[number]	k4	8,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kotleba	Kotleba	k1gFnSc1	Kotleba
–	–	k?	–
Ľudová	Ľudový	k2eAgFnSc1d1	Ľudová
strana	strana	k1gFnSc1	strana
Naše	náš	k3xOp1gNnSc4	náš
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SME	SME	k?	SME
RODINA	rodina	k1gFnSc1	rodina
-	-	kIx~	-
Boris	Boris	k1gMnSc1	Boris
Kollár	Kollár	k1gMnSc1	Kollár
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MOST-HÍD	MOST-HÍD	k1gMnSc1	MOST-HÍD
(	(	kIx(	(
<g/>
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
SIEŤ	SIEŤ	kA	SIEŤ
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
Bertelsmann	Bertelsmanna	k1gFnPc2	Bertelsmanna
a	a	k8xC	a
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc4	house
konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
respektována	respektován	k2eAgNnPc4d1	respektováno
náboženská	náboženský	k2eAgNnPc4d1	náboženské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
shromažďování	shromažďování	k1gNnSc4	shromažďování
i	i	k8xC	i
organizaci	organizace	k1gFnSc4	organizace
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
ale	ale	k9	ale
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
společnosti	společnost	k1gFnSc6	společnost
silná	silný	k2eAgFnSc1d1	silná
korupce	korupce	k1gFnSc1	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Kličky	klička	k1gFnPc1	klička
v	v	k7c6	v
zákonech	zákon	k1gInPc6	zákon
a	a	k8xC	a
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
politická	politický	k2eAgFnSc1d1	politická
kultura	kultura	k1gFnSc1	kultura
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
případní	případný	k2eAgMnPc1d1	případný
provinilci	provinilec	k1gMnPc1	provinilec
za	za	k7c4	za
korupční	korupční	k2eAgFnPc4d1	korupční
aktivity	aktivita	k1gFnPc4	aktivita
odsouzeni	odsouzen	k2eAgMnPc1d1	odsouzen
<g/>
.	.	kIx.	.
</s>
<s>
Opakují	opakovat	k5eAaImIp3nP	opakovat
se	se	k3xPyFc4	se
také	také	k9	také
případy	případ	k1gInPc1	případ
zastrašování	zastrašování	k1gNnPc2	zastrašování
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
zneužívání	zneužívání	k1gNnSc3	zneužívání
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
braní	braní	k1gNnSc2	braní
úplatků	úplatek	k1gInPc2	úplatek
uzákonila	uzákonit	k5eAaPmAgFnS	uzákonit
několik	několik	k4yIc4	několik
opatření	opatření	k1gNnPc2	opatření
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
především	především	k9	především
vláda	vláda	k1gFnSc1	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
přehnaně	přehnaně	k6eAd1	přehnaně
zatížený	zatížený	k2eAgInSc1d1	zatížený
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
soudci	soudce	k1gMnPc1	soudce
nemají	mít	k5eNaImIp3nP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
mnohé	mnohý	k2eAgInPc1d1	mnohý
soudní	soudní	k2eAgInPc1d1	soudní
procesy	proces	k1gInPc1	proces
trvají	trvat	k5eAaImIp3nP	trvat
neúměrně	úměrně	k6eNd1	úměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gMnPc1	jejich
účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
obracejí	obracet	k5eAaImIp3nP	obracet
na	na	k7c4	na
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
těmito	tento	k3xDgFnPc7	tento
záležitostmi	záležitost	k1gFnPc7	záležitost
zahlcen	zahltit	k5eAaPmNgInS	zahltit
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
věnovat	věnovat	k5eAaPmF	věnovat
některým	některý	k3yIgNnPc3	některý
kontroverzním	kontroverzní	k2eAgNnPc3d1	kontroverzní
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
otázce	otázka	k1gFnSc6	otázka
potratů	potrat	k1gInPc2	potrat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
zavedla	zavést	k5eAaPmAgFnS	zavést
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
rozdělení	rozdělení	k1gNnSc2	rozdělení
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
8	[number]	k4	8
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
jsou	být	k5eAaImIp3nP	být
Bratislavský	bratislavský	k2eAgInSc4d1	bratislavský
<g/>
,	,	kIx,	,
Košický	košický	k2eAgInSc4d1	košický
<g/>
,	,	kIx,	,
Nitranský	nitranský	k2eAgInSc4d1	nitranský
<g/>
,	,	kIx,	,
Prešovský	prešovský	k2eAgInSc4d1	prešovský
<g/>
,	,	kIx,	,
Žilinský	žilinský	k2eAgInSc4d1	žilinský
<g/>
,	,	kIx,	,
Trenčínský	trenčínský	k2eAgInSc4d1	trenčínský
<g/>
,	,	kIx,	,
Trnavský	trnavský	k2eAgInSc4d1	trnavský
a	a	k8xC	a
Banskobystrický	banskobystrický	k2eAgInSc4d1	banskobystrický
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
celkem	celkem	k6eAd1	celkem
79	[number]	k4	79
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
populace	populace	k1gFnSc1	populace
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
celkem	celek	k1gInSc7	celek
446	[number]	k4	446
819	[number]	k4	819
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nP	patřit
Košice	Košice	k1gInPc1	Košice
(	(	kIx(	(
<g/>
240	[number]	k4	240
915	[number]	k4	915
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prešov	Prešov	k1gInSc1	Prešov
(	(	kIx(	(
<g/>
92	[number]	k4	92
687	[number]	k4	687
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nitra	Nitra	k1gFnSc1	Nitra
(	(	kIx(	(
<g/>
87	[number]	k4	87
357	[number]	k4	357
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
(	(	kIx(	(
<g/>
86	[number]	k4	86
685	[number]	k4	685
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
a	a	k8xC	a
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
(	(	kIx(	(
<g/>
84	[number]	k4	84
919	[number]	k4	919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
Slovensko	Slovensko	k1gNnSc4	Slovensko
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
dvacetiletou	dvacetiletý	k2eAgFnSc4d1	dvacetiletá
existenci	existence	k1gFnSc4	existence
získalo	získat	k5eAaPmAgNnS	získat
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
nadnárodních	nadnárodní	k2eAgInPc6d1	nadnárodní
společenstvích	společenství	k1gNnPc6	společenství
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
obchodní	obchodní	k2eAgFnSc1d1	obchodní
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
OSCE	OSCE	kA	OSCE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
zastávala	zastávat	k5eAaImAgFnS	zastávat
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
pozici	pozice	k1gFnSc4	pozice
nestálého	stálý	k2eNgMnSc4d1	nestálý
člena	člen	k1gMnSc4	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
angažuje	angažovat	k5eAaBmIp3nS	angažovat
například	například	k6eAd1	například
ve	v	k7c6	v
Visegrádské	visegrádský	k2eAgFnSc6d1	Visegrádská
čtyřce	čtyřka	k1gFnSc6	čtyřka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
vlády	vláda	k1gFnPc1	vláda
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prosazování	prosazování	k1gNnSc2	prosazování
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
prohlubováním	prohlubování	k1gNnSc7	prohlubování
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
regionálních	regionální	k2eAgNnPc2d1	regionální
uskupení	uskupení	k1gNnPc2	uskupení
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
CEFTA	CEFTA	kA	CEFTA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
iniciativy	iniciativa	k1gFnSc2	iniciativa
(	(	kIx(	(
<g/>
SEI	SEI	kA	SEI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
sousedů	soused	k1gMnPc2	soused
udržuje	udržovat	k5eAaImIp3nS	udržovat
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
víceméně	víceméně	k9	víceméně
korektní	korektní	k2eAgInSc4d1	korektní
a	a	k8xC	a
stabilní	stabilní	k2eAgInPc4d1	stabilní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
nehrály	hrát	k5eNaImAgFnP	hrát
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
významnější	významný	k2eAgFnSc4d2	významnější
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgNnSc4d1	jisté
zlepšení	zlepšení	k1gNnSc4	zlepšení
přinesl	přinést	k5eAaPmAgInS	přinést
nástup	nástup	k1gInSc1	nástup
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
politika	politik	k1gMnSc2	politik
Viktora	Viktor	k1gMnSc2	Viktor
Juščenka	Juščenka	k1gFnSc1	Juščenka
a	a	k8xC	a
zejména	zejména	k9	zejména
oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
podporovaly	podporovat	k5eAaImAgFnP	podporovat
jak	jak	k6eAd1	jak
slovenské	slovenský	k2eAgFnPc1d1	slovenská
nevládní	vládní	k2eNgFnPc1d1	nevládní
organizace	organizace	k1gFnPc1	organizace
tak	tak	k8xC	tak
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovuzískání	znovuzískání	k1gNnSc6	znovuzískání
moci	moc	k1gFnSc2	moc
Viktora	Viktor	k1gMnSc2	Viktor
Janukoviče	Janukovič	k1gMnSc2	Janukovič
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
kulturní	kulturní	k2eAgFnSc1d1	kulturní
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
spolupráce	spolupráce	k1gFnSc1	spolupráce
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
upadla	upadnout	k5eAaPmAgFnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velmocí	velmoc	k1gFnPc2	velmoc
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
partnerem	partner	k1gMnSc7	partner
především	především	k9	především
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Američanů	Američan	k1gMnPc2	Američan
účastnila	účastnit	k5eAaImAgFnS	účastnit
války	válka	k1gFnPc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Roberta	Robert	k1gMnSc2	Robert
Fica	Fico	k1gMnSc2	Fico
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
opadly	opadnout	k5eAaPmAgFnP	opadnout
a	a	k8xC	a
slovenské	slovenský	k2eAgFnPc1d1	slovenská
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
staženy	stažen	k2eAgInPc1d1	stažen
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
až	až	k9	až
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
dohodě	dohoda	k1gFnSc6	dohoda
obou	dva	k4xCgFnPc2	dva
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
velmocí	velmoc	k1gFnPc2	velmoc
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
důsledným	důsledný	k2eAgMnSc7d1	důsledný
proponentem	proponent	k1gMnSc7	proponent
silnější	silný	k2eAgFnSc2d2	silnější
orientace	orientace	k1gFnSc2	orientace
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
právě	právě	k9	právě
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
.	.	kIx.	.
</s>
<s>
Komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
vztahy	vztah	k1gInPc1	vztah
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
zemích	zem	k1gFnPc6	zem
fungují	fungovat	k5eAaImIp3nP	fungovat
vlády	vláda	k1gFnPc1	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
agendu	agenda	k1gFnSc4	agenda
využívají	využívat	k5eAaImIp3nP	využívat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
nacionalistických	nacionalistický	k2eAgNnPc2d1	nacionalistické
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
opakovaně	opakovaně	k6eAd1	opakovaně
vyostřují	vyostřovat	k5eAaImIp3nP	vyostřovat
národnostní	národnostní	k2eAgInPc4d1	národnostní
konflikty	konflikt	k1gInPc4	konflikt
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
mají	mít	k5eAaImIp3nP	mít
dodnes	dodnes	k6eAd1	dodnes
blízké	blízký	k2eAgInPc4d1	blízký
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
jazykové	jazykový	k2eAgInPc4d1	jazykový
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c6	v
rozdílných	rozdílný	k2eAgInPc6d1	rozdílný
státních	státní	k2eAgInPc6d1	státní
útvarech	útvar	k1gInPc6	útvar
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
dva	dva	k4xCgInPc1	dva
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
etnické	etnický	k2eAgInPc1d1	etnický
celky	celek	k1gInPc1	celek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
přes	přes	k7c4	přes
časté	častý	k2eAgInPc4d1	častý
kontakty	kontakt	k1gInPc4	kontakt
navzájem	navzájem	k6eAd1	navzájem
udržely	udržet	k5eAaPmAgInP	udržet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
výlučnost	výlučnost	k1gFnSc4	výlučnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
probíhaly	probíhat	k5eAaImAgFnP	probíhat
mezi	mezi	k7c4	mezi
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Slováky	Slováky	k1gInPc4	Slováky
četné	četný	k2eAgInPc4d1	četný
kulturní	kulturní	k2eAgInPc4d1	kulturní
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc4d1	projevující
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xS	jako
literárního	literární	k2eAgInSc2d1	literární
a	a	k8xC	a
liturgického	liturgický	k2eAgInSc2d1	liturgický
jazyka	jazyk	k1gInSc2	jazyk
především	především	k6eAd1	především
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
protestanty	protestant	k1gMnPc7	protestant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
obě	dva	k4xCgNnPc1	dva
etnika	etnikum	k1gNnPc1	etnikum
spolupracovala	spolupracovat	k5eAaImAgNnP	spolupracovat
a	a	k8xC	a
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
společně	společně	k6eAd1	společně
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
novému	nový	k2eAgInSc3d1	nový
společnému	společný	k2eAgInSc3d1	společný
státnímu	státní	k2eAgInSc3d1	státní
útvaru	útvar	k1gInSc3	útvar
–	–	k?	–
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
vztahy	vztah	k1gInPc1	vztah
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
pojetí	pojetí	k1gNnSc6	pojetí
fungování	fungování	k1gNnSc4	fungování
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
politickými	politický	k2eAgFnPc7d1	politická
představiteli	představitel	k1gMnPc7	představitel
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
spojené	spojený	k2eAgInPc4d1	spojený
i	i	k8xC	i
slovenské	slovenský	k2eAgNnSc4d1	slovenské
autonomní	autonomní	k2eAgNnSc4d1	autonomní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
souviselo	souviset	k5eAaImAgNnS	souviset
i	i	k9	i
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
prvního	první	k4xOgInSc2	první
samostatného	samostatný	k2eAgInSc2d1	samostatný
slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nacistickému	nacistický	k2eAgMnSc3d1	nacistický
diktátorovi	diktátor	k1gMnSc3	diktátor
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
rozbití	rozbití	k1gNnSc1	rozbití
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
Češi	Čech	k1gMnPc1	Čech
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
proto	proto	k8xC	proto
chápali	chápat	k5eAaImAgMnP	chápat
Slováky	Slováky	k1gInPc4	Slováky
jako	jako	k8xS	jako
nespolehlivý	spolehlivý	k2eNgInSc4d1	nespolehlivý
element	element	k1gInSc4	element
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
oslaben	oslabit	k5eAaPmNgInS	oslabit
například	například	k6eAd1	například
událostmi	událost	k1gFnPc7	událost
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
tak	tak	k6eAd1	tak
nebylo	být	k5eNaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
formě	forma	k1gFnSc3	forma
česko-slovenského	českolovenský	k2eAgNnSc2d1	česko-slovenské
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
za	za	k7c2	za
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byl	být	k5eAaImAgInS	být
nastolen	nastolit	k5eAaPmNgInS	nastolit
přísně	přísně	k6eAd1	přísně
centrálně	centrálně	k6eAd1	centrálně
spravovaný	spravovaný	k2eAgInSc1d1	spravovaný
systém	systém	k1gInSc1	systém
řízený	řízený	k2eAgInSc1d1	řízený
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zůstala	zůstat	k5eAaPmAgFnS	zůstat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
a	a	k8xC	a
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
problémy	problém	k1gInPc4	problém
soužití	soužití	k1gNnSc2	soužití
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
státě	stát	k1gInSc6	stát
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
odsunuty	odsunout	k5eAaPmNgInP	odsunout
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
kompetencí	kompetence	k1gFnPc2	kompetence
mezi	mezi	k7c7	mezi
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částmi	část	k1gFnPc7	část
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
předmětem	předmět	k1gInSc7	předmět
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
a	a	k8xC	a
náročných	náročný	k2eAgNnPc2d1	náročné
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
politických	politický	k2eAgFnPc2d1	politická
špiček	špička	k1gFnPc2	špička
o	o	k7c6	o
odluce	odluka	k1gFnSc6	odluka
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
a	a	k8xC	a
ve	v	k7c4	v
vznik	vznik	k1gInSc4	vznik
dvou	dva	k4xCgInPc2	dva
samostatných	samostatný	k2eAgInPc2d1	samostatný
suverénních	suverénní	k2eAgInPc2d1	suverénní
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
výjimečné	výjimečný	k2eAgFnSc6d1	výjimečná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
spolu	spolu	k6eAd1	spolu
úzce	úzko	k6eAd1	úzko
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
na	na	k7c4	na
prosazení	prosazení	k1gNnSc4	prosazení
společných	společný	k2eAgInPc2d1	společný
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
čele	čelo	k1gNnSc6	čelo
strany	strana	k1gFnSc2	strana
opačné	opačný	k2eAgFnSc2d1	opačná
politické	politický	k2eAgFnSc2d1	politická
orientace	orientace	k1gFnSc2	orientace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pravicová	pravicový	k2eAgFnSc1d1	pravicová
ODS	ODS	kA	ODS
a	a	k8xC	a
levicový	levicový	k2eAgMnSc1d1	levicový
SMER	SMER	kA	SMER
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Maďarech	Maďar	k1gMnPc6	Maďar
a	a	k8xC	a
Romech	Rom	k1gMnPc6	Rom
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komunita	komunita	k1gFnSc1	komunita
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
44	[number]	k4	44
600	[number]	k4	600
osob	osoba	k1gFnPc2	osoba
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
menšinou	menšina	k1gFnSc7	menšina
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
také	také	k9	také
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
navzájem	navzájem	k6eAd1	navzájem
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zemích	zem	k1gFnPc6	zem
velvyslanecké	velvyslanecký	k2eAgInPc1d1	velvyslanecký
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
pozemních	pozemní	k2eAgFnPc2d1	pozemní
a	a	k8xC	a
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
silami	síla	k1gFnPc7	síla
zaměřenými	zaměřený	k2eAgFnPc7d1	zaměřená
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
slovenská	slovenský	k2eAgFnSc1d1	slovenská
armáda	armáda	k1gFnSc1	armáda
14	[number]	k4	14
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
letouny	letoun	k1gInPc1	letoun
typu	typ	k1gInSc2	typ
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
29	[number]	k4	29
a	a	k8xC	a
L-	L-	k1gMnSc1	L-
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
helikoptéry	helikoptéra	k1gFnPc4	helikoptéra
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
24	[number]	k4	24
a	a	k8xC	a
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
17	[number]	k4	17
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
transportní	transportní	k2eAgNnPc1d1	transportní
letadla	letadlo	k1gNnPc1	letadlo
An-	An-	k1gFnPc2	An-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
An-	An-	k1gFnSc1	An-
<g/>
26	[number]	k4	26
a	a	k8xC	a
L-	L-	k1gMnSc1	L-
<g/>
410	[number]	k4	410
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
probíhat	probíhat	k5eAaImF	probíhat
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
omlazení	omlazení	k1gNnSc1	omlazení
užívaných	užívaný	k2eAgNnPc2d1	užívané
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
modernizaci	modernizace	k1gFnSc3	modernizace
letového	letový	k2eAgInSc2d1	letový
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
budou	být	k5eAaImBp3nP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
všechny	všechen	k3xTgFnPc1	všechen
slovenské	slovenský	k2eAgFnPc1d1	slovenská
tankové	tankový	k2eAgFnPc1d1	tanková
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
provedlo	provést	k5eAaPmAgNnS	provést
Slovensko	Slovensko	k1gNnSc1	Slovensko
řadu	řad	k1gInSc2	řad
reforem	reforma	k1gFnPc2	reforma
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
především	především	k9	především
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
k	k	k7c3	k
plně	plně	k6eAd1	plně
profesionální	profesionální	k2eAgFnSc3d1	profesionální
armádě	armáda	k1gFnSc3	armáda
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
armádní	armádní	k2eAgFnPc1d1	armádní
jednotky	jednotka	k1gFnPc1	jednotka
momentálně	momentálně	k6eAd1	momentálně
operují	operovat	k5eAaImIp3nP	operovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
misích	mise	k1gFnPc6	mise
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
společně	společně	k6eAd1	společně
s	s	k7c7	s
NATO	NATO	kA	NATO
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
OSN	OSN	kA	OSN
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
a	a	k8xC	a
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
a	a	k8xC	a
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
těchto	tento	k3xDgFnPc2	tento
misí	mise	k1gFnPc2	mise
účastní	účastnit	k5eAaImIp3nS	účastnit
asi	asi	k9	asi
500	[number]	k4	500
slovenských	slovenský	k2eAgMnPc2d1	slovenský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
armádní	armádní	k2eAgFnSc2d1	armádní
policie	policie	k1gFnSc2	policie
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
dvě	dva	k4xCgFnPc4	dva
výzvědné	výzvědný	k2eAgFnPc4d1	výzvědná
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
vydává	vydávat	k5eAaImIp3nS	vydávat
vláda	vláda	k1gFnSc1	vláda
1,1	[number]	k4	1,1
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
lokální	lokální	k2eAgFnSc6d1	lokální
a	a	k8xC	a
státní	státní	k2eAgFnSc6d1	státní
a	a	k8xC	a
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
zatížena	zatížit	k5eAaPmNgFnS	zatížit
rozvojem	rozvoj	k1gInSc7	rozvoj
kriminality	kriminalita	k1gFnSc2	kriminalita
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
demokratizace	demokratizace	k1gFnSc2	demokratizace
a	a	k8xC	a
liberalizace	liberalizace	k1gFnSc2	liberalizace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
problémem	problém	k1gInSc7	problém
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
gangy	gang	k1gInPc4	gang
a	a	k8xC	a
produkce	produkce	k1gFnSc2	produkce
syntetických	syntetický	k2eAgFnPc2d1	syntetická
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Extáze	extáze	k1gFnSc1	extáze
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nadace	nadace	k1gFnSc2	nadace
Bertelsmann	Bertelsmann	k1gInSc1	Bertelsmann
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
vlády	vláda	k1gFnSc2	vláda
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
sil	síla	k1gFnPc2	síla
–	–	k?	–
všichni	všechen	k3xTgMnPc1	všechen
významní	významný	k2eAgMnPc1d1	významný
političtí	politický	k2eAgMnPc1d1	politický
a	a	k8xC	a
sociální	sociální	k2eAgMnPc1d1	sociální
aktéři	aktér	k1gMnPc1	aktér
respektují	respektovat	k5eAaImIp3nP	respektovat
legitimitu	legitimita	k1gFnSc4	legitimita
demokratických	demokratický	k2eAgFnPc2d1	demokratická
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
otevřená	otevřený	k2eAgFnSc1d1	otevřená
ekonomika	ekonomika	k1gFnSc1	ekonomika
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
export	export	k1gInSc4	export
je	být	k5eAaImIp3nS	být
Slovensko	Slovensko	k1gNnSc1	Slovensko
silně	silně	k6eAd1	silně
hospodářský	hospodářský	k2eAgMnSc1d1	hospodářský
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
největším	veliký	k2eAgMnSc6d3	veliký
obchodním	obchodní	k2eAgMnSc6d1	obchodní
partneru	partner	k1gMnSc6	partner
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
dekádě	dekáda	k1gFnSc6	dekáda
prošlo	projít	k5eAaPmAgNnS	projít
Slovensko	Slovensko	k1gNnSc4	Slovensko
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
–	–	k?	–
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
levná	levný	k2eAgFnSc1d1	levná
a	a	k8xC	a
schopná	schopný	k2eAgFnSc1d1	schopná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
vhodná	vhodný	k2eAgFnSc1d1	vhodná
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
daňová	daňový	k2eAgFnSc1d1	daňová
politika	politika	k1gFnSc1	politika
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
liberální	liberální	k2eAgInSc1d1	liberální
zákoník	zákoník	k1gInSc1	zákoník
práce	práce	k1gFnSc2	práce
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
investory	investor	k1gMnPc4	investor
velká	velký	k2eAgNnPc4d1	velké
pozitiva	pozitivum	k1gNnPc4	pozitivum
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
zahraničním	zahraniční	k2eAgInSc7d1	zahraniční
dluhem	dluh	k1gInSc7	dluh
<g/>
,	,	kIx,	,
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
schodkem	schodek	k1gInSc7	schodek
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inflací	inflace	k1gFnSc7	inflace
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
již	již	k6eAd1	již
Slovensko	Slovensko	k1gNnSc1	Slovensko
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
a	a	k8xC	a
stát	stát	k1gInSc1	stát
mohl	moct	k5eAaImAgInS	moct
fungovat	fungovat	k5eAaImF	fungovat
s	s	k7c7	s
vyrovnanějším	vyrovnaný	k2eAgInSc7d2	vyrovnanější
rozpočtem	rozpočet	k1gInSc7	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
i	i	k8xC	i
Slovensko	Slovensko	k1gNnSc4	Slovensko
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
světová	světový	k2eAgFnSc1d1	světová
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
obnovené	obnovený	k2eAgFnSc3d1	obnovená
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
poptávce	poptávka	k1gFnSc3	poptávka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
slovenské	slovenský	k2eAgNnSc4d1	slovenské
hospodářství	hospodářství	k1gNnPc4	hospodářství
opět	opět	k6eAd1	opět
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
Slovenska	Slovensko	k1gNnSc2	Slovensko
o	o	k7c4	o
4,9	[number]	k4	4,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
růst	růst	k1gInSc1	růst
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4,2	[number]	k4	4,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
3,3	[number]	k4	3,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
populistické	populistický	k2eAgInPc4d1	populistický
kroky	krok	k1gInPc4	krok
první	první	k4xOgFnSc2	první
Ficovy	Ficův	k2eAgFnSc2d1	Ficova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
většinou	většinou	k6eAd1	většinou
spočívaly	spočívat	k5eAaImAgFnP	spočívat
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
dotacích	dotace	k1gFnPc6	dotace
často	často	k6eAd1	často
neperspektivním	perspektivní	k2eNgInPc3d1	neperspektivní
podnikům	podnik	k1gInPc3	podnik
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
udržení	udržení	k1gNnSc2	udržení
umělé	umělý	k2eAgFnSc2d1	umělá
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
<g/>
,	,	kIx,	,
způsobily	způsobit	k5eAaPmAgFnP	způsobit
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
zatížení	zatížení	k1gNnSc4	zatížení
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
poté	poté	k6eAd1	poté
musela	muset	k5eAaImAgFnS	muset
následující	následující	k2eAgFnSc1d1	následující
vláda	vláda	k1gFnSc1	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
řešit	řešit	k5eAaImF	řešit
drastickým	drastický	k2eAgNnSc7d1	drastické
omezením	omezení	k1gNnSc7	omezení
výdajů	výdaj	k1gInPc2	výdaj
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
některých	některý	k3yIgFnPc2	některý
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
výše	vysoce	k6eAd2	vysoce
128,5	[number]	k4	128,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
celkově	celkově	k6eAd1	celkově
a	a	k8xC	a
23	[number]	k4	23
600	[number]	k4	600
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
činila	činit	k5eAaImAgFnS	činit
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
zhruba	zhruba	k6eAd1	zhruba
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
slovenská	slovenský	k2eAgFnSc1d1	slovenská
koruna	koruna	k1gFnSc1	koruna
a	a	k8xC	a
halíř	halíř	k1gInSc1	halíř
(	(	kIx(	(
<g/>
1	[number]	k4	1
koruna	koruna	k1gFnSc1	koruna
=	=	kIx~	=
100	[number]	k4	100
haléřů	haléř	k1gInPc2	haléř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
je	on	k3xPp3gInPc4	on
nahradily	nahradit	k5eAaPmAgInP	nahradit
euro	euro	k1gNnSc4	euro
a	a	k8xC	a
eurocent	eurocent	k1gInSc1	eurocent
(	(	kIx(	(
<g/>
1	[number]	k4	1
euro	euro	k1gNnSc1	euro
=	=	kIx~	=
100	[number]	k4	100
eurocentů	eurocent	k1gInPc2	eurocent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
vývoz	vývoz	k1gInSc1	vývoz
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
75,3	[number]	k4	75,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
exportovaných	exportovaný	k2eAgInPc6d1	exportovaný
produktech	produkt	k1gInPc6	produkt
převažovaly	převažovat	k5eAaImAgFnP	převažovat
zejména	zejména	k6eAd1	zejména
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
a	a	k8xC	a
elektronická	elektronický	k2eAgFnSc1d1	elektronická
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgInPc1d1	běžný
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
minerály	minerál	k1gInPc1	minerál
a	a	k8xC	a
plasty	plast	k1gInPc1	plast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
obsadily	obsadit	k5eAaPmAgInP	obsadit
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
obchodovaném	obchodovaný	k2eAgNnSc6d1	obchodované
zboží	zboží	k1gNnSc6	zboží
namísto	namísto	k7c2	namísto
automobilů	automobil	k1gInPc2	automobil
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
exportními	exportní	k2eAgMnPc7d1	exportní
partnery	partner	k1gMnPc7	partner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byly	být	k5eAaImAgInP	být
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
19,2	[number]	k4	19,2
%	%	kIx~	%
vývozu	vývoz	k1gInSc2	vývoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
13,7	[number]	k4	13,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
7,3	[number]	k4	7,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
6,8	[number]	k4	6,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
6,8	[number]	k4	6,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dovozem	dovoz	k1gInSc7	dovoz
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
72,03	[number]	k4	72,03
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
si	se	k3xPyFc3	se
Slovensko	Slovensko	k1gNnSc4	Slovensko
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
udrželo	udržet	k5eAaPmAgNnS	udržet
kladnou	kladný	k2eAgFnSc4d1	kladná
platební	platební	k2eAgFnSc4d1	platební
bilanci	bilance	k1gFnSc4	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Dovážely	dovážet	k5eAaImAgFnP	dovážet
se	se	k3xPyFc4	se
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
elektronické	elektronický	k2eAgNnSc1d1	elektronické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgInPc1d1	běžný
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
audio	audio	k2eAgMnSc1d1	audio
a	a	k8xC	a
video	video	k1gNnSc1	video
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
dovoz	dovoz	k1gInSc4	dovoz
farmaceutických	farmaceutický	k2eAgInPc2d1	farmaceutický
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byli	být	k5eAaImAgMnP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
dovozci	dovozce	k1gMnPc7	dovozce
především	především	k6eAd1	především
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
15,8	[number]	k4	15,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
10,2	[number]	k4	10,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
9,8	[number]	k4	9,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zásoby	zásoba	k1gFnPc1	zásoba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
lignitu	lignit	k1gInSc2	lignit
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
kolem	kolem	k7c2	kolem
Handlové	Handlová	k1gFnSc2	Handlová
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
poblíž	poblíž	k7c2	poblíž
Modrého	modrý	k2eAgInSc2d1	modrý
Kameňa	Kameň	k1gInSc2	Kameň
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Gbely	Gbely	k1gInPc1	Gbely
těží	těžet	k5eAaImIp3nP	těžet
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
menší	malý	k2eAgNnSc1d2	menší
naleziště	naleziště	k1gNnSc1	naleziště
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
antimonu	antimon	k1gInSc2	antimon
<g/>
,	,	kIx,	,
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
magnezitu	magnezit	k1gInSc2	magnezit
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
ložiska	ložisko	k1gNnPc1	ložisko
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
převážně	převážně	k6eAd1	převážně
nevyužita	využit	k2eNgFnSc1d1	nevyužita
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
má	mít	k5eAaImIp3nS	mít
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
sektor	sektor	k1gInSc1	sektor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zemi	zem	k1gFnSc3	zem
dostatek	dostatek	k1gInSc4	dostatek
produkce	produkce	k1gFnSc2	produkce
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
zaměstnávalo	zaměstnávat	k5eAaImAgNnS	zaměstnávat
zemědělství	zemědělství	k1gNnSc1	zemědělství
celkem	celkem	k6eAd1	celkem
3,5	[number]	k4	3,5
%	%	kIx~	%
slovenské	slovenský	k2eAgFnSc2d1	slovenská
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
zastoupení	zastoupení	k1gNnSc1	zastoupení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sektoru	sektor	k1gInSc6	sektor
mají	mít	k5eAaImIp3nP	mít
zejména	zejména	k9	zejména
zemědělská	zemědělský	k2eAgNnPc1d1	zemědělské
družstva	družstvo	k1gNnPc1	družstvo
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samostatní	samostatný	k2eAgMnPc1d1	samostatný
zemědělci	zemědělec	k1gMnPc1	zemědělec
disponují	disponovat	k5eAaBmIp3nP	disponovat
pouze	pouze	k6eAd1	pouze
8	[number]	k4	8
%	%	kIx~	%
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
činnost	činnost	k1gFnSc1	činnost
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Podunajské	podunajský	k2eAgFnSc6d1	Podunajská
a	a	k8xC	a
Východoslovenské	východoslovenský	k2eAgFnSc6d1	Východoslovenská
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
výrobě	výroba	k1gFnSc6	výroba
převažuje	převažovat	k5eAaImIp3nS	převažovat
zejména	zejména	k9	zejména
pšenice	pšenice	k1gFnSc1	pšenice
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
také	také	k9	také
lesní	lesní	k2eAgInPc1d1	lesní
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Váh	Váh	k1gInSc4	Váh
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
co	co	k9	co
do	do	k7c2	do
pěstování	pěstování	k1gNnSc2	pěstování
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
významným	významný	k2eAgInSc7d1	významný
regionem	region	k1gInSc7	region
Tokajská	tokajský	k2eAgFnSc1d1	tokajská
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
jsou	být	k5eAaImIp3nP	být
chována	chován	k2eAgNnPc1d1	chováno
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
a	a	k8xC	a
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
převažuje	převažovat	k5eAaImIp3nS	převažovat
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nacházely	nacházet	k5eAaImAgFnP	nacházet
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
výkonných	výkonný	k2eAgInPc2d1	výkonný
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
závodů	závod	k1gInPc2	závod
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
efektivnějšímu	efektivní	k2eAgNnSc3d2	efektivnější
hospodářství	hospodářství	k1gNnSc3	hospodářství
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
zpomalen	zpomalit	k5eAaPmNgInS	zpomalit
klientelismem	klientelismus	k1gInSc7	klientelismus
a	a	k8xC	a
nezodpovědnými	zodpovědný	k2eNgInPc7d1	nezodpovědný
kroky	krok	k1gInPc7	krok
Mečiarovy	Mečiarův	k2eAgFnSc2d1	Mečiarova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
slovenský	slovenský	k2eAgInSc1d1	slovenský
průmysl	průmysl	k1gInSc1	průmysl
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
–	–	k?	–
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
27	[number]	k4	27
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
centra	centrum	k1gNnPc1	centrum
představují	představovat	k5eAaImIp3nP	představovat
především	především	k6eAd1	především
Bratislava	Bratislava	k1gFnSc1	Bratislava
a	a	k8xC	a
Košice	Košice	k1gInPc1	Košice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
kolem	kolem	k6eAd1	kolem
povodí	povodit	k5eAaPmIp3nS	povodit
řeky	řeka	k1gFnPc1	řeka
Váhu	Váh	k1gInSc2	Váh
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
odvětví	odvětví	k1gNnSc1	odvětví
představuje	představovat	k5eAaImIp3nS	představovat
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dominují	dominovat	k5eAaImIp3nP	dominovat
tři	tři	k4xCgFnPc4	tři
největší	veliký	k2eAgFnPc4d3	veliký
firmy	firma	k1gFnPc4	firma
bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
Volkswagen	volkswagen	k1gInSc1	volkswagen
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
,	,	kIx,	,
trnavská	trnavský	k2eAgFnSc1d1	Trnavská
PSA	pes	k1gMnSc4	pes
Peugeot	peugeot	k1gInSc4	peugeot
Citroën	Citroën	k1gNnSc1	Citroën
a	a	k8xC	a
žilinská	žilinský	k2eAgFnSc1d1	Žilinská
KIA	KIA	kA	KIA
Motors	Motors	k1gInSc4	Motors
Slovakia	Slovakium	k1gNnSc2	Slovakium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
postupně	postupně	k6eAd1	postupně
obnovily	obnovit	k5eAaPmAgFnP	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
výrobu	výroba	k1gFnSc4	výroba
Východoslovenské	východoslovenský	k2eAgFnPc1d1	Východoslovenská
železárny	železárna	k1gFnPc1	železárna
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
sovětského	sovětský	k2eAgNnSc2d1	sovětské
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
plánování	plánování	k1gNnSc2	plánování
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
koupila	koupit	k5eAaPmAgFnS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Steel	Steel	k1gMnSc1	Steel
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgFnPc2	tento
významných	významný	k2eAgFnPc2d1	významná
firem	firma	k1gFnPc2	firma
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zastoupení	zastoupení	k1gNnSc2	zastoupení
také	také	k9	také
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
elektrochemický	elektrochemický	k2eAgInSc4d1	elektrochemický
<g/>
,	,	kIx,	,
ropný	ropný	k2eAgInSc4d1	ropný
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
a	a	k8xC	a
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
keramika	keramika	k1gFnSc1	keramika
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc1d1	přírodní
i	i	k8xC	i
umělé	umělý	k2eAgFnPc1d1	umělá
textilie	textilie	k1gFnPc1	textilie
<g/>
,	,	kIx,	,
materiály	materiál	k1gInPc1	materiál
potřebné	potřebný	k2eAgInPc1d1	potřebný
na	na	k7c6	na
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
upadá	upadat	k5eAaPmIp3nS	upadat
například	například	k6eAd1	například
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
zastoupení	zastoupení	k1gNnSc4	zastoupení
co	co	k9	co
do	do	k7c2	do
HDP	HDP	kA	HDP
i	i	k8xC	i
podílu	podíl	k1gInSc3	podíl
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
má	mít	k5eAaImIp3nS	mít
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
služby	služba	k1gFnPc1	služba
prošly	projít	k5eAaPmAgFnP	projít
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
proměnou	proměna	k1gFnSc7	proměna
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
většina	většina	k1gFnSc1	většina
firem	firma	k1gFnPc2	firma
tohoto	tento	k3xDgInSc2	tento
sektoru	sektor	k1gInSc2	sektor
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozkvět	rozkvět	k1gInSc1	rozkvět
zažil	zažít	k5eAaPmAgInS	zažít
především	především	k9	především
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
mnohé	mnohý	k2eAgFnPc4d1	mnohá
historické	historický	k2eAgFnPc4d1	historická
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgInPc4d1	umělecký
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
krásy	krása	k1gFnPc4	krása
či	či	k8xC	či
minerální	minerální	k2eAgInPc4d1	minerální
a	a	k8xC	a
termální	termální	k2eAgInPc4d1	termální
prameny	pramen	k1gInPc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
je	být	k5eAaImIp3nS	být
i	i	k9	i
pojišťovací	pojišťovací	k2eAgInSc1d1	pojišťovací
a	a	k8xC	a
bankovní	bankovní	k2eAgInSc1d1	bankovní
sektor	sektor	k1gInSc1	sektor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgNnSc4d1	silné
zastoupení	zastoupení	k1gNnSc4	zastoupení
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
telekomunikace	telekomunikace	k1gFnPc1	telekomunikace
a	a	k8xC	a
maloobchod	maloobchod	k1gInSc1	maloobchod
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obchodu	obchod	k1gInSc2	obchod
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
samostatných	samostatný	k2eAgFnPc2d1	samostatná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
sdružovány	sdružovat	k5eAaImNgFnP	sdružovat
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
a	a	k8xC	a
výrazněji	výrazně	k6eAd2	výrazně
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
kapitál	kapitál	k1gInSc1	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
objevují	objevovat	k5eAaImIp3nP	objevovat
supermarkety	supermarket	k1gInPc4	supermarket
a	a	k8xC	a
hypermarkety	hypermarket	k1gInPc4	hypermarket
<g/>
,	,	kIx,	,
stavěné	stavěný	k2eAgInPc4d1	stavěný
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
moderní	moderní	k2eAgFnSc4d1	moderní
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
především	především	k9	především
důsledkem	důsledek	k1gInSc7	důsledek
členitého	členitý	k2eAgInSc2d1	členitý
terénu	terén	k1gInSc2	terén
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
relativně	relativně	k6eAd1	relativně
řídce	řídce	k6eAd1	řídce
rozložená	rozložený	k2eAgFnSc1d1	rozložená
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgFnSc1d1	měřící
43	[number]	k4	43
761	[number]	k4	761
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
38	[number]	k4	38
085	[number]	k4	085
km	km	kA	km
zpevněné	zpevněný	k2eAgFnSc2d1	zpevněná
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
dostavěná	dostavěný	k2eAgFnSc1d1	dostavěná
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
spojovala	spojovat	k5eAaImAgFnS	spojovat
Bratislavu	Bratislava	k1gFnSc4	Bratislava
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
dokončení	dokončení	k1gNnSc1	dokončení
dálničního	dálniční	k2eAgNnSc2d1	dálniční
spojení	spojení	k1gNnSc2	spojení
mezi	mezi	k7c7	mezi
Bratislavou	Bratislava	k1gFnSc7	Bratislava
a	a	k8xC	a
Košicemi	Košice	k1gInPc7	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nachází	nacházet	k5eAaImIp3nS	nacházet
3	[number]	k4	3
622	[number]	k4	622
km	km	kA	km
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
trpí	trpět	k5eAaImIp3nS	trpět
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
neefektivností	neefektivnost	k1gFnPc2	neefektivnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
probíhají	probíhat	k5eAaImIp3nP	probíhat
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
dalším	další	k2eAgNnSc6d1	další
zlepšování	zlepšování	k1gNnSc6	zlepšování
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
je	být	k5eAaImIp3nS	být
především	především	k9	především
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
letišti	letiště	k1gNnSc6	letiště
<g/>
,	,	kIx,	,
napojení	napojení	k1gNnSc4	napojení
na	na	k7c4	na
silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
blízkosti	blízkost	k1gFnSc6	blízkost
k	k	k7c3	k
maďarským	maďarský	k2eAgFnPc3d1	maďarská
i	i	k8xC	i
rakouským	rakouský	k2eAgFnPc3d1	rakouská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
silničními	silniční	k2eAgInPc7d1	silniční
i	i	k8xC	i
železničními	železniční	k2eAgInPc7d1	železniční
uzly	uzel	k1gInPc7	uzel
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Zvolen	Zvolen	k1gInSc1	Zvolen
a	a	k8xC	a
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Bratislavy	Bratislava	k1gFnSc2	Bratislava
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
přístavem	přístav	k1gInSc7	přístav
také	také	k9	také
Komárno	Komárno	k1gNnSc1	Komárno
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
čítalo	čítat	k5eAaImAgNnS	čítat
11	[number]	k4	11
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
všechny	všechen	k3xTgFnPc1	všechen
byly	být	k5eAaImAgFnP	být
vlastněny	vlastněn	k2eAgInPc1d1	vlastněn
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc4	podíl
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
na	na	k7c4	na
37	[number]	k4	37
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19	[number]	k4	19
se	s	k7c7	s
zpevněnou	zpevněný	k2eAgFnSc7d1	zpevněná
přistávací	přistávací	k2eAgFnSc7d1	přistávací
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
mezinárodní	mezinárodní	k2eAgNnPc1d1	mezinárodní
letiště	letiště	k1gNnPc1	letiště
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
zastávají	zastávat	k5eAaImIp3nP	zastávat
přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
tepelnými	tepelný	k2eAgFnPc7d1	tepelná
a	a	k8xC	a
vodními	vodní	k2eAgFnPc7d1	vodní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
se	se	k3xPyFc4	se
především	především	k6eAd1	především
realizuje	realizovat	k5eAaBmIp3nS	realizovat
dostavba	dostavba	k1gFnSc1	dostavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
Mochovce	Mochovka	k1gFnSc6	Mochovka
a	a	k8xC	a
také	také	k9	také
plánuje	plánovat	k5eAaImIp3nS	plánovat
výstavba	výstavba	k1gFnSc1	výstavba
dalších	další	k2eAgInPc2d1	další
bloků	blok	k1gInPc2	blok
elektrárny	elektrárna	k1gFnSc2	elektrárna
Jaslovské	Jaslovský	k2eAgFnPc4d1	Jaslovský
Bohunice	Bohunice	k1gFnPc4	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Váh	Váh	k1gInSc1	Váh
<g/>
,	,	kIx,	,
Orava	Orava	k1gFnSc1	Orava
<g/>
,	,	kIx,	,
Hornád	Hornáda	k1gFnPc2	Hornáda
<g/>
,	,	kIx,	,
Slaná	slaný	k2eAgFnSc1d1	slaná
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
také	také	k9	také
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
vláda	vláda	k1gFnSc1	vláda
namísto	namísto	k7c2	namísto
solárních	solární	k2eAgFnPc2d1	solární
elektráren	elektrárna	k1gFnPc2	elektrárna
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
využití	využití	k1gNnSc1	využití
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Telekomunikace	telekomunikace	k1gFnSc1	telekomunikace
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
nového	nový	k2eAgNnSc2d1	nové
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
významně	významně	k6eAd1	významně
modernizují	modernizovat	k5eAaBmIp3nP	modernizovat
a	a	k8xC	a
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
využití	využití	k1gNnSc1	využití
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
používáno	používat	k5eAaImNgNnS	používat
celkem	celkem	k6eAd1	celkem
5,9	[number]	k4	5,9
milionu	milion	k4xCgInSc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
přes	přes	k7c4	přes
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
používalo	používat	k5eAaImAgNnS	používat
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
rozšíření	rozšíření	k1gNnSc4	rozšíření
těchto	tento	k3xDgFnPc2	tento
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
přístrojů	přístroj	k1gInPc2	přístroj
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zeměmi	zem	k1gFnPc7	zem
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vysílá	vysílat	k5eAaImIp3nS	vysílat
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
40	[number]	k4	40
národních	národní	k2eAgFnPc2d1	národní
<g/>
,	,	kIx,	,
regionálních	regionální	k2eAgFnPc2d1	regionální
i	i	k8xC	i
místních	místní	k2eAgFnPc2d1	místní
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
celonárodní	celonárodní	k2eAgInPc1d1	celonárodní
televizní	televizní	k2eAgInPc1d1	televizní
programy	program	k1gInPc1	program
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
státní	státní	k2eAgFnSc2d1	státní
společnosti	společnost	k1gFnSc2	společnost
Rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
funguje	fungovat	k5eAaImIp3nS	fungovat
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
radiových	radiový	k2eAgInPc2d1	radiový
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nadále	nadále	k6eAd1	nadále
užíván	užíván	k2eAgInSc4d1	užíván
analogový	analogový	k2eAgInSc4d1	analogový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
digitální	digitální	k2eAgNnSc4d1	digitální
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
také	také	k9	také
využití	využití	k1gNnSc1	využití
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
velkým	velký	k2eAgMnSc7d1	velký
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
slovenského	slovenský	k2eAgMnSc2d1	slovenský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
Ján	Ján	k1gMnSc1	Ján
Jesenský	Jesenský	k1gMnSc1	Jesenský
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgMnS	být
osvícenec	osvícenec	k1gMnSc1	osvícenec
Matej	Matej	k1gInSc4	Matej
Bel	Bela	k1gFnPc2	Bela
<g/>
.	.	kIx.	.
</s>
<s>
Největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Aurel	Aurel	k1gMnSc1	Aurel
Stodola	stodola	k1gFnSc1	stodola
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
technologii	technologie	k1gFnSc4	technologie
parních	parní	k2eAgFnPc2d1	parní
turbín	turbína	k1gFnPc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
optiky	optika	k1gFnSc2	optika
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
Josef	Josef	k1gMnSc1	Josef
Maximilián	Maximilián	k1gMnSc1	Maximilián
Petzval	Petzval	k1gMnSc1	Petzval
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
padáku	padák	k1gInSc2	padák
pracoval	pracovat	k5eAaImAgInS	pracovat
Štefan	Štefan	k1gMnSc1	Štefan
Banič	Banič	k1gMnSc1	Banič
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
byl	být	k5eAaImAgMnS	být
Jozef	Jozef	k1gMnSc1	Jozef
Murgaš	Murgaš	k1gMnSc1	Murgaš
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
Peter	Peter	k1gMnSc1	Peter
Kušnirák	Kušnirák	k1gMnSc1	Kušnirák
objevil	objevit	k5eAaPmAgMnS	objevit
přes	přes	k7c4	přes
230	[number]	k4	230
planetek	planetka	k1gFnPc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
německý	německý	k2eAgMnSc1d1	německý
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
nacistický	nacistický	k2eAgMnSc1d1	nacistický
exponent	exponent	k1gMnSc1	exponent
Philipp	Philipp	k1gMnSc1	Philipp
Lenard	Lenard	k1gMnSc1	Lenard
<g/>
.	.	kIx.	.
</s>
<s>
Slovenští	slovenský	k2eAgMnPc1d1	slovenský
vědci	vědec	k1gMnPc1	vědec
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
velice	velice	k6eAd1	velice
uznávanými	uznávaný	k2eAgMnPc7d1	uznávaný
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
že	že	k8xS	že
Slovensko	Slovensko	k1gNnSc4	Slovensko
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
přispívá	přispívat	k5eAaImIp3nS	přispívat
velice	velice	k6eAd1	velice
málo	málo	k4c7	málo
penězi	peníze	k1gInPc7	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
věda	věda	k1gFnSc1	věda
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
finančně	finančně	k6eAd1	finančně
nejchudší	chudý	k2eAgFnSc7d3	nejchudší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Slováci	Slovák	k1gMnPc1	Slovák
se	se	k3xPyFc4	se
například	například	k6eAd1	například
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
výzkumu	výzkum	k1gInSc6	výzkum
alternativního	alternativní	k2eAgInSc2d1	alternativní
zdroje	zdroj	k1gInSc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
technologiích	technologie	k1gFnPc6	technologie
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
6	[number]	k4	6
<g/>
letá	letý	k2eAgFnSc1d1	letá
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
zavedena	zaveden	k2eAgFnSc1d1	zavedena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
letá	letý	k2eAgNnPc1d1	leté
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
éry	éra	k1gFnSc2	éra
socialismu	socialismus	k1gInSc2	socialismus
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
10	[number]	k4	10
<g/>
letou	letý	k2eAgFnSc4d1	letá
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
žáci	žák	k1gMnPc1	žák
absolvovat	absolvovat	k5eAaPmF	absolvovat
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
na	na	k7c6	na
8	[number]	k4	8
<g/>
letých	letý	k2eAgNnPc6d1	leté
gymnáziích	gymnázium	k1gNnPc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Negramotnost	negramotnost	k1gFnSc1	negramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
0,6	[number]	k4	0,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
nad	nad	k7c4	nad
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
státní	státní	k2eAgFnPc1d1	státní
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnPc1d1	církevní
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávaní	vzdělávaný	k2eAgMnPc1d1	vzdělávaný
je	on	k3xPp3gNnSc4	on
poskytované	poskytovaný	k2eAgNnSc4d1	poskytované
i	i	k9	i
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
děti	dítě	k1gFnPc1	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
6	[number]	k4	6
až	až	k9	až
14	[number]	k4	14
(	(	kIx(	(
<g/>
či	či	k8xC	či
7	[number]	k4	7
až	až	k9	až
15	[number]	k4	15
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
standardní	standardní	k2eAgInPc1d1	standardní
stupně	stupeň	k1gInPc1	stupeň
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
jsou	být	k5eAaImIp3nP	být
státem	stát	k1gInSc7	stát
poskytované	poskytovaný	k2eAgFnSc2d1	poskytovaná
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
absolvovalo	absolvovat	k5eAaPmAgNnS	absolvovat
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
34	[number]	k4	34
535	[number]	k4	535
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
slovenských	slovenský	k2eAgFnPc2d1	slovenská
univerzit	univerzita	k1gFnPc2	univerzita
se	se	k3xPyFc4	se
nedostala	dostat	k5eNaPmAgFnS	dostat
mezi	mezi	k7c4	mezi
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
univerzit	univerzita	k1gFnPc2	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgFnPc4d3	nejkvalitnější
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
východoevropského	východoevropský	k2eAgInSc2d1	východoevropský
regionu	region	k1gInSc2	region
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
hodnocení	hodnocení	k1gNnSc2	hodnocení
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
publikování	publikování	k1gNnSc2	publikování
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
)	)	kIx)	)
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
patří	patřit	k5eAaImIp3nS	patřit
Univerzita	univerzita	k1gFnSc1	univerzita
Komenského	Komenského	k2eAgFnSc1d1	Komenského
<g/>
,	,	kIx,	,
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Košiciach	Košicia	k1gFnPc6	Košicia
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Bratislave	Bratislav	k1gMnSc5	Bratislav
<g/>
,	,	kIx,	,
Poľnohospodárska	Poľnohospodárska	k1gFnSc1	Poľnohospodárska
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Nitre	Nitr	k1gInSc5	Nitr
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
Pavla	Pavel	k1gMnSc2	Pavel
Jozefa	Jozef	k1gMnSc2	Jozef
Šafárika	Šafárik	k1gMnSc2	Šafárik
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Slovenska	Slovensko	k1gNnSc2	Slovensko
čítala	čítat	k5eAaImAgFnS	čítat
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
5	[number]	k4	5
410	[number]	k4	410
728	[number]	k4	728
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
51,3	[number]	k4	51,3
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
růst	růst	k1gInSc1	růst
populace	populace	k1gFnSc2	populace
0,104	[number]	k4	0,104
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
přirozený	přirozený	k2eAgInSc4d1	přirozený
přírůstek	přírůstek	k1gInSc4	přírůstek
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
1,65	[number]	k4	1,65
narozených	narozený	k2eAgMnPc2d1	narozený
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
55	[number]	k4	55
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
urbanizace	urbanizace	k1gFnSc1	urbanizace
stoupá	stoupat	k5eAaImIp3nS	stoupat
zhruba	zhruba	k6eAd1	zhruba
0,1	[number]	k4	0,1
%	%	kIx~	%
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
111	[number]	k4	111
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
85,8	[number]	k4	85,8
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
hlásilo	hlásit	k5eAaImAgNnS	hlásit
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
9,7	[number]	k4	9,7
%	%	kIx~	%
k	k	k7c3	k
maďarské	maďarský	k2eAgFnSc3d1	maďarská
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
%	%	kIx~	%
k	k	k7c3	k
romské	romský	k2eAgFnSc3d1	romská
<g/>
,	,	kIx,	,
0,8	[number]	k4	0,8
%	%	kIx~	%
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
<g/>
,	,	kIx,	,
0,4	[number]	k4	0,4
%	%	kIx~	%
k	k	k7c3	k
rusínské	rusínský	k2eAgFnSc3d1	rusínská
a	a	k8xC	a
0,2	[number]	k4	0,2
%	%	kIx~	%
k	k	k7c3	k
ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
používání	používání	k1gNnSc1	používání
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
;	;	kIx,	;
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
především	především	k9	především
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
rusínsky	rusínsky	k6eAd1	rusínsky
<g/>
,	,	kIx,	,
romsky	romsky	k6eAd1	romsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
kliniky	klinika	k1gFnPc1	klinika
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
zdravotnická	zdravotnický	k2eAgNnPc1d1	zdravotnické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
72	[number]	k4	72
let	léto	k1gNnPc2	léto
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
80	[number]	k4	80
let	léto	k1gNnPc2	léto
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
připadají	připadat	k5eAaPmIp3nP	připadat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3	[number]	k4	3
lékaři	lékař	k1gMnPc7	lékař
a	a	k8xC	a
na	na	k7c4	na
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
celkem	celkem	k6eAd1	celkem
8,5	[number]	k4	8,5
%	%	kIx~	%
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
15,6	[number]	k4	15,6
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
mladší	mladý	k2eAgFnSc1d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
65	[number]	k4	65
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
věku	věk	k1gInSc2	věk
bylo	být	k5eAaImAgNnS	být
71,6	[number]	k4	71,6
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
starších	starý	k2eAgNnPc2d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
12,8	[number]	k4	12,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2002	[number]	k4	2002
zhruba	zhruba	k6eAd1	zhruba
14,3	[number]	k4	14,3
%	%	kIx~	%
slovenského	slovenský	k2eAgNnSc2d1	slovenské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
trpělo	trpět	k5eAaImAgNnS	trpět
obezitou	obezita	k1gFnSc7	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Slovenska	Slovensko	k1gNnSc2	Slovensko
mající	mající	k2eAgNnSc4d1	mající
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
průměrný	průměrný	k2eAgInSc4d1	průměrný
životní	životní	k2eAgInSc4d1	životní
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
nedostatečném	dostatečný	k2eNgNnSc6d1	nedostatečné
množství	množství	k1gNnSc6	množství
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
mnoha	mnoho	k4c2	mnoho
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
postavených	postavený	k2eAgFnPc2d1	postavená
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
městské	městský	k2eAgFnSc2d1	městská
populace	populace	k1gFnSc2	populace
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
elektřině	elektřina	k1gFnSc3	elektřina
a	a	k8xC	a
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dodávky	dodávka	k1gFnPc1	dodávka
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
horší	zlý	k2eAgFnSc6d2	horší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavy	ústava	k1gFnSc2	ústava
oddělena	oddělen	k2eAgFnSc1d1	oddělena
církev	církev	k1gFnSc1	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hlásilo	hlásit	k5eAaImAgNnS	hlásit
68,9	[number]	k4	68,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
k	k	k7c3	k
evangelické	evangelický	k2eAgFnSc3d1	evangelická
církvi	církev	k1gFnSc3	církev
augšpurského	augšpurský	k2eAgNnSc2d1	augšpurské
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
luteránům	luterán	k1gMnPc3	luterán
<g/>
)	)	kIx)	)
6,9	[number]	k4	6,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
řeckokatolické	řeckokatolický	k2eAgFnSc3d1	řeckokatolická
církvi	církev	k1gFnSc3	církev
4,1	[number]	k4	4,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
Reformované	reformovaný	k2eAgFnSc3d1	reformovaná
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
kalvínitům	kalvínit	k1gInPc3	kalvínit
<g/>
)	)	kIx)	)
2,0	[number]	k4	2,0
%	%	kIx~	%
a	a	k8xC	a
k	k	k7c3	k
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
0,9	[number]	k4	0,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Církve	církev	k1gFnPc1	církev
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc1d1	náboženská
organizace	organizace	k1gFnPc1	organizace
s	s	k7c7	s
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
000	[number]	k4	000
členy	člen	k1gInPc7	člen
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
úlevy	úleva	k1gFnPc4	úleva
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
státem	stát	k1gInSc7	stát
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Vatikánem	Vatikán	k1gInSc7	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgNnPc1d1	náboženské
dogmata	dogma	k1gNnPc1	dogma
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc1	žádný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
fungování	fungování	k1gNnSc4	fungování
politiky	politika	k1gFnSc2	politika
ani	ani	k8xC	ani
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
skupin	skupina	k1gFnPc2	skupina
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
potratech	potrat	k1gInPc6	potrat
a	a	k8xC	a
případně	případně	k6eAd1	případně
tyto	tento	k3xDgFnPc4	tento
interrupce	interrupce	k1gFnPc4	interrupce
zcela	zcela	k6eAd1	zcela
zakázat	zakázat	k5eAaPmF	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
diskriminace	diskriminace	k1gFnSc1	diskriminace
romské	romský	k2eAgFnSc2d1	romská
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
i	i	k8xC	i
maďarské	maďarský	k2eAgFnPc4d1	maďarská
menšiny	menšina	k1gFnPc4	menšina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
Romové	Rom	k1gMnPc1	Rom
ve	v	k7c4	v
školství	školství	k1gNnSc4	školství
často	často	k6eAd1	často
segregováni	segregován	k2eAgMnPc1d1	segregován
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
romských	romský	k2eAgNnPc2d1	romské
sídlišť	sídliště	k1gNnPc2	sídliště
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgFnP	stavit
zdi	zeď	k1gFnPc1	zeď
na	na	k7c4	na
oddělení	oddělení	k1gNnSc4	oddělení
neromské	romský	k2eNgFnSc2d1	neromská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
též	též	k9	též
k	k	k7c3	k
vynucené	vynucený	k2eAgFnSc3d1	vynucená
sterilizaci	sterilizace	k1gFnSc3	sterilizace
několika	několik	k4yIc2	několik
romských	romský	k2eAgFnPc2d1	romská
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Romská	romský	k2eAgFnSc1d1	romská
populace	populace	k1gFnSc1	populace
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc4d1	ostatní
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
maďarské	maďarský	k2eAgFnSc2d1	maďarská
menšiny	menšina	k1gFnSc2	menšina
nepřestává	přestávat	k5eNaImIp3nS	přestávat
být	být	k5eAaImF	být
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
maďarská	maďarský	k2eAgFnSc1d1	maďarská
vláda	vláda	k1gFnSc1	vláda
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
její	její	k3xOp3gInSc4	její
protektor	protektor	k1gInSc4	protektor
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
podporuje	podporovat	k5eAaImIp3nS	podporovat
agresivnější	agresivní	k2eAgFnSc3d2	agresivnější
a	a	k8xC	a
nacionalističtější	nacionalistický	k2eAgFnSc3d2	nacionalističtější
části	část	k1gFnSc3	část
maďarské	maďarský	k2eAgFnSc2d1	maďarská
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Národnostně	národnostně	k6eAd1	národnostně
laděné	laděný	k2eAgInPc4d1	laděný
konflikty	konflikt	k1gInPc4	konflikt
rozdmýchává	rozdmýchávat	k5eAaImIp3nS	rozdmýchávat
i	i	k9	i
část	část	k1gFnSc1	část
slovenských	slovenský	k2eAgFnPc2d1	slovenská
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
především	především	k9	především
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
SMER	SMER	kA	SMER
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ženám	žena	k1gFnPc3	žena
jsou	být	k5eAaImIp3nP	být
zaručována	zaručovat	k5eAaImNgFnS	zaručovat
jejich	jejich	k3xOp3gNnPc4	jejich
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
jen	jen	k9	jen
málo	málo	k4c1	málo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dostane	dostat	k5eAaPmIp3nS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
stupních	stupeň	k1gInPc6	stupeň
v	v	k7c6	v
podnikání	podnikání	k1gNnSc6	podnikání
a	a	k8xC	a
státní	státní	k2eAgFnSc3d1	státní
správě	správa	k1gFnSc3	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
práv	právo	k1gNnPc2	právo
gayů	gay	k1gMnPc2	gay
a	a	k8xC	a
leseb	lesba	k1gFnPc2	lesba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jistým	jistý	k2eAgNnSc7d1	jisté
zlepšením	zlepšení	k1gNnSc7	zlepšení
–	–	k?	–
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
slovenského	slovenský	k2eAgInSc2d1	slovenský
zákoníku	zákoník	k1gInSc2	zákoník
práce	práce	k1gFnSc2	práce
zakomponována	zakomponovat	k5eAaPmNgFnS	zakomponovat
i	i	k8xC	i
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
složka	složka	k1gFnSc1	složka
evropské	evropský	k2eAgFnSc2d1	Evropská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
specificky	specificky	k6eAd1	specificky
slovenský	slovenský	k2eAgInSc1d1	slovenský
způsob	způsob	k1gInSc1	způsob
přeměňování	přeměňování	k1gNnSc2	přeměňování
zla	zlo	k1gNnSc2	zlo
na	na	k7c4	na
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
ošklivého	ošklivý	k2eAgMnSc4d1	ošklivý
na	na	k7c4	na
krásu	krása	k1gFnSc4	krása
především	především	k6eAd1	především
Slováky	Slovák	k1gMnPc7	Slovák
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
středoevropskými	středoevropský	k2eAgFnPc7d1	středoevropská
národními	národní	k2eAgFnPc7d1	národní
kulturami	kultura	k1gFnPc7	kultura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
<g/>
,	,	kIx,	,
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
<g/>
,	,	kIx,	,
polskou	polský	k2eAgFnSc4d1	polská
<g/>
,	,	kIx,	,
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
kultuře	kultura	k1gFnSc6	kultura
má	mít	k5eAaImIp3nS	mít
židovská	židovský	k2eAgFnSc1d1	židovská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
zámků	zámek	k1gInPc2	zámek
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
jsou	být	k5eAaImIp3nP	být
hmotné	hmotný	k2eAgFnPc1d1	hmotná
i	i	k8xC	i
nehmotné	hmotný	k2eNgInPc1d1	nehmotný
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
tance	tanec	k1gInPc4	tanec
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
duchovní	duchovní	k2eAgInPc4d1	duchovní
výtvory	výtvor	k1gInPc4	výtvor
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
folklór	folklór	k1gInSc1	folklór
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
folklorním	folklorní	k2eAgFnPc3d1	folklorní
skupinám	skupina	k1gFnPc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
jako	jako	k8xS	jako
hrad	hrad	k1gInSc1	hrad
Devín	Devína	k1gFnPc2	Devína
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Martině	Martina	k1gFnSc6	Martina
<g/>
,	,	kIx,	,
Dóm	dóm	k1gInSc1	dóm
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
celého	celý	k2eAgNnSc2d1	celé
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
nesou	nést	k5eAaImIp3nP	nést
označení	označení	k1gNnSc4	označení
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
a	a	k8xC	a
stát	stát	k1gInSc1	stát
jim	on	k3xPp3gMnPc3	on
věnuje	věnovat	k5eAaImIp3nS	věnovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
Seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
a	a	k8xC	a
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
přijatého	přijatý	k2eAgInSc2d1	přijatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
seznamu	seznam	k1gInSc2	seznam
osm	osm	k4xCc4	osm
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
–	–	k?	–
historické	historický	k2eAgNnSc4d1	historické
město	město	k1gNnSc4	město
Banská	banský	k2eAgFnSc1d1	Banská
Štiavnica	Štiavnica	k1gFnSc1	Štiavnica
a	a	k8xC	a
technické	technický	k2eAgFnPc1d1	technická
památky	památka	k1gFnPc1	památka
jejího	její	k3xOp3gNnSc2	její
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
lidové	lidový	k2eAgFnSc2d1	lidová
architektury	architektura	k1gFnSc2	architektura
Vlkolínec	Vlkolínec	k1gInSc1	Vlkolínec
a	a	k8xC	a
Spišský	spišský	k2eAgInSc1d1	spišský
hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
památkami	památka	k1gFnPc7	památka
(	(	kIx(	(
<g/>
Spišské	spišský	k2eAgFnPc1d1	Spišská
Podhradie	Podhradie	k1gFnPc1	Podhradie
<g/>
,	,	kIx,	,
Spišská	spišský	k2eAgFnSc1d1	Spišská
Kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
Žehře	Žehra	k1gFnSc6	Žehra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
přidáno	přidán	k2eAgNnSc4d1	Přidáno
historické	historický	k2eAgNnSc4d1	historické
šarišské	šarišský	k2eAgNnSc4d1	Šarišské
město	město	k1gNnSc4	město
Bardejov	Bardejov	k1gInSc1	Bardejov
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
kostely	kostel	k1gInPc4	kostel
Karpatského	karpatský	k2eAgInSc2d1	karpatský
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Levoče	Levoča	k1gFnSc2	Levoča
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přírodními	přírodní	k2eAgFnPc7d1	přírodní
památkami	památka	k1gFnPc7	památka
v	v	k7c6	v
sezname	seznam	k1gInSc5	seznam
UNESCO	UNESCO	kA	UNESCO
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Slovensko	Slovensko	k1gNnSc4	Slovensko
jeskyně	jeskyně	k1gFnSc2	jeskyně
a	a	k8xC	a
propasti	propast	k1gFnSc2	propast
Slovenského	slovenský	k2eAgInSc2d1	slovenský
krasu	kras	k1gInSc2	kras
a	a	k8xC	a
Dobšinská	dobšinský	k2eAgFnSc1d1	Dobšinská
ledová	ledový	k2eAgFnSc1d1	ledová
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
přidány	přidat	k5eAaPmNgInP	přidat
i	i	k9	i
karpatské	karpatský	k2eAgInPc1d1	karpatský
pralesy	prales	k1gInPc1	prales
v	v	k7c6	v
Bukovských	Bukovských	k2eAgInPc6d1	Bukovských
a	a	k8xC	a
Vihorlatských	vihorlatský	k2eAgInPc6d1	vihorlatský
vrších	vrch	k1gInPc6	vrch
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Fujara	fujara	k1gFnSc1	fujara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nejtypičtějším	typický	k2eAgMnSc7d3	nejtypičtější
slovenským	slovenský	k2eAgInSc7d1	slovenský
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2005	[number]	k4	2005
do	do	k7c2	do
Seznamu	seznam	k1gInSc2	seznam
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
díl	díl	k1gInSc1	díl
ústního	ústní	k2eAgNnSc2d1	ústní
a	a	k8xC	a
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
slovenské	slovenský	k2eAgFnSc2d1	slovenská
literatury	literatura	k1gFnSc2	literatura
začínají	začínat	k5eAaImIp3nP	začínat
už	už	k9	už
příchodem	příchod	k1gInSc7	příchod
soluňských	soluňský	k2eAgMnPc2d1	soluňský
věrozvěstů	věrozvěst	k1gMnPc2	věrozvěst
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
roku	rok	k1gInSc2	rok
863	[number]	k4	863
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
totiž	totiž	k9	totiž
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
sestavili	sestavit	k5eAaPmAgMnP	sestavit
slovanské	slovanský	k2eAgNnSc4d1	slovanské
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ve	v	k7c6	v
Velkomoravské	velkomoravský	k2eAgFnSc6d1	Velkomoravská
říši	říš	k1gFnSc6	říš
zavedli	zavést	k5eAaPmAgMnP	zavést
mj.	mj.	kA	mj.
jazyk	jazyk	k1gInSc4	jazyk
kulturní	kulturní	k2eAgInSc1d1	kulturní
<g/>
,	,	kIx,	,
spisovný	spisovný	k2eAgInSc1d1	spisovný
<g/>
,	,	kIx,	,
liturgický	liturgický	k2eAgInSc1d1	liturgický
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
státní	státní	k2eAgFnSc2d1	státní
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
tak	tak	k6eAd1	tak
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
literární	literární	k2eAgFnSc2d1	literární
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
sami	sám	k3xTgMnPc1	sám
přispěli	přispět	k5eAaPmAgMnP	přispět
několika	několik	k4yIc7	několik
psanými	psaný	k2eAgNnPc7d1	psané
díly	dílo	k1gNnPc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
překladů	překlad	k1gInPc2	překlad
evangelií	evangelium	k1gNnPc2	evangelium
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
náboženských	náboženský	k2eAgInPc2d1	náboženský
textů	text	k1gInPc2	text
napsali	napsat	k5eAaPmAgMnP	napsat
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
několik	několik	k4yIc4	několik
vlastních	vlastní	k2eAgFnPc2d1	vlastní
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
pojednání	pojednání	k1gNnSc1	pojednání
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
Konstantinův	Konstantinův	k2eAgInSc4d1	Konstantinův
úvod	úvod	k1gInSc4	úvod
k	k	k7c3	k
evangeliu	evangelium	k1gNnSc3	evangelium
(	(	kIx(	(
<g/>
Proglas	Proglas	k1gInSc4	Proglas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
slovanským	slovanský	k2eAgMnSc7d1	slovanský
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
žák	žák	k1gMnSc1	žák
bratrů	bratr	k1gMnPc2	bratr
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
důležitých	důležitý	k2eAgMnPc2d1	důležitý
žáků	žák	k1gMnPc2	žák
Gorazd	Gorazdo	k1gNnPc2	Gorazdo
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
,	,	kIx,	,
u	u	k7c2	u
Nitry	Nitra	k1gFnSc2	Nitra
<g/>
.	.	kIx.	.
</s>
<s>
Zánik	zánik	k1gInSc1	zánik
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
Slováky	Slovák	k1gMnPc4	Slovák
znamenala	znamenat	k5eAaImAgFnS	znamenat
tisíciletý	tisíciletý	k2eAgInSc4d1	tisíciletý
útlak	útlak	k1gInSc4	útlak
pod	pod	k7c7	pod
uherským	uherský	k2eAgInSc7d1	uherský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
liturgie	liturgie	k1gFnSc2	liturgie
došlo	dojít	k5eAaPmAgNnS	dojít
již	již	k6eAd1	již
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
literatura	literatura	k1gFnSc1	literatura
tak	tak	k6eAd1	tak
začala	začít	k5eAaPmAgFnS	začít
skoro	skoro	k6eAd1	skoro
zanikat	zanikat	k5eAaImF	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
obnovu	obnova	k1gFnSc4	obnova
zažila	zažít	k5eAaPmAgFnS	zažít
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
<g/>
,	,	kIx,	,
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
oživila	oživit	k5eAaPmAgFnS	oživit
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
funguje	fungovat	k5eAaImIp3nS	fungovat
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Uherska	Uhersko	k1gNnSc2	Uhersko
nastává	nastávat	k5eAaImIp3nS	nastávat
pro	pro	k7c4	pro
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
literaturu	literatura	k1gFnSc4	literatura
období	období	k1gNnSc2	období
zvané	zvaný	k2eAgFnSc2d1	zvaná
slovenská	slovenský	k2eAgFnSc1d1	slovenská
moderna	moderna	k1gFnSc1	moderna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
reálného	reálný	k2eAgInSc2d1	reálný
sociologického	sociologický	k2eAgInSc2d1	sociologický
a	a	k8xC	a
psychologického	psychologický	k2eAgInSc2d1	psychologický
rozměru	rozměr	k1gInSc2	rozměr
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
slovenské	slovenský	k2eAgMnPc4d1	slovenský
autory	autor	k1gMnPc4	autor
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Ján	Ján	k1gMnSc1	Ján
Kollár	Kollár	k1gMnSc1	Kollár
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafárik	Šafárik	k1gMnSc1	Šafárik
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Švantner	Švantner	k1gMnSc1	Švantner
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Karvaš	Karvaš	k1gMnSc1	Karvaš
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Fabry	Fabra	k1gFnSc2	Fabra
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Sloboda	sloboda	k1gFnSc1	sloboda
<g/>
,	,	kIx,	,
Ľudovít	Ľudovít	k1gMnSc1	Ľudovít
Štúr	Štúr	k1gMnSc1	Štúr
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Kalinčiak	Kalinčiak	k1gMnSc1	Kalinčiak
<g/>
,	,	kIx,	,
Samo	sám	k3xTgNnSc4	sám
Chalupka	Chalupka	k1gMnSc1	Chalupka
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
známé	známý	k2eAgFnSc2d1	známá
básně	báseň	k1gFnSc2	báseň
Mor	mora	k1gFnPc2	mora
ho	on	k3xPp3gMnSc4	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Sládkovič	Sládkovič	k1gMnSc1	Sládkovič
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
Kráľ	Kráľ	k1gMnSc1	Kráľ
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Botto	Botto	k1gNnSc1	Botto
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
Matúška	Matúšek	k1gMnSc2	Matúšek
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
textu	text	k1gInSc2	text
slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
hymny	hymna	k1gFnSc2	hymna
Nad	nad	k7c7	nad
Tatrou	Tatra	k1gFnSc7	Tatra
sa	sa	k?	sa
blýska	blýska	k1gFnSc1	blýska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Miloslav	Miloslav	k1gMnSc1	Miloslav
Hodža	Hodža	k1gMnSc1	Hodža
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Miloslav	Miloslav	k1gMnSc1	Miloslav
Hurban	Hurban	k1gMnSc1	Hurban
nebo	nebo	k8xC	nebo
Pavol	Pavol	k1gInSc1	Pavol
Országh	Országh	k1gMnSc1	Országh
Hviezdoslav	Hviezdoslav	k1gMnSc1	Hviezdoslav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
prozaik	prozaik	k1gMnSc1	prozaik
Ladislav	Ladislav	k1gMnSc1	Ladislav
Mňačko	Mňačko	k6eAd1	Mňačko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
například	například	k6eAd1	například
Milan	Milan	k1gMnSc1	Milan
Rúfus	Rúfus	k1gMnSc1	Rúfus
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
sahá	sahat	k5eAaImIp3nS	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
uměleckým	umělecký	k2eAgInSc7d1	umělecký
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Venuše	Venuše	k1gFnSc2	Venuše
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
neolitických	neolitický	k2eAgMnPc2d1	neolitický
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nalezené	nalezený	k2eAgInPc4d1	nalezený
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
slovenské	slovenský	k2eAgNnSc1d1	slovenské
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
stovky	stovka	k1gFnPc1	stovka
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
absolventů	absolvent	k1gMnPc2	absolvent
bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
Vysokej	Vysokej	k?	Vysokej
školy	škola	k1gFnSc2	škola
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umení	umení	k1gNnPc2	umení
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
dění	dění	k1gNnSc1	dění
organizuje	organizovat	k5eAaBmIp3nS	organizovat
hlavně	hlavně	k9	hlavně
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
únia	únia	k1gFnSc1	únia
a	a	k8xC	a
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
i	i	k9	i
Spolek	spolek	k1gInSc1	spolek
výtvarníkov	výtvarníkov	k1gInSc1	výtvarníkov
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Umelecká	Umelecký	k2eAgFnSc1d1	Umelecký
beseda	beseda	k1gFnSc1	beseda
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
uměleckých	umělecký	k2eAgInPc2d1	umělecký
spolků	spolek	k1gInPc2	spolek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odborné	odborný	k2eAgFnSc6d1	odborná
stránce	stránka	k1gFnSc6	stránka
mapují	mapovat	k5eAaImIp3nP	mapovat
současnou	současný	k2eAgFnSc4d1	současná
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
situaci	situace	k1gFnSc4	situace
časopisy	časopis	k1gInPc1	časopis
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
Výtvarnicke	Výtvarnicke	k1gInSc1	Výtvarnicke
noviny	novina	k1gFnSc2	novina
a	a	k8xC	a
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
desítky	desítka	k1gFnPc1	desítka
katalogů	katalog	k1gInPc2	katalog
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
sympozií	sympozion	k1gNnPc2	sympozion
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc4	možnost
prezentace	prezentace	k1gFnSc2	prezentace
současného	současný	k2eAgNnSc2d1	současné
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
síť	síť	k1gFnSc4	síť
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
výstavních	výstavní	k2eAgFnPc2d1	výstavní
síní	síň	k1gFnPc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
obraz	obraz	k1gInSc1	obraz
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
pestrý	pestrý	k2eAgInSc1d1	pestrý
<g/>
.	.	kIx.	.
</s>
<s>
Prolíná	prolínat	k5eAaImIp3nS	prolínat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
úsilí	úsilí	k1gNnSc2	úsilí
několika	několik	k4yIc2	několik
generačních	generační	k2eAgFnPc2d1	generační
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Kvality	kvalita	k1gFnPc1	kvalita
závěsného	závěsný	k2eAgNnSc2d1	závěsné
malířství	malířství	k1gNnSc2	malířství
tak	tak	k9	tak
nadále	nadále	k6eAd1	nadále
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
především	především	k9	především
příslušníci	příslušník	k1gMnPc1	příslušník
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mladší	mladý	k2eAgFnSc1d2	mladší
generace	generace	k1gFnSc1	generace
hledá	hledat	k5eAaImIp3nS	hledat
nové	nový	k2eAgFnPc4d1	nová
možnosti	možnost	k1gFnPc4	možnost
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
výtvarným	výtvarný	k2eAgMnSc7d1	výtvarný
umělcem	umělec	k1gMnSc7	umělec
narozeným	narozený	k2eAgMnSc7d1	narozený
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byl	být	k5eAaImAgMnS	být
bezpochyby	bezpochyby	k9	bezpochyby
zakladatel	zakladatel	k1gMnSc1	zakladatel
pop	pop	k1gMnSc1	pop
artu	arta	k1gFnSc4	arta
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hudby	hudba	k1gFnSc2	hudba
představuje	představovat	k5eAaImIp3nS	představovat
tisíciletý	tisíciletý	k2eAgInSc1d1	tisíciletý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
bezprostředně	bezprostředně	k6eAd1	bezprostředně
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
dějinnými	dějinný	k2eAgFnPc7d1	dějinná
událostmi	událost	k1gFnPc7	událost
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
kulturně-společenským	kulturněpolečenský	k2eAgInSc7d1	kulturně-společenský
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
lze	lze	k6eAd1	lze
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
hudební	hudební	k2eAgFnSc4d1	hudební
tvorbu	tvorba	k1gFnSc4	tvorba
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
obecně	obecně	k6eAd1	obecně
uznávaným	uznávaný	k2eAgFnPc3d1	uznávaná
dějinným	dějinný	k2eAgFnPc3d1	dějinná
epochám	epocha	k1gFnPc3	epocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
stál	stát	k5eAaImAgInS	stát
před	před	k7c7	před
hudebními	hudební	k2eAgMnPc7d1	hudební
umělci	umělec	k1gMnPc7	umělec
Slovenska	Slovensko	k1gNnSc2	Slovensko
náročný	náročný	k2eAgInSc4d1	náročný
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
překonat	překonat	k5eAaPmF	překonat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
zaostalost	zaostalost	k1gFnSc4	zaostalost
země	zem	k1gFnSc2	zem
i	i	k9	i
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
hudebního	hudební	k2eAgNnSc2d1	hudební
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vytvoření	vytvoření	k1gNnSc3	vytvoření
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
avšak	avšak	k8xC	avšak
formou	forma	k1gFnSc7	forma
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Slováci	Slovák	k1gMnPc1	Slovák
vydali	vydat	k5eAaPmAgMnP	vydat
cestou	cestou	k7c2	cestou
profesionalizace	profesionalizace	k1gFnSc2	profesionalizace
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zakládat	zakládat	k5eAaImF	zakládat
hudebně-kulturní	hudebněulturní	k2eAgFnPc4d1	hudebně-kulturní
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Vznikající	vznikající	k2eAgInSc1d1	vznikající
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
hudební	hudební	k2eAgInSc1d1	hudební
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
však	však	k9	však
neomezoval	omezovat	k5eNaImAgMnS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
metropoli	metropole	k1gFnSc4	metropole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
a	a	k8xC	a
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
vznikají	vznikat	k5eAaImIp3nP	vznikat
hudební	hudební	k2eAgFnPc1d1	hudební
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
vysláni	vyslat	k5eAaPmNgMnP	vyslat
učit	učit	k5eAaImF	učit
studenti	student	k1gMnPc1	student
bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
místní	místní	k2eAgMnPc1d1	místní
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
mnoha	mnoho	k4c2	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
hudebních	hudební	k2eAgFnPc2d1	hudební
institucí	instituce	k1gFnPc2	instituce
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
Slovensko	Slovensko	k1gNnSc1	Slovensko
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
nejenom	nejenom	k6eAd1	nejenom
vlastní	vlastní	k2eAgFnSc7d1	vlastní
operou	opera	k1gFnSc7	opera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svým	svůj	k3xOyFgNnSc7	svůj
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
konzervatoří	konzervatoř	k1gFnSc7	konzervatoř
<g/>
,	,	kIx,	,
orchestry	orchestr	k1gInPc7	orchestr
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
představitelem	představitel	k1gMnSc7	představitel
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hudební	hudební	k2eAgFnSc2d1	hudební
moderny	moderna	k1gFnSc2	moderna
byl	být	k5eAaImAgMnS	být
skladatel	skladatel	k1gMnSc1	skladatel
Alexander	Alexandra	k1gFnPc2	Alexandra
Moyzes	Moyzes	k1gMnSc1	Moyzes
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
mladším	mladý	k2eAgMnSc7d2	mladší
současníkem	současník	k1gMnSc7	současník
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Suchoň	Suchoň	k1gMnSc1	Suchoň
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Ján	Ján	k1gMnSc1	Ján
Cikker	Cikker	k1gMnSc1	Cikker
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Edita	Edita	k1gFnSc1	Edita
Gruberová	Gruberová	k1gFnSc1	Gruberová
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
Slovenska	Slovensko	k1gNnSc2	Slovensko
–	–	k?	–
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národné	národný	k2eAgNnSc1d1	národné
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
sehrávali	sehrávat	k5eAaImAgMnP	sehrávat
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
divadelníci	divadelník	k1gMnPc1	divadelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
existence	existence	k1gFnSc2	existence
Slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
<g/>
,	,	kIx,	,
odrážela	odrážet	k5eAaImAgFnS	odrážet
složitou	složitý	k2eAgFnSc4d1	složitá
společenskou	společenský	k2eAgFnSc4d1	společenská
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
činohra	činohra	k1gFnSc1	činohra
<g/>
.	.	kIx.	.
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
tvorba	tvorba	k1gFnSc1	tvorba
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
však	však	k9	však
zjevně	zjevně	k6eAd1	zjevně
ještě	ještě	k6eAd1	ještě
vyzrávala	vyzrávat	k5eAaImAgFnS	vyzrávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
i	i	k9	i
sféra	sféra	k1gFnSc1	sféra
nezávislých	závislý	k2eNgInPc2d1	nezávislý
divadelních	divadelní	k2eAgInPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
divadelní	divadelní	k2eAgInPc4d1	divadelní
festivaly	festival	k1gInPc4	festival
nezůstávají	zůstávat	k5eNaImIp3nP	zůstávat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
stranou	stranou	k6eAd1	stranou
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
Nitra	Nitra	k1gFnSc1	Nitra
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
účastí	účast	k1gFnSc7	účast
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
loutkařský	loutkařský	k2eAgInSc4d1	loutkařský
festival	festival	k1gInSc4	festival
Bábkarská	Bábkarský	k2eAgFnSc1d1	Bábkarská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
pohybe	pohyb	k1gInSc5	pohyb
každoročně	každoročně	k6eAd1	každoročně
organizuje	organizovat	k5eAaBmIp3nS	organizovat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
taneční	taneční	k2eAgInSc1d1	taneční
festival	festival	k1gInSc1	festival
současného	současný	k2eAgInSc2d1	současný
hudebního	hudební	k2eAgInSc2d1	hudební
tance	tanec	k1gInSc2	tanec
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
pohybe	pohyb	k1gInSc5	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
<g/>
,	,	kIx,	,
dokumentačního	dokumentační	k2eAgInSc2d1	dokumentační
<g/>
,	,	kIx,	,
informačního	informační	k2eAgInSc2d1	informační
<g/>
,	,	kIx,	,
propagačního	propagační	k2eAgMnSc2d1	propagační
a	a	k8xC	a
analytického	analytický	k2eAgNnSc2d1	analytické
střediska	středisko	k1gNnSc2	středisko
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Divadelný	Divadelný	k2eAgInSc1d1	Divadelný
ústav	ústav	k1gInSc1	ústav
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
základním	základní	k2eAgNnSc7d1	základní
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
cílevědomě	cílevědomě	k6eAd1	cílevědomě
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
hmotné	hmotný	k2eAgInPc4d1	hmotný
dokumenty	dokument	k1gInPc4	dokument
divadelní	divadelní	k2eAgFnSc2d1	divadelní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
kinematografie	kinematografie	k1gFnSc1	kinematografie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
mladší	mladý	k2eAgMnPc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
část	část	k1gFnSc1	část
místní	místní	k2eAgFnSc2d1	místní
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
až	až	k9	až
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
propagandistickým	propagandistický	k2eAgInSc7d1	propagandistický
nástrojem	nástroj	k1gInSc7	nástroj
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
izolovaném	izolovaný	k2eAgNnSc6d1	izolované
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
prostředí	prostředí	k1gNnSc6	prostředí
tak	tak	k6eAd1	tak
vznikaly	vznikat	k5eAaImAgInP	vznikat
převážně	převážně	k6eAd1	převážně
filmy	film	k1gInPc1	film
s	s	k7c7	s
budovatelskými	budovatelský	k2eAgInPc7d1	budovatelský
náměty	námět	k1gInPc7	námět
a	a	k8xC	a
výpovědí	výpověď	k1gFnSc7	výpověď
ze	z	k7c2	z
života	život	k1gInSc2	život
dělnického	dělnický	k2eAgInSc2d1	dělnický
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
slovenský	slovenský	k2eAgInSc4d1	slovenský
film	film	k1gInSc4	film
natočil	natočit	k5eAaBmAgMnS	natočit
Eduard	Eduard	k1gMnSc1	Eduard
Schreiber	Schreiber	k1gMnSc1	Schreiber
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
,	,	kIx,	,
nakládání	nakládání	k1gNnSc1	nakládání
dobytka	dobytek	k1gInSc2	dobytek
na	na	k7c4	na
vagony	vagon	k1gInPc4	vagon
<g/>
,	,	kIx,	,
modernizace	modernizace	k1gFnSc1	modernizace
pily	pila	k1gFnSc2	pila
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc4d1	nový
automobil	automobil	k1gInSc4	automobil
apod.	apod.	kA	apod.
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
Schreiberovy	Schreiberův	k2eAgFnSc2d1	Schreiberova
filmařské	filmařský	k2eAgFnSc2d1	filmařská
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hraná	hraný	k2eAgFnSc1d1	hraná
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
anekdota	anekdota	k1gFnSc1	anekdota
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Únos	únos	k1gInSc1	únos
<g/>
.	.	kIx.	.
</s>
<s>
Ján	Ján	k1gMnSc1	Ján
Kadár	Kadár	k1gMnSc1	Kadár
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgMnSc7d1	jediný
Slovákem	Slovák	k1gMnSc7	Slovák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tzv.	tzv.	kA	tzv.
československé	československý	k2eAgFnSc6d1	Československá
nové	nový	k2eAgFnSc6d1	nová
vlně	vlna	k1gFnSc6	vlna
patřil	patřit	k5eAaImAgMnS	patřit
Štefan	Štefan	k1gMnSc1	Štefan
Uher	Uher	k1gMnSc1	Uher
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgInSc4d1	originální
vizuální	vizuální	k2eAgInSc4d1	vizuální
výraz	výraz	k1gInSc4	výraz
našel	najít	k5eAaPmAgInS	najít
Juraj	Juraj	k1gInSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc1	Jakubisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Ivan	Ivan	k1gMnSc1	Ivan
Reitman	Reitman	k1gMnSc1	Reitman
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
mnoha	mnoho	k4c2	mnoho
populárních	populární	k2eAgFnPc2d1	populární
komedií	komedie	k1gFnPc2	komedie
(	(	kIx(	(
<g/>
Krotitelé	krotitel	k1gMnPc1	krotitel
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
Dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
,	,	kIx,	,
Policajt	Policajt	k?	Policajt
ze	z	k7c2	z
školky	školka	k1gFnSc2	školka
<g/>
,	,	kIx,	,
Junior	junior	k1gMnSc1	junior
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
V	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Adriana	Adriana	k1gFnSc1	Adriana
Karembeu	Karembeus	k1gInSc2	Karembeus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
podnebném	podnebný	k2eAgNnSc6d1	podnebné
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vykonávat	vykonávat	k5eAaImF	vykonávat
sporty	sport	k1gInPc4	sport
s	s	k7c7	s
letním	letní	k2eAgNnSc7d1	letní
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	s	k7c7	s
zimním	zimní	k2eAgNnSc7d1	zimní
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
populární	populární	k2eAgInPc4d1	populární
sporty	sport	k1gInPc4	sport
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
slalom	slalom	k1gInSc1	slalom
<g/>
,	,	kIx,	,
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
,	,	kIx,	,
cyklistika	cyklistika	k1gFnSc1	cyklistika
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
širší	široký	k2eAgFnSc3d2	širší
evropské	evropský	k2eAgFnSc3d1	Evropská
špičce	špička	k1gFnSc3	špička
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
fotbal	fotbal	k1gInSc1	fotbal
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
a	a	k8xC	a
probojovala	probojovat	k5eAaPmAgFnS	probojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
šestnáctky	šestnáctka	k1gFnSc2	šestnáctka
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
svaz	svaz	k1gInSc1	svaz
řídí	řídit	k5eAaImIp3nS	řídit
slovenský	slovenský	k2eAgInSc1d1	slovenský
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
záštitou	záštita	k1gFnSc7	záštita
se	se	k3xPyFc4	se
organizují	organizovat	k5eAaBmIp3nP	organizovat
oficiální	oficiální	k2eAgFnSc2d1	oficiální
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
např.	např.	kA	např.
Corgoň	Corgoň	k1gFnSc1	Corgoň
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
slovenská	slovenský	k2eAgFnSc1d1	slovenská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
hokejová	hokejový	k2eAgFnSc1d1	hokejová
reprezentace	reprezentace	k1gFnSc1	reprezentace
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
více	hodně	k6eAd2	hodně
úspěchů	úspěch	k1gInPc2	úspěch
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
medaile	medaile	k1gFnPc4	medaile
na	na	k7c6	na
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
či	či	k8xC	či
bronz	bronz	k1gInSc4	bronz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
na	na	k7c4	na
OH	OH	kA	OH
2010	[number]	k4	2010
a	a	k8xC	a
stříbro	stříbro	k1gNnSc1	stříbro
na	na	k7c6	na
MS	MS	kA	MS
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
svaz	svaz	k1gInSc1	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
řídící	řídící	k2eAgInSc1d1	řídící
orgán	orgán	k1gInSc1	orgán
slovenského	slovenský	k2eAgInSc2d1	slovenský
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
mužských	mužský	k2eAgFnPc2d1	mužská
hokejových	hokejový	k2eAgFnPc2d1	hokejová
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Slovnaft	Slovnaftum	k1gNnPc2	Slovnaftum
Extraligu	extraliga	k1gFnSc4	extraliga
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
je	být	k5eAaImIp3nS	být
i	i	k9	i
ženský	ženský	k2eAgInSc4d1	ženský
hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
lize	liga	k1gFnSc6	liga
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
juniorský	juniorský	k2eAgInSc4d1	juniorský
a	a	k8xC	a
dorostenecký	dorostenecký	k2eAgInSc4d1	dorostenecký
hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
ligách	liga	k1gFnPc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
Košicích	Košice	k1gInPc6	Košice
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
domácí	domácí	k2eAgMnPc1d1	domácí
Slováci	Slovák	k1gMnPc1	Slovák
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
kanadského	kanadský	k2eAgMnSc2d1	kanadský
kouče	kouč	k1gMnSc2	kouč
Glena	Glen	k1gMnSc2	Glen
Hanlona	Hanlon	k1gMnSc2	Hanlon
neuspěli	uspět	k5eNaPmAgMnP	uspět
<g/>
,	,	kIx,	,
skončili	skončit	k5eAaPmAgMnP	skončit
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
sporty	sport	k1gInPc4	sport
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
klub	klub	k1gInSc1	klub
je	být	k5eAaImIp3nS	být
HT	HT	kA	HT
Tatran	Tatran	k1gInSc1	Tatran
Prešov	Prešov	k1gInSc1	Prešov
<g/>
,	,	kIx,	,
a	a	k8xC	a
reprezentace	reprezentace	k1gFnSc1	reprezentace
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
3	[number]	k4	3
světových	světový	k2eAgMnPc2d1	světový
a	a	k8xC	a
2	[number]	k4	2
evropských	evropský	k2eAgInPc6d1	evropský
šampionátech	šampionát	k1gInPc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
Československa	Československo	k1gNnSc2	Československo
uspěla	uspět	k5eAaPmAgFnS	uspět
řada	řada	k1gFnSc1	řada
slovenských	slovenský	k2eAgMnPc2d1	slovenský
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Vicemistry	vicemistr	k1gMnPc7	vicemistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Štefan	Štefan	k1gMnSc1	Štefan
Čambal	Čambal	k1gInSc4	Čambal
<g/>
,	,	kIx,	,
Viliam	Viliam	k1gMnSc1	Viliam
Schrojf	Schrojf	k1gMnSc1	Schrojf
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Popluhár	Popluhár	k1gMnSc1	Popluhár
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Štibrányi	Štibrány	k1gFnSc2	Štibrány
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Scherer	Scherer	k1gMnSc1	Scherer
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Titus	Titus	k1gMnSc1	Titus
Buberník	Buberník	k1gMnSc1	Buberník
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Kvašňák	Kvašňák	k1gMnSc1	Kvašňák
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Mistry	mistr	k1gMnPc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
Karol	Karol	k1gInSc1	Karol
Dobiaš	Dobiaš	k1gInSc1	Dobiaš
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Čapkovič	Čapkovič	k1gMnSc1	Čapkovič
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Ondruš	Ondruš	k1gMnSc1	Ondruš
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Pivarník	Pivarník	k1gMnSc1	Pivarník
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Jurkemik	Jurkemik	k1gMnSc1	Jurkemik
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Móder	Móder	k1gMnSc1	Móder
<g/>
,	,	kIx,	,
Marián	Marián	k1gMnSc1	Marián
Masný	masný	k2eAgMnSc1d1	masný
<g/>
,	,	kIx,	,
Koloman	Koloman	k1gMnSc1	Koloman
Gögh	Gögh	k1gMnSc1	Gögh
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Barmoš	Barmoš	k1gMnSc1	Barmoš
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc4	Pavol
Biroš	Biroš	k1gMnSc1	Biroš
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Švehlík	Švehlík	k1gMnSc1	Švehlík
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
Galis	Galis	k1gFnSc2	Galis
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Petráš	Petráš	k1gMnSc1	Petráš
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Vencel	Vencel	k1gFnSc2	Vencel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oba	dva	k4xCgMnPc1	dva
trenéři	trenér	k1gMnPc1	trenér
mistrů	mistr	k1gMnPc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Ježek	Ježek	k1gMnSc1	Ježek
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Vengloš	Vengloš	k1gMnSc1	Vengloš
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Slováci	Slovák	k1gMnPc1	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgMnPc4d1	slovenský
rodiče	rodič	k1gMnPc4	rodič
měl	mít	k5eAaImAgMnS	mít
také	také	k6eAd1	také
slavný	slavný	k2eAgMnSc1d1	slavný
hráč	hráč	k1gMnSc1	hráč
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kubala	Kubala	k1gMnSc1	Kubala
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
získali	získat	k5eAaPmAgMnP	získat
boxeři	boxer	k1gMnPc1	boxer
Július	Július	k1gMnSc1	Július
Torma	Torma	k1gFnSc1	Torma
a	a	k8xC	a
Ján	Ján	k1gMnSc1	Ján
Zachara	Zachara	k1gFnSc1	Zachara
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Anton	Anton	k1gMnSc1	Anton
Tkáč	tkáč	k1gMnSc1	tkáč
<g/>
,	,	kIx,	,
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
Ondrej	Ondrej	k1gMnSc1	Ondrej
Nepela	Nepela	k1gFnSc1	Nepela
<g/>
,	,	kIx,	,
chodec	chodec	k1gMnSc1	chodec
Jozef	Jozef	k1gMnSc1	Jozef
Pribilinec	Pribilinec	k1gMnSc1	Pribilinec
a	a	k8xC	a
tenista	tenista	k1gMnSc1	tenista
Miloslav	Miloslav	k1gMnSc1	Miloslav
Mečíř	mečíř	k1gMnSc1	mečíř
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivní	kolektivní	k2eAgInPc4d1	kolektivní
zlaté	zlatý	k1gInPc4	zlatý
mají	mít	k5eAaImIp3nP	mít
fotbalisté	fotbalista	k1gMnPc1	fotbalista
František	František	k1gMnSc1	František
Kunzo	Kunza	k1gFnSc5	Kunza
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Seman	Seman	k1gMnSc1	Seman
a	a	k8xC	a
veslař	veslař	k1gMnSc1	veslař
Pavel	Pavel	k1gMnSc1	Pavel
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
s	s	k7c7	s
československou	československý	k2eAgFnSc7d1	Československá
reprezentací	reprezentace	k1gFnSc7	reprezentace
stal	stát	k5eAaPmAgMnS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dzurilla	Dzurilla	k1gMnSc1	Dzurilla
<g/>
,	,	kIx,	,
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
Vincent	Vincent	k1gMnSc1	Vincent
Lukáč	Lukáč	k1gMnSc1	Lukáč
<g/>
,	,	kIx,	,
Marián	Marián	k1gMnSc1	Marián
Šťastný	Šťastný	k1gMnSc1	Šťastný
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
titul	titul	k1gInSc4	titul
získali	získat	k5eAaPmAgMnP	získat
Igor	Igor	k1gMnSc1	Igor
Liba	Liba	k1gFnSc1	Liba
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
Pašek	Pašek	k1gMnSc1	Pašek
<g/>
,	,	kIx,	,
Dárius	Dárius	k1gMnSc1	Dárius
Rusnák	Rusnák	k1gMnSc1	Rusnák
<g/>
,	,	kIx,	,
Jerguš	Jerguš	k1gMnSc1	Jerguš
Bača	bača	k1gMnSc1	bača
<g/>
,	,	kIx,	,
Július	Július	k1gMnSc1	Július
Haas	Haas	k1gInSc1	Haas
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Tajcnár	Tajcnár	k1gMnSc1	Tajcnár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
Stan	stan	k1gInSc1	stan
Mikita	Mikitum	k1gNnSc2	Mikitum
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
diskař	diskař	k1gMnSc1	diskař
Imrich	Imrich	k1gMnSc1	Imrich
Bugár	Bugár	k1gMnSc1	Bugár
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
šachistům	šachista	k1gMnPc3	šachista
narozeným	narozený	k2eAgMnPc3d1	narozený
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
patřil	patřit	k5eAaImAgMnS	patřit
Richard	Richard	k1gMnSc1	Richard
Réti	Rét	k1gFnSc2	Rét
či	či	k8xC	či
Ignaz	Ignaz	k1gInSc4	Ignaz
Kolisch	Kolischa	k1gFnPc2	Kolischa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
z	z	k7c2	z
fotbalistů	fotbalista	k1gMnPc2	fotbalista
nejvíce	nejvíce	k6eAd1	nejvíce
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Peter	Peter	k1gMnSc1	Peter
Dubovský	Dubovský	k1gMnSc1	Dubovský
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Hamšík	Hamšík	k1gMnSc1	Hamšík
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Škrtel	Škrtel	k1gMnSc1	Škrtel
a	a	k8xC	a
Róbert	Róbert	k1gMnSc1	Róbert
Vittek	Vittek	k1gMnSc1	Vittek
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
mj.	mj.	kA	mj.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šatan	Šatan	k1gMnSc1	Šatan
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Bondra	Bondra	k1gFnSc1	Bondra
<g/>
,	,	kIx,	,
Žigmund	Žigmund	k1gMnSc1	Žigmund
Pálffy	Pálff	k1gInPc4	Pálff
či	či	k8xC	či
Ján	Ján	k1gMnSc1	Ján
Lašák	Lašák	k1gMnSc1	Lašák
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
stříbra	stříbro	k1gNnPc4	stříbro
má	mít	k5eAaImIp3nS	mít
Zdeno	Zdena	k1gFnSc5	Zdena
Chára	Chár	k1gInSc2	Chár
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
vítězem	vítěz	k1gMnSc7	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
je	být	k5eAaImIp3nS	být
Marián	Marián	k1gMnSc1	Marián
Hossa	Hoss	k1gMnSc2	Hoss
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
mají	mít	k5eAaImIp3nP	mít
deblkanoisté	deblkanoista	k1gMnPc1	deblkanoista
Pavol	Pavol	k1gInSc4	Pavol
Hochschorner	Hochschorner	k1gMnSc1	Hochschorner
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Hochschorner	Hochschorner	k1gMnSc1	Hochschorner
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
jejich	jejich	k3xOp3gNnSc4	jejich
kolegové	kolega	k1gMnPc1	kolega
Michal	Michal	k1gMnSc1	Michal
Martikán	Martikán	k1gMnSc1	Martikán
a	a	k8xC	a
Elena	Elena	k1gFnSc1	Elena
Kaliská	Kaliská	k1gFnSc1	Kaliská
a	a	k8xC	a
také	také	k9	také
Ladislav	Ladislav	k1gMnSc1	Ladislav
Škantár	Škantár	k1gMnSc1	Škantár
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Škantár	Škantár	k1gMnSc1	Škantár
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
slalomářské	slalomářský	k2eAgNnSc4d1	slalomářský
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
ho	on	k3xPp3gMnSc2	on
získal	získat	k5eAaPmAgInS	získat
Matej	Matej	k1gInSc4	Matej
Tóth	Tótha	k1gFnPc2	Tótha
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
též	též	k9	též
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
Anastasia	Anastasia	k1gFnSc1	Anastasia
Kuzminová	Kuzminová	k1gFnSc1	Kuzminová
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
<g/>
.	.	kIx.	.
</s>
