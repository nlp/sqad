<p>
<s>
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
v	v	k7c6	v
siswati	siswat	k1gMnPc1	siswat
eSwatini	eSwatin	k2eAgMnPc1d1	eSwatin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
diarchií	diarchie	k1gFnPc2	diarchie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Mswati	Mswat	k1gMnPc1	Mswat
III	III	kA	III
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
údajně	údajně	k6eAd1	údajně
osobní	osobní	k2eAgInSc4d1	osobní
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
kolem	kolem	k7c2	kolem
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
asi	asi	k9	asi
čtyři	čtyři	k4xCgFnPc4	čtyři
miliardy	miliarda	k4xCgFnPc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
70	[number]	k4	70
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
OSN	OSN	kA	OSN
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
bídy	bída	k1gFnSc2	bída
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
rozšířenosti	rozšířenost	k1gFnSc3	rozšířenost
infekce	infekce	k1gFnSc2	infekce
HIV	HIV	kA	HIV
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
činí	činit	k5eAaImIp3nS	činit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pouhých	pouhý	k2eAgInPc2d1	pouhý
49,42	[number]	k4	49,42
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
svazijský	svazijský	k2eAgInSc1d1	svazijský
stát	stát	k1gInSc1	stát
spojením	spojení	k1gNnSc7	spojení
několika	několik	k4yIc2	několik
bantuských	bantuský	k2eAgInPc2d1	bantuský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Búrové	Búr	k1gMnPc1	Búr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
anektováno	anektovat	k5eAaBmNgNnS	anektovat
Transvaalem	Transvaal	k1gInSc7	Transvaal
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
farem	farma	k1gFnPc2	farma
<g/>
,	,	kIx,	,
lesů	les	k1gInPc2	les
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
vznikal	vznikat	k5eAaImAgInS	vznikat
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
britští	britský	k2eAgMnPc1d1	britský
a	a	k8xC	a
nizozemští	nizozemský	k2eAgMnPc1d1	nizozemský
osadníci	osadník	k1gMnPc1	osadník
donutili	donutit	k5eAaPmAgMnP	donutit
podvodem	podvod	k1gInSc7	podvod
Svazijce	Svazijce	k1gMnSc2	Svazijce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
svých	svůj	k3xOyFgNnPc2	svůj
těžebních	těžební	k2eAgNnPc2d1	těžební
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
zabrali	zabrat	k5eAaPmAgMnP	zabrat
zemi	zem	k1gFnSc4	zem
bílí	bílý	k2eAgMnPc1d1	bílý
prospektoři	prospektor	k1gMnPc1	prospektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spojil	spojit	k5eAaPmAgMnS	spojit
Svazijské	svazijský	k2eAgNnSc1d1	Svazijské
království	království	k1gNnSc1	království
král	král	k1gMnSc1	král
Sobhuza	Sobhuz	k1gMnSc2	Sobhuz
I.	I.	kA	I.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezvítězila	zvítězit	k5eNaPmAgFnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
stalo	stát	k5eAaPmAgNnS	stát
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
Svazijsko	Svazijsko	k1gNnSc4	Svazijsko
až	až	k9	až
do	do	k7c2	do
získání	získání	k1gNnSc2	získání
jeho	jeho	k3xOp3gFnSc2	jeho
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1967	[number]	k4	1967
získala	získat	k5eAaPmAgFnS	získat
země	země	k1gFnSc1	země
omezenou	omezený	k2eAgFnSc4d1	omezená
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
království	království	k1gNnSc1	království
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
britského	britský	k2eAgInSc2d1	britský
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Mswati	Mswat	k2eAgMnPc1d1	Mswat
III	III	kA	III
<g/>
.	.	kIx.	.
změnil	změnit	k5eAaPmAgInS	změnit
koloniální	koloniální	k2eAgInSc1d1	koloniální
název	název	k1gInSc1	název
Svazijska	Svazijsko	k1gNnSc2	Svazijsko
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
eSwatini	eSwatin	k2eAgMnPc1d1	eSwatin
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
země	zem	k1gFnPc4	zem
Swazi	Swaze	k1gFnSc4	Swaze
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
Svazijců	Svazijce	k1gMnPc2	Svazijce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
učinil	učinit	k5eAaImAgMnS	učinit
tak	tak	k6eAd1	tak
během	během	k7c2	během
50	[number]	k4	50
<g/>
.	.	kIx.	.
oslav	oslava	k1gFnPc2	oslava
nezávislosti	nezávislost	k1gFnSc2	nezávislost
od	od	k7c2	od
britského	britský	k2eAgInSc2d1	britský
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
státu	stát	k1gInSc2	stát
Emlembe	Emlemb	k1gInSc5	Emlemb
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
m	m	kA	m
<g/>
)	)	kIx)	)
svažující	svažující	k2eAgFnSc2d1	svažující
se	se	k3xPyFc4	se
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
a	a	k8xC	a
příkře	příkro	k6eAd1	příkro
spadající	spadající	k2eAgMnSc1d1	spadající
k	k	k7c3	k
Mosambické	mosambický	k2eAgFnSc3d1	Mosambická
nížině	nížina	k1gFnSc3	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Komati	Komat	k2eAgMnPc1d1	Komat
a	a	k8xC	a
Usutu	Usuta	k1gFnSc4	Usuta
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
vlhké	vlhký	k2eAgInPc1d1	vlhký
a	a	k8xC	a
teplé	teplý	k2eAgInPc1d1	teplý
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
přicházejí	přicházet	k5eAaImIp3nP	přicházet
především	především	k9	především
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
jich	on	k3xPp3gNnPc2	on
spadne	spadnout	k5eAaPmIp3nS	spadnout
mezi	mezi	k7c4	mezi
500	[number]	k4	500
až	až	k9	až
1	[number]	k4	1
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnPc3d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
Mbabane	Mbaban	k1gMnSc5	Mbaban
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
velkými	velký	k2eAgMnPc7d1	velký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Manzini	Manzin	k1gMnPc1	Manzin
<g/>
,	,	kIx,	,
Lobamba	Lobamba	k1gMnSc1	Lobamba
a	a	k8xC	a
Siteki	Sitek	k1gMnPc1	Sitek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Mozambikem	Mozambik	k1gInSc7	Mozambik
jsou	být	k5eAaImIp3nP	být
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
savany	savana	k1gFnPc1	savana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Ekonomika	ekonomika	k1gFnSc1	ekonomika
SvazijskaSvazijsko	SvazijskaSvazijsko	k1gNnSc4	SvazijskaSvazijsko
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
stát	stát	k1gInSc1	stát
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
černé	černé	k1gNnSc1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
a	a	k8xC	a
azbest	azbest	k1gInSc1	azbest
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
produkce	produkce	k1gFnSc2	produkce
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
a	a	k8xC	a
citrusy	citrus	k1gInPc1	citrus
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
a	a	k8xC	a
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
Svazijců	Svazijce	k1gMnPc2	Svazijce
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
zlatonosných	zlatonosný	k2eAgInPc6d1	zlatonosný
dolech	dol	k1gInPc6	dol
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
země	zem	k1gFnSc2	zem
oficiálně	oficiálně	k6eAd1	oficiálně
stojí	stát	k5eAaImIp3nS	stát
král	král	k1gMnSc1	král
a	a	k8xC	a
královna-matka	královnaatka	k1gFnSc1	královna-matka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
fakticky	fakticky	k6eAd1	fakticky
většina	většina	k1gFnSc1	většina
moci	moc	k1gFnSc2	moc
připadá	připadat	k5eAaPmIp3nS	připadat
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Mswati	Mswat	k2eAgMnPc1d1	Mswat
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
Sobhuzu	Sobhuz	k1gInSc2	Sobhuz
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Mswati	Mswat	k2eAgMnPc1d1	Mswat
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ležela	ležet	k5eAaImAgFnS	ležet
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
země	země	k1gFnSc1	země
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
mezi	mezi	k7c4	mezi
bělošskou	bělošský	k2eAgFnSc4d1	bělošská
menšinu	menšina	k1gFnSc4	menšina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
chtěla	chtít	k5eAaImAgFnS	chtít
podržet	podržet	k5eAaPmF	podržet
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
černošskou	černošský	k2eAgFnSc4d1	černošská
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
rozšířit	rozšířit	k5eAaPmF	rozšířit
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
Svazijska	Svazijsko	k1gNnSc2	Svazijsko
značně	značně	k6eAd1	značně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
historické	historický	k2eAgFnPc1d1	historická
změny	změna	k1gFnPc1	změna
odehrávající	odehrávající	k2eAgFnPc1d1	odehrávající
se	se	k3xPyFc4	se
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
lid	lid	k1gInSc1	lid
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zvolil	zvolit	k5eAaPmAgMnS	zvolit
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
černošského	černošský	k2eAgMnSc2d1	černošský
prezidenta	prezident	k1gMnSc2	prezident
Nelsona	Nelson	k1gMnSc2	Nelson
Mandelu	Mandela	k1gFnSc4	Mandela
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
oficiálně	oficiálně	k6eAd1	oficiálně
existuje	existovat	k5eAaImIp3nS	existovat
senát	senát	k1gInSc1	senát
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
30	[number]	k4	30
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
je	být	k5eAaImIp3nS	být
absolutistickou	absolutistický	k2eAgFnSc7d1	absolutistická
diarchií	diarchie	k1gFnSc7	diarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
Sobhuza	Sobhuza	k1gFnSc1	Sobhuza
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1968	[number]	k4	1968
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dzeliwe	Dzeliwe	k1gFnSc1	Dzeliwe
(	(	kIx(	(
<g/>
královna-regentka	královnaegentka	k1gFnSc1	královna-regentka
<g/>
;	;	kIx,	;
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1982	[number]	k4	1982
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
princ	princ	k1gMnSc1	princ
Sozisa	Sozis	k1gMnSc2	Sozis
Dlamini	Dlamin	k2eAgMnPc1d1	Dlamin
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1983	[number]	k4	1983
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ndlovukati	Ndlovukat	k5eAaImF	Ndlovukat
Ntombi	Ntombe	k1gFnSc4	Ntombe
(	(	kIx(	(
<g/>
královna-regentka	královnaegentka	k1gFnSc1	královna-regentka
<g/>
;	;	kIx,	;
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1983	[number]	k4	1983
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mswati	Mswat	k1gMnPc1	Mswat
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
provincií	provincie	k1gFnPc2	provincie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hhohho	Hhohze	k6eAd1	Hhohze
</s>
</p>
<p>
<s>
Lubombo	Lubomba	k1gMnSc5	Lubomba
</s>
</p>
<p>
<s>
Manzini	Manzin	k1gMnPc1	Manzin
</s>
</p>
<p>
<s>
Shiselweni	Shiselwen	k2eAgMnPc1d1	Shiselwen
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
eSwatini	eSwatin	k2eAgMnPc1d1	eSwatin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Svazijsko	Svazijsko	k1gNnSc4	Svazijsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Svazijská	svazijský	k2eAgFnSc1d1	svazijská
vláda	vláda	k1gFnSc1	vláda
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
turistiky	turistika	k1gFnSc2	turistika
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
Divoká	divoký	k2eAgFnSc1d1	divoká
příroda	příroda	k1gFnSc1	příroda
Svazijska	Svazijsko	k1gNnSc2	Svazijsko
Fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
NP	NP	kA	NP
Hlane	Hlan	k1gInSc5	Hlan
a	a	k8xC	a
Mkhaya	Mkhaya	k1gFnSc1	Mkhaya
</s>
</p>
<p>
<s>
Swaziland	Swaziland	k1gInSc1	Swaziland
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Swaziland	Swaziland	k1gInSc1	Swaziland
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Swaziland	Swaziland	k1gInSc1	Swaziland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-07	[number]	k4	2011-04-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Swaziland	Swaziland	k1gInSc1	Swaziland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-21	[number]	k4	2011-01-21
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MASSON	MASSON	kA	MASSON
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Swaziland	Swaziland	k1gInSc1	Swaziland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
