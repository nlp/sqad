<s>
Canberra	Canberra	k1gFnSc1	Canberra
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyslovováno	vyslovovat	k5eAaImNgNnS	vyslovovat
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
v	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Australského	australský	k2eAgInSc2d1	australský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
populací	populace	k1gFnSc7	populace
323	[number]	k4	323
056	[number]	k4	056
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
vnitrozemské	vnitrozemský	k2eAgNnSc4d1	vnitrozemské
australské	australský	k2eAgNnSc4d1	Australské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Canberra	Canberra	k1gFnSc1	Canberra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Teritoriu	teritorium	k1gNnSc6	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
ACT	ACT	kA	ACT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
280	[number]	k4	280
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Sydney	Sydney	k1gNnSc2	Sydney
a	a	k8xC	a
660	[number]	k4	660
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Canberra	Canberra	k1gFnSc1	Canberra
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
jako	jako	k9	jako
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Austrálie	Austrálie	k1gFnSc2	Austrálie
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
mezi	mezi	k7c7	mezi
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
dvěma	dva	k4xCgFnPc7	dva
největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mezi	mezi	k7c4	mezi
Sydney	Sydney	k1gNnSc4	Sydney
a	a	k8xC	a
Melbourne	Melbourne	k1gNnSc4	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
design	design	k1gInSc4	design
města	město	k1gNnSc2	město
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
návrh	návrh	k1gInSc4	návrh
dvou	dva	k4xCgMnPc2	dva
chicagských	chicagský	k2eAgMnPc2d1	chicagský
architektů	architekt	k1gMnPc2	architekt
Waltera	Walter	k1gMnSc2	Walter
Burley	Burlea	k1gMnSc2	Burlea
Griffina	Griffin	k1gMnSc2	Griffin
a	a	k8xC	a
Marion	Marion	k1gInSc4	Marion
Mahony	mahon	k1gInPc4	mahon
Griffinové	Griffinový	k2eAgInPc4d1	Griffinový
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
manželů	manžel	k1gMnPc2	manžel
Griffinových	Griffinových	k2eAgMnSc1d1	Griffinových
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
geometrické	geometrický	k2eAgInPc4d1	geometrický
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
šestiúhelníky	šestiúhelník	k1gInPc1	šestiúhelník
a	a	k8xC	a
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
významné	významný	k2eAgFnPc4d1	významná
plochy	plocha	k1gFnPc4	plocha
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgMnPc3	který
Canberra	Canberra	k1gFnSc1	Canberra
získala	získat	k5eAaPmAgFnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
bush	bush	k1gInSc1	bush
capital	capitat	k5eAaImAgInS	capitat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
sídlí	sídlet	k5eAaImIp3nS	sídlet
Australská	australský	k2eAgFnSc1d1	australská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
Australský	australský	k2eAgInSc1d1	australský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
vládních	vládní	k2eAgInPc2d1	vládní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centr	k1gInSc7	centr
mnoha	mnoho	k4c2	mnoho
sociálních	sociální	k2eAgMnPc2d1	sociální
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
národního	národní	k2eAgInSc2d1	národní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Australská	australský	k2eAgFnSc1d1	australská
národní	národní	k2eAgFnSc1d1	národní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Australské	australský	k2eAgNnSc1d1	Australské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
či	či	k8xC	či
Australská	australský	k2eAgFnSc1d1	australská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>

