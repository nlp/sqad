<s>
Canberra	Canberra	k1gFnSc1	Canberra
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyslovováno	vyslovovat	k5eAaImNgNnS	vyslovovat
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
v	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Australského	australský	k2eAgInSc2d1	australský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
populací	populace	k1gFnSc7	populace
323	[number]	k4	323
056	[number]	k4	056
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
vnitrozemské	vnitrozemský	k2eAgNnSc4d1	vnitrozemské
australské	australský	k2eAgNnSc4d1	Australské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Canberra	Canberra	k1gFnSc1	Canberra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Teritoriu	teritorium	k1gNnSc6	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
ACT	ACT	kA	ACT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
280	[number]	k4	280
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Sydney	Sydney	k1gNnSc2	Sydney
a	a	k8xC	a
660	[number]	k4	660
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Canberra	Canberra	k1gFnSc1	Canberra
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
jako	jako	k9	jako
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Austrálie	Austrálie	k1gFnSc2	Austrálie
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
mezi	mezi	k7c7	mezi
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
dvěma	dva	k4xCgFnPc7	dva
největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mezi	mezi	k7c4	mezi
Sydney	Sydney	k1gNnSc4	Sydney
a	a	k8xC	a
Melbourne	Melbourne	k1gNnSc4	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
design	design	k1gInSc4	design
města	město	k1gNnSc2	město
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
návrh	návrh	k1gInSc4	návrh
dvou	dva	k4xCgMnPc2	dva
chicagských	chicagský	k2eAgMnPc2d1	chicagský
architektů	architekt	k1gMnPc2	architekt
Waltera	Walter	k1gMnSc2	Walter
Burley	Burlea	k1gMnSc2	Burlea
Griffina	Griffin	k1gMnSc2	Griffin
a	a	k8xC	a
Marion	Marion	k1gInSc4	Marion
Mahony	mahon	k1gInPc4	mahon
Griffinové	Griffinový	k2eAgInPc4d1	Griffinový
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
manželů	manžel	k1gMnPc2	manžel
Griffinových	Griffinových	k2eAgMnSc1d1	Griffinových
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
geometrické	geometrický	k2eAgInPc4d1	geometrický
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
šestiúhelníky	šestiúhelník	k1gInPc1	šestiúhelník
a	a	k8xC	a
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
významné	významný	k2eAgFnPc4d1	významná
plochy	plocha	k1gFnPc4	plocha
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgMnPc3	který
Canberra	Canberra	k1gFnSc1	Canberra
získala	získat	k5eAaPmAgFnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
bush	bush	k1gInSc1	bush
capital	capitat	k5eAaImAgInS	capitat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
sídlí	sídlet	k5eAaImIp3nS	sídlet
Australská	australský	k2eAgFnSc1d1	australská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
Australský	australský	k2eAgInSc1d1	australský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
vládních	vládní	k2eAgInPc2d1	vládní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centr	k1gInSc7	centr
mnoha	mnoho	k4c2	mnoho
sociálních	sociální	k2eAgMnPc2d1	sociální
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
národního	národní	k2eAgInSc2d1	národní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Australská	australský	k2eAgFnSc1d1	australská
národní	národní	k2eAgFnSc1d1	národní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Australské	australský	k2eAgNnSc1d1	Australské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
či	či	k8xC	či
Australská	australský	k2eAgFnSc1d1	australská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
australské	australský	k2eAgFnSc2d1	australská
kolonie	kolonie	k1gFnSc2	kolonie
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
sloučily	sloučit	k5eAaPmAgFnP	sloučit
v	v	k7c4	v
Commonwealth	Commonwealth	k1gInSc4	Commonwealth
of	of	k?	of
Australia	Australius	k1gMnSc2	Australius
<g/>
,	,	kIx,	,
Australané	Australan	k1gMnPc1	Australan
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
dohodnout	dohodnout	k5eAaPmF	dohodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
roli	role	k1gFnSc4	role
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
převezme	převzít	k5eAaPmIp3nS	převzít
Sydney	Sydney	k1gNnSc4	Sydney
nebo	nebo	k8xC	nebo
Melbourne	Melbourne	k1gNnSc4	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kompromis	kompromis	k1gInSc4	kompromis
Canberra	Canberr	k1gInSc2	Canberr
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Canberra	Canberr	k1gInSc2	Canberr
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
domorodců	domorodec	k1gMnPc2	domorodec
znamená	znamenat	k5eAaImIp3nS	znamenat
Místo	místo	k7c2	místo
setkávání	setkávání	k1gNnSc2	setkávání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přesně	přesně	k6eAd1	přesně
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
a	a	k8xC	a
postaveno	postavit	k5eAaPmNgNnS	postavit
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nechaly	nechat	k5eAaPmAgInP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
městem	město	k1gNnSc7	město
Chicago	Chicago	k1gNnSc4	Chicago
architekta	architekt	k1gMnSc2	architekt
Waltera	Walter	k1gMnSc2	Walter
Burleye	Burley	k1gMnSc2	Burley
Griffina	Griffin	k1gMnSc2	Griffin
<g/>
.	.	kIx.	.
</s>
<s>
Canberra	Canberra	k1gFnSc1	Canberra
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
oblast	oblast	k1gFnSc4	oblast
o	o	k7c4	o
805,6	[number]	k4	805,6
km2	km2	k4	km2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
nedaleko	nedaleko	k7c2	nedaleko
horského	horský	k2eAgInSc2d1	horský
hřebenu	hřeben	k1gInSc2	hřeben
Brindabella	Brindabella	k1gMnSc1	Brindabella
Ranges	Ranges	k1gMnSc1	Ranges
a	a	k8xC	a
okolo	okolo	k7c2	okolo
150	[number]	k4	150
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
východního	východní	k2eAgNnSc2d1	východní
australského	australský	k2eAgNnSc2d1	Australské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
550	[number]	k4	550
až	až	k9	až
700	[number]	k4	700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Majura	Majura	k1gFnSc1	Majura
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc1d1	vysoké
888	[number]	k4	888
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
větší	veliký	k2eAgInPc1d2	veliký
kopce	kopec	k1gInPc1	kopec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
Mt	Mt	k1gMnPc1	Mt
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
Mt	Mt	k1gMnSc1	Mt
Ainslie	Ainslie	k1gFnSc1	Ainslie
<g/>
,	,	kIx,	,
Mt	Mt	k1gFnSc1	Mt
Mugga	Mugga	k1gFnSc1	Mugga
Mugga	Mugga	k1gFnSc1	Mugga
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
eukalyptové	eukalyptový	k2eAgFnPc4d1	eukalyptová
savany	savana	k1gFnPc4	savana
<g/>
,	,	kIx,	,
bažiny	bažina	k1gFnPc4	bažina
a	a	k8xC	a
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Molonglo	Molonglo	k1gNnSc1	Molonglo
River	River	k1gInSc1	River
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
jezero	jezero	k1gNnSc1	jezero
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Lake	Lak	k1gFnSc2	Lak
Burley	Burlea	k1gFnSc2	Burlea
Griffin	Griffina	k1gFnPc2	Griffina
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Mountain	Mountain	k1gMnSc1	Mountain
-	-	kIx~	-
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
hora	hora	k1gFnSc1	hora
812	[number]	k4	812
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Telstra	Telstrum	k1gNnSc2	Telstrum
Tower	Towra	k1gFnPc2	Towra
-	-	kIx~	-
rozhledna	rozhledna	k1gFnSc1	rozhledna
195	[number]	k4	195
metrů	metr	k1gInPc2	metr
National	National	k1gFnSc2	National
Botanic	Botanice	k1gFnPc2	Botanice
Gardens	Gardens	k1gInSc1	Gardens
-	-	kIx~	-
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
Parliament	Parliament	k1gInSc4	Parliament
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Canberra	Canberra	k1gFnSc1	Canberra
<g/>
)	)	kIx)	)
-	-	kIx~	-
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
Old	Olda	k1gFnPc2	Olda
Parliament	Parliament	k1gInSc1	Parliament
House	house	k1gNnSc1	house
-	-	kIx~	-
stará	starý	k2eAgFnSc1d1	stará
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
Australian	Australiana	k1gFnPc2	Australiana
National	National	k1gFnSc2	National
Gallery	Galler	k1gInPc1	Galler
-	-	kIx~	-
galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
Austrálců	Austrálec	k1gMnPc2	Austrálec
High	High	k1gInSc1	High
Court	Courta	k1gFnPc2	Courta
-	-	kIx~	-
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
<g />
.	.	kIx.	.
</s>
<s>
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
National	National	k1gFnSc2	National
Library	Librara	k1gFnSc2	Librara
-	-	kIx~	-
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
National	National	k1gFnSc1	National
Science	Science	k1gFnSc1	Science
and	and	k?	and
Technology	technolog	k1gMnPc4	technolog
Centre	centr	k1gInSc5	centr
-	-	kIx~	-
Národní	národní	k2eAgNnSc1d1	národní
centrum	centrum	k1gNnSc1	centrum
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
ANZAC	ANZAC	kA	ANZAC
památník	památník	k1gInSc4	památník
-	-	kIx~	-
Australský	australský	k2eAgInSc4d1	australský
válečný	válečný	k2eAgInSc4d1	válečný
památník	památník	k1gInSc4	památník
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Canberra	Canberr	k1gInSc2	Canberr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Canberra	Canberra	k1gFnSc1	Canberra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc4	téma
Canberra	Canberr	k1gInSc2	Canberr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Galerie	galerie	k1gFnSc1	galerie
Canberra	Canberra	k1gFnSc1	Canberra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Satelitní	satelitní	k2eAgInSc4d1	satelitní
pohled	pohled	k1gInSc4	pohled
wikimapia	wikimapium	k1gNnSc2	wikimapium
Regionální	regionální	k2eAgFnSc1d1	regionální
mapa	mapa	k1gFnSc1	mapa
Architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
An	An	k1gFnSc2	An
Ideal	Ideal	k1gInSc4	Ideal
City	City	k1gFnSc2	City
<g/>
?	?	kIx.	?
</s>
<s>
The	The	k?	The
1912	[number]	k4	1912
Competition	Competition	k1gInSc1	Competition
to	ten	k3xDgNnSc1	ten
Design	design	k1gInSc4	design
Canberra	Canberr	k1gMnSc2	Canberr
Satelitní	satelitní	k2eAgInSc4d1	satelitní
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
Google	Google	k1gFnSc2	Google
Maps	Mapsa	k1gFnPc2	Mapsa
</s>
