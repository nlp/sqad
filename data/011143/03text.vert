<p>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
košíková	košíková	k1gFnSc1	košíková
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
míčový	míčový	k2eAgInSc1d1	míčový
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
s	s	k7c7	s
pěti	pět	k4xCc7	pět
hráči	hráč	k1gMnPc7	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
snaží	snažit	k5eAaImIp3nP	snažit
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
bodů	bod	k1gInPc2	bod
vhazováním	vhazování	k1gNnSc7	vhazování
míče	míč	k1gInSc2	míč
do	do	k7c2	do
obroučky	obroučka	k1gFnSc2	obroučka
basketbalového	basketbalový	k2eAgInSc2d1	basketbalový
koše	koš	k1gInSc2	koš
a	a	k8xC	a
zabránit	zabránit	k5eAaPmF	zabránit
protihráčům	protihráč	k1gMnPc3	protihráč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
body	bod	k1gInPc4	bod
získali	získat	k5eAaPmAgMnP	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hru	hra	k1gFnSc4	hra
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
James	James	k1gMnSc1	James
Naismith	Naismith	k1gMnSc1	Naismith
pro	pro	k7c4	pro
zpestření	zpestření	k1gNnSc4	zpestření
zimní	zimní	k2eAgFnSc2d1	zimní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
přípravy	příprava	k1gFnSc2	příprava
svých	svůj	k3xOyFgMnPc2	svůj
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nepříliš	příliš	k6eNd1	příliš
dynamický	dynamický	k2eAgInSc4d1	dynamický
sport	sport	k1gInSc4	sport
(	(	kIx(	(
<g/>
pravidla	pravidlo	k1gNnPc1	pravidlo
neumožňovala	umožňovat	k5eNaImAgNnP	umožňovat
pohyb	pohyb	k1gInSc4	pohyb
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
značnou	značný	k2eAgFnSc4d1	značná
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
nejen	nejen	k6eAd1	nejen
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Basketball	Basketball	k1gMnSc1	Basketball
Federation	Federation	k1gInSc1	Federation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
basketbal	basketbal	k1gInSc1	basketbal
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
pořad	pořad	k1gInSc4	pořad
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
i	i	k9	i
ženský	ženský	k2eAgInSc4d1	ženský
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
turnaj	turnaj	k1gInSc4	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
pracoval	pracovat	k5eAaImAgMnS	pracovat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
James	James	k1gMnSc1	James
Naismith	Naismith	k1gMnSc1	Naismith
jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
na	na	k7c4	na
Springfieldské	Springfieldský	k2eAgNnSc4d1	Springfieldské
YMCA	YMCA	kA	YMCA
International	International	k1gFnSc2	International
Training	Training	k1gInSc1	Training
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vymyslet	vymyslet	k5eAaPmF	vymyslet
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
provozován	provozovat	k5eAaImNgInS	provozovat
v	v	k7c6	v
tělocvičně	tělocvična	k1gFnSc6	tělocvična
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
James	James	k1gMnSc1	James
Naismith	Naismith	k1gMnSc1	Naismith
představil	představit	k5eAaPmAgMnS	představit
svým	svůj	k3xOyFgMnPc3	svůj
studentům	student	k1gMnPc3	student
novou	nový	k2eAgFnSc4d1	nová
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
pravidla	pravidlo	k1gNnPc4	pravidlo
shrnul	shrnout	k5eAaPmAgMnS	shrnout
do	do	k7c2	do
13	[number]	k4	13
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protilehlé	protilehlý	k2eAgFnPc4d1	protilehlá
strany	strana	k1gFnPc4	strana
tělocvičny	tělocvična	k1gFnSc2	tělocvična
přibil	přibít	k5eAaPmAgInS	přibít
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
10	[number]	k4	10
stop	stop	k2eAgInSc2d1	stop
koše	koš	k1gInSc2	koš
na	na	k7c4	na
sběr	sběr	k1gInSc4	sběr
broskví	broskev	k1gFnPc2	broskev
<g/>
,	,	kIx,	,
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přistavil	přistavit	k5eAaPmAgInS	přistavit
žebříky	žebřík	k1gInPc4	žebřík
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
žáky	žák	k1gMnPc4	žák
pověřil	pověřit	k5eAaPmAgMnS	pověřit
vyndaváním	vyndavání	k1gNnSc7	vyndavání
míče	míč	k1gInSc2	míč
z	z	k7c2	z
košů	koš	k1gInPc2	koš
<g/>
.	.	kIx.	.
</s>
<s>
Naismith	Naismith	k1gMnSc1	Naismith
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
pravidla	pravidlo	k1gNnPc4	pravidlo
osmnácti	osmnáct	k4xCc6	osmnáct
žákům	žák	k1gMnPc3	žák
své	svůj	k3xOyFgFnSc2	svůj
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Franka	Frank	k1gMnSc4	Frank
Mahana	Mahan	k1gMnSc4	Mahan
a	a	k8xC	a
Duncana	Duncan	k1gMnSc4	Duncan
Pattona	Patton	k1gMnSc4	Patton
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
osm	osm	k4xCc4	osm
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
a	a	k8xC	a
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
kam	kam	k6eAd1	kam
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Chase	chasa	k1gFnSc6	chasa
proměnil	proměnit	k5eAaPmAgInS	proměnit
střelecký	střelecký	k2eAgInSc1d1	střelecký
pokus	pokus	k1gInSc1	pokus
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
hřiště	hřiště	k1gNnSc2	hřiště
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
skončil	skončit	k5eAaPmAgMnS	skončit
výsledkem	výsledek	k1gInSc7	výsledek
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
studentů	student	k1gMnPc2	student
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
sport	sport	k1gInSc4	sport
okamžitě	okamžitě	k6eAd1	okamžitě
získal	získat	k5eAaPmAgMnS	získat
oblibu	obliba	k1gFnSc4	obliba
a	a	k8xC	a
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
škol	škola	k1gFnPc2	škola
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgInP	začít
chodit	chodit	k5eAaImF	chodit
dotazy	dotaz	k1gInPc4	dotaz
na	na	k7c4	na
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vydání	vydání	k1gNnSc3	vydání
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
časopisu	časopis	k1gInSc6	časopis
"	"	kIx"	"
<g/>
Triangle	triangl	k1gInSc5	triangl
<g/>
"	"	kIx"	"
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
NBA	NBA	kA	NBA
==	==	k?	==
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
NBA	NBA	kA	NBA
je	být	k5eAaImIp3nS	být
nejprestižnější	prestižní	k2eAgFnSc7d3	nejprestižnější
basketbalovou	basketbalový	k2eAgFnSc7d1	basketbalová
ligou	liga	k1gFnSc7	liga
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
nejúspěšnějšími	úspěšný	k2eAgInPc7d3	nejúspěšnější
kluby	klub	k1gInPc7	klub
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
Boston	Boston	k1gInSc4	Boston
Celtics	Celtics	k1gInSc1	Celtics
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Lakers	Lakers	k1gInSc1	Lakers
a	a	k8xC	a
Chicago	Chicago	k1gNnSc1	Chicago
Bulls	Bullsa	k1gFnPc2	Bullsa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
NBA	NBA	kA	NBA
30	[number]	k4	30
týmů	tým	k1gInPc2	tým
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Western	Western	kA	Western
<g/>
(	(	kIx(	(
<g/>
Západní	západní	k2eAgMnPc1d1	západní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eastern	Eastern	k1gInSc1	Eastern
(	(	kIx(	(
<g/>
Východní	východní	k2eAgFnSc1d1	východní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
postoupí	postoupit	k5eAaPmIp3nS	postoupit
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
8	[number]	k4	8
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
konference	konference	k1gFnSc2	konference
do	do	k7c2	do
playoff	playoff	k1gInSc4	playoff
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
kola	kolo	k1gNnSc2	kolo
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
postupuje	postupovat	k5eAaImIp3nS	postupovat
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
4	[number]	k4	4
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Playoff	Playoff	k1gInSc1	Playoff
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
kola	kolo	k1gNnSc2	kolo
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
postoupí	postoupit	k5eAaPmIp3nP	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
ten	ten	k3xDgInSc1	ten
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
tým	tým	k1gInSc1	tým
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Basketbal	basketbal	k1gInSc1	basketbal
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc3d1	Česká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
poprvé	poprvé	k6eAd1	poprvé
basketbal	basketbal	k1gInSc4	basketbal
představil	představit	k5eAaPmAgMnS	představit
učitel	učitel	k1gMnSc1	učitel
tělocviku	tělocvik	k1gInSc2	tělocvik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Karásek	Karásek	k1gMnSc1	Karásek
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
Mýtě	mýto	k1gNnSc6	mýto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
česká	český	k2eAgNnPc1d1	české
pravidla	pravidlo	k1gNnPc1	pravidlo
vyšla	vyjít	k5eAaPmAgNnP	vyjít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
soutěž	soutěž	k1gFnSc1	soutěž
je	být	k5eAaImIp3nS	být
Kooperativa	kooperativa	k1gFnSc1	kooperativa
NBL	NBL	kA	NBL
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
hrálo	hrát	k5eAaImAgNnS	hrát
12	[number]	k4	12
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
a	a	k8xC	a
ženská	ženská	k1gFnSc1	ženská
</s>
</p>
<p>
<s>
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
Trocal	Trocal	k1gFnSc1	Trocal
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
utkává	utkávat	k5eAaImIp3nS	utkávat
10	[number]	k4	10
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc7d2	nižší
soutěží	soutěž	k1gFnSc7	soutěž
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
jak	jak	k6eAd1	jak
mužů	muž	k1gMnPc2	muž
tak	tak	k8xS	tak
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
hrálo	hrát	k5eAaImAgNnS	hrát
mužskou	mužský	k2eAgFnSc4d1	mužská
ligu	liga	k1gFnSc4	liga
14	[number]	k4	14
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
ženskou	ženský	k2eAgFnSc4d1	ženská
ligu	liga	k1gFnSc4	liga
10	[number]	k4	10
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
3	[number]	k4	3
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
na	na	k7c4	na
2	[number]	k4	2
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
další	další	k2eAgFnSc2d1	další
sezony	sezona	k1gFnSc2	sezona
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1	mužská
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejná	stejný	k2eAgFnSc1d1	stejná
(	(	kIx(	(
<g/>
Mattoni	Matton	k1gMnPc1	Matton
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
ženská	ženský	k2eAgFnSc1d1	ženská
(	(	kIx(	(
<g/>
Trocal	Trocal	k1gFnSc1	Trocal
ŽBL	ŽBL	kA	ŽBL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
nastává	nastávat	k5eAaImIp3nS	nastávat
u	u	k7c2	u
názvů	název	k1gInPc2	název
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
když	když	k8xS	když
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
liga	liga	k1gFnSc1	liga
první	první	k4xOgFnSc1	první
a	a	k8xC	a
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
ligy	liga	k1gFnSc2	liga
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
mužstvem	mužstvo	k1gNnSc7	mužstvo
ČEZ	ČEZ	kA	ČEZ
Basketball	Basketball	k1gInSc1	Basketball
Nymburk	Nymburk	k1gInSc1	Nymburk
Mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
BK	BK	kA	BK
IMOS	IMOS	kA	IMOS
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
nepřetržitě	přetržitě	k6eNd1	přetržitě
vítězí	vítězit	k5eAaImIp3nS	vítězit
v	v	k7c6	v
Ženské	ženský	k2eAgFnSc6d1	ženská
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
lize	liga	k1gFnSc6	liga
a	a	k8xC	a
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
navíc	navíc	k6eAd1	navíc
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
soutěž	soutěž	k1gFnSc4	soutěž
-	-	kIx~	-
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
7	[number]	k4	7
<g/>
×	×	k?	×
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Euroligy	Euroliga	k1gFnSc2	Euroliga
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
jedenkrát	jedenkrát	k6eAd1	jedenkrát
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
následována	následován	k2eAgFnSc1d1	následována
týmem	tým	k1gInSc7	tým
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
Sika	sika	k1gMnSc1	sika
Brno	Brno	k1gNnSc1	Brno
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
účastmi	účast	k1gFnPc7	účast
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednou	jednou	k6eAd1	jednou
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
a	a	k8xC	a
Slovian	Slovian	k1gInSc4	Slovian
Orbis	orbis	k1gInSc1	orbis
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
probojoval	probojovat	k5eAaPmAgInS	probojovat
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
nezvítězil	zvítězit	k5eNaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
mužská	mužský	k2eAgFnSc1d1	mužská
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
účastnila	účastnit	k5eAaImAgFnS	účastnit
7	[number]	k4	7
<g/>
×	×	k?	×
a	a	k8xC	a
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
umístění	umístění	k1gNnSc2	umístění
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
však	však	k9	však
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
pořádaném	pořádaný	k2eAgNnSc6d1	pořádané
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
momentálně	momentálně	k6eAd1	momentálně
nejúspěšnější	úspěšný	k2eAgMnPc4d3	nejúspěšnější
české	český	k2eAgMnPc4d1	český
hráče	hráč	k1gMnPc4	hráč
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
trojice	trojice	k1gFnSc1	trojice
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
slavnou	slavný	k2eAgFnSc7d1	slavná
NBA	NBA	kA	NBA
<g/>
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Basketball	Basketball	k1gMnSc1	Basketball
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prvnímu	první	k4xOgMnSc3	první
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
Jiřímu	Jiří	k1gMnSc3	Jiří
Zídkovi	Zídek	k1gMnSc3	Zídek
juniorovi	junior	k1gMnSc3	junior
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
Jiří	Jiří	k1gMnSc1	Jiří
Welsch	Welsch	k1gMnSc1	Welsch
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011-14	[number]	k4	2011-14
i	i	k8xC	i
Jan	Jan	k1gMnSc1	Jan
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dnes	dnes	k6eAd1	dnes
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
Fenerbahce	Fenerbahce	k1gFnPc4	Fenerbahce
Ulker	Ulkra	k1gFnPc2	Ulkra
</s>
</p>
<p>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Welsch	Welsch	k1gMnSc1	Welsch
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Veselý	Veselý	k1gMnSc1	Veselý
si	se	k3xPyFc3	se
v	v	k7c6	v
NBA	NBA	kA	NBA
odbyli	odbýt	k5eAaPmAgMnP	odbýt
tříleté	tříletý	k2eAgInPc4d1	tříletý
kontrakty	kontrakt	k1gInPc4	kontrakt
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
NBA	NBA	kA	NBA
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Satoranský	Satoranský	k2eAgMnSc1d1	Satoranský
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Washington	Washington	k1gInSc1	Washington
Wizards	Wizards	k1gInSc1	Wizards
(	(	kIx(	(
<g/>
draftován	draftován	k2eAgMnSc1d1	draftován
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
odehraná	odehraný	k2eAgFnSc1d1	odehraná
sezóna	sezóna	k1gFnSc1	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
české	český	k2eAgFnPc4d1	Česká
hráčky	hráčka	k1gFnPc4	hráčka
patří	patřit	k5eAaImIp3nS	patřit
Eva	Eva	k1gFnSc1	Eva
Horáková	Horáková	k1gFnSc1	Horáková
–	–	k?	–
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
basketbalistka	basketbalistka	k1gFnSc1	basketbalistka
Evropy	Evropa	k1gFnSc2	Evropa
1996	[number]	k4	1996
a	a	k8xC	a
první	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
ve	v	k7c6	v
WNBA	WNBA	kA	WNBA
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Večeřová	Večeřová	k1gFnSc1	Večeřová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Kulichová	Kulichová	k1gFnSc1	Kulichová
či	či	k8xC	či
Eva	Eva	k1gFnSc1	Eva
Vítečková	Vítečková	k1gFnSc1	Vítečková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Parametry	parametr	k1gInPc1	parametr
==	==	k?	==
</s>
</p>
<p>
<s>
Obroučka	obroučka	k1gFnSc1	obroučka
basketbalového	basketbalový	k2eAgInSc2d1	basketbalový
koše	koš	k1gInSc2	koš
je	být	k5eAaImIp3nS	být
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3,05	[number]	k4	3,05
metru	metr	k1gInSc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
šířka	šířka	k1gFnSc1	šířka
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
180	[number]	k4	180
cm	cm	kA	cm
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
desky	deska	k1gFnSc2	deska
105	[number]	k4	105
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
délka	délka	k1gFnSc1	délka
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
měří	měřit	k5eAaImIp3nS	měřit
28	[number]	k4	28
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
oficiální	oficiální	k2eAgFnSc1d1	oficiální
šířka	šířka	k1gFnSc1	šířka
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
schváleny	schválen	k2eAgInPc1d1	schválen
rozměry	rozměr	k1gInPc1	rozměr
minimálně	minimálně	k6eAd1	minimálně
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zisk	zisk	k1gInSc1	zisk
bodů-	bodů-	k?	bodů-
pravidla	pravidlo	k1gNnPc4	pravidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
(	(	kIx(	(
<g/>
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
<g/>
)	)	kIx)	)
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
koš	koš	k1gInSc1	koš
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
hráč	hráč	k1gMnSc1	hráč
hodí	hodit	k5eAaPmIp3nS	hodit
do	do	k7c2	do
koše	koš	k1gInSc2	koš
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
nebo	nebo	k8xC	nebo
blízké	blízký	k2eAgFnSc2d1	blízká
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koš	koš	k1gInSc1	koš
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
ohodnocen	ohodnotit	k5eAaPmNgInS	ohodnotit
2	[number]	k4	2
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
koš	koš	k1gInSc1	koš
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
hráč	hráč	k1gMnSc1	hráč
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
dá	dát	k5eAaPmIp3nS	dát
koš	koš	k1gInSc1	koš
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
trojkovým	trojkový	k2eAgInSc7d1	trojkový
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
způsobem	způsob	k1gInSc7	způsob
jak	jak	k8xS	jak
dát	dát	k5eAaPmF	dát
koš	koš	k1gInSc4	koš
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
trestný	trestný	k2eAgInSc4d1	trestný
hod	hod	k1gInSc4	hod
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
šestka	šestka	k1gFnSc1	šestka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hráč	hráč	k1gMnSc1	hráč
hází	házet	k5eAaImIp3nS	házet
po	po	k7c6	po
faulu	faul	k1gInSc6	faul
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HLOUŠEK	Hloušek	k1gMnSc1	Hloušek
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
trenovati	trenovat	k5eAaImF	trenovat
košíkovou	košíková	k1gFnSc4	košíková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pour	Pour	k1gMnSc1	Pour
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
62	[number]	k4	62
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženy	žena	k1gFnPc4	žena
•	•	k?	•
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
•	•	k?	•
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
•	•	k?	•
hráček	hráčka	k1gFnPc2	hráčka
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženy	žena	k1gFnPc4	žena
•	•	k?	•
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
•	•	k?	•
hráček	hráčka	k1gFnPc2	hráčka
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
Cup	cup	k1gInSc1	cup
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
</s>
</p>
<p>
<s>
Euroliga	Euroliga	k1gFnSc1	Euroliga
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
(	(	kIx(	(
<g/>
Pohár	pohár	k1gInSc1	pohár
evropských	evropský	k2eAgMnPc2d1	evropský
mistrů	mistr	k1gMnPc2	mistr
<g/>
)	)	kIx)	)
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženy	žena	k1gFnPc1	žena
</s>
</p>
<p>
<s>
ULEB	ULEB	kA	ULEB
Eurocup	Eurocup	k1gInSc1	Eurocup
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
•	•	k?	•
FIBA	FIBA	kA	FIBA
Eurocup	Eurocup	k1gInSc1	Eurocup
-	-	kIx~	-
ženy	žena	k1gFnPc1	žena
</s>
</p>
<p>
<s>
ČESKÁ	český	k2eAgFnSc1d1	Česká
A	a	k8xC	a
SLOVENSKÁ	slovenský	k2eAgFnSc1d1	slovenská
BASKETBALOVÁ	basketbalový	k2eAgFnSc1d1	basketbalová
FEDERACE	federace	k1gFnSc1	federace
•	•	k?	•
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
ČBF	ČBF	kA	ČBF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
československých	československý	k2eAgMnPc2d1	československý
basketbalistů	basketbalista	k1gMnPc2	basketbalista
•	•	k?	•
Basketbalista	basketbalista	k1gMnSc1	basketbalista
roku	rok	k1gInSc2	rok
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
basketbalista	basketbalista	k1gMnSc1	basketbalista
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
•	•	k?	•
All-Star	All-Star	k1gMnSc1	All-Star
zápasy	zápas	k1gInPc4	zápas
české	český	k2eAgFnSc2d1	Česká
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
ligy	liga	k1gFnSc2	liga
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženy	žena	k1gFnPc1	žena
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženy	žena	k1gFnPc1	žena
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
•	•	k?	•
muži	muž	k1gMnSc3	muž
•	•	k?	•
ženyzaniklé	ženyzaniklý	k2eAgFnSc2d1	ženyzaniklý
evropské	evropský	k2eAgFnSc2d1	Evropská
klubové	klubový	k2eAgFnSc2d1	klubová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Saporta	Saporta	k1gFnSc1	Saporta
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Pohár	pohár	k1gInSc1	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
-	-	kIx~	-
PVP	PVP	kA	PVP
<g/>
)	)	kIx)	)
•	•	k?	•
Koračův	Koračův	k2eAgInSc4d1	Koračův
pohár	pohár	k1gInSc4	pohár
•	•	k?	•
FIBA	FIBA	kA	FIBA
EuroCup	EuroCup	k1gInSc1	EuroCup
Challenge	Challenge	k1gInSc1	Challenge
•	•	k?	•
Pohár	pohár	k1gInSc4	pohár
Lilianny	Lilianna	k1gFnSc2	Lilianna
Ronchettiové	Ronchettiový	k2eAgFnSc2d1	Ronchettiový
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
žen	žena	k1gFnPc2	žena
•	•	k?	•
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
basketbal	basketbal	k1gInSc4	basketbal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
basketbal	basketbal	k1gInSc1	basketbal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
</s>
</p>
