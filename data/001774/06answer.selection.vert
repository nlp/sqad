<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
Dodgson	Dodgson	k1gInSc1	Dodgson
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
velké	velký	k2eAgNnSc4d1	velké
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
knihu	kniha	k1gFnSc4	kniha
The	The	k1gFnSc2	The
Hunting	Hunting	k1gInSc1	Hunting
of	of	k?	of
the	the	k?	the
Snark	Snark	k1gInSc4	Snark
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
dětské	dětský	k2eAgFnPc4d1	dětská
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Gertrude	Gertrud	k1gInSc5	Gertrud
Chatawayové	Chatawayové	k2eAgMnPc6d1	Chatawayové
<g/>
.	.	kIx.	.
</s>
