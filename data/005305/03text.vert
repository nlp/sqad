<s>
Sonáta	sonáta	k1gFnSc1	sonáta
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
určená	určený	k2eAgFnSc1d1	určená
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
nástroj	nástroj	k1gInSc4	nástroj
nebo	nebo	k8xC	nebo
nástroj	nástroj	k1gInSc4	nástroj
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
až	až	k6eAd1	až
čtyři	čtyři	k4xCgFnPc1	čtyři
samostatné	samostatný	k2eAgFnPc1d1	samostatná
části	část	k1gFnPc1	část
(	(	kIx(	(
<g/>
věty	věta	k1gFnPc1	věta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mívá	mívat	k5eAaImIp3nS	mívat
sonátovou	sonátový	k2eAgFnSc4d1	sonátová
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
sonáta	sonáta	k1gFnSc1	sonáta
(	(	kIx(	(
<g/>
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
sonare	sonar	k1gInSc5	sonar
-	-	kIx~	-
znít	znít	k5eAaImF	znít
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
odlišeny	odlišit	k5eAaPmNgFnP	odlišit
od	od	k7c2	od
tokáty	tokáta	k1gFnSc2	tokáta
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
zpívané	zpívaný	k2eAgFnPc4d1	zpívaná
kantáty	kantáta	k1gFnPc4	kantáta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
se	se	k3xPyFc4	se
jako	jako	k9	jako
sonáta	sonáta	k1gFnSc1	sonáta
označovala	označovat	k5eAaImAgFnS	označovat
suita	suita	k1gFnSc1	suita
pro	pro	k7c4	pro
komorní	komorní	k2eAgInSc4d1	komorní
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obsazení	obsazení	k1gNnPc2	obsazení
to	ten	k3xDgNnSc1	ten
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
sólová	sólový	k2eAgFnSc1d1	sólová
sonáta	sonáta	k1gFnSc1	sonáta
-	-	kIx~	-
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k1gNnSc4	continuo
triová	triový	k2eAgFnSc1d1	triová
sonáta	sonáta	k1gFnSc1	sonáta
-	-	kIx~	-
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k1gNnSc4	continuo
Formálně	formálně	k6eAd1	formálně
a	a	k8xC	a
stavebně	stavebně	k6eAd1	stavebně
se	se	k3xPyFc4	se
barokní	barokní	k2eAgFnPc1d1	barokní
sonáty	sonáta	k1gFnPc1	sonáta
dělily	dělit	k5eAaImAgFnP	dělit
také	také	k9	také
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
:	:	kIx,	:
sonata	sonat	k2eAgFnSc1d1	sonata
da	da	k?	da
camera	camera	k1gFnSc1	camera
-	-	kIx~	-
"	"	kIx"	"
<g/>
komorní	komorní	k2eAgFnSc1d1	komorní
<g/>
"	"	kIx"	"
sonáta	sonáta	k1gFnSc1	sonáta
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
lehký	lehký	k2eAgInSc4d1	lehký
taneční	taneční	k2eAgInSc4d1	taneční
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
taneční	taneční	k2eAgFnSc2d1	taneční
suity	suita	k1gFnSc2	suita
<g/>
.	.	kIx.	.
sonata	sonat	k2eAgFnSc1d1	sonata
da	da	k?	da
chiesa	chiesa	k1gFnSc1	chiesa
-	-	kIx~	-
"	"	kIx"	"
<g/>
chrámová	chrámový	k2eAgFnSc1d1	chrámová
<g/>
"	"	kIx"	"
sonáta	sonáta	k1gFnSc1	sonáta
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
čtyři	čtyři	k4xCgFnPc4	čtyři
ustálené	ustálený	k2eAgFnPc4d1	ustálená
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
vážnější	vážní	k2eAgInSc4d2	vážnější
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
polyfonní	polyfonní	k2eAgInPc4d1	polyfonní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sonaty	sonata	k1gFnSc2	sonata
da	da	k?	da
chiesa	chiesa	k1gFnSc1	chiesa
je	být	k5eAaImIp3nS	být
odvozena	odvozen	k2eAgFnSc1d1	odvozena
struktura	struktura	k1gFnSc1	struktura
tzv.	tzv.	kA	tzv.
neapolské	neapolský	k2eAgFnSc2d1	neapolská
ouvertury	ouvertura	k1gFnSc2	ouvertura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Odstraněním	odstranění	k1gNnSc7	odstranění
úvodní	úvodní	k2eAgFnSc2d1	úvodní
pomalé	pomalý	k2eAgFnSc2d1	pomalá
části	část	k1gFnSc2	část
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
třídílná	třídílný	k2eAgFnSc1d1	třídílná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
volnější	volný	k2eAgFnSc1d2	volnější
a	a	k8xC	a
zpěvná	zpěvný	k2eAgFnSc1d1	zpěvná
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
opět	opět	k6eAd1	opět
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
často	často	k6eAd1	často
tanečního	taneční	k2eAgInSc2d1	taneční
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
žánrů	žánr	k1gInPc2	žánr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
právě	právě	k9	právě
pro	pro	k7c4	pro
klasické	klasický	k2eAgFnPc4d1	klasická
sonáty	sonáta	k1gFnPc4	sonáta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasicismu	klasicismus	k1gInSc6	klasicismus
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
ustálení	ustálení	k1gNnSc4	ustálení
sonáty	sonáta	k1gFnSc2	sonáta
(	(	kIx(	(
<g/>
a	a	k8xC	a
zejména	zejména	k9	zejména
sonátové	sonátový	k2eAgFnPc4d1	sonátová
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
bývá	bývat	k5eAaImIp3nS	bývat
psána	psát	k5eAaImNgFnS	psát
její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
název	název	k1gInSc4	název
sonáta	sonáta	k1gFnSc1	sonáta
začal	začít	k5eAaPmAgInS	začít
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
skladby	skladba	k1gFnPc4	skladba
určené	určený	k2eAgNnSc4d1	určené
buď	buď	k8xC	buď
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
klavírem	klavír	k1gInSc7	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
stavbu	stavba	k1gFnSc4	stavba
(	(	kIx(	(
<g/>
vícevětá	vícevětý	k2eAgFnSc1d1	vícevětý
skladba	skladba	k1gFnSc1	skladba
obsahující	obsahující	k2eAgFnSc4d1	obsahující
část	část	k1gFnSc4	část
v	v	k7c6	v
sonátové	sonátový	k2eAgFnSc6d1	sonátová
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obecněji	obecně	k6eAd2	obecně
nazývá	nazývat	k5eAaImIp3nS	nazývat
sonátový	sonátový	k2eAgInSc1d1	sonátový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k8xC	i
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
širší	široký	k2eAgNnSc4d2	širší
nástrojové	nástrojový	k2eAgNnSc4d1	nástrojové
obsazení	obsazení	k1gNnSc4	obsazení
-	-	kIx~	-
komorní	komorní	k2eAgInPc1d1	komorní
soubory	soubor	k1gInPc1	soubor
(	(	kIx(	(
<g/>
tria	trio	k1gNnPc1	trio
<g/>
,	,	kIx,	,
kvartety	kvartet	k1gInPc1	kvartet
<g/>
,	,	kIx,	,
kvintety	kvintet	k1gInPc1	kvintet
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sólistu	sólista	k1gMnSc4	sólista
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
koncerty	koncert	k1gInPc1	koncert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
podobě	podoba	k1gFnSc6	podoba
tři	tři	k4xCgFnPc1	tři
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tematicky	tematicky	k6eAd1	tematicky
a	a	k8xC	a
tonálně	tonálně	k6eAd1	tonálně
navzájem	navzájem	k6eAd1	navzájem
těsně	těsně	k6eAd1	těsně
propojeny	propojen	k2eAgInPc1d1	propojen
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
volně	volně	k6eAd1	volně
propojených	propojený	k2eAgFnPc2d1	propojená
části	část	k1gFnSc6	část
suity	suita	k1gFnPc1	suita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Allegro	allegro	k6eAd1	allegro
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
věta	věta	k1gFnSc1	věta
má	mít	k5eAaImIp3nS	mít
svižné	svižný	k2eAgNnSc4d1	svižné
tempo	tempo	k1gNnSc4	tempo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
téma	téma	k1gNnSc1	téma
sonáty	sonáta	k1gFnSc2	sonáta
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
v	v	k7c6	v
sonátově	sonátově	k6eAd1	sonátově
formě	forma	k1gFnSc3	forma
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvná	zpěvný	k2eAgFnSc1d1	zpěvná
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
pomalého	pomalý	k2eAgNnSc2d1	pomalé
tempa	tempo	k1gNnSc2	tempo
(	(	kIx(	(
<g/>
kontrast	kontrast	k1gInSc1	kontrast
k	k	k7c3	k
okolním	okolní	k2eAgFnPc3d1	okolní
větám	věta	k1gFnPc3	věta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
formy	forma	k1gFnPc1	forma
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různé	různý	k2eAgInPc1d1	různý
-	-	kIx~	-
často	často	k6eAd1	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
téma	téma	k1gNnSc4	téma
s	s	k7c7	s
variacemi	variace	k1gFnPc7	variace
nebo	nebo	k8xC	nebo
rondo	rondo	k1gNnSc1	rondo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
sonátová	sonátový	k2eAgFnSc1d1	sonátová
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
<g/>
,	,	kIx,	,
formy	forma	k1gFnPc1	forma
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřvětých	čtyřvětý	k2eAgFnPc6d1	čtyřvětá
sonátách	sonáta	k1gFnPc6	sonáta
bývá	bývat	k5eAaImIp3nS	bývat
mezi	mezi	k7c4	mezi
zpěvnou	zpěvný	k2eAgFnSc4d1	zpěvná
větu	věta	k1gFnSc4	věta
a	a	k8xC	a
finále	finále	k1gNnSc4	finále
vložena	vložen	k2eAgFnSc1d1	vložena
taneční	taneční	k2eAgFnSc1d1	taneční
věta	věta	k1gFnSc1	věta
(	(	kIx(	(
<g/>
menuet	menuet	k1gInSc1	menuet
<g/>
,	,	kIx,	,
scherzo	scherzo	k1gNnSc1	scherzo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
základní	základní	k2eAgFnSc1d1	základní
stavba	stavba	k1gFnSc1	stavba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
redukována	redukovat	k5eAaBmNgFnS	redukovat
vypuštěním	vypuštění	k1gNnSc7	vypuštění
některé	některý	k3yIgFnSc2	některý
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
dvě	dva	k4xCgFnPc4	dva
věty	věta	k1gFnPc4	věta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třívěté	třívětý	k2eAgFnPc1d1	třívětá
sonáty	sonáta	k1gFnPc1	sonáta
obsahující	obsahující	k2eAgFnSc4d1	obsahující
taneční	taneční	k2eAgFnSc4d1	taneční
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Sonáty	sonáta	k1gFnPc1	sonáta
můžeme	moct	k5eAaImIp1nP	moct
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
také	také	k9	také
podle	podle	k7c2	podle
celkové	celkový	k2eAgFnSc2d1	celková
kompoziční	kompoziční	k2eAgFnSc2d1	kompoziční
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
sestupná	sestupný	k2eAgNnPc1d1	sestupné
-	-	kIx~	-
těžiště	těžiště	k1gNnSc1	těžiště
skladby	skladba	k1gFnPc1	skladba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
delší	dlouhý	k2eAgNnSc4d2	delší
než	než	k8xS	než
ostatní	ostatní	k2eAgNnSc4d1	ostatní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
prezentováno	prezentován	k2eAgNnSc4d1	prezentováno
a	a	k8xC	a
rozvíjeno	rozvíjen	k2eAgNnSc4d1	rozvíjeno
nejzávažnější	závažný	k2eAgNnSc4d3	nejzávažnější
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
věty	věta	k1gFnPc1	věta
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
finále	finále	k1gNnSc2	finále
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
sonáty	sonáta	k1gFnPc4	sonáta
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
krajní	krajní	k2eAgFnPc1d1	krajní
věty	věta	k1gFnPc1	věta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
vzestupná	vzestupný	k2eAgFnSc1d1	vzestupná
-	-	kIx~	-
těžiště	těžiště	k1gNnSc1	těžiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
celá	celý	k2eAgFnSc1d1	celá
skladba	skladba	k1gFnSc1	skladba
směřuje	směřovat	k5eAaImIp3nS	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
a	a	k8xC	a
spíše	spíše	k9	spíše
v	v	k7c6	v
novějších	nový	k2eAgNnPc6d2	novější
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známá	známý	k2eAgFnSc1d1	známá
Beethovenova	Beethovenův	k2eAgFnSc1d1	Beethovenova
sonáta	sonáta	k1gFnSc1	sonáta
"	"	kIx"	"
<g/>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
svit	svit	k1gInSc1	svit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
úvodní	úvodní	k2eAgNnSc1d1	úvodní
allegro	allegro	k1gNnSc1	allegro
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
začíná	začínat	k5eAaImIp3nS	začínat
pomalou	pomalý	k2eAgFnSc7d1	pomalá
větou	věta	k1gFnSc7	věta
<g/>
.	.	kIx.	.
</s>
<s>
Drobnější	drobný	k2eAgFnSc1d2	drobnější
a	a	k8xC	a
interpretačně	interpretačně	k6eAd1	interpretačně
snazší	snadný	k2eAgFnSc1d2	snazší
sonáta	sonáta	k1gFnSc1	sonáta
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sonatina	sonatina	k1gFnSc1	sonatina
<g/>
.	.	kIx.	.
</s>
