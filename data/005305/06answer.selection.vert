<s>
Výraz	výraz	k1gInSc1	výraz
sonáta	sonáta	k1gFnSc1	sonáta
(	(	kIx(	(
<g/>
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
sonare	sonar	k1gInSc5	sonar
-	-	kIx~	-
znít	znít	k5eAaImF	znít
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
odlišeny	odlišit	k5eAaPmNgFnP	odlišit
od	od	k7c2	od
tokáty	tokáta	k1gFnSc2	tokáta
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
zpívané	zpívaný	k2eAgFnPc4d1	zpívaná
kantáty	kantáta	k1gFnPc4	kantáta
<g/>
.	.	kIx.	.
</s>
