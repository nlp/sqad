<s>
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
2	[number]	k4	2
provincie	provincie	k1gFnSc2	provincie
<g/>
:	:	kIx,	:
Las	laso	k1gNnPc2	laso
Palmas	Palmasa	k1gFnPc2	Palmasa
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
