<s>
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Canarias	Canarias	k1gMnSc1	Canarias
nebo	nebo	k8xC	nebo
Islas	Islas	k1gMnSc1	Islas
Canarias	Canarias	k1gMnSc1	Canarias
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
autonomní	autonomní	k2eAgNnSc1d1	autonomní
společenství	společenství	k1gNnSc1	společenství
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
souostroví	souostroví	k1gNnSc1	souostroví
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
geografické	geografický	k2eAgFnSc2d1	geografická
oblasti	oblast	k1gFnSc2	oblast
Makaronézie	Makaronézie	k1gFnSc2	Makaronézie
(	(	kIx(	(
<g/>
Macoronesia	Macoronesia	k1gFnSc1	Macoronesia
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kapverdami	Kapverda	k1gFnPc7	Kapverda
<g/>
,	,	kIx,	,
Madeirou	Madeira	k1gFnSc7	Madeira
<g/>
,	,	kIx,	,
Azory	Azor	k1gMnPc7	Azor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
canis	canis	k1gInSc1	canis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgInP	být
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
hlídány	hlídán	k2eAgInPc1d1	hlídán
obrovskými	obrovský	k2eAgInPc7d1	obrovský
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
3	[number]	k4	3
000	[number]	k4	000
před	před	k7c4	před
K.	K.	kA	K.
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
před	před	k7c4	před
K.	K.	kA	K.
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
Guančové	Guančový	k2eAgInPc4d1	Guančový
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Guančové	Guanč	k1gMnPc1	Guanč
žili	žít	k5eAaImAgMnP	žít
převážně	převážně	k6eAd1	převážně
usedle	usedle	k6eAd1	usedle
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
kamenné	kamenný	k2eAgInPc4d1	kamenný
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
věnovali	věnovat	k5eAaPmAgMnP	věnovat
se	se	k3xPyFc4	se
jednoduchému	jednoduchý	k2eAgNnSc3d1	jednoduché
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
jen	jen	k9	jen
fragmentárně	fragmentárně	k6eAd1	fragmentárně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
několika	několik	k4yIc2	několik
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
základních	základní	k2eAgFnPc2d1	základní
číslovek	číslovka	k1gFnPc2	číslovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
zbytků	zbytek	k1gInPc2	zbytek
je	být	k5eAaImIp3nS	být
však	však	k9	však
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
berberské	berberský	k2eAgFnSc2d1	berberská
skupiny	skupina	k1gFnSc2	skupina
afroasijské	afroasijský	k2eAgFnSc2d1	afroasijský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Guančové	Guanč	k1gMnPc1	Guanč
měli	mít	k5eAaImAgMnP	mít
světlejší	světlý	k2eAgFnSc4d2	světlejší
pleť	pleť	k1gFnSc4	pleť
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
neznali	neznat	k5eAaImAgMnP	neznat
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
populace	populace	k1gFnSc2	populace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ostrovů	ostrov	k1gInPc2	ostrov
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
byly	být	k5eAaImAgInP	být
izolované	izolovaný	k2eAgInPc1d1	izolovaný
<g/>
.	.	kIx.	.
</s>
<s>
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
známé	známá	k1gFnSc2	známá
díky	díky	k7c3	díky
řeckým	řecký	k2eAgMnPc3d1	řecký
a	a	k8xC	a
římským	římský	k2eAgMnPc3d1	římský
mořeplavcům	mořeplavec	k1gMnPc3	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
150	[number]	k4	150
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
"	"	kIx"	"
<g/>
zapomnělo	zapomenout	k5eAaPmAgNnS	zapomenout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1312	[number]	k4	1312
je	být	k5eAaImIp3nS	být
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
Janovan	Janovan	k1gMnSc1	Janovan
Lanzarotto	Lanzarott	k2eAgNnSc4d1	Lanzarott
Malocello	Malocello	k1gNnSc4	Malocello
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
dopluli	doplout	k5eAaPmAgMnP	doplout
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
Katalánci	Katalánec	k1gMnPc1	Katalánec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
dobyl	dobýt	k5eAaPmAgMnS	dobýt
normanský	normanský	k2eAgMnSc1d1	normanský
král	král	k1gMnSc1	král
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
Béthencourt	Béthencourt	k1gInSc1	Béthencourt
pro	pro	k7c4	pro
Kastilskou	kastilský	k2eAgFnSc4d1	Kastilská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
okolo	okolo	k7c2	okolo
35	[number]	k4	35
000	[number]	k4	000
Guančů	Guanč	k1gInPc2	Guanč
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
především	především	k6eAd1	především
na	na	k7c4	na
Gran	Gran	k1gInSc4	Gran
Canarii	Canarie	k1gFnSc4	Canarie
a	a	k8xC	a
Tenerife	Tenerif	k1gInSc5	Tenerif
(	(	kIx(	(
<g/>
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Palmě	palma	k1gFnSc6	palma
(	(	kIx(	(
<g/>
4	[number]	k4	4
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Hierru	Hierr	k1gInSc2	Hierr
(	(	kIx(	(
<g/>
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbylých	zbylý	k2eAgInPc6d1	zbylý
hlavních	hlavní	k2eAgInPc6d1	hlavní
ostrovech	ostrov	k1gInPc6	ostrov
žilo	žít	k5eAaImAgNnS	žít
jen	jen	k9	jen
pár	pár	k4xCyI	pár
stovek	stovka	k1gFnPc2	stovka
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1479	[number]	k4	1479
získali	získat	k5eAaPmAgMnP	získat
ostrovy	ostrov	k1gInPc4	ostrov
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Portugalci	Portugalec	k1gMnPc1	Portugalec
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Azorské	azorský	k2eAgInPc4d1	azorský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Kapverdy	Kapverda	k1gFnPc4	Kapverda
a	a	k8xC	a
Madeiru	Madeira	k1gFnSc4	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
boj	boj	k1gInSc1	boj
s	s	k7c7	s
Guanči	Guanči	k1gFnSc7	Guanči
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
byly	být	k5eAaImAgInP	být
ostrovy	ostrov	k1gInPc1	ostrov
plně	plně	k6eAd1	plně
pod	pod	k7c7	pod
španělskou	španělský	k2eAgFnSc7d1	španělská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
přístavem	přístav	k1gInSc7	přístav
pro	pro	k7c4	pro
námořníky	námořník	k1gMnPc4	námořník
plující	plující	k2eAgFnSc1d1	plující
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
italský	italský	k2eAgMnSc1d1	italský
historik	historik	k1gMnSc1	historik
Girolamo	Girolama	k1gFnSc5	Girolama
Benzoni	Benzoň	k1gFnSc3	Benzoň
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Guančové	Guančová	k1gFnSc2	Guančová
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
nežijí	žít	k5eNaImIp3nP	žít
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
byli	být	k5eAaImAgMnP	být
prodáni	prodat	k5eAaPmNgMnP	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
asimilovali	asimilovat	k5eAaBmAgMnP	asimilovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
zejména	zejména	k9	zejména
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Gran	Grana	k1gFnPc2	Grana
Canaria	Canarium	k1gNnSc2	Canarium
a	a	k8xC	a
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
zdrojem	zdroj	k1gInSc7	zdroj
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vývoz	vývoz	k1gInSc1	vývoz
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
plantáže	plantáž	k1gFnPc1	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
však	však	k9	však
vznikaly	vznikat	k5eAaImAgFnP	vznikat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
narušení	narušení	k1gNnSc1	narušení
ekosystému	ekosystém	k1gInSc2	ekosystém
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
erozi	eroze	k1gFnSc3	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
častým	častý	k2eAgInPc3d1	častý
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
pirátů	pirát	k1gMnPc2	pirát
a	a	k8xC	a
otrokářů	otrokář	k1gMnPc2	otrokář
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
několik	několik	k4yIc1	několik
obranných	obranný	k2eAgInPc2d1	obranný
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
tvrzí	tvrz	k1gFnPc2	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
admirála	admirál	k1gMnSc4	admirál
Nelsona	Nelson	k1gMnSc4	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
Levnější	levný	k2eAgFnSc1d2	levnější
výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
a	a	k8xC	a
Karibiku	Karibikum	k1gNnSc6	Karibikum
způsobila	způsobit	k5eAaPmAgFnS	způsobit
úpadek	úpadek	k1gInSc4	úpadek
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovarnictví	cukrovarnictví	k1gNnSc1	cukrovarnictví
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
vinařství	vinařství	k1gNnPc4	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
značný	značný	k2eAgInSc4d1	značný
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
vzniku	vznik	k1gInSc2	vznik
konkurence	konkurence	k1gFnSc2	konkurence
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
probíhala	probíhat	k5eAaImAgFnS	probíhat
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
vývozu	vývoz	k1gInSc2	vývoz
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
způsobila	způsobit	k5eAaPmAgFnS	způsobit
téměř	téměř	k6eAd1	téměř
pád	pád	k1gInSc4	pád
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
první	první	k4xOgInSc4	první
vývoz	vývoz	k1gInSc4	vývoz
košenily	košenila	k1gFnSc2	košenila
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgNnSc1d1	přírodní
barvivo	barvivo	k1gNnSc1	barvivo
získané	získaný	k2eAgNnSc1d1	získané
z	z	k7c2	z
hmyzu	hmyz	k1gInSc2	hmyz
využívané	využívaný	k2eAgFnPc1d1	využívaná
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgMnS	stát
významný	významný	k2eAgInSc4d1	významný
vývoz	vývoz	k1gInSc4	vývoz
banánů	banán	k1gInPc2	banán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
soupeření	soupeření	k1gNnSc3	soupeření
mezi	mezi	k7c7	mezi
největšími	veliký	k2eAgInPc7d3	veliký
ostrovy	ostrov	k1gInPc7	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
a	a	k8xC	a
Gran	Gran	k1gInSc4	Gran
Canaria	Canarium	k1gNnSc2	Canarium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
až	až	k6eAd1	až
rozdělením	rozdělení	k1gNnSc7	rozdělení
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
provincie	provincie	k1gFnPc4	provincie
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc4d1	západní
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Gomera	Gomero	k1gNnSc2	Gomero
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Hierro	Hierro	k1gNnSc4	Hierro
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
ostrovy	ostrov	k1gInPc1	ostrov
Gran	Grana	k1gFnPc2	Grana
Canaria	Canarium	k1gNnSc2	Canarium
<g/>
,	,	kIx,	,
Fuerteventura	Fuerteventura	k1gFnSc1	Fuerteventura
a	a	k8xC	a
Lanzarote	Lanzarot	k1gInSc5	Lanzarot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
převzal	převzít	k5eAaPmAgMnS	převzít
moc	moc	k1gFnSc4	moc
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
F.	F.	kA	F.
Franco	Franco	k6eAd1	Franco
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
vyloučení	vyloučení	k1gNnSc1	vyloučení
Španělska	Španělsko	k1gNnSc2	Španělsko
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
emigraci	emigrace	k1gFnSc4	emigrace
do	do	k7c2	do
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
7	[number]	k4	7
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Frankově	Frankův	k2eAgFnSc6d1	Frankova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgInP	získat
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
autonomii	autonomie	k1gFnSc4	autonomie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
zdrojem	zdroj	k1gInSc7	zdroj
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
(	(	kIx(	(
<g/>
na	na	k7c6	na
větších	veliký	k2eAgInPc6d2	veliký
ostrovech	ostrov	k1gInPc6	ostrov
až	až	k9	až
z	z	k7c2	z
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
menších	malý	k2eAgInPc6d2	menší
ostrovech	ostrov	k1gInPc6	ostrov
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
zdroji	zdroj	k1gInPc7	zdroj
především	především	k6eAd1	především
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
ostrovem	ostrov	k1gInSc7	ostrov
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
<s>
Fuerteventura	Fuerteventura	k1gFnSc1	Fuerteventura
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
rozlohou	rozloha	k1gFnSc7	rozloha
<g/>
,	,	kIx,	,
Gran	Gran	k1gMnSc1	Gran
Canaria	Canarium	k1gNnSc2	Canarium
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
ale	ale	k8xC	ale
až	až	k6eAd1	až
třetí	třetí	k4xOgMnSc1	třetí
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
španělských	španělský	k2eAgInPc2d1	španělský
ostrovů	ostrov	k1gInPc2	ostrov
po	po	k7c6	po
Mallorce	Mallorka	k1gFnSc6	Mallorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
hornaté	hornatý	k2eAgInPc1d1	hornatý
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
týká	týkat	k5eAaImIp3nS	týkat
západně	západně	k6eAd1	západně
položených	položený	k2eAgInPc2d1	položený
ostrovů	ostrov	k1gInPc2	ostrov
-	-	kIx~	-
Gran	Gran	k1gInSc1	Gran
Canaria	Canarium	k1gNnSc2	Canarium
<g/>
,	,	kIx,	,
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Gomera	Gomera	k1gFnSc1	Gomera
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Palma	palma	k1gFnSc1	palma
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Hierro	Hierro	k1gNnSc4	Hierro
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnPc1d1	horská
oblasti	oblast	k1gFnPc1	oblast
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vulkány	vulkán	k1gInPc1	vulkán
obvykle	obvykle	k6eAd1	obvykle
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
většinu	většina	k1gFnSc4	většina
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Teide	Teid	k1gInSc5	Teid
na	na	k7c4	na
Tenerife	Tenerif	k1gInSc5	Tenerif
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
3	[number]	k4	3
718	[number]	k4	718
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Fuerteventura	Fuerteventura	k1gFnSc1	Fuerteventura
a	a	k8xC	a
Lanzarote	Lanzarot	k1gInSc5	Lanzarot
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
kopcovité	kopcovitý	k2eAgFnPc1d1	kopcovitá
či	či	k8xC	či
tvořené	tvořený	k2eAgFnPc1d1	tvořená
místy	místy	k6eAd1	místy
vrchovinami	vrchovina	k1gFnPc7	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Fuerteventury	Fuerteventura	k1gFnSc2	Fuerteventura
má	mít	k5eAaImIp3nS	mít
807	[number]	k4	807
m	m	kA	m
<g/>
,	,	kIx,	,
Lanzarote	Lanzarot	k1gInSc5	Lanzarot
670	[number]	k4	670
m.	m.	k?	m.
Na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čtyři	čtyři	k4xCgInPc1	čtyři
z	z	k7c2	z
patnácti	patnáct	k4xCc2	patnáct
španělských	španělský	k2eAgInPc2d1	španělský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
více	hodně	k6eAd2	hodně
známým	známý	k2eAgMnSc7d1	známý
náleží	náležet	k5eAaImIp3nS	náležet
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Teide	Teid	k1gInSc5	Teid
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
španělskou	španělský	k2eAgFnSc7d1	španělská
horou	hora	k1gFnSc7	hora
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Teide	Teid	k1gInSc5	Teid
a	a	k8xC	a
kráterem	kráter	k1gInSc7	kráter
Las	laso	k1gNnPc2	laso
Caňadas	Caňadas	k1gMnSc1	Caňadas
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Timanfaya	Timanfay	k1gInSc2	Timanfay
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lanzarote	Lanzarot	k1gInSc5	Lanzarot
s	s	k7c7	s
takřka	takřka	k6eAd1	takřka
martickou	martický	k2eAgFnSc7d1	martická
krajinou	krajina	k1gFnSc7	krajina
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
výbuchy	výbuch	k1gInPc7	výbuch
vulkánů	vulkán	k1gInPc2	vulkán
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
dvěma	dva	k4xCgFnPc3	dva
parkům	park	k1gInPc3	park
náleží	náležet	k5eAaImIp3nS	náležet
vzácné	vzácný	k2eAgInPc1d1	vzácný
původní	původní	k2eAgInPc1d1	původní
vavřínové	vavřínový	k2eAgInPc1d1	vavřínový
lesy	les	k1gInPc1	les
na	na	k7c4	na
La	la	k1gNnSc4	la
Gomeře	Gomera	k1gFnSc3	Gomera
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
kaldery	kaldera	k1gFnSc2	kaldera
Caldera	Caldera	k1gFnSc1	Caldera
de	de	k?	de
Taburiente	Taburient	k1gInSc5	Taburient
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
La	la	k1gNnSc2	la
Palma	palma	k1gFnSc1	palma
<g/>
.	.	kIx.	.
</s>
<s>
Flóra	Flóra	k1gFnSc1	Flóra
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
velmi	velmi	k6eAd1	velmi
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
,	,	kIx,	,
nechybí	chybit	k5eNaPmIp3nS	chybit
zde	zde	k6eAd1	zde
sopečná	sopečný	k2eAgFnSc1d1	sopečná
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
palmové	palmový	k2eAgFnPc1d1	Palmová
oázy	oáza	k1gFnPc1	oáza
ani	ani	k8xC	ani
písečné	písečný	k2eAgFnPc1d1	písečná
duny	duna	k1gFnPc1	duna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
bývá	bývat	k5eAaImIp3nS	bývat
kromě	kromě	k7c2	kromě
klasického	klasický	k2eAgInSc2d1	klasický
světlého	světlý	k2eAgInSc2d1	světlý
písku	písek	k1gInSc2	písek
i	i	k8xC	i
písek	písek	k1gInSc4	písek
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1	[number]	k4	1
800	[number]	k4	800
rostlinných	rostlinný	k2eAgMnPc2d1	rostlinný
druhů	druh	k1gMnPc2	druh
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
původních	původní	k2eAgMnPc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
borovice	borovice	k1gFnSc2	borovice
kanárská	kanárský	k2eAgFnSc1d1	Kanárská
<g/>
,	,	kIx,	,
palma	palma	k1gFnSc1	palma
kanárská	kanárský	k2eAgFnSc1d1	Kanárská
<g/>
,	,	kIx,	,
jalovec	jalovec	k1gInSc1	jalovec
cedrovitý	cedrovitý	k2eAgInSc1d1	cedrovitý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
ostrovů	ostrov	k1gInPc2	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Gomera	Gomera	k1gFnSc1	Gomera
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Palma	palma	k1gFnSc1	palma
a	a	k8xC	a
Gran	Gran	k1gInSc1	Gran
Canaria	Canarium	k1gNnSc2	Canarium
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vlhké	vlhký	k2eAgInPc1d1	vlhký
lesy	les	k1gInPc1	les
a	a	k8xC	a
lesnatá	lesnatý	k2eAgNnPc1d1	lesnaté
území	území	k1gNnPc1	území
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
fayal-brezal	fayalrezat	k5eAaImAgMnS	fayal-brezat
a	a	k8xC	a
laurisilva	laurisilva	k6eAd1	laurisilva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
neobvyklým	obvyklý	k2eNgInPc3d1	neobvyklý
stromům	strom	k1gInPc3	strom
náleží	náležet	k5eAaImIp3nS	náležet
dračinec	dračinec	k1gInSc1	dračinec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
např.	např.	kA	např.
pryšec	pryšec	k1gInSc4	pryšec
balzámodárný	balzámodárný	k2eAgInSc1d1	balzámodárný
<g/>
,	,	kIx,	,
cesmína	cesmína	k1gFnSc1	cesmína
kanárská	kanárský	k2eAgFnSc1d1	Kanárská
<g/>
,	,	kIx,	,
smil	smil	k1gInSc1	smil
bavlníkový	bavlníkový	k2eAgInSc1d1	bavlníkový
nebo	nebo	k8xC	nebo
asydamie	asydamie	k1gFnSc1	asydamie
širokolistá	širokolistý	k2eAgFnSc1d1	širokolistá
<g/>
.	.	kIx.	.
</s>
<s>
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
2	[number]	k4	2
provincie	provincie	k1gFnSc2	provincie
<g/>
:	:	kIx,	:
Las	laso	k1gNnPc2	laso
Palmas	Palmasa	k1gFnPc2	Palmasa
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
regionu	region	k1gInSc2	region
jsou	být	k5eAaImIp3nP	být
střídavě	střídavě	k6eAd1	střídavě
města	město	k1gNnSc2	město
Santa	Santo	k1gNnSc2	Santo
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
a	a	k8xC	a
Las	laso	k1gNnPc2	laso
Palmas	Palmasa	k1gFnPc2	Palmasa
de	de	k?	de
Gran	Gran	k1gNnSc1	Gran
Canaria	Canarium	k1gNnSc2	Canarium
(	(	kIx(	(
<g/>
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
legislativním	legislativní	k2eAgNnSc6d1	legislativní
období	období	k1gNnSc6	období
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
předseda	předseda	k1gMnSc1	předseda
autonomní	autonomní	k2eAgFnSc2d1	autonomní
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
vicepremiér	vicepremiér	k1gMnSc1	vicepremiér
<g/>
;	;	kIx,	;
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
si	se	k3xPyFc3	se
sídla	sídlo	k1gNnSc2	sídlo
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
spravuje	spravovat	k5eAaImIp3nS	spravovat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
rada	rada	k1gFnSc1	rada
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
cabildo	cabildo	k1gNnSc4	cabildo
insular	insular	k1gInSc1	insular
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
statistickou	statistický	k2eAgFnSc4d1	statistická
jednotku	jednotka	k1gFnSc4	jednotka
NUTS	NUTS	kA	NUTS
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
především	především	k9	především
na	na	k7c6	na
turistickém	turistický	k2eAgInSc6d1	turistický
ruchu	ruch	k1gInSc6	ruch
<g/>
,	,	kIx,	,
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
ostrovy	ostrov	k1gInPc1	ostrov
navštíví	navštívit	k5eAaPmIp3nP	navštívit
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
kuchyně	kuchyně	k1gFnSc2	kuchyně
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnPc1	kukuřice
a	a	k8xC	a
banány	banán	k1gInPc1	banán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
ryb	ryba	k1gFnPc2	ryba
patří	patřit	k5eAaImIp3nS	patřit
sapín	sapín	k1gMnSc1	sapín
<g/>
,	,	kIx,	,
kanín	kanín	k1gMnSc1	kanín
<g/>
,	,	kIx,	,
mořský	mořský	k2eAgMnSc1d1	mořský
okoun	okoun	k1gMnSc1	okoun
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
pražma	pražma	k1gFnSc1	pražma
a	a	k8xC	a
očnatec	očnatec	k1gMnSc1	očnatec
<g/>
.	.	kIx.	.
</s>
<s>
Puchero	Puchero	k1gNnSc1	Puchero
Canario	Canario	k6eAd1	Canario
neboli	neboli	k8xC	neboli
guláš	guláš	k1gInSc4	guláš
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
z	z	k7c2	z
dýně	dýně	k1gFnSc2	dýně
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
sladkých	sladký	k2eAgFnPc2d1	sladká
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
vepřového	vepřové	k1gNnSc2	vepřové
a	a	k8xC	a
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Papas	Papas	k1gMnSc1	Papas
Arrugadas	Arrugadas	k1gMnSc1	Arrugadas
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pomačkáné	pomačkáný	k2eAgFnPc1d1	pomačkáný
brambory	brambora	k1gFnPc1	brambora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
populární	populární	k2eAgFnSc1d1	populární
příloha	příloha	k1gFnSc1	příloha
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
brambor	brambora	k1gFnPc2	brambora
vařených	vařený	k2eAgFnPc2d1	vařená
doměkka	doměkka	k6eAd1	doměkka
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Conejo	Conejo	k1gMnSc1	Conejo
al	ala	k1gFnPc2	ala
salmorejo	salmorejo	k1gMnSc1	salmorejo
neboli	neboli	k8xC	neboli
dušený	dušený	k2eAgMnSc1d1	dušený
králík	králík	k1gMnSc1	králík
s	s	k7c7	s
rajčaty	rajče	k1gNnPc7	rajče
<g/>
,	,	kIx,	,
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
přílohou	příloha	k1gFnSc7	příloha
pokrmu	pokrm	k1gInSc2	pokrm
jsou	být	k5eAaImIp3nP	být
Papas	Papas	k1gInSc4	Papas
Arrugadas	Arrugadasa	k1gFnPc2	Arrugadasa
<g/>
.	.	kIx.	.
</s>
<s>
Bienmesab	Bienmesab	k1gInSc1	Bienmesab
-	-	kIx~	-
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
si	se	k3xPyFc3	se
španělé	španělý	k2eAgFnPc1d1	španělý
představují	představovat	k5eAaImIp3nP	představovat
velice	velice	k6eAd1	velice
chutný	chutný	k2eAgInSc4d1	chutný
dezert	dezert	k1gInSc4	dezert
s	s	k7c7	s
mandlovým	mandlový	k2eAgInSc7d1	mandlový
krémem	krém	k1gInSc7	krém
z	z	k7c2	z
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
mletých	mletý	k2eAgFnPc2d1	mletá
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
skořice	skořice	k1gFnSc2	skořice
a	a	k8xC	a
žloutků	žloutek	k1gInPc2	žloutek
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
vyráběné	vyráběný	k2eAgNnSc1d1	vyráběné
víno	víno	k1gNnSc1	víno
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
u	u	k7c2	u
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
víny	vína	k1gFnPc4	vína
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tradiční	tradiční	k2eAgNnSc4d1	tradiční
sladké	sladký	k2eAgNnSc4d1	sladké
víno	víno	k1gNnSc4	víno
Malvaz	malvaz	k1gInSc1	malvaz
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lanzarote	Lanzarot	k1gInSc5	Lanzarot
v	v	k7c6	v
sopečných	sopečný	k2eAgFnPc6d1	sopečná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
deset	deset	k4xCc1	deset
původních	původní	k2eAgNnPc2d1	původní
označení	označení	k1gNnPc2	označení
pro	pro	k7c4	pro
vína	víno	k1gNnPc4	víno
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
:	:	kIx,	:
Abona	Abono	k1gNnSc2	Abono
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Hierro	Hierro	k1gNnSc1	Hierro
<g/>
,	,	kIx,	,
Lanzarote	Lanzarot	k1gMnSc5	Lanzarot
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
Tacoronte-Acentejo	Tacoronte-Acentejo	k1gNnSc1	Tacoronte-Acentejo
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
