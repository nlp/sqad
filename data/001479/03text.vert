<s>
Antoine	Antoinout	k5eAaPmIp3nS	Antoinout
Henri	Henre	k1gFnSc4	Henre
Becquerel	becquerel	k1gInSc1	becquerel
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Croisic	Croisic	k1gMnSc1	Croisic
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
za	za	k7c4	za
objev	objev	k1gInSc4	objev
přirozené	přirozený	k2eAgFnSc2d1	přirozená
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Henri	Henri	k1gNnSc1	Henri
Becquerel	becquerel	k1gInSc1	becquerel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
pařížského	pařížský	k2eAgNnSc2d1	pařížské
Muzea	muzeum	k1gNnSc2	muzeum
přírodních	přírodní	k2eAgFnPc2d1	přírodní
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
Muséum	Muséum	k1gInSc1	Muséum
national	nationat	k5eAaImAgMnS	nationat
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
histoire	histoir	k1gMnSc5	histoir
naturelle	naturell	k1gMnSc5	naturell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
jeho	jeho	k3xOp3gFnSc6	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Edmond	Edmond	k1gMnSc1	Edmond
Becquerel	becquerel	k1gInSc4	becquerel
<g/>
,	,	kIx,	,
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
Antoine-César	Antoine-César	k1gMnSc1	Antoine-César
Becquerel	becquerel	k1gInSc1	becquerel
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
lyceu	lyceum	k1gNnSc6	lyceum
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Velikého	veliký	k2eAgInSc2d1	veliký
(	(	kIx(	(
<g/>
Lycée	Lycé	k1gInSc2	Lycé
Louis-le-Grand	Louise-Grand	k1gInSc1	Louis-le-Grand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
mj.	mj.	kA	mj.
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
matematice	matematika	k1gFnSc3	matematika
Gaston	Gaston	k1gInSc4	Gaston
Darboux	Darboux	k1gInSc1	Darboux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
Polytechnickou	polytechnický	k2eAgFnSc4d1	polytechnická
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
École	École	k1gFnSc1	École
Polytechnique	Polytechniqu	k1gFnSc2	Polytechniqu
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
započal	započnout	k5eAaPmAgInS	započnout
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
inženýrské	inženýrský	k2eAgFnSc6d1	inženýrská
Škole	škola	k1gFnSc6	škola
mostů	most	k1gInPc2	most
a	a	k8xC	a
silnic	silnice	k1gFnPc2	silnice
(	(	kIx(	(
<g/>
École	École	k1gFnSc1	École
Nationale	Nationale	k1gFnSc2	Nationale
des	des	k1gNnSc1	des
Ponts	Ponts	k1gInSc1	Ponts
et	et	k?	et
Chaussées	Chaussées	k1gInSc1	Chaussées
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
také	také	k9	také
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Jaminovou	Jaminová	k1gFnSc7	Jaminová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
Polytechnické	polytechnický	k2eAgFnSc6d1	polytechnická
škole	škola	k1gFnSc6	škola
Julese	Julese	k1gFnSc1	Julese
Jamina	Jamina	k1gFnSc1	Jamina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
obdržel	obdržet	k5eAaPmAgInS	obdržet
inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
diplom	diplom	k1gInSc1	diplom
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
optikou	optika	k1gFnSc7	optika
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
polarizaci	polarizace	k1gFnSc4	polarizace
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
také	také	k9	také
infračervené	infračervený	k2eAgNnSc4d1	infračervené
spektrum	spektrum	k1gNnSc4	spektrum
par	para	k1gFnPc2	para
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
absorpci	absorpce	k1gFnSc4	absorpce
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
krystalech	krystal	k1gInPc6	krystal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
získal	získat	k5eAaPmAgInS	získat
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předtím	předtím	k6eAd1	předtím
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
i	i	k8xC	i
děd	děd	k1gMnSc1	děd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Louise	Louis	k1gMnSc2	Louis
Désirée	Désirée	k1gFnSc1	Désirée
Lorieux	Lorieux	k1gInSc1	Lorieux
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
Polytechnické	polytechnický	k2eAgFnSc6d1	polytechnická
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
fluorescence	fluorescence	k1gFnSc2	fluorescence
uranových	uranový	k2eAgFnPc2d1	uranová
solí	sůl	k1gFnPc2	sůl
objevil	objevit	k5eAaPmAgInS	objevit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Becquerel	becquerel	k1gInSc1	becquerel
zvolen	zvolit	k5eAaPmNgInS	zvolit
stálým	stálý	k2eAgMnSc7d1	stálý
tajemníkem	tajemník	k1gMnSc7	tajemník
Académie	Académie	k1gFnSc2	Académie
des	des	k1gNnPc2	des
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
.	.	kIx.	.
</s>
<s>
Henri	Henri	k6eAd1	Henri
Becquerel	becquerel	k1gInSc1	becquerel
zemřel	zemřít	k5eAaPmAgInS	zemřít
předčasně	předčasně	k6eAd1	předčasně
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
56	[number]	k4	56
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jednotka	jednotka	k1gFnSc1	jednotka
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
becquerel	becquerel	k1gInSc1	becquerel
<g/>
,	,	kIx,	,
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
a	a	k8xC	a
kráter	kráter	k1gInSc4	kráter
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgNnPc1d1	získané
ocenění	ocenění	k1gNnPc1	ocenění
<g/>
:	:	kIx,	:
Rumfordova	Rumfordův	k2eAgFnSc1d1	Rumfordova
Medaile	medaile	k1gFnSc1	medaile
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
Helmholtz	Helmholtz	k1gMnSc1	Helmholtz
Medal	Medal	k1gMnSc1	Medal
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Barnard	Barnard	k1gMnSc1	Barnard
Medal	Medal	k1gMnSc1	Medal
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
Becquerel	becquerel	k1gInSc1	becquerel
zabýval	zabývat	k5eAaImAgInS	zabývat
optickými	optický	k2eAgFnPc7d1	optická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgInS	hledat
Becquerel	becquerel	k1gInSc1	becquerel
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
tímto	tento	k3xDgNnSc7	tento
zářením	záření	k1gNnSc7	záření
a	a	k8xC	a
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
fluorescencí	fluorescence	k1gFnSc7	fluorescence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Becquerel	becquerel	k1gInSc1	becquerel
studoval	studovat	k5eAaImAgInS	studovat
fluorescenci	fluorescence	k1gFnSc4	fluorescence
uranových	uranový	k2eAgFnPc2d1	uranová
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
víceméně	víceméně	k9	víceméně
náhodou	náhodou	k6eAd1	náhodou
objevil	objevit	k5eAaPmAgMnS	objevit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Vložil	vložit	k5eAaPmAgInS	vložit
fluorescenční	fluorescenční	k2eAgInSc1d1	fluorescenční
minerál	minerál	k1gInSc1	minerál
mezi	mezi	k7c4	mezi
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
se	s	k7c7	s
solemi	sůl	k1gFnPc7	sůl
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
chemickým	chemický	k2eAgFnPc3d1	chemická
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nebyla	být	k5eNaImAgFnS	být
ozářena	ozářit	k5eAaPmNgFnS	ozářit
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
soli	sůl	k1gFnPc1	sůl
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
záření	záření	k1gNnSc2	záření
jiné	jiný	k2eAgFnSc2d1	jiná
než	než	k8xS	než
světelné	světelný	k2eAgFnSc2d1	světelná
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
uvedl	uvést	k5eAaPmAgInS	uvést
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
závěrům	závěr	k1gInPc3	závěr
dospěl	dochvít	k5eAaPmAgInS	dochvít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Silvanus	Silvanus	k1gMnSc1	Silvanus
P.	P.	kA	P.
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
Rumfordovu	Rumfordův	k2eAgFnSc4d1	Rumfordova
medaili	medaile	k1gFnSc4	medaile
(	(	kIx(	(
<g/>
Rumford	Rumford	k1gMnSc1	Rumford
Medal	Medal	k1gMnSc1	Medal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
tohoto	tento	k3xDgNnSc2	tento
nově	nově	k6eAd1	nově
objeveného	objevený	k2eAgNnSc2d1	objevené
záření	záření	k1gNnSc2	záření
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
zvolila	zvolit	k5eAaPmAgFnS	zvolit
za	za	k7c4	za
téma	téma	k1gNnSc4	téma
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
Marie	Maria	k1gFnSc2	Maria
Curie	curie	k1gNnSc1	curie
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
jeho	jeho	k3xOp3gMnSc2	jeho
kolegy	kolega	k1gMnSc2	kolega
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc5	Curie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
výzkumu	výzkum	k1gInSc2	výzkum
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
vlastností	vlastnost	k1gFnSc7	vlastnost
více	hodně	k6eAd2	hodně
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
nazvala	nazvat	k5eAaPmAgFnS	nazvat
tuto	tento	k3xDgFnSc4	tento
jejich	jejich	k3xOp3gFnSc1	jejich
vlastnost	vlastnost	k1gFnSc1	vlastnost
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Curieovi	Curieův	k2eAgMnPc1d1	Curieův
později	pozdě	k6eAd2	pozdě
objevili	objevit	k5eAaPmAgMnP	objevit
ještě	ještě	k6eAd1	ještě
prvky	prvek	k1gInPc1	prvek
polonium	polonium	k1gNnSc1	polonium
a	a	k8xC	a
radium	radium	k1gNnSc1	radium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
objevy	objev	k1gInPc4	objev
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
