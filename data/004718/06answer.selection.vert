<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Kierling	Kierling	k1gInSc1	Kierling
u	u	k7c2	u
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
;	;	kIx,	;
zřídka	zřídka	k6eAd1	zřídka
též	též	k9	též
František	František	k1gMnSc1	František
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
židovským	židovský	k2eAgNnSc7d1	Židovské
jménem	jméno	k1gNnSc7	jméno
Anschel	Anschela	k1gFnPc2	Anschela
<g/>
,	,	kIx,	,
א	א	k?	א
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
pražský	pražský	k2eAgMnSc1d1	pražský
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
literárně	literárně	k6eAd1	literárně
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Pražského	pražský	k2eAgInSc2d1	pražský
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
