<s>
Myspace	Myspace	k1gFnSc1	Myspace
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
MySpace	MySpace	k1gFnSc1	MySpace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
komunitní	komunitní	k2eAgInSc1d1	komunitní
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Většinovým	většinový	k2eAgMnSc7d1	většinový
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Specific	Specifice	k1gInPc2	Specifice
Media	medium	k1gNnSc2	medium
<g/>
.	.	kIx.	.
</s>
<s>
Myspace	Myspace	k1gFnSc1	Myspace
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
komunitních	komunitní	k2eAgInPc2d1	komunitní
serverů	server	k1gInPc2	server
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
sloganem	slogan	k1gInSc7	slogan
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
A	a	k8xC	a
place	plac	k1gInSc6	plac
for	forum	k1gNnPc2	forum
friends	friends	k6eAd1	friends
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
přátele	přítel	k1gMnPc4	přítel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgInPc2d1	klasický
profilů	profil	k1gInPc2	profil
se	se	k3xPyFc4	se
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
profily	profil	k1gInPc4	profil
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
filmařů	filmař	k1gMnPc2	filmař
nebo	nebo	k8xC	nebo
herců	herec	k1gMnPc2	herec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
server	server	k1gInSc1	server
přístupný	přístupný	k2eAgInSc1d1	přístupný
pouze	pouze	k6eAd1	pouze
lidem	lid	k1gInSc7	lid
starším	starý	k2eAgFnPc3d2	starší
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Myspace	Myspace	k1gFnSc1	Myspace
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
Instant	Instant	k1gInSc4	Instant
Messenger	Messenger	k1gMnSc1	Messenger
MySpaceIM	MySpaceIM	k1gMnSc1	MySpaceIM
<g/>
.	.	kIx.	.
</s>
<s>
MySpaceIM	MySpaceIM	k?	MySpaceIM
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
program	program	k1gInSc4	program
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgFnPc2d1	klasická
funkcí	funkce	k1gFnPc2	funkce
Instant	Instant	k1gInSc1	Instant
Messengeru	Messenger	k1gInSc2	Messenger
umí	umět	k5eAaImIp3nS	umět
navíc	navíc	k6eAd1	navíc
informovat	informovat	k5eAaBmF	informovat
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
na	na	k7c4	na
MySpace	MySpace	k1gFnPc4	MySpace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
příchozí	příchozí	k1gMnPc4	příchozí
soukromé	soukromý	k2eAgFnSc2d1	soukromá
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInSc2	komentář
či	či	k8xC	či
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
serveru	server	k1gInSc2	server
patří	patřit	k5eAaImIp3nP	patřit
chat	chata	k1gFnPc2	chata
<g/>
,	,	kIx,	,
diskusní	diskusní	k2eAgFnPc1d1	diskusní
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc1	sdílení
videí	video	k1gNnPc2	video
<g/>
,	,	kIx,	,
seznamka	seznamka	k1gFnSc1	seznamka
a	a	k8xC	a
blogy	bloga	k1gFnPc1	bloga
<g/>
.	.	kIx.	.
</s>
<s>
Myspace	Myspace	k1gFnPc1	Myspace
byl	být	k5eAaImAgInS	být
koupen	koupit	k5eAaPmNgInS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
za	za	k7c4	za
580	[number]	k4	580
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
společností	společnost	k1gFnPc2	společnost
News	Newsa	k1gFnPc2	Newsa
Corporation	Corporation	k1gInSc1	Corporation
mediálního	mediální	k2eAgMnSc2d1	mediální
magnáta	magnát	k1gMnSc2	magnát
Ruperta	Rupert	k1gMnSc2	Rupert
Murdocha	Murdoch	k1gMnSc2	Murdoch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
vzestupu	vzestup	k1gInSc3	vzestup
Facebooku	Facebook	k1gInSc2	Facebook
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
server	server	k1gInSc4	server
a	a	k8xC	a
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
News	News	k1gInSc4	News
Corporation	Corporation	k1gInSc4	Corporation
prodala	prodat	k5eAaPmAgFnS	prodat
95	[number]	k4	95
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
Specific	Specifice	k1gInPc2	Specifice
Media	medium	k1gNnSc2	medium
za	za	k7c4	za
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
