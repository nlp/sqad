<s>
Pošta	pošta	k1gFnSc1	pošta
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
logistiku	logistika	k1gFnSc4	logistika
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
,	,	kIx,	,
listovním	listovní	k2eAgFnPc3d1	listovní
službám	služba	k1gFnPc3	služba
<g/>
,	,	kIx,	,
rozesílání	rozesílání	k1gNnSc3	rozesílání
zásilek	zásilka	k1gFnPc2	zásilka
<g/>
,	,	kIx,	,
převodům	převod	k1gInPc3	převod
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
provozovány	provozovat	k5eAaImNgFnP	provozovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
správné	správný	k2eAgNnSc1d1	správné
fungování	fungování	k1gNnSc1	fungování
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
tuzemské	tuzemský	k2eAgNnSc4d1	tuzemské
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
společenství	společenství	k1gNnSc4	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
poštovní	poštovní	k2eAgInPc4d1	poštovní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
které	který	k3yQgFnPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
styk	styk	k1gInSc4	styk
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInPc7	jejich
poštovními	poštovní	k2eAgInPc7d1	poštovní
úřady	úřad	k1gInPc7	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
výměna	výměna	k1gFnSc1	výměna
pošty	pošta	k1gFnSc2	pošta
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
fungující	fungující	k2eAgFnSc7d1	fungující
službou	služba	k1gFnSc7	služba
i	i	k9	i
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
vojenských	vojenský	k2eAgInPc2d1	vojenský
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
nepřátelskými	přátelský	k2eNgInPc7d1	nepřátelský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
války	válka	k1gFnSc2	válka
bývají	bývat	k5eAaImIp3nP	bývat
zřízeny	zřízen	k2eAgInPc1d1	zřízen
specializované	specializovaný	k2eAgInPc1d1	specializovaný
poštovní	poštovní	k2eAgInPc1d1	poštovní
úřady	úřad	k1gInPc1	úřad
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
polní	polní	k2eAgFnSc1d1	polní
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
Dějiny	dějiny	k1gFnPc1	dějiny
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
poštovnictví	poštovnictví	k1gNnPc2	poštovnictví
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
výměny	výměna	k1gFnSc2	výměna
zpráv	zpráva	k1gFnPc2	zpráva
spadají	spadat	k5eAaPmIp3nP	spadat
už	už	k9	už
do	do	k7c2	do
starověkých	starověký	k2eAgFnPc2d1	starověká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pošta	pošta	k1gFnSc1	pošta
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
termínu	termín	k1gInSc2	termín
statio	statio	k1gMnSc1	statio
posita	posita	k1gMnSc1	posita
(	(	kIx(	(
<g/>
umístěná	umístěný	k2eAgFnSc1d1	umístěná
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označoval	označovat	k5eAaImAgMnS	označovat
domy	dům	k1gInPc4	dům
postavené	postavený	k2eAgInPc4d1	postavený
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
Římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgFnSc6d1	vybavená
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
doručovaly	doručovat	k5eAaImAgFnP	doručovat
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
náklad	náklad	k1gInSc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
započal	započnout	k5eAaPmAgInS	započnout
rozvoj	rozvoj	k1gInSc1	rozvoj
poštovnictví	poštovnictví	k1gNnSc2	poštovnictví
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Organizované	organizovaný	k2eAgFnSc2d1	organizovaná
dopravy	doprava	k1gFnSc2	doprava
zpráv	zpráva	k1gFnPc2	zpráva
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
arciknížete	arcikníže	k1gMnSc2	arcikníže
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
z	z	k7c2	z
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
zemi	zem	k1gFnSc6	zem
regulérní	regulérní	k2eAgFnSc1d1	regulérní
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
Habsburk	Habsburk	k1gMnSc1	Habsburk
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
důležité	důležitý	k2eAgNnSc1d1	důležité
období	období	k1gNnSc1	období
rozvoje	rozvoj	k1gInSc2	rozvoj
pošt	pošta	k1gFnPc2	pošta
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nastalo	nastat	k5eAaPmAgNnS	nastat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
převzat	převzat	k2eAgInSc4d1	převzat
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
využil	využít	k5eAaPmAgMnS	využít
služeb	služba	k1gFnPc2	služba
i	i	k8xC	i
zkušeností	zkušenost	k1gFnPc2	zkušenost
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
rodiny	rodina	k1gFnSc2	rodina
Taxisů	Taxis	k1gMnPc2	Taxis
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
poštovní	poštovní	k2eAgFnSc4d1	poštovní
přepravu	přeprava	k1gFnSc4	přeprava
takřka	takřka	k6eAd1	takřka
monopolně	monopolně	k6eAd1	monopolně
organizovali	organizovat	k5eAaBmAgMnP	organizovat
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
rozvětvené	rozvětvený	k2eAgFnSc2d1	rozvětvená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
dvorskému	dvorský	k2eAgMnSc3d1	dvorský
poštmistru	poštmistr	k1gMnSc3	poštmistr
Antoniu	Antonio	k1gMnSc3	Antonio
Taxisovi	Taxis	k1gMnSc3	Taxis
uložil	uložit	k5eAaPmAgMnS	uložit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yInSc4	co
nejrychleji	rychle	k6eAd3	rychle
zřídil	zřídit	k5eAaPmAgMnS	zřídit
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
chtěl	chtít	k5eAaImAgInS	chtít
mít	mít	k5eAaImF	mít
co	co	k9	co
nejčerstvější	čerstvý	k2eAgFnPc4d3	nejčerstvější
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
jeho	jeho	k3xOp3gFnSc1	jeho
volební	volební	k2eAgFnSc1d1	volební
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
provizoriu	provizorium	k1gNnSc6	provizorium
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
stálých	stálý	k2eAgFnPc2d1	stálá
poštovních	poštovní	k2eAgFnPc2d1	poštovní
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
vyměňovali	vyměňovat	k5eAaImAgMnP	vyměňovat
koně	kůň	k1gMnSc2	kůň
jízdních	jízdní	k2eAgInPc2d1	jízdní
kurýrů	kurýr	k1gMnPc2	kurýr
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
poštovní	poštovní	k2eAgFnSc1d1	poštovní
trasa	trasa	k1gFnSc1	trasa
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Vídní	Vídeň	k1gFnPc2	Vídeň
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
Tábor	Tábor	k1gInSc4	Tábor
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
vsi	ves	k1gFnSc2	ves
Košic	Košice	k1gInPc2	Košice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozdělovala	rozdělovat	k5eAaImAgFnS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
mířila	mířit	k5eAaImAgFnS	mířit
na	na	k7c4	na
Linec	Linec	k1gInSc4	Linec
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
větev	větev	k1gFnSc1	větev
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
Jindřichův	Jindřichův	k2eAgInSc4d1	Jindřichův
Hradec	Hradec	k1gInSc4	Hradec
a	a	k8xC	a
Slavonice	Slavonice	k1gFnPc4	Slavonice
dále	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vrchním	vrchní	k2eAgMnSc7d1	vrchní
pražským	pražský	k2eAgMnSc7d1	pražský
poštmistrem	poštmistr	k1gMnSc7	poštmistr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
synovec	synovec	k1gMnSc1	synovec
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
Antonia	Antonio	k1gMnSc2	Antonio
Taxise	Taxis	k1gMnSc2	Taxis
Ambrož	Ambrož	k1gMnSc1	Ambrož
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
Kryštof	Kryštof	k1gMnSc1	Kryštof
Taxis	Taxis	k1gMnSc1	Taxis
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc1d1	poštovní
monopol	monopol	k1gInSc1	monopol
rodiny	rodina	k1gFnSc2	rodina
Taxisů	Taxis	k1gMnPc2	Taxis
trval	trvat	k5eAaImAgInS	trvat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1564	[number]	k4	1564
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Kryštof	Kryštof	k1gMnSc1	Kryštof
Taxis	Taxis	k1gMnSc1	Taxis
pro	pro	k7c4	pro
dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
nepořádky	nepořádek	k1gInPc4	nepořádek
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Bílé	bílý	k2eAgFnSc6d1	bílá
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
české	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgMnPc2d1	jiný
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
přepravou	přeprava	k1gFnSc7	přeprava
soukromých	soukromý	k2eAgInPc2d1	soukromý
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
poštou	pošta	k1gFnSc7	pošta
přepravovány	přepravován	k2eAgFnPc1d1	přepravována
i	i	k8xC	i
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Dálkové	dálkový	k2eAgNnSc1d1	dálkové
spojení	spojení	k1gNnSc1	spojení
bylo	být	k5eAaImAgNnS	být
navázáno	navázat	k5eAaPmNgNnS	navázat
s	s	k7c7	s
Vratislaví	Vratislav	k1gFnSc7	Vratislav
<g/>
,	,	kIx,	,
Benátkami	Benátky	k1gFnPc7	Benátky
<g/>
,	,	kIx,	,
Bruselem	Brusel	k1gInSc7	Brusel
<g/>
,	,	kIx,	,
Paříží	Paříž	k1gFnSc7	Paříž
a	a	k8xC	a
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
získali	získat	k5eAaPmAgMnP	získat
úřad	úřad	k1gInSc4	úřad
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
dvorského	dvorský	k2eAgMnSc2d1	dvorský
poštmistra	poštmistr	k1gMnSc2	poštmistr
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jako	jako	k9	jako
dědičné	dědičný	k2eAgNnSc4d1	dědičné
léno	léno	k1gNnSc4	léno
(	(	kIx(	(
<g/>
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
příslušníci	příslušník	k1gMnPc1	příslušník
rodiny	rodina	k1gFnSc2	rodina
Paarů	Paar	k1gMnPc2	Paar
<g/>
.	.	kIx.	.
</s>
<s>
Vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
jej	on	k3xPp3gMnSc4	on
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1722	[number]	k4	1722
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
pošta	pošta	k1gFnSc1	pošta
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
zestátněna	zestátněn	k2eAgFnSc1d1	zestátněna
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
veřejní	veřejný	k2eAgMnPc1d1	veřejný
poštovní	poštovní	k2eAgMnSc1d1	poštovní
operátoři	operátor	k1gMnPc1	operátor
(	(	kIx(	(
<g/>
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
sdruženi	sdružit	k5eAaPmNgMnP	sdružit
ve	v	k7c6	v
Světové	světový	k2eAgFnSc6d1	světová
poštovní	poštovní	k2eAgFnSc6d1	poštovní
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
provozu	provoz	k1gInSc6	provoz
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
Akty	akt	k1gInPc1	akt
Světové	světový	k2eAgFnSc2d1	světová
poštovní	poštovní	k2eAgFnSc2d1	poštovní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
aktualizovány	aktualizovat	k5eAaBmNgFnP	aktualizovat
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
Kongresech	kongres	k1gInPc6	kongres
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
naposledy	naposledy	k6eAd1	naposledy
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
2012	[number]	k4	2012
<g/>
;	;	kIx,	;
budoucí	budoucí	k2eAgFnSc1d1	budoucí
20.9	[number]	k4	20.9
-	-	kIx~	-
7.10	[number]	k4	7.10
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
,	,	kIx,	,
Závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
Generální	generální	k2eAgInSc1d1	generální
řád	řád	k1gInSc1	řád
Světové	světový	k2eAgFnSc2d1	světová
poštovní	poštovní	k2eAgFnSc2d1	poštovní
unie	unie	k1gFnSc2	unie
Činnost	činnost	k1gFnSc1	činnost
pošty	pošta	k1gFnSc2	pošta
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
regulována	regulován	k2eAgFnSc1d1	regulována
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
těmito	tento	k3xDgFnPc7	tento
<g />
.	.	kIx.	.
</s>
<s>
zákony	zákon	k1gInPc1	zákon
<g/>
:	:	kIx,	:
1838	[number]	k4	1838
-	-	kIx~	-
císařský	císařský	k2eAgInSc4d1	císařský
patent	patent	k1gInSc4	patent
č.	č.	k?	č.
47	[number]	k4	47
sb	sb	kA	sb
<g/>
.	.	kIx.	.
zák	zák	k?	zák
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
a	a	k8xC	a
č.	č.	k?	č.
240	[number]	k4	240
sb	sb	kA	sb
<g/>
.	.	kIx.	.
zák	zák	k?	zák
<g/>
.	.	kIx.	.
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
poštovní	poštovní	k2eAgInSc1d1	poštovní
zákon	zákon	k1gInSc1	zákon
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
-	-	kIx~	-
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
222	[number]	k4	222
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1946	[number]	k4	1946
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
poště	pošta	k1gFnSc6	pošta
(	(	kIx(	(
<g/>
poštovní	poštovní	k2eAgInSc4d1	poštovní
zákon	zákon	k1gInSc4	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
-	-	kIx~	-
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
151	[number]	k4	151
<g/>
/	/	kIx~	/
<g/>
1949	[number]	k4	1949
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
Československé	československý	k2eAgFnSc6d1	Československá
poště	pošta	k1gFnSc6	pošta
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc6d1	národní
podniku	podnik	k1gInSc6	podnik
-	-	kIx~	-
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
Československá	československý	k2eAgFnSc1d1	Československá
pošta	pošta	k1gFnSc1	pošta
vzniká	vznikat	k5eAaImIp3nS	vznikat
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
<g/>
"	"	kIx"	"
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
-	-	kIx~	-
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ministra	ministr	k1gMnSc2	ministr
hospodářství	hospodářství	k1gNnSc2	hospodářství
č.	č.	k?	č.
378	[number]	k4	378
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
odstavce	odstavec	k1gInSc2	odstavec
II	II	kA	II
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Správa	správa	k1gFnSc1	správa
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
SPT	SPT	kA	SPT
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
podniky	podnik	k1gInPc4	podnik
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
a	a	k8xC	a
SPT	SPT	kA	SPT
Telecom	Telecom	k1gInSc1	Telecom
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
-	-	kIx~	-
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
29	[number]	k4	29
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
poštovních	poštovní	k2eAgFnPc6d1	poštovní
službách	služba	k1gFnPc6	služba
Na	na	k7c6	na
poštovních	poštovní	k2eAgFnPc6d1	poštovní
zásilkách	zásilka	k1gFnPc6	zásilka
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
údaji	údaj	k1gInPc7	údaj
<g/>
,	,	kIx,	,
upřesňujícími	upřesňující	k2eAgInPc7d1	upřesňující
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
čas	čas	k1gInSc4	čas
dodání	dodání	k1gNnSc2	dodání
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
informujícími	informující	k2eAgFnPc7d1	informující
o	o	k7c6	o
odesílateli	odesílatel	k1gMnSc6	odesílatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
styku	styk	k1gInSc6	styk
to	ten	k3xDgNnSc1	ten
bývají	bývat	k5eAaImIp3nP	bývat
například	například	k6eAd1	například
následující	následující	k2eAgInPc1d1	následující
francouzské	francouzský	k2eAgInPc1d1	francouzský
nápisy	nápis	k1gInPc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Á	á	k0	á
remettre	remettr	k1gMnSc5	remettr
en	en	k?	en
main	maina	k1gFnPc2	maina
propre	propr	k1gInSc5	propr
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Dodání	dodání	k1gNnSc1	dodání
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rukou	ruka	k1gFnPc2	ruka
adresáta	adresát	k1gMnSc2	adresát
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Avis	aviso	k1gNnPc2	aviso
de	de	k?	de
réception	réception	k1gInSc1	réception
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Dodejka	Dodejka	k1gFnSc1	Dodejka
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Colis	Colis	k1gInSc1	Colis
exprè	exprè	k?	exprè
<g/>
"	"	kIx"	"
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Spěšnina	spěšnina	k1gFnSc1	spěšnina
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Destinataire	Destinatair	k1gMnSc5	Destinatair
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Adresát	adresát	k1gMnSc1	adresát
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Économique	Économique	k1gFnSc1	Économique
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
En	En	k1gFnSc1	En
mains	mainsa	k1gFnPc2	mainsa
propres	propresa	k1gFnPc2	propresa
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rukou	ruka	k1gFnPc2	ruka
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Expéditeur	Expéditeur	k1gMnSc1	Expéditeur
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Fragile	Fragila	k1gFnSc3	Fragila
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Křehký	křehký	k2eAgMnSc1d1	křehký
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Non	Non	k1gFnSc2	Non
reclamé	reclamý	k2eAgFnSc2d1	reclamý
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Nevyžádáno	vyžádán	k2eNgNnSc1d1	vyžádán
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Par	para	k1gFnPc2	para
avion	avion	k1gInSc1	avion
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Letecky	letecky	k6eAd1	letecky
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Prioritaire	Prioritair	k1gMnSc5	Prioritair
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Přednostní	přednostní	k2eAgFnSc4d1	přednostní
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Recommandé	Recommandý	k2eAgInPc4d1	Recommandý
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Doporučeně	doporučeně	k6eAd1	doporučeně
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Remboursement	Remboursement	k1gInSc1	Remboursement
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Dobírka	dobírka	k1gFnSc1	dobírka
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Télégramme	Télégramme	k1gFnSc1	Télégramme
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Telegram	telegram	k1gInSc1	telegram
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Urgent	urgent	k1gMnSc1	urgent
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Pilný	pilný	k2eAgMnSc1d1	pilný
<g/>
"	"	kIx"	"
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ohradě	ohrada	k1gFnSc6	ohrada
měst	město	k1gNnPc2	město
a	a	k8xC	a
městských	městský	k2eAgFnPc6d1	městská
zdech	zeď	k1gFnPc6	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Wintra	Winter	k1gMnSc2	Winter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
poslové	posel	k1gMnPc1	posel
před	před	k7c7	před
třemi	tři	k4xCgInPc7	tři
věky	věk	k1gInPc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
KUNA	Kuna	k1gMnSc1	Kuna
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
poštovnictví	poštovnictví	k1gNnSc2	poštovnictví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
Poštovního	poštovní	k2eAgInSc2d1	poštovní
úřadu	úřad	k1gInSc2	úřad
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
pošty	pošta	k1gFnSc2	pošta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
zahájení	zahájení	k1gNnSc2	zahájení
železniční	železniční	k2eAgFnSc2d1	železniční
přepravy	přeprava	k1gFnSc2	přeprava
a	a	k8xC	a
zavedení	zavedení	k1gNnSc4	zavedení
vlakových	vlakový	k2eAgFnPc2d1	vlaková
pošt	pošta	k1gFnPc2	pošta
<g/>
..	..	k?	..
ZÁBĚHLICKÝ	záběhlický	k2eAgInSc1d1	záběhlický
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
telegrafu	telegraf	k1gInSc2	telegraf
a	a	k8xC	a
telefonu	telefon	k1gInSc2	telefon
v	v	k7c6	v
československých	československý	k2eAgFnPc6d1	Československá
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
až	až	k9	až
do	do	k7c2	do
převratu	převrat	k1gInSc2	převrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
n.	n.	k?	n.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1928	[number]	k4	1928
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
422	[number]	k4	422
s.	s.	k?	s.
KRATOCHVÍL	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgFnPc1d1	Pražská
pošty	pošta	k1gFnPc1	pošta
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7277-405-0	[number]	k4	978-80-7277-405-0
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pošta	pošta	k1gFnSc1	pošta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pošta	pošta	k1gFnSc1	pošta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
