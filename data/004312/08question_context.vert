<s>
Jednomu	jeden	k4xCgMnSc3
z	z	k7c2
členů	člen	k1gMnPc2
rozvětvené	rozvětvený	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
nejvyššímu	vysoký	k2eAgMnSc3d3
dvorskému	dvorský	k2eAgMnSc3d1
poštmistru	poštmistr	k1gMnSc3
Antoniu	Antonio	k1gMnSc3
Taxisovi	Taxis	k1gMnSc3
uložil	uložit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
co	co	k3yInSc4
nejrychleji	rychle	k6eAd3
zřídil	zřídit	k5eAaPmAgMnS
pravidelné	pravidelný	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
mezi	mezi	k7c7
Vídní	Vídeň	k1gFnSc7
a	a	k8xC
Prahou	Praha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
volbou	volba	k1gFnSc7
Ferdinanda	Ferdinand	k1gMnSc2
I.	I.	kA
za	za	k7c4
českého	český	k2eAgMnSc4d1
krále	král	k1gMnSc4
vznikla	vzniknout	k5eAaPmAgFnS
vlastně	vlastně	k9
v	v	k7c6
naší	náš	k3xOp1gFnSc6
zemi	zem	k1gFnSc6
regulérní	regulérní	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
dřív	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
Habsburk	Habsburk	k1gMnSc1
usedl	usednout	k5eAaPmAgMnS
na	na	k7c4
český	český	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1526	[number]	k4
<g/>
.	.	kIx.
</s>