<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Riesengebirge	Riesengebirge	k1gFnSc1	Riesengebirge
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Karkonosze	Karkonosze	k1gFnSc1	Karkonosze
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
a	a	k8xC	a
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
polské	polský	k2eAgFnSc2d1	polská
části	část	k1gFnSc2	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
je	být	k5eAaImIp3nS	být
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
střeží	střežit	k5eAaImIp3nS	střežit
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
bájný	bájný	k2eAgMnSc1d1	bájný
duch	duch	k1gMnSc1	duch
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgNnPc4d3	nejoblíbenější
horská	horský	k2eAgNnPc4d1	horské
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Širší	široký	k2eAgInSc4d2	širší
horský	horský	k2eAgInSc4d1	horský
celek	celek	k1gInSc4	celek
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k8xS	jako
Sudety	Sudety	k1gInPc4	Sudety
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
zřejmě	zřejmě	k6eAd1	zřejmě
keltského	keltský	k2eAgInSc2d1	keltský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
překládaný	překládaný	k2eAgInSc1d1	překládaný
jako	jako	k8xC	jako
Kančí	kančí	k2eAgFnPc1d1	kančí
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
balkánského	balkánský	k2eAgInSc2d1	balkánský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
překládaný	překládaný	k2eAgMnSc1d1	překládaný
jako	jako	k8xS	jako
Kozí	kozí	k2eAgFnPc1d1	kozí
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
(	(	kIx(	(
<g/>
asi	asi	k9	asi
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInPc4d1	dnešní
Sudety	Sudety	k1gInPc4	Sudety
názvy	název	k1gInPc4	název
Sudetayle	Sudetayl	k1gInSc5	Sudetayl
(	(	kIx(	(
<g/>
od	od	k7c2	od
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Askiburgion	Askiburgion	k1gInSc1	Askiburgion
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Jeseníky	Jeseník	k1gInPc4	Jeseník
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc4	okolí
vandalského	vandalský	k2eAgNnSc2d1	vandalské
města	město	k1gNnSc2	město
Askiburgium	Askiburgium	k1gNnSc4	Askiburgium
<g/>
,	,	kIx,	,
snad	snad	k9	snad
až	až	k9	až
po	po	k7c4	po
Lužické	lužický	k2eAgFnPc4d1	Lužická
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
včetně	včetně	k7c2	včetně
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dio	Dio	k?	Dio
Cassius	Cassius	k1gMnSc1	Cassius
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
Askiburgion	Askiburgion	k1gInSc4	Askiburgion
název	název	k1gInSc4	název
Vandalské	vandalský	k2eAgFnSc2d1	vandalská
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
Ptolemaiovy	Ptolemaiův	k2eAgFnPc1d1	Ptolemaiova
mapy	mapa	k1gFnPc1	mapa
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
či	či	k8xC	či
Pavel	Pavel	k1gMnSc1	Pavel
Skála	Skála	k1gMnSc1	Skála
ze	z	k7c2	z
Zhoře	Zhoř	k1gFnSc2	Zhoř
používali	používat	k5eAaImAgMnP	používat
rozšíření	rozšíření	k1gNnSc4	rozšíření
názvu	název	k1gInSc2	název
Sudety	Sudety	k1gFnPc4	Sudety
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
pás	pás	k1gInSc4	pás
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
letopisech	letopis	k1gInPc6	letopis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1095	[number]	k4	1095
nazvány	nazvat	k5eAaBmNgInP	nazvat
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc4	les
a	a	k8xC	a
Přibík	Přibík	k1gInSc4	Přibík
Pulkava	Pulkava	k1gFnSc1	Pulkava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
je	být	k5eAaImIp3nS	být
nazývá	nazývat	k5eAaImIp3nS	nazývat
Sněžné	sněžný	k2eAgFnPc4d1	sněžná
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
jednak	jednak	k8xC	jednak
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Kolo	kolo	k1gNnSc1	kolo
a	a	k8xC	a
také	také	k9	také
Kotel	kotel	k1gInSc1	kotel
neboli	neboli	k8xC	neboli
Kokrháč	kokrháč	k1gMnSc1	kokrháč
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
(	(	kIx(	(
<g/>
v	v	k7c6	v
singuláru	singulár	k1gInSc6	singulár
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
"	"	kIx"	"
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
horský	horský	k2eAgInSc4d1	horský
hřbet	hřbet	k1gInSc4	hřbet
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
v	v	k7c6	v
zápise	zápis	k1gInSc6	zápis
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
štěpanického	štěpanický	k2eAgNnSc2d1	štěpanický
panství	panství	k1gNnSc2	panství
na	na	k7c4	na
valdštejnský	valdštejnský	k2eAgInSc4d1	valdštejnský
a	a	k8xC	a
jilemnický	jilemnický	k2eAgInSc4d1	jilemnický
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
pak	pak	k6eAd1	pak
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc4d1	dochovaná
mapu	mapa	k1gFnSc4	mapa
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Klaudyán	Klaudyán	k1gMnSc1	Klaudyán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1518	[number]	k4	1518
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hájek	Hájek	k1gMnSc1	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
použil	použít	k5eAaPmAgInS	použít
název	název	k1gInSc4	název
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slezská	Slezská	k1gFnSc1	Slezská
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
doklad	doklad	k1gInSc1	doklad
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
názvu	název	k1gInSc2	název
na	na	k7c4	na
celé	celý	k2eAgFnPc4d1	celá
hory	hora	k1gFnPc4	hora
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
název	název	k1gInSc1	název
Krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
nápis	nápis	k1gInSc4	nápis
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
obrázek	obrázek	k1gInSc4	obrázek
čerta	čert	k1gMnSc2	čert
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložit	k5eAaPmNgInS	doložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1601	[number]	k4	1601
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
odvozeninu	odvozenina	k1gFnSc4	odvozenina
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
slovanského	slovanský	k2eAgInSc2d1	slovanský
základu	základ	k1gInSc2	základ
"	"	kIx"	"
<g/>
krk	krk	k1gInSc1	krk
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
krak	krak	k?	krak
<g/>
"	"	kIx"	"
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
kleč	kleč	k1gFnSc4	kleč
čili	čili	k8xC	čili
kosodřevinu	kosodřevina	k1gFnSc4	kosodřevina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
je	být	k5eAaImIp3nS	být
spojoval	spojovat	k5eAaImAgMnS	spojovat
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
germánského	germánský	k2eAgInSc2d1	germánský
či	či	k8xC	či
keltského	keltský	k2eAgInSc2d1	keltský
kmene	kmen	k1gInSc2	kmen
Corconti	Corconť	k1gFnSc2	Corconť
či	či	k8xC	či
Korkontoi	Korkonto	k1gFnSc2	Korkonto
<g/>
,	,	kIx,	,
zmiňovaného	zmiňovaný	k2eAgInSc2d1	zmiňovaný
Ptolemaiem	Ptolemaios	k1gMnSc7	Ptolemaios
<g/>
,	,	kIx,	,
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
pohoří	pohoří	k1gNnSc4	pohoří
Asciburgius	Asciburgius	k1gInSc4	Asciburgius
bylo	být	k5eAaImAgNnS	být
totožné	totožný	k2eAgNnSc1d1	totožné
s	s	k7c7	s
dnešními	dnešní	k2eAgMnPc7d1	dnešní
Krkonoši	Krkonoš	k1gMnPc7	Krkonoš
(	(	kIx(	(
<g/>
Korkontoi	Korkontoe	k1gFnSc4	Korkontoe
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
žít	žít	k5eAaImF	žít
někde	někde	k6eAd1	někde
poblíž	poblíž	k7c2	poblíž
pramenů	pramen	k1gInPc2	pramen
Visly	Visla	k1gFnSc2	Visla
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
bádání	bádání	k1gNnPc1	bádání
vykládají	vykládat	k5eAaImIp3nP	vykládat
původ	původ	k1gInSc4	původ
názvu	název	k1gInSc2	název
z	z	k7c2	z
praslovanského	praslovanský	k2eAgInSc2d1	praslovanský
základu	základ	k1gInSc2	základ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
úbočí	úbočí	k1gNnSc1	úbočí
<g/>
,	,	kIx,	,
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
pole	pole	k1gNnSc1	pole
<g/>
"	"	kIx"	"
a	a	k8xC	a
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
názvem	název	k1gInSc7	název
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
pohoří	pohoří	k1gNnSc2	pohoří
Gorgany	Gorgana	k1gFnSc2	Gorgana
ve	v	k7c6	v
Východních	východní	k2eAgInPc6d1	východní
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
i	i	k8xC	i
slezská	slezský	k2eAgFnSc1d1	Slezská
strana	strana	k1gFnSc1	strana
hor	hora	k1gFnPc2	hora
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
obydlena	obydlet	k5eAaPmNgFnS	obydlet
z	z	k7c2	z
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
Němci	Němec	k1gMnPc1	Němec
<g/>
)	)	kIx)	)
s	s	k7c7	s
významem	význam	k1gInSc7	význam
Obří	obří	k2eAgFnSc2d1	obří
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
Riesengebirge	Riesengebirge	k1gInSc1	Riesengebirge
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Giant	Gianta	k1gFnPc2	Gianta
Mountains	Mountains	k1gInSc1	Mountains
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
Risenberg	Risenberg	k1gInSc1	Risenberg
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Agricola	Agricola	k1gFnSc1	Agricola
(	(	kIx(	(
<g/>
Georgius	Georgius	k1gInSc1	Georgius
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1546	[number]	k4	1546
označil	označit	k5eAaPmAgMnS	označit
Sněžku	Sněžka	k1gFnSc4	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
Riesengebirge	Riesengebirg	k1gFnSc2	Riesengebirg
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
pohoří	pohoří	k1gNnSc4	pohoří
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
geologického	geologický	k2eAgNnSc2d1	geologické
složení	složení	k1gNnSc2	složení
tvoří	tvořit	k5eAaImIp3nS	tvořit
předprvohorní	předprvohorní	k2eAgFnSc2d1	předprvohorní
krystalické	krystalický	k2eAgFnSc2d1	krystalická
břidlice	břidlice	k1gFnSc2	břidlice
a	a	k8xC	a
prvohorní	prvohorní	k2eAgFnSc2d1	prvohorní
metamorfované	metamorfovaný	k2eAgFnSc2d1	metamorfovaná
horniny	hornina	k1gFnSc2	hornina
(	(	kIx(	(
<g/>
svor	svor	k1gInSc1	svor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
pohoří	pohoří	k1gNnSc2	pohoří
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vápence	vápenec	k1gInPc4	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Prastarým	prastarý	k2eAgNnSc7d1	prastaré
krystalinikem	krystalinikum	k1gNnSc7	krystalinikum
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
proniká	pronikat	k5eAaImIp3nS	pronikat
a	a	k8xC	a
krkonošsko-jizerský	krkonošskoizerský	k2eAgInSc1d1	krkonošsko-jizerský
pluton	pluton	k1gInSc1	pluton
(	(	kIx(	(
<g/>
žula	žula	k1gFnSc1	žula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtohorách	čtvrtohory	k1gFnPc6	čtvrtohory
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
modelovaly	modelovat	k5eAaImAgInP	modelovat
zdejší	zdejší	k2eAgFnSc4d1	zdejší
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc1	typ
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byly	být	k5eAaImAgInP	být
ledovce	ledovec	k1gInPc1	ledovec
údolního	údolní	k2eAgInSc2d1	údolní
typu	typ	k1gInSc2	typ
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
byly	být	k5eAaImAgFnP	být
skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
náhorní	náhorní	k2eAgFnPc1d1	náhorní
plošiny	plošina	k1gFnPc1	plošina
(	(	kIx(	(
<g/>
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
návrší	návrší	k1gNnSc1	návrší
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vznik	vznik	k1gInSc1	vznik
ledovci	ledovec	k1gInSc6	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
představu	představa	k1gFnSc4	představa
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
podívat	podívat	k5eAaPmF	podívat
na	na	k7c6	na
ledovce	ledovka	k1gFnSc6	ledovka
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nám	my	k3xPp1nPc3	my
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
mohlo	moct	k5eAaImAgNnS	moct
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
ukázkou	ukázka	k1gFnSc7	ukázka
glaciální	glaciální	k2eAgFnSc2d1	glaciální
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Labský	labský	k2eAgInSc4d1	labský
důl	důl	k1gInSc4	důl
nebo	nebo	k8xC	nebo
Obří	obří	k2eAgInSc4d1	obří
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
údolí	údolí	k1gNnPc1	údolí
vymodelované	vymodelovaný	k2eAgInPc1d1	vymodelovaný
ledovci	ledovec	k1gInPc7	ledovec
(	(	kIx(	(
<g/>
trogy	trog	k1gInPc7	trog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
ledovcovými	ledovcový	k2eAgInPc7d1	ledovcový
relikty	relikt	k1gInPc7	relikt
jsou	být	k5eAaImIp3nP	být
ledovcové	ledovcový	k2eAgInPc1d1	ledovcový
kary	kar	k1gInPc1	kar
(	(	kIx(	(
<g/>
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kary	kar	k1gInPc4	kar
známé	známý	k2eAgNnSc4d1	známé
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
jámy	jáma	k1gFnPc4	jáma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
Kotelní	kotelní	k2eAgFnPc4d1	kotelní
jámy	jáma	k1gFnPc4	jáma
a	a	k8xC	a
Sněžné	sněžný	k2eAgFnPc4d1	sněžná
jámy	jáma	k1gFnPc4	jáma
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Kary	kara	k1gFnPc1	kara
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nejcennějšímu	cenný	k2eAgMnSc3d3	nejcennější
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejvzácnější	vzácný	k2eAgFnPc4d3	nejvzácnější
krkonošské	krkonošský	k2eAgFnPc4d1	Krkonošská
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kryogenní	kryogenní	k2eAgFnSc3d1	kryogenní
činnosti	činnost	k1gFnSc3	činnost
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
ještě	ještě	k6eAd1	ještě
například	například	k6eAd1	například
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
kamenná	kamenný	k2eAgNnPc4d1	kamenné
moře	moře	k1gNnPc4	moře
(	(	kIx(	(
<g/>
svahy	svah	k1gInPc1	svah
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
Kola	kolo	k1gNnSc2	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mrazové	mrazový	k2eAgInPc1d1	mrazový
sruby	srub	k1gInPc1	srub
<g/>
.	.	kIx.	.
</s>
<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
hřeben	hřeben	k1gInSc1	hřeben
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
35	[number]	k4	35
km	km	kA	km
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
v	v	k7c6	v
Novosvětském	novosvětský	k2eAgNnSc6d1	Novosvětské
sedle	sedlo	k1gNnSc6	sedlo
(	(	kIx(	(
<g/>
888	[number]	k4	888
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
v	v	k7c6	v
Královeckém	Královecký	k2eAgNnSc6d1	Královecký
sedle	sedlo	k1gNnSc6	sedlo
(	(	kIx(	(
<g/>
516	[number]	k4	516
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholové	vrcholový	k2eAgFnPc1d1	vrcholová
partie	partie	k1gFnPc1	partie
pohoří	pohoří	k1gNnSc2	pohoří
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
a	a	k8xC	a
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
spadají	spadat	k5eAaPmIp3nP	spadat
prudce	prudko	k6eAd1	prudko
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
svahy	svah	k1gInPc1	svah
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
dolinami	dolina	k1gFnPc7	dolina
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
spadají	spadat	k5eAaImIp3nP	spadat
podstatně	podstatně	k6eAd1	podstatně
mírněji	mírně	k6eAd2	mírně
<g/>
.	.	kIx.	.
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
Krkonošské	krkonošský	k2eAgInPc4d1	krkonošský
rozsochy	rozsoch	k1gInPc4	rozsoch
<g/>
,	,	kIx,	,
Krkonošské	krkonošský	k2eAgInPc4d1	krkonošský
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
Vrchlabskou	vrchlabský	k2eAgFnSc4d1	vrchlabská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
členění	členění	k1gNnSc1	členění
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vrcholů	vrchol	k1gInPc2	vrchol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
leží	ležet	k5eAaImIp3nS	ležet
15	[number]	k4	15
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
právě	právě	k9	právě
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
části	část	k1gFnSc6	část
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
nachází	nacházet	k5eAaImIp3nS	nacházet
54	[number]	k4	54
hlavních	hlavní	k2eAgInPc2d1	hlavní
vrcholů	vrchol	k1gInPc2	vrchol
vyšších	vysoký	k2eAgInPc2d2	vyšší
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
18	[number]	k4	18
bočních	boční	k2eAgFnPc2d1	boční
rozsoch	rozsocha	k1gFnPc2	rozsocha
přesahujících	přesahující	k2eAgFnPc2d1	přesahující
tisícimetrovou	tisícimetrový	k2eAgFnSc4d1	tisícimetrová
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
vrcholů	vrchol	k1gInPc2	vrchol
leží	ležet	k5eAaImIp3nS	ležet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
hranici	hranice	k1gFnSc6	hranice
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
území	území	k1gNnSc6	území
nedaleko	nedaleko	k7c2	nedaleko
hranice	hranice	k1gFnSc2	hranice
(	(	kIx(	(
<g/>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pláň	pláň	k1gFnSc1	pláň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
polské	polský	k2eAgFnPc4d1	polská
tisícovky	tisícovka	k1gFnPc4	tisícovka
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
vrcholy	vrchol	k1gInPc1	vrchol
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInPc1d1	ležící
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
území	území	k1gNnSc6	území
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
m	m	kA	m
za	za	k7c7	za
státní	státní	k2eAgFnSc7d1	státní
hranicí	hranice	k1gFnSc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
tvoří	tvořit	k5eAaImIp3nP	tvořit
přirozené	přirozený	k2eAgNnSc4d1	přirozené
rozvodí	rozvodí	k1gNnSc4	rozvodí
mezi	mezi	k7c7	mezi
Severním	severní	k2eAgNnSc7d1	severní
a	a	k8xC	a
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
pramení	pramenit	k5eAaImIp3nP	pramenit
řeky	řeka	k1gFnPc1	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc2d1	Malé
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
,	,	kIx,	,
Jizerka	Jizerka	k1gFnSc1	Jizerka
a	a	k8xC	a
Mumlava	Mumlava	k1gFnSc1	Mumlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Kamienica	Kamienica	k1gFnSc1	Kamienica
a	a	k8xC	a
Łomniczka	Łomniczka	k1gFnSc1	Łomniczka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jezera	jezero	k1gNnSc2	jezero
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
Wielki	Wielk	k1gFnSc2	Wielk
a	a	k8xC	a
Mały	Mała	k1gFnSc2	Mała
Staw	Staw	k1gFnSc2	Staw
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
je	být	k5eAaImIp3nS	být
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
jen	jen	k6eAd1	jen
Mechové	mechový	k2eAgNnSc4d1	mechové
jezírko	jezírko	k1gNnSc4	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
reliktní	reliktní	k2eAgInPc1d1	reliktní
a	a	k8xC	a
endemické	endemický	k2eAgInPc1d1	endemický
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
největší	veliký	k2eAgFnPc4d3	veliký
plochy	plocha	k1gFnPc4	plocha
ležící	ležící	k2eAgFnPc4d1	ležící
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1200	[number]	k4	1200
<g/>
-	-	kIx~	-
<g/>
1300	[number]	k4	1300
m	m	kA	m
zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nS	končit
pásmo	pásmo	k1gNnSc4	pásmo
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
je	být	k5eAaImIp3nS	být
pásmo	pásmo	k1gNnSc4	pásmo
kosodřeviny	kosodřevina	k1gFnSc2	kosodřevina
<g/>
,	,	kIx,	,
smilkové	smilkový	k2eAgFnSc2d1	Smilková
hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
kamenná	kamenný	k2eAgNnPc1d1	kamenné
a	a	k8xC	a
suťová	suťový	k2eAgNnPc1d1	suťové
moře	moře	k1gNnPc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
biotop	biotop	k1gInSc4	biotop
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
arkto-alpínská	arktolpínský	k2eAgFnSc1d1	arkto-alpínská
tundra	tundra	k1gFnSc1	tundra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
dokonce	dokonce	k9	dokonce
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
smrkový	smrkový	k2eAgInSc1d1	smrkový
porost	porost	k1gInSc1	porost
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
zdevastován	zdevastován	k2eAgInSc1d1	zdevastován
vlivy	vliv	k1gInPc7	vliv
imisí	imise	k1gFnPc2	imise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
rostou	růst	k5eAaImIp3nP	růst
monokultury	monokultura	k1gFnPc1	monokultura
buku	buk	k1gInSc2	buk
a	a	k8xC	a
smrku	smrk	k1gInSc2	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Malakofauna	Malakofauna	k1gFnSc1	Malakofauna
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
čítá	čítat	k5eAaImIp3nS	čítat
90	[number]	k4	90
druhů	druh	k1gInPc2	druh
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
KRNAP	KRNAP	kA	KRNAP
<g/>
)	)	kIx)	)
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
36	[number]	k4	36
400	[number]	k4	400
ha	ha	kA	ha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
a	a	k8xC	a
Karkonoski	Karkonosk	k1gFnPc1	Karkonosk
Park	park	k1gInSc1	park
Narodowy	Narodowa	k1gFnSc2	Narodowa
(	(	kIx(	(
<g/>
KPN	KPN	kA	KPN
<g/>
)	)	kIx)	)
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
rozlohu	rozloh	k1gInSc2	rozloh
18	[number]	k4	18
400	[number]	k4	400
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
Bilaterální	bilaterální	k2eAgFnSc7d1	bilaterální
biosférickou	biosférický	k2eAgFnSc7d1	biosférická
rezervací	rezervace	k1gFnSc7	rezervace
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
54	[number]	k4	54
800	[number]	k4	800
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
KRNAP	KRNAP	kA	KRNAP
je	být	k5eAaImIp3nS	být
členěn	členit	k5eAaImNgInS	členit
do	do	k7c2	do
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
ochranné	ochranný	k2eAgFnSc2d1	ochranná
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
I.	I.	kA	I.
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
nejcennější	cenný	k2eAgNnSc4d3	nejcennější
a	a	k8xC	a
spadá	spadat	k5eAaImIp3nS	spadat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nejpřísnější	přísný	k2eAgInPc4d3	nejpřísnější
režim	režim	k1gInSc4	režim
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
např.	např.	kA	např.
opatření	opatření	k1gNnSc1	opatření
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
zóně	zóna	k1gFnSc3	zóna
je	být	k5eAaImIp3nS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
pohyb	pohyb	k1gInSc1	pohyb
mimo	mimo	k7c4	mimo
vyznačené	vyznačený	k2eAgFnPc4d1	vyznačená
turistické	turistický	k2eAgFnPc4d1	turistická
a	a	k8xC	a
lyžařské	lyžařský	k2eAgFnPc4d1	lyžařská
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
je	být	k5eAaImIp3nS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
též	též	k9	též
ptačí	ptačí	k2eAgFnSc1d1	ptačí
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
významné	významný	k2eAgNnSc1d1	významné
ptačí	ptačí	k2eAgNnSc1d1	ptačí
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
biosférické	biosférický	k2eAgFnSc2d1	biosférická
rezervace	rezervace	k1gFnSc2	rezervace
resp.	resp.	kA	resp.
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
včetně	včetně	k7c2	včetně
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
klimatického	klimatický	k2eAgNnSc2d1	klimatické
hlediska	hledisko	k1gNnSc2	hledisko
nejdrsnějším	drsný	k2eAgNnSc7d3	nejdrsnější
českým	český	k2eAgNnSc7d1	české
pohořím	pohoří	k1gNnSc7	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholové	vrcholový	k2eAgFnPc1d1	vrcholová
partie	partie	k1gFnPc1	partie
ležící	ležící	k2eAgFnPc1d1	ležící
nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
1400	[number]	k4	1400
m	m	kA	m
se	se	k3xPyFc4	se
podnebím	podnebí	k1gNnSc7	podnebí
dají	dát	k5eAaPmIp3nP	dát
srovnat	srovnat	k5eAaPmF	srovnat
s	s	k7c7	s
grónským	grónský	k2eAgNnSc7d1	grónské
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
především	především	k9	především
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
například	například	k6eAd1	například
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
je	být	k5eAaImIp3nS	být
6,8	[number]	k4	6,8
stupně	stupeň	k1gInSc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nejchladnějším	chladný	k2eAgNnSc7d3	nejchladnější
místem	místo	k1gNnSc7	místo
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
0,2	[number]	k4	0,2
stupně	stupeň	k1gInSc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejchladnější	chladný	k2eAgNnSc4d3	nejchladnější
místo	místo	k1gNnSc4	místo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
a	a	k8xC	a
orientaci	orientace	k1gFnSc6	orientace
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
mají	mít	k5eAaImIp3nP	mít
během	během	k7c2	během
roku	rok	k1gInSc2	rok
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
než	než	k8xS	než
stanice	stanice	k1gFnSc2	stanice
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
převládá	převládat	k5eAaImIp3nS	převládat
západní	západní	k2eAgNnSc1d1	západní
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
srážek	srážka	k1gFnPc2	srážka
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
během	během	k7c2	během
bouřek	bouřka	k1gFnPc2	bouřka
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
sněhu	sníh	k1gInSc2	sníh
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
méně	málo	k6eAd2	málo
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
nevypadává	vypadávat	k5eNaImIp3nS	vypadávat
nejvíce	hodně	k6eAd3	hodně
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
leží	ležet	k5eAaImIp3nP	ležet
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
hor.	hor.	k?	hor.
Na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
průměrně	průměrně	k6eAd1	průměrně
kolem	kolem	k7c2	kolem
1300	[number]	k4	1300
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
býti	být	k5eAaImF	být
až	až	k9	až
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
partií	partie	k1gFnSc7	partie
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
o	o	k7c6	o
datu	datum	k1gNnSc6	datum
výskytu	výskyt	k1gInSc2	výskyt
prvních	první	k4xOgFnPc2	první
sněhových	sněhový	k2eAgFnPc2d1	sněhová
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
nedá	dát	k5eNaPmIp3nS	dát
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
může	moct	k5eAaImIp3nS	moct
sněžit	sněžit	k5eAaImF	sněžit
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
průměrně	průměrně	k6eAd1	průměrně
od	od	k7c2	od
půlky	půlka	k1gFnSc2	půlka
listopadu	listopad	k1gInSc2	listopad
až	až	k6eAd1	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
kumulaci	kumulace	k1gFnSc3	kumulace
sněhu	sníh	k1gInSc2	sníh
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
závětrných	závětrný	k2eAgInPc6d1	závětrný
svazích	svah	k1gInPc6	svah
(	(	kIx(	(
<g/>
Mapa	mapa	k1gFnSc1	mapa
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výška	výška	k1gFnSc1	výška
sněhu	sníh	k1gInSc2	sníh
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
fouká	foukat	k5eAaImIp3nS	foukat
silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
naměřena	naměřit	k5eAaBmNgFnS	naměřit
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
216	[number]	k4	216
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
jsou	být	k5eAaImIp3nP	být
významným	významný	k2eAgInSc7d1	významný
centrem	centr	k1gInSc7	centr
letní	letní	k2eAgFnSc2d1	letní
i	i	k8xC	i
zimní	zimní	k2eAgFnSc2d1	zimní
turistiky	turistika	k1gFnSc2	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
turistická	turistický	k2eAgNnPc1d1	turistické
střediska	středisko	k1gNnPc1	středisko
oblasti	oblast	k1gFnSc2	oblast
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Pec	Pec	k1gFnSc1	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
Špindlerův	Špindlerův	k2eAgInSc1d1	Špindlerův
Mlýn	mlýn	k1gInSc1	mlýn
Horní	horní	k2eAgInSc1d1	horní
Maršov	Maršov	k1gInSc4	Maršov
Janské	janský	k2eAgFnSc2d1	Janská
Lázně	lázeň	k1gFnSc2	lázeň
Harrachov	Harrachov	k1gInSc1	Harrachov
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
Szklarska	Szklarska	k1gFnSc1	Szklarska
Poręba	Poręba	k1gFnSc1	Poręba
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
Karpacz	Karpacz	k1gInSc1	Karpacz
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
Benecko	Benecko	k1gNnSc1	Benecko
Největším	veliký	k2eAgFnPc3d3	veliký
krkonošským	krkonošský	k2eAgInPc3d1	krkonošský
(	(	kIx(	(
<g/>
i	i	k9	i
českým	český	k2eAgMnPc3d1	český
<g/>
)	)	kIx)	)
lyžařským	lyžařský	k2eAgNnSc7d1	lyžařské
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
SkiResort	SkiResort	k1gInSc4	SkiResort
Černá	Černá	k1gFnSc1	Černá
hora-Pec	hora-Pec	k1gInSc1	hora-Pec
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
spojením	spojení	k1gNnSc7	spojení
středisek	středisko	k1gNnPc2	středisko
v	v	k7c6	v
Janských	janský	k2eAgFnPc6d1	Janská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
Černém	černý	k2eAgInSc6d1	černý
Dole	dol	k1gInSc6	dol
a	a	k8xC	a
Svobodou	Svoboda	k1gMnSc7	Svoboda
nad	nad	k7c7	nad
Úpou	Úpa	k1gFnSc7	Úpa
<g/>
,	,	kIx,	,
se	s	k7c7	s
středisky	středisko	k1gNnPc7	středisko
v	v	k7c6	v
Peci	Pec	k1gFnSc6	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Úpě	Úpa	k1gFnSc6	Úpa
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Úpě	Úpa	k1gFnSc6	Úpa
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
českou	český	k2eAgFnSc7d1	Česká
sjezdovkou	sjezdovka	k1gFnSc7	sjezdovka
je	být	k5eAaImIp3nS	být
Turistická	turistický	k2eAgFnSc1d1	turistická
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
v	v	k7c6	v
Rokytnici	Rokytnice	k1gFnSc6	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
3185	[number]	k4	3185
m	m	kA	m
<g/>
,	,	kIx,	,
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
652	[number]	k4	652
m.	m.	k?	m.
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
počet	počet	k1gInSc1	počet
turistů	turist	k1gMnPc2	turist
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
oblast	oblast	k1gFnSc4	oblast
poprvé	poprvé	k6eAd1	poprvé
překonala	překonat	k5eAaPmAgFnS	překonat
hranici	hranice	k1gFnSc6	hranice
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
rekordních	rekordní	k2eAgInPc2d1	rekordní
1,13	[number]	k4	1,13
milionu	milion	k4xCgInSc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
skladba	skladba	k1gFnSc1	skladba
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
přibylo	přibýt	k5eAaPmAgNnS	přibýt
hlavně	hlavně	k6eAd1	hlavně
čechů	čech	k1gMnPc2	čech
a	a	k8xC	a
poláků	polák	k1gMnPc2	polák
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
němců	němec	k1gInPc2	němec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
převážně	převážně	k6eAd1	převážně
německojazyčné	německojazyčný	k2eAgNnSc1d1	německojazyčné
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
zejména	zejména	k9	zejména
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
architektuře	architektura	k1gFnSc6	architektura
a	a	k8xC	a
názvech	název	k1gInPc6	název
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
chalup	chalupa	k1gFnPc2	chalupa
atd.	atd.	kA	atd.
V	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
snímek	snímek	k1gInSc1	snímek
Vánice	vánice	k1gFnSc2	vánice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
režiséra	režisér	k1gMnSc2	režisér
Čeňka	Čeněk	k1gMnSc2	Čeněk
Duby	Duba	k1gMnSc2	Duba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tragédii	tragédie	k1gFnSc6	tragédie
Hanče	Hanče	k1gFnSc2	Hanče
a	a	k8xC	a
Vrbaty	Vrbata	k1gFnSc2	Vrbata
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
snímek	snímek	k1gInSc1	snímek
Synové	syn	k1gMnPc1	syn
hor	hora	k1gFnPc2	hora
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
režiséra	režisér	k1gMnSc2	režisér
Čeňka	Čeněk	k1gMnSc2	Čeněk
Duby	Duba	k1gMnSc2	Duba
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
večerníčkový	večerníčkový	k2eAgInSc1d1	večerníčkový
seriál	seriál	k1gInSc1	seriál
Krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
pohádky	pohádka	k1gFnSc2	pohádka
V	v	k7c6	v
Krnokoších	Krnokoch	k1gInPc6	Krnokoch
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
i	i	k8xC	i
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
filmu	film	k1gInSc2	film
Snowboarďáci	Snowboarďák	k1gMnPc1	Snowboarďák
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Známými	známá	k1gFnPc7	známá
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
i	i	k9	i
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
Jak	jak	k6eAd1	jak
vytrhnout	vytrhnout	k5eAaPmF	vytrhnout
velrybě	velryba	k1gFnSc3	velryba
stoličku	stolička	k1gFnSc4	stolička
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
také	také	k9	také
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
Seznam	seznam	k1gInSc4	seznam
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
Vysoké	vysoký	k2eAgInPc4d1	vysoký
Sudety	Sudety	k1gInPc4	Sudety
Moazagotl	Moazagotl	k1gFnSc2	Moazagotl
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
turistické	turistický	k2eAgFnSc2d1	turistická
stránky	stránka	k1gFnSc2	stránka
regionu	region	k1gInSc2	region
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
Krkonošský	krkonošský	k2eAgInSc4d1	krkonošský
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
webkamery	webkamera	k1gFnSc2	webkamera
-	-	kIx~	-
aktuální	aktuální	k2eAgFnPc4d1	aktuální
online	onlinout	k5eAaPmIp3nS	onlinout
webkamery	webkamera	k1gFnPc4	webkamera
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
Plus	plus	k1gInSc1	plus
-	-	kIx~	-
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
Fotografie	fotografia	k1gFnSc2	fotografia
zachycující	zachycující	k2eAgFnSc2d1	zachycující
krásy	krása	k1gFnSc2	krása
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
Hlavní	hlavní	k2eAgFnSc2d1	hlavní
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
<g />
.	.	kIx.	.
</s>
<s>
sjezdy	sjezd	k1gInPc1	sjezd
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
Karkonosze	Karkonosze	k1gFnSc1	Karkonosze
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Region	region	k1gInSc1	region
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
krakonos	krakonos	k1gInSc1	krakonos
<g/>
.	.	kIx.	.
<g/>
info	info	k1gNnSc1	info
-	-	kIx~	-
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
krkonošských	krkonošský	k2eAgNnPc6d1	Krkonošské
střediscích	středisko	k1gNnPc6	středisko
Krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
boudy	bouda	k1gFnSc2	bouda
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
krkonošských	krkonošský	k2eAgFnPc6d1	Krkonošská
boudách	bouda	k1gFnPc6	bouda
<g/>
,	,	kIx,	,
současných	současný	k2eAgInPc2d1	současný
i	i	k8xC	i
zaniklých	zaniklý	k2eAgInPc2d1	zaniklý
V	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
roztálo	roztát	k5eAaPmAgNnS	roztát
sněhové	sněhový	k2eAgNnSc1d1	sněhové
pole	pole	k1gNnSc4	pole
zvané	zvaný	k2eAgNnSc4d1	zvané
Mapa	mapa	k1gFnSc1	mapa
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
-	-	kIx~	-
turistické	turistický	k2eAgFnPc1d1	turistická
zajímavosti	zajímavost	k1gFnPc1	zajímavost
<g/>
,	,	kIx,	,
tipy	tip	k1gInPc1	tip
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
a	a	k8xC	a
kam	kam	k6eAd1	kam
za	za	k7c7	za
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
