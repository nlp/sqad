<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
mо	mо	k?	mо
nebo	nebo	k8xC	nebo
К	К	k?	К
<g/>
;	;	kIx,	;
persky	persky	k6eAd1	persky
د	د	k?	د
م	م	k?	م
<g/>
;	;	kIx,	;
ázerbájdžánsky	ázerbájdžánsky	k6eAd1	ázerbájdžánsky
Xə	Xə	k1gMnSc2	Xə
də	də	k?	də
<g/>
;	;	kIx,	;
kazašsky	kazašsky	k6eAd1	kazašsky
К	К	k?	К
т	т	k?	т
<g/>
;	;	kIx,	;
turkmensky	turkmensky	k6eAd1	turkmensky
Hazar	Hazar	k1gMnSc1	Hazar
deňizi	deňize	k1gFnSc4	deňize
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Kaspion	Kaspion	k1gInSc1	Kaspion
pelagos	pelagosa	k1gFnPc2	pelagosa
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Mare	Mare	k1gFnSc7	Mare
Caspium	Caspium	k1gNnSc4	Caspium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
endorheické	endorheický	k2eAgNnSc1d1	endorheický
jezero	jezero	k1gNnSc1	jezero
rozprostírající	rozprostírající	k2eAgMnSc1d1	rozprostírající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Evropou	Evropa	k1gFnSc7	Evropa
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ruskou	ruský	k2eAgFnSc7d1	ruská
částí	část	k1gFnSc7	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
