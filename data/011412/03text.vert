<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
přijímač	přijímač	k1gInSc1	přijímač
čili	čili	k8xC	čili
televizor	televizor	k1gInSc1	televizor
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepřesně	přesně	k6eNd1	přesně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
koncové	koncový	k2eAgNnSc4d1	koncové
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
elektronický	elektronický	k2eAgInSc4d1	elektronický
přístroj	přístroj	k1gInSc4	přístroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
lidé	člověk	k1gMnPc1	člověk
sledují	sledovat	k5eAaImIp3nP	sledovat
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
nebo	nebo	k8xC	nebo
vysílání	vysílání	k1gNnSc4	vysílání
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
přenos	přenos	k1gInSc1	přenos
obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
úspěchem	úspěch	k1gInSc7	úspěch
stojí	stát	k5eAaImIp3nS	stát
Skot	Skot	k1gMnSc1	Skot
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Baird	k1gMnSc1	Baird
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
první	první	k4xOgInSc1	první
dálkový	dálkový	k2eAgInSc1d1	dálkový
přenos	přenos	k1gInSc1	přenos
obrazu	obraz	k1gInSc2	obraz
mezi	mezi	k7c7	mezi
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
mechanické	mechanický	k2eAgFnPc1d1	mechanická
televize	televize	k1gFnPc1	televize
byly	být	k5eAaImAgFnP	být
prodávány	prodávat	k5eAaImNgFnP	prodávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
prodávaly	prodávat	k5eAaImAgInP	prodávat
jen	jen	k9	jen
elektronické	elektronický	k2eAgInPc1d1	elektronický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
představena	představit	k5eAaPmNgFnS	představit
Bairdem	Baird	k1gInSc7	Baird
zcela	zcela	k6eAd1	zcela
první	první	k4xOgFnSc1	první
elektronická	elektronický	k2eAgFnSc1d1	elektronická
barevná	barevný	k2eAgFnSc1d1	barevná
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
televize	televize	k1gFnSc1	televize
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
docentem	docent	k1gMnSc7	docent
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Šafránkem	Šafránek	k1gMnSc7	Šafránek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc4	způsob
přijmu	přijmout	k5eAaPmIp1nS	přijmout
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc4	zobrazení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
televizorech	televizor	k1gInPc6	televizor
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
doby	doba	k1gFnPc4	doba
vzniku	vznik	k1gInSc2	vznik
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
televizorů	televizor	k1gInPc2	televizor
<g/>
)	)	kIx)	)
používala	používat	k5eAaImAgFnS	používat
výhradně	výhradně	k6eAd1	výhradně
klasická	klasický	k2eAgFnSc1d1	klasická
televizní	televizní	k2eAgFnSc1d1	televizní
obrazovka	obrazovka	k1gFnSc1	obrazovka
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
katodové	katodový	k2eAgFnSc2d1	katodová
trubice	trubice	k1gFnSc2	trubice
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
plazmové	plazmový	k2eAgFnSc2d1	plazmová
obrazovky	obrazovka	k1gFnSc2	obrazovka
a	a	k8xC	a
LCD	LCD	kA	LCD
(	(	kIx(	(
<g/>
tekuté	tekutý	k2eAgInPc1d1	tekutý
krystaly	krystal	k1gInPc1	krystal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
převažují	převažovat	k5eAaImIp3nP	převažovat
LED	led	k1gInSc4	led
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
klamavé	klamavý	k2eAgNnSc1d1	klamavé
označení	označení	k1gNnSc1	označení
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
LCD	LCD	kA	LCD
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
LCD	LCD	kA	LCD
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
podsvětleny	podsvětlit	k5eAaPmNgInP	podsvětlit
bílými	bílý	k2eAgInPc7d1	bílý
LED	LED	kA	LED
diodami	dioda	k1gFnPc7	dioda
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
CCFL	CCFL	kA	CCFL
trubicemi	trubice	k1gFnPc7	trubice
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
televize	televize	k1gFnSc2	televize
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umí	umět	k5eAaImIp3nP	umět
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
i	i	k9	i
vysílání	vysílání	k1gNnSc4	vysílání
a	a	k8xC	a
filmy	film	k1gInPc4	film
stereoskopicky	stereoskopicky	k6eAd1	stereoskopicky
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
televize	televize	k1gFnPc1	televize
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hybridní	hybridní	k2eAgInSc4d1	hybridní
způsob	způsob	k1gInSc4	způsob
vysílání	vysílání	k1gNnSc2	vysílání
HbbTV	HbbTV	k1gFnSc2	HbbTV
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hybridní	hybridní	k2eAgMnSc1d1	hybridní
TV	TV	kA	TV
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
internet	internet	k1gInSc4	internet
(	(	kIx(	(
<g/>
televizi	televize	k1gFnSc4	televize
lze	lze	k6eAd1	lze
ovládat	ovládat	k5eAaImF	ovládat
pomocí	pomocí	k7c2	pomocí
klávesnice	klávesnice	k1gFnSc2	klávesnice
a	a	k8xC	a
myši	myš	k1gFnPc4	myš
zapojené	zapojený	k2eAgFnPc4d1	zapojená
v	v	k7c6	v
USB	USB	kA	USB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnPc1	televize
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
PC	PC	kA	PC
<g/>
,	,	kIx,	,
prohlížení	prohlížení	k1gNnSc4	prohlížení
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
videí	video	k1gNnPc2	video
uložených	uložený	k2eAgMnPc2d1	uložený
na	na	k7c4	na
flash	flash	k1gInSc4	flash
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Android	android	k1gInSc1	android
<g/>
)	)	kIx)	)
mívají	mívat	k5eAaImIp3nP	mívat
funkce	funkce	k1gFnPc1	funkce
tzv.	tzv.	kA	tzv.
smart	smarta	k1gFnPc2	smarta
TV	TV	kA	TV
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chytré	chytrá	k1gFnSc3	chytrá
TV	TV	kA	TV
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
každého	každý	k3xTgInSc2	každý
televizního	televizní	k2eAgInSc2d1	televizní
přijímače	přijímač	k1gInSc2	přijímač
je	být	k5eAaImIp3nS	být
zvukový	zvukový	k2eAgInSc4d1	zvukový
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
analogového	analogový	k2eAgInSc2d1	analogový
televizoru	televizor	k1gInSc2	televizor
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
přijímač	přijímač	k1gInSc1	přijímač
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
FM	FM	kA	FM
(	(	kIx(	(
<g/>
frekvenční	frekvenční	k2eAgFnSc2d1	frekvenční
modulace	modulace	k1gFnSc2	modulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kmitočet	kmitočet	k1gInSc1	kmitočet
se	s	k7c7	s
(	(	kIx(	(
<g/>
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
normě	norma	k1gFnSc6	norma
<g/>
)	)	kIx)	)
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
6,5	[number]	k4	6,5
MHz	Mhz	kA	Mhz
od	od	k7c2	od
kmitočtu	kmitočet	k1gInSc2	kmitočet
obrazového	obrazový	k2eAgInSc2d1	obrazový
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paradoxem	paradox	k1gInSc7	paradox
současných	současný	k2eAgInPc2d1	současný
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
analogových	analogový	k2eAgInPc2d1	analogový
i	i	k8xC	i
digitálních	digitální	k2eAgInPc2d1	digitální
<g/>
)	)	kIx)	)
televizorů	televizor	k1gInPc2	televizor
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
kvalita	kvalita	k1gFnSc1	kvalita
zvukové	zvukový	k2eAgFnSc2d1	zvuková
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
tenkému	tenký	k2eAgInSc3d1	tenký
profilu	profil	k1gInSc3	profil
<g/>
)	)	kIx)	)
–	–	k?	–
televizory	televizor	k1gInPc1	televizor
ze	z	k7c2	z
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
nebo	nebo	k8xC	nebo
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
mají	mít	k5eAaImIp3nP	mít
zvukový	zvukový	k2eAgInSc4d1	zvukový
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
reprodukci	reprodukce	k1gFnSc4	reprodukce
zvuku	zvuk	k1gInSc2	zvuk
<g/>
)	)	kIx)	)
podstatně	podstatně	k6eAd1	podstatně
kvalitnější	kvalitní	k2eAgInSc1d2	kvalitnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
televizoru	televizor	k1gInSc3	televizor
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
také	také	k9	také
připojit	připojit	k5eAaPmF	připojit
další	další	k2eAgNnPc1d1	další
zařízení	zařízení	k1gNnPc1	zařízení
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
nebo	nebo	k8xC	nebo
záznam	záznam	k1gInSc4	záznam
televizního	televizní	k2eAgInSc2d1	televizní
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
videorekordér	videorekordér	k1gInSc4	videorekordér
<g/>
,	,	kIx,	,
videokameru	videokamera	k1gFnSc4	videokamera
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
rekordér	rekordér	k1gInSc1	rekordér
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgInSc1d1	digitální
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
herní	herní	k2eAgInSc1d1	herní
konzole	konzola	k1gFnSc3	konzola
<g/>
,	,	kIx,	,
HDD	HDD	kA	HDD
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
také	také	k9	také
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příjem	příjem	k1gInSc1	příjem
klasického	klasický	k2eAgInSc2d1	klasický
televizního	televizní	k2eAgInSc2d1	televizní
signálu	signál	k1gInSc2	signál
(	(	kIx(	(
<g/>
šířený	šířený	k2eAgMnSc1d1	šířený
bezdrátově	bezdrátově	k6eAd1	bezdrátově
nebo	nebo	k8xC	nebo
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
kabelových	kabelový	k2eAgFnPc2d1	kabelová
televizí	televize	k1gFnPc2	televize
<g/>
)	)	kIx)	)
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
speciálního	speciální	k2eAgInSc2d1	speciální
hardwaru	hardware	k1gInSc2	hardware
-	-	kIx~	-
televizní	televizní	k2eAgFnPc1d1	televizní
karty	karta	k1gFnPc1	karta
</s>
</p>
<p>
<s>
internetové	internetový	k2eAgNnSc1d1	internetové
vysílání	vysílání	k1gNnSc1	vysílání
televizního	televizní	k2eAgInSc2d1	televizní
signálu	signál	k1gInSc2	signál
–	–	k?	–
signál	signál	k1gInSc1	signál
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
interpretován	interpretován	k2eAgInSc4d1	interpretován
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
specializovaného	specializovaný	k2eAgInSc2d1	specializovaný
softwaru	software	k1gInSc2	software
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
zkušební	zkušební	k2eAgInSc1d1	zkušební
obrazec	obrazec	k1gInSc1	obrazec
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
televizor	televizor	k1gInSc1	televizor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
televizor	televizor	k1gInSc1	televizor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgNnSc1d1	virtuální
muzeum	muzeum	k1gNnSc1	muzeum
československých	československý	k2eAgInPc2d1	československý
televizorů	televizor	k1gInPc2	televizor
</s>
</p>
<p>
<s>
Před	před	k7c7	před
osmdesáti	osmdesát	k4xCc7	osmdesát
lety	let	k1gInPc7	let
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
mechanická	mechanický	k2eAgFnSc1d1	mechanická
verze	verze	k1gFnSc1	verze
barevné	barevný	k2eAgFnSc2d1	barevná
televize	televize	k1gFnSc2	televize
DigiWeb	DigiWba	k1gFnPc2	DigiWba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
</s>
</p>
