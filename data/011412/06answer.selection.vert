<s>
Televizní	televizní	k2eAgInSc1d1	televizní
přijímač	přijímač	k1gInSc1	přijímač
čili	čili	k8xC	čili
televizor	televizor	k1gInSc1	televizor
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepřesně	přesně	k6eNd1	přesně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
koncové	koncový	k2eAgNnSc4d1	koncové
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
elektronický	elektronický	k2eAgInSc4d1	elektronický
přístroj	přístroj	k1gInSc4	přístroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
lidé	člověk	k1gMnPc1	člověk
sledují	sledovat	k5eAaImIp3nP	sledovat
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
nebo	nebo	k8xC	nebo
vysílání	vysílání	k1gNnSc4	vysílání
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
