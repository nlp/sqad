<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
americkým	americký	k2eAgMnSc7d1	americký
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
i	i	k8xC	i
prezidentem	prezident	k1gMnSc7	prezident
<g/>
?	?	kIx.	?
</s>
