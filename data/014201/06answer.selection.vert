<s>
Ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ÚSKP	ÚSKP	kA
ČR	ČR	kA
<g/>
)	)	kIx)
vede	vést	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
NPÚ	NPÚ	kA
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
ústřední	ústřední	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
.	.	kIx.
</s>