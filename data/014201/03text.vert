<s>
Ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ÚSKP	ÚSKP	kA
ČR	ČR	kA
<g/>
)	)	kIx)
vede	vést	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
NPÚ	NPÚ	kA
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
ústřední	ústřední	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
nově	nově	k6eAd1
zřízen	zřídit	k5eAaPmNgInS
s	s	k7c7
nabytím	nabytí	k1gNnSc7
účinnosti	účinnost	k1gFnSc2
zákona	zákon	k1gInSc2
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
něj	on	k3xPp3gMnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgNnPc1d1
data	datum	k1gNnPc4
z	z	k7c2
dosavadních	dosavadní	k2eAgInPc2d1
krajských	krajský	k2eAgInPc2d1
rejstříků	rejstřík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
až	až	k9
2015	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
usnesení	usnesení	k1gNnSc2
vlády	vláda	k1gFnSc2
digitalizace	digitalizace	k1gFnSc2
a	a	k8xC
modernizace	modernizace	k1gFnSc2
seznamu	seznam	k1gInSc2
a	a	k8xC
souvisejících	související	k2eAgInPc2d1
procesů	proces	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
k	k	k7c3
evidenci	evidence	k1gFnSc3
památek	památka	k1gFnPc2
užívána	užíván	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dílčích	dílčí	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2015	#num#	k4
je	být	k5eAaImIp3nS
spuštěn	spustit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
návrhu	návrh	k1gInSc2
zákona	zákon	k1gInSc2
o	o	k7c6
ochraně	ochrana	k1gFnSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
schváleného	schválený	k2eAgNnSc2d1
vládou	vláda	k1gFnSc7
v	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
ÚSKP	ÚSKP	kA
z	z	k7c2
právního	právní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
nahrazen	nahrazen	k2eAgInSc4d1
„	„	k?
<g/>
seznamem	seznam	k1gInSc7
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc4
na	na	k7c4
něj	on	k3xPp3gInSc4
přejmenován	přejmenován	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
evidence	evidence	k1gFnSc2
památek	památka	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
založena	založit	k5eAaPmNgFnS
Centrální	centrální	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
soupis	soupis	k1gInSc4
stavebních	stavební	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1872	#num#	k4
se	se	k3xPyFc4
zapisovaly	zapisovat	k5eAaImAgFnP
i	i	k9
movité	movitý	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1907	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
byly	být	k5eAaImAgInP
pod	pod	k7c7
vedením	vedení	k1gNnSc7
konzervátorů	konzervátor	k1gInPc2
Státního	státní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
sepsány	sepsán	k2eAgFnPc4d1
památky	památka	k1gFnPc4
z	z	k7c2
52	#num#	k4
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
22	#num#	k4
<g/>
/	/	kIx~
<g/>
1958	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
kulturních	kulturní	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
§	§	k?
7	#num#	k4
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
památky	památka	k1gFnPc1
se	se	k3xPyFc4
z	z	k7c2
evidenčních	evidenční	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
zapisují	zapisovat	k5eAaImIp3nP
do	do	k7c2
státních	státní	k2eAgInPc2d1
seznamů	seznam	k1gInPc2
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
chráněny	chráněn	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
památky	památka	k1gFnPc1
do	do	k7c2
seznamů	seznam	k1gInPc2
nezapsané	zapsaný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnSc2
zákon	zákon	k1gInSc1
v	v	k7c6
§	§	k?
2	#num#	k4
obecně	obecně	k6eAd1
definoval	definovat	k5eAaBmAgInS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
odst	odst	k2eAgMnSc1d1
<g/>
.	.	kIx.
3	#num#	k4
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
pochybnostech	pochybnost	k1gFnPc6
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
věc	věc	k1gFnSc1
za	za	k7c4
památku	památka	k1gFnSc4
až	až	k9
do	do	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
výkonného	výkonný	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
krajského	krajský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
před	před	k7c7
rozhodnutím	rozhodnutí	k1gNnSc7
vyžádá	vyžádat	k5eAaPmIp3nS
posudek	posudek	k1gInSc4
ústavu	ústav	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1984	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
generální	generální	k2eAgFnSc2d1
aktualizace	aktualizace	k1gFnSc2
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
byly	být	k5eAaImAgInP
seznamy	seznam	k1gInPc1
aktualizovány	aktualizovat	k5eAaBmNgInP
a	a	k8xC
zpřesněny	zpřesnit	k5eAaPmNgInP
<g/>
,	,	kIx,
a	a	k8xC
promítnuty	promítnut	k2eAgFnPc1d1
do	do	k7c2
katastru	katastr	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
památek	památka	k1gFnPc2
snížen	snížen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
,	,	kIx,
změnil	změnit	k5eAaPmAgMnS
seznamy	seznam	k1gInPc4
v	v	k7c6
závazné	závazný	k2eAgFnSc6d1
a	a	k8xC
definující	definující	k2eAgFnSc6d1
<g/>
,	,	kIx,
tj.	tj.	kA
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
věci	věc	k1gFnSc2
prohlášené	prohlášený	k2eAgFnPc1d1
za	za	k7c4
ně	on	k3xPp3gMnPc4
Ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
také	také	k9
památky	památka	k1gFnPc4
zapsané	zapsaný	k2eAgFnPc4d1
do	do	k7c2
státních	státní	k2eAgInPc2d1
seznamů	seznam	k1gInPc2
podle	podle	k7c2
dřívějších	dřívější	k2eAgInPc2d1
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
obnova	obnova	k1gFnSc1
identifikace	identifikace	k1gFnSc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
obnova	obnova	k1gFnSc1
identifikace	identifikace	k1gFnSc2
movitých	movitý	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
proběhla	proběhnout	k5eAaPmAgFnS
revize	revize	k1gFnSc1
památkově	památkově	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnova	obnova	k1gFnSc1
identifikace	identifikace	k1gFnSc1
se	se	k3xPyFc4
označovala	označovat	k5eAaImAgFnS
zkratkou	zkratka	k1gFnSc7
REI	Rea	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stav	stav	k1gInSc1
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
KP	KP	kA
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc4d1
základní	základní	k2eAgInPc4d1
systémy	systém	k1gInPc4
evidence	evidence	k1gFnSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
jsou	být	k5eAaImIp3nP
technologicky	technologicky	k6eAd1
zastaralé	zastaralý	k2eAgInPc1d1
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
na	na	k7c6
jednouživatelských	jednouživatelský	k2eAgInPc6d1
kancelářských	kancelářský	k2eAgInPc6d1
databázových	databázový	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
editaci	editace	k1gFnSc4
dat	datum	k1gNnPc2
pouze	pouze	k6eAd1
několika	několik	k4yIc3
pracovníkům	pracovník	k1gMnPc3
na	na	k7c6
ústředním	ústřední	k2eAgNnSc6d1
pracovišti	pracoviště	k1gNnSc6
NPÚ	NPÚ	kA
a	a	k8xC
neumožňuje	umožňovat	k5eNaImIp3nS
operativní	operativní	k2eAgFnSc4d1
online	onlinout	k5eAaPmIp3nS
editaci	editace	k1gFnSc4
z	z	k7c2
ostatních	ostatní	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
NPÚ	NPÚ	kA
a	a	k8xC
z	z	k7c2
MK	MK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
ani	ani	k8xC
přímé	přímý	k2eAgNnSc4d1
propojení	propojení	k1gNnSc4
s	s	k7c7
ČÚZK	ČÚZK	kA
pro	pro	k7c4
zanášení	zanášení	k1gNnSc4
informace	informace	k1gFnSc2
o	o	k7c6
památkové	památkový	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
do	do	k7c2
katastru	katastr	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
a	a	k8xC
propojení	propojení	k1gNnSc2
s	s	k7c7
územními	územní	k2eAgInPc7d1
články	článek	k1gInPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
krajské	krajský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
úřady	úřad	k1gInPc1
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
obcemi	obec	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
(	(	kIx(
<g/>
nemovité	movitý	k2eNgFnPc1d1
a	a	k8xC
movité	movitý	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
,	,	kIx,
mobiliární	mobiliární	k2eAgInPc1d1
fondy	fond	k1gInPc1
<g/>
,	,	kIx,
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
,	,	kIx,
území	území	k1gNnPc1
s	s	k7c7
archeologickými	archeologický	k2eAgInPc7d1
nálezy	nález	k1gInPc7
<g/>
)	)	kIx)
fungují	fungovat	k5eAaImIp3nP
odděleně	odděleně	k6eAd1
a	a	k8xC
neumožňují	umožňovat	k5eNaImIp3nP
přímé	přímý	k2eAgNnSc4d1
propojení	propojení	k1gNnSc4
souvisejících	související	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popisná	popisný	k2eAgFnSc1d1
a	a	k8xC
geografická	geografický	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
zpracována	zpracovat	k5eAaPmNgNnP
odděleně	odděleně	k6eAd1
<g/>
,	,	kIx,
související	související	k2eAgFnPc1d1
archivní	archivní	k2eAgFnPc1d1
dokumentace	dokumentace	k1gFnPc1
(	(	kIx(
<g/>
evidenční	evidenční	k2eAgFnPc1d1
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnPc1
<g/>
,	,	kIx,
mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
plány	plán	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
spisová	spisový	k2eAgFnSc1d1
agenda	agenda	k1gFnSc1
nejsou	být	k5eNaImIp3nP
digitalizovány	digitalizován	k2eAgMnPc4d1
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
aktuální	aktuální	k2eAgFnPc4d1
digitální	digitální	k2eAgFnPc4d1
polohopisná	polohopisný	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
,	,	kIx,
především	především	k6eAd1
vektorová	vektorový	k2eAgNnPc4d1
data	datum	k1gNnPc4
katastru	katastr	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
dat	datum	k1gNnPc2
jsou	být	k5eAaImIp3nP
již	již	k9
od	od	k7c2
jejich	jejich	k3xOp3gInSc2
vzniku	vznik	k1gInSc2
zanášeny	zanášen	k2eAgFnPc1d1
nepřesnosti	nepřesnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Související	související	k2eAgFnPc1d1
agendy	agenda	k1gFnPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgFnP
na	na	k7c6
oběhu	oběh	k1gInSc6
papírových	papírový	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacitně	kapacitně	k6eAd1
ani	ani	k8xC
finančně	finančně	k6eAd1
není	být	k5eNaImIp3nS
zajištěna	zajištěn	k2eAgFnSc1d1
digitalizace	digitalizace	k1gFnSc1
dostupných	dostupný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
dokumentů	dokument	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomuto	tento	k3xDgInSc3
stavu	stav	k1gInSc3
nelze	lze	k6eNd1
garantovat	garantovat	k5eAaBmF
potřebnou	potřebný	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
obsahu	obsah	k1gInSc2
ÚSKP	ÚSKP	kA
ani	ani	k8xC
zajistit	zajistit	k5eAaPmF
potřebné	potřebný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
při	při	k7c6
poskytování	poskytování	k1gNnSc6
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ani	ani	k8xC
služby	služba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
poskytovány	poskytovat	k5eAaImNgInP
ze	z	k7c2
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
Integrovaného	integrovaný	k2eAgInSc2d1
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
IISPP	IISPP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
budovaného	budovaný	k2eAgInSc2d1
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
Geografický	geografický	k2eAgInSc4d1
informační	informační	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
GIS	gis	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Metainformační	Metainformační	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
MIS	mísa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
do	do	k7c2
systému	systém	k1gInSc2
integrována	integrován	k2eAgNnPc1d1
data	datum	k1gNnPc1
z	z	k7c2
dosud	dosud	k6eAd1
používaných	používaný	k2eAgFnPc2d1
starších	starý	k2eAgFnPc2d2
aplikací	aplikace	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MonumIS	MonumIS	k?
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
lokálních	lokální	k2eAgFnPc2d1
databázových	databázový	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
primárně	primárně	k6eAd1
určených	určený	k2eAgFnPc2d1
pro	pro	k7c4
vedení	vedení	k1gNnSc4
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ČR	ČR	kA
podle	podle	k7c2
památkového	památkový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
v	v	k7c6
letech	let	k1gInPc6
1996	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
vývoj	vývoj	k1gInSc1
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
příprava	příprava	k1gFnSc1
systému	systém	k1gInSc2
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
stávající	stávající	k2eAgInSc1d1
systém	systém	k1gInSc1
nahrazen	nahradit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobiliární	mobiliární	k2eAgInPc1d1
fondy	fond	k1gInPc1
jsou	být	k5eAaImIp3nP
vyčleněny	vyčlenit	k5eAaPmNgFnP
do	do	k7c2
samostatné	samostatný	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
CastIS	CastIS	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MonumNet	MonumNet	k1gInSc1
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
webových	webový	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
určených	určený	k2eAgFnPc2d1
primárně	primárně	k6eAd1
pro	pro	k7c4
prezentaci	prezentace	k1gFnSc4
vybraných	vybraný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
ze	z	k7c2
systému	systém	k1gInSc2
MonumIS	MonumIS	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidruženy	přidružen	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
další	další	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
odborného	odborný	k2eAgInSc2d1
i	i	k8xC
provozního	provozní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
má	mít	k5eAaImIp3nS
veřejnou	veřejný	k2eAgFnSc4d1
část	část	k1gFnSc4
a	a	k8xC
část	část	k1gFnSc4
vyhrazenou	vyhrazený	k2eAgFnSc4d1
pro	pro	k7c4
pracovníky	pracovník	k1gMnPc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
ISAD	ISAD	kA
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
webových	webový	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
sloužících	sloužící	k2eAgFnPc2d1
jako	jako	k9
informační	informační	k2eAgInSc4d1
systém	systém	k1gInSc4
o	o	k7c6
archeologických	archeologický	k2eAgNnPc6d1
datech	datum	k1gNnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
prezentaci	prezentace	k1gFnSc4
dat	datum	k1gNnPc2
Státního	státní	k2eAgInSc2d1
archeologického	archeologický	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
ČR	ČR	kA
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yIgInPc3,k3yRgInPc3,k3yQgInPc3
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
přidružovány	přidružován	k2eAgFnPc1d1
další	další	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
odborného	odborný	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpřístupňuje	zpřístupňovat	k5eAaImIp3nS
též	též	k6eAd1
data	datum	k1gNnPc4
z	z	k7c2
dalších	další	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Obrazová	obrazový	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
archeologických	archeologický	k2eAgNnPc2d1
nalezišť	naleziště	k1gNnPc2
<g/>
,	,	kIx,
Významné	významný	k2eAgFnPc1d1
archeologické	archeologický	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
<g/>
,	,	kIx,
Archeologická	archeologický	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Přehledy	přehled	k1gInPc7
výzkumů	výzkum	k1gInPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
ArtGuard	ArtGuard	k1gMnSc1
je	být	k5eAaImIp3nS
aplkace	aplkace	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
ISO	ISO	kA
(	(	kIx(
<g/>
Integrovaný	integrovaný	k2eAgInSc1d1
systém	systém	k1gInSc1
ochrany	ochrana	k1gFnSc2
movitého	movitý	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
agendě	agenda	k1gFnSc3
evidence	evidence	k1gFnSc2
a	a	k8xC
procesů	proces	k1gInPc2
týkajících	týkající	k2eAgInPc2d1
se	se	k3xPyFc4
prodeje	prodej	k1gInSc2
a	a	k8xC
vývozu	vývoz	k1gInSc2
předmětů	předmět	k1gInPc2
kulturní	kulturní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
též	též	k6eAd1
k	k	k7c3
aktualizaci	aktualizace	k1gFnSc3
údajů	údaj	k1gInPc2
ÚSKP	ÚSKP	kA
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
movitých	movitý	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
CastIS	CastIS	k?
je	být	k5eAaImIp3nS
aplikace	aplikace	k1gFnPc4
pro	pro	k7c4
oddělenou	oddělený	k2eAgFnSc4d1
evidenci	evidence	k1gFnSc4
a	a	k8xC
správu	správa	k1gFnSc4
mobiliárních	mobiliární	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgFnSc2d1
asi	asi	k9
milion	milion	k4xCgInSc4
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
základní	základní	k2eAgFnSc4d1
evidenci	evidence	k1gFnSc4
předmětů	předmět	k1gInPc2
doplněnou	doplněný	k2eAgFnSc4d1
digitálním	digitální	k2eAgNnSc7d1
multimediálním	multimediální	k2eAgNnSc7d1
zpracováním	zpracování	k1gNnSc7
identifikace	identifikace	k1gFnSc2
(	(	kIx(
<g/>
fotografie	fotografia	k1gFnSc2
<g/>
,	,	kIx,
videosekvence	videosekvence	k1gFnSc2
rotace	rotace	k1gFnSc2
ap.	ap.	kA
<g/>
)	)	kIx)
</s>
<s>
Clavius	Clavius	k1gInSc1
je	být	k5eAaImIp3nS
aplikace	aplikace	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
evidenci	evidence	k1gFnSc4
a	a	k8xC
katalogizaci	katalogizace	k1gFnSc4
knihoven	knihovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
aplikace	aplikace	k1gFnSc2
MonumNet	MonumNeta	k1gFnPc2
</s>
<s>
Část	část	k1gFnSc1
evidence	evidence	k1gFnSc1
ze	z	k7c2
systému	systém	k1gInSc2
MonumIS	MonumIS	k1gFnSc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc2d1
nemovité	movitý	k2eNgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
(	(	kIx(
<g/>
památkové	památkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
a	a	k8xC
památkové	památkový	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
světové	světový	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
,	,	kIx,
národní	národní	k2eAgFnPc4d1
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
movitých	movitý	k2eAgFnPc2d1
<g/>
)	)	kIx)
a	a	k8xC
nejohroženější	ohrožený	k2eAgFnPc4d3
nemovité	movitý	k2eNgFnPc4d1
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
veřejně	veřejně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
na	na	k7c6
internetu	internet	k1gInSc6
prostřednictvím	prostřednictvím	k7c2
webové	webový	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
MonumNet	MonumNeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
MonumNet	MonumNet	k1gInSc1
byl	být	k5eAaImAgInS
základem	základ	k1gInSc7
pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
seznamů	seznam	k1gInPc2
českých	český	k2eAgFnPc2d1
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
na	na	k7c6
české	český	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
Wikipedie	Wikipedie	k1gFnSc2
a	a	k8xC
rozšiřováníí	rozšiřováníí	k2eAgFnSc2d1
fotografické	fotografický	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
soutěže	soutěž	k1gFnSc2
Wiki	Wik	k1gFnSc2
miluje	milovat	k5eAaImIp3nS
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
2015	#num#	k4
je	být	k5eAaImIp3nS
jako	jako	k9
nástupce	nástupce	k1gMnSc1
MonumNet	MonumNeta	k1gFnPc2
spuštěn	spustit	k5eAaPmNgInS
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgInSc1d1
portál	portál	k1gInSc1
Monumnet	Monumneta	k1gFnPc2
zůstal	zůstat	k5eAaPmAgInS
přístupný	přístupný	k2eAgInSc1d1
i	i	k9
nadále	nadále	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jako	jako	k9
rozcestník	rozcestník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
datová	datový	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
přístupná	přístupný	k2eAgFnSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
seznam	seznam	k1gInSc1
restaurátorů	restaurátor	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
movitých	movitý	k2eAgFnPc6d1
kulturních	kulturní	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
</s>
<s>
Do	do	k7c2
údajů	údaj	k1gInPc2
o	o	k7c6
movitých	movitý	k2eAgFnPc6d1
kulturních	kulturní	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
mohou	moct	k5eAaImIp3nP
do	do	k7c2
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
nahlížet	nahlížet	k5eAaImF
vlastník	vlastník	k1gMnSc1
movité	movitý	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
osoby	osoba	k1gFnPc1
při	při	k7c6
výkonu	výkon	k1gInSc6
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
tyto	tento	k3xDgInPc1
údaje	údaj	k1gInPc1
potřebují	potřebovat	k5eAaImIp3nP
pro	pro	k7c4
plnění	plnění	k1gNnSc4
svých	svůj	k3xOyFgInPc2
úkolů	úkol	k1gInPc2
<g/>
,	,	kIx,
osoby	osoba	k1gFnPc1
pro	pro	k7c4
studijní	studijní	k2eAgInPc4d1
účely	účel	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
písemného	písemný	k2eAgNnSc2d1
potvrzení	potvrzení	k1gNnSc2
příslušného	příslušný	k2eAgNnSc2d1
školského	školský	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
nebo	nebo	k8xC
příslušné	příslušný	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
a	a	k8xC
osoby	osoba	k1gFnSc2
pro	pro	k7c4
vědeckovýzkumné	vědeckovýzkumný	k2eAgInPc4d1
účely	účel	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
písemného	písemný	k2eAgNnSc2d1
potvrzení	potvrzení	k1gNnSc2
zadavatele	zadavatel	k1gMnSc2
výzkumného	výzkumný	k2eAgInSc2d1
úkolu	úkol	k1gInSc2
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
tak	tak	k6eAd1
mohou	moct	k5eAaImIp3nP
učinit	učinit	k5eAaImF,k5eAaPmF
pouze	pouze	k6eAd1
s	s	k7c7
písemným	písemný	k2eAgInSc7d1
souhlasem	souhlas	k1gInSc7
vlastníka	vlastník	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
a	a	k8xC
digitalizace	digitalizace	k1gFnSc1
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
KP	KP	kA
</s>
<s>
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
programový	programový	k2eAgInSc4d1
dokument	dokument	k1gInSc4
„	„	k?
<g/>
Základní	základní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
Strategie	strategie	k1gFnSc2
efektivní	efektivní	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
a	a	k8xC
přátelské	přátelský	k2eAgFnPc1d1
veřejné	veřejný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
(	(	kIx(
<g/>
Smart	Smart	k1gInSc1
Administration	Administration	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
2007	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
“	“	k?
<g/>
,	,	kIx,
schválený	schválený	k2eAgMnSc1d1
usnesením	usnesení	k1gNnSc7
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
197	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
vláda	vláda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
usnesením	usnesení	k1gNnSc7
č.	č.	k?
1440	#num#	k4
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
vyjádřila	vyjádřit	k5eAaPmAgFnS
souhlas	souhlas	k1gInSc4
s	s	k7c7
předloženým	předložený	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
digitalizace	digitalizace	k1gFnSc2
ÚSKP	ÚSKP	kA
a	a	k8xC
uložila	uložit	k5eAaPmAgFnS
ministru	ministr	k1gMnSc3
kultury	kultura	k1gFnSc2
zpracovat	zpracovat	k5eAaPmF
<g/>
,	,	kIx,
připravit	připravit	k5eAaPmF
a	a	k8xC
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
předložit	předložit	k5eAaPmF
vládě	vláda	k1gFnSc3
návrh	návrh	k1gInSc4
projektu	projekt	k1gInSc2
Uplatnění	uplatnění	k1gNnSc2
principu	princip	k1gInSc2
Smart	Smart	k1gInSc1
Administration	Administration	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
nakládání	nakládání	k1gNnSc2
s	s	k7c7
památkovým	památkový	k2eAgInSc7d1
fondem	fond	k1gInSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
ministru	ministr	k1gMnSc3
vnitra	vnitro	k1gNnSc2
a	a	k8xC
předsedovi	předseda	k1gMnSc3
Českého	český	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
zeměměřického	zeměměřický	k2eAgInSc2d1
a	a	k8xC
katastrálního	katastrální	k2eAgInSc2d1
uložila	uložit	k5eAaPmAgFnS
na	na	k7c6
přípravě	příprava	k1gFnSc6
projektu	projekt	k1gInSc2
s	s	k7c7
ministrem	ministr	k1gMnSc7
kultury	kultura	k1gFnSc2
spolupracovat	spolupracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
pověřilo	pověřit	k5eAaPmAgNnS
přípravou	příprava	k1gFnSc7
projektu	projekt	k1gInSc2
Národní	národní	k2eAgFnSc2d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
reflektuje	reflektovat	k5eAaImIp3nS
i	i	k9
Strategií	strategie	k1gFnSc7
rozvoje	rozvoj	k1gInSc2
služeb	služba	k1gFnPc2
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
na	na	k7c6
období	období	k1gNnSc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
schválila	schválit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
ČR	ČR	kA
usnesením	usnesení	k1gNnSc7
č.	č.	k?
854	#num#	k4
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
veden	vést	k5eAaImNgInS
pod	pod	k7c7
č.	č.	k?
12	#num#	k4
na	na	k7c6
indikativním	indikativní	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
záměrů	záměr	k1gInPc2
strategických	strategický	k2eAgInPc2d1
projektů	projekt	k1gInPc2
pro	pro	k7c4
čerpání	čerpání	k1gNnSc4
prostředků	prostředek	k1gInPc2
ze	z	k7c2
Strukturálních	strukturální	k2eAgInPc2d1
fondů	fond	k1gInPc2
EU	EU	kA
v	v	k7c6
rámci	rámec	k1gInSc6
Smart	Smarta	k1gFnPc2
Administration	Administration	k1gInSc4
–	–	k?
příloze	příloha	k1gFnSc6
ke	k	k7c3
strategii	strategie	k1gFnSc3
Efektivní	efektivní	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
a	a	k8xC
přátelské	přátelský	k2eAgFnPc1d1
veřejné	veřejný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
<g/>
,	,	kIx,
schválené	schválený	k2eAgNnSc1d1
usnesením	usnesení	k1gNnSc7
vlády	vláda	k1gFnSc2
ČR	ČR	kA
č.	č.	k?
536	#num#	k4
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
vytvořen	vytvořit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
elektronický	elektronický	k2eAgInSc1d1
systém	systém	k1gInSc1
evidence	evidence	k1gFnSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
jako	jako	k8xC,k8xS
registr	registr	k1gInSc4
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
má	mít	k5eAaImIp3nS
mít	mít	k5eAaImF
návaznost	návaznost	k1gFnSc4
na	na	k7c4
základní	základní	k2eAgInPc4d1
registry	registr	k1gInPc4
<g/>
,	,	kIx,
především	především	k9
na	na	k7c4
Registr	registr	k1gInSc4
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RUIAN	RUIAN	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
též	též	k9
elektronizovány	elektronizován	k2eAgInPc4d1
procesní	procesní	k2eAgInPc4d1
postupy	postup	k1gInPc4
například	například	k6eAd1
při	při	k7c6
vyhlašování	vyhlašování	k1gNnSc6
památek	památka	k1gFnPc2
a	a	k8xC
zajištěna	zajištěn	k2eAgFnSc1d1
naváznost	naváznost	k1gFnSc1
na	na	k7c4
Registr	registr	k1gInSc4
práv	právo	k1gNnPc2
a	a	k8xC
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zajištěna	zajištěn	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
aktualizace	aktualizace	k1gFnSc1
dat	datum	k1gNnPc2
včetně	včetně	k7c2
přesného	přesný	k2eAgNnSc2d1
prostorového	prostorový	k2eAgNnSc2d1
vymezení	vymezení	k1gNnSc2
památek	památka	k1gFnPc2
v	v	k7c6
geografickém	geografický	k2eAgInSc6d1
informačním	informační	k2eAgInSc6d1
systému	systém	k1gInSc6
(	(	kIx(
<g/>
GIS	gis	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
digitalizována	digitalizován	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
archivní	archivní	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
prostřednictvím	prostřednictvím	k7c2
webových	webový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
a	a	k8xC
komunikačních	komunikační	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
zpřístupněny	zpřístupnit	k5eAaPmNgInP
příslušným	příslušný	k2eAgInPc3d1
správním	správní	k2eAgInPc3d1
úřadům	úřad	k1gInPc3
<g/>
,	,	kIx,
dotčeným	dotčený	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
atd.	atd.	kA
a	a	k8xC
též	též	k9
zveřejněny	zveřejnit	k5eAaPmNgInP
prostřednictvím	prostřednictvím	k7c2
portálu	portál	k1gInSc2
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
elektronických	elektronický	k2eAgInPc2d1
formulářů	formulář	k1gInPc2
pro	pro	k7c4
podávání	podávání	k1gNnSc4
souvisejících	související	k2eAgFnPc2d1
žádostí	žádost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nedatovaný	datovaný	k2eNgInSc1d1
projekt	projekt	k1gInSc1
digitalizace	digitalizace	k1gFnSc2
zveřejněný	zveřejněný	k2eAgInSc1d1
na	na	k7c6
webu	web	k1gInSc6
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČR	ČR	kA
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
etap	etapa	k1gFnPc2
se	s	k7c7
stanovenými	stanovený	k2eAgInPc7d1
termíny	termín	k1gInPc7
a	a	k8xC
vyčísleným	vyčíslený	k2eAgInSc7d1
odhadem	odhad	k1gInSc7
nákladů	náklad	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
celkově	celkově	k6eAd1
vyčísleny	vyčíslen	k2eAgFnPc1d1
na	na	k7c4
230	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
.	.	kIx.
85	#num#	k4
%	%	kIx~
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
hrazeno	hradit	k5eAaImNgNnS
ze	z	k7c2
strukturálních	strukturální	k2eAgInPc2d1
fondů	fond	k1gInPc2
EU	EU	kA
–	–	k?
Evropského	evropský	k2eAgInSc2d1
fondu	fond	k1gInSc2
pro	pro	k7c4
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
ERDF	ERDF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbylých	zbylý	k2eAgNnPc2d1
15	#num#	k4
%	%	kIx~
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasazení	nasazení	k1gNnSc1
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
správu	správa	k1gFnSc4
dat	datum	k1gNnPc2
do	do	k7c2
ostrého	ostrý	k2eAgInSc2d1
provozu	provoz	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
NPÚ	NPÚ	kA
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
na	na	k7c4
průběh	průběh	k1gInSc4
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
dokončení	dokončení	k1gNnSc1
procesní	procesní	k2eAgFnSc2d1
a	a	k8xC
integrační	integrační	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
včetně	včetně	k7c2
zpřístupnění	zpřístupnění	k1gNnPc2
dat	datum	k1gNnPc2
bylo	být	k5eAaImAgNnS
směrováno	směrovat	k5eAaImNgNnS
na	na	k7c4
konec	konec	k1gInSc4
roku	rok	k1gInSc2
2013	#num#	k4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
digitalizace	digitalizace	k1gFnSc1
geografických	geografický	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
archivní	archivní	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
by	by	kYmCp3nS
přesahovala	přesahovat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etapy	etapa	k1gFnPc1
1	#num#	k4
až	až	k8xS
4	#num#	k4
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
vývoje	vývoj	k1gInPc1
a	a	k8xC
nasazení	nasazení	k1gNnSc1
aplikací	aplikace	k1gFnPc2
<g/>
:	:	kIx,
pro	pro	k7c4
1	#num#	k4
<g/>
.	.	kIx.
etapu	etapa	k1gFnSc4
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
analýza	analýza	k1gFnSc1
<g/>
,	,	kIx,
návrh	návrh	k1gInSc1
zadání	zadání	k1gNnSc2
<g/>
)	)	kIx)
uvádí	uvádět	k5eAaImIp3nS
období	období	k1gNnSc4
od	od	k7c2
září	září	k1gNnSc2
2007	#num#	k4
do	do	k7c2
konce	konec	k1gInSc2
září	září	k1gNnSc4
2009	#num#	k4
a	a	k8xC
náklady	náklad	k1gInPc1
11,2	11,2	k4
milionu	milion	k4xCgInSc2
Kč	Kč	kA
<g/>
,	,	kIx,
pro	pro	k7c4
druhou	druhý	k4xOgFnSc4
etapu	etapa	k1gFnSc4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
realizaci	realizace	k1gFnSc4
aplikací	aplikace	k1gFnPc2
<g/>
)	)	kIx)
navazující	navazující	k2eAgNnSc1d1
období	období	k1gNnSc1
do	do	k7c2
konce	konec	k1gInSc2
června	červen	k1gInSc2
2010	#num#	k4
a	a	k8xC
náklady	náklad	k1gInPc1
42,65	42,65	k4
milionu	milion	k4xCgInSc2
Kč	Kč	kA
<g/>
,	,	kIx,
pro	pro	k7c4
3	#num#	k4
<g/>
.	.	kIx.
etapu	etapa	k1gFnSc4
(	(	kIx(
<g/>
ověřovací	ověřovací	k2eAgInSc1d1
provoz	provoz	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vyhodnocení	vyhodnocení	k1gNnSc1
<g/>
)	)	kIx)
navazující	navazující	k2eAgNnSc1d1
období	období	k1gNnSc1
do	do	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
2010	#num#	k4
a	a	k8xC
náklady	náklad	k1gInPc1
8,25	8,25	k4
milionu	milion	k4xCgInSc2
Kč	Kč	kA
<g/>
,	,	kIx,
pro	pro	k7c4
4	#num#	k4
<g/>
.	.	kIx.
etapu	etapa	k1gFnSc4
(	(	kIx(
<g/>
rekonfigurace	rekonfigurace	k1gFnSc1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
ostrá	ostrý	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
dat	datum	k1gNnPc2
<g/>
)	)	kIx)
navazující	navazující	k2eAgNnPc1d1
období	období	k1gNnPc1
do	do	k7c2
konce	konec	k1gInSc2
ledna	leden	k1gInSc2
2011	#num#	k4
a	a	k8xC
náklady	náklad	k1gInPc7
5	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
pátá	pátý	k4xOgFnSc1
etapa	etapa	k1gFnSc1
je	být	k5eAaImIp3nS
označeno	označen	k2eAgNnSc4d1
vybavování	vybavování	k1gNnSc4
pracovišť	pracoviště	k1gNnPc2
komunikační	komunikační	k2eAgInSc4d1
infrastrukturou	infrastruktura	k1gFnSc7
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2009	#num#	k4
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
nákladem	náklad	k1gInSc7
32,7	32,7	k4
milionu	milion	k4xCgInSc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
6	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
termínem	termín	k1gInSc7
od	od	k7c2
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
prosince	prosinec	k1gInSc2
2015	#num#	k4
a	a	k8xC
náklady	náklad	k1gInPc7
55,4	55,4	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
uvedeno	uveden	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
geografických	geografický	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
digitalizace	digitalizace	k1gFnSc2
archivní	archivní	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmou	sedmý	k4xOgFnSc4
etapu	etapa	k1gFnSc4
je	být	k5eAaImIp3nS
návrh	návrh	k1gInSc1
a	a	k8xC
vytvoření	vytvoření	k1gNnSc1
integrační	integrační	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
zpřístupnění	zpřístupnění	k1gNnSc1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
náklady	náklad	k1gInPc7
35,4	35,4	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
sedmou	sedmý	k4xOgFnSc7
etapou	etapa	k1gFnSc7
je	být	k5eAaImIp3nS
elektronizace	elektronizace	k1gFnSc1
procesů	proces	k1gInPc2
souvisejících	související	k2eAgFnPc2d1
agend	agenda	k1gFnPc2
(	(	kIx(
<g/>
termín	termín	k1gInSc1
shodný	shodný	k2eAgInSc1d1
s	s	k7c7
6	#num#	k4
<g/>
.	.	kIx.
etapou	etapa	k1gFnSc7
<g/>
,	,	kIx,
náklady	náklad	k1gInPc7
39,4	39,4	k4
miliony	milion	k4xCgInPc4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zadávací	zadávací	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
na	na	k7c4
návrh	návrh	k1gInSc4
a	a	k8xC
implementaci	implementace	k1gFnSc4
aplikace	aplikace	k1gFnSc2
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
datovaná	datovaný	k2eAgFnSc1d1
rokem	rok	k1gInSc7
2011	#num#	k4
<g/>
,	,	kIx,
požadovala	požadovat	k5eAaImAgFnS
umožnit	umožnit	k5eAaPmF
evidenci	evidence	k1gFnSc4
nejen	nejen	k6eAd1
vyhlášených	vyhlášený	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dalších	další	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
status	status	k1gInSc4
KP	KP	kA
nemají	mít	k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
památkově	památkově	k6eAd1
chráněných	chráněný	k2eAgNnPc6d1
územích	území	k1gNnPc6
či	či	k8xC
v	v	k7c6
prostředí	prostředí	k1gNnSc6
KP	KP	kA
nebo	nebo	k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
objekty	objekt	k1gInPc4
či	či	k8xC
území	území	k1gNnSc4
s	s	k7c7
nepochybnou	pochybný	k2eNgFnSc7d1
kulturně	kulturně	k6eAd1
historickou	historický	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
místního	místní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
nová	nový	k2eAgFnSc1d1
část	část	k1gFnSc1
s	s	k7c7
pracovním	pracovní	k2eAgInSc7d1
názvem	název	k1gInSc7
Soupis	soupis	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zdrojovou	zdrojový	k2eAgFnSc7d1
bází	báze	k1gFnSc7
i	i	k9
pro	pro	k7c4
podávání	podávání	k1gNnSc4
návrhů	návrh	k1gInPc2
na	na	k7c4
prohlášení	prohlášení	k1gNnSc4
za	za	k7c4
KP	KP	kA
<g/>
,	,	kIx,
podkladem	podklad	k1gInSc7
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
plánů	plán	k1gInPc2
ochrany	ochrana	k1gFnSc2
a	a	k8xC
poskytování	poskytování	k1gNnSc1
podrobných	podrobný	k2eAgInPc2d1
územně	územně	k6eAd1
analytických	analytický	k2eAgInPc2d1
podkladů	podklad	k1gInPc2
(	(	kIx(
<g/>
ÚAP	ÚAP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
NPÚ	NPÚ	kA
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
koncipován	koncipovat	k5eAaBmNgInS
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
Integrovaného	integrovaný	k2eAgInSc2d1
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
IISPP	IISPP	kA
<g/>
)	)	kIx)
a	a	k8xC
zohledňovat	zohledňovat	k5eAaImF
vazby	vazba	k1gFnPc4
na	na	k7c4
další	další	k2eAgFnPc4d1
části	část	k1gFnPc4
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
(	(	kIx(
<g/>
Geografický	geografický	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
GIS	gis	k1gNnSc2
<g/>
,	,	kIx,
Metainformační	Metainformační	k2eAgInSc1d1
systém	systém	k1gInSc1
MIS	mísa	k1gFnPc2
<g/>
,	,	kIx,
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
o	o	k7c6
archeologických	archeologický	k2eAgNnPc6d1
datech	datum	k1gNnPc6
ISAD	ISAD	kA
<g/>
,	,	kIx,
Tritius	Tritius	k1gMnSc1
<g/>
,	,	kIx,
CastIS	CastIS	k1gMnSc1
<g/>
,	,	kIx,
ArtGuard	ArtGuard	k1gMnSc1
<g/>
,	,	kIx,
DMS	DMS	kA
<g/>
,	,	kIx,
Intranet	intranet	k1gInSc1
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
Elektronická	elektronický	k2eAgFnSc1d1
spisová	spisový	k2eAgFnSc1d1
služba	služba	k1gFnSc1
ESS	ESS	kA
a	a	k8xC
Centrální	centrální	k2eAgFnSc1d1
správa	správa	k1gFnSc1
číselníků	číselník	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
databáze	databáze	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
jeden	jeden	k4xCgInSc1
záznam	záznam	k1gInSc1
o	o	k7c6
„	„	k?
<g/>
památkové	památkový	k2eAgFnSc2d1
věci	věc	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
heritage	heritage	k1gInSc1
asset	asset	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
objektem	objekt	k1gInSc7
památkového	památkový	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
<g/>
,	,	kIx,
souborem	soubor	k1gInSc7
objektů	objekt	k1gInPc2
nebo	nebo	k8xC
částí	část	k1gFnSc7
KP	KP	kA
<g/>
,	,	kIx,
částí	část	k1gFnSc7
části	část	k1gFnSc2
KP	KP	kA
(	(	kIx(
<g/>
atd.	atd.	kA
až	až	k9
do	do	k7c2
jakékoli	jakýkoli	k3yIgFnSc2
libovolné	libovolný	k2eAgFnSc2d1
podrobnosti	podrobnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
částí	část	k1gFnSc7
objektu	objekt	k1gInSc2
památkového	památkový	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
(	(	kIx(
<g/>
opět	opět	k6eAd1
do	do	k7c2
jakékoli	jakýkoli	k3yIgFnSc2
podrobnosti	podrobnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
památkovou	památkový	k2eAgFnSc7d1
zónou	zóna	k1gFnSc7
<g/>
,	,	kIx,
rezervací	rezervace	k1gFnSc7
nebo	nebo	k8xC
ochranným	ochranný	k2eAgNnSc7d1
pásmem	pásmo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
záznam	záznam	k1gInSc1
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
identifikátor	identifikátor	k1gInSc1
a	a	k8xC
mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
vlastnosti	vlastnost	k1gFnPc4
(	(	kIx(
<g/>
atributy	atribut	k1gInPc4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
patřit	patřit	k5eAaImF
i	i	k9
právní	právní	k2eAgInSc1d1
vztah	vztah	k1gInSc1
a	a	k8xC
vazba	vazba	k1gFnSc1
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
záznamy	záznam	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geografická	geografický	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
prováděna	provádět	k5eAaImNgFnS
prostřednictvím	prostřednictvím	k7c2
definičních	definiční	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
polygonové	polygonový	k2eAgInPc1d1
prvky	prvek	k1gInPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
na	na	k7c6
úrovni	úroveň	k1gFnSc6
statistických	statistický	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
nově	nově	k6eAd1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
památkového	památkový	k2eAgInSc2d1
katalogu	katalog	k1gInSc2
NPÚ	NPÚ	kA
a	a	k8xC
IISPP	IISPP	kA
generovány	generovat	k5eAaImNgFnP
v	v	k7c4
GIS	gis	k1gNnPc4
rovněž	rovněž	k9
prostorové	prostorový	k2eAgFnPc4d1
relace	relace	k1gFnPc4
s	s	k7c7
polygony	polygon	k1gInPc7
rozsahu	rozsah	k1gInSc2
ochrany	ochrana	k1gFnSc2
vymezenými	vymezený	k2eAgInPc7d1
v	v	k7c6
datové	datový	k2eAgFnSc6d1
sadě	sada	k1gFnSc6
A1	A1	k1gFnSc2
GIS	gis	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
problém	problém	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
dokumentaci	dokumentace	k1gFnSc6
popisováno	popisovat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
NPÚ	NPÚ	kA
nemá	mít	k5eNaImIp3nS
online	onlinout	k5eAaPmIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
definičním	definiční	k2eAgInPc3d1
bodům	bod	k1gInPc3
parcel	parcela	k1gFnPc2
a	a	k8xC
polygonům	polygon	k1gInPc3
parcel	parcela	k1gFnPc2
katastru	katastr	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
má	mít	k5eAaImIp3nS
evidovat	evidovat	k5eAaImF
všechny	všechen	k3xTgFnPc4
provedené	provedený	k2eAgFnPc4d1
datové	datový	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
projekt	projekt	k1gInSc4
„	„	k?
<g/>
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
(	(	kIx(
<g/>
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
evidence	evidence	k1gFnSc2
památek	památka	k1gFnPc2
ve	v	k7c6
vazbě	vazba	k1gFnSc6
na	na	k7c4
RUIAN	RUIAN	kA
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
,	,	kIx,
plánovaný	plánovaný	k2eAgInSc4d1
na	na	k7c6
období	období	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Národnímu	národní	k2eAgInSc3d1
památkovému	památkový	k2eAgInSc3d1
ústavu	ústav	k1gInSc3
přiznána	přiznán	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
dotace	dotace	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
žádosti	žádost	k1gFnSc2
předložené	předložený	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
výzvy	výzva	k1gFnSc2
č.	č.	k?
17	#num#	k4
Integrovaného	integrovaný	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
jednoho	jeden	k4xCgInSc2
z	z	k7c2
operačních	operační	k2eAgInPc2d1
programů	program	k1gInPc2
EU	EU	kA
v	v	k7c6
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prioritní	prioritní	k2eAgFnSc1d1
osa	osa	k1gFnSc1
1	#num#	k4
<g/>
a	a	k8xC
a	a	k8xC
1	#num#	k4
<g/>
b	b	k?
–	–	k?
Rozvoj	rozvoj	k1gInSc4
informační	informační	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
ve	v	k7c6
veřejné	veřejný	k2eAgFnSc6d1
správě	správa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
projekt	projekt	k1gInSc4
„	„	k?
<g/>
Digitalizace	digitalizace	k1gFnSc2
fondů	fond	k1gInPc2
NPÚ	NPÚ	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
plánovaný	plánovaný	k2eAgInSc1d1
rovněž	rovněž	k9
na	na	k7c6
období	období	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Národnímu	národní	k2eAgInSc3d1
památkovému	památkový	k2eAgInSc3d1
ústavu	ústav	k1gInSc3
přiznána	přiznán	k2eAgFnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
dotace	dotace	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
žádosti	žádost	k1gFnSc2
předložené	předložený	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
výzvy	výzva	k1gFnSc2
č.	č.	k?
17	#num#	k4
Integrovaného	integrovaný	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
prioritní	prioritní	k2eAgFnSc1d1
osa	osa	k1gFnSc1
6.1	6.1	k4
<g/>
a	a	k8xC
Modernizace	modernizace	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
–	–	k?
cíl	cíl	k1gInSc1
Konvergence	konvergence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
části	část	k1gFnSc6
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
digitalizace	digitalizace	k1gFnSc1
dokumentačního	dokumentační	k2eAgInSc2d1
a	a	k8xC
fotografického	fotografický	k2eAgInSc2d1
fondu	fond	k1gInSc2
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
tj.	tj.	kA
fotosbírky	fotosbírka	k1gFnSc2
<g/>
,	,	kIx,
mobiliárních	mobiliární	k2eAgInPc2d1
fondů	fond	k1gInPc2
a	a	k8xC
základní	základní	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
související	související	k2eAgFnPc4d1
s	s	k7c7
agendou	agenda	k1gFnSc7
vedení	vedení	k1gNnSc2
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
projektu	projekt	k1gInSc2
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
ve	v	k7c6
specializovaném	specializovaný	k2eAgNnSc6d1
digitalizačním	digitalizační	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
na	na	k7c6
špičkových	špičkový	k2eAgInPc6d1
strojích	stroj	k1gInPc6
bezkontaktně	bezkontaktně	k6eAd1
skenovány	skenovat	k5eAaImNgFnP
části	část	k1gFnPc1
historických	historický	k2eAgInPc2d1
fondů	fond	k1gInPc2
NPÚ	NPÚ	kA
(	(	kIx(
<g/>
grafiky	grafika	k1gFnSc2
<g/>
,	,	kIx,
obrazy	obraz	k1gInPc4
<g/>
,	,	kIx,
textilie	textilie	k1gFnPc4
apod.	apod.	kA
<g/>
)	)	kIx)
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c6
mimořádně	mimořádně	k6eAd1
ohrožené	ohrožený	k2eAgInPc4d1
dokumentační	dokumentační	k2eAgInPc4d1
fondy	fond	k1gInPc4
–	–	k?
historické	historický	k2eAgFnPc4d1
fotografie	fotografia	k1gFnPc4
a	a	k8xC
skleněné	skleněný	k2eAgInPc4d1
negativy	negativ	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
obou	dva	k4xCgInPc2
projektů	projekt	k1gInPc2
činí	činit	k5eAaImIp3nS
podle	podle	k7c2
tiskové	tiskový	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
bezmála	bezmála	k6eAd1
39	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
projekt	projekt	k1gInSc4
„	„	k?
<g/>
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
“	“	k?
o	o	k7c6
celkovém	celkový	k2eAgInSc6d1
rozpočtu	rozpočet	k1gInSc6
21	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
přispějí	přispět	k5eAaPmIp3nP
strukturální	strukturální	k2eAgInPc1d1
fondy	fond	k1gInPc1
18	#num#	k4
miliony	milion	k4xCgInPc4
Kč	Kč	kA
<g/>
,	,	kIx,
na	na	k7c4
projekt	projekt	k1gInSc4
Digitalizace	digitalizace	k1gFnSc2
fondů	fond	k1gInPc2
NPÚ	NPÚ	kA
o	o	k7c6
rozpočtu	rozpočet	k1gInSc6
necelých	celý	k2eNgInPc2d1
18	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
přispějí	přispět	k5eAaPmIp3nP
15	#num#	k4
miliony	milion	k4xCgInPc4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
Památkového	památkový	k2eAgInSc2d1
katalogu	katalog	k1gInSc2
řídí	řídit	k5eAaImIp3nS
Ing.	ing.	kA
Petr	Petr	k1gMnSc1
Volfík	Volfík	k1gMnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
odboru	odbor	k1gInSc2
informatiky	informatika	k1gFnSc2
GŘ	GŘ	kA
NPÚ	NPÚ	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
chráněného	chráněný	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
</s>
<s>
Návrhy	návrh	k1gInPc1
nového	nový	k2eAgInSc2d1
Zákona	zákon	k1gInSc2
o	o	k7c6
památkovém	památkový	k2eAgInSc6d1
fondu	fond	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
a	a	k8xC
Zákona	zákon	k1gInSc2
o	o	k7c6
ochraně	ochrana	k1gFnSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
z	z	k7c2
let	léto	k1gNnPc2
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
nazývají	nazývat	k5eAaImIp3nP
ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
památek	památka	k1gFnPc2
a	a	k8xC
památkových	památkový	k2eAgNnPc2d1
území	území	k1gNnSc2
Seznam	seznam	k1gInSc4
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
či	či	k8xC
Seznam	seznam	k1gInSc4
chráněného	chráněný	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
návrhu	návrh	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
do	do	k7c2
nového	nový	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
památky	památka	k1gFnSc2
zapisovány	zapisován	k2eAgFnPc4d1
pouze	pouze	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
stanovených	stanovený	k2eAgFnPc2d1
náležitostí	náležitost	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
památky	památka	k1gFnPc1
<g/>
,	,	kIx,
o	o	k7c4
jejichž	jejichž	k3xOyRp3gNnSc4
prohlášení	prohlášení	k1gNnSc4
památkou	památka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
dostatečné	dostatečný	k2eAgInPc1d1
doklady	doklad	k1gInPc1
<g/>
,	,	kIx,
by	by	kYmCp3nP
musely	muset	k5eAaImAgFnP
projít	projít	k5eAaPmF
novým	nový	k2eAgNnSc7d1
správním	správní	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
ochrana	ochrana	k1gFnSc1
po	po	k7c6
přechodném	přechodný	k2eAgNnSc6d1
období	období	k1gNnSc6
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
návrh	návrh	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
předpokládá	předpokládat	k5eAaImIp3nS
opět	opět	k6eAd1
pouhé	pouhý	k2eAgNnSc4d1
automatické	automatický	k2eAgNnSc4d1
převzetí	převzetí	k1gNnSc4
celého	celý	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
de	de	k?
facto	fact	k2eAgNnSc1d1
pouhé	pouhý	k2eAgNnSc1d1
přejmenování	přejmenování	k1gNnSc1
původního	původní	k2eAgInSc2d1
ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
návrhy	návrh	k1gInPc1
(	(	kIx(
<g/>
2008	#num#	k4
i	i	k9
2013	#num#	k4
<g/>
)	)	kIx)
předpokládaly	předpokládat	k5eAaImAgFnP
zavedení	zavedení	k1gNnSc4
statusu	status	k1gInSc2
tzv.	tzv.	kA
památek	památka	k1gFnPc2
místního	místní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
až	až	k9
návrh	návrh	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
však	však	k9
požaduje	požadovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
evidenci	evidence	k1gFnSc4
v	v	k7c6
centrálním	centrální	k2eAgInSc6d1
Seznamu	seznam	k1gInSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
informačním	informační	k2eAgInSc7d1
systémem	systém	k1gInSc7
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
správcem	správce	k1gMnSc7
by	by	k9
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
a	a	k8xC
provozovatelem	provozovatel	k1gMnSc7
odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
propojen	propojit	k5eAaPmNgInS
se	s	k7c7
základním	základní	k2eAgNnSc7d1
registrem	registrum	k1gNnSc7
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
o	o	k7c6
ochraně	ochrana	k1gFnSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
schválený	schválený	k2eAgInSc4d1
vládou	vláda	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
navrženém	navržený	k2eAgInSc6d1
§	§	k?
42	#num#	k4
definuje	definovat	k5eAaBmIp3nS
seznam	seznam	k1gInSc4
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
(	(	kIx(
<g/>
zmiňuje	zmiňovat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
s	s	k7c7
malým	malý	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
jako	jako	k9
obecný	obecný	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
účelově	účelově	k6eAd1
zaměřeným	zaměřený	k2eAgNnSc7d1
a	a	k8xC
soustavně	soustavně	k6eAd1
doplňovaným	doplňovaný	k2eAgInSc7d1
souborem	soubor	k1gInSc7
údajů	údaj	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
podkladů	podklad	k1gInPc2
stanovených	stanovený	k2eAgInPc2d1
tímto	tento	k3xDgInSc7
zákonem	zákon	k1gInSc7
<g/>
,	,	kIx,
do	do	k7c2
nějž	jenž	k3xRgInSc2
se	se	k3xPyFc4
zapisují	zapisovat	k5eAaImIp3nP
údaje	údaj	k1gInPc1
o	o	k7c6
kulturních	kulturní	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
<g/>
,	,	kIx,
národních	národní	k2eAgFnPc6d1
kulturních	kulturní	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
a	a	k8xC
o	o	k7c6
památkových	památkový	k2eAgNnPc6d1
územích	území	k1gNnPc6
a	a	k8xC
ochranných	ochranný	k2eAgNnPc6d1
památkových	památkový	k2eAgNnPc6d1
pásmech	pásmo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnPc4
místního	místní	k2eAgInSc2d1
významu	význam	k1gInSc2
tento	tento	k3xDgInSc4
návrh	návrh	k1gInSc4
zákona	zákon	k1gInSc2
již	již	k6eAd1
nezmiňuje	zmiňovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
navržených	navržený	k2eAgNnPc2d1
přechodných	přechodný	k2eAgNnPc2d1
ustanovení	ustanovení	k1gNnSc2
se	se	k3xPyFc4
kulturní	kulturní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
chráněné	chráněný	k2eAgFnPc1d1
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ve	v	k7c6
znění	znění	k1gNnSc6
účinném	účinný	k2eAgNnSc6d1
přede	příst	k5eAaImIp3nS
dnem	den	k1gInSc7
nabytí	nabytí	k1gNnSc2
účinnosti	účinnost	k1gFnSc2
nového	nový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
považují	považovat	k5eAaImIp3nP
za	za	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
i	i	k9
podle	podle	k7c2
nového	nový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
převzaty	převzít	k5eAaPmNgInP
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
i	i	k9
prohlášené	prohlášený	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
památkové	památkový	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
<g/>
,	,	kIx,
památkové	památkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
a	a	k8xC
ochranná	ochranný	k2eAgNnPc1d1
památková	památkový	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
všech	všecek	k3xTgInPc2
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaje	údaj	k1gInPc1
obsažené	obsažený	k2eAgInPc1d1
v	v	k7c6
Ústředním	ústřední	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
ke	k	k7c3
dni	den	k1gInSc3
účinnosti	účinnost	k1gFnSc2
nového	nový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
mají	mít	k5eAaImIp3nP
zapsat	zapsat	k5eAaPmF
do	do	k7c2
seznamu	seznam	k1gInSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
veřejné	veřejný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
v	v	k7c6
seznamu	seznam	k1gInSc6
zákon	zákon	k1gInSc1
stanoví	stanovit	k5eAaPmIp3nS
fikci	fikce	k1gFnSc4
úplnosti	úplnost	k1gFnSc2
a	a	k8xC
pravdivosti	pravdivost	k1gFnSc2
(	(	kIx(
<g/>
O	o	k7c6
údajích	údaj	k1gInPc6
seznamu	seznam	k1gInSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
veřejné	veřejný	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
zapsány	zapsat	k5eAaPmNgInP
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
skutečným	skutečný	k2eAgInSc7d1
právním	právní	k2eAgInSc7d1
stavem	stav	k1gInSc7
a	a	k8xC
že	že	k8xS
jsou	být	k5eAaImIp3nP
úplné	úplný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejnými	veřejný	k2eAgInPc7d1
údaji	údaj	k1gInPc7
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
název	název	k1gInSc1
</s>
<s>
bližší	blízký	k2eAgFnSc1d2
identifikace	identifikace	k1gFnSc1
(	(	kIx(
<g/>
identifikační	identifikační	k2eAgInSc1d1
údaj	údaj	k1gInSc1
o	o	k7c6
územním	územní	k2eAgInSc6d1
prvku	prvek	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
prvků	prvek	k1gInPc2
v	v	k7c6
RÚIAN	RÚIAN	kA
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
slovní	slovní	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
rejstříkové	rejstříkový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
údaj	údaj	k1gInSc1
o	o	k7c6
vzniku	vznik	k1gInSc6
ochrany	ochrana	k1gFnSc2
<g/>
:	:	kIx,
datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
a	a	k8xC
datum	datum	k1gNnSc1
nabytí	nabytí	k1gNnSc2
právní	právní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
,	,	kIx,
právního	právní	k2eAgInSc2d1
aktu	akt	k1gInSc2
nebo	nebo	k8xC
právního	právní	k2eAgInSc2d1
předpisu	předpis	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
zapsané	zapsaný	k2eAgFnSc6d1
do	do	k7c2
státního	státní	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
datum	datum	k1gInSc1
zápisu	zápis	k1gInSc2
do	do	k7c2
tohoto	tento	k3xDgInSc2
seznamu	seznam	k1gInSc2
</s>
<s>
údaj	údaj	k1gInSc1
o	o	k7c6
změně	změna	k1gFnSc6
rozsahu	rozsah	k1gInSc2
ochrany	ochrana	k1gFnSc2
</s>
<s>
údaj	údaj	k1gInSc4
o	o	k7c6
mezinárodně	mezinárodně	k6eAd1
právní	právní	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
</s>
<s>
údaje	údaj	k1gInPc1
o	o	k7c6
vymezení	vymezení	k1gNnSc6
ochranného	ochranný	k2eAgNnSc2d1
památkového	památkový	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
</s>
<s>
u	u	k7c2
památkových	památkový	k2eAgNnPc2d1
území	území	k1gNnPc2
údaje	údaj	k1gInSc2
o	o	k7c6
plánu	plán	k1gInSc6
ochrany	ochrana	k1gFnSc2
</s>
<s>
Veřejný	veřejný	k2eAgInSc1d1
podklad	podklad	k1gInSc1
seznamu	seznam	k1gInSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
má	mít	k5eAaImIp3nS
dále	daleko	k6eAd2
tvořit	tvořit	k5eAaImF
fotodokumentace	fotodokumentace	k1gFnPc4
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
nebo	nebo	k8xC
památkového	památkový	k2eAgNnSc2d1
území	území	k1gNnSc2
pořízená	pořízený	k2eAgFnSc1d1
z	z	k7c2
veřejného	veřejný	k2eAgNnSc2d1
prostranství	prostranství	k1gNnSc2
a	a	k8xC
plán	plán	k1gInSc1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
neveřejné	veřejný	k2eNgInPc1d1
údaje	údaj	k1gInPc1
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
u	u	k7c2
věcí	věc	k1gFnPc2
a	a	k8xC
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
neevidují	evidovat	k5eNaImIp3nP
v	v	k7c6
katastru	katastr	k1gInSc6
nemovitostí	nemovitost	k1gFnPc2
<g/>
,	,	kIx,
zapisovat	zapisovat	k5eAaImF
údaje	údaj	k1gInPc4
o	o	k7c6
vlastníkovi	vlastník	k1gMnSc6
a	a	k8xC
údaj	údaj	k1gInSc4
o	o	k7c4
umístění	umístění	k1gNnSc4
věci	věc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neveřejný	veřejný	k2eNgInSc4d1
podklad	podklad	k1gInSc4
seznamu	seznam	k1gInSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
dále	daleko	k6eAd2
tvoří	tvořit	k5eAaImIp3nS
fotografická	fotografický	k2eAgFnSc1d1
a	a	k8xC
další	další	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
nad	nad	k7c4
rámec	rámec	k1gInSc4
veřejné	veřejný	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k9
například	například	k6eAd1
foto	foto	k1gNnSc1
z	z	k7c2
interiérů	interiér	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozhodnutí	rozhodnutí	k1gNnSc1
nebo	nebo	k8xC
závazná	závazný	k2eAgNnPc1d1
stanoviska	stanovisko	k1gNnPc1
orgánu	orgán	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
o	o	k7c6
věcech	věc	k1gFnPc6
v	v	k7c6
památkovém	památkový	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
nejsou	být	k5eNaImIp3nP
kulturními	kulturní	k2eAgFnPc7d1
památkami	památka	k1gFnPc7
<g/>
,	,	kIx,
stavebně	stavebně	k6eAd1
historický	historický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
<g/>
,	,	kIx,
závěrečná	závěrečný	k2eAgFnSc1d1
restaurátorská	restaurátorský	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
operativní	operativní	k2eAgFnSc1d1
a	a	k8xC
další	další	k2eAgFnSc1d1
průzkumná	průzkumný	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Údaj	údaj	k1gInSc4
stanovený	stanovený	k2eAgInSc4d1
zákonem	zákon	k1gInSc7
má	mít	k5eAaImIp3nS
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
zapsat	zapsat	k5eAaPmF
do	do	k7c2
seznamu	seznam	k1gInSc2
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
bezodkladně	bezodkladně	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gMnSc4
obdrží	obdržet	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaj	údaj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
základě	základ	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
zapsat	zapsat	k5eAaPmF
ve	v	k7c6
lhůtě	lhůta	k1gFnSc6
10	#num#	k4
dnů	den	k1gInPc2
ode	ode	k7c2
dne	den	k1gInSc2
vzniku	vznik	k1gInSc2
tohoto	tento	k3xDgInSc2
údaje	údaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapsané	zapsaný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
„	„	k?
<g/>
zpracovávat	zpracovávat	k5eAaImF
<g/>
“	“	k?
(	(	kIx(
<g/>
tj.	tj.	kA
uchovávat	uchovávat	k5eAaImF
<g/>
)	)	kIx)
trvale	trvale	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
údajů	údaj	k1gInPc2
o	o	k7c4
vlastnictví	vlastnictví	k1gNnSc4
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
uchovávají	uchovávat	k5eAaImIp3nP
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
od	od	k7c2
zápisu	zápis	k1gInSc2
zániku	zánik	k1gInSc2
památky	památka	k1gFnSc2
nebo	nebo	k8xC
zániku	zánik	k1gInSc2
její	její	k3xOp3gFnSc2
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chybné	chybný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vznikly	vzniknout	k5eAaPmAgInP
zřejmým	zřejmý	k2eAgInSc7d1
omylem	omyl	k1gInSc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
opravit	opravit	k5eAaPmF
„	„	k?
<g/>
na	na	k7c4
písemný	písemný	k2eAgInSc4d1
návrh	návrh	k1gInSc4
vlastníka	vlastník	k1gMnSc2
nebo	nebo	k8xC
i	i	k9
bez	bez	k7c2
návrhu	návrh	k1gInSc2
<g/>
“	“	k?
do	do	k7c2
30	#num#	k4
dnů	den	k1gInPc2
od	od	k7c2
zjištění	zjištění	k1gNnSc2
a	a	k8xC
opravu	oprava	k1gFnSc4
oznámí	oznámit	k5eAaPmIp3nS
vlastníkovi	vlastník	k1gMnSc3
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
nesouhlasu	nesouhlas	k1gInSc2
s	s	k7c7
opravou	oprava	k1gFnSc7
je	být	k5eAaImIp3nS
věc	věc	k1gFnSc1
předána	předán	k2eAgFnSc1d1
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
ministerstvu	ministerstvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Ing.	ing.	kA
arch	archa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandra	Alexandra	k1gFnSc1
Křížová	Křížová	k1gFnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
Mariana	Mariana	k1gFnSc1
Pisarčíková	Pisarčíková	k1gFnSc1
<g/>
:	:	kIx,
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
vedení	vedení	k1gNnSc4
ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
:	:	kIx,
Projekt	projekt	k1gInSc1
CARARE	CARARE	kA
a	a	k8xC
informační	informační	k2eAgInPc1d1
systémy	systém	k1gInPc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Europeana	Europeana	k1gFnSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
nedatovaná	datovaný	k2eNgFnSc1d1
prezentace	prezentace	k1gFnSc1
ppt	ppt	k?
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Uplatnění	uplatnění	k1gNnSc1
principu	princip	k1gInSc2
Smart	Smart	k1gInSc1
Administration	Administration	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
nakládání	nakládání	k1gNnSc2
s	s	k7c7
památkovým	památkový	k2eAgInSc7d1
fondem	fond	k1gInSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
Ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
jako	jako	k8xC,k8xS
zvláštní	zvláštní	k2eAgInSc1d1
datový	datový	k2eAgInSc1d1
systém	systém	k1gInSc1
nad	nad	k7c7
RUIAN	RUIAN	kA
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
další	další	k2eAgInPc4d1
základní	základní	k2eAgInPc4d1
registry	registr	k1gInPc4
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
web	web	k1gInSc1
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
ČR	ČR	kA
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
nepodepsán	podepsán	k2eNgMnSc1d1
a	a	k8xC
nedatován	datován	k2eNgMnSc1d1
<g/>
,	,	kIx,
v	v	k7c4
záhlaví	záhlaví	k1gNnSc4
uvedeno	uveden	k2eAgNnSc4d1
Ústřední	ústřední	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
NPÚ1	NPÚ1	k1gFnSc2
2	#num#	k4
Informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
a	a	k8xC
ICT	ICT	kA
v	v	k7c6
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
NPÚ1	NPÚ1	k1gFnSc1
2	#num#	k4
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
ČR	ČR	kA
<g/>
↑	↑	k?
MonumNet	MonumNet	k1gInSc1
<g/>
,	,	kIx,
Povolení	povolení	k1gNnSc4
k	k	k7c3
restaurování	restaurování	k1gNnSc3
<g/>
↑	↑	k?
Zadávací	zadávací	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
nadlimitní	nadlimitní	k2eAgNnSc4d1
veřejné	veřejný	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
zakázky	zakázka	k1gFnPc4
na	na	k7c4
služby	služba	k1gFnPc4
–	–	k?
Návrh	návrh	k1gInSc1
a	a	k8xC
implementace	implementace	k1gFnSc1
aplikace	aplikace	k1gFnSc2
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
NPÚ	NPÚ	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
↑	↑	k?
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
,	,	kIx,
Integrovaný	integrovaný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
projekt	projekt	k1gInSc1
reg.	reg.	k?
č.	č.	k?
CZ	CZ	kA
<g/>
.1	.1	k4
<g/>
.06	.06	k4
<g/>
/	/	kIx~
<g/>
1.1	1.1	k4
<g/>
.00	.00	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
17.094	17.094	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
web	web	k1gInSc1
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
↑	↑	k?
Digitalizace	digitalizace	k1gFnSc2
fondů	fond	k1gInPc2
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
Integrovaný	integrovaný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
projekt	projekt	k1gInSc1
reg.	reg.	k?
č.	č.	k?
CZ	CZ	kA
<g/>
.1	.1	k4
<g/>
.06	.06	k4
<g/>
/	/	kIx~
<g/>
1.1	1.1	k4
<g/>
.00	.00	k4
<g/>
/	/	kIx~
<g/>
17.093	17.093	k4
<g/>
97	#num#	k4
<g/>
,	,	kIx,
web	web	k1gInSc1
NPÚ	NPÚ	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
↑	↑	k?
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
zpřístupní	zpřístupnit	k5eAaPmIp3nS
historické	historický	k2eAgInPc4d1
fondy	fond	k1gInPc4
cestou	cestou	k7c2
elektronizace	elektronizace	k1gFnSc2
a	a	k8xC
digitalizace	digitalizace	k1gFnSc2
<g/>
,	,	kIx,
datovaná	datovaný	k2eAgFnSc1d1
pdf	pdf	k?
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
PR	pr	k0
článek	článek	k1gInSc1
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
tiskové	tiskový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
Odbor	odbor	k1gInSc1
strukturálních	strukturální	k2eAgInPc2d1
fondů	fond	k1gInPc2
MV	MV	kA
ČR	ČR	kA
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Příprava	příprava	k1gFnSc1
nového	nový	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
13	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
ČR	ČR	kA
<g/>
↑	↑	k?
Věcný	věcný	k2eAgInSc4d1
záměr	záměr	k1gInSc4
nového	nový	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
schválený	schválený	k2eAgInSc1d1
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
Usnesení	usnesení	k1gNnSc4
vlády	vláda	k1gFnSc2
ČR	ČR	kA
č.	č.	k?
156	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
návrhu	návrh	k1gInSc3
věcného	věcný	k2eAgNnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
záměru	záměr	k1gInSc2
zákona	zákon	k1gInSc2
o	o	k7c6
památkovém	památkový	k2eAgInSc6d1
fondu	fond	k1gInSc6
<g/>
↑	↑	k?
Pracovní	pracovní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
nového	nový	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
MK	MK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
o	o	k7c6
ochraně	ochrana	k1gFnSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
zákona	zákon	k1gInSc2
č.	č.	k?
634	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
správních	správní	k2eAgInPc6d1
poplatcích	poplatek	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
památkového	památkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ODok	ODok	k1gInSc1
<g/>
,	,	kIx,
Vláda	vláda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
2015	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
MonumNet	MonumNet	k1gInSc4
–	–	k?
Nemovité	movitý	k2eNgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc2d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
</s>
<s>
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
</s>
