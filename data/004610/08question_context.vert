<s>
Robert	Robert	k1gMnSc1	Robert
John	John	k1gMnSc1	John
Downey	Downe	k2eAgFnPc4d1	Downe
Jr	Jr	k1gFnPc4	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
rolím	role	k1gFnPc3	role
patří	patřit	k5eAaImIp3nS	patřit
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
Charlieho	Charlie	k1gMnSc2	Charlie
Chaplina	Chaplin	k2eAgMnSc2d1	Chaplin
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Chaplin	Chaplina	k1gFnPc2	Chaplina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
jako	jako	k9	jako
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
v	v	k7c6	v
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
filmech	film	k1gInPc6	film
série	série	k1gFnSc1	série
Marvel	Marvel	k1gInSc1	Marvel
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
od	od	k7c2	od
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
