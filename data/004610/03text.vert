<s>
Robert	Robert	k1gMnSc1	Robert
John	John	k1gMnSc1	John
Downey	Downe	k2eAgFnPc4d1	Downe
Jr	Jr	k1gFnPc4	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
rolím	role	k1gFnPc3	role
patří	patřit	k5eAaImIp3nS	patřit
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
Charlieho	Charlie	k1gMnSc2	Charlie
Chaplina	Chaplin	k2eAgMnSc2d1	Chaplin
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Chaplin	Chaplina	k1gFnPc2	Chaplina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
jako	jako	k9	jako
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
v	v	k7c6	v
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
filmech	film	k1gInPc6	film
série	série	k1gFnSc1	série
Marvel	Marvel	k1gInSc1	Marvel
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
od	od	k7c2	od
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
dalším	další	k2eAgInPc3d1	další
význačným	význačný	k2eAgInPc3d1	význačný
snímkům	snímek	k1gInPc3	snímek
patří	patřit	k5eAaImIp3nS	patřit
Skvělí	skvělý	k2eAgMnPc1d1	skvělý
chlapi	chlap	k1gMnPc1	chlap
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gothika	Gothika	k1gFnSc1	Gothika
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kiss	Kiss	k1gInSc4	Kiss
Kiss	Kissa	k1gFnPc2	Kissa
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zodiac	Zodiac	k1gFnSc1	Zodiac
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tropická	tropický	k2eAgFnSc1d1	tropická
bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
)	)	kIx)	)
a	a	k8xC	a
Soudce	soudce	k1gMnSc1	soudce
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
obdržel	obdržet	k5eAaPmAgInS	obdržet
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
herecké	herecký	k2eAgInPc4d1	herecký
výkony	výkon	k1gInPc4	výkon
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ally	Alla	k1gFnSc2	Alla
McBealová	McBealová	k1gFnSc1	McBealová
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
herečce	herečka	k1gFnSc3	herečka
Elsie	Elsie	k1gFnSc2	Elsie
a	a	k8xC	a
nezávislému	závislý	k2eNgMnSc3d1	nezávislý
filmaři	filmař	k1gMnSc3	filmař
Robertu	Roberta	k1gFnSc4	Roberta
Downeyovým	Downeyová	k1gFnPc3	Downeyová
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
starší	starý	k2eAgFnSc4d2	starší
sestru	sestra	k1gFnSc4	sestra
Allyson	Allysona	k1gFnPc2	Allysona
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
otcově	otcův	k2eAgFnSc3d1	otcova
práci	práce	k1gFnSc3	práce
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
rodičů	rodič	k1gMnPc2	rodič
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Robert	Robert	k1gMnSc1	Robert
mladší	mladý	k2eAgMnSc1d2	mladší
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čas	čas	k1gInSc4	čas
odjel	odjet	k5eAaPmAgInS	odjet
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
klasický	klasický	k2eAgInSc4d1	klasický
balet	balet	k1gInSc4	balet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gMnSc4	on
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
museli	muset	k5eAaImAgMnP	muset
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
vyhodit	vyhodit	k5eAaPmF	vyhodit
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Monice	Monika	k1gFnSc6	Monika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
vydržel	vydržet	k5eAaPmAgMnS	vydržet
pouhý	pouhý	k2eAgInSc4d1	pouhý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
opět	opět	k6eAd1	opět
vyloučili	vyloučit	k5eAaPmAgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
či	či	k8xC	či
prodáváním	prodávání	k1gNnSc7	prodávání
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Sarou	Sarý	k2eAgFnSc7d1	Sarý
Jessicou	Jessica	k1gFnSc7	Jessica
Parker	Parkra	k1gFnPc2	Parkra
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
žil	žít	k5eAaImAgMnS	žít
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
známosti	známost	k1gFnSc2	známost
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
a	a	k8xC	a
modelkou	modelka	k1gFnSc7	modelka
Deborah	Deboraha	k1gFnPc2	Deboraha
Falconer	Falconra	k1gFnPc2	Falconra
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Indio	Indio	k1gMnSc1	Indio
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
Downeyho	Downeyha	k1gFnSc5	Downeyha
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
ale	ale	k8xC	ale
rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
vůbec	vůbec	k9	vůbec
nefungoval	fungovat	k5eNaImAgInS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
definitivně	definitivně	k6eAd1	definitivně
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Downey	Downe	k1gMnPc4	Downe
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
léčit	léčit	k5eAaImF	léčit
svoje	svůj	k3xOyFgFnPc4	svůj
závislosti	závislost	k1gFnPc4	závislost
na	na	k7c6	na
klinikách	klinika	k1gFnPc6	klinika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
snímku	snímek	k1gInSc2	snímek
Gothika	Gothik	k1gMnSc2	Gothik
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
producentkou	producentka	k1gFnSc7	producentka
Susan	Susana	k1gFnPc2	Susana
Levin	Levina	k1gFnPc2	Levina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
překonal	překonat	k5eAaPmAgMnS	překonat
svoje	svůj	k3xOyFgFnPc4	svůj
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
herecké	herecký	k2eAgFnSc6d1	herecká
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Susan	Susany	k1gInPc2	Susany
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Extona	Exton	k1gMnSc4	Exton
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Avri	Avr	k1gFnSc2	Avr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
jeho	jeho	k3xOp3gMnSc4	jeho
otce	otec	k1gMnSc4	otec
Pound	pound	k1gInSc4	pound
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
výrazných	výrazný	k2eAgFnPc2d1	výrazná
filmových	filmový	k2eAgFnPc2d1	filmová
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
herecká	herecký	k2eAgFnSc1d1	herecká
šance	šance	k1gFnSc1	šance
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
s	s	k7c7	s
filmem	film	k1gInSc7	film
Chaplin	Chaplina	k1gFnPc2	Chaplina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
přímo	přímo	k6eAd1	přímo
Charlieho	Charlie	k1gMnSc4	Charlie
Chaplina	Chaplin	k2eAgMnSc4d1	Chaplin
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgInS	vysloužit
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
roky	rok	k1gInPc4	rok
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
spíše	spíše	k9	spíše
v	v	k7c6	v
béčkových	béčkový	k2eAgInPc6d1	béčkový
filmech	film	k1gInPc6	film
a	a	k8xC	a
rodinných	rodinný	k2eAgFnPc6d1	rodinná
komediích	komedie	k1gFnPc6	komedie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
často	často	k6eAd1	často
byly	být	k5eAaImAgInP	být
zastíněny	zastínit	k5eAaPmNgInP	zastínit
jeho	jeho	k3xOp3gInPc1	jeho
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
právníka	právník	k1gMnSc2	právník
Larryho	Larry	k1gMnSc2	Larry
Paula	Paul	k1gMnSc2	Paul
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ally	Alla	k1gFnSc2	Alla
McBealová	McBealová	k1gFnSc1	McBealová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
natáčením	natáčení	k1gNnSc7	natáčení
filmu	film	k1gInSc2	film
Gothika	Gothika	k1gFnSc1	Gothika
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
superhrdinském	superhrdinský	k2eAgInSc6d1	superhrdinský
snímku	snímek	k1gInSc6	snímek
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
titulní	titulní	k2eAgFnSc4d1	titulní
postavu	postava	k1gFnSc4	postava
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
alias	alias	k9	alias
Iron	iron	k1gInSc1	iron
Mana	Man	k1gMnSc2	Man
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
filmech	film	k1gInPc6	film
Neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
Hulk	Hulk	k1gInSc1	Hulk
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
2	[number]	k4	2
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
<g/>
:	:	kIx,	:
Age	Age	k1gMnSc1	Age
of	of	k?	of
Ultron	Ultron	k1gMnSc1	Ultron
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
<g/>
:	:	kIx,	:
Homecoming	Homecoming	k1gInSc1	Homecoming
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Tropická	tropický	k2eAgFnSc1d1	tropická
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
i	i	k8xC	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
a	a	k8xC	a
2011	[number]	k4	2011
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
ve	v	k7c6	v
snímcích	snímek	k1gInPc6	snímek
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
a	a	k8xC	a
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
<g/>
:	:	kIx,	:
Hra	hra	k1gFnSc1	hra
stínů	stín	k1gInPc2	stín
titulní	titulní	k2eAgFnSc4d1	titulní
postavu	postava	k1gFnSc4	postava
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
filmu	film	k1gInSc6	film
získal	získat	k5eAaPmAgMnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
nebo	nebo	k8xC	nebo
komedii	komedie	k1gFnSc3	komedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
produkoval	produkovat	k5eAaImAgMnS	produkovat
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
Downeyho	Downey	k1gMnSc2	Downey
nejčastěji	často	k6eAd3	často
dabují	dabovat	k5eAaBmIp3nP	dabovat
Jan	Jan	k1gMnSc1	Jan
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dvořák	Dvořák	k1gMnSc1	Dvořák
a	a	k8xC	a
Radovan	Radovan	k1gMnSc1	Radovan
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Smile	smil	k1gInSc5	smil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Chaplin	Chaplina	k1gFnPc2	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
soundtracích	soundtrace	k1gFnPc6	soundtrace
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
Ally	Alla	k1gFnSc2	Alla
McBealová	McBealová	k1gFnSc1	McBealová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vánoční	vánoční	k2eAgFnSc6d1	vánoční
desce	deska	k1gFnSc6	deska
Ally	Alla	k1gFnSc2	Alla
McBeal	McBeal	k1gInSc1	McBeal
<g/>
:	:	kIx,	:
A	a	k9	a
Very	Ver	k2eAgMnPc4d1	Ver
Ally	All	k1gMnPc4	All
Christmas	Christmasa	k1gFnPc2	Christmasa
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Christmas	Christmas	k1gInSc4	Christmas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Vondou	Vonda	k1gFnSc7	Vonda
Shepard	Sheparda	k1gFnPc2	Sheparda
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
River	River	k1gInSc1	River
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
albu	album	k1gNnSc6	album
Ally	Alla	k1gFnSc2	Alla
McBeal	McBeal	k1gInSc1	McBeal
<g/>
:	:	kIx,	:
For	forum	k1gNnPc2	forum
Once	Once	k1gNnSc2	Once
in	in	k?	in
My	my	k3xPp1nPc1	my
Life	Life	k1gNnSc1	Life
Featuring	Featuring	k1gInSc1	Featuring
Vonda	Vonda	k1gMnSc1	Vonda
Shepard	Shepard	k1gMnSc1	Shepard
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
nazpíval	nazpívat	k5eAaPmAgInS	nazpívat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Snakes	Snakes	k1gInSc4	Snakes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Vondou	Vonda	k1gFnSc7	Vonda
Shepard	Shepard	k1gMnSc1	Shepard
"	"	kIx"	"
<g/>
Chances	Chances	k1gMnSc1	Chances
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
a	a	k8xC	a
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Every	Ever	k1gInPc4	Ever
Breath	Breath	k1gInSc1	Breath
You	You	k1gMnPc2	You
Take	Tak	k1gFnSc2	Tak
<g/>
"	"	kIx"	"
se	s	k7c7	s
Stingem	Sting	k1gInSc7	Sting
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Futurist	Futurist	k1gMnSc1	Futurist
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
na	na	k7c4	na
saxofon	saxofon	k1gInSc4	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Broken	Broken	k1gInSc1	Broken
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
alba	album	k1gNnSc2	album
The	The	k1gMnSc1	The
Futurist	Futurist	k1gMnSc1	Futurist
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Kiss	Kissa	k1gFnPc2	Kissa
Kiss	Kiss	k1gInSc1	Kiss
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Susan	Susan	k1gInSc4	Susan
vlastní	vlastní	k2eAgInSc4d1	vlastní
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
Team	team	k1gInSc4	team
Downey	Downea	k1gFnSc2	Downea
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
prvním	první	k4xOgMnSc7	první
projektem	projekt	k1gInSc7	projekt
byl	být	k5eAaImAgMnS	být
film	film	k1gInSc4	film
Soudce	soudce	k1gMnSc1	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Sarah	Sarah	k1gFnSc7	Sarah
Jessicou	Jessica	k1gFnSc7	Jessica
Parker	Parker	k1gMnSc1	Parker
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozešla	rozejít	k5eAaPmAgFnS	rozejít
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInPc3	jeho
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
herečku	herečka	k1gFnSc4	herečka
a	a	k8xC	a
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Deborah	Deborah	k1gMnSc1	Deborah
Falconer	Falconer	k1gMnSc1	Falconer
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Indio	Indio	k1gMnSc1	Indio
Falconer	Falconer	k1gMnSc1	Falconer
Downey	Downea	k1gFnSc2	Downea
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
září	září	k1gNnSc6	září
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnPc3	jeho
neustálým	neustálý	k2eAgFnPc3d1	neustálá
návštěvám	návštěva	k1gFnPc3	návštěva
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
návštěvám	návštěva	k1gFnPc3	návštěva
v	v	k7c6	v
odvykacích	odvykací	k2eAgFnPc6d1	odvykací
center	centrum	k1gNnPc2	centrum
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Deborah	Deboraha	k1gFnPc2	Deboraha
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vzít	vzít	k5eAaPmF	vzít
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
odejít	odejít	k5eAaPmF	odejít
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
s	s	k7c7	s
producentkou	producentka	k1gFnSc7	producentka
Susan	Susan	k1gInSc4	Susan
Levin	Levin	k2eAgInSc4d1	Levin
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
Gothika	Gothikum	k1gNnSc2	Gothikum
<g/>
.	.	kIx.	.
</s>
<s>
Downey	Downea	k1gFnPc1	Downea
ji	on	k3xPp3gFnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
večer	večer	k6eAd1	večer
před	před	k7c7	před
jejími	její	k3xOp3gFnPc7	její
třicátými	třicátý	k4xOgFnPc7	třicátý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
v	v	k7c6	v
Amagansettu	Amagansett	k1gInSc6	Amagansett
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Roberta	k1gFnPc2	Roberta
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
