<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
komiksovém	komiksový	k2eAgInSc6d1	komiksový
filmu	film	k1gInSc6	film
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
obsadil	obsadit	k5eAaPmAgMnS	obsadit
právě	právě	k9	právě
roli	role	k1gFnSc4	role
Iron	iron	k1gInSc4	iron
Mana	Man	k1gMnSc2	Man
<g/>
?	?	kIx.	?
</s>
