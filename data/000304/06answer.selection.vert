<s>
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
North	North	k1gMnSc1	North
Sea	Sea	k1gMnSc1	Sea
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Mer	Mer	k1gMnSc1	Mer
du	du	k?	du
Nord	Nord	k1gMnSc1	Nord
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Nordsee	Nordsee	k1gFnSc1	Nordsee
<g/>
,	,	kIx,	,
frísky	frísky	k6eAd1	frísky
Noardsee	Noardsee	k1gFnSc1	Noardsee
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Noordzee	Noordzee	k1gFnSc1	Noordzee
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
Nordsø	Nordsø	k1gFnSc1	Nordsø
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
Nordsjø	Nordsjø	k1gFnSc1	Nordsjø
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okrajové	okrajový	k2eAgNnSc1d1	okrajové
<g/>
,	,	kIx,	,
šelfové	šelfový	k2eAgNnSc1d1	šelfové
moře	moře	k1gNnSc1	moře
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
