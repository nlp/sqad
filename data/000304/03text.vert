<s>
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
North	North	k1gMnSc1	North
Sea	Sea	k1gMnSc1	Sea
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Mer	Mer	k1gMnSc1	Mer
du	du	k?	du
Nord	Nord	k1gMnSc1	Nord
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Nordsee	Nordsee	k1gFnSc1	Nordsee
<g/>
,	,	kIx,	,
frísky	frísky	k6eAd1	frísky
Noardsee	Noardsee	k1gFnSc1	Noardsee
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Noordzee	Noordzee	k1gFnSc1	Noordzee
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
Nordsø	Nordsø	k1gFnSc1	Nordsø
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
Nordsjø	Nordsjø	k1gFnSc1	Nordsjø
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okrajové	okrajový	k2eAgNnSc1d1	okrajové
<g/>
,	,	kIx,	,
šelfové	šelfový	k2eAgNnSc1d1	šelfové
moře	moře	k1gNnSc1	moře
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
Britskými	britský	k2eAgInPc7d1	britský
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
Orknejemi	Orkneje	k1gFnPc7	Orkneje
a	a	k8xC	a
Shetlandami	Shetlanda	k1gFnPc7	Shetlanda
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Skandinávským	skandinávský	k2eAgMnSc7d1	skandinávský
a	a	k8xC	a
Jutským	jutský	k2eAgInSc7d1	jutský
poloostrovem	poloostrov	k1gInSc7	poloostrov
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pobřežím	pobřeží	k1gNnSc7	pobřeží
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Omývá	omývat	k5eAaImIp3nS	omývat
pobřeží	pobřeží	k1gNnSc4	pobřeží
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
570	[number]	k4	570
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hloubky	hloubka	k1gFnSc2	hloubka
96	[number]	k4	96
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
809	[number]	k4	809
m	m	kA	m
ve	v	k7c6	v
Skagerraku	Skagerrak	k1gInSc6	Skagerrak
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
42	[number]	k4	42
000	[number]	k4	000
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc6	novověk
staly	stát	k5eAaPmAgFnP	stát
nejvyspělejší	vyspělý	k2eAgFnSc7d3	nejvyspělejší
částí	část	k1gFnSc7	část
světa	svět	k1gInSc2	svět
a	a	k8xC	a
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
největší	veliký	k2eAgInSc1d3	veliký
evropský	evropský	k2eAgInSc1d1	evropský
přístav	přístav	k1gInSc1	přístav
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
světově	světově	k6eAd1	světově
významných	významný	k2eAgInPc2d1	významný
přístavů	přístav	k1gInPc2	přístav
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
nová	nový	k2eAgNnPc1d1	nové
ložiska	ložisko	k1gNnPc1	ložisko
stále	stále	k6eAd1	stále
přibývají	přibývat	k5eAaImIp3nP	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
Norské	norský	k2eAgNnSc4d1	norské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
přes	přes	k7c4	přes
Dánské	dánský	k2eAgFnPc4d1	dánská
úžiny	úžina	k1gFnPc4	úžina
(	(	kIx(	(
<g/>
Skagerrak	Skagerrak	k1gInSc1	Skagerrak
<g/>
,	,	kIx,	,
Kattegat	Kattegat	k1gInSc1	Kattegat
<g/>
,	,	kIx,	,
Öresund	Öresund	k1gInSc1	Öresund
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
Belt	Belt	k1gInSc1	Belt
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Belt	Belt	k1gInSc1	Belt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
přes	přes	k7c4	přes
Calaiskou	Calaiský	k2eAgFnSc4d1	Calaiský
úžinu	úžina	k1gFnSc4	úžina
a	a	k8xC	a
průliv	průliv	k1gInSc4	průliv
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
přes	přes	k7c4	přes
průlivy	průliv	k1gInPc4	průliv
mezi	mezi	k7c7	mezi
ostrovy	ostrov	k1gInPc7	ostrov
(	(	kIx(	(
<g/>
Pentland	Pentland	k1gInSc1	Pentland
Firth	Firth	k1gInSc1	Firth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
fjordy	fjord	k1gInPc4	fjord
(	(	kIx(	(
<g/>
Boknafjorden	Boknafjordna	k1gFnPc2	Boknafjordna
<g/>
,	,	kIx,	,
Hardangerfjorden	Hardangerfjordna	k1gFnPc2	Hardangerfjordna
<g/>
,	,	kIx,	,
Bjø	Bjø	k1gFnSc1	Bjø
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Limfjorden	Limfjordna	k1gFnPc2	Limfjordna
a	a	k8xC	a
Ringkø	Ringkø	k1gMnPc2	Ringkø
Fjord	fjord	k1gInSc1	fjord
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
)	)	kIx)	)
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Bø	Bø	k1gMnSc1	Bø
<g/>
,	,	kIx,	,
Stord	Stord	k1gMnSc1	Stord
<g/>
,	,	kIx,	,
Huftarø	Huftarø	k1gMnSc1	Huftarø
<g/>
,	,	kIx,	,
Tysnesø	Tysnesø	k1gMnSc1	Tysnesø
<g/>
,	,	kIx,	,
Store	Stor	k1gInSc5	Stor
Sotra	Sotra	k1gFnSc1	Sotra
<g/>
,	,	kIx,	,
Askø	Askø	k1gFnSc1	Askø
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Mors	Mors	k1gInSc4	Mors
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc1	pobřeží
převážně	převážně	k6eAd1	převážně
rovné	rovný	k2eAgFnSc2d1	rovná
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnSc2d1	nízká
a	a	k8xC	a
písčité	písčitý	k2eAgFnSc2d1	písčitá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
laguny	laguna	k1gFnPc1	laguna
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Bucht	Bucht	k1gMnSc1	Bucht
<g/>
,	,	kIx,	,
Helgoländer	Helgoländer	k1gMnSc1	Helgoländer
Bucht	Bucht	k1gMnSc1	Bucht
<g/>
,	,	kIx,	,
Waddenzee	Waddenzee	k1gFnSc1	Waddenzee
<g/>
)	)	kIx)	)
a	a	k8xC	a
písečné	písečný	k2eAgFnPc4d1	písečná
mělčiny	mělčina	k1gFnPc4	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
mělké	mělký	k2eAgInPc1d1	mělký
zálivy	záliv	k1gInPc1	záliv
(	(	kIx(	(
<g/>
Moray	Moray	k1gInPc1	Moray
Firth	Firtha	k1gFnPc2	Firtha
<g/>
,	,	kIx,	,
Firth	Firtha	k1gFnPc2	Firtha
of	of	k?	of
Forth	Forth	k1gMnSc1	Forth
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Wash	Wash	k1gMnSc1	Wash
<g/>
)	)	kIx)	)
a	a	k8xC	a
estuáry	estuár	k1gInPc1	estuár
(	(	kIx(	(
<g/>
Temže	Temže	k1gFnSc1	Temže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
je	být	k5eAaImIp3nS	být
sužováno	sužovat	k5eAaImNgNnS	sužovat
povodněmi	povodeň	k1gFnPc7	povodeň
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
obklopeno	obklopen	k2eAgNnSc4d1	obklopeno
hrázemi	hráz	k1gFnPc7	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odkrytém	odkrytý	k2eAgNnSc6d1	odkryté
moři	moře	k1gNnSc6	moře
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
jen	jen	k9	jen
podél	podél	k7c2	podél
východního	východní	k2eAgInSc2d1	východní
břehu	břeh	k1gInSc2	břeh
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
řetěz	řetěz	k1gInSc1	řetěz
Fríských	fríský	k2eAgInPc2d1	fríský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
leží	ležet	k5eAaImIp3nS	ležet
ostrov	ostrov	k1gInSc1	ostrov
Helgoland	Helgoland	k1gInSc1	Helgoland
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
dna	dno	k1gNnSc2	dno
představuje	představovat	k5eAaImIp3nS	představovat
rovinu	rovina	k1gFnSc4	rovina
mírně	mírně	k6eAd1	mírně
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
k	k	k7c3	k
severu	sever	k1gInSc3	sever
s	s	k7c7	s
hloubkami	hloubka	k1gFnPc7	hloubka
od	od	k7c2	od
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
m	m	kA	m
do	do	k7c2	do
150	[number]	k4	150
až	až	k9	až
170	[number]	k4	170
m.	m.	k?	m.
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
nevelkých	velký	k2eNgFnPc2d1	nevelká
mělčin	mělčina	k1gFnPc2	mělčina
tzv.	tzv.	kA	tzv.
lavic	lavice	k1gFnPc2	lavice
(	(	kIx(	(
<g/>
Doggerská	Doggerský	k2eAgFnSc1d1	Doggerský
lavice	lavice	k1gFnSc1	lavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgInPc1d1	složený
odplavenými	odplavený	k2eAgInPc7d1	odplavený
produkty	produkt	k1gInPc7	produkt
morénových	morénový	k2eAgFnPc2d1	morénová
usazenin	usazenina	k1gFnPc2	usazenina
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
pleistocéně	pleistocén	k1gInSc6	pleistocén
pokrýval	pokrývat	k5eAaImAgMnS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nevelké	velký	k2eNgInPc1d1	nevelký
písčito-štěrkové	písčito-štěrkový	k2eAgInPc1d1	písčito-štěrkový
hřebeny	hřeben	k1gInPc1	hřeben
protáhnuté	protáhnutý	k2eAgInPc1d1	protáhnutý
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
přílivovými	přílivový	k2eAgInPc7d1	přílivový
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
úzké	úzký	k2eAgFnPc1d1	úzká
úžlabiny	úžlabina	k1gFnPc1	úžlabina
hluboké	hluboký	k2eAgFnPc1d1	hluboká
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
m	m	kA	m
(	(	kIx(	(
<g/>
Devil	Devil	k1gMnSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hole	hole	k6eAd1	hole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
říčních	říční	k2eAgFnPc2d1	říční
dolin	dolina	k1gFnPc2	dolina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
byl	být	k5eAaImAgInS	být
jižní	jižní	k2eAgInSc1d1	jižní
úsek	úsek	k1gInSc1	úsek
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
bažinatou	bažinatý	k2eAgFnSc7d1	bažinatá
planinou	planina	k1gFnSc7	planina
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
táhli	táhnout	k5eAaImAgMnP	táhnout
losi	los	k1gMnPc1	los
a	a	k8xC	a
jeleni	jelen	k1gMnPc1	jelen
a	a	k8xC	a
žili	žít	k5eAaImAgMnP	žít
tu	tu	k6eAd1	tu
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
6000	[number]	k4	6000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
pevniny	pevnina	k1gFnSc2	pevnina
zdvihem	zdvih	k1gInSc7	zdvih
hladiny	hladina	k1gFnSc2	hladina
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
oteplením	oteplení	k1gNnSc7	oteplení
po	po	k7c6	po
konci	konec	k1gInSc6	konec
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
žlab	žlab	k1gInSc1	žlab
(	(	kIx(	(
<g/>
Norský	norský	k2eAgInSc1d1	norský
příkop	příkop	k1gInSc1	příkop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
podél	podél	k7c2	podél
linie	linie	k1gFnSc2	linie
zlomu	zlom	k1gInSc2	zlom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
300	[number]	k4	300
až	až	k8xS	až
400	[number]	k4	400
m	m	kA	m
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Skagerraku	Skagerrak	k1gInSc6	Skagerrak
až	až	k9	až
800	[number]	k4	800
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
tlustými	tlustý	k2eAgFnPc7d1	tlustá
(	(	kIx(	(
<g/>
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
km	km	kA	km
<g/>
)	)	kIx)	)
vrstvami	vrstva	k1gFnPc7	vrstva
usazenin	usazenina	k1gFnPc2	usazenina
od	od	k7c2	od
permských	permský	k2eAgFnPc2d1	Permská
(	(	kIx(	(
<g/>
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
starších	starý	k2eAgNnPc2d2	starší
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
antropogenní	antropogenní	k2eAgFnPc4d1	antropogenní
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ledovcových	ledovcový	k2eAgFnPc2d1	ledovcová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
systém	systém	k1gInSc4	systém
plochých	plochý	k2eAgFnPc2d1	plochá
vyvýšenin	vyvýšenina	k1gFnPc2	vyvýšenina
a	a	k8xC	a
propadlin	propadlina	k1gFnPc2	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
jsou	být	k5eAaImIp3nP	být
Jutská	jutský	k2eAgFnSc1d1	jutský
a	a	k8xC	a
Doggerská	Doggerský	k2eAgFnSc1d1	Doggerský
lavice	lavice	k1gFnSc1	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
propadliny	propadlina	k1gFnPc1	propadlina
jsou	být	k5eAaImIp3nP	být
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
Severodánská	Severodánský	k2eAgFnSc1d1	Severodánský
a	a	k8xC	a
Anglická	anglický	k2eAgFnSc1d1	anglická
<g/>
.	.	kIx.	.
</s>
<s>
Vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
svahy	svah	k1gInPc1	svah
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
protáhlými	protáhlý	k2eAgInPc7d1	protáhlý
a	a	k8xC	a
kruhovými	kruhový	k2eAgInPc7d1	kruhový
ohyby	ohyb	k1gInPc7	ohyb
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgFnPc1d1	spojena
naleziště	naleziště	k1gNnSc4	naleziště
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
(	(	kIx(	(
<g/>
Ekofisk	Ekofisk	k1gInSc1	Ekofisk
v	v	k7c6	v
Norském	norský	k2eAgInSc6d1	norský
sektoru	sektor	k1gInSc6	sektor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Permské	permský	k2eAgFnPc1d1	Permská
usazeniny	usazenina	k1gFnPc1	usazenina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgFnPc1d1	velká
zásoby	zásoba	k1gFnPc1	zásoba
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mnohé	mnohý	k2eAgFnPc4d1	mnohá
kopule	kopule	k1gFnPc4	kopule
a	a	k8xC	a
antiklinály	antiklinála	k1gFnPc4	antiklinála
pronikající	pronikající	k2eAgFnPc4d1	pronikající
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
protáhnuté	protáhnutý	k2eAgFnSc2d1	protáhnutá
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Usazeniny	usazenina	k1gFnPc1	usazenina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
na	na	k7c6	na
východě	východ	k1gInSc6	východ
oblast	oblast	k1gFnSc4	oblast
předkambrijské	předkambrijský	k2eAgFnSc2d1	předkambrijský
východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
kaledonské	kaledonský	k2eAgNnSc1d1	kaledonské
Norsko	Norsko	k1gNnSc1	Norsko
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
oblast	oblast	k1gFnSc4	oblast
bajkalského	bajkalský	k2eAgNnSc2d1	Bajkalské
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
kráter	kráter	k1gInSc4	kráter
Silverpit	Silverpit	k1gFnSc2	Silverpit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
hypotézy	hypotéza	k1gFnSc2	hypotéza
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c7	před
65	[number]	k4	65
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dopadu	dopad	k1gInSc2	dopad
úlomku	úlomek	k1gInSc2	úlomek
té	ten	k3xDgFnSc2	ten
samé	samý	k3xTgFnSc2	samý
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
kráter	kráter	k1gInSc4	kráter
Chicxulub	Chicxuluba	k1gFnPc2	Chicxuluba
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofa	katastrofa	k1gFnSc1	katastrofa
způsobila	způsobit	k5eAaPmAgFnS	způsobit
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
a	a	k8xC	a
vzniklá	vzniklý	k2eAgNnPc4d1	vzniklé
tsunami	tsunami	k1gNnPc4	tsunami
způsobila	způsobit	k5eAaPmAgFnS	způsobit
obrovské	obrovský	k2eAgFnPc4d1	obrovská
povodně	povodeň	k1gFnPc4	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
neustálým	neustálý	k2eAgInSc7d1	neustálý
vlivem	vliv	k1gInSc7	vliv
teplého	teplý	k2eAgInSc2d1	teplý
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
tlakové	tlakový	k2eAgFnSc2d1	tlaková
níže	níže	k1gFnSc2	níže
nad	nad	k7c7	nad
Islandem	Island	k1gInSc7	Island
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
přiměřeně	přiměřeně	k6eAd1	přiměřeně
mírná	mírný	k2eAgFnSc1d1	mírná
a	a	k8xC	a
léto	léto	k1gNnSc1	léto
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
převládají	převládat	k5eAaImIp3nP	převládat
západní	západní	k2eAgInPc1d1	západní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgInPc1d1	jihozápadní
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
největší	veliký	k2eAgFnPc4d3	veliký
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
četnost	četnost	k1gFnSc4	četnost
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
změna	změna	k1gFnSc1	změna
cyklón	cyklóna	k1gFnPc2	cyklóna
a	a	k8xC	a
anticyklón	anticyklóna	k1gFnPc2	anticyklóna
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
nestálost	nestálost	k1gFnSc4	nestálost
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
od	od	k7c2	od
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
východě	východ	k1gInSc6	východ
do	do	k7c2	do
-5	-5	k4	-5
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
od	od	k7c2	od
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severu	sever	k1gInSc6	sever
do	do	k7c2	do
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
při	při	k7c6	při
severním	severní	k2eAgInSc6d1	severní
větru	vítr	k1gInSc6	vítr
může	moct	k5eAaImIp3nS	moct
teplota	teplota	k1gFnSc1	teplota
klesnou	klesnout	k5eAaPmIp3nP	klesnout
až	až	k9	až
na	na	k7c4	na
-23	-23	k4	-23
°	°	k?	°
<g/>
C.	C.	kA	C.
Oblačnost	oblačnost	k1gFnSc4	oblačnost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
stupňů	stupeň	k1gInPc2	stupeň
s	s	k7c7	s
maximem	maximum	k1gNnSc7	maximum
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
mlhy	mlha	k1gFnPc1	mlha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
četnost	četnost	k1gFnSc1	četnost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
stoupá	stoupat	k5eAaImIp3nS	stoupat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
sezóny	sezóna	k1gFnSc2	sezóna
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
(	(	kIx(	(
<g/>
600	[number]	k4	600
až	až	k9	až
700	[number]	k4	700
mm	mm	kA	mm
<g/>
)	)	kIx)	)
k	k	k7c3	k
severu	sever	k1gInSc3	sever
(	(	kIx(	(
<g/>
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
časté	častý	k2eAgNnSc1d1	časté
sněžení	sněžení	k1gNnSc1	sněžení
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
silným	silný	k2eAgInSc7d1	silný
nárazovým	nárazový	k2eAgInSc7d1	nárazový
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
studie	studie	k1gFnSc1	studie
spolkového	spolkový	k2eAgInSc2d1	spolkový
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
mořskou	mořský	k2eAgFnSc4d1	mořská
plavbu	plavba	k1gFnSc4	plavba
a	a	k8xC	a
hydrografii	hydrografie	k1gFnSc4	hydrografie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
1	[number]	k4	1
stupeň	stupeň	k1gInSc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
11	[number]	k4	11
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
studenými	studený	k2eAgFnPc7d1	studená
pevninskými	pevninský	k2eAgFnPc7d1	pevninská
vodami	voda	k1gFnPc7	voda
na	na	k7c4	na
2	[number]	k4	2
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
12	[number]	k4	12
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
16	[number]	k4	16
až	až	k9	až
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
a	a	k8xC	a
18	[number]	k4	18
až	až	k9	až
19	[number]	k4	19
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Slanost	slanost	k1gFnSc1	slanost
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
34,7	[number]	k4	34,7
až	až	k9	až
35,3	[number]	k4	35,3
‰	‰	k?	‰
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
34,5	[number]	k4	34,5
až	až	k9	až
34,7	[number]	k4	34,7
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
31	[number]	k4	31
až	až	k9	až
32	[number]	k4	32
‰	‰	k?	‰
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
na	na	k7c4	na
29	[number]	k4	29
až	až	k9	až
31	[number]	k4	31
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Hloubkové	hloubkový	k2eAgFnPc1d1	hloubková
teploty	teplota	k1gFnPc1	teplota
a	a	k8xC	a
slanost	slanost	k1gFnSc1	slanost
se	se	k3xPyFc4	se
jen	jen	k9	jen
málo	málo	k6eAd1	málo
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
povrchové	povrchový	k2eAgFnSc2d1	povrchová
<g/>
.	.	kIx.	.
</s>
<s>
Přílivy	příliv	k1gInPc1	příliv
jsou	být	k5eAaImIp3nP	být
dvanáctihodinové	dvanáctihodinový	k2eAgFnPc1d1	dvanáctihodinová
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
od	od	k7c2	od
0,6	[number]	k4	0,6
do	do	k7c2	do
7,6	[number]	k4	7,6
m.	m.	k?	m.
Větrný	větrný	k2eAgInSc4d1	větrný
příboj	příboj	k1gInSc4	příboj
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
3	[number]	k4	3
m	m	kA	m
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
při	pře	k1gFnSc4	pře
poklesy	pokles	k1gInPc1	pokles
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
2,1	[number]	k4	2,1
m.	m.	k?	m.
Při	při	k7c6	při
spojení	spojení	k1gNnSc6	spojení
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
příboje	příboj	k1gInSc2	příboj
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
katastrofálním	katastrofální	k2eAgFnPc3d1	katastrofální
záplavám	záplava	k1gFnPc3	záplava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zatopena	zatopen	k2eAgFnSc1d1	zatopena
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
nízkých	nízký	k2eAgNnPc2d1	nízké
pobřeží	pobřeží	k1gNnPc2	pobřeží
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Větrem	vítr	k1gInSc7	vítr
vytvářené	vytvářený	k2eAgFnSc2d1	vytvářená
vlny	vlna	k1gFnSc2	vlna
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
zimách	zima	k1gFnPc6	zima
může	moct	k5eAaImIp3nS	moct
led	led	k1gInSc1	led
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
měsíc	měsíc	k1gInSc4	měsíc
pokrýt	pokrýt	k5eAaPmF	pokrýt
i	i	k9	i
větší	veliký	k2eAgFnPc4d2	veliký
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
odpady	odpad	k1gInPc7	odpad
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
ropných	ropný	k2eAgInPc2d1	ropný
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
ropou	ropa	k1gFnSc7	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Hydrologické	hydrologický	k2eAgFnPc1d1	hydrologická
podmínky	podmínka	k1gFnPc1	podmínka
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
geologické	geologický	k2eAgFnSc3d1	geologická
skladbě	skladba	k1gFnSc3	skladba
<g/>
,	,	kIx,	,
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc3d1	vodní
výměně	výměna	k1gFnSc3	výměna
s	s	k7c7	s
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
přítoku	přítok	k1gInSc2	přítok
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
teplá	teplý	k2eAgFnSc1d1	teplá
a	a	k8xC	a
slaná	slaný	k2eAgFnSc1d1	slaná
atlantická	atlantický	k2eAgFnSc1d1	Atlantická
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přitéká	přitékat	k5eAaImIp3nS	přitékat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
proud	proud	k1gInSc1	proud
přitéká	přitékat	k5eAaImIp3nS	přitékat
mezi	mezi	k7c7	mezi
Shetlandami	Shetlanda	k1gFnPc7	Shetlanda
a	a	k8xC	a
severním	severní	k2eAgInSc7d1	severní
koncem	konec	k1gInSc7	konec
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
přes	přes	k7c4	přes
Calaiskou	Calaiský	k2eAgFnSc4d1	Calaiský
úžinu	úžina	k1gFnSc4	úžina
<g/>
.	.	kIx.	.
</s>
<s>
Způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
slanost	slanost	k1gFnSc4	slanost
v	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
oblastech	oblast	k1gFnPc6	oblast
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
méně	málo	k6eAd2	málo
slaná	slaný	k2eAgFnSc1d1	slaná
baltská	baltský	k2eAgFnSc1d1	Baltská
voda	voda	k1gFnSc1	voda
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
přes	přes	k7c4	přes
průliv	průliv	k1gInSc4	průliv
Skagerrak	Skagerrak	k1gMnSc1	Skagerrak
<g/>
.	.	kIx.	.
</s>
<s>
Přítok	přítok	k1gInSc1	přítok
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
především	především	k9	především
na	na	k7c4	na
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
část	část	k1gFnSc4	část
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Labe	Labe	k1gNnSc1	Labe
<g/>
,	,	kIx,	,
Vezera	Vezera	k1gFnSc1	Vezera
<g/>
,	,	kIx,	,
Emže	Emže	k1gFnSc1	Emže
<g/>
,	,	kIx,	,
Rýn	Rýn	k1gInSc1	Rýn
<g/>
,	,	kIx,	,
Máza	Máza	k1gFnSc1	Máza
<g/>
,	,	kIx,	,
Šelda	Šelda	k1gFnSc1	Šelda
<g/>
,	,	kIx,	,
Temže	Temže	k1gFnSc1	Temže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitékající	přitékající	k2eAgFnSc1d1	přitékající
voda	voda	k1gFnSc1	voda
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
celkovou	celkový	k2eAgFnSc4d1	celková
cyklonální	cyklonální	k2eAgFnSc4d1	cyklonální
cirkulaci	cirkulace	k1gFnSc4	cirkulace
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
navazují	navazovat	k5eAaImIp3nP	navazovat
místní	místní	k2eAgFnSc1d1	místní
cirkulace	cirkulace	k1gFnSc1	cirkulace
stejného	stejný	k2eAgInSc2d1	stejný
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
do	do	k7c2	do
Norského	norský	k2eAgNnSc2d1	norské
moře	moře	k1gNnSc2	moře
mezi	mezi	k7c7	mezi
Skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
poloostrovem	poloostrov	k1gInSc7	poloostrov
a	a	k8xC	a
Shetlandami	Shetlanda	k1gFnPc7	Shetlanda
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
povrchových	povrchový	k2eAgInPc2d1	povrchový
proudů	proud	k1gInPc2	proud
obvykle	obvykle	k6eAd1	obvykle
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
0,3	[number]	k4	0,3
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
větrů	vítr	k1gInPc2	vítr
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
směr	směr	k1gInSc1	směr
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
může	moct	k5eAaImIp3nS	moct
značně	značně	k6eAd1	značně
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
přílivové	přílivový	k2eAgInPc1d1	přílivový
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
s	s	k7c7	s
dvanáctihodinovou	dvanáctihodinový	k2eAgFnSc7d1	dvanáctihodinová
periodou	perioda	k1gFnSc7	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
vlna	vlna	k1gFnSc1	vlna
přitéká	přitékat	k5eAaImIp3nS	přitékat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
za	za	k7c4	za
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
potkává	potkávat	k5eAaImIp3nS	potkávat
se	s	k7c7	s
slabší	slabý	k2eAgFnSc7d2	slabší
vlnou	vlna	k1gFnSc7	vlna
přicházející	přicházející	k2eAgFnSc7d1	přicházející
z	z	k7c2	z
průlivu	průliv	k1gInSc2	průliv
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
vlny	vlna	k1gFnSc2	vlna
u	u	k7c2	u
Shetland	Shetlanda	k1gFnPc2	Shetlanda
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
18	[number]	k4	18
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Přílivové	přílivový	k2eAgFnPc1d1	přílivová
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
celou	celý	k2eAgFnSc4d1	celá
hloubku	hloubka	k1gFnSc4	hloubka
moře	moře	k1gNnSc2	moře
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
promíchávání	promíchávání	k1gNnSc4	promíchávání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vertikální	vertikální	k2eAgNnSc4d1	vertikální
vyrovnávání	vyrovnávání	k1gNnSc4	vyrovnávání
slanosti	slanost	k1gFnSc2	slanost
<g/>
.	.	kIx.	.
</s>
<s>
Promíchávání	promíchávání	k1gNnSc1	promíchávání
vody	voda	k1gFnSc2	voda
větrem	vítr	k1gInSc7	vítr
probíhá	probíhat	k5eAaImIp3nS	probíhat
především	především	k9	především
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
mělké	mělký	k2eAgFnSc6d1	mělká
části	část	k1gFnSc6	část
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Organický	organický	k2eAgInSc1d1	organický
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
zoogeografickou	zoogeografický	k2eAgFnSc4d1	zoogeografická
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc4d1	podobný
Barentsovu	Barentsův	k2eAgFnSc4d1	Barentsova
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bohatší	bohatý	k2eAgFnSc1d2	bohatší
na	na	k7c4	na
teplomilné	teplomilný	k2eAgInPc4d1	teplomilný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pronikají	pronikat	k5eAaImIp3nP	pronikat
sem	sem	k6eAd1	sem
také	také	k9	také
některé	některý	k3yIgInPc1	některý
arktické	arktický	k2eAgInPc1d1	arktický
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moři	moře	k1gNnSc6	moře
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
1500	[number]	k4	1500
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
rozsivky	rozsivka	k1gFnSc2	rozsivka
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plankton	plankton	k1gInSc1	plankton
ale	ale	k8xC	ale
i	i	k9	i
větší	veliký	k2eAgFnPc4d2	veliký
zelené	zelený	k2eAgFnPc4d1	zelená
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnPc4d1	hnědá
a	a	k8xC	a
červené	červený	k2eAgFnPc4d1	červená
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
korýši	korýš	k1gMnPc1	korýš
(	(	kIx(	(
<g/>
asi	asi	k9	asi
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červi	červ	k1gMnPc1	červ
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
asi	asi	k9	asi
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
láčkovci	láčkovec	k1gMnPc1	láčkovec
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
asi	asi	k9	asi
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
terejovití	terejovití	k1gMnPc1	terejovití
<g/>
,	,	kIx,	,
rackovití	rackovitý	k2eAgMnPc1d1	rackovitý
<g/>
,	,	kIx,	,
alkouni	alkoun	k1gMnPc1	alkoun
úzkozobí	úzkozobý	k2eAgMnPc1d1	úzkozobý
a	a	k8xC	a
tlustozobí	tlustozobý	k2eAgMnPc1d1	tlustozobý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
litorály	litorál	k1gInPc4	litorál
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
porosty	porost	k1gInPc4	porost
fukoidů	fukoid	k1gInPc2	fukoid
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc2d1	velká
kolonie	kolonie	k1gFnSc2	kolonie
slávek	slávka	k1gFnPc2	slávka
<g/>
,	,	kIx,	,
svijonožců	svijonožec	k1gMnPc2	svijonožec
<g/>
,	,	kIx,	,
pískovníků	pískovník	k1gInPc2	pískovník
rybářských	rybářský	k2eAgInPc2d1	rybářský
<g/>
,	,	kIx,	,
blešivců	blešivec	k1gMnPc2	blešivec
<g/>
.	.	kIx.	.
</s>
<s>
Hlouběji	hluboko	k6eAd2	hluboko
roste	růst	k5eAaImIp3nS	růst
vocha	vocha	k1gFnSc1	vocha
mořská	mořský	k2eAgFnSc1d1	mořská
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
větší	veliký	k2eAgMnPc1d2	veliký
měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
slávička	slávička	k1gFnSc1	slávička
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
hřebenatkovití	hřebenatkovitý	k2eAgMnPc1d1	hřebenatkovitý
<g/>
,	,	kIx,	,
ústřice	ústřice	k1gFnSc1	ústřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
biomasy	biomasa	k1gFnSc2	biomasa
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
350	[number]	k4	350
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
sem	sem	k6eAd1	sem
lodě	loď	k1gFnPc1	loď
zavlekly	zavleknout	k5eAaPmAgFnP	zavleknout
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
kraba	krab	k1gMnSc2	krab
říčního	říční	k2eAgMnSc2d1	říční
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
rozmnožil	rozmnožit	k5eAaPmAgMnS	rozmnožit
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
rozrušuje	rozrušovat	k5eAaImIp3nS	rozrušovat
tam	tam	k6eAd1	tam
břehy	břeh	k1gInPc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
především	především	k9	především
platýs	platýs	k1gMnSc1	platýs
<g/>
,	,	kIx,	,
treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
treska	treska	k1gFnSc1	treska
jednoskvrnná	jednoskvrnný	k2eAgFnSc1d1	jednoskvrnná
<g/>
,	,	kIx,	,
treska	treska	k1gFnSc1	treska
tmavá	tmavý	k2eAgFnSc1d1	tmavá
<g/>
,	,	kIx,	,
makrely	makrela	k1gFnPc1	makrela
<g/>
,	,	kIx,	,
sleď	sleď	k1gMnSc1	sleď
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
šprot	šprot	k1gInSc4	šprot
obecný	obecný	k2eAgInSc4d1	obecný
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
rejnoků	rejnok	k1gMnPc2	rejnok
a	a	k8xC	a
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
probíhá	probíhat	k5eAaImIp3nS	probíhat
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vedl	vést	k5eAaImAgInS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
osud	osud	k1gInSc4	osud
vysloužilé	vysloužilý	k2eAgFnSc2d1	vysloužilá
ropné	ropný	k2eAgFnSc2d1	ropná
plošiny	plošina	k1gFnSc2	plošina
Brent	Brent	k?	Brent
Spar	Spar	k?	Spar
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
ropnou	ropný	k2eAgFnSc7d1	ropná
firmou	firma	k1gFnSc7	firma
Shell	Shella	k1gFnPc2	Shella
a	a	k8xC	a
původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
potopena	potopit	k5eAaPmNgFnS	potopit
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
komise	komise	k1gFnSc2	komise
úmluvy	úmluva	k1gFnSc2	úmluva
OSPAR	OSPAR	kA	OSPAR
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
moratorium	moratorium	k1gNnSc4	moratorium
na	na	k7c4	na
potápění	potápění	k1gNnSc4	potápění
ropných	ropný	k2eAgFnPc2d1	ropná
plošin	plošina	k1gFnPc2	plošina
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc2	jejich
ukládání	ukládání	k1gNnSc2	ukládání
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
členskými	členský	k2eAgFnPc7d1	členská
státy	stát	k1gInPc1	stát
OSPAR	OSPAR	kA	OSPAR
jednomyslně	jednomyslně	k6eAd1	jednomyslně
odhlasován	odhlasovat	k5eAaPmNgInS	odhlasovat
zákaz	zákaz	k1gInSc1	zákaz
potápění	potápění	k1gNnSc2	potápění
takových	takový	k3xDgNnPc2	takový
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nastal	nastat	k5eAaPmAgInS	nastat
pro	pro	k7c4	pro
naleziště	naleziště	k1gNnSc4	naleziště
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
ropný	ropný	k2eAgInSc4d1	ropný
vrchol	vrchol	k1gInSc4	vrchol
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
vytěžené	vytěžený	k2eAgFnSc2d1	vytěžená
ropy	ropa	k1gFnSc2	ropa
klesá	klesat	k5eAaImIp3nS	klesat
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
Bergen	Bergen	k1gInSc1	Bergen
<g/>
,	,	kIx,	,
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
propojeno	propojen	k2eAgNnSc1d1	propojeno
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
Kielským	kielský	k2eAgInSc7d1	kielský
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Selickaja	Selickaja	k1gMnSc1	Selickaja
E.	E.	kA	E.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Základní	základní	k2eAgInPc1d1	základní
rysy	rys	k1gInPc1	rys
hydrologického	hydrologický	k2eAgInSc2d1	hydrologický
režimu	režim	k1gInSc2	režim
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
Publikace	publikace	k1gFnSc1	publikace
Vládního	vládní	k2eAgInSc2d1	vládní
oceánografického	oceánografický	k2eAgInSc2d1	oceánografický
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
věstník	věstník	k1gInSc1	věstník
39	[number]	k4	39
<g/>
)	)	kIx)	)
rusky	rusky	k6eAd1	rusky
С	С	k?	С
Е	Е	k?	Е
С	С	k?	С
<g/>
,	,	kIx,	,
О	О	k?	О
ч	ч	k?	ч
г	г	k?	г
р	р	k?	р
С	С	k?	С
м	м	k?	м
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
Т	Т	k?	Т
Г	Г	k?	Г
<g />
.	.	kIx.	.
</s>
<s>
<g/>
о	о	k?	о
и	и	k?	и
<g/>
,	,	kIx,	,
в	в	k?	в
39	[number]	k4	39
<g/>
)	)	kIx)	)
Hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1964	[number]	k4	1964
rusky	rusky	k6eAd1	rusky
Г	Г	k?	Г
с	с	k?	с
С	С	k?	С
м	м	k?	м
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Naleziště	naleziště	k1gNnSc2	naleziště
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
oceánech	oceán	k1gInPc6	oceán
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1973	[number]	k4	1973
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
м	м	k?	м
и	и	k?	и
о	о	k?	о
<g/>
,	,	kIx,	,
М	М	k?	М
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Muratov	Muratov	k1gInSc1	Muratov
M.	M.	kA	M.
<g/>
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
Východoevropské	východoevropský	k2eAgFnSc3d1	východoevropská
plošině	plošina	k1gFnSc3	plošina
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
Publikace	publikace	k1gFnSc1	publikace
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
organizace	organizace	k1gFnSc2	organizace
zkoumání	zkoumání	k1gNnSc2	zkoumání
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc2	oddělení
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
<g/>
)	)	kIx)	)
rusky	rusky	k6eAd1	rusky
М	М	k?	М
М	М	k?	М
В	В	k?	В
<g/>
,	,	kIx,	,
С	С	k?	С
п	п	k?	п
и	и	k?	и
е	е	k?	е
с	с	k?	с
с	с	k?	с
<g />
.	.	kIx.	.
</s>
<s>
В	В	k?	В
п	п	k?	п
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Б	Б	k?	Б
<g/>
.	.	kIx.	.
М	М	k?	М
<g/>
.	.	kIx.	.
о	о	k?	о
и	и	k?	и
п	п	k?	п
<g/>
.	.	kIx.	.
О	О	k?	О
<g/>
.	.	kIx.	.
г	г	k?	г
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
No	no	k9	no
3	[number]	k4	3
Dietrich	Dietricha	k1gFnPc2	Dietricha
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
oblasti	oblast	k1gFnPc1	oblast
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
hydrografickém	hydrografický	k2eAgInSc6d1	hydrografický
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
Kieler	Kieler	k1gInSc1	Kieler
Meeresforschungen	Meeresforschungen	k1gInSc1	Meeresforschungen
1950	[number]	k4	1950
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Bd	Bd	k1gMnSc7	Bd
7	[number]	k4	7
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
německy	německy	k6eAd1	německy
Die	Die	k1gMnPc1	Die
naturlichen	naturlichna	k1gFnPc2	naturlichna
Regionen	Regionen	k2eAgInSc4d1	Regionen
von	von	k1gInSc4	von
Nord	Norda	k1gFnPc2	Norda
und	und	k?	und
Ostsee	Ostsee	k1gNnSc4	Ostsee
auf	auf	k?	auf
hydrographischer	hydrographischra	k1gFnPc2	hydrographischra
Grundlage	Grundlage	k1gFnSc1	Grundlage
<g/>
)	)	kIx)	)
Kо	Kо	k1gMnPc2	Kо
P.	P.	kA	P.
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Biological	Biological	k1gMnSc1	Biological
consequences	consequences	k1gMnSc1	consequences
of	of	k?	of
marine	marinout	k5eAaPmIp3nS	marinout
pollution	pollution	k1gInSc1	pollution
with	witha	k1gFnPc2	witha
special	speciat	k5eAaImAgInS	speciat
reference	reference	k1gFnPc4	reference
to	ten	k3xDgNnSc1	ten
the	the	k?	the
North	North	k1gInSc1	North
Sea	Sea	k1gMnSc1	Sea
fischeries	fischeries	k1gMnSc1	fischeries
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Helgolander	Helgolander	k1gInSc1	Helgolander
wis-senschaftliche	wisenschaftliche	k1gNnSc1	wis-senschaftliche
Meeresuntersuchungen	Meeresuntersuchungen	k1gInSc1	Meeresuntersuchungen
"	"	kIx"	"
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Bd	Bd	k1gFnSc1	Bd
17	[number]	k4	17
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
С	С	k?	С
М	М	k?	М
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severní	severní	k2eAgFnSc2d1	severní
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
