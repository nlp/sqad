<s>
Hugo	Hugo	k1gMnSc1	Hugo
Haas	Haasa	k1gFnPc2	Haasa
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1901	[number]	k4	1901
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
11	[number]	k4	11
v	v	k7c6	v
Biskupské	biskupský	k2eAgFnSc6d1	biskupská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
židovského	židovský	k2eAgMnSc2d1	židovský
majitele	majitel	k1gMnSc2	majitel
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
obuví	obuv	k1gFnSc7	obuv
U	u	k7c2	u
Zajíce	Zajíc	k1gMnSc2	Zajíc
Lipmanna	Lipmann	k1gMnSc2	Lipmann
(	(	kIx(	(
<g/>
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
)	)	kIx)	)
Haase	Haas	k1gMnSc2	Haas
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Elky	Elka	k1gFnSc2	Elka
(	(	kIx(	(
<g/>
Olgy	Olga	k1gFnSc2	Olga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Epsteinové	Epsteinová	k1gFnSc2	Epsteinová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
z	z	k7c2	z
Oděsy	Oděsa	k1gFnSc2	Oděsa
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
hercem	herec	k1gMnSc7	herec
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Hugův	Hugův	k2eAgMnSc1d1	Hugův
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
starší	starší	k1gMnSc1	starší
bratr	bratr	k1gMnSc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
Haas	Haasa	k1gFnPc2	Haasa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
postaveného	postavený	k2eAgInSc2d1	postavený
bytového	bytový	k2eAgInSc2d1	bytový
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Biskupské	biskupský	k2eAgFnSc6d1	biskupská
8	[number]	k4	8
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
architektem	architekt	k1gMnSc7	architekt
Maximem	Maxim	k1gMnSc7	Maxim
Johannem	Johann	k1gMnSc7	Johann
Monterem	Monter	k1gMnSc7	Monter
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tehdejším	tehdejší	k2eAgInPc3d1	tehdejší
zvykům	zvyk	k1gInPc3	zvyk
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
českému	český	k2eAgNnSc3d1	české
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
studoval	studovat	k5eAaImAgMnS	studovat
zpěv	zpěv	k1gInSc4	zpěv
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
také	také	k6eAd1	také
fonetiku	fonetika	k1gFnSc4	fonetika
u	u	k7c2	u
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
mu	on	k3xPp3gMnSc3	on
ředitel	ředitel	k1gMnSc1	ředitel
brněnského	brněnský	k2eAgNnSc2d1	brněnské
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Václav	Václav	k1gMnSc1	Václav
Štech	Štech	k1gMnSc1	Štech
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
angažmá	angažmá	k1gNnSc1	angažmá
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
sezóna	sezóna	k1gFnSc1	sezóna
1923	[number]	k4	1923
<g/>
/	/	kIx~	/
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
Divadla	divadlo	k1gNnSc2	divadlo
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
jej	on	k3xPp3gMnSc4	on
angažoval	angažovat	k5eAaBmAgMnS	angažovat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
jej	on	k3xPp3gMnSc4	on
Karel	Karel	k1gMnSc1	Karel
Hugo	Hugo	k1gMnSc1	Hugo
Hilar	Hilar	k1gMnSc1	Hilar
přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
činohry	činohra	k1gFnSc2	činohra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
hrál	hrát	k5eAaImAgInS	hrát
např.	např.	kA	např.
v	v	k7c6	v
inscenacích	inscenace	k1gFnPc6	inscenace
Karla	Karel	k1gMnSc2	Karel
Hugo	Hugo	k1gMnSc1	Hugo
Hilara	Hilar	k1gMnSc2	Hilar
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Frejky	Frejka	k1gFnSc2	Frejka
a	a	k8xC	a
K.	K.	kA	K.
Dostala	dostat	k5eAaPmAgFnS	dostat
(	(	kIx(	(
<g/>
Sokrates	Sokrates	k1gMnSc1	Sokrates
v	v	k7c6	v
Nezvalových	Nezvalových	k2eAgMnPc6d1	Nezvalových
Milencích	milenec	k1gMnPc6	milenec
z	z	k7c2	z
kiosku	kiosek	k1gInSc2	kiosek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
role	role	k1gFnPc4	role
patřil	patřit	k5eAaImAgMnS	patřit
doktor	doktor	k1gMnSc1	doktor
Galén	Galén	k1gInSc4	Galén
v	v	k7c6	v
Bílé	bílý	k2eAgFnSc6d1	bílá
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
přítel	přítel	k1gMnSc1	přítel
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
poslední	poslední	k2eAgFnSc7d1	poslední
rolí	role	k1gFnSc7	role
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
postava	postava	k1gFnSc1	postava
ředitele	ředitel	k1gMnSc2	ředitel
Busmana	Busman	k1gMnSc2	Busman
v	v	k7c6	v
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
V	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc3	role
notáře	notář	k1gMnSc2	notář
Voborského	Voborský	k2eAgMnSc2d1	Voborský
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
divadelní	divadelní	k2eAgFnSc2d1	divadelní
veselohry	veselohra	k1gFnSc2	veselohra
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Šamberka	Šamberka	k1gFnSc1	Šamberka
Jedenácté	jedenáctý	k4xOgInPc1	jedenáctý
přikázání	přikázání	k1gNnPc2	přikázání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
O	o	k7c4	o
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
roli	role	k1gFnSc4	role
zahrál	zahrát	k5eAaPmAgInS	zahrát
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
již	již	k6eAd1	již
zvukovém	zvukový	k2eAgInSc6d1	zvukový
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
filmu	film	k1gInSc6	film
režírovaném	režírovaný	k2eAgInSc6d1	režírovaný
Martinem	Martin	k1gMnSc7	Martin
Fričem	Frič	k1gMnSc7	Frič
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
mohl	moct	k5eAaImAgInS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
komediální	komediální	k2eAgInSc4d1	komediální
talent	talent	k1gInSc4	talent
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
offsidu	offsid	k1gInSc6	offsid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
asi	asi	k9	asi
ve	v	k7c6	v
třiceti	třicet	k4xCc6	třicet
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
poprvé	poprvé	k6eAd1	poprvé
režíroval	režírovat	k5eAaImAgMnS	režírovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Otakarem	Otakar	k1gMnSc7	Otakar
Vávrou	Vávra	k1gMnSc7	Vávra
film	film	k1gInSc4	film
Velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ještě	ještě	k9	ještě
režíroval	režírovat	k5eAaImAgMnS	režírovat
filmy	film	k1gInPc4	film
Kvočna	kvočna	k1gFnSc1	kvočna
(	(	kIx(	(
<g/>
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
film	film	k1gInSc4	film
složil	složit	k5eAaPmAgMnS	složit
bratra	bratr	k1gMnSc2	bratr
Pavel	Pavel	k1gMnSc1	Pavel
Haas	Haas	k1gInSc1	Haas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Děvčata	děvče	k1gNnPc1	děvče
<g/>
,	,	kIx,	,
nedejte	dát	k5eNaPmRp2nP	dát
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
šeptá	šeptat	k5eAaImIp3nS	šeptat
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Miroslava	Miroslav	k1gMnSc2	Miroslav
Cikána	cikán	k2eAgFnSc1d1	cikána
Andula	Andula	k1gFnSc1	Andula
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uvedena	uveden	k2eAgFnSc1d1	uvedena
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Olgy	Olga	k1gFnSc2	Olga
Scheinpflugové	Scheinpflugový	k2eAgFnSc2d1	Scheinpflugová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
výpověď	výpověď	k1gFnSc1	výpověď
kvůli	kvůli	k7c3	kvůli
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Bibi	Bib	k1gFnSc2	Bib
přes	přes	k7c4	přes
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Lisabon	Lisabon	k1gInSc1	Lisabon
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
syna	syn	k1gMnSc4	syn
Ivana	Ivan	k1gMnSc4	Ivan
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
bratr	bratr	k1gMnSc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
Haas	Haas	k1gInSc4	Haas
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Lipmann	Lipmann	k1gMnSc1	Lipmann
(	(	kIx(	(
<g/>
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
)	)	kIx)	)
i	i	k8xC	i
bratr	bratr	k1gMnSc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
hrál	hrát	k5eAaImAgInS	hrát
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Erwinem	Erwin	k1gMnSc7	Erwin
Piscatorem	Piscator	k1gMnSc7	Piscator
a	a	k8xC	a
Bertoltem	Bertolt	k1gInSc7	Bertolt
Brechtem	Brecht	k1gMnSc7	Brecht
<g/>
,	,	kIx,	,
např.	např.	kA	např.
role	role	k1gFnSc1	role
Galilea	Galilea	k1gFnSc1	Galilea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
v	v	k7c6	v
herecké	herecký	k2eAgFnSc6d1	herecká
škole	škola	k1gFnSc6	škola
Actor	Actora	k1gFnPc2	Actora
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Laboratory	Laborator	k1gInPc7	Laborator
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgInS	prosadit
se	se	k3xPyFc4	se
v	v	k7c6	v
i	i	k9	i
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
usadil	usadit	k5eAaPmAgMnS	usadit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
občas	občas	k6eAd1	občas
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátké	krátký	k2eAgFnSc2d1	krátká
návštěvy	návštěva	k1gFnSc2	návštěva
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
výročí	výročí	k1gNnPc2	výročí
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
už	už	k6eAd1	už
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
urna	urna	k1gFnSc1	urna
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
hrobu	hrob	k1gInSc2	hrob
na	na	k7c6	na
Židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
vedle	vedle	k7c2	vedle
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
Olgy	Olga	k1gFnSc2	Olga
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Ivana	Ivan	k1gMnSc2	Ivan
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
Sofie	Sofia	k1gFnSc2	Sofia
(	(	kIx(	(
<g/>
Soni	Soňa	k1gFnSc2	Soňa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
manželky	manželka	k1gFnSc2	manželka
bratra	bratr	k1gMnSc4	bratr
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
umělci	umělec	k1gMnPc7	umělec
jej	on	k3xPp3gMnSc4	on
spojovalo	spojovat	k5eAaImAgNnS	spojovat
dlouholeté	dlouholetý	k2eAgNnSc1d1	dlouholeté
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
společní	společní	k2eAgNnSc1d1	společní
známí	známit	k5eAaImIp3nS	známit
i	i	k9	i
podobný	podobný	k2eAgInSc1d1	podobný
způsob	způsob	k1gInSc1	způsob
nazírání	nazírání	k1gNnSc2	nazírání
na	na	k7c4	na
divadelní	divadelní	k2eAgFnSc4d1	divadelní
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
podobnému	podobný	k2eAgInSc3d1	podobný
smýšlení	smýšlení	k1gNnSc2	smýšlení
mu	on	k3xPp3gMnSc3	on
nechal	nechat	k5eAaPmAgMnS	nechat
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
při	při	k7c6	při
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
doktora	doktor	k1gMnSc2	doktor
Galéna	Galén	k1gMnSc2	Galén
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
hry	hra	k1gFnSc2	hra
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
nezasahoval	zasahovat	k5eNaImAgMnS	zasahovat
do	do	k7c2	do
zfilmování	zfilmování	k1gNnSc2	zfilmování
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
Hugo	Hugo	k1gMnSc1	Hugo
ujal	ujmout	k5eAaPmAgMnS	ujmout
jako	jako	k9	jako
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Haase	Haase	k6eAd1	Haase
se	se	k3xPyFc4	se
hluboce	hluboko	k6eAd1	hluboko
dotkla	dotknout	k5eAaPmAgFnS	dotknout
smrt	smrt	k1gFnSc1	smrt
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gMnSc4	jejich
společného	společný	k2eAgMnSc4d1	společný
přítele	přítel	k1gMnSc4	přítel
Karla	Karel	k1gMnSc4	Karel
Poláčka	Poláček	k1gMnSc4	Poláček
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důvěrné	důvěrný	k2eAgInPc4d1	důvěrný
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
udržoval	udržovat	k5eAaImAgMnS	udržovat
písemnou	písemný	k2eAgFnSc4d1	písemná
korespondenci	korespondence	k1gFnSc4	korespondence
i	i	k9	i
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nuceném	nucený	k2eAgInSc6d1	nucený
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
Čapkova	Čapkův	k2eAgFnSc1d1	Čapkova
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Olga	Olga	k1gFnSc1	Olga
Scheinpflugová	Scheinpflugový	k2eAgFnSc1d1	Scheinpflugová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
nejčastějšími	častý	k2eAgFnPc7d3	nejčastější
partnerkami	partnerka	k1gFnPc7	partnerka
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
pražských	pražský	k2eAgFnPc6d1	Pražská
scénách	scéna	k1gFnPc6	scéna
Vinohradského	vinohradský	k2eAgNnSc2d1	Vinohradské
a	a	k8xC	a
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
herečky	herečka	k1gFnSc2	herečka
Andula	Andula	k1gFnSc1	Andula
Sedláčková	Sedláčková	k1gFnSc1	Sedláčková
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šejbalová	Šejbalová	k1gFnSc1	Šejbalová
a	a	k8xC	a
Olga	Olga	k1gFnSc1	Olga
Scheinpflugová	Scheinpflugový	k2eAgFnSc1d1	Scheinpflugová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
herec	herec	k1gMnSc1	herec
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
své	svůj	k3xOyFgFnPc4	svůj
partnerky	partnerka	k1gFnPc4	partnerka
ve	v	k7c6	v
filmu	film	k1gInSc6	film
vybírat	vybírat	k5eAaImF	vybírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
nebylo	být	k5eNaImAgNnS	být
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
filmovou	filmový	k2eAgFnSc7d1	filmová
partnerkou	partnerka	k1gFnSc7	partnerka
Adina	Adina	k1gFnSc1	Adina
Mandlová	mandlový	k2eAgFnSc1d1	Mandlová
(	(	kIx(	(
<g/>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
nebožtík	nebožtík	k1gMnSc1	nebožtík
<g/>
,	,	kIx,	,
Děvčata	děvče	k1gNnPc4	děvče
nedejte	dát	k5eNaPmRp2nP	dát
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Mazlíček	mazlíček	k1gMnSc1	mazlíček
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
rovněž	rovněž	k9	rovněž
i	i	k9	i
Věra	Věra	k1gFnSc1	Věra
Ferbasová	Ferbasová	k1gFnSc1	Ferbasová
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Mravnost	mravnost	k1gFnSc4	mravnost
nade	nad	k7c4	nad
vše	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Andula	Andula	k1gFnSc1	Andula
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
Velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
a	a	k8xC	a
Okénko	okénko	k1gNnSc4	okénko
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
manžela	manžel	k1gMnSc4	manžel
Antonie	Antonie	k1gFnSc2	Antonie
Nedošinské	Nedošinský	k2eAgInPc1d1	Nedošinský
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Haasovy	Haasův	k2eAgFnPc1d1	Haasova
partnerky	partnerka	k1gFnPc1	partnerka
se	se	k3xPyFc4	se
představily	představit	k5eAaPmAgFnP	představit
také	také	k9	také
Suzanne	Suzann	k1gMnSc5	Suzann
Marwille	Marwill	k1gMnSc5	Marwill
(	(	kIx(	(
<g/>
Sestra	sestra	k1gFnSc1	sestra
Angelika	Angelika	k1gFnSc1	Angelika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
(	(	kIx(	(
<g/>
Madla	Madla	k1gFnSc1	Madla
z	z	k7c2	z
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
,	,	kIx,	,
Okénko	okénko	k1gNnSc1	okénko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jiřina	Jiřina	k1gFnSc1	Jiřina
Štěpničková	Štěpničková	k1gFnSc1	Štěpničková
(	(	kIx(	(
<g/>
Jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
,	,	kIx,	,
Velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
-	-	kIx~	-
role	role	k1gFnSc1	role
dcery	dcera	k1gFnSc2	dcera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
Pianola	pianola	k1gFnSc1	pianola
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
natočeno	natočen	k2eAgNnSc1d1	natočeno
snad	snad	k9	snad
už	už	k6eAd1	už
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
hráli	hrát	k5eAaImAgMnP	hrát
kdysi	kdysi	k6eAd1	kdysi
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
Paradise	Paradise	k1gFnSc1	Paradise
Alley	Allea	k1gFnSc2	Allea
1960	[number]	k4	1960
"	"	kIx"	"
<g/>
Bonanza	Bonanza	k1gFnSc1	Bonanza
<g/>
"	"	kIx"	"
1960	[number]	k4	1960
"	"	kIx"	"
<g/>
Adventures	Adventures	k1gInSc1	Adventures
in	in	k?	in
Paradise	Paradise	k1gFnSc2	Paradise
<g/>
"	"	kIx"	"
1959	[number]	k4	1959
Born	Borno	k1gNnPc2	Borno
to	ten	k3xDgNnSc4	ten
Be	Be	k1gMnSc1	Be
Loved	Loved	k1gMnSc1	Loved
1956-57	[number]	k4	1956-57
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gMnSc5	Telephon
Time	Timus	k1gMnSc5	Timus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
1957	[number]	k4	1957
Lizzie	Lizzie	k1gFnSc1	Lizzie
1957	[number]	k4	1957
Hit	hit	k1gInSc1	hit
and	and	k?	and
Run	run	k1gInSc1	run
1956	[number]	k4	1956
Edge	Edg	k1gFnSc2	Edg
of	of	k?	of
Hell	Hell	k1gMnSc1	Hell
1955	[number]	k4	1955
The	The	k1gFnSc2	The
Tender	tender	k1gInSc1	tender
Trap	trap	k1gInSc1	trap
1953-54	[number]	k4	1953-54
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ford	ford	k1gInSc1	ford
Television	Television	k1gInSc1	Television
Theatre	Theatr	k1gInSc5	Theatr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
The	The	k1gMnSc1	The
Other	Other	k1gMnSc1	Other
Woman	Woman	k1gMnSc1	Woman
1954	[number]	k4	1954
Bait	Bait	k1gInSc1	Bait
1953	[number]	k4	1953
Thy	Thy	k1gFnSc2	Thy
Neighbor	Neighbora	k1gFnPc2	Neighbora
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wife	Wife	k1gFnSc7	Wife
<g />
.	.	kIx.	.
</s>
<s>
1953	[number]	k4	1953
One	One	k1gFnSc1	One
Girl	girl	k1gFnSc1	girl
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Confession	Confession	k1gInSc1	Confession
1952	[number]	k4	1952
Strange	Strange	k1gNnSc2	Strange
Fascination	Fascination	k1gInSc4	Fascination
1951	[number]	k4	1951
The	The	k1gFnSc2	The
Girl	girl	k1gFnSc2	girl
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Bridge	Bridge	k1gNnSc1	Bridge
1951	[number]	k4	1951
Hlídač	hlídač	k1gInSc4	hlídač
č.	č.	k?	č.
47	[number]	k4	47
(	(	kIx(	(
<g/>
originál	originál	k1gMnSc1	originál
Pickup	Pickup	k1gMnSc1	Pickup
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
Vendetta	Vendett	k1gInSc2	Vendett
1950	[number]	k4	1950
King	King	k1gMnSc1	King
Solomon	Solomon	k1gMnSc1	Solomon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mines	Mines	k1gInSc1	Mines
1949	[number]	k4	1949
The	The	k1gMnPc2	The
Fighting	Fighting	k1gInSc4	Fighting
Kentuckian	Kentuckiana	k1gFnPc2	Kentuckiana
1949	[number]	k4	1949
"	"	kIx"	"
<g/>
Your	Your	k1gInSc1	Your
Show	show	k1gFnSc2	show
Time	Tim	k1gFnSc2	Tim
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
For	forum	k1gNnPc2	forum
the	the	k?	the
Love	lov	k1gInSc5	lov
of	of	k?	of
Mary	Mary	k1gFnSc7	Mary
1948	[number]	k4	1948
Casbah	Casbaha	k1gFnPc2	Casbaha
1948	[number]	k4	1948
My	my	k3xPp1nPc1	my
Girl	girl	k1gFnSc3	girl
Tisa	Tisa	k1gFnSc1	Tisa
1947	[number]	k4	1947
Merton	Merton	k1gInSc1	Merton
of	of	k?	of
the	the	k?	the
Movies	Movies	k1gInSc4	Movies
1947	[number]	k4	1947
The	The	k1gMnSc1	The
Foxes	Foxes	k1gMnSc1	Foxes
of	of	k?	of
Harrow	Harrow	k1gFnSc2	Harrow
1947	[number]	k4	1947
Northwest	Northwest	k1gFnSc1	Northwest
Outpost	Outpost	k1gFnSc1	Outpost
1947	[number]	k4	1947
Fiesta	fiesta	k1gFnSc1	fiesta
1947	[number]	k4	1947
The	The	k1gFnPc2	The
Private	Privat	k1gInSc5	Privat
Affairs	Affairs	k1gInSc4	Affairs
of	of	k?	of
Bel	bel	k1gInSc1	bel
Ami	Ami	k1gFnSc1	Ami
1947	[number]	k4	1947
Leben	Lebna	k1gFnPc2	Lebna
des	des	k1gNnSc4	des
Galilei	Galilei	k1gNnSc2	Galilei
1946	[number]	k4	1946
Holiday	Holidaa	k1gFnSc2	Holidaa
in	in	k?	in
Mexico	Mexico	k1gMnSc1	Mexico
1946	[number]	k4	1946
Two	Two	k1gFnSc2	Two
Smart	Smarta	k1gFnPc2	Smarta
People	People	k1gFnSc4	People
1945	[number]	k4	1945
Dakota	Dakota	k1gFnSc1	Dakota
1945	[number]	k4	1945
What	What	k1gMnSc1	What
Next	Next	k1gMnSc1	Next
<g/>
,	,	kIx,	,
Corporal	Corporal	k1gMnSc1	Corporal
Hargrove	Hargrov	k1gInSc5	Hargrov
<g/>
?	?	kIx.	?
</s>
<s>
1945	[number]	k4	1945
Jealousy	Jealous	k1gInPc7	Jealous
1945	[number]	k4	1945
A	a	k9	a
Bell	bell	k1gInSc1	bell
for	forum	k1gNnPc2	forum
Adano	Adano	k6eAd1	Adano
1945	[number]	k4	1945
Documents	Documentsa	k1gFnPc2	Documentsa
secrets	secrets	k6eAd1	secrets
1944	[number]	k4	1944
The	The	k1gFnPc2	The
Princess	Princessa	k1gFnPc2	Princessa
and	and	k?	and
the	the	k?	the
Pirate	Pirat	k1gInSc5	Pirat
1944	[number]	k4	1944
Mrs	Mrs	k1gFnPc3	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Parkington	Parkington	k1gInSc1	Parkington
1944	[number]	k4	1944
Strange	Strange	k1gNnSc2	Strange
Affair	Affair	k1gInSc4	Affair
1944	[number]	k4	1944
Summer	Summer	k1gMnSc1	Summer
Storm	Storm	k1gInSc4	Storm
1944	[number]	k4	1944
Days	Days	k1gInSc1	Days
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
1938	[number]	k4	1938
Co	co	k9	co
se	se	k3xPyFc4	se
šeptá	šeptat	k5eAaImIp3nS	šeptat
...	...	k?	...
Vilém	Vilém	k1gMnSc1	Vilém
Gregor	Gregor	k1gMnSc1	Gregor
1938	[number]	k4	1938
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
žebrá	žebrat	k5eAaImIp3nS	žebrat
...	...	k?	...
Josef	Josef	k1gMnSc1	Josef
Dostál	Dostál	k1gMnSc1	Dostál
1938	[number]	k4	1938
Andula	Andula	k1gFnSc1	Andula
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
...	...	k?	...
Pavel	Pavel	k1gMnSc1	Pavel
Haken	Haken	k1gMnSc1	Haken
1937	[number]	k4	1937
Děvčata	děvče	k1gNnPc4	děvče
<g/>
,	,	kIx,	,
nedejte	dát	k5eNaPmRp2nP	dát
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
...	...	k?	...
Prof.	prof.	kA	prof.
Emanuel	Emanuel	k1gMnSc1	Emanuel
Pokorný	Pokorný	k1gMnSc1	Pokorný
1937	[number]	k4	1937
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
...	...	k?	...
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galen	Galen	k1gInSc4	Galen
1936	[number]	k4	1936
Dobrodinec	dobrodinec	k1gMnSc1	dobrodinec
chudých	chudý	k2eAgMnPc2d1	chudý
psů	pes	k1gMnPc2	pes
...	...	k?	...
Pohodný	pohodný	k1gMnSc1	pohodný
Tobiáš	Tobiáš	k1gMnSc1	Tobiáš
1936	[number]	k4	1936
Ulička	ulička	k1gFnSc1	ulička
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
...	...	k?	...
Tobiáš	Tobiáš	k1gMnSc1	Tobiáš
1936	[number]	k4	1936
Tři	tři	k4xCgMnPc4	tři
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
...	...	k?	...
Továrník	továrník	k1gMnSc1	továrník
Bárta	Bárta	k1gMnSc1	Bárta
1936	[number]	k4	1936
Švadlenka	švadlenka	k1gFnSc1	švadlenka
...	...	k?	...
Francois	Francois	k1gMnSc1	Francois
Lorrain	Lorrain	k1gInSc4	Lorrain
1936	[number]	k4	1936
Mravnost	mravnost	k1gFnSc1	mravnost
nade	nad	k7c4	nad
vše	všechen	k3xTgNnSc4	všechen
...	...	k?	...
Prof.	prof.	kA	prof.
Antonín	Antonín	k1gMnSc1	Antonín
Karas	Karas	k1gMnSc1	Karas
1936	[number]	k4	1936
Velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
<g />
.	.	kIx.	.
</s>
<s>
...	...	k?	...
Žebrák	žebrák	k1gMnSc1	žebrák
Josef	Josef	k1gMnSc1	Josef
Pešta	Pešta	k1gMnSc1	Pešta
1935	[number]	k4	1935
Ať	ať	k8xC	ať
žije	žít	k5eAaImIp3nS	žít
nebožtík	nebožtík	k1gMnSc1	nebožtík
...	...	k?	...
Petr	Petr	k1gMnSc1	Petr
Suk	suk	k1gInSc4	suk
1935	[number]	k4	1935
Jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
přikázání	přikázání	k1gNnPc2	přikázání
...	...	k?	...
Jiří	Jiří	k1gMnSc1	Jiří
Voborský	Voborský	k2eAgMnSc1d1	Voborský
1934	[number]	k4	1934
Mazlíček	mazlíček	k1gMnSc1	mazlíček
...	...	k?	...
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Alois	Alois	k1gMnSc1	Alois
Pech	Pech	k1gMnSc1	Pech
<g/>
,	,	kIx,	,
vězeňský	vězeňský	k2eAgMnSc1d1	vězeňský
knihovník	knihovník	k1gMnSc1	knihovník
1934	[number]	k4	1934
Poslední	poslední	k2eAgMnSc1d1	poslední
muž	muž	k1gMnSc1	muž
...	...	k?	...
Prof.	prof.	kA	prof.
Alois	Alois	k1gMnSc1	Alois
Kohout	Kohout	k1gMnSc1	Kohout
1933	[number]	k4	1933
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
...	...	k?	...
Zajíček	Zajíček	k1gMnSc1	Zajíček
1933	[number]	k4	1933
Její	její	k3xOp3gMnSc1	její
lékař	lékař	k1gMnSc1	lékař
...	...	k?	...
Pavel	Pavel	k1gMnSc1	Pavel
Hodura	Hodura	k1gFnSc1	Hodura
1933	[number]	k4	1933
Okénko	okénko	k1gNnSc4	okénko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
...	...	k?	...
docent	docent	k1gMnSc1	docent
Jakub	Jakub	k1gMnSc1	Jakub
Johánek	Johánek	k1gMnSc1	Johánek
1933	[number]	k4	1933
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
...	...	k?	...
Skladatel	skladatel	k1gMnSc1	skladatel
Viktor	Viktor	k1gMnSc1	Viktor
Honzl	Honzl	k1gMnSc1	Honzl
1932	[number]	k4	1932
Obrácení	obrácení	k1gNnPc2	obrácení
Ferdyše	Ferdyš	k1gMnPc4	Ferdyš
Pištory	Pištor	k1gInPc4	Pištor
...	...	k?	...
Richard	Richard	k1gMnSc1	Richard
Rosenstok	Rosenstok	k1gInSc4	Rosenstok
1932	[number]	k4	1932
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
...	...	k?	...
Adam	Adam	k1gMnSc1	Adam
Hejnů	Hejna	k1gMnPc2	Hejna
1932	[number]	k4	1932
Sestra	sestra	k1gFnSc1	sestra
Angelika	Angelika	k1gFnSc1	Angelika
...	...	k?	...
Pavel	Pavel	k1gMnSc1	Pavel
Ryant	Ryant	k1gInSc4	Ryant
1932	[number]	k4	1932
Madla	madlo	k1gNnSc2	madlo
z	z	k7c2	z
cihelny	cihelna	k1gFnSc2	cihelna
...	...	k?	...
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
1931	[number]	k4	1931
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
...	...	k?	...
MUDr.	MUDr.	kA	MUDr.
Katz	Katz	k1gInSc4	Katz
1931	[number]	k4	1931
Načeradec	Načeradec	k1gMnSc1	Načeradec
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
kibiců	kibic	k1gMnPc2	kibic
...	...	k?	...
Richard	Richard	k1gMnSc1	Richard
Načeradec	Načeradec	k1gMnSc1	Načeradec
1931	[number]	k4	1931
Kariéra	kariéra	k1gFnSc1	kariéra
Pavla	Pavla	k1gFnSc1	Pavla
Čamrdy	čamrda	k1gFnSc2	čamrda
...	...	k?	...
Vokoun	Vokoun	k1gMnSc1	Vokoun
1931	[number]	k4	1931
Muži	muž	k1gMnPc7	muž
v	v	k7c6	v
offsidu	offsid	k1gInSc6	offsid
...	...	k?	...
Načeradec	Načeradec	k1gMnSc1	Načeradec
1930	[number]	k4	1930
Když	když	k8xS	když
struny	struna	k1gFnPc1	struna
lkají	lkát	k5eAaImIp3nP	lkát
...	...	k?	...
Host	host	k1gMnSc1	host
v	v	k7c6	v
baru	bar	k1gInSc6	bar
1929	[number]	k4	1929
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
mlýnů	mlýn	k1gInPc2	mlýn
1925	[number]	k4	1925
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
...	...	k?	...
Šlik	šlika	k1gFnPc2	šlika
</s>
