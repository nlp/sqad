<p>
<s>
Xavier	Xavier	k1gMnSc1	Xavier
Baumaxa	Baumaxa	k1gMnSc1	Baumaxa
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Lubomír	Lubomír	k1gMnSc1	Lubomír
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1973	[number]	k4	1973
Most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
litvínovský	litvínovský	k2eAgMnSc1d1	litvínovský
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
škole	škola	k1gFnSc6	škola
umění	umění	k1gNnSc2	umění
učil	učit	k5eAaImAgInS	učit
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kapelu	kapela	k1gFnSc4	kapela
–	–	k?	–
Prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
pedagogiku	pedagogika	k1gFnSc4	pedagogika
s	s	k7c7	s
aprobací	aprobace	k1gFnSc7	aprobace
na	na	k7c4	na
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
V.	V.	kA	V.
<g/>
M.	M.	kA	M.
<g/>
H.	H.	kA	H.
a	a	k8xC	a
Faulnamusic	Faulnamusic	k1gMnSc1	Faulnamusic
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
projev	projev	k1gInSc1	projev
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
slovních	slovní	k2eAgFnPc6d1	slovní
hříčkách	hříčka	k1gFnPc6	hříčka
<g/>
,	,	kIx,	,
parodiích	parodie	k1gFnPc6	parodie
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
a	a	k8xC	a
zpěváky	zpěvák	k1gMnPc4	zpěvák
a	a	k8xC	a
popkulturních	popkulturní	k2eAgInPc6d1	popkulturní
odkazech	odkaz	k1gInPc6	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
prokládány	prokládat	k5eAaImNgFnP	prokládat
minimalistickou	minimalistický	k2eAgFnSc7d1	minimalistická
poezií	poezie	k1gFnSc7	poezie
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
básníka	básník	k1gMnSc2	básník
Em	Ema	k1gFnPc2	Ema
Rudenka	Rudenka	k1gFnSc1	Rudenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alba	album	k1gNnSc2	album
==	==	k?	==
</s>
</p>
<p>
<s>
Fenkám	fenka	k1gFnPc3	fenka
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Buranissimo	Buranissimo	k6eAd1	Buranissimo
forte	forte	k6eAd1	forte
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Retrofutro	Retrofutro	k1gNnSc1	Retrofutro
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Desperanto	Desperanta	k1gFnSc5	Desperanta
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Hněddé	Hněddý	k2eAgInPc4d1	Hněddý
smyčce	smyčec	k1gInPc4	smyčec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
Clintn	Clintn	k1gMnSc1	Clintn
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
mi	já	k3xPp1nSc3	já
hýkal	hýkat	k5eAaImAgMnS	hýkat
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Coveranto	Coveranta	k1gFnSc5	Coveranta
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Hněddé	Hněddý	k2eAgInPc4d1	Hněddý
smyčce	smyčec	k1gInPc4	smyčec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dawntempo	Dawntempa	k1gFnSc5	Dawntempa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jazz	jazz	k1gInSc4	jazz
Sex	sex	k1gInSc1	sex
Tet	Teta	k1gFnPc2	Teta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pijano	Pijana	k1gFnSc5	Pijana
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Idueto	Idueto	k1gNnSc1	Idueto
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
píseň	píseň	k1gFnSc4	píseň
Presumpce	presumpce	k1gFnSc2	presumpce
neviny	nevina	k1gFnSc2	nevina
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
na	na	k7c6	na
sampleru	sampler	k1gInSc6	sampler
50	[number]	k4	50
miniatur	miniatura	k1gFnPc2	miniatura
a	a	k8xC	a
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Odešli	odejít	k5eAaPmAgMnP	odejít
spolu	spolu	k6eAd1	spolu
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
a	a	k8xC	a
Leo	Leo	k1gMnSc1	Leo
<g/>
)	)	kIx)	)
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Jana	Jan	k1gMnSc2	Jan
Buriana	Burian	k1gMnSc2	Burian
Muži	muž	k1gMnPc1	muž
jsou	být	k5eAaImIp3nP	být
křehcí	křehký	k2eAgMnPc1d1	křehký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Xavier	Xavira	k1gFnPc2	Xavira
Baumaxa	Baumax	k1gInSc2	Baumax
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Xavier	Xavira	k1gFnPc2	Xavira
Baumaxa	Baumax	k1gInSc2	Baumax
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Xavier	Xavier	k1gMnSc1	Xavier
Baumaxa	Baumaxa	k1gMnSc1	Baumaxa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Xaviera	Xaviero	k1gNnSc2	Xaviero
Baumaxy	Baumax	k1gInPc5	Baumax
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
desky	deska	k1gFnSc2	deska
Retrofutro	Retrofutro	k1gNnSc1	Retrofutro
v	v	k7c6	v
Reflexu	reflex	k1gInSc6	reflex
</s>
</p>
<p>
<s>
Xavier	Xavier	k1gInSc1	Xavier
v	v	k7c6	v
Paskvilu	paskvil	k1gInSc6	paskvil
</s>
</p>
