<s>
Déjà	Déjà	k?
vu	vu	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
„	„	k?
<g/>
již	již	k6eAd1
viděno	viděn	k2eAgNnSc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
vyslovováno	vyslovován	k2eAgNnSc1d1
[	[	kIx(
<g/>
deʒ	deʒ	k6eAd1
vy	vy	k3xPp2nPc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
v	v	k7c6
psychologii	psychologie	k1gFnSc6
jev	jev	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
z	z	k7c2
ničeho	nic	k3yNnSc2
nic	nic	k3yNnSc4
intenzivní	intenzivní	k2eAgInSc1d1
pocit	pocit	k1gInSc1
něčeho	něco	k3yInSc2
už	už	k9
dříve	dříve	k6eAd2
prožitého	prožitý	k2eAgNnSc2d1
<g/>
,	,	kIx,
viděného	viděný	k2eAgNnSc2d1
nebo	nebo	k8xC
slyšeného	slyšený	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>