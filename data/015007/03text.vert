<s>
Déjà	Déjà	k?
vu	vu	k?
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
psychologickém	psychologický	k2eAgInSc6d1
jevu	jev	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Déjà	Déjà	k1gMnSc2
vu	vu	k?
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Déjà	Déjà	k?
vu	vu	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
„	„	k?
<g/>
již	již	k6eAd1
viděno	viděn	k2eAgNnSc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
vyslovováno	vyslovován	k2eAgNnSc1d1
[	[	kIx(
<g/>
deʒ	deʒ	k6eAd1
vy	vy	k3xPp2nPc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
v	v	k7c6
psychologii	psychologie	k1gFnSc6
jev	jev	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
z	z	k7c2
ničeho	nic	k3yNnSc2
nic	nic	k3yNnSc4
intenzivní	intenzivní	k2eAgInSc1d1
pocit	pocit	k1gInSc1
něčeho	něco	k3yInSc2
už	už	k9
dříve	dříve	k6eAd2
prožitého	prožitý	k2eAgNnSc2d1
<g/>
,	,	kIx,
viděného	viděný	k2eAgNnSc2d1
nebo	nebo	k8xC
slyšeného	slyšený	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
řeči	řeč	k1gFnSc6
nebo	nebo	k8xC
např.	např.	kA
v	v	k7c6
divadelní	divadelní	k2eAgFnSc6d1
kritice	kritika	k1gFnSc6
může	moct	k5eAaImIp3nS
ale	ale	k9
znamenat	znamenat	k5eAaImF
také	také	k9
prostě	prostě	k9
něco	něco	k3yInSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
„	„	k?
<g/>
jsme	být	k5eAaImIp1nP
už	už	k6eAd1
viděli	vidět	k5eAaImAgMnP
<g/>
“	“	k?
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
není	být	k5eNaImIp3nS
originální	originální	k2eAgFnSc1d1
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Psychologie	psychologie	k1gFnSc1
déjà	déjà	k?
vu	vu	k?
</s>
<s>
Déjà	Déjà	k?
vu	vu	k?
jako	jako	k8xC,k8xS
psychologický	psychologický	k2eAgInSc4d1
termín	termín	k1gInSc4
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
Émile	Émile	k1gFnSc2
Boirac	Boirac	k1gInSc1
roku	rok	k1gInSc2
1876	#num#	k4
a	a	k8xC
zpopularizoval	zpopularizovat	k5eAaPmAgMnS
v	v	k7c6
knize	kniha	k1gFnSc6
Budoucnost	budoucnost	k1gFnSc1
psychologických	psychologický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
Avenir	Avenir	k1gMnSc1
des	des	k1gNnSc2
sciences	sciences	k1gMnSc1
psychiques	psychiques	k1gMnSc1
<g/>
,	,	kIx,
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prožitek	prožitek	k1gInSc1
déjà	déjà	k?
vu	vu	k?
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
doprovázen	doprovázet	k5eAaImNgInS
pocitem	pocit	k1gInSc7
důvěrné	důvěrný	k2eAgFnPc1d1
známosti	známost	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
něčeho	něco	k3yInSc2
zvláštního	zvláštní	k2eAgNnSc2d1
<g/>
,	,	kIx,
neobvyklého	obvyklý	k2eNgNnSc2d1
až	až	k9
tajemného	tajemný	k2eAgMnSc4d1
<g/>
,	,	kIx,
dotyčný	dotyčný	k2eAgMnSc1d1
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
vědomý	vědomý	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
pocit	pocit	k1gInSc1
známosti	známost	k1gFnSc2
není	být	k5eNaImIp3nS
opodstatněný	opodstatněný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Předchozí	předchozí	k2eAgFnSc1d1
<g/>
“	“	k?
zkušenost	zkušenost	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
přikládána	přikládán	k2eAgFnSc1d1
předchozímu	předchozí	k2eAgInSc3d1
snu	sen	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
mnohdy	mnohdy	k6eAd1
je	být	k5eAaImIp3nS
přítomen	přítomen	k2eAgInSc1d1
pocit	pocit	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
zkušenost	zkušenost	k1gFnSc1
byla	být	k5eAaImAgFnS
jistě	jistě	k6eAd1
prožita	prožít	k5eAaPmNgFnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zkušenost	zkušenost	k1gFnSc1
déjà	déjà	k?
vu	vu	k?
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
dosti	dosti	k6eAd1
běžná	běžný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
průzkumy	průzkum	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
asi	asi	k9
20	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pocit	pocit	k1gInSc1
déjà	déjà	k?
vu	vu	k?
alespoň	alespoň	k9
jednou	jednou	k6eAd1
zažilo	zažít	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Odkazy	odkaz	k1gInPc1
na	na	k7c4
zkušenost	zkušenost	k1gFnSc4
s	s	k7c7
déjà	déjà	k?
vu	vu	k?
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
také	také	k9
v	v	k7c6
literatuře	literatura	k1gFnSc6
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
značí	značit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nejde	jít	k5eNaImIp3nS
o	o	k7c4
nový	nový	k2eAgInSc4d1
fenomén	fenomén	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Pokusy	pokus	k1gInPc1
o	o	k7c4
<g/>
)	)	kIx)
výklad	výklad	k1gInSc4
</s>
<s>
Psychologové	psycholog	k1gMnPc1
jev	jev	k1gInSc1
druhého	druhý	k4xOgNnSc2
nebo	nebo	k8xC
opakovaného	opakovaný	k2eAgNnSc2d1
<g/>
,	,	kIx,
již	již	k6eAd1
známého	známý	k2eAgInSc2d1
prožitku	prožitek	k1gInSc2
vysvětlují	vysvětlovat	k5eAaImIp3nP
určitou	určitý	k2eAgFnSc7d1
podobností	podobnost	k1gFnSc7
současnosti	současnost	k1gFnSc2
se	s	k7c7
vzpomínkou	vzpomínka	k1gFnSc7
nebo	nebo	k8xC
krátkodobým	krátkodobý	k2eAgInSc7d1
výpadkem	výpadek	k1gInSc7
pozornosti	pozornost	k1gFnSc2
a	a	k8xC
jakýmsi	jakýsi	k3yIgNnSc7
zapomenutím	zapomenutí	k1gNnSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
jsme	být	k5eAaImIp1nP
nesoustředěně	soustředěně	k6eNd1
pozorovali	pozorovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jev	jev	k1gInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
oblibou	obliba	k1gFnSc7
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
v	v	k7c6
literatuře	literatura	k1gFnSc6
jako	jako	k8xC,k8xS
určitá	určitý	k2eAgFnSc1d1
předzvěst	předzvěst	k1gFnSc1
<g/>
,	,	kIx,
varování	varování	k1gNnSc1
před	před	k7c7
nebezpečím	nebezpečí	k1gNnSc7
<g/>
,	,	kIx,
upozornění	upozornění	k1gNnSc4
na	na	k7c4
věštecké	věštecký	k2eAgInPc4d1
sny	sen	k1gInPc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
z	z	k7c2
okolí	okolí	k1gNnPc2
psychotroniků	psychotronik	k1gMnPc2
popisují	popisovat	k5eAaImIp3nP
tuto	tento	k3xDgFnSc4
schopnost	schopnost	k1gFnSc4
jako	jako	k8xS,k8xC
určitý	určitý	k2eAgInSc4d1
minimální	minimální	k2eAgInSc4d1
náhled	náhled	k1gInSc4
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vypjatých	vypjatý	k2eAgNnPc6d1
životních	životní	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
jsou	být	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
schopni	schopen	k2eAgMnPc1d1
různých	různý	k2eAgInPc2d1
průniků	průnik	k1gInPc2
do	do	k7c2
parapsychologie	parapsychologie	k1gFnSc2
apod.	apod.	kA
a	a	k8xC
projev	projev	k1gInSc4
viděného	viděný	k2eAgMnSc2d1
<g/>
,	,	kIx,
slyšeného	slyšený	k2eAgMnSc2d1
a	a	k8xC
již	již	k6eAd1
zažitého	zažitý	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
jedním	jeden	k4xCgInSc7
z	z	k7c2
průvodních	průvodní	k2eAgInPc2d1
jevů	jev	k1gInPc2
v	v	k7c6
těchto	tento	k3xDgInPc6
stavech	stav	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
teorií	teorie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
popisují	popisovat	k5eAaImIp3nP
vznik	vznik	k1gInSc4
tohoto	tento	k3xDgInSc2
pocitu	pocit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
nachází	nacházet	k5eAaImIp3nS
příčinu	příčina	k1gFnSc4
v	v	k7c6
časovém	časový	k2eAgNnSc6d1
sladění	sladění	k1gNnSc6
mezi	mezi	k7c7
levou	levý	k2eAgFnSc7d1
a	a	k8xC
pravou	pravý	k2eAgFnSc7d1
hemisférou	hemisféra	k1gFnSc7
našeho	náš	k3xOp1gInSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
vnímá	vnímat	k5eAaImIp3nS
a	a	k8xC
zaznamenává	zaznamenávat	k5eAaImIp3nS
informace	informace	k1gFnPc4
a	a	k8xC
události	událost	k1gFnPc4
nezávisle	závisle	k6eNd1
na	na	k7c6
té	ten	k3xDgFnSc6
druhé	druhý	k4xOgFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustálá	neustálý	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
stranami	strana	k1gFnPc7
pak	pak	k8xC
vyvolává	vyvolávat	k5eAaImIp3nS
iluzi	iluze	k1gFnSc4
jejich	jejich	k3xOp3gFnSc2
jednoty	jednota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
však	však	k9
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
krátkému	krátké	k1gNnSc3
zpoždění	zpoždění	k1gNnSc2
přenosu	přenos	k1gInSc2
z	z	k7c2
„	„	k?
<g/>
nedominantní	dominantní	k2eNgNnSc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
pravé	pravý	k2eAgInPc1d1
<g/>
)	)	kIx)
do	do	k7c2
„	„	k?
<g/>
dominantní	dominantní	k2eAgFnSc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
levé	levý	k2eAgFnSc2d1
<g/>
)	)	kIx)
hemisféry	hemisféra	k1gFnSc2
<g/>
,	,	kIx,
obdrží	obdržet	k5eAaPmIp3nS
dominantní	dominantní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
mozku	mozek	k1gInSc2
stejnou	stejný	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
dvakrát	dvakrát	k6eAd1
–	–	k?
jednou	jednou	k6eAd1
přímo	přímo	k6eAd1
a	a	k8xC
jednou	jednou	k6eAd1
s	s	k7c7
určitým	určitý	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mozek	mozek	k1gInSc1
pak	pak	k6eAd1
v	v	k7c6
momentě	moment	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
„	„	k?
<g/>
dorazí	dorazit	k5eAaPmIp3nS
<g/>
“	“	k?
druhý	druhý	k4xOgInSc4
obraz	obraz	k1gInSc4
<g/>
,	,	kIx,
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
tam	tam	k6eAd1
už	už	k6eAd1
byl	být	k5eAaImAgMnS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedokáže	dokázat	k5eNaPmIp3nS
totiž	totiž	k9
rozlišit	rozlišit	k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
ta	ten	k3xDgFnSc1
„	„	k?
<g/>
první	první	k4xOgFnSc1
<g/>
“	“	k?
informace	informace	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
před	před	k7c7
půl	půl	k6eAd1
sekundou	sekunda	k1gFnSc7
nebo	nebo	k8xC
před	před	k7c7
rokem	rok	k1gInSc7
a	a	k8xC
tím	ten	k3xDgNnSc7
vyvolá	vyvolat	k5eAaPmIp3nS
dojem	dojem	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
danou	daný	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
jsme	být	k5eAaImIp1nP
již	již	k6eAd1
jednou	jednou	k6eAd1
prožili	prožít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
jev	jev	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsoben	způsobit	k5eAaPmNgInS
únavou	únava	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Podle	podle	k7c2
nejnovější	nový	k2eAgFnSc2d3
studie	studie	k1gFnSc2
Milana	Milana	k1gFnSc1
Brázdila	brázdit	k5eAaImAgFnS
<g/>
,	,	kIx,
oceněné	oceněný	k2eAgFnPc4d1
cenou	cena	k1gFnSc7
Johanna	Johann	k1gInSc2
Gregora	Gregor	k1gMnSc2
Mendela	Mendel	k1gMnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
déjà	déjà	k?
vu	vu	k?
neurologický	urologický	k2eNgInSc4d1,k2eAgInSc4d1
původ	původ	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
určitými	určitý	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
pocit	pocit	k1gInSc4
často	často	k6eAd1
zažívají	zažívat	k5eAaImIp3nP
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
menší	malý	k2eAgFnSc4d2
oblast	oblast	k1gFnSc4
zodpovědnou	zodpovědný	k2eAgFnSc4d1
za	za	k7c4
paměť	paměť	k1gFnSc4
a	a	k8xC
uchovávání	uchovávání	k1gNnSc4
vzpomínek	vzpomínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvisí	souviset	k5eAaImIp3nS
též	též	k9
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
dráždivostí	dráždivost	k1gFnSc7
hipokampu	hipokamp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Brázdila	brázdit	k5eAaImAgFnS
pak	pak	k6eAd1
pocit	pocit	k1gInSc4
častěji	často	k6eAd2
zažívají	zažívat	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
s	s	k7c7
vyšším	vysoký	k2eAgNnSc7d2
vzděláním	vzdělání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobnost	podobnost	k1gFnSc1
s	s	k7c7
jamais	jamais	k1gFnSc7
vu	vu	k?
</s>
<s>
Z	z	k7c2
určitého	určitý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
lze	lze	k6eAd1
jamais	jamais	k1gFnSc2
vu	vu	k?
chápat	chápat	k5eAaImF
jako	jako	k8xC,k8xS
přímý	přímý	k2eAgInSc1d1
opak	opak	k1gInSc1
déjà	déjà	k?
vu	vu	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
dva	dva	k4xCgInPc1
jevy	jev	k1gInPc1
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
velice	velice	k6eAd1
podobné	podobný	k2eAgInPc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
zaměňují	zaměňovat	k5eAaImIp3nP
pocit	pocit	k1gInSc4
a	a	k8xC
rozumovou	rozumový	k2eAgFnSc4d1
jistotu	jistota	k1gFnSc4
<g/>
:	:	kIx,
U	u	k7c2
déjà	déjà	k?
vu	vu	k?
má	mít	k5eAaImIp3nS
pozorovatel	pozorovatel	k1gMnSc1
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
už	už	k9
situaci	situace	k1gFnSc4
někdy	někdy	k6eAd1
viděl	vidět	k5eAaImAgMnS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
rozumově	rozumově	k6eAd1
nemůže	moct	k5eNaImIp3nS
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
opravdu	opravdu	k6eAd1
viděl	vidět	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jamais	jamais	k1gFnSc2
vu	vu	k?
má	mít	k5eAaImIp3nS
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
situaci	situace	k1gFnSc4
nikdy	nikdy	k6eAd1
neviděl	vidět	k5eNaImAgMnS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
rozumově	rozumově	k6eAd1
může	moct	k5eAaImIp3nS
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
opravdu	opravdu	k6eAd1
viděl	vidět	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Podobnost	podobnost	k1gFnSc1
s	s	k7c7
presque	presque	k1gFnSc7
vu	vu	k?
</s>
<s>
Presque	Presque	k1gFnSc1
vu	vu	k?
sdílí	sdílet	k5eAaImIp3nS
s	s	k7c7
déjà	déjà	k?
vu	vu	k?
spojení	spojení	k1gNnSc2
s	s	k7c7
minulostí	minulost	k1gFnSc7
jako	jako	k8xS,k8xC
se	s	k7c7
zdrojem	zdroj	k1gInSc7
zapomenutých	zapomenutý	k2eAgFnPc2d1
vzpomínek	vzpomínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
déjà	déjà	k?
vu	vu	k?
jsou	být	k5eAaImIp3nP
však	však	k9
vjemy	vjem	k1gInPc4
spojené	spojený	k2eAgInPc4d1
se	s	k7c7
vzpomínkou	vzpomínka	k1gFnSc7
znovu	znovu	k6eAd1
navozeny	navozit	k5eAaBmNgFnP,k5eAaPmNgFnP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
presque	presqu	k1gFnSc2
vu	vu	k?
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Déjà	Déjà	k1gFnSc2
vu	vu	k?
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Institut	institut	k1gInSc1
výzkumu	výzkum	k1gInSc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
ivdmr	ivdmr	k1gInSc1
<g/>
.	.	kIx.
<g/>
fss	fss	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Déjà	Déjà	k1gMnSc1
vu	vu	k?
začal	začít	k5eAaPmAgMnS
zkoumat	zkoumat	k5eAaImF
kvůli	kvůli	k7c3
epilepsii	epilepsie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
unikátní	unikátní	k2eAgFnSc3d1
studii	studie	k1gFnSc3
<g/>
↑	↑	k?
Déja	Déj	k1gInSc2
vu	vu	k?
je	být	k5eAaImIp3nS
ovlivněno	ovlivnit	k5eAaPmNgNnS
strukturami	struktura	k1gFnPc7
v	v	k7c6
mozku	mozek	k1gInSc6
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jamais	Jamais	k1gFnSc1
vu	vu	k?
</s>
<s>
Presque	Presque	k1gFnSc1
vu	vu	k?
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
esprit	esprit	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
escalier	escalier	k1gMnSc1
</s>
<s>
Paramnesie	paramnesie	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOLEKTIV	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
sv	sv	kA
II	II	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Kapitola	kapitola	k1gFnSc1
heslo	heslo	k1gNnSc1
Déja	Déjus	k1gMnSc2
vu	vu	k?
<g/>
,	,	kIx,
s.	s.	k?
1453	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VONDRÁČEK	Vondráček	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fantastické	fantastický	k2eAgInPc1d1
a	a	k8xC
magické	magický	k2eAgInPc1d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
psychiatrie	psychiatrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Columbus	Columbus	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Déjà	Déjà	k1gMnSc2
vu	vu	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
déjà	déjà	k?
vu	vu	k?
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Vědci	vědec	k1gMnPc1
z	z	k7c2
Brna	Brno	k1gNnSc2
vyřešili	vyřešit	k5eAaPmAgMnP
fenomén	fenomén	k1gMnSc1
déjà	déjà	k?
vu	vu	k?
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Psychologie	psychologie	k1gFnSc1
</s>
