<s>
Split	Split	k1gInSc1	Split
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Spalato	Spalat	k2eAgNnSc1d1	Spalato
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
na	na	k7c6	na
chorvatském	chorvatský	k2eAgNnSc6d1	Chorvatské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
210	[number]	k4	210
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
správní	správní	k2eAgNnSc4d1	správní
středisko	středisko	k1gNnSc4	středisko
Splitsko-dalmatské	splitskoalmatský	k2eAgFnSc2d1	splitsko-dalmatský
župy	župa	k1gFnSc2	župa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
Diokleciánem	Dioklecián	k1gMnSc7	Dioklecián
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
postavil	postavit	k5eAaPmAgMnS	postavit
svůj	svůj	k3xOyFgInSc4	svůj
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
okolní	okolní	k2eAgMnPc1d1	okolní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Slovanů	Slovan	k1gInPc2	Slovan
do	do	k7c2	do
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
Split	Split	k1gInSc1	Split
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Byzance	Byzanc	k1gFnSc2	Byzanc
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
zbývající	zbývající	k2eAgFnSc2d1	zbývající
latinské	latinský	k2eAgFnSc2d1	Latinská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
potomkům	potomek	k1gMnPc3	potomek
římských	římský	k2eAgMnPc2d1	římský
měšťanů	měšťan	k1gMnPc2	měšťan
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
říkali	říkat	k5eAaImAgMnP	říkat
Latini	Latin	k1gMnPc1	Latin
<g/>
.	.	kIx.	.
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
postupně	postupně	k6eAd1	postupně
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
o	o	k7c4	o
strategicky	strategicky	k6eAd1	strategicky
významné	významný	k2eAgNnSc4d1	významné
město	město	k1gNnSc4	město
usilovala	usilovat	k5eAaImAgFnS	usilovat
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1069	[number]	k4	1069
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
Chorvatského	chorvatský	k2eAgNnSc2d1	Chorvatské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
později	pozdě	k6eAd2	pozdě
připadlo	připadnout	k5eAaPmAgNnS	připadnout
k	k	k7c3	k
Uhrám	Uhry	k1gFnPc3	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
zde	zde	k6eAd1	zde
moc	moc	k6eAd1	moc
získala	získat	k5eAaPmAgFnS	získat
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
300	[number]	k4	300
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
humanistická	humanistický	k2eAgFnSc1d1	humanistická
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
až	až	k9	až
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
Království	království	k1gNnSc2	království
SHS	SHS	kA	SHS
spadal	spadat	k5eAaImAgMnS	spadat
pod	pod	k7c4	pod
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Dalmatské	dalmatský	k2eAgFnSc2d1	dalmatská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
město	město	k1gNnSc1	město
centrem	centrum	k1gNnSc7	centrum
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
bánoviny	bánovina	k1gFnSc2	bánovina
Primorje	Primorj	k1gFnSc2	Primorj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
obsazen	obsazen	k2eAgInSc1d1	obsazen
italským	italský	k2eAgNnSc7d1	italské
fašistickým	fašistický	k2eAgNnSc7d1	fašistické
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Split	Split	k1gInSc1	Split
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rijekou	Rijeka	k1gFnSc7	Rijeka
a	a	k8xC	a
Zadarem	Zadar	k1gInSc7	Zadar
hlavním	hlavní	k2eAgInSc7d1	hlavní
chorvatským	chorvatský	k2eAgInSc7d1	chorvatský
přístavem	přístav	k1gInSc7	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
Splitu	Split	k1gInSc2	Split
do	do	k7c2	do
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
nová	nový	k2eAgFnSc1d1	nová
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
je	být	k5eAaImIp3nS	být
dalmatské	dalmatský	k2eAgNnSc1d1	dalmatské
pobřeží	pobřeží	k1gNnSc1	pobřeží
,	,	kIx,	,
také	také	k9	také
zde	zde	k6eAd1	zde
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
Jadranská	jadranský	k2eAgFnSc1d1	Jadranská
Magistrála	magistrála	k1gFnSc1	magistrála
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
dostupnější	dostupný	k2eAgInSc1d2	dostupnější
<g/>
.	.	kIx.	.
<g/>
Split	Split	k1gInSc1	Split
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
železnici	železnice	k1gFnSc4	železnice
<g/>
,	,	kIx,	,
<g/>
jezdí	jezdit	k5eAaImIp3nS	jezdit
zde	zde	k6eAd1	zde
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
přímý	přímý	k2eAgInSc4d1	přímý
vlak	vlak	k1gInSc4	vlak
ze	z	k7c2	z
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
Diokleciánova	Diokleciánův	k2eAgInSc2d1	Diokleciánův
paláce	palác	k1gInSc2	palác
jsou	být	k5eAaImIp3nP	být
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
<g/>
,	,	kIx,	,
zařazeným	zařazený	k2eAgNnSc7d1	zařazené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgFnPc1d3	nejzajímavější
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
klenby	klenba	k1gFnPc1	klenba
a	a	k8xC	a
chodby	chodba	k1gFnPc1	chodba
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Splitu	Split	k1gInSc2	Split
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
moře	moře	k1gNnSc2	moře
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Split	Split	k1gInSc1	Split
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Split	Split	k1gInSc4	Split
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Splitu	Split	k1gInSc2	Split
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Split	Split	k1gInSc1	Split
<g/>
.	.	kIx.	.
<g/>
iNFO	iNFO	k?	iNFO
-	-	kIx~	-
Stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Splitu	Split	k1gInSc2	Split
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historical	Historical	k1gFnSc1	Historical
Complex	Complex	k1gInSc1	Complex
of	of	k?	of
Split	Split	k1gInSc1	Split
with	with	k1gInSc1	with
the	the	k?	the
Palace	Palace	k1gFnSc2	Palace
of	of	k?	of
Diocletian	Diocletian	k1gMnSc1	Diocletian
(	(	kIx(	(
<g/>
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Split	Split	k1gInSc1	Split
-	-	kIx~	-
podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
