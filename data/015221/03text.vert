<s>
Michal	Michal	k1gMnSc1
Huvar	Huvar	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
celkově	celkově	k6eAd1
wikifikovat	wikifikovat	k5eAaImF,k5eAaPmF,k5eAaBmF
<g/>
,	,	kIx,
-promo	-promo	k6eAd1
styl	styl	k1gInSc1
a	a	k8xC
hodnocení	hodnocení	k1gNnSc1
<g/>
,	,	kIx,
WP	WP	kA
<g/>
:	:	kIx,
<g/>
NPOV	NPOV	kA
<g/>
,	,	kIx,
na	na	k7c6
nej	nej	k?
a	a	k8xC
prvenství	prvenství	k1gNnSc1
vždy	vždy	k6eAd1
zdroje	zdroj	k1gInSc2
a	a	k8xC
reference	reference	k1gFnSc2
</s>
<s>
Michal	Michal	k1gMnSc1
Huvar	Huvar	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
61	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Hustopeče	Hustopeč	k1gFnPc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Povolání	povolání	k1gNnSc6
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Michal	Michal	k1gMnSc1
Huvar	Huvar	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1960	#num#	k4
Hustopeče	Hustopeč	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
nakladatel	nakladatel	k1gMnSc1
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
<g/>
,	,	kIx,
filmař	filmař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
založil	založit	k5eAaPmAgMnS
nakladatelství	nakladatelství	k1gNnSc4
Carpe	Carp	k1gInSc5
diem	diem	k6eAd1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
i	i	k9
vydavatelství	vydavatelství	k1gNnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
filmovou	filmový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpopulárnější	populární	k2eAgFnSc1d3
část	část	k1gFnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
literárního	literární	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
představují	představovat	k5eAaImIp3nP
knihy	kniha	k1gFnPc4
o	o	k7c6
českých	český	k2eAgMnPc6d1
písničkářích	písničkář	k1gMnPc6
<g/>
:	:	kIx,
Pavlu	Pavel	k1gMnSc3
Žalmanovi	Žalman	k1gMnSc3
Lohonkovi	Lohonek	k1gMnSc3
a	a	k8xC
Karlu	Karel	k1gMnSc3
Krylovi	Kryl	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filmové	filmový	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
koncertních	koncertní	k2eAgNnPc2d1
vystoupení	vystoupení	k1gNnSc2
zahraničních	zahraniční	k2eAgMnPc2d1
i	i	k8xC
tuzemských	tuzemský	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
promítají	promítat	k5eAaImIp3nP
kina	kino	k1gNnPc4
a	a	k8xC
vysílají	vysílat	k5eAaImIp3nP
televize	televize	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sféře	sféra	k1gFnSc6
cestovatelství	cestovatelství	k1gNnSc2
fotografuje	fotografovat	k5eAaImIp3nS
a	a	k8xC
natáčí	natáčet	k5eAaImIp3nS
na	na	k7c6
místech	místo	k1gNnPc6
ve	v	k7c6
stopách	stopa	k1gFnPc6
Hanzelky	Hanzelka	k1gMnSc2
a	a	k8xC
Zikmunda	Zikmund	k1gMnSc2
(	(	kIx(
<g/>
Afrikou	Afrika	k1gFnSc7
na	na	k7c4
dohled	dohled	k1gInSc4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
cestopisů	cestopis	k1gInPc2
Karla	Karel	k1gMnSc2
Čapka	Čapek	k1gMnSc2
(	(	kIx(
<g/>
film	film	k1gInSc1
Obrázky	obrázek	k1gInPc7
z	z	k7c2
Holandska	Holandsko	k1gNnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
ZŠ	ZŠ	kA
v	v	k7c6
Brumovicích	Brumovice	k1gFnPc6
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Kobylí	kobylí	k2eAgFnSc6d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
vystudoval	vystudovat	k5eAaPmAgInS
Střední	střední	k2eAgFnSc4d1
průmyslovou	průmyslový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
železniční	železniční	k2eAgFnSc4d1
v	v	k7c6
Břeclavi	Břeclav	k1gFnSc6
(	(	kIx(
<g/>
maturita	maturita	k1gFnSc1
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
na	na	k7c6
Stavební	stavební	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
pomocný	pomocný	k2eAgMnSc1d1
dělník	dělník	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1980	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
vystudoval	vystudovat	k5eAaPmAgInS
Pedagogickou	pedagogický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
obor	obor	k1gInSc4
Učitelství	učitelství	k1gNnSc2
všeobecně	všeobecně	k6eAd1
vzdělávacích	vzdělávací	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
<g/>
–	–	k?
<g/>
občanská	občanský	k2eAgFnSc1d1
nauka	nauka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
titul	titul	k1gInSc4
Mgr.	Mgr.	kA
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1985	#num#	k4
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c6
ZŠ	ZŠ	kA
ve	v	k7c6
Velkých	velký	k2eAgFnPc6d1
Pavlovicích	Pavlovice	k1gFnPc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
rok	rok	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
ve	v	k7c6
Valašském	valašský	k2eAgNnSc6d1
Meziříčí	Meziříčí	k1gNnSc6
na	na	k7c6
středním	střední	k2eAgNnSc6d1
vojenském	vojenský	k2eAgNnSc6d1
učilišti	učiliště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
vojny	vojna	k1gFnSc2
působil	působit	k5eAaImAgMnS
1986	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
na	na	k7c6
ZŠ	ZŠ	kA
v	v	k7c6
Kloboukách	Klobouky	k1gInPc6
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1987	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
byl	být	k5eAaImAgInS
středoškolským	středoškolský	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
na	na	k7c6
Gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Břeclavi	Břeclav	k1gFnSc6
a	a	k8xC
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
24	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
následně	následně	k6eAd1
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c6
Gymnáziu	gymnázium	k1gNnSc6
ve	v	k7c6
Velkých	velký	k2eAgFnPc6d1
Pavlovicích	Pavlovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
je	být	k5eAaImIp3nS
ve	v	k7c6
svobodném	svobodný	k2eAgNnSc6d1
povolání	povolání	k1gNnSc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
jako	jako	k9
žurnalista	žurnalista	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
předními	přední	k2eAgInPc7d1
deníky	deník	k1gInPc7
a	a	k8xC
časopisy	časopis	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c4
1250	#num#	k4
příspěvků	příspěvek	k1gInPc2
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
médiích	médium	k1gNnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
jako	jako	k9
nakladatel	nakladatel	k1gMnSc1
<g/>
,	,	kIx,
vydavatel	vydavatel	k1gMnSc1
(	(	kIx(
<g/>
knižní	knižní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
Carpe	Carp	k1gInSc5
diem	diem	k1gInSc4
založeno	založit	k5eAaPmNgNnS
1997	#num#	k4
<g/>
,	,	kIx,
hudební	hudební	k2eAgNnSc4d1
vydavatelství	vydavatelství	k1gNnSc4
Carpe	Carp	k1gInSc5
Diem	Diem	k1gInSc1
Records	Records	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
filmový	filmový	k2eAgMnSc1d1
dokumentarista	dokumentarista	k1gMnSc1
a	a	k8xC
producent	producent	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
zakládá	zakládat	k5eAaImIp3nS
filmovou	filmový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Carpe	Carp	k1gInSc5
Diem	Diem	k1gInPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumentární	dokumentární	k2eAgInPc4d1
filmy	film	k1gInPc4
odvysílala	odvysílat	k5eAaPmAgFnS
TV	TV	kA
Noe	Noe	k1gMnSc1
<g/>
,	,	kIx,
STV	STV	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
koncerty	koncert	k1gInPc4
ČT	ČT	kA
Art	Art	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
6	#num#	k4
knih	kniha	k1gFnPc2
a	a	k8xC
editorem	editor	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
390	#num#	k4
knižních	knižní	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
filmu	film	k1gInSc3
<g/>
,	,	kIx,
scénářům	scénář	k1gInPc3
<g/>
,	,	kIx,
kameře	kamera	k1gFnSc3
a	a	k8xC
režii	režie	k1gFnSc6
<g/>
,	,	kIx,
ho	on	k3xPp3gMnSc4
přivedly	přivést	k5eAaPmAgFnP
knihy	kniha	k1gFnPc1
a	a	k8xC
k	k	k7c3
nim	on	k3xPp3gFnPc3
speciálně	speciálně	k6eAd1
natáčené	natáčený	k2eAgFnPc1d1
a	a	k8xC
na	na	k7c6
DVD	DVD	kA
vydávané	vydávaný	k2eAgInPc4d1
přílohy	příloh	k1gInPc4
<g/>
,	,	kIx,
ponejvíce	ponejvíce	k6eAd1
adaptace	adaptace	k1gFnSc1
knih	kniha	k1gFnPc2
a	a	k8xC
záznamy	záznam	k1gInPc4
hudebních	hudební	k2eAgFnPc2d1
produkcí	produkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezřídka	nezřídka	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc2
podílel	podílet	k5eAaImAgMnS
i	i	k9
jako	jako	k9
kameraman	kameraman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
řadou	řada	k1gFnSc7
osobností	osobnost	k1gFnSc7
uměleckého	umělecký	k2eAgInSc2d1
světa	svět	k1gInSc2
/	/	kIx~
<g/>
Jana	Jana	k1gFnSc1
Paulová	Paulová	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Merta	Merta	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Kovářík	kovářík	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Štvrtecká	Štvrtecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Frídl	Frídl	k1gMnSc1
<g/>
,	,	kIx,
Gabriela	Gabriela	k1gFnSc1
Vránová	Vránová	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Derfler	Derfler	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Chudík	Chudík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Lakomý	Lakomý	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Moravec	Moravec	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Slávek	Slávek	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
<g/>
,	,	kIx,
Pepa	Pepa	k1gMnSc1
Nos	nos	k1gInSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
J.	J.	kA
Ryba	Ryba	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Šůcha	Šůcha	k1gMnSc1
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
Bittová	Bittová	k1gFnSc1
<g/>
,	,	kIx,
Damien	Damien	k2eAgMnSc1d1
Riba	Riba	k1gMnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dean	Dean	k1gMnSc1
Brown	Brown	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Suzanne	Suzann	k1gInSc5
Vega	Vega	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dominic	Dominice	k1gFnPc2
Miller	Miller	k1gMnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gerald	Gerald	k1gMnSc1
Clark	Clark	k1gInSc1
(	(	kIx(
<g/>
JAR	jaro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sarah	Sarah	k1gFnSc1
Tolar	tolar	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Kocáb	Kocáb	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Pavlíček	Pavlíček	k1gMnSc1
aj.	aj.	kA
<g/>
/	/	kIx~
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1997	#num#	k4
České	český	k2eAgFnPc1d1
uznání	uznání	k1gNnSc4
Obce	obec	k1gFnSc2
spisovatelů	spisovatel	k1gMnPc2
–	–	k?
spoludržitel	spoludržitel	k1gMnSc1
ceny	cena	k1gFnSc2
za	za	k7c4
činnost	činnost	k1gFnSc4
v	v	k7c6
divadle	divadlo	k1gNnSc6
poezie	poezie	k1gFnSc2
Regina	Regina	k1gFnSc1
Břeclav	Břeclav	k1gFnSc1
</s>
<s>
2006	#num#	k4
La	la	k1gNnPc1
Caméra	Camér	k1gMnSc2
d	d	k?
<g/>
‘	‘	k?
Or	Or	k1gFnSc7
2006	#num#	k4
–	–	k?
za	za	k7c4
filmový	filmový	k2eAgInSc4d1
dokument	dokument	k1gInSc4
Krajina	Krajina	k1gFnSc1
révového	révový	k2eAgInSc2d1
listu	list	k1gInSc2
<g/>
,	,	kIx,
uděleno	udělit	k5eAaPmNgNnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
2007	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
de	de	k?
la	la	k1gNnSc7
Ville	Ville	k1gFnSc2
de	de	k?
Mougins	Mougins	k1gInSc1
–	–	k?
hlavní	hlavní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
poroty	porota	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
filmového	filmový	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Festicam	Festicam	k1gInSc1
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Mougins	Mougins	k1gInSc1
za	za	k7c4
dokument	dokument	k1gInSc4
Krajina	Krajina	k1gFnSc1
révového	révový	k2eAgInSc2d1
listu	list	k1gInSc2
</s>
<s>
2007	#num#	k4
Hlavní	hlavní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
–	–	k?
hlavní	hlavní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
6	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
filmového	filmový	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Festicam	Festicam	k1gInSc1
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Mougins	Mougins	k1gInSc1
za	za	k7c4
dokument	dokument	k1gInSc4
Krajina	Krajina	k1gFnSc1
révového	révový	k2eAgInSc2d1
listu	list	k1gInSc2
</s>
<s>
Tvorba	tvorba	k1gFnSc1
</s>
<s>
1997	#num#	k4
tvůrce	tvůrce	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
prvního	první	k4xOgInSc2
českého	český	k2eAgInSc2d1
cestopisného	cestopisný	k2eAgInSc2d1
CD-ROMu	CD-ROMus	k1gInSc2
Afrika	Afrika	k1gFnSc1
50	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
stopách	stopa	k1gFnPc6
Hanzelky	Hanzelka	k1gMnSc2
a	a	k8xC
Zikmunda	Zikmund	k1gMnSc2
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
-	-	kIx~
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
autor	autor	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
první	první	k4xOgFnSc2
české	český	k2eAgFnSc2d1
divadelní	divadelní	k2eAgFnSc2d1
monografie	monografie	k1gFnSc2
doplněné	doplněná	k1gFnSc2
o	o	k7c6
CD	CD	kA
a	a	k8xC
DVD	DVD	kA
–	–	k?
Knížka	knížka	k1gFnSc1
o	o	k7c6
Regině	Regina	k1gFnSc6
</s>
<s>
2005	#num#	k4
producent	producent	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
prvního	první	k4xOgMnSc4
tuzemského	tuzemský	k2eAgMnSc4d1
dvojitého	dvojitý	k2eAgMnSc4d1
nosiče	nosič	k1gMnSc4
(	(	kIx(
<g/>
CD	CD	kA
<g/>
+	+	kIx~
<g/>
DVD	DVD	kA
<g/>
)	)	kIx)
–	–	k?
Bob	Bob	k1gMnSc1
Frídl	Frídl	k1gMnSc1
<g/>
:	:	kIx,
Sto	sto	k4xCgNnSc4
patnáct	patnáct	k4xCc4
havranů	havran	k1gMnPc2
</s>
<s>
2006	#num#	k4
tvůrce	tvůrce	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
první	první	k4xOgFnSc2
české	český	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
poezie	poezie	k1gFnSc2
doplněné	doplněná	k1gFnSc2
o	o	k7c4
její	její	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
jak	jak	k6eAd1
zvukové	zvukový	k2eAgFnSc2d1
(	(	kIx(
<g/>
CD	CD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
obrazové	obrazový	k2eAgInPc1d1
(	(	kIx(
<g/>
DVD	DVD	kA
<g/>
)	)	kIx)
–	–	k?
Petr	Petr	k1gMnSc1
Pichl	Pichl	k1gMnSc1
<g/>
:	:	kIx,
Tahle	tenhle	k3xDgFnSc1
kniha	kniha	k1gFnSc1
</s>
<s>
2011	#num#	k4
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
vydavatel	vydavatel	k1gMnSc1
publikující	publikující	k2eAgMnSc1d1
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
kompletní	kompletní	k2eAgInPc1d1
Spisy	spis	k1gInPc1
–	–	k?
František	František	k1gMnSc1
Gellner	Gellner	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
<g/>
-III	-III	k?
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
autor	autor	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
první	první	k4xOgFnSc2
české	český	k2eAgFnSc2d1
elektronické	elektronický	k2eAgFnSc2d1
interaktivní	interaktivní	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
–	–	k?
Afrikou	Afrika	k1gFnSc7
na	na	k7c4
dohled	dohled	k1gInSc4
(	(	kIx(
<g/>
vyšla	vyjít	k5eAaPmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
otevírá	otevírat	k5eAaImIp3nS
jako	jako	k9
první	první	k4xOgMnPc1
v	v	k7c6
ČR	ČR	kA
prodejní	prodejní	k2eAgInSc1d1
portál	portál	k1gInSc1
s	s	k7c7
digitálními	digitální	k2eAgInPc7d1
koncertními	koncertní	k2eAgInPc7d1
záznamy	záznam	k1gInPc7
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
–	–	k?
iKoncert	iKoncert	k1gInSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
</s>
<s>
2012	#num#	k4
autor	autor	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
prvního	první	k4xOgInSc2
českého	český	k2eAgInSc2d1
filmu	film	k1gInSc2
v	v	k7c6
Applu	Appl	k1gInSc6
(	(	kIx(
<g/>
Obrázky	obrázek	k1gInPc7
z	z	k7c2
Holandska	Holandsko	k1gNnSc2
/	/	kIx~
<g/>
zveřejněno	zveřejnit	k5eAaPmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
autor	autor	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
prvního	první	k4xOgInSc2
českého	český	k2eAgInSc2d1
filmu	film	k1gInSc2
s	s	k7c7
koncertní	koncertní	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
v	v	k7c6
Applu	Applo	k1gNnSc6
(	(	kIx(
<g/>
René	René	k1gFnSc6
Lacko	Lacko	k1gNnSc4
<g/>
/	/	kIx~
<g/>
Live	Live	k1gInSc1
in	in	k?
Bratislava	Bratislava	k1gFnSc1
/	/	kIx~
<g/>
zveřejněno	zveřejnit	k5eAaPmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
autor	autor	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
prvního	první	k4xOgInSc2
českého	český	k2eAgNnSc2d1
pouze	pouze	k6eAd1
digitálně	digitálně	k6eAd1
vydaného	vydaný	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
koncertu	koncert	k1gInSc2
(	(	kIx(
<g/>
Majerovky	Majerovka	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Live	Live	k1gInSc1
at	at	k?
Kaštan	kaštan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
českým	český	k2eAgMnSc7d1
režisérem	režisér	k1gMnSc7
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
záznam	záznam	k1gInSc1
výhradně	výhradně	k6eAd1
koncertní	koncertní	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
české	český	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
(	(	kIx(
<g/>
Laura	Laura	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
tygři	tygr	k1gMnPc1
<g/>
/	/	kIx~
<g/>
Big	Big	k1gMnSc1
Bang	Bang	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
;	;	kIx,
snímek	snímek	k1gInSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
i	i	k9
jako	jako	k9
první	první	k4xOgFnSc1
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
do	do	k7c2
seznamu	seznam	k1gInSc2
filmů	film	k1gInPc2
pro	pro	k7c4
České	český	k2eAgInPc4d1
lvy	lev	k1gInPc4
2014	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Michal	Michal	k1gMnSc1
Huvar	Huvar	k1gMnSc1
v	v	k7c6
Databázi	databáze	k1gFnSc6
knih	kniha	k1gFnPc2
</s>
<s>
Michal	Michal	k1gMnSc1
Huvar	Huvar	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
99240000337	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5521	#num#	k4
6512	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98095019	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61829806	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98095019	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
