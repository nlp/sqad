<s>
Juliet	Juliet	k1gMnSc1	Juliet
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgInSc4	šestý
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
vzdálen	vzdáleno	k1gNnPc2	vzdáleno
64	[number]	k4	64
360	[number]	k4	360
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
42	[number]	k4	42
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
5,6	[number]	k4	5,6
<g/>
×	×	k?	×
<g/>
1017	[number]	k4	1017
kg	kg	kA	kg
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
mu	on	k3xPp3gMnSc3	on
zabere	zabrat	k5eAaPmIp3nS	zabrat
0,493066	[number]	k4	0,493066
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
americkou	americký	k2eAgFnSc7d1	americká
sondou	sonda	k1gFnSc7	sonda
Voyagerem	Voyager	k1gMnSc7	Voyager
2	[number]	k4	2
(	(	kIx(	(
<g/>
Stephen	Stephen	k1gInSc1	Stephen
P.	P.	kA	P.
Synnott	Synnott	k1gInSc1	Synnott
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
