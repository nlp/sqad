<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
severní	severní	k2eAgFnSc2d1	severní
stěny	stěna	k1gFnSc2	stěna
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Svolinského	Svolinský	k2eAgMnSc2d1	Svolinský
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ztvárněna	ztvárnit	k5eAaPmNgFnS	ztvárnit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
heliocentrických	heliocentrický	k2eAgInPc2d1	heliocentrický
orlojů	orloj	k1gInPc2	orloj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výklenek	výklenek	k1gInSc1	výklenek
s	s	k7c7	s
lomeným	lomený	k2eAgInSc7d1	lomený
obloukem	oblouk	k1gInSc7	oblouk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
orloj	orloj	k1gInSc1	orloj
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
téměř	téměř	k6eAd1	téměř
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgInSc1d1	centrální
ciferník	ciferník	k1gInSc1	ciferník
orloje	orloj	k1gInSc2	orloj
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
polohy	poloh	k1gInPc4	poloh
planet	planeta	k1gFnPc2	planeta
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
zvířetníku	zvířetník	k1gInSc2	zvířetník
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
mozaiky	mozaika	k1gFnSc2	mozaika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
jízdu	jízda	k1gFnSc4	jízda
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
dělníka	dělník	k1gMnSc4	dělník
a	a	k8xC	a
chemika	chemik	k1gMnSc4	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
výklenku	výklenek	k1gInSc2	výklenek
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
medailony	medailon	k1gInPc1	medailon
s	s	k7c7	s
alegoriemi	alegorie	k1gFnPc7	alegorie
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kalendárium	kalendárium	k1gNnSc1	kalendárium
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
orloje	orloj	k1gInSc2	orloj
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
dny	den	k1gInPc4	den
významné	významný	k2eAgInPc4d1	významný
pro	pro	k7c4	pro
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
narozeniny	narozeniny	k1gFnPc1	narozeniny
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
Gottwalda	Gottwald	k1gMnSc2	Gottwald
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
pohlavárů	pohlavár	k1gMnPc2	pohlavár
<g/>
.	.	kIx.	.
</s>
<s>
Zvonkohra	zvonkohra	k1gFnSc1	zvonkohra
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
hrát	hrát	k5eAaImF	hrát
Internacionálu	Internacionála	k1gFnSc4	Internacionála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
zvoleny	zvolen	k2eAgFnPc1d1	zvolena
lidové	lidový	k2eAgFnPc1d1	lidová
písně	píseň	k1gFnPc1	píseň
Daleká	daleký	k2eAgFnSc1d1	daleká
<g/>
,	,	kIx,	,
šeroká	šeroký	k2eAgFnSc1d1	šeroká
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Holomóc	Holomóc	k1gFnSc4	Holomóc
<g/>
,	,	kIx,	,
Vrbe	Vrbe	k1gFnSc4	Vrbe
jož	jož	k?	jož
se	s	k7c7	s
zelenajó	zelenajó	k?	zelenajó
a	a	k8xC	a
Za	za	k7c4	za
Náměšťó	Náměšťó	k1gFnSc4	Náměšťó
na	na	k7c4	na
kopečko	kopečko	k1gNnSc4	kopečko
hádajó	hádajó	k?	hádajó
se	se	k3xPyFc4	se
o	o	k7c6	o
děvečko	děvečka	k1gFnSc5	děvečka
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
je	být	k5eAaImIp3nS	být
šestnáct	šestnáct	k4xCc1	šestnáct
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
odlila	odlít	k5eAaPmAgFnS	odlít
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
firma	firma	k1gFnSc1	firma
P.	P.	kA	P.
Hilzer	Hilzer	k1gInSc1	Hilzer
<g/>
.	.	kIx.	.
</s>
<s>
Poledne	poledne	k1gNnSc1	poledne
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
kokrháním	kokrhání	k1gNnSc7	kokrhání
mosazný	mosazný	k2eAgInSc1d1	mosazný
kohout	kohout	k1gInSc1	kohout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
v	v	k7c6	v
oknech	okno	k1gNnPc6	okno
orloje	orloj	k1gInSc2	orloj
po	po	k7c4	po
sedm	sedm	k4xCc4	sedm
minut	minuta	k1gFnPc2	minuta
defilují	defilovat	k5eAaImIp3nP	defilovat
figury	figura	k1gFnPc1	figura
kopáče	kopáč	k1gMnSc2	kopáč
<g/>
,	,	kIx,	,
pekaře	pekař	k1gMnSc2	pekař
<g/>
,	,	kIx,	,
úředníka	úředník	k1gMnSc2	úředník
<g/>
,	,	kIx,	,
volejbalistky	volejbalistka	k1gFnSc2	volejbalistka
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
profesí	profes	k1gFnPc2	profes
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgInS	být
orloj	orloj	k1gInSc1	orloj
postaven	postavit	k5eAaPmNgInS	postavit
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1422	[number]	k4	1422
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výzkumů	výzkum	k1gInPc2	výzkum
byl	být	k5eAaImAgInS	být
však	však	k9	však
prostor	prostor	k1gInSc1	prostor
radnice	radnice	k1gFnSc2	radnice
pro	pro	k7c4	pro
orloj	orloj	k1gInSc4	orloj
upraven	upravit	k5eAaPmNgInS	upravit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1474	[number]	k4	1474
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvůrce	tvůrce	k1gMnSc1	tvůrce
orloje	orloj	k1gInSc2	orloj
byl	být	k5eAaImAgMnS	být
oslepen	oslepen	k2eAgMnSc1d1	oslepen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohl	moct	k5eNaImAgMnS	moct
obdobné	obdobný	k2eAgNnSc4d1	obdobné
dílo	dílo	k1gNnSc4	dílo
vybudovat	vybudovat	k5eAaPmF	vybudovat
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
doloženi	doložen	k2eAgMnPc1d1	doložen
jsou	být	k5eAaImIp3nP	být
hodinář	hodinář	k1gMnSc1	hodinář
Hans	Hans	k1gMnSc1	Hans
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
orloj	orloj	k1gInSc4	orloj
opravil	opravit	k5eAaPmAgMnS	opravit
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Fabricius	Fabricius	k1gMnSc1	Fabricius
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
unikátní	unikátní	k2eAgFnSc2d1	unikátní
planisféry	planisféra	k1gFnSc2	planisféra
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
146	[number]	k4	146
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
několikrát	několikrát	k6eAd1	několikrát
přebudováván	přebudováván	k2eAgInSc1d1	přebudováván
a	a	k8xC	a
vybavován	vybavován	k2eAgInSc1d1	vybavován
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
loutek	loutka	k1gFnPc2	loutka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
s	s	k7c7	s
drakem	drak	k1gMnSc7	drak
<g/>
,	,	kIx,	,
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
na	na	k7c6	na
velbloudech	velbloud	k1gMnPc6	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
místo	místo	k7c2	místo
světců	světec	k1gMnPc2	světec
umístěn	umístit	k5eAaPmNgMnS	umístit
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
s	s	k7c7	s
monstrancí	monstrance	k1gFnSc7	monstrance
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
části	část	k1gFnPc1	část
dnešního	dnešní	k2eAgInSc2d1	dnešní
astronomického	astronomický	k2eAgInSc2d1	astronomický
stroje	stroj	k1gInSc2	stroj
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
orloj	orloj	k1gInSc1	orloj
vybaven	vybavit	k5eAaPmNgInS	vybavit
heliocentrickým	heliocentrický	k2eAgInSc7d1	heliocentrický
ciferníkem	ciferník	k1gInSc7	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
byl	být	k5eAaImAgMnS	být
vyzdoben	vyzdobit	k5eAaPmNgMnS	vyzdobit
významným	významný	k2eAgMnSc7d1	významný
umělcem	umělec	k1gMnSc7	umělec
Jano	Jano	k1gMnSc1	Jano
Kohlerem	Kohler	k1gMnSc7	Kohler
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
byl	být	k5eAaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
granátem	granát	k1gInSc7	granát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
vrácen	vrátit	k5eAaPmNgInS	vrátit
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgMnS	být
kompletně	kompletně	k6eAd1	kompletně
přebudován	přebudovat	k5eAaPmNgMnS	přebudovat
Karlem	Karel	k1gMnSc7	Karel
Svolinským	Svolinský	k2eAgMnSc7d1	Svolinský
<g/>
,	,	kIx,	,
rodákem	rodák	k1gMnSc7	rodák
ze	z	k7c2	z
Svatého	svatý	k2eAgInSc2d1	svatý
Kopečku	kopeček	k1gInSc2	kopeček
na	na	k7c4	na
socialistický	socialistický	k2eAgInSc4d1	socialistický
motiv	motiv	k1gInSc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
uvedení	uvedení	k1gNnSc1	uvedení
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
komunistických	komunistický	k2eAgMnPc2d1	komunistický
pohlavárů	pohlavár	k1gMnPc2	pohlavár
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
radnice	radnice	k1gFnSc1	radnice
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
Orloj	orloj	k1gInSc1	orloj
v	v	k7c6	v
Kryštofově	Kryštofův	k2eAgNnSc6d1	Kryštofovo
Údolí	údolí	k1gNnSc6	údolí
Hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Hodiny	hodina	k1gFnPc1	hodina
Čas	čas	k1gInSc4	čas
Astroláb	Astroláb	k1gInSc1	Astroláb
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
Čermák	Čermák	k1gMnSc1	Čermák
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Memoria	Memorium	k1gNnPc1	Memorium
:	:	kIx,	:
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85807	[number]	k4	85807
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Šimková	Šimková	k1gFnSc1	Šimková
<g/>
,	,	kIx,	,
Anežka	Anežka	k1gFnSc1	Anežka
<g/>
:	:	kIx,	:
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
oddechu	oddech	k1gInSc2	oddech
:	:	kIx,	:
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
16	[number]	k4	16
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900037	[number]	k4	900037
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
orloj	orloj	k1gInSc4	orloj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Velký	velký	k2eAgInSc1d1	velký
web	web	k1gInSc1	web
Pražského	pražský	k2eAgInSc2d1	pražský
orloje	orloj	k1gInSc2	orloj
včetně	včetně	k7c2	včetně
orloje	orloj	k1gInSc2	orloj
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
:	:	kIx,	:
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
simulátory	simulátor	k1gInPc1	simulátor
Dobové	dobový	k2eAgFnSc2d1	dobová
fotografie	fotografia	k1gFnSc2	fotografia
orloje	orloj	k1gInSc2	orloj
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
na	na	k7c6	na
webu	web	k1gInSc6	web
olomouc-tourism	olomoucourisma	k1gFnPc2	olomouc-tourisma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Opravené	opravený	k2eAgFnSc2d1	opravená
figurky	figurka	k1gFnSc2	figurka
se	se	k3xPyFc4	se
příští	příští	k2eAgInSc1d1	příští
týden	týden	k1gInSc1	týden
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
orloj	orloj	k1gInSc4	orloj
<g/>
,	,	kIx,	,
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
restaurátora	restaurátor	k1gMnSc2	restaurátor
omládnou	omládnout	k5eAaPmIp3nP	omládnout
další	další	k2eAgInSc4d1	další
Orloj	orloj	k1gInSc4	orloj
zblízka	zblízka	k6eAd1	zblízka
před	před	k7c7	před
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
figurek	figurka	k1gFnPc2	figurka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Záznam	záznam	k1gInSc1	záznam
zvonkohry	zvonkohra	k1gFnSc2	zvonkohra
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
orloje	orloj	k1gInSc2	orloj
asi	asi	k9	asi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgInS	chtít
bych	by	kYmCp1nS	by
mít	mít	k5eAaImF	mít
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
tužku	tužka	k1gFnSc4	tužka
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
unikátní	unikátní	k2eAgFnPc4d1	unikátní
archivní	archivní	k2eAgFnPc4d1	archivní
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
ukázky	ukázka	k1gFnPc4	ukázka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
okolnosti	okolnost	k1gFnPc1	okolnost
vzniku	vznik	k1gInSc2	vznik
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
orloje	orloj	k1gInSc2	orloj
</s>
