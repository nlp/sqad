<s>
Štíři	štír	k1gMnPc1	štír
(	(	kIx(	(
<g/>
Scorpionida	Scorpionida	k1gFnSc1	Scorpionida
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
řád	řád	k1gInSc4	řád
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
<g/>
,	,	kIx,	,
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
velkými	velký	k2eAgMnPc7d1	velký
<g/>
,	,	kIx,	,
klepetovitými	klepetovitý	k2eAgNnPc7d1	klepetovitý
makadly	makadlo	k1gNnPc7	makadlo
a	a	k8xC	a
zúženým	zúžený	k2eAgInSc7d1	zúžený
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
zadečkem	zadeček	k1gInSc7	zadeček
s	s	k7c7	s
jedovým	jedový	k2eAgInSc7d1	jedový
ostnem	osten	k1gInSc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Štír	štír	k1gMnSc1	štír
přežije	přežít	k5eAaPmIp3nS	přežít
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
člověka	člověk	k1gMnSc4	člověk
zabila	zabít	k5eAaPmAgFnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Vydrží	vydržet	k5eAaPmIp3nS	vydržet
nejíst	jíst	k5eNaImF	jíst
rok	rok	k1gInSc4	rok
a	a	k8xC	a
zadržovat	zadržovat	k5eAaImF	zadržovat
dech	dech	k1gInSc1	dech
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Přežije	přežít	k5eAaPmIp3nS	přežít
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
i	i	k8xC	i
zmrazení	zmrazení	k1gNnPc1	zmrazení
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
byli	být	k5eAaImAgMnP	být
svědkem	svědek	k1gMnSc7	svědek
počátku	počátek	k1gInSc2	počátek
i	i	k8xC	i
konce	konec	k1gInSc2	konec
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
však	však	k9	však
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnPc1d1	moderní
suchozemští	suchozemský	k2eAgMnPc1d1	suchozemský
štíři	štír	k1gMnPc1	štír
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
v	v	k7c6	v
období	období	k1gNnSc6	období
karbonu	karbon	k1gInSc2	karbon
asi	asi	k9	asi
před	před	k7c7	před
300	[number]	k4	300
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
vodní	vodní	k2eAgMnPc1d1	vodní
předchůdci	předchůdce	k1gMnPc1	předchůdce
však	však	k9	však
žili	žít	k5eAaImAgMnP	žít
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
siluru	silur	k1gInSc2	silur
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
surikata	surikata	k1gFnSc1	surikata
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
našla	najít	k5eAaPmAgFnS	najít
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
obranu	obrana	k1gFnSc4	obrana
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
štíra	štír	k1gMnSc2	štír
je	být	k5eAaImIp3nS	být
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
7	[number]	k4	7
článků	článek	k1gInPc2	článek
zadečku	zadeček	k1gInSc2	zadeček
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
trup	trup	k1gInSc1	trup
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
zadečku	zadeček	k1gInSc2	zadeček
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pěti	pět	k4xCc7	pět
články	článek	k1gInPc7	článek
a	a	k8xC	a
telsonem	telson	k1gInSc7	telson
<g/>
,	,	kIx,	,
jedovým	jedový	k2eAgInSc7d1	jedový
ostnem	osten	k1gInSc7	osten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
jedovým	jedový	k2eAgInSc7d1	jedový
hrotem	hrot	k1gInSc7	hrot
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
ústí	ústit	k5eAaImIp3nP	ústit
dvě	dva	k4xCgFnPc4	dva
jedové	jedový	k2eAgFnPc4d1	jedová
žlázy	žláza	k1gFnPc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
zadečku	zadeček	k1gInSc2	zadeček
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nesprávný	správný	k2eNgMnSc1d1	nesprávný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pravý	pravý	k2eAgInSc1d1	pravý
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
za	za	k7c7	za
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
štíra	štír	k1gMnSc2	štír
je	být	k5eAaImIp3nS	být
řitní	řitní	k2eAgInSc4d1	řitní
otvor	otvor	k1gInSc4	otvor
umístěný	umístěný	k2eAgInSc4d1	umístěný
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
těla	tělo	k1gNnSc2	tělo
mezi	mezi	k7c7	mezi
pátým	pátý	k4xOgInSc7	pátý
článkem	článek	k1gInSc7	článek
"	"	kIx"	"
<g/>
ocasu	ocas	k1gInSc2	ocas
<g/>
"	"	kIx"	"
a	a	k8xC	a
telsonem	telson	k1gInSc7	telson
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgInPc4d1	plicní
vaky	vak	k1gInPc4	vak
<g/>
,	,	kIx,	,
trávicí	trávicí	k2eAgFnSc4d1	trávicí
trubici	trubice	k1gFnSc4	trubice
a	a	k8xC	a
rozmnožovací	rozmnožovací	k2eAgInPc4d1	rozmnožovací
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
trupu	trup	k1gMnSc3	trup
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgInP	připojit
čtyři	čtyři	k4xCgInPc1	čtyři
páry	pár	k1gInPc1	pár
kráčivých	kráčivý	k2eAgFnPc2d1	kráčivá
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Zakrnělé	zakrnělý	k2eAgInPc1d1	zakrnělý
zbytky	zbytek	k1gInPc1	zbytek
dalších	další	k2eAgFnPc2d1	další
končetin	končetina	k1gFnPc2	končetina
tvoří	tvořit	k5eAaImIp3nP	tvořit
hřebínky	hřebínek	k1gInPc1	hřebínek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
smyslové	smyslový	k2eAgInPc1d1	smyslový
orgány	orgán	k1gInPc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
nese	nést	k5eAaImIp3nS	nést
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
středových	středový	k2eAgNnPc2d1	středové
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
až	až	k6eAd1	až
pět	pět	k4xCc4	pět
párů	pár	k1gInPc2	pár
dalších	další	k2eAgNnPc2d1	další
postranních	postranní	k2eAgNnPc2d1	postranní
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
klepítka	klepítko	k1gNnSc2	klepítko
(	(	kIx(	(
<g/>
chelicery	chelicera	k1gFnSc2	chelicera
<g/>
)	)	kIx)	)
a	a	k8xC	a
makadla	makadlo	k1gNnSc2	makadlo
(	(	kIx(	(
<g/>
pedipalpy	pedipalpa	k1gFnSc2	pedipalpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
přeměněna	přeměnit	k5eAaPmNgNnP	přeměnit
v	v	k7c4	v
klepeta	klepeto	k1gNnPc4	klepeto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jeskynní	jeskynní	k2eAgInPc1d1	jeskynní
druhy	druh	k1gInPc1	druh
štírů	štír	k1gMnPc2	štír
nemají	mít	k5eNaImIp3nP	mít
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozložení	rozložení	k1gNnSc3	rozložení
očí	oko	k1gNnPc2	oko
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
má	mít	k5eAaImIp3nS	mít
štír	štír	k1gMnSc1	štír
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
prakticky	prakticky	k6eAd1	prakticky
360	[number]	k4	360
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Štír	štír	k1gMnSc1	štír
používá	používat	k5eAaImIp3nS	používat
klepeta	klepeto	k1gNnPc4	klepeto
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
uchvacování	uchvacování	k1gNnSc3	uchvacování
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
<g/>
.	.	kIx.	.
</s>
<s>
Makadla	makadlo	k1gNnPc1	makadlo
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
šesti	šest	k4xCc2	šest
články	článek	k1gInPc7	článek
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
klepeto	klepeto	k1gNnSc1	klepeto
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
horní	horní	k2eAgFnSc7d1	horní
nepohyblivou	pohyblivý	k2eNgFnSc7d1	nepohyblivá
částí	část	k1gFnSc7	část
(	(	kIx(	(
<g/>
tibia	tibia	k1gFnSc1	tibia
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
(	(	kIx(	(
<g/>
tarsus	tarsus	k1gMnSc1	tarsus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
štírů	štír	k1gMnPc2	štír
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
neurotoxin	urotoxin	k2eNgInSc4d1	neurotoxin
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
sodných	sodný	k2eAgInPc2d1	sodný
a	a	k8xC	a
draselných	draselný	k2eAgInPc2d1	draselný
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
narušuje	narušovat	k5eAaImIp3nS	narušovat
přenos	přenos	k1gInSc1	přenos
nervových	nervový	k2eAgInPc2d1	nervový
vzruchů	vzruch	k1gInPc2	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
používají	používat	k5eAaImIp3nP	používat
svůj	svůj	k3xOyFgInSc4	svůj
jed	jed	k1gInSc4	jed
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
nebo	nebo	k8xC	nebo
paralyzování	paralyzování	k1gNnSc3	paralyzování
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
jed	jed	k1gInSc1	jed
proto	proto	k8xC	proto
působí	působit	k5eAaImIp3nS	působit
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
tak	tak	k6eAd1	tak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
štírovi	štírův	k2eAgMnPc1d1	štírův
kořist	kořist	k1gFnSc4	kořist
polapit	polapit	k5eAaPmF	polapit
a	a	k8xC	a
sežrat	sežrat	k5eAaPmF	sežrat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mláďata	mládě	k1gNnPc4	mládě
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
nejsou	být	k5eNaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
probodnout	probodnout	k5eAaPmF	probodnout
kůži	kůže	k1gFnSc3	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
štírů	štír	k1gMnPc2	štír
má	mít	k5eAaImIp3nS	mít
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
účinně	účinně	k6eAd1	účinně
působí	působit	k5eAaImIp3nS	působit
jen	jen	k9	jen
na	na	k7c4	na
jiné	jiný	k2eAgMnPc4d1	jiný
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
neškodný	škodný	k2eNgMnSc1d1	neškodný
<g/>
;	;	kIx,	;
bodnutí	bodnutí	k1gNnSc1	bodnutí
způsobí	způsobit	k5eAaPmIp3nS	způsobit
jen	jen	k9	jen
místní	místní	k2eAgFnSc4d1	místní
reakci	reakce	k1gFnSc4	reakce
(	(	kIx(	(
<g/>
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
otok	otok	k1gInSc1	otok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
štírů	štír	k1gMnPc2	štír
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Buthidae	Buthida	k1gFnSc2	Buthida
<g/>
,	,	kIx,	,
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
i	i	k8xC	i
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
nejsou	být	k5eNaImIp3nP	být
štíři	štír	k1gMnPc1	štír
schopní	schopný	k2eAgMnPc1d1	schopný
vyloučit	vyloučit	k5eAaPmF	vyloučit
při	při	k7c6	při
bodnutí	bodnutí	k1gNnSc6	bodnutí
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
jedu	jed	k1gInSc2	jed
potřebného	potřebné	k1gNnSc2	potřebné
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
zdravého	zdravý	k2eAgMnSc2d1	zdravý
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
bodnutí	bodnutí	k1gNnSc1	bodnutí
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zabít	zabít	k5eAaPmF	zabít
děti	dítě	k1gFnPc4	dítě
či	či	k8xC	či
staré	starý	k2eAgFnPc4d1	stará
nebo	nebo	k8xC	nebo
nemocné	nemocný	k2eAgFnPc4d1	nemocná
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejbolestivější	bolestivý	k2eAgNnSc1d3	nejbolestivější
bodnutí	bodnutí	k1gNnSc1	bodnutí
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
štír	štír	k1gMnSc1	štír
smrtonoš	smrtonoš	k1gMnSc1	smrtonoš
(	(	kIx(	(
<g/>
Leiurus	Leiurus	k1gMnSc1	Leiurus
quinquestriatus	quinquestriatus	k1gMnSc1	quinquestriatus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nejúčinnější	účinný	k2eAgInSc1d3	nejúčinnější
jed	jed	k1gInSc1	jed
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
útočný	útočný	k2eAgMnSc1d1	útočný
a	a	k8xC	a
nevyzpytatelný	vyzpytatelný	k2eNgMnSc1d1	nevyzpytatelný
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnSc1d3	nejhorší
pověst	pověst	k1gFnSc1	pověst
má	mít	k5eAaImIp3nS	mít
štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
údajně	údajně	k6eAd1	údajně
způsobí	způsobit	k5eAaPmIp3nS	způsobit
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc6	bodnutí
štírem	štír	k1gMnSc7	štír
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
vydrážděni	vydrážděn	k2eAgMnPc1d1	vydrážděn
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
štíři	štír	k1gMnPc1	štír
plachá	plachý	k2eAgNnPc4d1	plaché
a	a	k8xC	a
neškodná	škodný	k2eNgNnPc4d1	neškodné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
používají	používat	k5eAaImIp3nP	používat
jed	jed	k1gInSc4	jed
jen	jen	k9	jen
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
vůbec	vůbec	k9	vůbec
nehýbou	hýbat	k5eNaImIp3nP	hýbat
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhy	druh	k1gInPc4	druh
štírů	štír	k1gMnPc2	štír
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
vyvinutými	vyvinutý	k2eAgFnPc7d1	vyvinutá
klepety	klepeto	k1gNnPc7	klepeto
používají	používat	k5eAaImIp3nP	používat
jed	jed	k1gInSc4	jed
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
štíři	štír	k1gMnPc1	štír
Androctonus	Androctonus	k1gMnSc1	Androctonus
australis	australis	k1gInSc1	australis
(	(	kIx(	(
<g/>
štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
<g/>
)	)	kIx)	)
Leiurus	Leiurus	k1gMnSc1	Leiurus
quinquestriatus	quinquestriatus	k1gMnSc1	quinquestriatus
(	(	kIx(	(
<g/>
štír	štír	k1gMnSc1	štír
smrtonoš	smrtonoš	k1gMnSc1	smrtonoš
<g/>
)	)	kIx)	)
Tityus	Tityus	k1gMnSc1	Tityus
serrulatus	serrulatus	k1gMnSc1	serrulatus
(	(	kIx(	(
<g/>
štír	štír	k1gMnSc1	štír
samičí	samičí	k2eAgMnSc1d1	samičí
<g/>
)	)	kIx)	)
Tityus	Tityus	k1gMnSc1	Tityus
bahiensis	bahiensis	k1gFnSc2	bahiensis
Hemiscorpius	Hemiscorpius	k1gMnSc1	Hemiscorpius
lepturus	lepturus	k1gMnSc1	lepturus
Pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
citlivé	citlivý	k2eAgFnPc4d1	citlivá
na	na	k7c4	na
živočišné	živočišný	k2eAgInPc4d1	živočišný
jedy	jed	k1gInPc4	jed
a	a	k8xC	a
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
štíři	štír	k1gMnPc1	štír
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Androctonus	Androctonus	k1gInSc1	Androctonus
<g/>
,	,	kIx,	,
Tityus	Tityus	k1gInSc1	Tityus
<g/>
,	,	kIx,	,
Leiurus	Leiurus	k1gInSc1	Leiurus
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parabuthus	Parabuthus	k1gMnSc1	Parabuthus
<g/>
,	,	kIx,	,
Buthus	Buthus	k1gMnSc1	Buthus
(	(	kIx(	(
<g/>
africká	africký	k2eAgFnSc1d1	africká
populace	populace	k1gFnSc1	populace
Buthus	Buthus	k1gMnSc1	Buthus
occitanus	occitanus	k1gMnSc1	occitanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hottentotta	Hottentotta	k1gMnSc1	Hottentotta
a	a	k8xC	a
Centruroides	Centruroides	k1gMnSc1	Centruroides
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
štíři	štír	k1gMnPc1	štír
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
rodů	rod	k1gInPc2	rod
jsou	být	k5eAaImIp3nP	být
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
seznam	seznam	k1gInSc4	seznam
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
štírů	štír	k1gMnPc2	štír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
dávat	dávat	k5eAaImF	dávat
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
štíra	štír	k1gMnSc2	štír
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
nedovedeme	dovést	k5eNaPmIp1nP	dovést
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
jedovatosti	jedovatost	k1gFnSc6	jedovatost
štírů	štír	k1gMnPc2	štír
na	na	k7c4	na
sweb	sweb	k1gInSc4	sweb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
chovem	chov	k1gInSc7	chov
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hmyz	hmyz	k1gInSc4	hmyz
nebo	nebo	k8xC	nebo
pavoukovce	pavoukovec	k1gInPc4	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
chováni	chován	k2eAgMnPc1d1	chován
sklípkani	sklípkan	k1gMnPc1	sklípkan
a	a	k8xC	a
štíři	štír	k1gMnPc1	štír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
i	i	k8xC	i
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
čistě	čistě	k6eAd1	čistě
jako	jako	k8xS	jako
domácí	domácí	k2eAgNnSc4d1	domácí
zvířátko	zvířátko	k1gNnSc4	zvířátko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
nebo	nebo	k8xC	nebo
křeček	křeček	k1gMnSc1	křeček
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
štírů	štír	k1gMnPc2	štír
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadný	snadný	k2eAgMnSc1d1	snadný
<g/>
,	,	kIx,	,
štíři	štír	k1gMnPc1	štír
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
štíra	štír	k1gMnSc2	štír
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
terárium	terárium	k1gNnSc1	terárium
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
3	[number]	k4	3
<g/>
x	x	k?	x
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
štír	štír	k1gMnSc1	štír
a	a	k8xC	a
2	[number]	k4	2
<g/>
x	x	k?	x
tak	tak	k6eAd1	tak
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
loví	lovit	k5eAaImIp3nP	lovit
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc4	kořist
často	často	k6eAd1	často
registrují	registrovat	k5eAaBmIp3nP	registrovat
dotykem	dotyk	k1gInSc7	dotyk
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nP	lovit
většinou	většinou	k6eAd1	většinou
jiné	jiný	k2eAgMnPc4d1	jiný
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
druhy	druh	k1gInPc1	druh
dovedou	dovést	k5eAaPmIp3nP	dovést
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
ještěrku	ještěrka	k1gFnSc4	ještěrka
nebo	nebo	k8xC	nebo
malé	malý	k2eAgMnPc4d1	malý
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
,	,	kIx,	,
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
mají	mít	k5eAaImIp3nP	mít
komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
rituály	rituál	k1gInPc1	rituál
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejdříve	dříve	k6eAd3	dříve
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rituálnímu	rituální	k2eAgInSc3d1	rituální
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
sameček	sameček	k1gMnSc1	sameček
samičce	samičko	k6eAd1	samičko
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
<g/>
,	,	kIx,	,
sameček	sameček	k1gMnSc1	sameček
vypustí	vypustit	k5eAaPmIp3nS	vypustit
spermie	spermie	k1gFnPc4	spermie
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
samičku	samička	k1gFnSc4	samička
na	na	k7c4	na
buňky	buňka	k1gFnPc4	buňka
natlačí	natlačit	k5eAaBmIp3nP	natlačit
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
oplození	oplození	k1gNnSc3	oplození
samičky	samička	k1gFnSc2	samička
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nosí	nosit	k5eAaImIp3nP	nosit
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
30	[number]	k4	30
–	–	k?	–
35	[number]	k4	35
mláďat	mládě	k1gNnPc2	mládě
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ještě	ještě	k9	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
svlékání	svlékání	k1gNnSc6	svlékání
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
štíři	štír	k1gMnPc1	štír
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledí	čeleď	k1gFnPc2	čeleď
Hemiscorpiidae	Hemiscorpiida	k1gFnSc2	Hemiscorpiida
a	a	k8xC	a
Scorpionidae	Scorpionida	k1gInPc4	Scorpionida
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
délky	délka	k1gFnSc2	délka
až	až	k9	až
23	[number]	k4	23
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgMnPc2d3	nejnebezpečnější
predátorů	predátor	k1gMnPc2	predátor
štírů	štír	k1gMnPc2	štír
jsou	být	k5eAaImIp3nP	být
promyky	promyka	k1gFnPc1	promyka
-	-	kIx~	-
savci	savec	k1gMnPc1	savec
podobající	podobající	k2eAgMnPc1d1	podobající
se	se	k3xPyFc4	se
kuně	kuna	k1gFnSc6	kuna
či	či	k8xC	či
fretce	fretka	k1gFnSc6	fretka
<g/>
.	.	kIx.	.
</s>
<s>
Mangusty	Mangusta	k1gFnPc1	Mangusta
například	například	k6eAd1	například
využívají	využívat	k5eAaPmIp3nP	využívat
oslabení	oslabení	k1gNnSc4	oslabení
smyslů	smysl	k1gInPc2	smysl
štírů	štír	k1gMnPc2	štír
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zraku	zrak	k1gInSc3	zrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
vyhrabou	vyhrabat	k5eAaPmIp3nP	vyhrabat
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukousnutí	ukousnutí	k1gNnSc6	ukousnutí
části	část	k1gFnSc2	část
s	s	k7c7	s
jedovým	jedový	k2eAgInSc7d1	jedový
hrotem	hrot	k1gInSc7	hrot
jsou	být	k5eAaImIp3nP	být
štíři	štír	k1gMnPc1	štír
pro	pro	k7c4	pro
mangusty	mangust	k1gInPc4	mangust
neškodní	škodní	k2eNgInPc4d1	neškodní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
stává	stávat	k5eAaImIp3nS	stávat
potrava	potrava	k1gFnSc1	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
štíři	štír	k1gMnPc1	štír
české	český	k2eAgInPc4d1	český
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
anglických	anglický	k2eAgMnPc2d1	anglický
nepoužívají	používat	k5eNaImIp3nP	používat
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
štírů	štír	k1gMnPc2	štír
má	mít	k5eAaImIp3nS	mít
český	český	k2eAgInSc4d1	český
rodový	rodový	k2eAgInSc4d1	rodový
název	název	k1gInSc4	název
štír	štír	k1gMnSc1	štír
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
rodovým	rodový	k2eAgInSc7d1	rodový
názvem	název	k1gInSc7	název
australoštír	australoštír	k1gInSc1	australoštír
-	-	kIx~	-
rod	rod	k1gInSc1	rod
Urodacus	Urodacus	k1gMnSc1	Urodacus
afghanoštír	afghanoštír	k1gMnSc1	afghanoštír
-	-	kIx~	-
rod	rod	k1gInSc1	rod
Afghanorthochirus	Afghanorthochirus	k1gMnSc1	Afghanorthochirus
veleštír	veleštír	k1gMnSc1	veleštír
-	-	kIx~	-
rody	rod	k1gInPc1	rod
Heterometrus	Heterometrus	k1gMnSc1	Heterometrus
<g/>
,	,	kIx,	,
Pandinus	Pandinus	k1gMnSc1	Pandinus
<g/>
,	,	kIx,	,
Scorpio	Scorpio	k1gMnSc1	Scorpio
<g/>
,	,	kIx,	,
Opistophthalmus	Opistophthalmus	k1gMnSc1	Opistophthalmus
amerikoštír	amerikoštír	k1gMnSc1	amerikoštír
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
<g/>
)	)	kIx)	)
-	-	kIx~	-
čeleď	čeleď	k1gFnSc1	čeleď
Vaejovidae	Vaejovida	k1gMnSc2	Vaejovida
hottentotta	hottentott	k1gMnSc2	hottentott
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
štír	štír	k1gMnSc1	štír
<g/>
)	)	kIx)	)
-	-	kIx~	-
rod	rod	k1gInSc1	rod
Hottentotta	Hottentotta	k1gMnSc1	Hottentotta
hrotoštír	hrotoštír	k1gMnSc1	hrotoštír
-	-	kIx~	-
celá	celý	k2eAgFnSc1d1	celá
čeleď	čeleď	k1gFnSc1	čeleď
Bothriuridae	Bothriurida	k1gFnSc2	Bothriurida
afroštír	afroštír	k1gInSc1	afroštír
-	-	kIx~	-
Butheoloides	Butheoloides	k1gInSc1	Butheoloides
Jediný	jediný	k2eAgInSc1d1	jediný
původní	původní	k2eAgInSc1d1	původní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žil	žít	k5eAaImAgInS	žít
a	a	k8xC	a
možná	možná	k9	možná
ještě	ještě	k6eAd1	ještě
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
štír	štír	k1gMnSc1	štír
kýlnatý	kýlnatý	k2eAgMnSc1d1	kýlnatý
(	(	kIx(	(
<g/>
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
tergestinus	tergestinus	k1gMnSc1	tergestinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
Slapské	slapský	k2eAgFnSc2d1	Slapská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
nebyl	být	k5eNaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
ani	ani	k9	ani
jeden	jeden	k4xCgInSc1	jeden
exemplář	exemplář	k1gInSc1	exemplář
<g/>
:	:	kIx,	:
Lokalita	lokalita	k1gFnSc1	lokalita
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
vzácných	vzácný	k2eAgNnPc2d1	vzácné
zvířat	zvíře	k1gNnPc2	zvíře
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
početnou	početný	k2eAgFnSc4d1	početná
populaci	populace	k1gFnSc4	populace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
prokázána	prokázán	k2eAgFnSc1d1	prokázána
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k9	již
v	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slapských	slapský	k2eAgMnPc2d1	slapský
štírů	štír	k1gMnPc2	štír
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
otázkou	otázka	k1gFnSc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
carpathicus	carpathicus	k1gMnSc1	carpathicus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c4	po
revizi	revize	k1gFnSc4	revize
rodu	rod	k1gInSc2	rod
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgInSc1d1	chráněn
E.	E.	kA	E.
carpathicus	carpathicus	k1gInSc1	carpathicus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevyskytoval	vyskytovat	k5eNaImAgMnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
carpathicus	carpathicus	k1gMnSc1	carpathicus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vzácným	vzácný	k2eAgMnSc7d1	vzácný
obyvatelem	obyvatel	k1gMnSc7	obyvatel
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
endemit	endemit	k1gInSc1	endemit
tamějších	tamější	k2eAgInPc2d1	tamější
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
ze	z	k7c2	z
štírů	štír	k1gMnPc2	štír
rodu	rod	k1gInSc2	rod
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
není	být	k5eNaImIp3nS	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
ani	ani	k8xC	ani
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
tergestinus	tergestinus	k1gMnSc1	tergestinus
byla	být	k5eAaImAgFnS	být
původním	původní	k2eAgInSc7d1	původní
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
rozšíření	rozšíření	k1gNnSc2	rozšíření
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
výskytu	výskyt	k1gInSc2	výskyt
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
Dobou	doba	k1gFnSc7	doba
ledovou	ledový	k2eAgFnSc7d1	ledová
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc6d1	podobná
lokalitě	lokalita	k1gFnSc6	lokalita
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
Krems	Kremsa	k1gFnPc2	Kremsa
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgMnSc1	nějaký
štír	štír	k1gMnSc1	štír
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
nebo	nebo	k8xC	nebo
vypuštěn	vypustit	k5eAaPmNgInS	vypustit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
štírů	štír	k1gMnPc2	štír
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
synantropních	synantropní	k2eAgNnPc2d1	synantropní
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
lidských	lidský	k2eAgInPc2d1	lidský
příbytků	příbytek	k1gInPc2	příbytek
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
ryze	ryze	k6eAd1	ryze
přírodním	přírodní	k2eAgNnSc6d1	přírodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
břehy	břeh	k1gInPc4	břeh
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
stráně	stráň	k1gFnPc1	stráň
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
další	další	k2eAgMnPc1d1	další
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
ruinách	ruina	k1gFnPc6	ruina
<g/>
,	,	kIx,	,
v	v	k7c6	v
puklinách	puklina	k1gFnPc6	puklina
v	v	k7c6	v
omítce	omítka	k1gFnSc6	omítka
<g/>
,	,	kIx,	,
starých	starý	k2eAgFnPc6d1	stará
zídkách	zídka	k1gFnPc6	zídka
a	a	k8xC	a
místech	místo	k1gNnPc6	místo
hustě	hustě	k6eAd1	hustě
osídlených	osídlený	k2eAgNnPc6d1	osídlené
lidmi	člověk	k1gMnPc7	člověk
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
noční	noční	k2eAgMnSc1d1	noční
živočich	živočich	k1gMnSc1	živočich
milující	milující	k2eAgNnSc4d1	milující
vlhko	vlhko	k1gNnSc4	vlhko
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
pod	pod	k7c4	pod
podložky	podložka	k1gFnPc4	podložka
stanů	stan	k1gInPc2	stan
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
předměty	předmět	k1gInPc1	předmět
položené	položený	k2eAgInPc1d1	položený
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
nevědomky	nevědomky	k6eAd1	nevědomky
zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
npř	npř	k?	npř
<g/>
.	.	kIx.	.
z	z	k7c2	z
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
možná	možná	k9	možná
ještě	ještě	k6eAd1	ještě
existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c6	na
xerotermech	xeroterm	k1gInPc6	xeroterm
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
dvě	dva	k4xCgFnPc1	dva
kolonie	kolonie	k1gFnPc1	kolonie
štírů	štír	k1gMnPc2	štír
rodu	rod	k1gInSc2	rod
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
<g/>
,	,	kIx,	,
zavlečené	zavlečený	k2eAgFnPc1d1	zavlečená
nebo	nebo	k8xC	nebo
vypuštěné	vypuštěný	k2eAgFnPc1d1	vypuštěná
nejspíše	nejspíše	k9	nejspíše
nezodpovědnými	zodpovědný	k2eNgMnPc7d1	nezodpovědný
chovateli	chovatel	k1gMnPc7	chovatel
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
chovatelé	chovatel	k1gMnPc1	chovatel
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
o	o	k7c4	o
reintrodukci	reintrodukce	k1gFnSc4	reintrodukce
vzácného	vzácný	k2eAgMnSc2d1	vzácný
a	a	k8xC	a
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nezvěstného	zvěstný	k2eNgMnSc4d1	nezvěstný
štíra	štír	k1gMnSc4	štír
kýlnatého	kýlnatý	k2eAgInSc2d1	kýlnatý
romnožováním	romnožování	k1gNnSc7	romnožování
a	a	k8xC	a
odchovem	odchov	k1gInSc7	odchov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
choulostivém	choulostivý	k2eAgNnSc6d1	choulostivé
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
štírů	štír	k1gMnPc2	štír
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
již	již	k6eAd1	již
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
prý	prý	k9	prý
nalezl	naleznout	k5eAaPmAgMnS	naleznout
pravé	pravý	k2eAgNnSc4d1	pravé
a	a	k8xC	a
vskutku	vskutku	k9	vskutku
jedovaté	jedovatý	k2eAgMnPc4d1	jedovatý
štíry	štír	k1gMnPc4	štír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
invazivní	invazivní	k2eAgInSc1d1	invazivní
druh	druh	k1gInSc1	druh
štíra	štír	k1gMnSc2	štír
považován	považován	k2eAgInSc1d1	považován
Euscorpius	Euscorpius	k1gInSc1	Euscorpius
italicus	italicus	k1gInSc1	italicus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
stabilní	stabilní	k2eAgInSc1d1	stabilní
výskyt	výskyt	k1gInSc1	výskyt
nebyl	být	k5eNaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
oblasti	oblast	k1gFnSc3	oblast
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
biogeograficky	biogeograficky	k6eAd1	biogeograficky
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Panonské	panonský	k2eAgFnSc2d1	Panonská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
část	část	k1gFnSc1	část
jihu	jih	k1gInSc2	jih
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzácně	vzácně	k6eAd1	vzácně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
teplomilný	teplomilný	k2eAgMnSc1d1	teplomilný
scink	scink	k1gMnSc1	scink
Krátkonožka	krátkonožka	k1gFnSc1	krátkonožka
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
štíři	štír	k1gMnPc1	štír
rodu	rod	k1gInSc2	rod
Euscorpius	Euscorpius	k1gInSc1	Euscorpius
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
italicus	italicus	k1gInSc1	italicus
<g/>
)	)	kIx)	)
běžnou	běžný	k2eAgFnSc7d1	běžná
a	a	k8xC	a
potvrzenou	potvrzený	k2eAgFnSc7d1	potvrzená
součástí	součást	k1gFnSc7	součást
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
možnost	možnost	k1gFnSc4	možnost
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
jihomoravských	jihomoravský	k2eAgNnPc6d1	Jihomoravské
místech	místo	k1gNnPc6	místo
nálezů	nález	k1gInPc2	nález
štírů	štír	k1gMnPc2	štír
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
i	i	k8xC	i
další	další	k2eAgFnSc1d1	další
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flora	flora	k1gFnSc1	flora
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
jsou	být	k5eAaImIp3nP	být
takřka	takřka	k6eAd1	takřka
určitě	určitě	k6eAd1	určitě
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
působením	působení	k1gNnSc7	působení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Štíři	štír	k1gMnPc1	štír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
štír	štír	k1gMnSc1	štír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Scorpiones	Scorpiones	k1gInSc1	Scorpiones
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
The	The	k1gFnSc1	The
Scorpion	Scorpion	k1gInSc1	Scorpion
files	files	k1gInSc1	files
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Štíři	štír	k1gMnPc1	štír
rodu	rod	k1gInSc2	rod
Tityus	Tityus	k1gInSc1	Tityus
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
