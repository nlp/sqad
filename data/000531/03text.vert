<s>
Opat	opat	k1gMnSc1	opat
je	být	k5eAaImIp3nS	být
představený	představený	k2eAgMnSc1d1	představený
kláštera	klášter	k1gInSc2	klášter
některých	některý	k3yIgInPc2	některý
mužských	mužský	k2eAgInPc2d1	mužský
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
roli	role	k1gFnSc4	role
jako	jako	k8xC	jako
opat	opat	k1gMnSc1	opat
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
ženských	ženský	k2eAgInPc2d1	ženský
řádů	řád	k1gInPc2	řád
abatyše	abatyše	k1gFnSc2	abatyše
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
opat	opat	k1gMnSc1	opat
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
abbas	abbasit	k5eAaPmRp2nS	abbasit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
abba	abb	k1gInSc2	abb
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
křesťanských	křesťanský	k2eAgNnPc6d1	křesťanské
stoletích	století	k1gNnPc6	století
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
uctivé	uctivý	k2eAgNnSc1d1	uctivé
oslovení	oslovení	k1gNnSc1	oslovení
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
a	a	k8xC	a
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
řehole	řehole	k1gFnSc2	řehole
Bendikta	Bendikt	k1gInSc2	Bendikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
označení	označení	k1gNnSc1	označení
představeného	představený	k1gMnSc2	představený
mnišské	mnišský	k2eAgFnSc2d1	mnišská
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
opata	opat	k1gMnSc2	opat
užívají	užívat	k5eAaImIp3nP	užívat
benediktini	benediktin	k1gMnPc1	benediktin
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
mužské	mužský	k2eAgInPc1d1	mužský
řády	řád	k1gInPc1	řád
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jejich	jejich	k3xOp3gFnPc7	jejich
reformami	reforma	k1gFnPc7	reforma
(	(	kIx(	(
<g/>
cisterciáci	cisterciák	k1gMnPc1	cisterciák
<g/>
,	,	kIx,	,
trapisté	trapist	k1gMnPc1	trapist
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
premonstráti	premonstrát	k1gMnPc1	premonstrát
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc4	představení
klášterů	klášter	k1gInPc2	klášter
později	pozdě	k6eAd2	pozdě
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
řeholních	řeholní	k2eAgInPc2d1	řeholní
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
kongregací	kongregace	k1gFnPc2	kongregace
užívají	užívat	k5eAaImIp3nP	užívat
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
označení	označení	k1gNnSc2	označení
převor	převor	k1gMnSc1	převor
<g/>
,	,	kIx,	,
kvardián	kvardián	k1gMnSc1	kvardián
<g/>
,	,	kIx,	,
probošt	probošt	k1gMnSc1	probošt
nebo	nebo	k8xC	nebo
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
je	být	k5eAaImIp3nS	být
svrchovaný	svrchovaný	k2eAgMnSc1d1	svrchovaný
představený	představený	k1gMnSc1	představený
s	s	k7c7	s
rozhodovací	rozhodovací	k2eAgFnSc7d1	rozhodovací
i	i	k8xC	i
soudní	soudní	k2eAgFnSc7d1	soudní
pravomocí	pravomoc	k1gFnSc7	pravomoc
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
Tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
-	-	kIx~	-
<g/>
1563	[number]	k4	1563
<g/>
)	)	kIx)	)
podléhá	podléhat	k5eAaImIp3nS	podléhat
dozoru	dozor	k1gInSc2	dozor
místního	místní	k2eAgInSc2d1	místní
biskupa	biskup	k1gInSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
nebo	nebo	k8xC	nebo
koupi	koupě	k1gFnSc6	koupě
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
poradit	poradit	k5eAaPmF	poradit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Opatem	opat	k1gMnSc7	opat
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pouze	pouze	k6eAd1	pouze
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
ho	on	k3xPp3gNnSc4	on
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Volbu	volba	k1gFnSc4	volba
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nově	nově	k6eAd1	nově
zvolenému	zvolený	k2eAgMnSc3d1	zvolený
opatovi	opat	k1gMnSc3	opat
také	také	k9	také
předává	předávat	k5eAaImIp3nS	předávat
insignie	insignie	k1gFnPc4	insignie
jeho	on	k3xPp3gInSc2	on
úřadu	úřad	k1gInSc2	úřad
<g/>
:	:	kIx,	:
berlu	berla	k1gFnSc4	berla
<g/>
,	,	kIx,	,
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
mitru	mitra	k1gFnSc4	mitra
<g/>
.	.	kIx.	.
</s>
<s>
Opatská	opatský	k2eAgFnSc1d1	opatská
hodnost	hodnost	k1gFnSc1	hodnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
preláty	prelát	k1gInPc7	prelát
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
biskupské	biskupský	k2eAgNnSc4d1	Biskupské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnPc4	jeho
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Opatský	opatský	k2eAgInSc1d1	opatský
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
černým	černý	k2eAgInSc7d1	černý
širokým	široký	k2eAgInSc7d1	široký
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
,	,	kIx,	,
dvanácti	dvanáct	k4xCc7	dvanáct
černými	černý	k2eAgInPc7d1	černý
uzlíky	uzlík	k1gInPc7	uzlík
či	či	k8xC	či
třapci	třapec	k1gInPc7	třapec
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
svisle	svisle	k6eAd1	svisle
postavenou	postavený	k2eAgFnSc7d1	postavená
berlou	berla	k1gFnSc7	berla
za	za	k7c7	za
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
heslo	heslo	k1gNnSc4	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
primas	primas	k1gMnSc1	primas
je	být	k5eAaImIp3nS	být
volený	volený	k2eAgMnSc1d1	volený
předseda	předseda	k1gMnSc1	předseda
shromáždění	shromáždění	k1gNnSc2	shromáždění
všech	všecek	k3xTgMnPc2	všecek
opatů	opat	k1gMnPc2	opat
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádech	řád	k1gInPc6	řád
premonstrátů	premonstrát	k1gMnPc2	premonstrát
<g/>
,	,	kIx,	,
cisterciáků	cisterciák	k1gMnPc2	cisterciák
a	a	k8xC	a
trapistů	trapist	k1gMnPc2	trapist
existuje	existovat	k5eAaImIp3nS	existovat
pro	pro	k7c4	pro
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
funkci	funkce	k1gFnSc4	funkce
označení	označení	k1gNnSc2	označení
generální	generální	k2eAgMnSc1d1	generální
opat	opat	k1gMnSc1	opat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
samostatnost	samostatnost	k1gFnSc1	samostatnost
kláštera	klášter	k1gInSc2	klášter
zakládala	zakládat	k5eAaImAgFnS	zakládat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
nemovitém	movitý	k2eNgInSc6d1	nemovitý
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
opat	opat	k1gMnSc1	opat
spravoval	spravovat	k5eAaImAgMnS	spravovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
světským	světský	k2eAgMnSc7d1	světský
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
podléhal	podléhat	k5eAaImAgInS	podléhat
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
biskupové	biskup	k1gMnPc1	biskup
-	-	kIx~	-
svému	svůj	k3xOyFgMnSc3	svůj
lennímu	lenní	k2eAgMnSc3d1	lenní
pánovi	pán	k1gMnSc3	pán
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gInSc4	on
sám	sám	k3xTgMnSc1	sám
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
i	i	k9	i
laičtí	laický	k2eAgMnPc1d1	laický
opati	opat	k1gMnPc1	opat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
někdy	někdy	k6eAd1	někdy
nebyli	být	k5eNaImAgMnP	být
ani	ani	k8xC	ani
členy	člen	k1gMnPc7	člen
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
pouze	pouze	k6eAd1	pouze
tuto	tento	k3xDgFnSc4	tento
světskou	světský	k2eAgFnSc4d1	světská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
německé	německý	k2eAgFnSc2d1	německá
reformace	reformace	k1gFnSc2	reformace
si	se	k3xPyFc3	se
opati	opat	k1gMnPc1	opat
reformovaných	reformovaný	k2eAgInPc2d1	reformovaný
klášterů	klášter	k1gInPc2	klášter
ponechali	ponechat	k5eAaPmAgMnP	ponechat
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
však	však	k9	však
označovali	označovat	k5eAaImAgMnP	označovat
spíš	spíš	k9	spíš
jako	jako	k9	jako
preláti	prelát	k1gMnPc1	prelát
nebo	nebo	k8xC	nebo
probošti	probošt	k1gMnPc1	probošt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
funkci	funkce	k1gFnSc4	funkce
opata	opat	k1gMnSc2	opat
igumen	igumen	k1gMnSc1	igumen
(	(	kIx(	(
<g/>
hégúmenos	hégúmenos	k1gMnSc1	hégúmenos
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
archimandrita	archimandrita	k1gMnSc1	archimandrita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opět	opět	k6eAd1	opět
oživil	oživit	k5eAaPmAgInS	oživit
starý	starý	k2eAgInSc1d1	starý
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
abbas	abbasa	k1gFnPc2	abbasa
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
titul	titul	k1gInSc1	titul
abbé	abbé	k1gMnPc2	abbé
jako	jako	k8xC	jako
zdvořilé	zdvořilý	k2eAgNnSc1d1	zdvořilé
oslovení	oslovení	k1gNnSc1	oslovení
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
)	)	kIx)	)
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Benediktini	benediktin	k1gMnPc1	benediktin
Převor	převor	k1gMnSc1	převor
Řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
Řády	řád	k1gInPc1	řád
a	a	k8xC	a
řeholní	řeholní	k2eAgFnPc1d1	řeholní
kongregace	kongregace	k1gFnPc1	kongregace
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
opat	opat	k1gMnSc1	opat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
opat	opat	k1gMnSc1	opat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
