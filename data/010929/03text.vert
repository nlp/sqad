<p>
<s>
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
je	být	k5eAaImIp3nS	být
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
nerealizovaného	realizovaný	k2eNgInSc2d1	nerealizovaný
amerického	americký	k2eAgInSc2d1	americký
dobrodružného	dobrodružný	k2eAgInSc2d1	dobrodružný
fantasy	fantas	k1gInPc4	fantas
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
jej	on	k3xPp3gNnSc4	on
Jeffrey	Jeffrea	k1gFnPc1	Jeffrea
Reiner	Reinra	k1gFnPc2	Reinra
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
snímek	snímek	k1gInSc1	snímek
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
komiksových	komiksový	k2eAgInPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
o	o	k7c4	o
Wonder	Wonder	k1gInSc4	Wonder
Woman	Woman	k1gInSc1	Woman
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
jako	jako	k8xS	jako
pilot	pilot	k1gInSc1	pilot
pro	pro	k7c4	pro
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
seriál	seriál	k1gInSc4	seriál
stanice	stanice	k1gFnSc2	stanice
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
však	však	k8xC	však
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
si	se	k3xPyFc3	se
pořad	pořad	k1gInSc4	pořad
neobjednat	objednat	k5eNaPmF	objednat
<g/>
.	.	kIx.	.
</s>
<s>
Realizovaný	realizovaný	k2eAgInSc1d1	realizovaný
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
vysílán	vysílán	k2eAgInSc1d1	vysílán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Diana	Diana	k1gFnSc1	Diana
Themyscira	Themyscir	k1gInSc2	Themyscir
vede	vést	k5eAaImIp3nS	vést
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
velkou	velký	k2eAgFnSc4d1	velká
společnost	společnost	k1gFnSc4	společnost
Themyscira	Themysciro	k1gNnSc2	Themysciro
Industries	Industriesa	k1gFnPc2	Industriesa
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
zločincům	zločinec	k1gMnPc3	zločinec
jako	jako	k8xS	jako
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
superhrdinskou	superhrdinský	k2eAgFnSc7d1	superhrdinská
činností	činnost	k1gFnSc7	činnost
nijak	nijak	k6eAd1	nijak
neskrývá	skrývat	k5eNaImIp3nS	skrývat
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
klid	klid	k1gInSc4	klid
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
navíc	navíc	k6eAd1	navíc
i	i	k9	i
skrytou	skrytý	k2eAgFnSc4d1	skrytá
totožnost	totožnost	k1gFnSc4	totožnost
Diany	Diana	k1gFnSc2	Diana
Prince	princ	k1gMnSc2	princ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
svoji	svůj	k3xOyFgFnSc4	svůj
obchodní	obchodní	k2eAgFnSc4d1	obchodní
konkurentku	konkurentka	k1gFnSc4	konkurentka
<g/>
,	,	kIx,	,
Veronicu	Veronic	k2eAgFnSc4d1	Veronica
Cale	Cale	k1gFnSc4	Cale
<g/>
,	,	kIx,	,
že	že	k8xS	že
distribuuje	distribuovat	k5eAaBmIp3nS	distribuovat
nelegální	legální	k2eNgFnPc4d1	nelegální
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
uživatelům	uživatel	k1gMnPc3	uživatel
nadlidské	nadlidský	k2eAgFnPc4d1	nadlidská
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
které	který	k3yIgNnSc1	který
ovšem	ovšem	k9	ovšem
můžou	můžou	k?	můžou
také	také	k9	také
lidi	člověk	k1gMnPc4	člověk
znetvořit	znetvořit	k5eAaPmF	znetvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Adrianne	Adriannout	k5eAaImIp3nS	Adriannout
Palicki	Palicke	k1gFnSc4	Palicke
jako	jako	k8xC	jako
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
/	/	kIx~	/
Diana	Diana	k1gFnSc1	Diana
Themyscira	Themyscira	k1gFnSc1	Themyscira
/	/	kIx~	/
Diana	Diana	k1gFnSc1	Diana
Prince	princ	k1gMnSc2	princ
</s>
</p>
<p>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Hurley	Hurlea	k1gMnSc2	Hurlea
jako	jako	k8xS	jako
Veronica	Veronicus	k1gMnSc2	Veronicus
Cale	Cal	k1gMnSc2	Cal
</s>
</p>
<p>
<s>
Tracie	Tracie	k1gFnSc1	Tracie
Thoms	Thomsa	k1gFnPc2	Thomsa
jako	jako	k8xS	jako
Etta	Ettum	k1gNnSc2	Ettum
Candy	Canda	k1gFnSc2	Canda
</s>
</p>
<p>
<s>
Pedro	Pedro	k6eAd1	Pedro
Pascal	pascal	k1gInSc1	pascal
jako	jako	k8xS	jako
Ed	Ed	k1gFnSc1	Ed
Indelicato	Indelicat	k2eAgNnSc1d1	Indelicat
</s>
</p>
<p>
<s>
Cary	car	k1gMnPc4	car
Elwes	Elwesa	k1gFnPc2	Elwesa
jako	jako	k8xC	jako
Henry	Henry	k1gMnSc1	Henry
Detmer	Detmer	k1gMnSc1	Detmer
</s>
</p>
<p>
<s>
Justin	Justin	k1gMnSc1	Justin
Bruening	Bruening	k1gInSc1	Bruening
jako	jako	k8xS	jako
Steve	Steve	k1gMnSc1	Steve
Trevor	Trevor	k1gMnSc1	Trevor
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
TV	TV	kA	TV
pilot	pilota	k1gFnPc2	pilota
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
