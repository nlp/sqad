<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
asi	asi	k9	asi
907	[number]	k4	907
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
929	[number]	k4	929
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Wenzel	Wenzel	k1gMnSc3	Wenzel
von	von	k1gInSc4	von
Böhmen	Böhmen	k2eAgInSc4d1	Böhmen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
a	a	k8xC	a
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
byl	být	k5eAaImAgMnS	být
vychováván	vychováván	k2eAgMnSc1d1	vychováván
svou	svůj	k3xOyFgFnSc7	svůj
babičkou	babička	k1gFnSc7	babička
svatou	svatý	k2eAgFnSc7d1	svatá
Ludmilou	Ludmila	k1gFnSc7	Ludmila
a	a	k8xC	a
vzdělával	vzdělávat	k5eAaImAgInS	vzdělávat
se	se	k3xPyFc4	se
na	na	k7c6	na
Budči	Budeč	k1gFnSc6	Budeč
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
saským	saský	k2eAgMnSc7d1	saský
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Ptáčníkem	Ptáčník	k1gMnSc7	Ptáčník
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zachovat	zachovat	k5eAaPmF	zachovat
suverenitu	suverenita	k1gFnSc4	suverenita
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
kostel	kostel	k1gInSc1	kostel
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
ve	v	k7c6	v
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgFnSc6d1	Staré
<g/>
)	)	kIx)	)
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
sídle	sídlo	k1gNnSc6	sídlo
svého	svůj	k1gMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
převzal	převzít	k5eAaPmAgMnS	převzít
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
Václav	Václav	k1gMnSc1	Václav
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
svatý	svatý	k1gMnSc1	svatý
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
v	v	k7c6	v
legendách	legenda	k1gFnPc6	legenda
mu	on	k3xPp3gMnSc3	on
připisovanou	připisovaný	k2eAgFnSc4d1	připisovaná
zbožnost	zbožnost	k1gFnSc4	zbožnost
(	(	kIx(	(
<g/>
vlastnoruční	vlastnoruční	k2eAgNnPc4d1	vlastnoruční
pěstování	pěstování	k1gNnPc4	pěstování
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
obilí	obilí	k1gNnSc2	obilí
pro	pro	k7c4	pro
svaté	svatý	k2eAgNnSc4d1	svaté
přijímání	přijímání	k1gNnSc4	přijímání
<g/>
,	,	kIx,	,
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
chudé	chudý	k1gMnPc4	chudý
<g/>
,	,	kIx,	,
otroky	otrok	k1gMnPc4	otrok
a	a	k8xC	a
vězně	vězeň	k1gMnPc4	vězeň
<g/>
,	,	kIx,	,
stavění	stavění	k1gNnSc4	stavění
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
kácení	kácení	k1gNnSc1	kácení
šibenic	šibenice	k1gFnPc2	šibenice
<g/>
,	,	kIx,	,
ničení	ničení	k1gNnSc4	ničení
pohanských	pohanský	k2eAgFnPc2d1	pohanská
svatyní	svatyně	k1gFnPc2	svatyně
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
a	a	k8xC	a
posmrtné	posmrtný	k2eAgInPc1d1	posmrtný
zázraky	zázrak	k1gInPc1	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
symbolem	symbol	k1gInSc7	symbol
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Kodexu	kodex	k1gInSc6	kodex
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
<g/>
,	,	kIx,	,
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Myslbekově	Myslbekův	k2eAgInSc6d1	Myslbekův
pomníku	pomník	k1gInSc6	pomník
<g/>
,	,	kIx,	,
a	a	k8xC	a
taky	taky	k6eAd1	taky
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
zničen	zničen	k2eAgInSc4d1	zničen
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poznání	poznání	k1gNnSc4	poznání
Václavova	Václavův	k2eAgInSc2d1	Václavův
života	život	k1gInSc2	život
chybí	chybit	k5eAaPmIp3nS	chybit
dostatek	dostatek	k1gInSc1	dostatek
spolehlivých	spolehlivý	k2eAgInPc2d1	spolehlivý
dobových	dobový	k2eAgInPc2d1	dobový
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
sepsány	sepsat	k5eAaPmNgInP	sepsat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
věrně	věrně	k6eAd1	věrně
zachytit	zachytit	k5eAaPmF	zachytit
historického	historický	k2eAgNnSc2d1	historické
knížete	kníže	k1gNnSc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
čerpány	čerpat	k5eAaImNgFnP	čerpat
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
okrajové	okrajový	k2eAgFnPc4d1	okrajová
zprávy	zpráva	k1gFnPc4	zpráva
kroniky	kronika	k1gFnSc2	kronika
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
mladšího	mladý	k2eAgMnSc2d2	mladší
saského	saský	k2eAgMnSc2d1	saský
kronikáře	kronikář	k1gMnSc2	kronikář
Widukinda	Widukind	k1gMnSc2	Widukind
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nepísemných	písemný	k2eNgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
archeologie	archeologie	k1gFnPc4	archeologie
a	a	k8xC	a
paleoantropologické	paleoantropologický	k2eAgInPc4d1	paleoantropologický
výzkumy	výzkum	k1gInPc4	výzkum
ostatků	ostatek	k1gInPc2	ostatek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
prováděné	prováděný	k2eAgInPc1d1	prováděný
zejména	zejména	k6eAd1	zejména
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
publikoval	publikovat	k5eAaBmAgMnS	publikovat
historiky	historik	k1gMnPc4	historik
málo	málo	k6eAd1	málo
přijímaný	přijímaný	k2eAgInSc1d1	přijímaný
obraz	obraz	k1gInSc1	obraz
prvních	první	k4xOgMnPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
světcově	světcův	k2eAgInSc6d1	světcův
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
a	a	k8xC	a
historiografie	historiografie	k1gFnSc1	historiografie
umí	umět	k5eAaImIp3nS	umět
podat	podat	k5eAaPmF	podat
jen	jen	k9	jen
nejasný	jasný	k2eNgInSc1d1	nejasný
či	či	k8xC	či
možný	možný	k2eAgInSc1d1	možný
obraz	obraz	k1gInSc1	obraz
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vždy	vždy	k6eAd1	vždy
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
kritickém	kritický	k2eAgNnSc6d1	kritické
zhodnocení	zhodnocení	k1gNnSc6	zhodnocení
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
interpretace	interpretace	k1gFnSc1	interpretace
nedostatečných	dostatečný	k2eNgInPc2d1	nedostatečný
a	a	k8xC	a
nespolehlivých	spolehlivý	k2eNgInPc2d1	nespolehlivý
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
knížete	kníže	k1gMnSc2	kníže
Vratislava	Vratislav	k1gMnSc2	Vratislav
(	(	kIx(	(
<g/>
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
Ludmily	Ludmila	k1gFnSc2	Ludmila
a	a	k8xC	a
prvního	první	k4xOgMnSc2	první
doloženého	doložený	k2eAgMnSc2d1	doložený
knížete	kníže	k1gMnSc2	kníže
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Drahomíry	Drahomíra	k1gFnPc4	Drahomíra
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnPc4	dcera
knížete	kníže	k1gNnSc2wR	kníže
Havolanů	Havolan	k1gInPc2	Havolan
(	(	kIx(	(
<g/>
polabští	polabský	k2eAgMnPc1d1	polabský
Slované	Slovan	k1gMnPc1	Slovan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
Vratislavovy	Vratislavův	k2eAgFnPc1d1	Vratislavova
jediné	jediný	k2eAgFnPc1d1	jediná
manželky	manželka	k1gFnPc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
rok	rok	k1gInSc4	rok
narození	narození	k1gNnSc2	narození
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kombinace	kombinace	k1gFnSc2	kombinace
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
legendách	legenda	k1gFnPc6	legenda
uváděn	uváděn	k2eAgInSc4d1	uváděn
rok	rok	k1gInSc4	rok
907	[number]	k4	907
<g/>
.	.	kIx.	.
</s>
<s>
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
výchovy	výchova	k1gFnSc2	výchova
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
Budči	Budeč	k1gFnSc6	Budeč
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
u	u	k7c2	u
panovníků	panovník	k1gMnPc2	panovník
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
spočívalo	spočívat	k5eAaImAgNnS	spočívat
ve	v	k7c6	v
studiu	studio	k1gNnSc3	studio
především	především	k6eAd1	především
latinského	latinský	k2eAgInSc2d1	latinský
žaltáře	žaltář	k1gInSc2	žaltář
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
staroslovanské	staroslovanský	k2eAgFnPc1d1	staroslovanská
legendy	legenda	k1gFnPc1	legenda
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
i	i	k9	i
studium	studium	k1gNnSc4	studium
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
legenda	legenda	k1gFnSc1	legenda
líčí	líčit	k5eAaImIp3nS	líčit
Václavovy	Václavův	k2eAgFnPc4d1	Václavova
postřižiny	postřižiny	k1gFnPc4	postřižiny
<g/>
,	,	kIx,	,
obřad	obřad	k1gInSc4	obřad
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
stříhání	stříhání	k1gNnSc2	stříhání
vlasů	vlas	k1gInPc2	vlas
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předpokládaného	předpokládaný	k2eAgNnSc2d1	předpokládané
data	datum	k1gNnSc2	datum
narození	narození	k1gNnSc2	narození
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vratislav	Vratislav	k1gMnSc1	Vratislav
na	na	k7c4	na
postřižiny	postřižiny	k1gFnPc4	postřižiny
pozval	pozvat	k5eAaPmAgMnS	pozvat
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
obřad	obřad	k1gInSc4	obřad
klást	klást	k5eAaImF	klást
do	do	k7c2	do
roku	rok	k1gInSc2	rok
915	[number]	k4	915
<g/>
,	,	kIx,	,
po	po	k7c6	po
Vratislavově	Vratislavův	k2eAgInSc6d1	Vratislavův
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Václavova	Václavův	k2eAgMnSc2d1	Václavův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
Vratislava	Vratislav	k1gMnSc2	Vratislav
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
921	[number]	k4	921
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
regentské	regentský	k2eAgFnPc4d1	regentská
vlády	vláda	k1gFnPc4	vláda
Václavova	Václavův	k2eAgFnSc1d1	Václavova
matka	matka	k1gFnSc1	matka
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Boleslavem	Boleslav	k1gMnSc7	Boleslav
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
stoupenci	stoupenec	k1gMnPc7	stoupenec
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
spor	spor	k1gInSc4	spor
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
a	a	k8xC	a
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
zřejmě	zřejmě	k6eAd1	zřejmě
šlo	jít	k5eAaImAgNnS	jít
i	i	k9	i
o	o	k7c4	o
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Sasku	Sasko	k1gNnSc3	Sasko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
východofranské	východofranský	k2eAgFnSc2d1	Východofranská
říše	říš	k1gFnSc2	říš
přesunula	přesunout	k5eAaPmAgFnS	přesunout
moc	moc	k6eAd1	moc
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Ptáčníka	Ptáčník	k1gMnSc2	Ptáčník
podrobovalo	podrobovat	k5eAaImAgNnS	podrobovat
sousední	sousední	k2eAgInPc1d1	sousední
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
pro	pro	k7c4	pro
Ludmilu	Ludmila	k1gFnSc4	Ludmila
skončil	skončit	k5eAaPmAgMnS	skončit
jejím	její	k3xOp3gNnSc7	její
zavražděním	zavraždění	k1gNnSc7	zavraždění
Drahomířinými	Drahomířin	k2eAgMnPc7d1	Drahomířin
družiníky	družiník	k1gMnPc7	družiník
Tunnou	tunný	k2eAgFnSc7d1	tunný
a	a	k8xC	a
Gommonem	Gommon	k1gInSc7	Gommon
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
921	[number]	k4	921
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
legendy	legenda	k1gFnPc1	legenda
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
líčí	líčit	k5eAaImIp3nS	líčit
Drahomíru	Drahomíra	k1gFnSc4	Drahomíra
jako	jako	k8xS	jako
zastánkyni	zastánkyně	k1gFnSc4	zastánkyně
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yQgFnPc4	který
stála	stát	k5eAaImAgFnS	stát
zastánkyně	zastánkyně	k1gFnSc1	zastánkyně
křesťanství	křesťanství	k1gNnSc2	křesťanství
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
roli	role	k1gFnSc4	role
později	pozdě	k6eAd2	pozdě
převzal	převzít	k5eAaPmAgMnS	převzít
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Vratislavovy	Vratislavův	k2eAgFnSc2d1	Vratislavova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
921	[number]	k4	921
Václav	Václav	k1gMnSc1	Václav
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
dospělého	dospělý	k1gMnSc4	dospělý
a	a	k8xC	a
proto	proto	k8xC	proto
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
Václav	Václav	k1gMnSc1	Václav
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
dospělosti	dospělost	k1gFnPc4	dospělost
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
to	ten	k3xDgNnSc1	ten
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
925	[number]	k4	925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
vládl	vládnout	k5eAaImAgMnS	vládnout
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
převézt	převézt	k5eAaPmF	převézt
ostatky	ostatek	k1gInPc4	ostatek
kněžny	kněžna	k1gFnSc2	kněžna
Ludmily	Ludmila	k1gFnSc2	Ludmila
z	z	k7c2	z
Tetína	Tetín	k1gInSc2	Tetín
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historicky	historicky	k6eAd1	historicky
nepodložené	podložený	k2eNgFnSc2d1	nepodložená
místní	místní	k2eAgFnSc2d1	místní
pověsti	pověst	k1gFnSc2	pověst
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
hradišti	hradiště	k1gNnSc6	hradiště
ve	v	k7c6	v
Stochově	Stochův	k2eAgFnSc6d1	Stochova
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
"	"	kIx"	"
<g/>
sto	sto	k4xCgNnSc4	sto
chův	chůva	k1gFnPc2	chůva
<g/>
"	"	kIx"	"
=	=	kIx~	=
Stochov	Stochov	k1gInSc1	Stochov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
dubu	dub	k1gInSc2	dub
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
místě	místo	k1gNnSc6	místo
měla	mít	k5eAaImAgFnS	mít
zasadit	zasadit	k5eAaPmF	zasadit
Václavova	Václavův	k2eAgFnSc1d1	Václavova
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
kněžna	kněžna	k1gFnSc1	kněžna
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Torzo	torzo	k1gNnSc1	torzo
stromu	strom	k1gInSc2	strom
na	na	k7c6	na
stochovském	stochovský	k2eAgNnSc6d1	stochovský
náměstí	náměstí	k1gNnSc6	náměstí
U	u	k7c2	u
Dubu	dub	k1gInSc2	dub
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
921	[number]	k4	921
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
knížete	kníže	k1gMnSc2	kníže
Vratislava	Vratislav	k1gMnSc2	Vratislav
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
925	[number]	k4	925
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovské	přemyslovský	k2eAgNnSc1d1	přemyslovské
knížectví	knížectví	k1gNnSc1	knížectví
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
hradišti	hradiště	k1gNnPc7	hradiště
umístěnými	umístěný	k2eAgInPc7d1	umístěný
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
třicetikilometrovém	třicetikilometrový	k2eAgInSc6d1	třicetikilometrový
okruhu	okruh	k1gInSc6	okruh
kolem	kolem	k7c2	kolem
ústředního	ústřední	k2eAgNnSc2d1	ústřední
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
pražského	pražský	k2eAgNnSc2d1	Pražské
hradiště	hradiště	k1gNnSc2	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbývajících	zbývající	k2eAgFnPc6d1	zbývající
částech	část	k1gFnPc6	část
Čech	Čechy	k1gFnPc2	Čechy
byla	být	k5eAaImAgNnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
další	další	k2eAgNnPc1d1	další
česká	český	k2eAgNnPc1d1	české
knížectví	knížectví	k1gNnPc1	knížectví
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
s	s	k7c7	s
těmito	tento	k3xDgMnPc7	tento
sousedními	sousední	k2eAgMnPc7d1	sousední
vládci	vládce	k1gMnPc7	vládce
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
vojenského	vojenský	k2eAgInSc2d1	vojenský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Kristiánova	Kristiánův	k2eAgFnSc1d1	Kristiánova
legenda	legenda	k1gFnSc1	legenda
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
střet	střet	k1gInSc4	střet
Václava	Václav	k1gMnSc2	Václav
s	s	k7c7	s
kouřimským	kouřimský	k2eAgMnSc7d1	kouřimský
knížetem	kníže	k1gMnSc7	kníže
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
přemohl	přemoct	k5eAaPmAgMnS	přemoct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vládu	vláda	k1gFnSc4	vláda
mu	on	k3xPp3gMnSc3	on
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
svědčilo	svědčit	k5eAaImAgNnS	svědčit
o	o	k7c6	o
Václavově	Václavův	k2eAgFnSc6d1	Václavova
politice	politika	k1gFnSc6	politika
upevňování	upevňování	k1gNnSc2	upevňování
stávajícího	stávající	k2eAgNnSc2d1	stávající
knížectví	knížectví	k1gNnSc2	knížectví
místo	místo	k7c2	místo
jeho	on	k3xPp3gNnSc2	on
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hrozby	hrozba	k1gFnSc2	hrozba
expanzivní	expanzivní	k2eAgFnSc2d1	expanzivní
saské	saský	k2eAgFnSc2d1	saská
politiky	politika	k1gFnSc2	politika
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc1d2	pozdější
Dalimilova	Dalimilův	k2eAgFnSc1d1	Dalimilova
kronika	kronika	k1gFnSc1	kronika
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
kouřimského	kouřimský	k2eAgMnSc4d1	kouřimský
knížete	kníže	k1gMnSc4	kníže
jako	jako	k8xS	jako
zlického	zlický	k2eAgMnSc4d1	zlický
Radslava	Radslav	k1gMnSc4	Radslav
<g/>
.	.	kIx.	.
</s>
<s>
Hypoteticky	hypoteticky	k6eAd1	hypoteticky
může	moct	k5eAaImIp3nS	moct
střet	střet	k1gInSc1	střet
souviset	souviset	k5eAaImF	souviset
se	s	k7c7	s
sasko-bavorskou	saskoavorský	k2eAgFnSc7d1	sasko-bavorský
intervencí	intervence	k1gFnSc7	intervence
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
roku	rok	k1gInSc2	rok
929	[number]	k4	929
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
tzv.	tzv.	kA	tzv.
Radslava	Radslav	k1gMnSc2	Radslav
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
určitou	určitý	k2eAgFnSc4d1	určitá
jednotnost	jednotnost	k1gFnSc4	jednotnost
Čech	Čechy	k1gFnPc2	Čechy
vůči	vůči	k7c3	vůči
zahraničí	zahraničí	k1gNnSc3	zahraničí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vystopovat	vystopovat	k5eAaPmF	vystopovat
už	už	k6eAd1	už
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
daně	daň	k1gFnPc4	daň
Východofranské	východofranský	k2eAgFnPc4d1	Východofranská
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
křest	křest	k1gInSc4	křest
14	[number]	k4	14
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
roku	rok	k1gInSc2	rok
845	[number]	k4	845
v	v	k7c6	v
bavorském	bavorský	k2eAgNnSc6d1	bavorské
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jistém	jistý	k2eAgNnSc6d1	jisté
výsadním	výsadní	k2eAgNnSc6d1	výsadní
postavení	postavení	k1gNnSc6	postavení
Přemyslovce	Přemyslovec	k1gMnSc2	Přemyslovec
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
českého	český	k2eAgInSc2d1	český
celku	celek	k1gInSc2	celek
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
svědčit	svědčit	k5eAaImF	svědčit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
saskému	saský	k2eAgMnSc3d1	saský
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
zavázal	zavázat	k5eAaPmAgMnS	zavázat
odvádět	odvádět	k5eAaImF	odvádět
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
(	(	kIx(	(
<g/>
celých	celý	k2eAgFnPc2d1	celá
<g/>
)	)	kIx)	)
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
nebylo	být	k5eNaImAgNnS	být
ani	ani	k9	ani
vlastní	vlastní	k2eAgNnSc1d1	vlastní
přemyslovské	přemyslovský	k2eAgNnSc1d1	přemyslovské
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Dokládá	dokládat	k5eAaImIp3nS	dokládat
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
střet	střet	k1gInSc1	střet
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
s	s	k7c7	s
Ludmilou	Ludmila	k1gFnSc7	Ludmila
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
Václava	Václav	k1gMnSc2	Václav
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Mocenské	mocenský	k2eAgFnPc4d1	mocenská
skupiny	skupina	k1gFnPc4	skupina
okolo	okolo	k7c2	okolo
obou	dva	k4xCgFnPc2	dva
žen	žena	k1gFnPc2	žena
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
Ludmily	Ludmila	k1gFnSc2	Ludmila
těžko	těžko	k6eAd1	těžko
mohly	moct	k5eAaImAgFnP	moct
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
spjat	spjat	k2eAgMnSc1d1	spjat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
kolem	kolem	k7c2	kolem
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gNnSc4	on
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
zprávy	zpráva	k1gFnPc1	zpráva
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
vlády	vláda	k1gFnSc2	vláda
nechal	nechat	k5eAaPmAgInS	nechat
přenést	přenést	k5eAaPmF	přenést
ostatky	ostatek	k1gInPc4	ostatek
kněžny	kněžna	k1gFnSc2	kněžna
Ludmily	Ludmila	k1gFnSc2	Ludmila
z	z	k7c2	z
Tetína	Tetín	k1gInSc2	Tetín
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
povolal	povolat	k5eAaPmAgInS	povolat
zpět	zpět	k6eAd1	zpět
Drahomírou	Drahomíra	k1gFnSc7	Drahomíra
vyhnané	vyhnaný	k2eAgMnPc4d1	vyhnaný
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
Drahomíru	Drahomíra	k1gFnSc4	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
měl	mít	k5eAaImAgMnS	mít
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
asi	asi	k9	asi
po	po	k7c6	po
upevnění	upevnění	k1gNnSc6	upevnění
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
povolat	povolat	k5eAaPmF	povolat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
střet	střet	k1gInSc4	střet
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Boleslavem	Boleslav	k1gMnSc7	Boleslav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
stoupenci	stoupenec	k1gMnPc7	stoupenec
usazenými	usazený	k2eAgInPc7d1	usazený
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
hradišti	hradiště	k1gNnSc6	hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
skončil	skončit	k5eAaPmAgMnS	skončit
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
vraždou	vražda	k1gFnSc7	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hledat	hledat	k5eAaImF	hledat
jak	jak	k8xS	jak
v	v	k7c6	v
osobních	osobní	k2eAgFnPc6d1	osobní
ambicích	ambice	k1gFnPc6	ambice
později	pozdě	k6eAd2	pozdě
velmi	velmi	k6eAd1	velmi
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
vládce	vládce	k1gMnSc4	vládce
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
poddání	poddání	k1gNnSc1	poddání
se	se	k3xPyFc4	se
Sasku	Sasko	k1gNnSc6	Sasko
Boleslav	Boleslav	k1gMnSc1	Boleslav
obnovil	obnovit	k5eAaPmAgMnS	obnovit
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
čtrnáctiletém	čtrnáctiletý	k2eAgNnSc6d1	čtrnáctileté
nepřátelství	nepřátelství	k1gNnSc6	nepřátelství
<g/>
.	.	kIx.	.
</s>
<s>
Václavův	Václavův	k2eAgInSc1d1	Václavův
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
je	být	k5eAaImIp3nS	být
legendami	legenda	k1gFnPc7	legenda
doveden	doveden	k2eAgInSc1d1	doveden
do	do	k7c2	do
těžko	těžko	k6eAd1	těžko
uvěřitelného	uvěřitelný	k2eAgInSc2d1	uvěřitelný
ideálu	ideál	k1gInSc2	ideál
"	"	kIx"	"
<g/>
mnicha	mnich	k1gMnSc2	mnich
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přehnaně	přehnaně	k6eAd1	přehnaně
asketický	asketický	k2eAgInSc1d1	asketický
obraz	obraz	k1gInSc1	obraz
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
prokázal	prokázat	k5eAaPmAgMnS	prokázat
kvality	kvalita	k1gFnSc2	kvalita
schopného	schopný	k2eAgMnSc2d1	schopný
vládce	vládce	k1gMnSc2	vládce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přičíst	přičíst	k5eAaPmF	přičíst
vlivu	vliv	k1gInSc2	vliv
ideálů	ideál	k1gInPc2	ideál
zbožnosti	zbožnost	k1gFnSc2	zbožnost
clunyjského	clunyjský	k2eAgNnSc2d1	clunyjské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
českou	český	k2eAgFnSc4d1	Česká
oblast	oblast	k1gFnSc4	oblast
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
až	až	k6eAd1	až
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
prvních	první	k4xOgFnPc2	první
legend	legenda	k1gFnPc2	legenda
o	o	k7c6	o
Václavovi	Václav	k1gMnSc6	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
kácení	kácení	k1gNnSc6	kácení
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
vlastnoručním	vlastnoruční	k2eAgNnSc6d1	vlastnoruční
pěstování	pěstování	k1gNnSc6	pěstování
révy	réva	k1gFnSc2	réva
(	(	kIx(	(
<g/>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
vinice	vinice	k1gFnSc1	vinice
<g/>
)	)	kIx)	)
a	a	k8xC	a
obilí	obilí	k1gNnSc4	obilí
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
hostií	hostie	k1gFnPc2	hostie
<g/>
,	,	kIx,	,
vykupování	vykupování	k1gNnSc1	vykupování
otroků	otrok	k1gMnPc2	otrok
ap.	ap.	kA	ap.
mu	on	k3xPp3gMnSc3	on
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
dodatečně	dodatečně	k6eAd1	dodatečně
připsány	připsat	k5eAaPmNgInP	připsat
jako	jako	k9	jako
obecné	obecný	k2eAgFnPc1d1	obecná
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
ctnosti	ctnost	k1gFnPc1	ctnost
bez	bez	k7c2	bez
historického	historický	k2eAgInSc2d1	historický
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Uváděné	uváděný	k2eAgNnSc1d1	uváděné
stavění	stavění	k1gNnSc1	stavění
kostelů	kostel	k1gInPc2	kostel
také	také	k9	také
není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradištích	hradiště	k1gNnPc6	hradiště
kostely	kostel	k1gInPc7	kostel
stavěli	stavět	k5eAaImAgMnP	stavět
již	již	k9	již
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
výjimkou	výjimka	k1gFnSc7	výjimka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
hradišti	hradiště	k1gNnSc6	hradiště
nový	nový	k2eAgInSc4d1	nový
kostel	kostel	k1gInSc4	kostel
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
(	(	kIx(	(
<g/>
asi	asi	k9	asi
930	[number]	k4	930
<g/>
)	)	kIx)	)
svatému	svatý	k1gMnSc3	svatý
Vítu	Vít	k1gMnSc3	Vít
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
ostatek	ostatek	k1gInSc4	ostatek
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
saského	saský	k2eAgMnSc2d1	saský
Jindřicha	Jindřich	k1gMnSc2	Jindřich
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
porážce	porážka	k1gFnSc6	porážka
roku	rok	k1gInSc2	rok
929	[number]	k4	929
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
ostatkem	ostatek	k1gInSc7	ostatek
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
doložená	doložený	k2eAgFnSc1d1	doložená
světcova	světcův	k2eAgFnSc1d1	světcova
ruka	ruka	k1gFnSc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
sídle	sídlo	k1gNnSc6	sídlo
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
již	již	k9	již
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Spytihněv	Spytihněv	k1gMnSc1	Spytihněv
hradiště	hradiště	k1gNnSc4	hradiště
opevnil	opevnit	k5eAaPmAgMnS	opevnit
valem	valem	k6eAd1	valem
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Vratislav	Vratislav	k1gMnSc1	Vratislav
postavil	postavit	k5eAaPmAgMnS	postavit
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
třetího	třetí	k4xOgInSc2	třetí
kostela	kostel	k1gInSc2	kostel
musel	muset	k5eAaImAgMnS	muset
mít	mít	k5eAaImF	mít
Václav	Václav	k1gMnSc1	Václav
nějaký	nějaký	k3yIgMnSc1	nějaký
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
symbolický	symbolický	k2eAgInSc4d1	symbolický
a	a	k8xC	a
reprezentativní	reprezentativní	k2eAgInSc4d1	reprezentativní
důvod	důvod	k1gInSc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rotunda	rotunda	k1gFnSc1	rotunda
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
byla	být	k5eAaImAgFnS	být
prý	prý	k9	prý
postavena	postaven	k2eAgFnSc1d1	postavena
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
kostela	kostel	k1gInSc2	kostel
římského	římský	k2eAgInSc2d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
svatopetrský	svatopetrský	k2eAgInSc1d1	svatopetrský
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
podélná	podélný	k2eAgFnSc1d1	podélná
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c6	na
falcích	falc	k1gFnPc6	falc
běžném	běžný	k2eAgInSc6d1	běžný
vzoru	vzor	k1gInSc6	vzor
kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
obnovitelem	obnovitel	k1gMnSc7	obnovitel
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc4	význam
mělo	mít	k5eAaImAgNnS	mít
jistě	jistě	k9	jistě
i	i	k9	i
umístění	umístění	k1gNnSc4	umístění
rotundy	rotunda	k1gFnSc2	rotunda
uprostřed	uprostřed	k7c2	uprostřed
dnešního	dnešní	k2eAgInSc2d1	dnešní
III	III	kA	III
<g/>
.	.	kIx.	.
hradního	hradní	k2eAgNnSc2d1	hradní
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
,	,	kIx,	,
u	u	k7c2	u
vyvýšeniny	vyvýšenina	k1gFnSc2	vyvýšenina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
archeologicky	archeologicky	k6eAd1	archeologicky
zjištěno	zjištěn	k2eAgNnSc4d1	zjištěno
starší	starý	k2eAgNnSc4d2	starší
významné	významný	k2eAgNnSc4d1	významné
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
doložené	doložený	k2eAgNnSc1d1	doložené
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
rotunda	rotunda	k1gFnSc1	rotunda
postavena	postaven	k2eAgFnSc1d1	postavena
u	u	k7c2	u
kamenného	kamenný	k2eAgInSc2d1	kamenný
nastolovacího	nastolovací	k2eAgInSc2d1	nastolovací
knížecího	knížecí	k2eAgInSc2d1	knížecí
trůnu	trůn	k1gInSc2	trůn
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bájným	bájný	k2eAgInSc7d1	bájný
posvátným	posvátný	k2eAgInSc7d1	posvátný
pahorkem	pahorek	k1gInSc7	pahorek
Žiži	Žiž	k1gFnSc2	Žiž
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
vyvýšenina	vyvýšenina	k1gFnSc1	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c4	na
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
významném	významný	k2eAgNnSc6d1	významné
<g/>
,	,	kIx,	,
centrálním	centrální	k2eAgNnSc6d1	centrální
místě	místo	k1gNnSc6	místo
hradiště	hradiště	k1gNnSc2	hradiště
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
stávajícími	stávající	k2eAgInPc7d1	stávající
kostely	kostel	k1gInPc7	kostel
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
hradiště	hradiště	k1gNnSc2	hradiště
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
české	český	k2eAgFnSc2d1	Česká
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
východofranské	východofranský	k2eAgFnSc3d1	Východofranská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
Karlovců	Karlovac	k1gInPc2	Karlovac
roku	rok	k1gInSc2	rok
911	[number]	k4	911
rozpadal	rozpadat	k5eAaImAgInS	rozpadat
jednotný	jednotný	k2eAgInSc1d1	jednotný
stát	stát	k1gInSc1	stát
a	a	k8xC	a
formovalo	formovat	k5eAaImAgNnS	formovat
nové	nový	k2eAgNnSc4d1	nové
mocenské	mocenský	k2eAgNnSc4d1	mocenské
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
roku	rok	k1gInSc3	rok
962	[number]	k4	962
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
založení	založení	k1gNnSc6	založení
(	(	kIx(	(
<g/>
Svaté	svatý	k2eAgFnSc2d1	svatá
<g/>
)	)	kIx)	)
Říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
Otou	Ota	k1gMnSc7	Ota
I.	I.	kA	I.
Čechy	Čechy	k1gFnPc1	Čechy
měly	mít	k5eAaImAgFnP	mít
tradiční	tradiční	k2eAgInSc4d1	tradiční
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Bavorsku	Bavorsko	k1gNnSc3	Bavorsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
také	také	k9	také
pod	pod	k7c4	pod
řezenskou	řezenský	k2eAgFnSc4d1	řezenská
diecézi	diecéze	k1gFnSc4	diecéze
spadala	spadat	k5eAaPmAgFnS	spadat
česká	český	k2eAgFnSc1d1	Česká
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Mocenské	mocenský	k2eAgNnSc1d1	mocenské
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
však	však	k9	však
odtud	odtud	k6eAd1	odtud
přeneslo	přenést	k5eAaPmAgNnS	přenést
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
Ptáčník	Ptáčník	k1gMnSc1	Ptáčník
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Oty	Ota	k1gMnSc2	Ota
I.	I.	kA	I.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
919	[number]	k4	919
zvolen	zvolit	k5eAaPmNgMnS	zvolit
východofranským	východofranský	k2eAgMnSc7d1	východofranský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
uspořádání	uspořádání	k1gNnSc1	uspořádání
musel	muset	k5eAaImAgInS	muset
přijmout	přijmout	k5eAaPmF	přijmout
i	i	k9	i
bavorský	bavorský	k2eAgMnSc1d1	bavorský
vévoda	vévoda	k1gMnSc1	vévoda
Arnulf	Arnulf	k1gMnSc1	Arnulf
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
uznání	uznání	k1gNnSc4	uznání
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
Jindřich	Jindřich	k1gMnSc1	Jindřich
vynutil	vynutit	k5eAaPmAgMnS	vynutit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
výpravou	výprava	k1gFnSc7	výprava
roku	rok	k1gInSc2	rok
921	[number]	k4	921
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
politika	politika	k1gFnSc1	politika
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
expanzi	expanze	k1gFnSc6	expanze
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
polabských	polabský	k2eAgInPc2d1	polabský
Slovanů	Slovan	k1gInPc2	Slovan
a	a	k8xC	a
obraně	obraně	k6eAd1	obraně
proti	proti	k7c3	proti
výbojným	výbojný	k2eAgInPc3d1	výbojný
Maďarům	maďar	k1gInPc3	maďar
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
i	i	k9	i
Arnulf	Arnulf	k1gMnSc1	Arnulf
<g/>
.	.	kIx.	.
</s>
<s>
Změněnou	změněný	k2eAgFnSc7d1	změněná
situací	situace	k1gFnSc7	situace
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
Václava	Václav	k1gMnSc2	Václav
narušena	narušen	k2eAgFnSc1d1	narušena
tradiční	tradiční	k2eAgFnSc1d1	tradiční
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
severní	severní	k2eAgFnPc4d1	severní
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
knížecích	knížecí	k2eAgFnPc2d1	knížecí
rodin	rodina	k1gFnPc2	rodina
pocházely	pocházet	k5eAaImAgFnP	pocházet
manželky	manželka	k1gFnPc1	manželka
Václavova	Václavův	k2eAgMnSc2d1	Václavův
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
děda	děd	k1gMnSc2	děd
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aliance	aliance	k1gFnSc1	aliance
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
namířena	namířen	k2eAgFnSc1d1	namířena
proti	proti	k7c3	proti
nebezpečně	bezpečně	k6eNd1	bezpečně
vzrůstající	vzrůstající	k2eAgFnSc3d1	vzrůstající
saské	saský	k2eAgFnSc3d1	saská
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
snad	snad	k9	snad
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
pasivně	pasivně	k6eAd1	pasivně
podporovala	podporovat	k5eAaImAgFnS	podporovat
maďarské	maďarský	k2eAgInPc4d1	maďarský
výpady	výpad	k1gInPc4	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojenectví	spojenectví	k1gNnSc6	spojenectví
se	s	k7c7	s
Slovany	Slovan	k1gMnPc7	Slovan
asi	asi	k9	asi
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
izolace	izolace	k1gFnSc2	izolace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
Arnulfem	Arnulf	k1gMnSc7	Arnulf
a	a	k8xC	a
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
927	[number]	k4	927
<g/>
)	)	kIx)	)
i	i	k8xC	i
s	s	k7c7	s
Maďary	Maďar	k1gMnPc7	Maďar
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
za	za	k7c4	za
mír	mír	k1gInSc4	mír
zavázal	zavázat	k5eAaPmAgInS	zavázat
platit	platit	k5eAaImF	platit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
sousedství	sousedství	k1gNnSc6	sousedství
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
dobyl	dobýt	k5eAaPmAgMnS	dobýt
území	území	k1gNnSc4	území
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vojensky	vojensky	k6eAd1	vojensky
vypravil	vypravit	k5eAaPmAgInS	vypravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
929	[number]	k4	929
<g/>
,	,	kIx,	,
podporován	podporovat	k5eAaImNgMnS	podporovat
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
Arnulfovým	Arnulfův	k2eAgNnSc7d1	Arnulfovo
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Widukind	Widukind	k1gInSc4	Widukind
<g/>
,	,	kIx,	,
přirazil	přirazit	k5eAaPmAgMnS	přirazit
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
"	"	kIx"	"
<g/>
krále	král	k1gMnSc4	král
<g/>
"	"	kIx"	"
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c6	v
poddanství	poddanství	k1gNnSc6	poddanství
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
regemque	regemque	k1gInSc1	regemque
eius	eius	k1gInSc1	eius
in	in	k?	in
deditione	dedition	k1gInSc5	dedition
accepit	accepit	k5eAaPmF	accepit
<g/>
)	)	kIx)	)
a	a	k8xC	a
učinil	učinit	k5eAaPmAgMnS	učinit
Čechy	Čechy	k1gFnPc4	Čechy
poplatnými	poplatný	k2eAgInPc7d1	poplatný
(	(	kIx(	(
<g/>
Bohemias	Bohemias	k1gInSc1	Bohemias
tributarias	tributariasa	k1gFnPc2	tributariasa
faciens	faciensa	k1gFnPc2	faciensa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
nepravidelně	pravidelně	k6eNd1	pravidelně
odváděly	odvádět	k5eAaImAgFnP	odvádět
daň	daň	k1gFnSc4	daň
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
tributum	tributum	k1gNnSc1	tributum
pacis	pacis	k1gFnPc2	pacis
<g/>
)	)	kIx)	)
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
806	[number]	k4	806
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arnulfova	Arnulfův	k2eAgFnSc1d1	Arnulfova
podpora	podpora	k1gFnSc1	podpora
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
převedení	převedení	k1gNnSc1	převedení
platební	platební	k2eAgFnSc2d1	platební
povinnosti	povinnost	k1gFnSc2	povinnost
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnPc2	jeho
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Smírné	smírný	k2eAgNnSc1d1	smírné
řešení	řešení	k1gNnSc1	řešení
a	a	k8xC	a
"	"	kIx"	"
<g/>
poddání	poddání	k1gNnSc1	poddání
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
placením	placení	k1gNnSc7	placení
daně	daň	k1gFnSc2	daň
uchránilo	uchránit	k5eAaPmAgNnS	uchránit
rodící	rodící	k2eAgInSc4d1	rodící
se	se	k3xPyFc4	se
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
před	před	k7c7	před
Jindřichovou	Jindřichův	k2eAgFnSc7d1	Jindřichova
dobyvačnou	dobyvačný	k2eAgFnSc7d1	dobyvačná
politikou	politika	k1gFnSc7	politika
uplatňovanou	uplatňovaný	k2eAgFnSc7d1	uplatňovaná
vůči	vůči	k7c3	vůči
severnějším	severní	k2eAgInPc3d2	severnější
Slovanům	Slovan	k1gInPc3	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgInSc1d1	související
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
dar	dar	k1gInSc1	dar
relikvie	relikvie	k1gFnSc2	relikvie
(	(	kIx(	(
<g/>
paže	paže	k1gFnSc2	paže
<g/>
)	)	kIx)	)
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václava	Václav	k1gMnSc4	Václav
uznával	uznávat	k5eAaImAgMnS	uznávat
za	za	k7c4	za
politického	politický	k2eAgMnSc4d1	politický
a	a	k8xC	a
křesťanského	křesťanský	k2eAgMnSc4d1	křesťanský
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
poddání	poddání	k1gNnSc1	poddání
se	se	k3xPyFc4	se
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
daň	daň	k1gFnSc1	daň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
výkladů	výklad	k1gInPc2	výklad
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
další	další	k2eAgMnPc1d1	další
je	on	k3xPp3gMnPc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
poddaní	poddaný	k1gMnPc1	poddaný
se	se	k3xPyFc4	se
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
začleňování	začleňování	k1gNnSc2	začleňování
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
(	(	kIx(	(
<g/>
až	až	k9	až
později	pozdě	k6eAd2	pozdě
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
Římské	římský	k2eAgNnSc1d1	římské
<g/>
)	)	kIx)	)
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Novotný	Novotný	k1gMnSc1	Novotný
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
(	(	kIx(	(
<g/>
chvályhodné	chvályhodný	k2eAgNnSc4d1	chvályhodné
<g/>
)	)	kIx)	)
přijetí	přijetí	k1gNnSc4	přijetí
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
méně	málo	k6eAd2	málo
vědeckými	vědecký	k2eAgInPc7d1	vědecký
výklady	výklad	k1gInPc7	výklad
<g/>
,	,	kIx,	,
od	od	k7c2	od
záchrany	záchrana	k1gFnSc2	záchrana
národa	národ	k1gInSc2	národ
před	před	k7c7	před
osudem	osud	k1gInSc7	osud
polabských	polabský	k2eAgInPc2d1	polabský
Slovanů	Slovan	k1gInPc2	Slovan
až	až	k9	až
po	po	k7c6	po
zaprodání	zaprodání	k1gNnSc6	zaprodání
země	zem	k1gFnSc2	zem
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
předmětu	předmět	k1gInSc3	předmět
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
Palacký	Palacký	k1gMnSc1	Palacký
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
údaj	údaj	k1gInSc1	údaj
mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Kosmovy	Kosmův	k2eAgFnSc2d1	Kosmova
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
bylo	být	k5eAaImAgNnS	být
odváděno	odvádět	k5eAaImNgNnS	odvádět
120	[number]	k4	120
volů	vůl	k1gMnPc2	vůl
a	a	k8xC	a
500	[number]	k4	500
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vztažení	vztažení	k1gNnSc4	vztažení
k	k	k7c3	k
synovi	syn	k1gMnSc3	syn
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
Kosmův	Kosmův	k2eAgInSc4d1	Kosmův
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
dobytka	dobytek	k1gInSc2	dobytek
je	být	k5eAaImIp3nS	být
doložená	doložená	k1gFnSc1	doložená
k	k	k7c3	k
roku	rok	k1gInSc3	rok
991	[number]	k4	991
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
tedy	tedy	k9	tedy
taková	takový	k3xDgFnSc1	takový
být	být	k5eAaImF	být
již	již	k6eAd1	již
za	za	k7c4	za
Václava	Václav	k1gMnSc4	Václav
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
teze	teze	k1gFnSc2	teze
o	o	k7c4	o
120	[number]	k4	120
volech	vůl	k1gMnPc6	vůl
a	a	k8xC	a
500	[number]	k4	500
hřivnách	hřivna	k1gFnPc6	hřivna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
historického	historický	k2eAgNnSc2d1	historické
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
spolehlivým	spolehlivý	k2eAgInSc7d1	spolehlivý
pramenem	pramen	k1gInSc7	pramen
doložená	doložená	k1gFnSc1	doložená
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
pozval	pozvat	k5eAaPmAgMnS	pozvat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Václava	Václav	k1gMnSc4	Václav
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
svátku	svátek	k1gInSc2	svátek
svatých	svatý	k1gMnPc2	svatý
Kosmy	Kosma	k1gMnSc2	Kosma
a	a	k8xC	a
Damiána	Damián	k1gMnSc2	Damián
<g/>
,	,	kIx,	,
k	k	k7c3	k
sobě	se	k3xPyFc3	se
do	do	k7c2	do
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgFnSc6d1	Staré
<g/>
)	)	kIx)	)
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
rozedněním	rozednění	k1gNnSc7	rozednění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
ranní	ranní	k2eAgFnSc4d1	ranní
pobožnost	pobožnost	k1gFnSc4	pobožnost
<g/>
.	.	kIx.	.
</s>
<s>
Potkal	potkat	k5eAaPmAgMnS	potkat
cestou	cesta	k1gFnSc7	cesta
Boleslava	Boleslav	k1gMnSc2	Boleslav
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Včera	včera	k6eAd1	včera
jsi	být	k5eAaImIp2nS	být
nám	my	k3xPp1nPc3	my
pěkně	pěkně	k6eAd1	pěkně
posloužil	posloužit	k5eAaPmAgMnS	posloužit
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
ti	ten	k3xDgMnPc1	ten
to	ten	k3xDgNnSc1	ten
oplať	oplatit	k5eAaPmRp2nS	oplatit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Boleslav	Boleslav	k1gMnSc1	Boleslav
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
dnes	dnes	k6eAd1	dnes
ti	ty	k3xPp2nSc3	ty
chci	chtít	k5eAaImIp1nS	chtít
takhle	takhle	k6eAd1	takhle
posloužit	posloužit	k5eAaPmF	posloužit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
udeřil	udeřit	k5eAaPmAgInS	udeřit
ho	on	k3xPp3gMnSc4	on
mečem	meč	k1gInSc7	meč
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Způsobil	způsobit	k5eAaPmAgInS	způsobit
mu	on	k3xPp3gNnSc3	on
jen	jen	k9	jen
menší	malý	k2eAgNnSc4d2	menší
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Václav	Václav	k1gMnSc1	Václav
mu	on	k3xPp3gMnSc3	on
vytrhl	vytrhnout	k5eAaPmAgMnS	vytrhnout
meč	meč	k1gInSc4	meč
<g/>
,	,	kIx,	,
odhodil	odhodit	k5eAaPmAgMnS	odhodit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechtěl	chtít	k5eNaImAgMnS	chtít
prolít	prolít	k5eAaPmF	prolít
krev	krev	k1gFnSc4	krev
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
Boleslav	Boleslav	k1gMnSc1	Boleslav
přivolal	přivolat	k5eAaPmAgMnS	přivolat
své	svůj	k3xOyFgMnPc4	svůj
družiníky	družiník	k1gMnPc4	družiník
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
Václava	Václav	k1gMnSc4	Václav
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
to	ten	k3xDgNnSc1	ten
alespoň	alespoň	k9	alespoň
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
legendy	legenda	k1gFnPc4	legenda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nejstarší	starý	k2eAgFnSc1d3	nejstarší
latinsky	latinsky	k6eAd1	latinsky
psaná	psaný	k2eAgFnSc1d1	psaná
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
podle	podle	k7c2	podle
začátku	začátek	k1gInSc2	začátek
textu	text	k1gInSc2	text
Crescente	Crescent	k1gInSc5	Crescent
fide	fide	k1gNnPc6	fide
christiana	christiana	k1gFnSc1	christiana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
legenda	legenda	k1gFnSc1	legenda
popisuje	popisovat	k5eAaImIp3nS	popisovat
vraždu	vražda	k1gFnSc4	vražda
podrobněji	podrobně	k6eAd2	podrobně
<g/>
:	:	kIx,	:
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
po	po	k7c6	po
hostině	hostina	k1gFnSc6	hostina
se	se	k3xPyFc4	se
spiklenci	spiklenec	k1gMnPc1	spiklenec
uradili	uradit	k5eAaPmAgMnP	uradit
s	s	k7c7	s
Boleslavem	Boleslav	k1gMnSc7	Boleslav
ve	v	k7c6	v
dvorci	dvorec	k1gInSc6	dvorec
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Hněvsy	Hněvs	k1gInPc7	Hněvs
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ranním	ranní	k2eAgInSc6d1	ranní
střetu	střet	k1gInSc6	střet
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
odehrát	odehrát	k5eAaPmF	odehrát
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
<g/>
,	,	kIx,	,
přispěchal	přispěchat	k5eAaPmAgMnS	přispěchat
nejdřív	dříve	k6eAd3	dříve
Tuža	Tuža	k1gMnSc1	Tuža
a	a	k8xC	a
ťal	tít	k5eAaPmAgMnS	tít
Václava	Václav	k1gMnSc4	Václav
do	do	k7c2	do
paže	paže	k1gFnSc2	paže
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
utíkal	utíkat	k5eAaImAgMnS	utíkat
schovat	schovat	k5eAaPmF	schovat
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dveří	dveře	k1gFnPc2	dveře
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
nejspíš	nejspíš	k9	nejspíš
zavřené	zavřený	k2eAgFnPc1d1	zavřená
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc2	on
Tira	Tir	k1gInSc2	Tir
a	a	k8xC	a
Česta	Često	k1gNnPc4	Često
ubili	ubít	k5eAaPmAgMnP	ubít
a	a	k8xC	a
Hněvsa	Hněvs	k1gMnSc4	Hněvs
probodl	probodnout	k5eAaPmAgMnS	probodnout
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
Krastěj	Krastěj	k1gMnSc1	Krastěj
tělo	tělo	k1gNnSc4	tělo
přikryl	přikrýt	k5eAaPmAgMnS	přikrýt
<g/>
,	,	kIx,	,
pak	pak	k8xC	pak
jej	on	k3xPp3gMnSc4	on
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
odnesla	odnést	k5eAaPmAgFnS	odnést
do	do	k7c2	do
knězova	knězův	k2eAgInSc2d1	knězův
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ošetřila	ošetřit	k5eAaPmAgFnS	ošetřit
a	a	k8xC	a
odnesla	odnést	k5eAaPmAgFnS	odnést
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Pobožnost	pobožnost	k1gFnSc4	pobožnost
nechal	nechat	k5eAaPmAgMnS	nechat
vykonat	vykonat	k5eAaPmF	vykonat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
povolal	povolat	k5eAaPmAgMnS	povolat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
kněze	kněz	k1gMnSc2	kněz
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
hlavní	hlavní	k2eAgMnSc1d1	hlavní
pražský	pražský	k2eAgMnSc1d1	pražský
kněz	kněz	k1gMnSc1	kněz
již	již	k6eAd1	již
od	od	k7c2	od
časů	čas	k1gInPc2	čas
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
a	a	k8xC	a
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
byli	být	k5eAaImAgMnP	být
i	i	k9	i
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
stoupenci	stoupenec	k1gMnPc1	stoupenec
včetně	včetně	k7c2	včetně
urozeného	urozený	k2eAgMnSc2d1	urozený
muže	muž	k1gMnSc2	muž
Mstiny	Mstina	k1gMnSc2	Mstina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kristiánovy	Kristiánův	k2eAgFnSc2d1	Kristiánova
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc4	život
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vrahové	vrah	k1gMnPc1	vrah
svatého	svatý	k2eAgMnSc2d1	svatý
mučedníka	mučedník	k1gMnSc2	mučedník
rychlou	rychlý	k2eAgFnSc7d1	rychlá
jízdou	jízda	k1gFnSc7	jízda
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
pospíšili	pospíšit	k5eAaPmAgMnP	pospíšit
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
rozličným	rozličný	k2eAgInSc7d1	rozličný
způsobem	způsob	k1gInSc7	způsob
krutě	krutě	k6eAd1	krutě
zahubili	zahubit	k5eAaPmAgMnP	zahubit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
zaživa	zaživa	k6eAd1	zaživa
do	do	k7c2	do
hloubi	hloub	k1gFnSc2	hloub
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
potopili	potopit	k5eAaPmAgMnP	potopit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
události	událost	k1gFnSc3	událost
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
u	u	k7c2	u
nezachovaného	zachovaný	k2eNgInSc2d1	nezachovaný
kostelíka	kostelík	k1gInSc2	kostelík
svatých	svatý	k1gMnPc2	svatý
Kosmy	Kosma	k1gMnSc2	Kosma
a	a	k8xC	a
Damiána	Damián	k1gMnSc2	Damián
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
krypta	krypta	k1gFnSc1	krypta
stejného	stejný	k2eAgNnSc2d1	stejné
zasvěcení	zasvěcení	k1gNnSc2	zasvěcení
pod	pod	k7c7	pod
dnešním	dnešní	k2eAgInSc7d1	dnešní
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Legendy	legenda	k1gFnPc1	legenda
uvádějí	uvádět	k5eAaImIp3nP	uvádět
datum	datum	k1gNnSc4	datum
zavraždění	zavraždění	k1gNnSc2	zavraždění
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadají	připadat	k5eAaPmIp3nP	připadat
roky	rok	k1gInPc1	rok
929	[number]	k4	929
a	a	k8xC	a
935	[number]	k4	935
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
pondělí	pondělí	k1gNnSc2	pondělí
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
pramenů	pramen	k1gInPc2	pramen
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
otázku	otázka	k1gFnSc4	otázka
data	datum	k1gNnSc2	datum
úmrtí	úmrť	k1gFnPc2	úmrť
definitivně	definitivně	k6eAd1	definitivně
rozřešit	rozřešit	k5eAaPmF	rozřešit
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
rok	rok	k1gInSc1	rok
935	[number]	k4	935
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
jako	jako	k9	jako
pravděpodobnějšímu	pravděpodobný	k2eAgInSc3d2	Pravděpodobnější
došla	dojít	k5eAaPmAgFnS	dojít
řada	řada	k1gFnSc1	řada
historiků	historik	k1gMnPc2	historik
poměrně	poměrně	k6eAd1	poměrně
složitými	složitý	k2eAgInPc7d1	složitý
rozbory	rozbor	k1gInPc7	rozbor
a	a	k8xC	a
porovnáním	porovnání	k1gNnSc7	porovnání
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
asi	asi	k9	asi
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
uváděl	uvádět	k5eAaImAgInS	uvádět
rok	rok	k1gInSc1	rok
929	[number]	k4	929
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
legendách	legenda	k1gFnPc6	legenda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
sepsány	sepsat	k5eAaPmNgFnP	sepsat
až	až	k9	až
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
desetiletí	desetiletí	k1gNnPc2	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
rok	rok	k1gInSc1	rok
zpětně	zpětně	k6eAd1	zpětně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
svatořečeným	svatořečený	k2eAgMnSc7d1	svatořečený
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
příslušníkem	příslušník	k1gMnSc7	příslušník
první	první	k4xOgFnSc2	první
dynastie	dynastie	k1gFnSc2	dynastie
panovníků	panovník	k1gMnPc2	panovník
-	-	kIx~	-
rodu	rod	k1gInSc6	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
symbolem	symbol	k1gInSc7	symbol
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
patronem	patron	k1gMnSc7	patron
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
českých	český	k2eAgMnPc2d1	český
svatých	svatý	k1gMnPc2	svatý
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
tradice	tradice	k1gFnSc1	tradice
sehrála	sehrát	k5eAaPmAgFnS	sehrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
emancipaci	emancipace	k1gFnSc6	emancipace
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
posmrtně	posmrtně	k6eAd1	posmrtně
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
Václavův	Václavův	k2eAgInSc4d1	Václavův
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
pokání	pokání	k1gNnSc6	pokání
sám	sám	k3xTgMnSc1	sám
vrah	vrah	k1gMnSc1	vrah
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
bratrově	bratrův	k2eAgFnSc6d1	bratrova
smrti	smrt	k1gFnSc6	smrt
nechal	nechat	k5eAaPmAgMnS	nechat
jeho	jeho	k3xOp3gInPc4	jeho
ostatky	ostatek	k1gInPc4	ostatek
převézt	převézt	k5eAaPmF	převézt
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
kult	kult	k1gInSc1	kult
byl	být	k5eAaImAgInS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
v	v	k7c6	v
církevním	církevní	k2eAgNnSc6d1	církevní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnPc1d2	mladší
první	první	k4xOgFnPc1	první
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
legendy	legenda	k1gFnPc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
projevuje	projevovat	k5eAaImIp3nS	projevovat
dvojí	dvojí	k4xRgInSc4	dvojí
význam	význam	k1gInSc4	význam
Václava	Václav	k1gMnSc2	Václav
<g/>
:	:	kIx,	:
jako	jako	k9	jako
světce	světec	k1gMnSc2	světec
vyznačujícího	vyznačující	k2eAgMnSc2d1	vyznačující
se	s	k7c7	s
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
ctnostmi	ctnost	k1gFnPc7	ctnost
a	a	k8xC	a
jako	jako	k9	jako
symbolu	symbol	k1gInSc2	symbol
vládce	vládce	k1gMnPc4	vládce
<g/>
,	,	kIx,	,
přemyslovské	přemyslovský	k2eAgMnPc4d1	přemyslovský
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dvojí	dvojit	k5eAaImIp3nS	dvojit
pojetí	pojetí	k1gNnSc4	pojetí
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
proměnách	proměna	k1gFnPc6	proměna
objevuje	objevovat	k5eAaImIp3nS	objevovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
následující	následující	k2eAgFnSc4d1	následující
historii	historie	k1gFnSc4	historie
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přenesení	přenesení	k1gNnSc6	přenesení
(	(	kIx(	(
<g/>
translaci	translace	k1gFnSc6	translace
<g/>
)	)	kIx)	)
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rovnalo	rovnat	k5eAaImAgNnS	rovnat
dnešní	dnešní	k2eAgFnSc3d1	dnešní
papežské	papežský	k2eAgFnSc3d1	Papežská
kanonizaci	kanonizace	k1gFnSc3	kanonizace
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
nedochovaná	dochovaný	k2eNgFnSc1d1	nedochovaná
legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Slavisté	slavista	k1gMnPc1	slavista
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
První	první	k4xOgFnSc4	první
staroslověnskou	staroslověnský	k2eAgFnSc4d1	staroslověnská
legendu	legenda	k1gFnSc4	legenda
a	a	k8xC	a
translaci	translace	k1gFnSc4	translace
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
pražskou	pražský	k2eAgFnSc7d1	Pražská
tradicí	tradice	k1gFnSc7	tradice
spojují	spojovat	k5eAaImIp3nP	spojovat
se	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
rokem	rok	k1gInSc7	rok
po	po	k7c6	po
Václavově	Václavův	k2eAgFnSc6d1	Václavova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
932	[number]	k4	932
event.	event.	k?	event.
938	[number]	k4	938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mladší	mladý	k2eAgMnPc1d2	mladší
badatelé	badatel	k1gMnPc1	badatel
(	(	kIx(	(
<g/>
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
naopak	naopak	k6eAd1	naopak
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
o	o	k7c6	o
latinské	latinský	k2eAgFnSc6d1	Latinská
legendě	legenda	k1gFnSc6	legenda
a	a	k8xC	a
o	o	k7c6	o
úzké	úzký	k2eAgFnSc6d1	úzká
souvislosti	souvislost	k1gFnSc6	souvislost
mezi	mezi	k7c7	mezi
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
<g/>
.	.	kIx.	.
</s>
<s>
Biskupství	biskupství	k1gNnSc1	biskupství
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
hroby	hrob	k1gInPc7	hrob
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
rotundě	rotunda	k1gFnSc6	rotunda
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc4	Vít
a	a	k8xC	a
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
,	,	kIx,	,
babičky	babička	k1gFnSc2	babička
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
<g/>
,	,	kIx,	,
v	v	k7c6	v
předrománském	předrománský	k2eAgInSc6d1	předrománský
chrámu	chrám	k1gInSc6	chrám
(	(	kIx(	(
<g/>
bazilice	bazilika	k1gFnSc6	bazilika
<g/>
)	)	kIx)	)
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
973	[number]	k4	973
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
976	[number]	k4	976
byla	být	k5eAaImAgFnS	být
jistě	jistě	k9	jistě
v	v	k7c6	v
řezenském	řezenský	k2eAgNnSc6d1	řezenské
prostředí	prostředí	k1gNnSc6	prostředí
sepsána	sepsán	k2eAgFnSc1d1	sepsána
jiná	jiný	k2eAgFnSc1d1	jiná
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
latinská	latinský	k2eAgFnSc1d1	Latinská
Crescente	Crescent	k1gInSc5	Crescent
fide	fide	k1gInSc4	fide
christiana	christian	k1gMnSc4	christian
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
víra	víra	k1gFnSc1	víra
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Legenda	legenda	k1gFnSc1	legenda
tak	tak	k6eAd1	tak
řečeného	řečený	k2eAgMnSc2d1	řečený
Kristiána	Kristián	k1gMnSc2	Kristián
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
některými	některý	k3yIgFnPc7	některý
badateli	badatel	k1gMnPc7	badatel
pokládána	pokládat	k5eAaImNgNnP	pokládat
za	za	k7c4	za
falzum	falzum	k1gNnSc4	falzum
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úcta	úcta	k1gFnSc1	úcta
ke	k	k7c3	k
sv.	sv.	kA	sv.
Václavu	Václava	k1gFnSc4	Václava
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
už	už	k6eAd1	už
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
staroslověnským	staroslověnský	k2eAgFnPc3d1	staroslověnská
legendám	legenda	k1gFnPc3	legenda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gInSc1	jeho
kult	kult	k1gInSc1	kult
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
Rus	Rus	k1gFnSc4	Rus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
tamní	tamní	k2eAgInPc1d1	tamní
rukopisy	rukopis	k1gInPc1	rukopis
tzv.	tzv.	kA	tzv.
První	první	k4xOgFnPc1	první
staroslověnské	staroslověnský	k2eAgFnPc1d1	staroslověnská
legendy	legenda	k1gFnPc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
legendách	legenda	k1gFnPc6	legenda
svatoludmilských	svatoludmilský	k2eAgMnPc2d1	svatoludmilský
(	(	kIx(	(
<g/>
Fuit	Fuit	k1gMnSc1	Fuit
in	in	k?	in
Provincia	Provincia	k1gFnSc1	Provincia
Bohemorum	Bohemorum	k1gInSc1	Bohemorum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úctu	úcta	k1gFnSc4	úcta
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
zase	zase	k9	zase
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
jak	jak	k6eAd1	jak
Gumpoldova	Gumpoldův	k2eAgFnSc1d1	Gumpoldova
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
980	[number]	k4	980
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
císaře	císař	k1gMnSc2	císař
Oty	Ota	k1gMnSc2	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
rozšíření	rozšíření	k1gNnSc1	rozšíření
rukopisů	rukopis	k1gInPc2	rukopis
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
ostatků	ostatek	k1gInPc2	ostatek
v	v	k7c6	v
říšských	říšský	k2eAgInPc6d1	říšský
kostelech	kostel	k1gInPc6	kostel
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
svaté	svatý	k2eAgFnSc6d1	svatá
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
historiích	historie	k1gFnPc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
světcem	světec	k1gMnSc7	světec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
kalendáře	kalendář	k1gInSc2	kalendář
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavské	svatováclavský	k2eAgNnSc1d1	Svatováclavské
milénium	milénium	k1gNnSc1	milénium
bylo	být	k5eAaImAgNnS	být
slaveno	slavit	k5eAaImNgNnS	slavit
za	za	k7c4	za
První	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
otevření	otevření	k1gNnSc6	otevření
dostavěné	dostavěný	k2eAgFnSc2d1	dostavěná
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Františka	František	k1gMnSc2	František
Kordače	Kordač	k1gMnSc2	Kordač
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
veřejné	veřejný	k2eAgNnSc4d1	veřejné
miléniové	miléniový	k2eAgNnSc4d1	miléniový
cvičení	cvičení	k1gNnSc4	cvičení
orelských	orelský	k2eAgFnPc2d1	orelská
jednot	jednota	k1gFnPc2	jednota
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1929	[number]	k4	1929
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Jubilejní	jubilejní	k2eAgFnSc4d1	jubilejní
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
ke	k	k7c3	k
svatováclavskému	svatováclavský	k2eAgNnSc3d1	Svatováclavské
miléniu	milénium	k1gNnSc3	milénium
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
si	se	k3xPyFc3	se
Masaryk	Masaryk	k1gMnSc1	Masaryk
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
soukromě	soukromě	k6eAd1	soukromě
den	den	k1gInSc4	den
předem	předem	k6eAd1	předem
<g/>
,	,	kIx,	,
zrána	zrána	k6eAd1	zrána
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Slavilo	slavit	k5eAaImAgNnS	slavit
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
předal	předat	k5eAaPmAgMnS	předat
osobně	osobně	k6eAd1	osobně
jubilejní	jubilejní	k2eAgFnSc4d1	jubilejní
standartu	standarta	k1gFnSc4	standarta
Jezdeckému	jezdecký	k2eAgInSc3d1	jezdecký
pluku	pluk	k1gInSc3	pluk
8	[number]	k4	8
"	"	kIx"	"
<g/>
Knížete	kníže	k1gNnSc4wR	kníže
Václava	Václav	k1gMnSc2	Václav
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
A.	A.	kA	A.
G.	G.	kA	G.
Roncalliho	Roncalli	k1gMnSc2	Roncalli
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
dostalo	dostat	k5eAaPmAgNnS	dostat
tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
i	i	k8xC	i
náměstí	náměstí	k1gNnSc1	náměstí
před	před	k7c7	před
Dómem	dóm	k1gInSc7	dóm
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihovanými	vyzdvihovaný	k2eAgFnPc7d1	vyzdvihovaná
přednostmi	přednost	k1gFnPc7	přednost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
zbožný	zbožný	k2eAgInSc4d1	zbožný
a	a	k8xC	a
mravný	mravný	k2eAgInSc4d1	mravný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
horlivost	horlivost	k1gFnSc4	horlivost
pro	pro	k7c4	pro
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
národě	národ	k1gInSc6	národ
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
četných	četný	k2eAgInPc2d1	četný
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
častá	častý	k2eAgFnSc1d1	častá
návštěva	návštěva	k1gFnSc1	návštěva
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
,	,	kIx,	,
všestranné	všestranný	k2eAgNnSc1d1	všestranné
konání	konání	k1gNnSc1	konání
skutků	skutek	k1gInPc2	skutek
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
ctil	ctít	k5eAaImAgMnS	ctít
svou	svůj	k3xOyFgFnSc4	svůj
babičku	babička	k1gFnSc4	babička
<g/>
,	,	kIx,	,
pečoval	pečovat	k5eAaImAgMnS	pečovat
o	o	k7c4	o
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
,	,	kIx,	,
nemocné	nemocný	k2eAgMnPc4d1	nemocný
a	a	k8xC	a
sirotky	sirotek	k1gMnPc4	sirotek
<g/>
,	,	kIx,	,
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
přístřeší	přístřeší	k1gNnSc4	přístřeší
a	a	k8xC	a
pohostinství	pohostinství	k1gNnSc4	pohostinství
pocestným	pocestný	k1gMnPc3	pocestný
a	a	k8xC	a
cizincům	cizinec	k1gMnPc3	cizinec
a	a	k8xC	a
netrpěl	trpět	k5eNaImAgMnS	trpět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
komukoliv	kdokoliv	k3yInSc3	kdokoliv
stala	stát	k5eAaPmAgFnS	stát
křivda	křivda	k1gFnSc1	křivda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
osobně	osobně	k6eAd1	osobně
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kníže	kníže	k1gMnSc1	kníže
byl	být	k5eAaImAgMnS	být
výborný	výborný	k2eAgInSc4d1	výborný
jezdec	jezdec	k1gInSc4	jezdec
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
událo	udát	k5eAaPmAgNnS	udát
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInSc7	jeho
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
resp	resp	kA	resp
28	[number]	k4	28
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
až	až	k6eAd1	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Václavská	václavský	k2eAgFnSc1d1	Václavská
hagiografie	hagiografie	k1gFnSc1	hagiografie
<g/>
.	.	kIx.	.
</s>
<s>
Svatému	svatý	k2eAgMnSc3d1	svatý
Václavovi	Václav	k1gMnSc3	Václav
byly	být	k5eAaImAgInP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
různé	různý	k2eAgInPc1d1	různý
zázraky	zázrak	k1gInPc1	zázrak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
dosvědčovat	dosvědčovat	k5eAaImF	dosvědčovat
jeho	jeho	k3xOp3gFnSc4	jeho
svatost	svatost	k1gFnSc4	svatost
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
pokořením	pokoření	k1gNnSc7	pokoření
kouřimského	kouřimský	k2eAgMnSc2d1	kouřimský
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Radslava	Radslav	k1gMnSc2	Radslav
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
údajně	údajně	k6eAd1	údajně
uviděl	uvidět	k5eAaPmAgMnS	uvidět
na	na	k7c6	na
Václavově	Václavův	k2eAgNnSc6d1	Václavovo
čele	čelo	k1gNnSc6	čelo
zářící	zářící	k2eAgNnSc1d1	zářící
znamení	znamení	k1gNnSc1	znamení
kříže	kříž	k1gInSc2	kříž
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
anděly	anděl	k1gMnPc7	anděl
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
údajně	údajně	k6eAd1	údajně
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
kouřimského	kouřimský	k2eAgMnSc4d1	kouřimský
vévodu	vévoda	k1gMnSc4	vévoda
k	k	k7c3	k
osobnímu	osobní	k2eAgInSc3d1	osobní
souboji	souboj	k1gInSc3	souboj
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnes	dnes	k6eAd1	dnes
nazývaném	nazývaný	k2eAgNnSc6d1	nazývané
Přistoupim	Přistoupim	k?	Přistoupim
(	(	kIx(	(
<g/>
stojí	stát	k5eAaImIp3nS	stát
tam	tam	k6eAd1	tam
barokní	barokní	k2eAgFnSc1d1	barokní
plastika	plastika	k1gFnSc1	plastika
Václavova	Václavův	k2eAgFnSc1d1	Václavova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
vidění	vidění	k1gNnSc6	vidění
zlický	zlický	k2eAgMnSc1d1	zlický
kníže	kníže	k1gMnSc1	kníže
sesedl	sesednout	k5eAaPmAgMnS	sesednout
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
pravil	pravit	k5eAaImAgMnS	pravit
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
k	k	k7c3	k
Václavovi	Václav	k1gMnSc3	Václav
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přistoupim	Přistoupim	k?	Přistoupim
na	na	k7c4	na
Tvé	tvůj	k3xOp2gFnPc4	tvůj
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
zázrak	zázrak	k1gInSc1	zázrak
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
udál	udát	k5eAaPmAgInS	udát
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Ptáčník	Ptáčník	k1gMnSc1	Ptáčník
se	se	k3xPyFc4	se
rozhněval	rozhněvat	k5eAaPmAgMnS	rozhněvat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
včas	včas	k6eAd1	včas
na	na	k7c4	na
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
a	a	k8xC	a
přikázal	přikázat	k5eAaPmAgMnS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
nikdo	nikdo	k3yNnSc1	nikdo
neprojevoval	projevovat	k5eNaImAgMnS	projevovat
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
zůstali	zůstat	k5eAaPmAgMnP	zůstat
sedět	sedět	k5eAaImF	sedět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgMnS	přijít
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
vstal	vstát	k5eAaPmAgMnS	vstát
a	a	k8xC	a
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
uviděl	uvidět	k5eAaPmAgInS	uvidět
zlatý	zlatý	k2eAgInSc4d1	zlatý
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInPc6	jeho
bocích	bok	k1gInPc6	bok
dva	dva	k4xCgMnPc4	dva
anděly	anděl	k1gMnPc4	anděl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
hrozili	hrozit	k5eAaImAgMnP	hrozit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sněmu	sněm	k1gInSc2	sněm
otevřel	otevřít	k5eAaPmAgMnS	otevřít
královskou	královský	k2eAgFnSc4d1	královská
pokladnici	pokladnice	k1gFnSc4	pokladnice
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
dar	dar	k1gInSc4	dar
podle	podle	k7c2	podle
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
relikviář	relikviář	k1gInSc4	relikviář
s	s	k7c7	s
ostatkem	ostatek	k1gInSc7	ostatek
ruky	ruka	k1gFnSc2	ruka
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
rotundu	rotunda	k1gFnSc4	rotunda
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
zázraku	zázrak	k1gInSc2	zázrak
bylo	být	k5eAaImAgNnS	být
hledáno	hledat	k5eAaImNgNnS	hledat
ve	v	k7c6	v
zlatém	zlatý	k2eAgInSc6d1	zlatý
nánosníku	nánosník	k1gInSc6	nánosník
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
přilbě	přilba	k1gFnSc6	přilba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
mladšího	mladý	k2eAgInSc2d2	mladší
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
zázraky	zázrak	k1gInPc1	zázrak
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
udát	udát	k5eAaPmF	udát
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
události	událost	k1gFnPc1	událost
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
přenesení	přenesení	k1gNnSc6	přenesení
vozu	vůz	k1gInSc2	vůz
přes	přes	k7c4	přes
rozvodněnou	rozvodněný	k2eAgFnSc4d1	rozvodněná
řeku	řeka	k1gFnSc4	řeka
a	a	k8xC	a
zhojené	zhojený	k2eAgFnPc4d1	zhojená
rány	rána	k1gFnPc4	rána
při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
ostatků	ostatek	k1gInPc2	ostatek
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
zázraky	zázrak	k1gInPc1	zázrak
se	se	k3xPyFc4	se
děly	dít	k5eAaBmAgInP	dít
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
uložení	uložení	k1gNnSc4	uložení
v	v	k7c6	v
rotundě	rotunda	k1gFnSc6	rotunda
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc4	osvobození
ze	z	k7c2	z
žaláře	žalář	k1gInSc2	žalář
a	a	k8xC	a
okovů	okov	k1gInPc2	okov
a	a	k8xC	a
uzdravení	uzdravení	k1gNnPc2	uzdravení
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
modlili	modlit	k5eAaImAgMnP	modlit
k	k	k7c3	k
mučedníku	mučedník	k1gMnSc3	mučedník
Václavovi	Václav	k1gMnSc3	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
všech	všecek	k3xTgFnPc2	všecek
legend	legenda	k1gFnPc2	legenda
Svatováclavských	svatováclavský	k2eAgFnPc2d1	Svatováclavská
a	a	k8xC	a
svatoludmilských	svatoludmilský	k2eAgFnPc2d1	svatoludmilský
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
závěr	závěr	k1gInSc4	závěr
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čím	co	k3yInSc7	co
starší	starý	k2eAgFnSc1d2	starší
legenda	legenda	k1gFnSc1	legenda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
zázraků	zázrak	k1gInPc2	zázrak
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zázraky	zázrak	k1gInPc1	zázrak
totiž	totiž	k9	totiž
přibývají	přibývat	k5eAaImIp3nP	přibývat
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
stal	stát	k5eAaPmAgMnS	stát
symbolem	symbol	k1gInSc7	symbol
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kodex	kodex	k1gInSc1	kodex
vyšehradský	vyšehradský	k2eAgInSc1d1	vyšehradský
-	-	kIx~	-
vydaný	vydaný	k2eAgInSc1d1	vydaný
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
prvního	první	k4xOgMnSc2	první
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
zpodobněn	zpodobnit	k5eAaPmNgInS	zpodobnit
trůnící	trůnící	k2eAgMnPc1d1	trůnící
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
na	na	k7c6	na
iluminaci	iluminace	k1gFnSc6	iluminace
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
mincí	mince	k1gFnPc2	mince
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
"	"	kIx"	"
<g/>
věčný	věčný	k2eAgMnSc1d1	věčný
panovník	panovník	k1gMnSc1	panovník
<g/>
"	"	kIx"	"
pomník	pomník	k1gInSc1	pomník
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
symbolický	symbolický	k2eAgInSc4d1	symbolický
střed	střed	k1gInSc4	střed
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
místo	místo	k7c2	místo
národních	národní	k2eAgFnPc2d1	národní
demonstrací	demonstrace	k1gFnPc2	demonstrace
Méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
již	již	k9	již
výrok	výrok	k1gInSc1	výrok
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
zachycený	zachycený	k2eAgInSc1d1	zachycený
i	i	k9	i
v	v	k7c6	v
Čapkových	Čapkových	k2eAgInPc6d1	Čapkových
"	"	kIx"	"
<g/>
Hovorech	hovor	k1gInPc6	hovor
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
:	:	kIx,	:
"	"	kIx"	"
<g/>
Humanita	humanita	k1gFnSc1	humanita
<g/>
,	,	kIx,	,
toť	toť	k?	toť
náš	náš	k3xOp1gInSc4	náš
národní	národní	k2eAgInSc4d1	národní
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
<g/>
,	,	kIx,	,
Kollára	Kollár	k1gMnSc2	Kollár
<g/>
,	,	kIx,	,
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
Havlíčka	Havlíček	k1gMnSc2	Havlíček
a	a	k8xC	a
už	už	k6eAd1	už
Komenského	Komenský	k1gMnSc4	Komenský
<g/>
,	,	kIx,	,
králů	král	k1gMnPc2	král
Jiříka	Jiřík	k1gMnSc2	Jiřík
i	i	k8xC	i
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
podoba	podoba	k1gFnSc1	podoba
"	"	kIx"	"
<g/>
svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
orientace	orientace	k1gFnSc2	orientace
<g/>
"	"	kIx"	"
zrozené	zrozený	k2eAgFnSc2d1	zrozená
v	v	k7c6	v
novodobých	novodobý	k2eAgFnPc6d1	novodobá
dějinách	dějiny	k1gFnPc6	dějiny
jako	jako	k8xC	jako
koncepce	koncepce	k1gFnSc2	koncepce
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
ideologicky	ideologicky	k6eAd1	ideologicky
pokřivený	pokřivený	k2eAgInSc4d1	pokřivený
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svatého	svatý	k2eAgMnSc4d1	svatý
Václava	Václav	k1gMnSc4	Václav
byly	být	k5eAaImAgInP	být
zneužity	zneužít	k5eAaPmNgInP	zneužít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
jedoucí	jedoucí	k2eAgInSc1d1	jedoucí
Sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
dokonce	dokonce	k9	dokonce
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
pětitisícové	pětitisícový	k2eAgFnSc6d1	pětitisícová
bankovce	bankovka	k1gFnSc6	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
orlice	orlice	k1gFnSc1	orlice
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
protektorátním	protektorátní	k2eAgNnSc7d1	protektorátní
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
kolaborovali	kolaborovat	k5eAaImAgMnP	kolaborovat
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
i	i	k9	i
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
dvacetikorunové	dvacetikorunový	k2eAgFnSc6d1	dvacetikorunová
minci	mince	k1gFnSc6	mince
(	(	kIx(	(
<g/>
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
nedej	dát	k5eNaPmRp2nS	dát
zahynouti	zahynout	k5eAaPmF	zahynout
nám	my	k3xPp1nPc3	my
i	i	k9	i
budoucím	budoucí	k2eAgMnPc3d1	budoucí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
československých	československý	k2eAgFnPc6d1	Československá
známkách	známka	k1gFnPc6	známka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
a	a	k8xC	a
nových	nový	k2eAgFnPc6d1	nová
poštovních	poštovní	k2eAgFnPc6d1	poštovní
známkách	známka	k1gFnPc6	známka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
velikostech	velikost	k1gFnPc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
umučení	umučení	k1gNnSc2	umučení
knížete	kníže	k1gMnSc2	kníže
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
oslavujeme	oslavovat	k5eAaImIp1nP	oslavovat
Den	den	k1gInSc4	den
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
i	i	k9	i
Den	den	k1gInSc4	den
přenesení	přenesení	k1gNnSc2	přenesení
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc4d1	připomínající
transfer	transfer	k1gInSc4	transfer
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
932	[number]	k4	932
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
938	[number]	k4	938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
ve	v	k7c6	v
Svatováclavských	svatováclavský	k2eAgFnPc6d1	Svatováclavská
legendách	legenda	k1gFnPc6	legenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
putoval	putovat	k5eAaImAgInS	putovat
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
ostatek	ostatek	k1gInSc1	ostatek
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
a	a	k8xC	a
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
doprovod	doprovod	k1gInSc1	doprovod
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
katolických	katolický	k2eAgFnPc6d1	katolická
farnostech	farnost	k1gFnPc6	farnost
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
nebo	nebo	k8xC	nebo
katolické	katolický	k2eAgInPc1d1	katolický
oddíly	oddíl	k1gInPc1	oddíl
junáků	junák	k1gMnPc2	junák
skautů	skaut	k1gMnPc2	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
přijat	přijmout	k5eAaPmNgInS	přijmout
za	za	k7c2	za
ochránce	ochránce	k1gMnSc2	ochránce
(	(	kIx(	(
<g/>
patrona	patrona	k1gFnSc1	patrona
<g/>
)	)	kIx)	)
českých	český	k2eAgMnPc2d1	český
junáků	junák	k1gMnPc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
junáky	junák	k1gMnPc4	junák
vzorem	vzor	k1gInSc7	vzor
v	v	k7c6	v
pomoci	pomoc	k1gFnSc6	pomoc
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
v	v	k7c6	v
statečném	statečný	k2eAgMnSc6d1	statečný
a	a	k8xC	a
laskavém	laskavý	k2eAgNnSc6d1	laskavé
jednání	jednání	k1gNnSc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
odkazem	odkaz	k1gInSc7	odkaz
pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
junáky	junák	k1gMnPc4	junák
je	být	k5eAaImIp3nS	být
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
odpuštění	odpuštění	k1gNnSc4	odpuštění
bratru	bratr	k1gMnSc3	bratr
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgFnPc1d1	uložena
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
ve	v	k7c6	v
svatováclavské	svatováclavský	k2eAgFnSc6d1	Svatováclavská
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
vystavována	vystavován	k2eAgFnSc1d1	vystavována
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
zdobeném	zdobený	k2eAgInSc6d1	zdobený
polštáři	polštář	k1gInSc6	polštář
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
korunu	koruna	k1gFnSc4	koruna
s	s	k7c7	s
čelenkou	čelenka	k1gFnSc7	čelenka
<g/>
,	,	kIx,	,
věnovanou	věnovaný	k2eAgFnSc7d1	věnovaná
v	v	k7c6	v
době	doba	k1gFnSc6	doba
První	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
klubem	klub	k1gInSc7	klub
dam	dáma	k1gFnPc2	dáma
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nařízení	nařízení	k1gNnSc2	nařízení
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
spočívat	spočívat	k5eAaImF	spočívat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mimo	mimo	k7c4	mimo
korunovační	korunovační	k2eAgFnSc4d1	korunovační
ceremonii	ceremonie	k1gFnSc4	ceremonie
tzv.	tzv.	kA	tzv.
svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Karel	Karel	k1gMnSc1	Karel
symbolicky	symbolicky	k6eAd1	symbolicky
věnoval	věnovat	k5eAaImAgMnS	věnovat
českému	český	k2eAgMnSc3d1	český
světci	světec	k1gMnSc3	světec
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
křestnímu	křestní	k2eAgMnSc3d1	křestní
patronovi	patron	k1gMnSc3	patron
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
jménem	jméno	k1gNnSc7	jméno
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
patronovi	patron	k1gMnSc3	patron
(	(	kIx(	(
<g/>
ochránci	ochránce	k1gMnSc3	ochránce
<g/>
)	)	kIx)	)
i	i	k8xC	i
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
přemyslovským	přemyslovský	k2eAgMnPc3d1	přemyslovský
předkům	předek	k1gMnPc3	předek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
chápali	chápat	k5eAaImAgMnP	chápat
Václava	Václav	k1gMnSc4	Václav
jako	jako	k8xC	jako
patrona	patron	k1gMnSc4	patron
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
a	a	k8xC	a
od	od	k7c2	od
sklonku	sklonek	k1gInSc2	sklonek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
věčného	věčný	k2eAgMnSc4d1	věčný
knížete	kníže	k1gMnSc4	kníže
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
zbroj	zbroj	k1gFnSc1	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
zbroj	zbroj	k1gFnSc1	zbroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
přílba	přílba	k1gFnSc1	přílba
<g/>
,	,	kIx,	,
pláštík	pláštík	k1gInSc1	pláštík
<g/>
,	,	kIx,	,
kroužková	kroužkový	k2eAgFnSc1d1	kroužková
drátěná	drátěný	k2eAgFnSc1d1	drátěná
košile	košile	k1gFnSc1	košile
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
i	i	k9	i
meč	meč	k1gInSc1	meč
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
předmět	předmět	k1gInSc4	předmět
úcty	úcta	k1gFnSc2	úcta
součást	součást	k1gFnSc1	součást
svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
užívání	užívání	k1gNnSc3	užívání
Václavem	Václav	k1gMnSc7	Václav
však	však	k9	však
není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
.	.	kIx.	.
</s>
<s>
Zbroj	zbroj	k1gFnSc1	zbroj
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
mladší	mladý	k2eAgFnSc4d2	mladší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dle	dle	k7c2	dle
zjištění	zjištění	k1gNnPc2	zjištění
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2008-2013	[number]	k4	2008-2013
může	moct	k5eAaImIp3nS	moct
opravdu	opravdu	k6eAd1	opravdu
pocházet	pocházet	k5eAaImF	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
součást	součást	k1gFnSc1	součást
zbroje	zbroj	k1gFnSc2	zbroj
je	být	k5eAaImIp3nS	být
přílba	přílba	k1gFnSc1	přílba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
nánosník	nánosník	k1gInSc1	nánosník
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kříže	kříž	k1gInSc2	kříž
je	být	k5eAaImIp3nS	být
však	však	k9	však
mladšího	mladý	k2eAgMnSc4d2	mladší
původu	původa	k1gMnSc4	původa
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vyryta	vyryt	k2eAgFnSc1d1	vyryta
postava	postava	k1gFnSc1	postava
ukřižovaného	ukřižovaný	k2eAgMnSc2d1	ukřižovaný
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
tezí	teze	k1gFnPc2	teze
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
skandinávského	skandinávský	k2eAgMnSc4d1	skandinávský
boha	bůh	k1gMnSc4	bůh
Ódina	Ódin	k1gMnSc4	Ódin
vpleteného	vpletený	k2eAgMnSc4d1	vpletený
do	do	k7c2	do
Yggdrasilu	Yggdrasil	k1gInSc2	Yggdrasil
<g/>
.	.	kIx.	.
</s>
<s>
Nánosník	nánosník	k1gInSc1	nánosník
bývá	bývat	k5eAaImIp3nS	bývat
dáván	dávat	k5eAaImNgInS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
se	s	k7c7	s
zářícím	zářící	k2eAgInSc7d1	zářící
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
Kristiánovy	Kristiánův	k2eAgFnSc2d1	Kristiánova
legendy	legenda	k1gFnSc2	legenda
spatřil	spatřit	k5eAaPmAgMnS	spatřit
kouřimský	kouřimský	k2eAgMnSc1d1	kouřimský
kníže	kníže	k1gMnSc1	kníže
Radslav	Radslav	k1gMnSc1	Radslav
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
kaple	kaple	k1gFnSc1	kaple
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Stanislava	Stanislav	k1gMnSc4	Stanislav
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
A	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Národní	národní	k2eAgFnSc1d1	národní
svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výročí	výročí	k1gNnSc6	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
oslavy	oslava	k1gFnPc1	oslava
a	a	k8xC	a
pouť	pouť	k1gFnSc1	pouť
do	do	k7c2	do
Staré	Stará	k1gFnSc2	Stará
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
oslav	oslava	k1gFnPc2	oslava
aktivně	aktivně	k6eAd1	aktivně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Svatý	svatý	k2eAgMnSc1d1	svatý
Otec	otec	k1gMnSc1	otec
římský	římský	k2eAgMnSc1d1	římský
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
..	..	k?	..
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Václavská	václavský	k2eAgFnSc1d1	Václavská
hagiografie	hagiografie	k1gFnSc1	hagiografie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Václavu	Václav	k1gMnSc6	Václav
legenda	legenda	k1gFnSc1	legenda
Crescente	Crescent	k1gInSc5	Crescent
fide	fide	k1gFnSc6	fide
Kristiánova	Kristiánův	k2eAgFnSc1d1	Kristiánova
legenda	legenda	k1gFnSc1	legenda
Gumpoldova	Gumpoldův	k2eAgFnSc1d1	Gumpoldova
legenda	legenda	k1gFnSc1	legenda
Laurentiova	Laurentiův	k2eAgFnSc1d1	Laurentiův
(	(	kIx(	(
<g/>
Vavřincova	Vavřincův	k2eAgFnSc1d1	Vavřincova
<g/>
)	)	kIx)	)
legenda	legenda	k1gFnSc1	legenda
Druhá	druhý	k4xOgFnSc1	druhý
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Václavu	Václav	k1gMnSc6	Václav
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
kronika	kronika	k1gFnSc1	kronika
kronika	kronika	k1gFnSc1	kronika
saského	saský	k2eAgMnSc2d1	saský
Widukinda	Widukind	k1gMnSc2	Widukind
Nejstarší	starý	k2eAgNnSc4d3	nejstarší
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
iluminovaném	iluminovaný	k2eAgInSc6d1	iluminovaný
rukopisu	rukopis	k1gInSc6	rukopis
tzv.	tzv.	kA	tzv.
Gumpoldovy	Gumpoldův	k2eAgFnSc2d1	Gumpoldova
legendy	legenda	k1gFnSc2	legenda
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
stříbrných	stříbrný	k2eAgFnPc6d1	stříbrná
českých	český	k2eAgFnPc6d1	Česká
mincích	mince	k1gFnPc6	mince
denárech	denár	k1gInPc6	denár
<g/>
,	,	kIx,	,
pečetích	pečeť	k1gFnPc6	pečeť
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
iluminovaných	iluminovaný	k2eAgInPc6d1	iluminovaný
rukopisech	rukopis	k1gInPc6	rukopis
doby	doba	k1gFnSc2	doba
románské	románský	k2eAgFnSc2d1	románská
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejkrásnějším	krásný	k2eAgMnPc3d3	nejkrásnější
patří	patřit	k5eAaImIp3nS	patřit
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
pařížském	pařížský	k2eAgNnSc6d1	pařížské
Zlomku	zlomek	k1gInSc6	zlomek
Dalimilovy	Dalimilův	k2eAgFnPc1d1	Dalimilova
kroniky	kronika	k1gFnPc1	kronika
z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
legenda	legenda	k1gFnSc1	legenda
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Velislavovy	Velislavův	k2eAgFnSc2d1	Velislavova
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
cyklus	cyklus	k1gInSc1	cyklus
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
také	také	k9	také
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
věži	věž	k1gFnSc6	věž
hradu	hrad	k1gInSc2	hrad
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
přístupového	přístupový	k2eAgNnSc2d1	přístupové
schodiště	schodiště	k1gNnSc2	schodiště
do	do	k7c2	do
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
votivní	votivní	k2eAgInSc1d1	votivní
obraz	obraz	k1gInSc1	obraz
druhého	druhý	k4xOgMnSc2	druhý
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Jana	Jan	k1gMnSc2	Jan
Očka	očko	k1gNnSc2	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
vyobrazeni	vyobrazen	k2eAgMnPc1d1	vyobrazen
čtyři	čtyři	k4xCgMnPc1	čtyři
patroni	patron	k1gMnPc1	patron
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
a	a	k8xC	a
českých	český	k2eAgMnPc2d1	český
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
jako	jako	k8xS	jako
patron	patron	k1gMnSc1	patron
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pokládá	pokládat	k5eAaImIp3nS	pokládat
ruku	ruka	k1gFnSc4	ruka
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
rameno	rameno	k1gNnSc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
deskové	deskový	k2eAgFnPc1d1	desková
malby	malba	k1gFnPc1	malba
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
:	:	kIx,	:
oltářní	oltářní	k2eAgInSc4d1	oltářní
obraz	obraz	k1gInSc4	obraz
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
retáblu	retábl	k1gInSc6	retábl
od	od	k7c2	od
italského	italský	k2eAgMnSc2d1	italský
malíře	malíř	k1gMnSc2	malíř
Tomáše	Tomáš	k1gMnSc2	Tomáš
z	z	k7c2	z
Modeny	Modena	k1gFnSc2	Modena
a	a	k8xC	a
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
relikviářovou	relikviářův	k2eAgFnSc7d1	relikviářův
schránkou	schránka	k1gFnSc7	schránka
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc4	Kříž
od	od	k7c2	od
Dětřicha	Dětřich	k1gMnSc2	Dětřich
Pražského	pražský	k2eAgMnSc2d1	pražský
(	(	kIx(	(
<g/>
mistra	mistr	k1gMnSc2	mistr
Theodorika	Theodorik	k1gMnSc2	Theodorik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
význam	význam	k1gInSc1	význam
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
tvrzení	tvrzení	k1gNnSc2	tvrzení
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
dva	dva	k4xCgMnPc4	dva
dědečky	dědeček	k1gMnPc4	dědeček
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc4d1	svatý
Václava	Václav	k1gMnSc4	Václav
(	(	kIx(	(
<g/>
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
císaře	císař	k1gMnSc2	císař
bylo	být	k5eAaImAgNnS	být
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ikony	ikona	k1gFnPc1	ikona
těchto	tento	k3xDgMnPc2	tento
dvou	dva	k4xCgMnPc2	dva
světců	světec	k1gMnPc2	světec
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
symetricky	symetricky	k6eAd1	symetricky
uprostřed	uprostřed	k7c2	uprostřed
obkladu	obklad	k1gInSc2	obklad
východní	východní	k2eAgFnSc2d1	východní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
pozdní	pozdní	k2eAgFnSc2d1	pozdní
jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
gotiky	gotika	k1gFnSc2	gotika
pochází	pocházet	k5eAaImIp3nS	pocházet
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
cyklus	cyklus	k1gInSc1	cyklus
Mistra	mistr	k1gMnSc2	mistr
Litoměřického	litoměřický	k2eAgInSc2d1	litoměřický
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
vymalovaný	vymalovaný	k2eAgInSc1d1	vymalovaný
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
kaple	kaple	k1gFnSc2	kaple
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1509	[number]	k4	1509
a	a	k8xC	a
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
zlacená	zlacený	k2eAgFnSc1d1	zlacená
busta	busta	k1gFnSc1	busta
na	na	k7c4	na
lebku	lebka	k1gFnSc4	lebka
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
vystavená	vystavený	k2eAgFnSc1d1	vystavená
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
klenotnici	klenotnice	k1gFnSc6	klenotnice
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
nádvoří	nádvoří	k1gNnSc6	nádvoří
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
renesanční	renesanční	k2eAgFnSc2d1	renesanční
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
zásadně	zásadně	k6eAd1	zásadně
vyobrazoval	vyobrazovat	k5eAaImAgMnS	vyobrazovat
v	v	k7c6	v
plátové	plátový	k2eAgFnSc6d1	plátová
zbroji	zbroj	k1gFnSc6	zbroj
jako	jako	k9	jako
těžkooděnec	těžkooděnec	k1gMnSc1	těžkooděnec
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
obraz	obraz	k1gInSc1	obraz
monogramisty	monogramista	k1gMnSc2	monogramista
I.W.	I.W.	k1gMnSc2	I.W.
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
svatováclavské	svatováclavský	k2eAgNnSc4d1	Svatováclavské
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
knížete	kníže	k1gNnSc4wR	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
držícího	držící	k2eAgMnSc2d1	držící
se	se	k3xPyFc4	se
klepadla	klepadlo	k1gNnSc2	klepadlo
bezezbraně	bezezbraně	k6eAd1	bezezbraně
"	"	kIx"	"
<g/>
v	v	k7c6	v
knížecím	knížecí	k2eAgInSc6d1	knížecí
civilním	civilní	k2eAgInSc6d1	civilní
oděvu	oděv	k1gInSc6	oděv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
barokních	barokní	k2eAgInPc2d1	barokní
obrazů	obraz	k1gInPc2	obraz
bývá	bývat	k5eAaImIp3nS	bývat
za	za	k7c4	za
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
považováno	považován	k2eAgNnSc4d1	považováno
trojí	trojí	k4xRgNnSc4	trojí
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Škréty	Škréta	k1gMnSc2	Škréta
<g/>
,	,	kIx,	,
především	především	k9	především
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
cyklus	cyklus	k1gInSc1	cyklus
obrazů	obraz	k1gInPc2	obraz
lunetového	lunetový	k2eAgInSc2d1	lunetový
formátu	formát	k1gInSc2	formát
<g/>
,	,	kIx,	,
namalovaný	namalovaný	k2eAgInSc1d1	namalovaný
pro	pro	k7c4	pro
klášter	klášter	k1gInSc4	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Na	na	k7c6	na
Zderaze	Zderaz	k1gInSc6	Zderaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
mezi	mezi	k7c4	mezi
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Lobkowiczkou	Lobkowiczka	k1gFnSc7	Lobkowiczka
sbírku	sbírka	k1gFnSc4	sbírka
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
se	s	k7c7	s
stojící	stojící	k2eAgFnSc7d1	stojící
figurou	figura	k1gFnSc7	figura
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
s	s	k7c7	s
praporem	prapor	k1gInSc7	prapor
v	v	k7c6	v
pravici	pravice	k1gFnSc6	pravice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
visí	viset	k5eAaImIp3nS	viset
v	v	k7c6	v
sakristii	sakristie	k1gFnSc6	sakristie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
postavy	postava	k1gFnSc2	postava
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
zdobí	zdobit	k5eAaImIp3nS	zdobit
četné	četný	k2eAgInPc4d1	četný
oltáře	oltář	k1gInPc4	oltář
českých	český	k2eAgInPc2d1	český
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
sv.	sv.	kA	sv.
Jindřicha	Jindřich	k1gMnSc2	Jindřich
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
Karlově	Karlův	k2eAgInSc6d1	Karlův
mostě	most	k1gInSc6	most
stávala	stávat	k5eAaImAgFnS	stávat
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
mezi	mezi	k7c4	mezi
anděly	anděl	k1gMnPc4	anděl
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Ottavia	Ottavius	k1gMnSc2	Ottavius
Mosta	Most	k1gMnSc2	Most
(	(	kIx(	(
<g/>
po	po	k7c6	po
povodni	povodeň	k1gFnSc6	povodeň
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
přesunutá	přesunutý	k2eAgFnSc1d1	přesunutá
na	na	k7c4	na
rampu	rampa	k1gFnSc4	rampa
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
odtamtud	odtamtud	k6eAd1	odtamtud
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
socha	socha	k1gFnSc1	socha
od	od	k7c2	od
Čeňka	Čeněk	k1gMnSc2	Čeněk
Vosmíka	Vosmík	k1gMnSc2	Vosmík
<g/>
,	,	kIx,	,
do	do	k7c2	do
Lapidária	lapidárium	k1gNnSc2	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
objevuje	objevovat	k5eAaImIp3nS	objevovat
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
patrony	patron	k1gMnPc7	patron
na	na	k7c6	na
morových	morový	k2eAgInPc6d1	morový
sloupech	sloup	k1gInPc6	sloup
barokní	barokní	k2eAgFnSc2d1	barokní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
Malostranském	malostranský	k2eAgNnSc6d1	Malostranské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kult	kult	k1gInSc1	kult
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
nosným	nosný	k2eAgNnSc7d1	nosné
tématem	téma	k1gNnSc7	téma
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
ze	z	k7c2	z
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Myslbekova	Myslbekův	k2eAgFnSc1d1	Myslbekova
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
mezi	mezi	k7c7	mezi
svatými	svatý	k1gMnPc7	svatý
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
<g/>
,	,	kIx,	,
Prokopem	Prokop	k1gMnSc7	Prokop
<g/>
,	,	kIx,	,
Ludmilou	Ludmila	k1gFnSc7	Ludmila
a	a	k8xC	a
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Myslbekovo	Myslbekův	k2eAgNnSc1d1	Myslbekovo
dílo	dílo	k1gNnSc1	dílo
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
starší	starý	k2eAgFnSc4d2	starší
jezdeckou	jezdecký	k2eAgFnSc4d1	jezdecká
sochu	socha	k1gFnSc4	socha
raně	raně	k6eAd1	raně
barokního	barokní	k2eAgMnSc2d1	barokní
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
Bendla	Bendla	k1gMnSc2	Bendla
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
Lapidária	lapidárium	k1gNnSc2	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
zůstala	zůstat	k5eAaPmAgFnS	zůstat
její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
pořídit	pořídit	k5eAaPmF	pořídit
probošt	probošt	k1gMnSc1	probošt
Václav	Václav	k1gMnSc1	Václav
Štulc	Štulc	k1gFnSc1	Štulc
<g/>
.	.	kIx.	.
</s>
<s>
Sochařsky	sochařsky	k6eAd1	sochařsky
virtuózní	virtuózní	k2eAgNnSc1d1	virtuózní
provedení	provedení	k1gNnSc1	provedení
knížete	kníže	k1gMnSc2	kníže
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
přenáší	přenášet	k5eAaImIp3nS	přenášet
statiku	statika	k1gFnSc4	statika
celé	celý	k2eAgFnSc2d1	celá
figury	figura	k1gFnSc2	figura
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
styčných	styčný	k2eAgFnPc2d1	styčná
ploch	plocha	k1gFnPc2	plocha
-	-	kIx~	-
na	na	k7c4	na
přední	přední	k2eAgFnSc4d1	přední
levou	levý	k2eAgFnSc4d1	levá
a	a	k8xC	a
zadní	zadní	k2eAgFnSc4d1	zadní
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
Václavova	Václavův	k2eAgMnSc2d1	Václavův
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
sochy	socha	k1gFnSc2	socha
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
<g/>
,	,	kIx,	,
vévodo	vévoda	k1gMnSc5	vévoda
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
náš	náš	k3xOp1gMnSc1	náš
<g/>
,	,	kIx,	,
nedej	dát	k5eNaPmRp2nS	dát
zahynouti	zahynout	k5eAaPmF	zahynout
nám	my	k3xPp1nPc3	my
i	i	k9	i
budoucím	budoucí	k2eAgMnPc3d1	budoucí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
byla	být	k5eAaImAgFnS	být
námětem	námět	k1gInSc7	námět
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
na	na	k7c6	na
bankovkách	bankovka	k1gFnPc6	bankovka
i	i	k8xC	i
mincích	mince	k1gFnPc6	mince
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
dvacetikorunové	dvacetikorunový	k2eAgFnSc6d1	dvacetikorunová
minci	mince	k1gFnSc6	mince
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svatováclavský	svatováclavský	k2eAgInSc4d1	svatováclavský
chorál	chorál	k1gInSc4	chorál
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
chorál	chorál	k1gInSc1	chorál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
patrně	patrně	k6eAd1	patrně
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
tři	tři	k4xCgFnPc1	tři
pětiřádkové	pětiřádkový	k2eAgFnPc1d1	pětiřádková
sloky	sloka	k1gFnPc1	sloka
neumělých	umělý	k2eNgInPc2d1	neumělý
<g/>
,	,	kIx,	,
sdružených	sdružený	k2eAgInPc2d1	sdružený
rýmů	rým	k1gInPc2	rým
<g/>
,	,	kIx,	,
ukončené	ukončený	k2eAgFnSc6d1	ukončená
refrénem	refrén	k1gInSc7	refrén
"	"	kIx"	"
<g/>
Kyrie	Kyrie	k1gNnSc1	Kyrie
eleison	eleisona	k1gFnPc2	eleisona
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obsahem	obsah	k1gInSc7	obsah
je	být	k5eAaImIp3nS	být
prostá	prostý	k2eAgFnSc1d1	prostá
modlitba	modlitba	k1gFnSc1	modlitba
k	k	k7c3	k
patronovi	patron	k1gMnSc3	patron
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
Svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
<g/>
,	,	kIx,	,
vévodo	vévoda	k1gMnSc5	vévoda
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
náš	náš	k3xOp1gMnSc1	náš
<g/>
,	,	kIx,	,
pros	proso	k1gNnPc2	proso
za	za	k7c2	za
nás	my	k3xPp1nPc2	my
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
Kriste	Kristus	k1gMnSc5	Kristus
eleison	eleison	k1gMnSc1	eleison
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
dědic	dědic	k1gMnSc1	dědic
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
rozpomeň	rozpomenout	k5eAaPmRp2nS	rozpomenout
se	se	k3xPyFc4	se
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
plémě	plémě	k1gNnSc4	plémě
<g/>
.	.	kIx.	.
</s>
<s>
Nedej	dát	k5eNaPmRp2nS	dát
zahynouti	zahynout	k5eAaPmF	zahynout
nám	my	k3xPp1nPc3	my
i	i	k8xC	i
budoucím	budoucí	k2eAgMnSc6d1	budoucí
<g/>
,	,	kIx,	,
svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
<g/>
,	,	kIx,	,
Kriste	Kristus	k1gMnSc5	Kristus
eleison	eleison	k1gInSc1	eleison
<g/>
.	.	kIx.	.
</s>
<s>
Pomoci	pomoct	k5eAaPmF	pomoct
my	my	k3xPp1nPc1	my
tvé	tvůj	k3xOp2gMnPc4	tvůj
žádáme	žádat	k5eAaImIp1nP	žádat
<g/>
,	,	kIx,	,
smiluj	smilovat	k5eAaPmRp2nS	smilovat
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
utěš	utěšit	k5eAaPmRp2nS	utěšit
smutné	smutný	k2eAgFnSc2d1	smutná
<g/>
,	,	kIx,	,
zažeň	zahnat	k5eAaPmRp2nS	zahnat
vše	všechen	k3xTgNnSc1	všechen
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
,	,	kIx,	,
svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
<g/>
,	,	kIx,	,
Kriste	Kristus	k1gMnSc5	Kristus
eleison	eleison	k1gInSc4	eleison
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
oslav	oslava	k1gFnPc2	oslava
předpokládaného	předpokládaný	k2eAgNnSc2d1	předpokládané
svatováclavského	svatováclavský	k2eAgNnSc2d1	Svatováclavské
milénia	milénium	k1gNnSc2	milénium
byl	být	k5eAaImAgInS	být
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
natočen	natočen	k2eAgInSc4d1	natočen
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
antropolog	antropolog	k1gMnSc1	antropolog
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgInSc2	svůj
antropologického	antropologický	k2eAgInSc2d1	antropologický
výzkumu	výzkum	k1gInSc2	výzkum
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pohřbených	pohřbený	k2eAgMnPc2d1	pohřbený
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
u	u	k7c2	u
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lokalizoval	lokalizovat	k5eAaBmAgInS	lokalizovat
hroby	hrob	k1gInPc4	hrob
čtyř	čtyři	k4xCgInPc2	čtyři
nejstarších	starý	k2eAgMnPc2d3	nejstarší
známých	známý	k2eAgMnPc2d1	známý
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
(	(	kIx(	(
<g/>
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
<g/>
,	,	kIx,	,
Spytihněva	Spytihněv	k1gMnSc2	Spytihněv
<g/>
,	,	kIx,	,
Vratislava	Vratislav	k1gMnSc2	Vratislav
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
pak	pak	k6eAd1	pak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Vratislav	Vratislav	k1gMnSc1	Vratislav
dožili	dožít	k5eAaPmAgMnP	dožít
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
současnými	současný	k2eAgInPc7d1	současný
historiky	historik	k1gMnPc4	historik
(	(	kIx(	(
<g/>
u	u	k7c2	u
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
42	[number]	k4	42
nebo	nebo	k8xC	nebo
45	[number]	k4	45
let	léto	k1gNnPc2	léto
namísto	namísto	k7c2	namísto
28	[number]	k4	28
a	a	k8xC	a
u	u	k7c2	u
Vratislava	Vratislav	k1gMnSc2	Vratislav
o	o	k7c4	o
46	[number]	k4	46
oproti	oproti	k7c3	oproti
33	[number]	k4	33
<g/>
,	,	kIx,	,
narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Vlčka	Vlček	k1gMnSc2	Vlček
se	se	k3xPyFc4	se
věk	věk	k1gInSc1	věk
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
knížat	kníže	k1gMnPc2wR	kníže
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
a	a	k8xC	a
Spytihněva	Spytihněv	k1gMnSc4	Spytihněv
shodoval	shodovat	k5eAaImAgMnS	shodovat
s	s	k7c7	s
historickým	historický	k2eAgNnSc7d1	historické
schématem	schéma	k1gNnSc7	schéma
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
svatováclavských	svatováclavský	k2eAgFnPc2d1	Svatováclavská
a	a	k8xC	a
svatoludmilských	svatoludmilský	k2eAgFnPc2d1	svatoludmilský
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
antropologický	antropologický	k2eAgInSc1d1	antropologický
výzkum	výzkum	k1gInSc1	výzkum
byl	být	k5eAaImAgInS	být
však	však	k9	však
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
D.	D.	kA	D.
Třeštík	Třeštík	k1gInSc1	Třeštík
<g/>
)	)	kIx)	)
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
jako	jako	k8xC	jako
chybný	chybný	k2eAgInSc1d1	chybný
a	a	k8xC	a
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
nevyjasněné	vyjasněný	k2eNgFnSc3d1	nevyjasněná
totožnosti	totožnost	k1gFnSc3	totožnost
ostatků	ostatek	k1gInPc2	ostatek
zemřelých	zemřelý	k1gMnPc2	zemřelý
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
výzkum	výzkum	k1gInSc1	výzkum
prováděn	provádět	k5eAaImNgInS	provádět
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
chybné	chybný	k2eAgFnPc4d1	chybná
interpretace	interpretace	k1gFnPc4	interpretace
některých	některý	k3yIgInPc2	některý
souvisejících	související	k2eAgInPc2d1	související
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
použité	použitý	k2eAgFnSc2d1	použitá
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
(	(	kIx(	(
<g/>
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
Gustafsonova	Gustafsonův	k2eAgFnSc1d1	Gustafsonův
metoda	metoda	k1gFnSc1	metoda
určení	určení	k1gNnSc2	určení
zubního	zubní	k2eAgInSc2d1	zubní
věku	věk	k1gInSc2	věk
a	a	k8xC	a
upravená	upravený	k2eAgFnSc1d1	upravená
metoda	metoda	k1gFnSc1	metoda
Leopolda	Leopold	k1gMnSc2	Leopold
a	a	k8xC	a
Jagowa	Jagowus	k1gMnSc2	Jagowus
stanovení	stanovení	k1gNnSc2	stanovení
věku	věk	k1gInSc2	věk
podle	podle	k7c2	podle
osifikace	osifikace	k1gFnSc2	osifikace
chrupavky	chrupavka	k1gFnSc2	chrupavka
štítné	štítný	k2eAgInPc1d1	štítný
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
paleoantropologického	paleoantropologický	k2eAgInSc2d1	paleoantropologický
materiálu	materiál	k1gInSc2	materiál
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
recentních	recentní	k2eAgFnPc2d1	recentní
populací	populace	k1gFnPc2	populace
-	-	kIx~	-
nejnověji	nově	k6eAd3	nově
označovány	označován	k2eAgInPc1d1	označován
za	za	k7c4	za
málo	málo	k1gNnSc4	málo
přesné	přesný	k2eAgNnSc4d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nedostatku	nedostatek	k1gInSc3	nedostatek
spolehlivých	spolehlivý	k2eAgInPc2d1	spolehlivý
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
žádného	žádný	k1gMnSc4	žádný
bratra	bratr	k1gMnSc4	bratr
vůbec	vůbec	k9	vůbec
neměl	mít	k5eNaImAgInS	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jména	jméno	k1gNnSc2	jméno
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
znamenají	znamenat	k5eAaImIp3nP	znamenat
totéž	týž	k3xTgNnSc4	týž
-	-	kIx~	-
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
slav	slavit	k5eAaImRp2nS	slavit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bole	bola	k1gFnSc3	bola
=	=	kIx~	=
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
Gloria	Gloria	k1gFnSc1	Gloria
Maior	Maior	k1gInSc1	Maior
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Maior	Maior	k1gInSc1	Maior
Gloria	Gloria	k1gFnSc1	Gloria
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Boleslav	Boleslav	k1gMnSc1	Boleslav
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
byla	být	k5eAaImAgFnS	být
tatáž	týž	k3xTgFnSc1	týž
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc4d3	nejznámější
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
neexistenci	neexistence	k1gFnSc4	neexistence
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	dílo	k1gNnSc6	dílo
České	český	k2eAgNnSc1d1	české
pohanství	pohanství	k1gNnSc1	pohanství
Záviš	Záviš	k1gMnSc1	Záviš
Kalandra	Kalandr	k1gMnSc2	Kalandr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
existenci	existence	k1gFnSc4	existence
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
skutečné	skutečný	k2eAgFnPc1d1	skutečná
historické	historický	k2eAgFnPc1d1	historická
postavy	postava	k1gFnPc1	postava
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jej	on	k3xPp3gNnSc2	on
historicky	historicky	k6eAd1	historicky
ztotožnil	ztotožnit	k5eAaPmAgMnS	ztotožnit
s	s	k7c7	s
Boleslavem	Boleslav	k1gMnSc7	Boleslav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
jej	on	k3xPp3gInSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
pohanského	pohanský	k2eAgInSc2d1	pohanský
panteonu	panteon	k1gInSc2	panteon
Čechů	Čech	k1gMnPc2	Čech
teprve	teprve	k6eAd1	teprve
dodatečně	dodatečně	k6eAd1	dodatečně
využitého	využitý	k2eAgInSc2d1	využitý
křesťanstvím	křesťanství	k1gNnPc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nepodložený	podložený	k2eNgInSc1d1	nepodložený
názor	názor	k1gInSc1	názor
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
historiky	historik	k1gMnPc4	historik
přijat	přijat	k2eAgInSc1d1	přijat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
mu	on	k3xPp3gMnSc3	on
neporodila	porodit	k5eNaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
byla	být	k5eAaImAgFnS	být
bezdětná	bezdětný	k2eAgFnSc1d1	bezdětná
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
svazku	svazek	k1gInSc2	svazek
narodily	narodit	k5eAaPmAgFnP	narodit
pouze	pouze	k6eAd1	pouze
dcery	dcera	k1gFnSc2	dcera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Údajného	údajný	k2eAgMnSc4d1	údajný
syna	syn	k1gMnSc4	syn
Zbraslava	Zbraslav	k1gMnSc4	Zbraslav
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
vzpomínají	vzpomínat	k5eAaImIp3nP	vzpomínat
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Václav	Václav	k1gMnSc1	Václav
zřejmě	zřejmě	k6eAd1	zřejmě
zplodil	zplodit	k5eAaPmAgMnS	zplodit
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
souložnicí	souložnice	k1gFnSc7	souložnice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
<g/>
,	,	kIx,	,
nemuselo	muset	k5eNaImAgNnS	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
nešťastnou	šťastný	k2eNgFnSc4d1	nešťastná
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
:	:	kIx,	:
Onoho	onen	k3xDgNnSc2	onen
rána	ráno	k1gNnSc2	ráno
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
potkal	potkat	k5eAaPmAgMnS	potkat
Václav	Václav	k1gMnSc1	Václav
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
mladší	mladý	k2eAgMnSc1d2	mladší
Boleslav	Boleslav	k1gMnSc1	Boleslav
rozlítil	rozlítit	k5eAaPmAgMnS	rozlítit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
spory	spora	k1gFnSc2	spora
<g/>
.	.	kIx.	.
</s>
<s>
Tasil	tasit	k5eAaPmAgInS	tasit
meč	meč	k1gInSc1	meč
a	a	k8xC	a
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
a	a	k8xC	a
povalil	povalit	k5eAaPmAgMnS	povalit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Boleslavovo	Boleslavův	k2eAgNnSc4d1	Boleslavovo
volání	volání	k1gNnSc4	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
přiběhli	přiběhnout	k5eAaPmAgMnP	přiběhnout
jeho	jeho	k3xOp3gMnPc1	jeho
družiníci	družiník	k1gMnPc1	družiník
<g/>
.	.	kIx.	.
</s>
<s>
Viděli	vidět	k5eAaImAgMnP	vidět
Václava	Václav	k1gMnSc4	Václav
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
ležícím	ležící	k2eAgMnSc7d1	ležící
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Vrhli	vrhnout	k5eAaPmAgMnP	vrhnout
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
knížete	kníže	k1gMnSc4	kníže
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nestačil	stačit	k5eNaBmAgInS	stačit
ukrýt	ukrýt	k5eAaPmF	ukrýt
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
prchal	prchat	k5eAaImAgMnS	prchat
<g/>
,	,	kIx,	,
ubili	ubít	k5eAaPmAgMnP	ubít
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
obvykle	obvykle	k6eAd1	obvykle
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
smyslu	smysl	k1gInSc2	smysl
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
cesty	cesta	k1gFnSc2	cesta
za	za	k7c7	za
bratrem	bratr	k1gMnSc7	bratr
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabít	zabít	k5eAaPmF	zabít
ho	on	k3xPp3gMnSc4	on
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
bojovníci	bojovník	k1gMnPc1	bojovník
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
skrývali	skrývat	k5eAaImAgMnP	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
interpretoval	interpretovat	k5eAaBmAgInS	interpretovat
smrt	smrt	k1gFnSc4	smrt
Václava	Václav	k1gMnSc2	Václav
i	i	k9	i
historik	historik	k1gMnSc1	historik
František	František	k1gMnSc1	František
Dvorník	dvorník	k1gMnSc1	dvorník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
výkladem	výklad	k1gInSc7	výklad
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
části	část	k1gFnSc2	část
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
přípravy	příprava	k1gFnPc4	příprava
na	na	k7c4	na
Václavovo	Václavův	k2eAgNnSc4d1	Václavovo
zavraždění	zavraždění	k1gNnSc4	zavraždění
(	(	kIx(	(
<g/>
Hněvsův	Hněvsův	k2eAgInSc1d1	Hněvsův
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vrazi	vrah	k1gMnPc1	vrah
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
Tuža	Tužus	k1gMnSc4	Tužus
<g/>
,	,	kIx,	,
Tyra	Tyrus	k1gMnSc4	Tyrus
<g/>
,	,	kIx,	,
Hněvsa	Hněvs	k1gMnSc4	Hněvs
a	a	k8xC	a
Čista	čist	k2eAgFnSc1d1	čista
radili	radit	k5eAaImAgMnP	radit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
