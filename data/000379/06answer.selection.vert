<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
asi	asi	k9	asi
907	[number]	k4	907
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
929	[number]	k4	929
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Wenzel	Wenzel	k1gMnSc3	Wenzel
von	von	k1gInSc4	von
Böhmen	Böhmen	k2eAgInSc4d1	Böhmen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
a	a	k8xC	a
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
