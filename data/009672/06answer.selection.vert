<s>
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc1	připojištění
nebo	nebo	k8xC	nebo
také	také	k9	také
důchodové	důchodový	k2eAgNnSc4d1	důchodové
připojištění	připojištění	k1gNnSc4	připojištění
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
důchodovým	důchodový	k2eAgNnSc7d1	důchodové
pojištěním	pojištění	k1gNnSc7	pojištění
neboli	neboli	k8xC	neboli
penzijním	penzijní	k2eAgNnSc7d1	penzijní
pojištěním	pojištění	k1gNnSc7	pojištění
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
důchodovým	důchodový	k2eAgNnSc7d1	důchodové
spořením	spoření	k1gNnSc7	spoření
neboli	neboli	k8xC	neboli
penzijním	penzijní	k2eAgNnSc7d1	penzijní
spořením	spoření	k1gNnSc7	spoření
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
způsobů	způsob	k1gInPc2	způsob
spoření	spoření	k1gNnSc2	spoření
–	–	k?	–
zajištění	zajištění	k1gNnSc2	zajištění
na	na	k7c4	na
penzi	penze	k1gFnSc4	penze
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
třetího	třetí	k4xOgInSc2	třetí
pilíře	pilíř	k1gInSc2	pilíř
důchodového	důchodový	k2eAgInSc2d1	důchodový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
