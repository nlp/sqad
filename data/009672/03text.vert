<p>
<s>
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc1	připojištění
nebo	nebo	k8xC	nebo
také	také	k9	také
důchodové	důchodový	k2eAgNnSc4d1	důchodové
připojištění	připojištění	k1gNnSc4	připojištění
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
důchodovým	důchodový	k2eAgNnSc7d1	důchodové
pojištěním	pojištění	k1gNnSc7	pojištění
neboli	neboli	k8xC	neboli
penzijním	penzijní	k2eAgNnSc7d1	penzijní
pojištěním	pojištění	k1gNnSc7	pojištění
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
důchodovým	důchodový	k2eAgNnSc7d1	důchodové
spořením	spoření	k1gNnSc7	spoření
neboli	neboli	k8xC	neboli
penzijním	penzijní	k2eAgNnSc7d1	penzijní
spořením	spoření	k1gNnSc7	spoření
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
způsobů	způsob	k1gInPc2	způsob
spoření	spoření	k1gNnSc2	spoření
–	–	k?	–
zajištění	zajištění	k1gNnSc2	zajištění
na	na	k7c4	na
penzi	penze	k1gFnSc4	penze
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
třetího	třetí	k4xOgInSc2	třetí
pilíře	pilíř	k1gInSc2	pilíř
důchodového	důchodový	k2eAgInSc2d1	důchodový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc1	připojištění
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
příspěvkem	příspěvek	k1gInSc7	příspěvek
==	==	k?	==
</s>
</p>
<p>
<s>
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc1	připojištění
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
spoření	spoření	k1gNnSc2	spoření
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydáním	vydání	k1gNnSc7	vydání
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
42	[number]	k4	42
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
státem	stát	k1gInSc7	stát
regulovaný	regulovaný	k2eAgInSc1d1	regulovaný
spořicí	spořicí	k2eAgInSc1d1	spořicí
produkt	produkt	k1gInSc1	produkt
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
bezpečného	bezpečný	k2eAgNnSc2d1	bezpečné
ukládání	ukládání	k1gNnSc2	ukládání
a	a	k8xC	a
zhodnocování	zhodnocování	k1gNnSc2	zhodnocování
peněžních	peněžní	k2eAgInPc2d1	peněžní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
uzavřít	uzavřít	k5eAaPmF	uzavřít
nejpozději	pozdě	k6eAd3	pozdě
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc4	připojištění
si	se	k3xPyFc3	se
mohla	moct	k5eAaImAgFnS	moct
zřídit	zřídit	k5eAaPmF	zřídit
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
osoba	osoba	k1gFnSc1	osoba
starší	starý	k2eAgFnSc1d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
občanem	občan	k1gMnSc7	občan
ČR	ČR	kA	ČR
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
země	zem	k1gFnSc2	zem
EU	EU	kA	EU
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
nebo	nebo	k8xC	nebo
důchodového	důchodový	k2eAgNnSc2d1	důchodové
pojištění	pojištění	k1gNnSc2	pojištění
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
toto	tento	k3xDgNnSc1	tento
spoření	spoření	k1gNnSc1	spoření
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
novým	nový	k2eAgNnSc7d1	nové
doplňkovým	doplňkový	k2eAgNnSc7d1	doplňkové
penzijním	penzijní	k2eAgNnSc7d1	penzijní
spořením	spoření	k1gNnSc7	spoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
spoření	spoření	k1gNnSc1	spoření
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
úspory	úspora	k1gFnPc1	úspora
klientů	klient	k1gMnPc2	klient
účetně	účetně	k6eAd1	účetně
vyčlenily	vyčlenit	k5eAaPmAgInP	vyčlenit
z	z	k7c2	z
penzijního	penzijní	k2eAgInSc2d1	penzijní
fondu	fond	k1gInSc2	fond
do	do	k7c2	do
Transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
spravuje	spravovat	k5eAaImIp3nS	spravovat
penzijní	penzijní	k2eAgFnSc1d1	penzijní
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Spoření	spoření	k1gNnSc1	spoření
i	i	k8xC	i
výplata	výplata	k1gFnSc1	výplata
dávek	dávka	k1gFnPc2	dávka
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
řídí	řídit	k5eAaImIp3nP	řídit
původními	původní	k2eAgFnPc7d1	původní
podmínkami	podmínka	k1gFnPc7	podmínka
a	a	k8xC	a
zejména	zejména	k9	zejména
platným	platný	k2eAgInSc7d1	platný
penzijním	penzijní	k2eAgInSc7d1	penzijní
plánem	plán	k1gInSc7	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výše	výše	k1gFnSc1	výše
spoření	spoření	k1gNnSc2	spoření
===	===	k?	===
</s>
</p>
<p>
<s>
Zakládá	zakládat	k5eAaImIp3nS	zakládat
se	se	k3xPyFc4	se
na	na	k7c6	na
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
měsíčních	měsíční	k2eAgInPc6d1	měsíční
příspěvcích	příspěvek	k1gInPc6	příspěvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
lze	lze	k6eAd1	lze
na	na	k7c4	na
účet	účet	k1gInSc4	účet
transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
fondu	fond	k1gInSc2	fond
poukazovat	poukazovat	k5eAaImF	poukazovat
i	i	k9	i
za	za	k7c4	za
delší	dlouhý	k2eAgNnPc4d2	delší
časová	časový	k2eAgNnPc4d1	časové
období	období	k1gNnPc4	období
(	(	kIx(	(
<g/>
čtvrtletí	čtvrtletí	k1gNnSc4	čtvrtletí
<g/>
,	,	kIx,	,
pololetí	pololetí	k1gNnSc4	pololetí
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepravidelně	pravidelně	k6eNd1	pravidelně
jednorázově	jednorázově	k6eAd1	jednorázově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
výši	výše	k1gFnSc4	výše
měsíčního	měsíční	k2eAgInSc2d1	měsíční
příspěvku	příspěvek	k1gInSc2	příspěvek
nebo	nebo	k8xC	nebo
spoření	spoření	k1gNnSc2	spoření
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
naspořeným	naspořený	k2eAgInPc3d1	naspořený
příspěvkům	příspěvek	k1gInPc3	příspěvek
klienta	klient	k1gMnSc2	klient
jsou	být	k5eAaImIp3nP	být
připisovány	připisován	k2eAgInPc4d1	připisován
státní	státní	k2eAgInPc4d1	státní
příspěvky	příspěvek	k1gInPc4	příspěvek
a	a	k8xC	a
jednorázově	jednorázově	k6eAd1	jednorázově
ročně	ročně	k6eAd1	ročně
podíly	podíl	k1gInPc1	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
Transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
fondu	fond	k1gInSc2	fond
(	(	kIx(	(
<g/>
výnosy	výnos	k1gInPc1	výnos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
penzijní	penzijní	k2eAgNnSc4d1	penzijní
připojištění	připojištění	k1gNnSc4	připojištění
může	moct	k5eAaImIp3nS	moct
přispívat	přispívat	k5eAaImF	přispívat
také	také	k9	také
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc1	možnost
přechodu	přechod	k1gInSc2	přechod
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
penzijní	penzijní	k2eAgFnSc3d1	penzijní
společnosti	společnost	k1gFnSc3	společnost
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
podmínek	podmínka	k1gFnPc2	podmínka
byla	být	k5eAaImAgFnS	být
možné	možný	k2eAgFnPc4d1	možná
jen	jen	k9	jen
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
penzijní	penzijní	k2eAgFnSc2d1	penzijní
společnosti	společnost	k1gFnSc2	společnost
možná	možná	k9	možná
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
doplňkovým	doplňkový	k2eAgNnSc7d1	doplňkové
penzijním	penzijní	k2eAgNnSc7d1	penzijní
spořením	spoření	k1gNnSc7	spoření
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nejdříve	dříve	k6eAd3	dříve
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
doplňkové	doplňkový	k2eAgNnSc4d1	doplňkové
penzijní	penzijní	k2eAgNnSc4d1	penzijní
spoření	spoření	k1gNnSc4	spoření
u	u	k7c2	u
stávající	stávající	k2eAgFnSc2d1	stávající
penzijní	penzijní	k2eAgFnSc2d1	penzijní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
změnit	změnit	k5eAaPmF	změnit
penzijní	penzijní	k2eAgFnSc4d1	penzijní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgNnSc1d1	státní
zvýhodnění	zvýhodnění	k1gNnSc1	zvýhodnění
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Státní	státní	k2eAgInPc1d1	státní
příspěvky	příspěvek	k1gInPc1	příspěvek
====	====	k?	====
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
doplňkového	doplňkový	k2eAgNnSc2d1	doplňkové
penzijního	penzijní	k2eAgNnSc2d1	penzijní
spoření	spoření	k1gNnSc2	spoření
přispívá	přispívat	k5eAaImIp3nS	přispívat
státním	státní	k2eAgInSc7d1	státní
příspěvkem	příspěvek	k1gInSc7	příspěvek
podle	podle	k7c2	podle
následujícího	následující	k2eAgNnSc2d1	následující
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
příspěvky	příspěvek	k1gInPc1	příspěvek
nelze	lze	k6eNd1	lze
žádat	žádat	k5eAaImF	žádat
na	na	k7c4	na
příspěvky	příspěvek	k1gInPc4	příspěvek
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
příspěvky	příspěvek	k1gInPc4	příspěvek
placené	placený	k2eAgNnSc1d1	placené
účastníkem	účastník	k1gMnSc7	účastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Daňové	daňový	k2eAgNnSc4d1	daňové
zvýhodnění	zvýhodnění	k1gNnSc4	zvýhodnění
pro	pro	k7c4	pro
klienta	klient	k1gMnSc4	klient
====	====	k?	====
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
klient	klient	k1gMnSc1	klient
za	za	k7c4	za
rok	rok	k1gInSc4	rok
vloží	vložit	k5eAaPmIp3nS	vložit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
spoření	spoření	k1gNnSc4	spoření
částku	částka	k1gFnSc4	částka
sjednanou	sjednaný	k2eAgFnSc4d1	sjednaná
jako	jako	k8xC	jako
příspěvek	příspěvek	k1gInSc4	příspěvek
klienta	klient	k1gMnSc2	klient
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
12	[number]	k4	12
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
možnost	možnost	k1gFnSc4	možnost
odečíst	odečíst	k5eAaPmF	odečíst
si	se	k3xPyFc3	se
od	od	k7c2	od
základu	základ	k1gInSc2	základ
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
část	část	k1gFnSc1	část
spoření	spoření	k1gNnSc2	spoření
přesahující	přesahující	k2eAgFnSc1d1	přesahující
12	[number]	k4	12
000	[number]	k4	000
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
však	však	k9	však
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
24	[number]	k4	24
000	[number]	k4	000
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pravidelném	pravidelný	k2eAgNnSc6d1	pravidelné
měsíčním	měsíční	k2eAgNnSc6d1	měsíční
spoření	spoření	k1gNnSc6	spoření
2	[number]	k4	2
000	[number]	k4	000
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
24	[number]	k4	24
000	[number]	k4	000
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
získá	získat	k5eAaPmIp3nS	získat
klient	klient	k1gMnSc1	klient
na	na	k7c4	na
prvních	první	k4xOgInPc2	první
dvanáct	dvanáct	k4xCc4	dvanáct
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
státní	státní	k2eAgInSc4d1	státní
příspěvek	příspěvek	k1gInSc4	příspěvek
2	[number]	k4	2
760	[number]	k4	760
Kč	Kč	kA	Kč
a	a	k8xC	a
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
dvanáct	dvanáct	k4xCc4	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
získá	získat	k5eAaPmIp3nS	získat
další	další	k2eAgNnSc4d1	další
státní	státní	k2eAgNnSc4d1	státní
zvýhodnění	zvýhodnění	k1gNnSc4	zvýhodnění
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
daňové	daňový	k2eAgFnSc2d1	daňová
úlevy	úleva	k1gFnSc2	úleva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
plného	plný	k2eAgInSc2d1	plný
státního	státní	k2eAgInSc2d1	státní
příspěvku	příspěvek	k1gInSc2	příspěvek
(	(	kIx(	(
230	[number]	k4	230
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
a	a	k8xC	a
maximálního	maximální	k2eAgNnSc2d1	maximální
daňového	daňový	k2eAgNnSc2d1	daňové
zvýhodnění	zvýhodnění	k1gNnSc2	zvýhodnění
je	být	k5eAaImIp3nS	být
optimální	optimální	k2eAgFnSc1d1	optimální
měsíční	měsíční	k2eAgFnSc1d1	měsíční
platba	platba	k1gFnSc1	platba
3	[number]	k4	3
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
základní	základní	k2eAgFnSc6d1	základní
15	[number]	k4	15
<g/>
%	%	kIx~	%
sazbě	sazba	k1gFnSc3	sazba
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
3	[number]	k4	3
600	[number]	k4	600
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zvýhodnění	zvýhodnění	k1gNnPc4	zvýhodnění
při	při	k7c6	při
spoření	spoření	k1gNnSc6	spoření
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
====	====	k?	====
</s>
</p>
<p>
<s>
Zvýhodnění	zvýhodnění	k1gNnSc1	zvýhodnění
příspěvků	příspěvek	k1gInPc2	příspěvek
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
</s>
</p>
<p>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
je	být	k5eAaImIp3nS	být
daňově	daňově	k6eAd1	daňově
uznatelným	uznatelný	k2eAgInSc7d1	uznatelný
nákladem	náklad	k1gInSc7	náklad
v	v	k7c6	v
neomezené	omezený	k2eNgFnSc6d1	neomezená
výši	výše	k1gFnSc6	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
od	od	k7c2	od
placení	placení	k1gNnSc2	placení
pojistného	pojistné	k1gNnSc2	pojistné
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
příspěvku	příspěvek	k1gInSc2	příspěvek
50	[number]	k4	50
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
limit	limit	k1gInSc4	limit
pro	pro	k7c4	pro
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c4	na
doplňkové	doplňkový	k2eAgNnSc4d1	doplňkové
penzijní	penzijní	k2eAgNnSc4d1	penzijní
spoření	spoření	k1gNnSc4	spoření
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
původního	původní	k2eAgNnSc2d1	původní
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
<g/>
)	)	kIx)	)
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
<g/>
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
</s>
</p>
<p>
<s>
Osvobození	osvobození	k1gNnSc4	osvobození
příspěvku	příspěvek	k1gInSc2	příspěvek
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
od	od	k7c2	od
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
od	od	k7c2	od
placení	placení	k1gNnSc2	placení
pojistného	pojistné	k1gNnSc2	pojistné
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
<g/>
obojí	oboj	k1gFnSc7	oboj
max	max	kA	max
<g/>
.	.	kIx.	.
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
příspěvku	příspěvek	k1gInSc2	příspěvek
50	[number]	k4	50
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
limit	limit	k1gInSc4	limit
u	u	k7c2	u
jednoho	jeden	k4xCgMnSc2	jeden
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
pro	pro	k7c4	pro
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c4	na
penzijní	penzijní	k2eAgNnSc4d1	penzijní
připojištění	připojištění	k1gNnSc4	připojištění
<g/>
/	/	kIx~	/
<g/>
doplňkové	doplňkový	k2eAgNnSc4d1	doplňkové
penzijní	penzijní	k2eAgNnSc4d1	penzijní
spoření	spoření	k1gNnSc4	spoření
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Garance	garance	k1gFnSc1	garance
===	===	k?	===
</s>
</p>
<p>
<s>
Spoření	spoření	k1gNnSc1	spoření
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
fondu	fond	k1gInSc2	fond
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
původní	původní	k2eAgFnSc4d1	původní
vlastnost	vlastnost	k1gFnSc4	vlastnost
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
-	-	kIx~	-
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
penzijní	penzijní	k2eAgFnSc1d1	penzijní
společnost	společnost	k1gFnSc1	společnost
musí	muset	k5eAaImIp3nS	muset
zajistit	zajistit	k5eAaPmF	zajistit
připsání	připsání	k1gNnSc1	připsání
pouze	pouze	k6eAd1	pouze
kladného	kladný	k2eAgInSc2d1	kladný
výsledku	výsledek	k1gInSc2	výsledek
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
roční	roční	k2eAgFnSc6d1	roční
bázi	báze	k1gFnSc6	báze
tak	tak	k6eAd1	tak
penzijní	penzijní	k2eAgFnSc1d1	penzijní
společnost	společnost	k1gFnSc1	společnost
ručí	ručit	k5eAaImIp3nS	ručit
za	za	k7c4	za
kladné	kladný	k2eAgNnSc4d1	kladné
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
podmínky	podmínka	k1gFnSc2	podmínka
je	být	k5eAaImIp3nS	být
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
charakter	charakter	k1gInSc4	charakter
investování	investování	k1gNnSc2	investování
transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Penzijní	penzijní	k2eAgFnSc2d1	penzijní
společnosti	společnost	k1gFnSc2	společnost
také	také	k9	také
zůstala	zůstat	k5eAaPmAgFnS	zůstat
možnost	možnost	k1gFnSc4	možnost
účtovat	účtovat	k5eAaImF	účtovat
si	se	k3xPyFc3	se
za	za	k7c4	za
správu	správa	k1gFnSc4	správa
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
Transformovaném	transformovaný	k2eAgInSc6d1	transformovaný
fondu	fond	k1gInSc6	fond
vyšší	vysoký	k2eAgInPc4d2	vyšší
poplatky	poplatek	k1gInPc4	poplatek
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
účastnických	účastnický	k2eAgInPc2d1	účastnický
fondů	fond	k1gInPc2	fond
Doplňkového	doplňkový	k2eAgNnSc2d1	doplňkové
penzijního	penzijní	k2eAgNnSc2d1	penzijní
spoření	spoření	k1gNnSc2	spoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
investování	investování	k1gNnSc2	investování
podléhají	podléhat	k5eAaImIp3nP	podléhat
kontrole	kontrola	k1gFnSc3	kontrola
depozitáře	depozitář	k1gInSc2	depozitář
a	a	k8xC	a
státnímu	státní	k2eAgInSc3d1	státní
dozoru	dozor	k1gInSc3	dozor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Penzijní	penzijní	k2eAgInPc1d1	penzijní
fondy	fond	k1gInPc1	fond
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
povinny	povinen	k2eAgFnPc1d1	povinna
připsat	připsat	k5eAaPmF	připsat
klientům	klient	k1gMnPc3	klient
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
%	%	kIx~	%
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
dosaženého	dosažený	k2eAgInSc2d1	dosažený
za	za	k7c4	za
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
se	se	k3xPyFc4	se
povinně	povinně	k6eAd1	povinně
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
rezervního	rezervní	k2eAgInSc2d1	rezervní
fondu	fond	k1gInSc2	fond
a	a	k8xC	a
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ukončení	ukončení	k1gNnSc1	ukončení
spoření	spoření	k1gNnSc2	spoření
<g/>
,	,	kIx,	,
výplata	výplata	k1gFnSc1	výplata
úspor	úspora	k1gFnPc2	úspora
z	z	k7c2	z
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
===	===	k?	===
</s>
</p>
<p>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
a	a	k8xC	a
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
penzijním	penzijní	k2eAgInSc7d1	penzijní
plánem	plán	k1gInSc7	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Starobní	starobní	k2eAgFnSc2d1	starobní
penze	penze	k1gFnSc2	penze
====	====	k?	====
</s>
</p>
<p>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
starobní	starobní	k2eAgFnSc2d1	starobní
penze	penze	k1gFnSc2	penze
vzniká	vznikat	k5eAaImIp3nS	vznikat
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
60	[number]	k4	60
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
klienta	klient	k1gMnSc2	klient
a	a	k8xC	a
po	po	k7c6	po
minimálně	minimálně	k6eAd1	minimálně
60	[number]	k4	60
měsících	měsíc	k1gInPc6	měsíc
placení	placení	k1gNnSc6	placení
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyplácené	vyplácený	k2eAgFnSc6d1	vyplácená
částce	částka	k1gFnSc6	částka
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
všechny	všechen	k3xTgInPc1	všechen
vložené	vložený	k2eAgInPc1d1	vložený
prostředky	prostředek	k1gInPc1	prostředek
(	(	kIx(	(
<g/>
státní	státní	k2eAgInPc4d1	státní
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
,	,	kIx,	,
příspěvky	příspěvek	k1gInPc4	příspěvek
zaměstnavatele	zaměstnavatel	k1gMnSc4	zaměstnavatel
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starobní	starobní	k2eAgFnSc1d1	starobní
penze	penze	k1gFnSc1	penze
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyplacena	vyplatit	k5eAaPmNgFnS	vyplatit
jednorázově	jednorázově	k6eAd1	jednorázově
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
měsíční	měsíční	k2eAgFnSc2d1	měsíční
renty	renta	k1gFnSc2	renta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výsluhová	výsluhový	k2eAgFnSc1d1	výsluhová
penze	penze	k1gFnSc1	penze
====	====	k?	====
</s>
</p>
<p>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
výsluhové	výsluhový	k2eAgFnSc2d1	výsluhová
penze	penze	k1gFnSc2	penze
vzniká	vznikat	k5eAaImIp3nS	vznikat
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
placení	placení	k1gNnSc2	placení
penzijního	penzijní	k2eAgNnSc2d1	penzijní
připojištění	připojištění	k1gNnSc2	připojištění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyplácena	vyplácet	k5eAaImNgFnS	vyplácet
max	max	kA	max
<g/>
.	.	kIx.	.
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
%	%	kIx~	%
celkově	celkově	k6eAd1	celkově
naspořené	naspořený	k2eAgFnPc4d1	naspořená
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Výsluhová	výsluhový	k2eAgFnSc1d1	výsluhová
penze	penze	k1gFnSc1	penze
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
smlouvě	smlouva	k1gFnSc6	smlouva
sjednána	sjednat	k5eAaPmNgFnS	sjednat
<g/>
.	.	kIx.	.
</s>
<s>
Výsluhová	výsluhový	k2eAgFnSc1d1	výsluhová
penze	penze	k1gFnSc1	penze
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyplacena	vyplatit	k5eAaPmNgFnS	vyplatit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
starobní	starobní	k2eAgFnSc1d1	starobní
jednorázově	jednorázově	k6eAd1	jednorázově
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
měsíční	měsíční	k2eAgFnSc2d1	měsíční
renty	renta	k1gFnSc2	renta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
můžeme	moct	k5eAaImIp1nP	moct
popsat	popsat	k5eAaPmF	popsat
spoření	spoření	k1gNnSc4	spoření
v	v	k7c6	v
transformovaném	transformovaný	k2eAgInSc6d1	transformovaný
fondu	fond	k1gInSc6	fond
(	(	kIx(	(
<g/>
penzijním	penzijní	k2eAgNnSc6d1	penzijní
připojištění	připojištění	k1gNnSc6	připojištění
<g/>
)	)	kIx)	)
jako	jako	k9	jako
dvě	dva	k4xCgNnPc4	dva
konta	konto	k1gNnPc4	konto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vložená	vložený	k2eAgFnSc1d1	vložená
částka	částka	k1gFnSc1	částka
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
konta	konto	k1gNnPc4	konto
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
starobní	starobní	k2eAgFnSc2d1	starobní
penze	penze	k1gFnSc2	penze
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
výsluhové	výsluhový	k2eAgFnSc2d1	výsluhová
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
</s>
<s>
Konto	konto	k1gNnSc1	konto
starobní	starobní	k2eAgFnSc2d1	starobní
penze	penze	k1gFnSc2	penze
lze	lze	k6eAd1	lze
vybrat	vybrat	k5eAaPmF	vybrat
kdykoli	kdykoli	k6eAd1	kdykoli
po	po	k7c6	po
dovršení	dovršení	k1gNnSc6	dovršení
60	[number]	k4	60
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
konto	konto	k1gNnSc1	konto
výsluhové	výsluhový	k2eAgFnSc2d1	výsluhová
penze	penze	k1gFnSc2	penze
kdykoliv	kdykoliv	k6eAd1	kdykoliv
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
spoření	spoření	k1gNnSc2	spoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Invalidní	invalidní	k2eAgFnSc2d1	invalidní
penze	penze	k1gFnSc2	penze
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vyplácena	vyplácet	k5eAaImNgFnS	vyplácet
po	po	k7c4	po
prokázání	prokázání	k1gNnSc4	prokázání
přiznání	přiznání	k1gNnSc2	přiznání
plného	plný	k2eAgInSc2d1	plný
invalidního	invalidní	k2eAgInSc2d1	invalidní
důchodu	důchod	k1gInSc2	důchod
podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
penzijního	penzijní	k2eAgInSc2d1	penzijní
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pozůstalostní	pozůstalostní	k2eAgFnSc2d1	pozůstalostní
penze	penze	k1gFnSc2	penze
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vyplácena	vyplácet	k5eAaImNgFnS	vyplácet
oprávněným	oprávněný	k2eAgFnPc3d1	oprávněná
osobám	osoba	k1gFnPc3	osoba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
smrti	smrt	k1gFnSc2	smrt
spořicího	spořicí	k2eAgMnSc4d1	spořicí
klienta	klient	k1gMnSc4	klient
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
vzniká	vznikat	k5eAaImIp3nS	vznikat
po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
36	[number]	k4	36
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
splátek	splátka	k1gFnPc2	splátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Odbytné	odbytný	k2eAgMnPc4d1	odbytný
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
předčasného	předčasný	k2eAgNnSc2d1	předčasné
ukončení	ukončení	k1gNnSc2	ukončení
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
před	před	k7c7	před
nárokem	nárok	k1gInSc7	nárok
na	na	k7c4	na
starobní	starobní	k2eAgFnSc4d1	starobní
a	a	k8xC	a
výsluhovou	výsluhový	k2eAgFnSc4d1	výsluhová
penzi	penze	k1gFnSc4	penze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
výplata	výplata	k1gFnSc1	výplata
úspor	úspora	k1gFnPc2	úspora
bez	bez	k7c2	bez
státních	státní	k2eAgInPc2d1	státní
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
odbytné	odbytné	k1gNnSc4	odbytné
vzniká	vznikat	k5eAaImIp3nS	vznikat
zaplacením	zaplacení	k1gNnSc7	zaplacení
minimálně	minimálně	k6eAd1	minimálně
12	[number]	k4	12
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
plateb	platba	k1gFnPc2	platba
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
r.	r.	kA	r.
2009	[number]	k4	2009
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
(	(	kIx(	(
<g/>
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
zaplacením	zaplacení	k1gNnSc7	zaplacení
poplatku	poplatek	k1gInSc2	poplatek
až	až	k9	až
800	[number]	k4	800
Kč	Kč	kA	Kč
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neuplynulo	uplynout	k5eNaPmAgNnS	uplynout
5	[number]	k4	5
let	léto	k1gNnPc2	léto
od	od	k7c2	od
sjednání	sjednání	k1gNnSc2	sjednání
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odkazy	odkaz	k1gInPc4	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Reference	reference	k1gFnPc1	reference
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
====	====	k?	====
</s>
</p>
<p>
<s>
Finance	finance	k1gFnPc1	finance
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Penzijní	penzijní	k2eAgNnSc1d1	penzijní
připojištění	připojištění	k1gNnSc1	připojištění
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
Doplňkové	doplňkový	k2eAgNnSc1d1	doplňkové
penzijní	penzijní	k2eAgNnSc1d1	penzijní
spoření	spoření	k1gNnSc1	spoření
a	a	k8xC	a
důchodové	důchodový	k2eAgNnSc1d1	důchodové
spoření	spoření	k1gNnSc1	spoření
</s>
</p>
<p>
<s>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
427	[number]	k4	427
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
...	...	k?	...
http://portal.gov.cz/app/zakony/zakon.jsp?page=0&	[url]	k?	http://portal.gov.cz/app/zakony/zakon.jsp?page=0&
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
penzijních	penzijní	k2eAgFnPc2d1	penzijní
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
ČR	ČR	kA	ČR
...	...	k?	...
https://web.archive.org/web/20160304123127/http://www.certifikacni.cz/Penzijni-spolecnosti-v-CR-cl37.aspx	[url]	k1gInSc1	https://web.archive.org/web/20160304123127/http://www.certifikacni.cz/Penzijni-spolecnosti-v-CR-cl37.aspx
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
penzijních	penzijní	k2eAgInPc2d1	penzijní
fondů	fond	k1gInPc2	fond
ČR	ČR	kA	ČR
...	...	k?	...
http://www.apfcr.cz/	[url]	k?	http://www.apfcr.cz/
</s>
</p>
