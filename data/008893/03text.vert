<p>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
příslušníka	příslušník	k1gMnSc2	příslušník
důstojnického	důstojnický	k2eAgInSc2d1	důstojnický
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
hodnosti	hodnost	k1gFnSc3	hodnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
i	i	k9	i
v	v	k7c6	v
policejních	policejní	k2eAgInPc6d1	policejní
sborech	sbor	k1gInPc6	sbor
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
hierarchicky	hierarchicky	k6eAd1	hierarchicky
strukturovaných	strukturovaný	k2eAgFnPc6d1	strukturovaná
složkách	složka	k1gFnPc6	složka
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
velí	velet	k5eAaImIp3nS	velet
zpravidla	zpravidla	k6eAd1	zpravidla
rotě	rota	k1gFnSc3	rota
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
četě	četa	k1gFnSc3	četa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnost	hodnost	k1gFnSc4	hodnost
nižšího	nízký	k2eAgMnSc2d2	nižší
důstojníka	důstojník	k1gMnSc2	důstojník
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
též	též	k9	též
zkratkou	zkratka	k1gFnSc7	zkratka
kpt.	kpt.	k?	kpt.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc4d3	nejbližší
nižší	nízký	k2eAgFnSc4d2	nižší
hodnost	hodnost	k1gFnSc4	hodnost
je	být	k5eAaImIp3nS	být
nadporučík	nadporučík	k1gMnSc1	nadporučík
a	a	k8xC	a
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hodnost	hodnost	k1gFnSc4	hodnost
je	být	k5eAaImIp3nS	být
major	major	k1gMnSc1	major
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
NATO	nato	k6eAd1	nato
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hodnost	hodnost	k1gFnSc4	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
zařazení	zařazení	k1gNnSc2	zařazení
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
OF-	OF-	k1gFnSc1	OF-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
hodnost	hodnost	k1gFnSc4	hodnost
kapitána	kapitán	k1gMnSc4	kapitán
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
hodností	hodnost	k1gFnSc7	hodnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
nebo	nebo	k8xC	nebo
které	který	k3yQgNnSc1	který
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
neprofesionální	profesionální	k2eNgMnSc1d1	neprofesionální
voják	voják	k1gMnSc1	voják
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zaměňována	zaměňovat	k5eAaImNgFnS	zaměňovat
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
námořního	námořní	k2eAgMnSc2d1	námořní
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hodnosti	hodnost	k1gFnPc4	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
revolucí	revoluce	k1gFnSc7	revoluce
byl	být	k5eAaImAgInS	být
kapitánem	kapitán	k1gMnSc7	kapitán
obvykle	obvykle	k6eAd1	obvykle
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
právo	právo	k1gNnSc4	právo
velení	velení	k1gNnSc2	velení
od	od	k7c2	od
předchozího	předchozí	k2eAgNnSc2d1	předchozí
držitele	držitel	k1gMnSc4	držitel
tohoto	tento	k3xDgNnSc2	tento
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
peníze	peníz	k1gInPc1	peníz
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odcházel	odcházet	k5eAaImAgMnS	odcházet
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
služby	služba	k1gFnSc2	služba
mohl	moct	k5eAaImAgMnS	moct
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
od	od	k7c2	od
jiného	jiný	k2eAgMnSc2d1	jiný
šlechtice	šlechtic	k1gMnSc2	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
poručík	poručík	k1gMnSc1	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
byl	být	k5eAaImAgMnS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
financování	financování	k1gNnPc4	financování
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
náleželo	náležet	k5eAaImAgNnS	náležet
od	od	k7c2	od
panovníka	panovník	k1gMnSc2	panovník
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
kapitán	kapitán	k1gMnSc1	kapitán
mužstvo	mužstvo	k1gNnSc4	mužstvo
neplatil	platit	k5eNaImAgMnS	platit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
vojenským	vojenský	k2eAgInSc7d1	vojenský
soudem	soud	k1gInSc7	soud
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
přečiny	přečin	k1gInPc4	přečin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
a	a	k8xC	a
panovník	panovník	k1gMnSc1	panovník
mohl	moct	k5eAaImAgMnS	moct
prodat	prodat	k5eAaPmF	prodat
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Zlatým	zlatý	k2eAgFnPc3d1	zlatá
trojcípým	trojcípý	k2eAgFnPc3d1	trojcípá
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
hodnostní	hodnostní	k2eAgNnSc4d1	hodnostní
označení	označení	k1gNnSc4	označení
kapitána	kapitán	k1gMnSc2	kapitán
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hovorově	hovorově	k6eAd1	hovorově
říká	říkat	k5eAaImIp3nS	říkat
mercedesy	mercedes	k1gInPc4	mercedes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
nalodění	nalodění	k1gNnSc6	nalodění
pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
pozemní	pozemní	k2eAgMnPc1d1	pozemní
velitelé	velitel	k1gMnPc1	velitel
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
dočasně	dočasně	k6eAd1	dočasně
povyšují	povyšovat	k5eAaImIp3nP	povyšovat
na	na	k7c4	na
majory	major	k1gMnPc4	major
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
kolizím	kolize	k1gFnPc3	kolize
terminologie	terminologie	k1gFnSc2	terminologie
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnostní	hodnostní	k2eAgNnSc1d1	hodnostní
označení	označení	k1gNnSc1	označení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
níže	nízce	k6eAd2	nízce
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
různá	různý	k2eAgNnPc1d1	různé
hodnostní	hodnostní	k2eAgNnPc1d1	hodnostní
označení	označení	k1gNnPc1	označení
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obdobné	obdobný	k2eAgFnPc4d1	obdobná
hodnosti	hodnost	k1gFnPc4	hodnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Captain	Captain	k1gMnSc1	Captain
(	(	kIx(	(
<g/>
armed	armed	k1gMnSc1	armed
forces	forces	k1gMnSc1	forces
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kapitán	kapitán	k1gMnSc1	kapitán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kapitán	kapitán	k1gMnSc1	kapitán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
