<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc1d3	veliký
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
planetární	planetární	k2eAgNnSc1d1	planetární
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
současných	současný	k2eAgInPc2d1	současný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
nejspíše	nejspíše	k9	nejspíše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
jediný	jediný	k2eAgInSc4d1	jediný
přirozený	přirozený	k2eAgInSc4d1	přirozený
satelit	satelit	k1gInSc4	satelit
–	–	k?	–
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
elipse	elipsa	k1gFnSc6	elipsa
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc7d1	malá
excentricitou	excentricita	k1gFnSc7	excentricita
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
jako	jako	k8xC	jako
domovský	domovský	k2eAgInSc1d1	domovský
svět	svět	k1gInSc1	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
názvů	název	k1gInPc2	název
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
národu	národ	k1gInSc6	národ
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
název	název	k1gInSc4	název
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
Terra	Terro	k1gNnSc2	Terro
<g/>
,	,	kIx,	,
či	či	k8xC	či
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Gaia	Gai	k1gInSc2	Gai
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
dynamickou	dynamický	k2eAgFnSc7d1	dynamická
planetou	planeta	k1gFnSc7	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemských	zemský	k2eAgFnPc2d1	zemská
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nedokonalou	dokonalý	k2eNgFnSc4d1	nedokonalá
kouli	koule	k1gFnSc4	koule
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
6378	[number]	k4	6378
km	km	kA	km
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malé	malý	k2eAgNnSc1d1	malé
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
obklopené	obklopený	k2eAgNnSc1d1	obklopené
polotekutým	polotekutý	k2eAgNnSc7d1	polotekuté
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
litosférickými	litosférický	k2eAgFnPc7d1	litosférická
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
pohybu	pohyb	k1gInSc6	pohyb
vlivem	vliv	k1gInSc7	vliv
procesu	proces	k1gInSc2	proces
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
souvislého	souvislý	k2eAgInSc2d1	souvislý
oceánu	oceán	k1gInSc2	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
71	[number]	k4	71
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
úzkém	úzký	k2eAgInSc6d1	úzký
pásu	pás	k1gInSc6	pás
rozhraní	rozhraní	k1gNnSc2	rozhraní
mezi	mezi	k7c7	mezi
litosférou	litosféra	k1gFnSc7	litosféra
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
biosféra	biosféra	k1gFnSc1	biosféra
<g/>
,	,	kIx,	,
živý	živý	k2eAgInSc4d1	živý
obal	obal	k1gInSc4	obal
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
činností	činnost	k1gFnSc7	činnost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
části	část	k1gFnSc2	část
litosféry	litosféra	k1gFnSc2	litosféra
na	na	k7c4	na
půdní	půdní	k2eAgInSc4d1	půdní
obal	obal	k1gInSc4	obal
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pedosféru	pedosféra	k1gFnSc4	pedosféra
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
směs	směs	k1gFnSc4	směs
obvykle	obvykle	k6eAd1	obvykle
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
jako	jako	k8xC	jako
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
reprezentujícího	reprezentující	k2eAgMnSc2d1	reprezentující
poledník	poledník	k1gInSc1	poledník
a	a	k8xC	a
rovník	rovník	k1gInSc1	rovník
<g/>
;	;	kIx,	;
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
variantách	varianta	k1gFnPc6	varianta
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc1	kříž
vysunut	vysunout	k5eAaPmNgInS	vysunout
nad	nad	k7c4	nad
kruh	kruh	k1gInSc4	kruh
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
⊕	⊕	k?	⊕
nebo	nebo	k8xC	nebo
♁	♁	k?	♁
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
slov	slovo	k1gNnPc2	slovo
odvozených	odvozený	k2eAgInPc2d1	odvozený
od	od	k7c2	od
Terra	Terro	k1gNnSc2	Terro
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
terestrický	terestrický	k2eAgMnSc1d1	terestrický
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pojmy	pojem	k1gInPc4	pojem
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
také	také	k6eAd1	také
prefix	prefix	k1gInSc4	prefix
telur-	telur-	k?	telur-
nebo	nebo	k8xC	nebo
tellur-	tellur-	k?	tellur-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
telurický	telurický	k2eAgMnSc1d1	telurický
<g/>
,	,	kIx,	,
tellurit	tellurit	k1gInSc1	tellurit
podle	podle	k7c2	podle
bohyně	bohyně	k1gFnSc2	bohyně
Tellū	Tellū	k1gFnSc2	Tellū
<g/>
)	)	kIx)	)
a	a	k8xC	a
geo-	geo-	k?	geo-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc1	geologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgInSc7d1	domovský
světem	svět	k1gInSc7	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c6	na
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
skrze	skrze	k?	skrze
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc4	cestování
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
slunečního	sluneční	k2eAgInSc2d1	sluneční
systému	systém	k1gInSc2	systém
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
akrecí	akrece	k1gFnPc2	akrece
z	z	k7c2	z
pracho-plynného	pracholynný	k2eAgInSc2d1	pracho-plynný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
rodící	rodící	k2eAgFnSc2d1	rodící
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Srážkami	srážka	k1gFnPc7	srážka
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
formovat	formovat	k5eAaImF	formovat
malá	malý	k2eAgNnPc1d1	malé
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
další	další	k2eAgFnSc1d1	další
částice	částice	k1gFnSc1	částice
a	a	k8xC	a
okolní	okolní	k2eAgInSc1d1	okolní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
první	první	k4xOgFnPc1	první
planetesimály	planetesimála	k1gFnPc1	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
srážely	srážet	k5eAaImAgInP	srážet
a	a	k8xC	a
formovaly	formovat	k5eAaImAgInP	formovat
větší	veliký	k2eAgFnPc4d2	veliký
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgInPc1	čtyři
terestrické	terestrický	k2eAgInPc1d1	terestrický
protoplanety	protoplanet	k1gInPc1	protoplanet
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
srážky	srážka	k1gFnPc1	srážka
planetisimál	planetisimála	k1gFnPc2	planetisimála
společně	společně	k6eAd1	společně
s	s	k7c7	s
uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
teplem	teplo	k1gNnSc7	teplo
z	z	k7c2	z
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
rozpadů	rozpad	k1gInPc2	rozpad
roztavily	roztavit	k5eAaPmAgInP	roztavit
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
roztavený	roztavený	k2eAgInSc1d1	roztavený
povrch	povrch	k1gInSc1	povrch
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
přibližně	přibližně	k6eAd1	přibližně
miliardu	miliarda	k4xCgFnSc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zformování	zformování	k1gNnSc1	zformování
protoplanety	protoplanet	k1gInPc7	protoplanet
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
bombardování	bombardování	k1gNnSc3	bombardování
povrchu	povrch	k1gInSc2	povrch
zbylým	zbylý	k2eAgInSc7d1	zbylý
materiálem	materiál	k1gInSc7	materiál
ze	z	k7c2	z
vzniku	vznik	k1gInSc2	vznik
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	on	k3xPp3gInSc2	on
neustálé	neustálý	k2eAgNnSc1d1	neustálé
přetváření	přetváření	k1gNnSc1	přetváření
<g/>
,	,	kIx,	,
přetavování	přetavování	k1gNnSc1	přetavování
a	a	k8xC	a
přínos	přínos	k1gInSc1	přínos
nového	nový	k2eAgInSc2d1	nový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
roztaven	roztavit	k5eAaPmNgInS	roztavit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
docházelo	docházet	k5eAaImAgNnS	docházet
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
k	k	k7c3	k
diferenciací	diferenciace	k1gFnPc2	diferenciace
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
když	když	k8xS	když
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
klesaly	klesat	k5eAaImAgFnP	klesat
vlivem	vlivem	k7c2	vlivem
gravitace	gravitace	k1gFnSc2	gravitace
do	do	k7c2	do
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
těžkého	těžký	k2eAgNnSc2d1	těžké
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
lehké	lehký	k2eAgInPc1d1	lehký
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
zasloužily	zasloužit	k5eAaPmAgInP	zasloužit
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
sféra	sféra	k1gFnSc1	sféra
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
nálezy	nález	k1gInPc1	nález
nejstarších	starý	k2eAgFnPc2d3	nejstarší
hornin	hornina	k1gFnPc2	hornina
starých	starý	k2eAgFnPc2d1	stará
až	až	k9	až
4	[number]	k4	4
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
Země	zem	k1gFnSc2	zem
zůstala	zůstat	k5eAaPmAgFnS	zůstat
akumulovaná	akumulovaný	k2eAgFnSc1d1	akumulovaná
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
předchozích	předchozí	k2eAgNnPc2d1	předchozí
období	období	k1gNnPc2	období
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
rozpady	rozpad	k1gInPc7	rozpad
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
uvolňovalo	uvolňovat	k5eAaImAgNnS	uvolňovat
do	do	k7c2	do
svrchních	svrchní	k2eAgFnPc2d1	svrchní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
vznik	vznik	k1gInSc4	vznik
aktivního	aktivní	k2eAgInSc2d1	aktivní
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
,	,	kIx,	,
tektonických	tektonický	k2eAgInPc2d1	tektonický
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
deskové	deskový	k2eAgFnPc4d1	desková
tektoniky	tektonika	k1gFnPc4	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
lávových	lávový	k2eAgFnPc2d1	lávová
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
uvolňovalo	uvolňovat	k5eAaImAgNnS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
atmosféře	atmosféra	k1gFnSc3	atmosféra
tvořené	tvořený	k2eAgFnPc1d1	tvořená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
unikla	uniknout	k5eAaPmAgFnS	uniknout
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
Země	zem	k1gFnSc2	zem
svojí	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgInPc1d1	neustálý
dopady	dopad	k1gInPc1	dopad
komet	kometa	k1gFnPc2	kometa
zvyšovaly	zvyšovat	k5eAaImAgInP	zvyšovat
obsah	obsah	k1gInSc4	obsah
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
teploty	teplota	k1gFnSc2	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vznik	vznik	k1gInSc4	vznik
prvních	první	k4xOgFnPc2	první
výrazných	výrazný	k2eAgFnPc2d1	výrazná
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc1	déšť
se	se	k3xPyFc4	se
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
okamžitě	okamžitě	k6eAd1	okamžitě
vypařil	vypařit	k5eAaPmAgMnS	vypařit
a	a	k8xC	a
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
opět	opět	k6eAd1	opět
zkondenzoval	zkondenzovat	k5eAaPmAgMnS	zkondenzovat
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
nesčetněkrát	nesčetněkrát	k6eAd1	nesčetněkrát
opakoval	opakovat	k5eAaImAgMnS	opakovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
postupně	postupně	k6eAd1	postupně
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
vody	voda	k1gFnSc2	voda
umožnila	umožnit	k5eAaPmAgFnS	umožnit
navazování	navazování	k1gNnSc4	navazování
uhlíku	uhlík	k1gInSc2	uhlík
do	do	k7c2	do
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zmenšovalo	zmenšovat	k5eAaImAgNnS	zmenšovat
jeho	jeho	k3xOp3gNnSc1	jeho
zastoupení	zastoupení	k1gNnSc1	zastoupení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc4	první
život	život	k1gInSc4	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c7	před
4	[number]	k4	4
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
ještě	ještě	k6eAd1	ještě
obohacena	obohatit	k5eAaPmNgFnS	obohatit
volným	volný	k2eAgInSc7d1	volný
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
reakční	reakční	k2eAgNnSc4d1	reakční
činidlo	činidlo	k1gNnSc4	činidlo
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
potřebných	potřebný	k2eAgFnPc2d1	potřebná
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
primitivní	primitivní	k2eAgInPc4d1	primitivní
organismy	organismus	k1gInPc4	organismus
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začaly	začít	k5eAaPmAgFnP	začít
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
jen	jen	k9	jen
vzácným	vzácný	k2eAgInSc7d1	vzácný
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Postupnou	postupný	k2eAgFnSc7d1	postupná
činností	činnost	k1gFnSc7	činnost
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přetvoření	přetvoření	k1gNnSc3	přetvoření
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
prvků	prvek	k1gInPc2	prvek
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
reagoval	reagovat	k5eAaBmAgMnS	reagovat
s	s	k7c7	s
dopadajícím	dopadající	k2eAgNnSc7d1	dopadající
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
a	a	k8xC	a
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
navázání	navázání	k1gNnSc3	navázání
na	na	k7c4	na
ozón	ozón	k1gInSc4	ozón
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k9	tak
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
dopadu	dopad	k1gInSc6	dopad
škodlivého	škodlivý	k2eAgNnSc2d1	škodlivé
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rozšíření	rozšíření	k1gNnSc1	rozšíření
života	život	k1gInSc2	život
i	i	k9	i
mimo	mimo	k7c4	mimo
oblasti	oblast	k1gFnPc4	oblast
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7	rozšíření
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
začal	začít	k5eAaPmAgMnS	začít
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
i	i	k9	i
další	další	k2eAgInSc4d1	další
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
rozkladných	rozkladný	k2eAgInPc2d1	rozkladný
procesů	proces	k1gInPc2	proces
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
kamenitém	kamenitý	k2eAgInSc6d1	kamenitý
povrchu	povrch	k1gInSc6	povrch
oproti	oproti	k7c3	oproti
plynným	plynný	k2eAgMnPc3d1	plynný
obrům	obr	k1gMnPc3	obr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
či	či	k8xC	či
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
převážně	převážně	k6eAd1	převážně
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
prvenství	prvenství	k1gNnPc4	prvenství
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
také	také	k9	také
mezi	mezi	k7c7	mezi
terestrickými	terestrický	k2eAgNnPc7d1	terestrické
tělesy	těleso	k1gNnPc7	těleso
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
nejsilnějším	silný	k2eAgNnSc7d3	nejsilnější
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
rotací	rotace	k1gFnSc7	rotace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
aktivní	aktivní	k2eAgFnSc4d1	aktivní
deskovou	deskový	k2eAgFnSc4d1	desková
tektoniku	tektonika	k1gFnSc4	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
6,5	[number]	k4	6,5
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
křivost	křivost	k1gFnSc1	křivost
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnPc4	zakřivení
způsobená	způsobený	k2eAgFnSc1d1	způsobená
geologickou	geologický	k2eAgFnSc7d1	geologická
aktivitou	aktivita	k1gFnSc7	aktivita
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
výraznější	výrazný	k2eAgFnPc4d2	výraznější
než	než	k8xS	než
zakřivení	zakřivení	k1gNnPc1	zakřivení
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kulatosti	kulatost	k1gFnSc2	kulatost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
názoru	názor	k1gInSc3	názor
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
svědčily	svědčit	k5eAaImAgFnP	svědčit
různé	různý	k2eAgInPc4d1	různý
vědecké	vědecký	k2eAgInPc4d1	vědecký
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
například	například	k6eAd1	například
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jižní	jižní	k2eAgNnPc1d1	jižní
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
v	v	k7c6	v
jižnějších	jižní	k2eAgFnPc6d2	jižnější
oblastech	oblast	k1gFnPc6	oblast
vycházejí	vycházet	k5eAaImIp3nP	vycházet
výš	vysoce	k6eAd2	vysoce
nad	nad	k7c4	nad
obzor	obzor	k1gInSc4	obzor
a	a	k8xC	a
také	také	k9	také
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
Země	zem	k1gFnSc2	zem
vrhá	vrhat	k5eAaImIp3nS	vrhat
vždy	vždy	k6eAd1	vždy
kruhový	kruhový	k2eAgInSc4d1	kruhový
stín	stín	k1gInSc4	stín
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
Země	zem	k1gFnSc2	zem
poprvé	poprvé	k6eAd1	poprvé
spočítal	spočítat	k5eAaPmAgMnS	spočítat
Eratosthenés	Eratosthenés	k1gInSc4	Eratosthenés
z	z	k7c2	z
Kyrény	Kyréna	k1gFnSc2	Kyréna
podle	podle	k7c2	podle
rozdílu	rozdíl	k1gInSc2	rozdíl
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
poledního	polední	k2eAgInSc2d1	polední
stínu	stín	k1gInSc2	stín
mezi	mezi	k7c7	mezi
Asuánem	Asuán	k1gInSc7	Asuán
a	a	k8xC	a
Alexandrií	Alexandrie	k1gFnSc7	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Kulatost	kulatost	k1gFnSc1	kulatost
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
i	i	k8xC	i
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
centrálně	centrálně	k6eAd1	centrálně
kolem	kolem	k7c2	kolem
těžiště	těžiště	k1gNnSc2	těžiště
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
sférickou	sférický	k2eAgFnSc4d1	sférická
symetrii	symetrie	k1gFnSc4	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
však	však	k9	však
narušen	narušen	k2eAgMnSc1d1	narušen
<g/>
.	.	kIx.	.
</s>
<s>
Lepším	dobrý	k2eAgNnSc7d2	lepší
přiblížením	přiblížení	k1gNnSc7	přiblížení
skutečnosti	skutečnost	k1gFnSc2	skutečnost
je	být	k5eAaImIp3nS	být
rotační	rotační	k2eAgInSc1d1	rotační
elipsoid	elipsoid	k1gInSc1	elipsoid
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
excentricitou	excentricita	k1gFnSc7	excentricita
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
pólů	pól	k1gInPc2	pól
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
43	[number]	k4	43
km	km	kA	km
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
střední	střední	k2eAgFnSc2d1	střední
rovníkový	rovníkový	k2eAgInSc4d1	rovníkový
průměr	průměr	k1gInSc4	průměr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rotací	rotace	k1gFnSc7	rotace
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
odstředivou	odstředivý	k2eAgFnSc4d1	odstředivá
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
směřuje	směřovat	k5eAaImIp3nS	směřovat
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
vektorově	vektorově	k6eAd1	vektorově
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
s	s	k7c7	s
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
a	a	k8xC	a
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
nejmenší	malý	k2eAgInPc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
kružnice	kružnice	k1gFnPc1	kružnice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poledníky	poledník	k1gInPc1	poledník
jsou	být	k5eAaImIp3nP	být
elipsy	elipsa	k1gFnPc4	elipsa
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
výstředností	výstřednost	k1gFnSc7	výstřednost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
matematický	matematický	k2eAgInSc4d1	matematický
popis	popis	k1gInSc4	popis
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pojem	pojem	k1gInSc1	pojem
geoid	geoid	k1gInSc1	geoid
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenějším	vzdálený	k2eAgNnSc7d3	nejvzdálenější
místem	místo	k1gNnSc7	místo
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
jejímu	její	k3xOp3gNnSc3	její
zploštění	zploštění	k1gNnSc1	zploštění
vrcholek	vrcholek	k1gInSc4	vrcholek
hory	hora	k1gFnSc2	hora
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
terestrické	terestrický	k2eAgFnPc4d1	terestrická
planety	planeta	k1gFnPc4	planeta
vnitřně	vnitřně	k6eAd1	vnitřně
diferencována	diferencován	k2eAgFnSc1d1	diferencována
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
křemíkovou	křemíkový	k2eAgFnSc4d1	křemíková
pevnou	pevný	k2eAgFnSc4d1	pevná
kůru	kůra	k1gFnSc4	kůra
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
viskózní	viskózní	k2eAgInSc1d1	viskózní
plášť	plášť	k1gInSc1	plášť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
diferenciaci	diferenciace	k1gFnSc3	diferenciace
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vlivem	k7c2	vlivem
roztavení	roztavení	k1gNnSc2	roztavení
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
gravitací	gravitace	k1gFnPc2	gravitace
směřovaly	směřovat	k5eAaImAgFnP	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
malého	malý	k2eAgNnSc2d1	malé
kompaktního	kompaktní	k2eAgNnSc2d1	kompaktní
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
jádra	jádro	k1gNnSc2	jádro
–	–	k?	–
tzv.	tzv.	kA	tzv.
jadérka	jadérko	k1gNnSc2	jadérko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
současných	současný	k2eAgInPc2d1	současný
poznatků	poznatek	k1gInPc2	poznatek
nejspíše	nejspíše	k9	nejspíše
pevné	pevný	k2eAgFnPc1d1	pevná
a	a	k8xC	a
tvořené	tvořený	k2eAgFnPc1d1	tvořená
převážně	převážně	k6eAd1	převážně
železem	železo	k1gNnSc7	železo
(	(	kIx(	(
<g/>
86,2	[number]	k4	86,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
niklem	nikl	k1gInSc7	nikl
(	(	kIx(	(
<g/>
7,25	[number]	k4	7,25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
pevným	pevný	k2eAgNnSc7d1	pevné
jádrem	jádro	k1gNnSc7	jádro
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
1278	[number]	k4	1278
km	km	kA	km
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnější	vnější	k2eAgNnSc1d1	vnější
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
roztavenou	roztavený	k2eAgFnSc7d1	roztavená
polotekutou	polotekutý	k2eAgFnSc7d1	polotekutá
směsí	směs	k1gFnSc7	směs
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
zasahující	zasahující	k2eAgFnPc4d1	zasahující
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
2900	[number]	k4	2900
km	km	kA	km
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
odděleno	oddělit	k5eAaPmNgNnS	oddělit
Gutenbergovou	Gutenbergův	k2eAgFnSc7d1	Gutenbergova
diskontinuitou	diskontinuita	k1gFnSc7	diskontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Tekuté	tekutý	k2eAgNnSc1d1	tekuté
vnější	vnější	k2eAgNnSc1d1	vnější
jádro	jádro	k1gNnSc1	jádro
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
slabého	slabý	k2eAgNnSc2d1	slabé
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
vlivem	vlivem	k7c2	vlivem
konvekce	konvekce	k1gFnSc2	konvekce
jeho	jeho	k3xOp3gInSc2	jeho
elektricky	elektricky	k6eAd1	elektricky
vodivého	vodivý	k2eAgInSc2d1	vodivý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
akumulovaného	akumulovaný	k2eAgNnSc2d1	akumulované
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pohyb	pohyb	k1gInSc4	pohyb
roztaveného	roztavený	k2eAgInSc2d1	roztavený
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Teplejší	teplý	k2eAgInSc1d2	teplejší
materiál	materiál	k1gInSc1	materiál
ohřátý	ohřátý	k2eAgInSc1d1	ohřátý
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plášťových	plášťový	k2eAgInPc2d1	plášťový
chocholů	chochol	k1gInPc2	chochol
stoupat	stoupat	k5eAaImF	stoupat
a	a	k8xC	a
dostávat	dostávat	k5eAaImF	dostávat
se	se	k3xPyFc4	se
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
proražení	proražení	k1gNnSc3	proražení
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
úniku	únik	k1gInSc2	únik
magmatu	magma	k1gNnSc2	magma
skrz	skrz	k7c4	skrz
sopky	sopka	k1gFnPc4	sopka
a	a	k8xC	a
trhliny	trhlina	k1gFnPc4	trhlina
v	v	k7c6	v
oceánských	oceánský	k2eAgFnPc6d1	oceánská
deskách	deska	k1gFnPc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
tvořena	tvořen	k2eAgFnSc1d1	tvořena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
před	před	k7c7	před
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgFnPc1d3	nejstarší
známé	známý	k2eAgFnPc1d1	známá
žíly	žíla	k1gFnPc1	žíla
minerálů	minerál	k1gInPc2	minerál
jsou	být	k5eAaImIp3nP	být
4,4	[number]	k4	4,4
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
kůru	kůra	k1gFnSc4	kůra
přinejmenším	přinejmenším	k6eAd1	přinejmenším
po	po	k7c4	po
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc4d1	zemské
složení	složení	k1gNnSc4	složení
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgFnSc4d1	variabilní
dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
složení	složení	k1gNnSc1	složení
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
od	od	k7c2	od
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
<g/>
,	,	kIx,	,
plášť	plášť	k1gInSc1	plášť
od	od	k7c2	od
kůry	kůra	k1gFnSc2	kůra
apod.	apod.	kA	apod.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgNnSc4d1	globální
zemské	zemský	k2eAgNnSc4d1	zemské
složení	složení	k1gNnSc4	složení
podle	podle	k7c2	podle
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stratifikace	stratifikace	k1gFnSc2	stratifikace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
vrstev	vrstva	k1gFnPc2	vrstva
tzv.	tzv.	kA	tzv.
geosfér	geosféra	k1gFnPc2	geosféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
volně	volně	k6eAd1	volně
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
detekovány	detekovat	k5eAaImNgFnP	detekovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
šíření	šíření	k1gNnSc2	šíření
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
geosféry	geosféra	k1gFnPc1	geosféra
jsou	být	k5eAaImIp3nP	být
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
řazeny	řadit	k5eAaImNgFnP	řadit
soustředně	soustředně	k6eAd1	soustředně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
obepínají	obepínat	k5eAaImIp3nP	obepínat
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
rozložení	rozložení	k1gNnSc1	rozložení
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
hmotností	hmotnost	k1gFnSc7	hmotnost
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
litosféra	litosféra	k1gFnSc1	litosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
mocnost	mocnost	k1gFnSc4	mocnost
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
(	(	kIx(	(
<g/>
místně	místně	k6eAd1	místně
kolísá	kolísat	k5eAaImIp3nS	kolísat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Litosféra	litosféra	k1gFnSc1	litosféra
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
mocností	mocnost	k1gFnSc7	mocnost
0	[number]	k4	0
až	až	k9	až
35	[number]	k4	35
km	km	kA	km
a	a	k8xC	a
svrchního	svrchní	k2eAgInSc2d1	svrchní
pláště	plášť	k1gInSc2	plášť
s	s	k7c7	s
mocností	mocnost	k1gFnSc7	mocnost
35	[number]	k4	35
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
cca	cca	kA	cca
35	[number]	k4	35
až	až	k9	až
2890	[number]	k4	2890
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
až	až	k9	až
700	[number]	k4	700
km	km	kA	km
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
astenosféra	astenosféra	k1gFnSc1	astenosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pláštěm	plášť	k1gInSc7	plášť
je	být	k5eAaImIp3nS	být
situované	situovaný	k2eAgNnSc1d1	situované
jádro	jádro	k1gNnSc1	jádro
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
2890	[number]	k4	2890
až	až	k9	až
5100	[number]	k4	5100
km	km	kA	km
vnější	vnější	k2eAgFnSc4d1	vnější
tekuté	tekutý	k2eAgNnSc4d1	tekuté
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
5100	[number]	k4	5100
až	až	k9	až
6378	[number]	k4	6378
km	km	kA	km
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zemské	zemský	k2eAgNnSc4d1	zemské
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
5515	[number]	k4	5515
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
nejhustší	hustý	k2eAgFnSc7d3	nejhustší
planetou	planeta	k1gFnSc7	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
materiálu	materiál	k1gInSc2	materiál
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
však	však	k9	však
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
asi	asi	k9	asi
3000	[number]	k4	3000
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
těžší	těžký	k2eAgInPc1d2	těžší
materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
před	před	k7c4	před
asi	asi	k9	asi
4,5	[number]	k4	4,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
povrch	povrch	k7c2wR	povrch
Země	zem	k1gFnSc2	zem
roztaven	roztaven	k2eAgMnSc1d1	roztaven
a	a	k8xC	a
hustší	hustý	k2eAgFnSc1d2	hustší
hmota	hmota	k1gFnSc1	hmota
klesala	klesat	k5eAaImAgFnS	klesat
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
planetární	planetární	k2eAgFnSc2d1	planetární
diferenciace	diferenciace	k1gFnSc2	diferenciace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lehčí	lehký	k2eAgInPc1d2	lehčí
materiály	materiál	k1gInPc1	materiál
vyplavaly	vyplavat	k5eAaPmAgInP	vyplavat
do	do	k7c2	do
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
tvořeno	tvořit	k5eAaImNgNnS	tvořit
především	především	k6eAd1	především
železem	železo	k1gNnSc7	železo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
niklem	nikl	k1gInSc7	nikl
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
lehčími	lehký	k2eAgInPc7d2	lehčí
prvky	prvek	k1gInPc7	prvek
<g/>
;	;	kIx,	;
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
olovo	olovo	k1gNnSc1	olovo
nebo	nebo	k8xC	nebo
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
příliš	příliš	k6eAd1	příliš
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
významnými	významný	k2eAgFnPc7d1	významná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
se	se	k3xPyFc4	se
slučovat	slučovat	k5eAaImF	slučovat
s	s	k7c7	s
lehčími	lehký	k2eAgInPc7d2	lehčí
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
proto	proto	k8xC	proto
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
felsické	felsická	k1gFnSc2	felsická
horniny	hornina	k1gFnSc2	hornina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
:	:	kIx,	:
pevné	pevný	k2eAgNnSc1d1	pevné
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
~	~	kIx~	~
<g/>
1250	[number]	k4	1250
km	km	kA	km
a	a	k8xC	a
tekuté	tekutý	k2eAgNnSc1d1	tekuté
vnější	vnější	k2eAgNnSc1d1	vnější
jádro	jádro	k1gNnSc1	jádro
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
poloměru	poloměr	k1gInSc6	poloměr
~	~	kIx~	~
<g/>
3500	[number]	k4	3500
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
je	on	k3xPp3gNnPc4	on
Diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
Lehmanové	Lehmanová	k1gFnSc2	Lehmanová
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgFnPc4d1	pevná
a	a	k8xC	a
složené	složený	k2eAgFnPc4d1	složená
především	především	k9	především
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
z	z	k7c2	z
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
obhajují	obhajovat	k5eAaImIp3nP	obhajovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jediného	jediný	k2eAgInSc2d1	jediný
krystalu	krystal	k1gInSc2	krystal
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vnějším	vnější	k2eAgNnSc6d1	vnější
jádru	jádro	k1gNnSc6	jádro
obklopujícím	obklopující	k2eAgNnSc6d1	obklopující
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
tekutého	tekutý	k2eAgNnSc2d1	tekuté
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
stopového	stopový	k2eAgNnSc2d1	stopové
množství	množství	k1gNnSc2	množství
lehčích	lehký	k2eAgInPc2d2	lehčí
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
konvekce	konvekce	k1gFnSc1	konvekce
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
jádru	jádro	k1gNnSc6	jádro
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
s	s	k7c7	s
mícháním	míchání	k1gNnSc7	míchání
způsobeným	způsobený	k2eAgInSc7d1	způsobený
zemskou	zemský	k2eAgFnSc7d1	zemská
rotací	rotace	k1gFnSc7	rotace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zemské	zemský	k2eAgFnSc2d1	zemská
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
procesem	proces	k1gInSc7	proces
popsaným	popsaný	k2eAgInSc7d1	popsaný
teorií	teorie	k1gFnSc7	teorie
dynama	dynamo	k1gNnPc1	dynamo
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1	pevné
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
horké	horký	k2eAgNnSc1d1	horké
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
nositelem	nositel	k1gMnSc7	nositel
stálého	stálý	k2eAgNnSc2d1	stálé
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
však	však	k9	však
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
pole	pole	k1gFnSc2	pole
generovaného	generovaný	k2eAgInSc2d1	generovaný
tekutým	tekutý	k2eAgNnSc7d1	tekuté
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
tak	tak	k6eAd1	tak
připadá	připadat	k5eAaPmIp3nS	připadat
okolo	okolo	k7c2	okolo
31	[number]	k4	31
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
důkazy	důkaz	k1gInPc1	důkaz
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
Země	zem	k1gFnSc2	zem
nejspíš	nejspíš	k9	nejspíš
rotuje	rotovat	k5eAaImIp3nS	rotovat
poněkud	poněkud	k6eAd1	poněkud
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
planety	planeta	k1gFnSc2	planeta
o	o	k7c4	o
asi	asi	k9	asi
~	~	kIx~	~
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
°	°	k?	°
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zemský	zemský	k2eAgInSc4d1	zemský
plášť	plášť	k1gInSc4	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vrstev	vrstva	k1gFnPc2	vrstva
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
shora	shora	k6eAd1	shora
vymezená	vymezený	k2eAgFnSc1d1	vymezená
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
a	a	k8xC	a
zespodu	zespodu	k6eAd1	zespodu
zemským	zemský	k2eAgNnSc7d1	zemské
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
odděleným	oddělený	k2eAgInSc7d1	oddělený
Gutenbergovou	Gutenbergův	k2eAgFnSc7d1	Gutenbergova
diskontinuitou	diskontinuita	k1gFnSc7	diskontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geofyzikálního	geofyzikální	k2eAgNnSc2d1	geofyzikální
i	i	k8xC	i
geochemického	geochemický	k2eAgNnSc2d1	geochemické
hlediska	hledisko	k1gNnSc2	hledisko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zemský	zemský	k2eAgInSc1d1	zemský
plášť	plášť	k1gInSc1	plášť
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
svrchní	svrchní	k2eAgInSc4d1	svrchní
a	a	k8xC	a
spodní	spodní	k2eAgInSc4d1	spodní
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
současných	současný	k2eAgInPc2d1	současný
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
plášti	plášť	k1gInSc6	plášť
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podrobnou	podrobný	k2eAgFnSc7d1	podrobná
analýzou	analýza	k1gFnSc7	analýza
příchodů	příchod	k1gInPc2	příchod
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
probíhá	probíhat	k5eAaImIp3nS	probíhat
neustále	neustále	k6eAd1	neustále
plášťová	plášťový	k2eAgFnSc1d1	plášťová
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
deskovou	deskový	k2eAgFnSc7d1	desková
tektonikou	tektonika	k1gFnSc7	tektonika
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
obraz	obraz	k1gInSc4	obraz
můžeme	moct	k5eAaImIp1nP	moct
získat	získat	k5eAaPmF	získat
pomocí	pomocí	k7c2	pomocí
seismické	seismický	k2eAgFnSc2d1	seismická
tomografie	tomografie	k1gFnSc2	tomografie
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
plášť	plášť	k1gInSc1	plášť
jako	jako	k8xC	jako
celé	celý	k2eAgNnSc1d1	celé
těleso	těleso	k1gNnSc1	těleso
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
69	[number]	k4	69
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
84	[number]	k4	84
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
svrchní	svrchní	k2eAgFnSc1d1	svrchní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
křemičitanů	křemičitan	k1gInPc2	křemičitan
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
hořčíků	hořčík	k1gInPc2	hořčík
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
sulfidů	sulfid	k1gInPc2	sulfid
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
pláště	plášť	k1gInSc2	plášť
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
pozvolném	pozvolný	k2eAgInSc6d1	pozvolný
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
materiálu	materiál	k1gInSc2	materiál
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
rozpadu	rozpad	k1gInSc2	rozpad
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
draslík	draslík	k1gInSc4	draslík
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
70	[number]	k4	70
km	km	kA	km
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Nejtenčí	tenký	k2eAgFnSc7d3	nejtenčí
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánů	oceán	k1gInPc2	oceán
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
mafických	mafický	k2eAgFnPc2d1	mafický
hornin	hornina	k1gFnPc2	hornina
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
křemík	křemík	k1gInSc4	křemík
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
hořčík	hořčík	k1gInSc4	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgFnSc1d2	silnější
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
vrstvu	vrstva	k1gFnSc4	vrstva
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
felsických	felsický	k2eAgFnPc2d1	felsický
hornin	hornina	k1gFnPc2	hornina
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
křemík	křemík	k1gInSc4	křemík
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
a	a	k8xC	a
hliník	hliník	k1gInSc1	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rozhraní	rozhraní	k1gNnSc4	rozhraní
mezi	mezi	k7c7	mezi
kůrou	kůra	k1gFnSc7	kůra
a	a	k8xC	a
pláštěm	plášť	k1gInSc7	plášť
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
dva	dva	k4xCgInPc4	dva
fyzikálně	fyzikálně	k6eAd1	fyzikálně
odlišné	odlišný	k2eAgInPc4d1	odlišný
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
existuje	existovat	k5eAaImIp3nS	existovat
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Mohorovičićova	Mohorovičićův	k2eAgFnSc1d1	Mohorovičićova
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
této	tento	k3xDgFnSc2	tento
diskontinuity	diskontinuita	k1gFnSc2	diskontinuita
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
hornin	hornina	k1gFnPc2	hornina
od	od	k7c2	od
hornin	hornina	k1gFnPc2	hornina
obsahující	obsahující	k2eAgInPc1d1	obsahující
plagioklasy	plagioklas	k1gInPc1	plagioklas
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
po	po	k7c6	po
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žádné	žádný	k3yNgInPc1	žádný
živce	živec	k1gInPc1	živec
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
(	(	kIx(	(
<g/>
dole	dole	k6eAd1	dole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
mezi	mezi	k7c7	mezi
ultramafickými	ultramafický	k2eAgFnPc7d1	ultramafický
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
natavenými	natavený	k2eAgInPc7d1	natavený
harzburgity	harzburgit	k1gInPc7	harzburgit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
částech	část	k1gFnPc6	část
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
obdukovány	obdukovat	k5eAaBmNgFnP	obdukovat
do	do	k7c2	do
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
uchovány	uchován	k2eAgFnPc1d1	uchována
jako	jako	k8xS	jako
ofiolitické	ofiolitický	k2eAgFnPc1d1	ofiolitický
sekvence	sekvence	k1gFnPc1	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Povrch	povrch	k7c2wR	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
510	[number]	k4	510
065	[number]	k4	065
284,702	[number]	k4	284,702
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ale	ale	k8xC	ale
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
70,8	[number]	k4	70,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
Světovým	světový	k2eAgInSc7d1	světový
oceánem	oceán	k1gInSc7	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
361	[number]	k4	361
126	[number]	k4	126
221,569	[number]	k4	221,569
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
souš	souš	k1gFnSc1	souš
zabírá	zabírat	k5eAaImIp3nS	zabírat
29,2	[number]	k4	29,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
148	[number]	k4	148
939	[number]	k4	939
0	[number]	k4	0
<g/>
63,133	[number]	k4	63,133
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Oceány	oceán	k1gInPc1	oceán
a	a	k8xC	a
pevnina	pevnina	k1gFnSc1	pevnina
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
souše	souš	k1gFnSc2	souš
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Souš	souš	k1gFnSc1	souš
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
nepravidelně	pravidelně	k6eNd1	pravidelně
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
velkých	velký	k2eAgFnPc2d1	velká
oblastí	oblast	k1gFnPc2	oblast
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gFnPc7	on
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
kontinentů	kontinent	k1gInPc2	kontinent
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
stabilními	stabilní	k2eAgFnPc7d1	stabilní
platformami	platforma	k1gFnPc7	platforma
(	(	kIx(	(
<g/>
štíty	štít	k1gInPc1	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
staré	starý	k2eAgFnPc1d1	stará
několik	několik	k4yIc4	několik
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nestejnorodý	stejnorodý	k2eNgInSc1d1	nestejnorodý
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
výškovou	výškový	k2eAgFnSc7d1	výšková
rozdílností	rozdílnost	k1gFnSc7	rozdílnost
<g/>
.	.	kIx.	.
</s>
<s>
Oceánské	oceánský	k2eAgFnPc4d1	oceánská
oblasti	oblast	k1gFnPc4	oblast
tvořené	tvořený	k2eAgNnSc1d1	tvořené
oceánskou	oceánský	k2eAgFnSc7d1	oceánská
kůrou	kůra	k1gFnSc7	kůra
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
obrovské	obrovský	k2eAgFnPc4d1	obrovská
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nulové	nulový	k2eAgFnSc3d1	nulová
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
pod	pod	k7c4	pod
její	její	k3xOp3gFnSc4	její
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
hloubkového	hloubkový	k2eAgInSc2d1	hloubkový
extrému	extrém	k1gInSc2	extrém
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mariánského	mariánský	k2eAgInSc2d1	mariánský
příkopu	příkop	k1gInSc2	příkop
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnPc4	hodnota
−	−	k?	−
911	[number]	k4	911
m	m	kA	m
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
většinou	většinou	k6eAd1	většinou
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
nulovou	nulový	k2eAgFnSc7d1	nulová
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemské	suchozemský	k2eAgNnSc1d1	suchozemské
maximum	maximum	k1gNnSc1	maximum
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
Země	zem	k1gFnSc2	zem
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
8	[number]	k4	8
850	[number]	k4	850
m	m	kA	m
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
endogenních	endogenní	k2eAgMnPc2d1	endogenní
a	a	k8xC	a
exogenních	exogenní	k2eAgMnPc2d1	exogenní
pochodů	pochod	k1gInPc2	pochod
neustále	neustále	k6eAd1	neustále
přetvářen	přetvářen	k2eAgMnSc1d1	přetvářen
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
pochodů	pochod	k1gInPc2	pochod
Země	zem	k1gFnSc2	zem
vznikají	vznikat	k5eAaImIp3nP	vznikat
pásemná	pásemný	k2eAgNnPc1d1	pásemné
pohoří	pohoří	k1gNnPc1	pohoří
či	či	k8xC	či
tabule	tabule	k1gFnPc1	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnosti	činnost	k1gFnSc3	činnost
vynáší	vynášet	k5eAaImIp3nS	vynášet
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
Země	zem	k1gFnSc2	zem
nový	nový	k2eAgInSc4d1	nový
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ukládán	ukládán	k2eAgInSc1d1	ukládán
jak	jak	k8xC	jak
vertikálně	vertikálně	k6eAd1	vertikálně
tak	tak	k9	tak
i	i	k9	i
horizontálně	horizontálně	k6eAd1	horizontálně
<g/>
.	.	kIx.	.
</s>
<s>
Horstva	horstvo	k1gNnPc1	horstvo
jsou	být	k5eAaImIp3nP	být
vlivem	vlivem	k7c2	vlivem
erozivních	erozivní	k2eAgInPc2d1	erozivní
činitelů	činitel	k1gInPc2	činitel
opět	opět	k6eAd1	opět
zahlazovány	zahlazovat	k5eAaImNgInP	zahlazovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
sedimentů	sediment	k1gInPc2	sediment
a	a	k8xC	a
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
rovinatých	rovinatý	k2eAgFnPc2d1	rovinatá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Vývoj	vývoj	k1gInSc1	vývoj
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
Geologický	geologický	k2eAgInSc4d1	geologický
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Rozvržení	rozvržení	k1gNnSc1	rozvržení
souše	souš	k1gFnSc2	souš
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
jaké	jaký	k3yIgNnSc1	jaký
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
historie	historie	k1gFnSc2	historie
Země	zem	k1gFnSc2	zem
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
pohybu	pohyb	k1gInSc2	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
značně	značně	k6eAd1	značně
měnilo	měnit	k5eAaImAgNnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Měnily	měnit	k5eAaImAgInP	měnit
se	se	k3xPyFc4	se
jak	jak	k8xC	jak
velikosti	velikost	k1gFnSc3	velikost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
rozložení	rozložení	k1gNnSc1	rozložení
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgNnP	vznikat
nová	nový	k2eAgNnPc1d1	nové
moře	moře	k1gNnPc1	moře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přecházela	přecházet	k5eAaImAgFnS	přecházet
v	v	k7c4	v
oceány	oceán	k1gInPc4	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
zase	zase	k9	zase
zanikaly	zanikat	k5eAaImAgInP	zanikat
a	a	k8xC	a
zmenšovaly	zmenšovat	k5eAaImAgInP	zmenšovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
také	také	k9	také
ke	k	k7c3	k
vzájemným	vzájemný	k2eAgFnPc3d1	vzájemná
kolizím	kolize	k1gFnPc3	kolize
<g/>
,	,	kIx,	,
ponořováním	ponořování	k1gNnSc7	ponořování
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
pohybům	pohyb	k1gInPc3	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zcela	zcela	k6eAd1	zcela
měnily	měnit	k5eAaImAgFnP	měnit
tvář	tvář	k1gFnSc4	tvář
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zpětně	zpětně	k6eAd1	zpětně
odvozovat	odvozovat	k5eAaImF	odvozovat
podobu	podoba	k1gFnSc4	podoba
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
pohyby	pohyb	k1gInPc1	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mnoha	mnoho	k4c2	mnoho
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc1	tvář
Země	zem	k1gFnSc2	zem
měnila	měnit	k5eAaImAgFnS	měnit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vědecká	vědecký	k2eAgFnSc1d1	vědecká
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
shodnout	shodnout	k5eAaBmF	shodnout
na	na	k7c6	na
pohybech	pohyb	k1gInPc6	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
starších	starý	k2eAgFnPc2d2	starší
než	než	k8xS	než
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
doklady	doklad	k1gInPc1	doklad
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
1,3	[number]	k4	1,3
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
začal	začít	k5eAaPmAgInS	začít
formovat	formovat	k5eAaImF	formovat
srážkou	srážka	k1gFnSc7	srážka
tří	tři	k4xCgInPc2	tři
až	až	k8xS	až
čtyř	čtyři	k4xCgInPc2	čtyři
kontinentů	kontinent	k1gInPc2	kontinent
superkontinent	superkontinent	k1gInSc4	superkontinent
Rodinie	Rodinie	k1gFnSc2	Rodinie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc4	vznik
pohoří	pohoří	k1gNnSc2	pohoří
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
superkontinent	superkontinent	k1gInSc1	superkontinent
existoval	existovat	k5eAaImAgInS	existovat
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
750	[number]	k4	750
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Rodinie	Rodinie	k1gFnSc1	Rodinie
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaPmF	rozpadat
na	na	k7c4	na
8	[number]	k4	8
menších	malý	k2eAgInPc2d2	menší
kontinentů	kontinent	k1gInPc2	kontinent
s	s	k7c7	s
jádrovou	jádrový	k2eAgFnSc7d1	jádrová
oblastí	oblast	k1gFnSc7	oblast
Laurentie	Laurentie	k1gFnSc2	Laurentie
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
přibližně	přibližně	k6eAd1	přibližně
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
budoucí	budoucí	k2eAgFnSc1d1	budoucí
východní	východní	k2eAgFnSc1d1	východní
Gondwana	Gondwana	k1gFnSc1	Gondwana
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
Baltika	Baltika	k1gFnSc1	Baltika
a	a	k8xC	a
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
dnešního	dnešní	k2eAgInSc2d1	dnešní
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
západní	západní	k2eAgFnSc1d1	západní
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
.	.	kIx.	.
</s>
<s>
Kontinenty	kontinent	k1gInPc4	kontinent
Západní	západní	k2eAgFnSc1d1	západní
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
,	,	kIx,	,
Laurentie	Laurentie	k1gFnSc1	Laurentie
<g/>
,	,	kIx,	,
Baltika	Baltika	k1gFnSc1	Baltika
a	a	k8xC	a
Sibiř	Sibiř	k1gFnSc1	Sibiř
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
Protolaurasii	Protolaurasie	k1gFnSc4	Protolaurasie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
protiváhou	protiváha	k1gFnSc7	protiváha
byla	být	k5eAaImAgFnS	být
Protogondwana	Protogondwana	k1gFnSc1	Protogondwana
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgFnSc1d1	budoucí
východní	východní	k2eAgFnSc1d1	východní
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ležela	ležet	k5eAaImAgFnS	ležet
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
310	[number]	k4	310
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nového	nový	k2eAgInSc2d1	nový
základu	základ	k1gInSc2	základ
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
superkontinent	superkontinent	k1gInSc4	superkontinent
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Pangea	Pangea	k1gFnSc1	Pangea
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vzájemnými	vzájemný	k2eAgFnPc7d1	vzájemná
kolizemi	kolize	k1gFnPc7	kolize
zvětšoval	zvětšovat	k5eAaImAgMnS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Pangei	Pangea	k1gFnSc3	Pangea
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
oceán	oceán	k1gInSc4	oceán
Panthalassa	Panthalass	k1gMnSc2	Panthalass
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
200	[number]	k4	200
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
jury	jura	k1gFnSc2	jura
se	se	k3xPyFc4	se
Pangea	Pangea	k1gFnSc1	Pangea
začíná	začínat	k5eAaImIp3nS	začínat
rozpadat	rozpadat	k5eAaPmF	rozpadat
na	na	k7c4	na
Laurasii	Laurasie	k1gFnSc4	Laurasie
a	a	k8xC	a
Gondwanu	Gondwana	k1gFnSc4	Gondwana
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
kontinenty	kontinent	k1gInPc4	kontinent
oddělil	oddělit	k5eAaPmAgInS	oddělit
oceán	oceán	k1gInSc1	oceán
Tethys	Tethys	k1gInSc1	Tethys
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
150	[number]	k4	150
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
rozpadat	rozpadat	k5eAaPmF	rozpadat
Laurasie	Laurasie	k1gFnPc4	Laurasie
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
Eurasii	Eurasie	k1gFnSc4	Eurasie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
vzniká	vznikat	k5eAaImIp3nS	vznikat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
zvětšovat	zvětšovat	k5eAaImF	zvětšovat
(	(	kIx(	(
<g/>
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozpad	rozpad	k1gInSc1	rozpad
Gondwany	Gondwana	k1gFnSc2	Gondwana
nastává	nastávat	k5eAaImIp3nS	nastávat
před	před	k7c7	před
140	[number]	k4	140
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c6	na
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc6d1	budoucí
oblasti	oblast	k1gFnSc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
Antarktidu	Antarktida	k1gFnSc4	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
100	[number]	k4	100
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
vzniká	vznikat	k5eAaImIp3nS	vznikat
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
současný	současný	k2eAgInSc4d1	současný
vzhled	vzhled	k1gInSc4	vzhled
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
desky	deska	k1gFnPc4	deska
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
tvář	tvář	k1gFnSc1	tvář
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
značně	značně	k6eAd1	značně
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
riftové	riftová	k1gFnPc1	riftová
údolí	údolí	k1gNnSc2	údolí
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nS	oddělit
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
novým	nový	k2eAgInSc7d1	nový
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
Afrika	Afrika	k1gFnSc1	Afrika
bude	být	k5eAaImBp3nS	být
posouvat	posouvat	k5eAaImF	posouvat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
spojí	spojit	k5eAaPmIp3nP	spojit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
Středozemní	středozemní	k2eAgNnSc1d1	středozemní
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
nárazem	náraz	k1gInSc7	náraz
kontinentů	kontinent	k1gInPc2	kontinent
zvětší	zvětšit	k5eAaPmIp3nP	zvětšit
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
případ	případ	k1gInSc1	případ
jako	jako	k8xS	jako
u	u	k7c2	u
Indie	Indie	k1gFnSc2	Indie
s	s	k7c7	s
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
souřadnice	souřadnice	k1gFnSc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
formována	formován	k2eAgFnSc1d1	formována
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
kouli	koule	k1gFnSc3	koule
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
určení	určení	k1gNnSc4	určení
pozice	pozice	k1gFnSc2	pozice
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
kouli	koule	k1gFnSc6	koule
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zavedly	zavést	k5eAaPmAgFnP	zavést
zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
souřadnice	souřadnice	k1gFnPc1	souřadnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přesně	přesně	k6eAd1	přesně
definují	definovat	k5eAaBmIp3nP	definovat
polohu	poloha	k1gFnSc4	poloha
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Používané	používaný	k2eAgFnPc1d1	používaná
souřadnice	souřadnice	k1gFnPc1	souřadnice
jsou	být	k5eAaImIp3nP	být
souřadnice	souřadnice	k1gFnPc4	souřadnice
geocentrické	geocentrický	k2eAgFnPc4d1	geocentrická
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jejich	jejich	k3xOp3gInSc1	jejich
střed	střed	k1gInSc1	střed
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
těleso	těleso	k1gNnSc1	těleso
protíná	protínat	k5eAaImIp3nS	protínat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
Kolmo	kolmo	k6eAd1	kolmo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
proloží	proložit	k5eAaPmIp3nS	proložit
rovina	rovina	k1gFnSc1	rovina
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rovina	rovina	k1gFnSc1	rovina
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
zemský	zemský	k2eAgInSc1d1	zemský
rovník	rovník	k1gInSc1	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
rovník	rovník	k1gInSc4	rovník
s	s	k7c7	s
počátky	počátek	k1gInPc7	počátek
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
pólech	pól	k1gInPc6	pól
procházejí	procházet	k5eAaImIp3nP	procházet
poledníky	poledník	k1gInPc1	poledník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tak	tak	k6eAd1	tak
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
stejné	stejná	k1gFnSc2	stejná
jako	jako	k8xC	jako
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
bodem	bod	k1gInSc7	bod
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
prochází	procházet	k5eAaImIp3nS	procházet
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
poledník	poledník	k1gInSc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
poledníku	poledník	k1gInSc2	poledník
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
námořní	námořní	k2eAgFnSc1d1	námořní
míle	míle	k1gFnSc1	míle
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
jedna	jeden	k4xCgFnSc1	jeden
úhlová	úhlový	k2eAgFnSc1d1	úhlová
minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
360	[number]	k4	360
stupňů	stupeň	k1gInPc2	stupeň
360	[number]	k4	360
<g/>
×	×	k?	×
<g/>
60	[number]	k4	60
<g/>
=	=	kIx~	=
<g/>
21600	[number]	k4	21600
úhlových	úhlový	k2eAgFnPc2d1	úhlová
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
21600	[number]	k4	21600
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
nebo	nebo	k8xC	nebo
21600	[number]	k4	21600
<g/>
×	×	k?	×
<g/>
1,852	[number]	k4	1,852
<g/>
=	=	kIx~	=
<g/>
40.003,2	[number]	k4	40.003,2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
obvod	obvod	k1gInSc1	obvod
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
360	[number]	k4	360
úhlových	úhlový	k2eAgInPc2d1	úhlový
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
oběh	oběh	k1gInSc4	oběh
země	zem	k1gFnSc2	zem
1440	[number]	k4	1440
<g/>
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
15	[number]	k4	15
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
za	za	k7c4	za
1	[number]	k4	1
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
časového	časový	k2eAgNnSc2d1	časové
pásma	pásmo	k1gNnSc2	pásmo
je	být	k5eAaImIp3nS	být
360	[number]	k4	360
<g/>
\	\	kIx~	\
<g/>
24	[number]	k4	24
<g/>
=	=	kIx~	=
<g/>
15	[number]	k4	15
úhlových	úhlový	k2eAgInPc2d1	úhlový
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
početní	početní	k2eAgFnPc4d1	početní
operace	operace	k1gFnPc4	operace
zavedl	zavést	k5eAaPmAgMnS	zavést
sir	sir	k1gMnSc1	sir
George	Georg	k1gMnSc2	Georg
Airy	Aira	k1gFnSc2	Aira
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
nultý	nultý	k4xOgMnSc1	nultý
poledník	poledník	k1gMnSc1	poledník
procházející	procházející	k2eAgInSc1d1	procházející
anglickým	anglický	k2eAgInSc7d1	anglický
Greenwichem	Greenwich	k1gInSc7	Greenwich
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rychle	rychle	k6eAd1	rychle
používat	používat	k5eAaImF	používat
v	v	k7c6	v
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnPc4d1	námořní
mapy	mapa	k1gFnPc4	mapa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dominantním	dominantní	k2eAgInSc7d1	dominantní
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
přijal	přijmout	k5eAaPmAgMnS	přijmout
celosvětově	celosvětově	k6eAd1	celosvětově
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
mapy	mapa	k1gFnPc4	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
poledník	poledník	k1gInSc1	poledník
procházející	procházející	k2eAgInSc1d1	procházející
daným	daný	k2eAgInSc7d1	daný
bodem	bod	k1gInSc7	bod
určuje	určovat	k5eAaImIp3nS	určovat
přesně	přesně	k6eAd1	přesně
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc4d1	jižní
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
šířku	šířka	k1gFnSc4	šířka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
úhel	úhel	k1gInSc4	úhel
mezi	mezi	k7c7	mezi
rovinou	rovina	k1gFnSc7	rovina
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
spojnicí	spojnice	k1gFnSc7	spojnice
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
místním	místní	k2eAgInSc7d1	místní
poledníkem	poledník	k1gInSc7	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
pozice	pozice	k1gFnSc2	pozice
daného	daný	k2eAgInSc2d1	daný
bodu	bod	k1gInSc2	bod
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
ale	ale	k9	ale
znát	znát	k5eAaImF	znát
i	i	k9	i
přesnou	přesný	k2eAgFnSc4d1	přesná
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
dle	dle	k7c2	dle
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
úhlem	úhel	k1gInSc7	úhel
mezi	mezi	k7c7	mezi
rovinou	rovina	k1gFnSc7	rovina
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
místního	místní	k2eAgInSc2d1	místní
poledníku	poledník	k1gInSc2	poledník
daného	daný	k2eAgInSc2d1	daný
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
hodnot	hodnota	k1gFnPc2	hodnota
0	[number]	k4	0
<g/>
°	°	k?	°
až	až	k9	až
+180	+180	k4	+180
<g/>
°	°	k?	°
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
greenwichského	greenwichský	k2eAgInSc2d1	greenwichský
poledníku	poledník	k1gInSc2	poledník
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
0	[number]	k4	0
<g/>
°	°	k?	°
až	až	k9	až
-180	-180	k4	-180
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kartografie	kartografie	k1gFnSc2	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
zakresluje	zakreslovat	k5eAaImIp3nS	zakreslovat
do	do	k7c2	do
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
zmenšeným	zmenšený	k2eAgInSc7d1	zmenšený
rovinným	rovinný	k2eAgInSc7d1	rovinný
obrazem	obraz	k1gInSc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
výrobou	výroba	k1gFnSc7	výroba
map	mapa	k1gFnPc2	mapa
je	být	k5eAaImIp3nS	být
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vznik	vznik	k1gInSc1	vznik
map	mapa	k1gFnPc2	mapa
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
vzdělaností	vzdělanost	k1gFnSc7	vzdělanost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
chápání	chápání	k1gNnSc3	chápání
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zakreslení	zakreslení	k1gNnSc6	zakreslení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
vývojem	vývoj	k1gInSc7	vývoj
představ	představa	k1gFnPc2	představa
člověka	člověk	k1gMnSc2	člověk
o	o	k7c4	o
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
i	i	k9	i
mapy	mapa	k1gFnPc1	mapa
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
preferovaném	preferovaný	k2eAgInSc6d1	preferovaný
tvaru	tvar	k1gInSc6	tvar
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
již	již	k6eAd1	již
definitivně	definitivně	k6eAd1	definitivně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
kulatá	kulatý	k2eAgFnSc1d1	kulatá
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
mapy	mapa	k1gFnSc2	mapa
tomuto	tento	k3xDgInSc3	tento
faktu	fakt	k1gInSc3	fakt
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivenou	zakřivený	k2eAgFnSc4d1	zakřivená
plochu	plocha	k1gFnSc4	plocha
skutečného	skutečný	k2eAgInSc2d1	skutečný
povrchu	povrch	k1gInSc2	povrch
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
rozvinout	rozvinout	k5eAaPmF	rozvinout
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
najít	najít	k5eAaPmF	najít
správný	správný	k2eAgInSc4d1	správný
způsob	způsob	k1gInSc4	způsob
zakreslení	zakreslení	k1gNnSc2	zakreslení
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
referenční	referenční	k2eAgFnPc1d1	referenční
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
pro	pro	k7c4	pro
kartografické	kartografický	k2eAgNnSc4d1	kartografické
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
dle	dle	k7c2	dle
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
polohy	poloha	k1gFnSc2	poloha
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
dle	dle	k7c2	dle
vlastnosti	vlastnost	k1gFnSc2	vlastnost
zakreslení	zakreslení	k1gNnSc2	zakreslení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
planetou	planeta	k1gFnSc7	planeta
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
71	[number]	k4	71
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
97	[number]	k4	97
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kontinentech	kontinent	k1gInPc6	kontinent
pak	pak	k6eAd1	pak
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
<g/>
,	,	kIx,	,
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
a	a	k8xC	a
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
bohatá	bohatý	k2eAgFnSc1d1	bohatá
atmosféra	atmosféra	k1gFnSc1	atmosféra
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgFnPc4d1	jedinečná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dohromady	dohromady	k6eAd1	dohromady
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vodní	vodní	k2eAgFnSc4d1	vodní
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
formování	formování	k1gNnSc2	formování
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
voda	voda	k1gFnSc1	voda
zřejmě	zřejmě	k6eAd1	zřejmě
nenacházela	nacházet	k5eNaImAgFnS	nacházet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
vypařit	vypařit	k5eAaPmF	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vodu	voda	k1gFnSc4	voda
přinesly	přinést	k5eAaPmAgFnP	přinést
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
později	pozdě	k6eAd2	pozdě
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
formovaly	formovat	k5eAaImAgFnP	formovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
nacházela	nacházet	k5eAaImAgFnS	nacházet
jen	jen	k9	jen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
přinášejí	přinášet	k5eAaImIp3nP	přinášet
vodu	voda	k1gFnSc4	voda
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
sem	sem	k6eAd1	sem
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
prvního	první	k4xOgNnSc2	první
velkého	velký	k2eAgNnSc2d1	velké
bombardování	bombardování	k1gNnSc2	bombardování
10	[number]	k4	10
až	až	k9	až
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
zaručujících	zaručující	k2eAgInPc2d1	zaručující
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
teplo	teplo	k1gNnSc4	teplo
pro	pro	k7c4	pro
kapalnou	kapalný	k2eAgFnSc4d1	kapalná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
voda	voda	k1gFnSc1	voda
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologické	paleontologický	k2eAgInPc1d1	paleontologický
nálezy	nález	k1gInPc1	nález
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
modrozelené	modrozelený	k2eAgFnPc1d1	modrozelená
sinice	sinice	k1gFnPc1	sinice
(	(	kIx(	(
<g/>
Cyanobacteria	Cyanobacterium	k1gNnPc1	Cyanobacterium
<g/>
)	)	kIx)	)
kolonizovaly	kolonizovat	k5eAaBmAgInP	kolonizovat
oceány	oceán	k1gInPc1	oceán
a	a	k8xC	a
vyčerpaly	vyčerpat	k5eAaPmAgInP	vyčerpat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
selhal	selhat	k5eAaPmAgInS	selhat
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
a	a	k8xC	a
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
zemské	zemský	k2eAgInPc1d1	zemský
oceány	oceán	k1gInPc1	oceán
nejspíš	nejspíš	k9	nejspíš
zcela	zcela	k6eAd1	zcela
zamrzly	zamrznout	k5eAaPmAgFnP	zamrznout
na	na	k7c4	na
10	[number]	k4	10
až	až	k9	až
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
planetách	planeta	k1gFnPc6	planeta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
molekuly	molekula	k1gFnPc1	molekula
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
slunečním	sluneční	k2eAgNnPc3d1	sluneční
ultrafialovým	ultrafialový	k2eAgNnPc3d1	ultrafialové
zářením	záření	k1gNnPc3	záření
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
byl	být	k5eAaImAgInS	být
ionizován	ionizovat	k5eAaBmNgInS	ionizovat
a	a	k8xC	a
odvanut	odvanout	k5eAaPmNgInS	odvanout
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúprosný	úprosný	k2eNgInSc1d1	neúprosný
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hypotéz	hypotéza	k1gFnPc2	hypotéza
vysvětlujících	vysvětlující	k2eAgFnPc2d1	vysvětlující
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nemá	mít	k5eNaImIp3nS	mít
Venuše	Venuše	k1gFnSc1	Venuše
žádnou	žádný	k3yNgFnSc4	žádný
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
vodíku	vodík	k1gInSc2	vodík
kyslík	kyslík	k1gInSc1	kyslík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
v	v	k7c6	v
pevných	pevný	k2eAgInPc6d1	pevný
minerálech	minerál	k1gInPc6	minerál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
ozónu	ozón	k1gInSc2	ozón
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
většinu	většina	k1gFnSc4	většina
vysokoenergetického	vysokoenergetický	k2eAgNnSc2d1	vysokoenergetické
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
efekt	efekt	k1gInSc1	efekt
rozbíjení	rozbíjení	k1gNnSc2	rozbíjení
molekul	molekula	k1gFnPc2	molekula
tak	tak	k6eAd1	tak
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Ozón	ozón	k1gInSc1	ozón
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
volného	volný	k2eAgInSc2d1	volný
dvouatomového	dvouatomový	k2eAgInSc2d1	dvouatomový
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
biosféře	biosféra	k1gFnSc6	biosféra
(	(	kIx(	(
<g/>
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
také	také	k9	také
chrání	chránit	k5eAaImIp3nS	chránit
ionosféru	ionosféra	k1gFnSc4	ionosféra
před	před	k7c7	před
přímým	přímý	k2eAgNnSc7d1	přímé
odfukováním	odfukování	k1gNnSc7	odfukování
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
neustále	neustále	k6eAd1	neustále
dostává	dostávat	k5eAaImIp3nS	dostávat
voda	voda	k1gFnSc1	voda
zevnitř	zevnitř	k6eAd1	zevnitř
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
recyklace	recyklace	k1gFnSc2	recyklace
subdukuje	subdukovat	k5eAaImIp3nS	subdukovat
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vápencových	vápencový	k2eAgFnPc2d1	vápencová
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
činnosti	činnost	k1gFnSc6	činnost
jako	jako	k9	jako
plynný	plynný	k2eAgInSc4d1	plynný
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
horniny	hornina	k1gFnPc1	hornina
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
10	[number]	k4	10
<g/>
×	×	k?	×
více	hodně	k6eAd2	hodně
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
zadržované	zadržovaný	k2eAgFnSc2d1	zadržovaná
vody	voda	k1gFnSc2	voda
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,4	[number]	k4	1,4
<g/>
×	×	k?	×
<g/>
1021	[number]	k4	1021
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
0,023	[number]	k4	0,023
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
souvislý	souvislý	k2eAgInSc4d1	souvislý
vodní	vodní	k2eAgInSc4d1	vodní
obal	obal	k1gInSc4	obal
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
zálivů	záliv	k1gInPc2	záliv
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
vodní	vodní	k2eAgFnSc2d1	vodní
masy	masa	k1gFnSc2	masa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
většina	většina	k1gFnSc1	většina
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
souvislou	souvislý	k2eAgFnSc4d1	souvislá
vodní	vodní	k2eAgFnSc4d1	vodní
plochu	plocha	k1gFnSc4	plocha
se	s	k7c7	s
společnou	společný	k2eAgFnSc7d1	společná
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
kolem	kolem	k7c2	kolem
střední	střední	k2eAgFnSc2d1	střední
hodnoty	hodnota	k1gFnSc2	hodnota
vlivem	vlivem	k7c2	vlivem
vnějších	vnější	k2eAgInPc2d1	vnější
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kvůli	kvůli	k7c3	kvůli
gravitačním	gravitační	k2eAgMnPc3d1	gravitační
vlivům	vliv	k1gInPc3	vliv
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
napájena	napájet	k5eAaImNgFnS	napájet
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
<g/>
,	,	kIx,	,
srážkovou	srážkový	k2eAgFnSc7d1	srážková
popř.	popř.	kA	popř.
podzemní	podzemní	k2eAgFnSc7d1	podzemní
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jezera	jezero	k1gNnPc4	jezero
1,8	[number]	k4	1,8
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
velká	velký	k2eAgNnPc1d1	velké
bezodtoká	bezodtoký	k2eAgNnPc1d1	bezodtoké
jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
vnitrozemskými	vnitrozemský	k2eAgNnPc7d1	vnitrozemské
moři	moře	k1gNnPc7	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
slanou	slaný	k2eAgFnSc4d1	slaná
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Saltonské	Saltonský	k2eAgNnSc1d1	Saltonské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumáním	zkoumání	k1gNnSc7	zkoumání
jezer	jezero	k1gNnPc2	jezero
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
věda	věda	k1gFnSc1	věda
zvaná	zvaný	k2eAgFnSc1d1	zvaná
limnologie	limnologie	k1gFnSc1	limnologie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řeka	Řek	k1gMnSc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgInSc4d2	veliký
objemový	objemový	k2eAgInSc4d1	objemový
průtok	průtok	k1gInSc4	průtok
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
nebo	nebo	k8xC	nebo
rozlohu	rozloha	k1gFnSc4	rozloha
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
"	"	kIx"	"
<g/>
V	V	kA	V
<g/>
"	"	kIx"	"
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
usazenin	usazenina	k1gFnPc2	usazenina
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
eroze	eroze	k1gFnSc1	eroze
i	i	k8xC	i
sedimentace	sedimentace	k1gFnSc1	sedimentace
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnPc4d1	říční
údolí	údolí	k1gNnPc4	údolí
je	být	k5eAaImIp3nS	být
plošší	plochý	k2eAgNnSc1d2	plošší
a	a	k8xC	a
s	s	k7c7	s
již	již	k6eAd1	již
významným	významný	k2eAgInSc7d1	významný
podílem	podíl	k1gInSc7	podíl
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
toku	tok	k1gInSc2	tok
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
písmena	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
sedimentace	sedimentace	k1gFnSc2	sedimentace
–	–	k?	–
údolí	údolí	k1gNnSc6	údolí
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
ploché	plochý	k2eAgNnSc1d1	ploché
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
masivní	masivní	k2eAgFnSc3d1	masivní
sedimentaci	sedimentace	k1gFnSc3	sedimentace
vznikají	vznikat	k5eAaImIp3nP	vznikat
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
říční	říční	k2eAgFnSc2d1	říční
nivy	niva	k1gFnSc2	niva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
78	[number]	k4	78
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
21	[number]	k4	21
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
0,93	[number]	k4	0,93
%	%	kIx~	%
argonu	argon	k1gInSc2	argon
<g/>
,	,	kIx,	,
0,038	[number]	k4	0,038
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
stopové	stopový	k2eAgNnSc4d1	stopové
množství	množství	k1gNnSc4	množství
jiných	jiný	k2eAgInPc2d1	jiný
plynů	plyn	k1gInPc2	plyn
včetně	včetně	k7c2	včetně
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
chrání	chránit	k5eAaImIp3nS	chránit
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
silně	silně	k6eAd1	silně
ovlivněno	ovlivněn	k2eAgNnSc4d1	ovlivněno
biosférou	biosféra	k1gFnSc7	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
volného	volný	k2eAgInSc2d1	volný
dvouatomového	dvouatomový	k2eAgInSc2d1	dvouatomový
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pozemské	pozemský	k2eAgFnPc1d1	pozemská
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
bez	bez	k7c2	bez
nichž	jenž	k3xRgFnPc2	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
geologicky	geologicky	k6eAd1	geologicky
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
sloučil	sloučit	k5eAaPmAgMnS	sloučit
s	s	k7c7	s
materiály	materiál	k1gInPc7	materiál
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
známkou	známka	k1gFnSc7	známka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
druhotnou	druhotný	k2eAgFnSc7d1	druhotná
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pozměnily	pozměnit	k5eAaPmAgInP	pozměnit
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
atmosféra	atmosféra	k1gFnSc1	atmosféra
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
při	při	k7c6	při
zformování	zformování	k1gNnSc6	zformování
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
toxickou	toxický	k2eAgFnSc4d1	toxická
směs	směs	k1gFnSc4	směs
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
uvolňovaly	uvolňovat	k5eAaImAgInP	uvolňovat
při	při	k7c6	při
odplynování	odplynování	k1gNnSc6	odplynování
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
,	,	kIx,	,
mezosféry	mezosféra	k1gFnSc2	mezosféra
<g/>
,	,	kIx,	,
termosféry	termosféra	k1gFnSc2	termosféra
a	a	k8xC	a
exosféry	exosféra	k1gFnSc2	exosféra
<g/>
)	)	kIx)	)
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
planety	planeta	k1gFnSc2	planeta
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sezónních	sezónní	k2eAgInPc6d1	sezónní
vlivech	vliv	k1gInPc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Obloha	obloha	k1gFnSc1	obloha
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
molekuly	molekula	k1gFnPc1	molekula
vzduchu	vzduch	k1gInSc2	vzduch
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
proti	proti	k7c3	proti
očím	oko	k1gNnPc3	oko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
nejvíce	nejvíce	k6eAd1	nejvíce
právě	právě	k6eAd1	právě
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5,1	[number]	k4	5,1
<g/>
×	×	k?	×
<g/>
1018	[number]	k4	1018
kg	kg	kA	kg
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přibližně	přibližně	k6eAd1	přibližně
0,000	[number]	k4	0,000
000	[number]	k4	000
9	[number]	k4	9
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejší	teplý	k2eAgFnSc1d3	nejteplejší
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc6	rovník
<g/>
,	,	kIx,	,
nejstudenější	studený	k2eAgInSc1d3	nejstudenější
pak	pak	k6eAd1	pak
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
klima	klima	k1gNnSc1	klima
mnohokrát	mnohokrát	k6eAd1	mnohokrát
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přežití	přežití	k1gNnSc4	přežití
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtohorách	čtvrtohory	k1gFnPc6	čtvrtohory
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opakujícím	opakující	k2eAgInPc3d1	opakující
se	se	k3xPyFc4	se
dobám	doba	k1gFnPc3	doba
ledovým	ledový	k2eAgNnPc3d1	ledové
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
střídají	střídat	k5eAaImIp3nP	střídat
teplejší	teplý	k2eAgNnSc4d2	teplejší
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
skončila	skončit	k5eAaPmAgFnS	skončit
před	před	k7c7	před
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
stav	stav	k1gInSc4	stav
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
stavem	stav	k1gInSc7	stav
všech	všecek	k3xTgInPc2	všecek
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
jevů	jev	k1gInPc2	jev
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
nebo	nebo	k8xC	nebo
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
souborem	soubor	k1gInSc7	soubor
hodnot	hodnota	k1gFnPc2	hodnota
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
naměřeny	naměřen	k2eAgInPc1d1	naměřen
meteorologickými	meteorologický	k2eAgInPc7d1	meteorologický
přístroji	přístroj	k1gInPc7	přístroj
nebo	nebo	k8xC	nebo
zjištěny	zjistit	k5eAaPmNgInP	zjistit
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
směr	směr	k1gInSc4	směr
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
sněžení	sněžení	k1gNnSc1	sněžení
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Změny	změna	k1gFnPc1	změna
počasí	počasí	k1gNnSc2	počasí
jsou	být	k5eAaImIp3nP	být
způsobeny	způsoben	k2eAgFnPc1d1	způsobena
především	především	k9	především
zemskou	zemský	k2eAgFnSc7d1	zemská
rotací	rotace	k1gFnSc7	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Ohromné	ohromný	k2eAgFnPc1d1	ohromná
masy	masa	k1gFnPc1	masa
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
vlivem	vlivem	k7c2	vlivem
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
tendenci	tendence	k1gFnSc4	tendence
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
masy	masa	k1gFnPc1	masa
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oblačnost	oblačnost	k1gFnSc4	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
udává	udávat	k5eAaImIp3nS	udávat
stupeň	stupeň	k1gInSc4	stupeň
pokrytí	pokrytí	k1gNnSc4	pokrytí
oblohy	obloha	k1gFnSc2	obloha
oblaky	oblak	k1gInPc4	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
meteorologickým	meteorologický	k2eAgInSc7d1	meteorologický
a	a	k8xC	a
klimatologickým	klimatologický	k2eAgInSc7d1	klimatologický
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klimatologii	klimatologie	k1gFnSc6	klimatologie
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
desetinách	desetina	k1gFnPc6	desetina
pokrytí	pokrytí	k1gNnSc1	pokrytí
oblohy	obloha	k1gFnSc2	obloha
–	–	k?	–
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
do	do	k7c2	do
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
synoptické	synoptický	k2eAgFnSc6d1	synoptická
meteorologii	meteorologie	k1gFnSc6	meteorologie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
osmin	osmina	k1gFnPc2	osmina
–	–	k?	–
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
až	až	k9	až
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
pokrytí	pokrytí	k1gNnSc2	pokrytí
oblohy	obloha	k1gFnSc2	obloha
oblaky	oblak	k1gInPc4	oblak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
znamená	znamenat	k5eAaImIp3nS	znamenat
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
nebo	nebo	k8xC	nebo
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
jasnou	jasný	k2eAgFnSc4d1	jasná
bezoblačnou	bezoblačný	k2eAgFnSc4d1	bezoblačná
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
nebo	nebo	k8xC	nebo
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
znamená	znamenat	k5eAaImIp3nS	znamenat
zcela	zcela	k6eAd1	zcela
zataženou	zatažený	k2eAgFnSc4d1	zatažená
oblohu	obloha	k1gFnSc4	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
biosféra	biosféra	k1gFnSc1	biosféra
a	a	k8xC	a
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
někdy	někdy	k6eAd1	někdy
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
"	"	kIx"	"
<g/>
biosféru	biosféra	k1gFnSc4	biosféra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c7	před
3,7	[number]	k4	3,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
ve	v	k7c6	v
známém	známý	k2eAgInSc6d1	známý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nepochybná	pochybný	k2eNgFnSc1d1	nepochybná
existence	existence	k1gFnSc1	existence
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc4	život
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
spíše	spíše	k9	spíše
řídkým	řídký	k2eAgInSc7d1	řídký
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
biosféra	biosféra	k1gFnSc1	biosféra
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
množství	množství	k1gNnSc2	množství
biomů	biom	k1gMnPc2	biom
<g/>
,	,	kIx,	,
osídlených	osídlený	k2eAgMnPc2d1	osídlený
vždy	vždy	k6eAd1	vždy
zhruba	zhruba	k6eAd1	zhruba
typickými	typický	k2eAgInPc7d1	typický
organizmy	organizmus	k1gInPc7	organizmus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
flórou	flóra	k1gFnSc7	flóra
a	a	k8xC	a
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
biomy	bioma	k1gFnPc4	bioma
především	především	k6eAd1	především
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
a	a	k8xC	a
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgInPc1d1	zemský
biomy	biom	k1gInPc1	biom
ležící	ležící	k2eAgInPc1d1	ležící
za	za	k7c7	za
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
jsou	být	k5eAaImIp3nP	být
pusté	pustý	k2eAgFnPc1d1	pustá
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
prosté	prostý	k2eAgInPc1d1	prostý
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
nejpočetněji	početně	k6eAd3	početně
osídlené	osídlený	k2eAgInPc1d1	osídlený
biomy	biom	k1gInPc1	biom
leží	ležet	k5eAaImIp3nP	ležet
poblíž	poblíž	k7c2	poblíž
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
skupinou	skupina	k1gFnSc7	skupina
organizmů	organizmus	k1gInPc2	organizmus
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
bakterie	bakterie	k1gFnPc1	bakterie
(	(	kIx(	(
<g/>
asi	asi	k9	asi
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
1030	[number]	k4	1030
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
organizmy	organizmus	k1gInPc1	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
archebakterie	archebakterie	k1gFnPc1	archebakterie
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
velice	velice	k6eAd1	velice
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
žít	žít	k5eAaImF	žít
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
organizmy	organizmus	k1gInPc1	organizmus
byly	být	k5eAaImAgInP	být
zřejmě	zřejmě	k6eAd1	zřejmě
prvními	první	k4xOgMnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
před	před	k7c4	před
asi	asi	k9	asi
1,8	[number]	k4	1,8
–	–	k?	–
1,3	[number]	k4	1,3
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
eukaryota	eukaryota	k1gFnSc1	eukaryota
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
řadíme	řadit	k5eAaImIp1nP	řadit
i	i	k9	i
dnešní	dnešní	k2eAgFnPc1d1	dnešní
mnohobuněčné	mnohobuněčný	k2eAgFnPc1d1	mnohobuněčná
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
nebo	nebo	k8xC	nebo
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
pomocí	pomocí	k7c2	pomocí
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
živočichové	živočich	k1gMnPc1	živočich
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
heterotrofně	heterotrofně	k6eAd1	heterotrofně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
živočichy	živočich	k1gMnPc7	živočich
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
osídlil	osídlit	k5eAaPmAgMnS	osídlit
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
milionech	milion	k4xCgInPc6	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
pevného	pevný	k2eAgNnSc2d1	pevné
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
polotekutého	polotekutý	k2eAgNnSc2d1	polotekuté
vnějšího	vnější	k2eAgNnSc2d1	vnější
jádra	jádro	k1gNnSc2	jádro
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
sfér	sféra	k1gFnPc2	sféra
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vnitřek	vnitřek	k1gInSc1	vnitřek
Země	zem	k1gFnSc2	zem
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
dynamo	dynamo	k1gNnSc1	dynamo
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
ke	k	k7c3	k
generování	generování	k1gNnSc3	generování
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
planety	planeta	k1gFnSc2	planeta
pomocí	pomocí	k7c2	pomocí
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
siločar	siločára	k1gFnPc2	siločára
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
chráněna	chránit	k5eAaImNgFnS	chránit
štítem	štít	k1gInSc7	štít
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odklání	odklánět	k5eAaImIp3nS	odklánět
dopadající	dopadající	k2eAgFnPc4d1	dopadající
vysokoenergetické	vysokoenergetický	k2eAgFnPc4d1	vysokoenergetická
částice	částice	k1gFnPc4	částice
vycházející	vycházející	k2eAgFnPc4d1	vycházející
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
Slunce	slunce	k1gNnSc2	slunce
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
je	být	k5eAaImIp3nS	být
zdánlivě	zdánlivě	k6eAd1	zdánlivě
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
více	hodně	k6eAd2	hodně
protažena	protáhnout	k5eAaPmNgFnS	protáhnout
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Van	vana	k1gFnPc2	vana
Allenovy	Allenův	k2eAgInPc1d1	Allenův
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Geomagnetické	geomagnetický	k2eAgNnSc1d1	geomagnetické
pole	pole	k1gNnSc1	pole
odklání	odklánět	k5eAaImIp3nS	odklánět
a	a	k8xC	a
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
protony	proton	k1gInPc4	proton
a	a	k8xC	a
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
vysílány	vysílat	k5eAaImNgInP	vysílat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
energetické	energetický	k2eAgFnPc1d1	energetická
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
odkláněny	odkláněn	k2eAgFnPc1d1	odkláněna
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
akumulaci	akumulace	k1gFnSc3	akumulace
do	do	k7c2	do
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
tzv.	tzv.	kA	tzv.
Van	van	k1gInSc4	van
Allenovy	Allenův	k2eAgInPc1d1	Allenův
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Pásy	pás	k1gInPc1	pás
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc4d1	vnější
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
k	k	k7c3	k
poloze	poloha	k1gFnSc3	poloha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
pásů	pás	k1gInPc2	pás
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
vypuštění	vypuštění	k1gNnSc6	vypuštění
první	první	k4xOgFnSc2	první
americké	americký	k2eAgFnSc2d1	americká
sondy	sonda	k1gFnSc2	sonda
Explorer	Explorer	k1gInSc4	Explorer
1	[number]	k4	1
a	a	k8xC	a
vnější	vnější	k2eAgInPc1d1	vnější
pásy	pás	k1gInPc1	pás
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
sondy	sonda	k1gFnSc2	sonda
Luna	luna	k1gFnSc1	luna
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Allenovy	Allenův	k2eAgInPc1d1	Allenův
pásy	pás	k1gInPc1	pás
začínají	začínat	k5eAaImIp3nP	začínat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
km	km	kA	km
nad	nad	k7c7	nad
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
radiační	radiační	k2eAgInSc1d1	radiační
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zhuštěním	zhuštění	k1gNnSc7	zhuštění
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
okolo	okolo	k7c2	okolo
3000	[number]	k4	3000
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc4d1	tvořen
energetickými	energetický	k2eAgInPc7d1	energetický
protony	proton	k1gInPc7	proton
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
oblast	oblast	k1gFnSc1	oblast
zhuštění	zhuštění	k1gNnSc2	zhuštění
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
000	[number]	k4	000
km	km	kA	km
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
vysokoenergetickými	vysokoenergetický	k2eAgInPc7d1	vysokoenergetický
elektrony	elektron	k1gInPc7	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Slunce	slunce	k1gNnSc4	slunce
za	za	k7c4	za
365,256	[number]	k4	365,256
<g/>
4	[number]	k4	4
průměrných	průměrný	k2eAgInPc2d1	průměrný
slunečních	sluneční	k2eAgInPc2d1	sluneční
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
1	[number]	k4	1
siderický	siderický	k2eAgInSc4d1	siderický
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
to	ten	k3xDgNnSc1	ten
dává	dávat	k5eAaImIp3nS	dávat
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
1	[number]	k4	1
°	°	k?	°
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pohyb	pohyb	k1gInSc1	pohyb
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
o	o	k7c4	o
sluneční	sluneční	k2eAgInSc4d1	sluneční
či	či	k8xC	či
měsíční	měsíční	k2eAgInSc4d1	měsíční
průměr	průměr	k1gInSc4	průměr
za	za	k7c4	za
každých	každý	k3xTgFnPc2	každý
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
stačí	stačit	k5eAaBmIp3nS	stačit
k	k	k7c3	k
uražení	uražení	k1gNnSc3	uražení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
zemského	zemský	k2eAgInSc2d1	zemský
průměru	průměr	k1gInSc2	průměr
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
12	[number]	k4	12
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
za	za	k7c4	za
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
–	–	k?	–
Měsíc	měsíc	k1gInSc1	měsíc
(	(	kIx(	(
<g/>
384	[number]	k4	384
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
za	za	k7c4	za
4	[number]	k4	4
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
přirozený	přirozený	k2eAgInSc1d1	přirozený
satelit	satelit	k1gInSc1	satelit
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
27	[number]	k4	27
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
pohyb	pohyb	k1gInSc1	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
hvězdám	hvězda	k1gFnPc3	hvězda
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
12	[number]	k4	12
°	°	k?	°
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c4	o
měsíční	měsíční	k2eAgInSc4d1	měsíční
poloměr	poloměr	k1gInSc4	poloměr
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Viděno	vidět	k5eAaImNgNnS	vidět
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
jsou	být	k5eAaImIp3nP	být
pohyb	pohyb	k1gInSc4	pohyb
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gInSc2	její
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
rotace	rotace	k1gFnSc2	rotace
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
všechny	všechen	k3xTgFnPc4	všechen
proti	proti	k7c3	proti
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Roviny	rovina	k1gFnSc2	rovina
orbity	orbita	k1gFnPc1	orbita
a	a	k8xC	a
rotace	rotace	k1gFnPc1	rotace
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
nekryjí	krýt	k5eNaImIp3nP	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
vychýlena	vychýlit	k5eAaPmNgFnS	vychýlit
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
23,5	[number]	k4	23,5
stupňů	stupeň	k1gInPc2	stupeň
proti	proti	k7c3	proti
rovině	rovina	k1gFnSc3	rovina
Země	zem	k1gFnSc2	zem
–	–	k?	–
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
a	a	k8xC	a
rovina	rovina	k1gFnSc1	rovina
Země	zem	k1gFnSc2	zem
–	–	k?	–
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
sklon	sklon	k1gInSc4	sklon
asi	asi	k9	asi
5	[number]	k4	5
stupňů	stupeň	k1gInPc2	stupeň
proti	proti	k7c3	proti
rovině	rovina	k1gFnSc3	rovina
Země	zem	k1gFnSc2	zem
<g/>
–	–	k?	–
<g/>
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
bychom	by	kYmCp1nP	by
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
zatmění	zatměný	k2eAgMnPc1d1	zatměný
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
(	(	kIx(	(
<g/>
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,5	[number]	k4	1,5
Gm	Gm	k1gFnPc2	Gm
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
jediného	jediný	k2eAgInSc2d1	jediný
přirozeného	přirozený	k2eAgInSc2d1	přirozený
satelitu	satelit	k1gInSc2	satelit
(	(	kIx(	(
<g/>
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
pohodlně	pohodlně	k6eAd1	pohodlně
vejde	vejít	k5eAaPmIp3nS	vejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
podléhá	podléhat	k5eAaImIp3nS	podléhat
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
pomalému	pomalý	k2eAgInSc3d1	pomalý
precesnímu	precesní	k2eAgInSc3d1	precesní
pohybu	pohyb	k1gInSc3	pohyb
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
dobrých	dobrý	k2eAgInPc2d1	dobrý
25	[number]	k4	25
725	[number]	k4	725
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nutaci	nutace	k1gFnSc4	nutace
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
periodou	perioda	k1gFnSc7	perioda
18,6	[number]	k4	18,6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pohyby	pohyb	k1gInPc1	pohyb
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
diferenciálním	diferenciální	k2eAgInSc7d1	diferenciální
vlivem	vliv	k1gInSc7	vliv
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c4	na
rovníkovou	rovníkový	k2eAgFnSc4d1	Rovníková
deformaci	deformace	k1gFnSc4	deformace
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
zploštěním	zploštění	k1gNnSc7	zploštění
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
spojené	spojený	k2eAgInPc1d1	spojený
se	s	k7c7	s
zemským	zemský	k2eAgNnSc7d1	zemské
tělesem	těleso	k1gNnSc7	těleso
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
rotace	rotace	k1gFnSc1	rotace
také	také	k9	také
lehce	lehko	k6eAd1	lehko
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
kvůli	kvůli	k7c3	kvůli
pohybu	pohyb	k1gInSc3	pohyb
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
kvaziperiodický	kvaziperiodický	k2eAgInSc1d1	kvaziperiodický
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
roční	roční	k2eAgFnSc4d1	roční
složku	složka	k1gFnSc4	složka
a	a	k8xC	a
složku	složka	k1gFnSc4	složka
se	s	k7c7	s
čtrnáctiměsíčním	čtrnáctiměsíční	k2eAgInSc7d1	čtrnáctiměsíční
cyklem	cyklus	k1gInSc7	cyklus
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Chandlerova	Chandlerův	k2eAgFnSc1d1	Chandlerova
perioda	perioda	k1gFnSc1	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
vlivem	vlivem	k7c2	vlivem
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
proměnná	proměnný	k2eAgFnSc1d1	proměnná
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nastává	nastávat	k5eAaImIp3nS	nastávat
zemský	zemský	k2eAgInSc4d1	zemský
perihel	perihel	k1gInSc4	perihel
vždy	vždy	k6eAd1	vždy
kolem	kolem	k7c2	kolem
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
afel	afel	k1gInSc4	afel
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
dobách	doba	k1gFnPc6	doba
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
precese	precese	k1gFnPc4	precese
a	a	k8xC	a
Milankovičovy	Milankovičův	k2eAgInPc4d1	Milankovičův
cykly	cyklus	k1gInPc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
spojující	spojující	k2eAgMnSc1d1	spojující
severní	severní	k2eAgMnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
trvá	trvat	k5eAaImIp3nS	trvat
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
56	[number]	k4	56
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
4,091	[number]	k4	4,091
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
1	[number]	k4	1
siderický	siderický	k2eAgInSc4d1	siderický
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
pohybu	pohyb	k1gInSc2	pohyb
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
meteorů	meteor	k1gInPc2	meteor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
nízko	nízko	k6eAd1	nízko
obíhajícími	obíhající	k2eAgInPc7d1	obíhající
satelity	satelit	k1gInPc7	satelit
<g/>
)	)	kIx)	)
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k8xS	jako
pohyb	pohyb	k1gInSc1	pohyb
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
15	[number]	k4	15
°	°	k?	°
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
=	=	kIx~	=
15	[number]	k4	15
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
sluneční	sluneční	k2eAgInSc4d1	sluneční
nebo	nebo	k8xC	nebo
měsíční	měsíční	k2eAgInSc4d1	měsíční
průměr	průměr	k1gInSc4	průměr
každé	každý	k3xTgFnPc4	každý
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
obří	obří	k2eAgInSc4d1	obří
setrvačník	setrvačník	k1gInSc4	setrvačník
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
nemá	mít	k5eNaImIp3nS	mít
neměnnou	neměnný	k2eAgFnSc4d1	neměnná
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
silné	silný	k2eAgNnSc1d1	silné
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
ji	on	k3xPp3gFnSc4	on
vychýlilo	vychýlit	k5eAaPmAgNnS	vychýlit
asi	asi	k9	asi
o	o	k7c4	o
16	[number]	k4	16
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Časové	časový	k2eAgNnSc1d1	časové
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přesunuje	přesunovat	k5eAaImIp3nS	přesunovat
oblast	oblast	k1gFnSc1	oblast
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
příchod	příchod	k1gInSc4	příchod
a	a	k8xC	a
odchod	odchod	k1gInSc4	odchod
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
celý	celý	k2eAgInSc4d1	celý
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
na	na	k7c4	na
24	[number]	k4	24
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
dle	dle	k7c2	dle
poledníků	poledník	k1gInPc2	poledník
po	po	k7c4	po
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Pásmový	pásmový	k2eAgInSc1d1	pásmový
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
dle	dle	k7c2	dle
střední	střední	k2eAgFnSc2d1	střední
hodnoty	hodnota	k1gFnSc2	hodnota
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
poledník	poledník	k1gInSc4	poledník
se	s	k7c7	s
středem	střed	k1gInSc7	střed
na	na	k7c4	na
7,5	[number]	k4	7,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
dopočítává	dopočítávat	k5eAaImIp3nS	dopočítávat
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
koordinovaného	koordinovaný	k2eAgInSc2d1	koordinovaný
světového	světový	k2eAgInSc2d1	světový
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
posun	posun	k1gInSc1	posun
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
určen	určit	k5eAaPmNgInS	určit
celistvým	celistvý	k2eAgInSc7d1	celistvý
počtem	počet	k1gInSc7	počet
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plus	plus	k1gInSc1	plus
či	či	k8xC	či
mínus	mínus	k1gInSc1	mínus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
sklonění	sklonění	k1gNnSc2	sklonění
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
Země	zem	k1gFnSc2	zem
o	o	k7c4	o
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
a	a	k8xC	a
oběžné	oběžný	k2eAgFnPc4d1	oběžná
trajektorie	trajektorie	k1gFnPc4	trajektorie
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
elipsy	elipsa	k1gFnSc2	elipsa
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
či	či	k8xC	či
druhou	druhý	k4xOgFnSc4	druhý
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
projevuje	projevovat	k5eAaImIp3nS	projevovat
střídáním	střídání	k1gNnSc7	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
jaro	jaro	k6eAd1	jaro
<g/>
,	,	kIx,	,
léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc1	podzim
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
vždy	vždy	k6eAd1	vždy
více	hodně	k6eAd2	hodně
přivrací	přivracet	k5eAaImIp3nS	přivracet
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
polokoule	polokoule	k1gFnSc1	polokoule
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
střídání	střídání	k1gNnSc1	střídání
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
protočené	protočený	k2eAgFnSc2d1	protočená
a	a	k8xC	a
tedy	tedy	k9	tedy
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
mezi	mezi	k7c7	mezi
severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc7d1	jižní
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
zima	zima	k6eAd1	zima
a	a	k8xC	a
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
eliptická	eliptický	k2eAgFnSc1d1	eliptická
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
mírnější	mírný	k2eAgMnPc1d2	mírnější
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
perihélia	perihélium	k1gNnSc2	perihélium
a	a	k8xC	a
tedy	tedy	k9	tedy
nejblíže	blízce	k6eAd3	blízce
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
léta	léto	k1gNnPc4	léto
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
létům	léto	k1gNnPc3	léto
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
studenější	studený	k2eAgFnSc6d2	studenější
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
Země	zem	k1gFnSc2	zem
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
při	při	k7c6	při
perihelu	perihel	k1gInSc6	perihel
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zimním	zimní	k2eAgInSc6d1	zimní
slunovratu	slunovrat	k1gInSc6	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdále	daleko	k6eAd3	daleko
je	být	k5eAaImIp3nS	být
pří	pře	k1gFnSc7	pře
afelu	afel	k1gInSc2	afel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
dle	dle	k7c2	dle
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
zákonů	zákon	k1gInPc2	zákon
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
stejně	stejně	k6eAd1	stejně
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
má	mít	k5eAaImIp3nS	mít
současně	současně	k6eAd1	současně
i	i	k9	i
největší	veliký	k2eAgFnSc4d3	veliký
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
kratší	krátký	k2eAgFnSc6d2	kratší
než	než	k8xS	než
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
severní	severní	k2eAgFnSc6d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kratší	krátký	k2eAgFnSc1d2	kratší
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
příklad	příklad	k1gInSc4	příklad
léto	léto	k1gNnSc4	léto
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
93	[number]	k4	93
dní	den	k1gInPc2	den
a	a	k8xC	a
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
pouze	pouze	k6eAd1	pouze
89	[number]	k4	89
dní	den	k1gInPc2	den
a	a	k8xC	a
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
či	či	k8xC	či
též	též	k9	též
Luna	luna	k1gFnSc1	luna
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgNnSc1d1	velké
terestrické	terestrický	k2eAgNnSc1d1	terestrické
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
zemského	zemský	k2eAgInSc2d1	zemský
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Plutova	Plutův	k2eAgMnSc2d1	Plutův
Charona	Charon	k1gMnSc2	Charon
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
planety	planeta	k1gFnSc2	planeta
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgInPc4d1	přirozený
satelity	satelit	k1gInPc4	satelit
obíhající	obíhající	k2eAgInPc4d1	obíhající
kolem	kolem	k7c2	kolem
planet	planeta	k1gFnPc2	planeta
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
měsíce	měsíc	k1gInPc4	měsíc
<g/>
"	"	kIx"	"
právě	právě	k9	právě
podle	podle	k7c2	podle
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnPc1d1	gravitační
síly	síla	k1gFnPc1	síla
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Měsícem	měsíc	k1gInSc7	měsíc
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Tatáž	týž	k3xTgFnSc1	týž
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vázané	vázaný	k2eAgFnSc3d1	vázaná
rotaci	rotace	k1gFnSc3	rotace
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc1	jeho
rotační	rotační	k2eAgFnSc1d1	rotační
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
oběhu	oběh	k1gInSc3	oběh
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
planetě	planeta	k1gFnSc3	planeta
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Měsíc	měsíc	k1gInSc1	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Sluncem	slunce	k1gNnSc7	slunce
osvětlovány	osvětlován	k2eAgFnPc1d1	osvětlována
jeho	jeho	k3xOp3gFnPc1	jeho
různé	různý	k2eAgFnPc1d1	různá
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
měsíčním	měsíční	k2eAgFnPc3d1	měsíční
fázím	fáze	k1gFnPc3	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Temná	temný	k2eAgFnSc1d1	temná
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
osvětlené	osvětlený	k2eAgMnPc4d1	osvětlený
slunečním	sluneční	k2eAgInSc7d1	sluneční
terminátorem	terminátor	k1gInSc7	terminátor
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
dramaticky	dramaticky	k6eAd1	dramaticky
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc1	vývoj
života	život	k1gInSc2	život
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
brání	bránit	k5eAaImIp3nS	bránit
prudkým	prudký	k2eAgFnPc3d1	prudká
změnám	změna	k1gFnPc3	změna
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
počítačové	počítačový	k2eAgFnPc4d1	počítačová
simulace	simulace	k1gFnPc4	simulace
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výchylka	výchylka	k1gFnSc1	výchylka
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
je	být	k5eAaImIp3nS	být
stabilizována	stabilizován	k2eAgFnSc1d1	stabilizována
jeho	jeho	k3xOp3gFnPc7	jeho
slapovými	slapový	k2eAgFnPc7d1	slapová
interakcemi	interakce	k1gFnPc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
teoretikové	teoretik	k1gMnPc1	teoretik
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
stabilizace	stabilizace	k1gFnSc2	stabilizace
by	by	kYmCp3nS	by
točivý	točivý	k2eAgInSc1d1	točivý
moment	moment	k1gInSc1	moment
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
planet	planeta	k1gFnPc2	planeta
na	na	k7c4	na
zemskou	zemský	k2eAgFnSc4d1	zemská
rovníkovou	rovníkový	k2eAgFnSc4d1	Rovníková
deformaci	deformace	k1gFnSc4	deformace
způsobil	způsobit	k5eAaPmAgInS	způsobit
chaotickou	chaotický	k2eAgFnSc4d1	chaotická
nestabilitu	nestabilita	k1gFnSc4	nestabilita
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc1	rotace
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
rovině	rovina	k1gFnSc3	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
,	,	kIx,	,
podnebí	podnebí	k1gNnSc1	podnebí
by	by	kYmCp3nS	by
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
extrémně	extrémně	k6eAd1	extrémně
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
sezónními	sezónní	k2eAgInPc7d1	sezónní
rozdíly	rozdíl	k1gInPc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pól	pól	k1gInSc1	pól
nasměrován	nasměrovat	k5eAaPmNgInS	nasměrovat
přímo	přímo	k6eAd1	přímo
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
odvrácen	odvrátit	k5eAaPmNgInS	odvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Planetologové	Planetolog	k1gMnPc1	Planetolog
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
studovali	studovat	k5eAaImAgMnP	studovat
<g/>
,	,	kIx,	,
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
všech	všecek	k3xTgNnPc2	všecek
větších	veliký	k2eAgNnPc2d2	veliký
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
forem	forma	k1gFnPc2	forma
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
téma	téma	k1gNnSc1	téma
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kontroverzním	kontroverzní	k2eAgMnPc3d1	kontroverzní
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
studie	studie	k1gFnPc4	studie
Marsu	Mars	k1gInSc2	Mars
–	–	k?	–
který	který	k3yIgInSc1	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
zemskou	zemský	k2eAgFnSc4d1	zemská
rotační	rotační	k2eAgFnSc4d1	rotační
periodu	perioda	k1gFnSc4	perioda
a	a	k8xC	a
vychýlení	vychýlení	k1gNnSc4	vychýlení
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
velký	velký	k2eAgInSc4d1	velký
měsíc	měsíc	k1gInSc4	měsíc
ani	ani	k8xC	ani
tekuté	tekutý	k2eAgNnSc4d1	tekuté
jádro	jádro	k1gNnSc4	jádro
–	–	k?	–
mohou	moct	k5eAaImIp3nP	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
jiný	jiný	k2eAgInSc4d1	jiný
náhled	náhled	k1gInSc4	náhled
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
Měsíce	měsíc	k1gInSc2	měsíc
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nepatrné	patrný	k2eNgNnSc1d1	nepatrné
zpomalování	zpomalování	k1gNnSc1	zpomalování
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
platí	platit	k5eAaImIp3nS	platit
zákon	zákon	k1gInSc4	zákon
zachování	zachování	k1gNnSc4	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zvolna	zvolna	k6eAd1	zvolna
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Široce	široko	k6eAd1	široko
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
Měsíce	měsíc	k1gInSc2	měsíc
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgMnS	zformovat
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
rané	raný	k2eAgFnSc2d1	raná
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
protoplanetou	protoplaneta	k1gFnSc7	protoplaneta
velikosti	velikost	k1gFnSc2	velikost
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
impaktu	impakt	k1gInSc2	impakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgFnPc7d1	jiná
věcmi	věc	k1gFnPc7	věc
<g/>
)	)	kIx)	)
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
relativní	relativní	k2eAgInSc4d1	relativní
nedostatek	nedostatek	k1gInSc4	nedostatek
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
těkavých	těkavý	k2eAgInPc2d1	těkavý
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
identické	identický	k2eAgInPc4d1	identický
se	se	k3xPyFc4	se
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
viděno	viděn	k2eAgNnSc1d1	viděno
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
velikost	velikost	k1gFnSc4	velikost
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc4	slunce
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
400	[number]	k4	400
<g/>
×	×	k?	×
vzdálenější	vzdálený	k2eAgMnSc1d2	vzdálenější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
lze	lze	k6eAd1	lze
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
pozorovat	pozorovat	k5eAaImF	pozorovat
úplná	úplný	k2eAgNnPc4d1	úplné
i	i	k8xC	i
prstencovitá	prstencovitý	k2eAgNnPc4d1	prstencovité
zatmění	zatmění	k1gNnPc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Měsíce	měsíc	k1gInSc2	měsíc
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
žádný	žádný	k3yNgInSc4	žádný
přirozený	přirozený	k2eAgInSc4d1	přirozený
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
obíhal	obíhat	k5eAaImAgInS	obíhat
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
planetky	planetka	k1gFnPc4	planetka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňovat	k5eAaImNgFnP	ovlivňovat
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
polem	pole	k1gNnSc7	pole
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
sladěnou	sladěný	k2eAgFnSc4d1	sladěná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
velký	velký	k2eAgInSc4d1	velký
asteroid	asteroid	k1gInSc4	asteroid
3753	[number]	k4	3753
Cruithne	Cruithne	k1gMnSc1	Cruithne
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
protáhlou	protáhlý	k2eAgFnSc4d1	protáhlá
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
v	v	k7c6	v
nejvzdálenějším	vzdálený	k2eAgInSc6d3	nejvzdálenější
bodě	bod	k1gInSc6	bod
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
až	až	k9	až
za	za	k7c4	za
drahou	drahá	k1gFnSc4	drahá
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
:	:	kIx,	:
364,01	[number]	k4	364,01
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
pomocí	pomocí	k7c2	pomocí
infračerveného	infračervený	k2eAgInSc2d1	infračervený
kosmického	kosmický	k2eAgInSc2d1	kosmický
dalekohledu	dalekohled	k1gInSc2	dalekohled
WISE	WISE	kA	WISE
objevena	objeven	k2eAgFnSc1d1	objevena
jiná	jiný	k2eAgFnSc1d1	jiná
planetka	planetka	k1gFnSc1	planetka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgFnSc6d1	stejná
dráze	dráha	k1gFnSc6	dráha
jako	jako	k8xC	jako
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
60	[number]	k4	60
<g/>
°	°	k?	°
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
trojan	trojan	k1gInSc1	trojan
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
však	však	k9	však
jen	jen	k9	jen
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slapové	slapový	k2eAgInPc4d1	slapový
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
působí	působit	k5eAaImIp3nS	působit
svým	svůj	k3xOyFgInSc7	svůj
gravitačním	gravitační	k2eAgInSc7d1	gravitační
vlivem	vliv	k1gInSc7	vliv
–	–	k?	–
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
–	–	k?	–
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnPc4d1	malá
deformace	deformace	k1gFnPc4	deformace
jejího	její	k3xOp3gInSc2	její
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
viditelnými	viditelný	k2eAgInPc7d1	viditelný
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
jsou	být	k5eAaImIp3nP	být
příliv	příliv	k1gInSc4	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
periodou	perioda	k1gFnSc7	perioda
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
zdánlivě	zdánlivě	k6eAd1	zdánlivě
obíhá	obíhat	k5eAaImIp3nS	obíhat
Měsíc	měsíc	k1gInSc1	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
–	–	k?	–
tedy	tedy	k9	tedy
24	[number]	k4	24
h	h	k?	h
50	[number]	k4	50
min	mina	k1gFnPc2	mina
<g/>
..	..	k?	..
Přítomnost	přítomnost	k1gFnSc1	přítomnost
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zjevná	zjevný	k2eAgFnSc1d1	zjevná
již	již	k6eAd1	již
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
obrovské	obrovský	k2eAgFnPc1d1	obrovská
zalesněné	zalesněný	k2eAgFnPc1d1	zalesněná
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgInPc4d1	vystupující
korálové	korálový	k2eAgInPc4d1	korálový
útesy	útes	k1gInPc4	útes
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k8xC	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
dostal	dostat	k5eAaPmAgInS	dostat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
několika	několik	k4yIc2	několik
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
fotosyntézy	fotosyntéza	k1gFnPc4	fotosyntéza
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
známá	známý	k2eAgFnSc1d1	známá
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
a	a	k8xC	a
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
prototypem	prototyp	k1gInSc7	prototyp
obytné	obytný	k2eAgFnSc2d1	obytná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
vhodné	vhodný	k2eAgNnSc4d1	vhodné
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
udržela	udržet	k5eAaPmAgFnS	udržet
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
splněna	splnit	k5eAaPmNgFnS	splnit
základní	základní	k2eAgFnSc1d1	základní
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
představ	představa	k1gFnPc2	představa
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
život	život	k1gInSc1	život
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
vzniku	vznik	k1gInSc3	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
obývají	obývat	k5eAaImIp3nP	obývat
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
už	už	k6eAd1	už
asi	asi	k9	asi
3,8	[number]	k4	3,8
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
obydlena	obydlen	k2eAgFnSc1d1	obydlena
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
řádově	řádově	k6eAd1	řádově
1033	[number]	k4	1033
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Formy	forma	k1gFnPc1	forma
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
od	od	k7c2	od
nejjednodušších	jednoduchý	k2eAgFnPc2d3	nejjednodušší
bezjaderných	bezjaderný	k2eAgFnPc2d1	Bezjaderná
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
jednobuněčných	jednobuněčný	k2eAgFnPc2d1	jednobuněčná
(	(	kIx(	(
<g/>
prokaryot	prokaryota	k1gFnPc2	prokaryota
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
větší	veliký	k2eAgInPc4d2	veliký
jednobuněčné	jednobuněčný	k2eAgInPc4d1	jednobuněčný
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
řasy	řasa	k1gFnSc2	řasa
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
a	a	k8xC	a
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
druhy	druh	k1gInPc1	druh
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
představují	představovat	k5eAaImIp3nP	představovat
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc4	zlomek
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
obývají	obývat	k5eAaImIp3nP	obývat
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
určitou	určitý	k2eAgFnSc4d1	určitá
vrstvu	vrstva	k1gFnSc4	vrstva
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
spodní	spodní	k2eAgFnPc1d1	spodní
části	část	k1gFnPc1	část
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
těch	ten	k3xDgMnPc6	ten
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
nejextrémnějších	extrémní	k2eAgNnPc6d3	nejextrémnější
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
:	:	kIx,	:
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
oceánských	oceánský	k2eAgInPc2d1	oceánský
příkopů	příkop	k1gInPc2	příkop
bez	bez	k7c2	bez
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
obrovskému	obrovský	k2eAgInSc3d1	obrovský
hydrostatickému	hydrostatický	k2eAgInSc3d1	hydrostatický
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
v	v	k7c6	v
horkých	horký	k2eAgInPc6d1	horký
sirných	sirný	k2eAgInPc6d1	sirný
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
v	v	k7c6	v
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
ledu	led	k1gInSc6	led
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejsušších	suchý	k2eAgFnPc6d3	nejsušší
pouštích	poušť	k1gFnPc6	poušť
i	i	k8xC	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
dýchatelného	dýchatelný	k2eAgInSc2d1	dýchatelný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Země	zem	k1gFnSc2	zem
obydlena	obydlet	k5eAaPmNgFnS	obydlet
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
biosféra	biosféra	k1gFnSc1	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Biosféra	biosféra	k1gFnSc1	biosféra
a	a	k8xC	a
neživé	živý	k2eNgFnPc1d1	neživá
složky	složka	k1gFnPc1	složka
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
litosféra	litosféra	k1gFnSc1	litosféra
<g/>
,	,	kIx,	,
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
a	a	k8xC	a
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
těsně	těsně	k6eAd1	těsně
propojeny	propojit	k5eAaPmNgFnP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
koloběhu	koloběh	k1gInSc6	koloběh
mnoha	mnoho	k4c2	mnoho
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
koloběh	koloběh	k1gInSc4	koloběh
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
koloběh	koloběh	k1gInSc4	koloběh
mnoha	mnoho	k4c2	mnoho
klíčových	klíčový	k2eAgFnPc2d1	klíčová
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
koloběh	koloběh	k1gInSc1	koloběh
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
koloběh	koloběh	k1gInSc1	koloběh
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
koloběh	koloběh	k1gInSc1	koloběh
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
procesech	proces	k1gInPc6	proces
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
jak	jak	k6eAd1	jak
živé	živý	k2eAgInPc1d1	živý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
neživé	živý	k2eNgFnPc1d1	neživá
složky	složka	k1gFnPc1	složka
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
život	život	k1gInSc1	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
by	by	kYmCp3nS	by
bez	bez	k7c2	bez
těchto	tento	k3xDgInPc2	tento
koloběhů	koloběh	k1gInPc2	koloběh
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovskou	domovský	k2eAgFnSc7d1	domovská
planetou	planeta	k1gFnSc7	planeta
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
nezávislých	závislý	k2eNgInPc2d1	nezávislý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dubnu	duben	k1gInSc3	duben
2014	[number]	k4	2014
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
158	[number]	k4	158
138	[number]	k4	138
650	[number]	k4	650
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
obydlena	obydlen	k2eAgFnSc1d1	obydlena
nebo	nebo	k8xC	nebo
zřetelně	zřetelně	k6eAd1	zřetelně
změněna	změnit	k5eAaPmNgNnP	změnit
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
noosféra	noosféra	k1gFnSc1	noosféra
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
názvů	název	k1gInPc2	název
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kulturách	kultura	k1gFnPc6	kultura
či	či	k8xC	či
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
patří	patřit	k5eAaImIp3nS	patřit
označení	označení	k1gNnSc4	označení
Gaia	Gaium	k1gNnSc2	Gaium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
slovní	slovní	k2eAgNnSc1d1	slovní
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
matka	matka	k1gFnSc1	matka
Země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
Chaosu	chaos	k1gInSc2	chaos
a	a	k8xC	a
manželku	manželka	k1gFnSc4	manželka
Úranovu	Úranův	k2eAgFnSc4d1	Úranův
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
porodila	porodit	k5eAaPmAgFnS	porodit
Titány	Titán	k1gMnPc4	Titán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
později	pozdě	k6eAd2	pozdě
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
válku	válka	k1gFnSc4	válka
mezi	mezi	k7c7	mezi
giganty	gigant	k1gMnPc7	gigant
a	a	k8xC	a
olympskými	olympský	k2eAgMnPc7d1	olympský
bohy	bůh	k1gMnPc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
BEAZLEY	BEAZLEY	kA	BEAZLEY
<g/>
,	,	kIx,	,
Mitchell	Mitchell	k1gInSc1	Mitchell
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Orig	Orig	k1gMnSc1	Orig
<g/>
.	.	kIx.	.
</s>
<s>
Ljubljana	Ljubljana	k1gFnSc1	Ljubljana
<g/>
:	:	kIx,	:
Mladinska	Mladinsko	k1gNnSc2	Mladinsko
knjiga	knjiga	k1gFnSc1	knjiga
<g/>
.	.	kIx.	.
</s>
<s>
LOVELOCK	LOVELOCK	kA	LOVELOCK
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Gaia	Gaia	k1gFnSc1	Gaia
:	:	kIx,	:
živoucí	živoucí	k2eAgFnSc1d1	živoucí
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Anton	Anton	k1gMnSc1	Anton
Markoš	Markoš	k1gMnSc1	Markoš
<g/>
;	;	kIx,	;
Přebal	přebal	k1gInSc1	přebal
<g/>
,	,	kIx,	,
vazba	vazba	k1gFnSc1	vazba
a	a	k8xC	a
graf	graf	k1gInSc1	graf
<g/>
.	.	kIx.	.
úprava	úprava	k1gFnSc1	úprava
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nárožník	nárožník	k1gInSc1	nárožník
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ČTK	ČTK	kA	ČTK
Repro	Repro	k1gNnSc1	Repro
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
;	;	kIx,	;
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
221	[number]	k4	221
s.	s.	k?	s.
<g/>
,	,	kIx,	,
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
fotogr	fotogr	k1gMnSc1	fotogr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tab	tab	kA	tab
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
grafy	graf	k1gInPc1	graf
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
436	[number]	k4	436
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
NETOPIL	topit	k5eNaImAgMnS	topit
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
.	.	kIx.	.
</s>
<s>
Hydrologie	hydrologie	k1gFnSc1	hydrologie
pevnin	pevnina	k1gFnPc2	pevnina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
294	[number]	k4	294
s.	s.	k?	s.
<g/>
,	,	kIx,	,
12	[number]	k4	12
s.	s.	k?	s.
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
KOL	kol	k6eAd1	kol
<g/>
..	..	k?	..
Naše	náš	k3xOp1gFnSc1	náš
modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Ladislav	Ladislava	k1gFnPc2	Ladislava
Pros	prosa	k1gFnPc2	prosa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
KOL	kol	k6eAd1	kol
<g/>
..	..	k?	..
Naše	náš	k3xOp1gFnSc1	náš
modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Souše	souš	k1gFnPc1	souš
a	a	k8xC	a
vodstvo	vodstvo	k1gNnSc1	vodstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Ladislav	Ladislava	k1gFnPc2	Ladislava
Pros	prosa	k1gFnPc2	prosa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
KOL	kol	k6eAd1	kol
<g/>
..	..	k?	..
Naše	náš	k3xOp1gFnSc1	náš
modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Ladislav	Ladislava	k1gFnPc2	Ladislava
Pros	prosa	k1gFnPc2	prosa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
213	[number]	k4	213
s.	s.	k?	s.
TRÁVNÍČEK	Trávníček	k1gMnSc1	Trávníček
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Objevování	objevování	k1gNnSc1	objevování
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Boučka	Bouček	k1gMnSc2	Bouček
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnSc1	ilustrace
Ladislav	Ladislava	k1gFnPc2	Ladislava
Pros	prosa	k1gFnPc2	prosa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
FARNDON	FARNDON	kA	FARNDON
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Kamila	Kamila	k1gFnSc1	Kamila
Šírová	Šírová	k1gFnSc1	Šírová
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
61	[number]	k4	61
s.	s.	k?	s.
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgFnPc1d1	barevná
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
654	[number]	k4	654
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ATTENBOROUGH	ATTENBOROUGH	kA	ATTENBOROUGH
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Žďárek	Žďárek	k1gMnSc1	Žďárek
<g/>
;	;	kIx,	;
Obálka	obálka	k1gFnSc1	obálka
<g/>
,	,	kIx,	,
vazba	vazba	k1gFnSc1	vazba
a	a	k8xC	a
graf	graf	k1gInSc1	graf
<g/>
.	.	kIx.	.
úprava	úprava	k1gFnSc1	úprava
Václav	Václav	k1gMnSc1	Václav
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7038	[number]	k4	7038
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
Earth	Earth	k1gMnSc1	Earth
NASA	NASA	kA	NASA
World	World	k1gMnSc1	World
Wind	Wind	k1gMnSc1	Wind
Zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
rekordy	rekord	k1gInPc4	rekord
světa	svět	k1gInSc2	svět
Pozice	pozice	k1gFnSc2	pozice
Země	zem	k1gFnSc2	zem
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
Populace	populace	k1gFnSc2	populace
Galerie	galerie	k1gFnSc2	galerie
Země	zem	k1gFnSc2	zem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Země	zem	k1gFnSc2	zem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Země	zem	k1gFnSc2	zem
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
NASA	NASA	kA	NASA
Solar	Solar	k1gInSc4	Solar
System	Syst	k1gInSc7	Syst
Exploration	Exploration	k1gInSc1	Exploration
</s>
