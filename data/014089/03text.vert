<s>
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
</s>
<s>
20	#num#	k4
<g/>
šilinková	šilinkový	k2eAgFnSc1d1
mince	mince	k1gFnSc1
</s>
<s>
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
byl	být	k5eAaImAgInS
rakouskou	rakouský	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
od	od	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgInPc1d1
období	období	k1gNnSc6
platnosti	platnost	k1gFnSc2
šilinku	šilink	k1gInSc2
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
dvě	dva	k4xCgNnPc4
období	období	k1gNnPc4
–	–	k?
první	první	k4xOgFnSc1
republika	republika	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byl	být	k5eAaImAgInS
zaveden	zaveden	k2eAgInSc1d1
šilink	šilink	k1gInSc1
místo	místo	k7c2
inflační	inflační	k2eAgFnSc2d1
Rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
připojení	připojení	k1gNnSc2
Rakouska	Rakousko	k1gNnSc2
k	k	k7c3
Třetí	třetí	k4xOgFnSc3
říši	říš	k1gFnSc3
platila	platit	k5eAaImAgNnP
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
říšská	říšský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
zaveden	zavést	k5eAaPmNgInS
oběh	oběh	k1gInSc1
šilinku	šilink	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šilink	šilink	k1gInSc1
se	se	k3xPyFc4
dělil	dělit	k5eAaImAgInS
na	na	k7c4
100	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc2
a	a	k8xC
bankovky	bankovka	k1gFnPc4
vydávala	vydávat	k5eAaImAgFnS,k5eAaPmAgFnS
Rakouská	rakouský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
(	(	kIx(
<g/>
Österreichische	Österreichische	k1gFnSc1
Nationalbank	Nationalbank	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyměňovány	vyměňován	k2eAgInPc1d1
za	za	k7c4
euro	euro	k1gNnSc4
byly	být	k5eAaImAgFnP
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
měny	měna	k1gFnSc2
byl	být	k5eAaImAgMnS
ATS	ATS	kA
<g/>
.	.	kIx.
</s>
<s>
Šilink	šilink	k1gInSc1
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
měnovou	měnový	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1925	#num#	k4
nahradil	nahradit	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
<g/>
,	,	kIx,
inflací	inflace	k1gFnSc7
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
znehodnocenou	znehodnocený	k2eAgFnSc4d1
<g/>
,	,	kIx,
rakousko-uherskou	rakousko-uherský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnPc1
byly	být	k5eAaImAgFnP
raženy	razit	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
grošů	groš	k1gInPc2
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
zlaté	zlatý	k2eAgFnPc1d1
mince	mince	k1gFnPc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
25	#num#	k4
a	a	k8xC
100	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
byly	být	k5eAaImAgFnP
vydávány	vydávat	k5eAaImNgFnP,k5eAaPmNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
a	a	k8xC
1000	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
byla	být	k5eAaImAgFnS
emise	emise	k1gFnSc1
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
bankovku	bankovka	k1gFnSc4
50	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
anšlusu	anšlus	k1gInSc6
Rakouska	Rakousko	k1gNnSc2
Třetí	třetí	k4xOgFnSc7
říší	říš	k1gFnSc7
byl	být	k5eAaImAgInS
šilink	šilink	k1gInSc1
nahrazen	nahradit	k5eAaPmNgInS
Říšskou	říšský	k2eAgFnSc7d1
markou	marka	k1gFnSc7
v	v	k7c6
nevýhodném	výhodný	k2eNgInSc6d1
kurzu	kurz	k1gInSc6
1	#num#	k4
RM	RM	kA
=	=	kIx~
1,50	1,50	k4
S.	S.	kA
</s>
<s>
Šilink	šilink	k1gInSc1
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Byly	být	k5eAaImAgFnP
zavedeny	zaveden	k2eAgFnPc1d1
mince	mince	k1gFnPc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
grošů	groš	k1gInPc2
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
šilinků	šilink	k1gInPc2
a	a	k8xC
také	také	k9
méně	málo	k6eAd2
užívané	užívaný	k2eAgInPc4d1
20	#num#	k4
a	a	k8xC
50	#num#	k4
šilinky	šilink	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
úplná	úplný	k2eAgFnSc1d1
emise	emise	k1gFnSc1
oběžných	oběžný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
1	#num#	k4
a	a	k8xC
20	#num#	k4
grošů	groš	k1gInPc2
se	se	k3xPyFc4
brzy	brzy	k6eAd1
přestaly	přestat	k5eAaPmAgFnP
razit	razit	k5eAaImF
z	z	k7c2
důvodu	důvod	k1gInSc2
své	svůj	k3xOyFgFnSc2
neekonomičnosti	neekonomičnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnovější	nový	k2eAgFnSc7d3
mincí	mince	k1gFnSc7
byla	být	k5eAaImAgFnS
bimetalická	bimetalický	k2eAgFnSc1d1
mince	mince	k1gFnSc1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
50	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
,	,	kIx,
připomínala	připomínat	k5eAaImAgFnS
rakouské	rakouský	k2eAgNnSc4d1
milénium	milénium	k1gNnSc4
a	a	k8xC
obíhala	obíhat	k5eAaImAgFnS
společně	společně	k6eAd1
s	s	k7c7
bankovkou	bankovka	k1gFnSc7
téže	tenže	k3xDgFnSc2,k3xTgFnSc2
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
líci	líc	k1gInSc6
všech	všecek	k3xTgFnPc2
rakouských	rakouský	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
byl	být	k5eAaImAgInS
buď	buď	k8xC
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
nebo	nebo	k8xC
číslovka	číslovka	k1gFnSc1
nominální	nominální	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rubu	rub	k1gInSc6
byl	být	k5eAaImAgInS
motiv	motiv	k1gInSc1
spojený	spojený	k2eAgInSc1d1
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
byly	být	k5eAaImAgFnP
vydávány	vydávat	k5eAaImNgFnP,k5eAaPmNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
a	a	k8xC
5000	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
neúplná	úplný	k2eNgFnSc1d1
emise	emise	k1gFnSc1
bankovek	bankovka	k1gFnPc2
byla	být	k5eAaImAgFnS
dána	dát	k5eAaPmNgFnS
do	do	k7c2
oběhu	oběh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jen	jen	k9
nominály	nominál	k1gInPc1
500	#num#	k4
a	a	k8xC
1000	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
líci	líc	k1gInSc6
všech	všecek	k3xTgFnPc2
rakouských	rakouský	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
byly	být	k5eAaImAgInP
portréty	portrét	k1gInPc1
významných	významný	k2eAgMnPc2d1
Rakušanů	Rakušan	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
20	#num#	k4
šilinků	šilink	k1gInPc2
–	–	k?
Moritz	moritz	k1gInSc1
Daffinger	Daffinger	k1gInSc1
/	/	kIx~
Vídeňské	vídeňský	k2eAgNnSc1d1
výtvarné	výtvarný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Albertina	Albertin	k2eAgInSc2d1
<g/>
,	,	kIx,
</s>
<s>
50	#num#	k4
šilinků	šilink	k1gInPc2
–	–	k?
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
/	/	kIx~
Vídeňská	vídeňský	k2eAgFnSc1d1
lékařská	lékařský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Josephinum	Josephinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
100	#num#	k4
šilinků	šilink	k1gInPc2
–	–	k?
Eugen	Eugen	k1gInSc1
Böhm	Böhm	k1gInSc1
/	/	kIx~
<g/>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
</s>
<s>
500	#num#	k4
šilinků	šilink	k1gInPc2
–	–	k?
architekt	architekt	k1gMnSc1
Otto	Otto	k1gMnSc1
Wagner	Wagner	k1gMnSc1
/	/	kIx~
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
</s>
<s>
1000	#num#	k4
šilinků	šilink	k1gInPc2
–	–	k?
Erwin	Erwin	k1gMnSc1
Schrödinger	Schrödinger	k1gMnSc1
/	/	kIx~
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
a	a	k8xC
vědci	vědec	k1gMnPc1
při	při	k7c6
práci	práce	k1gFnSc6
</s>
<s>
5000	#num#	k4
šilinků	šilink	k1gInPc2
Wolfgang	Wolfgang	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
Mozart	Mozart	k1gMnSc1
/	/	kIx~
Vídeňská	vídeňský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kurz	kurz	k1gInSc1
šilinku	šilink	k1gInSc2
k	k	k7c3
euru	euro	k1gNnSc3
při	při	k7c6
měnové	měnový	k2eAgFnSc6d1
výměně	výměna	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
byl	být	k5eAaImAgInS
1S	1S	k4
=	=	kIx~
0,07267	0,07267	k4
<g/>
€	€	k?
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Euro	euro	k1gNnSc1
</s>
<s>
Rakouské	rakouský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rakouský	rakouský	k2eAgInSc4d1
šilink	šilink	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Rakouské	rakouský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnPc1
nahrazené	nahrazený	k2eAgFnPc1d1
eurem	euro	k1gNnSc7
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Estonská	estonský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Francouzský	francouzský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Italská	italský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Kyperská	kyperský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Litevský	litevský	k2eAgInSc1d1
litas	litas	k1gInSc1
•	•	k?
Lotyšský	lotyšský	k2eAgInSc1d1
lat	lat	k1gInSc1
•	•	k?
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Maltská	maltský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Monacký	monacký	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Nizozemský	nizozemský	k2eAgInSc1d1
gulden	gulden	k1gInSc1
•	•	k?
Portugalské	portugalský	k2eAgNnSc4d1
escudo	escudo	k1gNnSc4
•	•	k?
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
drachma	drachma	k1gFnSc1
•	•	k?
Sanmarinská	Sanmarinský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Slovinský	slovinský	k2eAgInSc1d1
tolar	tolar	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
peseta	peseta	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
