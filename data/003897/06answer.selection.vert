<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
Skotska	Skotsko	k1gNnSc2	Skotsko
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
dalriadský	dalriadský	k2eAgMnSc1d1	dalriadský
král	král	k1gMnSc1	král
Kenneth	Kenneth	k1gMnSc1	Kenneth
Mac	Mac	kA	Mac
Alpin	alpinum	k1gNnPc2	alpinum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
843	[number]	k4	843
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Říši	říše	k1gFnSc4	říše
Piktů	Pikt	k1gInPc2	Pikt
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
království	království	k1gNnSc4	království
Alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
bezprostředního	bezprostřední	k2eAgMnSc2d1	bezprostřední
předchůdce	předchůdce	k1gMnSc2	předchůdce
Skotského	skotský	k2eAgNnSc2d1	skotské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
