<s>
Studiem	studio	k1gNnSc7	studio
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
asyriologie	asyriologie	k1gFnSc1	asyriologie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
první	první	k4xOgMnPc1	první
archeologové	archeolog	k1gMnPc1	archeolog
se	se	k3xPyFc4	se
zajímali	zajímat	k5eAaImAgMnP	zajímat
o	o	k7c6	o
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
zkoumané	zkoumaný	k2eAgInPc1d1	zkoumaný
materiály	materiál	k1gInPc1	materiál
dostupnější	dostupný	k2eAgInPc1d2	dostupnější
<g/>
.	.	kIx.	.
</s>
