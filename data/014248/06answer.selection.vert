<s>
Britská	britský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgNnSc2d1
a	a	k8xC
televizního	televizní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
The	The	kA
British	British	k2eAgFnSc1d1
Academy	Academa	k1gFnSc1
of	of	k7c2
Film	film	k1gInSc2
and	and	k8xC
Television	Television	k2eAgInPc2d1
Arts	Arts	k1gInPc2
<g/>
,	,	kIx,
BAFTA	BAFTA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
každoročně	každoročně	k6eAd1
udílí	udílet	k5eAaImIp3nS
ceny	cena	k1gFnPc4
za	za	k7c4
film	film	k1gInSc4
<g/>
,	,	kIx,
televizi	televize	k1gFnSc4
<g/>
,	,	kIx,
dětský	dětský	k2eAgInSc4d1
film	film	k1gInSc4
a	a	k8xC
média	médium	k1gNnPc4
<g/>
.	.	kIx.
</s>