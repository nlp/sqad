<s>
Britská	britský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgNnSc2d1
a	a	k8xC
televizního	televizní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
</s>
<s>
Britská	britský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgInSc2d1
a	a	k8xC
televizního	televizní	k2eAgInSc2d1
uměníZákladní	uměníZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1947	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Piccadilly	Piccadill	k1gInPc1
195	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
W1J	W1J	k1gFnSc1
9	#num#	k4
<g/>
LN	LN	kA
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc4
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
www.bafta.org	www.bafta.org	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgNnSc2d1
a	a	k8xC
televizního	televizní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
The	The	kA
British	British	k2eAgFnSc1d1
Academy	Academa	k1gFnSc1
of	of	k7c2
Film	film	k1gInSc2
and	and	k8xC
Television	Television	k2eAgInPc2d1
Arts	Arts	k1gInPc2
<g/>
,	,	kIx,
BAFTA	BAFTA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
každoročně	každoročně	k6eAd1
udílí	udílet	k5eAaImIp3nS
ceny	cena	k1gFnPc4
za	za	k7c4
film	film	k1gInSc4
<g/>
,	,	kIx,
televizi	televize	k1gFnSc4
<g/>
,	,	kIx,
dětský	dětský	k2eAgInSc4d1
film	film	k1gInSc4
a	a	k8xC
média	médium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
jako	jako	k8xC,k8xS
Britská	britský	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
se	se	k3xPyFc4
spojila	spojit	k5eAaPmAgNnP
se	s	k7c7
Společností	společnost	k1gFnSc7
televizních	televizní	k2eAgMnPc2d1
producentů	producent	k1gMnPc2
a	a	k8xC
režisérů	režisér	k1gMnPc2
a	a	k8xC
vytvořily	vytvořit	k5eAaPmAgFnP
společně	společně	k6eAd1
Společnost	společnost	k1gFnSc4
filmu	film	k1gInSc2
a	a	k8xC
televize	televize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Britskou	britský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
filmového	filmový	k2eAgNnSc2d1
a	a	k8xC
televizního	televizní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
BAFTY	BAFTY	kA
je	být	k5eAaImIp3nS
na	na	k7c4
Piccadilly	Piccadill	k1gInPc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
i	i	k9
menší	malý	k2eAgNnPc1d2
sídla	sídlo	k1gNnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
Skotsku	Skotsko	k1gNnSc6
<g/>
,	,	kIx,
Walesu	Wales	k1gInSc6
<g/>
,	,	kIx,
New	New	k1gFnSc4
Yorku	York	k1gInSc2
a	a	k8xC
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
ceny	cena	k1gFnSc2
BAFTA	BAFTA	kA
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
divadelní	divadelní	k2eAgFnSc2d1
masky	maska	k1gFnSc2
navržené	navržený	k2eAgFnSc2d1
americkým	americký	k2eAgMnSc7d1
sochařem	sochař	k1gMnSc7
Mici	Mic	k1gFnSc2
Cunliffem	Cunliff	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
British	British	k1gInSc1
Academy	Academ	k1gInPc1
of	of	k?
Film	film	k1gInSc1
and	and	k?
Television	Television	k1gInSc1
Arts	Arts	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
(	(	kIx(
<g/>
zjednodušené	zjednodušený	k2eAgFnSc6d1
<g/>
)	)	kIx)
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Britská	britský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgNnSc2d1
a	a	k8xC
televizního	televizní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2325	#num#	k4
0852	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98056521	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
148061978	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98056521	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
