<s>
Petřín	Petřín	k1gInSc1	Petřín
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Laurenziberg	Laurenziberg	k1gInSc1	Laurenziberg
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Petrin	Petrin	k1gInSc1	Petrin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
327	[number]	k4	327
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
kopec	kopec	k1gInSc4	kopec
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vrcholu	vrchol	k1gInSc6	vrchol
stojí	stát	k5eAaImIp3nS	stát
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
najdeme	najít	k5eAaPmIp1nP	najít
skály	skála	k1gFnPc4	skála
zejména	zejména	k9	zejména
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Petřín	Petřín	k1gInSc1	Petřín
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc6	staletí
několik	několik	k4yIc4	několik
názvů	název	k1gInPc2	název
<g/>
:	:	kIx,	:
Petřín	Petřín	k1gInSc1	Petřín
<g/>
,	,	kIx,	,
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
Vrch	vrch	k1gInSc1	vrch
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Laurenziberg	Laurenziberg	k1gInSc1	Laurenziberg
-	-	kIx~	-
z	z	k7c2	z
lat.	lat.	k?	lat.
Laurentius	Laurentius	k1gMnSc1	Laurentius
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
a	a	k8xC	a
Berg	Berg	k1gMnSc1	Berg
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kopec	kopec	k1gInSc1	kopec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posledně	posledně	k6eAd1	posledně
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
podle	podle	k7c2	podle
zasvěcení	zasvěcení	k1gNnSc2	zasvěcení
kostela	kostel	k1gInSc2	kostel
stojícího	stojící	k2eAgInSc2d1	stojící
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
temeni	temeno	k1gNnSc6	temeno
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Kosmas	Kosmas	k1gMnSc1	Kosmas
popisuje	popisovat	k5eAaImIp3nS	popisovat
Petřín	Petřín	k1gInSc4	Petřín
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
místo	místo	k1gNnSc4	místo
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
skal	skála	k1gFnPc2	skála
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
petra	petra	k6eAd1	petra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
údajně	údajně	k6eAd1	údajně
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Petřín	Petřín	k1gInSc4	Petřín
(	(	kIx(	(
<g/>
qui	qui	k?	qui
a	a	k8xC	a
petris	petris	k1gInSc1	petris
dicitur	dicitura	k1gFnPc2	dicitura
Petrin	Petrin	k1gInSc1	Petrin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
lámal	lámat	k5eAaImAgInS	lámat
kámen	kámen	k1gInSc1	kámen
–	–	k?	–
opuka	opuka	k1gFnSc1	opuka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
postavila	postavit	k5eAaPmAgFnS	postavit
řada	řada	k1gFnSc1	řada
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prvních	první	k4xOgInPc2	první
kostelů	kostel	k1gInPc2	kostel
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
nebo	nebo	k8xC	nebo
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historická	historický	k2eAgFnSc1d1	historická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
petřínských	petřínský	k2eAgInPc6d1	petřínský
lomech	lom	k1gInPc6	lom
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Mnicha	mnich	k1gMnSc2	mnich
sázavského	sázavský	k2eAgMnSc2d1	sázavský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kameny	kámen	k1gInPc1	kámen
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dal	dát	k5eAaPmAgMnS	dát
opat	opat	k1gMnSc1	opat
Silvestr	Silvestr	k1gMnSc1	Silvestr
vydláždit	vydláždit	k5eAaPmF	vydláždit
kostel	kostel	k1gInSc4	kostel
Sázavského	sázavský	k2eAgInSc2d1	sázavský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Petřín	Petřín	k1gInSc1	Petřín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
písemných	písemný	k2eAgFnPc6d1	písemná
památkách	památka	k1gFnPc6	památka
poprvé	poprvé	k6eAd1	poprvé
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
roku	rok	k1gInSc3	rok
1108	[number]	k4	1108
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
velmožského	velmožský	k2eAgInSc2d1	velmožský
rodu	rod	k1gInSc2	rod
Vršovců	Vršovec	k1gInPc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
Vršovci	Vršovec	k1gMnPc1	Vršovec
sídlili	sídlit	k5eAaImAgMnP	sídlit
v	v	k7c6	v
Libici	Libice	k1gFnSc6	Libice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
Přemyslovcům	Přemyslovec	k1gMnPc3	Přemyslovec
s	s	k7c7	s
povražděním	povraždění	k1gNnSc7	povraždění
části	část	k1gFnSc2	část
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
v	v	k7c6	v
Libici	Libice	k1gFnSc6	Libice
roku	rok	k1gInSc2	rok
995	[number]	k4	995
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
Vršovci	Vršovec	k1gMnPc1	Vršovec
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1108	[number]	k4	1108
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
hloupá	hloupý	k2eAgFnSc1d1	hloupá
hovada	hovado	k1gNnSc2	hovado
skoleni	skolen	k2eAgMnPc1d1	skolen
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Petříně	Petřín	k1gInSc6	Petřín
sťati	stnout	k5eAaPmNgMnP	stnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
popraviště	popraviště	k1gNnSc1	popraviště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
stávalo	stávat	k5eAaImAgNnS	stávat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
sloužilo	sloužit	k5eAaImAgNnS	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
trestání	trestání	k1gNnSc3	trestání
velkých	velký	k2eAgInPc2d1	velký
protistátních	protistátní	k2eAgInPc2d1	protistátní
přečinů	přečin	k1gInPc2	přečin
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
Hladové	hladový	k2eAgFnSc2d1	hladová
zdi	zeď	k1gFnSc2	zeď
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
popraviště	popraviště	k1gNnSc1	popraviště
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
na	na	k7c4	na
protější	protější	k2eAgInSc4d1	protější
vrch	vrch	k1gInSc4	vrch
Vítkov	Vítkov	k1gInSc1	Vítkov
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
na	na	k7c4	na
Petřín	Petřín	k1gInSc4	Petřín
-	-	kIx~	-
původní	původní	k2eAgFnSc1d1	původní
dráha	dráha	k1gFnSc1	dráha
postavená	postavený	k2eAgFnSc1d1	postavená
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozhlednou	rozhledna	k1gFnSc7	rozhledna
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
za	za	k7c2	za
Jubilejní	jubilejní	k2eAgFnSc2d1	jubilejní
výstavy	výstava	k1gFnSc2	výstava
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
Zrcadlové	zrcadlový	k2eAgNnSc4d1	zrcadlové
bludiště	bludiště	k1gNnSc4	bludiště
-	-	kIx~	-
původně	původně	k6eAd1	původně
pavilon	pavilon	k1gInSc1	pavilon
KČT	KČT	kA	KČT
na	na	k7c6	na
Jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
výstavě	výstava	k1gFnSc6	výstava
připomínající	připomínající	k2eAgFnSc4d1	připomínající
vyšehradskou	vyšehradský	k2eAgFnSc4d1	Vyšehradská
bránu	brána	k1gFnSc4	brána
Špička	Špička	k1gMnSc1	Špička
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
-	-	kIx~	-
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
románského	románský	k2eAgInSc2d1	románský
<g/>
,	,	kIx,	,
katedrální	katedrální	k2eAgFnSc2d1	katedrální
<g />
.	.	kIx.	.
</s>
<s>
kostel	kostel	k1gInSc1	kostel
Starokatolické	starokatolický	k2eAgFnSc2d1	Starokatolická
církve	církev	k1gFnSc2	církev
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
s	s	k7c7	s
kaplemi	kaple	k1gFnPc7	kaple
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
a	a	k8xC	a
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
stráni	stráň	k1gFnSc6	stráň
severně	severně	k6eAd1	severně
od	od	k7c2	od
rozhledny	rozhledna	k1gFnSc2	rozhledna
Hladová	hladový	k2eAgFnSc1d1	hladová
zeď	zeď	k1gFnSc1	zeď
-	-	kIx~	-
ohraničující	ohraničující	k2eAgFnSc4d1	ohraničující
Malou	malý	k2eAgFnSc4d1	malá
Stranu	strana	k1gFnSc4	strana
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
souběžnými	souběžný	k2eAgFnPc7d1	souběžná
Mariánskými	mariánský	k2eAgFnPc7d1	Mariánská
hradbami	hradba	k1gFnPc7	hradba
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c4	po
spoluzakladatele	spoluzakladatel	k1gMnPc4	spoluzakladatel
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
astronomu	astronom	k1gMnSc3	astronom
M.	M.	kA	M.
<g />
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Štefánikovi	Štefánik	k1gMnSc3	Štefánik
Sloupové	sloupový	k2eAgFnSc2d1	sloupová
sluneční	sluneční	k2eAgFnSc2d1	sluneční
hodiny	hodina	k1gFnSc2	hodina
-	-	kIx~	-
u	u	k7c2	u
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Restaurace	restaurace	k1gFnSc2	restaurace
Nebozízek	nebozízek	k1gInSc1	nebozízek
-	-	kIx~	-
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
zchátralá	zchátralý	k2eAgFnSc1d1	zchátralá
barokně-klasicistní	baroknělasicistní	k2eAgFnSc1d1	barokně-klasicistní
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
Strahovský	strahovský	k2eAgInSc1d1	strahovský
klášter	klášter	k1gInSc1	klášter
-	-	kIx~	-
nejstarší	starý	k2eAgInSc1d3	nejstarší
český	český	k2eAgInSc1d1	český
premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vstupu	vstup	k1gInSc2	vstup
z	z	k7c2	z
Petřína	Petřín	k1gInSc2	Petřín
na	na	k7c4	na
Hradčany	Hradčany	k1gInPc4	Hradčany
Kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
archanděla	archanděl	k1gMnSc2	archanděl
-	-	kIx~	-
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kostelík	kostelík	k1gInSc1	kostelík
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
přenesený	přenesený	k2eAgInSc1d1	přenesený
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
Letohrádek	letohrádek	k1gInSc1	letohrádek
Kinských	Kinských	k2eAgInSc1d1	Kinských
-	-	kIx~	-
klasicistní	klasicistní	k2eAgInSc1d1	klasicistní
letohrádek	letohrádek	k1gInSc1	letohrádek
na	na	k7c6	na
smíchovském	smíchovský	k2eAgNnSc6d1	Smíchovské
úpatí	úpatí	k1gNnSc6	úpatí
Petřína	Petřín	k1gInSc2	Petřín
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
Národopisného	národopisný	k2eAgNnSc2d1	Národopisné
muzea	muzeum	k1gNnSc2	muzeum
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
komunismu	komunismus	k1gInSc2	komunismus
-	-	kIx~	-
od	od	k7c2	od
Olbrama	Olbram	k1gMnSc2	Olbram
Zoubka	Zoubek	k1gMnSc2	Zoubek
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Vítězné	vítězný	k2eAgFnSc3d1	vítězná
ulici	ulice	k1gFnSc3	ulice
Sokol	Sokol	k1gInSc4	Sokol
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
-	-	kIx~	-
Sokolovna	sokolovna	k1gFnSc1	sokolovna
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
členy	člen	k1gInPc7	člen
patřili	patřit	k5eAaImAgMnP	patřit
českoslovenští	československý	k2eAgMnPc1d1	československý
prezidenti	prezident	k1gMnPc1	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
K.	K.	kA	K.
Gottwald	Gottwald	k1gMnSc1	Gottwald
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
stráně	stráň	k1gFnSc2	stráň
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
strahovská	strahovský	k2eAgFnSc1d1	Strahovská
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
pod	pod	k7c7	pod
Strahovským	strahovský	k2eAgInSc7d1	strahovský
klášterem	klášter	k1gInSc7	klášter
<g/>
,	,	kIx,	,
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
údolíčka	údolíčko	k1gNnSc2	údolíčko
mezi	mezi	k7c7	mezi
Petřínem	Petřín	k1gInSc7	Petřín
a	a	k8xC	a
Úvozem	úvoz	k1gInSc7	úvoz
Lobkovická	lobkovický	k2eAgFnSc1d1	Lobkovická
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
zahrada	zahrada	k1gFnSc1	zahrada
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
německého	německý	k2eAgNnSc2d1	německé
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
severní	severní	k2eAgFnSc1d1	severní
stráň	stráň	k1gFnSc1	stráň
Petřína	Petřín	k1gInSc2	Petřín
Schönbornská	Schönbornský	k2eAgFnSc1d1	Schönbornský
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
zahrada	zahrada	k1gFnSc1	zahrada
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Seminářská	seminářský	k2eAgFnSc1d1	Seminářská
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
sadově	sadově	k6eAd1	sadově
upravená	upravený	k2eAgFnSc1d1	upravená
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
východní	východní	k2eAgFnSc2d1	východní
stráně	stráň	k1gFnSc2	stráň
Petřína	Petřín	k1gInSc2	Petřín
Zahrada	zahrada	k1gFnSc1	zahrada
Nebozízek	nebozízek	k1gInSc1	nebozízek
-	-	kIx~	-
část	část	k1gFnSc1	část
východní	východní	k2eAgFnSc2d1	východní
stráně	stráň	k1gFnSc2	stráň
Petřína	Petřín	k1gInSc2	Petřín
mezi	mezi	k7c7	mezi
lanovou	lanový	k2eAgFnSc7d1	lanová
dráhou	dráha	k1gFnSc7	dráha
a	a	k8xC	a
Hladovou	hladový	k2eAgFnSc7d1	hladová
zdí	zeď	k1gFnSc7	zeď
Kinského	Kinského	k2eAgFnSc1d1	Kinského
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
nad	nad	k7c7	nad
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
letohrádkem	letohrádek	k1gInSc7	letohrádek
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
smíchovské	smíchovský	k2eAgFnSc6d1	Smíchovská
stráni	stráň	k1gFnSc6	stráň
Petřína	Petřín	k1gInSc2	Petřín
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Petřína	Petřín	k1gInSc2	Petřín
Park	park	k1gInSc1	park
u	u	k7c2	u
petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
Růžový	růžový	k2eAgInSc1d1	růžový
sad	sad	k1gInSc1	sad
-	-	kIx~	-
u	u	k7c2	u
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Petřína	Petřín	k1gInSc2	Petřín
Zahrada	zahrada	k1gFnSc1	zahrada
Květnice	Květnice	k1gFnSc1	Květnice
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
květinová	květinový	k2eAgFnSc1d1	květinová
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
sevření	sevření	k1gNnSc6	sevření
hradeb	hradba	k1gFnPc2	hradba
poblíž	poblíž	k7c2	poblíž
Růžového	růžový	k2eAgInSc2d1	růžový
sadu	sad	k1gInSc2	sad
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Petřín	Petřín	k1gInSc1	Petřín
-	-	kIx~	-
Petřínské	petřínský	k2eAgFnPc1d1	Petřínská
skalky	skalka	k1gFnPc1	skalka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lemují	lemovat	k5eAaImIp3nP	lemovat
celé	celý	k2eAgNnSc4d1	celé
temeno	temeno	k1gNnSc4	temeno
kopce	kopec	k1gInSc2	kopec
Pramen	pramen	k1gInSc1	pramen
Petřínka	Petřínka	k1gFnSc1	Petřínka
-	-	kIx~	-
v	v	k7c6	v
Seminářské	seminářský	k2eAgFnSc6d1	Seminářská
zahradě	zahrada	k1gFnSc6	zahrada
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
hlavní	hlavní	k2eAgFnPc1d1	hlavní
parkové	parkový	k2eAgFnPc1d1	parková
cesty	cesta	k1gFnPc1	cesta
nedaleko	nedaleko	k7c2	nedaleko
restaurace	restaurace	k1gFnSc2	restaurace
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
terasy	terasa	k1gFnSc2	terasa
Pod	pod	k7c7	pod
Petřínem	Petřín	k1gInSc7	Petřín
-	-	kIx~	-
původně	původně	k6eAd1	původně
pítko	pítko	k1gNnSc1	pítko
<g/>
,	,	kIx,	,
s	s	k7c7	s
kamenicky	kamenicky	k6eAd1	kamenicky
řešeným	řešený	k2eAgNnSc7d1	řešené
řečištěm	řečiště	k1gNnSc7	řečiště
v	v	k7c6	v
Hellichově	Hellichův	k2eAgFnSc6d1	Hellichova
ulici	ulice	k1gFnSc6	ulice
Ve	v	k7c6	v
Strahovské	strahovský	k2eAgFnSc6d1	Strahovská
zahradě	zahrada	k1gFnSc6	zahrada
(	(	kIx(	(
<g/>
Kaštanka	kaštanka	k1gFnSc1	kaštanka
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
zalesněné	zalesněný	k2eAgFnSc6d1	zalesněná
stráni	stráň	k1gFnSc6	stráň
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Strahovského	strahovský	k2eAgInSc2d1	strahovský
kláštera	klášter	k1gInSc2	klášter
Pramen	pramen	k1gInSc1	pramen
sira	sir	k1gMnSc2	sir
Nicolase	Nicolasa	k1gFnSc6	Nicolasa
Wintona	Winton	k1gMnSc4	Winton
-	-	kIx~	-
obnovený	obnovený	k2eAgInSc1d1	obnovený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
u	u	k7c2	u
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
<g />
.	.	kIx.	.
</s>
<s>
cesty	cesta	k1gFnPc1	cesta
také	také	k9	také
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Velké	velký	k2eAgFnSc2d1	velká
strahovské	strahovský	k2eAgFnSc2d1	Strahovská
zahrady	zahrada	k1gFnSc2	zahrada
U	u	k7c2	u
Památného	památný	k2eAgInSc2d1	památný
stromu	strom	k1gInSc2	strom
(	(	kIx(	(
<g/>
jasanu	jasan	k1gInSc2	jasan
<g/>
)	)	kIx)	)
-	-	kIx~	-
nedaleko	nedaleko	k7c2	nedaleko
Wintonova	Wintonův	k2eAgInSc2d1	Wintonův
pramenu	pramen	k1gInSc2	pramen
Kinská	Kinská	k1gFnSc1	Kinská
<g/>
,	,	kIx,	,
U	u	k7c2	u
jezírka	jezírko	k1gNnSc2	jezírko
(	(	kIx(	(
<g/>
též	též	k9	též
Vodní	vodní	k2eAgInPc1d1	vodní
schody	schod	k1gInPc1	schod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Kaštankou	kaštanka	k1gFnSc7	kaštanka
-	-	kIx~	-
prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
studánky	studánka	k1gFnPc1	studánka
v	v	k7c6	v
Kinského	Kinského	k2eAgFnSc6d1	Kinského
zahradě	zahrada	k1gFnSc6	zahrada
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
pod	pod	k7c7	pod
Petřínem	Petřín	k1gInSc7	Petřín
<g/>
.	.	kIx.	.
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
stojící	stojící	k2eAgFnSc1d1	stojící
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	s	k7c7	s
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
drží	držet	k5eAaImIp3nS	držet
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
zídku	zídka	k1gFnSc4	zídka
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
sešitem	sešit	k1gInSc7	sešit
a	a	k8xC	a
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
kytici	kytice	k1gFnSc4	kytice
šeříku	šeřík	k1gInSc2	šeřík
<g/>
.	.	kIx.	.
</s>
<s>
Podstavec	podstavec	k1gInSc1	podstavec
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
leštěné	leštěný	k2eAgFnSc2d1	leštěná
žuly	žula	k1gFnSc2	žula
<g/>
.	.	kIx.	.
</s>
<s>
Sochu	socha	k1gFnSc4	socha
vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
,	,	kIx,	,
stojících	stojící	k2eAgInPc2d1	stojící
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Petřína	Petřín	k1gInSc2	Petřín
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
stojící	stojící	k2eAgFnSc4d1	stojící
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
drží	držet	k5eAaImIp3nS	držet
klobouk	klobouk	k1gInSc1	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
bronzové	bronzový	k2eAgFnSc2d1	bronzová
sochy	socha	k1gFnSc2	socha
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Simota	Simot	k1gMnSc2	Simot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vyhotovil	vyhotovit	k5eAaPmAgInS	vyhotovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Josef	Josef	k1gMnSc1	Josef
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
sedící	sedící	k2eAgFnSc4d1	sedící
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
okřídlenými	okřídlený	k2eAgMnPc7d1	okřídlený
génii	génius	k1gMnPc7	génius
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
letec	letec	k1gMnSc1	letec
<g/>
.	.	kIx.	.
</s>
<s>
Stojící	stojící	k2eAgFnSc1d1	stojící
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
kombinéze	kombinéza	k1gFnSc6	kombinéza
<g/>
,	,	kIx,	,
držící	držící	k2eAgFnSc6d1	držící
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
písemnosti	písemnost	k1gFnSc2	písemnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
soše	socha	k1gFnSc6	socha
určené	určený	k2eAgFnSc6d1	určená
pro	pro	k7c4	pro
Bratislavu	Bratislava	k1gFnSc4	Bratislava
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
roztavena	roztavit	k5eAaPmNgFnS	roztavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
před	před	k7c7	před
Štefánikovou	Štefánikův	k2eAgFnSc7d1	Štefánikova
hvězdárnou	hvězdárna	k1gFnSc7	hvězdárna
odhalil	odhalit	k5eAaPmAgInS	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nový	nový	k2eAgInSc4d1	nový
odlitek	odlitek	k1gInSc4	odlitek
podle	podle	k7c2	podle
třetinového	třetinový	k2eAgInSc2d1	třetinový
modelu	model	k1gInSc2	model
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Nazývána	nazýván	k2eAgFnSc1d1	nazývána
U	u	k7c2	u
žabiček	žabička	k1gFnPc2	žabička
(	(	kIx(	(
<g/>
Hrající	hrající	k2eAgFnSc1d1	hrající
si	se	k3xPyFc3	se
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
též	též	k9	též
Masarykovi	Masarykův	k2eAgMnPc1d1	Masarykův
vnuci	vnuk	k1gMnPc1	vnuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukazující	ukazující	k2eAgFnSc4d1	ukazující
dvojici	dvojice	k1gFnSc4	dvojice
chlapců	chlapec	k1gMnPc2	chlapec
se	s	k7c7	s
šesti	šest	k4xCc7	šest
žabkami	žabka	k1gFnPc7	žabka
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
ještěrkami	ještěrka	k1gFnPc7	ještěrka
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc1d1	hrající
si	se	k3xPyFc3	se
s	s	k7c7	s
rybou	ryba	k1gFnSc7	ryba
u	u	k7c2	u
fontány	fontána	k1gFnSc2	fontána
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
pro	pro	k7c4	pro
sošky	soška	k1gFnPc4	soška
byli	být	k5eAaImAgMnP	být
vnuci	vnuk	k1gMnPc1	vnuk
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
synové	syn	k1gMnPc1	syn
Olgy	Olga	k1gFnSc2	Olga
Masarykové	Masarykové	k2eAgMnPc1d1	Masarykové
Leonard	Leonard	k1gMnSc1	Leonard
a	a	k8xC	a
Herbert	Herbert	k1gMnSc1	Herbert
Revilliodovi	Revilliodův	k2eAgMnPc1d1	Revilliodův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c2	za
války	válka	k1gFnSc2	válka
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
odboji	odboj	k1gInSc6	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
stát	stát	k5eAaImF	stát
na	na	k7c6	na
Lánském	lánský	k2eAgInSc6d1	lánský
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
Karel	Karel	k1gMnSc1	Karel
Dvořák	Dvořák	k1gMnSc1	Dvořák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Socha	Socha	k1gMnSc1	Socha
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
sedící	sedící	k2eAgFnSc4d1	sedící
postavu	postava	k1gFnSc4	postava
v	v	k7c6	v
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
šatech	šat	k1gInPc6	šat
a	a	k8xC	a
s	s	k7c7	s
kyticí	kytice	k1gFnSc7	kytice
na	na	k7c6	na
klíně	klín	k1gInSc6	klín
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Štursa	Šturs	k1gMnSc2	Šturs
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vyhotovil	vyhotovit	k5eAaPmAgInS	vyhotovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sochou	socha	k1gFnSc7	socha
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
urna	urna	k1gFnSc1	urna
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
této	tento	k3xDgFnSc2	tento
herečky	herečka	k1gFnSc2	herečka
–	–	k?	–
členky	členka	k1gFnSc2	členka
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sochu	socha	k1gFnSc4	socha
znázorňující	znázorňující	k2eAgMnSc1d1	znázorňující
lachtana	lachtan	k1gMnSc4	lachtan
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Jan	Jan	k1gMnSc1	Jan
Lauda	Lauda	k1gMnSc1	Lauda
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Laub	Laub	k1gMnSc1	Laub
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
virtuos	virtuos	k1gMnSc1	virtuos
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
<g/>
.	.	kIx.	.
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
stojící	stojící	k2eAgFnSc4d1	stojící
postavu	postava	k1gFnSc4	postava
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Jan	Jan	k1gMnSc1	Jan
Kodet	Kodet	k1gMnSc1	Kodet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soše	socha	k1gFnSc6	socha
je	být	k5eAaImIp3nS	být
urna	urna	k1gFnSc1	urna
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
východočeské	východočeský	k2eAgFnSc6d1	Východočeská
Skutči	Skuteč	k1gFnSc6	Skuteč
<g/>
.	.	kIx.	.
</s>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
autora	autor	k1gMnSc2	autor
Josefa	Josef	k1gMnSc2	Josef
Mařatky	Mařatka	k1gFnSc2	Mařatka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Olbram	Olbram	k1gInSc1	Olbram
Zoubek	Zoubek	k1gMnSc1	Zoubek
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
Petřína	Petřín	k1gInSc2	Petřín
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
komunismu	komunismus	k1gInSc3	komunismus
(	(	kIx(	(
<g/>
zločinů	zločin	k1gInPc2	zločin
komunizmu	komunizmus	k1gInSc2	komunizmus
<g/>
)	)	kIx)	)
na	na	k7c6	na
Újezdě	Újezd	k1gInSc6	Újezd
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
k	k	k7c3	k
pomníku	pomník	k1gInSc3	pomník
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
mramorového	mramorový	k2eAgInSc2d1	mramorový
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc1d1	stojící
poblíž	poblíž	k6eAd1	poblíž
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Petříně	Petřín	k1gInSc6	Petřín
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
16801	[number]	k4	16801
<g/>
)	)	kIx)	)
Petřínpragensis	Petřínpragensis	k1gFnSc2	Petřínpragensis
objevená	objevený	k2eAgFnSc1d1	objevená
Petrem	Petr	k1gMnSc7	Petr
Pravcem	Pravec	k1gMnSc7	Pravec
v	v	k7c6	v
Ondřejovně	Ondřejovna	k1gFnSc6	Ondřejovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Planetku	planetka	k1gFnSc4	planetka
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
nazvat	nazvat	k5eAaPmF	nazvat
jednoduše	jednoduše	k6eAd1	jednoduše
Petřín	Petřín	k1gInSc4	Petřín
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pletla	plést	k5eAaImAgFnS	plést
s	s	k7c7	s
již	již	k6eAd1	již
existující	existující	k2eAgFnSc7d1	existující
planetkou	planetka	k1gFnSc7	planetka
(	(	kIx(	(
<g/>
482	[number]	k4	482
<g/>
)	)	kIx)	)
Petrina	Petrina	k1gFnSc1	Petrina
objevené	objevený	k2eAgNnSc1d1	objevené
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
planetek	planetka	k1gFnPc2	planetka
toto	tento	k3xDgNnSc1	tento
nepřipouštějí	připouštět	k5eNaImIp3nP	připouštět
<g/>
.	.	kIx.	.
</s>
