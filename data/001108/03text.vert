<s>
Kabát	kabát	k1gInSc1	kabát
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Teplic	Teplice	k1gFnPc2	Teplice
existující	existující	k2eAgFnSc2d1	existující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
a	a	k8xC	a
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
aktivně	aktivně	k6eAd1	aktivně
působící	působící	k2eAgInPc1d1	působící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
<g/>
letou	letý	k2eAgFnSc4d1	letá
kariéru	kariéra	k1gFnSc4	kariéra
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
11	[number]	k4	11
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovšem	ovšem	k9	ovšem
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
desátého	desátý	k4xOgInSc2	desátý
slavíka	slavík	k1gInSc2	slavík
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
jejich	jejich	k3xOp3gNnSc2	jejich
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
výroční	výroční	k2eAgNnSc1d1	výroční
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Vypichu	vypich	k1gInSc6	vypich
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
přes	přes	k7c4	přes
70	[number]	k4	70
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
aktivně	aktivně	k6eAd1	aktivně
působící	působící	k2eAgFnSc4d1	působící
českou	český	k2eAgFnSc4d1	Česká
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
hraje	hrát	k5eAaImIp3nS	hrát
bez	bez	k7c2	bez
personální	personální	k2eAgFnSc2d1	personální
změny	změna	k1gFnSc2	změna
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepřerušila	přerušit	k5eNaPmAgFnS	přerušit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
Kabát	kabát	k1gInSc1	kabát
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
baskytarista	baskytarista	k1gMnSc1	baskytarista
Milan	Milan	k1gMnSc1	Milan
Špalek	špalek	k1gInSc4	špalek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původně	původně	k6eAd1	původně
thrashmetalové	thrashmetalový	k2eAgFnSc3d1	thrashmetalová
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Krulich	Krulich	k1gMnSc1	Krulich
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Radek	Radek	k1gMnSc1	Radek
Hurčík	Hurčík	k1gMnSc1	Hurčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byli	být	k5eAaImAgMnP	být
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
odvedeni	odveden	k2eAgMnPc1d1	odveden
na	na	k7c4	na
tehdy	tehdy	k6eAd1	tehdy
povinnou	povinný	k2eAgFnSc4d1	povinná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
a	a	k8xC	a
Kabát	kabát	k1gInSc4	kabát
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
zastavil	zastavit	k5eAaPmAgInS	zastavit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
skupiny	skupina	k1gFnSc2	skupina
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zlomu	zlom	k1gInSc3	zlom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydali	vydat	k5eAaPmAgMnP	vydat
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
na	na	k7c6	na
projektových	projektový	k2eAgNnPc6d1	projektové
albech	album	k1gNnPc6	album
Rockmapa	Rockmapa	k1gFnSc1	Rockmapa
a	a	k8xC	a
Ultrametal	Ultrametal	k1gFnSc1	Ultrametal
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Kabáti	Kabát	k1gMnPc1	Kabát
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
s	s	k7c7	s
kytaristy	kytarista	k1gMnPc7	kytarista
Jiřím	Jiří	k1gMnSc7	Jiří
Buškem	Bušek	k1gMnSc7	Bušek
a	a	k8xC	a
René	René	k1gFnSc7	René
Horňákem	Horňák	k1gMnSc7	Horňák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
kapelu	kapela	k1gFnSc4	kapela
chvíli	chvíle	k1gFnSc4	chvíle
poté	poté	k6eAd1	poté
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nahrazeni	nahradit	k5eAaPmNgMnP	nahradit
vracejícím	vracející	k2eAgFnPc3d1	vracející
se	se	k3xPyFc4	se
Tomášem	Tomáš	k1gMnSc7	Tomáš
Krulichem	Krulich	k1gMnSc7	Krulich
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
kytaristou	kytarista	k1gMnSc7	kytarista
Otou	Ota	k1gMnSc7	Ota
Váňou	Váňa	k1gMnSc7	Váňa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
obsadila	obsadit	k5eAaPmAgFnS	obsadit
skupina	skupina	k1gFnSc1	skupina
třetí	třetí	k4xOgFnSc2	třetí
místo	místo	k7c2	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Koleda	koleda	k1gFnSc1	koleda
<g/>
,	,	kIx,	,
koleda	koleda	k1gFnSc1	koleda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
a	a	k8xC	a
1990	[number]	k4	1990
natočila	natočit	k5eAaBmAgFnS	natočit
skupina	skupina	k1gFnSc1	skupina
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
demo	demo	k2eAgFnPc4d1	demo
s	s	k7c7	s
názvem	název	k1gInSc7	název
Orgazmus	orgazmus	k1gInSc1	orgazmus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
a	a	k8xC	a
šířilo	šířit	k5eAaImAgNnS	šířit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
posluchači	posluchač	k1gMnPc7	posluchač
na	na	k7c6	na
audiokazetách	audiokazeta	k1gFnPc6	audiokazeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
poté	poté	k6eAd1	poté
Kabáti	Kabát	k1gMnPc1	Kabát
uvedli	uvést	k5eAaPmAgMnP	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
pod	pod	k7c7	pod
společností	společnost	k1gFnSc7	společnost
Monitor	monitor	k1gInSc1	monitor
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vydání	vydání	k1gNnSc6	vydání
desky	deska	k1gFnSc2	deska
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
zásluhu	zásluha	k1gFnSc4	zásluha
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kapele	kapela	k1gFnSc3	kapela
umožnil	umožnit	k5eAaPmAgInS	umožnit
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
některých	některý	k3yIgFnPc2	některý
písniček	písnička	k1gFnPc2	písnička
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Rockmapa	Rockmapa	k1gFnSc1	Rockmapa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
jich	on	k3xPp3gFnPc2	on
všimli	všimnout	k5eAaPmAgMnP	všimnout
právě	právě	k6eAd1	právě
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Monitor	monitor	k1gInSc1	monitor
a	a	k8xC	a
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
kapele	kapela	k1gFnSc3	kapela
Kabát	kabát	k1gInSc1	kabát
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
prodalo	prodat	k5eAaPmAgNnS	prodat
69	[number]	k4	69
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
úspěchu	úspěch	k1gInSc3	úspěch
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ceny	cena	k1gFnPc1	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
obsadila	obsadit	k5eAaPmAgFnS	obsadit
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
soutěži	soutěž	k1gFnSc6	soutěž
talentů	talent	k1gInPc2	talent
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
první	první	k4xOgFnSc2	první
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
managementem	management	k1gInSc7	management
skupiny	skupina	k1gFnSc2	skupina
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Power	Power	k1gMnSc1	Power
Voice	Voice	k1gMnSc1	Voice
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
Kabát	kabát	k1gInSc1	kabát
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
Kabáti	Kabát	k1gMnPc1	Kabát
vydali	vydat	k5eAaPmAgMnP	vydat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
album	album	k1gNnSc4	album
Živě	živě	k6eAd1	živě
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Břeclavi	Břeclav	k1gFnSc6	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
"	"	kIx"	"
<g/>
živáku	živák	k1gInSc6	živák
<g/>
"	"	kIx"	"
kromě	kromě	k7c2	kromě
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
studiové	studiový	k2eAgFnSc2d1	studiová
desky	deska	k1gFnSc2	deska
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
také	také	k9	také
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Žízeň	žízeň	k1gFnSc1	žízeň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
hitem	hit	k1gInSc7	hit
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Děvky	děvka	k1gFnSc2	děvka
ty	ten	k3xDgMnPc4	ten
to	ten	k3xDgNnSc1	ten
znaj	znaj	k?	znaj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
měla	mít	k5eAaImAgFnS	mít
skupina	skupina	k1gFnSc1	skupina
natočeno	natočit	k5eAaBmNgNnS	natočit
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc4	obal
desky	deska	k1gFnSc2	deska
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
malíř	malíř	k1gMnSc1	malíř
Petr	Petr	k1gMnSc1	Petr
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
Kabáti	Kabát	k1gMnPc1	Kabát
následně	následně	k6eAd1	následně
věnovali	věnovat	k5eAaPmAgMnP	věnovat
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
Pivrnec	Pivrnec	k1gInSc4	Pivrnec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
alba	alba	k1gFnSc1	alba
skupina	skupina	k1gFnSc1	skupina
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc3	první
"	"	kIx"	"
<g/>
velké	velký	k2eAgNnSc4d1	velké
<g/>
"	"	kIx"	"
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
obrovský	obrovský	k2eAgInSc4d1	obrovský
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyprodat	vyprodat	k5eAaPmF	vyprodat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
vystoupením	vystoupení	k1gNnSc7	vystoupení
a	a	k8xC	a
současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
akcí	akce	k1gFnSc7	akce
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
nakonec	nakonec	k6eAd1	nakonec
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
85	[number]	k4	85
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
překonat	překonat	k5eAaPmF	překonat
počet	počet	k1gInSc4	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
předchozí	předchozí	k2eAgFnSc2d1	předchozí
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
třetí	třetí	k4xOgFnSc4	třetí
řadovou	řadový	k2eAgFnSc4d1	řadová
desku	deska	k1gFnSc4	deska
Colorado	Colorado	k1gNnSc4	Colorado
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
25	[number]	k4	25
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
také	také	k9	také
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
50	[number]	k4	50
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
ocenění	ocenění	k1gNnPc2	ocenění
Kabát	kabát	k1gInSc1	kabát
přebral	přebrat	k5eAaPmAgInS	přebrat
na	na	k7c6	na
vyprodaném	vyprodaný	k2eAgInSc6d1	vyprodaný
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
deskou	deska	k1gFnSc7	deska
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
zároveň	zároveň	k6eAd1	zároveň
dostala	dostat	k5eAaPmAgFnS	dostat
také	také	k9	také
do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
rádií	rádio	k1gNnPc2	rádio
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
paradoxně	paradoxně	k6eAd1	paradoxně
s	s	k7c7	s
bluegrassovou	bluegrassový	k2eAgFnSc7d1	bluegrassová
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
zamýšlena	zamýšlet	k5eAaImNgFnS	zamýšlet
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
vtip	vtip	k1gInSc4	vtip
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
studiovou	studiový	k2eAgFnSc7d1	studiová
nahrávkou	nahrávka	k1gFnSc7	nahrávka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
kolem	kolem	k7c2	kolem
96	[number]	k4	96
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
ti	ten	k3xDgMnPc1	ten
šlapou	šlapat	k5eAaImIp3nP	šlapat
Kabáti	Kabát	k1gMnPc1	Kabát
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
hymnou	hymna	k1gFnSc7	hymna
českého	český	k2eAgInSc2d1	český
hokejového	hokejový	k2eAgInSc2d1	hokejový
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
album	album	k1gNnSc4	album
Colorado	Colorado	k1gNnSc1	Colorado
od	od	k7c2	od
časopisu	časopis	k1gInSc2	časopis
Rock	rock	k1gInSc1	rock
Report	report	k1gInSc1	report
ceny	cena	k1gFnSc2	cena
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
a	a	k8xC	a
deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
přebrala	přebrat	k5eAaPmAgFnS	přebrat
také	také	k9	také
od	od	k7c2	od
časopisu	časopis	k1gInSc2	časopis
Bang	Banga	k1gFnPc2	Banga
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
a	a	k8xC	a
radost	radost	k1gFnSc4	radost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Hacienda	hacienda	k1gFnSc1	hacienda
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Země	země	k1gFnSc1	země
plná	plný	k2eAgFnSc1d1	plná
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
kapely	kapela	k1gFnSc2	kapela
znít	znít	k5eAaImF	znít
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
je	být	k5eAaImIp3nS	být
negr	negr	k1gInSc4	negr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozporuplné	rozporuplný	k2eAgNnSc4d1	rozporuplné
přijetí	přijetí	k1gNnSc4	přijetí
fanoušky	fanoušek	k1gMnPc7	fanoušek
se	se	k3xPyFc4	se
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
82	[number]	k4	82
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Vojtek	Vojtek	k1gMnSc1	Vojtek
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gNnSc1	jeho
nejméně	málo	k6eAd3	málo
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
točila	točit	k5eAaImAgFnS	točit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Kabát	kabát	k1gInSc1	kabát
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
sportovních	sportovní	k2eAgFnPc6d1	sportovní
halách	hala	k1gFnPc6	hala
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc6d1	kulturní
domech	dům	k1gInPc6	dům
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Země	země	k1gFnSc1	země
plná	plný	k2eAgFnSc1d1	plná
trpaslíků	trpaslík	k1gMnPc2	trpaslík
začala	začít	k5eAaPmAgFnS	začít
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Propast	propast	k1gFnSc4	propast
nahrávat	nahrávat	k5eAaImF	nahrávat
další	další	k2eAgNnSc4d1	další
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čert	čert	k1gMnSc1	čert
na	na	k7c6	na
koze	koza	k1gFnSc6	koza
jel	jet	k5eAaImAgMnS	jet
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Martin	Martin	k1gMnSc1	Martin
Lepka	Lepka	k1gMnSc1	Lepka
<g/>
,	,	kIx,	,
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
čerta	čert	k1gMnSc4	čert
sedícího	sedící	k2eAgMnSc4d1	sedící
na	na	k7c6	na
ženském	ženský	k2eAgNnSc6d1	ženské
poprsí	poprsí	k1gNnSc6	poprsí
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
novinářů	novinář	k1gMnPc2	novinář
podobá	podobat	k5eAaImIp3nS	podobat
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václavu	Václav	k1gMnSc3	Václav
Klausovi	Klaus	k1gMnSc3	Klaus
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Kabátu	kabát	k1gInSc2	kabát
Milan	Milan	k1gMnSc1	Milan
Špalek	špalek	k1gInSc1	špalek
ovšem	ovšem	k9	ovšem
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
čertíka	čertík	k1gMnSc4	čertík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
Klausovi	Klaus	k1gMnSc3	Klaus
jen	jen	k9	jen
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
prodalo	prodat	k5eAaPmAgNnS	prodat
72	[number]	k4	72
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
umístila	umístit	k5eAaPmAgFnS	umístit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
desky	deska	k1gFnSc2	deska
navíc	navíc	k6eAd1	navíc
skupina	skupina	k1gFnSc1	skupina
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
hale	hala	k1gFnSc6	hala
Rondo	rondo	k1gNnSc1	rondo
byl	být	k5eAaImAgInS	být
natáčen	natáčen	k2eAgInSc1d1	natáčen
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vysílán	vysílat	k5eAaImNgInS	vysílat
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
slavíku	slavík	k1gInSc6	slavík
se	se	k3xPyFc4	se
Kabáti	Kabát	k1gMnPc1	Kabát
propadli	propadnout	k5eAaPmAgMnP	propadnout
o	o	k7c4	o
pět	pět	k4xCc4	pět
pozic	pozice	k1gFnPc2	pozice
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
album	album	k1gNnSc1	album
MegaHu	MegaHus	k1gInSc2	MegaHus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Sono	Sono	k6eAd1	Sono
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přebalu	přebal	k1gInSc6	přebal
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
fotka	fotka	k1gFnSc1	fotka
mořícího	mořící	k2eAgMnSc2d1	mořící
bezdomovce	bezdomovec	k1gMnSc2	bezdomovec
z	z	k7c2	z
Košťan	Košťan	k1gMnSc1	Košťan
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
kolovala	kolovat	k5eAaImAgFnS	kolovat
fáma	fáma	k1gFnSc1	fáma
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
penis	penis	k1gInSc1	penis
gigantických	gigantický	k2eAgInPc2d1	gigantický
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1999	[number]	k4	1999
odjela	odjet	k5eAaPmAgFnS	odjet
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
po	po	k7c6	po
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc4	turné
měl	mít	k5eAaImAgInS	mít
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
Radek	Radek	k1gMnSc1	Radek
Havlíček	Havlíček	k1gMnSc1	Havlíček
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
"	"	kIx"	"
<g/>
Panki	Panki	k1gNnSc1	Panki
<g/>
"	"	kIx"	"
Volek	Volek	k1gMnSc1	Volek
z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
Pink	pink	k2eAgFnPc2d1	pink
Panther	Panthra	k1gFnPc2	Panthra
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
MegaHu	MegaHu	k6eAd1	MegaHu
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
prodalo	prodat	k5eAaPmAgNnS	prodat
50	[number]	k4	50
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
umístila	umístit	k5eAaPmAgFnS	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
také	také	k9	také
kapela	kapela	k1gFnSc1	kapela
přešla	přejít	k5eAaPmAgFnS	přejít
od	od	k7c2	od
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
managementu	management	k1gInSc2	management
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Power	Power	k1gMnSc1	Power
Voice	Voice	k1gMnSc1	Voice
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Pink	pink	k2eAgFnPc2d1	pink
Panther	Panthra	k1gFnPc2	Panthra
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Kabáti	Kabát	k1gMnPc1	Kabát
změnili	změnit	k5eAaPmAgMnP	změnit
strategii	strategie	k1gFnSc4	strategie
koncertování	koncertování	k1gNnSc2	koncertování
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
klubech	klub	k1gInPc6	klub
začali	začít	k5eAaPmAgMnP	začít
upřednostňovat	upřednostňovat	k5eAaImF	upřednostňovat
méně	málo	k6eAd2	málo
koncertování	koncertování	k1gNnSc4	koncertování
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
prostorách	prostora	k1gFnPc6	prostora
s	s	k7c7	s
profesionální	profesionální	k2eAgFnSc7d1	profesionální
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
a	a	k8xC	a
světelnou	světelný	k2eAgFnSc7d1	světelná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2000	[number]	k4	2000
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
desku	deska	k1gFnSc4	deska
byly	být	k5eAaImAgInP	být
vyvěšeny	vyvěšen	k2eAgInPc1d1	vyvěšen
billboardy	billboard	k1gInPc1	billboard
se	s	k7c7	s
slogany	slogan	k1gInPc7	slogan
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nelíbily	líbit	k5eNaImAgFnP	líbit
skupině	skupina	k1gFnSc6	skupina
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
podali	podat	k5eAaPmAgMnP	podat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
stížnost	stížnost	k1gFnSc4	stížnost
k	k	k7c3	k
Radě	rada	k1gFnSc3	rada
pro	pro	k7c4	pro
reklamu	reklama	k1gFnSc4	reklama
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
urážku	urážka	k1gFnSc4	urážka
všech	všecek	k3xTgMnPc2	všecek
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
židovských	židovský	k2eAgFnPc2d1	židovská
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
muslimů	muslim	k1gMnPc2	muslim
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Žádost	žádost	k1gFnSc1	žádost
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
urážku	urážka	k1gFnSc4	urážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
parodii	parodie	k1gFnSc6	parodie
na	na	k7c4	na
slogany	slogan	k1gInPc4	slogan
Go	Go	k1gFnSc2	Go
Ježíšku	Ježíšek	k1gMnSc3	Ježíšek
go	go	k?	go
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Eurotel	Eurotel	kA	Eurotel
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
záležitost	záležitost	k1gFnSc1	záležitost
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
televizních	televizní	k2eAgFnPc2d1	televizní
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
udělala	udělat	k5eAaPmAgFnS	udělat
tak	tak	k6eAd1	tak
skupině	skupina	k1gFnSc3	skupina
reklamu	reklama	k1gFnSc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
desky	deska	k1gFnSc2	deska
kapela	kapela	k1gFnSc1	kapela
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
plně	plně	k6eAd1	plně
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Pink	pink	k2eAgFnSc7d1	pink
Panther	Panthra	k1gFnPc2	Panthra
Agency	Agenca	k1gFnPc4	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
Kabáti	Kabát	k1gMnPc1	Kabát
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
sportovních	sportovní	k2eAgFnPc6d1	sportovní
halách	hala	k1gFnPc6	hala
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
byla	být	k5eAaImAgFnS	být
kolem	kolem	k7c2	kolem
30	[number]	k4	30
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vydán	vydat	k5eAaPmNgInS	vydat
jako	jako	k8xC	jako
první	první	k4xOgFnPc4	první
DVD	DVD	kA	DVD
kapely	kapela	k1gFnPc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
prodaných	prodaný	k2eAgNnPc2d1	prodané
alb	album	k1gNnPc2	album
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
ke	k	k7c3	k
48	[number]	k4	48
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
kompilaci	kompilace	k1gFnSc6	kompilace
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
2	[number]	k4	2
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
byl	být	k5eAaImAgInS	být
výčet	výčet	k1gInSc1	výčet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
plus	plus	k1gInSc1	plus
jako	jako	k8xS	jako
bonus	bonus	k1gInSc1	bonus
předělaná	předělaný	k2eAgFnSc1d1	předělaná
verze	verze	k1gFnSc1	verze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
<g/>
"	"	kIx"	"
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Pohoda	pohoda	k1gFnSc1	pohoda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
kompilaci	kompilace	k1gFnSc4	kompilace
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
manažera	manažer	k1gMnSc2	manažer
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
nahrávání	nahrávání	k1gNnSc4	nahrávání
provázely	provázet	k5eAaImAgInP	provázet
projevy	projev	k1gInPc1	projev
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
textaře	textař	k1gMnSc2	textař
Špalka	Špalek	k1gMnSc2	Špalek
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nelíbila	líbit	k5eNaImAgFnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
CD	CD	kA	CD
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
zvukový	zvukový	k2eAgInSc4d1	zvukový
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
albu	album	k1gNnSc3	album
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
<g/>
.	.	kIx.	.
</s>
<s>
Kompilace	kompilace	k1gFnSc1	kompilace
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
130	[number]	k4	130
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
šestiplatinovou	šestiplatinový	k2eAgFnSc4d1	šestiplatinový
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
Kabáti	Kabát	k1gMnPc1	Kabát
jako	jako	k8xC	jako
první	první	k4xOgFnSc3	první
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
připravili	připravit	k5eAaPmAgMnP	připravit
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgNnSc2	který
bylo	být	k5eAaImAgNnS	být
pódium	pódium	k1gNnSc1	pódium
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
sportovních	sportovní	k2eAgFnPc2d1	sportovní
hal	hala	k1gFnPc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celek	k1gInSc7	celek
turné	turné	k1gNnSc2	turné
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
60	[number]	k4	60
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
překonána	překonán	k2eAgFnSc1d1	překonána
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
minulého	minulý	k2eAgNnSc2d1	Minulé
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
kapela	kapela	k1gFnSc1	kapela
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
větší	veliký	k2eAgNnSc4d2	veliký
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc1	první
DVD	DVD	kA	DVD
skupiny	skupina	k1gFnSc2	skupina
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
-	-	kIx~	-
Best	Best	k1gMnSc1	Best
of	of	k?	of
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
hrstce	hrstka	k1gFnSc3	hrstka
českých	český	k2eAgMnPc2d1	český
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vydali	vydat	k5eAaPmAgMnP	vydat
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
jak	jak	k6eAd1	jak
videoklipy	videoklip	k1gInPc7	videoklip
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
Go	Go	k1gFnSc2	Go
satane	satan	k1gInSc5	satan
go	go	k?	go
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
sportovní	sportovní	k2eAgFnSc2d1	sportovní
haly	hala	k1gFnSc2	hala
Tipsport	Tipsport	k1gInSc4	Tipsport
arena	areen	k2eAgFnSc1d1	arena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
slavíku	slavík	k1gInSc6	slavík
se	se	k3xPyFc4	se
Kabát	Kabát	k1gMnSc1	Kabát
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
deska	deska	k1gFnSc1	deska
Dole	dole	k6eAd1	dole
v	v	k7c6	v
dole	dol	k1gInSc6	dol
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čtyřplatinová	čtyřplatinový	k2eAgFnSc1d1	čtyřplatinový
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
desky	deska	k1gFnSc2	deska
skupina	skupina	k1gFnSc1	skupina
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c4	v
den	den	k1gInSc4	den
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začalo	začít	k5eAaPmAgNnS	začít
halové	halový	k2eAgNnSc1d1	halové
turné	turné	k1gNnSc1	turné
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
vyprodala	vyprodat	k5eAaPmAgFnS	vyprodat
všechny	všechen	k3xTgInPc4	všechen
zimní	zimní	k2eAgInPc4d1	zimní
stadiony	stadion	k1gInPc4	stadion
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
poté	poté	k6eAd1	poté
Kabát	Kabát	k1gMnSc1	Kabát
přebral	přebrat	k5eAaPmAgMnS	přebrat
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
Praha	Praha	k1gFnSc1	Praha
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
kapela	kapela	k1gFnSc1	kapela
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
a	a	k8xC	a
nejprodávanější	prodávaný	k2eAgInSc4d3	nejprodávanější
titul	titul	k1gInSc4	titul
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
78	[number]	k4	78
000	[number]	k4	000
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
skupina	skupina	k1gFnSc1	skupina
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
samostatné	samostatný	k2eAgFnSc6d1	samostatná
open-air	openir	k1gInSc4	open-air
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgInSc2	který
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
deseti	deset	k4xCc6	deset
fotbalových	fotbalový	k2eAgInPc6d1	fotbalový
stadionech	stadion	k1gInPc6	stadion
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
jednom	jeden	k4xCgMnSc6	jeden
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc3	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
turné	turné	k1gNnSc2	turné
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
180	[number]	k4	180
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
15	[number]	k4	15
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
Kabáti	Kabát	k1gMnPc1	Kabát
vydali	vydat	k5eAaPmAgMnP	vydat
DVD	DVD	kA	DVD
Kabát	kabát	k1gInSc4	kabát
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
záznam	záznam	k1gInSc4	záznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
areně	areeň	k1gFnSc2	areeň
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
na	na	k7c6	na
atletickém	atletický	k2eAgInSc6d1	atletický
stadionu	stadion	k1gInSc6	stadion
Slavia	Slavia	k1gFnSc1	Slavia
a	a	k8xC	a
dokument	dokument	k1gInSc1	dokument
k	k	k7c3	k
turné	turné	k1gNnSc3	turné
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
skupina	skupina	k1gFnSc1	skupina
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
obhájila	obhájit	k5eAaPmAgFnS	obhájit
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Českém	český	k2eAgMnSc6d1	český
slavíkovi	slavík	k1gMnSc6	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2006	[number]	k4	2006
začali	začít	k5eAaPmAgMnP	začít
Kabáti	Kabát	k1gMnPc1	Kabát
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
studiové	studiový	k2eAgFnSc6d1	studiová
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
mělo	mít	k5eAaImAgNnS	mít
album	album	k1gNnSc1	album
vyjít	vyjít	k5eAaPmF	vyjít
již	již	k6eAd1	již
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nedopsaným	dopsaný	k2eNgInPc3d1	nedopsaný
textům	text	k1gInPc3	text
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
vydání	vydání	k1gNnSc1	vydání
postupně	postupně	k6eAd1	postupně
odkládalo	odkládat	k5eAaImAgNnS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Corrida	Corrida	k1gFnSc1	Corrida
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vyšla	vyjít	k5eAaPmAgFnS	vyjít
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
podzimní	podzimní	k2eAgNnSc1d1	podzimní
turné	turné	k1gNnSc1	turné
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
prodeje	prodej	k1gInSc2	prodej
získali	získat	k5eAaPmAgMnP	získat
Kabáti	Kabát	k1gMnPc1	Kabát
za	za	k7c4	za
album	album	k1gNnSc4	album
tříplatinovou	tříplatinový	k2eAgFnSc4d1	tříplatinový
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
45	[number]	k4	45
000	[number]	k4	000
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
56	[number]	k4	56
900	[number]	k4	900
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
za	za	k7c4	za
nejprodávanější	prodávaný	k2eAgFnSc4d3	nejprodávanější
desku	deska	k1gFnSc4	deska
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Malá	malý	k2eAgFnSc1d1	malá
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
české	český	k2eAgNnSc1d1	české
kolo	kolo	k1gNnSc1	kolo
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
soutěže	soutěž	k1gFnSc2	soutěž
Eurovision	Eurovision	k1gInSc1	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
soutěže	soutěž	k1gFnSc2	soutěž
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
kapela	kapela	k1gFnSc1	kapela
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
tehdy	tehdy	k6eAd1	tehdy
poslední	poslední	k2eAgFnSc3d1	poslední
desce	deska	k1gFnSc3	deska
Corrida	Corrida	k1gFnSc1	Corrida
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncertní	koncertní	k2eAgFnSc1d1	koncertní
šňůra	šňůra	k1gFnSc1	šňůra
čítala	čítat	k5eAaImAgFnS	čítat
celkem	celkem	k6eAd1	celkem
třináct	třináct	k4xCc4	třináct
koncertů	koncert	k1gInPc2	koncert
po	po	k7c6	po
fotbalových	fotbalový	k2eAgInPc6d1	fotbalový
stadionech	stadion	k1gInPc6	stadion
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
okolo	okolo	k7c2	okolo
240	[number]	k4	240
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
poté	poté	k6eAd1	poté
Kabáti	Kabát	k1gMnPc1	Kabát
získali	získat	k5eAaPmAgMnP	získat
svého	svůj	k3xOyFgInSc2	svůj
třetího	třetí	k4xOgInSc2	třetí
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získala	získat	k5eAaPmAgFnS	získat
kapela	kapela	k1gFnSc1	kapela
cenu	cena	k1gFnSc4	cena
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Allianz	Allianza	k1gFnPc2	Allianza
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejprodávanější	prodávaný	k2eAgMnSc1d3	nejprodávanější
interpret	interpret	k1gMnSc1	interpret
patnáctiletí	patnáctiletý	k2eAgMnPc1d1	patnáctiletý
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
825	[number]	k4	825
606	[number]	k4	606
kusů	kus	k1gInPc2	kus
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
českých	český	k2eAgInPc6d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgInPc6d1	slovenský
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Votvírák	Votvírák	k1gInSc1	Votvírák
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
pořadatele	pořadatel	k1gMnSc2	pořadatel
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
přes	přes	k7c4	přes
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
DVD	DVD	kA	DVD
Kabát	kabát	k1gInSc1	kabát
Corrida	Corrida	k1gFnSc1	Corrida
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
lehkoatletického	lehkoatletický	k2eAgInSc2d1	lehkoatletický
stadionu	stadion	k1gInSc2	stadion
na	na	k7c6	na
Slavii	slavie	k1gFnSc6	slavie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
balení	balení	k1gNnSc2	balení
byl	být	k5eAaImAgInS	být
i	i	k9	i
dokument	dokument	k1gInSc1	dokument
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
také	také	k9	také
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
fotkami	fotka	k1gFnPc7	fotka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
prvenství	prvenství	k1gNnSc4	prvenství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
chystá	chystat	k5eAaImIp3nS	chystat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
by	by	kYmCp3nS	by
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
oslavy	oslava	k1gFnSc2	oslava
20	[number]	k4	20
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
termínu	termín	k1gInSc2	termín
chtěli	chtít	k5eAaImAgMnP	chtít
Kabáti	Kabát	k1gMnPc1	Kabát
vydat	vydat	k5eAaPmF	vydat
také	také	k9	také
novou	nový	k2eAgFnSc4d1	nová
řadovou	řadový	k2eAgFnSc4d1	řadová
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Výroční	výroční	k2eAgInSc1d1	výroční
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
u	u	k7c2	u
pražského	pražský	k2eAgInSc2d1	pražský
letohrádku	letohrádek	k1gInSc2	letohrádek
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
koncertu	koncert	k1gInSc2	koncert
byly	být	k5eAaImAgFnP	být
dovezeny	dovézt	k5eAaPmNgFnP	dovézt
speciální	speciální	k2eAgFnPc1d1	speciální
tribuny	tribuna	k1gFnPc1	tribuna
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
závodech	závod	k1gInPc6	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
na	na	k7c6	na
Hungaroringu	Hungaroring	k1gInSc6	Hungaroring
<g/>
.	.	kIx.	.
</s>
<s>
Zvukový	zvukový	k2eAgInSc1d1	zvukový
aparát	aparát	k1gInSc1	aparát
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Acoustics	Acoustics	k1gInSc1	Acoustics
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
W	W	kA	W
dodala	dodat	k5eAaPmAgFnS	dodat
společnost	společnost	k1gFnSc1	společnost
Black	Black	k1gInSc4	Black
Box	box	k1gInSc1	box
Music	Musice	k1gFnPc2	Musice
<g/>
,	,	kIx,	,
pódium	pódium	k1gNnSc4	pódium
a	a	k8xC	a
zastřešení	zastřešení	k1gNnSc4	zastřešení
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
zapůjčila	zapůjčit	k5eAaPmAgFnS	zapůjčit
Stageco	Stageco	k6eAd1	Stageco
<g/>
,	,	kIx,	,
osvětlení	osvětlení	k1gNnSc1	osvětlení
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
AC	AC	kA	AC
Lighting	Lighting	k1gInSc4	Lighting
a	a	k8xC	a
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Výbavou	výbava	k1gFnSc7	výbava
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
světových	světový	k2eAgMnPc2d1	světový
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
Robbie	Robbie	k1gFnSc1	Robbie
Williams	Williamsa	k1gFnPc2	Williamsa
nebo	nebo	k8xC	nebo
Metallica	Metallicum	k1gNnSc2	Metallicum
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
manažer	manažer	k1gInSc1	manažer
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
prodáno	prodat	k5eAaPmNgNnS	prodat
50	[number]	k4	50
000	[number]	k4	000
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
plánovaných	plánovaný	k2eAgNnPc2d1	plánované
osm	osm	k4xCc4	osm
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
zahrála	zahrát	k5eAaPmAgFnS	zahrát
36	[number]	k4	36
svých	svůj	k3xOyFgInPc2	svůj
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
od	od	k7c2	od
alba	album	k1gNnSc2	album
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
až	až	k8xS	až
po	po	k7c6	po
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejnovější	nový	k2eAgNnSc1d3	nejnovější
album	album	k1gNnSc1	album
Corrida	Corrida	k1gFnSc1	Corrida
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostavilo	dostavit	k5eAaPmAgNnS	dostavit
přes	přes	k7c4	přes
60	[number]	k4	60
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Kabát	Kabát	k1gMnSc1	Kabát
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
koncertu	koncert	k1gInSc2	koncert
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Nedvědových	Nedvědových	k2eAgMnSc2d1	Nedvědových
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Strahově	Strahov	k1gInSc6	Strahov
a	a	k8xC	a
předčil	předčit	k5eAaBmAgMnS	předčit
srpnový	srpnový	k2eAgInSc4d1	srpnový
koncert	koncert	k1gInSc4	koncert
Madonny	Madonna	k1gFnSc2	Madonna
na	na	k7c6	na
Chodově	Chodov	k1gInSc6	Chodov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
Vypichu	vypich	k1gInSc6	vypich
kapela	kapela	k1gFnSc1	kapela
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
listopadu	listopad	k1gInSc2	listopad
ho	on	k3xPp3gInSc4	on
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
DVD	DVD	kA	DVD
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
živáku	živák	k1gInSc6	živák
<g/>
"	"	kIx"	"
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
Po	po	k7c6	po
čertech	čert	k1gMnPc6	čert
velkej	velkej	k?	velkej
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
kapela	kapela	k1gFnSc1	kapela
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
000	[number]	k4	000
bodů	bod	k1gInPc2	bod
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
skupinu	skupina	k1gFnSc4	skupina
Chinaski	Chinaske	k1gFnSc4	Chinaske
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
tak	tak	k6eAd1	tak
svého	svůj	k3xOyFgInSc2	svůj
pátého	pátý	k4xOgInSc2	pátý
slavíka	slavík	k1gInSc2	slavík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získali	získat	k5eAaPmAgMnP	získat
Kabáti	Kabát	k1gMnPc1	Kabát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
<g/>
&	&	k?	&
<g/>
nbsp	nbsp	k1gMnSc1	nbsp
Česku	Česko	k1gNnSc6	Česko
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nevystoupili	vystoupit	k5eNaPmAgMnP	vystoupit
a	a	k8xC	a
odehráli	odehrát	k5eAaPmAgMnP	odehrát
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
koncerty	koncert	k1gInPc4	koncert
na	na	k7c6	na
slovenských	slovenský	k2eAgInPc6d1	slovenský
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
kapelu	kapela	k1gFnSc4	kapela
převzal	převzít	k5eAaPmAgMnS	převzít
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kočandrle	Kočandrl	k1gMnSc2	Kočandrl
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
připravované	připravovaný	k2eAgFnSc6d1	připravovaná
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
vydali	vydat	k5eAaPmAgMnP	vydat
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
s	s	k7c7	s
68	[number]	k4	68
000	[number]	k4	000
prodanými	prodaný	k2eAgFnPc7d1	prodaná
kopiemi	kopie	k1gFnPc7	kopie
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
deskou	deska	k1gFnSc7	deska
Kabáti	Kabát	k1gMnPc1	Kabát
zároveň	zároveň	k6eAd1	zároveň
překonali	překonat	k5eAaPmAgMnP	překonat
hranici	hranice	k1gFnSc4	hranice
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k9	což
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
během	během	k7c2	během
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
desku	deska	k1gFnSc4	deska
dostal	dostat	k5eAaPmAgMnS	dostat
také	také	k9	také
jeden	jeden	k4xCgMnSc1	jeden
vylosovaný	vylosovaný	k2eAgMnSc1d1	vylosovaný
fanoušek	fanoušek	k1gMnSc1	fanoušek
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
čítalo	čítat	k5eAaImAgNnS	čítat
sedmnáct	sedmnáct	k4xCc1	sedmnáct
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
halách	hala	k1gFnPc6	hala
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
zvolila	zvolit	k5eAaPmAgFnS	zvolit
variantu	varianta	k1gFnSc4	varianta
s	s	k7c7	s
pódiem	pódium	k1gNnSc7	pódium
uprostřed	uprostřed	k7c2	uprostřed
haly	hala	k1gFnSc2	hala
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
již	již	k6eAd1	již
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
koncepci	koncepce	k1gFnSc3	koncepce
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
překonávala	překonávat	k5eAaImAgFnS	překonávat
rekordy	rekord	k1gInPc4	rekord
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hal	hala	k1gFnPc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
pražský	pražský	k2eAgInSc1d1	pražský
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
zaznamenávala	zaznamenávat	k5eAaImAgFnS	zaznamenávat
a	a	k8xC	a
následně	následně	k6eAd1	následně
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
Turné	turné	k1gNnSc1	turné
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
tříplatinové	tříplatinový	k2eAgNnSc1d1	tříplatinový
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc3	rok
kapela	kapela	k1gFnSc1	kapela
ještě	ještě	k9	ještě
získala	získat	k5eAaPmAgFnS	získat
svého	svůj	k3xOyFgInSc2	svůj
sedmého	sedmý	k4xOgInSc2	sedmý
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
kapela	kapela	k1gFnSc1	kapela
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
desce	deska	k1gFnSc3	deska
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Prag	k1gMnSc4	Prag
cenu	cena	k1gFnSc4	cena
OSA	osa	k1gFnSc1	osa
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
koncert	koncert	k1gInSc4	koncert
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
baskytarista	baskytarista	k1gMnSc1	baskytarista
Milan	Milana	k1gFnPc2	Milana
Špalek	špalek	k1gInSc1	špalek
zároveň	zároveň	k6eAd1	zároveň
obdržel	obdržet	k5eAaPmAgInS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
jako	jako	k8xS	jako
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
textař	textař	k1gMnSc1	textař
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
kapela	kapela	k1gFnSc1	kapela
nachystala	nachystat	k5eAaBmAgFnS	nachystat
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
big	big	k?	big
bandem	bando	k1gNnSc7	bando
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
tajně	tajně	k6eAd1	tajně
připravovala	připravovat	k5eAaImAgFnS	připravovat
téměř	téměř	k6eAd1	téměř
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
technické	technický	k2eAgFnSc3d1	technická
náročnosti	náročnost	k1gFnSc3	náročnost
mělo	mít	k5eAaImAgNnS	mít
turné	turné	k1gNnSc3	turné
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
zastávek	zastávka	k1gFnPc2	zastávka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Kabáti	Kabát	k1gMnPc1	Kabát
přidali	přidat	k5eAaPmAgMnP	přidat
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rodných	rodný	k2eAgFnPc6d1	rodná
Teplicích	Teplice	k1gFnPc6	Teplice
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadionu	stadion	k1gInSc6	stadion
Na	na	k7c6	na
Stínadlech	stínadlo	k1gNnPc6	stínadlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celá	k1gFnPc1	celá
scéna	scéna	k1gFnSc1	scéna
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
po	po	k7c6	po
30	[number]	k4	30
<g/>
ti	ten	k3xDgMnPc1	ten
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
pojata	pojmout	k5eAaPmNgNnP	pojmout
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
designu	design	k1gInSc2	design
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
kabaretů	kabaret	k1gInPc2	kabaret
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Big	Big	k?	Big
band	band	k1gInSc1	band
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
vždy	vždy	k6eAd1	vždy
připojil	připojit	k5eAaPmAgInS	připojit
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
odhalila	odhalit	k5eAaPmAgFnS	odhalit
také	také	k9	také
celá	celý	k2eAgFnSc1d1	celá
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
bigbandovských	bigbandovský	k2eAgFnPc2d1	bigbandovský
aranží	aranže	k1gFnPc2	aranže
dočkala	dočkat	k5eAaPmAgFnS	dočkat
jen	jen	k9	jen
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Dole	dole	k6eAd1	dole
v	v	k7c6	v
dole	dol	k1gInSc6	dol
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zahráli	zahrát	k5eAaPmAgMnP	zahrát
Kabáti	Kabát	k1gMnPc1	Kabát
bez	bez	k7c2	bez
big	big	k?	big
bandu	band	k1gInSc2	band
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
Vypich	vypich	k1gInSc4	vypich
a	a	k8xC	a
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
další	další	k2eAgInSc4d1	další
výroční	výroční	k2eAgInSc4d1	výroční
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
zároveň	zároveň	k6eAd1	zároveň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
reedice	reedice	k1gFnSc1	reedice
kompilace	kompilace	k1gFnSc1	kompilace
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
původně	původně	k6eAd1	původně
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
2	[number]	k4	2
CD	CD	kA	CD
byla	být	k5eAaImAgFnS	být
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Fialou	Fiala	k1gMnSc7	Fiala
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
kvalita	kvalita	k1gFnSc1	kvalita
zvuku	zvuk	k1gInSc2	zvuk
starších	starý	k2eAgFnPc2d2	starší
skladeb	skladba	k1gFnPc2	skladba
podobala	podobat	k5eAaImAgFnS	podobat
kvalitě	kvalita	k1gFnSc3	kvalita
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgInP	být
jako	jako	k8xC	jako
bonus	bonus	k1gInSc1	bonus
přibaleny	přibalen	k2eAgFnPc1d1	přibalena
2	[number]	k4	2
DVD	DVD	kA	DVD
s	s	k7c7	s
videoklipy	videoklip	k1gInPc7	videoklip
a	a	k8xC	a
záznamem	záznam	k1gInSc7	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
původně	původně	k6eAd1	původně
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
edici	edice	k1gFnSc6	edice
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
-	-	kIx~	-
Best	Best	k1gMnSc1	Best
of	of	k?	of
video	video	k1gNnSc4	video
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
reedice	reedice	k1gFnSc1	reedice
byla	být	k5eAaImAgFnS	být
balena	balit	k5eAaImNgFnS	balit
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
obalu	obal	k1gInSc6	obal
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
aktuální	aktuální	k2eAgFnPc4d1	aktuální
fotky	fotka	k1gFnPc4	fotka
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
skupiny	skupina	k1gFnSc2	skupina
Kryštof	Kryštof	k1gMnSc1	Kryštof
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
přerušena	přerušen	k2eAgFnSc1d1	přerušena
série	série	k1gFnSc1	série
šesti	šest	k4xCc2	šest
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
oslavy	oslava	k1gFnSc2	oslava
výročí	výročí	k1gNnSc2	výročí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kabát	Kabát	k1gMnSc1	Kabát
odehrál	odehrát	k5eAaPmAgMnS	odehrát
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
přidala	přidat	k5eAaPmAgFnS	přidat
skupina	skupina	k1gFnSc1	skupina
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
kromě	kromě	k7c2	kromě
avizovaného	avizovaný	k2eAgNnSc2d1	avizované
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
Vypichu	vypich	k1gInSc6	vypich
také	také	k9	také
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Topfest	Topfest	k1gInSc1	Topfest
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
původně	původně	k6eAd1	původně
plánovala	plánovat	k5eAaImAgFnS	plánovat
vydat	vydat	k5eAaPmF	vydat
novou	nový	k2eAgFnSc4d1	nová
studiovou	studiový	k2eAgFnSc4d1	studiová
desku	deska	k1gFnSc4	deska
ještě	ještě	k9	ještě
před	před	k7c7	před
létem	léto	k1gNnSc7	léto
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
píše	psát	k5eAaImIp3nS	psát
Milan	Milan	k1gMnSc1	Milan
Špalek	špalek	k1gInSc4	špalek
<g/>
,	,	kIx,	,
nedodržela	dodržet	k5eNaPmAgFnS	dodržet
ani	ani	k9	ani
termín	termín	k1gInSc4	termín
před	před	k7c7	před
výročním	výroční	k2eAgInSc7d1	výroční
koncertem	koncert	k1gInSc7	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
zahajovala	zahajovat	k5eAaImAgFnS	zahajovat
jako	jako	k8xS	jako
předskokan	předskokan	k1gMnSc1	předskokan
vítězka	vítězka	k1gFnSc1	vítězka
soutěže	soutěž	k1gFnSc2	soutěž
Hlas	hlas	k1gInSc1	hlas
Československa	Československo	k1gNnSc2	Československo
2014	[number]	k4	2014
Lenka	lenka	k1gFnPc2	lenka
"	"	kIx"	"
<g/>
Lo	Lo	k1gFnSc1	Lo
<g/>
"	"	kIx"	"
Hrůzová	Hrůzová	k1gFnSc1	Hrůzová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
s	s	k7c7	s
frontmanem	frontman	k1gInSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Josefem	Josef	k1gMnSc7	Josef
Vojtkem	Vojtek	k1gMnSc7	Vojtek
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
již	již	k6eAd1	již
během	během	k7c2	během
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
roli	role	k1gFnSc6	role
mentora	mentor	k1gMnSc2	mentor
také	také	k6eAd1	také
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
krátkém	krátký	k2eAgInSc6d1	krátký
setu	set	k1gInSc6	set
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
dánská	dánský	k2eAgFnSc1d1	dánská
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
D-A-D	D-A-D	k1gFnSc1	D-A-D
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
Kabáti	Kabát	k1gMnPc1	Kabát
pozvali	pozvat	k5eAaPmAgMnP	pozvat
jako	jako	k9	jako
speciální	speciální	k2eAgMnPc4d1	speciální
hosty	host	k1gMnPc4	host
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
před	před	k7c7	před
pěti	pět	k4xCc7	pět
lety	léto	k1gNnPc7	léto
i	i	k8xC	i
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
Kabáti	Kabát	k1gMnPc1	Kabát
připravili	připravit	k5eAaPmAgMnP	připravit
show	show	k1gNnSc4	show
s	s	k7c7	s
kvalitním	kvalitní	k2eAgInSc7d1	kvalitní
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
spoustou	spousta	k1gFnSc7	spousta
světel	světlo	k1gNnPc2	světlo
a	a	k8xC	a
s	s	k7c7	s
pyrotechnickými	pyrotechnický	k2eAgInPc7d1	pyrotechnický
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
zaznamenáváno	zaznamenávat	k5eAaImNgNnS	zaznamenávat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
neoficiálních	neoficiální	k2eAgInPc2d1	neoficiální
odhadů	odhad	k1gInPc2	odhad
Po	po	k7c6	po
čertech	čert	k1gMnPc6	čert
velkej	velkej	k?	velkej
koncert	koncert	k1gInSc1	koncert
II	II	kA	II
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
celkem	celkem	k6eAd1	celkem
75	[number]	k4	75
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
koncertu	koncert	k1gInSc6	koncert
českého	český	k2eAgMnSc2d1	český
interpreta	interpret	k1gMnSc2	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
kapela	kapela	k1gFnSc1	kapela
přesný	přesný	k2eAgInSc4d1	přesný
počet	počet	k1gInSc4	počet
diváků	divák	k1gMnPc2	divák
nezveřejnila	zveřejnit	k5eNaPmAgFnS	zveřejnit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
přišlo	přijít	k5eAaPmAgNnS	přijít
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
před	před	k7c7	před
i	i	k9	i
po	po	k7c6	po
koncertu	koncert	k1gInSc6	koncert
pršelo	pršet	k5eAaImAgNnS	pršet
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
louka	louka	k1gFnSc1	louka
na	na	k7c6	na
Vypichu	vypich	k1gInSc6	vypich
vlivem	vlivem	k7c2	vlivem
použití	použití	k1gNnSc2	použití
těžkých	těžký	k2eAgNnPc2d1	těžké
aut	auto	k1gNnPc2	auto
na	na	k7c4	na
odvoz	odvoz	k1gInSc4	odvoz
techniky	technika	k1gFnSc2	technika
naprosto	naprosto	k6eAd1	naprosto
zdevastována	zdevastován	k2eAgFnSc1d1	zdevastována
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
protesty	protest	k1gInPc1	protest
některých	některý	k3yIgMnPc2	některý
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Management	management	k1gInSc1	management
kapely	kapela	k1gFnSc2	kapela
pak	pak	k6eAd1	pak
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
dal	dát	k5eAaPmAgMnS	dát
celou	celý	k2eAgFnSc4d1	celá
louku	louka	k1gFnSc4	louka
do	do	k7c2	do
pořádku	pořádek	k1gInSc2	pořádek
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
osel	osít	k5eAaPmAgMnS	osít
trávu	tráva	k1gFnSc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
skupina	skupina	k1gFnSc1	skupina
Kabát	kabát	k1gInSc1	kabát
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
000	[number]	k4	000
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dlouho	dlouho	k6eAd1	dlouho
odkládaná	odkládaný	k2eAgFnSc1d1	odkládaná
deska	deska	k1gFnSc1	deska
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
dvanáct	dvanáct	k4xCc1	dvanáct
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
domluvila	domluvit	k5eAaPmAgFnS	domluvit
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Yardou	Yarda	k1gMnSc7	Yarda
Halešicem	Halešic	k1gMnSc7	Halešic
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
"	"	kIx"	"
<g/>
pořádně	pořádně	k6eAd1	pořádně
nakopnout	nakopnout	k5eAaPmF	nakopnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
berlínského	berlínský	k2eAgNnSc2d1	berlínské
Riverside	Riversid	k1gInSc5	Riversid
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
největší	veliký	k2eAgFnSc4d3	veliký
spokojenost	spokojenost	k1gFnSc4	spokojenost
preferovali	preferovat	k5eAaImAgMnP	preferovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
mixem	mix	k1gInSc7	mix
z	z	k7c2	z
českého	český	k2eAgMnSc2d1	český
Sono	Sono	k6eAd1	Sono
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Brousíme	brousit	k5eAaImIp1nP	brousit
nože	nůž	k1gInPc4	nůž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kapela	kapela	k1gFnSc1	kapela
věnovala	věnovat	k5eAaPmAgFnS	věnovat
českým	český	k2eAgMnPc3d1	český
hokejistům	hokejista	k1gMnPc3	hokejista
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
písničku	písnička	k1gFnSc4	písnička
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
propagace	propagace	k1gFnSc1	propagace
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
open	open	k1gInSc4	open
air	air	k?	air
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
městech	město	k1gNnPc6	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
také	také	k9	také
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Topfest	Topfest	k1gInSc1	Topfest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
Kabáti	Kabát	k1gMnPc1	Kabát
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
Praha	Praha	k1gFnSc1	Praha
převzali	převzít	k5eAaPmAgMnP	převzít
jubilejního	jubilejní	k2eAgMnSc2d1	jubilejní
desátého	desátý	k4xOgInSc2	desátý
zlatého	zlatý	k2eAgInSc2d1	zlatý
slavíka	slavík	k1gInSc2	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
poté	poté	k6eAd1	poté
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
koncertu	koncert	k1gInSc6	koncert
ve	v	k7c6	v
vyprodané	vyprodaný	k2eAgFnSc6d1	vyprodaná
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
kapela	kapela	k1gFnSc1	kapela
převzala	převzít	k5eAaPmAgFnS	převzít
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Pink	pink	k2eAgFnPc2d1	pink
Panther	Panthra	k1gFnPc2	Panthra
Media	medium	k1gNnSc2	medium
"	"	kIx"	"
<g/>
pětiplatinovou	pětiplatinový	k2eAgFnSc4d1	pětiplatinový
<g/>
"	"	kIx"	"
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
v	v	k7c6	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
CD	CD	kA	CD
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
také	také	k9	také
ve	v	k7c6	v
vyprodaném	vyprodaný	k2eAgInSc6d1	vyprodaný
brněnském	brněnský	k2eAgInSc6d1	brněnský
klubu	klub	k1gInSc6	klub
Sono	Sono	k6eAd1	Sono
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
udílením	udílení	k1gNnSc7	udílení
ocenění	ocenění	k1gNnSc2	ocenění
Ceny	cena	k1gFnSc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
cen	cena	k1gFnPc2	cena
"	"	kIx"	"
<g/>
de	de	k?	de
facto	facto	k1gNnSc1	facto
<g/>
"	"	kIx"	"
vyřazena	vyřazen	k2eAgFnSc1d1	vyřazena
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
album	album	k1gNnSc1	album
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
prodávala	prodávat	k5eAaImAgFnS	prodávat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
prodejen	prodejna	k1gFnPc2	prodejna
Tesco	Tesco	k6eAd1	Tesco
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
nehlásí	hlásit	k5eNaImIp3nP	hlásit
prodeje	prodej	k1gFnPc4	prodej
alb	album	k1gNnPc2	album
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
federaci	federace	k1gFnSc4	federace
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kabáti	Kabát	k1gMnPc1	Kabát
prodali	prodat	k5eAaPmAgMnP	prodat
koncovým	koncový	k2eAgMnPc3d1	koncový
zákazníkům	zákazník	k1gMnPc3	zákazník
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
celkem	celkem	k6eAd1	celkem
52	[number]	k4	52
000	[number]	k4	000
fyzických	fyzický	k2eAgMnPc2d1	fyzický
nosičů	nosič	k1gMnPc2	nosič
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
Anděl	Anděla	k1gFnPc2	Anděla
2015	[number]	k4	2015
nakonec	nakonec	k9	nakonec
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
kapela	kapela	k1gFnSc1	kapela
Kryštof	Kryštof	k1gMnSc1	Kryštof
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
albem	album	k1gNnSc7	album
Srdcebeat	Srdcebeat	k2eAgInSc1d1	Srdcebeat
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
28	[number]	k4	28
753	[number]	k4	753
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
OSA	osa	k1gFnSc1	osa
později	pozdě	k6eAd2	pozdě
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
nahlášených	nahlášený	k2eAgInPc2d1	nahlášený
prodejů	prodej	k1gInPc2	prodej
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
vydavatelů	vydavatel	k1gMnPc2	vydavatel
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
desek	deska	k1gFnPc2	deska
skutečně	skutečně	k6eAd1	skutečně
prodala	prodat	k5eAaPmAgFnS	prodat
kapela	kapela	k1gFnSc1	kapela
Kabát	kabát	k1gInSc1	kabát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
50	[number]	k4	50
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
ocenění	ocenění	k1gNnPc2	ocenění
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
již	již	k6eAd1	již
nemělo	mít	k5eNaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Valkýra	valkýra	k1gFnSc1	valkýra
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
videoklip	videoklip	k1gInSc4	videoklip
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
nejdražší	drahý	k2eAgMnSc1d3	nejdražší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
pak	pak	k6eAd1	pak
skupina	skupina	k1gFnSc1	skupina
objela	objet	k5eAaPmAgFnS	objet
Topfest	Topfest	k1gFnSc4	Topfest
<g/>
,	,	kIx,	,
Vysočinafest	Vysočinafest	k1gFnSc4	Vysočinafest
a	a	k8xC	a
putovní	putovní	k2eAgInSc4d1	putovní
festival	festival	k1gInSc4	festival
Hrady	hrad	k1gInPc7	hrad
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
vyšel	vyjít	k5eAaPmAgInS	vyjít
multinosič	multinosič	k1gInSc1	multinosič
Kabát	kabát	k1gInSc1	kabát
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
big	big	k?	big
bandem	band	k1gInSc7	band
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
výročního	výroční	k2eAgInSc2d1	výroční
koncertu	koncert	k1gInSc2	koncert
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Vypichu	vypich	k1gInSc6	vypich
a	a	k8xC	a
jako	jako	k9	jako
bonus	bonus	k1gInSc1	bonus
navíc	navíc	k6eAd1	navíc
poslední	poslední	k2eAgFnSc2d1	poslední
řadové	řadový	k2eAgFnSc2d1	řadová
CD	CD	kA	CD
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
kapela	kapela	k1gFnSc1	kapela
pojedenácté	pojedenácté	k4xO	pojedenácté
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Českého	český	k2eAgInSc2d1	český
slavíka	slavík	k1gInSc2	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Kabáti	Kabát	k1gMnPc1	Kabát
zároveň	zároveň	k6eAd1	zároveň
slavností	slavnost	k1gFnSc7	slavnost
večer	večer	k6eAd1	večer
zahajovali	zahajovat	k5eAaImAgMnP	zahajovat
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Myslivecký	myslivecký	k2eAgInSc1d1	myslivecký
ples	ples	k1gInSc1	ples
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
kapela	kapela	k1gFnSc1	kapela
plánuje	plánovat	k5eAaImIp3nS	plánovat
halové	halový	k2eAgNnSc4d1	halové
turné	turné	k1gNnSc4	turné
"	"	kIx"	"
<g/>
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
Do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
turné	turné	k1gNnSc6	turné
Kabáti	Kabát	k1gMnPc1	Kabát
postaví	postavit	k5eAaPmIp3nP	postavit
pódium	pódium	k1gNnSc4	pódium
opět	opět	k6eAd1	opět
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nP	by
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
podle	podle	k7c2	podle
Vojtka	Vojtek	k1gMnSc2	Vojtek
chtěli	chtít	k5eAaImAgMnP	chtít
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
taková	takový	k3xDgNnPc4	takový
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
dosud	dosud	k6eAd1	dosud
nehráli	hrát	k5eNaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
skupina	skupina	k1gFnSc1	skupina
plánuje	plánovat	k5eAaImIp3nS	plánovat
zařadit	zařadit	k5eAaPmF	zařadit
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc1	Třinec
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc4	Znojmo
a	a	k8xC	a
Bratislavu	Bratislava	k1gFnSc4	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
založena	založit	k5eAaPmNgFnS	založit
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
thrashmetalová	thrashmetalový	k2eAgFnSc1d1	thrashmetalová
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
Kabátům	Kabát	k1gMnPc3	Kabát
vydržel	vydržet	k5eAaPmAgInS	vydržet
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
vydání	vydání	k1gNnSc2	vydání
jejich	jejich	k3xOp3gFnSc2	jejich
debutové	debutový	k2eAgFnSc2d1	debutová
desky	deska	k1gFnSc2	deska
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
postupně	postupně	k6eAd1	postupně
přibližovat	přibližovat	k5eAaImF	přibližovat
k	k	k7c3	k
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
klasickému	klasický	k2eAgInSc3d1	klasický
rocku	rock	k1gInSc3	rock
a	a	k8xC	a
bigbítu	bigbít	k1gInSc3	bigbít
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kabáti	Kabát	k1gMnPc1	Kabát
hrají	hrát	k5eAaImIp3nP	hrát
"	"	kIx"	"
<g/>
přímočarej	přímočarej	k?	přímočarej
rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
roll	rollum	k1gNnPc2	rollum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
skupiny	skupina	k1gFnSc2	skupina
objevovaly	objevovat	k5eAaImAgFnP	objevovat
hlavně	hlavně	k9	hlavně
vulgarity	vulgarita	k1gFnPc1	vulgarita
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgInPc1d1	sexuální
motivy	motiv	k1gInPc1	motiv
a	a	k8xC	a
opěvování	opěvování	k1gNnSc1	opěvování
opilosti	opilost	k1gFnSc2	opilost
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
motivů	motiv	k1gInPc2	motiv
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
desky	deska	k1gFnSc2	deska
Colorado	Colorado	k1gNnSc1	Colorado
ubylo	ubýt	k5eAaPmAgNnS	ubýt
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
především	především	k9	především
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
a	a	k8xC	a
humorné	humorný	k2eAgInPc4d1	humorný
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
komentováno	komentován	k2eAgNnSc4d1	komentováno
politické	politický	k2eAgNnSc4d1	politické
nebo	nebo	k8xC	nebo
sociální	sociální	k2eAgNnSc4d1	sociální
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
zároveň	zároveň	k6eAd1	zároveň
Kabáti	Kabát	k1gMnPc1	Kabát
začali	začít	k5eAaPmAgMnP	začít
kombinovat	kombinovat	k5eAaImF	kombinovat
další	další	k2eAgInPc4d1	další
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
country	country	k2eAgMnSc1d1	country
nebo	nebo	k8xC	nebo
big	big	k?	big
beat	beat	k1gInSc1	beat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
odehráli	odehrát	k5eAaPmAgMnP	odehrát
jedno	jeden	k4xCgNnSc4	jeden
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
big	big	k?	big
bandu	band	k1gInSc2	band
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k9	jako
klasické	klasický	k2eAgNnSc4d1	klasické
rockové	rockový	k2eAgNnSc4d1	rockové
kvarteto	kvarteto	k1gNnSc4	kvarteto
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Milan	Milan	k1gMnSc1	Milan
Špalek	špalek	k1gInSc1	špalek
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ovšem	ovšem	k9	ovšem
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
prvně	prvně	k?	prvně
slyšet	slyšet	k5eAaImF	slyšet
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
pak	pak	k6eAd1	pak
text	text	k1gInSc4	text
napíše	napsat	k5eAaBmIp3nS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
skládá	skládat	k5eAaImIp3nS	skládat
dvojice	dvojice	k1gFnSc2	dvojice
kytaristů	kytarista	k1gMnPc2	kytarista
Ota	Ota	k1gMnSc1	Ota
Váňa	Váňa	k1gMnSc1	Váňa
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Krulich	Krulich	k1gMnSc1	Krulich
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
psané	psaný	k2eAgMnPc4d1	psaný
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
anglickém	anglický	k2eAgInSc6d1	anglický
textu	text	k1gInSc6	text
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
u	u	k7c2	u
písničky	písnička	k1gFnSc2	písnička
"	"	kIx"	"
<g/>
Malá	malý	k2eAgFnSc1d1	malá
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
hudební	hudební	k2eAgFnSc6d1	hudební
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k9	ale
podle	podle	k7c2	podle
Špalka	Špalko	k1gNnSc2	Špalko
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nebudou	být	k5eNaImBp3nP	být
podlejzat	podlejzat	k5eAaPmF	podlejzat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vojtek	Vojtek	k1gMnSc1	Vojtek
ještě	ještě	k9	ještě
doplnil	doplnit	k5eAaPmAgMnS	doplnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jsou	být	k5eAaImIp3nP	být
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zpívaj	zpívaj	k?	zpívaj
česky	česky	k6eAd1	česky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
urážlivě	urážlivě	k6eAd1	urážlivě
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
agro	agro	k6eAd1	agro
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
začátkům	začátek	k1gInPc3	začátek
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
přebalu	přebal	k1gInSc2	přebal
desky	deska	k1gFnSc2	deska
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
Kabáti	Kabát	k1gMnPc1	Kabát
nechali	nechat	k5eAaPmAgMnP	nechat
nafotit	nafotit	k5eAaPmF	nafotit
ve	v	k7c6	v
vepříně	vepřín	k1gInSc6	vepřín
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prasnicemi	prasnice	k1gFnPc7	prasnice
a	a	k8xC	a
traktorem	traktor	k1gInSc7	traktor
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
totiž	totiž	k9	totiž
nechtěli	chtít	k5eNaImAgMnP	chtít
napodobovat	napodobovat	k5eAaImF	napodobovat
americké	americký	k2eAgFnPc4d1	americká
skupiny	skupina	k1gFnPc4	skupina
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
naprostí	naprostý	k2eAgMnPc1d1	naprostý
burani	buran	k1gMnPc1	buran
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kytarista	kytarista	k1gMnSc1	kytarista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Krulich	Krulich	k1gMnSc1	Krulich
je	on	k3xPp3gNnSc4	on
znám	znát	k5eAaImIp1nS	znát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
kouří	kouřit	k5eAaImIp3nP	kouřit
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
za	za	k7c4	za
vystoupení	vystoupení	k1gNnSc4	vystoupení
zapálí	zapálit	k5eAaPmIp3nS	zapálit
kolem	kolem	k7c2	kolem
sedmi	sedm	k4xCc2	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
uhasí	uhasit	k5eAaPmIp3nS	uhasit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
musí	muset	k5eAaImIp3nS	muset
zpívat	zpívat	k5eAaImF	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
Ota	Ota	k1gMnSc1	Ota
Váňa	Váňa	k1gMnSc1	Váňa
zase	zase	k9	zase
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
nosí	nosit	k5eAaImIp3nP	nosit
kšiltovku	kšiltovka	k1gFnSc4	kšiltovka
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Tomáš	Tomáš	k1gMnSc1	Tomáš
Krulich	Krulich	k1gMnSc1	Krulich
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Milan	Milan	k1gMnSc1	Milan
Špalek	špalek	k1gInSc1	špalek
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Radek	Radek	k1gMnSc1	Radek
"	"	kIx"	"
<g/>
Hurvajs	Hurvajs	k1gInSc1	Hurvajs
<g/>
"	"	kIx"	"
Hurčík	Hurčík	k1gInSc1	Hurčík
-	-	kIx~	-
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Ota	Ota	k1gMnSc1	Ota
Váňa	Váňa	k1gMnSc1	Váňa
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Orgazmus	orgazmus	k1gInSc1	orgazmus
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Má	mít	k5eAaImIp3nS	mít
jí	on	k3xPp3gFnSc7	on
motorovou	motorový	k2eAgFnSc7d1	motorová
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Děvky	děvka	k1gFnSc2	děvka
ty	ty	k3xPp2nSc5	ty
to	ten	k3xDgNnSc4	ten
znaj	znaj	k?	znaj
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Colorado	Colorado	k1gNnSc1	Colorado
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
plná	plný	k2eAgFnSc1d1	plná
trpaslíků	trpaslík	k1gMnPc2	trpaslík
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Čert	čert	k1gMnSc1	čert
na	na	k7c6	na
koze	koza	k1gFnSc6	koza
jel	jet	k5eAaImAgMnS	jet
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
MegaHu	MegaHus	k1gInSc2	MegaHus
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Go	Go	k1gMnSc5	Go
satane	satan	k1gMnSc5	satan
go	go	k?	go
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Dole	dole	k6eAd1	dole
v	v	k7c6	v
dole	dol	k1gInSc6	dol
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Corrida	Corrida	k1gFnSc1	Corrida
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Živě	živě	k6eAd1	živě
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Monitor	monitor	k1gInSc1	monitor
<g/>
)	)	kIx)	)
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Po	po	k7c6	po
čertech	čert	k1gMnPc6	čert
velkej	velkej	k?	velkej
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
Turné	turné	k1gNnPc2	turné
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Box	box	k1gInSc1	box
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Kabát	kabát	k1gInSc1	kabát
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
3	[number]	k4	3
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
1	[number]	k4	1
CD	CD	kA	CD
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Original	Original	k1gFnSc1	Original
Albums	Albumsa	k1gFnPc2	Albumsa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Suma	suma	k1gFnSc1	suma
sumárum	sumárum	k9	sumárum
-	-	kIx~	-
Best	Best	k1gMnSc1	Best
of	of	k?	of
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
VHS	VHS	kA	VHS
<g/>
/	/	kIx~	/
<g/>
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Kabát	kabát	k1gInSc1	kabát
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Kabát	kabát	k1gInSc1	kabát
Corrida	Corrida	k1gFnSc1	Corrida
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Po	po	k7c6	po
čertech	čert	k1gMnPc6	čert
velkej	velkej	k?	velkej
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
2	[number]	k4	2
DVD	DVD	kA	DVD
<g/>
/	/	kIx~	/
<g/>
Blu-ray	Bluaa	k1gMnSc2	Blu-raa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Banditi	Banditi	k?	Banditi
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
Turné	turné	k1gNnPc2	turné
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
2	[number]	k4	2
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Pink	pink	k2eAgFnPc2d1	pink
Panther	Panthra	k1gFnPc2	Panthra
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kabát	kabát	k1gInSc4	kabát
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Oficiální	oficiální	k2eAgInSc1d1	oficiální
facebook	facebook	k1gInSc1	facebook
profil	profil	k1gInSc4	profil
Stránky	stránka	k1gFnSc2	stránka
fanklubu	fanklub	k1gInSc2	fanklub
</s>
