<p>
<s>
Rozběřice	Rozběřice	k1gFnSc1	Rozběřice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Rosberschitz	Rosberschitz	k1gInSc1	Rosberschitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Všestary	Všestara	k1gFnSc2	Všestara
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Všestar	Všestara	k1gFnPc2	Všestara
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nP	procházet
zde	zde	k6eAd1	zde
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
72	[number]	k4	72
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
187	[number]	k4	187
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Rozběřice	Rozběřice	k1gFnSc1	Rozběřice
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
2,09	[number]	k4	2,09
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1371	[number]	k4	1371
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
z	z	k7c2	z
války	válka	k1gFnSc2	válka
1866	[number]	k4	1866
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rozběřice	Rozběřice	k1gFnSc2	Rozběřice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Rozběřice	Rozběřice	k1gFnSc2	Rozběřice
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Rozběřic	Rozběřice	k1gFnPc2	Rozběřice
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Radka	Radek	k1gMnSc2	Radek
Zdvořilého	zdvořilý	k2eAgInSc2d1	zdvořilý
</s>
</p>
