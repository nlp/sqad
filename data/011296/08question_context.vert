<s>
Rozběřice	Rozběřice	k1gFnSc1	Rozběřice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Rosberschitz	Rosberschitz	k1gInSc1	Rosberschitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Všestary	Všestara	k1gFnSc2	Všestara
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Všestar	Všestara	k1gFnPc2	Všestara
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nP	procházet
zde	zde	k6eAd1	zde
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
