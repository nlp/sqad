<s>
V	v	k7c6
roce	rok	k1gInSc6
1949	[number]	k4
začala	začít	k5eAaPmAgFnS
v	v	k7c6
sousedství	sousedství	k1gNnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
čtvrtě	čtvrt	k1gFnSc2
na	na	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
bodě	bod	k1gInSc6
Kraví	kraví	k2eAgFnSc2d1
hory	hora	k1gFnSc2
stavba	stavba	k1gFnSc1
dvou	dva	k4xCgFnPc2
pozorovatelen	pozorovatelna	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jednu	jeden	k4xCgFnSc4
dodnes	dodnes	k6eAd1
využívá	využívat	k5eAaPmIp3nS
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
druhou	druhý	k4xOgFnSc4
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
se	se	k3xPyFc4
záhy	záhy	k6eAd1
připojila	připojit	k5eAaPmAgFnS
i	i	k9
menší	malý	k2eAgFnSc1d2
budova	budova	k1gFnSc1
planetária	planetárium	k1gNnSc2
(	(	kIx(
<g/>
1959	[number]	k4
<g/>
)	)	kIx)
a	a	k8xC
celá	celý	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
r.	r.	kA
1973	[number]	k4
na	na	k7c4
počest	počest	k1gFnSc4
500	[number]	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
narození	narození	k1gNnSc2
Mikuláše	Mikuláš	k1gMnSc2
Koperníka	Koperník	k1gMnSc2
jméno	jméno	k1gNnSc4
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Mikuláše	Mikuláš	k1gMnSc2
Koperníka	Koperník	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>