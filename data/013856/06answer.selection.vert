<s>
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
používána	používat	k5eAaImNgFnS
k	k	k7c3
měření	měření	k1gNnSc3
tlaku	tlak	k1gInSc2
v	v	k7c6
technických	technický	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
ve	v	k7c6
strojírenství	strojírenství	k1gNnSc6
a	a	k8xC
značila	značit	k5eAaImAgFnS
se	se	k3xPyFc4
at	at	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
ata	ata	kA
(	(	kIx(
<g/>
atmosféra	atmosféra	k1gFnSc1
technická	technický	k2eAgFnSc1d1
absolutní	absolutní	k2eAgFnSc1d1
<g/>
)	)	kIx)
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
celkového	celkový	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
atp	atp	kA
(	(	kIx(
<g/>
atmosféra	atmosféra	k1gFnSc1
technická	technický	k2eAgFnSc1d1
přetlaku	přetlak	k1gInSc2
<g/>
)	)	kIx)
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
rozdílu	rozdíl	k1gInSc2
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
parním	parní	k2eAgInSc6d1
kotli	kotel	k1gInSc6
proti	proti	k7c3
okolnímu	okolní	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
vzduchu	vzduch	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>