<s>
Atmosféra	atmosféra	k1gFnSc1
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
dvou	dva	k4xCgFnPc2
zastaralých	zastaralý	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
tlaku	tlak	k1gInSc2
<g/>
,	,	kIx,
nepatřících	patřící	k2eNgInPc2d1
do	do	k7c2
soustavy	soustava	k1gFnSc2
jednotek	jednotka	k1gFnPc2
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
technickou	technický	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
a	a	k8xC
fyzikální	fyzikální	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
<g/>
;	;	kIx,
obě	dva	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
jsou	být	k5eAaImIp3nP
velikostí	velikost	k1gFnSc7
velmi	velmi	k6eAd1
podobné	podobný	k2eAgFnPc1d1
a	a	k8xC
přibližně	přibližně	k6eAd1
se	se	k3xPyFc4
rovnají	rovnat	k5eAaImIp3nP
normálnímu	normální	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
zemské	zemský	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
při	při	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepatří	patřit	k5eNaImIp3nS
mezi	mezi	k7c4
zákonné	zákonný	k2eAgFnPc4d1
měrové	měrový	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
používání	používání	k1gNnSc4
normy	norma	k1gFnSc2
nepřipouštějí	připouštět	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Technická	technický	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Technická	technický	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
používána	používat	k5eAaImNgFnS
k	k	k7c3
měření	měření	k1gNnSc3
tlaku	tlak	k1gInSc2
v	v	k7c6
technických	technický	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
ve	v	k7c6
strojírenství	strojírenství	k1gNnSc6
a	a	k8xC
značila	značit	k5eAaImAgFnS
se	se	k3xPyFc4
at	at	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
ata	ata	kA
(	(	kIx(
<g/>
atmosféra	atmosféra	k1gFnSc1
technická	technický	k2eAgFnSc1d1
absolutní	absolutní	k2eAgFnSc1d1
<g/>
)	)	kIx)
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
celkového	celkový	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
atp	atp	kA
(	(	kIx(
<g/>
atmosféra	atmosféra	k1gFnSc1
technická	technický	k2eAgFnSc1d1
přetlaku	přetlak	k1gInSc2
<g/>
)	)	kIx)
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
rozdílu	rozdíl	k1gInSc2
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
parním	parní	k2eAgInSc6d1
kotli	kotel	k1gInSc6
proti	proti	k7c3
okolnímu	okolní	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
vzduchu	vzduch	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technická	technický	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
hydrostatickému	hydrostatický	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
10	#num#	k4
m	m	kA
vodního	vodní	k2eAgInSc2d1
sloupce	sloupec	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
definována	definován	k2eAgFnSc1d1
přesně	přesně	k6eAd1
převodním	převodní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
na	na	k7c4
jednotku	jednotka	k1gFnSc4
pascal	pascal	k1gInSc4
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
at	at	k?
=	=	kIx~
9,806	9,806	k4
65	#num#	k4
<g/>
×	×	k?
<g/>
104	#num#	k4
Pa	Pa	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
čili	čili	k8xC
přibližně	přibližně	k6eAd1
</s>
<s>
1	#num#	k4
at	at	k?
=	=	kIx~
0,1	0,1	k4
MPa	MPa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
převod	převod	k1gInSc4
na	na	k7c4
fyzikální	fyzikální	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
platí	platit	k5eAaImIp3nS
přibližný	přibližný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
</s>
<s>
1	#num#	k4
at	at	k?
=	=	kIx~
0,967	0,967	k4
84	#num#	k4
atm	atm	k?
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Fyzikální	fyzikální	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
alternativně	alternativně	k6eAd1
nazývala	nazývat	k5eAaImAgFnS
též	též	k9
absolutní	absolutní	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
(	(	kIx(
<g/>
nezaměňovat	zaměňovat	k5eNaImF
s	s	k7c7
atmosférou	atmosféra	k1gFnSc7
technickou	technický	k2eAgFnSc7d1
absolutní	absolutní	k2eAgFnSc7d1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
používána	používat	k5eAaImNgFnS
zejména	zejména	k9
ve	v	k7c6
fyzice	fyzika	k1gFnSc6
a	a	k8xC
přírodních	přírodní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
obecně	obecně	k6eAd1
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
meteorologii	meteorologie	k1gFnSc6
<g/>
,	,	kIx,
geologii	geologie	k1gFnSc6
<g/>
,	,	kIx,
geofyzice	geofyzika	k1gFnSc6
a	a	k8xC
planetologii	planetologie	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
značila	značit	k5eAaImAgFnS
se	se	k3xPyFc4
atm	atm	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
jako	jako	k8xC,k8xS
normální	normální	k2eAgInSc4d1
tlak	tlak	k1gInSc4
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dána	dán	k2eAgFnSc1d1
přesně	přesně	k6eAd1
převodním	převodní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
na	na	k7c4
jednotku	jednotka	k1gFnSc4
pascal	pascal	k1gInSc4
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
atm	atm	k?
=	=	kIx~
101	#num#	k4
325	#num#	k4
Pa	Pa	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
čili	čili	k8xC
přibližně	přibližně	k6eAd1
</s>
<s>
1	#num#	k4
atm	atm	k?
=	=	kIx~
0,1	0,1	k4
MPa	MPa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
převod	převod	k1gInSc4
na	na	k7c4
technickou	technický	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
platí	platit	k5eAaImIp3nS
přibližný	přibližný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
</s>
<s>
1	#num#	k4
atm	atm	k?
=	=	kIx~
1,033	1,033	k4
<g/>
2	#num#	k4
27	#num#	k4
at	at	k?
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
převod	převod	k1gInSc4
na	na	k7c4
jednotku	jednotka	k1gFnSc4
torr	torr	k1gInSc1
platí	platit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
atm	atm	k?
=	=	kIx~
760	#num#	k4
torrů	torr	k1gInPc2
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
