<p>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
reprodukce	reprodukce	k1gFnSc1	reprodukce
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
lékařské	lékařský	k2eAgInPc4d1	lékařský
postupy	postup	k1gInPc4	postup
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
manipulaci	manipulace	k1gFnSc3	manipulace
se	s	k7c7	s
zárodečnými	zárodečný	k2eAgFnPc7d1	zárodečná
buňkami	buňka	k1gFnPc7	buňka
nebo	nebo	k8xC	nebo
s	s	k7c7	s
embryi	embryo	k1gNnPc7	embryo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnPc2	jejich
uchovávání	uchovávání	k1gNnPc2	uchovávání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
léčby	léčba	k1gFnSc2	léčba
neplodnosti	neplodnost	k1gFnSc2	neplodnost
ženy	žena	k1gFnPc1	žena
nebo	nebo	k8xC	nebo
muže	muž	k1gMnPc4	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
reprodukce	reprodukce	k1gFnSc1	reprodukce
(	(	kIx(	(
<g/>
umělé	umělý	k2eAgNnSc1d1	umělé
oplodnění	oplodnění	k1gNnSc1	oplodnění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lékařská	lékařský	k2eAgFnSc1d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
partnerskému	partnerský	k2eAgInSc3d1	partnerský
páru	pár	k1gInSc3	pár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc1	problém
s	s	k7c7	s
otěhotněním	otěhotnění	k1gNnSc7	otěhotnění
<g/>
.	.	kIx.	.
</s>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
reprodukce	reprodukce	k1gFnSc1	reprodukce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
medicínský	medicínský	k2eAgInSc1d1	medicínský
zásah	zásah	k1gInSc1	zásah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
lidskému	lidský	k2eAgNnSc3d1	lidské
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
reprodukce	reprodukce	k1gFnSc1	reprodukce
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
etických	etický	k2eAgInPc2d1	etický
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získal	získat	k5eAaPmAgMnS	získat
britský	britský	k2eAgMnSc1d1	britský
fyziolog	fyziolog	k1gMnSc1	fyziolog
Robert	Robert	k1gMnSc1	Robert
G.	G.	kA	G.
Edwards	Edwards	k1gInSc1	Edwards
za	za	k7c4	za
vývoj	vývoj	k1gInSc4	vývoj
metody	metoda	k1gFnSc2	metoda
oplodnění	oplodnění	k1gNnSc2	oplodnění
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
(	(	kIx(	(
<g/>
in	in	k?	in
vitro	vitro	k6eAd1	vitro
fertilizaci	fertilizace	k1gFnSc4	fertilizace
<g/>
)	)	kIx)	)
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
Ignacio	Ignacio	k1gMnSc1	Ignacio
Carrasco	Carrasco	k1gMnSc1	Carrasco
de	de	k?	de
Paula	Paula	k1gFnSc1	Paula
udělení	udělení	k1gNnPc2	udělení
ceny	cena	k1gFnSc2	cena
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gNnSc4	jeho
ústní	ústní	k2eAgNnSc4d1	ústní
prohlášení	prohlášení	k1gNnSc4	prohlášení
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
Edwardse	Edwards	k1gMnPc4	Edwards
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pochopitelnou	pochopitelný	k2eAgFnSc4d1	pochopitelná
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc4	nic
proti	proti	k7c3	proti
metodám	metoda	k1gFnPc3	metoda
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
léčí	léčit	k5eAaImIp3nP	léčit
neplodnost	neplodnost	k1gFnSc4	neplodnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
metody	metoda	k1gFnPc4	metoda
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
umělém	umělý	k2eAgNnSc6d1	umělé
oplodnění	oplodnění	k1gNnSc6	oplodnění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nedůstojné	důstojný	k2eNgFnSc2d1	nedůstojná
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělé	umělý	k2eAgNnSc1d1	umělé
oplodnění	oplodnění	k1gNnSc1	oplodnění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
česká	český	k2eAgFnSc1d1	Česká
legislativa	legislativa	k1gFnSc1	legislativa
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stala	stát	k5eAaPmAgFnS	stát
velmoc	velmoc	k1gFnSc4	velmoc
pro	pro	k7c4	pro
poskytování	poskytování	k1gNnSc4	poskytování
umělého	umělý	k2eAgNnSc2d1	umělé
oplodnění	oplodnění	k1gNnSc4	oplodnění
cizincům	cizinec	k1gMnPc3	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
několik	několik	k4yIc1	několik
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministryně	ministryně	k1gFnSc1	ministryně
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Michaela	Michaela	k1gFnSc1	Michaela
Marksová	Marksová	k1gFnSc1	Marksová
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c4	o
umělé	umělý	k2eAgNnSc4d1	umělé
oplodnění	oplodnění	k1gNnSc4	oplodnění
mohly	moct	k5eAaImAgFnP	moct
požádat	požádat	k5eAaPmF	požádat
ženy	žena	k1gFnPc1	žena
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
toto	tento	k3xDgNnSc1	tento
umožňující	umožňující	k2eAgMnSc1d1	umožňující
však	však	k8xC	však
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
neschválila	schválit	k5eNaPmAgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
navrhován	navrhovat	k5eAaImNgInS	navrhovat
i	i	k8xC	i
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
zákonnou	zákonný	k2eAgFnSc4d1	zákonná
podmínku	podmínka	k1gFnSc4	podmínka
souhlasu	souhlas	k1gInSc2	souhlas
partnera	partner	k1gMnSc4	partner
obcházejí	obcházet	k5eAaImIp3nP	obcházet
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
přivedou	přivést	k5eAaPmIp3nP	přivést
jakéhokoliv	jakýkoliv	k3yIgMnSc4	jakýkoliv
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc1	jejich
partner	partner	k1gMnSc1	partner
<g/>
,	,	kIx,	,
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
jim	on	k3xPp3gMnPc3	on
souhlas	souhlas	k1gInSc4	souhlas
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kliniky	klinika	k1gFnPc1	klinika
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
podepisující	podepisující	k2eAgInSc4d1	podepisující
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
oplodněním	oplodnění	k1gNnSc7	oplodnění
skutečně	skutečně	k6eAd1	skutečně
ženiným	ženin	k2eAgMnSc7d1	ženin
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
ministr	ministr	k1gMnSc1	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Miloslav	Miloslav	k1gMnSc1	Miloslav
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
by	by	kYmCp3nS	by
stát	stát	k1gInSc1	stát
měl	mít	k5eAaImAgInS	mít
preferovat	preferovat	k5eAaImF	preferovat
úplné	úplný	k2eAgFnPc4d1	úplná
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
mimo	mimo	k7c4	mimo
manželství	manželství	k1gNnSc4	manželství
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
polovina	polovina	k1gFnSc1	polovina
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Řežábek	Řežábek	k1gMnSc1	Řežábek
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Léčba	léčba	k1gFnSc1	léčba
neplodnosti	neplodnost	k1gFnSc2	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-247-2103-3	[number]	k4	978-80-247-2103-3
</s>
</p>
<p>
<s>
Doherty	Dohert	k1gInPc1	Dohert
<g/>
,	,	kIx,	,
C.	C.	kA	C.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
M.	M.	kA	M.
M.	M.	kA	M.
Léčba	léčba	k1gFnSc1	léčba
neplodnosti	neplodnost	k1gFnSc2	neplodnost
<g/>
:	:	kIx,	:
podrobný	podrobný	k2eAgMnSc1d1	podrobný
rádce	rádce	k1gMnSc1	rádce
pro	pro	k7c4	pro
neplodné	plodný	k2eNgInPc4d1	neplodný
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-251-0771-X	[number]	k4	80-251-0771-X
</s>
</p>
<p>
<s>
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Etické	etický	k2eAgInPc1d1	etický
a	a	k8xC	a
právní	právní	k2eAgInPc1d1	právní
aspekty	aspekt	k1gInPc1	aspekt
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
1700	[number]	k4	1700
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falcone	Falcon	k1gMnSc5	Falcon
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
,	,	kIx,	,
Hurd	hurda	k1gFnPc2	hurda
<g/>
,	,	kIx,	,
W.	W.	kA	W.
Clinical	Clinical	k1gFnSc1	Clinical
reproductive	reproductiv	k1gInSc5	reproductiv
medicine	medicinout	k5eAaPmIp3nS	medicinout
and	and	k?	and
surgery	surger	k1gInPc4	surger
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Elsevier	Elsevier	k1gInSc1	Elsevier
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Reprodukční	reprodukční	k2eAgFnSc1d1	reprodukční
technologie	technologie	k1gFnSc1	technologie
</s>
</p>
<p>
<s>
Ovulace	ovulace	k1gFnSc1	ovulace
</s>
</p>
<p>
<s>
Plodné	plodný	k2eAgNnSc1d1	plodné
období	období	k1gNnSc1	období
</s>
</p>
<p>
<s>
Neplodnost	neplodnost	k1gFnSc1	neplodnost
</s>
</p>
<p>
<s>
Maria	Maria	k1gFnSc1	Maria
del	del	k?	del
Carmen	Carmen	k1gInSc1	Carmen
Bousada	Bousada	k1gFnSc1	Bousada
de	de	k?	de
Lara	Lar	k1gInSc2	Lar
–	–	k?	–
nejstarší	starý	k2eAgFnSc1d3	nejstarší
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
umělému	umělý	k2eAgNnSc3d1	umělé
oplodnění	oplodnění	k1gNnSc3	oplodnění
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
</s>
</p>
