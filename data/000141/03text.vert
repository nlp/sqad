<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
ár-í-em	ár-ím	k6eAd1	ár-í-em
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Athens	Athensa	k1gFnPc2	Athensa
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
založili	založit	k5eAaPmAgMnP	založit
bubeník	bubeník	k1gMnSc1	bubeník
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Peter	Peter	k1gMnSc1	Peter
Buck	Buck	k1gMnSc1	Buck
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Mike	Mik	k1gFnSc2	Mik
Mills	Mills	k1gInSc1	Mills
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gMnSc5	Stip
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
kapely	kapela	k1gFnPc4	kapela
hrající	hrající	k2eAgInSc4d1	hrající
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc4	radio
Free	Fre	k1gFnSc2	Fre
Europe	Europ	k1gInSc5	Europ
<g/>
"	"	kIx"	"
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
u	u	k7c2	u
labelu	label	k1gInSc2	label
Hib-Tone	Hib-Ton	k1gInSc5	Hib-Ton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
následovalo	následovat	k5eAaImAgNnS	následovat
vydání	vydání	k1gNnSc1	vydání
EP	EP	kA	EP
Chronic	Chronice	k1gFnPc2	Chronice
Town	Towna	k1gFnPc2	Towna
u	u	k7c2	u
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
alba	album	k1gNnSc2	album
Murmur	Murmura	k1gFnPc2	Murmura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
velice	velice	k6eAd1	velice
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
reakcí	reakce	k1gFnPc2	reakce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
hudebních	hudební	k2eAgMnPc2d1	hudební
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
díky	díky	k7c3	díky
dalším	další	k2eAgMnPc3d1	další
albům	album	k1gNnPc3	album
úspěchů	úspěch	k1gInPc2	úspěch
na	na	k7c6	na
alternativní	alternativní	k2eAgFnSc6d1	alternativní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
mainstreamu	mainstream	k1gInSc2	mainstream
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
One	One	k1gFnSc2	One
I	i	k9	i
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Records	Records	k6eAd1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vydali	vydat	k5eAaPmAgMnP	vydat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
označováni	označován	k2eAgMnPc1d1	označován
za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
alba	album	k1gNnPc1	album
Out	Out	k1gMnSc2	Out
of	of	k?	of
Time	Tim	k1gMnSc2	Tim
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
zvuk	zvuk	k1gInSc1	zvuk
se	se	k3xPyFc4	se
však	však	k9	však
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
doposavadních	doposavadní	k2eAgNnPc2d1	doposavadní
alb	album	k1gNnPc2	album
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
albem	album	k1gNnSc7	album
Monster	monstrum	k1gNnPc2	monstrum
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
vrátili	vrátit	k5eAaPmAgMnP	vrátit
k	k	k7c3	k
více	hodně	k6eAd2	hodně
rockovému	rockový	k2eAgInSc3d1	rockový
zvuku	zvuk	k1gInSc3	zvuk
a	a	k8xC	a
po	po	k7c6	po
šestileté	šestiletý	k2eAgFnSc6d1	šestiletá
odmlce	odmlka	k1gFnSc6	odmlka
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
potíže	potíž	k1gFnPc1	potíž
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
za	za	k7c2	za
tehdy	tehdy	k6eAd1	tehdy
rekordních	rekordní	k2eAgInPc2d1	rekordní
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
kapelu	kapela	k1gFnSc4	kapela
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
opustil	opustit	k5eAaPmAgMnS	opustit
bubeník	bubeník	k1gMnSc1	bubeník
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tříčlenná	tříčlenný	k2eAgFnSc1d1	tříčlenná
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
hráli	hrát	k5eAaImAgMnP	hrát
a	a	k8xC	a
vydávali	vydávat	k5eAaPmAgMnP	vydávat
desky	deska	k1gFnPc4	deska
i	i	k9	i
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgInP	setkat
se	s	k7c7	s
smíšenými	smíšený	k2eAgFnPc7d1	smíšená
reakcemi	reakce	k1gFnPc7	reakce
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
také	také	k9	také
aktivně	aktivně	k6eAd1	aktivně
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
politických	politický	k2eAgFnPc6d1	politická
a	a	k8xC	a
společenských	společenský	k2eAgNnPc6d1	společenské
hnutích	hnutí	k1gNnPc6	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byli	být	k5eAaImAgMnP	být
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
a	a	k8xC	a
Peter	Petra	k1gFnPc2	Petra
Buck	Bucko	k1gNnPc2	Bucko
potkali	potkat	k5eAaPmAgMnP	potkat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
nahrávkami	nahrávka	k1gFnPc7	nahrávka
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Buck	Buck	k1gMnSc1	Buck
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
líbí	líbit	k5eAaImIp3nS	líbit
podobná	podobný	k2eAgFnSc1d1	podobná
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
punk	punk	k1gMnSc1	punk
rock	rock	k1gInSc1	rock
a	a	k8xC	a
protopunk	protopunk	k6eAd1	protopunk
reprezentované	reprezentovaný	k2eAgFnPc1d1	reprezentovaná
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
Patti	Patt	k1gMnPc1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Television	Television	k1gInSc1	Television
a	a	k8xC	a
The	The	k1gFnSc1	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Stipe	Stipat	k5eAaPmIp3nS	Stipat
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
kupoval	kupovat	k5eAaImAgMnS	kupovat
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
Buck	Buck	k1gMnSc1	Buck
<g/>
)	)	kIx)	)
odkládal	odkládat	k5eAaImAgMnS	odkládat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stipe	Stip	k1gMnSc5	Stip
a	a	k8xC	a
Buck	Buck	k1gInSc4	Buck
později	pozdě	k6eAd2	pozdě
potkali	potkat	k5eAaPmAgMnP	potkat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
párty	párty	k1gFnSc6	párty
studenty	student	k1gMnPc4	student
University	universita	k1gFnSc2	universita
of	of	k?	of
Georgia	Georgius	k1gMnSc2	Georgius
Mikea	Mikeus	k1gMnSc2	Mikeus
Millse	Mills	k1gMnSc2	Mills
a	a	k8xC	a
Billa	Bill	k1gMnSc2	Bill
Berryho	Berry	k1gMnSc2	Berry
kteří	který	k3yQgMnPc1	který
spolu	spolu	k6eAd1	spolu
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
už	už	k6eAd1	už
od	od	k7c2	od
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kvartet	kvartet	k1gInSc1	kvartet
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
napíšou	napsat	k5eAaBmIp3nP	napsat
několik	několik	k4yIc4	několik
písniček	písnička	k1gFnPc2	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Stipe	Stipat	k5eAaPmIp3nS	Stipat
později	pozdě	k6eAd2	pozdě
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgInSc4	žádný
velký	velký	k2eAgInSc4d1	velký
plán	plán	k1gInSc4	plán
a	a	k8xC	a
pokus	pokus	k1gInSc4	pokus
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
.	.	kIx.	.
</s>
<s>
Nepojmenovaná	pojmenovaný	k2eNgFnSc1d1	nepojmenovaná
kapela	kapela	k1gFnSc1	kapela
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
cvičila	cvičit	k5eAaImAgFnS	cvičit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
narozeninové	narozeninový	k2eAgFnSc6d1	narozeninová
party	party	k1gFnSc6	party
jejich	jejich	k3xOp3gMnSc2	jejich
kamaráda	kamarád	k1gMnSc2	kamarád
v	v	k7c6	v
episkopálním	episkopální	k2eAgInSc6d1	episkopální
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvažování	zvažování	k1gNnSc6	zvažování
jmen	jméno	k1gNnPc2	jméno
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Twisted	Twisted	k1gMnSc1	Twisted
Kites	Kites	k1gMnSc1	Kites
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cans	Cans	k1gInSc1	Cans
of	of	k?	of
Piss	Piss	k1gInSc1	Piss
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Negro	Negro	k1gNnSc1	Negro
Wives	Wivesa	k1gFnPc2	Wivesa
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
nakonec	nakonec	k6eAd1	nakonec
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stipe	Stip	k1gInSc5	Stip
zabořil	zabořit	k5eAaPmAgInS	zabořit
náhodně	náhodně	k6eAd1	náhodně
prst	prst	k1gInSc1	prst
do	do	k7c2	do
slovníku	slovník	k1gInSc2	slovník
právě	právě	k9	právě
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
opustili	opustit	k5eAaPmAgMnP	opustit
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaPmF	věnovat
kapele	kapela	k1gFnSc3	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
manažerem	manažer	k1gMnSc7	manažer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jefferson	Jefferson	k1gMnSc1	Jefferson
Holt	Holt	k?	Holt
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
vystoupením	vystoupení	k1gNnSc7	vystoupení
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
domovském	domovský	k2eAgNnSc6d1	domovské
městě	město	k1gNnSc6	město
Chapel	Chapel	k1gMnSc1	Chapel
Hill	Hill	k1gMnSc1	Hill
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
tak	tak	k6eAd1	tak
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Athnes	Athnesa	k1gFnPc2	Athnesa
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
v	v	k7c4	v
Athens	Athens	k1gInSc4	Athens
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
začalo	začít	k5eAaPmAgNnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
negativnímu	negativní	k2eAgInSc3d1	negativní
postoji	postoj	k1gInSc3	postoj
mnohých	mnohý	k2eAgMnPc2d1	mnohý
místních	místní	k2eAgMnPc2d1	místní
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalšího	další	k2eAgInSc2d1	další
roku	rok	k1gInSc2	rok
a	a	k8xC	a
půl	půl	k1xP	půl
již	již	k6eAd1	již
kapela	kapela	k1gFnSc1	kapela
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
turné	turné	k1gNnSc1	turné
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
skromné	skromný	k2eAgNnSc1d1	skromné
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
cestovala	cestovat	k5eAaImAgFnS	cestovat
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
dodávce	dodávka	k1gFnSc6	dodávka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
řídil	řídit	k5eAaImAgInS	řídit
Holt	Holt	k?	Holt
<g/>
,	,	kIx,	,
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
mohli	moct	k5eAaImAgMnP	moct
za	za	k7c4	za
jídlo	jídlo	k1gNnSc4	jídlo
utratit	utratit	k5eAaPmF	utratit
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
dolary	dolar	k1gInPc4	dolar
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1981	[number]	k4	1981
nahráli	nahrát	k5eAaBmAgMnP	nahrát
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc4	radio
Free	Fre	k1gFnSc2	Fre
Europe	Europ	k1gInSc5	Europ
<g/>
"	"	kIx"	"
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Mitche	Mitch	k1gMnSc2	Mitch
Eastera	Easter	k1gMnSc2	Easter
ve	v	k7c6	v
Winston-Salem	Winston-Sal	k1gMnSc7	Winston-Sal
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
místním	místní	k2eAgInSc7d1	místní
nezávislým	závislý	k2eNgInSc7d1	nezávislý
labelem	label	k1gInSc7	label
Hib-Tone	Hib-Ton	k1gInSc5	Hib-Ton
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
1	[number]	k4	1
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
okamžitě	okamžitě	k6eAd1	okamžitě
vyprodán	vyprodat	k5eAaPmNgInS	vyprodat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nedostatkovým	dostatkový	k2eNgNnSc7d1	nedostatkové
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
kritici	kritik	k1gMnPc1	kritik
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
nešetřili	šetřit	k5eNaImAgMnP	šetřit
chválou	chvála	k1gFnSc7	chvála
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
ho	on	k3xPp3gMnSc4	on
zařadili	zařadit	k5eAaPmAgMnP	zařadit
mezi	mezi	k7c7	mezi
10	[number]	k4	10
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
singlů	singl	k1gInPc2	singl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
Village	Village	k1gNnSc2	Village
Choice	Choice	k1gFnSc2	Choice
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
singl	singl	k1gInSc4	singl
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1981	[number]	k4	1981
nahráli	nahrát	k5eAaBmAgMnP	nahrát
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mitchem	Mitch	k1gMnSc7	Mitch
Easterem	Easter	k1gMnSc7	Easter
EP	EP	kA	EP
Chronic	Chronice	k1gFnPc2	Chronice
Town	Towna	k1gFnPc2	Towna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
vydané	vydaný	k2eAgInPc4d1	vydaný
u	u	k7c2	u
Hib-Tone	Hib-Ton	k1gInSc5	Hib-Ton
<g/>
.	.	kIx.	.
</s>
<s>
Demo	demo	k2eAgFnPc1d1	demo
nahrávky	nahrávka	k1gFnPc1	nahrávka
se	se	k3xPyFc4	se
však	však	k9	však
dostalo	dostat	k5eAaPmAgNnS	dostat
k	k	k7c3	k
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
podepsala	podepsat	k5eAaPmAgFnS	podepsat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1982	[number]	k4	1982
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
upřednostnili	upřednostnit	k5eAaPmAgMnP	upřednostnit
před	před	k7c7	před
nabídkou	nabídka	k1gFnSc7	nabídka
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc6d2	veliký
společnosti	společnost	k1gFnSc6	společnost
RCA	RCA	kA	RCA
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Chronic	Chronice	k1gFnPc2	Chronice
Town	Town	k1gNnSc1	Town
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
v	v	k7c6	v
NME	NME	kA	NME
ocenila	ocenit	k5eAaPmAgFnS	ocenit
mystický	mystický	k2eAgInSc4d1	mystický
nádech	nádech	k1gInSc4	nádech
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
zní	znět	k5eAaImIp3nS	znět
opravdově	opravdově	k6eAd1	opravdově
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
skvělé	skvělý	k2eAgNnSc1d1	skvělé
slyšet	slyšet	k5eAaImF	slyšet
něco	něco	k3yInSc4	něco
tak	tak	k6eAd1	tak
nenuceného	nucený	k2eNgNnSc2d1	nenucené
a	a	k8xC	a
chytrého	chytrý	k2eAgNnSc2d1	chytré
jako	jako	k9	jako
toto	tento	k3xDgNnSc4	tento
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
přidělila	přidělit	k5eAaPmAgFnS	přidělit
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
producenta	producent	k1gMnSc2	producent
Stephena	Stephen	k1gMnSc2	Stephen
Haugea	Haugeus	k1gMnSc2	Haugeus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
nahrál	nahrát	k5eAaPmAgMnS	nahrát
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
přílišný	přílišný	k2eAgInSc1d1	přílišný
technický	technický	k2eAgInSc1d1	technický
perfekcionalismus	perfekcionalismus	k1gInSc1	perfekcionalismus
kapelu	kapela	k1gFnSc4	kapela
neoslovil	oslovit	k5eNaPmAgMnS	oslovit
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
požádala	požádat	k5eAaPmAgFnS	požádat
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
nahrát	nahrát	k5eAaBmF	nahrát
album	album	k1gNnSc4	album
s	s	k7c7	s
Easterem	Easter	k1gInSc7	Easter
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
nechala	nechat	k5eAaPmAgFnS	nechat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
nahrát	nahrát	k5eAaPmF	nahrát
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Pilgrimage	Pilgrimage	k1gFnSc4	Pilgrimage
<g/>
"	"	kIx"	"
s	s	k7c7	s
Easterem	Easter	k1gMnSc7	Easter
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
kolegou	kolega	k1gMnSc7	kolega
Donem	Don	k1gMnSc7	Don
Dixonem	Dixon	k1gMnSc7	Dixon
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
po	po	k7c6	po
poslechu	poslech	k1gInSc6	poslech
této	tento	k3xDgFnSc2	tento
nahrávky	nahrávka	k1gFnSc2	nahrávka
dovolilo	dovolit	k5eAaPmAgNnS	dovolit
kapele	kapela	k1gFnSc3	kapela
nahrát	nahrát	k5eAaBmF	nahrát
album	album	k1gNnSc4	album
s	s	k7c7	s
Easterem	Easter	k1gMnSc7	Easter
a	a	k8xC	a
Dixonem	Dixon	k1gMnSc7	Dixon
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
do	do	k7c2	do
alba	album	k1gNnSc2	album
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
kytarová	kytarový	k2eAgNnPc4d1	kytarové
sóla	sólo	k1gNnPc4	sólo
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
populární	populární	k2eAgInSc4d1	populární
syntetizátory	syntetizátor	k1gInPc4	syntetizátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
nadčasový	nadčasový	k2eAgInSc4d1	nadčasový
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Murmur	Murmura	k1gFnPc2	Murmura
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
pozitivního	pozitivní	k2eAgMnSc2d1	pozitivní
přijetí	přijetí	k1gNnSc1	přijetí
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
jej	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nahrávkou	nahrávka	k1gFnSc7	nahrávka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
nahraná	nahraný	k2eAgFnSc1d1	nahraná
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jako	jako	k9	jako
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
a	a	k8xC	a
na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
singlů	singl	k1gInPc2	singl
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
78	[number]	k4	78
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
mnohá	mnohý	k2eAgNnPc4d1	mnohé
ocenění	ocenění	k1gNnPc4	ocenění
se	se	k3xPyFc4	se
alba	alba	k1gFnSc1	alba
Murmur	Murmura	k1gFnPc2	Murmura
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
200	[number]	k4	200
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
očekávala	očekávat	k5eAaImAgFnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
poprvé	poprvé	k6eAd1	poprvé
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
celostátní	celostátní	k2eAgFnSc6d1	celostátní
televizi	televize	k1gFnSc6	televize
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1983	[number]	k4	1983
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Late	lat	k1gInSc5	lat
Night	Night	k1gMnSc1	Night
with	witha	k1gFnPc2	witha
David	David	k1gMnSc1	David
Letterman	Letterman	k1gMnSc1	Letterman
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahráli	zahrát	k5eAaPmAgMnP	zahrát
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc4	radio
Free	Fre	k1gFnSc2	Fre
Europe	Europ	k1gInSc5	Europ
<g/>
"	"	kIx"	"
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
nepojmenovanou	pojmenovaný	k2eNgFnSc4d1	nepojmenovaná
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
později	pozdě	k6eAd2	pozdě
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
"	"	kIx"	"
<g/>
So	So	kA	So
<g/>
.	.	kIx.	.
</s>
<s>
Central	Centrat	k5eAaImAgMnS	Centrat
Rain	Rain	k1gMnSc1	Rain
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Sorry	sorry	k9	sorry
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
následujícího	následující	k2eAgNnSc2d1	následující
alba	album	k1gNnSc2	album
Reckoning	Reckoning	k1gInSc1	Reckoning
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
opět	opět	k6eAd1	opět
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
Easter	Easter	k1gMnSc1	Easter
a	a	k8xC	a
Dixon	Dixon	k1gMnSc1	Dixon
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
recenzí	recenze	k1gFnPc2	recenze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
časopis	časopis	k1gInSc1	časopis
NME	NME	kA	NME
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reckoning	Reckoning	k1gInSc1	Reckoning
"	"	kIx"	"
<g/>
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
vzrušujících	vzrušující	k2eAgFnPc2d1	vzrušující
kapel	kapela	k1gFnPc2	kapela
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
poprvé	poprvé	k6eAd1	poprvé
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Reckoning	Reckoning	k1gInSc4	Reckoning
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
za	za	k7c7	za
oceánem	oceán	k1gInSc7	oceán
se	se	k3xPyFc4	se
albu	album	k1gNnSc6	album
kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
distribuci	distribuce	k1gFnSc3	distribuce
a	a	k8xC	a
malému	malý	k2eAgInSc3d1	malý
prostoru	prostor	k1gInSc3	prostor
věnovanému	věnovaný	k2eAgInSc3d1	věnovaný
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
jen	jen	k9	jen
na	na	k7c4	na
91	[number]	k4	91
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Fables	Fables	k1gInSc1	Fables
of	of	k?	of
the	the	k?	the
Reconstruction	Reconstruction	k1gInSc1	Reconstruction
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
obrat	obrat	k1gInSc4	obrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
Dixona	Dixon	k1gMnSc2	Dixon
a	a	k8xC	a
Eastera	Easter	k1gMnSc2	Easter
produkoval	produkovat	k5eAaImAgMnS	produkovat
album	album	k1gNnSc4	album
Joe	Joe	k1gMnSc1	Joe
Boyd	Boyd	k1gMnSc1	Boyd
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Fairport	Fairport	k1gInSc4	Fairport
Convention	Convention	k1gInSc1	Convention
a	a	k8xC	a
Nickem	Nicek	k1gMnSc7	Nicek
Drakem	drak	k1gMnSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
nahrávání	nahrávání	k1gNnSc1	nahrávání
extrémně	extrémně	k6eAd1	extrémně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
chladnému	chladný	k2eAgNnSc3d1	chladné
zimnímu	zimní	k2eAgNnSc3d1	zimní
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
mizernému	mizerný	k2eAgNnSc3d1	mizerné
jídlu	jídlo	k1gNnSc3	jídlo
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Pochmurná	pochmurný	k2eAgFnSc1d1	pochmurná
atmosféra	atmosféra	k1gFnSc1	atmosféra
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
se	se	k3xPyFc4	se
promítla	promítnout	k5eAaPmAgFnS	promítnout
i	i	k9	i
do	do	k7c2	do
alba	album	k1gNnSc2	album
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Stipe	Stipat	k5eAaPmIp3nS	Stipat
psal	psát	k5eAaImAgInS	psát
texty	text	k1gInPc4	text
vycházející	vycházející	k2eAgFnSc4d1	vycházející
z	z	k7c2	z
jižanské	jižanský	k2eAgFnSc2d1	jižanská
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
"	"	kIx"	"
<g/>
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
starých	starý	k2eAgInPc6d1	starý
lidech	lid	k1gInPc6	lid
sedících	sedící	k2eAgInPc6d1	sedící
okolo	okolo	k7c2	okolo
ohně	oheň	k1gInSc2	oheň
předávajíce	předávat	k5eAaImSgFnP	předávat
legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
pohádky	pohádka	k1gFnPc1	pohádka
svým	svůj	k3xOyFgInSc7	svůj
vnoučatům	vnouče	k1gNnPc3	vnouče
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Fables	Fables	k1gInSc1	Fables
of	of	k?	of
the	the	k?	the
Reconstruction	Reconstruction	k1gInSc1	Reconstruction
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
albem	album	k1gNnSc7	album
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Records	k1gInSc4	Records
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
však	však	k9	však
albu	album	k1gNnSc3	album
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
ohlasy	ohlas	k1gInPc1	ohlas
kritiky	kritika	k1gFnSc2	kritika
byly	být	k5eAaImAgInP	být
tentokrát	tentokrát	k6eAd1	tentokrát
smíšené	smíšený	k2eAgInPc1d1	smíšený
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
albu	alba	k1gFnSc4	alba
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
pochmurné	pochmurný	k2eAgNnSc1d1	pochmurné
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
nahrané	nahraný	k2eAgInPc1d1	nahraný
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předchozího	předchozí	k2eAgNnSc2d1	předchozí
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
i	i	k9	i
singly	singl	k1gInPc4	singl
z	z	k7c2	z
Fables	Fablesa	k1gFnPc2	Fablesa
of	of	k?	of
the	the	k?	the
Reconstruction	Reconstruction	k1gInSc1	Reconstruction
prakticky	prakticky	k6eAd1	prakticky
zcela	zcela	k6eAd1	zcela
ignorovány	ignorován	k2eAgInPc4d1	ignorován
mainstreamovými	mainstreamový	k2eAgInPc7d1	mainstreamový
rádii	rádius	k1gInPc7	rádius
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Records	k1gInSc4	Records
tak	tak	k6eAd1	tak
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
frustrováno	frustrován	k2eAgNnSc1d1	frustrováno
neochotou	neochota	k1gFnSc7	neochota
kapely	kapela	k1gFnSc2	kapela
prorazit	prorazit	k5eAaPmF	prorazit
i	i	k9	i
do	do	k7c2	do
mainstreamu	mainstream	k1gInSc2	mainstream
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Don	Don	k1gMnSc1	Don
Genham	Genham	k1gInSc4	Genham
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Lifes	Lifes	k1gMnSc1	Lifes
Rich	Rich	k1gMnSc1	Rich
Pageant	Pageant	k1gMnSc1	Pageant
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
přístupnější	přístupný	k2eAgNnSc1d2	přístupnější
i	i	k9	i
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
mimo	mimo	k7c4	mimo
univerzitní	univerzitní	k2eAgNnPc4d1	univerzitní
rádia	rádio	k1gNnPc4	rádio
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Stipeův	Stipeův	k2eAgInSc1d1	Stipeův
mumlavý	mumlavý	k2eAgInSc1d1	mumlavý
charakter	charakter	k1gInSc1	charakter
zpěvu	zpěv	k1gInSc2	zpěv
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
celkově	celkově	k6eAd1	celkově
pozitivnější	pozitivní	k2eAgMnSc1d2	pozitivnější
než	než	k8xS	než
Fables	Fables	k1gInSc1	Fables
of	of	k?	of
Reconstruction	Reconstruction	k1gInSc1	Reconstruction
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Peter	Peter	k1gMnSc1	Peter
Buck	Buck	k1gMnSc1	Buck
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Chicago	Chicago	k1gNnSc4	Chicago
Tribune	tribun	k1gMnSc5	tribun
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Michael	Michael	k1gMnSc1	Michael
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
jistější	jistý	k2eAgMnSc1d2	jistější
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
charakteru	charakter	k1gInSc6	charakter
jeho	on	k3xPp3gInSc2	on
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lifes	Lifes	k1gMnSc1	Lifes
Rich	Rich	k1gMnSc1	Rich
Pageant	Pageant	k1gMnSc1	Pageant
překonalo	překonat	k5eAaPmAgNnS	překonat
své	svůj	k3xOyFgMnPc4	svůj
předchůdce	předchůdce	k1gMnPc4	předchůdce
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Fall	Fall	k1gInSc4	Fall
on	on	k3xPp3gMnSc1	on
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
hrán	hrát	k5eAaImNgMnS	hrát
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
komerčních	komerční	k2eAgNnPc6d1	komerční
rádiích	rádio	k1gNnPc6	rádio
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
šlo	jít	k5eAaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
ta	ten	k3xDgNnPc4	ten
méně	málo	k6eAd2	málo
významná	významný	k2eAgNnPc4d1	významné
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
mělo	mít	k5eAaImAgNnS	mít
album	album	k1gNnSc4	album
stále	stále	k6eAd1	stále
na	na	k7c6	na
univerzitních	univerzitní	k2eAgNnPc6d1	univerzitní
rádiích	rádio	k1gNnPc6	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
alba	album	k1gNnSc2	album
Lifes	Lifesa	k1gFnPc2	Lifesa
Rich	Rich	k1gMnSc1	Rich
Pageant	Pageant	k1gMnSc1	Pageant
se	se	k3xPyFc4	se
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Records	k1gInSc4	Records
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
album	album	k1gNnSc4	album
Dead	Deada	k1gFnPc2	Deada
Letter	Lettra	k1gFnPc2	Lettra
Office	Office	kA	Office
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
především	především	k9	především
písně	píseň	k1gFnPc1	píseň
obsažené	obsažený	k2eAgFnPc1d1	obsažená
jako	jako	k8xS	jako
B-strany	Btrana	k1gFnPc1	B-strana
na	na	k7c6	na
doposud	doposud	k6eAd1	doposud
vydaných	vydaný	k2eAgInPc6d1	vydaný
singlech	singl	k1gInPc6	singl
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
vydala	vydat	k5eAaPmAgFnS	vydat
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Records	k1gInSc1	Records
všechny	všechen	k3xTgMnPc4	všechen
doposud	doposud	k6eAd1	doposud
nahrané	nahraný	k2eAgInPc1d1	nahraný
videoklipy	videoklip	k1gInPc1	videoklip
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
Wolves	Wolves	k1gMnSc1	Wolves
<g/>
,	,	kIx,	,
Lower	Lower	k1gMnSc1	Lower
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Succumbs	Succumbsa	k1gFnPc2	Succumbsa
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Genham	Genham	k1gInSc4	Genham
neprodukoval	produkovat	k5eNaImAgMnS	produkovat
páté	pátý	k4xOgNnSc4	pátý
album	album	k1gNnSc4	album
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
a	a	k8xC	a
kapele	kapela	k1gFnSc3	kapela
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
za	za	k7c2	za
producenta	producent	k1gMnSc2	producent
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Scotta	Scotta	k1gFnSc1	Scotta
Litta	Litt	k1gMnSc2	Litt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
nakonec	nakonec	k6eAd1	nakonec
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
pěti	pět	k4xCc6	pět
albech	album	k1gNnPc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Document	Document	k1gMnSc1	Document
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
některé	některý	k3yIgMnPc4	některý
otevřeně	otevřeně	k6eAd1	otevřeně
politické	politický	k2eAgFnPc4d1	politická
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Occupation	Occupation	k1gInSc1	Occupation
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Exhuming	Exhuming	k1gInSc1	Exhuming
McCarthy	McCartha	k1gFnSc2	McCartha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
reagovaly	reagovat	k5eAaBmAgInP	reagovat
na	na	k7c4	na
konzervativní	konzervativní	k2eAgNnSc4d1	konzervativní
politické	politický	k2eAgNnSc4d1	politické
prostředí	prostředí	k1gNnSc4	prostředí
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Pareles	Pareles	k1gMnSc1	Pareles
z	z	k7c2	z
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
napsal	napsat	k5eAaPmAgInS	napsat
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Document	Document	k1gInSc1	Document
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
sebevědomé	sebevědomý	k2eAgNnSc1d1	sebevědomé
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
vyzývavé	vyzývavý	k2eAgNnSc4d1	vyzývavé
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
kultovní	kultovní	k2eAgFnSc2d1	kultovní
kapely	kapela	k1gFnSc2	kapela
stát	stát	k5eAaImF	stát
kapelou	kapela	k1gFnSc7	kapela
masově	masově	k6eAd1	masově
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Document	Document	k1gInSc1	Document
bylo	být	k5eAaImAgNnS	být
opravdu	opravdu	k6eAd1	opravdu
průlomové	průlomový	k2eAgNnSc1d1	průlomové
album	album	k1gNnSc1	album
a	a	k8xC	a
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
I	i	k9	i
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
mezi	mezi	k7c4	mezi
dvacet	dvacet	k4xCc4	dvacet
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
jak	jak	k8xC	jak
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1988	[number]	k4	1988
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
milion	milion	k4xCgInSc1	milion
kopií	kopie	k1gFnPc2	kopie
alba	album	k1gNnSc2	album
Document	Document	k1gMnSc1	Document
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1987	[number]	k4	1987
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
označeni	označit	k5eAaPmNgMnP	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
americkou	americký	k2eAgFnSc4d1	americká
rock	rock	k1gInSc1	rock
&	&	k?	&
rollovou	rollový	k2eAgFnSc4d1	rollová
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nespokojení	nespokojení	k1gNnSc1	nespokojení
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
úspěchy	úspěch	k1gInPc7	úspěch
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
neprodloužit	prodloužit	k5eNaPmF	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
podepsali	podepsat	k5eAaPmAgMnP	podepsat
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Records	Records	k6eAd1	Records
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vydala	vydat	k5eAaPmAgFnS	vydat
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
kompilaci	kompilace	k1gFnSc3	kompilace
Eponymous	Eponymous	k1gInSc1	Eponymous
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
singly	singl	k1gInPc4	singl
a	a	k8xC	a
raritní	raritní	k2eAgFnPc4d1	raritní
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vydali	vydat	k5eAaPmAgMnP	vydat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
u	u	k7c2	u
Warner	Warnra	k1gFnPc2	Warnra
Bros	Bros	k1gInSc4	Bros
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Green	Green	k1gInSc4	Green
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
nahrané	nahraný	k2eAgNnSc1d1	nahrané
v	v	k7c6	v
Nashvillu	Nashvillo	k1gNnSc6	Nashvillo
v	v	k7c6	v
Tennessee	Tennessee	k1gNnSc6	Tennessee
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
experimentováním	experimentování	k1gNnSc7	experimentování
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1	nahrávka
se	se	k3xPyFc4	se
různily	různit	k5eAaImAgFnP	různit
od	od	k7c2	od
optimistického	optimistický	k2eAgInSc2d1	optimistický
prvního	první	k4xOgMnSc2	první
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Stand	Standa	k1gFnPc2	Standa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
velkým	velký	k2eAgInSc7d1	velký
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
politické	politický	k2eAgFnPc4d1	politická
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Orange	Orange	k1gNnSc1	Orange
Crush	Crusha	k1gFnPc2	Crusha
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
World	World	k1gMnSc1	World
Leader	leader	k1gMnSc1	leader
Pretend	Pretend	k1gMnSc1	Pretend
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
věnovaly	věnovat	k5eAaPmAgInP	věnovat
Vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
válce	válec	k1gInPc1	válec
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
válce	válec	k1gInPc4	válec
studené	studený	k2eAgInPc4d1	studený
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
Green	Grena	k1gFnPc2	Grena
se	se	k3xPyFc4	se
prodaly	prodat	k5eAaPmAgInP	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
čtyři	čtyři	k4xCgNnPc4	čtyři
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
také	také	k9	také
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgFnSc6d3	veliký
a	a	k8xC	a
po	po	k7c6	po
vizuální	vizuální	k2eAgFnSc6d1	vizuální
stránce	stránka	k1gFnSc6	stránka
i	i	k8xC	i
nejpropracovanější	propracovaný	k2eAgNnSc4d3	nejpropracovanější
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
na	na	k7c4	na
rok	rok	k1gInSc4	rok
pauzu	pauza	k1gFnSc4	pauza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nahráli	nahrát	k5eAaBmAgMnP	nahrát
své	svůj	k3xOyFgNnSc4	svůj
sedmé	sedmý	k4xOgNnSc4	sedmý
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Out	Out	k1gFnSc4	Out
of	of	k?	of
Time	Time	k1gFnSc1	Time
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předcházejících	předcházející	k2eAgNnPc2d1	předcházející
alb	album	k1gNnPc2	album
začali	začít	k5eAaPmAgMnP	začít
členové	člen	k1gMnPc1	člen
používat	používat	k5eAaImF	používat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
nástroje	nástroj	k1gInPc4	nástroj
než	než	k8xS	než
elektrické	elektrický	k2eAgFnPc4d1	elektrická
kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
mandolínu	mandolína	k1gFnSc4	mandolína
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
akustické	akustický	k2eAgFnPc4d1	akustická
kytary	kytara	k1gFnPc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
amerického	americký	k2eAgInSc2d1	americký
i	i	k8xC	i
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
prodaly	prodat	k5eAaPmAgInP	prodat
4,2	[number]	k4	4,2
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
okolo	okolo	k7c2	okolo
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Losing	Losing	k1gInSc1	Losing
my	my	k3xPp1nPc1	my
Religion	religion	k1gInSc1	religion
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
hitem	hit	k1gInSc7	hit
jak	jak	k8xC	jak
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
MTV	MTV	kA	MTV
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
hudebních	hudební	k2eAgFnPc6d1	hudební
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
učinilo	učinit	k5eAaPmAgNnS	učinit
doposud	doposud	k6eAd1	doposud
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
singl	singl	k1gInSc4	singl
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Mike	Mike	k1gFnPc2	Mike
Mills	Millsa	k1gFnPc2	Millsa
později	pozdě	k6eAd2	pozdě
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
kariéře	kariéra	k1gFnSc6	kariéra
bylo	být	k5eAaImAgNnS	být
málo	málo	k6eAd1	málo
opravdu	opravdu	k6eAd1	opravdu
přelomových	přelomový	k2eAgFnPc2d1	přelomová
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
ale	ale	k8xC	ale
chcete	chtít	k5eAaImIp2nP	chtít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
opravdovém	opravdový	k2eAgInSc6d1	opravdový
zlomu	zlom	k1gInSc6	zlom
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
'	'	kIx"	'
<g/>
Losing	Losing	k1gInSc1	Losing
My	my	k3xPp1nPc1	my
Religion	religion	k1gInSc4	religion
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
singlu	singl	k1gInSc6	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Shiny	Shina	k1gFnPc1	Shina
Happy	Happa	k1gFnSc2	Happa
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zpívala	zpívat	k5eAaImAgFnS	zpívat
Kate	kat	k1gInSc5	kat
Pierson	Pierson	k1gMnSc1	Pierson
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
B-52	B-52	k1gFnSc4	B-52
pocházející	pocházející	k2eAgFnSc4d1	pocházející
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
z	z	k7c2	z
města	město	k1gNnSc2	město
Athens	Athensa	k1gFnPc2	Athensa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
desáté	desátý	k4xOgNnSc4	desátý
místo	místo	k1gNnSc4	místo
amerického	americký	k2eAgNnSc2d1	americké
a	a	k8xC	a
na	na	k7c4	na
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
oblibu	obliba	k1gFnSc4	obliba
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
zanevřeli	zanevřít	k5eAaPmAgMnP	zanevřít
a	a	k8xC	a
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
ji	on	k3xPp3gFnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nehrají	hrát	k5eNaImIp3nP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
získali	získat	k5eAaPmAgMnP	získat
díky	díky	k7c3	díky
Out	Out	k1gFnSc3	Out
of	of	k?	of
Time	Tim	k1gInSc2	Tim
sedm	sedm	k4xCc1	sedm
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
umělci	umělec	k1gMnPc7	umělec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
alternativním	alternativní	k2eAgNnSc7d1	alternativní
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Losing	Losing	k1gInSc1	Losing
My	my	k3xPp1nPc1	my
Religion	religion	k1gInSc1	religion
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
videoklip	videoklip	k1gInSc4	videoklip
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
popovou	popový	k2eAgFnSc4d1	popová
nahrávku	nahrávka	k1gFnSc4	nahrávka
nahranou	nahraný	k2eAgFnSc4d1	nahraná
duem	duo	k1gNnSc7	duo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
nekoncertovali	koncertovat	k5eNaImAgMnP	koncertovat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
jen	jen	k6eAd1	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
jednorázových	jednorázový	k2eAgFnPc6d1	jednorázová
akcích	akce	k1gFnPc6	akce
včetně	včetně	k7c2	včetně
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c4	na
MTV	MTV	kA	MTV
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
série	série	k1gFnSc2	série
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
přestávce	přestávka	k1gFnSc6	přestávka
začali	začít	k5eAaPmAgMnP	začít
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
album	album	k1gNnSc4	album
tvrdší	tvrdý	k2eAgNnSc4d2	tvrdší
než	než	k8xS	než
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Melody	Meloda	k1gFnSc2	Meloda
Maker	makro	k1gNnPc2	makro
se	se	k3xPyFc4	se
pochmurné	pochmurný	k2eAgFnPc1d1	pochmurná
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
stalo	stát	k5eAaPmAgNnS	stát
ještě	ještě	k6eAd1	ještě
zoufalejším	zoufalý	k2eAgNnSc7d2	zoufalejší
<g/>
.	.	kIx.	.
</s>
<s>
Nosnými	nosný	k2eAgNnPc7d1	nosné
tématy	téma	k1gNnPc7	téma
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
ztráta	ztráta	k1gFnSc1	ztráta
a	a	k8xC	a
smutek	smutek	k1gInSc1	smutek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Bucka	bucek	k1gMnSc2	bucek
bylo	být	k5eAaImAgNnS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
"	"	kIx"	"
<g/>
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
člověku	člověk	k1gMnSc3	člověk
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
třicítku	třicítka	k1gFnSc4	třicítka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Smyčové	Smyčový	k2eAgFnPc1d1	Smyčový
aranže	aranže	k1gFnPc1	aranže
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
písních	píseň	k1gFnPc6	píseň
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
baskytarista	baskytarista	k1gMnSc1	baskytarista
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Jones	Jones	k1gMnSc1	Jones
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
kritici	kritik	k1gMnPc1	kritik
i	i	k9	i
Buck	Buck	k1gMnSc1	Buck
s	s	k7c7	s
Millsem	Mills	k1gMnSc7	Mills
považovali	považovat	k5eAaImAgMnP	považovat
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
za	za	k7c4	za
doposud	doposud	k6eAd1	doposud
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
americké	americký	k2eAgFnSc2d1	americká
i	i	k8xC	i
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
a	a	k8xC	a
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc4	drive
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Everybody	Everyboda	k1gFnSc2	Everyboda
Hurts	Hurts	k1gInSc1	Hurts
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předchozího	předchozí	k2eAgNnSc2d1	předchozí
alba	album	k1gNnSc2	album
Out	Out	k1gMnPc2	Out
of	of	k?	of
Time	Time	k1gNnPc2	Time
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
kapela	kapela	k1gFnSc1	kapela
nevyjela	vyjet	k5eNaPmAgFnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpěvák	zpěvák	k1gMnSc1	zpěvák
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
kapela	kapela	k1gFnSc1	kapela
okamžitě	okamžitě	k6eAd1	okamžitě
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
dvě	dva	k4xCgNnPc4	dva
pomalá	pomalý	k2eAgNnPc4d1	pomalé
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
Monster	monstrum	k1gNnPc2	monstrum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
slov	slovo	k1gNnPc2	slovo
Petera	Peter	k1gMnSc2	Peter
Bucka	bucek	k1gMnSc2	bucek
"	"	kIx"	"
<g/>
rocková	rockový	k2eAgFnSc1d1	rocková
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
uvozovkách	uvozovka	k1gFnPc6	uvozovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgMnS	lišit
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
spousta	spousta	k1gFnSc1	spousta
elektrických	elektrický	k2eAgFnPc2d1	elektrická
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
připomínal	připomínat	k5eAaImAgInS	připomínat
grunge	grunge	k1gInSc1	grunge
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Out	Out	k1gFnSc1	Out
of	of	k?	of
Time	Time	k1gFnPc2	Time
<g/>
,	,	kIx,	,
také	také	k9	také
Monster	monstrum	k1gNnPc2	monstrum
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
jak	jak	k8xC	jak
britského	britský	k2eAgMnSc2d1	britský
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
asi	asi	k9	asi
devět	devět	k4xCc1	devět
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
Frequency	Frequencum	k1gNnPc7	Frequencum
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gInSc4	Kenneth
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bang	Bang	k1gInSc1	Bang
a	a	k8xC	a
Blame	Blam	k1gInSc5	Blam
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
posledními	poslední	k2eAgFnPc7d1	poslední
písněmi	píseň	k1gFnPc7	píseň
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
většího	veliký	k2eAgInSc2d2	veliký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
a	a	k8xC	a
také	také	k9	také
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Crush	Crush	k1gMnSc1	Crush
With	With	k1gMnSc1	With
Eyeliner	Eyeliner	k1gMnSc1	Eyeliner
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tongue	Tongue	k1gNnSc1	Tongue
<g/>
"	"	kIx"	"
vydané	vydaný	k2eAgNnSc1d1	vydané
jen	jen	k9	jen
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
umístily	umístit	k5eAaPmAgFnP	umístit
mezi	mezi	k7c7	mezi
třiceti	třicet	k4xCc7	třicet
nejúspěšnějšími	úspěšný	k2eAgMnPc7d3	nejúspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1995	[number]	k4	1995
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
obrovského	obrovský	k2eAgInSc2d1	obrovský
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
extrémně	extrémně	k6eAd1	extrémně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1995	[number]	k4	1995
zkolaboval	zkolabovat	k5eAaPmAgMnS	zkolabovat
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
diagnostikována	diagnostikován	k2eAgNnPc4d1	diagnostikováno
aneurysma	aneurysma	k1gNnSc4	aneurysma
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
byl	být	k5eAaImAgInS	být
operován	operován	k2eAgInSc1d1	operován
<g/>
.	.	kIx.	.
</s>
<s>
Berry	Berra	k1gFnPc1	Berra
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zotavil	zotavit	k5eAaPmAgMnS	zotavit
a	a	k8xC	a
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
mohlo	moct	k5eAaImAgNnS	moct
turné	turné	k1gNnSc1	turné
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
však	však	k9	však
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
potíže	potíž	k1gFnPc1	potíž
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k6eAd1	Mike
Mills	Mills	k1gInSc1	Mills
musel	muset	k5eAaImAgInS	muset
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
absolvovat	absolvovat	k5eAaPmF	absolvovat
břišní	břišní	k2eAgFnSc4d1	břišní
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
Michaelu	Michaela	k1gFnSc4	Michaela
Stipeovi	Stipeův	k2eAgMnPc1d1	Stipeův
operována	operován	k2eAgFnSc1d1	operována
kýla	kýla	k1gFnSc1	kýla
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
potíže	potíž	k1gFnPc1	potíž
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgInP	dotknout
i	i	k9	i
českého	český	k2eAgInSc2d1	český
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
dvakrát	dvakrát	k6eAd1	dvakrát
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Přese	přese	k7c4	přese
všechny	všechen	k3xTgFnPc4	všechen
potíže	potíž	k1gFnPc4	potíž
nahráli	nahrát	k5eAaBmAgMnP	nahrát
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
polovinu	polovina	k1gFnSc4	polovina
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
skončení	skončení	k1gNnSc1	skončení
album	album	k1gNnSc4	album
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
dokončili	dokončit	k5eAaPmAgMnP	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gFnPc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
údajně	údajně	k6eAd1	údajně
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rekordní	rekordní	k2eAgFnSc1d1	rekordní
částka	částka	k1gFnSc1	částka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
New	New	k1gFnPc4	New
Adventures	Adventuresa	k1gFnPc2	Adventuresa
in	in	k?	in
Hi-Fi	Hi-Fi	k1gNnSc4	Hi-Fi
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
a	a	k8xC	a
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
však	však	k9	však
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pokles	pokles	k1gInSc4	pokles
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předchozími	předchozí	k2eAgNnPc7d1	předchozí
alby	album	k1gNnPc7	album
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Christophera	Christopher	k1gMnSc2	Christopher
Johna	John	k1gMnSc2	John
Farleyho	Farley	k1gMnSc2	Farley
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gFnSc2	Tim
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
prodejnost	prodejnost	k1gFnSc1	prodejnost
způsobena	způsobit	k5eAaPmNgFnS	způsobit
nevýrazností	nevýraznost	k1gFnSc7	nevýraznost
alba	album	k1gNnSc2	album
a	a	k8xC	a
obecným	obecný	k2eAgInSc7d1	obecný
poklesem	pokles	k1gInSc7	pokles
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
také	také	k9	také
rozešli	rozejít	k5eAaPmAgMnP	rozejít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manažerem	manažer	k1gMnSc7	manažer
Jeffersonem	Jefferson	k1gMnSc7	Jefferson
Holtem	Holt	k1gMnSc7	Holt
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
obviněním	obvinění	k1gNnSc7	obvinění
ze	z	k7c2	z
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
harašení	harašení	k1gNnSc2	harašení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gInSc3	on
vznesla	vznést	k5eAaPmAgFnS	vznést
jedna	jeden	k4xCgFnSc1	jeden
členka	členka	k1gFnSc1	členka
týmu	tým	k1gInSc2	tým
okolo	okolo	k7c2	okolo
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Pozici	pozice	k1gFnSc4	pozice
manažera	manažer	k1gMnSc2	manažer
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
právník	právník	k1gMnSc1	právník
Bertis	Bertis	k1gFnSc2	Bertis
Downs	Downs	k1gInSc1	Downs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
Buckově	buckův	k2eAgInSc6d1	buckův
domě	dům	k1gInSc6	dům
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nahráli	nahrát	k5eAaBmAgMnP	nahrát
dema	dema	k6eAd1	dema
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgMnPc2	který
mělo	mít	k5eAaImAgNnS	mít
vzejít	vzejít	k5eAaPmF	vzejít
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
chtěla	chtít	k5eAaImAgFnS	chtít
změnit	změnit	k5eAaPmF	změnit
svůj	svůj	k3xOyFgInSc4	svůj
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
zamýšlela	zamýšlet	k5eAaImAgFnS	zamýšlet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
bicími	bicí	k2eAgFnPc7d1	bicí
a	a	k8xC	a
perkusemi	perkuse	k1gFnPc7	perkuse
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mělo	mít	k5eAaImAgNnS	mít
nahrávání	nahrávání	k1gNnSc1	nahrávání
alba	album	k1gNnSc2	album
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
začít	začít	k5eAaPmF	začít
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
se	se	k3xPyFc4	se
po	po	k7c6	po
měsících	měsíc	k1gInPc6	měsíc
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
debat	debata	k1gFnPc2	debata
s	s	k7c7	s
Millsem	Millso	k1gNnSc7	Millso
a	a	k8xC	a
Downsem	Downso	k1gNnSc7	Downso
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
kapelu	kapela	k1gFnSc4	kapela
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
zbývajícím	zbývající	k2eAgMnPc3d1	zbývající
členům	člen	k1gMnPc3	člen
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapelu	kapela	k1gFnSc4	kapela
ovšem	ovšem	k9	ovšem
neopustí	opustit	k5eNaPmIp3nS	opustit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
znamenat	znamenat	k5eAaImF	znamenat
její	její	k3xOp3gInSc4	její
rozpad	rozpad	k1gInSc4	rozpad
a	a	k8xC	a
Mills	Mills	k1gInSc4	Mills
<g/>
,	,	kIx,	,
Buck	Buck	k1gInSc4	Buck
a	a	k8xC	a
Stipe	Stip	k1gInSc5	Stip
se	se	k3xPyFc4	se
tak	tak	k9	tak
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
oznámil	oznámit	k5eAaPmAgMnS	oznámit
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Berry	Berra	k1gFnPc4	Berra
tisku	tisk	k1gInSc2	tisk
řekl	říct	k5eAaPmAgInS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejsem	být	k5eNaImIp1nS	být
tímhle	tenhle	k3xDgInSc7	tenhle
vším	všecek	k3xTgInSc7	všecek
tak	tak	k9	tak
nadšen	nadšen	k2eAgMnSc1d1	nadšen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsem	být	k5eAaImIp1nS	být
býval	bývat	k5eAaImAgMnS	bývat
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jsem	být	k5eAaImIp1nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
ustoupit	ustoupit	k5eAaPmF	ustoupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
a	a	k8xC	a
možná	možná	k9	možná
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
být	být	k5eAaImF	být
popovou	popový	k2eAgFnSc7d1	popová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stipe	Stip	k1gMnSc5	Stip
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
bez	bez	k7c2	bez
Berryho	Berry	k1gMnSc2	Berry
znění	znění	k1gNnSc2	znění
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
Mikea	Mikeus	k1gMnSc2	Mikeus
a	a	k8xC	a
Petera	Peter	k1gMnSc2	Peter
vzniká	vznikat	k5eAaImIp3nS	vznikat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsme	být	k5eAaImIp1nP	být
stále	stále	k6eAd1	stále
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
nohami	noha	k1gFnPc7	noha
je	být	k5eAaImIp3nS	být
taky	taky	k6eAd1	taky
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
naučit	naučit	k5eAaPmF	naučit
jinak	jinak	k6eAd1	jinak
běhat	běhat	k5eAaImF	běhat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kapela	kapela	k1gFnSc1	kapela
po	po	k7c4	po
Berryho	Berry	k1gMnSc4	Berry
odchodu	odchod	k1gInSc2	odchod
zrušila	zrušit	k5eAaPmAgFnS	zrušit
plánované	plánovaný	k2eAgNnSc4d1	plánované
nahrávání	nahrávání	k1gNnSc4	nahrávání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
Billa	Bill	k1gMnSc2	Bill
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
,	,	kIx,	,
matoucí	matoucí	k2eAgMnSc1d1	matoucí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
později	pozdě	k6eAd2	pozdě
Mills	Mills	k1gInSc4	Mills
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nevěděli	vědět	k5eNaImAgMnP	vědět
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
máme	mít	k5eAaImIp1nP	mít
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
bubeníka	bubeník	k1gMnSc2	bubeník
jsme	být	k5eAaImIp1nP	být
nemohli	moct	k5eNaImAgMnP	moct
zkoušet	zkoušet	k5eAaImF	zkoušet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
zahájili	zahájit	k5eAaPmAgMnP	zahájit
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1998	[number]	k4	1998
v	v	k7c4	v
Toast	toast	k1gInSc4	toast
Studios	Studios	k?	Studios
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
ukončila	ukončit	k5eAaPmAgFnS	ukončit
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc4d1	trvající
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Scottem	Scott	k1gMnSc7	Scott
Littem	Litt	k1gMnSc7	Litt
a	a	k8xC	a
za	za	k7c2	za
producenta	producent	k1gMnSc2	producent
si	se	k3xPyFc3	se
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Pata	pata	k1gFnSc1	pata
McCarthyho	McCarthy	k1gMnSc2	McCarthy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
asistentem	asistent	k1gMnSc7	asistent
byl	být	k5eAaImAgMnS	být
Nigel	Nigel	k1gMnSc1	Nigel
Godrich	Godrich	k1gMnSc1	Godrich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pro	pro	k7c4	pro
nahrávání	nahrávání	k1gNnSc4	nahrávání
najal	najmout	k5eAaPmAgMnS	najmout
bubeníky	bubeník	k1gMnPc7	bubeník
Barretta	Barretta	k1gFnSc1	Barretta
Martina	Martina	k1gFnSc1	Martina
<g/>
,	,	kIx,	,
ex-člena	ex-člena	k1gFnSc1	ex-člena
Screaming	Screaming	k1gInSc1	Screaming
Trees	Trees	k1gMnSc1	Trees
a	a	k8xC	a
Joeyho	Joey	k1gMnSc4	Joey
Waronkera	Waronker	k1gMnSc4	Waronker
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Beck	Becka	k1gFnPc2	Becka
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
byla	být	k5eAaImAgFnS	být
vypjatá	vypjatý	k2eAgFnSc1d1	vypjatá
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Bertis	Bertis	k1gInSc1	Bertis
Downs	Downsa	k1gFnPc2	Downsa
svolal	svolat	k5eAaPmAgInS	svolat
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
členové	člen	k1gMnPc1	člen
vyřešili	vyřešit	k5eAaPmAgMnP	vyřešit
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
v	v	k7c4	v
nahrávání	nahrávání	k1gNnSc4	nahrávání
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Up	Up	k1gMnSc1	Up
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
jak	jak	k8xC	jak
amerického	americký	k2eAgMnSc2d1	americký
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k6eAd1	jen
900	[number]	k4	900
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
asi	asi	k9	asi
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
prodejnost	prodejnost	k1gFnSc1	prodejnost
alb	album	k1gNnPc2	album
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
singly	singl	k1gInPc1	singl
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
dostávaly	dostávat	k5eAaImAgFnP	dostávat
mezi	mezi	k7c4	mezi
dvacet	dvacet	k4xCc4	dvacet
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Up	Up	k1gFnSc2	Up
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
Muž	muž	k1gMnSc1	muž
na	na	k7c6	na
Měsíci	měsíc	k1gInSc3	měsíc
pojednávajícímu	pojednávající	k2eAgInSc3d1	pojednávající
o	o	k7c4	o
komikovi	komik	k1gMnSc3	komik
Andy	Anda	k1gFnSc2	Anda
Kaufmanovi	Kaufman	k1gMnSc3	Kaufman
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
písni	píseň	k1gFnSc6	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
The	The	k1gMnPc2	The
Great	Great	k1gInSc4	Great
Beyond	Beyonda	k1gFnPc2	Beyonda
ze	z	k7c2	z
soundtracku	soundtrack	k1gInSc2	soundtrack
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
singl	singl	k1gInSc1	singl
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
umístila	umístit	k5eAaPmAgFnS	umístit
až	až	k9	až
na	na	k7c4	na
56	[number]	k4	56
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůbec	vůbec	k9	vůbec
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
písní	píseň	k1gFnPc2	píseň
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
většinu	většina	k1gFnSc4	většina
svého	své	k1gNnSc2	své
dvanáctého	dvanáctý	k4xOgNnSc2	dvanáctý
alba	album	k1gNnSc2	album
Reveal	Reveal	k1gInSc1	Reveal
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
mezi	mezi	k7c7	mezi
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
říjnem	říjen	k1gInSc7	říjen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Reveal	Reveal	k1gInSc4	Reveal
bylo	být	k5eAaImAgNnS	být
laděno	ladit	k5eAaImNgNnS	ladit
podobně	podobně	k6eAd1	podobně
posmutněle	posmutněle	k6eAd1	posmutněle
jako	jako	k8xC	jako
předcházející	předcházející	k2eAgNnSc4d1	předcházející
album	album	k1gNnSc4	album
Up	Up	k1gFnSc2	Up
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
bubnoval	bubnovat	k5eAaImAgInS	bubnovat
Joey	Joey	k1gInPc4	Joey
Waronker	Waronkra	k1gFnPc2	Waronkra
a	a	k8xC	a
podíleli	podílet	k5eAaImAgMnP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
zakládající	zakládající	k2eAgInSc4d1	zakládající
člen	člen	k1gInSc4	člen
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnPc1	The
Minus	minus	k1gNnSc1	minus
5	[number]	k4	5
Scott	Scottum	k1gNnPc2	Scottum
McCaughey	McCaughea	k1gFnSc2	McCaughea
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
kapely	kapela	k1gFnSc2	kapela
Posies	Posies	k1gMnSc1	Posies
Ken	Ken	k1gMnSc1	Ken
Stringfellow	Stringfellow	k1gMnSc1	Stringfellow
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
prodaly	prodat	k5eAaPmAgInP	prodat
asi	asi	k9	asi
čtyři	čtyři	k4xCgNnPc4	čtyři
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
prodejnost	prodejnost	k1gFnSc1	prodejnost
stejného	stejný	k2eAgInSc2d1	stejný
výsledku	výsledek	k1gInSc2	výsledek
jako	jako	k8xS	jako
u	u	k7c2	u
alba	album	k1gNnSc2	album
Up	Up	k1gFnSc2	Up
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Imitation	Imitation	k1gInSc1	Imitation
of	of	k?	of
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Al	ala	k1gFnPc2	ala
Friston	Friston	k1gInSc1	Friston
popsal	popsat	k5eAaPmAgInS	popsat
album	album	k1gNnSc4	album
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Naložené	naložený	k2eAgNnSc1d1	naložené
zlatou	zlatý	k2eAgFnSc7d1	zlatá
půvabností	půvabnost	k1gFnSc7	půvabnost
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
otočce	otočka	k1gFnSc6	otočka
a	a	k8xC	a
zatáčce	zatáčka	k1gFnSc6	zatáčka
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
nepřesvědčivými	přesvědčivý	k2eNgNnPc7d1	nepřesvědčivé
alby	album	k1gNnPc7	album
New	New	k1gMnSc1	New
Adventures	Adventures	k1gMnSc1	Adventures
in	in	k?	in
Hi-Fi	Hi-Fi	k1gNnSc4	Hi-Fi
a	a	k8xC	a
Up	Up	k1gFnSc4	Up
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rob	roba	k1gFnPc2	roba
Sheffield	Sheffielda	k1gFnPc2	Sheffielda
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
pochválil	pochválit	k5eAaPmAgMnS	pochválit
album	album	k1gNnSc4	album
za	za	k7c4	za
jeho	jeho	k3xOp3gMnPc4	jeho
"	"	kIx"	"
<g/>
neustále	neustále	k6eAd1	neustále
udivující	udivující	k2eAgFnSc4d1	udivující
krásu	krása	k1gFnSc4	krása
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vydali	vydat	k5eAaPmAgMnP	vydat
Warner	Warner	k1gInSc4	Warner
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
kompilaci	kompilace	k1gFnSc4	kompilace
In	In	k1gMnSc2	In
Time	Tim	k1gMnSc2	Tim
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
kromě	kromě	k7c2	kromě
starých	starý	k2eAgFnPc2d1	stará
písní	píseň	k1gFnPc2	píseň
zařazeny	zařadit	k5eAaPmNgInP	zařadit
i	i	k8xC	i
novinky	novinka	k1gFnPc1	novinka
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc1	Bad
Day	Day	k1gFnSc2	Day
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Animal	animal	k1gMnSc1	animal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Berry	Berra	k1gFnPc4	Berra
při	při	k7c6	při
koncertě	koncert	k1gInSc6	koncert
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Raleigh	Raleigha	k1gFnPc2	Raleigha
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpíval	zpívat	k5eAaImAgMnS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
u	u	k7c2	u
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
a	a	k8xC	a
bubnoval	bubnovat	k5eAaImAgMnS	bubnovat
u	u	k7c2	u
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Permanent	permanent	k1gInSc1	permanent
Vacation	Vacation	k1gInSc1	Vacation
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
vystoupení	vystoupení	k1gNnSc4	vystoupení
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Around	Around	k1gInSc1	Around
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
Mills	Mills	k1gInSc4	Mills
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Upřímně	upřímně	k6eAd1	upřímně
si	se	k3xPyFc3	se
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
rychlosti	rychlost	k1gFnSc3	rychlost
písní	píseň	k1gFnPc2	píseň
trochu	trochu	k6eAd1	trochu
pomalejší	pomalý	k2eAgInPc1d2	pomalejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
původně	původně	k6eAd1	původně
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reakce	reakce	k1gFnSc1	reakce
kritiky	kritika	k1gFnSc2	kritika
na	na	k7c4	na
album	album	k1gNnSc4	album
byly	být	k5eAaImAgFnP	být
smíšené	smíšený	k2eAgFnPc1d1	smíšená
a	a	k8xC	a
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
třináctém	třináctý	k4xOgInSc6	třináctý
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Leaving	Leaving	k1gInSc1	Leaving
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
pátém	pátý	k4xOgInSc6	pátý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
následném	následný	k2eAgNnSc6d1	následné
turné	turné	k1gNnSc6	turné
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
bubeník	bubeník	k1gMnSc1	bubeník
Bill	Bill	k1gMnSc1	Bill
Rieflin	Rieflin	k2eAgMnSc1d1	Rieflin
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Ministry	ministr	k1gMnPc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brucem	Bruce	k1gMnSc7	Bruce
Springsteenem	Springsteen	k1gMnSc7	Springsteen
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gMnSc1	Pearl
Jam	jam	k1gInSc1	jam
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
participovali	participovat	k5eAaImAgMnP	participovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Vote	Vot	k1gInSc2	Vot
for	forum	k1gNnPc2	forum
Change	change	k1gFnSc2	change
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c4	na
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
turné	turné	k1gNnSc4	turné
od	od	k7c2	od
alba	album	k1gNnSc2	album
Monster	monstrum	k1gNnPc2	monstrum
před	před	k7c7	před
deseti	deset	k4xCc2	deset
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
navštívili	navštívit	k5eAaPmAgMnP	navštívit
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
také	také	k6eAd1	také
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Sazka	Sazka	k1gFnSc1	Sazka
Arenu	Aren	k1gInSc2	Aren
a	a	k8xC	a
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
charitativního	charitativní	k2eAgInSc2d1	charitativní
koncertu	koncert	k1gInSc2	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastní	vlastní	k2eAgInSc4d1	vlastní
katalog	katalog	k1gInSc4	katalog
písní	píseň	k1gFnPc2	píseň
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
kompilaci	kompilace	k1gFnSc4	kompilace
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
nahráli	nahrát	k5eAaPmAgMnP	nahrát
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
labelem	label	k1gInSc7	label
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
And	Anda	k1gFnPc2	Anda
I	i	k8xC	i
Feel	Feela	k1gFnPc2	Feela
Fine	Fin	k1gMnSc5	Fin
<g/>
...	...	k?	...
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Years	Years	k1gInSc4	Years
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
i	i	k9	i
DVD	DVD	kA	DVD
When	When	k1gMnSc1	When
The	The	k1gMnSc1	The
Light	Light	k1gMnSc1	Light
Is	Is	k1gMnSc1	Is
Mine	minout	k5eAaImIp3nS	minout
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Years	Years	k1gInSc4	Years
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
byli	být	k5eAaImAgMnP	být
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Georgia	Georgium	k1gNnSc2	Georgium
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
při	při	k7c6	při
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
sestavě	sestava	k1gFnSc6	sestava
i	i	k9	i
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Berrym	Berrym	k1gInSc4	Berrym
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zkoušení	zkoušení	k1gNnSc6	zkoušení
před	před	k7c7	před
vystoupením	vystoupení	k1gNnSc7	vystoupení
nahráli	nahrát	k5eAaPmAgMnP	nahrát
cover	cover	k1gInSc4	cover
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
"	"	kIx"	"
<g/>
#	#	kIx~	#
<g/>
9	[number]	k4	9
Dream	Dream	k1gInSc1	Dream
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
Instant	Instant	k1gInSc1	Instant
Karma	karma	k1gFnSc1	karma
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc2	International
Campaign	Campaigna	k1gFnPc2	Campaigna
to	ten	k3xDgNnSc1	ten
Save	Save	k1gNnSc7	Save
Darfur	Darfura	k1gFnPc2	Darfura
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
podpořit	podpořit	k5eAaPmF	podpořit
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
genocidě	genocida	k1gFnSc3	genocida
v	v	k7c6	v
Dárfúru	Dárfúr	k1gInSc6	Dárfúr
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
i	i	k9	i
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
studiovou	studiový	k2eAgFnSc4d1	studiová
nahrávku	nahrávka	k1gFnSc4	nahrávka
Billa	Bill	k1gMnSc2	Bill
Berryho	Berry	k1gMnSc2	Berry
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
po	po	k7c6	po
takřka	takřka	k6eAd1	takřka
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
splnila	splnit	k5eAaPmAgFnS	splnit
kapela	kapela	k1gFnSc1	kapela
kritéria	kritérion	k1gNnSc2	kritérion
pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
byli	být	k5eAaImAgMnP	být
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
umělců	umělec	k1gMnPc2	umělec
uvedených	uvedený	k2eAgMnPc2d1	uvedený
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Waldorf-Astoria	Waldorf-Astorium	k1gNnSc2	Waldorf-Astorium
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
uvedeni	uvést	k5eAaPmNgMnP	uvést
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jam	k1gInSc1	jam
Eddie	Eddie	k1gFnSc2	Eddie
Vaderem	Vader	k1gInSc7	Vader
a	a	k8xC	a
zahráli	zahrát	k5eAaPmAgMnP	zahrát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Berrym	Berrym	k1gInSc4	Berrym
čtyři	čtyři	k4xCgFnPc4	čtyři
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
soustředila	soustředit	k5eAaPmAgFnS	soustředit
také	také	k9	také
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Accelerate	Accelerat	k1gMnSc5	Accelerat
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
produkoval	produkovat	k5eAaImAgInS	produkovat
Jacknife	Jacknif	k1gInSc5	Jacknif
Lee	Lea	k1gFnSc6	Lea
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
odehráli	odehrát	k5eAaPmAgMnP	odehrát
v	v	k7c6	v
Olympia	Olympia	k1gFnSc1	Olympia
Theater	Theater	k1gMnSc1	Theater
pět	pět	k4xCc4	pět
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
vydali	vydat	k5eAaPmAgMnP	vydat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Live	Live	k1gNnSc4	Live
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
nahrávky	nahrávka	k1gFnPc4	nahrávka
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Accelerate	Accelerat	k1gInSc5	Accelerat
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
31	[number]	k4	31
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2008	[number]	k4	2008
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
navštívili	navštívit	k5eAaPmAgMnP	navštívit
i	i	k8xC	i
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Collapse	Collapse	k1gFnSc1	Collapse
into	into	k1gMnSc1	into
Now	Now	k1gMnSc1	Now
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
řada	řada	k1gFnSc1	řada
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
Patti	Patt	k1gMnPc1	Patt
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
Frequency	Frequencum	k1gNnPc7	Frequencum
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gInSc4	Kenneth
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Monster	monstrum	k1gNnPc2	monstrum
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
Chronic	Chronice	k1gFnPc2	Chronice
Town	Town	k1gInSc1	Town
Dead	Dead	k1gMnSc1	Dead
Letter	Letter	k1gMnSc1	Letter
Office	Office	kA	Office
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
neuveřejněné	uveřejněný	k2eNgFnPc4d1	neuveřejněná
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
B-strany	Btrana	k1gFnPc4	B-strana
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
EP	EP	kA	EP
Chronic	Chronice	k1gFnPc2	Chronice
Town	Towna	k1gFnPc2	Towna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Eponymous	Eponymous	k1gInSc1	Eponymous
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
nahraných	nahraný	k2eAgFnPc2d1	nahraná
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
nahraných	nahraný	k2eAgFnPc2d1	nahraná
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Singles	Singles	k1gMnSc1	Singles
Collected	Collected	k1gMnSc1	Collected
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
singly	singl	k1gInPc4	singl
nahrané	nahraný	k2eAgInPc4d1	nahraný
u	u	k7c2	u
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
the	the	k?	the
Attic	Attic	k1gMnSc1	Attic
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
radio	radio	k1gNnSc1	radio
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
živá	živý	k2eAgNnPc4d1	živé
vystoupení	vystoupení	k1gNnPc4	vystoupení
a	a	k8xC	a
B-strany	Btrana	k1gFnPc4	B-strana
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Not	nota	k1gFnPc2	nota
Bad	Bad	k1gMnPc2	Bad
for	forum	k1gNnPc2	forum
No	no	k9	no
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
živé	živá	k1gFnSc2	živá
EP	EP	kA	EP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
In	In	k1gFnSc1	In
Time	Time	k1gFnSc1	Time
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
1988-2003	[number]	k4	1988-2003
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
nahraných	nahraný	k2eAgInPc2d1	nahraný
u	u	k7c2	u
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
And	Anda	k1gFnPc2	Anda
I	i	k8xC	i
Feel	Feela	k1gFnPc2	Feela
Fine	Fin	k1gMnSc5	Fin
<g/>
...	...	k?	...
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Years	Years	k1gInSc4	Years
1982-1987	[number]	k4	1982-1987
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
nahraných	nahraný	k2eAgFnPc2d1	nahraná
u	u	k7c2	u
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Live	Live	k1gNnPc2	Live
Pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
seznam	seznam	k1gInSc1	seznam
všech	všecek	k3xTgFnPc2	všecek
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc4	seznam
písní	píseň	k1gFnPc2	píseň
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Road	Road	k1gInSc1	Road
Movie	Movie	k1gFnSc2	Movie
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Tourfilm	Tourfilm	k1gInSc1	Tourfilm
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
This	Thisa	k1gFnPc2	Thisa
Film	film	k1gInSc1	film
Is	Is	k1gMnSc1	Is
On	on	k3xPp3gMnSc1	on
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Parallel	Parallel	k1gFnSc1	Parallel
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
Tourfilm	Tourfilm	k1gInSc1	Tourfilm
+	+	kIx~	+
This	This	k1gInSc1	This
Film	film	k1gInSc1	film
Is	Is	k1gFnSc1	Is
<g />
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
+	+	kIx~	+
Parallel	Parallel	k1gMnSc1	Parallel
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
View	View	k1gMnSc1	View
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Perfect	Perfect	k2eAgInSc1d1	Perfect
Square	square	k1gInSc1	square
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
When	When	k1gMnSc1	When
The	The	k1gMnSc1	The
Light	Light	k1gMnSc1	Light
Is	Is	k1gMnSc1	Is
Mine	minout	k5eAaImIp3nS	minout
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
the	the	k?	the
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Years	Years	k1gInSc4	Years
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
Video	video	k1gNnSc1	video
Collection	Collection	k1gInSc1	Collection
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Live	Liv	k1gMnSc2	Liv
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zpráva	zpráva	k1gFnSc1	zpráva
Skupina	skupina	k1gFnSc1	skupina
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
po	po	k7c6	po
31	[number]	k4	31
letech	léto	k1gNnPc6	léto
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ukázky	ukázka	k1gFnPc4	ukázka
všech	všecek	k3xTgFnPc2	všecek
písní	píseň	k1gFnPc2	píseň
Program	program	k1gInSc1	program
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
R.	R.	kA	R.
E.	E.	kA	E.
M.	M.	kA	M.
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
rozohní	rozohnit	k5eAaPmIp3nS	rozohnit
Prahu	Praha	k1gFnSc4	Praha
</s>
