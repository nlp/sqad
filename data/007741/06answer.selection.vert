<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
povrchovým	povrchový	k2eAgInSc7d1	povrchový
útvarem	útvar	k1gInSc7	útvar
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
1400	[number]	k4	1400
km	km	kA	km
se	se	k3xPyFc4	se
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
Caloris	Caloris	k1gFnSc2	Caloris
Basin	Basina	k1gFnPc2	Basina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
kráter	kráter	k1gInSc4	kráter
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
