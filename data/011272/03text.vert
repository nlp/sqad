<p>
<s>
IHS	IHS	kA	IHS
nebo	nebo	k8xC	nebo
také	také	k9	také
JHS	JHS	kA	JHS
(	(	kIx(	(
<g/>
písmena	písmeno	k1gNnPc4	písmeno
I	i	k8xC	i
a	a	k8xC	a
J	J	kA	J
se	se	k3xPyFc4	se
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
dlouho	dlouho	k6eAd1	dlouho
nerozlišovala	rozlišovat	k5eNaImAgFnS	rozlišovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
jména	jméno	k1gNnSc2	jméno
Ježíš	ježit	k5eAaImIp2nS	ježit
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
(	(	kIx(	(
<g/>
Ι	Ι	k?	Ι
<g/>
,	,	kIx,	,
Jésús	Jésús	k1gInSc1	Jésús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
i	i	k8xC	i
východní	východní	k2eAgFnSc6d1	východní
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
zkratky	zkratka	k1gFnSc2	zkratka
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
výklady	výklad	k1gInPc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgNnP	být
řecká	řecký	k2eAgNnPc1d1	řecké
písmena	písmeno	k1gNnPc1	písmeno
Ι	Ι	k?	Ι
ióta-éta-sigma	ióta-étaigm	k1gMnSc2	ióta-éta-sigm
<g/>
,	,	kIx,	,
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
byl	být	k5eAaImAgMnS	být
místo	místo	k7c2	místo
éty	éty	k?	éty
použit	použít	k5eAaPmNgInS	použít
stejně	stejně	k6eAd1	stejně
vypadající	vypadající	k2eAgInSc1d1	vypadající
znak	znak	k1gInSc1	znak
H	H	kA	H
(	(	kIx(	(
<g/>
označující	označující	k2eAgMnSc1d1	označující
však	však	k9	však
[	[	kIx(	[
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
sigma	sigma	k1gNnPc4	sigma
nahrazena	nahrazen	k2eAgNnPc4d1	nahrazeno
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
S.	S.	kA	S.
Na	na	k7c6	na
východních	východní	k2eAgFnPc6d1	východní
ikonách	ikona	k1gFnPc6	ikona
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
zkratka	zkratka	k1gFnSc1	zkratka
IH	IH	kA	IH
XP	XP	kA	XP
(	(	kIx(	(
<g/>
Jésús	Jésús	k1gInSc1	Jésús
Chriostos	Chriostos	k1gInSc1	Chriostos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
znalosti	znalost	k1gFnPc1	znalost
řečtiny	řečtina	k1gFnSc2	řečtina
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnPc1	písmeno
začala	začít	k5eAaPmAgNnP	začít
číst	číst	k5eAaImF	číst
jako	jako	k9	jako
latinská	latinský	k2eAgNnPc4d1	latinské
a	a	k8xC	a
vykládat	vykládat	k5eAaImF	vykládat
například	například	k6eAd1	například
jako	jako	k8xC	jako
Iesus	Iesus	k1gMnSc1	Iesus
Hominum	Hominum	k1gNnSc1	Hominum
Salvator	Salvator	k1gMnSc1	Salvator
(	(	kIx(	(
<g/>
Ježíš	Ježíš	k1gMnSc1	Ježíš
spasitel	spasitel	k1gMnSc1	spasitel
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc4d1	jiný
výklad	výklad	k1gInSc4	výklad
spojoval	spojovat	k5eAaImAgMnS	spojovat
monogram	monogram	k1gInSc4	monogram
IHS	IHS	kA	IHS
s	s	k7c7	s
legendou	legenda	k1gFnSc7	legenda
o	o	k7c4	o
vidění	vidění	k1gNnSc4	vidění
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Milvijského	Milvijský	k2eAgInSc2d1	Milvijský
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
312	[number]	k4	312
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
uslyšet	uslyšet	k5eAaPmF	uslyšet
příslib	příslib	k1gInSc4	příslib
In	In	k1gFnSc2	In
Hoc	Hoc	k1gFnPc2	Hoc
Signo	Signo	k1gNnSc1	Signo
vinces	vinces	k1gMnSc1	vinces
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
znamení	znamení	k1gNnSc6	znamení
zvítězíš	zvítězit	k5eAaPmIp2nS	zvítězit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
JHS	JHS	kA	JHS
někdy	někdy	k6eAd1	někdy
vykládalo	vykládat	k5eAaImAgNnS	vykládat
jako	jako	k8xC	jako
Ježíš	Ježíš	k1gMnSc1	Ježíš
hříšných	hříšný	k2eAgMnPc2d1	hříšný
spasitel	spasitel	k1gMnSc1	spasitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
monogram	monogram	k1gInSc1	monogram
IHS	IHS	kA	IHS
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
eucharistickou	eucharistický	k2eAgFnSc7d1	eucharistická
zbožností	zbožnost	k1gFnSc7	zbožnost
<g/>
,	,	kIx,	,
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
svátku	svátek	k1gInSc2	svátek
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
1264	[number]	k4	1264
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
ke	k	k7c3	k
svátosti	svátost	k1gFnSc3	svátost
oltářní	oltářní	k2eAgFnSc1d1	oltářní
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnPc1	písmeno
IHS	IHS	kA	IHS
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
kruhové	kruhový	k2eAgFnSc6d1	kruhová
hostii	hostie	k1gFnSc6	hostie
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgInPc4d1	obklopený
plameny	plamen	k1gInPc4	plamen
<g/>
,	,	kIx,	,
a	a	k8xC	a
doplněna	doplnit	k5eAaPmNgFnS	doplnit
křížem	křížem	k6eAd1	křížem
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
srdcem	srdce	k1gNnSc7	srdce
nebo	nebo	k8xC	nebo
třemi	tři	k4xCgInPc7	tři
hřeby	hřeb	k1gInPc7	hřeb
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
užívat	užívat	k5eAaImF	užívat
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
převzalo	převzít	k5eAaPmAgNnS	převzít
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
znak	znak	k1gInSc4	znak
Tovaryšstvo	tovaryšstvo	k1gNnSc4	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
(	(	kIx(	(
<g/>
Societas	Societas	k1gInSc4	Societas
Jesu	Jesus	k1gInSc2	Jesus
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnSc2	písmeno
IHS	IHS	kA	IHS
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vykládala	vykládat	k5eAaImAgFnS	vykládat
jako	jako	k9	jako
Iesum	Iesum	k1gInSc4	Iesum
Habemus	Habemus	k1gInSc1	Habemus
Socium	Socium	k1gNnSc1	Socium
(	(	kIx(	(
<g/>
Ježíš	Ježíš	k1gMnSc1	Ježíš
je	být	k5eAaImIp3nS	být
náš	náš	k3xOp1gMnSc1	náš
společník	společník	k1gMnSc1	společník
<g/>
)	)	kIx)	)
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
na	na	k7c6	na
jezuitských	jezuitský	k2eAgFnPc6d1	jezuitská
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
na	na	k7c6	na
liturgických	liturgický	k2eAgInPc6d1	liturgický
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
na	na	k7c6	na
náhrobcích	náhrobek	k1gInPc6	náhrobek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
IHS	IHS	kA	IHS
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
</s>
</p>
<p>
<s>
Kristův	Kristův	k2eAgInSc1d1	Kristův
monogram	monogram	k1gInSc1	monogram
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
IHS	IHS	kA	IHS
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
IHS	IHS	kA	IHS
Monogram	monogram	k1gInSc4	monogram
(	(	kIx(	(
<g/>
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Holy	hola	k1gFnPc1	hola
Name	Nam	k1gFnPc1	Nam
of	of	k?	of
Jesus	Jesus	k1gMnSc1	Jesus
(	(	kIx(	(
<g/>
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sacred	Sacred	k1gMnSc1	Sacred
names	names	k1gMnSc1	names
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Monogram	monogram	k1gInSc1	monogram
IHS	IHS	kA	IHS
na	na	k7c6	na
hřbitovech	hřbitov	k1gInPc6	hřbitov
</s>
</p>
