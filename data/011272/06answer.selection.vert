<s>
IHS	IHS	kA	IHS
nebo	nebo	k8xC	nebo
také	také	k9	také
JHS	JHS	kA	JHS
(	(	kIx(	(
<g/>
písmena	písmeno	k1gNnPc4	písmeno
I	i	k8xC	i
a	a	k8xC	a
J	J	kA	J
se	se	k3xPyFc4	se
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
dlouho	dlouho	k6eAd1	dlouho
nerozlišovala	rozlišovat	k5eNaImAgFnS	rozlišovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
jména	jméno	k1gNnSc2	jméno
Ježíš	ježit	k5eAaImIp2nS	ježit
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
(	(	kIx(	(
<g/>
Ι	Ι	k?	Ι
<g/>
,	,	kIx,	,
Jésús	Jésús	k1gInSc1	Jésús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
i	i	k8xC	i
východní	východní	k2eAgFnSc6d1	východní
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
.	.	kIx.	.
</s>
