<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
technologický	technologický	k2eAgInSc4d1	technologický
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
vstupní	vstupní	k2eAgFnSc1d1	vstupní
surovina	surovina	k1gFnSc1	surovina
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nečistot	nečistota	k1gFnPc2	nečistota
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
<g/>
?	?	kIx.	?
</s>
