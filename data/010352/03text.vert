<p>
<s>
Rafinace	rafinace	k1gFnSc1	rafinace
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
raffiner	raffiner	k1gMnSc1	raffiner
<g/>
,	,	kIx,	,
přečistit	přečistit	k5eAaPmF	přečistit
<g/>
,	,	kIx,	,
zjemnit	zjemnit	k5eAaPmF	zjemnit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technologický	technologický	k2eAgInSc4d1	technologický
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
vstupní	vstupní	k2eAgFnSc1d1	vstupní
surovina	surovina	k1gFnSc1	surovina
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
upravuje	upravovat	k5eAaImIp3nS	upravovat
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
produkt	produkt	k1gInSc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
aplikaci	aplikace	k1gFnSc6	aplikace
destilace	destilace	k1gFnSc2	destilace
<g/>
,	,	kIx,	,
krakování	krakování	k1gNnSc2	krakování
<g/>
,	,	kIx,	,
odstředění	odstředění	k1gNnSc2	odstředění
nebo	nebo	k8xC	nebo
dalších	další	k2eAgInPc2d1	další
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c6	na
rafinování	rafinování	k1gNnSc6	rafinování
je	být	k5eAaImIp3nS	být
rafinerie	rafinerie	k1gFnSc1	rafinerie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ropná	ropný	k2eAgFnSc1d1	ropná
rafinerie	rafinerie	k1gFnSc1	rafinerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
nebo	nebo	k8xC	nebo
směsného	směsný	k2eAgInSc2d1	směsný
šrotu	šrot	k1gInSc2	šrot
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
postupy	postup	k1gInPc1	postup
zvané	zvaný	k2eAgFnSc2d1	zvaná
rafinace	rafinace	k1gFnSc2	rafinace
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
dva	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
hutní	hutnit	k5eAaImIp3nS	hutnit
rafinace	rafinace	k1gFnSc1	rafinace
a	a	k8xC	a
elektrolytická	elektrolytický	k2eAgFnSc1d1	elektrolytická
rafinace	rafinace	k1gFnSc1	rafinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Časté	častý	k2eAgInPc1d1	častý
objekty	objekt	k1gInPc1	objekt
rafinace	rafinace	k1gFnSc2	rafinace
==	==	k?	==
</s>
</p>
<p>
<s>
cukr	cukr	k1gInSc4	cukr
</s>
</p>
<p>
<s>
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
ropa	ropa	k1gFnSc1	ropa
</s>
</p>
<p>
<s>
elektrum	elektrum	k1gNnSc1	elektrum
</s>
</p>
