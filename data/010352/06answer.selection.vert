<s>
Rafinace	rafinace	k1gFnSc1	rafinace
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
raffiner	raffiner	k1gMnSc1	raffiner
<g/>
,	,	kIx,	,
přečistit	přečistit	k5eAaPmF	přečistit
<g/>
,	,	kIx,	,
zjemnit	zjemnit	k5eAaPmF	zjemnit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technologický	technologický	k2eAgInSc4d1	technologický
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
vstupní	vstupní	k2eAgFnSc1d1	vstupní
surovina	surovina	k1gFnSc1	surovina
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
upravuje	upravovat	k5eAaImIp3nS	upravovat
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
produkt	produkt	k1gInSc1	produkt
<g/>
.	.	kIx.	.
</s>
