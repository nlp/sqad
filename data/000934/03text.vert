<s>
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1852	[number]	k4	1852
Mirotice	Mirotice	k1gFnSc1	Mirotice
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
součást	součást	k1gFnSc1	součást
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
dekoratér	dekoratér	k1gMnSc1	dekoratér
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
Generace	generace	k1gFnSc2	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
klasik	klasik	k1gMnSc1	klasik
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ranějším	raný	k2eAgNnSc6d2	ranější
období	období	k1gNnSc6	období
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
romantickém	romantický	k2eAgInSc6d1	romantický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
odkazu	odkaz	k1gInSc2	odkaz
Josefa	Josef	k1gMnSc2	Josef
Mánese	Mánes	k1gMnSc5	Mánes
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
směřoval	směřovat	k5eAaImAgInS	směřovat
spíše	spíše	k9	spíše
k	k	k7c3	k
secesi	secese	k1gFnSc3	secese
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
písař	písař	k1gMnSc1	písař
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
městský	městský	k2eAgMnSc1d1	městský
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
významné	významný	k2eAgFnSc2d1	významná
a	a	k8xC	a
zámožné	zámožný	k2eAgFnSc2d1	zámožná
mirotické	mirotický	k2eAgFnSc2d1	mirotický
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Veronika	Veronika	k1gFnSc1	Veronika
Alšová	Alšová	k1gFnSc1	Alšová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Famfulová	Famfulový	k2eAgFnSc1d1	Famfulový
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
několikrát	několikrát	k6eAd1	několikrát
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Písku	Písek	k1gInSc2	Písek
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
situaci	situace	k1gFnSc4	situace
zlepšit	zlepšit	k5eAaPmF	zlepšit
a	a	k8xC	a
Alšové	Aleš	k1gMnPc1	Aleš
se	se	k3xPyFc4	se
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1859	[number]	k4	1859
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Mirotic	Mirotice	k1gFnPc2	Mirotice
<g/>
.	.	kIx.	.
</s>
<s>
Kreslit	kreslit	k5eAaImF	kreslit
začal	začít	k5eAaPmAgInS	začít
už	už	k6eAd1	už
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
František	František	k1gMnSc1	František
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
pro	pro	k7c4	pro
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
tamních	tamní	k2eAgMnPc2d1	tamní
pedagogů	pedagog	k1gMnPc2	pedagog
musel	muset	k5eAaImAgMnS	muset
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
učit	učit	k5eAaImF	učit
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
písecké	písecký	k2eAgFnSc2d1	Písecká
reálky	reálka	k1gFnSc2	reálka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
si	se	k3xPyFc3	se
přivydělával	přivydělávat	k5eAaImAgInS	přivydělávat
vyučováním	vyučování	k1gNnSc7	vyučování
kreslení	kreslení	k1gNnPc2	kreslení
a	a	k8xC	a
drobnými	drobný	k2eAgFnPc7d1	drobná
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
profesory	profesor	k1gMnPc7	profesor
byli	být	k5eAaImAgMnP	být
Josef	Josef	k1gMnSc1	Josef
Matyáš	Matyáš	k1gMnSc1	Matyáš
Trenkwald	Trenkwald	k1gMnSc1	Trenkwald
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Swerts	Swertsa	k1gFnPc2	Swertsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
demonstrace	demonstrace	k1gFnSc2	demonstrace
proti	proti	k7c3	proti
profesoru	profesor	k1gMnSc3	profesor
A.	A.	kA	A.
Woltmannovi	Woltmann	k1gMnSc3	Woltmann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
přednášky	přednáška	k1gFnSc2	přednáška
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
vzdělávacím	vzdělávací	k2eAgInSc6d1	vzdělávací
klubu	klub	k1gInSc6	klub
Concordia	Concordium	k1gNnSc2	Concordium
popřel	popřít	k5eAaPmAgInS	popřít
sice	sice	k8xC	sice
existenci	existence	k1gFnSc4	existence
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
gotické	gotický	k2eAgFnSc2d1	gotická
a	a	k8xC	a
barokní	barokní	k2eAgFnSc2d1	barokní
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
české	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
pouze	pouze	k6eAd1	pouze
přejímalo	přejímat	k5eAaImAgNnS	přejímat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
německé	německý	k2eAgInPc4d1	německý
vzory	vzor	k1gInPc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přednášku	přednáška	k1gFnSc4	přednáška
navštívili	navštívit	k5eAaPmAgMnP	navštívit
<g/>
,	,	kIx,	,
hlasitě	hlasitě	k6eAd1	hlasitě
protestovali	protestovat	k5eAaBmAgMnP	protestovat
a	a	k8xC	a
neopustili	opustit	k5eNaPmAgMnP	opustit
přednáškovou	přednáškový	k2eAgFnSc4d1	přednášková
síň	síň	k1gFnSc4	síň
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
profesor	profesor	k1gMnSc1	profesor
Woltmann	Woltmann	k1gMnSc1	Woltmann
reagoval	reagovat	k5eAaBmAgInS	reagovat
následným	následný	k2eAgNnSc7d1	následné
vyklizením	vyklizení	k1gNnSc7	vyklizení
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
protesty	protest	k1gInPc1	protest
přerostly	přerůst	k5eAaPmAgInP	přerůst
v	v	k7c4	v
potyčky	potyčka	k1gFnPc4	potyčka
mezi	mezi	k7c7	mezi
německými	německý	k2eAgMnPc7d1	německý
a	a	k8xC	a
českými	český	k2eAgMnPc7d1	český
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
několikadenním	několikadenní	k2eAgNnSc7d1	několikadenní
vězením	vězení	k1gNnSc7	vězení
a	a	k8xC	a
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
výtržnost	výtržnost	k1gFnSc4	výtržnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
cely	cela	k1gFnSc2	cela
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
pouze	pouze	k6eAd1	pouze
Bibli	bible	k1gFnSc4	bible
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
předčasně	předčasně	k6eAd1	předčasně
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gInPc2	jeho
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
1867	[number]	k4	1867
Jan	Jana	k1gFnPc2	Jana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
macechou	macecha	k1gFnSc7	macecha
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nespřátelil	spřátelit	k5eNaPmAgMnS	spřátelit
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
bez	bez	k7c2	bez
prostředků	prostředek	k1gInPc2	prostředek
odjel	odjet	k5eAaPmAgMnS	odjet
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
společně	společně	k6eAd1	společně
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Ženíškem	Ženíšek	k1gMnSc7	Ženíšek
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
s	s	k7c7	s
cyklem	cyklus	k1gInSc7	cyklus
Vlast	Vlasta	k1gFnPc2	Vlasta
v	v	k7c6	v
konkursu	konkurs	k1gInSc6	konkurs
na	na	k7c4	na
výzdobu	výzdoba	k1gFnSc4	výzdoba
foyer	foyer	k1gNnSc2	foyer
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
celkem	celkem	k6eAd1	celkem
o	o	k7c4	o
14	[number]	k4	14
lunet	luneta	k1gFnPc2	luneta
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
Miroticích	Mirotice	k1gFnPc6	Mirotice
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
dětskou	dětský	k2eAgFnSc7d1	dětská
láskou	láska	k1gFnSc7	láska
Marinou	Marina	k1gFnSc7	Marina
rozenou	rozený	k2eAgFnSc4d1	rozená
Kailovou	Kailová	k1gFnSc4	Kailová
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
začali	začít	k5eAaPmAgMnP	začít
bydlet	bydlet	k5eAaImF	bydlet
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
studijní	studijní	k2eAgFnSc4d1	studijní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
čtrnácti	čtrnáct	k4xCc6	čtrnáct
velkých	velký	k2eAgFnPc6d1	velká
lunetách	luneta	k1gFnPc6	luneta
pro	pro	k7c4	pro
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
ilustracích	ilustrace	k1gFnPc6	ilustrace
pro	pro	k7c4	pro
Arbesův	Arbesův	k2eAgInSc4d1	Arbesův
časopis	časopis	k1gInSc4	časopis
Šotek	šotek	k1gMnSc1	šotek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
cyklus	cyklus	k1gInSc1	cyklus
ilustrací	ilustrace	k1gFnPc2	ilustrace
národních	národní	k2eAgFnPc2d1	národní
písní	píseň	k1gFnPc2	píseň
Osiřelo	osiřet	k5eAaPmAgNnS	osiřet
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
cykly	cyklus	k1gInPc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
dílem	díl	k1gInSc7	díl
byl	být	k5eAaImAgInS	být
akvarel	akvarel	k1gInSc4	akvarel
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
návrhy	návrh	k1gInPc4	návrh
výzdoby	výzdoba	k1gFnSc2	výzdoba
domovních	domovní	k2eAgFnPc2d1	domovní
fasád	fasáda	k1gFnPc2	fasáda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Wiehlem	Wiehl	k1gMnSc7	Wiehl
<g/>
.	.	kIx.	.
</s>
<s>
Wiehlovy	Wiehlův	k2eAgFnPc1d1	Wiehlova
fasády	fasáda	k1gFnPc1	fasáda
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
přijímány	přijímán	k2eAgFnPc1d1	přijímána
veřejností	veřejnost	k1gFnSc7	veřejnost
i	i	k8xC	i
odborníky	odborník	k1gMnPc7	odborník
příznivě	příznivě	k6eAd1	příznivě
jako	jako	k8xC	jako
nový	nový	k2eAgInSc1d1	nový
prvek	prvek	k1gInSc1	prvek
ve	v	k7c6	v
výzdobě	výzdoba	k1gFnSc6	výzdoba
domů	dům	k1gInPc2	dům
a	a	k8xC	a
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
pražských	pražský	k2eAgFnPc2d1	Pražská
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
na	na	k7c6	na
výzdobě	výzdoba	k1gFnSc6	výzdoba
domů	dům	k1gInPc2	dům
postupně	postupně	k6eAd1	postupně
precizoval	precizovat	k5eAaBmAgInS	precizovat
svoje	svůj	k3xOyFgNnSc4	svůj
pojetí	pojetí	k1gNnSc4	pojetí
české	český	k2eAgFnSc2d1	Česká
novorenesance	novorenesance	k1gFnSc2	novorenesance
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
ponechával	ponechávat	k5eAaImAgMnS	ponechávat
spolupracujícím	spolupracující	k2eAgMnSc7d1	spolupracující
malířům	malíř	k1gMnPc3	malíř
a	a	k8xC	a
sochařům	sochař	k1gMnPc3	sochař
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
pouze	pouze	k6eAd1	pouze
vymezil	vymezit	k5eAaPmAgInS	vymezit
plochu	plocha	k1gFnSc4	plocha
pro	pro	k7c4	pro
sgrafita	sgrafito	k1gNnPc4	sgrafito
a	a	k8xC	a
vlastní	vlastní	k2eAgInPc4d1	vlastní
návrhy	návrh	k1gInPc4	návrh
výzdoby	výzdoba	k1gFnSc2	výzdoba
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
v	v	k7c6	v
kartonech	karton	k1gInPc6	karton
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
s	s	k7c7	s
Mikolášem	Mikoláš	k1gMnSc7	Mikoláš
Alšem	Aleš	k1gMnSc7	Aleš
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
výzdobě	výzdoba	k1gFnSc6	výzdoba
domů	dům	k1gInPc2	dům
čp.	čp.	k?	čp.
1682	[number]	k4	1682
Na	na	k7c6	na
Poříčí	Poříčí	k1gNnSc6	Poříčí
<g/>
,	,	kIx,	,
U	u	k7c2	u
Mladých	mladý	k2eAgMnPc2d1	mladý
Goliášů	Goliáš	k1gMnPc2	Goliáš
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
.	.	kIx.	.
</s>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
pro	pro	k7c4	pro
fasády	fasáda	k1gFnPc4	fasáda
těchto	tento	k3xDgInPc2	tento
domů	dům	k1gInPc2	dům
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
sgrafita	sgrafito	k1gNnPc4	sgrafito
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInPc4d1	renesanční
štíty	štít	k1gInPc4	štít
a	a	k8xC	a
lunety	luneta	k1gFnPc4	luneta
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
této	tento	k3xDgFnSc2	tento
spolupráce	spolupráce	k1gFnSc2	spolupráce
jsou	být	k5eAaImIp3nP	být
Alšovy	Alšův	k2eAgFnPc1d1	Alšova
alegorie	alegorie	k1gFnPc1	alegorie
na	na	k7c6	na
monumentální	monumentální	k2eAgFnSc6d1	monumentální
výzdobě	výzdoba	k1gFnSc6	výzdoba
Wiehlova	Wiehlův	k2eAgInSc2d1	Wiehlův
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Wiehlův	Wiehlův	k2eAgMnSc1d1	Wiehlův
kolega	kolega	k1gMnSc1	kolega
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Koula	Koula	k1gMnSc1	Koula
Wiehlovo	Wiehlův	k2eAgNnSc4d1	Wiehlův
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
definoval	definovat	k5eAaBmAgInS	definovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
ve	v	k7c6	v
Zprávách	zpráva	k1gFnPc6	zpráva
Spolku	spolek	k1gInSc2	spolek
architektů	architekt	k1gMnPc2	architekt
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
výklad	výklad	k1gInSc1	výklad
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
stylu	styl	k1gInSc3	styl
A.	A.	kA	A.
Wiehla	Wiehla	k1gFnSc2	Wiehla
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Wiehl	Wiehl	k1gFnSc1	Wiehl
bojuje	bojovat	k5eAaImIp3nS	bojovat
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
vyjádření	vyjádření	k1gNnSc4	vyjádření
architektonické	architektonický	k2eAgNnSc4d1	architektonické
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Čechy	Čechy	k1gFnPc4	Čechy
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
typických	typický	k2eAgFnPc2d1	typická
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
po	po	k7c6	po
prvé	prvý	k4xOgFnSc6	prvý
<g/>
,	,	kIx,	,
když	když	k8xS	když
postavil	postavit	k5eAaPmAgInS	postavit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
sgrafitový	sgrafitový	k2eAgInSc4d1	sgrafitový
domek	domek	k1gInSc4	domek
<g/>
"	"	kIx"	"
v	v	k7c6	v
Poštovské	poštovský	k2eAgFnSc6d1	Poštovská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pilně	pilně	k6eAd1	pilně
sbíral	sbírat	k5eAaImAgMnS	sbírat
památky	památka	k1gFnPc4	památka
naší	náš	k3xOp1gFnSc2	náš
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
hleděl	hledět	k5eAaImAgMnS	hledět
jich	on	k3xPp3gMnPc2	on
užíti	užít	k5eAaPmF	užít
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Wiehlovým	Wiehlův	k2eAgNnSc7d1	Wiehlův
přičiněním	přičinění	k1gNnSc7	přičinění
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
"	"	kIx"	"
<g/>
české	český	k2eAgFnSc3d1	Česká
renesanci	renesance	k1gFnSc3	renesance
<g/>
;	;	kIx,	;
cítíme	cítit	k5eAaImIp1nP	cítit
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
dosud	dosud	k6eAd1	dosud
nestanovil	stanovit	k5eNaPmAgInS	stanovit
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
ráz	ráz	k1gInSc4	ráz
těch	ten	k3xDgFnPc2	ten
staveb	stavba	k1gFnPc2	stavba
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
O	o	k7c6	o
uplatnění	uplatnění	k1gNnSc6	uplatnění
sgrafit	sgrafito	k1gNnPc2	sgrafito
referoval	referovat	k5eAaBmAgMnS	referovat
Jan	Jan	k1gMnSc1	Jan
Koula	Koula	k1gMnSc1	Koula
v	v	k7c6	v
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
Domy	dům	k1gInPc4	dům
pp	pp	k?	pp
<g/>
.	.	kIx.	.
architektů	architekt	k1gMnPc2	architekt
V.	V.	kA	V.
Skučka	Skučka	k1gFnSc1	Skučka
a	a	k8xC	a
J.	J.	kA	J.
Zeyera	Zeyer	k1gMnSc2	Zeyer
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgInS	být
uznáván	uznávat	k5eAaImNgInS	uznávat
už	už	k6eAd1	už
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
spíše	spíše	k9	spíše
jen	jen	k9	jen
jako	jako	k9	jako
kreslíř	kreslíř	k1gMnSc1	kreslíř
a	a	k8xC	a
dekoratér	dekoratér	k1gMnSc1	dekoratér
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
olejomalba	olejomalba	k1gFnSc1	olejomalba
byla	být	k5eAaImAgFnS	být
doceněna	docenit	k5eAaPmNgFnS	docenit
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Národopisné	národopisný	k2eAgFnSc6d1	národopisná
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
bylo	být	k5eAaImAgNnS	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
mnoho	mnoho	k4c4	mnoho
jeho	jeho	k3xOp3gNnPc2	jeho
sgrafit	sgrafito	k1gNnPc2	sgrafito
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
vydal	vydat	k5eAaPmAgInS	vydat
spolek	spolek	k1gInSc1	spolek
Mánes	Mánes	k1gMnSc1	Mánes
první	první	k4xOgFnSc4	první
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
publikaci	publikace	k1gFnSc4	publikace
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
výbor	výbor	k1gInSc1	výbor
prací	práce	k1gFnPc2	práce
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Alše	Aleš	k1gMnSc2	Aleš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
šedesátým	šedesátý	k4xOgFnPc3	šedesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
pražským	pražský	k2eAgMnSc7d1	pražský
měšťanem	měšťan	k1gMnSc7	měšťan
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gMnSc3	on
udělen	udělit	k5eAaPmNgInS	udělit
titul	titul	k1gInSc1	titul
inspektora	inspektor	k1gMnSc2	inspektor
kreslení	kreslení	k1gNnSc2	kreslení
na	na	k7c6	na
měšťanských	měšťanský	k2eAgFnPc6d1	měšťanská
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
rádce	rádce	k1gMnPc4	rádce
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
uměleckých	umělecký	k2eAgInPc6d1	umělecký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zásluhy	zásluha	k1gFnPc1	zásluha
o	o	k7c4	o
české	český	k2eAgNnSc4d1	české
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
byly	být	k5eAaImAgFnP	být
oceněny	oceněn	k2eAgFnPc1d1	oceněna
členstvím	členství	k1gNnSc7	členství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
řádným	řádný	k2eAgInSc7d1	řádný
členem	člen	k1gInSc7	člen
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
dopisujícím	dopisující	k2eAgInSc7d1	dopisující
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
mimořádným	mimořádný	k2eAgNnSc7d1	mimořádné
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dostávalo	dostávat	k5eAaImAgNnS	dostávat
i	i	k8xC	i
nevděku	nevděk	k1gInSc2	nevděk
a	a	k8xC	a
ponižování	ponižování	k1gNnSc2	ponižování
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
Nebejt	Nebejt	k?	Nebejt
toho	ten	k3xDgMnSc4	ten
žida	žid	k1gMnSc4	žid
Brandejse	Brandejs	k1gMnSc4	Brandejs
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
snad	snad	k9	snad
zemřel	zemřít	k5eAaPmAgMnS	zemřít
hlady	hlad	k1gInPc7	hlad
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Alexandr	Alexandr	k1gMnSc1	Alexandr
Brandejs	Brandejs	k1gMnSc1	Brandejs
byl	být	k5eAaImAgMnS	být
milovník	milovník	k1gMnSc1	milovník
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Alše	Aleš	k1gMnSc2	Aleš
často	často	k6eAd1	často
hostil	hostit	k5eAaImAgInS	hostit
za	za	k7c4	za
různé	různý	k2eAgFnPc4d1	různá
umělecké	umělecký	k2eAgFnPc4d1	umělecká
protislužby	protislužba	k1gFnPc4	protislužba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
například	například	k6eAd1	například
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Šebestián	Šebestián	k1gMnSc1	Šebestián
Daubek	Daubek	k1gMnSc1	Daubek
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
velkostatku	velkostatek	k1gInSc2	velkostatek
Liteň	Liteň	k1gFnSc1	Liteň
a	a	k8xC	a
Brněnec	Brněnec	k1gMnSc1	Brněnec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gNnSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
předními	přední	k2eAgMnPc7d1	přední
umělci	umělec	k1gMnPc7	umělec
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Brožík	Brožík	k1gMnSc1	Brožík
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hynais	Hynais	k1gFnSc2	Hynais
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
zval	zvát	k5eAaImAgMnS	zvát
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Litni	Liteň	k1gFnSc6	Liteň
a	a	k8xC	a
do	do	k7c2	do
Brněnce	Brněnec	k1gInSc2	Brněnec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ztratil	ztratit	k5eAaPmAgMnS	ztratit
na	na	k7c4	na
čas	čas	k1gInSc4	čas
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
slovenský	slovenský	k2eAgInSc4d1	slovenský
Černokňažník	Černokňažník	k1gInSc4	Černokňažník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
v	v	k7c6	v
Martině	Martina	k1gFnSc6	Martina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1893	[number]	k4	1893
a	a	k8xC	a
1897	[number]	k4	1897
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
;	;	kIx,	;
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
cestě	cesta	k1gFnSc6	cesta
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Bratislavu	Bratislava	k1gFnSc4	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Bělehradská	bělehradský	k2eAgFnSc1d1	Bělehradská
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
<g/>
)	)	kIx)	)
1018	[number]	k4	1018
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
(	(	kIx(	(
<g/>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Šejnosta	Šejnost	k1gMnSc2	Šejnost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
13	[number]	k4	13
let	let	k1gInSc1	let
tvořil	tvořit	k5eAaImAgInS	tvořit
a	a	k8xC	a
dne	den	k1gInSc2	den
10.7	[number]	k4	10.7
<g/>
.1913	.1913	k4	.1913
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
třípokojovém	třípokojový	k2eAgInSc6d1	třípokojový
bytě	byt	k1gInSc6	byt
bydlel	bydlet	k5eAaImAgMnS	bydlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
(	(	kIx(	(
<g/>
hrob	hrob	k1gInSc1	hrob
12	[number]	k4	12
<g/>
B-	B-	k1gFnPc2	B-
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vnukem	vnuk	k1gMnSc7	vnuk
jeho	jeho	k3xOp3gMnSc2	jeho
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
,	,	kIx,	,
řezbáře	řezbář	k1gMnSc2	řezbář
Jana	Jan	k1gMnSc2	Jan
Tesky	tesk	k1gInPc7	tesk
(	(	kIx(	(
<g/>
autora	autor	k1gMnSc2	autor
kazetového	kazetový	k2eAgInSc2d1	kazetový
stropu	strop	k1gInSc2	strop
Teskova	Teskův	k2eAgInSc2d1	Teskův
sálu	sál	k1gInSc2	sál
na	na	k7c6	na
Orlíku	orlík	k1gMnSc6	orlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgMnSc1d1	československý
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
významný	významný	k2eAgMnSc1d1	významný
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
publikačně	publikačně	k6eAd1	publikačně
afiliovaný	afiliovaný	k2eAgInSc1d1	afiliovaný
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
MUDr.	MUDr.	kA	MUDr.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mikulecký	Mikulecký	k2eAgMnSc1d1	Mikulecký
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
Z	z	k7c2	z
alšovské	alšovský	k2eAgFnSc2d1	alšovský
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
Slovensku	Slovensko	k1gNnSc3	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
tradiční	tradiční	k2eAgInSc1d1	tradiční
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
kariérní	kariérní	k2eAgInSc4d1	kariérní
život	život	k1gInSc4	život
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
Univerzitě	univerzita	k1gFnSc6	univerzita
Komenského	Komenského	k2eAgFnSc6d1	Komenského
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
Alšovým	Alšův	k2eAgMnSc7d1	Alšův
bratrancem	bratranec	k1gMnSc7	bratranec
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Šimon	Šimon	k1gMnSc1	Šimon
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
okultní	okultní	k2eAgFnSc2d1	okultní
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
lyžování	lyžování	k1gNnSc4	lyžování
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
malíře	malíř	k1gMnSc2	malíř
Josefa	Josef	k1gMnSc2	Josef
Váchala	Váchal	k1gMnSc2	Váchal
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
/	/	kIx~	/
Ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
čp.	čp.	k?	čp.
126	[number]	k4	126
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
2	[number]	k4	2
<g/>
/	/	kIx~	/
Nebovidská	Nebovidský	k2eAgFnSc1d1	Nebovidská
čp.	čp.	k?	čp.
462	[number]	k4	462
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
3	[number]	k4	3
<g/>
/	/	kIx~	/
Suchdol	Suchdol	k1gInSc1	Suchdol
4	[number]	k4	4
<g/>
/	/	kIx~	/
Lázeňská	lázeňská	k1gFnSc1	lázeňská
<g />
.	.	kIx.	.
</s>
<s>
čp.	čp.	k?	čp.
283	[number]	k4	283
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
5	[number]	k4	5
<g/>
/	/	kIx~	/
Osada	osada	k1gFnSc1	osada
Tejnka	Tejnko	k1gNnSc2	Tejnko
v	v	k7c6	v
Břevnově	Břevnov	k1gInSc6	Břevnov
6	[number]	k4	6
<g/>
/	/	kIx~	/
Thunovská	Thunovský	k2eAgFnSc1d1	Thunovská
čp.	čp.	k?	čp.
181	[number]	k4	181
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
7	[number]	k4	7
<g/>
/	/	kIx~	/
Chotkova	Chotkův	k2eAgInSc2d1	Chotkův
čp.	čp.	k?	čp.
538	[number]	k4	538
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
8	[number]	k4	8
<g/>
/	/	kIx~	/
Kroftova	Kroftův	k2eAgInSc2d1	Kroftův
čp.	čp.	k?	čp.
415	[number]	k4	415
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
12	[number]	k4	12
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
Pohořelec	Pohořelec	k1gInSc1	Pohořelec
čp.	čp.	k?	čp.
110	[number]	k4	110
<g/>
/	/	kIx~	/
<g/>
26	[number]	k4	26
10	[number]	k4	10
<g/>
/	/	kIx~	/
Karmelitská	karmelitský	k2eAgFnSc1d1	Karmelitská
čp.	čp.	k?	čp.
373	[number]	k4	373
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
11	[number]	k4	11
<g/>
/	/	kIx~	/
Sokolská	sokolská	k1gFnSc1	sokolská
čp.	čp.	k?	čp.
1492	[number]	k4	1492
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
19	[number]	k4	19
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
/	/	kIx~	/
Na	na	k7c6	na
Bojišti	bojiště	k1gNnSc6	bojiště
čp.	čp.	k?	čp.
1733	[number]	k4	1733
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
13	[number]	k4	13
<g/>
/	/	kIx~	/
Bělehradská	bělehradský	k2eAgFnSc1d1	Bělehradská
<g />
.	.	kIx.	.
</s>
<s>
čp.	čp.	k?	čp.
1018	[number]	k4	1018
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
42	[number]	k4	42
<g/>
)	)	kIx)	)
cykly	cyklus	k1gInPc1	cyklus
obrazů	obraz	k1gInPc2	obraz
Smysly	smysl	k1gInPc1	smysl
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Živly	živel	k1gInPc1	živel
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
další	další	k2eAgInPc1d1	další
obrazy	obraz	k1gInPc1	obraz
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
olejomalba	olejomalba	k1gFnSc1	olejomalba
"	"	kIx"	"
<g/>
Setkání	setkání	k1gNnSc1	setkání
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
s	s	k7c7	s
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
akvarel	akvarel	k1gInSc4	akvarel
"	"	kIx"	"
<g/>
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
fresky	freska	k1gFnPc4	freska
a	a	k8xC	a
sgrafita	sgrafito	k1gNnPc4	sgrafito
na	na	k7c4	na
průčelí	průčelí	k1gNnSc4	průčelí
mnoha	mnoho	k4c2	mnoho
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rottův	Rottův	k2eAgInSc1d1	Rottův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Wiehlův	Wiehlův	k2eAgInSc1d1	Wiehlův
dům	dům	k1gInSc1	dům
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
v	v	k7c6	v
Nuslích	Nusle	k1gFnPc6	Nusle
<g/>
)	)	kIx)	)
návrhy	návrh	k1gInPc4	návrh
fresek	freska	k1gFnPc2	freska
a	a	k8xC	a
sgrafit	sgrafito	k1gNnPc2	sgrafito
na	na	k7c6	na
15	[number]	k4	15
fasádách	fasáda	k1gFnPc6	fasáda
domů	domů	k6eAd1	domů
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
fasády	fasáda	k1gFnPc1	fasáda
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Nerudově	Nerudův	k2eAgNnSc6d1	Nerudovo
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
10	[number]	k4	10
cyklus	cyklus	k1gInSc1	cyklus
Sběr	sběr	k1gInSc4	sběr
léčivých	léčivý	k2eAgFnPc2d1	léčivá
bylin	bylina	k1gFnPc2	bylina
na	na	k7c6	na
domě	dům	k1gInSc6	dům
s	s	k7c7	s
lékárnou	lékárna	k1gFnSc7	lékárna
na	na	k7c4	na
nám.	nám.	k?	nám.
Republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
27	[number]	k4	27
dům	dům	k1gInSc1	dům
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
Červeného	Červeného	k2eAgNnSc1d1	Červeného
srdce	srdce	k1gNnSc1	srdce
na	na	k7c4	na
nám.	nám.	k?	nám.
Republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
36	[number]	k4	36
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
cyklus	cyklus	k1gInSc1	cyklus
Řemesla	řemeslo	k1gNnSc2	řemeslo
na	na	k7c6	na
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Sedláčkově	Sedláčkův	k2eAgNnSc6d1	Sedláčkovo
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
31	[number]	k4	31
fasáda	fasáda	k1gFnSc1	fasáda
Cingrošova	Cingrošův	k2eAgInSc2d1	Cingrošův
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Bezručově	Bezručův	k2eAgNnSc6d1	Bezručovo
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
31	[number]	k4	31
cyklus	cyklus	k1gInSc1	cyklus
Sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Štechově	Štechův	k2eAgFnSc6d1	Štechova
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Dvořákově	Dvořákův	k2eAgNnSc6d1	Dvořákovo
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
1	[number]	k4	1
fasády	fasáda	k1gFnSc2	fasáda
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Tovární	tovární	k2eAgFnSc6d1	tovární
ul	ul	kA	ul
<g/>
<g />
.	.	kIx.	.
</s>
<s>
č.	č.	k?	č.
6	[number]	k4	6
a	a	k8xC	a
8	[number]	k4	8
výzdoba	výzdoba	k1gFnSc1	výzdoba
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Purkyňově	Purkyňův	k2eAgNnSc6d1	Purkyňovo
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
35	[number]	k4	35
a	a	k8xC	a
domu	dům	k1gInSc2	dům
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Petákovy	Petákův	k2eAgFnSc2d1	Petákova
a	a	k8xC	a
Jegellonské	Jegellonský	k2eAgFnSc2d1	Jegellonský
ulice	ulice	k1gFnSc2	ulice
fasády	fasáda	k1gFnSc2	fasáda
domů	dům	k1gInPc2	dům
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
sgrafita	sgrafito	k1gNnSc2	sgrafito
na	na	k7c6	na
průčelích	průčelí	k1gNnPc6	průčelí
hotelu	hotel	k1gInSc2	hotel
Otava	Otava	k1gFnSc1	Otava
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
sgrafita	sgrafito	k1gNnSc2	sgrafito
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
a	a	k8xC	a
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
výzdoba	výzdoba	k1gFnSc1	výzdoba
fasády	fasáda	k1gFnSc2	fasáda
domu	dům	k1gInSc2	dům
<g />
.	.	kIx.	.
</s>
<s>
č.	č.	k?	č.
209	[number]	k4	209
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Národní	národní	k2eAgFnSc2d1	národní
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
výzdoba	výzdoba	k1gFnSc1	výzdoba
chrámu	chrám	k1gInSc2	chrám
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
ve	v	k7c6	v
Vodňanech	Vodňan	k1gMnPc6	Vodňan
společně	společně	k6eAd1	společně
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Ženíškem	Ženíšek	k1gMnSc7	Ženíšek
cyklus	cyklus	k1gInSc4	cyklus
obrazů	obraz	k1gInPc2	obraz
"	"	kIx"	"
<g/>
Vlast	Vlasta	k1gFnPc2	Vlasta
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
Národní	národní	k2eAgNnSc4d1	národní
Divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
-	-	kIx~	-
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
14	[number]	k4	14
lunet	luneta	k1gFnPc2	luneta
<g/>
,	,	kIx,	,
4	[number]	k4	4
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
a	a	k8xC	a
3	[number]	k4	3
nástropní	nástropní	k2eAgFnSc2d1	nástropní
pole	pole	k1gFnSc2	pole
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
foyeru	foyer	k1gInSc2	foyer
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
fasáda	fasáda	k1gFnSc1	fasáda
-	-	kIx~	-
sgrafita	sgrafito	k1gNnSc2	sgrafito
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
1682	[number]	k4	1682
Na	na	k7c6	na
Poříčí	Poříčí	k1gNnSc6	Poříčí
fasáda	fasáda	k1gFnSc1	fasáda
-	-	kIx~	-
sgrafita	sgrafito	k1gNnPc4	sgrafito
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
vodárna	vodárna	k1gFnSc1	vodárna
fasáda	fasáda	k1gFnSc1	fasáda
-	-	kIx~	-
sgrafita	sgrafito	k1gNnSc2	sgrafito
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Mladých	mladý	k2eAgMnPc2d1	mladý
Goliášů	Goliáš	k1gMnPc2	Goliáš
fasáda	fasáda	k1gFnSc1	fasáda
-	-	kIx~	-
sgrafita	sgrafito	k1gNnPc4	sgrafito
Wiehlova	Wiehlův	k2eAgNnPc4d1	Wiehlův
domuVáclavské	domuVáclavský	k2eAgFnSc6d1	domuVáclavský
nám.	nám.	k?	nám.
792	[number]	k4	792
knižní	knižní	k2eAgFnSc1d1	knižní
ilustrace	ilustrace	k1gFnSc1	ilustrace
děl	dělo	k1gNnPc2	dělo
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Čelakovského	Čelakovský	k2eAgMnSc2d1	Čelakovský
<g/>
,	,	kIx,	,
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
Arbese	Arbes	k1gMnSc2	Arbes
(	(	kIx(	(
<g/>
české	český	k2eAgFnSc2d1	Česká
lidové	lidový	k2eAgFnSc2d1	lidová
písně	píseň	k1gFnSc2	píseň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Špalíček	špalíček	k1gInSc4	špalíček
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
přísloví	přísloví	k1gNnSc2	přísloví
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
kresby	kresba	k1gFnPc1	kresba
pro	pro	k7c4	pro
časopisy	časopis	k1gInPc4	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Šotek	šotek	k1gMnSc1	šotek
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiných	jiný	k2eAgFnPc2d1	jiná
autorem	autor	k1gMnSc7	autor
největšího	veliký	k2eAgInSc2d3	veliký
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
Pobití	pobití	k1gNnSc1	pobití
Sasíků	Sasík	k1gMnPc2	Sasík
pod	pod	k7c7	pod
Hrubou	hrubý	k2eAgFnSc7d1	hrubá
Skálou	skála	k1gFnSc7	skála
malba	malba	k1gFnSc1	malba
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
a	a	k8xC	a
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Mikulči	Mikulč	k1gFnSc6	Mikulč
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
režisérem	režisér	k1gMnSc7	režisér
Václavem	Václav	k1gMnSc7	Václav
Krškou	Krška	k1gMnSc7	Krška
natočen	natočit	k5eAaBmNgInS	natočit
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
však	však	k9	však
dost	dost	k6eAd1	dost
poplatný	poplatný	k2eAgInSc1d1	poplatný
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
věrohodný	věrohodný	k2eAgInSc1d1	věrohodný
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgMnS	hrát
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
.	.	kIx.	.
</s>
<s>
Pešina	Pešina	k1gFnSc1	Pešina
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
malířská	malířský	k2eAgFnSc1d1	malířská
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
Kotalík	Kotalík	k1gMnSc1	Kotalík
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
NG	NG	kA	NG
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
MÁDL	MÁDL	kA	MÁDL
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Aleš	Aleš	k1gMnSc1	Aleš
:	:	kIx,	:
S	s	k7c7	s
podobiznou	podobizna	k1gFnSc7	podobizna
od	od	k7c2	od
M.	M.	kA	M.
Švabinského	Švabinský	k2eAgInSc2d1	Švabinský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Spolek	spolek	k1gInSc1	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zlatoroh	Zlatoroh	k1gInSc1	Zlatoroh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Volavková	Volavková	k1gFnSc1	Volavková
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
MUCHA	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
51	[number]	k4	51
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
,	,	kIx,	,
372	[number]	k4	372
<g/>
,	,	kIx,	,
408	[number]	k4	408
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
ŠTECH	ŠTECH	kA	ŠTECH
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zamlženém	zamlžený	k2eAgNnSc6d1	zamlžené
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
první	první	k4xOgInSc1	první
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
I.	I.	kA	I.
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
A.	A.	kA	A.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
155	[number]	k4	155
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Brandejs	Brandejs	k1gMnSc1	Brandejs
František	František	k1gMnSc1	František
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
Josef	Josef	k1gMnSc1	Josef
Bosáček	bosáček	k1gMnSc1	bosáček
Antonín	Antonín	k1gMnSc1	Antonín
Wiehl	Wiehl	k1gMnSc1	Wiehl
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Daubek	Daubek	k1gMnSc1	Daubek
Josef	Josef	k1gMnSc1	Josef
Šebestián	Šebestián	k1gMnSc1	Šebestián
Daubek	Daubek	k1gMnSc1	Daubek
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g />
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Aleš	Aleš	k1gMnSc1	Aleš
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
:	:	kIx,	:
Setkání	setkání	k1gNnSc1	setkání
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
s	s	k7c7	s
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gInSc7	Korvín
Matriční	matriční	k2eAgInSc1d1	matriční
zápis	zápis	k1gInSc1	zápis
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
shora	shora	k6eAd1	shora
<g/>
)	)	kIx)	)
narození	narození	k1gNnSc1	narození
v	v	k7c6	v
Miroticích	Mirotice	k1gFnPc6	Mirotice
119	[number]	k4	119
-	-	kIx~	-
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
"	"	kIx"	"
Alšovo	Alšův	k2eAgNnSc1d1	Alšovo
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
virtuální	virtuální	k2eAgFnSc6d1	virtuální
galerii	galerie	k1gFnSc6	galerie
(	(	kIx(	(
<g/>
artchiv	artchiv	k6eAd1	artchiv
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Text	text	k1gInSc1	text
o	o	k7c6	o
Mikoláši	Mikoláš	k1gMnSc6	Mikoláš
Alšovi	Aleš	k1gMnSc6	Aleš
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Josefa	Josef	k1gMnSc2	Josef
Žáka	Žák	k1gMnSc2	Žák
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
ve	v	k7c6	v
Slaném	Slaný	k1gInSc6	Slaný
</s>
