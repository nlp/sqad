<s>
Lando	Landa	k1gMnSc5
Norris	Norris	k1gInSc1
</s>
<s>
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnPc6
</s>
<s>
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1999	#num#	k4
(	(	kIx(
<g/>
21	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Bristol	Bristol	k1gInSc1
Současné	současný	k2eAgInPc1d1
působení	působení	k1gNnSc4
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Současný	současný	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
<g/>
–	–	k?
<g/>
Mercedes	mercedes	k1gInSc1
Číslo	číslo	k1gNnSc1
vozu	vůz	k1gInSc2
</s>
<s>
4	#num#	k4
Umístění	umístění	k1gNnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
41	#num#	k4
b.	b.	k?
<g/>
)	)	kIx)
Kariéra	kariéra	k1gFnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Aktivní	aktivní	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
</s>
<s>
2019	#num#	k4
<g/>
-dosud	-dosuda	k1gFnPc2
Týmy	tým	k1gInPc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
-dosud	-dosuda	k1gFnPc2
<g/>
)	)	kIx)
Závody	závod	k1gInPc1
</s>
<s>
42	#num#	k4
(	(	kIx(
<g/>
42	#num#	k4
startů	start	k1gInPc2
<g/>
)	)	kIx)
Mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
0	#num#	k4
Vyhrané	vyhraný	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
0	#num#	k4
Stupně	stupeň	k1gInSc2
vítězů	vítěz	k1gMnPc2
</s>
<s>
2	#num#	k4
Pole	pole	k1gNnSc1
positions	positionsa	k1gFnPc2
</s>
<s>
0	#num#	k4
Nejrychlejší	rychlý	k2eAgFnSc1d3
kola	kola	k1gFnSc1
</s>
<s>
2	#num#	k4
Body	bod	k1gInPc7
celkem	celkem	k6eAd1
</s>
<s>
187	#num#	k4
Nejlepší	dobrý	k2eAgInPc1d3
umístění	umístění	k1gNnSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Nejlepší	dobrý	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
závodě	závod	k1gInSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
První	první	k4xOgInSc1
závod	závod	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
2019	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnPc3
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1999	#num#	k4
Bristol	Bristol	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
automobilový	automobilový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
jezdí	jezdit	k5eAaImIp3nP
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
za	za	k7c4
tým	tým	k1gInSc4
McLaren	McLarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2015	#num#	k4
až	až	k9
2016	#num#	k4
získal	získat	k5eAaPmAgInS
několik	několik	k4yIc4
mistrovských	mistrovský	k2eAgInPc2d1
titulů	titul	k1gInPc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
formulových	formulový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
získal	získat	k5eAaPmAgInS
mistrovský	mistrovský	k2eAgInSc1d1
titul	titul	k1gInSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
3	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
působil	působit	k5eAaImAgMnS
ve	v	k7c6
Formuli	formule	k1gFnSc6
2	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
2020	#num#	k4
založil	založit	k5eAaPmAgMnS
esportový	esportový	k2eAgInSc4d1
tým	tým	k1gInSc4
Quadrant	Quadrant	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
juniorskému	juniorský	k2eAgInSc3d1
týmu	tým	k1gInSc3
stáje	stáj	k1gFnSc2
McLaren	McLarna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
oficiálním	oficiální	k2eAgInSc7d1
testovacím	testovací	k2eAgInSc7d1
a	a	k8xC
rezervním	rezervní	k2eAgMnSc7d1
jezdcem	jezdec	k1gMnSc7
McLarenu	McLaren	k1gInSc2
pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
nastoupil	nastoupit	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
pátečním	páteční	k2eAgInSc6d1
volném	volný	k2eAgInSc6d1
tréninku	trénink	k1gInSc6
na	na	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2019	#num#	k4
oficiálním	oficiální	k2eAgInSc7d1
jezdcem	jezdec	k1gInSc7
McLarenu	McLaren	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
týmovým	týmový	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
je	být	k5eAaImIp3nS
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
získal	získat	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
podium	podium	k1gNnSc4
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kompletní	kompletní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
</s>
<s>
Legenda	legenda	k1gFnSc1
k	k	k7c3
tabulce	tabulka	k1gFnSc3
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
ZlatáVítěz	ZlatáVítěz	k1gMnSc1
</s>
<s>
Stříbrná	stříbrná	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ZelenáBodované	ZelenáBodovaný	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Nebodované	bodovaný	k2eNgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS
neklasifikován	klasifikován	k2eNgMnSc1d1
(	(	kIx(
<g/>
NC	NC	kA
<g/>
)	)	kIx)
</s>
<s>
FialováOdstoupil	FialováOdstoupit	k5eAaPmAgInS
(	(	kIx(
<g/>
Ret	ret	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Červená	červený	k2eAgFnSc1d1
</s>
<s>
Nekvalifikoval	kvalifikovat	k5eNaBmAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNQ	DNQ	kA
<g/>
)	)	kIx)
</s>
<s>
Nepředkvalifikoval	Nepředkvalifikovat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNPQ	DNPQ	kA
<g/>
)	)	kIx)
</s>
<s>
ČernáDiskvalifikován	ČernáDiskvalifikován	k2eAgMnSc1d1
(	(	kIx(
<g/>
DSQ	DSQ	kA
<g/>
)	)	kIx)
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
Nestartoval	startovat	k5eNaBmAgMnS
(	(	kIx(
<g/>
DNS	DNS	kA
<g/>
)	)	kIx)
</s>
<s>
Závod	závod	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Pouze	pouze	k6eAd1
trénoval	trénovat	k5eAaImAgMnS
(	(	kIx(
<g/>
PO	po	k7c6
<g/>
)	)	kIx)
</s>
<s>
Páteční	páteční	k2eAgMnSc1d1
testovací	testovací	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
TD	TD	kA
<g/>
)	)	kIx)
</s>
<s>
Bez	bez	k7c2
barvy	barva	k1gFnSc2
</s>
<s>
Netrénoval	trénovat	k5eNaImAgMnS
(	(	kIx(
<g/>
DNP	DNP	kA
<g/>
)	)	kIx)
</s>
<s>
Vyřazen	vyřazen	k2eAgMnSc1d1
(	(	kIx(
<g/>
EX	ex	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nepřijel	přijet	k5eNaPmAgMnS
(	(	kIx(
<g/>
DNA	dna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odvolal	odvolat	k5eAaPmAgMnS
účast	účast	k1gFnSc4
(	(	kIx(
<g/>
WD	WD	kA
<g/>
)	)	kIx)
</s>
<s>
Označení	označení	k1gNnSc1
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Tučnost	tučnost	k1gFnSc1
</s>
<s>
Pole	pole	k1gFnSc1
position	position	k1gInSc1
</s>
<s>
Kurzíva	kurzíva	k1gFnSc1
</s>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3
kolo	kolo	k1gNnSc1
</s>
<s>
F	F	kA
</s>
<s>
FanBoost	FanBoost	k1gFnSc1
</s>
<s>
G	G	kA
</s>
<s>
Nejrychlejší	rychlý	k2eAgMnSc1d3
v	v	k7c6
kvalifikační	kvalifikační	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
</s>
<s>
†	†	k?
</s>
<s>
Jezdec	jezdec	k1gMnSc1
nedojel	dojet	k5eNaPmAgMnS
do	do	k7c2
cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
klasifikován	klasifikovat	k5eAaImNgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
odjel	odjet	k5eAaPmAgMnS
více	hodně	k6eAd2
než	než	k8xS
90	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
‡	‡	k?
</s>
<s>
Byl	být	k5eAaImAgMnS
udělován	udělován	k2eAgInSc4d1
poloviční	poloviční	k2eAgInSc4d1
počet	počet	k1gInSc4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
bylo	být	k5eAaImAgNnS
odjeto	odjet	k2eAgNnSc1d1
méně	málo	k6eAd2
než	než	k8xS
75	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Šasi	šasi	k1gNnSc1
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL33	MCL33	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
R.	R.	kA
<g/>
E	E	kA
<g/>
.18	.18	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUS	AUS	kA
</s>
<s>
BHR	BHR	kA
</s>
<s>
CHN	CHN	kA
</s>
<s>
AZE	AZE	kA
</s>
<s>
ESP	ESP	kA
</s>
<s>
MON	MON	kA
</s>
<s>
CAN	CAN	kA
</s>
<s>
FRA	FRA	kA
</s>
<s>
AUT	aut	k1gInSc1
</s>
<s>
GBR	GBR	kA
</s>
<s>
GER	Gera	k1gFnPc2
</s>
<s>
HUN	Hun	k1gMnSc1
</s>
<s>
BELTD	BELTD	kA
</s>
<s>
ITATD	ITATD	kA
</s>
<s>
SIN	sin	kA
</s>
<s>
RUSTD	RUSTD	kA
</s>
<s>
JPNTD	JPNTD	kA
</s>
<s>
USATD	USATD	kA
</s>
<s>
MEXTD	MEXTD	kA
</s>
<s>
BRATD	BRATD	kA
</s>
<s>
ABU	ABU	kA
</s>
<s>
–	–	k?
</s>
<s>
–	–	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL34	MCL34	k1gFnPc2
</s>
<s>
Renault	renault	k1gInSc1
E-Tech	E-Tech	k1gInSc1
19	#num#	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUS12	AUS12	k4
</s>
<s>
BHR6	BHR6	k4
</s>
<s>
CHN	CHN	kA
<g/>
18	#num#	k4
<g/>
†	†	k?
</s>
<s>
AZE8	AZE8	k4
</s>
<s>
ESPRet	ESPRet	k1gMnSc1
</s>
<s>
MON11	MON11	k4
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
FRA9	FRA9	k4
</s>
<s>
AUT6	AUT6	k4
</s>
<s>
GBR11	GBR11	k4
</s>
<s>
GERRet	GERRet	k1gMnSc1
</s>
<s>
HUN9	HUN9	k4
</s>
<s>
BEL	bel	k1gInSc1
<g/>
11	#num#	k4
<g/>
†	†	k?
</s>
<s>
ITA10	ITA10	k4
</s>
<s>
SIN7	SIN7	k4
</s>
<s>
RUS8	RUS8	k4
</s>
<s>
JPN11	JPN11	k4
</s>
<s>
MEXRet	MEXRet	k1gMnSc1
</s>
<s>
USA7	USA7	k4
</s>
<s>
BRA8	BRA8	k4
</s>
<s>
ABU8	ABU8	k4
</s>
<s>
49	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2020	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL35	MCL35	k1gFnPc2
</s>
<s>
Renault	renault	k1gInSc1
E-Tech	E-Tech	k1gInSc1
20	#num#	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUT3	AUT3	k4
</s>
<s>
STY5	STY5	k4
</s>
<s>
HUN13	HUN13	k4
</s>
<s>
GBR5	GBR5	k4
</s>
<s>
70A9	70A9	k4
</s>
<s>
ESP10	ESP10	k4
</s>
<s>
BEL7	BEL7	k4
</s>
<s>
ITA4	ITA4	k4
</s>
<s>
TUS6	TUS6	k4
</s>
<s>
RUS15	RUS15	k4
</s>
<s>
EIFRet	EIFRet	k1gMnSc1
</s>
<s>
POR13	POR13	k4
</s>
<s>
EMI8	EMI8	k4
</s>
<s>
TUR8	TUR8	k4
</s>
<s>
BHR4	BHR4	k4
</s>
<s>
SKR10	SKR10	k4
</s>
<s>
ABU5	ABU5	k4
</s>
<s>
97	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2021	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL35M	MCL35M	k1gFnSc2
</s>
<s>
Mercedes	mercedes	k1gInSc1
M12	M12	k1gMnPc2
E	E	kA
Performance	performance	k1gFnSc1
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
BHR4	BHR4	k4
</s>
<s>
EMI3	EMI3	k4
</s>
<s>
POR5	POR5	k4
</s>
<s>
ESP8	ESP8	k4
</s>
<s>
MON	MON	kA
</s>
<s>
AZE	AZE	kA
</s>
<s>
TUR	tur	k1gMnSc1
</s>
<s>
FRA	FRA	kA
</s>
<s>
AUT	aut	k1gInSc1
</s>
<s>
GBR	GBR	kA
</s>
<s>
HUN	Hun	k1gMnSc1
</s>
<s>
BEL	bel	k1gInSc1
</s>
<s>
NED	NED	kA
</s>
<s>
ITA	ITA	kA
</s>
<s>
RUS	Rus	k1gFnSc1
</s>
<s>
SIN	sin	kA
</s>
<s>
JPN	JPN	kA
</s>
<s>
USA	USA	kA
</s>
<s>
MEX	MEX	kA
</s>
<s>
SAP	sapa	k1gFnPc2
</s>
<s>
AUS	AUS	kA
</s>
<s>
SAU	SAU	kA
</s>
<s>
ABU	ABU	kA
</s>
<s>
41	#num#	k4
<g/>
*	*	kIx~
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
*	*	kIx~
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
*	*	kIx~
Sezóna	sezóna	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
KHOROUNZHIY	KHOROUNZHIY	kA
<g/>
,	,	kIx,
Valentin	Valentin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnPc2
team	team	k1gInSc1
signs	signs	k6eAd1
Lando	Landa	k1gMnSc5
Norris	Norris	k1gInSc1
to	ten	k3xDgNnSc1
its	its	k?
junior	junior	k1gMnSc1
programme	programit	k5eAaPmRp1nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
February	Februara	k1gFnSc2
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lando	Landa	k1gMnSc5
Norris	Norris	k1gInSc1
to	ten	k3xDgNnSc1
drive	drive	k1gInSc1
for	forum	k1gNnPc2
McLaren	McLarno	k1gNnPc2
in	in	k?
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
McLaren	McLarna	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
Geoff	Geoff	k1gMnSc1
Crossley	Crosslea	k1gFnSc2
•	•	k?
Joe	Joe	k1gMnSc1
Fry	Fry	k1gMnSc1
•	•	k?
Bob	Bob	k1gMnSc1
Gérard	Gérard	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Hampshire	Hampshir	k1gInSc5
•	•	k?
Cuth	Cuth	k1gMnSc1
Harrison	Harrison	k1gMnSc1
•	•	k?
Leslie	Leslie	k1gFnSc2
Johnson	Johnson	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Murray	Murraa	k1gFnSc2
•	•	k?
Reg	Reg	k1gMnSc1
Parnell	Parnell	k1gMnSc1
•	•	k?
Tony	Tony	k1gMnSc1
Rolt	Rolt	k1gMnSc1
•	•	k?
Brian	Brian	k1gMnSc1
Shawe	Shaw	k1gFnSc2
Taylor	Taylor	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Walker	Walker	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Whitehead	Whitehead	k1gInSc1
•	•	k?
George	George	k1gInSc1
Abecassis	Abecassis	k1gFnSc1
•	•	k?
Peter	Petra	k1gFnPc2
Fotheringham	Fotheringham	k1gInSc1
Parker	Parker	k1gMnSc1
•	•	k?
Duncan	Duncan	k1gInSc1
Hamilton	Hamilton	k1gInSc1
•	•	k?
John	John	k1gMnSc1
James	James	k1gMnSc1
•	•	k?
Stirling	Stirling	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Moss	Moss	k1gInSc1
•	•	k?
Ken	Ken	k1gMnSc1
Richardson	Richardson	k1gMnSc1
•	•	k?
Bill	Bill	k1gMnSc1
Aston	Aston	k1gMnSc1
•	•	k?
Eric	Eric	k1gInSc1
Brandon	Brandon	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Collins	Collinsa	k1gFnPc2
•	•	k?
Tony	tonus	k1gInPc1
Crook	Crook	k1gInSc1
•	•	k?
Ken	Ken	k1gFnSc1
Downing	Downing	k1gInSc1
•	•	k?
Mike	Mike	k1gInSc1
Hawthorn	Hawthorn	k1gInSc1
•	•	k?
Lance	lance	k1gNnSc2
Macklin	Macklina	k1gFnPc2
•	•	k?
Ken	Ken	k1gMnSc5
McAlpine	McAlpin	k1gMnSc5
•	•	k?
Robin	Robina	k1gFnPc2
Montgomerie	Montgomerie	k1gFnSc1
Charrington	Charrington	k1gInSc1
•	•	k?
Dennis	Dennis	k1gInSc1
Poore	Poor	k1gInSc5
•	•	k?
Roy	Roy	k1gMnSc1
Salvadori	Salvador	k1gFnSc2
•	•	k?
Eric	Eric	k1gInSc1
Thompson	Thompson	k1gMnSc1
•	•	k?
Ken	Ken	k1gFnSc1
Wharton	Wharton	k1gInSc1
•	•	k?
Graham	graham	k1gInSc1
Whitehead	Whitehead	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Barber	Barber	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
Fairman	Fairman	k1gMnSc1
•	•	k?
Rodney	Rodnea	k1gFnSc2
Nuckey	Nuckea	k1gFnSc2
•	•	k?
Ian	Ian	k1gMnSc1
Stewart	Stewart	k1gMnSc1
•	•	k?
Jimmy	Jimma	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
Don	Don	k1gMnSc1
Beauman	Beauman	k1gMnSc1
•	•	k?
Ron	ron	k1gInSc1
Flockhart	Flockhart	k1gInSc1
•	•	k?
Horace	Horace	k1gFnSc2
Gould	Gould	k1gMnSc1
•	•	k?
Leslie	Leslie	k1gFnSc2
Marr	Marr	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Riseley	Riselea	k1gFnSc2
Prichard	Prichard	k1gMnSc1
•	•	k?
Leslie	Leslie	k1gFnSc2
Thorne	Thorn	k1gInSc5
•	•	k?
Bill	Bill	k1gMnSc1
Whitehouse	Whitehouse	k1gFnSc2
•	•	k?
Ted	Ted	k1gMnSc1
Whiteaway	Whiteawaa	k1gFnSc2
•	•	k?
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
Colin	Colin	k1gMnSc1
Chapman	Chapman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Emery	Emera	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bruce	Bruce	k1gMnSc1
Halford	Halford	k1gMnSc1
•	•	k?
Les	les	k1gInSc1
Leston	Leston	k1gInSc1
•	•	k?
Mike	Mike	k1gInSc1
Oliver	Oliver	k1gInSc1
•	•	k?
Archie	Archie	k1gFnSc2
Scott	Scott	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
Desmond	Desmond	k1gInSc1
Titterington	Titterington	k1gInSc1
•	•	k?
Ivor	Ivor	k1gInSc1
Bueb	Bueb	k1gMnSc1
•	•	k?
Dick	Dick	k1gMnSc1
Gibson	Gibson	k1gMnSc1
•	•	k?
Ivor	Ivor	k1gMnSc1
Bueb	Bueb	k1gMnSc1
•	•	k?
Mike	Mike	k1gInSc1
MacDowel	MacDowel	k1gMnSc1
•	•	k?
Tony	Tony	k1gMnSc1
Marsh	Marsh	k1gMnSc1
•	•	k?
Brian	Brian	k1gMnSc1
Naylor	Naylor	k1gMnSc1
•	•	k?
Cliff	Cliff	k1gMnSc1
Allison	Allison	k1gMnSc1
•	•	k?
Tom	Tom	k1gMnSc1
Bridger	Bridger	k1gMnSc1
•	•	k?
Ian	Ian	k1gFnSc2
Burgess	Burgessa	k1gFnPc2
•	•	k?
Colin	Colin	k1gMnSc1
Davis	Davis	k1gFnSc2
•	•	k?
Bernie	Bernie	k1gFnSc2
Ecclestone	Eccleston	k1gInSc5
•	•	k?
Graham	Graham	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Hill	Hill	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
Stacey	Stacea	k1gFnSc2
•	•	k?
Peter	Peter	k1gMnSc1
Ashdown	Ashdown	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Bristow	Bristow	k1gMnSc1
•	•	k?
Keith	Keith	k1gMnSc1
Greene	Green	k1gInSc5
•	•	k?
Bill	Bill	k1gMnSc1
Moss	Mossa	k1gFnPc2
•	•	k?
Mike	Mik	k1gInPc1
Parkes	Parkes	k1gInSc1
•	•	k?
Tim	Tim	k?
Parnell	Parnell	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Piper	Piper	k1gMnSc1
•	•	k?
Dennis	Dennis	k1gInSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
Henry	Henry	k1gMnSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
Mike	Mike	k1gInSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
Trevor	Trevor	k1gMnSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
Brian	Brian	k1gMnSc1
Whitehouse	Whitehouse	k1gFnSc2
•	•	k?
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
Arthur	Arthur	k1gMnSc1
Owen	Owen	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Surtees	Surtees	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vic	Vic	k1gMnSc1
Wilson	Wilson	k1gMnSc1
•	•	k?
Gerry	Gerra	k1gFnSc2
Ashmore	Ashmor	k1gInSc5
•	•	k?
John	John	k1gMnSc1
Campbell	Campbell	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
Geoff	Geoff	k1gMnSc1
Duke	Duk	k1gFnSc2
•	•	k?
Jackie	Jackie	k1gFnSc2
Lewis	Lewis	k1gFnSc2
•	•	k?
Bob	Bob	k1gMnSc1
Anderson	Anderson	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Arundell	Arundell	k1gMnSc1
•	•	k?
Mike	Mik	k1gFnSc2
Hailwood	Hailwooda	k1gFnPc2
•	•	k?
David	David	k1gMnSc1
Prophet	Prophet	k1gMnSc1
•	•	k?
Ian	Ian	k1gMnSc1
Raby	rab	k1gMnPc7
•	•	k?
Mike	Mik	k1gInSc2
Spence	Spenec	k1gInSc2
•	•	k?
Dick	Dick	k1gInSc1
Attwood	Attwood	k1gInSc1
•	•	k?
Brian	Brian	k1gMnSc1
Gubby	Gubba	k1gFnSc2
•	•	k?
John	John	k1gMnSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Rhodes	Rhodes	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
Rollinson	Rollinson	k1gMnSc1
•	•	k?
Jackie	Jackie	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Stewart	Stewart	k1gMnSc1
•	•	k?
Piers	Piers	k1gInSc1
Courage	Courage	k1gInSc1
•	•	k?
Chris	Chris	k1gFnSc2
Irwin	Irwin	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Lawrence	Lawrence	k1gFnSc2
•	•	k?
Alan	Alan	k1gMnSc1
Rees	Reesa	k1gFnPc2
•	•	k?
David	David	k1gMnSc1
Hobbs	Hobbs	k1gInSc1
•	•	k?
Jackie	Jackie	k1gFnSc2
Oliver	Oliver	k1gMnSc1
•	•	k?
Brian	Brian	k1gMnSc1
Redman	Redman	k1gMnSc1
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Williams	Williams	k1gInSc1
•	•	k?
Derek	Derek	k1gInSc1
Bell	bell	k1gInSc1
•	•	k?
Vic	Vic	k1gFnSc2
Elford	Elford	k1gMnSc1
•	•	k?
Tony	Tony	k1gMnSc1
Lanfranchi	Lanfranch	k1gFnSc2
•	•	k?
Keith	Keith	k1gMnSc1
St	St	kA
John	John	k1gMnSc1
•	•	k?
Robin	robin	k2eAgInSc1d1
Widdows	Widdows	k1gInSc1
•	•	k?
John	John	k1gMnSc1
Miles	Miles	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Westbury	Westbura	k1gFnSc2
•	•	k?
Peter	Peter	k1gMnSc1
Gethin	Gethin	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mike	Mike	k1gNnSc1
Beuttler	Beuttler	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Craft	Craft	k1gMnSc1
•	•	k?
Ray	Ray	k1gMnSc1
Allen	Allen	k1gMnSc1
•	•	k?
Tony	Tony	k1gMnSc1
Trimmer	Trimmer	k1gMnSc1
•	•	k?
James	James	k1gInSc1
Hunt	hunt	k1gInSc1
•	•	k?
David	David	k1gMnSc1
Purley	Purlea	k1gFnSc2
•	•	k?
John	John	k1gMnSc1
Watson	Watson	k1gMnSc1
•	•	k?
Roger	Roger	k1gMnSc1
Williamson	Williamson	k1gMnSc1
•	•	k?
Ian	Ian	k1gMnSc1
Ashley	Ashlea	k1gFnSc2
•	•	k?
Guy	Guy	k1gFnSc2
Edwards	Edwardsa	k1gFnPc2
•	•	k?
Tom	Tom	k1gMnSc1
Pryce	Pryce	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Robarts	Robarts	k1gInSc1
•	•	k?
Andy	Anda	k1gFnSc2
Sutcliffe	Sutcliff	k1gInSc5
•	•	k?
Mike	Mike	k1gNnSc2
Wilds	Wildsa	k1gFnPc2
•	•	k?
Tony	Tony	k1gMnSc1
Brise	Brise	k1gFnSc2
•	•	k?
Jim	on	k3xPp3gFnPc3
Crawford	Crawford	k1gInSc1
•	•	k?
Bob	Bob	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Evans	Evans	k1gInSc1
•	•	k?
Brian	Brian	k1gInSc1
Henton	Henton	k1gInSc1
•	•	k?
Damien	Damien	k2eAgInSc1d1
Magee	Magee	k1gInSc1
•	•	k?
Dave	Dav	k1gInSc5
Morgan	morgan	k1gMnSc1
•	•	k?
Divina	divin	k2eAgInSc2d1
Galica	Galic	k1gInSc2
•	•	k?
Rupert	Rupert	k1gMnSc1
Keegan	Keegan	k1gMnSc1
•	•	k?
Geoff	Geoff	k1gInSc1
Lees	Lees	k1gInSc1
•	•	k?
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gMnSc1
•	•	k?
Tiff	Tiff	k1gMnSc1
Needell	Needell	k1gMnSc1
•	•	k?
Stephen	Stephen	k2eAgMnSc1d1
South	South	k1gMnSc1
•	•	k?
Derek	Derek	k1gMnSc1
Warwick	Warwick	k1gMnSc1
•	•	k?
Kenny	Kenna	k1gFnSc2
Acheson	Acheson	k1gMnSc1
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Palmer	Palmer	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Brundle	Brundle	k1gMnSc1
•	•	k?
Johnny	Johnna	k1gFnSc2
Dumfries	Dumfries	k1gMnSc1
•	•	k?
Julian	Julian	k1gMnSc1
Bailey	Bailea	k1gFnSc2
•	•	k?
Martin	Martin	k1gMnSc1
Donnelly	Donnella	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Johnny	Johnna	k1gFnPc1
Herbert	Herbert	k1gMnSc1
•	•	k?
Mark	Mark	k1gMnSc1
Blundell	Blundell	k1gMnSc1
•	•	k?
Damon	Damon	k1gMnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Perry	Perra	k1gFnSc2
McCarthy	McCartha	k1gFnSc2
•	•	k?
Eddie	Eddius	k1gMnSc5
Irvine	Irvin	k1gMnSc5
•	•	k?
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc1
•	•	k?
Anthony	Anthona	k1gFnSc2
Davidson	Davidson	k1gMnSc1
•	•	k?
Allan	Allan	k1gMnSc1
McNish	McNish	k1gMnSc1
•	•	k?
Ralph	Ralph	k1gMnSc1
Firman	Firman	k1gMnSc1
•	•	k?
Justin	Justin	k1gMnSc1
Wilson	Wilson	k1gMnSc1
•	•	k?
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
•	•	k?
Paul	Paul	k1gMnSc1
di	di	k?
Resta	Resta	k1gMnSc1
•	•	k?
Max	max	kA
Chilton	Chilton	k1gInSc1
•	•	k?
Will	Will	k1gInSc1
Stevens	Stevens	k1gInSc1
•	•	k?
Jolyon	Jolyon	k1gInSc1
Palmer	Palmer	k1gMnSc1
•	•	k?
George	George	k1gInSc1
Russell	Russell	k1gMnSc1
•	•	k?
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnPc6
•	•	k?
Jack	Jack	k1gInSc1
Aitken	Aitken	k1gInSc1
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
–	–	k?
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Formule	formule	k1gFnSc1
1	#num#	k4
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
