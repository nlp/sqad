<s>
Příkladem	příklad	k1gInSc7	příklad
genocidy	genocida	k1gFnSc2	genocida
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nacistické	nacistický	k2eAgFnSc2d1	nacistická
rasové	rasový	k2eAgFnSc2d1	rasová
ideologie	ideologie	k1gFnSc2	ideologie
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
šest	šest	k4xCc1	šest
milionů	milion	k4xCgInPc2	milion
evropských	evropský	k2eAgMnPc2d1	evropský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
