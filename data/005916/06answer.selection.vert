<s>
Dračí	dračí	k2eAgNnSc1d1	dračí
doupě	doupě	k1gNnSc1	doupě
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
fantasy	fantas	k1gInPc4	fantas
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Altar	Altar	k1gInSc1	Altar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
přizpůsobilo	přizpůsobit	k5eAaPmAgNnS	přizpůsobit
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgMnPc2d1	vlastní
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
