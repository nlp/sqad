<s>
Dračí	dračí	k2eAgNnSc1d1	dračí
doupě	doupě	k1gNnSc1	doupě
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
fantasy	fantas	k1gInPc4	fantas
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Altar	Altar	k1gInSc1	Altar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
přizpůsobilo	přizpůsobit	k5eAaPmAgNnS	přizpůsobit
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgMnPc2d1	vlastní
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
z	z	k7c2	z
části	část	k1gFnSc2	část
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
Dungeons	Dungeonsa	k1gFnPc2	Dungeonsa
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
(	(	kIx(	(
<g/>
členění	členění	k1gNnSc1	členění
příruček	příručka	k1gFnPc2	příručka
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc1	některý
položky	položka	k1gFnPc1	položka
v	v	k7c6	v
bestiáři	bestiář	k1gInSc6	bestiář
<g/>
;	;	kIx,	;
rasy	rasa	k1gFnSc2	rasa
až	až	k9	až
na	na	k7c4	na
krolla	kroll	k1gMnSc4	kroll
a	a	k8xC	a
kudůka	kudůek	k1gMnSc4	kudůek
<g/>
;	;	kIx,	;
povolání	povolání	k1gNnSc2	povolání
–	–	k?	–
chybí	chybět	k5eAaImIp3nS	chybět
kněz	kněz	k1gMnSc1	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyšla	vyjít	k5eAaPmAgFnS	vyjít
nová	nový	k2eAgFnSc1d1	nová
hra	hra	k1gFnSc1	hra
Dračí	dračí	k2eAgFnSc1d1	dračí
doupě	doupě	k1gNnSc4	doupě
Plus	plus	k1gInSc1	plus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Pluska	Plusek	k1gMnSc2	Plusek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
komplikovaným	komplikovaný	k2eAgInSc7d1	komplikovaný
jednotným	jednotný	k2eAgInSc7d1	jednotný
systémem	systém	k1gInSc7	systém
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
převodových	převodový	k2eAgFnPc6d1	převodová
logaritmických	logaritmický	k2eAgFnPc6d1	logaritmická
tabulkách	tabulka	k1gFnPc6	tabulka
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
spíše	spíše	k9	spíše
pokročilejším	pokročilý	k2eAgMnPc3d2	pokročilejší
hráčům	hráč	k1gMnPc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgFnSc3d1	nová
Dračí	dračí	k2eAgFnSc3d1	dračí
doupě	doupa	k1gFnSc3	doupa
II	II	kA	II
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
nahradit	nahradit	k5eAaPmF	nahradit
staré	starý	k2eAgNnSc4d1	staré
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
jednodušším	jednoduchý	k2eAgMnSc7d2	jednodušší
a	a	k8xC	a
ucelenějším	ucelený	k2eAgInSc7d2	ucelenější
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
snadným	snadný	k2eAgNnSc7d1	snadné
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
herní	herní	k2eAgInPc1d1	herní
systémy	systém	k1gInPc1	systém
nemají	mít	k5eNaImIp3nP	mít
přímou	přímý	k2eAgFnSc4d1	přímá
návaznost	návaznost	k1gFnSc4	návaznost
na	na	k7c4	na
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
1.0	[number]	k4	1.0
–	–	k?	–
1.6	[number]	k4	1.6
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
rovině	rovina	k1gFnSc6	rovina
sdílí	sdílet	k5eAaImIp3nS	sdílet
zaměření	zaměření	k1gNnSc1	zaměření
na	na	k7c4	na
fantasy	fantas	k1gInPc4	fantas
(	(	kIx(	(
<g/>
meč	meč	k1gInSc1	meč
<g/>
&	&	k?	&
<g/>
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
hobiti	hobit	k1gMnPc1	hobit
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgNnSc1d1	dračí
doupě	doupě	k1gNnSc1	doupě
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
klasické	klasický	k2eAgFnPc4d1	klasická
papírové	papírový	k2eAgFnPc4d1	papírová
(	(	kIx(	(
<g/>
nepočítačové	počítačový	k2eNgFnPc4d1	nepočítačová
<g/>
)	)	kIx)	)
hry	hra	k1gFnPc4	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
PJ	PJ	kA	PJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
před	před	k7c7	před
hrou	hra	k1gFnSc7	hra
připraví	připravit	k5eAaPmIp3nS	připravit
úvodní	úvodní	k2eAgInSc4d1	úvodní
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc1	mapa
apod.	apod.	kA	apod.
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
tohoto	tento	k3xDgInSc2	tento
příběhu	příběh	k1gInSc2	příběh
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
postavu	postava	k1gFnSc4	postava
fantasy	fantas	k1gInPc1	fantas
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
trpasličího	trpasličí	k2eAgMnSc2d1	trpasličí
bojovníka	bojovník	k1gMnSc2	bojovník
<g/>
,	,	kIx,	,
elfího	elfí	k2eAgMnSc2d1	elfí
kouzelníka	kouzelník	k1gMnSc2	kouzelník
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
družina	družina	k1gFnSc1	družina
pak	pak	k6eAd1	pak
dohromady	dohromady	k6eAd1	dohromady
putuje	putovat	k5eAaImIp3nS	putovat
světem	svět	k1gInSc7	svět
vytvořeným	vytvořený	k2eAgInSc7d1	vytvořený
PJ	PJ	kA	PJ
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
splnit	splnit	k5eAaPmF	splnit
přijaté	přijatý	k2eAgInPc4d1	přijatý
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
neexistují	existovat	k5eNaImIp3nP	existovat
limity	limit	k1gInPc1	limit
<g/>
,	,	kIx,	,
co	co	k9	co
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
<g/>
,	,	kIx,	,
o	o	k7c6	o
následcích	následek	k1gInPc6	následek
jejich	jejich	k3xOp3gMnPc2	jejich
činů	čin	k1gInPc2	čin
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
PJ	PJ	kA	PJ
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
běžné	běžný	k2eAgFnPc1d1	běžná
herní	herní	k2eAgFnPc1d1	herní
situace	situace	k1gFnPc1	situace
(	(	kIx(	(
<g/>
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
sesílání	sesílání	k1gNnSc1	sesílání
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
využívání	využívání	k1gNnSc1	využívání
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
schopností	schopnost	k1gFnPc2	schopnost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
číselných	číselný	k2eAgFnPc2d1	číselná
tabulek	tabulka	k1gFnPc2	tabulka
uvedených	uvedený	k2eAgFnPc2d1	uvedená
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
a	a	k8xC	a
prvku	prvek	k1gInSc2	prvek
náhody	náhoda	k1gFnSc2	náhoda
–	–	k?	–
hodů	hod	k1gInPc2	hod
kostkami	kostka	k1gFnPc7	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
kostky	kostka	k1gFnPc1	kostka
klasické	klasický	k2eAgInPc4d1	klasický
šestistěnné	šestistěnný	k2eAgInPc4d1	šestistěnný
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
netradičnější	tradiční	k2eNgFnSc1d2	netradičnější
desetistěnné	desetistěnný	k2eAgMnPc4d1	desetistěnný
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
osmi-	osmi-	k?	osmi-
či	či	k8xC	či
čtyřstěnné	čtyřstěnný	k2eAgFnPc1d1	čtyřstěnná
<g/>
.	.	kIx.	.
</s>
<s>
Procentní	procentní	k2eAgInSc1d1	procentní
hod	hod	k1gInSc1	hod
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
buď	buď	k8xC	buď
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
hodem	hod	k1gInSc7	hod
desetistěnnou	desetistěnný	k2eAgFnSc7d1	desetistěnná
kostkou	kostka	k1gFnSc7	kostka
<g/>
,	,	kIx,	,
či	či	k8xC	či
použitím	použití	k1gNnSc7	použití
kostek	kostka	k1gFnPc2	kostka
dvou	dva	k4xCgMnPc6	dva
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
existuje	existovat	k5eAaImIp3nS	existovat
speciální	speciální	k2eAgFnSc1d1	speciální
procentuální	procentuální	k2eAgFnSc1d1	procentuální
kostka	kostka	k1gFnSc1	kostka
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
čtverečkovaném	čtverečkovaný	k2eAgMnSc6d1	čtverečkovaný
<g/>
,	,	kIx,	,
hexovém	hexový	k2eAgMnSc6d1	hexový
nebo	nebo	k8xC	nebo
prázdném	prázdný	k2eAgInSc6d1	prázdný
papíře	papír	k1gInSc6	papír
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Průvodní	průvodní	k2eAgFnSc7d1	průvodní
vlastností	vlastnost	k1gFnSc7	vlastnost
herního	herní	k2eAgInSc2d1	herní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
zvýhodňování	zvýhodňování	k1gNnSc4	zvýhodňování
maximalizované	maximalizovaný	k2eAgFnSc2d1	maximalizovaná
postavy	postava	k1gFnSc2	postava
(	(	kIx(	(
<g/>
min-max	minax	k1gInSc1	min-max
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
/	/	kIx~	/
<g/>
kroll-válečník	krollálečník	k1gMnSc1	kroll-válečník
<g/>
,	,	kIx,	,
hobit-zloděj	hobitlodět	k5eAaImRp2nS	hobit-zlodět
<g/>
,	,	kIx,	,
elf-kouzelník	elfouzelník	k1gInSc1	elf-kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Podrobná	podrobný	k2eAgNnPc1d1	podrobné
pravidla	pravidlo	k1gNnPc1	pravidlo
Dračího	dračí	k2eAgInSc2d1	dračí
Doupě	doupě	k1gNnSc4	doupě
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
a	a	k8xC	a
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
tří	tři	k4xCgFnPc2	tři
knihách	kniha	k1gFnPc6	kniha
pro	pro	k7c4	pro
Pána	pán	k1gMnSc4	pán
Jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
knihách	kniha	k1gFnPc6	kniha
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
mají	mít	k5eAaImIp3nP	mít
dohromady	dohromady	k6eAd1	dohromady
kolem	kolem	k7c2	kolem
550	[number]	k4	550
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Jeskyně	jeskyně	k1gFnSc2	jeskyně
je	být	k5eAaImIp3nS	být
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
hází	házet	k5eAaImIp3nS	házet
kostkou	kostka	k1gFnSc7	kostka
za	za	k7c4	za
protivníky	protivník	k1gMnPc4	protivník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
postavy	postav	k1gInPc7	postav
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
proběhnutí	proběhnutí	k1gNnSc2	proběhnutí
náhodné	náhodný	k2eAgFnSc2d1	náhodná
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hře	hra	k1gFnSc6	hra
zvané	zvaný	k2eAgFnSc2d1	zvaná
Dungeons	Dungeons	k1gInSc4	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
se	se	k3xPyFc4	se
obdobná	obdobný	k2eAgFnSc1d1	obdobná
role	role	k1gFnSc1	role
nazývá	nazývat	k5eAaImIp3nS	nazývat
Dungeon	Dungeon	k1gMnSc1	Dungeon
Master	master	k1gMnSc1	master
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
hrách	hra	k1gFnPc6	hra
často	často	k6eAd1	často
Pán	pán	k1gMnSc1	pán
Hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Game	game	k1gInSc1	game
Master	master	k1gMnSc1	master
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgNnSc1d1	dračí
doupě	doupě	k1gNnSc1	doupě
bývá	bývat	k5eAaImIp3nS	bývat
ale	ale	k9	ale
často	často	k6eAd1	často
zaměňováno	zaměňován	k2eAgNnSc1d1	zaměňováno
s	s	k7c7	s
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
papírových	papírový	k2eAgInPc2d1	papírový
stolních	stolní	k2eAgInPc2d1	stolní
RPG	RPG	kA	RPG
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgInSc4d1	jiný
herní	herní	k2eAgInSc4d1	herní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
tvor	tvor	k1gMnSc1	tvor
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
postava	postava	k1gFnSc1	postava
má	mít	k5eAaImIp3nS	mít
tyto	tento	k3xDgFnPc4	tento
základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Síla	síla	k1gFnSc1	síla
–	–	k?	–
v	v	k7c6	v
boji	boj	k1gInSc6	boj
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
zbraně	zbraň	k1gFnSc2	zbraň
určuje	určovat	k5eAaImIp3nS	určovat
sílu	síla	k1gFnSc4	síla
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
blízko	blízko	k1gNnSc4	blízko
(	(	kIx(	(
<g/>
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
)	)	kIx)	)
a	a	k8xC	a
tedy	tedy	k9	tedy
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
zásah	zásah	k1gInSc4	zásah
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
zranění	zranění	k1gNnSc2	zranění
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
zásahu	zásah	k1gInSc2	zásah
<g/>
;	;	kIx,	;
mimo	mimo	k7c4	mimo
boj	boj	k1gInSc4	boj
pak	pak	k6eAd1	pak
šanci	šance	k1gFnSc4	šance
odolat	odolat	k5eAaPmF	odolat
různým	různý	k2eAgFnPc3d1	různá
výzvám	výzva	k1gFnPc3	výzva
typu	typ	k1gInSc2	typ
vyražení	vyražení	k1gNnSc4	vyražení
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
odvalení	odvalení	k1gNnSc4	odvalení
balvanu	balvan	k1gInSc2	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Obratnost	obratnost	k1gFnSc1	obratnost
–	–	k?	–
v	v	k7c6	v
boji	boj	k1gInSc6	boj
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
typem	typ	k1gInSc7	typ
zbroje	zbroj	k1gFnSc2	zbroj
určuje	určovat	k5eAaImIp3nS	určovat
výši	výše	k1gFnSc4	výše
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
tedy	tedy	k9	tedy
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
obránce	obránce	k1gMnSc1	obránce
útoku	útok	k1gInSc2	útok
vyhne	vyhnout	k5eAaPmIp3nS	vyhnout
či	či	k8xC	či
jak	jak	k6eAd1	jak
velké	velký	k2eAgNnSc4d1	velké
zranění	zranění	k1gNnSc4	zranění
obdrží	obdržet	k5eAaPmIp3nS	obdržet
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
v	v	k7c6	v
boji	boj	k1gInSc6	boj
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
střelné	střelný	k2eAgFnSc2d1	střelná
či	či	k8xC	či
vrhací	vrhací	k2eAgFnSc2d1	vrhací
zbraně	zbraň	k1gFnSc2	zbraň
určuje	určovat	k5eAaImIp3nS	určovat
sílu	síla	k1gFnSc4	síla
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
(	(	kIx(	(
<g/>
střelecký	střelecký	k2eAgInSc1d1	střelecký
souboj	souboj	k1gInSc1	souboj
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
mimo	mimo	k7c4	mimo
boj	boj	k1gInSc4	boj
pak	pak	k6eAd1	pak
šanci	šance	k1gFnSc4	šance
odolat	odolat	k5eAaPmF	odolat
různým	různý	k2eAgFnPc3d1	různá
výzvám	výzva	k1gFnPc3	výzva
typu	typ	k1gInSc2	typ
uhnutí	uhnutí	k1gNnSc2	uhnutí
letící	letící	k2eAgFnSc6d1	letící
šipce	šipka	k1gFnSc6	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
zloděj	zloděj	k1gMnSc1	zloděj
při	při	k7c6	při
většině	většina	k1gFnSc6	většina
nebojových	bojový	k2eNgFnPc2d1	nebojová
činností	činnost	k1gFnPc2	činnost
(	(	kIx(	(
<g/>
páčení	páčení	k1gNnSc1	páčení
zámků	zámek	k1gInPc2	zámek
<g/>
,	,	kIx,	,
šplh	šplh	k1gInSc1	šplh
<g/>
,	,	kIx,	,
vybírání	vybírání	k1gNnSc1	vybírání
kapes	kapsa	k1gFnPc2	kapsa
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
alchymisty	alchymista	k1gMnSc2	alchymista
ovlivnuje	ovlivnovat	k5eAaPmIp3nS	ovlivnovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
množství	množství	k1gNnSc1	množství
životů	život	k1gInPc2	život
(	(	kIx(	(
<g/>
zásahových	zásahový	k2eAgInPc2d1	zásahový
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
tvor	tvor	k1gMnSc1	tvor
(	(	kIx(	(
<g/>
postava	postava	k1gFnSc1	postava
<g/>
)	)	kIx)	)
zemře	zemřít	k5eAaPmIp3nS	zemřít
a	a	k8xC	a
míru	míra	k1gFnSc4	míra
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
než	než	k8xS	než
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
bezevědomí	bezevědomí	k1gNnSc2	bezevědomí
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
šanci	šance	k1gFnSc4	šance
odolat	odolat	k5eAaPmF	odolat
různým	různý	k2eAgFnPc3d1	různá
výzvám	výzva	k1gFnPc3	výzva
typu	typ	k1gInSc2	typ
otrava	otrava	k1gFnSc1	otrava
jedem	jed	k1gInSc7	jed
(	(	kIx(	(
<g/>
rychle	rychle	k6eAd1	rychle
účinkující	účinkující	k2eAgInPc1d1	účinkující
jedy	jed	k1gInPc1	jed
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
boje	boj	k1gInSc2	boj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
nárazy	náraz	k1gInPc4	náraz
a	a	k8xC	a
hladovění	hladovění	k1gNnPc4	hladovění
<g/>
.	.	kIx.	.
</s>
<s>
Inteligence	inteligence	k1gFnSc1	inteligence
–	–	k?	–
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výši	výše	k1gFnSc4	výše
magické	magický	k2eAgFnSc2d1	magická
energie	energie	k1gFnSc2	energie
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
hádanek	hádanka	k1gFnPc2	hádanka
a	a	k8xC	a
rébusů	rébus	k1gInPc2	rébus
<g/>
;	;	kIx,	;
u	u	k7c2	u
kouzelníka	kouzelník	k1gMnSc2	kouzelník
ovlivnuje	ovlivnovat	k5eAaPmIp3nS	ovlivnovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
kouzlení	kouzlení	k1gNnSc4	kouzlení
<g/>
.	.	kIx.	.
</s>
<s>
Charisma	charisma	k1gNnSc1	charisma
–	–	k?	–
směs	směs	k1gFnSc4	směs
osobní	osobní	k2eAgFnSc2d1	osobní
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
vůle	vůle	k1gFnSc2	vůle
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
při	při	k7c6	při
smlouvání	smlouvání	k1gNnSc6	smlouvání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
zohledňování	zohledňování	k1gNnSc6	zohledňování
sympatií	sympatie	k1gFnPc2	sympatie
a	a	k8xC	a
antipatií	antipatie	k1gFnPc2	antipatie
<g/>
,	,	kIx,	,
odolávání	odolávání	k1gNnSc4	odolávání
některým	některý	k3yIgMnPc3	některý
druhům	druh	k1gMnPc3	druh
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
zloděj	zloděj	k1gMnSc1	zloděj
v	v	k7c6	v
části	část	k1gFnSc6	část
nebojových	bojový	k2eNgFnPc2d1	nebojová
činností	činnost	k1gFnPc2	činnost
(	(	kIx(	(
<g/>
převleky	převlek	k1gInPc1	převlek
<g/>
,	,	kIx,	,
získání	získání	k1gNnSc1	získání
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
postavy	postava	k1gFnSc2	postava
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
hráč	hráč	k1gMnSc1	hráč
vybere	vybrat	k5eAaPmIp3nS	vybrat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
ras	rasa	k1gFnPc2	rasa
<g/>
:	:	kIx,	:
Hobit	hobit	k1gMnSc1	hobit
–	–	k?	–
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
J.	J.	kA	J.
<g/>
R.	R.	kA	R.
<g/>
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
chlupaté	chlupatý	k2eAgFnPc4d1	chlupatá
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nP	chodit
bez	bez	k7c2	bez
bot	bota	k1gFnPc2	bota
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
má	mít	k5eAaImIp3nS	mít
kudrnaté	kudrnatý	k2eAgInPc4d1	kudrnatý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
kulaté	kulatý	k2eAgNnSc4d1	kulaté
bříško	bříško	k1gNnSc4	bříško
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
rasovou	rasový	k2eAgFnSc7d1	rasová
schopností	schopnost	k1gFnSc7	schopnost
je	být	k5eAaImIp3nS	být
čich	čich	k1gInSc1	čich
–	–	k?	–
schopnost	schopnost	k1gFnSc4	schopnost
vycítit	vycítit	k5eAaPmF	vycítit
blízkost	blízkost	k1gFnSc4	blízkost
jiných	jiný	k2eAgMnPc2d1	jiný
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Vynikají	vynikat	k5eAaImIp3nP	vynikat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
obratností	obratnost	k1gFnSc7	obratnost
a	a	k8xC	a
charisma	charisma	k1gNnSc1	charisma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
dělá	dělat	k5eAaImIp3nS	dělat
vynikající	vynikající	k2eAgMnPc4d1	vynikající
zloděje	zloděj	k1gMnPc4	zloděj
<g/>
.	.	kIx.	.
</s>
<s>
Rodovou	rodový	k2eAgFnSc7d1	rodová
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
lehká	lehký	k2eAgFnSc1d1	lehká
kuše	kuše	k1gFnSc1	kuše
(	(	kIx(	(
<g/>
bonus	bonus	k1gInSc1	bonus
na	na	k7c4	na
útok	útok	k1gInSc4	útok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kudůk	Kudůk	k1gInSc1	Kudůk
–	–	k?	–
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
humanoidní	humanoidní	k2eAgInSc1d1	humanoidní
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
kudůk	kudůk	k1gInSc1	kudůk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
psáno	psán	k2eAgNnSc1d1	psáno
správněji	správně	k6eAd2	správně
"	"	kIx"	"
<g/>
kudúk	kudúk	k6eAd1	kudúk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
též	též	k9	též
vypůjčeno	vypůjčit	k5eAaPmNgNnS	vypůjčit
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
J.	J.	kA	J.
<g/>
R.	R.	kA	R.
<g/>
R.	R.	kA	R.
<g/>
Tolkiena	Tolkieno	k1gNnPc4	Tolkieno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vlastní	vlastní	k2eAgFnSc6d1	vlastní
řeči	řeč	k1gFnSc6	řeč
nazývají	nazývat	k5eAaImIp3nP	nazývat
hobiti	hobit	k1gMnPc1	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Kudůkové	Kudůková	k1gFnPc1	Kudůková
v	v	k7c6	v
D	D	kA	D
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
splynutím	splynutí	k1gNnSc7	splynutí
části	část	k1gFnSc2	část
plemene	plemeno	k1gNnSc2	plemeno
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
hobitů	hobit	k1gMnPc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
nízkým	nízký	k2eAgInSc7d1	nízký
vzrůstem	vzrůst	k1gInSc7	vzrůst
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
hobiti	hobit	k1gMnPc1	hobit
<g/>
,	,	kIx,	,
menší	malý	k2eAgMnPc1d2	menší
než	než	k8xS	než
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šikovností	šikovnost	k1gFnSc7	šikovnost
a	a	k8xC	a
zručností	zručnost	k1gFnSc7	zručnost
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
jako	jako	k9	jako
alchymisté	alchymista	k1gMnPc1	alchymista
nebo	nebo	k8xC	nebo
zloději	zloděj	k1gMnPc1	zloděj
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
rodovou	rodový	k2eAgFnSc7d1	rodová
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
sekera	sekera	k1gFnSc1	sekera
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslík	trpaslík	k1gMnSc1	trpaslík
–	–	k?	–
trpaslíci	trpaslík	k1gMnPc1	trpaslík
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
nadaní	nadaný	k2eAgMnPc1d1	nadaný
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
schopností	schopnost	k1gFnSc7	schopnost
infravidění	infravidění	k1gNnSc2	infravidění
–	–	k?	–
schopnosti	schopnost	k1gFnSc2	schopnost
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
vidět	vidět	k5eAaImF	vidět
teplo	teplo	k6eAd1	teplo
(	(	kIx(	(
<g/>
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
teplokrevné	teplokrevný	k2eAgMnPc4d1	teplokrevný
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
studená	studený	k2eAgNnPc1d1	studené
jezírka	jezírko	k1gNnPc1	jezírko
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Silní	silnit	k5eAaImIp3nS	silnit
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
odolní	odolný	k2eAgMnPc1d1	odolný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
herně	herna	k1gFnSc3	herna
<g/>
)	)	kIx)	)
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
býti	být	k5eAaImF	být
válečníky	válečník	k1gMnPc4	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Elf	elf	k1gMnSc1	elf
–	–	k?	–
elfové	elf	k1gMnPc1	elf
jsou	být	k5eAaImIp3nP	být
dlouhověcí	dlouhověký	k2eAgMnPc1d1	dlouhověký
(	(	kIx(	(
<g/>
herně	herna	k1gFnSc6	herna
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
vynikají	vynikat	k5eAaImIp3nP	vynikat
zejména	zejména	k9	zejména
svou	svůj	k3xOyFgFnSc7	svůj
inteligencí	inteligence	k1gFnSc7	inteligence
a	a	k8xC	a
charismou	charisma	k1gFnSc7	charisma
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	on	k3xPp3gMnPc4	on
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
pro	pro	k7c4	pro
povolání	povolání	k1gNnSc4	povolání
Kouzelníka	kouzelník	k1gMnSc2	kouzelník
či	či	k8xC	či
Hraničáře	hraničář	k1gMnSc2	hraničář
<g/>
.	.	kIx.	.
</s>
<s>
Rodovou	rodový	k2eAgFnSc7d1	rodová
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
luk	luk	k1gInSc4	luk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
–	–	k?	–
klasický	klasický	k2eAgMnSc1d1	klasický
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
průměrné	průměrný	k2eAgFnPc4d1	průměrná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgFnPc4	žádný
speciální	speciální	k2eAgFnPc4d1	speciální
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
rodovou	rodový	k2eAgFnSc7d1	rodová
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
meč	meč	k1gInSc1	meč
<g/>
.	.	kIx.	.
</s>
<s>
Barbar	Barbara	k1gFnPc2	Barbara
–	–	k?	–
lidé	člověk	k1gMnPc1	člověk
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
pustinách	pustina	k1gFnPc6	pustina
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Odloučení	odloučení	k1gNnSc1	odloučení
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
stopy	stopa	k1gFnSc2	stopa
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgFnPc1d2	silnější
a	a	k8xC	a
odolnější	odolný	k2eAgFnPc1d2	odolnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
neohrabanější	neohrabaný	k2eAgMnPc1d2	neohrabanější
a	a	k8xC	a
ošklivější	ošklivý	k2eAgMnPc1d2	ošklivější
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
nejběžnějšími	běžný	k2eAgInPc7d3	nejběžnější
povoláními	povolání	k1gNnPc7	povolání
jsou	být	k5eAaImIp3nP	být
válečník	válečník	k1gMnSc1	válečník
a	a	k8xC	a
hraničář	hraničář	k1gMnSc1	hraničář
<g/>
.	.	kIx.	.
</s>
<s>
Rodovou	rodový	k2eAgFnSc7d1	rodová
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc4	meč
bastard	bastard	k1gMnSc1	bastard
<g/>
.	.	kIx.	.
</s>
<s>
Kroll	Krollum	k1gNnPc2	Krollum
–	–	k?	–
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
vysokým	vysoký	k2eAgInSc7d1	vysoký
vzrůstem	vzrůst	k1gInSc7	vzrůst
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
2	[number]	k4	2
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divokou	divoký	k2eAgFnSc4d1	divoká
a	a	k8xC	a
bojovnou	bojovný	k2eAgFnSc4d1	bojovná
povahou	povaha	k1gFnSc7	povaha
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
drsnou	drsný	k2eAgFnSc4d1	drsná
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
našedlou	našedlý	k2eAgFnSc4d1	našedlá
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
připomínají	připomínat	k5eAaImIp3nP	připomínat
pračlověka	pračlověk	k1gMnSc4	pračlověk
<g/>
.	.	kIx.	.
</s>
<s>
Nápadné	nápadný	k2eAgFnPc1d1	nápadná
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc1	jejich
veliké	veliký	k2eAgFnPc1d1	veliká
uši	ucho	k1gNnPc1	ucho
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
ultrazvukové	ultrazvukový	k2eAgFnSc6d1	ultrazvuková
echolokaci	echolokace	k1gFnSc6	echolokace
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zbraní	zbraň	k1gFnSc7	zbraň
jsou	být	k5eAaImIp3nP	být
palcáty	palcát	k1gInPc1	palcát
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
kyje	kyj	k1gInPc1	kyj
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
krollů	kroll	k1gInPc2	kroll
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
ras	rasa	k1gFnPc2	rasa
nepanuje	panovat	k5eNaImIp3nS	panovat
žádná	žádný	k3yNgFnSc1	žádný
obecná	obecný	k2eAgFnSc1d1	obecná
intuitivní	intuitivní	k2eAgFnSc1d1	intuitivní
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
způsobu	způsob	k1gInSc6	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
atd.	atd.	kA	atd.
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
proto	proto	k8xC	proto
herně	herna	k1gFnSc3	herna
plytcí	plytký	k2eAgMnPc1d1	plytký
a	a	k8xC	a
nezajímaví	zajímavý	k2eNgMnPc1d1	nezajímavý
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
vynahradit	vynahradit	k5eAaPmF	vynahradit
jejich	jejich	k3xOp3gFnSc1	jejich
nízká	nízký	k2eAgFnSc1d1	nízká
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svádí	svádět	k5eAaImIp3nS	svádět
k	k	k7c3	k
nejrůznějším	různý	k2eAgInPc3d3	nejrůznější
vtípkům	vtípek	k1gInPc3	vtípek
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
Páni	pan	k1gMnPc1	pan
jeskyně	jeskyně	k1gFnSc2	jeskyně
například	například	k6eAd1	například
určují	určovat	k5eAaImIp3nP	určovat
počet	počet	k1gInSc4	počet
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kroll	kroll	k1gInSc1	kroll
zná	znát	k5eAaImIp3nS	znát
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Na	na	k7c4	na
inspirativní	inspirativní	k2eAgNnSc4d1	inspirativní
zpracování	zpracování	k1gNnSc4	zpracování
krollů	kroll	k1gInPc2	kroll
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
narazit	narazit	k5eAaPmF	narazit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
serverech	server	k1gInPc6	server
zabývajících	zabývající	k2eAgInPc6d1	zabývající
se	s	k7c7	s
hrou	hra	k1gFnSc7	hra
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
také	také	k6eAd1	také
může	moct	k5eAaImIp3nS	moct
vymyslet	vymyslet	k5eAaPmF	vymyslet
Pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
rasy	rasa	k1gFnSc2	rasa
zvolí	zvolit	k5eAaPmIp3nS	zvolit
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
povolání	povolání	k1gNnPc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
povolání	povolání	k1gNnSc2	povolání
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
vlastnosti	vlastnost	k1gFnPc1	vlastnost
určují	určovat	k5eAaImIp3nP	určovat
podle	podle	k7c2	podle
tabulky	tabulka	k1gFnSc2	tabulka
povolání	povolání	k1gNnSc2	povolání
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
podle	podle	k7c2	podle
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
bonus	bonus	k1gInSc1	bonus
či	či	k8xC	či
postih	postih	k1gInSc1	postih
k	k	k7c3	k
takto	takto	k6eAd1	takto
určené	určený	k2eAgFnSc3d1	určená
vlastnosti	vlastnost	k1gFnSc3	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
trpaslík	trpaslík	k1gMnSc1	trpaslík
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
trpaslíka-zloděje	trpaslíkalodět	k5eAaPmIp3nS	trpaslíka-zlodět
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
obratnosti	obratnost	k1gFnSc2	obratnost
dle	dle	k7c2	dle
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
ponížené	ponížený	k2eAgFnSc2d1	ponížená
o	o	k7c4	o
drobný	drobný	k2eAgInSc4d1	drobný
postih	postih	k1gInSc4	postih
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
odlišil	odlišit	k5eAaPmAgInS	odlišit
např.	např.	kA	např.
od	od	k7c2	od
hobita-zloděje	hobitalodět	k5eAaPmIp3nS	hobita-zlodět
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
naopak	naopak	k6eAd1	naopak
obratnosti	obratnost	k1gFnPc4	obratnost
přičte	přičíst	k5eAaPmIp3nS	přičíst
bonus	bonus	k1gInSc1	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Maxima	Maxima	k1gFnSc1	Maxima
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pochopitelně	pochopitelně	k6eAd1	pochopitelně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
povolání	povolání	k1gNnSc1	povolání
se	se	k3xPyFc4	se
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
6	[number]	k4	6
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
specializace	specializace	k1gFnPc4	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Válečník	válečník	k1gMnSc1	válečník
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
boj	boj	k1gInSc4	boj
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
(	(	kIx(	(
<g/>
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
zbroje	zbroj	k1gFnPc1	zbroj
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
odhadovat	odhadovat	k5eAaImF	odhadovat
sílu	síla	k1gFnSc4	síla
protivníků	protivník	k1gMnPc2	protivník
<g/>
,	,	kIx,	,
léčit	léčit	k5eAaImF	léčit
svá	svůj	k3xOyFgNnPc4	svůj
zranění	zranění	k1gNnPc4	zranění
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pokusit	pokusit	k5eAaPmF	pokusit
zastrašit	zastrašit	k5eAaPmF	zastrašit
a	a	k8xC	a
zahnat	zahnat	k5eAaPmF	zahnat
nestvůry	nestvůra	k1gFnPc4	nestvůra
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Bojovník	bojovník	k1gMnSc1	bojovník
je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
v	v	k7c6	v
boji	boj	k1gInSc6	boj
velkými	velký	k2eAgFnPc7d1	velká
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
hrubou	hrubý	k2eAgFnSc7d1	hrubá
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
proměnit	proměnit	k5eAaPmF	proměnit
na	na	k7c4	na
berserkra	berserkr	k1gMnSc4	berserkr
<g/>
,	,	kIx,	,
nezadržitelné	zadržitelný	k2eNgNnSc4d1	nezadržitelné
bojující	bojující	k2eAgNnSc4d1	bojující
monstrum	monstrum	k1gNnSc4	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Šermíř	šermíř	k1gMnSc1	šermíř
je	být	k5eAaImIp3nS	být
mistr	mistr	k1gMnSc1	mistr
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
humanoidy	humanoid	k1gInPc7	humanoid
(	(	kIx(	(
<g/>
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
skřety	skřet	k1gMnPc7	skřet
<g/>
,	,	kIx,	,
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
několik	několik	k4yIc1	několik
velmi	velmi	k6eAd1	velmi
užitečných	užitečný	k2eAgFnPc2d1	užitečná
fint	finta	k1gFnPc2	finta
zvyšujících	zvyšující	k2eAgFnPc2d1	zvyšující
jeho	jeho	k3xOp3gNnSc1	jeho
šance	šance	k1gFnPc1	šance
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Hraničář	hraničář	k1gMnSc1	hraničář
zná	znát	k5eAaImIp3nS	znát
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ji	on	k3xPp3gFnSc4	on
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopným	schopný	k2eAgMnSc7d1	schopný
stopařem	stopař	k1gMnSc7	stopař
<g/>
.	.	kIx.	.
</s>
<s>
Hraničáři	hraničár	k1gMnPc1	hraničár
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
provázeni	provázen	k2eAgMnPc1d1	provázen
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Hraničář	hraničář	k1gMnSc1	hraničář
ovládá	ovládat	k5eAaImIp3nS	ovládat
lehké	lehký	k2eAgFnPc4d1	lehká
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
sesílat	sesílat	k5eAaImF	sesílat
léčivá	léčivý	k2eAgNnPc4d1	léčivé
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
úrovních	úroveň	k1gFnPc6	úroveň
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
psionické	psionický	k2eAgFnPc4d1	psionická
schopnosti	schopnost	k1gFnPc4	schopnost
(	(	kIx(	(
<g/>
telekineze	telekineze	k1gFnSc1	telekineze
<g/>
,	,	kIx,	,
pyrokineze	pyrokineze	k1gFnSc1	pyrokineze
<g/>
,	,	kIx,	,
telepatie	telepatie	k1gFnSc1	telepatie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Druid	Druid	k1gMnSc1	Druid
je	být	k5eAaImIp3nS	být
lesní	lesní	k2eAgMnSc1d1	lesní
čaroděj	čaroděj	k1gMnSc1	čaroděj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
duchovní	duchovní	k2eAgFnSc4d1	duchovní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdysi	kdysi	k6eAd1	kdysi
vládl	vládnout	k5eAaImAgInS	vládnout
mistrně	mistrně	k6eAd1	mistrně
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
lesní	lesní	k2eAgNnPc4d1	lesní
kouzla	kouzlo	k1gNnPc4	kouzlo
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k9	i
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
spíše	spíše	k9	spíše
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Chodec	chodec	k1gMnSc1	chodec
je	být	k5eAaImIp3nS	být
hraničář	hraničář	k1gMnSc1	hraničář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
spíše	spíše	k9	spíše
své	svůj	k3xOyFgFnPc4	svůj
bojové	bojový	k2eAgFnPc4d1	bojová
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Toulá	toulat	k5eAaImIp3nS	toulat
se	se	k3xPyFc4	se
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
a	a	k8xC	a
zahání	zahánět	k5eAaImIp3nS	zahánět
zlé	zlý	k2eAgFnPc4d1	zlá
nestvůry	nestvůra	k1gFnPc4	nestvůra
od	od	k7c2	od
lidských	lidský	k2eAgInPc2d1	lidský
příbytků	příbytek	k1gInPc2	příbytek
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
kouzla	kouzlo	k1gNnPc4	kouzlo
k	k	k7c3	k
ulehčení	ulehčení	k1gNnSc3	ulehčení
těžkého	těžký	k2eAgInSc2d1	těžký
života	život	k1gInSc2	život
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
umí	umět	k5eAaImIp3nS	umět
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
lidských	lidský	k2eAgFnPc2d1	lidská
duší	duše	k1gFnPc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Alchymista	alchymista	k1gMnSc1	alchymista
je	být	k5eAaImIp3nS	být
šikovný	šikovný	k2eAgMnSc1d1	šikovný
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
vydrží	vydržet	k5eAaPmIp3nS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
lektvarů	lektvar	k1gInPc2	lektvar
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
magických	magický	k2eAgInPc2d1	magický
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
znění	znění	k1gNnSc2	znění
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
boji	boj	k1gInSc6	boj
obvykle	obvykle	k6eAd1	obvykle
nepomáhá	pomáhat	k5eNaImIp3nS	pomáhat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgNnSc2	svůj
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
platným	platný	k2eAgMnSc7d1	platný
pomocníkem	pomocník	k1gMnSc7	pomocník
–	–	k?	–
díky	díky	k7c3	díky
vyšší	vysoký	k2eAgFnSc3d2	vyšší
obratnosti	obratnost	k1gFnSc3	obratnost
dobře	dobře	k6eAd1	dobře
střílí	střílet	k5eAaImIp3nS	střílet
a	a	k8xC	a
díky	díky	k7c3	díky
odolnosti	odolnost	k1gFnSc3	odolnost
i	i	k9	i
dost	dost	k6eAd1	dost
vydrží	vydržet	k5eAaPmIp3nS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
obratnost	obratnost	k1gFnSc1	obratnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Theurg	Theurg	k1gInSc1	Theurg
ovládá	ovládat	k5eAaImIp3nS	ovládat
moc	moc	k6eAd1	moc
dvou	dva	k4xCgInPc2	dva
světů	svět	k1gInPc2	svět
<g/>
:	:	kIx,	:
astrálních	astrální	k2eAgFnPc2d1	astrální
sfér	sféra	k1gFnPc2	sféra
a	a	k8xC	a
základních	základní	k2eAgInPc2d1	základní
živlů	živel	k1gInPc2	živel
<g/>
.	.	kIx.	.
</s>
<s>
Astrální	astrální	k2eAgFnPc4d1	astrální
sféry	sféra	k1gFnPc4	sféra
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
současnosti	současnost	k1gFnSc2	současnost
či	či	k8xC	či
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
třeba	třeba	k6eAd1	třeba
i	i	k9	i
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
nad	nad	k7c4	nad
živly	živel	k1gInPc4	živel
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přivolat	přivolat	k5eAaPmF	přivolat
posly	posel	k1gMnPc4	posel
živlů	živel	k1gInPc2	živel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bojovali	bojovat	k5eAaImAgMnP	bojovat
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
<g/>
,	,	kIx,	,
či	či	k8xC	či
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
přivolat	přivolat	k5eAaPmF	přivolat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
katastrofy	katastrofa	k1gFnPc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrábět	vyrábět	k5eAaImF	vyrábět
trvalé	trvalý	k2eAgInPc4d1	trvalý
magické	magický	k2eAgInPc4d1	magický
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Pyrofor	pyrofor	k1gInSc1	pyrofor
(	(	kIx(	(
<g/>
ohňonoš	ohňonoš	k1gMnSc1	ohňonoš
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
pastí	past	k1gFnPc2	past
a	a	k8xC	a
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1	kouzelník
je	být	k5eAaImIp3nS	být
chytrý	chytrý	k2eAgMnSc1d1	chytrý
a	a	k8xC	a
umí	umět	k5eAaImIp3nS	umět
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
a	a	k8xC	a
zbroj	zbroj	k1gFnSc1	zbroj
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
věci	věc	k1gFnPc4	věc
podřadné	podřadný	k2eAgNnSc4d1	podřadné
a	a	k8xC	a
raději	rád	k6eAd2	rád
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
kouzelným	kouzelný	k2eAgMnSc7d1	kouzelný
pomocníkem	pomocník	k1gMnSc7	pomocník
–	–	k?	–
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
havranem	havran	k1gMnSc7	havran
či	či	k8xC	či
ďáblíkem	ďáblík	k1gMnSc7	ďáblík
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
inteligence	inteligence	k1gFnSc1	inteligence
a	a	k8xC	a
charisma	charisma	k1gNnSc1	charisma
<g/>
.	.	kIx.	.
</s>
<s>
Mág	mág	k1gMnSc1	mág
ovládá	ovládat	k5eAaImIp3nS	ovládat
zejména	zejména	k9	zejména
magii	magie	k1gFnSc4	magie
iluzí	iluze	k1gFnPc2	iluze
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
myšlení	myšlení	k1gNnSc3	myšlení
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
zabít	zabít	k5eAaPmF	zabít
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
pouhou	pouhý	k2eAgFnSc7d1	pouhá
silou	síla	k1gFnSc7	síla
své	svůj	k3xOyFgFnSc2	svůj
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1	čaroděj
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
kouzelníka	kouzelník	k1gMnSc2	kouzelník
z	z	k7c2	z
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
–	–	k?	–
více	hodně	k6eAd2	hodně
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
praktičnosti	praktičnost	k1gFnSc2	praktičnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
Experty	expert	k1gMnPc4	expert
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
nová	nový	k2eAgNnPc4d1	nové
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
podporuje	podporovat	k5eAaImIp3nS	podporovat
magickou	magický	k2eAgFnSc7d1	magická
holí	hole	k1gFnSc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Zloděj	zloděj	k1gMnSc1	zloděj
je	být	k5eAaImIp3nS	být
mrštný	mrštný	k2eAgMnSc1d1	mrštný
a	a	k8xC	a
umí	umět	k5eAaImIp3nS	umět
se	se	k3xPyFc4	se
vloudit	vloudit	k5eAaPmF	vloudit
do	do	k7c2	do
přízně	přízeň	k1gFnSc2	přízeň
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
zpravidla	zpravidla	k6eAd1	zpravidla
nestojí	stát	k5eNaImIp3nS	stát
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
i	i	k9	i
tvrdě	tvrdě	k6eAd1	tvrdě
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
–	–	k?	–
zejména	zejména	k9	zejména
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odborníky	odborník	k1gMnPc4	odborník
na	na	k7c4	na
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
důvtip	důvtip	k1gInSc4	důvtip
a	a	k8xC	a
obratnost	obratnost	k1gFnSc4	obratnost
místo	místo	k7c2	místo
hrubé	hrubý	k2eAgFnSc2d1	hrubá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc1	hledání
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc1	odstraňování
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
obratnost	obratnost	k1gFnSc1	obratnost
a	a	k8xC	a
charisma	charisma	k1gNnSc1	charisma
<g/>
.	.	kIx.	.
</s>
<s>
Lupič	lupič	k1gMnSc1	lupič
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
tichého	tichý	k2eAgInSc2d1	tichý
a	a	k8xC	a
nenápadného	nápadný	k2eNgInSc2d1	nenápadný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
umí	umět	k5eAaImIp3nS	umět
unikat	unikat	k5eAaImF	unikat
z	z	k7c2	z
pout	pouto	k1gNnPc2	pouto
<g/>
,	,	kIx,	,
imitovat	imitovat	k5eAaBmF	imitovat
hlasy	hlas	k1gInPc4	hlas
či	či	k8xC	či
působit	působit	k5eAaImF	působit
nenápadně	nápadně	k6eNd1	nápadně
v	v	k7c6	v
davu	dav	k1gInSc6	dav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
novým	nový	k2eAgFnPc3d1	nová
silám	síla	k1gFnPc3	síla
patří	patřit	k5eAaImIp3nS	patřit
boj	boj	k1gInSc4	boj
beze	beze	k7c2	beze
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Sicco	Sicco	k1gMnSc1	Sicco
je	být	k5eAaImIp3nS	být
vládcem	vládce	k1gMnSc7	vládce
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
své	svůj	k3xOyFgFnPc4	svůj
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
zločinecké	zločinecký	k2eAgFnPc4d1	zločinecká
sítě	síť	k1gFnPc4	síť
a	a	k8xC	a
zadávat	zadávat	k5eAaImF	zadávat
jim	on	k3xPp3gMnPc3	on
různé	různý	k2eAgInPc4d1	různý
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
krádeže	krádež	k1gFnPc1	krádež
<g/>
,	,	kIx,	,
únosy	únos	k1gInPc1	únos
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc1	vražda
<g/>
,	,	kIx,	,
sabotáže	sabotáž	k1gFnPc1	sabotáž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
ochranu	ochrana	k1gFnSc4	ochrana
osob	osoba	k1gFnPc2	osoba
či	či	k8xC	či
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgNnPc2d1	základní
D	D	kA	D
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
úroveň	úroveň	k1gFnSc1	úroveň
postav	postava	k1gFnPc2	postava
<g/>
)	)	kIx)	)
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
díly	díl	k1gInPc4	díl
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
D	D	kA	D
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
(	(	kIx(	(
<g/>
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
postavy	postava	k1gFnPc4	postava
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
rozepsáním	rozepsání	k1gNnSc7	rozepsání
dvou	dva	k4xCgFnPc2	dva
specializací	specializace	k1gFnPc2	specializace
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
povolání	povolání	k1gNnSc4	povolání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
D	D	kA	D
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
Experty	expert	k1gMnPc4	expert
(	(	kIx(	(
<g/>
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
zpracované	zpracovaný	k2eAgFnSc2d1	zpracovaná
36	[number]	k4	36
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ALTAR	ALTAR	kA	ALTAR
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
D	D	kA	D
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
projekt	projekt	k1gInSc1	projekt
ASTERION	ASTERION	kA	ASTERION
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
nové	nový	k2eAgFnSc2d1	nová
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
povolání	povolání	k1gNnSc2	povolání
atd.	atd.	kA	atd.
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
to	ten	k3xDgNnSc1	ten
zdaleka	zdaleka	k6eAd1	zdaleka
nekončí	končit	k5eNaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
svůj	svůj	k3xOyFgInSc4	svůj
Asterion	Asterion	k1gInSc4	Asterion
rozvinout	rozvinout	k5eAaPmF	rozvinout
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
geografické	geografický	k2eAgInPc4d1	geografický
<g/>
/	/	kIx~	/
<g/>
tematické	tematický	k2eAgInPc4d1	tematický
moduly	modul	k1gInPc4	modul
(	(	kIx(	(
<g/>
Geografické	geografický	k2eAgInPc1d1	geografický
moduly	modul	k1gInPc1	modul
rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
a	a	k8xC	a
podrobně	podrobně	k6eAd1	podrobně
popisují	popisovat	k5eAaImIp3nP	popisovat
různé	různý	k2eAgFnPc4d1	různá
oblasti	oblast	k1gFnPc4	oblast
původního	původní	k2eAgInSc2d1	původní
Asterionu	Asterion	k1gInSc2	Asterion
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Obloha	obloha	k1gFnSc1	obloha
z	z	k7c2	z
listí	listí	k1gNnSc2	listí
a	a	k8xC	a
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
zelené	zelený	k2eAgFnSc2d1	zelená
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tematické	tematický	k2eAgInPc1d1	tematický
moduly	modul	k1gInPc1	modul
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
svět	svět	k1gInSc4	svět
D	D	kA	D
Asterion	Asterion	k1gInSc1	Asterion
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k6eAd1	třeba
magii	magie	k1gFnSc4	magie
nebo	nebo	k8xC	nebo
zlodějské	zlodějský	k2eAgFnPc4d1	zlodějská
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Falešná	falešný	k2eAgFnSc1d1	falešná
apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
nebo	nebo	k8xC	nebo
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
pavučina	pavučina	k1gFnSc1	pavučina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modulů	modul	k1gInPc2	modul
samozřejmě	samozřejmě	k6eAd1	samozřejmě
existuje	existovat	k5eAaImIp3nS	existovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
se	se	k3xPyFc4	se
z	z	k7c2	z
vaší	váš	k3xOp2gFnSc2	váš
Hry	hra	k1gFnSc2	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
hra	hra	k1gFnSc1	hra
rovnající	rovnající	k2eAgFnSc1d1	rovnající
se	se	k3xPyFc4	se
i	i	k9	i
novým	nový	k2eAgFnPc3d1	nová
počítačovým	počítačový	k2eAgFnPc3d1	počítačová
hrám	hra	k1gFnPc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
lze	lze	k6eAd1	lze
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
chat	chata	k1gFnPc2	chata
či	či	k8xC	či
přes	přes	k7c4	přes
specializované	specializovaný	k2eAgNnSc4d1	specializované
fórum	fórum	k1gNnSc4	fórum
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
těchto	tento	k3xDgFnPc2	tento
fór	fór	k1gInSc1	fór
bývá	bývat	k5eAaImIp3nS	bývat
automatická	automatický	k2eAgFnSc1d1	automatická
správa	správa	k1gFnSc1	správa
statistik	statistika	k1gFnPc2	statistika
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kvalita	kvalita	k1gFnSc1	kvalita
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
fórum	fórum	k1gNnSc1	fórum
od	od	k7c2	od
fóra	fórum	k1gNnSc2	fórum
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
pomalost	pomalost	k1gFnSc1	pomalost
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
chybějící	chybějící	k2eAgInSc4d1	chybějící
osobní	osobní	k2eAgInSc4d1	osobní
kontakt	kontakt	k1gInSc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vyváženo	vyvážit	k5eAaPmNgNnS	vyvážit
možností	možnost	k1gFnSc7	možnost
obohatit	obohatit	k5eAaPmF	obohatit
hru	hra	k1gFnSc4	hra
ostatním	ostatní	k1gNnSc7	ostatní
o	o	k7c4	o
detailní	detailní	k2eAgInPc4d1	detailní
popisy	popis	k1gInPc4	popis
či	či	k8xC	či
sdílení	sdílení	k1gNnSc4	sdílení
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
monologů	monolog	k1gInPc2	monolog
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Altar	Altar	k1gMnSc1	Altar
Recenze	recenze	k1gFnSc2	recenze
D	D	kA	D
po	po	k7c6	po
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
RPGfóru	RPGfór	k1gInSc6	RPGfór
Nahrávka	nahrávka	k1gFnSc1	nahrávka
ukázky	ukázka	k1gFnSc2	ukázka
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
povídání	povídání	k1gNnSc2	povídání
o	o	k7c4	o
D	D	kA	D
na	na	k7c6	na
radiu	radio	k1gNnSc6	radio
Wave	Wav	k1gFnSc2	Wav
</s>
