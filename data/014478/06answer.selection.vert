<s>
Černá	černý	k2eAgFnSc1d1
díra	díra	k1gFnSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
hmotný	hmotný	k2eAgInSc1d1
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc1
gravitační	gravitační	k2eAgNnSc1d1
pole	pole	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
jisté	jistý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
časoprostoru	časoprostor	k1gInSc2
natolik	natolik	k6eAd1
silné	silný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
žádný	žádný	k3yNgInSc1
objekt	objekt	k1gInSc1
včetně	včetně	k7c2
světla	světlo	k1gNnSc2
nemůže	moct	k5eNaImIp3nS
tuto	tento	k3xDgFnSc4
oblast	oblast	k1gFnSc4
opustit	opustit	k5eAaPmF
<g/>
.	.	kIx.
</s>