<p>
<s>
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1881	[number]	k4	1881
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
a	a	k8xC	a
geograf	geograf	k1gMnSc1	geograf
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
profesora	profesor	k1gMnSc2	profesor
brněnského	brněnský	k2eAgNnSc2d1	brněnské
gymnázia	gymnázium	k1gNnSc2	gymnázium
Františka	František	k1gMnSc2	František
Koláčka	Koláček	k1gMnSc2	Koláček
staršího	starší	k1gMnSc2	starší
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Aloisie	Aloisie	k1gFnSc2	Aloisie
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Vodičkové	Vodičková	k1gFnSc2	Vodičková
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
Hermína	Hermína	k1gFnSc1	Hermína
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
také	také	k6eAd1	také
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
byla	být	k5eAaImAgFnS	být
rodina	rodina	k1gFnSc1	rodina
policejně	policejně	k6eAd1	policejně
hlášena	hlásit	k5eAaImNgFnS	hlásit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Královských	královský	k2eAgInPc6d1	královský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
brněnskou	brněnský	k2eAgFnSc7d1	brněnská
lékařkou	lékařka	k1gFnSc7	lékařka
MUDr.	MUDr.	kA	MUDr.
Miroslavou	Miroslava	k1gFnSc7	Miroslava
Mazalovou	Mazalův	k2eAgFnSc7d1	Mazalův
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Kühnovou	Kühnová	k1gFnSc4	Kühnová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesní	profesní	k2eAgFnSc1d1	profesní
kariéra	kariéra	k1gFnSc1	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
studoval	studovat	k5eAaImAgInS	studovat
dějepis	dějepis	k1gInSc1	dějepis
a	a	k8xC	a
zeměpis	zeměpis	k1gInSc1	zeměpis
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
doktorátem	doktorát	k1gInSc7	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
soukromým	soukromý	k2eAgMnSc7d1	soukromý
docentem	docent	k1gMnSc7	docent
regionálního	regionální	k2eAgInSc2d1	regionální
zeměpisu	zeměpis	k1gInSc2	zeměpis
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
mimořádným	mimořádný	k2eAgInPc3d1	mimořádný
a	a	k8xC	a
1929	[number]	k4	1929
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
geografie	geografie	k1gFnSc2	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
děkanem	děkan	k1gMnSc7	děkan
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okupace	okupace	k1gFnPc4	okupace
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
brněnskými	brněnský	k2eAgMnPc7d1	brněnský
vysokoškolskými	vysokoškolský	k2eAgMnPc7d1	vysokoškolský
pedagogy	pedagog	k1gMnPc7	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
zatčen	zatčen	k2eAgInSc1d1	zatčen
brněnským	brněnský	k2eAgNnSc7d1	brněnské
Gestapem	gestapo	k1gNnSc7	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zatýkání	zatýkání	k1gNnSc6	zatýkání
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
Gestapo	gestapo	k1gNnSc1	gestapo
i	i	k9	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
mapu	mapa	k1gFnSc4	mapa
území	území	k1gNnSc2	území
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
byly	být	k5eAaImAgFnP	být
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
plochy	plocha	k1gFnPc1	plocha
vhodné	vhodný	k2eAgFnPc1d1	vhodná
z	z	k7c2	z
geografického	geografický	k2eAgInSc2d1	geografický
a	a	k8xC	a
klimatologického	klimatologický	k2eAgNnSc2d1	klimatologické
hlediska	hledisko	k1gNnSc2	hledisko
pro	pro	k7c4	pro
případné	případný	k2eAgInPc4d1	případný
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
výsadky	výsadek	k1gInPc4	výsadek
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Koláček	Koláček	k1gMnSc1	Koláček
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
vyslýchání	vyslýchání	k1gNnSc6	vyslýchání
zatčených	zatčený	k1gMnPc2	zatčený
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
těchto	tento	k3xDgFnPc2	tento
analýz	analýza	k1gFnPc2	analýza
a	a	k8xC	a
následně	následně	k6eAd1	následně
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc6	smrt
brněnským	brněnský	k2eAgInSc7d1	brněnský
stanným	stanný	k2eAgInSc7d1	stanný
soudem	soud	k1gInSc7	soud
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Popraven	popraven	k2eAgInSc1d1	popraven
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
72	[number]	k4	72
odbojáři	odbojář	k1gMnPc7	odbojář
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
vydal	vydat	k5eAaPmAgMnS	vydat
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
</s>
</p>
<p>
<s>
===	===	k?	===
Studie	studie	k1gFnPc1	studie
o	o	k7c6	o
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
===	===	k?	===
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
o	o	k7c6	o
českých	český	k2eAgNnPc6d1	české
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karpatské	karpatský	k2eAgNnSc1d1	Karpatské
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
Přírodověd	přírodověda	k1gFnPc2	přírodověda
<g/>
.	.	kIx.	.
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
otřesů	otřes	k1gInPc2	otřes
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
Přírodověd	přírodověda	k1gFnPc2	přírodověda
<g/>
.	.	kIx.	.
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
publikace	publikace	k1gFnPc4	publikace
a	a	k8xC	a
učebnice	učebnice	k1gFnPc4	učebnice
===	===	k?	===
</s>
</p>
<p>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
kartografie	kartografie	k1gFnSc2	kartografie
čili	čili	k8xC	čili
do	do	k7c2	do
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
mapách	mapa	k1gFnPc6	mapa
(	(	kIx(	(
<g/>
Zemský	zemský	k2eAgInSc1d1	zemský
ústřední	ústřední	k2eAgInSc1d1	ústřední
spolek	spolek	k1gInSc1	spolek
Jednot	jednota	k1gFnPc2	jednota
učitelských	učitelský	k2eAgFnPc2d1	učitelská
v	v	k7c6	v
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fysikální	fysikální	k2eAgInSc1d1	fysikální
zeměpis	zeměpis	k1gInSc1	zeměpis
karpatské	karpatský	k2eAgFnSc2d1	Karpatská
části	část	k1gFnSc2	část
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
/	/	kIx~	/
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
grafická	grafický	k2eAgFnSc1d1	grafická
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
Jos	Jos	k1gFnPc6	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zeměpis	zeměpis	k1gInSc1	zeměpis
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
Melantrich	Melantrich	k1gInSc4	Melantrich
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
zeměpisu	zeměpis	k1gInSc2	zeměpis
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
třídy	třída	k1gFnPc4	třída
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
Urbanem	Urban	k1gMnSc7	Urban
a	a	k8xC	a
Františkem	František	k1gMnSc7	František
Zpěvákem	Zpěvák	k1gMnSc7	Zpěvák
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
grafická	grafický	k2eAgFnSc1d1	grafická
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
;	;	kIx,	;
Česká	český	k2eAgFnSc1d1	Česká
grafická	grafický	k2eAgFnSc1d1	grafická
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kartografické	kartografický	k2eAgFnPc1d1	kartografická
práce	práce	k1gFnPc1	práce
-	-	kIx~	-
mapy	mapa	k1gFnPc1	mapa
===	===	k?	===
</s>
</p>
<p>
<s>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
mapa	mapa	k1gFnSc1	mapa
republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
:	:	kIx,	:
Západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
<g/>
:	:	kIx,	:
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
(	(	kIx(	(
<g/>
Měř	měřit	k5eAaImRp2nS	měřit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
PhDr.	PhDr.	kA	PhDr.
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Horákem	Horák	k1gMnSc7	Horák
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
učitelské	učitelský	k2eAgFnSc2d1	učitelská
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
odtoková	odtokový	k2eAgFnSc1d1	odtoková
mapa	mapa	k1gFnSc1	mapa
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
Carte	Cart	k1gMnSc5	Cart
géographique	géographiquus	k1gMnSc5	géographiquus
du	du	k?	du
débit	débit	k1gInSc1	débit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
eau	eau	k?	eau
relatif	relatif	k1gInSc1	relatif
de	de	k?	de
la	la	k1gNnSc7	la
Moravie	Moravie	k1gFnSc2	Moravie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
okresu	okres	k1gInSc2	okres
Tišnovského	tišnovský	k2eAgInSc2d1	tišnovský
(	(	kIx(	(
<g/>
Měř	měřit	k5eAaImRp2nS	měřit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
F.	F.	kA	F.
Syrovátkou	syrovátka	k1gFnSc7	syrovátka
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc4	některý
práce	práce	k1gFnPc4	práce
Františka	František	k1gMnSc2	František
Koláčka	Koláček	k1gMnSc2	Koláček
byly	být	k5eAaImAgFnP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
historickou	historický	k2eAgFnSc4d1	historická
kartografii	kartografie	k1gFnSc4	kartografie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
mapa	mapa	k1gFnSc1	mapa
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Dva	dva	k4xCgInPc1	dva
plány	plán	k1gInPc1	plán
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1741	[number]	k4	1741
a	a	k8xC	a
1742	[number]	k4	1742
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
map	mapa	k1gFnPc2	mapa
v	v	k7c6	v
rajhradském	rajhradský	k2eAgInSc6d1	rajhradský
klášteře	klášter	k1gInSc6	klášter
a	a	k8xC	a
Některé	některý	k3yIgFnPc1	některý
poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
Müllerových	Müllerův	k2eAgFnPc6d1	Müllerova
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
popularizoval	popularizovat	k5eAaImAgMnS	popularizovat
vědu	věda	k1gFnSc4	věda
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
přednáškami	přednáška	k1gFnPc7	přednáška
o	o	k7c6	o
klimatologii	klimatologie	k1gFnSc6	klimatologie
a	a	k8xC	a
kartografii	kartografie	k1gFnSc6	kartografie
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Sávy	Sáva	k1gFnSc2	Sáva
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
Jugoslavii	Jugoslavie	k1gFnSc4	Jugoslavie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
</s>
</p>
<p>
<s>
Čestný	čestný	k2eAgMnSc1d1	čestný
člen	člen	k1gMnSc1	člen
Muzejního	muzejní	k2eAgInSc2d1	muzejní
spolku	spolek	k1gInSc2	spolek
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Františka	František	k1gMnSc2	František
Koláčka	Koláček	k1gMnSc2	Koláček
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c6	na
pamětní	pamětní	k2eAgFnSc6d1	pamětní
desce	deska	k1gFnSc6	deska
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
učitelů	učitel	k1gMnPc2	učitel
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
umučených	umučený	k2eAgFnPc2d1	umučená
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
v	v	k7c6	v
letech	let	k1gInPc6	let
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
a	a	k8xC	a
na	na	k7c6	na
pamětní	pamětní	k2eAgFnSc6d1	pamětní
desce	deska	k1gFnSc6	deska
Sokola	Sokol	k1gMnSc2	Sokol
Brno	Brno	k1gNnSc1	Brno
I	i	k8xC	i
v	v	k7c6	v
Kounicově	Kounicově	k1gFnSc6	Kounicově
ulici	ulice	k1gFnSc6	ulice
</s>
</p>
<p>
<s>
ČGS	ČGS	kA	ČGS
mu	on	k3xPp3gMnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
členství	členství	k1gNnSc4	členství
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
domem	dům	k1gInSc7	dům
Pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
umístěn	umístěn	k2eAgInSc1d1	umístěn
"	"	kIx"	"
<g/>
Kámen	kámen	k1gInSc1	kámen
zmizelých	zmizelý	k2eAgMnPc2d1	zmizelý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stolperstein	Stolperstein	k1gInSc1	Stolperstein
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Františka	František	k1gMnSc2	František
Koláčka	Koláček	k1gMnSc2	Koláček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
(	(	kIx(	(
<g/>
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÁ	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgNnPc1d1	volné
díla	dílo	k1gNnPc1	dílo
českých	český	k2eAgMnPc2d1	český
kartografů	kartograf	k1gMnPc2	kartograf
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
v	v	k7c6	v
období	období	k1gNnSc6	období
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Geodetický	geodetický	k2eAgInSc1d1	geodetický
a	a	k8xC	a
kartografický	kartografický	k2eAgInSc1d1	kartografický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
102	[number]	k4	102
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MU	MU	kA	MU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
fotografií	fotografia	k1gFnPc2	fotografia
ze	z	k7c2	z
života	život	k1gInSc2	život
Františka	František	k1gMnSc2	František
Koláčka	Koláček	k1gMnSc2	Koláček
</s>
</p>
