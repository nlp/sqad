<s>
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1881	[number]	k4	1881
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
a	a	k8xC	a
geograf	geograf	k1gMnSc1	geograf
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
