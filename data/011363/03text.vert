<p>
<s>
<g/>
free	free	k1gInSc1	free
je	být	k5eAaImIp3nS	být
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
projekt	projekt	k1gInSc1	projekt
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
dotFree	dotFreat	k5eAaPmIp3nS	dotFreat
Group	Group	k1gInSc1	Group
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
domén	doména	k1gFnPc2	doména
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
domény	doména	k1gFnPc4	doména
.	.	kIx.	.
<g/>
free	free	k6eAd1	free
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
počtu	počet	k1gInSc6	počet
zdarma	zdarma	k6eAd1	zdarma
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
člověka	člověk	k1gMnSc4	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Domény	doména	k1gFnPc1	doména
.	.	kIx.	.
<g/>
free	free	k6eAd1	free
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prozatím	prozatím	k6eAd1	prozatím
registrovat	registrovat	k5eAaBmF	registrovat
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
nejdříve	dříve	k6eAd3	dříve
projít	projít	k5eAaPmF	projít
schválením	schválení	k1gNnSc7	schválení
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
fáze	fáze	k1gFnSc1	fáze
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
vlastníků	vlastník	k1gMnPc2	vlastník
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
Sunrise	Sunrise	k1gFnSc1	Sunrise
<g/>
)	)	kIx)	)
a	a	k8xC	a
fáze	fáze	k1gFnSc1	fáze
předregistrace	předregistrace	k1gFnSc1	předregistrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
doména	doména	k1gFnSc1	doména
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
k	k	k7c3	k
registraci	registrace	k1gFnSc3	registrace
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Omezení	omezení	k1gNnSc1	omezení
==	==	k?	==
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
registrovat	registrovat	k5eAaBmF	registrovat
si	se	k3xPyFc3	se
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
doménu	doména	k1gFnSc4	doména
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
chtít	chtít	k5eAaImF	chtít
uživatel	uživatel	k1gMnSc1	uživatel
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
domén	doména	k1gFnPc2	doména
.	.	kIx.	.
<g/>
free	freat	k5eAaPmIp3nS	freat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
si	se	k3xPyFc3	se
muset	muset	k5eAaImF	muset
pořídit	pořídit	k5eAaPmF	pořídit
VIP	VIP	kA	VIP
účet	účet	k1gInSc4	účet
a	a	k8xC	a
zaplatit	zaplatit	k5eAaPmF	zaplatit
za	za	k7c4	za
každých	každý	k3xTgFnPc2	každý
5	[number]	k4	5
domén	doména	k1gFnPc2	doména
určený	určený	k2eAgInSc4d1	určený
poplatek	poplatek	k1gInSc4	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
však	však	k9	však
bude	být	k5eAaImBp3nS	být
50	[number]	k4	50
domén	doména	k1gFnPc2	doména
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
VIP	VIP	kA	VIP
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
