<s>
Dvě	dva	k4xCgFnPc4	dva
věže	věž	k1gFnPc4	věž
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
druhého	druhý	k4xOgInSc2	druhý
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
svazků	svazek	k1gInPc2	svazek
románu	román	k1gInSc2	román
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
sleduje	sledovat	k5eAaImIp3nS	sledovat
osudy	osud	k1gInPc4	osud
Aragorna	Aragorno	k1gNnSc2	Aragorno
<g/>
,	,	kIx,	,
Legolase	Legolasa	k1gFnSc3	Legolasa
a	a	k8xC	a
Gimliho	Gimli	k1gMnSc4	Gimli
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
sledovat	sledovat	k5eAaImF	sledovat
skřety	skřet	k1gMnPc4	skřet
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
uneseným	unesený	k2eAgInSc7d1	unesený
Smíškem	smíšek	k1gInSc7	smíšek
a	a	k8xC	a
Pipinem	Pipin	k1gInSc7	Pipin
směřují	směřovat	k5eAaImIp3nP	směřovat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Železného	železný	k2eAgInSc2d1	železný
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
zlého	zlý	k2eAgMnSc2d1	zlý
čaroděje	čaroděj	k1gMnSc2	čaroděj
Sarumana	Saruman	k1gMnSc2	Saruman
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
skřeti	skřet	k1gMnPc1	skřet
jsou	být	k5eAaImIp3nP	být
pobiti	pobít	k5eAaPmNgMnP	pobít
jezdeckou	jezdecký	k2eAgFnSc7d1	jezdecká
družinou	družina	k1gFnSc7	družina
Rohirů	Rohir	k1gInPc2	Rohir
<g/>
,	,	kIx,	,
obyvateli	obyvatel	k1gMnSc3	obyvatel
Rohanu	Rohan	k1gMnSc3	Rohan
<g/>
,	,	kIx,	,
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
území	území	k1gNnSc4	území
skřeti	skřet	k1gMnPc1	skřet
putují	putovat	k5eAaImIp3nP	putovat
<g/>
.	.	kIx.	.
</s>
<s>
Zajatí	zajatý	k2eAgMnPc1d1	zajatý
hobiti	hobit	k1gMnPc1	hobit
přitom	přitom	k6eAd1	přitom
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
uniknou	uniknout	k5eAaPmIp3nP	uniknout
a	a	k8xC	a
dostanou	dostat	k5eAaPmIp3nP	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
blízkého	blízký	k2eAgInSc2d1	blízký
lesa	les	k1gInSc2	les
Fangornu	Fangorn	k1gInSc2	Fangorn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
entů	ent	k1gInPc2	ent
<g/>
,	,	kIx,	,
stromům	strom	k1gInPc3	strom
podobných	podobný	k2eAgFnPc2d1	podobná
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
les	les	k1gInSc1	les
opatrují	opatrovat	k5eAaImIp3nP	opatrovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
enta	entus	k1gMnSc4	entus
jménem	jméno	k1gNnSc7	jméno
Stromovous	Stromovous	k1gMnSc1	Stromovous
Smíšek	Smíšek	k1gMnSc1	Smíšek
s	s	k7c7	s
Pipinem	Pipin	k1gMnSc7	Pipin
narazí	narazit	k5eAaPmIp3nS	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
příchodem	příchod	k1gInSc7	příchod
přimějí	přimět	k5eAaPmIp3nP	přimět
enty	ent	k2eAgFnPc1d1	ent
se	se	k3xPyFc4	se
sejít	sejít	k5eAaPmF	sejít
a	a	k8xC	a
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
jednáním	jednání	k1gNnSc7	jednání
Sarumana	Saruman	k1gMnSc2	Saruman
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
sídlo	sídlo	k1gNnSc1	sídlo
s	s	k7c7	s
Fangornem	Fangorn	k1gInSc7	Fangorn
sousedí	sousedit	k5eAaImIp3nS	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Hněv	hněv	k1gInSc1	hněv
Entů	Ent	k1gMnPc2	Ent
nad	nad	k7c7	nad
Sarumanem	Saruman	k1gInSc7	Saruman
konečně	konečně	k6eAd1	konečně
přeteče	přetéct	k5eAaPmIp3nS	přetéct
a	a	k8xC	a
Entové	Entové	k2eAgMnSc1d1	Entové
se	se	k3xPyFc4	se
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
hobity	hobit	k1gMnPc7	hobit
vydají	vydat	k5eAaPmIp3nP	vydat
k	k	k7c3	k
Sarumanovu	Sarumanův	k2eAgNnSc3d1	Sarumanovo
sídlu	sídlo	k1gNnSc3	sídlo
<g/>
,	,	kIx,	,
věži	věž	k1gFnSc3	věž
Orthanku	Orthanka	k1gFnSc4	Orthanka
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
uprostřed	uprostřed	k7c2	uprostřed
kruhu	kruh	k1gInSc2	kruh
skal	skála	k1gFnPc2	skála
v	v	k7c6	v
Železném	železný	k2eAgInSc6d1	železný
pasu	pas	k1gInSc6	pas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Železném	železný	k2eAgInSc6d1	železný
pasu	pas	k1gInSc6	pas
si	se	k3xPyFc3	se
Saruman	Saruman	k1gMnSc1	Saruman
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
dílny	dílna	k1gFnPc4	dílna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vyzbrojuje	vyzbrojovat	k5eAaImIp3nS	vyzbrojovat
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dříve	dříve	k6eAd2	dříve
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
po	po	k7c6	po
Jednom	jeden	k4xCgInSc6	jeden
prstenu	prsten	k1gInSc6	prsten
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
získat	získat	k5eAaPmF	získat
místo	místo	k6eAd1	místo
Saurona	Saurona	k1gFnSc1	Saurona
(	(	kIx(	(
<g/>
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
čtenář	čtenář	k1gMnSc1	čtenář
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
z	z	k7c2	z
kapitoly	kapitola	k1gFnSc2	kapitola
o	o	k7c6	o
Elrondově	Elrondův	k2eAgFnSc6d1	Elrondova
poradě	porada	k1gFnSc6	porada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Entové	Entové	k2eAgFnPc1d1	Entové
dílny	dílna	k1gFnPc1	dílna
v	v	k7c6	v
Železném	železný	k2eAgInSc6d1	železný
pasu	pas	k1gInSc6	pas
zničí	zničit	k5eAaPmIp3nS	zničit
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
Orthank	Orthanka	k1gFnPc2	Orthanka
obklíčí	obklíčit	k5eAaPmIp3nS	obklíčit
(	(	kIx(	(
<g/>
nemohou	moct	k5eNaImIp3nP	moct
ji	on	k3xPp3gFnSc4	on
zničit	zničit	k5eAaPmF	zničit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saruman	Saruman	k1gMnSc1	Saruman
je	být	k5eAaImIp3nS	být
polapen	polapit	k5eAaPmNgMnS	polapit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
sídle	sídlo	k1gNnSc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
s	s	k7c7	s
Legolasem	Legolas	k1gInSc7	Legolas
a	a	k8xC	a
Gimlim	Gimlim	k1gMnSc1	Gimlim
nedostihnou	dostihnout	k5eNaPmIp3nP	dostihnout
skřety	skřet	k1gMnPc7	skřet
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
pobiti	pobit	k2eAgMnPc1d1	pobit
Jezdci	jezdec	k1gMnPc1	jezdec
Rohanu	Rohan	k1gMnSc3	Rohan
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zajatými	zajatý	k2eAgMnPc7d1	zajatý
hobity	hobit	k1gMnPc7	hobit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nesetkají	setkat	k5eNaPmIp3nP	setkat
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
rohanskou	rohanský	k2eAgFnSc7d1	rohanská
družinou	družina	k1gFnSc7	družina
<g/>
,	,	kIx,	,
vedenou	vedený	k2eAgFnSc4d1	vedená
Éomerem	Éomer	k1gInSc7	Éomer
<g/>
,	,	kIx,	,
synovcem	synovec	k1gMnSc7	synovec
rohanského	rohanský	k2eAgInSc2d1	rohanský
krále	král	k1gMnSc4	král
Théodena	Théoden	k2eAgFnSc1d1	Théoden
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
hledat	hledat	k5eAaImF	hledat
do	do	k7c2	do
Fangornu	Fangorn	k1gInSc2	Fangorn
Smíška	Smíšek	k1gMnSc2	Smíšek
s	s	k7c7	s
Pipinem	Pipino	k1gNnSc7	Pipino
<g/>
,	,	kIx,	,
narazí	narazit	k5eAaPmIp3nP	narazit
tam	tam	k6eAd1	tam
však	však	k9	však
na	na	k7c4	na
Gandalfa	Gandalf	k1gMnSc4	Gandalf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
balrogem	balrog	k1gInSc7	balrog
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
svět	svět	k1gInSc4	svět
ještě	ještě	k6eAd1	ještě
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
splnil	splnit	k5eAaPmAgMnS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
-	-	kIx~	-
jako	jako	k9	jako
Gandalf	Gandalf	k1gInSc1	Gandalf
Bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gInSc1	Gandalf
Aragorna	Aragorno	k1gNnSc2	Aragorno
s	s	k7c7	s
druhy	druh	k1gInPc7	druh
odvrací	odvracet	k5eAaImIp3nS	odvracet
od	od	k7c2	od
dalšího	další	k2eAgNnSc2d1	další
hledání	hledání	k1gNnSc2	hledání
hobitů	hobit	k1gMnPc2	hobit
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
rohanského	rohanský	k2eAgNnSc2d1	rohanské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Edorasu	Edoras	k1gInSc2	Edoras
<g/>
.	.	kIx.	.
</s>
<s>
Rohanský	Rohanský	k2eAgMnSc1d1	Rohanský
král	král	k1gMnSc1	král
Théoden	Théodna	k1gFnPc2	Théodna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
vlivu	vliv	k1gInSc2	vliv
svého	svůj	k3xOyFgMnSc2	svůj
rádce	rádce	k1gMnSc2	rádce
Grímy	Gríma	k1gFnSc2	Gríma
Červivce	Červivec	k1gMnSc2	Červivec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Sarumanova	Sarumanův	k2eAgMnSc2d1	Sarumanův
sluhy	sluha	k1gMnSc2	sluha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
Rohiry	Rohir	k1gMnPc4	Rohir
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
Sarumanově	Sarumanův	k2eAgInSc6d1	Sarumanův
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Grímy	Gríma	k1gFnSc2	Gríma
osvobozený	osvobozený	k2eAgInSc4d1	osvobozený
Théoden	Théodna	k1gFnPc2	Théodna
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
Železného	železný	k2eAgInSc2d1	železný
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skoncoval	skoncovat	k5eAaPmAgInS	skoncovat
se	s	k7c7	s
Sarumanovou	Sarumanův	k2eAgFnSc7d1	Sarumanova
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Saruman	Saruman	k1gMnSc1	Saruman
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
Rohiry	Rohira	k1gMnSc2	Rohira
zničit	zničit	k5eAaPmF	zničit
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
Uruk-hai	Uruka	k1gFnPc1	Uruk-ha
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
rohanské	rohanský	k2eAgFnSc2d1	rohanská
pevnosti	pevnost	k1gFnSc2	pevnost
Hlásky	hláska	k1gFnSc2	hláska
v	v	k7c6	v
Helmově	helmově	k6eAd1	helmově
žlebu	žleb	k1gInSc2	žleb
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Sarumanovo	Sarumanův	k2eAgNnSc1d1	Sarumanovo
vojsko	vojsko	k1gNnSc1	vojsko
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
zničeno	zničit	k5eAaPmNgNnS	zničit
a	a	k8xC	a
Gandalf	Gandalf	k1gInSc1	Gandalf
s	s	k7c7	s
Éomerem	Éomer	k1gInSc7	Éomer
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
přemoci	přemoct	k5eAaPmF	přemoct
samotného	samotný	k2eAgMnSc4d1	samotný
Sarumana	Saruman	k1gMnSc4	Saruman
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
pokusí	pokusit	k5eAaPmIp3nS	pokusit
silou	síla	k1gFnSc7	síla
své	svůj	k3xOyFgFnSc2	svůj
výmluvnosti	výmluvnost	k1gFnSc2	výmluvnost
svést	svést	k5eAaPmF	svést
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Gandalf	Gandalf	k1gInSc4	Gandalf
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
odejme	odejmout	k5eAaPmIp3nS	odejmout
jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Sarumanově	Sarumanův	k2eAgFnSc6d1	Sarumanova
porážce	porážka	k1gFnSc6	porážka
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
Sauron	Sauron	k1gMnSc1	Sauron
(	(	kIx(	(
<g/>
Saruman	Saruman	k1gMnSc1	Saruman
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
vidoucí	vidoucí	k2eAgInSc4d1	vidoucí
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
palantír	palantír	k1gInSc4	palantír
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
spojení	spojení	k1gNnSc4	spojení
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
podívá	podívat	k5eAaPmIp3nS	podívat
Pipin	pipina	k1gFnPc2	pipina
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšle	vyšle	k6eAd1	vyšle
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nazgû	nazgû	k?	nazgû
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
dopravil	dopravit	k5eAaPmAgMnS	dopravit
hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nese	nést	k5eAaImIp3nS	nést
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
předzvěst	předzvěst	k1gFnSc1	předzvěst
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Sauron	Sauron	k1gMnSc1	Sauron
hodlá	hodlat	k5eAaImIp3nS	hodlat
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Gandalf	Gandalf	k1gMnSc1	Gandalf
spěšně	spěšně	k6eAd1	spěšně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pipinem	Pipino	k1gNnSc7	Pipino
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
cestu	cesta	k1gFnSc4	cesta
Froda	Frod	k1gMnSc2	Frod
a	a	k8xC	a
Sama	Sam	k1gMnSc2	Sam
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Společenstva	společenstvo	k1gNnSc2	společenstvo
narazí	narazit	k5eAaPmIp3nP	narazit
na	na	k7c4	na
Gluma	Glum	k1gMnSc4	Glum
<g/>
,	,	kIx,	,
tvora	tvor	k1gMnSc4	tvor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
před	před	k7c7	před
Bilbem	Bilb	k1gInSc7	Bilb
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc6	hobit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
sledoval	sledovat	k5eAaImAgMnS	sledovat
Společenstvo	společenstvo	k1gNnSc4	společenstvo
od	od	k7c2	od
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přebýval	přebývat	k5eAaImAgMnS	přebývat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
toužil	toužit	k5eAaImAgInS	toužit
prsten	prsten	k1gInSc4	prsten
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
Gluma	Glumum	k1gNnSc2	Glumum
zkrotí	zkrotit	k5eAaPmIp3nS	zkrotit
a	a	k8xC	a
přiměje	přimět	k5eAaPmIp3nS	přimět
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
Froda	Froda	k1gMnSc1	Froda
se	se	k3xPyFc4	se
Samem	Samos	k1gInSc7	Samos
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
Černé	Černá	k1gFnSc3	Černá
bráně	brána	k1gFnSc3	brána
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
obklopené	obklopený	k2eAgFnSc2d1	obklopená
horami	hora	k1gFnPc7	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
silně	silně	k6eAd1	silně
střežena	střežit	k5eAaImNgFnS	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k6eAd1	Glum
pak	pak	k6eAd1	pak
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
hledání	hledání	k1gNnSc6	hledání
narazí	narazit	k5eAaPmIp3nP	narazit
hobiti	hobit	k1gMnPc1	hobit
s	s	k7c7	s
Glumem	Glum	k1gInSc7	Glum
na	na	k7c4	na
družinu	družina	k1gFnSc4	družina
Faramira	Faramir	k1gInSc2	Faramir
<g/>
,	,	kIx,	,
Boromirova	Boromirův	k2eAgMnSc4d1	Boromirův
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
tamní	tamní	k2eAgFnSc6d1	tamní
končině	končina	k1gFnSc6	končina
hlídá	hlídat	k5eAaImIp3nS	hlídat
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
Gondoru	Gondor	k1gInSc2	Gondor
před	před	k7c7	před
Sauronovými	Sauronův	k2eAgMnPc7d1	Sauronův
zlými	zlý	k2eAgMnPc7d1	zlý
služebníky	služebník	k1gMnPc7	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Faramir	Faramir	k1gInSc1	Faramir
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
prstenu	prsten	k1gInSc6	prsten
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Frodo	Frodo	k1gNnSc4	Frodo
nese	nést	k5eAaImIp3nS	nést
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yRgInSc4	jaký
prsten	prsten	k1gInSc4	prsten
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
málem	málem	k6eAd1	málem
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
sám	sám	k3xTgMnSc1	sám
zatouží	zatoužit	k5eAaPmIp3nS	zatoužit
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
však	však	k9	však
hobity	hobit	k1gMnPc4	hobit
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
pod	pod	k7c7	pod
Glumovým	Glumův	k2eAgNnSc7d1	Glumovo
vedením	vedení	k1gNnSc7	vedení
dojdou	dojít	k5eAaPmIp3nP	dojít
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
průsmyku	průsmyk	k1gInSc3	průsmyk
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vede	vést	k5eAaImIp3nS	vést
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dojdou	dojít	k5eAaPmIp3nP	dojít
k	k	k7c3	k
podzemním	podzemní	k2eAgInPc3d1	podzemní
tunelům	tunel	k1gInPc3	tunel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ústí	ústit	k5eAaImIp3nP	ústit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Mordoru	Mordor	k1gInSc6	Mordor
za	za	k7c7	za
hřebenem	hřeben	k1gInSc7	hřeben
hraničních	hraniční	k2eAgFnPc2d1	hraniční
hor.	hor.	k?	hor.
Glum	Gluma	k1gFnPc2	Gluma
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
tunelů	tunel	k1gInPc2	tunel
zavede	zavést	k5eAaPmIp3nS	zavést
a	a	k8xC	a
potom	potom	k6eAd1	potom
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k6eAd1	Glum
totiž	totiž	k9	totiž
stále	stále	k6eAd1	stále
touží	toužit	k5eAaImIp3nP	toužit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Froda	Froda	k1gFnSc1	Froda
zbaví	zbavit	k5eAaPmIp3nS	zbavit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Oduly	odout	k5eAaPmAgFnP	odout
<g/>
,	,	kIx,	,
obrovské	obrovský	k2eAgFnSc2d1	obrovská
pavoučice	pavoučice	k1gFnSc2	pavoučice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tunely	tunel	k1gInPc4	tunel
obývá	obývat	k5eAaImIp3nS	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Odula	odout	k5eAaPmAgFnS	odout
skutečně	skutečně	k6eAd1	skutečně
hobity	hobit	k1gMnPc4	hobit
přepadne	přepadnout	k5eAaPmIp3nS	přepadnout
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
jedem	jed	k1gInSc7	jed
ochromí	ochromit	k5eAaPmIp3nS	ochromit
Froda	Froda	k1gFnSc1	Froda
<g/>
.	.	kIx.	.
</s>
<s>
Sam	Sam	k1gMnSc1	Sam
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Frodo	Frodo	k1gNnSc4	Frodo
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
prsten	prsten	k1gInSc4	prsten
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
nevědomky	nevědomky	k6eAd1	nevědomky
zabrání	zabrání	k1gNnSc2	zabrání
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
prstenu	prsten	k1gInSc3	prsten
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vzápětí	vzápětí	k6eAd1	vzápětí
Frodovo	Frodův	k2eAgNnSc4d1	Frodovo
tělo	tělo	k1gNnSc4	tělo
naleznou	nalézt	k5eAaBmIp3nP	nalézt
skřeti	skřet	k1gMnPc1	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Sam	Sam	k1gMnSc1	Sam
je	být	k5eAaImIp3nS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
dokončit	dokončit	k5eAaPmF	dokončit
poslání	poslání	k1gNnSc3	poslání
sám	sám	k3xTgInSc4	sám
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
donést	donést	k5eAaPmF	donést
k	k	k7c3	k
Hoře	hora	k1gFnSc3	hora
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
však	však	k9	však
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Frodo	Frodo	k1gNnSc1	Frodo
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
omráčen	omráčen	k2eAgInSc1d1	omráčen
<g/>
.	.	kIx.	.
</s>
