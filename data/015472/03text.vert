<s>
Edith	Edith	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
</s>
<s>
Edith	Edith	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
</s>
<s>
V	v	k7c6
roli	role	k1gFnSc6
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1901	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1909	#num#	k4
Předchůdkyně	předchůdkyně	k1gFnSc2
</s>
<s>
Ida	Ida	k1gFnSc1
Saxton	Saxton	k1gInSc1
McKinleyová	McKinleyová	k1gFnSc1
Nástupkyně	nástupkyně	k1gFnSc1
</s>
<s>
Helen	Helena	k1gFnPc2
Herron	Herron	k1gMnSc1
Taftová	taftový	k2eAgFnSc5d1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Republikánka	republikánka	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1861	#num#	k4
Norwich	Norwich	k1gMnSc1
Connecticut	Connecticut	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1948	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Oyster	Oyster	k1gMnSc1
Bay	Bay	k1gMnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Youngs	Youngs	k6eAd1
Memorial	Memorial	k1gInSc1
Cemetery	Cemeter	k1gInPc7
Choť	choť	k1gMnSc1
</s>
<s>
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Charles	Charles	k1gMnSc1
Carow	Carow	k1gMnSc1
a	a	k8xC
Gertrude	Gertrud	k1gInSc5
Elizabeth	Elizabeth	k1gFnSc4
Tyler	Tyler	k1gInSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Theodore	Theodor	k1gMnSc5
Jr	Jr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kermit	Kermita	k1gFnPc2
Ethel	Ethel	k1gMnSc1
Archibald	Archibald	k1gMnSc1
Quentin	Quentin	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Alice	Alice	k1gFnSc1
Rooseveltová	Rooseveltová	k1gFnSc1
Longworthová	Longworthová	k1gFnSc1
(	(	kIx(
<g/>
nevlastní	vlastní	k2eNgFnSc1d1
dcera	dcera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Quentin	Quentin	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
II	II	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
III	III	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Cornelius	Cornelius	k1gMnSc1
V.	V.	kA
S.	S.	kA
Roosevelt	Roosevelt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Grace	Grace	k1gMnSc1
Green	Grena	k1gFnPc2
Roosevelt	Roosevelt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Oyster	Oyster	k1gMnSc1
Bay	Bay	k1gFnSc2
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Miss	miss	k1gFnSc1
Comstock	Comstocka	k1gFnPc2
<g/>
´	´	k?
<g/>
s	s	k7c7
School	Schoola	k1gFnPc2
Zaměstnání	zaměstnání	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
Profese	profese	k1gFnSc1
</s>
<s>
politička	politička	k1gFnSc1
Podpis	podpis	k1gInSc1
</s>
<s>
Commons	Commons	k6eAd1
</s>
<s>
Edith	Edith	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Edith	Edith	k1gInSc1
Kermit	Kermit	k1gInSc1
Carow	Carow	k1gFnSc1
Rooseveltová	Rooseveltová	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1861	#num#	k4
<g/>
,	,	kIx,
Norwich	Norwich	k1gInSc1
<g/>
,	,	kIx,
Connecticut	Connecticut	k1gInSc1
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1948	#num#	k4
<g/>
,	,	kIx,
Oyster	Oyster	k1gMnSc1
Bay	Bay	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
druhou	druhý	k4xOgFnSc7
manželkou	manželka	k1gFnSc7
prezidenta	prezident	k1gMnSc2
USA	USA	kA
Theodora	Theodor	k1gMnSc2
Roosevelta	Roosevelt	k1gMnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1901	#num#	k4
až	až	k9
1909	#num#	k4
vykonávala	vykonávat	k5eAaImAgFnS
funkci	funkce	k1gFnSc4
první	první	k4xOgFnSc2
dámy	dáma	k1gFnSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
majetným	majetný	k2eAgMnPc3d1
kupeckým	kupecký	k2eAgMnPc3d1
rodičům	rodič	k1gMnPc3
Gertrude	Gertrud	k1gInSc5
Tylerové	Tylerový	k2eAgFnSc6d1
Carowové	Carowové	k2eAgFnSc3d1
a	a	k8xC
Charlesi	Charles	k1gMnSc3
Carowovi	Carowa	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvovala	navštěvovat	k5eAaImAgFnS
školu	škola	k1gFnSc4
pro	pro	k7c4
dcery	dcera	k1gFnPc4
z	z	k7c2
lepších	dobrý	k2eAgFnPc2d2
rodin	rodina	k1gFnPc2
Miss	miss	k1gFnSc1
Comstock	Comstock	k1gInSc1
<g/>
´	´	k?
<g/>
s	s	k7c7
School	Schoola	k1gFnPc2
na	na	k7c6
Manhattanu	Manhattan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Theodorem	Theodor	k1gMnSc7
Rooseveltem	Roosevelt	k1gMnSc7
se	se	k3xPyFc4
znala	znát	k5eAaImAgFnS
od	od	k7c2
dětství	dětství	k1gNnSc2
<g/>
,	,	kIx,
vzali	vzít	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1886	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rooseveltovi	Rooseveltův	k2eAgMnPc5d1
měli	mít	k5eAaImAgMnP
čtyři	čtyři	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
dceru	dcera	k1gFnSc4
<g/>
:	:	kIx,
Theodora	Theodora	k1gFnSc1
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kermita	Kermita	k1gMnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ethel	Ethel	k1gMnSc1
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Archibalda	Archibalda	k1gMnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Quentina	Quentina	k1gFnSc1
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Theodor	Theodor	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
po	po	k7c6
atentátu	atentát	k1gInSc6
na	na	k7c4
McKinleyho	McKinley	k1gMnSc4
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
Edith	Edith	k1gMnSc1
byla	být	k5eAaImAgNnP
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažila	snažil	k1gMnSc2
se	se	k3xPyFc4
chránit	chránit	k5eAaImF
rodinu	rodina	k1gFnSc4
před	před	k7c7
veřejností	veřejnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
prezidentova	prezidentův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
přijala	přijmout	k5eAaPmAgFnS
placenou	placený	k2eAgFnSc4d1
spolupracovnici	spolupracovnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
poprvé	poprvé	k6eAd1
nazvala	nazvat	k5eAaPmAgFnS,k5eAaBmAgFnS
prezidentovo	prezidentův	k2eAgNnSc4d1
bydliště	bydliště	k1gNnSc4
jako	jako	k8xS,k8xC
Bílý	bílý	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
,	,	kIx,
spolupracovala	spolupracovat	k5eAaImAgFnS
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
renovaci	renovace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
shromáždit	shromáždit	k5eAaPmF
portréty	portrét	k1gInPc4
všech	všecek	k3xTgFnPc2
dosavadních	dosavadní	k2eAgFnPc2d1
prvních	první	k4xOgFnPc2
dam	dáma	k1gFnPc2
a	a	k8xC
dala	dát	k5eAaPmAgFnS
je	on	k3xPp3gMnPc4
vystavit	vystavit	k5eAaPmF
(	(	kIx(
<g/>
tyto	tento	k3xDgInPc4
obrazy	obraz	k1gInPc4
dodnes	dodnes	k6eAd1
visí	viset	k5eAaImIp3nS
v	v	k7c6
Bílém	bílý	k2eAgInSc6d1
domě	dům	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgNnP
velmi	velmi	k6eAd1
nakloněna	nakloněn	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
,	,	kIx,
využila	využít	k5eAaPmAgFnS
svého	svůj	k3xOyFgInSc2
vlivu	vliv	k1gInSc2
k	k	k7c3
založení	založení	k1gNnSc3
Národní	národní	k2eAgFnSc2d1
umělecké	umělecký	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgMnS
její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
zaneprázdněn	zaneprázdněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
přijímala	přijímat	k5eAaImAgFnS
jeho	jeho	k3xOp3gMnPc4
hosty	host	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
prostředník	prostředník	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
anglický	anglický	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
jí	jíst	k5eAaImIp3nS
posílal	posílat	k5eAaImAgInS
v	v	k7c6
soukromých	soukromý	k2eAgFnPc6d1
dopisem	dopis	k1gInSc7
popis	popis	k1gInSc4
aktuálního	aktuální	k2eAgNnSc2d1
dění	dění	k1gNnSc2
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
to	ten	k3xDgNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
velké	velký	k2eAgInPc1d1
diplomatické	diplomatický	k2eAgInPc1d1
faux	faux	k1gInSc1
pas	pas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Edith	Editha	k1gFnPc2
Rooseveltová	Rooseveltová	k1gFnSc1
zavedla	zavést	k5eAaPmAgFnS
pravidelné	pravidelný	k2eAgFnPc4d1
schůzky	schůzka	k1gFnPc4
s	s	k7c7
ženami	žena	k1gFnPc7
členů	člen	k1gMnPc2
kabinetu	kabinet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
tzv.	tzv.	kA
první	první	k4xOgInSc4
pár	pár	k1gInSc4
(	(	kIx(
<g/>
tj.	tj.	kA
First	First	k1gInSc4
couple	couple	k6eAd1
<g/>
)	)	kIx)
vnesl	vnést	k5eAaPmAgMnS
do	do	k7c2
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
jistou	jistý	k2eAgFnSc4d1
aristokratickou	aristokratický	k2eAgFnSc4d1
eleganci	elegance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
prezidentskou	prezidentský	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
večer	večer	k6eAd1
před	před	k7c7
uvedením	uvedení	k1gNnSc7
nového	nový	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Williama	William	k1gMnSc2
Tafta	Taft	k1gMnSc2
do	do	k7c2
funkce	funkce	k1gFnSc2
pozvala	pozvat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
ženu	žena	k1gFnSc4
na	na	k7c4
večeři	večeře	k1gFnSc4
a	a	k8xC
prohlídku	prohlídka	k1gFnSc4
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
manžela	manžel	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
cestovala	cestovat	k5eAaImAgFnS
po	po	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
synem	syn	k1gMnSc7
Kermitem	Kermit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Byla	být	k5eAaImAgFnS
proti	proti	k7c3
volebnímu	volební	k2eAgNnSc3d1
právu	právo	k1gNnSc3
žen	žena	k1gFnPc2
a	a	k8xC
zasazovala	zasazovat	k5eAaImAgFnS
se	se	k3xPyFc4
o	o	k7c6
ukončení	ukončení	k1gNnSc6
kandidatury	kandidatura	k1gFnSc2
muže	muž	k1gMnPc4
její	její	k3xOp3gFnSc2
neteře	neteř	k1gFnSc2
Franklina	Franklin	k2eAgMnSc4d1
Roosevelta	Roosevelt	k1gMnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
odmítala	odmítat	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
1948	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
na	na	k7c4
srdeční	srdeční	k2eAgFnSc4d1
chorobu	choroba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
Její	její	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
americkým	americký	k2eAgMnSc7d1
generálem	generál	k1gMnSc7
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Armády	armáda	k1gFnSc2
USA	USA	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
brigádního	brigádní	k2eAgMnSc2d1
generála	generál	k1gMnSc2
ve	v	k7c6
funkci	funkce	k1gFnSc6
zástupce	zástupce	k1gMnSc2
velitele	velitel	k1gMnSc2
divize	divize	k1gFnSc2
osobně	osobně	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
dne	den	k1gInSc2
spojeneckého	spojenecký	k2eAgNnSc2d1
vylodění	vylodění	k1gNnSc2
v	v	k7c6
Normandii	Normandie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Kindred	Kindred	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Darryl	Darryl	k1gInSc1
Roger	Roger	k1gInSc1
Lundy	Lunda	k1gMnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc2
Peerage	Peerag	k1gMnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GASSERT	GASSERT	kA
<g/>
,	,	kIx,
Philipp	Philipp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
dámy	dáma	k1gFnPc1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
133	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Dokonalá	dokonalý	k2eAgFnSc1d1
první	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
První	první	k4xOgFnPc1
dámy	dáma	k1gFnPc1
USA	USA	kA
</s>
<s>
Martha	Martha	k1gFnSc1
Washingtonová	Washingtonový	k2eAgFnSc1d1
•	•	k?
Abigail	Abigaila	k1gFnPc2
Adamsová	Adamsový	k2eAgFnSc1d1
•	•	k?
Martha	Martha	k1gFnSc1
Jeffersonová	Jeffersonová	k1gFnSc1
Randolphová	Randolphová	k1gFnSc1
•	•	k?
Dolley	Dollea	k1gFnSc2
Madisonová	Madisonová	k1gFnSc1
•	•	k?
Elizabeth	Elizabeth	k1gFnSc1
Monroeová	Monroeová	k1gFnSc1
•	•	k?
Louisa	Louisa	k?
Adamsová	Adamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Emily	Emil	k1gMnPc4
Donelsonová	Donelsonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
Sarah	Sarah	k1gFnSc1
Yorke	Yorke	k1gFnSc1
Jacksonová	Jacksonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rachel	Rachel	k1gInSc1
Jacksonová	Jacksonová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Angelica	Angelica	k1gFnSc1
Van	van	k1gInSc1
Burenová	Burenová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hannah	Hannah	k1gInSc1
Van	van	k1gInSc1
Burenová	Burenová	k1gFnSc1
•	•	k?
Anna	Anna	k1gFnSc1
Harrisonová	Harrisonová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
Jane	Jan	k1gMnSc5
Irwin	Irwin	k1gMnSc1
Harrisonová	Harrisonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Letitia	Letitia	k1gFnSc1
Tylerová	Tylerová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Priscilla	Priscilla	k1gFnSc1
Tylerová	Tylerová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Julia	Julius	k1gMnSc2
Tylerová	Tylerový	k2eAgFnSc1d1
•	•	k?
Sarah	Sarah	k1gFnSc1
Polková	polkový	k2eAgFnSc1d1
•	•	k?
Margaret	Margareta	k1gFnPc2
Taylorová	Taylorová	k1gFnSc1
•	•	k?
Abigail	Abigail	k1gInSc1
Fillmoreová	Fillmoreová	k1gFnSc1
•	•	k?
Jane	Jan	k1gMnSc5
Pierceová	Pierceová	k1gFnSc1
•	•	k?
Harriet	Harrieta	k1gFnPc2
Laneová	Laneový	k2eAgFnSc1d1
•	•	k?
Mary	Mary	k1gFnSc1
Toddová	Toddová	k1gFnSc1
Lincolnová	Lincolnová	k1gFnSc1
•	•	k?
Eliza	Eliza	k1gFnSc1
McCardle	McCardle	k1gFnSc1
Johnsonová	Johnsonová	k1gFnSc1
•	•	k?
Julia	Julius	k1gMnSc2
Grantová	grantový	k2eAgFnSc1d1
•	•	k?
Lucy	Luca	k1gMnSc2
Hayesová	Hayesový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Lucretia	Lucretia	k1gFnSc1
Garfieldová	Garfieldová	k1gFnSc1
•	•	k?
Ellen	Ellna	k1gFnPc2
Arthurová	Arthurový	k2eAgFnSc1d1
•	•	k?
(	(	kIx(
<g/>
Mary	Mary	k1gFnSc1
McElroyová	McElroyová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
Rose	Ros	k1gMnSc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Frances	Francesa	k1gFnPc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
•	•	k?
Caroline	Carolin	k1gInSc5
Harrisonová	Harrisonová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Mary	Mary	k1gFnSc1
Harrisonová	Harrisonová	k1gFnSc1
McKeeová	McKeeová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Frances	Francesa	k1gFnPc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
•	•	k?
Ida	Ida	k1gFnSc1
McKinleyová	McKinleyová	k1gFnSc1
•	•	k?
Edith	Edith	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
•	•	k?
Helen	Helena	k1gFnPc2
Taftová	taftový	k2eAgFnSc1d1
•	•	k?
Ellen	Ellen	k1gInSc1
Wilsonová	Wilsonová	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Edith	Editha	k1gFnPc2
Wilsonová	Wilsonová	k1gFnSc1
•	•	k?
Florence	Florenc	k1gFnSc2
Hardingová	Hardingový	k2eAgFnSc1d1
•	•	k?
Grace	Grace	k1gFnSc1
Coolidgeová	Coolidgeový	k2eAgFnSc1d1
•	•	k?
Lou	Lou	k1gFnSc1
Hooverová	Hooverová	k1gFnSc1
•	•	k?
Eleanor	Eleanor	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
•	•	k?
Bess	Bessa	k1gFnPc2
Trumanová	Trumanový	k2eAgFnSc1d1
•	•	k?
Mamie	Mamie	k1gFnSc1
Eisenhowerová	Eisenhowerová	k1gFnSc1
•	•	k?
Jacqueline	Jacquelin	k1gInSc5
Kennedyová	Kennedyový	k2eAgFnSc1d1
•	•	k?
Lady	lady	k1gFnSc1
Bird	Bird	k1gInSc1
Johnsonová	Johnsonová	k1gFnSc1
•	•	k?
Pat	pata	k1gFnPc2
Nixonová	Nixonový	k2eAgFnSc1d1
•	•	k?
Betty	Betty	k1gFnSc1
Fordová	Fordová	k1gFnSc1
•	•	k?
Rosalynn	Rosalynna	k1gFnPc2
Carterová	Carterový	k2eAgFnSc1d1
•	•	k?
Nancy	Nancy	k1gFnSc1
Reaganová	Reaganová	k1gFnSc1
•	•	k?
Barbara	Barbara	k1gFnSc1
Bushová	Bushová	k1gFnSc1
•	•	k?
Hillary	Hillara	k1gFnSc2
Clintonová	Clintonová	k1gFnSc1
•	•	k?
Laura	Laura	k1gFnSc1
Bushová	Bushová	k1gFnSc1
•	•	k?
Michelle	Michelle	k1gInSc1
Obamová	Obamová	k1gFnSc1
•	•	k?
Melania	Melanium	k1gNnSc2
Trumpová	Trumpová	k1gFnSc1
•	•	k?
Jill	Jill	k1gInSc1
Bidenová	Bidenová	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118920715	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
2102	#num#	k4
7502	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79085128	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
35255319	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79085128	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
