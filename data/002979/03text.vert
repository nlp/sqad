<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
(	(	kIx(	(
<g/>
Mygalomorphae	Mygalomorphae	k1gInSc1	Mygalomorphae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
členovci	členovec	k1gMnPc1	členovec
řádu	řád	k1gInSc2	řád
pavouci	pavouk	k1gMnPc5	pavouk
(	(	kIx(	(
<g/>
Araneida	Araneida	k1gFnSc1	Araneida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
suchozemským	suchozemský	k2eAgMnPc3d1	suchozemský
bezobratlým	bezobratlý	k2eAgMnPc3d1	bezobratlý
živočichům	živočich	k1gMnPc3	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dokonalí	dokonalý	k2eAgMnPc1d1	dokonalý
lovci	lovec	k1gMnPc1	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
téměř	téměř	k6eAd1	téměř
nezměnili	změnit	k5eNaPmAgMnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
chovaní	chovaný	k2eAgMnPc1d1	chovaný
sklípkani	sklípkan	k1gMnPc1	sklípkan
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Sklípkanovití	Sklípkanovitý	k2eAgMnPc1d1	Sklípkanovitý
(	(	kIx(	(
<g/>
Theraphosidae	Theraphosidae	k1gNnSc7	Theraphosidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkanům	sklípkan	k1gMnPc3	sklípkan
se	se	k3xPyFc4	se
lidově	lidově	k6eAd1	lidově
říká	říkat	k5eAaImIp3nS	říkat
vlastně	vlastně	k9	vlastně
nesprávně	správně	k6eNd1	správně
tarantule	tarantule	k1gFnSc2	tarantule
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
přísluší	příslušet	k5eAaImIp3nS	příslušet
původně	původně	k6eAd1	původně
pavoukům	pavouk	k1gMnPc3	pavouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slíďákovití	slíďákovitý	k2eAgMnPc1d1	slíďákovitý
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc4	von
Linné	Linná	k1gFnSc2	Linná
a	a	k8xC	a
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
jihoitalské	jihoitalský	k2eAgFnSc6d1	jihoitalská
provincii	provincie	k1gFnSc6	provincie
Tarent	Tarent	k1gInSc4	Tarent
je	být	k5eAaImIp3nS	být
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
aranea	aranea	k6eAd1	aranea
tarantula	tarantula	k1gFnSc1	tarantula
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
cizojazyčné	cizojazyčný	k2eAgFnPc1d1	cizojazyčná
formy	forma	k1gFnPc1	forma
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
často	často	k6eAd1	často
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
zcela	zcela	k6eAd1	zcela
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
velkých	velký	k2eAgMnPc2d1	velký
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
sklípkanů	sklípkan	k1gMnPc2	sklípkan
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
osm	osm	k4xCc4	osm
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rychle	rychle	k6eAd1	rychle
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
sklípkanů	sklípkan	k1gMnPc2	sklípkan
čeledi	čeleď	k1gFnSc2	čeleď
Theraphosidaese	Theraphosidaese	k1gFnSc1	Theraphosidaese
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
chodidla	chodidlo	k1gNnSc2	chodidlo
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
chloupky	chloupek	k1gInPc1	chloupek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rozštěpeny	rozštěpen	k2eAgFnPc4d1	rozštěpena
do	do	k7c2	do
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
chloupků	chloupek	k1gInPc2	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
i	i	k9	i
po	po	k7c6	po
hladkých	hladký	k2eAgFnPc6d1	hladká
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
šplhat	šplhat	k5eAaImF	šplhat
po	po	k7c6	po
skle	sklo	k1gNnSc6	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
klepítka	klepítko	k1gNnPc4	klepítko
(	(	kIx(	(
<g/>
chelicery	chelicer	k1gInPc1	chelicer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgFnPc2	jenž
kořist	kořist	k1gFnSc1	kořist
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
vstříknutím	vstříknutí	k1gNnSc7	vstříknutí
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Chelicery	Chelicera	k1gFnPc1	Chelicera
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
ortognátně	ortognátně	k6eAd1	ortognátně
orientovány	orientovat	k5eAaBmNgFnP	orientovat
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chelicery	chelicera	k1gFnPc1	chelicera
otevírají	otevírat	k5eAaImIp3nP	otevírat
podélně	podélně	k6eAd1	podélně
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
pavoukova	pavoukův	k2eAgNnSc2d1	Pavoukovo
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
klepítek	klepítko	k1gNnPc2	klepítko
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
makadla	makadlo	k1gNnPc4	makadlo
(	(	kIx(	(
<g/>
pedipalpy	pedipalp	k1gInPc1	pedipalp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
opoře	opora	k1gFnSc3	opora
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
a	a	k8xC	a
k	k	k7c3	k
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
těchto	tento	k3xDgNnPc2	tento
makadel	makadlo	k1gNnPc2	makadlo
mají	mít	k5eAaImIp3nP	mít
samečci	sameček	k1gMnPc1	sameček
tzv.	tzv.	kA	tzv.
samčí	samčí	k2eAgInPc1d1	samčí
bulby	bulbus	k1gInPc1	bulbus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
samci	samec	k1gMnPc1	samec
přenáší	přenášet	k5eAaImIp3nP	přenášet
sperma	sperma	k1gNnSc4	sperma
<g/>
,	,	kIx,	,
když	když	k8xS	když
hledají	hledat	k5eAaImIp3nP	hledat
samici	samice	k1gFnSc4	samice
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spářit	spářit	k5eAaPmF	spářit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgFnPc1	čtyři
snovací	snovací	k2eAgFnPc1d1	snovací
bradavky	bradavka	k1gFnPc1	bradavka
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
mohou	moct	k5eAaImIp3nP	moct
tkát	tkát	k5eAaImF	tkát
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
netkají	tkát	k5eNaImIp3nP	tkát
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
pavučinu	pavučina	k1gFnSc4	pavučina
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
při	při	k7c6	při
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
,	,	kIx,	,
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
kokonu	kokon	k1gInSc2	kokon
nebo	nebo	k8xC	nebo
při	při	k7c6	při
svlékání	svlékání	k1gNnSc6	svlékání
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
mnozí	mnohý	k2eAgMnPc1d1	mnohý
sklípkani	sklípkan	k1gMnPc1	sklípkan
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
žahavé	žahavý	k2eAgInPc4d1	žahavý
chloupky	chloupek	k1gInPc4	chloupek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkan	sklípkan	k1gMnSc1	sklípkan
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
párů	pár	k1gInPc2	pár
končetin	končetina	k1gFnPc2	končetina
První	první	k4xOgInSc1	první
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
přeměněn	přeměněn	k2eAgMnSc1d1	přeměněn
na	na	k7c4	na
klepítka	klepítko	k1gNnPc4	klepítko
(	(	kIx(	(
<g/>
chelicery	chelicer	k1gInPc1	chelicer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
jedová	jedový	k2eAgFnSc1d1	jedová
žláza	žláza	k1gFnSc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
přeměněn	přeměněn	k2eAgMnSc1d1	přeměněn
na	na	k7c4	na
makadla	makadlo	k1gNnPc4	makadlo
(	(	kIx(	(
<g/>
pedipalpy	pedipalp	k1gInPc1	pedipalp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uchycování	uchycování	k1gNnSc3	uchycování
a	a	k8xC	a
ke	k	k7c3	k
žvýkání	žvýkání	k1gNnSc3	žvýkání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
pomocné	pomocný	k2eAgNnSc4d1	pomocné
pářicí	pářicí	k2eAgNnSc4d1	pářicí
ústrojí	ústrojí	k1gNnSc4	ústrojí
u	u	k7c2	u
samečků	sameček	k1gMnPc2	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
až	až	k8xS	až
šestý	šestý	k4xOgInSc1	šestý
pár	pár	k1gInSc1	pár
končetin	končetina	k1gFnPc2	končetina
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
osmi	osm	k4xCc2	osm
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
coxa	coxa	k1gFnSc1	coxa
<g/>
,	,	kIx,	,
trochanter	trochanter	k1gInSc1	trochanter
<g/>
,	,	kIx,	,
femur	femur	k1gMnSc1	femur
<g/>
,	,	kIx,	,
patella	patella	k1gMnSc1	patella
<g/>
,	,	kIx,	,
tibia	tibia	k1gFnSc1	tibia
<g/>
,	,	kIx,	,
metatarsus	metatarsus	k1gInSc1	metatarsus
<g/>
,	,	kIx,	,
tarsus	tarsus	k1gInSc1	tarsus
<g/>
,	,	kIx,	,
tarsální	tarsální	k2eAgInSc1d1	tarsální
drápek	drápek	k1gInSc1	drápek
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
nesou	nést	k5eAaImIp3nP	nést
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
chloupky	chloupek	k1gInPc1	chloupek
(	(	kIx(	(
<g/>
scopulae	scopulae	k1gFnSc1	scopulae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chloupky	chloupek	k1gInPc1	chloupek
slouží	sloužit	k5eAaImIp3nP	sloužit
například	například	k6eAd1	například
jako	jako	k9	jako
přísavky	přísavka	k1gFnSc2	přísavka
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sklípkanům	sklípkan	k1gMnPc3	sklípkan
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
hladkých	hladký	k2eAgInPc6d1	hladký
površích	povrch	k1gInPc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
na	na	k7c4	na
stromové	stromový	k2eAgInPc4d1	stromový
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInPc4d1	zemní
a	a	k8xC	a
podzemní	podzemní	k2eAgInPc4d1	podzemní
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Stromoví	stromový	k2eAgMnPc1d1	stromový
sklípkani	sklípkan	k1gMnPc1	sklípkan
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
pralesích	prales	k1gInPc6	prales
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přebývají	přebývat	k5eAaImIp3nP	přebývat
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
také	také	k9	také
stavějí	stavět	k5eAaImIp3nP	stavět
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgMnPc1d1	zemní
sklípkani	sklípkan	k1gMnPc1	sklípkan
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
v	v	k7c6	v
úkrytech	úkryt	k1gInPc6	úkryt
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
čí	čí	k3xOyQgInPc4	čí
kousky	kousek	k1gInPc4	kousek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
a	a	k8xC	a
opouští	opouštět	k5eAaImIp3nS	opouštět
je	on	k3xPp3gMnPc4	on
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgMnPc1d1	podzemní
sklípkani	sklípkan	k1gMnPc1	sklípkan
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
chodbičkách	chodbička	k1gFnPc6	chodbička
tvaru	tvar	k1gInSc2	tvar
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
vystlány	vystlat	k5eAaPmNgFnP	vystlat
pavučinou	pavučina	k1gFnSc7	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
tráví	trávit	k5eAaImIp3nP	trávit
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
nory	nora	k1gFnSc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
pouštní	pouštní	k2eAgInPc1d1	pouštní
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
hmyzem	hmyz	k1gInSc7	hmyz
či	či	k8xC	či
jinými	jiný	k2eAgMnPc7d1	jiný
pavoukovci	pavoukovec	k1gMnPc7	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
druhy	druh	k1gInPc1	druh
loví	lovit	k5eAaImIp3nP	lovit
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
malé	malý	k2eAgMnPc4d1	malý
hady	had	k1gMnPc4	had
<g/>
,	,	kIx,	,
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
stromové	stromový	k2eAgInPc1d1	stromový
druhy	druh	k1gInPc1	druh
také	také	k9	také
ptačí	ptačit	k5eAaImIp3nP	ptačit
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkan	sklípkan	k1gMnSc1	sklípkan
probodne	probodnout	k5eAaPmIp3nS	probodnout
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
klepítky	klepítko	k1gNnPc7	klepítko
a	a	k8xC	a
vstříkne	vstříknout	k5eAaPmIp3nS	vstříknout
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
jed	jed	k1gInSc4	jed
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ji	on	k3xPp3gFnSc4	on
ochromí	ochromit	k5eAaPmIp3nP	ochromit
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
sežere	sežrat	k5eAaPmIp3nS	sežrat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
vstříkne	vstříknout	k5eAaPmIp3nS	vstříknout
trávící	trávící	k2eAgInPc4d1	trávící
enzymy	enzym	k1gInPc4	enzym
a	a	k8xC	a
pozře	pozřít	k5eAaPmIp3nS	pozřít
již	již	k6eAd1	již
rozloženou	rozložený	k2eAgFnSc4d1	rozložená
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sklípkan	sklípkan	k1gMnSc1	sklípkan
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
rozložit	rozložit	k5eAaPmF	rozložit
(	(	kIx(	(
<g/>
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
<g/>
kůže	kůže	k1gFnSc1	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zformované	zformovaný	k2eAgInPc1d1	zformovaný
do	do	k7c2	do
takřka	takřka	k6eAd1	takřka
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
"	"	kIx"	"
<g/>
kuličky	kulička	k1gFnSc2	kulička
<g/>
"	"	kIx"	"
a	a	k8xC	a
odhozeny	odhozen	k2eAgFnPc1d1	odhozena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
sklípkani	sklípkan	k1gMnPc1	sklípkan
chováni	chován	k2eAgMnPc1d1	chován
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
v	v	k7c6	v
teráriu	terárium	k1gNnSc6	terárium
je	být	k5eAaImIp3nS	být
zajímavostí	zajímavost	k1gFnSc7	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
"	"	kIx"	"
<g/>
kuličky	kulička	k1gFnPc1	kulička
<g/>
"	"	kIx"	"
zpravidla	zpravidla	k6eAd1	zpravidla
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
teráriu	terárium	k1gNnSc6	terárium
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
obraných	obraný	k2eAgFnPc2d1	obraná
taktik	taktika	k1gFnPc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sklípkanů	sklípkan	k1gMnPc2	sklípkan
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
právě	právě	k9	právě
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
obranné	obranný	k2eAgNnSc4d1	obranné
postavení	postavení	k1gNnSc4	postavení
-	-	kIx~	-
vzpřímí	vzpřímit	k5eAaPmIp3nS	vzpřímit
hlavohruď	hlavohruď	k1gFnSc1	hlavohruď
a	a	k8xC	a
roztáhne	roztáhnout	k5eAaPmIp3nS	roztáhnout
klepítka	klepítko	k1gNnPc4	klepítko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
toto	tento	k3xDgNnSc1	tento
nepřítele	nepřítel	k1gMnSc4	nepřítel
neodradí	odradit	k5eNaPmIp3nS	odradit
<g/>
,	,	kIx,	,
bleskurychle	bleskurychle	k6eAd1	bleskurychle
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
a	a	k8xC	a
silně	silně	k6eAd1	silně
jej	on	k3xPp3gMnSc4	on
udeří	udeřit	k5eAaPmIp3nP	udeřit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ani	ani	k8xC	ani
toto	tento	k3xDgNnSc1	tento
nezapůsobí	zapůsobit	k5eNaPmIp3nS	zapůsobit
<g/>
,	,	kIx,	,
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
s	s	k7c7	s
jedovatým	jedovatý	k2eAgNnSc7d1	jedovaté
kousnutím	kousnutí	k1gNnSc7	kousnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
kousnutí	kousnutí	k1gNnSc4	kousnutí
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
smrtelně	smrtelně	k6eAd1	smrtelně
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Nejnebezpečnější	bezpečný	k2eNgInPc1d3	nejnebezpečnější
jsou	být	k5eAaImIp3nP	být
rody	rod	k1gInPc1	rod
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
zdraví	zdraví	k1gNnSc4	zdraví
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
sklípkani	sklípkan	k1gMnPc1	sklípkan
mají	mít	k5eAaImIp3nP	mít
jed	jed	k1gInSc4	jed
člověku	člověk	k1gMnSc6	člověk
neškodný	škodný	k2eNgMnSc1d1	neškodný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
obrany	obrana	k1gFnSc2	obrana
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
sklípkani	sklípkan	k1gMnPc1	sklípkan
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pavouk	pavouk	k1gMnSc1	pavouk
v	v	k7c4	v
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
nastaví	nastavit	k5eAaPmIp3nP	nastavit
proti	proti	k7c3	proti
útočníkovi	útočník	k1gMnSc3	útočník
zadeček	zadeček	k1gInSc4	zadeček
a	a	k8xC	a
rychlými	rychlý	k2eAgInPc7d1	rychlý
pohyby	pohyb	k1gInPc7	pohyb
zadních	zadní	k2eAgFnPc2d1	zadní
nohou	noha	k1gFnPc2	noha
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stírá	stírat	k5eAaImIp3nS	stírat
chloupky	chloupek	k1gInPc1	chloupek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
k	k	k7c3	k
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chloupky	chloupek	k1gInPc1	chloupek
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
svědění	svěděný	k2eAgMnPc1d1	svěděný
a	a	k8xC	a
pálení	pálený	k2eAgMnPc1d1	pálený
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc2	jejich
vdechnutí	vdechnutí	k1gNnSc2	vdechnutí
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
nebezpečnější	bezpečný	k2eNgNnSc1d2	nebezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Chloupky	chloupek	k1gInPc1	chloupek
se	se	k3xPyFc4	se
sklípkanovi	sklípkan	k1gMnSc3	sklípkan
obnovují	obnovovat	k5eAaImIp3nP	obnovovat
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
svleku	svléct	k5eAaPmIp1nSwK	svléct
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
druhu	druh	k1gInSc6	druh
obrany	obrana	k1gFnSc2	obrana
pavouk	pavouk	k1gMnSc1	pavouk
nadzvedne	nadzvednout	k5eAaPmIp3nS	nadzvednout
zadeček	zadeček	k1gInSc4	zadeček
a	a	k8xC	a
ostříkne	ostříknout	k5eAaPmIp3nS	ostříknout
útočníka	útočník	k1gMnSc4	útočník
proudem	proud	k1gInSc7	proud
trusu	trus	k1gInSc2	trus
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
taktiku	taktika	k1gFnSc4	taktika
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
sklípkanem	sklípkan	k1gMnSc7	sklípkan
je	být	k5eAaImIp3nS	být
sklípkan	sklípkan	k1gMnSc1	sklípkan
největší	veliký	k2eAgMnSc1d3	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
tělo	tělo	k1gNnSc4	tělo
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
12	[number]	k4	12
cm	cm	kA	cm
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc2	rozpětí
nohou	noha	k1gFnPc2	noha
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
blízkými	blízký	k2eAgFnPc7d1	blízká
příbuznými	příbuzná	k1gFnPc7	příbuzná
sklípkanů	sklípkan	k1gMnPc2	sklípkan
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
podřádu	podřád	k1gInSc2	podřád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sklípkánci	sklípkánek	k1gMnPc1	sklípkánek
rodu	rod	k1gInSc2	rod
Atypus	Atypus	k1gInSc4	Atypus
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Atypidae	Atypida	k1gFnSc2	Atypida
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samci	samec	k1gInSc3	samec
sklípkanů	sklípkan	k1gMnPc2	sklípkan
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dožívají	dožívat	k5eAaImIp3nP	dožívat
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
sklípkánků	sklípkánek	k1gInPc2	sklípkánek
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
červený	červený	k2eAgInSc4d1	červený
seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
zkamenělých	zkamenělý	k2eAgInPc2d1	zkamenělý
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
fosilií	fosilie	k1gFnPc2	fosilie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
zpětné	zpětný	k2eAgInPc4d1	zpětný
klíče	klíč	k1gInPc4	klíč
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
kmenovému	kmenový	k2eAgInSc3d1	kmenový
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
fosilní	fosilní	k2eAgFnPc4d1	fosilní
příbuzné	příbuzná	k1gFnPc4	příbuzná
dnešních	dnešní	k2eAgMnPc2d1	dnešní
sklípkanů	sklípkan	k1gMnPc2	sklípkan
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
úlomcích	úlomek	k1gInPc6	úlomek
jantaru	jantar	k1gInSc2	jantar
ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
devonu	devon	k1gInSc2	devon
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
staré	starý	k2eAgNnSc1d1	staré
téměř	téměř	k6eAd1	téměř
350	[number]	k4	350
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
310	[number]	k4	310
až	až	k9	až
240	[number]	k4	240
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
sklípkani	sklípkan	k1gMnPc1	sklípkan
nejhojnějším	hojný	k2eAgFnPc3d3	nejhojnější
druhům	druh	k1gInPc3	druh
pavouků	pavouk	k1gMnPc2	pavouk
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
ode	ode	k7c2	ode
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
od	od	k7c2	od
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
také	také	k9	také
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
sklípkani	sklípkan	k1gMnPc1	sklípkan
během	běh	k1gInSc7	běh
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
tělesně	tělesně	k6eAd1	tělesně
skoro	skoro	k6eAd1	skoro
vůbec	vůbec	k9	vůbec
nezměnili	změnit	k5eNaPmAgMnP	změnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
právu	právo	k1gNnSc6	právo
označováni	označovat	k5eAaImNgMnP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
živoucí	živoucí	k2eAgFnSc1d1	živoucí
fosilie	fosilie	k1gFnSc1	fosilie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
