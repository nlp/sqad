<s>
Riga	Riga	k1gFnSc1	Riga
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
:	:	kIx,	:
Rī	Rī	k1gFnSc1	Rī
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Daugavy	Daugava	k1gFnSc2	Daugava
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnPc1d1	západní
Dviny	Dvina	k1gFnPc1	Dvina
<g/>
)	)	kIx)	)
do	do	k7c2	do
Rižského	rižský	k2eAgInSc2d1	rižský
zálivu	záliv	k1gInSc2	záliv
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
307	[number]	k4	307
km2	km2	k4	km2
a	a	k8xC	a
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
702	[number]	k4	702
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgNnSc4d1	vzdělávací
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc4d1	politické
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc4d1	obchodní
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
středisko	středisko	k1gNnSc4	středisko
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
plní	plnit	k5eAaImIp3nS	plnit
i	i	k9	i
důležitou	důležitý	k2eAgFnSc4d1	důležitá
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
roli	role	k1gFnSc4	role
mezi	mezi	k7c7	mezi
západními	západní	k2eAgInPc7d1	západní
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Rigy	Riga	k1gFnSc2	Riga
<g/>
,	,	kIx,	,
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
hanzovního	hanzovní	k2eAgNnSc2d1	hanzovní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
;	;	kIx,	;
krom	krom	k7c2	krom
několika	několik	k4yIc2	několik
středověkých	středověký	k2eAgFnPc2d1	středověká
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
výraznou	výrazný	k2eAgFnSc7d1	výrazná
secesní	secesní	k2eAgFnSc7d1	secesní
architekturou	architektura	k1gFnSc7	architektura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
porovnatelná	porovnatelný	k2eAgFnSc1d1	porovnatelná
s	s	k7c7	s
městy	město	k1gNnPc7	město
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
nebo	nebo	k8xC	nebo
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
přijímaným	přijímaný	k2eAgNnSc7d1	přijímané
etymologickým	etymologický	k2eAgNnSc7d1	etymologické
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
jména	jméno	k1gNnSc2	jméno
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
německou	německý	k2eAgFnSc4d1	německá
zkomoleninu	zkomolenina	k1gFnSc4	zkomolenina
z	z	k7c2	z
livonštiny	livonština	k1gFnSc2	livonština
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
pobřeží	pobřeží	k1gNnSc2	pobřeží
kolem	kolem	k7c2	kolem
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Rigy	Riga	k1gFnSc2	Riga
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
sídlem	sídlo	k1gNnSc7	sídlo
kmene	kmen	k1gInSc2	kmen
Livů	Liv	k1gMnPc2	Liv
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mj.	mj.	kA	mj.
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
i	i	k8xC	i
středověkému	středověký	k2eAgNnSc3d1	středověké
Livonsku	Livonsko	k1gNnSc3	Livonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
existuje	existovat	k5eAaImIp3nS	existovat
slovo	slovo	k1gNnSc1	slovo
ringa	ringa	k1gFnSc1	ringa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
,	,	kIx,	,
oblouk	oblouk	k1gInSc1	oblouk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
četná	četný	k2eAgNnPc4d1	četné
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
okrouhlou	okrouhlý	k2eAgFnSc4d1	okrouhlá
zátočinu	zátočina	k1gFnSc4	zátočina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tu	tu	k6eAd1	tu
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
vyústěním	vyústění	k1gNnSc7	vyústění
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Daugava	Daugava	k1gFnSc1	Daugava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
teploty	teplota	k1gFnPc1	teplota
okolo	okolo	k7c2	okolo
-5	-5	k4	-5
<g/>
°	°	k?	°
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
mezi	mezi	k7c7	mezi
15	[number]	k4	15
a	a	k8xC	a
20	[number]	k4	20
<g/>
°	°	k?	°
C.	C.	kA	C.
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
přímořské	přímořský	k2eAgNnSc1d1	přímořské
<g/>
,	,	kIx,	,
se	s	k7c7	s
zřetelnou	zřetelný	k2eAgFnSc7d1	zřetelná
aktivitou	aktivita	k1gFnSc7	aktivita
cyklonů	cyklon	k1gInPc2	cyklon
a	a	k8xC	a
značným	značný	k2eAgInSc7d1	značný
podílem	podíl	k1gInSc7	podíl
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
akademických	akademický	k2eAgFnPc2d1	akademická
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Latvijas	Latvijas	k1gMnSc1	Latvijas
Universitā	Universitā	k1gMnSc1	Universitā
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Rī	Rī	k1gMnSc1	Rī
Tehniskā	Tehniskā	k1gMnSc1	Tehniskā
universitā	universitā	k?	universitā
<g/>
)	)	kIx)	)
a	a	k8xC	a
univerzity	univerzita	k1gFnSc2	univerzita
Stradin	Stradina	k1gFnPc2	Stradina
(	(	kIx(	(
<g/>
Rī	Rī	k1gFnSc1	Rī
Stradiņ	Stradiņ	k1gFnSc2	Stradiņ
universitā	universitā	k?	universitā
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
zasedá	zasedat	k5eAaImIp3nS	zasedat
i	i	k9	i
lotyšský	lotyšský	k2eAgInSc1d1	lotyšský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Saeima	Saeima	k1gFnSc1	Saeima
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Rižském	rižský	k2eAgInSc6d1	rižský
hradě	hrad	k1gInSc6	hrad
sídlí	sídlet	k5eAaImIp3nS	sídlet
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
obchodní	obchodní	k2eAgInSc1d1	obchodní
a	a	k8xC	a
turistický	turistický	k2eAgInSc1d1	turistický
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
komerční	komerční	k2eAgFnSc2d1	komerční
a	a	k8xC	a
turistické	turistický	k2eAgFnSc2d1	turistická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Riga	Riga	k1gFnSc1	Riga
jako	jako	k8xC	jako
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cestovním	cestovní	k2eAgInSc7d1	cestovní
uzlem	uzel	k1gInSc7	uzel
místní	místní	k2eAgFnSc2d1	místní
silniční	silniční	k2eAgFnSc2d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
turistů	turist	k1gMnPc2	turist
cestuje	cestovat	k5eAaImIp3nS	cestovat
do	do	k7c2	do
Rigy	Riga	k1gFnSc2	Riga
letecky	letecky	k6eAd1	letecky
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c4	v
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zmodernizované	zmodernizovaný	k2eAgNnSc1d1	zmodernizované
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
k	k	k7c3	k
800	[number]	k4	800
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1993	[number]	k4	1993
a	a	k8xC	a
2001	[number]	k4	2001
se	se	k3xPyFc4	se
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Baltské	baltský	k2eAgInPc1d1	baltský
námořní	námořní	k2eAgInPc1d1	námořní
trajekty	trajekt	k1gInPc1	trajekt
spojují	spojovat	k5eAaImIp3nP	spojovat
Rigu	Riga	k1gFnSc4	Riga
se	s	k7c7	s
Stockholmem	Stockholm	k1gInSc7	Stockholm
<g/>
,	,	kIx,	,
Kielem	Kiel	k1gInSc7	Kiel
a	a	k8xC	a
Lübeckem	Lübeck	k1gInSc7	Lübeck
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
také	také	k6eAd1	také
dvě	dva	k4xCgFnPc4	dva
letecké	letecký	k2eAgFnPc4d1	letecká
základny	základna	k1gFnPc4	základna
<g/>
:	:	kIx,	:
Rumbula	Rumbul	k1gMnSc2	Rumbul
a	a	k8xC	a
Spilve	Spilev	k1gFnSc2	Spilev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
všechny	všechen	k3xTgFnPc1	všechen
důležité	důležitý	k2eAgFnPc1d1	důležitá
finanční	finanční	k2eAgFnPc1d1	finanční
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
se	se	k3xPyFc4	se
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
celé	celý	k2eAgFnSc2d1	celá
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
kapacity	kapacita	k1gFnSc2	kapacita
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgFnPc2d1	veřejná
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
potravinářství	potravinářství	k1gNnSc4	potravinářství
<g/>
,	,	kIx,	,
farmakologie	farmakologie	k1gFnPc4	farmakologie
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
tiskařství	tiskařství	k1gNnSc2	tiskařství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
textilní	textilní	k2eAgInSc1d1	textilní
a	a	k8xC	a
nábytkářský	nábytkářský	k2eAgInSc1d1	nábytkářský
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
komunikačních	komunikační	k2eAgNnPc2d1	komunikační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
centrem	centrum	k1gNnSc7	centrum
dopravy	doprava	k1gFnSc2	doprava
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Rigy	Riga	k1gFnSc2	Riga
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
znatelné	znatelný	k2eAgNnSc1d1	znatelné
na	na	k7c6	na
údajích	údaj	k1gInPc6	údaj
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Způsobila	způsobit	k5eAaPmAgFnS	způsobit
to	ten	k3xDgNnSc1	ten
emigrace	emigrace	k1gFnSc1	emigrace
etnických	etnický	k2eAgMnPc2d1	etnický
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
odchod	odchod	k1gInSc4	odchod
části	část	k1gFnSc2	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
727	[number]	k4	727
578	[number]	k4	578
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
Lotyši	Lotyš	k1gMnPc1	Lotyš
kolem	kolem	k7c2	kolem
43	[number]	k4	43
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc1d1	stejné
procento	procento	k1gNnSc1	procento
je	být	k5eAaImIp3nS	být
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
žije	žít	k5eAaImIp3nS	žít
59	[number]	k4	59
%	%	kIx~	%
Lotyšů	Lotyš	k1gMnPc2	Lotyš
<g/>
,	,	kIx,	,
28,5	[number]	k4	28,5
%	%	kIx~	%
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
3,8	[number]	k4	3,8
%	%	kIx~	%
Bělorusů	Bělorus	k1gMnPc2	Bělorus
<g/>
,	,	kIx,	,
2,5	[number]	k4	2,5
%	%	kIx~	%
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
2,4	[number]	k4	2,4
%	%	kIx~	%
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
1,4	[number]	k4	1,4
%	%	kIx~	%
Litevců	Litevec	k1gMnPc2	Litevec
a	a	k8xC	a
2,4	[number]	k4	2,4
%	%	kIx~	%
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Lotyšů	Lotyš	k1gMnPc2	Lotyš
je	být	k5eAaImIp3nS	být
protestantské	protestantský	k2eAgFnSc2d1	protestantská
evangelické	evangelický	k2eAgFnSc2d1	evangelická
luteránské	luteránský	k2eAgFnSc2d1	luteránská
nebo	nebo	k8xC	nebo
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Rusů	Rus	k1gMnPc2	Rus
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
ruské	ruský	k2eAgFnSc3d1	ruská
ortodoxní	ortodoxní	k2eAgFnSc3d1	ortodoxní
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
mnoho	mnoho	k4c1	mnoho
nelotyšských	lotyšský	k2eNgFnPc2d1	lotyšský
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
přistěhovaly	přistěhovat	k5eAaPmAgFnP	přistěhovat
po	po	k7c6	po
anexi	anexe	k1gFnSc6	anexe
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
původních	původní	k2eAgFnPc2d1	původní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgInSc2	tento
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
emigrací	emigrace	k1gFnPc2	emigrace
z	z	k7c2	z
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
je	být	k5eAaImIp3nS	být
snižování	snižování	k1gNnSc1	snižování
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
procentuálně	procentuálně	k6eAd1	procentuálně
počet	počet	k1gInSc1	počet
Lotyšů	Lotyš	k1gMnPc2	Lotyš
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Rigy	Riga	k1gFnSc2	Riga
zvané	zvaný	k2eAgNnSc1d1	zvané
Stará	starat	k5eAaImIp3nS	starat
Riga	Riga	k1gFnSc1	Riga
(	(	kIx(	(
<g/>
Vecrī	Vecrī	k1gFnSc1	Vecrī
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jmenovat	jmenovat	k5eAaBmF	jmenovat
<g/>
:	:	kIx,	:
Rižský	rižský	k2eAgInSc4d1	rižský
dóm	dóm	k1gInSc4	dóm
(	(	kIx(	(
<g/>
Rī	Rī	k1gFnSc4	Rī
Doms	Domsa	k1gFnPc2	Domsa
<g/>
)	)	kIx)	)
na	na	k7c6	na
Katedrálním	katedrální	k2eAgNnSc6d1	katedrální
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
Doma	doma	k6eAd1	doma
laukums	laukums	k6eAd1	laukums
<g/>
)	)	kIx)	)
Románská	románský	k2eAgFnSc1d1	románská
monumentální	monumentální	k2eAgFnSc1d1	monumentální
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
patřící	patřící	k2eAgFnSc1d1	patřící
luteránům	luterán	k1gMnPc3	luterán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
kostelem	kostel	k1gInSc7	kostel
v	v	k7c6	v
pobaltských	pobaltský	k2eAgInPc6d1	pobaltský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
roku	rok	k1gInSc2	rok
1211	[number]	k4	1211
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
přestavována	přestavován	k2eAgFnSc1d1	přestavována
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
stavbě	stavba	k1gFnSc3	stavba
byly	být	k5eAaImAgInP	být
strženy	stržen	k2eAgInPc1d1	stržen
původní	původní	k2eAgInPc1d1	původní
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jejich	jejich	k3xOp3gFnSc4	jejich
přítomnost	přítomnost	k1gFnSc4	přítomnost
připomínají	připomínat	k5eAaImIp3nP	připomínat
jenom	jenom	k9	jenom
dlažební	dlažební	k2eAgFnPc1d1	dlažební
kostky	kostka	k1gFnPc1	kostka
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
sestavené	sestavený	k2eAgInPc4d1	sestavený
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
půdorysech	půdorys	k1gInPc6	půdorys
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
mj.	mj.	kA	mj.
i	i	k8xC	i
věže	věž	k1gFnSc2	věž
tří	tři	k4xCgInPc2	tři
velkých	velký	k2eAgInPc2d1	velký
rižských	rižský	k2eAgInPc2d1	rižský
kostelů	kostel	k1gInPc2	kostel
<g/>
:	:	kIx,	:
krom	krom	k7c2	krom
Dómu	dóm	k1gInSc2	dóm
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
věže	věž	k1gFnPc1	věž
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc4	Jakub
a	a	k8xC	a
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nejzajímavější	zajímavý	k2eAgNnSc1d3	nejzajímavější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
věžičce	věžička	k1gFnSc6	věžička
není	být	k5eNaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nachází	nacházet	k5eAaImIp3nS	nacházet
větrné	větrný	k2eAgFnPc4d1	větrná
korouhvičky	korouhvička	k1gFnPc4	korouhvička
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kohoutů	kohout	k1gMnPc2	kohout
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
podle	podle	k7c2	podle
pohanských	pohanský	k2eAgFnPc2d1	pohanská
pověr	pověra	k1gFnPc2	pověra
mají	mít	k5eAaImIp3nP	mít
odrazovat	odrazovat	k5eAaImF	odrazovat
zlé	zlý	k2eAgMnPc4d1	zlý
duchy	duch	k1gMnPc4	duch
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
varhany	varhany	k1gInPc1	varhany
s	s	k7c7	s
6768	[number]	k4	6768
píšťalami	píšťala	k1gFnPc7	píšťala
<g/>
,	,	kIx,	,
rejstříky	rejstřík	k1gInPc1	rejstřík
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
zvuky	zvuk	k1gInPc1	zvuk
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
varhany	varhany	k1gInPc1	varhany
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
300	[number]	k4	300
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Rižský	rižský	k2eAgInSc1d1	rižský
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
Rī	Rī	k1gFnSc1	Rī
pils	pilsa	k1gFnPc2	pilsa
<g/>
)	)	kIx)	)
Původně	původně	k6eAd1	původně
hrad	hrad	k1gInSc1	hrad
postavený	postavený	k2eAgInSc1d1	postavený
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
křižáků	křižák	k1gMnPc2	křižák
(	(	kIx(	(
<g/>
Mečoví	mečový	k2eAgMnPc1d1	mečový
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
mimo	mimo	k6eAd1	mimo
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
městské	městský	k2eAgFnSc2d1	městská
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
starší	starý	k2eAgInSc4d2	starší
hrad	hrad	k1gInSc4	hrad
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
zničený	zničený	k2eAgInSc4d1	zničený
Rižany	Rižan	k1gInPc4	Rižan
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1481	[number]	k4	1481
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
sídlil	sídlit	k5eAaImAgMnS	sídlit
velmistr	velmistr	k1gMnSc1	velmistr
řádu	řád	k1gInSc2	řád
<g/>
;	;	kIx,	;
o	o	k7c4	o
3	[number]	k4	3
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
měšťany	měšťan	k1gMnPc4	měšťan
vypálen	vypálen	k2eAgInSc4d1	vypálen
a	a	k8xC	a
pobořen	pobořen	k2eAgInSc4d1	pobořen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1509	[number]	k4	1509
<g/>
-	-	kIx~	-
<g/>
1515	[number]	k4	1515
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
mečonošů	mečonošů	k?	mečonošů
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
Rigou	Riga	k1gFnSc7	Riga
opět	opět	k6eAd1	opět
postaven	postavit	k5eAaPmNgMnS	postavit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
už	už	k9	už
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pozdější	pozdní	k2eAgFnSc2d2	pozdější
polské	polský	k2eAgFnSc2d1	polská
a	a	k8xC	a
švédské	švédský	k2eAgFnSc2d1	švédská
vlády	vláda	k1gFnSc2	vláda
hrad	hrad	k1gInSc4	hrad
několikrát	několikrát	k6eAd1	několikrát
stavebně	stavebně	k6eAd1	stavebně
upravovali	upravovat	k5eAaImAgMnP	upravovat
<g/>
;	;	kIx,	;
za	za	k7c2	za
ruské	ruský	k2eAgFnSc2d1	ruská
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
zásadně	zásadně	k6eAd1	zásadně
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
zámecké	zámecký	k2eAgFnSc2d1	zámecká
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
velkým	velký	k2eAgInSc7d1	velký
zásahem	zásah	k1gInSc7	zásah
byla	být	k5eAaImAgFnS	být
adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
palác	palác	k1gInSc4	palác
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pietně	pietně	k6eAd1	pietně
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
znovudosažení	znovudosažení	k1gNnSc6	znovudosažení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
a	a	k8xC	a
opět	opět	k6eAd1	opět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
zámek	zámek	k1gInSc1	zámek
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc1	sídlo
prezidenta	prezident	k1gMnSc2	prezident
Lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
prostor	prostora	k1gFnPc2	prostora
je	být	k5eAaImIp3nS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
Muzeum	muzeum	k1gNnSc4	muzeum
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
Pē	Pē	k1gFnSc1	Pē
<g/>
)	)	kIx)	)
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
středověkou	středověký	k2eAgFnSc4d1	středověká
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
věž	věž	k1gFnSc1	věž
několikrát	několikrát	k6eAd1	několikrát
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
za	za	k7c4	za
31	[number]	k4	31
let	léto	k1gNnPc2	léto
shořela	shořet	k5eAaPmAgFnS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Opětovně	opětovně	k6eAd1	opětovně
postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
staviteli	stavitel	k1gMnSc6	stavitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prý	prý	k9	prý
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
kostelní	kostelní	k2eAgFnSc2d1	kostelní
věže	věž	k1gFnSc2	věž
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
vypil	vypít	k5eAaPmAgMnS	vypít
sklenici	sklenice	k1gFnSc4	sklenice
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
hodil	hodit	k5eAaPmAgMnS	hodit
jí	jíst	k5eAaImIp3nS	jíst
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kolik	kolik	k4yRc4	kolik
střepů	střep	k1gInPc2	střep
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
tolik	tolik	k4yIc4	tolik
století	století	k1gNnSc2	století
bude	být	k5eAaImBp3nS	být
nová	nový	k2eAgFnSc1d1	nová
věž	věž	k1gFnSc1	věž
stát	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sklenice	sklenice	k1gFnSc1	sklenice
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
do	do	k7c2	do
slámy	sláma	k1gFnSc2	sláma
a	a	k8xC	a
rozbila	rozbít	k5eAaPmAgFnS	rozbít
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
opravdu	opravdu	k6eAd1	opravdu
-	-	kIx~	-
za	za	k7c4	za
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc4	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
věž	věž	k1gFnSc1	věž
při	při	k7c6	při
německém	německý	k2eAgNnSc6d1	německé
bombardování	bombardování	k1gNnSc6	bombardování
bomba	bomba	k1gFnSc1	bomba
<g/>
...	...	k?	...
Poslední	poslední	k2eAgFnSc1d1	poslední
oprava	oprava	k1gFnSc1	oprava
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
věže	věž	k1gFnSc2	věž
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kovovou	kovový	k2eAgFnSc7d1	kovová
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
Jā	Jā	k1gMnSc2	Jā
baznī	baznī	k?	baznī
<g/>
)	)	kIx)	)
s	s	k7c7	s
dominikánským	dominikánský	k2eAgInSc7d1	dominikánský
klášterem	klášter	k1gInSc7	klášter
Podle	podle	k7c2	podle
pověry	pověra	k1gFnSc2	pověra
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
zdí	zdít	k5eAaImIp3nP	zdít
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
zaživa	zaživa	k6eAd1	zaživa
dva	dva	k4xCgMnPc1	dva
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
takto	takto	k6eAd1	takto
zajistí	zajistit	k5eAaPmIp3nP	zajistit
svatost	svatost	k1gFnSc4	svatost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ale	ale	k9	ale
ostatky	ostatek	k1gInPc1	ostatek
mnichů	mnich	k1gMnPc2	mnich
opravdu	opravdu	k6eAd1	opravdu
našly	najít	k5eAaPmAgInP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
ponechány	ponechat	k5eAaPmNgFnP	ponechat
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
(	(	kIx(	(
<g/>
Jē	Jē	k1gMnSc2	Jē
baznī	baznī	k?	baznī
<g/>
)	)	kIx)	)
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
kostele	kostel	k1gInSc6	kostel
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1225	[number]	k4	1225
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
také	také	k9	také
vytesáno	vytesat	k5eAaPmNgNnS	vytesat
nad	nad	k7c7	nad
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
tvar	tvar	k1gInSc1	tvar
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgInSc1d1	kostelní
zvon	zvon	k1gInSc1	zvon
totiž	totiž	k9	totiž
nevisí	viset	k5eNaImIp3nS	viset
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vně	vně	k7c2	vně
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
zvon	zvon	k1gInSc1	zvon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
původní	původní	k2eAgInSc1d1	původní
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dovezen	dovézt	k5eAaPmNgMnS	dovézt
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
roztaven	roztaven	k2eAgMnSc1d1	roztaven
<g/>
.	.	kIx.	.
</s>
<s>
Prašná	prašný	k2eAgFnSc1d1	prašná
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
Pulvertornis	Pulvertornis	k1gInSc1	Pulvertornis
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
částí	část	k1gFnSc7	část
bývalých	bývalý	k2eAgFnPc2d1	bývalá
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
zničena	zničit	k5eAaPmNgFnS	zničit
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
opravena	opravit	k5eAaPmNgFnS	opravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ale	ale	k8xC	ale
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
chátrala	chátrat	k5eAaImAgNnP	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ujali	ujmout	k5eAaPmAgMnP	ujmout
němečtí	německý	k2eAgMnPc1d1	německý
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
město	město	k1gNnSc1	město
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
budovu	budova	k1gFnSc4	budova
pronajalo	pronajmout	k5eAaPmAgNnS	pronajmout
za	za	k7c4	za
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
částku	částka	k1gFnSc4	částka
jednoho	jeden	k4xCgInSc2	jeden
rublu	rubl	k1gInSc2	rubl
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
opravena	opravit	k5eAaPmNgFnS	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
holubí	holubí	k2eAgInSc4d1	holubí
trus	trus	k1gInSc4	trus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
léta	léto	k1gNnPc4	léto
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
<g/>
,	,	kIx,	,
a	a	k8xC	a
prodávali	prodávat	k5eAaImAgMnP	prodávat
ho	on	k3xPp3gNnSc4	on
jako	jako	k8xC	jako
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Vydělali	vydělat	k5eAaPmAgMnP	vydělat
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
studentům	student	k1gMnPc3	student
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
Prašná	prašný	k2eAgFnSc1d1	prašná
věž	věž	k1gFnSc1	věž
zachovala	zachovat	k5eAaPmAgFnS	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Lotyšského	lotyšský	k2eAgNnSc2d1	lotyšské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
Černohlavců	Černohlavec	k1gMnPc2	Černohlavec
(	(	kIx(	(
<g/>
Melngalvju	Melngalvju	k1gFnSc1	Melngalvju
nams	namsa	k1gFnPc2	namsa
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1334	[number]	k4	1334
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rižském	rižský	k2eAgNnSc6d1	Rižské
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dům	dům	k1gInSc1	dům
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
buď	buď	k8xC	buď
po	po	k7c6	po
sv.	sv.	kA	sv.
Mořicovi	Mořic	k1gMnSc6	Mořic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
patronem	patron	k1gMnSc7	patron
cechu	cech	k1gInSc2	cech
svobodných	svobodný	k2eAgMnPc2d1	svobodný
kupců	kupec	k1gMnPc2	kupec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
černým	černý	k2eAgFnPc3d1	černá
pokrývkám	pokrývka	k1gFnPc3	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kupci	kupec	k1gMnPc1	kupec
nosili	nosit	k5eAaImAgMnP	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svobodným	svobodný	k2eAgFnPc3d1	svobodná
kupcům	kupec	k1gMnPc3	kupec
patřila	patřit	k5eAaImAgFnS	patřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vybombardována	vybombardován	k2eAgFnSc1d1	vybombardována
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
Trī	Trī	k1gMnSc1	Trī
brā	brā	k?	brā
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
jsou	být	k5eAaImIp3nP	být
skupina	skupina	k1gFnSc1	skupina
tří	tři	k4xCgInPc2	tři
historických	historický	k2eAgMnPc2d1	historický
domů	dům	k1gInPc2	dům
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Mazā	Mazā	k1gMnPc2	Mazā
Pils	Pilsa	k1gFnPc2	Pilsa
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
17	[number]	k4	17
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
zděnou	zděný	k2eAgFnSc7d1	zděná
obytnou	obytný	k2eAgFnSc7d1	obytná
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
19	[number]	k4	19
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1646	[number]	k4	1646
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
21	[number]	k4	21
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prošel	projít	k5eAaPmAgMnS	projít
přestavbou	přestavba	k1gFnSc7	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
skupinou	skupina	k1gFnSc7	skupina
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
estonském	estonský	k2eAgInSc6d1	estonský
Tallinnu	Tallinn	k1gInSc6	Tallinn
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
cechovní	cechovní	k2eAgInSc1d1	cechovní
dům	dům	k1gInSc1	dům
řemeslníků	řemeslník	k1gMnPc2	řemeslník
(	(	kIx(	(
<g/>
Mā	Mā	k1gMnSc1	Mā
Ģ	Ģ	k?	Ģ
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
cechovní	cechovní	k2eAgInSc1d1	cechovní
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Lielā	Lielā	k1gFnSc1	Lielā
Ģ	Ģ	k?	Ģ
<g/>
)	)	kIx)	)
Kočičí	kočičí	k2eAgInSc1d1	kočičí
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Kaķ	Kaķ	k1gFnSc1	Kaķ
mā	mā	k?	mā
<g/>
)	)	kIx)	)
V	v	k7c6	v
parku	park	k1gInSc6	park
obklopujícím	obklopující	k2eAgNnSc7d1	obklopující
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
stojí	stát	k5eAaImIp3nS	stát
Pomník	pomník	k1gInSc4	pomník
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Brī	Brī	k1gFnSc1	Brī
piemineklis	piemineklis	k1gFnSc2	piemineklis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Kárlise	Kárlise	k1gFnSc2	Kárlise
Záleho	Zále	k1gMnSc2	Zále
a	a	k8xC	a
architekta	architekt	k1gMnSc2	architekt
Ernestse	Ernests	k1gMnSc2	Ernests
Štā	Štā	k1gMnSc2	Štā
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
darů	dar	k1gInPc2	dar
celonárodně	celonárodně	k6eAd1	celonárodně
uspořádané	uspořádaný	k2eAgFnSc2d1	uspořádaná
sbírky	sbírka	k1gFnSc2	sbírka
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
pomníku	pomník	k1gInSc2	pomník
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
mladého	mladý	k2eAgMnSc2d1	mladý
<g/>
,	,	kIx,	,
svobodného	svobodný	k2eAgInSc2d1	svobodný
a	a	k8xC	a
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
travertinového	travertinový	k2eAgInSc2d1	travertinový
obelisku	obelisk	k1gInSc2	obelisk
<g/>
,	,	kIx,	,
vyrůstajícího	vyrůstající	k2eAgInSc2d1	vyrůstající
ze	z	k7c2	z
základny	základna	k1gFnSc2	základna
památníku	památník	k1gInSc2	památník
stojí	stát	k5eAaImIp3nS	stát
měděná	měděný	k2eAgFnSc1d1	měděná
9	[number]	k4	9
<g/>
metrová	metrový	k2eAgFnSc1d1	metrová
socha	socha	k1gFnSc1	socha
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
familiárně	familiárně	k6eAd1	familiárně
Rižany	Rižan	k1gMnPc7	Rižan
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Milda	Milda	k1gFnSc1	Milda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
Svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
samotné	samotný	k2eAgNnSc1d1	samotné
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztyčených	vztyčený	k2eAgFnPc6d1	vztyčená
rukou	ruka	k1gFnPc6	ruka
třímá	třímat	k5eAaImIp3nS	třímat
tři	tři	k4xCgFnPc4	tři
pěticípé	pěticípý	k2eAgFnPc4d1	pěticípá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
lotyšské	lotyšský	k2eAgFnPc4d1	lotyšská
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
Kuronsko	Kuronsko	k1gNnSc1	Kuronsko
<g/>
,	,	kIx,	,
Vidzemsko	Vidzemsko	k1gNnSc1	Vidzemsko
a	a	k8xC	a
Latgale	Latgala	k1gFnSc6	Latgala
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
úpatí	úpatí	k1gNnSc2	úpatí
památníku	památník	k1gInSc2	památník
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
reliéfů	reliéf	k1gInPc2	reliéf
představujících	představující	k2eAgInPc2d1	představující
postavy	postava	k1gFnPc4	postava
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
události	událost	k1gFnSc2	událost
lotyšských	lotyšský	k2eAgFnPc2d1	lotyšská
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
symbolická	symbolický	k2eAgNnPc1d1	symbolické
zobrazení	zobrazení	k1gNnPc1	zobrazení
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
památníku	památník	k1gInSc6	památník
hlásá	hlásat	k5eAaImIp3nS	hlásat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vlasti	vlast	k1gFnSc3	vlast
a	a	k8xC	a
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tē	Tē	k1gMnSc1	Tē
un	un	k?	un
Brī	Brī	k1gMnSc1	Brī
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
tohoto	tento	k3xDgInSc2	tento
monumentu	monument	k1gInSc2	monument
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
sovětského	sovětský	k2eAgInSc2d1	sovětský
záboru	zábor	k1gInSc2	zábor
země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Lotyše	Lotyš	k1gMnPc4	Lotyš
obzvláště	obzvláště	k6eAd1	obzvláště
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
skrývaná	skrývaný	k2eAgFnSc1d1	skrývaná
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
památník	památník	k1gInSc1	památník
unikl	uniknout	k5eAaPmAgInS	uniknout
zničení	zničení	k1gNnSc4	zničení
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
přisouzen	přisouzen	k2eAgInSc4d1	přisouzen
nový	nový	k2eAgInSc4d1	nový
oficiální	oficiální	k2eAgInSc4d1	oficiální
význam	význam	k1gInSc4	význam
-	-	kIx~	-
tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
měly	mít	k5eAaImAgFnP	mít
představovat	představovat	k5eAaImF	představovat
tři	tři	k4xCgFnPc1	tři
pobaltské	pobaltský	k2eAgFnPc1d1	pobaltská
republiky	republika	k1gFnPc1	republika
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
"	"	kIx"	"
<g/>
osvoboditelky	osvoboditelka	k1gFnPc1	osvoboditelka
<g/>
"	"	kIx"	"
Matky	matka	k1gFnPc1	matka
Rusi	Rus	k1gFnSc2	Rus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
tradiční	tradiční	k2eAgNnSc4d1	tradiční
místo	místo	k1gNnSc4	místo
schůzek	schůzka	k1gFnPc2	schůzka
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
oslav	oslava	k1gFnPc2	oslava
či	či	k8xC	či
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
úpatí	úpatí	k1gNnSc2	úpatí
pomníku	pomník	k1gInSc2	pomník
pravidelně	pravidelně	k6eAd1	pravidelně
hlídá	hlídat	k5eAaImIp3nS	hlídat
a	a	k8xC	a
pochoduje	pochodovat	k5eAaImIp3nS	pochodovat
čestná	čestný	k2eAgFnSc1d1	čestná
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
1796	[number]	k4	1796
Riga	Riga	k1gFnSc1	Riga
nese	nést	k5eAaImIp3nS	nést
planetka	planetka	k1gFnSc1	planetka
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
74	[number]	k4	74
kilometrů	kilometr	k1gInPc2	kilometr
objevená	objevený	k2eAgFnSc1d1	objevená
ruským	ruský	k2eAgMnSc7d1	ruský
astronomem	astronom	k1gMnSc7	astronom
Nikolajem	Nikolaj	k1gMnSc7	Nikolaj
Černychem	Černych	k1gMnSc7	Černych
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
