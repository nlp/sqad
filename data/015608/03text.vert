<s>
Milla	Milla	k1gMnSc1
Jovovich	Jovovich	k1gMnSc1
</s>
<s>
Milla	Milla	k1gMnSc1
Jovovich	Jovovich	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Milica	Milic	k2eAgFnSc1d1
Bogdanovna	Bogdanovna	k1gFnSc1
Jovovich	Jovovicha	k1gFnPc2
Narození	narození	k1gNnPc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1975	#num#	k4
(	(	kIx(
<g/>
45	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
USSR	USSR	kA
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1985	#num#	k4
–	–	k?
současnost	současnost	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Shawn	Shawn	k1gInSc1
Andrews	Andrews	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
Luc	Luc	k1gMnSc1
Besson	Besson	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
–	–	k?
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
Paul	Paul	k1gMnSc1
W.	W.	kA
<g/>
S.	S.	kA
Anderson	Anderson	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Frusciante	Frusciant	k1gMnSc5
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
Anno	Anna	k1gFnSc5
Birkin	Birkin	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Ever	Ever	k1gMnSc1
Gabo	Gabo	k1gMnSc1
Jovovich-Anderson	Jovovich-Anderson	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
Dashiel	Dashiel	k1gMnSc1
Edan	Edan	k1gMnSc1
Jovovich-Anderson	Jovovich-Anderson	k1gMnSc1
(	(	kIx(
<g/>
duben	duben	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Bogić	Bogić	k?
JovovićGalina	JovovićGalina	k1gFnSc1
Jovovich	Jovovicha	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Marco	Marco	k1gMnSc1
Jovovich	Jovovich	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.millaj.comwww.millanews.com	www.millaj.comwww.millanews.com	k1gInSc1
Český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
</s>
<s>
Jitka	Jitka	k1gFnSc1
JežkováKateřina	JežkováKateřina	k1gFnSc1
LojdováVeronika	LojdováVeronika	k1gFnSc1
Veselá	Veselá	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
Yvetta	Yvetta	k1gFnSc1
Blanarovičová	Blanarovičová	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Pátý	pátý	k4xOgInSc1
Element	element	k1gInSc1
<g/>
)	)	kIx)
Umělecké	umělecký	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
Science	Science	k1gFnSc1
Fiction	Fiction	k1gInSc1
herečka	herečka	k1gFnSc1
<g/>
2008	#num#	k4
-	-	kIx~
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Zánik	zánik	k1gInSc1
<g/>
2011	#num#	k4
-	-	kIx~
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
AfterlifeNejlepší	AfterlifeNejlepší	k2eAgFnSc1d1
Herečka	herečka	k1gFnSc1
<g/>
2010	#num#	k4
-	-	kIx~
Případ	případ	k1gInSc1
Stone	ston	k1gInSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Milla	Milla	k1gMnSc1
Jovovich	Jovovich	k1gMnSc1
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Milica	Milicum	k1gNnSc2
Bogdanovna	Bogdanovna	k1gFnSc1
Jovović	Jovović	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
*	*	kIx~
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1975	#num#	k4
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
zpěvačka	zpěvačka	k1gFnSc1
a	a	k8xC
módní	módní	k2eAgFnSc1d1
návrhářka	návrhářka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Její	její	k3xOp3gNnSc1
příjmení	příjmení	k1gNnSc1
se	se	k3xPyFc4
vyslovuje	vyslovovat	k5eAaImIp3nS
[	[	kIx(
<g/>
ˈ	ˈ	k?
<g/>
]	]	kIx)
tedy	tedy	k9
[	[	kIx(
<g/>
jovovič	jovovič	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
М	М	k?
Й	Й	k?
<g/>
,	,	kIx,
srbsky	srbsky	k6eAd1
М	М	k?
Ј	Ј	k?
<g/>
/	/	kIx~
<g/>
Milica	Milica	k1gMnSc1
Jovović	Jovović	k1gMnSc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
nesprávně	správně	k6eNd1
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k8xC,k8xS
Milla	Milla	k1gFnSc1
Nataša	Nataša	k1gFnSc1
Jovovich	Jovovich	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Bogić	Bogić	k1gMnSc1
Jovović	Jovović	k1gMnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
zmiňovaný	zmiňovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
Bogi	Bog	k1gFnPc1
Jovovič	Jovovič	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
srbský	srbský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Galina	Galina	k1gFnSc1
Loginova	Loginův	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
ruská	ruský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
z	z	k7c2
Tuapse	Tuapse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevlastní	vlastnit	k5eNaImIp3nS
bratr	bratr	k1gMnSc1
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
Marco	Marco	k6eAd1
Jovović	Jovović	k1gFnSc1
<g/>
,	,	kIx,
sestřenice	sestřenice	k1gFnSc1
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Natalia	Natalius	k1gMnSc2
Jovović	Jovović	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dětství	dětství	k1gNnSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
kyjevské	kyjevský	k2eAgFnSc6d1
porodnici	porodnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
matce	matka	k1gFnSc3
lékaři	lékař	k1gMnPc7
předtím	předtím	k6eAd1
stanovili	stanovit	k5eAaPmAgMnP
diagnózu	diagnóza	k1gFnSc4
neplodnosti	neplodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
pěti	pět	k4xCc3
let	léto	k1gNnPc2
navštěvovala	navštěvovat	k5eAaImAgFnS
mateřskou	mateřský	k2eAgFnSc4d1
školku	školka	k1gFnSc4
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc4d1
vízum	vízum	k1gNnSc4
Bogimu	Bogim	k1gMnSc3
Jovovičovi	Jovovič	k1gMnSc3
vypršelo	vypršet	k5eAaPmAgNnS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
odejít	odejít	k5eAaPmF
do	do	k7c2
Západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
Galina	Galina	k1gFnSc1
Loginova	Loginův	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
tehdy	tehdy	k6eAd1
vycházející	vycházející	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
možnost	možnost	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
navštívit	navštívit	k5eAaPmF
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgFnPc6
návštěvách	návštěva	k1gFnPc6
se	se	k3xPyFc4
napotřetí	napotřetí	k6eAd1
rozhodla	rozhodnout	k5eAaPmAgFnS
nevrátit	vrátit	k5eNaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Londýna	Londýn	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
pěti	pět	k4xCc6
letech	let	k1gInPc6
rodina	rodina	k1gFnSc1
odstěhovala	odstěhovat	k5eAaPmAgFnS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
do	do	k7c2
městečka	městečko	k1gNnSc2
vzdáleného	vzdálený	k2eAgNnSc2d1
20	#num#	k4
km	km	kA
od	od	k7c2
kalifornského	kalifornský	k2eAgNnSc2d1
Sacramenta	Sacramento	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
pronajala	pronajmout	k5eAaPmAgFnS
dům	dům	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nP
rodina	rodina	k1gFnSc1
ušetřila	ušetřit	k5eAaPmAgFnS
na	na	k7c6
nájmu	nájem	k1gInSc6
<g/>
,	,	kIx,
poslali	poslat	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
rodiče	rodič	k1gMnPc4
na	na	k7c4
prázdniny	prázdniny	k1gFnPc4
na	na	k7c4
letní	letní	k2eAgInSc4d1
tábor	tábor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navázali	navázat	k5eAaPmAgMnP
přátelský	přátelský	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
majitelem	majitel	k1gMnSc7
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zpočátku	zpočátku	k6eAd1
neměl	mít	k5eNaImAgInS
důvěru	důvěra	k1gFnSc4
k	k	k7c3
přistěhovalcům	přistěhovalec	k1gMnPc3
ze	z	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
však	však	k9
začaly	začít	k5eAaPmAgFnP
rodině	rodina	k1gFnSc3
docházet	docházet	k5eAaImF
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
začít	začít	k5eAaPmF
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jovović	Jovović	k1gMnSc1
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
soukromý	soukromý	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
a	a	k8xC
Galina	Galina	k1gFnSc1
pracovala	pracovat	k5eAaImAgFnS
jako	jako	k9
kuchařka	kuchařka	k1gFnSc1
a	a	k8xC
uklízečka	uklízečka	k1gFnSc1
v	v	k7c6
domácnostech	domácnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Milla	Milla	k1gFnSc1
stala	stát	k5eAaPmAgFnS
filmovou	filmový	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
již	již	k6eAd1
jako	jako	k8xC,k8xS
malé	malý	k2eAgNnSc1d1
děvče	děvče	k1gNnSc1
ji	on	k3xPp3gFnSc4
posílala	posílat	k5eAaImAgFnS
na	na	k7c4
castingy	casting	k1gInPc4
a	a	k8xC
konkurzy	konkurz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
rodičům	rodič	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
prosadit	prosadit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
dceru	dcera	k1gFnSc4
do	do	k7c2
několika	několik	k4yIc2
malých	malý	k2eAgFnPc2d1
dětských	dětský	k2eAgFnPc2d1
rolí	role	k1gFnPc2
v	v	k7c6
televizních	televizní	k2eAgInPc6d1
filmech	film	k1gInPc6
Two	Two	k1gFnSc1
Moon	Moon	k1gInSc1
Junction	Junction	k1gInSc1
a	a	k8xC
The	The	k1gMnSc1
Night	Night	k1gMnSc1
Train	Train	k1gMnSc1
to	ten	k3xDgNnSc1
Kathmandu	Kathmanda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
začali	začít	k5eAaPmAgMnP
všímat	všímat	k5eAaImF
i	i	k9
modelingoví	modelingový	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devíti	devět	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
prvních	první	k4xOgFnPc6
profesionálních	profesionální	k2eAgFnPc6d1
fotografiích	fotografia	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
malá	malý	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
byla	být	k5eAaImAgFnS
oblečena	obléct	k5eAaPmNgFnS
do	do	k7c2
konfekce	konfekce	k1gFnSc2
<g/>
,	,	kIx,
nalíčena	nalíčen	k2eAgFnSc1d1
a	a	k8xC
vyfotografována	vyfotografován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
začala	začít	k5eAaPmAgFnS
její	její	k3xOp3gFnSc1
strmá	strmý	k2eAgFnSc1d1
modelingová	modelingový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
11	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
titulní	titulní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
italského	italský	k2eAgInSc2d1
módního	módní	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Lei	lei	k1gInSc2
a	a	k8xC
o	o	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
i	i	k9
na	na	k7c6
titulní	titulní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
časopisu	časopis	k1gInSc2
pro	pro	k7c4
mladé	mladý	k2eAgFnPc4d1
dívky	dívka	k1gFnPc4
Seventeen	Seventena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
objevovat	objevovat	k5eAaImF
i	i	k9
na	na	k7c6
titulních	titulní	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
nejrůznějších	různý	k2eAgInPc2d3
módních	módní	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
televizních	televizní	k2eAgFnPc6d1
i	i	k8xC
tiskových	tiskový	k2eAgFnPc6d1
reklamách	reklama	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
honoráře	honorář	k1gInPc4
rostly	růst	k5eAaImAgInP
až	až	k6eAd1
na	na	k7c4
10	#num#	k4
000	#num#	k4
$	$	kIx~
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
modelingové	modelingový	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
i	i	k8xC
filmům	film	k1gInPc3
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejznámější	známý	k2eAgInPc4d3
z	z	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
Návrat	návrat	k1gInSc1
do	do	k7c2
Modré	modrý	k2eAgFnSc2d1
laguny	laguna	k1gFnSc2
(	(	kIx(
<g/>
Return	Return	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Blue	Blue	k1gNnPc2
Lagoon	Lagoon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
volné	volný	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc4
úspěšného	úspěšný	k2eAgInSc2d1
romantického	romantický	k2eAgInSc2d1
filmu	film	k1gInSc2
Modrá	modrý	k2eAgFnSc1d1
laguna	laguna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
režisér	režisér	k1gMnSc1
měl	mít	k5eAaImAgMnS
extrémní	extrémní	k2eAgInPc4d1
pracovní	pracovní	k2eAgInPc4d1
nároky	nárok	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
zákony	zákon	k1gInPc1
USA	USA	kA
omezují	omezovat	k5eAaImIp3nP
maximální	maximální	k2eAgFnPc1d1
počet	počet	k1gInSc4
hodin	hodina	k1gFnPc2
odpracovaných	odpracovaný	k2eAgFnPc2d1
dětmi	dítě	k1gFnPc7
<g/>
,	,	kIx,
rodiče	rodič	k1gMnPc1
ji	on	k3xPp3gFnSc4
prohlásili	prohlásit	k5eAaPmAgMnP
finančně	finančně	k6eAd1
i	i	k8xC
právně	právně	k6eAd1
nezávislou	závislý	k2eNgFnSc4d1
na	na	k7c6
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
14	#num#	k4
letech	léto	k1gNnPc6
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
plnoletost	plnoletost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevily	projevit	k5eAaPmAgInP
se	se	k3xPyFc4
u	u	k7c2
ní	on	k3xPp3gFnSc2
známky	známka	k1gFnSc2
dospívání	dospívání	k1gNnSc2
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
kouřit	kouřit	k5eAaImF
a	a	k8xC
po	po	k7c6
skončení	skončení	k1gNnSc6
natáčení	natáčení	k1gNnSc2
filmu	film	k1gInSc2
Dazed	Dazed	k1gMnSc1
and	and	k?
Confused	Confused	k1gMnSc1
si	se	k3xPyFc3
vzala	vzít	k5eAaPmAgFnS
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
svého	svůj	k3xOyFgMnSc2
hereckého	herecký	k2eAgMnSc2d1
partnera	partner	k1gMnSc2
Shawna	Shawn	k1gMnSc2
Andrewse	Andrews	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželství	manželství	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
záhy	záhy	k6eAd1
anulováno	anulovat	k5eAaBmNgNnS
jejími	její	k3xOp3gMnPc7
rodiči	rodič	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
dali	dát	k5eAaPmAgMnP
podmínku	podmínka	k1gFnSc4
„	„	k?
<g/>
buďte	budit	k5eAaImRp2nP
spolu	spolu	k6eAd1
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
a	a	k8xC
když	když	k8xS
vše	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
,	,	kIx,
uspořádáme	uspořádat	k5eAaPmIp1nP
vám	vy	k3xPp2nPc3
velkou	velký	k2eAgFnSc4d1
oficiální	oficiální	k2eAgFnSc4d1
svatbu	svatba	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
patří	patřit	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
Shawnem	Shawn	k1gMnSc7
Andrewsem	Andrews	k1gMnSc7
se	se	k3xPyFc4
však	však	k9
brzy	brzy	k6eAd1
rozešla	rozejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
odjela	odjet	k5eAaPmAgFnS
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahrála	nahrát	k5eAaPmAgFnS,k5eAaBmAgFnS
hudební	hudební	k2eAgNnSc4d1
album	album	k1gNnSc4
The	The	k1gFnSc2
Divine	Divin	k1gInSc5
Comedy	Comed	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
jí	on	k3xPp3gFnSc3
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
18	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
kombinuje	kombinovat	k5eAaImIp3nS
několik	několik	k4yIc4
různých	různý	k2eAgInPc2d1
stylů	styl	k1gInPc2
včetně	včetně	k7c2
folku	folk	k1gInSc2
a	a	k8xC
popu	pop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
alba	album	k1gNnSc2
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
zpátky	zpátky	k6eAd1
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
koncertovala	koncertovat	k5eAaImAgFnS
v	v	k7c6
několika	několik	k4yIc6
městech	město	k1gNnPc6
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
dráhu	dráha	k1gFnSc4
modelky	modelka	k1gFnSc2
a	a	k8xC
zpěvačky	zpěvačka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konkurzu	konkurz	k1gInSc6
ve	v	k7c6
filmu	film	k1gInSc6
Pátý	pátý	k4xOgInSc1
element	element	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Fifth	Fifth	k1gMnSc1
Element	element	k1gInSc1
<g/>
)	)	kIx)
uspěla	uspět	k5eAaPmAgFnS
až	až	k9
napodruhé	napodruhé	k6eAd1
a	a	k8xC
získala	získat	k5eAaPmAgFnS
rovnou	rovnou	k6eAd1
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
natáčení	natáčení	k1gNnSc2
se	se	k3xPyFc4
sblížila	sblížit	k5eAaPmAgFnS
s	s	k7c7
filmovým	filmový	k2eAgMnSc7d1
režisérem	režisér	k1gMnSc7
Lucem	Luc	k1gMnSc7
Bessonem	Besson	k1gMnSc7
<g/>
,	,	kIx,
za	za	k7c4
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
se	se	k3xPyFc4
vdala	vdát	k5eAaPmAgFnS
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
vydrželo	vydržet	k5eAaPmAgNnS
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
skončilo	skončit	k5eAaPmAgNnS
brzy	brzy	k6eAd1
po	po	k7c4
natočení	natočení	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
druhého	druhý	k4xOgInSc2
společného	společný	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
,	,	kIx,
Johanka	Johanka	k1gFnSc1
z	z	k7c2
Arku	arkus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
natáčel	natáčet	k5eAaImAgInS
také	také	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vyhledávanou	vyhledávaný	k2eAgFnSc7d1
herečkou	herečka	k1gFnSc7
a	a	k8xC
modelkou	modelka	k1gFnSc7
<g/>
,	,	kIx,
objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
exkluzivním	exkluzivní	k2eAgInSc6d1
kalendáři	kalendář	k1gInSc6
Pirelli	Pirelle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrála	hrát	k5eAaImAgFnS
také	také	k9
v	v	k7c6
hollywoodských	hollywoodský	k2eAgInPc6d1
filmech	film	k1gInPc6
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
a	a	k8xC
dostala	dostat	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k9
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
širokého	široký	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
časopisu	časopis	k1gInSc2
Forbes	forbes	k1gInSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejlépe	dobře	k6eAd3
placených	placený	k2eAgFnPc2d1
modelek	modelka	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
s	s	k7c7
ročním	roční	k2eAgInSc7d1
příjmem	příjem	k1gInSc7
přes	přes	k7c4
deset	deset	k4xCc4
a	a	k8xC
půl	půl	k1xP
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
za	za	k7c4
modeling	modeling	k1gInSc4
<g/>
,	,	kIx,
hlavně	hlavně	k9
díky	díky	k7c3
kontraktu	kontrakt	k1gInSc3
s	s	k7c7
L	L	kA
<g/>
'	'	kIx"
<g/>
Oreal	Oreal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
je	být	k5eAaImIp3nS
také	také	k9
matkou	matka	k1gFnSc7
dcery	dcera	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
dostala	dostat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Ever	Evera	k1gFnPc2
Gabo	Gabo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otcem	otec	k1gMnSc7
dítěte	dítě	k1gNnSc2
je	být	k5eAaImIp3nS
režisér	režisér	k1gMnSc1
Paul	Paul	k1gMnSc1
W.	W.	kA
S.	S.	kA
Anderson	Anderson	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
při	při	k7c6
natáčení	natáčení	k1gNnSc6
filmu	film	k1gInSc2
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
za	za	k7c2
manžela	manžel	k1gMnSc2
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
vzala	vzít	k5eAaPmAgFnS
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
domě	dům	k1gInSc6
v	v	k7c4
Beverly	Beverla	k1gFnPc4
Hills	Hillsa	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
porodila	porodit	k5eAaPmAgFnS
druhou	druhý	k4xOgFnSc4
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
dostala	dostat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Dashiel	Dashilo	k1gNnPc2
Edan	Edana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
2019	#num#	k4
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
oznámila	oznámit	k5eAaPmAgFnS
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
čeká	čekat	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc4
dceru	dcera	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
předtím	předtím	k6eAd1
potratila	potratit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Osian	Osian	k1gInSc1
Lark	Lark	k1gInSc1
Elliot	Elliota	k1gFnPc2
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
TBA	TBA	kA
</s>
<s>
Corto	Corto	k1gNnSc1
Maltese	Maltese	k1gFnSc2
</s>
<s>
2019	#num#	k4
</s>
<s>
Monster	monstrum	k1gNnPc2
Hunter	Huntra	k1gFnPc2
</s>
<s>
Artemis	Artemis	k1gFnSc1
</s>
<s>
Hellboy	Hellboa	k1gFnPc1
</s>
<s>
Nimue	Nimue	k1gFnSc1
<g/>
,	,	kIx,
Královna	královna	k1gFnSc1
krve	krev	k1gFnSc2
</s>
<s>
Paradise	Paradise	k1gFnSc1
Hills	Hillsa	k1gFnPc2
</s>
<s>
The	The	k?
Duchess	Duchess	k1gInSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
The	The	k?
Rookies	Rookies	k1gInSc1
</s>
<s>
Bruce	Bruce	k1gMnSc1
</s>
<s>
čínský	čínský	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Future	Futur	k1gMnSc5
World	World	k1gMnSc1
</s>
<s>
Drug	Drug	k1gMnSc1
Lord	lord	k1gMnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Shock	Shock	k1gMnSc1
and	and	k?
Awe	Awe	k1gMnSc1
</s>
<s>
Vlatka	Vlatka	k1gFnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Poslední	poslední	k2eAgFnSc1d1
kapitola	kapitola	k1gFnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
Zoolander	Zoolander	k1gInSc1
No	no	k9
<g/>
.	.	kIx.
2	#num#	k4
</s>
<s>
Katinka	Katinka	k1gFnSc1
Ingabogovinanana	Ingabogovinanana	k1gFnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Savva	Savva	k1gFnSc1
<g/>
:	:	kIx,
Srdce	srdce	k1gNnSc1
bojovníka	bojovník	k1gMnSc2
</s>
<s>
Savva	Savva	k6eAd1
</s>
<s>
dabing	dabing	k1gInSc1
(	(	kIx(
<g/>
anglické	anglický	k2eAgNnSc1d1
znění	znění	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
přežití	přežití	k1gNnSc4
</s>
<s>
Kate	kat	k1gMnSc5
Abbott	Abbott	k1gMnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Cymbeline	Cymbelin	k1gInSc5
</s>
<s>
The	The	k?
Queen	Queen	k1gInSc1
(	(	kIx(
<g/>
Královna	královna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Odveta	odveta	k1gFnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Tváře	tvář	k1gFnPc1
v	v	k7c6
davu	dav	k1gInSc6
</s>
<s>
Anna	Anna	k1gFnSc1
Marchant	Marchanta	k1gFnPc2
</s>
<s>
Tři	tři	k4xCgMnPc1
mušketýři	mušketýr	k1gMnPc1
</s>
<s>
Milady	Milada	k1gFnPc1
de	de	k?
Winter	Winter	k1gMnSc1
</s>
<s>
Moje	můj	k3xOp1gFnSc1
potrhlá	potrhlý	k2eAgFnSc1d1
máma	máma	k1gFnSc1
</s>
<s>
Olive	Olivat	k5eAaPmIp3nS
</s>
<s>
Vykrutasy	Vykrutasa	k1gFnPc1
</s>
<s>
Nadya	Nadya	k6eAd1
</s>
<s>
ruský	ruský	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Dirty	Dirta	k1gFnPc1
Girl	girl	k1gFnSc2
</s>
<s>
Sue-Ann	Sue-Ann	k1gInSc1
Edmondston	Edmondston	k1gInSc1
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Afterlife	Afterlif	k1gInSc5
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
Past	past	k1gFnSc1
</s>
<s>
Lucetta	Lucetta	k1gMnSc1
Creeson	Creeson	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1
druh	druh	k1gMnSc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Abigail	Abigail	k1gMnSc1
"	"	kIx"
<g/>
Abbey	Abbea	k1gMnSc2
<g/>
"	"	kIx"
Tyler	Tyler	k1gMnSc1
</s>
<s>
Dokonalý	dokonalý	k2eAgInSc1d1
únik	únik	k1gInSc1
</s>
<s>
Cydney	Cydnea	k1gMnSc2
Anderson	Anderson	k1gMnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Přestřelka	přestřelka	k1gFnSc1
v	v	k7c6
Palermu	Palermo	k1gNnSc6
</s>
<s>
Milla	Milla	k1gMnSc1
Jovovich	Jovovich	k1gMnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Zánik	zánik	k1gInSc1
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Ultraviolet	Ultraviolet	k1gInSc1
</s>
<s>
Violet	Violeta	k1gFnPc2
Song	song	k1gInSc1
Jat	jat	k2eAgInSc4d1
Shariff	Shariff	k1gInSc4
</s>
<s>
Ráže	ráže	k1gFnSc1
.45	.45	k4
</s>
<s>
Kat	kat	k1gMnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Caligula	Caligula	k1gFnSc1
</s>
<s>
Julia	Julius	k1gMnSc2
Drusilla	Drusill	k1gMnSc2
</s>
<s>
trailer	trailer	k1gInSc1
/	/	kIx~
kraťas	kraťas	k1gInSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Apokalypsa	apokalypsa	k1gFnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Loutka	loutka	k1gFnSc1
</s>
<s>
Fangora	Fangora	k1gFnSc1
"	"	kIx"
<g/>
Fanny	Fanen	k2eAgInPc1d1
<g/>
"	"	kIx"
Gurkel	Gurkel	k1gInSc1
</s>
<s>
Hra	hra	k1gFnSc1
o	o	k7c4
čas	čas	k1gInSc4
</s>
<s>
Erin	Erin	k1gInSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
Dvě	dva	k4xCgFnPc1
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
</s>
<s>
Nadine	Nadinout	k5eAaPmIp3nS
</s>
<s>
2001	#num#	k4
</s>
<s>
Zoolander	Zoolander	k1gMnSc1
</s>
<s>
Katinka	Katinka	k1gFnSc1
Ingabogovinanana	Ingabogovinanana	k1gFnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
Vykoupení	vykoupení	k1gNnSc1
</s>
<s>
Lucia	Lucia	k1gFnSc1
</s>
<s>
Million	Million	k1gInSc1
Dollar	dollar	k1gInSc1
Hotel	hotel	k1gInSc1
</s>
<s>
Eloise	Eloise	k1gFnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Johanka	Johanka	k1gFnSc1
z	z	k7c2
Arku	arkus	k1gInSc2
</s>
<s>
Jana	Jana	k1gFnSc1
z	z	k7c2
Arku	arkus	k1gInSc2
</s>
<s>
1998	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
</s>
<s>
Dakota	Dakota	k1gFnSc1
Burns	Burnsa	k1gFnPc2
</s>
<s>
1997	#num#	k4
</s>
<s>
Pátý	pátý	k4xOgInSc1
element	element	k1gInSc1
</s>
<s>
Leeloo	Leeloo	k6eAd1
de	de	k?
Sabat	sabat	k1gInSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
Omámení	omámení	k1gNnSc1
a	a	k8xC
zmatení	zmatení	k1gNnSc1
</s>
<s>
Michelle	Michelle	k1gFnSc1
Burroughs	Burroughsa	k1gFnPc2
</s>
<s>
1992	#num#	k4
</s>
<s>
Kuffs	Kuffs	k6eAd1
</s>
<s>
Maya	Maya	k?
Carlton	Carlton	k1gInSc1
</s>
<s>
Chaplin	Chaplin	k1gInSc1
</s>
<s>
Mildred	Mildred	k1gInSc1
Harris	Harris	k1gFnSc2
</s>
<s>
1991	#num#	k4
</s>
<s>
Návrat	návrat	k1gInSc1
do	do	k7c2
Modré	modrý	k2eAgFnSc2d1
laguny	laguna	k1gFnSc2
</s>
<s>
Lilli	Lill	k1gMnPc1
Hargrave	Hargrav	k1gInSc5
</s>
<s>
1988	#num#	k4
</s>
<s>
Spojení	spojení	k1gNnSc1
dvou	dva	k4xCgMnPc2
měsíců	měsíc	k1gInPc2
</s>
<s>
Samantha	Samantha	k1gFnSc1
Delongpre	Delongpr	k1gInSc5
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Díly	díl	k1gInPc1
</s>
<s>
1988	#num#	k4
</s>
<s>
The	The	k?
Night	Night	k1gMnSc1
Train	Train	k1gMnSc1
to	ten	k3xDgNnSc4
Kathmandu	Kathmando	k1gNnSc3
</s>
<s>
Lily	lít	k5eAaImAgFnP
McLeod	McLeod	k1gInSc4
</s>
<s>
televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Paradise	Paradise	k1gFnSc1
</s>
<s>
Katie	Katie	k1gFnSc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Childhood	Childhood	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
End	End	k1gFnSc7
<g/>
"	"	kIx"
</s>
<s>
1989	#num#	k4
</s>
<s>
Ženatý	ženatý	k2eAgMnSc1d1
se	se	k3xPyFc4
závazky	závazek	k1gInPc7
</s>
<s>
Yvette	Yvette	k5eAaPmIp2nP
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Fair	fair	k2eAgFnSc1d1
Exchange	Exchange	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
1990	#num#	k4
</s>
<s>
Parker	Parker	k1gInSc1
Lewis	Lewis	k1gFnSc2
musí	muset	k5eAaImIp3nS
vyhrát	vyhrát	k5eAaPmF
</s>
<s>
Robin	robin	k2eAgInSc1d1
Fecknowitz	Fecknowitz	k1gInSc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Pilot	pilot	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
2002	#num#	k4
</s>
<s>
Tatík	tatík	k1gMnSc1
Hill	Hill	k1gMnSc1
a	a	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Serena	Seren	k2eAgFnSc1d1
Shaw	Shaw	k1gFnSc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Get	Get	k1gMnSc1
Your	Your	k1gMnSc1
Freak	Freak	k1gMnSc1
Off	Off	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
2009	#num#	k4
</s>
<s>
Heidi	Heid	k1gMnPc1
klum	klum	k6eAd1
<g/>
:	:	kIx,
svět	svět	k1gInSc1
modelingu	modeling	k1gInSc2
</s>
<s>
sama	sám	k3xTgFnSc1
sebe	sebe	k3xPyFc4
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Around	Around	k1gInSc1
the	the	k?
World	World	k1gInSc1
in	in	k?
Two	Two	k1gFnSc2
Days	Daysa	k1gFnPc2
<g/>
"	"	kIx"
</s>
<s>
Videoklip	videoklip	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
videoklipu	videoklip	k1gInSc2
</s>
<s>
Interpret	interpret	k1gMnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
2016	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Withorwithout	Withorwithout	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
PARCELS	PARCELS	kA
</s>
<s>
2016	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Signal	Signal	k1gMnPc5
<g/>
"	"	kIx"
</s>
<s>
SOHN	SOHN	kA
</s>
<s>
2013	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
I	i	k9
Wanna	Wanen	k2eAgFnSc1d1
Be	Be	k1gFnSc1
A	a	k8xC
Warhol	Warhol	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Alkaline	Alkalin	k1gInSc5
Trio	trio	k1gNnSc1
</s>
<s>
My	my	k3xPp1nPc1
Shame	Sham	k1gMnSc5
Is	Is	k1gMnSc5
True	Truus	k1gMnSc5
<g/>
'	'	kIx"
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Alba	alba	k1gFnSc1
</s>
<s>
1994	#num#	k4
–	–	k?
The	The	k1gMnSc5
Divine	Divin	k1gMnSc5
Comedy	Comeda	k1gMnSc2
</s>
<s>
1998	#num#	k4
–	–	k?
The	The	k1gMnSc1
People	People	k1gMnSc1
Tree	Tree	k1gFnSc4
Sessions	Sessionsa	k1gFnPc2
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
1994	#num#	k4
-	-	kIx~
Gentleman	gentleman	k1gMnSc1
Who	Who	k1gMnSc1
Fell	Fell	k1gMnSc1
</s>
<s>
1994	#num#	k4
-	-	kIx~
Bang	Bang	k1gMnSc1
Your	Your	k1gMnSc1
Head	Head	k1gMnSc1
</s>
<s>
1994	#num#	k4
-	-	kIx~
It	It	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Your	Youra	k1gFnPc2
Life	Life	k1gNnSc1
</s>
<s>
2012	#num#	k4
-	-	kIx~
Electric	Electric	k1gMnSc1
Sky	Sky	k1gMnSc1
</s>
<s>
2013	#num#	k4
–	–	k?
Let	let	k1gInSc1
You	You	k1gMnSc1
Go	Go	k1gMnSc1
</s>
<s>
Soundtracky	soundtrack	k1gInPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Píseň	píseň	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
"	"	kIx"
<g/>
Satellite	Satellit	k1gInSc5
of	of	k?
Love	lov	k1gInSc5
<g/>
"	"	kIx"
<g/>
The	The	k1gFnSc1
Million	Million	k1gInSc4
Dollar	dollar	k1gInSc1
Hotel	hotel	k1gInSc1
</s>
<s>
2002	#num#	k4
<g/>
"	"	kIx"
<g/>
The	The	k1gMnSc1
Gentleman	gentleman	k1gMnSc1
Who	Who	k1gMnSc1
Fell	Fell	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Pravidla	pravidlo	k1gNnPc4
vášně	vášeň	k1gFnSc2
</s>
<s>
"	"	kIx"
<g/>
Shein	Shein	k1gInSc1
VI	VI	kA
Di	Di	k1gFnSc2
l	l	kA
<g/>
'	'	kIx"
<g/>
Vone	von	k1gInSc5
<g/>
"	"	kIx"
<g/>
Loutka	loutko	k1gNnPc4
</s>
<s>
"	"	kIx"
<g/>
Mezinka	Mezinka	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
2003	#num#	k4
<g/>
"	"	kIx"
<g/>
Rocket	Rocket	k1gInSc1
Collecting	Collecting	k1gInSc1
<g/>
"	"	kIx"
<g/>
Underworld	Underworld	k1gInSc1
</s>
<s>
2009	#num#	k4
<g/>
"	"	kIx"
<g/>
Underneath	Underneath	k1gInSc1
the	the	k?
Stars	Stars	k1gInSc1
(	(	kIx(
<g/>
Renholder	Renholder	k1gInSc1
Mix	mix	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
Underworld	Underworld	k1gInSc1
<g/>
:	:	kIx,
Vzpoura	vzpoura	k1gFnSc1
Lycanů	Lycan	k1gInPc2
</s>
<s>
2010	#num#	k4
<g/>
"	"	kIx"
<g/>
The	The	k1gFnSc1
Mission	Mission	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
feat	feat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Puscifer	Puscifer	k1gMnSc1
&	&	k?
Renholder	Renholder	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Blood	Blood	k1gInSc1
Into	Into	k1gNnSc1
Wine	Wine	k1gInSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Ti	ten	k3xDgMnPc1
Zh	Zh	k1gMnPc1
Mene	Men	k1gFnSc2
Pidmanula	Pidmanula	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
Prozhektorperiskhilton	Prozhektorperiskhilton	k1gInSc1
</s>
<s>
"	"	kIx"
<g/>
Proud	proud	k1gInSc1
Mary	Mary	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
Moje	můj	k3xOp1gFnSc1
potrhlá	potrhlý	k2eAgFnSc1d1
máma	máma	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
nominace	nominace	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc4
ocenění	ocenění	k1gNnPc2
a	a	k8xC
nominací	nominace	k1gFnPc2
Milly	Milla	k1gFnSc2
Jovovich	Jovovich	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Г	Г	k?
Л	Л	k?
<g/>
:	:	kIx,
С	С	k?
К	К	k?
<g/>
,	,	kIx,
н	н	k?
в	в	k?
д	д	k?
<g/>
,	,	kIx,
г	г	k?
в	в	k?
i	i	k8xC
н	н	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-08-23	2005-08-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
sub	sub	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
potratu	potrat	k1gInSc6
je	být	k5eAaImIp3nS
Jovovichová	Jovovichová	k1gFnSc1
ve	v	k7c6
třiačtyřiceti	třiačtyřicet	k4xCc6
letech	léto	k1gNnPc6
opět	opět	k6eAd1
těhotná	těhotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-08	2019-08-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Milla	Millo	k1gNnSc2
Jovovich	Jovovicha	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Milly	Milla	k1gFnSc2
Jovovich	Jovovicha	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Módní	módní	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Milly	Milla	k1gFnSc2
Jovovich	Jovovicha	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39299	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
130895326	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1476	#num#	k4
6152	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98129196	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85546108	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98129196	#num#	k4
</s>
