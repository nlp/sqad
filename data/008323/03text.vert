<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
Zwickau	Zwickaus	k1gInSc2	Zwickaus
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Endenich	Endenich	k1gMnSc1	Endenich
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgMnPc4d3	nejvýraznější
představitele	představitel	k1gMnPc4	představitel
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
skládal	skládat	k5eAaImAgMnS	skládat
zejména	zejména	k9	zejména
klavírní	klavírní	k2eAgFnSc4d1	klavírní
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
synem	syn	k1gMnSc7	syn
Augusta	August	k1gMnSc2	August
Schumanna	Schumann	k1gMnSc2	Schumann
<g/>
,	,	kIx,	,
plodného	plodný	k2eAgMnSc2d1	plodný
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
překladatele	překladatel	k1gMnSc2	překladatel
romantické	romantický	k2eAgFnSc2d1	romantická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
"	"	kIx"	"
<g/>
ctihodného	ctihodný	k2eAgMnSc4d1	ctihodný
občana	občan	k1gMnSc4	občan
a	a	k8xC	a
knihkupce	knihkupec	k1gMnPc4	knihkupec
<g/>
"	"	kIx"	"
ze	z	k7c2	z
saského	saský	k2eAgNnSc2d1	Saské
městečka	městečko	k1gNnSc2	městečko
Cvikov	Cvikov	k1gInSc1	Cvikov
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Zwickau	Zwickaus	k1gInSc2	Zwickaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
projevoval	projevovat	k5eAaImAgMnS	projevovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
četl	číst	k5eAaImAgMnS	číst
díla	dílo	k1gNnPc4	dílo
klasiků	klasik	k1gMnPc2	klasik
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
psal	psát	k5eAaImAgMnS	psát
beletrii	beletrie	k1gFnSc4	beletrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
odborné	odborný	k2eAgInPc4d1	odborný
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
příspěvek	příspěvek	k1gInSc1	příspěvek
do	do	k7c2	do
sborníku	sborník	k1gInSc2	sborník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
učil	učit	k5eAaImAgInS	učit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Johanna	Johann	k1gMnSc2	Johann
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Kuntsche	Kuntsch	k1gMnSc2	Kuntsch
<g/>
,	,	kIx,	,
středoškolského	středoškolský	k2eAgMnSc2d1	středoškolský
profesora	profesor	k1gMnSc2	profesor
a	a	k8xC	a
varhaníka	varhaník	k1gMnSc2	varhaník
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
také	také	k9	také
skládal	skládat	k5eAaImAgMnS	skládat
první	první	k4xOgFnPc4	první
hudební	hudební	k2eAgFnPc4d1	hudební
dílka	dílko	k1gNnPc4	dílko
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
podporoval	podporovat	k5eAaImAgMnS	podporovat
jeho	jeho	k3xOp3gFnSc4	jeho
literární	literární	k2eAgFnSc4d1	literární
i	i	k8xC	i
hudební	hudební	k2eAgFnSc4d1	hudební
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
chtěl	chtít	k5eAaImAgMnS	chtít
domluvit	domluvit	k5eAaPmF	domluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Roberta	Robert	k1gMnSc4	Robert
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
hudbu	hudba	k1gFnSc4	hudba
Carl	Carla	k1gFnPc2	Carla
Maria	Maria	k1gFnSc1	Maria
von	von	k1gInSc1	von
Weber	weber	k1gInSc1	weber
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
nedalekých	daleký	k2eNgInPc6d1	nedaleký
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
plány	plán	k1gInPc4	plán
však	však	k9	však
již	již	k6eAd1	již
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
August	August	k1gMnSc1	August
Schumann	Schumann	k1gMnSc1	Schumann
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
otce	otec	k1gMnSc2	otec
dolehla	dolehnout	k5eAaPmAgFnS	dolehnout
na	na	k7c4	na
rodinu	rodina	k1gFnSc4	rodina
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
Robertova	Robertův	k2eAgFnSc1d1	Robertova
sestra	sestra	k1gFnSc1	sestra
Emilie	Emilie	k1gFnSc2	Emilie
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
–	–	k?	–
<g/>
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
otec	otec	k1gMnSc1	otec
jeho	on	k3xPp3gInSc4	on
hudební	hudební	k2eAgInSc4d1	hudební
talent	talent	k1gInSc4	talent
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc4	on
poslala	poslat	k5eAaPmAgFnS	poslat
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
studium	studium	k1gNnSc1	studium
práv	právo	k1gNnPc2	právo
naprosto	naprosto	k6eAd1	naprosto
nezajímá	zajímat	k5eNaImIp3nS	zajímat
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
šťasten	šťasten	k2eAgMnSc1d1	šťasten
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
skládá	skládat	k5eAaImIp3nS	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
koncertním	koncertní	k2eAgMnSc7d1	koncertní
klavíristou	klavírista	k1gMnSc7	klavírista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
u	u	k7c2	u
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wiecka	Wiecko	k1gNnSc2	Wiecko
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Wieck	Wieck	k1gMnSc1	Wieck
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Claru	Clara	k1gFnSc4	Clara
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
Schumann	Schumann	k1gInSc1	Schumann
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vynikající	vynikající	k2eAgFnSc7d1	vynikající
klavíristkou	klavíristka	k1gFnSc7	klavíristka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chronické	chronický	k2eAgNnSc1d1	chronické
zranění	zranění	k1gNnSc1	zranění
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
výsledek	výsledek	k1gInSc1	výsledek
nošení	nošení	k1gNnSc2	nošení
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
dlahy	dlaha	k1gFnSc2	dlaha
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
prstokladu	prstoklad	k1gInSc2	prstoklad
<g/>
)	)	kIx)	)
učinilo	učinit	k5eAaPmAgNnS	učinit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
konec	konec	k1gInSc1	konec
Schumannovým	Schumannův	k2eAgFnPc3d1	Schumannova
interpretačním	interpretační	k2eAgFnPc3d1	interpretační
ambicím	ambice	k1gFnPc3	ambice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
komponování	komponování	k1gNnSc1	komponování
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
u	u	k7c2	u
skladatele	skladatel	k1gMnSc2	skladatel
Heinricha	Heinrich	k1gMnSc2	Heinrich
Dorna	Dorn	k1gMnSc2	Dorn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
větší	veliký	k2eAgInPc4d2	veliký
úspěchy	úspěch	k1gInPc4	úspěch
slavil	slavit	k5eAaImAgInS	slavit
s	s	k7c7	s
programní	programní	k2eAgFnSc7d1	programní
skladbou	skladba	k1gFnSc7	skladba
Karneval	karneval	k1gInSc1	karneval
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgNnPc2d1	následující
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skládal	skládat	k5eAaImAgMnS	skládat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
některá	některý	k3yIgNnPc4	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
nejslavnějších	slavný	k2eAgNnPc2d3	nejslavnější
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Dětské	dětský	k2eAgFnPc1d1	dětská
scény	scéna	k1gFnPc1	scéna
<g/>
,	,	kIx,	,
Fantazie	fantazie	k1gFnPc1	fantazie
v	v	k7c6	v
C	C	kA	C
dur	dur	k1gNnSc1	dur
či	či	k8xC	či
Kreisleriana	Kreisleriana	k1gFnSc1	Kreisleriana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
několika	několik	k4yIc7	několik
společníky	společník	k1gMnPc7	společník
vydávat	vydávat	k5eAaPmF	vydávat
hudebně	hudebně	k6eAd1	hudebně
kritický	kritický	k2eAgInSc1d1	kritický
časopis	časopis	k1gInSc1	časopis
Neue	Neue	k1gNnSc2	Neue
Zeitschrift	Zeitschrifta	k1gFnPc2	Zeitschrifta
für	für	k?	für
Musik	musika	k1gFnPc2	musika
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
klasiků	klasik	k1gMnPc2	klasik
zde	zde	k6eAd1	zde
propagoval	propagovat	k5eAaImAgInS	propagovat
i	i	k9	i
některé	některý	k3yIgMnPc4	některý
své	svůj	k3xOyFgMnPc4	svůj
současníky	současník	k1gMnPc4	současník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
hudbu	hudba	k1gFnSc4	hudba
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Chopina	Chopin	k1gMnSc4	Chopin
či	či	k8xC	či
Berlioze	Berlioz	k1gMnSc4	Berlioz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Clarou	Clara	k1gFnSc7	Clara
Wieckovou	Wiecková	k1gFnSc7	Wiecková
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
začal	začít	k5eAaPmAgInS	začít
romantický	romantický	k2eAgInSc1d1	romantický
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
patnáctiletou	patnáctiletý	k2eAgFnSc7d1	patnáctiletá
Clarou	Clara	k1gFnSc7	Clara
Wieckovou	Wieckův	k2eAgFnSc7d1	Wieckův
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
to	ten	k3xDgNnSc1	ten
zjistil	zjistit	k5eAaPmAgMnS	zjistit
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
oběma	dva	k4xCgMnPc7	dva
zakázal	zakázat	k5eAaPmAgInS	zakázat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
dál	daleko	k6eAd2	daleko
stýkat	stýkat	k5eAaImF	stýkat
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
spálit	spálit	k5eAaPmF	spálit
veškerou	veškerý	k3xTgFnSc4	veškerý
jejich	jejich	k3xOp3gFnSc4	jejich
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
přesto	přesto	k8xC	přesto
tajně	tajně	k6eAd1	tajně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
a	a	k8xC	a
Schumann	Schumann	k1gInSc1	Schumann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
požádal	požádat	k5eAaPmAgInS	požádat
Wiecka	Wiecko	k1gNnPc4	Wiecko
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rázném	rázný	k2eAgNnSc6d1	rázné
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
soudně	soudně	k6eAd1	soudně
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
trvalo	trvat	k5eAaImAgNnS	trvat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1840	[number]	k4	1840
se	se	k3xPyFc4	se
s	s	k7c7	s
Clarou	Clara	k1gFnSc7	Clara
konečně	konečně	k6eAd1	konečně
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wieck	Wieck	k1gMnSc1	Wieck
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
usmířil	usmířit	k5eAaPmAgMnS	usmířit
až	až	k9	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
osm	osm	k4xCc4	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
i	i	k9	i
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
zůstala	zůstat	k5eAaPmAgFnS	zůstat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
mladší	mladý	k2eAgFnPc4d2	mladší
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
matčiných	matčin	k2eAgInPc2d1	matčin
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
správkyní	správkyně	k1gFnSc7	správkyně
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
</s>
</p>
<p>
<s>
Elise	elise	k1gFnSc1	elise
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
učitelkou	učitelka	k1gFnSc7	učitelka
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c2	za
obchodníka	obchodník	k1gMnSc2	obchodník
Louise	Louis	k1gMnSc2	Louis
Sommerhoffa	Sommerhoff	k1gMnSc2	Sommerhoff
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
měla	mít	k5eAaImAgFnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
<s>
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
-	-	kIx~	-
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
nalomenému	nalomený	k2eAgNnSc3d1	nalomené
zdraví	zdraví	k1gNnSc3	zdraví
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
u	u	k7c2	u
přátel	přítel	k1gMnPc2	přítel
Schumannových	Schumannův	k2eAgMnPc2d1	Schumannův
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c2	za
italského	italský	k2eAgMnSc2d1	italský
šlechtice	šlechtic	k1gMnSc2	šlechtic
se	se	k3xPyFc4	se
kterým	který	k3yIgNnSc7	který
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
však	však	k9	však
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
teprve	teprve	k6eAd1	teprve
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
</s>
</p>
<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
-	-	kIx~	-
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
problematicky	problematicky	k6eAd1	problematicky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
nervová	nervový	k2eAgFnSc1d1	nervová
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yIgFnSc3	který
byl	být	k5eAaImAgInS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
do	do	k7c2	do
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgMnPc4d1	chorý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	se	k3xPyFc4	se
obchodníkem	obchodník	k1gMnSc7	obchodník
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
šest	šest	k4xCc1	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
morfiu	morfium	k1gNnSc6	morfium
a	a	k8xC	a
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
uživit	uživit	k5eAaPmF	uživit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
postarat	postarat	k5eAaPmF	postarat
Clara	Clara	k1gFnSc1	Clara
</s>
</p>
<p>
<s>
Eugenie	Eugenie	k1gFnSc1	Eugenie
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
klavíristkou	klavíristka	k1gFnSc7	klavíristka
a	a	k8xC	a
učitelkou	učitelka	k1gFnSc7	učitelka
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
sepsala	sepsat	k5eAaPmAgFnS	sepsat
několik	několik	k4yIc4	několik
vzpomínkových	vzpomínkový	k2eAgFnPc2d1	vzpomínková
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
rodině	rodina	k1gFnSc6	rodina
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
literární	literární	k2eAgInPc4d1	literární
i	i	k8xC	i
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
na	na	k7c6	na
tuberkulózuV	tuberkulózuV	k?	tuberkulózuV
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
Schumann	Schumann	k1gMnSc1	Schumann
také	také	k9	také
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
skladatelský	skladatelský	k2eAgInSc4d1	skladatelský
rejstřík	rejstřík	k1gInSc4	rejstřík
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
přes	přes	k7c4	přes
130	[number]	k4	130
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odrážejí	odrážet	k5eAaImIp3nP	odrážet
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
Claře	Clara	k1gFnSc3	Clara
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
nejistá	jistý	k2eNgFnSc1d1	nejistá
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
vytoužený	vytoužený	k2eAgInSc4d1	vytoužený
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
tvorbě	tvorba	k1gFnSc3	tvorba
symfonií	symfonie	k1gFnPc2	symfonie
a	a	k8xC	a
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Skládání	skládání	k1gNnSc1	skládání
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
omezil	omezit	k5eAaPmAgMnS	omezit
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
těžkými	těžký	k2eAgFnPc7d1	těžká
depresemi	deprese	k1gFnPc7	deprese
a	a	k8xC	a
neurózami	neuróza	k1gFnPc7	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
na	na	k7c6	na
koncertním	koncertní	k2eAgNnSc6d1	koncertní
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
složil	složit	k5eAaPmAgMnS	složit
svou	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
operu	opera	k1gFnSc4	opera
Jenovéfa	Jenovéfa	k1gFnSc1	Jenovéfa
(	(	kIx(	(
<g/>
Genoveva	Genoveva	k1gFnSc1	Genoveva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
hudebního	hudební	k2eAgMnSc2d1	hudební
ředitele	ředitel	k1gMnSc2	ředitel
v	v	k7c6	v
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
působil	působit	k5eAaImAgMnS	působit
pouze	pouze	k6eAd1	pouze
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
dobrým	dobrý	k2eAgMnSc7d1	dobrý
dirigentem	dirigent	k1gMnSc7	dirigent
a	a	k8xC	a
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
efektivně	efektivně	k6eAd1	efektivně
řídit	řídit	k5eAaImF	řídit
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
Schumannovi	Schumannův	k2eAgMnPc1d1	Schumannův
poznali	poznat	k5eAaPmAgMnP	poznat
tehdy	tehdy	k6eAd1	tehdy
dvacetiletého	dvacetiletý	k2eAgNnSc2d1	dvacetileté
Johannese	Johannese	k1gFnPc4	Johannese
Brahmse	Brahms	k1gMnSc2	Brahms
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
a	a	k8xC	a
spřátelili	spřátelit	k5eAaPmAgMnP	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Brahms	Brahms	k1gMnSc1	Brahms
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
Claře	Clara	k1gFnSc3	Clara
s	s	k7c7	s
propagací	propagace	k1gFnSc7	propagace
Schumannova	Schumannův	k2eAgNnSc2d1	Schumannovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátily	vrátit	k5eAaPmAgFnP	vrátit
těžké	těžký	k2eAgInPc4d1	těžký
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
Schumann	Schumann	k1gMnSc1	Schumann
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skok	k1gInSc7	skok
do	do	k7c2	do
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zachráněn	zachráněn	k2eAgInSc1d1	zachráněn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
sám	sám	k3xTgMnSc1	sám
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
v	v	k7c6	v
Endenichu	Endenich	k1gInSc6	Endenich
poblíž	poblíž	k7c2	poblíž
Bonnu	Bonn	k1gInSc2	Bonn
strávil	strávit	k5eAaPmAgMnS	strávit
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
zde	zde	k6eAd1	zde
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
46	[number]	k4	46
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Schumann	Schumann	k1gNnSc1	Schumann
a	a	k8xC	a
Čechy	Čechy	k1gFnPc1	Čechy
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
pražského	pražský	k2eAgInSc2d1	pražský
virtuóza	virtuóza	k1gFnSc1	virtuóza
Ignáce	Ignác	k1gMnSc2	Ignác
Moschelese	Moschelese	k1gFnSc2	Moschelese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
Čechách	Čechy	k1gFnPc6	Čechy
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
měl	mít	k5eAaImAgInS	mít
společný	společný	k2eAgInSc1d1	společný
koncert	koncert	k1gInSc1	koncert
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Bedřichem	Bedřich	k1gMnSc7	Bedřich
Smetanou	Smetana	k1gMnSc7	Smetana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Složil	Složil	k1gMnSc1	Složil
přinejmenším	přinejmenším	k6eAd1	přinejmenším
183	[number]	k4	183
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
4	[number]	k4	4
symfonie	symfonie	k1gFnPc4	symfonie
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc4d1	mnohý
nástrojové	nástrojový	k2eAgInPc4d1	nástrojový
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
písňových	písňový	k2eAgInPc2d1	písňový
cyklů	cyklus	k1gInPc2	cyklus
<g/>
,	,	kIx,	,
kantáty	kantáta	k1gFnSc2	kantáta
<g/>
,	,	kIx,	,
oratoria	oratorium	k1gNnSc2	oratorium
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc4	sbor
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
vynikajících	vynikající	k2eAgNnPc2d1	vynikající
komorních	komorní	k2eAgNnPc2d1	komorní
děl	dělo	k1gNnPc2	dělo
<g/>
...	...	k?	...
Jeho	jeho	k3xOp3gFnSc1	jeho
pozdní	pozdní	k2eAgFnSc1d1	pozdní
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
duševní	duševní	k2eAgFnSc7d1	duševní
chorobou	choroba	k1gFnSc7	choroba
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
méněcennou	méněcenný	k2eAgFnSc7d1	méněcenná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
muzikology	muzikolog	k1gMnPc4	muzikolog
opakovaně	opakovaně	k6eAd1	opakovaně
vyvráceno	vyvrácen	k2eAgNnSc1d1	vyvráceno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
symfonie	symfonie	k1gFnSc1	symfonie
</s>
</p>
<p>
<s>
Karneval	karneval	k1gInSc1	karneval
</s>
</p>
<p>
<s>
Motýlek	motýlek	k1gMnSc1	motýlek
</s>
</p>
<p>
<s>
Dětské	dětský	k2eAgFnPc1d1	dětská
scény	scéna	k1gFnPc1	scéna
</s>
</p>
<p>
<s>
Eusebius	Eusebius	k1gMnSc1	Eusebius
</s>
</p>
<p>
<s>
Davidův	Davidův	k2eAgInSc1d1	Davidův
spolek	spolek	k1gInSc1	spolek
</s>
</p>
<p>
<s>
Dichterliebe	Dichterliebat	k5eAaPmIp3nS	Dichterliebat
</s>
</p>
<p>
<s>
Liederkreis	Liederkreis	k1gFnSc1	Liederkreis
</s>
</p>
<p>
<s>
Frauenliebe	Frauenliebat	k5eAaPmIp3nS	Frauenliebat
und	und	k?	und
Leben	Leben	k1gInSc1	Leben
</s>
</p>
<p>
<s>
oratorium	oratorium	k1gNnSc1	oratorium
Ráj	rája	k1gFnPc2	rája
a	a	k8xC	a
Peri	peri	k1gFnPc2	peri
(	(	kIx(	(
<g/>
Das	Das	k1gMnSc1	Das
Paradies	Paradies	k1gMnSc1	Paradies
und	und	k?	und
die	die	k?	die
Peri	peri	k1gFnSc2	peri
<g/>
)	)	kIx)	)
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
50	[number]	k4	50
<g/>
,	,	kIx,	,
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
Flechsig	Flechsig	k1gMnSc1	Flechsig
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
eposu	epos	k1gInSc2	epos
Lalla	Lalla	k1gMnSc1	Lalla
Rookh	Rookh	k1gMnSc1	Rookh
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
od	od	k7c2	od
Thomase	Thomas	k1gMnSc2	Thomas
Moora	Moor	k1gMnSc2	Moor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
tvorby	tvorba	k1gFnSc2	tvorba
k	k	k7c3	k
dědičnému	dědičný	k2eAgNnSc3d1	dědičné
zatížení	zatížení	k1gNnSc3	zatížení
==	==	k?	==
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
trpěl	trpět	k5eAaImAgMnS	trpět
maniodepresivní	maniodepresivní	k2eAgFnSc7d1	maniodepresivní
psychózou	psychóza	k1gFnSc7	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
roky	rok	k1gInPc1	rok
aktivní	aktivní	k2eAgFnSc2d1	aktivní
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
vždy	vždy	k6eAd1	vždy
opusová	opusový	k2eAgNnPc4d1	opusové
čísla	číslo	k1gNnPc4	číslo
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
roce	rok	k1gInSc6	rok
napsány	napsán	k2eAgFnPc1d1	napsána
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
tabulky	tabulka	k1gFnSc2	tabulka
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schumann	Schumann	k1gMnSc1	Schumann
komponoval	komponovat	k5eAaImAgMnS	komponovat
nejvíce	nejvíce	k6eAd1	nejvíce
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
hypománie	hypománie	k1gFnSc2	hypománie
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
depresi	deprese	k1gFnSc6	deprese
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
provázená	provázený	k2eAgFnSc1d1	provázená
dvěma	dva	k4xCgInPc7	dva
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
vynikají	vynikat	k5eAaImIp3nP	vynikat
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
tvorbou	tvorba	k1gFnSc7	tvorba
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mánie	mánie	k1gFnSc2	mánie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
rokem	rok	k1gInSc7	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Schumann	Schumann	k1gMnSc1	Schumann
přes	přes	k7c4	přes
krajní	krajní	k2eAgInSc4d1	krajní
odpor	odpor	k1gInSc4	odpor
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Wiecka	Wiecko	k1gNnSc2	Wiecko
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
otce	otka	k1gFnSc3	otka
své	svůj	k3xOyFgFnSc2	svůj
snoubenky	snoubenka	k1gFnSc2	snoubenka
Kláry	Klára	k1gFnSc2	Klára
<g/>
,	,	kIx,	,
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
sňatek	sňatek	k1gInSc4	sňatek
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaImNgInS	věnovat
především	především	k9	především
písni	píseň	k1gFnSc6	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
rok	rok	k1gInSc1	rok
vysoké	vysoký	k2eAgFnSc2d1	vysoká
aktivity	aktivita	k1gFnSc2	aktivita
1849	[number]	k4	1849
odráží	odrážet	k5eAaImIp3nS	odrážet
Schumannovo	Schumannův	k2eAgNnSc1d1	Schumannovo
přestěhování	přestěhování	k1gNnSc1	přestěhování
z	z	k7c2	z
Drážďan	Drážďany	k1gInPc2	Drážďany
do	do	k7c2	do
Düsseldorfu	Düsseldorf	k1gInSc2	Düsseldorf
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
de	de	k?	de
facto	facto	k1gNnSc4	facto
pokusu	pokus	k1gInSc2	pokus
začít	začít	k5eAaPmF	začít
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tvorby	tvorba	k1gFnSc2	tvorba
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
například	například	k6eAd1	například
klavírní	klavírní	k2eAgInSc1d1	klavírní
cyklus	cyklus	k1gInSc1	cyklus
Lesní	lesní	k2eAgFnSc2d1	lesní
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc2	jeho
rodiče	rodič	k1gMnPc1	rodič
trpěli	trpět	k5eAaImAgMnP	trpět
depresemi	deprese	k1gFnPc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
skladatelových	skladatelův	k2eAgMnPc2d1	skladatelův
synů	syn	k1gMnPc2	syn
strávil	strávit	k5eAaPmAgInS	strávit
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
nemocné	nemocný	k2eAgMnPc4d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
Opusová	opusový	k2eAgNnPc4d1	opusové
čísla	číslo	k1gNnPc4	číslo
skladeb	skladba	k1gFnPc2	skladba
</s>
</p>
<p>
<s>
1829	[number]	k4	1829
-	-	kIx~	-
007	[number]	k4	007
</s>
</p>
<p>
<s>
1830	[number]	k4	1830
-	-	kIx~	-
001	[number]	k4	001
</s>
</p>
<p>
<s>
1831	[number]	k4	1831
-	-	kIx~	-
008	[number]	k4	008
Allegro	allegro	k1gNnSc1	allegro
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
8	[number]	k4	8
</s>
</p>
<p>
<s>
1832	[number]	k4	1832
-	-	kIx~	-
002	[number]	k4	002
003	[number]	k4	003
004	[number]	k4	004
124	[number]	k4	124
</s>
</p>
<p>
<s>
1833	[number]	k4	1833
-	-	kIx~	-
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
-	-	kIx~	-
005	[number]	k4	005
010	[number]	k4	010
</s>
</p>
<p>
<s>
1834	[number]	k4	1834
-	-	kIx~	-
099	[number]	k4	099
</s>
</p>
<p>
<s>
1835	[number]	k4	1835
-	-	kIx~	-
009	[number]	k4	009
011	[number]	k4	011
022	[number]	k4	022
</s>
</p>
<p>
<s>
1836	[number]	k4	1836
-	-	kIx~	-
013	[number]	k4	013
014	[number]	k4	014
017	[number]	k4	017
</s>
</p>
<p>
<s>
1837	[number]	k4	1837
-	-	kIx~	-
006	[number]	k4	006
012	[number]	k4	012
</s>
</p>
<p>
<s>
1838	[number]	k4	1838
-	-	kIx~	-
015	[number]	k4	015
016	[number]	k4	016
018	[number]	k4	018
021	[number]	k4	021
032	[number]	k4	032
</s>
</p>
<p>
<s>
1839	[number]	k4	1839
-	-	kIx~	-
019	[number]	k4	019
020	[number]	k4	020
023	[number]	k4	023
028	[number]	k4	028
</s>
</p>
<p>
<s>
1840	[number]	k4	1840
-	-	kIx~	-
Hypománie	Hypománie	k1gFnSc2	Hypománie
-	-	kIx~	-
024	[number]	k4	024
025	[number]	k4	025
026	[number]	k4	026
027	[number]	k4	027
029	[number]	k4	029
030	[number]	k4	030
031	[number]	k4	031
033	[number]	k4	033
034	[number]	k4	034
035	[number]	k4	035
036	[number]	k4	036
039	[number]	k4	039
040	[number]	k4	040
042	[number]	k4	042
043	[number]	k4	043
045	[number]	k4	045
048	[number]	k4	048
049	[number]	k4	049
051	[number]	k4	051
053	[number]	k4	053
057	[number]	k4	057
077	[number]	k4	077
127	[number]	k4	127
142	[number]	k4	142
</s>
</p>
<p>
<s>
1841	[number]	k4	1841
-	-	kIx~	-
037	[number]	k4	037
038	[number]	k4	038
052	[number]	k4	052
054	[number]	k4	054
064	[number]	k4	064
120	[number]	k4	120
</s>
</p>
<p>
<s>
1842	[number]	k4	1842
-	-	kIx~	-
041	[number]	k4	041
044	[number]	k4	044
047	[number]	k4	047
</s>
</p>
<p>
<s>
1843	[number]	k4	1843
-	-	kIx~	-
046	[number]	k4	046
050	[number]	k4	050
</s>
</p>
<p>
<s>
1844	[number]	k4	1844
-	-	kIx~	-
Těžká	těžký	k2eAgFnSc1d1	těžká
deprese	deprese	k1gFnSc1	deprese
</s>
</p>
<p>
<s>
1845	[number]	k4	1845
-	-	kIx~	-
055	[number]	k4	055
056	[number]	k4	056
058	[number]	k4	058
060	[number]	k4	060
072	[number]	k4	072
</s>
</p>
<p>
<s>
1846	[number]	k4	1846
-	-	kIx~	-
059	[number]	k4	059
061	[number]	k4	061
</s>
</p>
<p>
<s>
1847	[number]	k4	1847
-	-	kIx~	-
062	[number]	k4	062
063	[number]	k4	063
065	[number]	k4	065
080	[number]	k4	080
084	[number]	k4	084
</s>
</p>
<p>
<s>
1848	[number]	k4	1848
-	-	kIx~	-
066	[number]	k4	066
068	[number]	k4	068
071	[number]	k4	071
081	[number]	k4	081
115	[number]	k4	115
</s>
</p>
<p>
<s>
1849	[number]	k4	1849
-	-	kIx~	-
Hypománie	Hypománie	k1gFnSc2	Hypománie
-	-	kIx~	-
067	[number]	k4	067
069	[number]	k4	069
070	[number]	k4	070
073	[number]	k4	073
074	[number]	k4	074
075	[number]	k4	075
076	[number]	k4	076
078	[number]	k4	078
079	[number]	k4	079
082	[number]	k4	082
085	[number]	k4	085
086	[number]	k4	086
091	[number]	k4	091
092	[number]	k4	092
093	[number]	k4	093
094	[number]	k4	094
095	[number]	k4	095
098	[number]	k4	098
101	[number]	k4	101
102	[number]	k4	102
106	[number]	k4	106
108	[number]	k4	108
137	[number]	k4	137
138	[number]	k4	138
141	[number]	k4	141
145	[number]	k4	145
146	[number]	k4	146
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
-	-	kIx~	-
083	[number]	k4	083
087	[number]	k4	087
088	[number]	k4	088
089	[number]	k4	089
090	[number]	k4	090
096	[number]	k4	096
097	[number]	k4	097
125	[number]	k4	125
129	[number]	k4	129
130	[number]	k4	130
144	[number]	k4	144
</s>
</p>
<p>
<s>
1851	[number]	k4	1851
-	-	kIx~	-
100	[number]	k4	100
103	[number]	k4	103
104	[number]	k4	104
105	[number]	k4	105
107	[number]	k4	107
109	[number]	k4	109
110	[number]	k4	110
111	[number]	k4	111
112	[number]	k4	112
113	[number]	k4	113
117	[number]	k4	117
119	[number]	k4	119
121	[number]	k4	121
128	[number]	k4	128
136	[number]	k4	136
</s>
</p>
<p>
<s>
1852	[number]	k4	1852
-	-	kIx~	-
122	[number]	k4	122
135	[number]	k4	135
139	[number]	k4	139
140	[number]	k4	140
147	[number]	k4	147
148	[number]	k4	148
</s>
</p>
<p>
<s>
1853	[number]	k4	1853
-	-	kIx~	-
114	[number]	k4	114
118	[number]	k4	118
123	[number]	k4	123
126	[number]	k4	126
131	[number]	k4	131
132	[number]	k4	132
133	[number]	k4	133
134	[number]	k4	134
143	[number]	k4	143
</s>
</p>
<p>
<s>
1854	[number]	k4	1854
-	-	kIx~	-
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
</s>
</p>
<p>
<s>
1856	[number]	k4	1856
-	-	kIx~	-
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Robertu	Robert	k1gMnSc6	Robert
Schumannovi	Schumann	k1gMnSc6	Schumann
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
pár	pár	k1gInSc1	pár
dálkových	dálkový	k2eAgInPc2d1	dálkový
vlaků	vlak	k1gInPc2	vlak
společností	společnost	k1gFnPc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
Deutsche	Deutsch	k1gFnSc2	Deutsch
Bahn	Bahna	k1gFnPc2	Bahna
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Dresden	Dresdna	k1gFnPc2	Dresdna
-	-	kIx~	-
Berlin	berlina	k1gFnPc2	berlina
-	-	kIx~	-
Hamburg-Altona	Hamburg-Altona	k1gFnSc1	Hamburg-Altona
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gNnSc4	Schumann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaImAgMnS	dít
od	od	k7c2	od
R.	R.	kA	R.
Schumanna	Schumanen	k2eAgFnSc1d1	Schumanen
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
