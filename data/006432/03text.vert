<s>
Městečko	městečko	k1gNnSc1	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Twin	Twin	k1gMnSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
dramatický	dramatický	k2eAgInSc1d1	dramatický
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
a	a	k8xC	a
Mark	Mark	k1gMnSc1	Mark
Frost	Frost	k1gMnSc1	Frost
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
sleduje	sledovat	k5eAaImIp3nS	sledovat
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
brutální	brutální	k2eAgFnSc2d1	brutální
vraždy	vražda	k1gFnSc2	vražda
populární	populární	k2eAgFnSc2d1	populární
dívky	dívka	k1gFnSc2	dívka
a	a	k8xC	a
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
Laury	Laura	k1gFnSc2	Laura
Palmerové	Palmerová	k1gFnSc2	Palmerová
(	(	kIx(	(
<g/>
Sheryl	Sheryl	k1gInSc1	Sheryl
Lee	Lea	k1gFnSc3	Lea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
speciální	speciální	k2eAgMnSc1d1	speciální
agent	agent	k1gMnSc1	agent
FBI	FBI	kA	FBI
Dale	Dale	k1gFnSc1	Dale
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
Kyle	Kyle	k1gFnSc1	Kyle
MacLachlan	MacLachlan	k1gMnSc1	MacLachlan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
Městečka	městečko	k1gNnSc2	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vysílán	vysílat	k5eAaImNgInS	vysílat
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
ABC	ABC	kA	ABC
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
dalších	další	k2eAgInPc2d1	další
sedm	sedm	k4xCc4	sedm
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
seriálu	seriál	k1gInSc2	seriál
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
zasazení	zasazení	k1gNnSc2	zasazení
do	do	k7c2	do
malého	malý	k2eAgNnSc2d1	malé
fiktivního	fiktivní	k2eAgNnSc2d1	fiktivní
městečka	městečko	k1gNnSc2	městečko
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Exteriéry	exteriér	k1gInPc1	exteriér
byly	být	k5eAaImAgInP	být
natočeny	natočit	k5eAaBmNgInP	natočit
v	v	k7c6	v
městech	město	k1gNnPc6	město
Snoqualmie	Snoqualmie	k1gFnSc2	Snoqualmie
a	a	k8xC	a
North	North	k1gMnSc1	North
Bend	Bend	k1gMnSc1	Bend
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
interiérů	interiér	k1gInPc2	interiér
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
v	v	k7c6	v
San	San	k1gFnSc6	San
Fernando	Fernanda	k1gFnSc5	Fernanda
Valley	Valle	k2eAgMnPc4d1	Valle
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
hodnocených	hodnocený	k2eAgInPc2d1	hodnocený
seriálů	seriál	k1gInPc2	seriál
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
s	s	k7c7	s
příznivými	příznivý	k2eAgInPc7d1	příznivý
kritickými	kritický	k2eAgInPc7d1	kritický
ohlasy	ohlas	k1gInPc7	ohlas
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
širokou	široký	k2eAgFnSc4d1	široká
základnu	základna	k1gFnSc4	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
popkultury	popkultury	k?	popkultury
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
odkazů	odkaz	k1gInPc2	odkaz
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
,	,	kIx,	,
reklamách	reklama	k1gFnPc6	reklama
<g/>
,	,	kIx,	,
komiksech	komiks	k1gInPc6	komiks
<g/>
,	,	kIx,	,
videohrách	videohra	k1gFnPc6	videohra
<g/>
,	,	kIx,	,
filmech	film	k1gInPc6	film
a	a	k8xC	a
textech	text	k1gInPc6	text
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Snižující	snižující	k2eAgFnSc1d1	snižující
se	se	k3xPyFc4	se
divácká	divácký	k2eAgFnSc1d1	divácká
sledovanost	sledovanost	k1gFnSc1	sledovanost
vedla	vést	k5eAaImAgFnS	vést
ABC	ABC	kA	ABC
k	k	k7c3	k
nátlaku	nátlak	k1gInSc3	nátlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
totožnost	totožnost	k1gFnSc4	totožnost
Lauřina	Lauřin	k2eAgMnSc2d1	Lauřin
vraha	vrah	k1gMnSc2	vrah
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
již	již	k6eAd1	již
uprostřed	uprostřed	k7c2	uprostřed
druhé	druhý	k4xOgFnSc2	druhý
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
odhalení	odhalení	k1gNnSc1	odhalení
však	však	k9	však
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
střetem	střet	k1gInSc7	střet
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
seriály	seriál	k1gInPc7	seriál
vysílanými	vysílaný	k2eAgInPc7d1	vysílaný
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgInSc3d2	veliký
propadu	propad	k1gInSc3	propad
ve	v	k7c6	v
sledovanosti	sledovanost	k1gFnSc6	sledovanost
a	a	k8xC	a
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
celého	celý	k2eAgInSc2d1	celý
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
celovečerní	celovečerní	k2eAgInSc1d1	celovečerní
film	film	k1gInSc1	film
Twin	Twin	k1gNnSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
prequel	prequel	k1gInSc4	prequel
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
však	však	k9	však
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
nebyl	být	k5eNaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
získá	získat	k5eAaPmIp3nS	získat
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
seriálu	seriál	k1gInSc2	seriál
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
Frost	Frost	k1gInSc4	Frost
a	a	k8xC	a
Lynch	Lynch	k1gInSc4	Lynch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Showtime	Showtim	k1gInSc5	Showtim
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Twin	Twin	k1gNnSc1	Twin
Peaks	Peaks	k1gInSc4	Peaks
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
dřevorubec	dřevorubec	k1gMnSc1	dřevorubec
Pete	Pet	k1gInSc2	Pet
Martell	Martell	k1gMnSc1	Martell
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
nahé	nahý	k2eAgNnSc1d1	nahé
tělo	tělo	k1gNnSc1	tělo
pečlivě	pečlivě	k6eAd1	pečlivě
zabalené	zabalený	k2eAgNnSc1d1	zabalené
v	v	k7c6	v
igelitu	igelit	k1gInSc6	igelit
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
příjezd	příjezd	k1gInSc4	příjezd
šerifa	šerif	k1gMnSc2	šerif
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Trumana	Truman	k1gMnSc2	Truman
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
zástupců	zástupce	k1gMnPc2	zástupce
a	a	k8xC	a
doktora	doktor	k1gMnSc4	doktor
Willa	Will	k1gMnSc4	Will
Haywarda	Hayward	k1gMnSc4	Hayward
<g/>
,	,	kIx,	,
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
patří	patřit	k5eAaImIp3nS	patřit
místní	místní	k2eAgFnSc3d1	místní
středoškolské	středoškolský	k2eAgFnSc3d1	středoškolská
krasavici	krasavice	k1gFnSc3	krasavice
Lauře	Laura	k1gFnSc3	Laura
Palmerové	Palmerová	k1gFnSc3	Palmerová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
nevinnosti	nevinnost	k1gFnSc2	nevinnost
a	a	k8xC	a
mravní	mravní	k2eAgFnSc2d1	mravní
čistoty	čistota	k1gFnSc2	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
především	především	k9	především
mezi	mezi	k7c7	mezi
Lauřinou	Lauřin	k2eAgFnSc7d1	Lauřina
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
nalezena	nalezen	k2eAgFnSc1d1	nalezena
další	další	k2eAgFnSc1d1	další
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
Ronette	Ronett	k1gInSc5	Ronett
Pulaskiová	Pulaskiový	k2eAgFnSc1d1	Pulaskiový
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bez	bez	k7c2	bez
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
nepříčetně	příčetně	k6eNd1	příčetně
bloudí	bloudit	k5eAaImIp3nP	bloudit
podél	podél	k7c2	podél
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Ronette	Ronett	k1gInSc5	Ronett
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
případu	případ	k1gInSc2	případ
povolán	povolán	k2eAgMnSc1d1	povolán
agent	agent	k1gMnSc1	agent
FBI	FBI	kA	FBI
Dale	Dale	k1gFnSc1	Dale
Cooper	Cooper	k1gInSc1	Cooper
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
při	při	k7c6	při
prvotní	prvotní	k2eAgFnSc6d1	prvotní
prohlídce	prohlídka	k1gFnSc6	prohlídka
těla	tělo	k1gNnSc2	tělo
odhalí	odhalit	k5eAaPmIp3nS	odhalit
malý	malý	k2eAgInSc1d1	malý
papírek	papírek	k1gInSc1	papírek
s	s	k7c7	s
písmenem	písmeno	k1gNnSc7	písmeno
R	R	kA	R
ukrytý	ukrytý	k2eAgInSc1d1	ukrytý
pod	pod	k7c7	pod
nehtem	nehet	k1gInSc7	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
městském	městský	k2eAgNnSc6d1	Městské
shromáždění	shromáždění	k1gNnSc6	shromáždění
ještě	ještě	k9	ještě
téže	tenže	k3xDgFnSc2	tenže
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
Cooper	Cooper	k1gMnSc1	Cooper
informuje	informovat	k5eAaBmIp3nS	informovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
že	že	k8xS	že
styl	styl	k1gInSc1	styl
provedení	provedení	k1gNnSc2	provedení
vraždy	vražda	k1gFnSc2	vražda
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
vrahem	vrah	k1gMnSc7	vrah
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
jinou	jiný	k2eAgFnSc4d1	jiná
dívku	dívka	k1gFnSc4	dívka
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
a	a	k8xC	a
důkazy	důkaz	k1gInPc1	důkaz
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
vrah	vrah	k1gMnSc1	vrah
žije	žít	k5eAaImIp3nS	žít
v	v	k7c4	v
Twin	Twin	k1gNnSc4	Twin
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
.	.	kIx.	.
</s>
<s>
Cooperovo	Cooperův	k2eAgNnSc1d1	Cooperovo
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
brzy	brzy	k6eAd1	brzy
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Laura	Laura	k1gFnSc1	Laura
vedla	vést	k5eAaImAgFnS	vést
dvojí	dvojí	k4xRgInSc4	dvojí
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podváděla	podvádět	k5eAaImAgFnS	podvádět
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
fotbalového	fotbalový	k2eAgMnSc4d1	fotbalový
kapitána	kapitán	k1gMnSc4	kapitán
Bobbyho	Bobby	k1gMnSc4	Bobby
Briggse	Briggs	k1gMnSc4	Briggs
<g/>
,	,	kIx,	,
s	s	k7c7	s
motorkářem	motorkář	k1gMnSc7	motorkář
Jamesem	James	k1gMnSc7	James
Hurleym	Hurleym	k1gInSc4	Hurleym
<g/>
,	,	kIx,	,
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
prostituci	prostituce	k1gFnSc3	prostituce
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Lea	Leo	k1gMnSc2	Leo
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
,	,	kIx,	,
místního	místní	k2eAgMnSc2d1	místní
řidiče	řidič	k1gMnSc2	řidič
náklaďáku	náklaďák	k1gInSc2	náklaďák
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jacquese	Jacques	k1gMnSc4	Jacques
Renaulta	Renault	k1gMnSc4	Renault
<g/>
,	,	kIx,	,
pasáka	pasák	k1gMnSc4	pasák
a	a	k8xC	a
dealera	dealer	k1gMnSc4	dealer
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Laura	Laura	k1gFnSc1	Laura
stala	stát	k5eAaPmAgFnS	stát
závislou	závislý	k2eAgFnSc4d1	závislá
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobil	způsobit	k5eAaPmAgMnS	způsobit
její	její	k3xOp3gMnSc1	její
přítel	přítel	k1gMnSc1	přítel
Bobby	Bobba	k1gFnSc2	Bobba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
prováděl	provádět	k5eAaImAgInS	provádět
obchody	obchod	k1gInPc4	obchod
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Renaultem	renault	k1gInSc7	renault
<g/>
.	.	kIx.	.
</s>
<s>
Lauřina	Lauřin	k2eAgFnSc1d1	Lauřina
smrt	smrt	k1gFnSc1	smrt
spouští	spouštět	k5eAaImIp3nS	spouštět
řetězec	řetězec	k1gInSc1	řetězec
událostí	událost	k1gFnPc2	událost
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Lauřin	Lauřin	k2eAgMnSc1d1	Lauřin
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Leland	Leland	k1gInSc1	Leland
Palmer	Palmer	k1gInSc1	Palmer
<g/>
,	,	kIx,	,
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
<g/>
,	,	kIx,	,
Donna	donna	k1gFnSc1	donna
Haywardová	Haywardová	k1gFnSc1	Haywardová
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Lauřiny	Lauřin	k2eAgFnSc2d1	Lauřina
sestřenice	sestřenice	k1gFnSc2	sestřenice
<g/>
,	,	kIx,	,
Maddy	Madda	k1gFnSc2	Madda
Fergusonové	Fergusonová	k1gFnSc2	Fergusonová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
do	do	k7c2	do
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Lauřina	Lauřin	k2eAgMnSc2d1	Lauřin
psychiatra	psychiatr	k1gMnSc2	psychiatr
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Jacobyho	Jacoby	k1gMnSc2	Jacoby
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgMnSc6	jenž
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Laurou	Laura	k1gFnSc7	Laura
posedlý	posedlý	k2eAgMnSc1d1	posedlý
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nevinný	vinný	k2eNgMnSc1d1	nevinný
<g/>
,	,	kIx,	,
a	a	k8xC	a
plán	plán	k1gInSc1	plán
na	na	k7c4	na
vniknutí	vniknutí	k1gNnSc4	vniknutí
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
bytu	byt	k1gInSc2	byt
končí	končit	k5eAaImIp3nS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Jacoby	Jacoba	k1gFnSc2	Jacoba
napaden	napadnout	k5eAaPmNgInS	napadnout
v	v	k7c6	v
parku	park	k1gInSc6	park
a	a	k8xC	a
hospitalizován	hospitalizovat	k5eAaBmNgMnS	hospitalizovat
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
paměti	paměť	k1gFnSc2	paměť
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
zápach	zápach	k1gInSc4	zápach
spáleného	spálený	k2eAgInSc2d1	spálený
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Horne	Horn	k1gMnSc5	Horn
<g/>
,	,	kIx,	,
nejbohatší	bohatý	k2eAgMnSc1d3	nejbohatší
muž	muž	k1gMnSc1	muž
v	v	k7c4	v
Twin	Twin	k1gNnSc4	Twin
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
,	,	kIx,	,
spouští	spouštět	k5eAaImIp3nS	spouštět
poslední	poslední	k2eAgFnSc4d1	poslední
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
místní	místní	k2eAgFnSc2d1	místní
pily	pila	k1gFnSc2	pila
a	a	k8xC	a
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
Catherine	Catherin	k1gInSc5	Catherin
Martellové	Martellové	k2eAgInSc3d1	Martellové
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
ji	on	k3xPp3gFnSc4	on
vede	vést	k5eAaImIp3nS	vést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
nakoupit	nakoupit	k5eAaPmF	nakoupit
pozemky	pozemek	k1gInPc4	pozemek
za	za	k7c4	za
sníženou	snížený	k2eAgFnSc4d1	snížená
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
upevnit	upevnit	k5eAaPmF	upevnit
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
ekonomického	ekonomický	k2eAgMnSc2d1	ekonomický
vládce	vládce	k1gMnSc2	vládce
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
narůstající	narůstající	k2eAgFnSc1d1	narůstající
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
začne	začít	k5eAaPmIp3nS	začít
trápit	trápit	k5eAaImF	trápit
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
Audrey	Audre	k2eAgInPc4d1	Audre
Horneovou	Horneův	k2eAgFnSc7d1	Horneův
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
agenta	agent	k1gMnSc2	agent
Coopera	Cooper	k1gMnSc2	Cooper
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohla	pomoct	k5eAaPmAgFnS	pomoct
vyřešit	vyřešit	k5eAaPmF	vyřešit
Lauřinu	Lauřin	k2eAgFnSc4d1	Lauřina
vraždu	vražda	k1gFnSc4	vražda
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gFnSc4	jeho
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
noc	noc	k1gFnSc4	noc
ve	v	k7c6	v
městě	město	k1gNnSc6	město
dostane	dostat	k5eAaPmIp3nS	dostat
Cooper	Cooper	k1gMnSc1	Cooper
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
potká	potkat	k5eAaPmIp3nS	potkat
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c4	v
Twin	Twin	k1gNnSc4	Twin
Peaks	Peaksa	k1gFnPc2	Peaksa
jednorukého	jednoruký	k2eAgMnSc2d1	jednoruký
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
říká	říkat	k5eAaImIp3nS	říkat
Mike	Mike	k1gInSc4	Mike
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k6eAd1	Mike
se	se	k3xPyFc4	se
přestaví	přestavit	k5eAaPmIp3nS	přestavit
jako	jako	k9	jako
nadpozemská	nadpozemský	k2eAgFnSc1d1	nadpozemská
bytost	bytost	k1gFnSc1	bytost
a	a	k8xC	a
sdělí	sdělit	k5eAaPmIp3nS	sdělit
Cooperovi	Cooper	k1gMnSc3	Cooper
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lauřiným	Lauřin	k2eAgMnSc7d1	Lauřin
vrahem	vrah	k1gMnSc7	vrah
je	být	k5eAaImIp3nS	být
Zabiják	zabiják	k1gMnSc1	zabiják
Bob	Bob	k1gMnSc1	Bob
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
nadpozemská	nadpozemský	k2eAgFnSc1d1	nadpozemská
postava	postava	k1gFnSc1	postava
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
pak	pak	k6eAd1	pak
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
Boba	Bob	k1gMnSc2	Bob
<g/>
,	,	kIx,	,
krutého	krutý	k2eAgMnSc2d1	krutý
šedovlasého	šedovlasý	k2eAgMnSc2d1	šedovlasý
muže	muž	k1gMnSc2	muž
v	v	k7c6	v
džínové	džínový	k2eAgFnSc6d1	džínová
bundě	bunda	k1gFnSc6	bunda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
zabíjení	zabíjení	k1gNnSc6	zabíjení
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
poté	poté	k6eAd1	poté
vidí	vidět	k5eAaImIp3nS	vidět
své	své	k1gNnSc4	své
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
starší	starý	k2eAgNnSc4d2	starší
já	já	k3xPp1nSc1	já
sedící	sedící	k2eAgFnSc7d1	sedící
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
obklopené	obklopený	k2eAgFnSc6d1	obklopená
červenými	červený	k2eAgInPc7d1	červený
závěsy	závěs	k1gInPc7	závěs
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
dojem	dojem	k1gInSc4	dojem
nadpozemského	nadpozemský	k2eAgNnSc2d1	nadpozemské
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
němu	on	k3xPp3gNnSc3	on
stojí	stát	k5eAaImIp3nS	stát
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
obleku	oblek	k1gInSc6	oblek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Laura	Laura	k1gFnSc1	Laura
Palmer	Palmra	k1gFnPc2	Palmra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
muž	muž	k1gMnSc1	muž
představí	představit	k5eAaPmIp3nS	představit
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
sestřenici	sestřenice	k1gFnSc4	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
zjevně	zjevně	k6eAd1	zjevně
zašifrovaný	zašifrovaný	k2eAgInSc1d1	zašifrovaný
dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
Cooperem	Cooper	k1gInSc7	Cooper
<g/>
,	,	kIx,	,
vstane	vstát	k5eAaPmIp3nS	vstát
muž	muž	k1gMnSc1	muž
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
židle	židle	k1gFnSc2	židle
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
tancovat	tancovat	k5eAaImF	tancovat
okolo	okolo	k7c2	okolo
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Laura	Laura	k1gFnSc1	Laura
něco	něco	k3yInSc4	něco
zašeptá	zašeptat	k5eAaPmIp3nS	zašeptat
Cooperovi	Cooper	k1gMnSc3	Cooper
do	do	k7c2	do
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc4d1	následující
ráno	ráno	k1gNnSc4	ráno
se	se	k3xPyFc4	se
Cooper	Cooper	k1gMnSc1	Cooper
schází	scházet	k5eAaImIp3nS	scházet
se	s	k7c7	s
šerifem	šerif	k1gMnSc7	šerif
Trumanem	Truman	k1gMnSc7	Truman
<g/>
,	,	kIx,	,
popíše	popsat	k5eAaPmIp3nS	popsat
mu	on	k3xPp3gMnSc3	on
svůj	svůj	k3xOyFgInSc4	svůj
sen	sen	k1gInSc4	sen
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
rozšifrovat	rozšifrovat	k5eAaPmF	rozšifrovat
symboly	symbol	k1gInPc4	symbol
ze	z	k7c2	z
snu	sen	k1gInSc2	sen
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zabil	zabít	k5eAaPmAgMnS	zabít
Lauru	Laura	k1gFnSc4	Laura
<g/>
.	.	kIx.	.
</s>
<s>
Cooperovi	Cooper	k1gMnSc3	Cooper
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
policejním	policejní	k2eAgNnSc7d1	policejní
oddělením	oddělení	k1gNnSc7	oddělení
Twin	Twino	k1gNnPc2	Twino
Peaks	Peaks	k1gInSc4	Peaks
nalézt	nalézt	k5eAaBmF	nalézt
jednorukého	jednoruký	k2eAgMnSc4d1	jednoruký
muže	muž	k1gMnSc4	muž
z	z	k7c2	z
Cooperova	Cooperův	k2eAgInSc2d1	Cooperův
snu	sen	k1gInSc2	sen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
být	být	k5eAaImF	být
cestujícím	cestující	k2eAgMnSc7d1	cestující
prodejcem	prodejce	k1gMnSc7	prodejce
jménem	jméno	k1gNnSc7	jméno
Philip	Philip	k1gMnSc1	Philip
Gerard	Gerard	k1gMnSc1	Gerard
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Coopra	k1gFnPc2	Coopra
se	se	k3xPyFc4	se
Gerarda	Gerarda	k1gFnSc1	Gerarda
ptá	ptat	k5eAaImIp3nS	ptat
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gerard	Gerard	k1gInSc1	Gerard
opravdu	opravdu	k6eAd1	opravdu
zná	znát	k5eAaImIp3nS	znát
Boba	Bob	k1gMnSc4	Bob
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
veterinářem	veterinář	k1gMnSc7	veterinář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
léčil	léčit	k5eAaImAgMnS	léčit
ochočeného	ochočený	k2eAgMnSc4d1	ochočený
ptáka	pták	k1gMnSc4	pták
Jacquese	Jacques	k1gMnSc2	Jacques
Renaulta	Renault	k1gMnSc2	Renault
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
dojde	dojít	k5eAaPmIp3nS	dojít
Cooper	Cooper	k1gInSc1	Cooper
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Renault	renault	k1gInSc1	renault
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgMnSc7	ten
hledaným	hledaný	k2eAgMnSc7d1	hledaný
vrahem	vrah	k1gMnSc7	vrah
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
Trumanovou	Trumanův	k2eAgFnSc7d1	Trumanova
pomocí	pomoc	k1gFnSc7	pomoc
vystopuje	vystopovat	k5eAaPmIp3nS	vystopovat
Renaulta	Renaulta	k1gFnSc1	Renaulta
do	do	k7c2	do
nevěstince	nevěstinec	k1gInSc2	nevěstinec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vlastní	vlastní	k2eAgInSc1d1	vlastní
Ben	Ben	k1gInSc1	Ben
Horne	Horn	k1gMnSc5	Horn
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setká	setkat	k5eAaPmIp3nS	setkat
a	a	k8xC	a
vyláká	vylákat	k5eAaPmIp3nS	vylákat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
u	u	k7c2	u
čističky	čistička	k1gFnSc2	čistička
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Renault	renault	k1gInSc1	renault
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
zatčení	zatčení	k1gNnSc2	zatčení
u	u	k7c2	u
čističky	čistička	k1gFnSc2	čistička
postřelen	postřelen	k2eAgMnSc1d1	postřelen
a	a	k8xC	a
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
Leland	Leland	k1gInSc1	Leland
Palmer	Palmra	k1gFnPc2	Palmra
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Renault	renault	k1gInSc1	renault
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
<g/>
,	,	kIx,	,
proklouzne	proklouznout	k5eAaPmIp3nS	proklouznout
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
nemocničního	nemocniční	k2eAgInSc2d1	nemocniční
pokoje	pokoj	k1gInSc2	pokoj
a	a	k8xC	a
zavraždí	zavraždit	k5eAaPmIp3nP	zavraždit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
nařídí	nařídit	k5eAaPmIp3nS	nařídit
Ben	Ben	k1gInSc1	Ben
Horne	Horn	k1gMnSc5	Horn
Leovi	Leo	k1gMnSc3	Leo
aby	aby	kYmCp3nS	aby
zapálil	zapálit	k5eAaPmAgMnS	zapálit
pilu	pila	k1gFnSc4	pila
i	i	k9	i
s	s	k7c7	s
Catherine	Catherin	k1gInSc5	Catherin
uvězněnou	uvězněný	k2eAgFnSc4d1	uvězněná
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
však	však	k9	však
poté	poté	k6eAd1	poté
pošle	poslat	k5eAaPmIp3nS	poslat
k	k	k7c3	k
Leovi	Leo	k1gMnSc3	Leo
nájemného	nájemné	k1gNnSc2	nájemné
zabijáka	zabiják	k1gMnSc4	zabiják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujistil	ujistit	k5eAaPmAgMnS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jacquesově	Jacquesův	k2eAgNnSc6d1	Jacquesův
zatčení	zatčení	k1gNnSc6	zatčení
se	se	k3xPyFc4	se
Cooper	Cooper	k1gInSc1	Cooper
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
několikrát	několikrát	k6eAd1	několikrát
postřelen	postřelit	k5eAaPmNgMnS	postřelit
zamaskovaným	zamaskovaný	k2eAgMnSc7d1	zamaskovaný
střelcem	střelec	k1gMnSc7	střelec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
končí	končit	k5eAaImIp3nS	končit
celá	celý	k2eAgFnSc1d1	celá
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
Cooper	Cooper	k1gMnSc1	Cooper
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
svého	svůj	k3xOyFgInSc2	svůj
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
zraněném	zraněný	k2eAgInSc6d1	zraněný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
jen	jen	k9	jen
napůl	napůl	k6eAd1	napůl
při	při	k7c6	při
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
,	,	kIx,	,
prožije	prožít	k5eAaPmIp3nS	prožít
Cooper	Cooper	k1gMnSc1	Cooper
vizi	vize	k1gFnSc4	vize
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zjeví	zjevit	k5eAaPmIp3nS	zjevit
Obří	obří	k2eAgMnSc1d1	obří
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Obr	obr	k1gMnSc1	obr
agentu	agent	k1gMnSc3	agent
Cooperovi	Cooper	k1gMnSc3	Cooper
prozradí	prozradit	k5eAaPmIp3nP	prozradit
tři	tři	k4xCgFnPc4	tři
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Narazíte	narazit	k5eAaPmIp2nP	narazit
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
ve	v	k7c6	v
smějícím	smějící	k2eAgInSc6d1	smějící
se	se	k3xPyFc4	se
pytli	pytel	k1gInSc3	pytel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sovy	sova	k1gFnPc1	sova
nejsou	být	k5eNaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
chemikálií	chemikálie	k1gFnPc2	chemikálie
vám	vy	k3xPp2nPc3	vy
on	on	k3xPp3gMnSc1	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
sdělí	sdělit	k5eAaPmIp3nP	sdělit
"	"	kIx"	"
<g/>
Budete	být	k5eAaImBp2nP	být
potřebovat	potřebovat	k5eAaImF	potřebovat
lékařské	lékařský	k2eAgNnSc4d1	lékařské
ošetření	ošetření	k1gNnSc4	ošetření
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obr	obr	k1gMnSc1	obr
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
vezme	vzít	k5eAaPmIp3nS	vzít
Cooperův	Cooperův	k2eAgInSc4d1	Cooperův
zlatý	zlatý	k2eAgInSc4d1	zlatý
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
navrácen	navrácen	k2eAgMnSc1d1	navrácen
<g/>
,	,	kIx,	,
až	až	k8xS	až
Cooper	Cooper	k1gMnSc1	Cooper
všechna	všechen	k3xTgNnPc4	všechen
tři	tři	k4xCgFnPc4	tři
jeho	jeho	k3xOp3gInSc6	jeho
odhalení	odhalení	k1gNnSc1	odhalení
správně	správně	k6eAd1	správně
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Leo	Leo	k1gMnSc1	Leo
Johnson	Johnson	k1gMnSc1	Johnson
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
operaci	operace	k1gFnSc4	operace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
přežije	přežít	k5eAaPmIp3nS	přežít
svá	svůj	k3xOyFgNnPc4	svůj
střelná	střelný	k2eAgNnPc4d1	střelné
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
těžce	těžce	k6eAd1	těžce
postiženým	postižený	k2eAgFnPc3d1	postižená
<g/>
.	.	kIx.	.
</s>
<s>
Catherine	Catherin	k1gInSc5	Catherin
Martellové	Martellové	k2eAgInSc1d1	Martellové
se	se	k3xPyFc4	se
také	také	k9	také
podaří	podařit	k5eAaPmIp3nS	podařit
přežít	přežít	k5eAaPmF	přežít
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaPmIp3nS	využívat
této	tento	k3xDgFnSc3	tento
příležitosti	příležitost	k1gFnSc3	příležitost
a	a	k8xC	a
předstírá	předstírat	k5eAaImIp3nS	předstírat
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravila	připravit	k5eAaPmAgFnS	připravit
odplatu	odplata	k1gFnSc4	odplata
Benu	Ben	k1gInSc2	Ben
Horneovi	Horneus	k1gMnSc3	Horneus
<g/>
.	.	kIx.	.
</s>
<s>
Leland	Leland	k1gInSc1	Leland
Palmer	Palmra	k1gFnPc2	Palmra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
vlasy	vlas	k1gInPc1	vlas
zbělaly	zbělat	k5eAaPmAgInP	zbělat
doslova	doslova	k6eAd1	doslova
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
Renaultově	Renaultův	k2eAgFnSc6d1	Renaultův
smrti	smrt	k1gFnSc6	smrt
vrací	vracet	k5eAaImIp3nS	vracet
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
očividně	očividně	k6eAd1	očividně
zcela	zcela	k6eAd1	zcela
zotaven	zotavit	k5eAaPmNgMnS	zotavit
Renaultovou	Renaultový	k2eAgFnSc7d1	Renaultový
vraždou	vražda	k1gFnSc7	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Philip	Philip	k1gMnSc1	Philip
Gerard	Gerard	k1gMnSc1	Gerard
je	být	k5eAaImIp3nS	být
hostitelem	hostitel	k1gMnSc7	hostitel
Mikea	Mike	k1gInSc2	Mike
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
být	být	k5eAaImF	být
démonickým	démonický	k2eAgMnSc7d1	démonický
duchem	duch	k1gMnSc7	duch
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
využívá	využívat	k5eAaImIp3nS	využívat
Boba	Bob	k1gMnSc2	Bob
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k6eAd1	Mike
Cooperovi	Cooper	k1gMnSc3	Cooper
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bob	Bob	k1gMnSc1	Bob
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
někoho	někdo	k3yInSc4	někdo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
mu	on	k3xPp3gMnSc3	on
sdělit	sdělit	k5eAaPmF	sdělit
kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
dalšího	další	k2eAgInSc2d1	další
Lauřina	Lauřin	k2eAgInSc2d1	Lauřin
deníku	deník	k1gInSc2	deník
přispěje	přispět	k5eAaPmIp3nS	přispět
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bob	Bob	k1gMnSc1	Bob
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
přítelem	přítel	k1gMnSc7	přítel
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgMnS	začít
sexuálně	sexuálně	k6eAd1	sexuálně
obtěžovat	obtěžovat	k5eAaImF	obtěžovat
a	a	k8xC	a
znásilňovat	znásilňovat	k5eAaImF	znásilňovat
již	již	k6eAd1	již
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
začala	začít	k5eAaPmAgFnS	začít
užívat	užívat	k5eAaImF	užívat
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
zneužíváním	zneužívání	k1gNnSc7	zneužívání
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
začne	začít	k5eAaPmIp3nS	začít
prošetřovat	prošetřovat	k5eAaImF	prošetřovat
Lelandovy	Lelandův	k2eAgMnPc4d1	Lelandův
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
a	a	k8xC	a
Harrymu	Harrymum	k1gNnSc6	Harrymum
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrahem	vrah	k1gMnSc7	vrah
je	být	k5eAaImIp3nS	být
Ben	Ben	k1gInSc4	Ben
Horne	Horn	k1gMnSc5	Horn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
konfrontace	konfrontace	k1gFnSc2	konfrontace
se	se	k3xPyFc4	se
Horne	Horn	k1gMnSc5	Horn
přizná	přiznat	k5eAaPmIp3nS	přiznat
Cooperovi	Cooperův	k2eAgMnPc1d1	Cooperův
a	a	k8xC	a
Audrey	Audrea	k1gFnPc1	Audrea
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
Laurou	Laura	k1gFnSc7	Laura
sexuální	sexuální	k2eAgFnSc4d1	sexuální
aféru	aféra	k1gFnSc4	aféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
nezabil	zabít	k5eNaPmAgMnS	zabít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamilovaný	zamilovaný	k2eAgInSc1d1	zamilovaný
<g/>
.	.	kIx.	.
</s>
<s>
Maddy	Maddy	k6eAd1	Maddy
Fergusonová	Fergusonová	k1gFnSc1	Fergusonová
je	být	k5eAaImIp3nS	být
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
nalezena	naleznout	k5eAaPmNgFnS	naleznout
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
,	,	kIx,	,
zabalená	zabalený	k2eAgFnSc1d1	zabalená
do	do	k7c2	do
igelitu	igelit	k1gInSc2	igelit
společně	společně	k6eAd1	společně
se	s	k7c7	s
srstí	srst	k1gFnSc7	srst
z	z	k7c2	z
vycpaných	vycpaný	k2eAgNnPc2d1	vycpané
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
Benovy	Benův	k2eAgFnSc2d1	Benova
kanceláře	kancelář	k1gFnSc2	kancelář
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
za	za	k7c4	za
Lauřinu	Lauřin	k2eAgFnSc4d1	Lauřina
vraždu	vražda	k1gFnSc4	vražda
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Bena	Bena	k?	Bena
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
Catherine	Catherin	k1gInSc5	Catherin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
výsměchem	výsměch	k1gInSc7	výsměch
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Laura	Laura	k1gFnSc1	Laura
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
strávili	strávit	k5eAaPmAgMnP	strávit
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
ho	on	k3xPp3gMnSc4	on
očistit	očistit	k5eAaPmF	očistit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zahnal	zahnat	k5eAaPmAgMnS	zahnat
nejistoty	nejistota	k1gFnPc4	nejistota
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
<g/>
,	,	kIx,	,
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
Cooper	Cooper	k1gMnSc1	Cooper
všechny	všechen	k3xTgFnPc4	všechen
podezřelé	podezřelý	k2eAgFnPc4d1	podezřelá
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
–	–	k?	–
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
mezi	mezi	k7c7	mezi
podezřelými	podezřelý	k2eAgMnPc7d1	podezřelý
–	–	k?	–
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objeví	objevit	k5eAaPmIp3nS	objevit
nějaké	nějaký	k3yIgNnSc1	nějaký
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
identifikovat	identifikovat	k5eAaBmF	identifikovat
vraha	vrah	k1gMnSc4	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
žvýkačku	žvýkačka	k1gFnSc4	žvýkačka
Lelandu	Leland	k1gInSc2	Leland
Palmerovi	Palmer	k1gMnSc3	Palmer
<g/>
,	,	kIx,	,
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
ducha	duch	k1gMnSc4	duch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k9	již
jednou	jeden	k4xCgFnSc7	jeden
zjevil	zjevit	k5eAaPmAgMnS	zjevit
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
postřelen	postřelit	k5eAaPmNgMnS	postřelit
a	a	k8xC	a
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
než	než	k8xS	než
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
Obřího	obří	k2eAgMnSc4d1	obří
muže	muž	k1gMnSc4	muž
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
samá	samý	k3xTgFnSc1	samý
bytost	bytost	k1gFnSc1	bytost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pronese	pronést	k5eAaPmIp3nS	pronést
frázi	fráze	k1gFnSc4	fráze
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
již	již	k6eAd1	již
Copper	Copper	k1gMnSc1	Copper
slyšel	slyšet	k5eAaImAgMnS	slyšet
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
snu	sen	k1gInSc6	sen
od	od	k7c2	od
Muže	muž	k1gMnSc2	muž
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Obří	obří	k2eAgMnSc1d1	obří
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
Cooperovi	Cooper	k1gMnSc3	Cooper
<g/>
,	,	kIx,	,
že	že	k8xS	že
Leland	Leland	k1gInSc1	Leland
je	být	k5eAaImIp3nS	být
hostitelem	hostitel	k1gMnSc7	hostitel
Boba	Bob	k1gMnSc2	Bob
<g/>
,	,	kIx,	,
vraha	vrah	k1gMnSc2	vrah
Laury	Laura	k1gFnSc2	Laura
a	a	k8xC	a
Maddy	Madda	k1gFnSc2	Madda
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
a	a	k8xC	a
Truman	Truman	k1gMnSc1	Truman
jej	on	k3xPp3gInSc4	on
zatknou	zatknout	k5eAaPmIp3nP	zatknout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Bob	Bob	k1gMnSc1	Bob
získá	získat	k5eAaPmIp3nS	získat
úplnou	úplný	k2eAgFnSc4d1	úplná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Lelandovým	Lelandový	k2eAgNnSc7d1	Lelandový
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
přizná	přiznat	k5eAaPmIp3nS	přiznat
se	se	k3xPyFc4	se
k	k	k7c3	k
sérii	série	k1gFnSc3	série
vražd	vražda	k1gFnPc2	vražda
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
donutí	donutit	k5eAaPmIp3nS	donutit
Lelanda	Lelanda	k1gFnSc1	Lelanda
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Leland	Leland	k1gInSc1	Leland
<g/>
,	,	kIx,	,
umírající	umírající	k2eAgInSc1d1	umírající
v	v	k7c6	v
Cooperově	Cooperův	k2eAgNnSc6d1	Cooperovo
náručí	náručí	k1gNnSc6	náručí
a	a	k8xC	a
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
Bobova	Bobův	k2eAgInSc2d1	Bobův
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
Cooperovi	Cooperův	k2eAgMnPc1d1	Cooperův
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Bob	Bob	k1gMnSc1	Bob
ovládal	ovládat	k5eAaImAgMnS	ovládat
a	a	k8xC	a
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
už	už	k6eAd1	už
jako	jako	k8xC	jako
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Prosí	prosit	k5eAaImIp3nS	prosit
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjeví	zjevit	k5eAaPmIp3nS	zjevit
Laura	Laura	k1gFnSc1	Laura
a	a	k8xC	a
uvítá	uvítat	k5eAaPmIp3nS	uvítat
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
ráno	ráno	k1gNnSc4	ráno
se	se	k3xPyFc4	se
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Truman	Truman	k1gMnSc1	Truman
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
co	co	k9	co
pracují	pracovat	k5eAaImIp3nP	pracovat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
zákona	zákon	k1gInSc2	zákon
ptají	ptat	k5eAaImIp3nP	ptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
Leland	Leland	k1gInSc1	Leland
skutečně	skutečně	k6eAd1	skutečně
někým	někdo	k3yInSc7	někdo
ovládán	ovládat	k5eAaImNgMnS	ovládat
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
mentálně	mentálně	k6eAd1	mentálně
postižený	postižený	k2eAgMnSc1d1	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
obávají	obávat	k5eAaImIp3nP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
správná	správný	k2eAgFnSc1d1	správná
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bob	Bob	k1gMnSc1	Bob
stále	stále	k6eAd1	stále
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Twin	Twin	k1gNnSc1	Twin
Peaks	Peaks	k1gInSc4	Peaks
<g/>
,	,	kIx,	,
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
si	se	k3xPyFc3	se
nového	nový	k2eAgMnSc4d1	nový
hostitele	hostitel	k1gMnSc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
vraždy	vražda	k1gFnSc2	vražda
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
je	být	k5eAaImIp3nS	být
Cooper	Cooper	k1gInSc4	Cooper
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odjezdem	odjezd	k1gInSc7	odjezd
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
napomáhání	napomáhání	k1gNnSc2	napomáhání
zločinnému	zločinný	k2eAgMnSc3d1	zločinný
Jeanu	Jean	k1gMnSc3	Jean
Renaultovi	Renault	k1gMnSc3	Renault
v	v	k7c6	v
obchodování	obchodování	k1gNnSc6	obchodování
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
suspendován	suspendován	k2eAgInSc1d1	suspendován
<g/>
.	.	kIx.	.
</s>
<s>
Renault	renault	k1gInSc1	renault
považuje	považovat	k5eAaImIp3nS	považovat
Coopera	Cooper	k1gMnSc4	Cooper
za	za	k7c4	za
zodpovědného	zodpovědný	k2eAgMnSc4d1	zodpovědný
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
Jacquese	Jacques	k1gMnSc2	Jacques
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Lelandem	Lelando	k1gNnSc7	Lelando
Palmerem	Palmero	k1gNnSc7	Palmero
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Renault	renault	k1gInSc1	renault
pod	pod	k7c7	pod
policejním	policejní	k2eAgInSc7d1	policejní
dohledem	dohled	k1gInSc7	dohled
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
Renault	renault	k1gInSc1	renault
zabit	zabit	k2eAgInSc1d1	zabit
v	v	k7c6	v
přestřelce	přestřelka	k1gFnSc6	přestřelka
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
Cooper	Cooper	k1gMnSc1	Cooper
je	být	k5eAaImIp3nS	být
očištěn	očistit	k5eAaPmNgMnS	očistit
od	od	k7c2	od
všech	všecek	k3xTgNnPc2	všecek
obvinění	obvinění	k1gNnPc2	obvinění
<g/>
,	,	kIx,	,
Cooperův	Cooperův	k2eAgMnSc1d1	Cooperův
bývalý	bývalý	k2eAgMnSc1d1	bývalý
partner	partner	k1gMnSc1	partner
a	a	k8xC	a
rádce	rádce	k1gMnSc1	rádce
Windom	Windom	k1gInSc4	Windom
Earle	earl	k1gMnSc5	earl
dorazí	dorazit	k5eAaPmIp3nP	dorazit
do	do	k7c2	do
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
sehrál	sehrát	k5eAaPmAgMnS	sehrát
vražednou	vražedný	k2eAgFnSc4d1	vražedná
šachovou	šachový	k2eAgFnSc4d1	šachová
partii	partie	k1gFnSc4	partie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
Cooperovu	Cooperův	k2eAgFnSc4d1	Cooperova
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
figurku	figurka	k1gFnSc4	figurka
zemře	zemřít	k5eAaPmIp3nS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
později	pozdě	k6eAd2	pozdě
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Trumanovi	Truman	k1gMnSc3	Truman
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
jeho	jeho	k3xOp3gInPc2	jeho
začátků	začátek	k1gInPc2	začátek
v	v	k7c6	v
FBI	FBI	kA	FBI
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Earla	earl	k1gMnSc2	earl
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Cooper	Cooper	k1gMnSc1	Cooper
začal	začít	k5eAaPmAgMnS	začít
aférku	aférka	k1gFnSc4	aférka
s	s	k7c7	s
Earlovou	earlův	k2eAgFnSc7d1	Earlova
ženou	žena	k1gFnSc7	žena
Caroline	Carolin	k1gInSc5	Carolin
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
ochranou	ochrana	k1gFnSc7	ochrana
jako	jako	k8xC	jako
svědek	svědek	k1gMnSc1	svědek
federálního	federální	k2eAgInSc2d1	federální
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Earl	earl	k1gMnSc1	earl
se	se	k3xPyFc4	se
naštval	naštvat	k5eAaPmAgMnS	naštvat
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
Caroline	Carolin	k1gInSc5	Carolin
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
Coopera	Cooper	k1gMnSc2	Cooper
vykuchat	vykuchat	k5eAaPmF	vykuchat
pomocí	pomocí	k7c2	pomocí
nože	nůž	k1gInSc2	nůž
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
uvržen	uvrhnout	k5eAaPmNgInS	uvrhnout
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
útěku	útěk	k1gInSc6	útěk
a	a	k8xC	a
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
se	se	k3xPyFc4	se
Earle	earl	k1gMnSc5	earl
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
osnovat	osnovat	k5eAaImF	osnovat
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
na	na	k7c4	na
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
snaží	snažit	k5eAaImIp3nP	snažit
zjistit	zjistit	k5eAaPmF	zjistit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
životě	život	k1gInSc6	život
Boba	Bob	k1gMnSc2	Bob
<g/>
,	,	kIx,	,
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
tajemných	tajemný	k2eAgInPc6d1	tajemný
temných	temný	k2eAgInPc6d1	temný
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
Twin	Twin	k1gInSc4	Twin
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
Bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
Černého	Černého	k2eAgInSc2d1	Černého
vigvamu	vigvam	k1gInSc2	vigvam
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc6	dva
mystických	mystický	k2eAgFnPc2d1	mystická
extra	extra	k2eAgFnPc2d1	extra
dimenzionálních	dimenzionální	k2eAgFnPc2d1	dimenzionální
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
Nebe	nebe	k1gNnSc4	nebe
a	a	k8xC	a
Peklo	peklo	k1gNnSc4	peklo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vchod	vchod	k1gInSc1	vchod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
někde	někde	k6eAd1	někde
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Copper	Copper	k1gMnSc1	Copper
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bob	Bob	k1gMnSc1	Bob
<g/>
,	,	kIx,	,
Obří	obří	k2eAgMnSc1d1	obří
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
vigvamů	vigvam	k1gInPc2	vigvam
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
se	se	k3xPyFc4	se
také	také	k9	také
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
dívky	dívka	k1gFnSc2	dívka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Annie	Annie	k1gFnSc1	Annie
Blackburnové	Blackburnová	k1gFnSc2	Blackburnová
(	(	kIx(	(
<g/>
Heather	Heathra	k1gFnPc2	Heathra
Grahamová	Grahamová	k1gFnSc1	Grahamová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Annie	Annie	k1gFnSc1	Annie
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
Miss	miss	k1gFnSc1	miss
Twin	Twin	k1gMnSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
unesena	unesen	k2eAgFnSc1d1	unesena
Windomem	Windom	k1gMnSc7	Windom
Earlem	earl	k1gMnSc7	earl
a	a	k8xC	a
odvlečena	odvlečen	k2eAgFnSc1d1	odvlečena
ke	k	k7c3	k
vchodu	vchod	k1gInSc3	vchod
do	do	k7c2	do
Černého	Černého	k2eAgInSc2d1	Černého
vigvamu	vigvam	k1gInSc2	vigvam
v	v	k7c4	v
Glastonbury	Glastonbur	k1gInPc4	Glastonbur
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
Copper	Copper	k1gMnSc1	Copper
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Windom	Windom	k1gInSc1	Windom
Earle	earl	k1gMnSc5	earl
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
Černého	Černého	k2eAgInSc2d1	Černého
vigvamu	vigvam	k1gInSc2	vigvam
a	a	k8xC	a
všechnu	všechen	k3xTgFnSc4	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
moc	moc	k6eAd1	moc
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
šachová	šachový	k2eAgFnSc1d1	šachová
hra	hra	k1gFnSc1	hra
<g/>
"	"	kIx"	"
sloužila	sloužit	k5eAaImAgFnS	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
odvedení	odvedení	k1gNnSc3	odvedení
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
dámy	dáma	k1gFnSc2	dáma
s	s	k7c7	s
polenem	poleno	k1gNnSc7	poleno
sleduje	sledovat	k5eAaImIp3nS	sledovat
Cooper	Cooper	k1gInSc1	Cooper
Annie	Annie	k1gFnSc2	Annie
a	a	k8xC	a
Earla	earl	k1gMnSc2	earl
do	do	k7c2	do
Vigvamu	vigvam	k1gInSc2	vigvam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
být	být	k5eAaImF	být
pokojem	pokoj	k1gInSc7	pokoj
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
závěsy	závěs	k1gInPc7	závěs
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uvítám	uvítat	k5eAaPmIp1nS	uvítat
Mužem	muž	k1gMnSc7	muž
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Obřím	obří	k2eAgMnSc7d1	obří
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
duchem	duch	k1gMnSc7	duch
Laury	Laura	k1gFnSc2	Laura
Palmerové	Palmerová	k1gFnSc2	Palmerová
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
mu	on	k3xPp3gMnSc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
zakódované	zakódovaný	k2eAgNnSc4d1	zakódované
poselství	poselství	k1gNnSc4	poselství
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc2	jeho
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
předvedou	předvést	k5eAaPmIp3nP	předvést
mu	on	k3xPp3gMnSc3	on
možnosti	možnost	k1gFnPc4	možnost
Černého	Černého	k2eAgInSc2d1	Černého
vigvamu	vigvam	k1gInSc2	vigvam
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
popírají	popírat	k5eAaImIp3nP	popírat
zákony	zákon	k1gInPc4	zákon
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c4	po
Annie	Annie	k1gFnPc4	Annie
a	a	k8xC	a
Earlovi	earl	k1gMnSc3	earl
narazí	narazit	k5eAaPmIp3nS	narazit
Cooper	Cooper	k1gInSc4	Cooper
na	na	k7c4	na
několik	několik	k4yIc4	několik
duchů	duch	k1gMnPc2	duch
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Maddy	Madda	k1gFnSc2	Madda
a	a	k8xC	a
Lelanda	Lelando	k1gNnSc2	Lelando
Palmera	Palmero	k1gNnSc2	Palmero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gInSc3	on
přednese	přednést	k5eAaPmIp3nS	přednést
několik	několik	k4yIc1	několik
směšných	směšný	k2eAgNnPc2d1	směšné
nepravdivých	pravdivý	k2eNgNnPc2d1	nepravdivé
sdělení	sdělení	k1gNnPc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
duchové	duch	k1gMnPc1	duch
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
přivedou	přivést	k5eAaPmIp3nP	přivést
až	až	k9	až
k	k	k7c3	k
Earlovi	earl	k1gMnSc3	earl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žádá	žádat	k5eAaImIp3nS	žádat
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Cooper	Cooper	k1gMnSc1	Cooper
vzdal	vzdát	k5eAaPmAgMnS	vzdát
své	svůj	k3xOyFgFnPc4	svůj
duše	duše	k1gFnPc4	duše
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
život	život	k1gInSc4	život
Annie	Annie	k1gFnSc2	Annie
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
a	a	k8xC	a
Earle	earl	k1gMnSc5	earl
jej	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
zabiják	zabiják	k1gMnSc1	zabiják
Bob	Bob	k1gMnSc1	Bob
<g/>
,	,	kIx,	,
obrátí	obrátit	k5eAaPmIp3nS	obrátit
běh	běh	k1gInSc1	běh
času	čas	k1gInSc2	čas
ve	v	k7c6	v
vigvamu	vigvam	k1gInSc6	vigvam
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nP	vrátit
Cooperovi	Cooperův	k2eAgMnPc1d1	Cooperův
zpátky	zpátky	k6eAd1	zpátky
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
zlostně	zlostně	k6eAd1	zlostně
sdělí	sdělit	k5eAaPmIp3nS	sdělit
Earlovi	earl	k1gMnSc3	earl
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vigvamu	vigvam	k1gInSc2	vigvam
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
lidskou	lidský	k2eAgFnSc4d1	lidská
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
potrestal	potrestat	k5eAaPmAgInS	potrestat
tak	tak	k6eAd1	tak
jej	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
obrátí	obrátit	k5eAaPmIp3nP	obrátit
ke	k	k7c3	k
Cooperovi	Cooper	k1gMnSc3	Cooper
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
Vigvamu	vigvam	k1gInSc6	vigvam
pozná	poznat	k5eAaPmIp3nS	poznat
co	co	k9	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
strach	strach	k1gInSc1	strach
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
pak	pak	k6eAd1	pak
utíká	utíkat	k5eAaImIp3nS	utíkat
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
Bobem	Bob	k1gMnSc7	Bob
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
vlastním	vlastní	k2eAgMnSc7d1	vlastní
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
vigvamu	vigvam	k1gInSc2	vigvam
jsou	být	k5eAaImIp3nP	být
Cooper	Cooper	k1gInSc1	Cooper
a	a	k8xC	a
Annie	Annie	k1gFnSc1	Annie
objeveni	objeven	k2eAgMnPc1d1	objeven
šerifem	šerif	k1gMnSc7	šerif
Trumanem	Truman	k1gMnSc7	Truman
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Annie	Annie	k1gFnSc1	Annie
je	být	k5eAaImIp3nS	být
hospitalizována	hospitalizovat	k5eAaBmNgFnS	hospitalizovat
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Cooperova	Cooperův	k2eAgNnPc1d1	Cooperovo
zranění	zranění	k1gNnPc1	zranění
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
lehká	lehký	k2eAgNnPc1d1	lehké
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
Hayward	Hayward	k1gMnSc1	Hayward
jej	on	k3xPp3gNnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
převézt	převézt	k5eAaPmF	převézt
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
pokoje	pokoj	k1gInSc2	pokoj
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Great	Great	k2eAgInSc4d1	Great
Northern	Northern	k1gInSc4	Northern
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
se	se	k3xPyFc4	se
Cooper	Cooper	k1gInSc1	Cooper
ptá	ptat	k5eAaImIp3nS	ptat
na	na	k7c4	na
stav	stav	k1gInSc4	stav
Annie	Annie	k1gFnSc2	Annie
a	a	k8xC	a
poté	poté	k6eAd1	poté
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
vyčistit	vyčistit	k5eAaPmF	vyčistit
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Cooper	Cooper	k1gMnSc1	Cooper
pohlédne	pohlédnout	k5eAaPmIp3nS	pohlédnout
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
do	do	k7c2	do
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
přenesl	přenést	k5eAaPmAgInS	přenést
Bob	bob	k1gInSc4	bob
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
poté	poté	k6eAd1	poté
nasadí	nasadit	k5eAaPmIp3nS	nasadit
škleb	škleb	k1gInSc1	škleb
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
<g/>
/	/	kIx~	/
<g/>
Cooperově	Cooperův	k2eAgInSc6d1	Cooperův
obličeji	obličej	k1gInSc6	obličej
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
a	a	k8xC	a
ironicky	ironicky	k6eAd1	ironicky
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
na	na	k7c4	na
stav	stav	k1gInSc4	stav
Annie	Annie	k1gFnSc2	Annie
a	a	k8xC	a
směje	smát	k5eAaImIp3nS	smát
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
otevřeně	otevřeně	k6eAd1	otevřeně
končí	končit	k5eAaImIp3nS	končit
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
