<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
;	;	kIx,	;
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
řada	řada	k1gFnSc1	řada
komentátorů	komentátor	k1gMnPc2	komentátor
<g/>
,	,	kIx,	,
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
tenistů	tenista	k1gMnPc2	tenista
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
historie	historie	k1gFnSc2	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
olympijského	olympijský	k2eAgMnSc4d1	olympijský
vítěze	vítěz	k1gMnSc4	vítěz
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
z	z	k7c2	z
Pekingských	pekingský	k2eAgFnPc2d1	Pekingská
her	hra	k1gFnPc2	hra
2008	[number]	k4	2008
a	a	k8xC	a
stříbrného	stříbrný	k2eAgMnSc4d1	stříbrný
olympijského	olympijský	k2eAgMnSc4d1	olympijský
medailistu	medailista	k1gMnSc4	medailista
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
dvouhře	dvouhra	k1gFnSc6	dvouhra
z	z	k7c2	z
Londýnských	londýnský	k2eAgFnPc2d1	londýnská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
profesionály	profesionál	k1gMnPc7	profesionál
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
byl	být	k5eAaImAgMnS	být
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
osmdesát	osmdesát	k4xCc4	osmdesát
osm	osm	k4xCc4	osm
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
osmačtyřicetkrát	osmačtyřicetkrát	k6eAd1	osmačtyřicetkrát
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k8xC	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
osm	osm	k4xCc4	osm
trofejí	trofej	k1gFnPc2	trofej
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
šest	šest	k4xCc4	šest
finálových	finálový	k2eAgFnPc2d1	finálová
proher	prohra	k1gFnPc2	prohra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
sedmnácti	sedmnáct	k4xCc2	sedmnáct
singlových	singlový	k2eAgInPc2d1	singlový
titulů	titul	k1gInPc2	titul
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
je	být	k5eAaImIp3nS	být
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
mužským	mužský	k2eAgMnSc7d1	mužský
tenistou	tenista	k1gMnSc7	tenista
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
spoludrží	spoludržit	k5eAaPmIp3nS	spoludržit
rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
trofejí	trofej	k1gFnPc2	trofej
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
grandslamech	grandslam	k1gInPc6	grandslam
-	-	kIx~	-
sedmi	sedm	k4xCc2	sedm
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
a	a	k8xC	a
pěti	pět	k4xCc2	pět
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Pařížským	pařížský	k2eAgInSc7d1	pařížský
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
French	Frencha	k1gFnPc2	Frencha
Open	Open	k1gNnSc1	Open
2009	[number]	k4	2009
zkompletoval	zkompletovat	k5eAaPmAgMnS	zkompletovat
jako	jako	k9	jako
šestý	šestý	k4xOgMnSc1	šestý
hráč	hráč	k1gMnSc1	hráč
historie	historie	k1gFnSc2	historie
kariérní	kariérní	k2eAgMnSc1d1	kariérní
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mu	on	k3xPp3gMnSc3	on
řada	řada	k1gFnSc1	řada
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
27	[number]	k4	27
finálových	finálový	k2eAgFnPc2d1	finálová
účastí	účast	k1gFnPc2	účast
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
čtyřky	čtyřka	k1gFnPc4	čtyřka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
muž	muž	k1gMnSc1	muž
historie	historie	k1gFnSc2	historie
probojoval	probojovat	k5eAaPmAgMnS	probojovat
nejméně	málo	k6eAd3	málo
pětkrát	pětkrát	k6eAd1	pětkrát
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gInSc4	Federer
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2005	[number]	k4	2005
do	do	k7c2	do
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gInSc1	Open
2010	[number]	k4	2010
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
z	z	k7c2	z
devatenácti	devatenáct	k4xCc2	devatenáct
finále	finále	k1gNnPc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
třech	tři	k4xCgNnPc6	tři
semifinále	semifinále	k1gNnPc6	semifinále
a	a	k8xC	a
třiceti	třicet	k4xCc7	třicet
šesti	šest	k4xCc2	šest
čtvrtfinále	čtvrtfinále	k1gNnPc7	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2016	[number]	k4	2016
završil	završit	k5eAaPmAgInS	završit
rekordní	rekordní	k2eAgFnSc4d1	rekordní
sérii	série	k1gFnSc4	série
65	[number]	k4	65
<g/>
.	.	kIx.	.
grandslamů	grandslam	k1gInPc2	grandslam
bez	bez	k7c2	bez
abscence	abscence	k1gFnSc2	abscence
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2016	[number]	k4	2016
navýšil	navýšit	k5eAaPmAgInS	navýšit
rekordní	rekordní	k2eAgInSc1d1	rekordní
zápis	zápis	k1gInSc1	zápis
48	[number]	k4	48
<g/>
.	.	kIx.	.
čtvrtfinálem	čtvrtfinále	k1gNnSc7	čtvrtfinále
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
semifinálem	semifinále	k1gNnSc7	semifinále
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
čtyřky	čtyřka	k1gFnPc4	čtyřka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
307	[number]	k4	307
vítězných	vítězný	k2eAgInPc2d1	vítězný
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
grandslamu	grandslam	k1gInSc2	grandslam
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c4	za
Serenou	Serený	k2eAgFnSc4d1	Serený
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
65	[number]	k4	65
utkáních	utkání	k1gNnPc6	utkání
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
trofej	trofej	k1gFnSc4	trofej
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
rekordních	rekordní	k2eAgInPc2d1	rekordní
šest	šest	k4xCc4	šest
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
52	[number]	k4	52
utkání	utkání	k1gNnSc4	utkání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c4	v
jeho	jeho	k3xOp3gNnPc2	jeho
14	[number]	k4	14
ročnících	ročník	k1gInPc6	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
série	série	k1gFnSc2	série
ATP	atp	kA	atp
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
finále	finále	k1gNnSc4	finále
všech	všecek	k3xTgMnPc2	všecek
devíti	devět	k4xCc2	devět
aktuálně	aktuálně	k6eAd1	aktuálně
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
také	také	k9	také
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
počtu	počet	k1gInSc2	počet
patnácti	patnáct	k4xCc2	patnáct
trofejí	trofej	k1gFnPc2	trofej
ATP	atp	kA	atp
z	z	k7c2	z
akcí	akce	k1gFnPc2	akce
hraných	hraný	k2eAgFnPc2d1	hraná
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
a	a	k8xC	a
šedesáti	šedesát	k4xCc2	šedesát
na	na	k7c6	na
tvrdém	tvrdé	k1gNnSc6	tvrdé
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
ATP	atp	kA	atp
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
singlového	singlový	k2eAgInSc2d1	singlový
žebříčku	žebříček	k1gInSc2	žebříček
strávil	strávit	k5eAaPmAgInS	strávit
historicky	historicky	k6eAd1	historicky
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
období	období	k1gNnSc4	období
302	[number]	k4	302
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
opět	opět	k6eAd1	opět
rekordních	rekordní	k2eAgInPc2d1	rekordní
237	[number]	k4	237
týdnů	týden	k1gInPc2	týden
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
sezón	sezóna	k1gFnPc2	sezóna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
skončil	skončit	k5eAaPmAgInS	skončit
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
a	a	k8xC	a
deset	deset	k4xCc4	deset
sezón	sezóna	k1gFnPc2	sezóna
za	za	k7c7	za
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
nezakončil	zakončit	k5eNaPmAgMnS	zakončit
hůře	zle	k6eAd2	zle
než	než	k8xS	než
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
tenista	tenista	k1gMnSc1	tenista
překročil	překročit	k5eAaPmAgMnS	překročit
na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
hranici	hranice	k1gFnSc4	hranice
50	[number]	k4	50
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
také	také	k9	také
90	[number]	k4	90
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
statistice	statistika	k1gFnSc6	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahajovacích	zahajovací	k2eAgInPc6d1	zahajovací
ceremoniálech	ceremoniál	k1gInPc6	ceremoniál
Athénské	athénský	k2eAgFnSc2d1	Athénská
olympiády	olympiáda	k1gFnSc2	olympiáda
2004	[number]	k4	2004
a	a	k8xC	a
Pekingské	pekingský	k2eAgFnSc2d1	Pekingská
olympiády	olympiáda	k1gFnSc2	olympiáda
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
vlajkonošem	vlajkonoš	k1gMnSc7	vlajkonoš
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Trénuje	trénovat	k5eAaImIp3nS	trénovat
ho	on	k3xPp3gNnSc4	on
kapitán	kapitán	k1gMnSc1	kapitán
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
daviscupového	daviscupový	k2eAgInSc2d1	daviscupový
týmu	tým	k1gInSc2	tým
Severin	Severin	k1gMnSc1	Severin
Lüthi	Lüthi	k1gNnPc2	Lüthi
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
tak	tak	k6eAd1	tak
nahradil	nahradit	k5eAaPmAgInS	nahradit
Američana	Američan	k1gMnSc4	Američan
Paula	Paul	k1gMnSc4	Paul
Annacona	Annacon	k1gMnSc4	Annacon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
koučem	kouč	k1gMnSc7	kouč
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sezóny	sezóna	k1gFnSc2	sezóna
2016	[number]	k4	2016
se	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
trenérem	trenér	k1gMnSc7	trenér
stala	stát	k5eAaPmAgFnS	stát
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
trojka	trojka	k1gFnSc1	trojka
Ivan	Ivan	k1gMnSc1	Ivan
Ljubičić	Ljubičić	k1gMnSc1	Ljubičić
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
fyzioterapeuta	fyzioterapeut	k1gMnSc2	fyzioterapeut
působí	působit	k5eAaImIp3nS	působit
Daniel	Daniel	k1gMnSc1	Daniel
Troxler	Troxler	k1gMnSc1	Troxler
a	a	k8xC	a
kondiční	kondiční	k2eAgFnSc4d1	kondiční
přípravu	příprava	k1gFnSc4	příprava
vede	vést	k5eAaImIp3nS	vést
Pierre	Pierr	k1gInSc5	Pierr
Paganini	Paganin	k2eAgMnPc5d1	Paganin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
se	se	k3xPyFc4	se
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
devítiletém	devítiletý	k2eAgInSc6d1	devítiletý
vztahu	vztah	k1gInSc6	vztah
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
tenistkou	tenistka	k1gFnSc7	tenistka
slovenského	slovenský	k2eAgInSc2d1	slovenský
původu	původ	k1gInSc2	původ
Miroslavou	Miroslava	k1gFnSc7	Miroslava
Vavrinecovou	Vavrinecová	k1gFnSc7	Vavrinecová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	s	k7c7	s
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
narodily	narodit	k5eAaPmAgFnP	narodit
dcery	dcera	k1gFnPc1	dcera
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
Myla	mýt	k5eAaImAgFnS	mýt
Rose	Ros	k1gMnSc4	Ros
a	a	k8xC	a
Charlene	Charlen	k1gInSc5	Charlen
Riva	Rivum	k1gNnPc4	Rivum
Federerovy	Federerův	k2eAgFnPc1d1	Federerova
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgNnPc1	druhý
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
synové	syn	k1gMnPc1	syn
Leo	Leo	k1gMnSc1	Leo
a	a	k8xC	a
Lenny	Lenny	k?	Lenny
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
také	také	k6eAd1	také
filantropii	filantropie	k1gFnSc3	filantropie
a	a	k8xC	a
charitě	charita	k1gFnSc3	charita
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
nadaci	nadace	k1gFnSc4	nadace
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
znevýhodněných	znevýhodněný	k2eAgMnPc2d1	znevýhodněný
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
propagace	propagace	k1gFnSc2	propagace
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
UNICEF	UNICEF	kA	UNICEF
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
země	zem	k1gFnSc2	zem
helvetského	helvetský	k2eAgInSc2d1	helvetský
kříže	kříž	k1gInSc2	kříž
dovedl	dovést	k5eAaPmAgMnS	dovést
tým	tým	k1gInSc4	tým
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
2014	[number]	k4	2014
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Martinou	Martina	k1gFnSc7	Martina
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
obdržel	obdržet	k5eAaPmAgMnS	obdržet
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc3	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Pětkrát	pětkrát	k6eAd1	pětkrát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
jej	on	k3xPp3gInSc4	on
pětkrát	pětkrát	k6eAd1	pětkrát
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
tenistou	tenista	k1gMnSc7	tenista
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hráčem	hráč	k1gMnSc7	hráč
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
třetího	třetí	k4xOgNnSc2	třetí
milenia	milenium	k1gNnSc2	milenium
<g/>
.	.	kIx.	.
</s>
<s>
Pětkrát	pětkrát	k6eAd1	pětkrát
také	také	k9	také
převzal	převzít	k5eAaPmAgMnS	převzít
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
sportovní	sportovní	k2eAgFnSc4d1	sportovní
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gInSc4	Laureus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
výkony	výkon	k1gInPc1	výkon
mu	on	k3xPp3gMnSc3	on
opakovaně	opakovaně	k6eAd1	opakovaně
zajistily	zajistit	k5eAaPmAgInP	zajistit
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
švýcarského	švýcarský	k2eAgMnSc4d1	švýcarský
sportovce	sportovec	k1gMnSc4	sportovec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Švýcarem	Švýcar	k1gMnSc7	Švýcar
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1981	[number]	k4	1981
v	v	k7c6	v
kantonální	kantonální	k2eAgFnSc6d1	kantonální
nemocnici	nemocnice	k1gFnSc6	nemocnice
švýcarského	švýcarský	k2eAgNnSc2d1	švýcarské
města	město	k1gNnSc2	město
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
,	,	kIx,	,
metropoli	metropole	k1gFnSc4	metropole
historického	historický	k2eAgInSc2d1	historický
polokantonu	polokanton	k1gInSc2	polokanton
Basilej-město	Basilejěsta	k1gFnSc5	Basilej-města
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Robert	Robert	k1gMnSc1	Robert
Federer	Federer	k1gMnSc1	Federer
je	být	k5eAaImIp3nS	být
Švýcar	Švýcar	k1gMnSc1	Švýcar
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
Bernecku	Berneck	k1gInSc2	Berneck
<g/>
,	,	kIx,	,
obce	obec	k1gFnSc2	obec
ležící	ležící	k2eAgFnSc2d1	ležící
blízko	blízko	k6eAd1	blízko
švýcarsko-rakousko-německé	švýcarskoakouskoěmecký	k2eAgFnSc2d1	švýcarsko-rakousko-německý
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Lynette	Lynett	k1gInSc5	Lynett
Federerová	Federerový	k2eAgFnSc1d1	Federerový
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Durandová	Durandový	k2eAgFnSc1d1	Durandová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Jihoafričanka	Jihoafričanka	k1gFnSc1	Jihoafričanka
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
transvaalském	transvaalský	k2eAgNnSc6d1	transvaalský
městě	město	k1gNnSc6	město
Kempton	Kempton	k1gInSc1	Kempton
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Gauteng	Gautenga	k1gFnPc2	Gautenga
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
předci	předek	k1gMnPc1	předek
byli	být	k5eAaImAgMnP	být
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
a	a	k8xC	a
francouzští	francouzský	k2eAgMnPc1d1	francouzský
hugenoti	hugenot	k1gMnPc1	hugenot
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
na	na	k7c6	na
služební	služební	k2eAgFnSc6d1	služební
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
když	když	k8xS	když
oba	dva	k4xCgMnPc1	dva
pracovali	pracovat	k5eAaImAgMnP	pracovat
pro	pro	k7c4	pro
farmaceutickou	farmaceutický	k2eAgFnSc4d1	farmaceutická
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
měsíců	měsíc	k1gInPc2	měsíc
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Diana	Diana	k1gFnSc1	Diana
Federerová	Federerová	k1gFnSc1	Federerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesí	profes	k1gFnSc7	profes
je	být	k5eAaImIp3nS	být
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
dvojí	dvojí	k4xRgNnSc4	dvojí
občanství	občanství	k1gNnSc4	občanství
-	-	kIx~	-
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
a	a	k8xC	a
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
severošvýcarských	severošvýcarský	k2eAgFnPc2d1	severošvýcarský
obcí	obec	k1gFnPc2	obec
Birsfelden	Birsfeldna	k1gFnPc2	Birsfeldna
<g/>
,	,	kIx,	,
Riehen	Riehna	k1gFnPc2	Riehna
a	a	k8xC	a
Münchenstein	Münchensteina	k1gFnPc2	Münchensteina
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Plynně	plynně	k6eAd1	plynně
hovoří	hovořit	k5eAaImIp3nS	hovořit
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
němčinou	němčina	k1gFnSc7	němčina
<g/>
,	,	kIx,	,
francouzštinou	francouzština	k1gFnSc7	francouzština
a	a	k8xC	a
angličtinou	angličtina	k1gFnSc7	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Vychován	vychován	k2eAgMnSc1d1	vychován
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turnaje	turnaj	k1gInSc2	turnaj
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc4	Masters
2006	[number]	k4	2006
vykonal	vykonat	k5eAaPmAgMnS	vykonat
audienci	audience	k1gFnSc4	audience
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
muži	muž	k1gMnPc1	muž
měl	mít	k5eAaImAgInS	mít
absolvovat	absolvovat	k5eAaPmF	absolvovat
povinnou	povinný	k2eAgFnSc4d1	povinná
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
však	však	k9	však
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
povinnosti	povinnost	k1gFnSc2	povinnost
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
zneschopnění	zneschopnění	k1gNnSc3	zneschopnění
pro	pro	k7c4	pro
dlouhotrvající	dlouhotrvající	k2eAgInPc4d1	dlouhotrvající
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
civilní	civilní	k2eAgFnSc6d1	civilní
službě	služba	k1gFnSc6	služba
a	a	k8xC	a
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
bylo	být	k5eAaImAgNnS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odvedl	odvést	k5eAaPmAgMnS	odvést
3	[number]	k4	3
%	%	kIx~	%
zdanitelného	zdanitelný	k2eAgInSc2d1	zdanitelný
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
je	být	k5eAaImIp3nS	být
příznivcem	příznivec	k1gMnSc7	příznivec
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
FC	FC	kA	FC
Basel	Basel	k1gInSc1	Basel
a	a	k8xC	a
národního	národní	k2eAgInSc2d1	národní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
řadu	řada	k1gFnSc4	řada
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
věnoval	věnovat	k5eAaPmAgMnS	věnovat
badmintonu	badminton	k1gInSc2	badminton
a	a	k8xC	a
basketbalu	basketbal	k1gInSc2	basketbal
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
koordinaci	koordinace	k1gFnSc4	koordinace
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
motoriky	motorik	k1gMnPc4	motorik
<g/>
,	,	kIx,	,
periferního	periferní	k2eAgNnSc2d1	periferní
vidění	vidění	k1gNnSc2	vidění
a	a	k8xC	a
také	také	k9	také
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
flexibilitu	flexibilita	k1gFnSc4	flexibilita
-	-	kIx~	-
při	při	k7c6	při
tenisových	tenisový	k2eAgInPc6d1	tenisový
úderech	úder	k1gInPc6	úder
jinak	jinak	k6eAd1	jinak
pevného	pevný	k2eAgNnSc2d1	pevné
<g/>
,	,	kIx,	,
zápěstí	zápěstí	k1gNnSc2	zápěstí
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
využíval	využívat	k5eAaImAgInS	využívat
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
svých	svůj	k3xOyFgFnPc6	svůj
"	"	kIx"	"
<g/>
zázračných	zázračný	k2eAgInPc6d1	zázračný
úderech	úder	k1gInPc6	úder
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
opakovaně	opakovaně	k6eAd1	opakovaně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
fanouškem	fanoušek	k1gMnSc7	fanoušek
kriketu	kriket	k1gInSc2	kriket
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
indickým	indický	k2eAgMnSc7d1	indický
hráčem	hráč	k1gMnSc7	hráč
Sačinem	Sačin	k1gMnSc7	Sačin
Tendulkarem	Tendulkar	k1gMnSc7	Tendulkar
<g/>
,	,	kIx,	,
považovaným	považovaný	k2eAgMnSc7d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
kriketistů	kriketista	k1gMnPc2	kriketista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
míčových	míčový	k2eAgInPc2d1	míčový
sportů	sport	k1gInPc2	sport
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
velmi	velmi	k6eAd1	velmi
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnPc2	součást
hry	hra	k1gFnSc2	hra
míč	míč	k1gInSc1	míč
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
stal	stát	k5eAaPmAgMnS	stát
americký	americký	k2eAgMnSc1d1	americký
golfista	golfista	k1gMnSc1	golfista
Tiger	Tigra	k1gFnPc2	Tigra
Woods	Woods	k1gInSc1	Woods
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
golfu	golf	k1gInSc2	golf
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
se	s	k7c7	s
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
profesionální	profesionální	k2eAgFnSc7d1	profesionální
tenistkou	tenistka	k1gFnSc7	tenistka
Mirkou	Mirka	k1gFnSc7	Mirka
Vavrincovou	Vavrincův	k2eAgFnSc7d1	Vavrincův
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarkou	Švýcarka	k1gFnSc7	Švýcarka
narozenou	narozený	k2eAgFnSc7d1	narozená
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
riehenské	riehenský	k2eAgFnSc6d1	riehenský
Wenkenhofově	Wenkenhofův	k2eAgFnSc6d1	Wenkenhofův
vile	vila	k1gFnSc6	vila
u	u	k7c2	u
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
několika	několik	k4yIc2	několik
blízkých	blízký	k2eAgMnPc2d1	blízký
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
poznala	poznat	k5eAaPmAgFnS	poznat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2000	[number]	k4	2000
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oba	dva	k4xCgMnPc1	dva
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
započali	započnout	k5eAaPmAgMnP	započnout
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
nemedializovali	medializovat	k5eNaBmAgMnP	medializovat
až	až	k9	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
také	také	k9	také
odehráli	odehrát	k5eAaPmAgMnP	odehrát
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Vavrinecová	Vavrinecová	k1gFnSc1	Vavrinecová
ukončila	ukončit	k5eAaPmAgFnS	ukončit
profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
pro	pro	k7c4	pro
opakující	opakující	k2eAgNnSc4d1	opakující
se	se	k3xPyFc4	se
poranění	poranění	k1gNnSc4	poranění
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	s	k7c7	s
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc4	dvojče
Myla	mýt	k5eAaImAgFnS	mýt
Rose	Ros	k1gMnSc4	Ros
a	a	k8xC	a
Charlene	Charlen	k1gInSc5	Charlen
Riva	Rivum	k1gNnPc4	Rivum
Federerovy	Federerův	k2eAgFnPc1d1	Federerova
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
manželé	manžel	k1gMnPc1	manžel
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
očekávají	očekávat	k5eAaImIp3nP	očekávat
narození	narození	k1gNnSc4	narození
třetího	třetí	k4xOgMnSc4	třetí
potomka	potomek	k1gMnSc4	potomek
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
narodila	narodit	k5eAaPmAgFnS	narodit
druhá	druhý	k4xOgNnPc4	druhý
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
,	,	kIx,	,
synové	syn	k1gMnPc1	syn
Leo	Leo	k1gMnSc1	Leo
a	a	k8xC	a
Lenny	Lenny	k?	Lenny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
během	během	k7c2	během
juniorské	juniorský	k2eAgFnSc2d1	juniorská
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
tenista	tenista	k1gMnSc1	tenista
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
firmou	firma	k1gFnSc7	firma
IMG	IMG	kA	IMG
<g/>
.	.	kIx.	.
</s>
<s>
Jejích	její	k3xOp3gFnPc2	její
služeb	služba	k1gFnPc2	služba
přestal	přestat	k5eAaPmAgInS	přestat
využívat	využívat	k5eAaImF	využívat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
ziskem	zisk	k1gInSc7	zisk
prvního	první	k4xOgInSc2	první
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnPc4d1	obchodní
záležitosti	záležitost	k1gFnPc4	záležitost
následně	následně	k6eAd1	následně
delegoval	delegovat	k5eAaBmAgMnS	delegovat
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Roger	Roger	k1gMnSc1	Roger
Federer	Federra	k1gFnPc2	Federra
Management	management	k1gInSc4	management
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
právního	právní	k2eAgMnSc2d1	právní
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
,	,	kIx,	,
finančního	finanční	k2eAgMnSc2d1	finanční
poradce	poradce	k1gMnSc2	poradce
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
Lynette	Lynett	k1gInSc5	Lynett
Federerové	Federerové	k2eAgInSc2d1	Federerové
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
partnerka	partnerka	k1gFnSc1	partnerka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Vavrinecová	Vavrinecová	k1gFnSc1	Vavrinecová
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
pověřena	pověřit	k5eAaPmNgFnS	pověřit
záležitostmi	záležitost	k1gFnPc7	záležitost
styku	styk	k1gInSc3	styk
s	s	k7c7	s
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
organizací	organizace	k1gFnSc7	organizace
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
manažer	manažer	k1gMnSc1	manažer
Tony	Tony	k1gMnSc1	Tony
Godsick	Godsick	k1gMnSc1	Godsick
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
opustil	opustit	k5eAaPmAgInS	opustit
společnost	společnost	k1gFnSc4	společnost
IMG	IMG	kA	IMG
Worldwide	Worldwid	k1gMnSc5	Worldwid
Inc	Inc	k1gMnSc5	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
exkluzivitu	exkluzivita	k1gFnSc4	exkluzivita
na	na	k7c4	na
zastupování	zastupování	k1gNnSc4	zastupování
Federera	Federero	k1gNnSc2	Federero
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
s	s	k7c7	s
tenistou	tenista	k1gMnSc7	tenista
cestuje	cestovat	k5eAaImIp3nS	cestovat
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
fond	fond	k1gInSc4	fond
Roger	Roger	k1gInSc1	Roger
Federer	Federer	k1gInSc1	Federer
Foundation	Foundation	k1gInSc4	Foundation
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
znevýhodněných	znevýhodněný	k2eAgMnPc2d1	znevýhodněný
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydražil	vydražit	k5eAaPmAgMnS	vydražit
raketu	raketa	k1gFnSc4	raketa
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výtěžek	výtěžek	k1gInSc1	výtěžek
směřoval	směřovat	k5eAaImAgInS	směřovat
osobám	osoba	k1gFnPc3	osoba
zasaženým	zasažený	k2eAgInSc7d1	zasažený
ničivým	ničivý	k2eAgInSc7d1	ničivý
hurikánem	hurikán	k1gInSc7	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
jej	on	k3xPp3gMnSc4	on
Dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
březnového	březnový	k2eAgInSc2d1	březnový
Mastersu	Masters	k1gInSc2	Masters
Pacific	Pacifice	k1gFnPc2	Pacifice
Life	Life	k1gFnSc1	Life
Open	Open	k1gInSc1	Open
2005	[number]	k4	2005
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
exhibici	exhibice	k1gFnSc4	exhibice
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
několik	několik	k4yIc1	několik
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
hráček	hráčka	k1gFnPc2	hráčka
hrajících	hrající	k2eAgFnPc2d1	hrající
na	na	k7c6	na
okruzích	okruh	k1gInPc6	okruh
ATP	atp	kA	atp
a	a	k8xC	a
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Výměna	výměna	k1gFnSc1	výměna
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Rally	Rall	k1gInPc1	Rall
for	forum	k1gNnPc2	forum
Relief	Relief	k1gInSc1	Relief
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
obětem	oběť	k1gFnPc3	oběť
ničivé	ničivý	k2eAgFnSc2d1	ničivá
tsunami	tsunami	k1gNnSc4	tsunami
po	po	k7c4	po
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
veřejných	veřejný	k2eAgNnPc6d1	veřejné
sděleních	sdělení	k1gNnPc6	sdělení
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
obecné	obecný	k2eAgNnSc4d1	obecné
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c4	o
nemoci	nemoc	k1gFnPc4	nemoc
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
ničivé	ničivý	k2eAgNnSc4d1	ničivé
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
postihlo	postihnout	k5eAaPmAgNnS	postihnout
zejména	zejména	k9	zejména
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
předními	přední	k2eAgMnPc7d1	přední
tenisty	tenista	k1gMnPc7	tenista
-	-	kIx~	-
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
<g/>
,	,	kIx,	,
Novakem	Novak	k1gMnSc7	Novak
Djokovićem	Djoković	k1gMnSc7	Djoković
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnSc2	Anda
Roddickem	Roddicko	k1gNnSc7	Roddicko
<g/>
,	,	kIx,	,
Kim	Kim	k1gFnSc7	Kim
Clijstersovou	Clijstersův	k2eAgFnSc7d1	Clijstersův
<g/>
,	,	kIx,	,
Serenou	Serený	k2eAgFnSc7d1	Serený
Williamsovou	Williamsová	k1gFnSc7	Williamsová
<g/>
,	,	kIx,	,
Lleytonem	Lleyton	k1gInSc7	Lleyton
Hewitem	Hewit	k1gInSc7	Hewit
a	a	k8xC	a
Samanthou	Samantha	k1gFnSc7	Samantha
Stosurovou	stosurový	k2eAgFnSc4d1	stosurový
charitativní	charitativní	k2eAgNnSc4d1	charitativní
vystoupení	vystoupení	k1gNnSc4	vystoupení
pojmenované	pojmenovaný	k2eAgNnSc4d1	pojmenované
"	"	kIx"	"
<g/>
Úder	úder	k1gInSc4	úder
pro	pro	k7c4	pro
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hit	hit	k1gInSc1	hit
for	forum	k1gNnPc2	forum
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dvorec	dvorec	k1gInSc4	dvorec
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
společně	společně	k6eAd1	společně
místo	místo	k1gNnSc4	místo
posledního	poslední	k2eAgInSc2d1	poslední
dne	den	k1gInSc2	den
přípravy	příprava	k1gFnSc2	příprava
před	před	k7c7	před
úvodním	úvodní	k2eAgInSc7d1	úvodní
grandslamem	grandslam	k1gInSc7	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gInSc1	Open
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Tenisté	tenista	k1gMnPc1	tenista
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
dva	dva	k4xCgInPc4	dva
čtyřčlenné	čtyřčlenný	k2eAgInPc4d1	čtyřčlenný
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
barev	barva	k1gFnPc2	barva
haitské	haitský	k2eAgFnSc2d1	Haitská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
na	na	k7c6	na
kurtu	kurt	k1gInSc6	kurt
odehráli	odehrát	k5eAaPmAgMnP	odehrát
trojhru	trojhra	k1gFnSc4	trojhra
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
byl	být	k5eAaImAgInS	být
zaslán	zaslat	k5eAaPmNgInS	zaslat
lidem	lid	k1gInSc7	lid
postiženým	postižený	k2eAgInSc7d1	postižený
tímto	tento	k3xDgInSc7	tento
přírodním	přírodní	k2eAgInSc7d1	přírodní
živlem	živel	k1gInSc7	živel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ženevském	ženevský	k2eAgInSc6d1	ženevský
Světovém	světový	k2eAgInSc6d1	světový
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
fóru	fór	k1gInSc6	fór
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
přidružené	přidružený	k2eAgFnSc2d1	přidružená
organizace	organizace	k1gFnSc2	organizace
Young	Young	k1gMnSc1	Young
Global	globat	k5eAaImAgMnS	globat
Leaders	Leaders	k1gInSc4	Leaders
(	(	kIx(	(
<g/>
Mladých	mladý	k2eAgMnPc2d1	mladý
globálních	globální	k2eAgMnPc2d1	globální
lídrů	lídr	k1gMnPc2	lídr
<g/>
,	,	kIx,	,
osobností	osobnost	k1gFnPc2	osobnost
napříč	napříč	k6eAd1	napříč
obory	obor	k1gInPc1	obor
do	do	k7c2	do
40	[number]	k4	40
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
uznání	uznání	k1gNnSc2	uznání
jeho	on	k3xPp3gNnSc2	on
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
příspěvku	příspěvek	k1gInSc2	příspěvek
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
dalšího	další	k2eAgInSc2d1	další
charitativního	charitativní	k2eAgInSc2d1	charitativní
zápasu	zápas	k1gInSc2	zápas
"	"	kIx"	"
<g/>
Výměna	výměna	k1gFnSc1	výměna
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
konaného	konaný	k2eAgNnSc2d1	konané
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
obětí	oběť	k1gFnPc2	oběť
záplav	záplava	k1gFnPc2	záplava
v	v	k7c6	v
Queenslandu	Queenslando	k1gNnSc6	Queenslando
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
jej	on	k3xPp3gMnSc4	on
časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
zařadil	zařadit	k5eAaPmAgInS	zařadit
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
sta	sto	k4xCgNnSc2	sto
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
světových	světový	k2eAgFnPc2d1	světová
celebrit	celebrita	k1gFnPc2	celebrita
s	s	k7c7	s
výdělkem	výdělek	k1gInSc7	výdělek
79	[number]	k4	79
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
nejvýdělečnějších	výdělečný	k2eAgMnPc2d3	nejvýdělečnější
sportovců	sportovec	k1gMnPc2	sportovec
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
mu	on	k3xPp3gNnSc3	on
stejný	stejný	k2eAgInSc4d1	stejný
magazín	magazín	k1gInSc4	magazín
přisoudil	přisoudit	k5eAaPmAgMnS	přisoudit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sdílel	sdílet	k5eAaImAgInS	sdílet
s	s	k7c7	s
golfistou	golfista	k1gMnSc7	golfista
Tigerem	Tiger	k1gMnSc7	Tiger
Woodsem	Woods	k1gMnSc7	Woods
<g/>
,	,	kIx,	,
když	když	k8xS	když
měly	mít	k5eAaImAgInP	mít
jejich	jejich	k3xOp3gInPc1	jejich
zisky	zisk	k1gInPc1	zisk
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
46	[number]	k4	46
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Probíhající	probíhající	k2eAgInSc4d1	probíhající
Federerův	Federerův	k2eAgInSc4d1	Federerův
10	[number]	k4	10
<g/>
letý	letý	k2eAgInSc4d1	letý
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Nike	Nike	k1gFnSc2	Nike
byl	být	k5eAaImAgInS	být
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
na	na	k7c4	na
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Juniorská	juniorský	k2eAgFnSc1d1	juniorská
kariéra	kariéra	k1gFnSc1	kariéra
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
juniorského	juniorský	k2eAgInSc2d1	juniorský
tenisu	tenis	k1gInSc2	tenis
se	se	k3xPyFc4	se
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
soutěžích	soutěž	k1gFnPc6	soutěž
turnajů	turnaj	k1gInPc2	turnaj
odehrál	odehrát	k5eAaPmAgInS	odehrát
devadesát	devadesát	k4xCc4	devadesát
jedna	jeden	k4xCgFnSc1	jeden
utkání	utkání	k1gNnSc6	utkání
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
padesát	padesát	k4xCc1	padesát
devět	devět	k4xCc4	devět
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Premiérovým	premiérový	k2eAgInSc7d1	premiérový
Grand	grand	k1gMnSc1	grand
Slamem	slam	k1gInSc7	slam
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gNnSc1	Open
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
stopku	stopka	k1gFnSc4	stopka
vystavil	vystavit	k5eAaPmAgInS	vystavit
Švéd	Švéd	k1gMnSc1	Švéd
Andreas	Andreas	k1gMnSc1	Andreas
Vinciguerra	Vinciguerra	k1gMnSc1	Vinciguerra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
French	French	k1gInSc1	French
Open	Open	k1gNnSc1	Open
1998	[number]	k4	1998
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
Čechem	Čech	k1gMnSc7	Čech
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Levinským	Levinský	k2eAgMnSc7d1	Levinský
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
výsledku	výsledek	k1gInSc2	výsledek
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
"	"	kIx"	"
<g/>
double	double	k2eAgNnSc4d1	double
<g/>
"	"	kIx"	"
-	-	kIx~	-
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
dvouhry	dvouhra	k1gFnSc2	dvouhra
porazil	porazit	k5eAaPmAgInS	porazit
gruzínského	gruzínský	k2eAgMnSc4d1	gruzínský
hráče	hráč	k1gMnSc4	hráč
Irákliho	Irákli	k1gMnSc2	Irákli
Labadzeho	Labadze	k1gMnSc2	Labadze
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
o	o	k7c4	o
deblovou	deblový	k2eAgFnSc4d1	deblová
trofej	trofej	k1gFnSc4	trofej
přehrál	přehrát	k5eAaPmAgInS	přehrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Belgičana	Belgičan	k1gMnSc2	Belgičan
Oliviera	Oliviero	k1gNnSc2	Oliviero
Rochuse	Rochuse	k1gFnSc2	Rochuse
francouzsko-izraelský	francouzskozraelský	k2eAgInSc4d1	francouzsko-izraelský
pár	pár	k4xCyI	pár
Michaël	Michaëla	k1gFnPc2	Michaëla
Llodra	Llodra	k1gFnSc1	Llodra
a	a	k8xC	a
Andy	Anda	k1gFnPc1	Anda
Ram	Ram	k1gFnSc2	Ram
stejným	stejný	k2eAgInSc7d1	stejný
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
titul	titul	k1gInSc4	titul
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
také	také	k9	také
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
argentinskému	argentinský	k2eAgMnSc3d1	argentinský
hráči	hráč	k1gMnSc3	hráč
Davidu	David	k1gMnSc3	David
Nalbandianovi	Nalbandian	k1gMnSc3	Nalbandian
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pěti	pět	k4xCc2	pět
turnajovým	turnajový	k2eAgInPc3d1	turnajový
vítězstvím	vítězství	k1gNnSc7	vítězství
z	z	k7c2	z
juniorské	juniorský	k2eAgFnSc2d1	juniorská
dvouhry	dvouhra	k1gFnSc2	dvouhra
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
trofej	trofej	k1gFnSc4	trofej
na	na	k7c6	na
floridském	floridský	k2eAgNnSc6d1	floridské
Orange	Orange	k1gNnSc6	Orange
Bowlu	Bowl	k1gInSc2	Bowl
1998	[number]	k4	1998
kategorie	kategorie	k1gFnSc2	kategorie
18	[number]	k4	18
<g/>
letých	letý	k2eAgMnPc2d1	letý
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Argentince	Argentinec	k1gMnPc4	Argentinec
Guillerma	Guillermum	k1gNnSc2	Guillermum
Coriu	Corius	k1gMnSc3	Corius
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zápas	zápas	k1gInSc1	zápas
hraný	hraný	k2eAgInSc1d1	hraný
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
juniorské	juniorský	k2eAgFnSc6d1	juniorská
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
juniorském	juniorský	k2eAgNnSc6d1	juniorské
žebříčku	žebříčko	k1gNnSc6	žebříčko
ITF	ITF	kA	ITF
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
jej	on	k3xPp3gMnSc4	on
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
juniorským	juniorský	k2eAgMnSc7d1	juniorský
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisové	tenisový	k2eAgFnSc2d1	tenisová
sezóny	sezóna	k1gFnSc2	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
1998	[number]	k4	1998
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
z	z	k7c2	z
wimbledonské	wimbledonský	k2eAgFnSc2d1	wimbledonská
juniorky	juniorka	k1gFnSc2	juniorka
1998	[number]	k4	1998
mu	on	k3xPp3gMnSc3	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
start	start	k1gInSc4	start
na	na	k7c6	na
premiérovém	premiérový	k2eAgInSc6d1	premiérový
turnaji	turnaj	k1gInSc6	turnaj
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
profesionálem	profesionál	k1gMnSc7	profesionál
na	na	k7c6	na
gstaadském	gstaadský	k2eAgInSc6d1	gstaadský
Swiss	Swiss	k1gInSc1	Swiss
Open	Openo	k1gNnPc2	Openo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Argentinci	Argentinec	k1gMnPc1	Argentinec
Lucasi	Lucas	k1gMnPc1	Lucas
Arnoldu	Arnold	k1gMnSc6	Arnold
Kerovi	Kera	k1gMnSc6	Kera
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
odehrál	odehrát	k5eAaPmAgInS	odehrát
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
události	událost	k1gFnPc1	událost
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
mužské	mužský	k2eAgFnSc2d1	mužská
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
plnohodnotná	plnohodnotný	k2eAgFnSc1d1	plnohodnotná
sezóna	sezóna	k1gFnSc1	sezóna
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
nadešla	nadejít	k5eAaPmAgFnS	nadejít
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
obdržel	obdržet	k5eAaPmAgMnS	obdržet
osm	osm	k4xCc4	osm
divokých	divoký	k2eAgFnPc2d1	divoká
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
French	Frencha	k1gFnPc2	Frencha
Open	Open	k1gNnSc1	Open
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
premiérové	premiérový	k2eAgFnSc3d1	premiérová
grandslamové	grandslamový	k2eAgFnSc3d1	grandslamová
dvouhře	dvouhra	k1gFnSc3	dvouhra
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
favorizovaného	favorizovaný	k2eAgMnSc2d1	favorizovaný
australského	australský	k2eAgMnSc2d1	australský
hráče	hráč	k1gMnSc2	hráč
Patricka	Patricko	k1gNnSc2	Patricko
Raftera	Rafter	k1gMnSc2	Rafter
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
debutoval	debutovat	k5eAaBmAgMnS	debutovat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Australanem	Australan	k1gMnSc7	Australan
Lleytonem	Lleyton	k1gInSc7	Lleyton
Hewittem	Hewitt	k1gMnSc7	Hewitt
došli	dojít	k5eAaPmAgMnP	dojít
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nestačili	stačit	k5eNaBmAgMnP	stačit
na	na	k7c4	na
favorizovaný	favorizovaný	k2eAgInSc4d1	favorizovaný
pár	pár	k1gInSc4	pár
Björkman	Björkman	k1gMnSc1	Björkman
a	a	k8xC	a
Rafter	Rafter	k1gMnSc1	Rafter
<g/>
.	.	kIx.	.
</s>
<s>
Nejdále	daleko	k6eAd3	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1999	[number]	k4	1999
probojoval	probojovat	k5eAaPmAgMnS	probojovat
na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
turnaji	turnaj	k1gInSc6	turnaj
CA-TennisTrophy	CA-TennisTropha	k1gFnSc2	CA-TennisTropha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
britského	britský	k2eAgMnSc2d1	britský
vítěze	vítěz	k1gMnSc2	vítěz
Grega	Greg	k1gMnSc2	Greg
Rusedskiho	Rusedski	k1gMnSc2	Rusedski
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
daviscupovém	daviscupový	k2eAgInSc6d1	daviscupový
týmu	tým	k1gInSc6	tým
debutoval	debutovat	k5eAaBmAgInS	debutovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
neuchâtelským	uchâtelský	k2eNgNnPc3d1	uchâtelský
utkáním	utkání	k1gNnPc3	utkání
1	[number]	k4	1
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Družstvu	družstvo	k1gNnSc3	družstvo
helvetského	helvetský	k2eAgInSc2d1	helvetský
kříže	kříž	k1gInSc2	kříž
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
páteční	páteční	k2eAgNnSc1d1	páteční
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Davidem	David	k1gMnSc7	David
Sanguinettim	Sanguinettima	k1gFnPc2	Sanguinettima
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
stovky	stovka	k1gFnSc2	stovka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
95	[number]	k4	95
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
hráčem	hráč	k1gMnSc7	hráč
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c6	na
marseillském	marseillský	k2eAgInSc6d1	marseillský
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
když	když	k8xS	když
zdolal	zdolat	k5eAaPmAgMnS	zdolat
pátého	pátý	k4xOgInSc2	pátý
tenistu	tenista	k1gMnSc4	tenista
žebříčku	žebříček	k1gInSc6	žebříček
Carlose	Carlosa	k1gFnSc6	Carlosa
Moyu	Moyus	k1gInSc6	Moyus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
finále	finále	k1gNnSc2	finále
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
únorovém	únorový	k2eAgNnSc6d1	únorové
Open	Open	k1gNnSc1	Open
13	[number]	k4	13
hraném	hraný	k2eAgNnSc6d1	hrané
v	v	k7c6	v
Marseille	Marseille	k1gFnSc1	Marseille
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
o	o	k7c4	o
titul	titul	k1gInSc4	titul
porazil	porazit	k5eAaPmAgMnS	porazit
krajan	krajan	k1gMnSc1	krajan
Marc	Marc	k1gFnSc4	Marc
Rosset	Rosseta	k1gFnPc2	Rosseta
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
boje	boj	k1gInSc2	boj
o	o	k7c4	o
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
trofej	trofej	k1gFnSc4	trofej
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c6	na
říjnovém	říjnový	k2eAgInSc6d1	říjnový
Swiss	Swiss	k1gInSc1	Swiss
Indoors	Indoors	k1gInSc1	Indoors
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neuspěl	uspět	k5eNaPmAgMnS	uspět
se	s	k7c7	s
Švédem	Švéd	k1gMnSc7	Švéd
Thomasem	Thomas	k1gMnSc7	Thomas
Enqvistem	Enqvist	k1gMnSc7	Enqvist
po	po	k7c6	po
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
nejdále	daleko	k6eAd3	daleko
došel	dojít	k5eAaPmAgMnS	dojít
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
španělský	španělský	k2eAgMnSc1d1	španělský
antukář	antukář	k1gMnSc1	antukář
À	À	k?	À
Corretja	Corretja	k1gMnSc1	Corretja
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Jevgenijem	Jevgenij	k1gMnSc7	Jevgenij
Kafelnikovem	Kafelnikov	k1gInSc7	Kafelnikov
<g/>
,	,	kIx,	,
předvedl	předvést	k5eAaPmAgMnS	předvést
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
majorový	majorový	k2eAgInSc4d1	majorový
výsledek	výsledek	k1gInSc4	výsledek
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
na	na	k7c6	na
Hrách	hra	k1gFnPc6	hra
XXVII	XXVII	kA	XXVII
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Výrazněji	výrazně	k6eAd2	výrazně
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
německému	německý	k2eAgMnSc3d1	německý
olympionikovi	olympionik	k1gMnSc3	olympionik
Tommymu	Tommym	k1gInSc2	Tommym
Haasovi	Haas	k1gMnSc3	Haas
a	a	k8xC	a
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
zápasu	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
neuspěl	uspět	k5eNaPmAgMnS	uspět
s	s	k7c7	s
Francouzem	Francouz	k1gMnSc7	Francouz
Arnaudem	Arnaud	k1gInSc7	Arnaud
di	di	k?	di
Pasqualem	Pasqual	k1gMnSc7	Pasqual
po	po	k7c6	po
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
říjnovém	říjnový	k2eAgInSc6d1	říjnový
basilejském	basilejský	k2eAgInSc6d1	basilejský
turnaji	turnaj	k1gInSc6	turnaj
si	se	k3xPyFc3	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dominikem	Dominik	k1gMnSc7	Dominik
Hrbatým	hrbatý	k2eAgMnSc7d1	hrbatý
zahrál	zahrát	k5eAaPmAgInS	zahrát
své	svůj	k3xOyFgNnSc4	svůj
premiérové	premiérový	k2eAgNnSc1d1	premiérové
finále	finále	k1gNnSc1	finále
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
však	však	k9	však
nestačili	stačit	k5eNaBmAgMnP	stačit
na	na	k7c4	na
americko-jihoafrickou	americkoihoafrický	k2eAgFnSc4d1	americko-jihoafrický
dvojici	dvojice	k1gFnSc4	dvojice
Donald	Donald	k1gMnSc1	Donald
Johnson	Johnson	k1gMnSc1	Johnson
a	a	k8xC	a
Piet	pieta	k1gFnPc2	pieta
Norval	Norval	k1gFnSc2	Norval
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
tenista	tenista	k1gMnSc1	tenista
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
tréninkové	tréninkový	k2eAgNnSc4d1	tréninkové
zázemí	zázemí	k1gNnSc4	zázemí
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
svazu	svaz	k1gInSc2	svaz
Swiss	Swissa	k1gFnPc2	Swissa
Tennis	Tennis	k1gFnSc2	Tennis
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
novým	nový	k2eAgMnSc7d1	nový
osobním	osobní	k2eAgMnSc7d1	osobní
trenérem	trenér	k1gMnSc7	trenér
stal	stát	k5eAaPmAgMnS	stát
Švéd	Švéd	k1gMnSc1	Švéd
Peter	Peter	k1gMnSc1	Peter
Lundgren	Lundgrna	k1gFnPc2	Lundgrna
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Kondičním	kondiční	k2eAgMnSc7d1	kondiční
koučem	kouč	k1gMnSc7	kouč
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
Pierre	Pierr	k1gInSc5	Pierr
Paganini	Paganin	k2eAgMnPc5d1	Paganin
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Toura	k1gFnPc2	Toura
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
během	během	k7c2	během
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
turnaji	turnaj	k1gInSc6	turnaj
Milan	Milan	k1gMnSc1	Milan
Indoor	Indoor	k1gMnSc1	Indoor
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
francouzského	francouzský	k2eAgMnSc4d1	francouzský
hráče	hráč	k1gMnSc4	hráč
Juliena	Julien	k1gMnSc2	Julien
Bouttera	Boutter	k1gMnSc2	Boutter
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
pak	pak	k6eAd1	pak
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Jonase	Jonasa	k1gFnSc6	Jonasa
Björkmana	Björkman	k1gMnSc2	Björkman
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
si	se	k3xPyFc3	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Maratem	Marat	k1gMnSc7	Marat
Safinem	Safin	k1gMnSc7	Safin
připsal	připsat	k5eAaPmAgMnS	připsat
druhou	druhý	k4xOgFnSc4	druhý
deblovou	deblový	k2eAgFnSc4d1	deblová
trofej	trofej	k1gFnSc4	trofej
z	z	k7c2	z
gstaadského	gstaadský	k2eAgNnSc2d1	gstaadský
UBS	UBS	kA	UBS
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc3	slam
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
španělského	španělský	k2eAgMnSc4d1	španělský
antukáře	antukář	k1gMnSc4	antukář
À	À	k?	À
Corretju	Corretju	k1gMnSc1	Corretju
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
skončil	skončit	k5eAaPmAgInS	skončit
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
osmičkou	osmička	k1gFnSc7	osmička
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
wimbledonském	wimbledonský	k2eAgInSc6d1	wimbledonský
pažitu	pažit	k1gInSc6	pažit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
přemožitelem	přemožitel	k1gMnSc7	přemožitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Brit	Brit	k1gMnSc1	Brit
Tim	Tim	k?	Tim
Henman	Henman	k1gMnSc1	Henman
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
kolo	kolo	k1gNnSc4	kolo
dříve	dříve	k6eAd2	dříve
sehrál	sehrát	k5eAaPmAgInS	sehrát
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
trávě	tráva	k1gFnSc6	tráva
památné	památný	k2eAgNnSc4d1	památné
utkání	utkání	k1gNnSc4	utkání
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kvality	kvalita	k1gFnSc2	kvalita
proti	proti	k7c3	proti
Petu	Pet	k1gMnSc3	Pet
Samprasovi	Sampras	k1gMnSc3	Sampras
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
získal	získat	k5eAaPmAgMnS	získat
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
po	po	k7c6	po
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Duel	duel	k1gInSc1	duel
představoval	představovat	k5eAaImAgInS	představovat
symbolické	symbolický	k2eAgNnSc4d1	symbolické
předání	předání	k1gNnSc4	předání
"	"	kIx"	"
<g/>
wimbledonského	wimbledonský	k2eAgNnSc2d1	wimbledonské
žezla	žezlo	k1gNnSc2	žezlo
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
odcházejícím	odcházející	k2eAgMnSc7d1	odcházející
a	a	k8xC	a
nastupujícím	nastupující	k2eAgMnSc7d1	nastupující
šampiónem	šampión	k1gMnSc7	šampión
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
finále	finále	k1gNnSc2	finále
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
Rotterdam	Rotterdam	k1gInSc4	Rotterdam
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Francouz	Francouz	k1gMnSc1	Francouz
Nicolas	Nicolas	k1gMnSc1	Nicolas
Escudé	Escudý	k2eAgFnSc2d1	Escudý
<g/>
,	,	kIx,	,
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c4	na
Davidoff	Davidoff	k1gInSc4	Davidoff
Swiss	Swiss	k1gInSc4	Swiss
Indoors	Indoorsa	k1gFnPc2	Indoorsa
<g/>
,	,	kIx,	,
když	když	k8xS	když
opět	opět	k6eAd1	opět
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Timem	Tim	k1gMnSc7	Tim
Henmanem	Henman	k1gMnSc7	Henman
<g/>
.	.	kIx.	.
</s>
<s>
Debutový	debutový	k2eAgInSc1d1	debutový
start	start	k1gInSc1	start
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
týmu	tým	k1gInSc6	tým
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
proměnil	proměnit	k5eAaPmAgMnS	proměnit
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
partnerkou	partnerka	k1gFnSc7	partnerka
<g/>
,	,	kIx,	,
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
Martinou	Martina	k1gFnSc7	Martina
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
čtyři	čtyři	k4xCgNnPc1	čtyři
mezistátní	mezistátní	k2eAgNnPc1d1	mezistátní
utkání	utkání	k1gNnPc1	utkání
a	a	k8xC	a
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
si	se	k3xPyFc3	se
poradili	poradit	k5eAaPmAgMnP	poradit
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
hrajícími	hrající	k2eAgInPc7d1	hrající
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Monika	Monika	k1gFnSc1	Monika
Selešová	Selešová	k1gFnSc1	Selešová
a	a	k8xC	a
Jan-Michael	Jan-Michael	k1gMnSc1	Jan-Michael
Gambill	Gambill	k1gMnSc1	Gambill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
již	již	k6eAd1	již
plnil	plnit	k5eAaImAgInS	plnit
roli	role	k1gFnSc4	role
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
země	zem	k1gFnSc2	zem
helvetského	helvetský	k2eAgInSc2d1	helvetský
kříže	kříž	k1gInSc2	kříž
se	se	k3xPyFc4	se
probojovali	probojovat	k5eAaPmAgMnP	probojovat
do	do	k7c2	do
červnového	červnový	k2eAgNnSc2d1	červnové
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
raketách	raketa	k1gFnPc6	raketa
francouzských	francouzský	k2eAgMnPc2d1	francouzský
tenistů	tenista	k1gMnPc2	tenista
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
profesionální	profesionální	k2eAgFnSc2d1	profesionální
sezóny	sezóna	k1gFnSc2	sezóna
vkročil	vkročit	k5eAaPmAgInS	vkročit
lednovým	lednový	k2eAgNnSc7d1	lednové
finále	finále	k1gNnSc7	finále
sydneyského	sydneyský	k2eAgInSc2d1	sydneyský
turnaje	turnaj	k1gInSc2	turnaj
Adidas	Adidas	k1gMnSc1	Adidas
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
zdolal	zdolat	k5eAaPmAgMnS	zdolat
argentinského	argentinský	k2eAgMnSc4d1	argentinský
hráče	hráč	k1gMnSc4	hráč
Juana	Juan	k1gMnSc2	Juan
Ignacia	Ignacium	k1gNnSc2	Ignacium
Chelu	Chel	k1gInSc2	Chel
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květno	k1gNnSc6	květno
si	se	k3xPyFc3	se
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Rusem	Rus	k1gMnSc7	Rus
Maratem	Marat	k1gMnSc7	Marat
Safinem	Safin	k1gMnSc7	Safin
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
antukového	antukový	k2eAgInSc2d1	antukový
Hamburg	Hamburg	k1gInSc1	Hamburg
Masters	Masters	k1gInSc4	Masters
připsal	připsat	k5eAaPmAgInS	připsat
premiérový	premiérový	k2eAgInSc1d1	premiérový
triumf	triumf	k1gInSc1	triumf
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
Švýcara	Švýcar	k1gMnSc4	Švýcar
premiérový	premiérový	k2eAgInSc1d1	premiérový
průnik	průnik	k1gInSc1	průnik
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
trofej	trofej	k1gInSc1	trofej
pak	pak	k6eAd1	pak
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
CA-Tennis	CA-Tennis	k1gInSc1	CA-Tennis
Trophy	Tropha	k1gFnPc1	Tropha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přehrál	přehrát	k5eAaPmAgMnS	přehrát
českého	český	k2eAgMnSc4d1	český
hráče	hráč	k1gMnSc4	hráč
Jiřího	Jiří	k1gMnSc2	Jiří
Nováka	Novák	k1gMnSc2	Novák
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
halového	halový	k2eAgInSc2d1	halový
Milan	Milan	k1gMnSc1	Milan
Indoor	Indoor	k1gInSc1	Indoor
a	a	k8xC	a
miamského	miamský	k2eAgInSc2d1	miamský
Mastersu	Masters	k1gInSc2	Masters
NASDAQ-100	NASDAQ-100	k1gMnSc1	NASDAQ-100
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Andremu	Andrem	k1gMnSc3	Andrem
Agassimu	Agassim	k1gMnSc3	Agassim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc3	slam
se	se	k3xPyFc4	se
nejdále	daleko	k6eAd3	daleko
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
s	s	k7c7	s
Němcem	Němec	k1gMnSc7	Němec
Tommym	Tommym	k1gInSc4	Tommym
Haasem	Haas	k1gInSc7	Haas
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
šestnáctkou	šestnáctka	k1gFnSc7	šestnáctka
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gNnSc4	Open
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
stabilního	stabilní	k2eAgMnSc2d1	stabilní
deblového	deblový	k2eAgMnSc2d1	deblový
partnera	partner	k1gMnSc2	partner
Maxe	Max	k1gMnSc2	Max
Mirného	Mirný	k1gMnSc2	Mirný
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
halové	halový	k2eAgInPc1d1	halový
tituly	titul	k1gInPc1	titul
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Bělorusem	Bělorus	k1gMnSc7	Bělorus
Mirným	Mirný	k1gMnSc7	Mirný
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
si	se	k3xPyFc3	se
odvezli	odvézt	k5eAaPmAgMnP	odvézt
z	z	k7c2	z
únorového	únorový	k2eAgInSc2d1	únorový
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
Open	Openo	k1gNnPc2	Openo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
obhájil	obhájit	k5eAaPmAgMnS	obhájit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
hattrick	hattrick	k1gInSc4	hattrick
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
triumf	triumf	k1gInSc1	triumf
pak	pak	k6eAd1	pak
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
na	na	k7c6	na
říjnovém	říjnový	k2eAgInSc6d1	říjnový
Kremlin	Kremlin	k1gInSc1	Kremlin
Cupu	cup	k1gInSc6	cup
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
tým	tým	k1gInSc4	tým
podruhé	podruhé	k6eAd1	podruhé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Hopmanova	Hopmanův	k2eAgInSc2d1	Hopmanův
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
životní	životní	k2eAgFnSc7d1	životní
i	i	k8xC	i
týmovou	týmový	k2eAgFnSc7d1	týmová
partnerkou	partnerka	k1gFnSc7	partnerka
Mirkou	Mirka	k1gFnSc7	Mirka
Vavrinecovou	Vavrinecová	k1gFnSc7	Vavrinecová
nepostoupili	postoupit	k5eNaPmAgMnP	postoupit
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
nepřevedl	převést	k5eNaPmAgMnS	převést
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
týmu	tým	k1gInSc2	tým
družstvo	družstvo	k1gNnSc4	družstvo
přes	přes	k7c4	přes
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
když	když	k8xS	když
tenisté	tenista	k1gMnPc1	tenista
helvetského	helvetský	k2eAgInSc2d1	helvetský
kříže	kříž	k1gInSc2	kříž
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Debutovou	debutový	k2eAgFnSc4d1	debutová
účast	účast	k1gFnSc4	účast
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
-	-	kIx~	-
Tennis	Tennis	k1gFnPc2	Tennis
Masters	Masters	k1gInSc1	Masters
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
základní	základní	k2eAgFnSc4d1	základní
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
zastavila	zastavit	k5eAaPmAgFnS	zastavit
australská	australský	k2eAgFnSc1d1	australská
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Lleyton	Lleyton	k1gInSc1	Lleyton
Hewitt	Hewitt	k1gInSc1	Hewitt
po	po	k7c6	po
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
průběhu	průběh	k1gInSc6	průběh
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
sezónu	sezóna	k1gFnSc4	sezóna
poprvé	poprvé	k6eAd1	poprvé
zakončil	zakončit	k5eAaPmAgMnS	zakončit
v	v	k7c6	v
elitní	elitní	k2eAgFnSc6d1	elitní
světové	světový	k2eAgFnSc6d1	světová
desítce	desítka	k1gFnSc6	desítka
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
patřilo	patřit	k5eAaImAgNnS	patřit
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
figuroval	figurovat	k5eAaImAgInS	figurovat
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
celkově	celkově	k6eAd1	celkově
sedm	sedm	k4xCc4	sedm
singlových	singlový	k2eAgInPc2d1	singlový
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
deblové	deblový	k2eAgInPc4d1	deblový
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
premiérová	premiérový	k2eAgFnSc1d1	premiérová
grandslamová	grandslamový	k2eAgFnSc1d1	grandslamová
trofej	trofej	k1gFnSc1	trofej
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
získal	získat	k5eAaPmAgInS	získat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
finálovou	finálový	k2eAgFnSc7d1	finálová
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Australanem	Australan	k1gMnSc7	Australan
Markem	Marek	k1gMnSc7	Marek
Philippoussisem	Philippoussis	k1gInSc7	Philippoussis
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
také	také	k9	také
jediné	jediný	k2eAgNnSc4d1	jediné
deblové	deblový	k2eAgNnSc4d1	deblové
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Masters	k1gInSc4	Masters
během	během	k7c2	během
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
běloruským	běloruský	k2eAgMnSc7d1	běloruský
hráčem	hráč	k1gMnSc7	hráč
Maxem	Max	k1gMnSc7	Max
Mirným	Mirný	k1gMnSc7	Mirný
triumfovali	triumfovat	k5eAaBmAgMnP	triumfovat
na	na	k7c6	na
floridském	floridský	k2eAgNnSc6d1	floridské
Miami	Miami	k1gNnSc6	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
jediného	jediný	k2eAgNnSc2d1	jediné
finále	finále	k1gNnSc2	finále
dvouhry	dvouhra	k1gFnSc2	dvouhra
roku	rok	k1gInSc2	rok
-	-	kIx~	-
antukového	antukový	k2eAgInSc2d1	antukový
Rome	Rom	k1gMnSc5	Rom
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
titul	titul	k1gInSc4	titul
jej	on	k3xPp3gMnSc4	on
těsným	těsný	k2eAgInSc7d1	těsný
rozdílem	rozdíl	k1gInSc7	rozdíl
přehrál	přehrát	k5eAaPmAgMnS	přehrát
španělský	španělský	k2eAgMnSc1d1	španělský
antukář	antukář	k1gMnSc1	antukář
Félix	Félix	k1gInSc4	Félix
Mantilla	Mantillo	k1gNnSc2	Mantillo
až	až	k6eAd1	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc2	tiebreak
třetí	třetí	k4xOgFnSc2	třetí
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zlaté	zlatý	k2eAgFnSc6d1	zlatá
sérii	série	k1gFnSc6	série
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
na	na	k7c6	na
arabském	arabský	k2eAgInSc6d1	arabský
Dubai	Duba	k1gInSc6	Duba
Tennis	Tennis	k1gFnSc1	Tennis
Championships	Championships	k1gInSc1	Championships
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgMnS	poradit
s	s	k7c7	s
Čechem	Čech	k1gMnSc7	Čech
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
trofej	trofej	k1gFnSc4	trofej
pak	pak	k6eAd1	pak
přidal	přidat	k5eAaPmAgMnS	přidat
z	z	k7c2	z
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
CA-TennisTrophy	CA-TennisTropha	k1gMnSc2	CA-TennisTropha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
španělského	španělský	k2eAgMnSc4d1	španělský
hráče	hráč	k1gMnSc4	hráč
Carlose	Carlosa	k1gFnSc3	Carlosa
Moyu	Moyus	k1gInSc2	Moyus
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
životního	životní	k2eAgInSc2d1	životní
výsledku	výsledek	k1gInSc2	výsledek
v	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
postupem	postup	k1gInSc7	postup
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k9	jako
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
jednička	jednička	k1gFnSc1	jednička
dovedl	dovést	k5eAaPmAgMnS	dovést
družstvo	družstvo	k1gNnSc4	družstvo
k	k	k7c3	k
výhrám	výhra	k1gFnPc3	výhra
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
nad	nad	k7c7	nad
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
poměrem	poměr	k1gInSc7	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
zástupce	zástupce	k1gMnPc4	zástupce
helvetského	helvetský	k2eAgInSc2d1	helvetský
tenisu	tenis	k1gInSc2	tenis
Austrálie	Austrálie	k1gFnSc2	Austrálie
opět	opět	k6eAd1	opět
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Páteční	páteční	k2eAgFnSc4d1	páteční
dvouhru	dvouhra	k1gFnSc4	dvouhra
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
Parku	park	k1gInSc2	park
proti	proti	k7c3	proti
Philippoussisovi	Philippoussis	k1gMnSc3	Philippoussis
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rossetem	Rosset	k1gInSc7	Rosset
neuspěli	uspět	k5eNaPmAgMnP	uspět
v	v	k7c6	v
sobotní	sobotní	k2eAgFnSc6d1	sobotní
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
první	první	k4xOgFnSc4	první
singlovou	singlový	k2eAgFnSc4d1	singlová
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
ročníku	ročník	k1gInSc6	ročník
utržil	utržit	k5eAaPmAgInS	utržit
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
od	od	k7c2	od
Hewitta	Hewitt	k1gMnSc2	Hewitt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
zdolal	zdolat	k5eAaPmAgMnS	zdolat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
trofej	trofej	k1gFnSc4	trofej
sezóny	sezóna	k1gFnSc2	sezóna
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
utkání	utkání	k1gNnSc6	utkání
nestačil	stačit	k5eNaBmAgMnS	stačit
zkušený	zkušený	k2eAgMnSc1d1	zkušený
Američan	Američan	k1gMnSc1	Američan
Andre	Andr	k1gInSc5	Andr
Agassi	Agass	k1gMnSc3	Agass
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
zakončil	zakončit	k5eAaPmAgInS	zakončit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
za	za	k7c7	za
Roddickem	Roddick	k1gInSc7	Roddick
a	a	k8xC	a
před	před	k7c7	před
třetím	třetí	k4xOgInSc7	třetí
Ferrerem	Ferrer	k1gInSc7	Ferrer
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
znamenající	znamenající	k2eAgInSc1d1	znamenající
začátek	začátek	k1gInSc4	začátek
Federerovy	Federerův	k2eAgFnSc2d1	Federerova
dominance	dominance	k1gFnSc2	dominance
nad	nad	k7c7	nad
mužským	mužský	k2eAgInSc7d1	mužský
světovým	světový	k2eAgInSc7d1	světový
tenisem	tenis	k1gInSc7	tenis
přinesla	přinést	k5eAaPmAgFnS	přinést
Švýcarovi	Švýcar	k1gMnSc3	Švýcar
jedenáct	jedenáct	k4xCc1	jedenáct
výher	výhra	k1gFnPc2	výhra
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
finále	finále	k1gNnPc2	finále
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
rozešel	rozejít	k5eAaPmAgMnS	rozejít
po	po	k7c6	po
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Peterem	Peter	k1gMnSc7	Peter
Lundgrenem	Lundgren	k1gMnSc7	Lundgren
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgInS	odehrát
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
bez	bez	k7c2	bez
kouče	kouč	k1gMnSc2	kouč
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
stal	stát	k5eAaPmAgInS	stát
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
posun	posun	k1gInSc1	posun
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
zajistily	zajistit	k5eAaPmAgFnP	zajistit
body	bod	k1gInPc4	bod
z	z	k7c2	z
vítězného	vítězný	k2eAgInSc2d1	vítězný
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
úvodního	úvodní	k2eAgInSc2d1	úvodní
melbournského	melbournský	k2eAgInSc2d1	melbournský
grandslamu	grandslam	k1gInSc2	grandslam
přehrál	přehrát	k5eAaPmAgInS	přehrát
Marata	Marat	k2eAgNnPc4d1	Marat
Safina	Safino	k1gNnPc4	Safino
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
setrval	setrvat	k5eAaPmAgMnS	setrvat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
lídra	lídr	k1gMnSc2	lídr
klasifikace	klasifikace	k1gFnSc2	klasifikace
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tím	ten	k3xDgNnSc7	ten
absolutní	absolutní	k2eAgInSc1d1	absolutní
tenisový	tenisový	k2eAgInSc4d1	tenisový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
trvání	trvání	k1gNnSc2	trvání
237	[number]	k4	237
týdnů	týden	k1gInPc2	týden
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
i	i	k9	i
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
obhajobu	obhajoba	k1gFnSc4	obhajoba
titulu	titul	k1gInSc2	titul
zajistil	zajistit	k5eAaPmAgInS	zajistit
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c4	nad
Andy	Anda	k1gFnPc4	Anda
Roddickem	Roddicko	k1gNnSc7	Roddicko
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
major	major	k1gMnSc1	major
sezóny	sezóna	k1gFnSc2	sezóna
dobyl	dobýt	k5eAaPmAgMnS	dobýt
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
duelu	duel	k1gInSc6	duel
o	o	k7c4	o
titul	titul	k1gInSc4	titul
porazil	porazit	k5eAaPmAgMnS	porazit
Lleytona	Lleyton	k1gMnSc4	Lleyton
Hewitta	Hewitto	k1gNnSc2	Hewitto
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
tenistou	tenista	k1gMnSc7	tenista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
tři	tři	k4xCgFnPc1	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
Grand	grand	k1gMnSc1	grand
Slamů	slam	k1gInPc2	slam
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
podařil	podařit	k5eAaPmAgInS	podařit
švédskému	švédský	k2eAgMnSc3d1	švédský
hráči	hráč	k1gMnSc3	hráč
Matsi	Mats	k1gMnSc3	Mats
Wilanderovi	Wilander	k1gMnSc3	Wilander
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
athénských	athénský	k2eAgFnPc2d1	Athénská
Her	hra	k1gFnPc2	hra
XXVIII	XXVIII	kA	XXVIII
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
mužské	mužský	k2eAgFnSc2d1	mužská
dvouhry	dvouhra	k1gFnSc2	dvouhra
jej	on	k3xPp3gMnSc4	on
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Čech	Čech	k1gMnSc1	Čech
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
pro	pro	k7c4	pro
Švýcara	Švýcar	k1gMnSc4	Švýcar
znamenala	znamenat	k5eAaImAgFnS	znamenat
poslední	poslední	k2eAgFnSc4d1	poslední
singlovou	singlový	k2eAgFnSc4d1	singlová
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
šestnáct	šestnáct	k4xCc1	šestnáct
utkání	utkání	k1gNnPc2	utkání
dovedl	dovést	k5eAaPmAgInS	dovést
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
titulů	titul	k1gInPc2	titul
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
Hamburg	Hamburg	k1gInSc1	Hamburg
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nestačil	stačit	k5eNaBmAgInS	stačit
Guillermo	Guillerma	k1gFnSc5	Guillerma
Coria	Corium	k1gNnPc4	Corium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
přidal	přidat	k5eAaPmAgMnS	přidat
na	na	k7c6	na
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
površích	povrch	k1gInPc6	povrch
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
a	a	k8xC	a
Canada	Canada	k1gFnSc1	Canada
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
titul	titul	k1gInSc4	titul
porazil	porazit	k5eAaPmAgMnS	porazit
Tima	Timus	k1gMnSc4	Timus
Henmana	Henman	k1gMnSc4	Henman
a	a	k8xC	a
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
pak	pak	k6eAd1	pak
Andyho	Andy	k1gMnSc4	Andy
Roddicka	Roddicko	k1gNnSc2	Roddicko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ATP	atp	kA	atp
International	International	k1gFnPc1	International
Series	Series	k1gMnSc1	Series
Gold	Gold	k1gMnSc1	Gold
podruhé	podruhé	k6eAd1	podruhé
opanoval	opanovat	k5eAaPmAgMnS	opanovat
dubajský	dubajský	k2eAgInSc4d1	dubajský
turnaj	turnaj	k1gInSc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Trofej	trofej	k1gInSc1	trofej
také	také	k9	také
obhájil	obhájit	k5eAaPmAgInS	obhájit
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
Hewittem	Hewitt	k1gInSc7	Hewitt
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
obhájit	obhájit	k5eAaPmF	obhájit
dva	dva	k4xCgInPc4	dva
Grand	grand	k1gMnSc1	grand
Slamy	slam	k1gInPc4	slam
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Andyho	Andy	k1gMnSc4	Andy
Roddicka	Roddicko	k1gNnSc2	Roddicko
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
přehrál	přehrát	k5eAaPmAgMnS	přehrát
veterána	veterán	k1gMnSc4	veterán
Andreho	Andre	k1gMnSc4	Andre
Agassiho	Agassi	k1gMnSc4	Agassi
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbylých	zbylý	k2eAgInPc6d1	zbylý
dvou	dva	k4xCgInPc6	dva
turnajích	turnaj	k1gInPc6	turnaj
velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
ruský	ruský	k2eAgMnSc1d1	ruský
hráč	hráč	k1gMnSc1	hráč
Marat	Marat	k2eAgInSc4d1	Marat
Safin	Safin	k1gInSc4	Safin
a	a	k8xC	a
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
French	French	k1gInSc1	French
Open	Open	k1gNnSc4	Open
pak	pak	k6eAd1	pak
Španěl	Španěl	k1gMnSc1	Španěl
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
čtyři	čtyři	k4xCgInPc4	čtyři
turnajové	turnajový	k2eAgInPc4d1	turnajový
vavříny	vavřín	k1gInPc4	vavřín
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgMnSc1	jeden
antukový	antukový	k2eAgMnSc1d1	antukový
na	na	k7c4	na
Hamburg	Hamburg	k1gInSc4	Hamburg
Masters	Mastersa	k1gFnPc2	Mastersa
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
z	z	k7c2	z
amerických	americký	k2eAgInPc2d1	americký
betonů	beton	k1gInPc2	beton
vnanbsp	vnanbsp	k1gMnSc1	vnanbsp
<g/>
;	;	kIx,	;
<g/>
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc1	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
a	a	k8xC	a
Cincinnati	Cincinnati	k1gFnPc2	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
International	International	k1gMnSc1	International
Series	Series	k1gMnSc1	Series
Gold	Gold	k1gMnSc1	Gold
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgFnPc4	dva
trofeje	trofej	k1gFnPc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
Open	Open	k1gNnSc1	Open
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
na	na	k7c6	na
arabském	arabský	k2eAgInSc6d1	arabský
Dubai	Duba	k1gInSc6	Duba
Tennis	Tennis	k1gFnSc1	Tennis
Championships	Championships	k1gInSc1	Championships
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
obhájce	obhájce	k1gMnSc1	obhájce
vítězství	vítězství	k1gNnSc4	vítězství
ze	z	k7c2	z
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
hattrick	hattrick	k1gInSc4	hattrick
nezískal	získat	k5eNaPmAgInS	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
argentinského	argentinský	k2eAgMnSc4d1	argentinský
hráče	hráč	k1gMnSc4	hráč
Davida	David	k1gMnSc2	David
Nalbandiana	Nalbandian	k1gMnSc2	Nalbandian
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
tak	tak	k6eAd1	tak
ukončil	ukončit	k5eAaPmAgMnS	ukončit
sérii	série	k1gFnSc4	série
24	[number]	k4	24
finálových	finálový	k2eAgFnPc2d1	finálová
výher	výhra	k1gFnPc2	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
předešlém	předešlý	k2eAgInSc6d1	předešlý
ročníku	ročník	k1gInSc6	ročník
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
jedenácti	jedenáct	k4xCc6	jedenáct
událostech	událost	k1gFnPc6	událost
dvouhry	dvouhra	k1gFnSc2	dvouhra
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přidal	přidat	k5eAaPmAgMnS	přidat
jeden	jeden	k4xCgInSc4	jeden
deblový	deblový	k2eAgInSc4d1	deblový
titul	titul	k1gInSc4	titul
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
trávy	tráva	k1gFnSc2	tráva
Gerry	Gerra	k1gFnSc2	Gerra
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startoval	startovat	k5eAaBmAgInS	startovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
krajana	krajan	k1gMnSc2	krajan
Yvse	Yvse	k1gNnSc2	Yvse
Allegra	allegro	k1gNnSc2	allegro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
neopustil	opustit	k5eNaPmAgMnS	opustit
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
<g/>
,	,	kIx,	,
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
také	také	k9	také
obdržel	obdržet	k5eAaPmAgInS	obdržet
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gInSc1	Laureus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
pro	pro	k7c4	pro
Federera	Federer	k1gMnSc4	Federer
znamenala	znamenat	k5eAaImAgFnS	znamenat
hráčsky	hráčsky	k6eAd1	hráčsky
nedominantnější	dominantní	k2eNgInSc4d2	dominantní
rok	rok	k1gInSc4	rok
celé	celý	k2eAgFnSc2d1	celá
profesionální	profesionální	k2eAgFnSc2d1	profesionální
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
Grand	grand	k1gMnSc1	grand
Slamy	slam	k1gInPc7	slam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sedmnácti	sedmnáct	k4xCc2	sedmnáct
odehraných	odehraný	k2eAgInPc2d1	odehraný
turnajů	turnaj	k1gInPc2	turnaj
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
se	se	k3xPyFc4	se
na	na	k7c6	na
všech	všecek	k3xTgInPc2	všecek
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
srpnového	srpnový	k2eAgMnSc2d1	srpnový
Cincinnati	Cincinnati	k1gMnSc2	Cincinnati
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvanáctkrát	dvanáctkrát	k6eAd1	dvanáctkrát
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
porazil	porazit	k5eAaPmAgMnS	porazit
Kypřana	Kypřan	k1gMnSc4	Kypřan
Marcose	Marcosa	k1gFnSc3	Marcosa
Baghdatise	Baghdatise	k1gFnSc1	Baghdatise
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
následně	následně	k6eAd1	následně
obhájil	obhájit	k5eAaPmAgMnS	obhájit
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
španělským	španělský	k2eAgMnSc7d1	španělský
tenistou	tenista	k1gMnSc7	tenista
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
na	na	k7c6	na
newyorském	newyorský	k2eAgNnSc6d1	newyorské
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přehrál	přehrát	k5eAaPmAgInS	přehrát
Američana	Američan	k1gMnSc4	Američan
Andyho	Andy	k1gMnSc2	Andy
Roddicka	Roddicko	k1gNnSc2	Roddicko
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc4d1	čistý
grandslam	grandslam	k1gInSc4	grandslam
mu	on	k3xPp3gMnSc3	on
unikl	uniknout	k5eAaPmAgMnS	uniknout
prohraným	prohraná	k1gFnPc3	prohraná
finále	finále	k1gNnSc2	finále
antukového	antukový	k2eAgMnSc4d1	antukový
French	French	k1gMnSc1	French
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c6	na
Nadala	nadat	k5eAaPmAgNnP	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Budgeovi	Budgeus	k1gMnSc6	Budgeus
a	a	k8xC	a
Laverovi	Laver	k1gMnSc6	Laver
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestal	stát	k5eNaPmAgMnS	stát
třetím	třetí	k4xOgMnSc7	třetí
tenistou	tenista	k1gMnSc7	tenista
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
muž	muž	k1gMnSc1	muž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
a	a	k8xC	a
Laverova	Laverův	k2eAgInSc2d1	Laverův
výkonu	výkon	k1gInSc2	výkon
si	se	k3xPyFc3	se
však	však	k9	však
zahrál	zahrát	k5eAaPmAgMnS	zahrát
finále	finále	k1gNnSc7	finále
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
majorů	major	k1gMnPc2	major
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
se	se	k3xPyFc4	se
šestkrát	šestkrát	k6eAd1	šestkrát
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc6	Miami
<g/>
,	,	kIx,	,
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc6	Carl
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
favorizovaný	favorizovaný	k2eAgMnSc1d1	favorizovaný
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
kategorie	kategorie	k1gFnSc1	kategorie
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
série	série	k1gFnSc2	série
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
podzimní	podzimní	k2eAgFnSc4d1	podzimní
japonskou	japonský	k2eAgFnSc4d1	japonská
událost	událost	k1gFnSc4	událost
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
a	a	k8xC	a
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
ATP	atp	kA	atp
International	International	k1gMnSc2	International
Series	Series	k1gMnSc1	Series
získal	získat	k5eAaPmAgMnS	získat
tři	tři	k4xCgFnPc4	tři
trofeje	trofej	k1gFnPc4	trofej
z	z	k7c2	z
Dauhá	Dauhý	k2eAgNnPc1d1	Dauhý
<g/>
,	,	kIx,	,
Halle	Halle	k1gNnSc1	Halle
a	a	k8xC	a
Basileje	Basilej	k1gFnPc1	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
opanoval	opanovat	k5eAaPmAgMnS	opanovat
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
událost	událost	k1gFnSc4	událost
sezóny	sezóna	k1gFnSc2	sezóna
Turnaj	turnaj	k1gInSc1	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
duelu	duel	k1gInSc6	duel
roku	rok	k1gInSc2	rok
zdolal	zdolat	k5eAaPmAgMnS	zdolat
amerického	americký	k2eAgMnSc4d1	americký
hráče	hráč	k1gMnSc4	hráč
Jamese	Jamese	k1gFnSc2	Jamese
Blakea	Blakea	k1gFnSc1	Blakea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
klasifikaci	klasifikace	k1gFnSc6	klasifikace
neopustil	opustit	k5eNaPmAgMnS	opustit
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
držel	držet	k5eAaImAgInS	držet
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
bodů	bod	k1gInPc2	bod
před	před	k7c7	před
druhým	druhý	k4xOgNnSc7	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc2	pořadí
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
<g/>
,	,	kIx,	,
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
obdržel	obdržet	k5eAaPmAgInS	obdržet
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gInSc1	Laureus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
označil	označit	k5eAaPmAgMnS	označit
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Tignor	Tignor	k1gMnSc1	Tignor
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
editor	editor	k1gInSc4	editor
internetového	internetový	k2eAgNnSc2d1	internetové
média	médium	k1gNnSc2	médium
Tennis	Tennis	k1gFnSc2	Tennis
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
Federerovu	Federerův	k2eAgFnSc4d1	Federerova
sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
hned	hned	k6eAd1	hned
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
1969	[number]	k4	1969
australské	australský	k2eAgFnSc2d1	australská
legendy	legenda	k1gFnSc2	legenda
Roda	Rodus	k1gMnSc2	Rodus
Lavera	Laver	k1gMnSc2	Laver
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sezóně	sezóna	k1gFnSc6	sezóna
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
Grand	grand	k1gMnSc1	grand
Slamů	slam	k1gInPc2	slam
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
tenista	tenista	k1gMnSc1	tenista
historie	historie	k1gFnSc2	historie
potřetí	potřetí	k4xO	potřetí
získal	získat	k5eAaPmAgInS	získat
tři	tři	k4xCgInPc4	tři
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
porazil	porazit	k5eAaPmAgMnS	porazit
Chilana	Chilan	k1gMnSc4	Chilan
Fernanda	Fernando	k1gNnSc2	Fernando
Gonzáleze	Gonzáleze	k1gFnSc2	Gonzáleze
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
́	́	k?	́
<g/>
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
rozhodující	rozhodující	k2eAgNnSc4d1	rozhodující
utkání	utkání	k1gNnSc4	utkání
opět	opět	k6eAd1	opět
nad	nad	k7c7	nad
španělským	španělský	k2eAgMnSc7d1	španělský
tenistou	tenista	k1gMnSc7	tenista
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
kariérní	kariérní	k2eAgInSc4d1	kariérní
titul	titul	k1gInSc4	titul
na	na	k7c6	na
newyorském	newyorský	k2eAgNnSc6d1	newyorské
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
se	s	k7c7	s
Srbem	Srb	k1gMnSc7	Srb
Novakem	Novak	k1gMnSc7	Novak
Djokovićem	Djoković	k1gMnSc7	Djoković
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
grandslam	grandslam	k1gInSc1	grandslam
mu	on	k3xPp3gMnSc3	on
podruhé	podruhé	k6eAd1	podruhé
unikl	uniknout	k5eAaPmAgMnS	uniknout
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrál	prohrát	k5eAaPmAgMnS	prohrát
finále	finále	k1gNnSc4	finále
antukového	antukový	k2eAgMnSc2d1	antukový
French	Frencha	k1gFnPc2	Frencha
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nenašel	najít	k5eNaPmAgInS	najít
recept	recept	k1gInSc1	recept
na	na	k7c4	na
Nadala	nadat	k5eAaPmAgFnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
čtyřsetového	čtyřsetový	k2eAgInSc2d1	čtyřsetový
duelu	duel	k1gInSc2	duel
zněl	znět	k5eAaImAgInS	znět
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
čtyřech	čtyři	k4xCgFnPc6	čtyři
sezónách	sezóna	k1gFnPc6	sezóna
tak	tak	k9	tak
dokázal	dokázat	k5eAaPmAgInS	dokázat
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jedenáct	jedenáct	k4xCc1	jedenáct
z	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
uskutečněných	uskutečněný	k2eAgInPc2d1	uskutečněný
grandslamů	grandslam	k1gInPc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
se	se	k3xPyFc4	se
pětkrát	pětkrát	k6eAd1	pětkrát
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
dva	dva	k4xCgInPc1	dva
triumfy	triumf	k1gInPc1	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
obhájil	obhájit	k5eAaPmAgMnS	obhájit
titul	titul	k1gInSc4	titul
na	na	k7c6	na
hamburské	hamburský	k2eAgFnSc6d1	hamburská
antuce	antuka	k1gFnSc6	antuka
a	a	k8xC	a
poté	poté	k6eAd1	poté
přidal	přidat	k5eAaPmAgMnS	přidat
výhru	výhra	k1gFnSc4	výhra
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ATP	atp	kA	atp
International	International	k1gFnPc1	International
Series	Series	k1gMnSc1	Series
Gold	Gold	k1gMnSc1	Gold
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
událost	událost	k1gFnSc4	událost
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
ATP	atp	kA	atp
International	International	k1gMnSc1	International
Series	Series	k1gMnSc1	Series
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
další	další	k2eAgFnSc4d1	další
obhajobu	obhajoba	k1gFnSc4	obhajoba
z	z	k7c2	z
basilejského	basilejský	k2eAgMnSc2d1	basilejský
Swiss	Swiss	k1gInSc4	Swiss
Indoors	Indoorsa	k1gFnPc2	Indoorsa
<g/>
.	.	kIx.	.
</s>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
opanoval	opanovat	k5eAaPmAgMnS	opanovat
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
událost	událost	k1gFnSc4	událost
sezóny	sezóna	k1gFnSc2	sezóna
Turnaj	turnaj	k1gInSc1	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
duelu	duel	k1gInSc6	duel
zdolal	zdolat	k5eAaPmAgMnS	zdolat
španělského	španělský	k2eAgMnSc4d1	španělský
hráče	hráč	k1gMnSc4	hráč
Davida	David	k1gMnSc2	David
Ferrera	Ferrer	k1gMnSc2	Ferrer
po	po	k7c6	po
hladkém	hladký	k2eAgInSc6d1	hladký
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Rafaelu	Rafael	k1gMnSc3	Rafael
Nadalovi	Nadal	k1gMnSc3	Nadal
odehrál	odehrát	k5eAaPmAgMnS	odehrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
exhibici	exhibice	k1gFnSc4	exhibice
"	"	kIx"	"
<g/>
Bitva	bitva	k1gFnSc1	bitva
povrchů	povrch	k1gInPc2	povrch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
dvorci	dvorec	k1gInSc6	dvorec
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
pokrytého	pokrytý	k2eAgInSc2d1	pokrytý
antukou	antuka	k1gFnSc7	antuka
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
trávou	tráva	k1gFnSc7	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Dějištěm	dějiště	k1gNnSc7	dějiště
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
krytá	krytý	k2eAgFnSc1d1	krytá
aréna	aréna	k1gFnSc1	aréna
v	v	k7c6	v
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gFnSc1	Mallorca
<g/>
.	.	kIx.	.
</s>
<s>
Španěl	Španěl	k1gMnSc1	Španěl
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
těsným	těsný	k2eAgInSc7d1	těsný
poměrem	poměr	k1gInSc7	poměr
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
měsíc	měsíc	k1gInSc4	měsíc
se	se	k3xPyFc4	se
basilejský	basilejský	k2eAgMnSc1d1	basilejský
rodák	rodák	k1gMnSc1	rodák
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
koučem	kouč	k1gMnSc7	kouč
Tonym	Tony	k1gMnSc7	Tony
Rochem	Roch	k1gInSc7	Roch
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
trenérského	trenérský	k2eAgNnSc2d1	trenérské
vedení	vedení	k1gNnSc2	vedení
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
Estoril	Estorila	k1gFnPc2	Estorila
Open	Open	k1gNnSc1	Open
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
novým	nový	k2eAgMnSc7d1	nový
koučem	kouč	k1gMnSc7	kouč
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
stal	stát	k5eAaPmAgInS	stát
José	Josá	k1gFnSc2	Josá
Higueras	Higueras	k1gInSc1	Higueras
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
opět	opět	k6eAd1	opět
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
neopustil	opustit	k5eNaPmAgMnS	opustit
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
držel	držet	k5eAaImAgMnS	držet
před	před	k7c7	před
druhým	druhý	k4xOgMnSc7	druhý
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gInSc7	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
<g/>
,	,	kIx,	,
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
za	za	k7c7	za
sebou	se	k3xPyFc7	se
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gInSc4	Laureus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
finančních	finanční	k2eAgFnPc6d1	finanční
odměnách	odměna	k1gFnPc6	odměna
vydělal	vydělat	k5eAaPmAgInS	vydělat
10	[number]	k4	10
130	[number]	k4	130
620	[number]	k4	620
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
peněžní	peněžní	k2eAgInSc4d1	peněžní
zisk	zisk	k1gInSc4	zisk
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
hlavní	hlavní	k2eAgInSc1d1	hlavní
editor	editor	k1gInSc1	editor
Stephen	Stephen	k2eAgInSc1d1	Stephen
Tignor	Tignor	k1gInSc4	Tignor
z	z	k7c2	z
Tennis	Tennis	k1gFnSc2	Tennis
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
Federerovu	Federerův	k2eAgFnSc4d1	Federerova
sezónu	sezóna	k1gFnSc4	sezóna
2007	[number]	k4	2007
za	za	k7c7	za
šestou	šestý	k4xOgFnSc7	šestý
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
světového	světový	k2eAgInSc2d1	světový
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výkonům	výkon	k1gInPc3	výkon
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
na	na	k7c6	na
dvorcích	dvorec	k1gInPc6	dvorec
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
chování	chování	k1gNnSc3	chování
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
časopis	časopis	k1gInSc4	časopis
Time	Time	k1gInSc1	Time
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osobností	osobnost	k1gFnPc2	osobnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
legenda	legenda	k1gFnSc1	legenda
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
držitel	držitel	k1gMnSc1	držitel
čistého	čistý	k2eAgInSc2d1	čistý
grandslamu	grandslam	k1gInSc2	grandslam
Rod	rod	k1gInSc1	rod
Laver	lavra	k1gFnPc2	lavra
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
adresu	adresa	k1gFnSc4	adresa
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejobdivuhodnějších	obdivuhodný	k2eAgMnPc2d3	nejobdivuhodnější
šampiónů	šampión	k1gMnPc2	šampión
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yInSc2	co
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
určitě	určitě	k6eAd1	určitě
radovat	radovat	k5eAaImF	radovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ztratil	ztratit	k5eAaPmAgMnS	ztratit
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jeden	jeden	k4xCgMnSc1	jeden
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc1	slam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zářijovém	zářijový	k2eAgNnSc6d1	zářijové
US	US	kA	US
Open	Open	k1gMnSc1	Open
porazil	porazit	k5eAaPmAgMnS	porazit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Skota	Skot	k1gMnSc2	Skot
Andyho	Andy	k1gMnSc2	Andy
Murrayho	Murray	k1gMnSc2	Murray
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
z	z	k7c2	z
Flushing	Flushing	k1gInSc1	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
boje	boj	k1gInSc2	boj
o	o	k7c4	o
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
jej	on	k3xPp3gNnSc4	on
přehrál	přehrát	k5eAaPmAgInS	přehrát
španělský	španělský	k2eAgMnSc1d1	španělský
hráč	hráč	k1gMnSc1	hráč
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
mu	on	k3xPp3gNnSc3	on
hladce	hladko	k6eAd1	hladko
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gNnSc4	Open
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
první	první	k4xOgFnSc4	první
finálovou	finálový	k2eAgFnSc4d1	finálová
porážku	porážka	k1gFnSc4	porážka
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
pětisetová	pětisetový	k2eAgFnSc1d1	pětisetová
bitva	bitva	k1gFnSc1	bitva
vyzněla	vyznít	k5eAaPmAgFnS	vyznít
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Španěla	Španěl	k1gMnSc2	Španěl
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Výhrou	výhra	k1gFnSc7	výhra
tak	tak	k8xC	tak
Federerovi	Federer	k1gMnSc3	Federer
zmařil	zmařit	k5eAaPmAgMnS	zmařit
šanci	šance	k1gFnSc4	šance
odpoutat	odpoutat	k5eAaPmF	odpoutat
se	se	k3xPyFc4	se
od	od	k7c2	od
rekordní	rekordní	k2eAgFnSc1d1	rekordní
série	série	k1gFnSc1	série
pěti	pět	k4xCc2	pět
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Švýcar	Švýcar	k1gMnSc1	Švýcar
držel	držet	k5eAaImAgMnS	držet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Björnem	Björno	k1gNnSc7	Björno
Borgem	Borg	k1gMnSc7	Borg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úvodním	úvodní	k2eAgInSc6d1	úvodní
melbournském	melbournský	k2eAgInSc6d1	melbournský
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
byl	být	k5eAaImAgMnS	být
nad	nad	k7c4	nad
jeho	jeho	k3xOp3gFnPc4	jeho
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tak	tak	k6eAd1	tak
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jeho	jeho	k3xOp3gFnSc4	jeho
šňůru	šňůra	k1gFnSc4	šňůra
deseti	deset	k4xCc2	deset
grandslamových	grandslamový	k2eAgNnPc2d1	grandslamové
finále	finále	k1gNnPc2	finále
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
rekordem	rekord	k1gInSc7	rekord
se	se	k3xPyFc4	se
však	však	k9	však
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
historických	historický	k2eAgFnPc2d1	historická
statistik	statistika	k1gFnPc2	statistika
mužského	mužský	k2eAgInSc2d1	mužský
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
antukových	antukový	k2eAgInPc2d1	antukový
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
a	a	k8xC	a
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc3	Carl
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
naději	nadát	k5eAaBmIp1nS	nadát
na	na	k7c4	na
titul	titul	k1gInSc4	titul
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
zmařil	zmařit	k5eAaPmAgInS	zmařit
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgInS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
třetí	třetí	k4xOgFnSc2	třetí
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
série	série	k1gFnSc2	série
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
tři	tři	k4xCgFnPc4	tři
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
v	v	k7c6	v
Estorilu	Estoril	k1gInSc6	Estoril
<g/>
,	,	kIx,	,
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
třetí	třetí	k4xOgInSc4	třetí
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
na	na	k7c6	na
halové	halový	k2eAgFnSc6d1	halová
basilejské	basilejský	k2eAgFnSc6d1	Basilejská
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pekingských	pekingský	k2eAgFnPc6d1	Pekingská
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Stanislase	Stanislas	k1gInSc6	Stanislas
Wawrinky	Wawrinka	k1gFnSc2	Wawrinka
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřsetovém	čtyřsetový	k2eAgNnSc6d1	čtyřsetový
finále	finále	k1gNnSc6	finále
Švýcaři	Švýcar	k1gMnPc1	Švýcar
porazili	porazit	k5eAaPmAgMnP	porazit
švédskou	švédský	k2eAgFnSc7d1	švédská
dvojicí	dvojice	k1gFnSc7	dvojice
Simon	Simona	k1gFnPc2	Simona
Aspelin	Aspelina	k1gFnPc2	Aspelina
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Johansson	Johansson	k1gMnSc1	Johansson
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
osmého	osmý	k4xOgMnSc2	osmý
nasazeného	nasazený	k2eAgMnSc2d1	nasazený
Američana	Američan	k1gMnSc2	Američan
Jamese	Jamese	k1gFnSc2	Jamese
Blakea	Blakeus	k1gMnSc2	Blakeus
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
osmi	osm	k4xCc7	osm
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
když	když	k8xS	když
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
duelů	duel	k1gInPc2	duel
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
jediný	jediný	k2eAgInSc1d1	jediný
proti	proti	k7c3	proti
Radku	Radek	k1gMnSc3	Radek
Štěpánkovi	Štěpánek	k1gMnSc3	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
klasifikaci	klasifikace	k1gFnSc6	klasifikace
ATP	atp	kA	atp
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
nefiguroval	figurovat	k5eNaImAgInS	figurovat
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
patřilo	patřit	k5eAaImAgNnS	patřit
mu	on	k3xPp3gMnSc3	on
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
Nadalem	Nadal	k1gInSc7	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
obdržel	obdržet	k5eAaPmAgMnS	obdržet
počtvrté	počtvrté	k4xO	počtvrté
za	za	k7c7	za
sebou	se	k3xPyFc7	se
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gInSc4	Laureus
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
sportovce	sportovec	k1gMnSc4	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
dvě	dva	k4xCgFnPc4	dva
grandslamové	grandslamový	k2eAgFnPc4d1	grandslamová
trofeje	trofej	k1gFnPc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Švéda	Švéd	k1gMnSc4	Švéd
Robina	robin	k2eAgFnSc1d1	Robina
Söderlinga	Söderlinga	k1gFnSc1	Söderlinga
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
zkompletoval	zkompletovat	k5eAaPmAgInS	zkompletovat
tak	tak	k9	tak
jako	jako	k9	jako
šestý	šestý	k4xOgMnSc1	šestý
hráč	hráč	k1gMnSc1	hráč
historie	historie	k1gFnSc2	historie
kariérní	kariérní	k2eAgInSc4d1	kariérní
grandslam	grandslam	k1gInSc4	grandslam
-	-	kIx~	-
vítězství	vítězství	k1gNnSc4	vítězství
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
majorů	major	k1gMnPc2	major
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
opět	opět	k6eAd1	opět
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Američanem	Američan	k1gMnSc7	Američan
Andym	Andym	k1gInSc4	Andym
Roddickem	Roddicko	k1gNnSc7	Roddicko
v	v	k7c6	v
pěti	pět	k4xCc6	pět
setech	set	k1gInPc6	set
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
slavil	slavit	k5eAaImAgInS	slavit
šestou	šestý	k4xOgFnSc4	šestý
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
mu	on	k3xPp3gMnSc3	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
neopustil	opustit	k5eNaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Wimbledonská	wimbledonský	k2eAgFnSc1d1	wimbledonská
trofej	trofej	k1gFnSc1	trofej
znamenala	znamenat	k5eAaImAgFnS	znamenat
zisk	zisk	k1gInSc4	zisk
rekordního	rekordní	k2eAgNnSc2d1	rekordní
15	[number]	k4	15
<g/>
.	.	kIx.	.
grandslamu	grandslam	k1gInSc2	grandslam
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
překonal	překonat	k5eAaPmAgMnS	překonat
14	[number]	k4	14
titulů	titul	k1gInPc2	titul
Peta	Pet	k1gInSc2	Pet
Samprase	Samprasa	k1gFnSc3	Samprasa
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
parametru	parametr	k1gInSc6	parametr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
mužem	muž	k1gMnSc7	muž
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
zbylých	zbylý	k2eAgFnPc2d1	zbylá
událostí	událost	k1gFnPc2	událost
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
melbournském	melbournský	k2eAgInSc6d1	melbournský
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gMnSc1	Open
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
španělskému	španělský	k2eAgMnSc3d1	španělský
hráči	hráč	k1gMnSc3	hráč
Rafaelu	Rafael	k1gMnSc3	Rafael
Nadalovi	Nadal	k1gMnSc3	Nadal
a	a	k8xC	a
na	na	k7c6	na
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
US	US	kA	US
Open	Open	k1gNnSc1	Open
jeho	jeho	k3xOp3gFnSc4	jeho
šňůru	šňůra	k1gFnSc4	šňůra
pěti	pět	k4xCc2	pět
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Argentinec	Argentinec	k1gMnSc1	Argentinec
Juan	Juan	k1gMnSc1	Juan
Martín	Martín	k1gMnSc1	Martín
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
pětisetovém	pětisetový	k2eAgNnSc6d1	Pětisetové
dramatu	drama	k1gNnSc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Masters	k1gInSc1	Masters
proměnil	proměnit	k5eAaPmAgInS	proměnit
dvě	dva	k4xCgFnPc4	dva
finálové	finálový	k2eAgFnPc4d1	finálová
účasti	účast	k1gFnPc4	účast
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Madrid	Madrid	k1gInSc4	Madrid
Open	Opena	k1gFnPc2	Opena
poprvé	poprvé	k6eAd1	poprvé
probíhajícím	probíhající	k2eAgInSc7d1	probíhající
na	na	k7c6	na
antukových	antukový	k2eAgInPc6d1	antukový
dvorcích	dvorec	k1gInPc6	dvorec
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
favorizovaným	favorizovaný	k2eAgMnSc7d1	favorizovaný
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
letního	letní	k2eAgNnSc2d1	letní
Cincinnati	Cincinnati	k1gFnSc4	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Srba	Srba	k1gMnSc1	Srba
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gFnSc2	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nově	nově	k6eAd1	nově
konstituované	konstituovaný	k2eAgFnPc4d1	konstituovaná
třetí	třetí	k4xOgFnPc4	třetí
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
kategorie	kategorie	k1gFnPc4	kategorie
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
500	[number]	k4	500
Series	Seriesa	k1gFnPc2	Seriesa
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
jako	jako	k9	jako
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
obhájce	obhájce	k1gMnSc1	obhájce
titulu	titul	k1gInSc2	titul
Djokovićovi	Djokovića	k1gMnSc3	Djokovića
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
Swiss	Swissa	k1gFnPc2	Swissa
Indoors	Indoorsa	k1gFnPc2	Indoorsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
ruského	ruský	k2eAgMnSc2d1	ruský
hráče	hráč	k1gMnSc2	hráč
a	a	k8xC	a
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
vítěze	vítěz	k1gMnSc2	vítěz
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Davyděnka	Davyděnka	k1gFnSc1	Davyděnka
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
klasifikaci	klasifikace	k1gFnSc6	klasifikace
ATP	atp	kA	atp
se	se	k3xPyFc4	se
po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Rafaela	Rafael	k1gMnSc2	Rafael
Nadala	nadat	k5eAaPmAgNnP	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Popáté	popáté	k4xO	popáté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
okruhu	okruh	k1gInSc2	okruh
ATP.	atp.	kA	atp.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
dokázal	dokázat	k5eAaPmAgMnS	dokázat
dobýt	dobýt	k5eAaPmF	dobýt
jediný	jediný	k2eAgMnSc1d1	jediný
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
melbournského	melbournský	k2eAgInSc2d1	melbournský
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Skota	Skot	k1gMnSc2	Skot
Andy	Anda	k1gFnSc2	Anda
Murrayho	Murray	k1gMnSc2	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
tak	tak	k6eAd1	tak
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
tenisový	tenisový	k2eAgInSc4d1	tenisový
rekord	rekord	k1gInSc4	rekord
na	na	k7c4	na
šestnáct	šestnáct	k4xCc4	šestnáct
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
trofejí	trofej	k1gFnPc2	trofej
z	z	k7c2	z
úvodního	úvodní	k2eAgInSc2d1	úvodní
majoru	major	k1gMnSc3	major
se	se	k3xPyFc4	se
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
Andreho	Andre	k1gMnSc2	Andre
Agassiho	Agassi	k1gMnSc2	Agassi
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
třech	tři	k4xCgNnPc6	tři
majorech	major	k1gMnPc6	major
skončil	skončit	k5eAaPmAgInS	skončit
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
nad	nad	k7c7	nad
Julianem	Julian	k1gMnSc7	Julian
Reisterem	Reister	k1gMnSc7	Reister
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představovalo	představovat	k5eAaImAgNnS	představovat
jubilejní	jubilejní	k2eAgNnSc4d1	jubilejní
700	[number]	k4	700
<g/>
.	.	kIx.	.
výhru	výhra	k1gFnSc4	výhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Toura	k1gFnPc2	Toura
a	a	k8xC	a
150	[number]	k4	150
<g/>
.	.	kIx.	.
kariérní	kariérní	k2eAgFnSc4d1	kariérní
výhru	výhra	k1gFnSc4	výhra
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
prohra	prohra	k1gFnSc1	prohra
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	s	k7c7	s
Švédem	Švéd	k1gMnSc7	Švéd
Robinem	Robin	k1gMnSc7	Robin
Söderlingem	Söderling	k1gInSc7	Söderling
<g/>
,	,	kIx,	,
však	však	k9	však
završila	završit	k5eAaPmAgFnS	završit
jeho	jeho	k3xOp3gFnSc4	jeho
rekordní	rekordní	k2eAgFnSc4d1	rekordní
šňůru	šňůra	k1gFnSc4	šňůra
dvacetí	dvacetí	k1gNnSc2	dvacetí
tří	tři	k4xCgNnPc2	tři
semifinále	semifinále	k1gNnPc2	semifinále
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
dosažených	dosažený	k2eAgInPc2d1	dosažený
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
absolutní	absolutní	k2eAgInSc4d1	absolutní
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
týdnů	týden	k1gInPc2	týden
strávených	strávený	k2eAgInPc2d1	strávený
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
mu	on	k3xPp3gMnSc3	on
scházel	scházet	k5eAaImAgInS	scházet
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c7	za
Samprasovými	Samprasová	k1gFnPc7	Samprasová
286	[number]	k4	286
týdny	týden	k1gInPc7	týden
jich	on	k3xPp3gMnPc2	on
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
285	[number]	k4	285
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřsetová	čtyřsetový	k2eAgFnSc1d1	čtyřsetový
porážka	porážka	k1gFnSc1	porážka
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
hráče	hráč	k1gMnSc4	hráč
Tomáše	Tomáš	k1gMnSc4	Tomáš
Berdycha	Berdych	k1gMnSc4	Berdych
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
ukončení	ukončení	k1gNnSc3	ukončení
další	další	k2eAgFnSc2d1	další
rekordní	rekordní	k2eAgFnSc2d1	rekordní
šňůry	šňůra	k1gFnSc2	šňůra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
sedminásobné	sedminásobný	k2eAgFnPc1d1	sedminásobná
účasti	účast	k1gFnPc1	účast
ve	v	k7c6	v
wimbledonském	wimbledonský	k2eAgNnSc6d1	wimbledonské
finále	finále	k1gNnSc6	finále
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
londýnském	londýnský	k2eAgMnSc6d1	londýnský
majoru	major	k1gMnSc6	major
klesl	klesnout	k5eAaPmAgMnS	klesnout
na	na	k7c6	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
-	-	kIx~	-
nejnižší	nízký	k2eAgNnPc4d3	nejnižší
postavení	postavení	k1gNnPc4	postavení
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
patřila	patřit	k5eAaImAgFnS	patřit
stejná	stejný	k2eAgFnSc1d1	stejná
příčka	příčka	k1gFnSc1	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
série	série	k1gFnSc2	série
US	US	kA	US
Open	Open	k1gInSc1	Open
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonech	beton	k1gInPc6	beton
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
Paul	Paula	k1gFnPc2	Paula
Annacone	Annacon	k1gInSc5	Annacon
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
kouč	kouč	k1gMnSc1	kouč
Samprase	Sampras	k1gInSc6	Sampras
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zlepšit	zlepšit	k5eAaPmF	zlepšit
jeho	jeho	k3xOp3gFnSc4	jeho
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
Federer	Federer	k1gMnSc1	Federer
prošel	projít	k5eAaPmAgMnS	projít
do	do	k7c2	do
sedmého	sedmý	k4xOgNnSc2	sedmý
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pětisetové	pětisetový	k2eAgFnSc2d1	pětisetová
bitvy	bitva	k1gFnSc2	bitva
proti	proti	k7c3	proti
srbskému	srbský	k2eAgMnSc3d1	srbský
hráči	hráč	k1gMnSc3	hráč
Novaku	Novak	k1gMnSc3	Novak
Djokovićovi	Djokovića	k1gMnSc3	Djokovića
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
když	když	k8xS	když
promarnil	promarnit	k5eAaPmAgMnS	promarnit
nabídku	nabídka	k1gFnSc4	nabídka
dvou	dva	k4xCgInPc2	dva
mečbolů	mečbol	k1gInPc2	mečbol
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
ukončení	ukončení	k1gNnSc6	ukončení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Masters	Masters	k1gInSc1	Masters
proměnil	proměnit	k5eAaPmAgInS	proměnit
čtyři	čtyři	k4xCgFnPc4	čtyři
finálové	finálový	k2eAgFnPc4d1	finálová
účasti	účast	k1gFnPc4	účast
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
triumf	triumf	k1gInSc4	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c4	na
Cincinnati	Cincinnati	k1gFnSc4	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
utkání	utkání	k1gNnSc6	utkání
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Američana	Američan	k1gMnSc4	Američan
Mardyho	Mardy	k1gMnSc4	Mardy
Fishe	Fish	k1gMnSc4	Fish
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáctým	sedmnáctý	k4xOgInSc7	sedmnáctý
titulem	titul	k1gInSc7	titul
na	na	k7c4	na
Masters	Masters	k1gInSc4	Masters
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
Agassiho	Agassi	k1gMnSc2	Agassi
výkon	výkon	k1gInSc1	výkon
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
63	[number]	k4	63
<g/>
.	.	kIx.	.
trofejí	trofej	k1gFnSc7	trofej
kariéry	kariéra	k1gFnSc2	kariéra
zaostával	zaostávat	k5eAaImAgMnS	zaostávat
jednu	jeden	k4xCgFnSc4	jeden
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
výhru	výhra	k1gFnSc4	výhra
za	za	k7c7	za
Borgem	Borg	k1gMnSc7	Borg
a	a	k8xC	a
Samprasem	Sampras	k1gMnSc7	Sampras
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
Canada	Canada	k1gFnSc1	Canada
Masters	Mastersa	k1gFnPc2	Mastersa
a	a	k8xC	a
Shanghai	Shangha	k1gFnSc2	Shangha
ATP	atp	kA	atp
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
odešel	odejít	k5eAaPmAgMnS	odejít
naopak	naopak	k6eAd1	naopak
ze	z	k7c2	z
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
duelu	duel	k1gInSc2	duel
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ATP	atp	kA	atp
500	[number]	k4	500
Series	Series	k1gInSc1	Series
docílil	docílit	k5eAaPmAgInS	docílit
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
Swiss	Swiss	k1gInSc4	Swiss
Indoors	Indoorsa	k1gFnPc2	Indoorsa
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
popáté	popáté	k4xO	popáté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Španěla	Španěl	k1gMnSc4	Španěl
Rafaela	Rafael	k1gMnSc4	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
klasifikaci	klasifikace	k1gFnSc6	klasifikace
mu	on	k3xPp3gMnSc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dokázal	dokázat	k5eAaPmAgMnS	dokázat
třemi	tři	k4xCgInPc7	tři
tituly	titul	k1gInPc7	titul
z	z	k7c2	z
podzimní	podzimní	k2eAgFnSc2d1	podzimní
části	část	k1gFnSc2	část
sezóny	sezóna	k1gFnSc2	sezóna
předstihnout	předstihnout	k5eAaPmF	předstihnout
Novaka	Novak	k1gMnSc4	Novak
Djokoviće	Djoković	k1gMnSc4	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
překročil	překročit	k5eAaPmAgMnS	překročit
třicátý	třicátý	k4xOgInSc4	třicátý
rok	rok	k1gInSc4	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
žádný	žádný	k1gMnSc1	žádný
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc4	slam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gMnSc1	Open
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
osmifinálovou	osmifinálový	k2eAgFnSc7d1	osmifinálová
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Španělem	Španěl	k1gMnSc7	Španěl
Tommym	Tommym	k1gInSc4	Tommym
Robredem	Robred	k1gMnSc7	Robred
rekord	rekord	k1gInSc4	rekord
Jimmyho	Jimmy	k1gMnSc4	Jimmy
Connorse	Connorse	k1gFnSc2	Connorse
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
27	[number]	k4	27
čtvrtfinálových	čtvrtfinálový	k2eAgFnPc2d1	čtvrtfinálová
účastí	účast	k1gFnPc2	účast
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
zastavil	zastavit	k5eAaPmAgMnS	zastavit
Srb	Srb	k1gMnSc1	Srb
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
výsledku	výsledek	k1gInSc2	výsledek
roku	rok	k1gInSc2	rok
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
French	French	k1gInSc1	French
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
fázi	fáze	k1gFnSc6	fáze
ukončil	ukončit	k5eAaPmAgInS	ukončit
43	[number]	k4	43
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
šňůru	šňůra	k1gFnSc4	šňůra
Djokoviće	Djoković	k1gInSc2	Djoković
bez	bez	k7c2	bez
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Rafaelu	Rafaela	k1gFnSc4	Rafaela
Nadalovi	Nadal	k1gMnSc3	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
wimbledonském	wimbledonský	k2eAgInSc6d1	wimbledonský
pažitu	pažit	k1gInSc6	pažit
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nejhorší	zlý	k2eAgInSc1d3	Nejhorší
výsledek	výsledek	k1gInSc1	výsledek
roku	rok	k1gInSc2	rok
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turnajů	turnaj	k1gInPc2	turnaj
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
čtyřky	čtyřka	k1gFnPc4	čtyřka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
devatenáctým	devatenáctý	k4xOgMnSc7	devatenáctý
hráčem	hráč	k1gMnSc7	hráč
světa	svět	k1gInSc2	svět
Jo-Wilfriedem	Jo-Wilfried	k1gInSc7	Jo-Wilfried
Tsongou	Tsonga	k1gFnSc7	Tsonga
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
179	[number]	k4	179
odehraných	odehraný	k2eAgInPc2d1	odehraný
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
duelů	duel	k1gInPc2	duel
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
ztratil	ztratit	k5eAaPmAgMnS	ztratit
zápas	zápas	k1gInSc4	zápas
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
na	na	k7c4	na
sety	set	k1gInPc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Pocitu	pocit	k1gInSc2	pocit
"	"	kIx"	"
<g/>
déjà	déjà	k?	déjà
vu	vu	k?	vu
<g/>
"	"	kIx"	"
mohl	moct	k5eAaImAgInS	moct
nabýt	nabýt	k5eAaPmF	nabýt
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
rekordního	rekordní	k2eAgInSc2d1	rekordní
třicátého	třicátý	k4xOgInSc2	třicátý
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
majoru	major	k1gMnSc3	major
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
,	,	kIx,	,
když	když	k8xS	když
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
dějství	dějství	k1gNnSc1	dějství
vyznělo	vyznět	k5eAaImAgNnS	vyznět
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Srba	Srba	k1gMnSc1	Srba
poměrem	poměr	k1gInSc7	poměr
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
basilejský	basilejský	k2eAgMnSc1d1	basilejský
rodák	rodák	k1gMnSc1	rodák
opět	opět	k6eAd1	opět
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
proměnit	proměnit	k5eAaPmF	proměnit
dva	dva	k4xCgInPc4	dva
mečboly	mečbol	k1gInPc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
atributy	atribut	k1gInPc4	atribut
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
semifinále	semifinále	k1gNnSc1	semifinále
na	na	k7c6	na
předchozím	předchozí	k2eAgInSc6d1	předchozí
ročníku	ročník	k1gInSc6	ročník
Flushing	Flushing	k1gInSc1	Flushing
Meadows	Meadows	k1gInSc1	Meadows
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
navíc	navíc	k6eAd1	navíc
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vedení	vedení	k1gNnSc3	vedení
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
na	na	k7c4	na
sety	set	k1gInPc4	set
v	v	k7c6	v
grandslamovém	grandslamový	k2eAgInSc6d1	grandslamový
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
září	září	k1gNnSc2	září
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
globální	globální	k2eAgFnSc1d1	globální
studie	studie	k1gFnSc1	studie
agentury	agentura	k1gFnSc2	agentura
Reputation	Reputation	k1gInSc1	Reputation
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
konaná	konaný	k2eAgFnSc1d1	konaná
ve	v	k7c6	v
25	[number]	k4	25
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
Federer	Federer	k1gInSc1	Federer
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
nejdůvěryhodnější	důvěryhodný	k2eAgInSc1d3	nejdůvěryhodnější
a	a	k8xC	a
nejuznávanější	uznávaný	k2eAgMnSc1d3	nejuznávanější
člověk	člověk	k1gMnSc1	člověk
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
za	za	k7c7	za
prvním	první	k4xOgMnSc7	první
Nelsonem	Nelson	k1gMnSc7	Nelson
Mandelou	Mandela	k1gFnSc7	Mandela
a	a	k8xC	a
před	před	k7c7	před
třetím	třetí	k4xOgMnSc7	třetí
Billem	Bill	k1gMnSc7	Bill
Gatesem	Gates	k1gInSc7	Gates
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Masters	Mastersa	k1gFnPc2	Mastersa
získal	získat	k5eAaPmAgMnS	získat
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
hráč	hráč	k1gMnSc1	hráč
jediný	jediný	k2eAgInSc4d1	jediný
titul	titul	k1gInSc4	titul
z	z	k7c2	z
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
haly	hala	k1gFnSc2	hala
Bercy	Berca	k1gFnSc2	Berca
na	na	k7c6	na
Paris	Paris	k1gMnSc1	Paris
Masters	Masters	k1gInSc1	Masters
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
raketě	raketa	k1gFnSc6	raketa
Francouz	Francouz	k1gMnSc1	Francouz
Tsonga	Tsonga	k1gFnSc1	Tsonga
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
utkání	utkání	k1gNnPc2	utkání
o	o	k7c4	o
titul	titul	k1gInSc4	titul
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ATP	atp	kA	atp
500	[number]	k4	500
Series	Seriesa	k1gFnPc2	Seriesa
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
rekordní	rekordní	k2eAgNnSc4d1	rekordní
páté	pátý	k4xOgNnSc4	pátý
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
basilejském	basilejský	k2eAgInSc6d1	basilejský
Swiss	Swiss	k1gInSc1	Swiss
Indoors	Indoors	k1gInSc1	Indoors
a	a	k8xC	a
únorový	únorový	k2eAgInSc1d1	únorový
Dubai	Dubae	k1gFnSc4	Dubae
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championships	k1gInSc1	Championships
opustil	opustit	k5eAaPmAgMnS	opustit
jako	jako	k9	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Triumfem	triumf	k1gInSc7	triumf
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
skončila	skončit	k5eAaPmAgFnS	skončit
úvodní	úvodní	k2eAgFnSc1d1	úvodní
událost	událost	k1gFnSc1	událost
sezóny	sezóna	k1gFnSc2	sezóna
Qatar	Qatar	k1gMnSc1	Qatar
ExxonMobil	ExxonMobil	k1gMnSc1	ExxonMobil
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
katarském	katarský	k2eAgInSc6d1	katarský
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
kategorie	kategorie	k1gFnSc2	kategorie
ATP	atp	kA	atp
250	[number]	k4	250
Series	Seriesa	k1gFnPc2	Seriesa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
halového	halový	k2eAgInSc2d1	halový
turnaje	turnaj	k1gInSc2	turnaj
Shanghai	Shangha	k1gFnSc2	Shangha
ATP	atp	kA	atp
Masters	Masters	k1gInSc1	Masters
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
neobhájil	obhájit	k5eNaPmAgMnS	obhájit
600	[number]	k4	600
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
minulého	minulý	k2eAgInSc2d1	minulý
roku	rok	k1gInSc2	rok
a	a	k8xC	a
na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
ATP	atp	kA	atp
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
osmi	osm	k4xCc6	osm
a	a	k8xC	a
půl	půl	k1xP	půl
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
červnového	červnový	k2eAgInSc2d1	červnový
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
mimo	mimo	k7c4	mimo
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
trojici	trojice	k1gFnSc4	trojice
světových	světový	k2eAgMnPc2d1	světový
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
událostí	událost	k1gFnSc7	událost
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
již	již	k9	již
tradičně	tradičně	k6eAd1	tradičně
stal	stát	k5eAaPmAgInS	stát
Turnaj	turnaj	k1gInSc1	turnaj
mistrů	mistr	k1gMnPc2	mistr
konaný	konaný	k2eAgMnSc1d1	konaný
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
B	B	kA	B
porazil	porazit	k5eAaPmAgMnS	porazit
všechny	všechen	k3xTgMnPc4	všechen
tři	tři	k4xCgMnPc4	tři
soupeře	soupeř	k1gMnPc4	soupeř
Jo-Wilfrieda	Jo-Wilfried	k1gMnSc2	Jo-Wilfried
Tsongu	Tsong	k1gInSc2	Tsong
<g/>
,	,	kIx,	,
debutanta	debutant	k1gMnSc2	debutant
Mardyho	Mardy	k1gMnSc2	Mardy
Fishe	Fish	k1gMnSc2	Fish
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
velkého	velký	k2eAgMnSc2d1	velký
rivala	rival	k1gMnSc2	rival
Rafaela	Rafael	k1gMnSc2	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešel	přejít	k5eAaPmAgInS	přejít
přes	přes	k7c4	přes
druhého	druhý	k4xOgMnSc4	druhý
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
Davida	David	k1gMnSc2	David
Ferrera	Ferrer	k1gMnSc2	Ferrer
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
100	[number]	k4	100
<g/>
.	.	kIx.	.
finále	finále	k1gNnSc6	finále
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
zdolal	zdolat	k5eAaPmAgMnS	zdolat
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Tsongu	Tsong	k1gInSc2	Tsong
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
připsal	připsat	k5eAaPmAgInS	připsat
si	se	k3xPyFc3	se
celkově	celkově	k6eAd1	celkově
70	[number]	k4	70
<g/>
.	.	kIx.	.
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Šestým	šestý	k4xOgInSc7	šestý
triumfem	triumf	k1gInSc7	triumf
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
události	událost	k1gFnSc6	událost
sezóny	sezóna	k1gFnSc2	sezóna
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
od	od	k7c2	od
pěti	pět	k4xCc2	pět
titulů	titul	k1gInPc2	titul
Lendla	Lendla	k1gFnSc2	Lendla
a	a	k8xC	a
Samprase	Samprasa	k1gFnSc6	Samprasa
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
také	také	k9	také
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
na	na	k7c4	na
Lendlův	Lendlův	k2eAgInSc4d1	Lendlův
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
absolutním	absolutní	k2eAgInSc6d1	absolutní
počtu	počet	k1gInSc6	počet
39	[number]	k4	39
vítězných	vítězný	k2eAgNnPc6d1	vítězné
utkáních	utkání	k1gNnPc6	utkání
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Výhrou	výhra	k1gFnSc7	výhra
ve	v	k7c6	v
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejstarším	starý	k2eAgMnSc7d3	nejstarší
šampiónem	šampión	k1gMnSc7	šampión
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnSc4d1	aktuální
sérii	série	k1gFnSc4	série
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
na	na	k7c4	na
17	[number]	k4	17
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
získal	získat	k5eAaPmAgMnS	získat
dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgInSc4d1	poslední
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
mužský	mužský	k2eAgMnSc1d1	mužský
tenista	tenista	k1gMnSc1	tenista
historie	historie	k1gFnSc2	historie
po	po	k7c6	po
Williamu	William	k1gInSc6	William
Renshawovi	Renshawa	k1gMnSc3	Renshawa
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
osmého	osmý	k4xOgNnSc2	osmý
finále	finále	k1gNnSc2	finále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
porazil	porazit	k5eAaPmAgMnS	porazit
skotského	skotský	k1gInSc2	skotský
hráče	hráč	k1gMnSc2	hráč
Andyho	Andy	k1gMnSc2	Andy
Murrayho	Murray	k1gMnSc2	Murray
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sedmou	sedmý	k4xOgFnSc7	sedmý
wimbledonskou	wimbledonský	k2eAgFnSc7d1	wimbledonská
trofejí	trofej	k1gFnSc7	trofej
se	se	k3xPyFc4	se
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
na	na	k7c4	na
Peta	Petus	k1gMnSc4	Petus
Samprase	Samprasa	k1gFnSc6	Samprasa
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
17	[number]	k4	17
<g/>
.	.	kIx.	.
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc3	slam
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
nový	nový	k2eAgInSc1d1	nový
historický	historický	k2eAgInSc1d1	historický
rekord	rekord	k1gInSc1	rekord
mužského	mužský	k2eAgInSc2d1	mužský
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
nejstarším	starý	k2eAgMnSc7d3	nejstarší
finalistou	finalista	k1gMnSc7	finalista
londýnského	londýnský	k2eAgInSc2d1	londýnský
turnaje	turnaj	k1gInSc2	turnaj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
závěrečného	závěrečný	k2eAgNnSc2d1	závěrečné
utkání	utkání	k1gNnSc2	utkání
postoupil	postoupit	k5eAaPmAgInS	postoupit
Jimmy	Jimm	k1gMnPc4	Jimm
Connors	Connors	k1gInSc4	Connors
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejstarším	starý	k2eAgMnSc7d3	nejstarší
vítězem	vítěz	k1gMnSc7	vítěz
od	od	k7c2	od
titulu	titul	k1gInSc2	titul
Arthura	Arthur	k1gMnSc2	Arthur
Ashe	Ash	k1gMnSc2	Ash
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgInPc1d1	získaný
body	bod	k1gInPc1	bod
jej	on	k3xPp3gNnSc4	on
katapultovaly	katapultovat	k5eAaBmAgInP	katapultovat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
červencovém	červencový	k2eAgInSc6d1	červencový
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
"	"	kIx"	"
<g/>
tenisový	tenisový	k2eAgInSc4d1	tenisový
trůn	trůn	k1gInSc4	trůn
<g/>
"	"	kIx"	"
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
jediný	jediný	k2eAgInSc4d1	jediný
týden	týden	k1gInSc4	týden
na	na	k7c4	na
Samprasův	Samprasův	k2eAgInSc4d1	Samprasův
rekord	rekord	k1gInSc4	rekord
286	[number]	k4	286
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgFnSc3d1	světová
klasifikaci	klasifikace	k1gFnSc3	klasifikace
vévodil	vévodit	k5eAaImAgInS	vévodit
nepřetržitě	přetržitě	k6eNd1	přetržitě
dalších	další	k2eAgInPc2d1	další
16	[number]	k4	16
týdnů	týden	k1gInPc2	týden
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
nový	nový	k2eAgInSc4d1	nový
absolutní	absolutní	k2eAgInSc4d1	absolutní
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
302	[number]	k4	302
týdnů	týden	k1gInPc2	týden
strávených	strávený	k2eAgInPc2d1	strávený
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
prvního	první	k4xOgMnSc2	první
hráče	hráč	k1gMnSc2	hráč
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gMnSc1	Open
postoupil	postoupit	k5eAaPmAgMnS	postoupit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
třicátník	třicátník	k1gMnSc1	třicátník
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Agassiho	Agassi	k1gMnSc2	Agassi
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gNnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
světová	světový	k2eAgFnSc1d1	světová
dvojka	dvojka	k1gFnSc1	dvojka
Rafael	Rafaela	k1gFnPc2	Rafaela
Nadal	nadat	k5eAaPmAgInS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Španěl	Španěl	k1gMnSc1	Španěl
tak	tak	k6eAd1	tak
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jeho	jeho	k3xOp3gNnSc4	jeho
24	[number]	k4	24
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
šňůru	šňůra	k1gFnSc4	šňůra
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zahájil	zahájit	k5eAaPmAgInS	zahájit
v	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Antukové	antukový	k2eAgFnPc1d1	antuková
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
představovalo	představovat	k5eAaImAgNnS	představovat
již	již	k6eAd1	již
32	[number]	k4	32
<g/>
.	.	kIx.	.
čtvrtfinálovou	čtvrtfinálový	k2eAgFnSc4d1	čtvrtfinálová
Švýcarovu	Švýcarův	k2eAgFnSc4d1	Švýcarova
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
antuce	antuka	k1gFnSc6	antuka
však	však	k9	však
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgMnSc6d1	poslední
majoru	major	k1gMnSc6	major
sezóny	sezóna	k1gFnSc2	sezóna
US	US	kA	US
Open	Open	k1gInSc1	Open
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
stopku	stopka	k1gFnSc4	stopka
vystavil	vystavit	k5eAaPmAgMnS	vystavit
sedmý	sedmý	k4xOgMnSc1	sedmý
tenista	tenista	k1gMnSc1	tenista
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
ve	v	k7c6	v
čtvrtfinálové	čtvrtfinálový	k2eAgFnSc6d1	čtvrtfinálová
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
na	na	k7c6	na
travnatých	travnatý	k2eAgInPc6d1	travnatý
dvorcích	dvorec	k1gInPc6	dvorec
All	All	k1gFnSc1	All
England	England	k1gInSc1	England
Clubu	club	k1gInSc2	club
tenisový	tenisový	k2eAgInSc1d1	tenisový
turnaj	turnaj	k1gInSc1	turnaj
Her	hra	k1gFnPc2	hra
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Singlovou	singlový	k2eAgFnSc7d1	singlová
soutěží	soutěž	k1gFnSc7	soutěž
prošel	projít	k5eAaPmAgInS	projít
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Pošesté	pošesté	k4xO	pošesté
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
Argentincem	Argentinec	k1gMnSc7	Argentinec
Juanem	Juan	k1gMnSc7	Juan
Martínem	Martín	k1gMnSc7	Martín
del	del	k?	del
Potrem	Potr	k1gMnSc7	Potr
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
semifnále	semifnála	k1gFnSc6	semifnála
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
utkání	utkání	k1gNnSc2	utkání
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
až	až	k9	až
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
sadě	sada	k1gFnSc6	sada
poměrem	poměr	k1gInSc7	poměr
gamů	game	k1gInPc2	game
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Rekordně	rekordně	k6eAd1	rekordně
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
utkání	utkání	k1gNnSc1	utkání
se	se	k3xPyFc4	se
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
do	do	k7c2	do
historických	historický	k2eAgFnPc2d1	historická
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
zápasem	zápas	k1gInSc7	zápas
hraným	hraný	k2eAgInSc7d1	hraný
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
vítězné	vítězný	k2eAgFnPc4d1	vítězná
sady	sada	k1gFnPc4	sada
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
činila	činit	k5eAaImAgFnS	činit
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
26	[number]	k4	26
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
zlatý	zlatý	k2eAgInSc4d1	zlatý
olympijský	olympijský	k2eAgInSc4d1	olympijský
kov	kov	k1gInSc4	kov
však	však	k9	však
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
britskému	britský	k2eAgMnSc3d1	britský
reprezentantu	reprezentant	k1gMnSc3	reprezentant
Andymu	Andym	k1gInSc2	Andym
Murraymu	Murraym	k1gInSc2	Murraym
hladce	hladko	k6eAd1	hladko
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zisk	zisk	k1gInSc4	zisk
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pekingském	pekingský	k2eAgNnSc6d1	pekingské
zlatu	zlato	k1gNnSc6	zlato
vstupoval	vstupovat	k5eAaImAgInS	vstupovat
se	s	k7c7	s
Stanislasem	Stanislas	k1gInSc7	Stanislas
Wawrinkou	Wawrinka	k1gFnSc7	Wawrinka
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
mužské	mužský	k2eAgFnSc2d1	mužská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
v	v	k7c6	v
roli	role	k1gFnSc6	role
obhájce	obhájce	k1gMnSc2	obhájce
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
pár	pár	k1gInSc1	pár
skončil	skončit	k5eAaPmAgInS	skončit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
dvojice	dvojice	k1gFnSc1	dvojice
izraelských	izraelský	k2eAgMnPc2d1	izraelský
deblových	deblový	k2eAgMnPc2d1	deblový
specialistů	specialista	k1gMnPc2	specialista
Jonatan	Jonatan	k1gMnSc1	Jonatan
Erlich	Erlich	k1gMnSc1	Erlich
a	a	k8xC	a
Andy	Anda	k1gFnPc1	Anda
Ram	Ram	k1gFnSc2	Ram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sérii	série	k1gFnSc4	série
Masters	Mastersa	k1gFnPc2	Mastersa
basilejský	basilejský	k2eAgMnSc1d1	basilejský
rodák	rodák	k1gMnSc1	rodák
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
tři	tři	k4xCgNnPc4	tři
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
tenista	tenista	k1gMnSc1	tenista
historie	historie	k1gFnSc2	historie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
turnaj	turnaj	k1gInSc4	turnaj
hraný	hraný	k2eAgInSc4d1	hraný
na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
antuce	antuka	k1gFnSc6	antuka
-	-	kIx~	-
španělský	španělský	k2eAgMnSc1d1	španělský
Mutua	Mutua	k1gMnSc1	Mutua
Madrileñ	Madrileñ	k1gFnSc2	Madrileñ
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
účastí	účast	k1gFnSc7	účast
ve	v	k7c6	v
32	[number]	k4	32
<g/>
.	.	kIx.	.
finále	finále	k1gNnSc3	finále
této	tento	k3xDgFnSc2	tento
série	série	k1gFnSc2	série
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
od	od	k7c2	od
Ivana	Ivan	k1gMnSc4	Ivan
Lendla	Lendla	k1gMnSc2	Lendla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
madridském	madridský	k2eAgNnSc6d1	madridské
finále	finále	k1gNnSc6	finále
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Berdychem	Berdych	k1gMnSc7	Berdych
a	a	k8xC	a
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
rekordních	rekordní	k2eAgInPc2d1	rekordní
dvacet	dvacet	k4xCc4	dvacet
Nadalových	Nadalův	k2eAgFnPc2d1	Nadalova
trofejí	trofej	k1gFnPc2	trofej
v	v	k7c4	v
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
počítaných	počítaný	k2eAgFnPc2d1	počítaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
trofeje	trofej	k1gFnPc4	trofej
si	se	k3xPyFc3	se
odvezl	odvézt	k5eAaPmAgInS	odvézt
z	z	k7c2	z
amerických	americký	k2eAgInPc2d1	americký
betonů	beton	k1gInPc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šestileté	šestiletý	k2eAgFnSc6d1	šestiletá
pauze	pauza	k1gFnSc6	pauza
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
turnajový	turnajový	k2eAgInSc4d1	turnajový
triumf	triumf	k1gInSc4	triumf
a	a	k8xC	a
poté	poté	k6eAd1	poté
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
srpnovém	srpnový	k2eAgMnSc6d1	srpnový
Cincinnati	Cincinnati	k1gMnSc6	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
získal	získat	k5eAaPmAgMnS	získat
rekordní	rekordní	k2eAgInSc4d1	rekordní
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
neztratil	ztratit	k5eNaPmAgMnS	ztratit
žádnou	žádný	k3yNgFnSc4	žádný
sadu	sada	k1gFnSc4	sada
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
neprohrál	prohrát	k5eNaPmAgMnS	prohrát
své	svůj	k3xOyFgNnSc4	svůj
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
kategorii	kategorie	k1gFnSc6	kategorie
ATP	atp	kA	atp
500	[number]	k4	500
Series	Series	k1gInSc1	Series
docílil	docílit	k5eAaPmAgInS	docílit
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
fázi	fáze	k1gFnSc6	fáze
sezóny	sezóna	k1gFnSc2	sezóna
dvou	dva	k4xCgFnPc2	dva
výher	výhra	k1gFnPc2	výhra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c6	na
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
Open	Openo	k1gNnPc2	Openo
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
jeho	jeho	k3xOp3gNnSc1	jeho
páté	pátý	k4xOgNnSc1	pátý
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
arabském	arabský	k2eAgInSc6d1	arabský
turnaji	turnaj	k1gInSc6	turnaj
Dubai	Duba	k1gFnSc2	Duba
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
šestou	šestý	k4xOgFnSc4	šestý
trofej	trofej	k1gFnSc4	trofej
ze	z	k7c2	z
Swiss	Swissa	k1gFnPc2	Swissa
Indoors	Indoorsa	k1gFnPc2	Indoorsa
nezískal	získat	k5eNaPmAgInS	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Juan	Juan	k1gMnSc1	Juan
Martín	Martín	k1gMnSc1	Martín
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
až	až	k6eAd1	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
odjížděl	odjíždět	k5eAaImAgMnS	odjíždět
jako	jako	k9	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
ze	z	k7c2	z
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
nezvládnutých	zvládnutý	k2eNgFnPc6d1	nezvládnutá
koncovkách	koncovka	k1gFnPc6	koncovka
obou	dva	k4xCgInPc2	dva
setů	set	k1gInPc2	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
nestačil	stačit	k5eNaBmAgInS	stačit
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
duelu	duel	k1gInSc6	duel
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Srbský	srbský	k2eAgMnSc1d1	srbský
hráč	hráč	k1gMnSc1	hráč
tak	tak	k6eAd1	tak
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jeho	jeho	k3xOp3gNnSc4	jeho
12	[number]	k4	12
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
na	na	k7c6	na
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
události	událost	k1gFnSc6	událost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
završil	završit	k5eAaPmAgInS	završit
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc3	příčka
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
právě	právě	k9	právě
za	za	k7c4	za
Djokovićem	Djokovićem	k1gInSc4	Djokovićem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
znamenala	znamenat	k5eAaImAgFnS	znamenat
výsledkově	výsledkově	k6eAd1	výsledkově
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
bilanci	bilance	k1gFnSc4	bilance
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
titul	titul	k1gInSc4	titul
z	z	k7c2	z
halleského	halleský	k2eAgMnSc2d1	halleský
Gerry	Gerra	k1gMnSc2	Gerra
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gMnSc1	Open
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Michailem	Michail	k1gMnSc7	Michail
Južným	Južný	k1gMnSc7	Južný
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
připsal	připsat	k5eAaPmAgInS	připsat
77	[number]	k4	77
<g/>
.	.	kIx.	.
kariérní	kariérní	k2eAgFnSc4d1	kariérní
trofej	trofej	k1gFnSc4	trofej
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
statistik	statistika	k1gFnPc2	statistika
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Američana	Američan	k1gMnSc2	Američan
Johna	John	k1gMnSc2	John
McEnroea	McEnroeus	k1gMnSc2	McEnroeus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc3	slam
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
nejdále	daleko	k6eAd3	daleko
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
nestačil	stačit	k5eNaBmAgMnS	stačit
na	na	k7c4	na
Jo-Wilfrieda	Jo-Wilfried	k1gMnSc4	Jo-Wilfried
Tsongu	Tsong	k1gInSc2	Tsong
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejkratší	krátký	k2eAgMnPc1d3	nejkratší
účinkovaní	účinkovaný	k2eAgMnPc1d1	účinkovaný
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnSc2d1	černá
středy	středa	k1gFnSc2	středa
<g/>
"	"	kIx"	"
skončil	skončit	k5eAaPmAgMnS	skončit
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Ukrajince	Ukrajinec	k1gMnSc2	Ukrajinec
Sergije	Sergije	k1gMnSc2	Sergije
Stachovského	Stachovský	k2eAgMnSc2d1	Stachovský
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
stovky	stovka	k1gFnSc2	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
nezahrál	zahrát	k5eNaPmAgMnS	zahrát
žádné	žádný	k3yNgNnSc4	žádný
finále	finále	k1gNnSc4	finále
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
.	.	kIx.	.
</s>
<s>
Bodová	bodový	k2eAgFnSc1d1	bodová
ztráta	ztráta	k1gFnSc1	ztráta
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
srpnovém	srpnový	k2eAgInSc6d1	srpnový
Cincinnati	Cincinnati	k1gMnSc4	Cincinnati
Masters	Masters	k1gInSc1	Masters
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
postavení	postavení	k1gNnSc1	postavení
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
římského	římský	k2eAgMnSc2d1	římský
Internazionali	Internazionali	k1gMnSc2	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
s	s	k7c7	s
Nadalem	Nadal	k1gInSc7	Nadal
a	a	k8xC	a
z	z	k7c2	z
halového	halový	k2eAgMnSc2d1	halový
Swiss	Swiss	k1gInSc1	Swiss
Indoors	Indoors	k1gInSc1	Indoors
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c6	na
Argentince	Argentinka	k1gFnSc6	Argentinka
Juana	Juan	k1gMnSc2	Juan
Martína	Martín	k1gMnSc2	Martín
del	del	k?	del
Potra	Potr	k1gMnSc2	Potr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
na	na	k7c6	na
basilejské	basilejský	k2eAgFnSc6d1	Basilejská
události	událost	k1gFnSc6	událost
znamenala	znamenat	k5eAaImAgFnS	znamenat
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
Villasova	Villasův	k2eAgInSc2d1	Villasův
rekordu	rekord	k1gInSc2	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Federer	Federer	k1gMnSc1	Federer
probojoval	probojovat	k5eAaPmAgMnS	probojovat
desetkrát	desetkrát	k6eAd1	desetkrát
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
jediného	jediný	k2eAgInSc2d1	jediný
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Pojedenácté	pojedenácté	k4xO	pojedenácté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
semifinále	semifinále	k1gNnSc4	semifinále
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gNnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
a	a	k8xC	a
velký	velký	k2eAgMnSc1d1	velký
soupeř	soupeř	k1gMnSc1	soupeř
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Rafael	Rafaela	k1gFnPc2	Rafaela
Nadal	nadat	k5eAaPmAgInS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Zisk	zisk	k1gInSc1	zisk
400	[number]	k4	400
bodů	bod	k1gInPc2	bod
znamenal	znamenat	k5eAaImAgInS	znamenat
konečnou	konečná	k1gFnSc4	konečná
6	[number]	k4	6
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
klasifikaci	klasifikace	k1gFnSc6	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
účastí	účastí	k1gNnSc2	účastí
na	na	k7c4	na
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc5	International
<g/>
,	,	kIx,	,
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
úmysl	úmysl	k1gInSc1	úmysl
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
jednička	jednička	k1gFnSc1	jednička
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
dvouhry	dvouhra	k1gFnSc2	dvouhra
postoupil	postoupit	k5eAaPmAgMnS	postoupit
přes	přes	k7c4	přes
Francouze	Francouz	k1gMnSc4	Francouz
Jérémy	Jérém	k1gInPc7	Jérém
Chardyho	Chardy	k1gMnSc2	Chardy
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
bývalému	bývalý	k2eAgMnSc3d1	bývalý
prvnímu	první	k4xOgNnSc3	první
hráči	hráč	k1gMnPc7	hráč
žebříčku	žebříček	k1gInSc2	žebříček
Lleytonu	Lleyton	k1gInSc2	Lleyton
Hewittovi	Hewitt	k1gMnSc3	Hewitt
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nicolasem	Nicolas	k1gMnSc7	Nicolas
Mahutem	Mahut	k1gMnSc7	Mahut
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
také	také	k9	také
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
došli	dojít	k5eAaPmAgMnP	dojít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gNnSc4	Open
vstupoval	vstupovat	k5eAaImAgInS	vstupovat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
šestého	šestý	k4xOgMnSc2	šestý
nasazeného	nasazený	k2eAgMnSc2d1	nasazený
<g/>
.	.	kIx.	.
</s>
<s>
Padesátou	padesátý	k4xOgFnSc7	padesátý
sedmou	sedma	k1gFnSc7	sedma
grandslamovou	grandslamový	k2eAgFnSc7d1	grandslamová
účastí	účast	k1gFnSc7	účast
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
tenisový	tenisový	k2eAgInSc4d1	tenisový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtvrtfinálové	čtvrtfinálový	k2eAgFnSc6d1	čtvrtfinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
světovou	světový	k2eAgFnSc7d1	světová
čtyřkou	čtyřka	k1gFnSc7	čtyřka
Andym	Andymum	k1gNnPc2	Andymum
Murraym	Murraym	k1gInSc1	Murraym
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
prvního	první	k4xOgMnSc2	první
nasazeného	nasazený	k2eAgMnSc2d1	nasazený
Rafaela	Rafael	k1gMnSc2	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
získaného	získaný	k2eAgInSc2d1	získaný
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
odehrál	odehrát	k5eAaPmAgInS	odehrát
Dubai	Dubai	k1gNnSc4	Dubai
Duty	Duty	k?	Duty
Free	Fre	k1gInSc2	Fre
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
velkých	velký	k2eAgMnPc2d1	velký
soupeřů	soupeř	k1gMnPc2	soupeř
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
poměr	poměr	k1gInSc4	poměr
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
na	na	k7c4	na
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgMnS	poradit
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Berdychem	Berdych	k1gMnSc7	Berdych
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
a	a	k8xC	a
odvezl	odvézt	k5eAaPmAgInS	odvézt
si	se	k3xPyFc3	se
celkově	celkově	k6eAd1	celkově
šestou	šestý	k4xOgFnSc4	šestý
dubajskou	dubajský	k2eAgFnSc4d1	dubajská
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
78	[number]	k4	78
<g/>
.	.	kIx.	.
titul	titul	k1gInSc4	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
od	od	k7c2	od
Johna	John	k1gMnSc2	John
McEnroea	McEnroeus	k1gMnSc2	McEnroeus
<g/>
.	.	kIx.	.
</s>
<s>
Vyhraným	vyhraný	k2eAgInSc7d1	vyhraný
turnajem	turnaj	k1gInSc7	turnaj
ve	v	k7c6	v
čtrnácté	čtrnáctý	k4xOgFnSc6	čtrnáctý
sezóně	sezóna	k1gFnSc6	sezóna
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
také	také	k9	také
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
Ivana	Ivan	k1gMnSc2	Ivan
Lendla	Lendla	k1gMnSc2	Lendla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
Masters	Masters	k1gInSc1	Masters
prohrál	prohrát	k5eAaPmAgInS	prohrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	s	k7c7	s
Srbem	Srb	k1gMnSc7	Srb
Novakem	Novak	k1gMnSc7	Novak
Djokovićem	Djoković	k1gMnSc7	Djoković
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
až	až	k9	až
tiebreak	tiebreak	k1gInSc1	tiebreak
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
finále	finále	k1gNnSc1	finále
na	na	k7c4	na
Monte-Carlo	Monte-Carlo	k1gNnSc4	Monte-Carlo
Masters	Mastersa	k1gFnPc2	Mastersa
neproměnil	proměnit	k5eNaPmAgInS	proměnit
v	v	k7c4	v
titul	titul	k1gInSc4	titul
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
porazil	porazit	k5eAaPmAgMnS	porazit
krajan	krajan	k1gMnSc1	krajan
Stan	stan	k1gInSc4	stan
Wawrinka	Wawrinka	k1gFnSc1	Wawrinka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
silný	silný	k2eAgInSc4d1	silný
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
tým	tým	k1gInSc4	tým
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
a	a	k8xC	a
po	po	k7c6	po
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c7	nad
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Kazachstánem	Kazachstán	k1gInSc7	Kazachstán
<g/>
,	,	kIx,	,
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
zářijového	zářijový	k2eAgNnSc2d1	zářijové
semifinále	semifinále	k1gNnSc2	semifinále
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
své	svůj	k3xOyFgNnSc4	svůj
kariérní	kariérní	k2eAgNnSc4d1	kariérní
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
French	French	k1gInSc1	French
Open	Open	k1gNnSc1	Open
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nejhorší	zlý	k2eAgInSc1d3	Nejhorší
výsledek	výsledek	k1gInSc1	výsledek
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
s	s	k7c7	s
Lotyšem	Lotyš	k1gMnSc7	Lotyš
Ernestsem	Ernests	k1gMnSc7	Ernests
Gulbisem	Gulbis	k1gInSc7	Gulbis
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Gerry	Gerr	k1gInPc4	Gerr
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gMnSc1	Open
se	se	k3xPyFc4	se
podeváté	podeváté	k4xO	podeváté
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
navýšil	navýšit	k5eAaPmAgInS	navýšit
rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
halleských	halleský	k2eAgInPc2d1	halleský
titulů	titul	k1gInPc2	titul
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
průběhu	průběh	k1gInSc6	průběh
proti	proti	k7c3	proti
69	[number]	k4	69
<g/>
.	.	kIx.	.
kolumbijskému	kolumbijský	k2eAgInSc3d1	kolumbijský
hráči	hráč	k1gMnPc7	hráč
žebříčku	žebříček	k1gInSc2	žebříček
Alejandru	Alejandr	k1gInSc2	Alejandr
Fallovi	Fallův	k2eAgMnPc1d1	Fallův
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
tiebreaky	tiebreak	k1gInPc4	tiebreak
obou	dva	k4xCgInPc2	dva
setů	set	k1gInPc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
pronikl	proniknout	k5eAaPmAgInS	proniknout
také	také	k9	také
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
po	po	k7c6	po
boku	bok	k1gInSc6	bok
krajana	krajan	k1gMnSc2	krajan
Chiudinelliho	Chiudinelli	k1gMnSc2	Chiudinelli
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
však	však	k8xC	však
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
německo-rakouským	německoakouský	k2eAgMnPc3d1	německo-rakouský
deblovým	deblový	k2eAgMnPc3d1	deblový
specialistům	specialista	k1gMnPc3	specialista
Begemannovi	Begemann	k1gMnSc6	Begemann
s	s	k7c7	s
Knowlem	Knowl	k1gInSc7	Knowl
<g/>
.	.	kIx.	.
</s>
<s>
Deváté	devátý	k4xOgNnSc4	devátý
wimbledonské	wimbledonský	k2eAgNnSc4d1	wimbledonské
finále	finále	k1gNnSc4	finále
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
proti	proti	k7c3	proti
nejvýše	vysoce	k6eAd3	vysoce
nasazené	nasazený	k2eAgFnSc3d1	nasazená
světové	světový	k2eAgFnSc3d1	světová
dvojce	dvojka	k1gFnSc3	dvojka
Novaku	Novak	k1gInSc2	Novak
Djokovićovi	Djokovića	k1gMnSc3	Djokovića
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
proměnil	proměnit	k5eAaPmAgMnS	proměnit
třetí	třetí	k4xOgInSc4	třetí
mečbol	mečbol	k1gInSc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tak	tak	k8xS	tak
Švýcar	Švýcar	k1gMnSc1	Švýcar
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Popáté	popáté	k4xO	popáté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
přímý	přímý	k2eAgInSc4d1	přímý
boj	boj	k1gInSc4	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
na	na	k7c6	na
letním	letní	k2eAgNnSc6d1	letní
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Francouz	Francouz	k1gMnSc1	Francouz
Jo-Wilfried	Jo-Wilfried	k1gMnSc1	Jo-Wilfried
Tsonga	Tsonga	k1gFnSc1	Tsonga
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
titul	titul	k1gInSc4	titul
ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
když	když	k8xS	když
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
opět	opět	k6eAd1	opět
na	na	k7c6	na
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gMnSc1	Southern
Open	Open	k1gMnSc1	Open
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Davidem	David	k1gMnSc7	David
Ferrerem	Ferrer	k1gMnSc7	Ferrer
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
udržel	udržet	k5eAaPmAgMnS	udržet
neporazitelnost	neporazitelnost	k1gFnSc1	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
titulů	titul	k1gInPc2	titul
ze	z	k7c2	z
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
Masters	Masters	k1gInSc1	Masters
tak	tak	k6eAd1	tak
navýšil	navýšit	k5eAaPmAgInS	navýšit
na	na	k7c4	na
šest	šest	k4xCc4	šest
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
nad	nad	k7c7	nad
Vaskem	Vasek	k1gMnSc7	Vasek
Pospisilem	Pospisil	k1gMnSc7	Pospisil
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
tenistou	tenista	k1gMnSc7	tenista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
300	[number]	k4	300
výher	výhra	k1gFnPc2	výhra
ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turnaji	turnaj	k1gInSc6	turnaj
měl	mít	k5eAaImAgMnS	mít
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
bilanci	bilance	k1gFnSc4	bilance
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
prošel	projít	k5eAaPmAgMnS	projít
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
otočil	otočit	k5eAaPmAgMnS	otočit
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
s	s	k7c7	s
Gaëlem	Gaël	k1gMnSc7	Gaël
Monfilsem	Monfils	k1gMnSc7	Monfils
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vítěz	vítěz	k1gMnSc1	vítěz
Marin	Marina	k1gFnPc2	Marina
Čilić	Čilić	k1gMnSc1	Čilić
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
titul	titul	k1gInSc1	titul
roku	rok	k1gInSc2	rok
završil	završit	k5eAaPmAgInS	završit
říjnovou	říjnový	k2eAgFnSc7d1	říjnová
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Francouzem	Francouz	k1gMnSc7	Francouz
Gillesem	Gilles	k1gMnSc7	Gilles
Simonem	Simon	k1gMnSc7	Simon
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Shanghai	Shangha	k1gFnSc2	Shangha
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
první	první	k4xOgFnPc4	první
trofeje	trofej	k1gFnPc4	trofej
na	na	k7c6	na
čínské	čínský	k2eAgFnSc6d1	čínská
půdě	půda	k1gFnSc6	půda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
23	[number]	k4	23
turnajový	turnajový	k2eAgInSc4d1	turnajový
vavřín	vavřín	k1gInSc4	vavřín
ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Belgičanem	Belgičan	k1gMnSc7	Belgičan
Davidem	David	k1gMnSc7	David
Goffinem	Goffin	k1gMnSc7	Goffin
získal	získat	k5eAaPmAgMnS	získat
šestý	šestý	k4xOgInSc4	šestý
titul	titul	k1gInSc4	titul
na	na	k7c4	na
Swiss	Swiss	k1gInSc4	Swiss
Indoors	Indoorsa	k1gFnPc2	Indoorsa
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáctým	jedenáctý	k4xOgNnSc7	jedenáctý
finále	finále	k1gNnSc7	finále
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
mužský	mužský	k2eAgInSc4d1	mužský
rekord	rekord	k1gInSc4	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
tenisu	tenis	k1gInSc2	tenis
jako	jako	k8xS	jako
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
nejvíce	nejvíce	k6eAd1	nejvíce
finále	finále	k1gNnSc4	finále
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
třináctou	třináctý	k4xOgFnSc7	třináctý
účastí	účast	k1gFnSc7	účast
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Podvanácté	podvanácté	k4xO	podvanácté
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
a	a	k8xC	a
podeváté	podeváté	k4xO	podeváté
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
vyrovnal	vyrovnat	k5eAaPmAgInS	vyrovnat
Lendlův	Lendlův	k2eAgInSc4d1	Lendlův
rekordní	rekordní	k2eAgInSc4d1	rekordní
zápis	zápis	k1gInSc4	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
duelem	duel	k1gInSc7	duel
s	s	k7c7	s
Djokovićem	Djoković	k1gMnSc7	Djoković
však	však	k8xC	však
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
pro	pro	k7c4	pro
potíže	potíž	k1gFnPc4	potíž
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
slavil	slavit	k5eAaImAgMnS	slavit
s	s	k7c7	s
daviscupovým	daviscupový	k2eAgInSc7d1	daviscupový
týmem	tým	k1gInSc7	tým
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
triumf	triumf	k1gInSc1	triumf
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
porazili	porazit	k5eAaPmAgMnP	porazit
Francii	Francie	k1gFnSc4	Francie
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Švýcaři	Švýcar	k1gMnPc1	Švýcar
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zvedli	zvednout	k5eAaPmAgMnP	zvednout
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
"	"	kIx"	"
<g/>
salátovou	salátový	k2eAgFnSc4d1	salátová
mísu	mísa	k1gFnSc4	mísa
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
šampiony	šampion	k1gMnPc4	šampion
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
vkročil	vkročit	k5eAaPmAgInS	vkročit
titulem	titul	k1gInSc7	titul
na	na	k7c4	na
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc5	International
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Raonice	Raonice	k1gFnPc4	Raonice
<g/>
.	.	kIx.	.
</s>
<s>
Daný	daný	k2eAgInSc1d1	daný
zápas	zápas	k1gInSc1	zápas
znamenal	znamenat	k5eAaImAgInS	znamenat
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
profesionálním	profesionální	k2eAgInSc6d1	profesionální
tenisu	tenis	k1gInSc6	tenis
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Connorse	Connorse	k1gFnSc2	Connorse
a	a	k8xC	a
Lendla	Lendla	k1gMnSc2	Lendla
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
trofej	trofej	k1gFnSc4	trofej
v	v	k7c6	v
15	[number]	k4	15
sezónách	sezóna	k1gFnPc6	sezóna
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gNnSc4	Open
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
již	již	k6eAd1	již
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
překvapivě	překvapivě	k6eAd1	překvapivě
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Itala	Ital	k1gMnSc4	Ital
Andrease	Andreasa	k1gFnSc6	Andreasa
Seppiho	Seppi	k1gMnSc2	Seppi
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
předchozí	předchozí	k2eAgFnSc4d1	předchozí
bilanci	bilance	k1gFnSc4	bilance
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgFnSc4d1	rekordní
sedmou	sedmý	k4xOgFnSc4	sedmý
trofej	trofej	k1gFnSc4	trofej
dobyl	dobýt	k5eAaPmAgMnS	dobýt
na	na	k7c4	na
Dubai	Dubai	k1gNnSc4	Dubai
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
a	a	k8xC	a
jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
muž	muž	k1gMnSc1	muž
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
9	[number]	k4	9
000	[number]	k4	000
es	es	k1gNnPc2	es
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
navazující	navazující	k2eAgFnSc6d1	navazující
březnové	březnový	k2eAgFnSc6d1	březnová
události	událost	k1gFnSc6	událost
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
mu	on	k3xPp3gMnSc3	on
Srb	Srb	k1gMnSc1	Srb
oplatil	oplatit	k5eAaPmAgMnS	oplatit
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
prohru	prohra	k1gFnSc4	prohra
v	v	k7c4	v
příměm	příměm	k1gInSc1	příměm
boji	boj	k1gInSc3	boj
o	o	k7c4	o
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
antukový	antukový	k2eAgInSc1d1	antukový
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
když	když	k8xS	když
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
úvodní	úvodní	k2eAgInSc4d1	úvodní
ročník	ročník	k1gInSc4	ročník
Istanbulu	Istanbul	k1gInSc2	Istanbul
Open	Opena	k1gFnPc2	Opena
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Urugaycem	Urugayce	k1gMnSc7	Urugayce
Pablem	Pabl	k1gMnSc7	Pabl
Cuevasem	Cuevas	k1gMnSc7	Cuevas
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stalo	stát	k5eAaPmAgNnS	stát
19	[number]	k4	19
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
si	se	k3xPyFc3	se
odvezl	odvézt	k5eAaPmAgInS	odvézt
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
od	od	k7c2	od
Djokoviće	Djoković	k1gFnSc2	Djoković
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
antuce	antuka	k1gFnSc6	antuka
Internazionali	Internazionali	k1gFnSc2	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
.	.	kIx.	.
</s>
<s>
Odměna	odměna	k1gFnSc1	odměna
308	[number]	k4	308
tisíc	tisíc	k4xCgInPc2	tisíc
eur	euro	k1gNnPc2	euro
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
tenistou	tenista	k1gMnSc7	tenista
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vydělal	vydělat	k5eAaPmAgInS	vydělat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
s	s	k7c7	s
osmým	osmý	k4xOgMnSc7	osmý
nasazeným	nasazený	k2eAgMnSc7d1	nasazený
krajanem	krajan	k1gMnSc7	krajan
a	a	k8xC	a
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vítězem	vítěz	k1gMnSc7	vítěz
Stanem	stan	k1gInSc7	stan
Wawrinkou	Wawrinký	k2eAgFnSc4d1	Wawrinký
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tak	tak	k6eAd1	tak
snížil	snížit	k5eAaPmAgInS	snížit
pasivní	pasivní	k2eAgInSc1d1	pasivní
poměr	poměr	k1gInSc1	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
duelů	duel	k1gInPc2	duel
na	na	k7c4	na
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgFnSc4d1	rekordní
osmou	osmý	k4xOgFnSc4	osmý
trofej	trofej	k1gFnSc4	trofej
dobyl	dobýt	k5eAaPmAgMnS	dobýt
na	na	k7c6	na
halleském	halleský	k2eAgInSc6d1	halleský
Gerry	Gerra	k1gFnSc2	Gerra
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gMnSc1	Open
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Andrease	Andreasa	k1gFnSc3	Andreasa
Seppiho	Seppi	k1gMnSc2	Seppi
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
teprve	teprve	k6eAd1	teprve
třetím	třetí	k4xOgMnSc7	třetí
mužem	muž	k1gMnSc7	muž
open	open	k1gInSc4	open
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
stejný	stejný	k2eAgInSc1d1	stejný
turnaj	turnaj	k1gInSc1	turnaj
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
nejméně	málo	k6eAd3	málo
osmkrát	osmkrát	k6eAd1	osmkrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
postoupil	postoupit	k5eAaPmAgMnS	postoupit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
rekordního	rekordní	k2eAgInSc2d1	rekordní
desátého	desátý	k4xOgInSc2	desátý
finále	finále	k1gNnSc6	finále
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
finalista	finalista	k1gMnSc1	finalista
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
39	[number]	k4	39
<g/>
letého	letý	k2eAgInSc2d1	letý
Kena	Ken	k1gInSc2	Ken
Rosewalla	Rosewallo	k1gNnSc2	Rosewallo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
ročníku	ročník	k1gInSc6	ročník
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Novaku	Novak	k1gMnSc3	Novak
Djokovićovi	Djokovića	k1gMnSc3	Djokovića
po	po	k7c6	po
čtyřsetovém	čtyřsetový	k2eAgInSc6d1	čtyřsetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgInSc4d1	rekordní
sedmý	sedmý	k4xOgInSc4	sedmý
vavřín	vavřín	k1gInSc4	vavřín
si	se	k3xPyFc3	se
odvezl	odvézt	k5eAaPmAgMnS	odvézt
ze	z	k7c2	z
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
oplatil	oplatit	k5eAaPmAgInS	oplatit
porážku	porážka	k1gFnSc4	porážka
Djokovićovi	Djokovića	k1gMnSc3	Djokovića
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
turnaji	turnaj	k1gInSc6	turnaj
porazil	porazit	k5eAaPmAgMnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
i	i	k8xC	i
dvojku	dvojka	k1gFnSc4	dvojka
v	v	k7c6	v
navazujících	navazující	k2eAgInPc6d1	navazující
zápasech	zápas	k1gInPc6	zápas
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
během	během	k7c2	během
pěti	pět	k4xCc2	pět
kol	kolo	k1gNnPc2	kolo
neztratil	ztratit	k5eNaPmAgInS	ztratit
žádný	žádný	k3yNgInSc4	žádný
set	set	k1gInSc4	set
ani	ani	k8xC	ani
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
bez	bez	k7c2	bez
ztracené	ztracený	k2eAgFnSc2d1	ztracená
sady	sada	k1gFnSc2	sada
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
hráčů	hráč	k1gMnPc2	hráč
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
patého	patého	k2eAgMnPc4d1	patého
muže	muž	k1gMnPc4	muž
žebříčku	žebříček	k1gInSc2	žebříček
Stana	Stan	k1gMnSc2	Stan
Wawrinku	Wawrink	k1gInSc2	Wawrink
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
souboji	souboj	k1gInSc6	souboj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
však	však	k9	však
nenašel	najít	k5eNaPmAgMnS	najít
<g/>
,	,	kIx,	,
po	po	k7c6	po
čtyřsetovém	čtyřsetový	k2eAgInSc6d1	čtyřsetový
průběhu	průběh	k1gInSc6	průběh
<g/>
,	,	kIx,	,
recept	recept	k1gInSc4	recept
na	na	k7c4	na
Novaka	Novak	k1gMnSc4	Novak
Djokoviće	Djoković	k1gMnSc4	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
Švýcar	Švýcar	k1gMnSc1	Švýcar
objevil	objevit	k5eAaPmAgMnS	objevit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
posedmé	posedmé	k4xO	posedmé
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
Swiss	Swissa	k1gFnPc2	Swissa
Indoors	Indoorsa	k1gFnPc2	Indoorsa
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvanácté	dvanáctý	k4xOgFnSc6	dvanáctý
sezóně	sezóna	k1gFnSc6	sezóna
za	za	k7c7	za
sebou	se	k3xPyFc7	se
potkal	potkat	k5eAaPmAgMnS	potkat
s	s	k7c7	s
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
zdolal	zdolat	k5eAaPmAgMnS	zdolat
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
a	a	k8xC	a
půl	půl	k1xP	půl
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
šňůře	šňůra	k1gFnSc6	šňůra
pěti	pět	k4xCc2	pět
porážek	porážka	k1gFnPc2	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Navýšil	navýšit	k5eAaPmAgInS	navýšit
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
rekord	rekord	k1gInSc4	rekord
open	open	k1gNnSc4	open
éry	éra	k1gFnSc2	éra
dvanáctým	dvanáctý	k4xOgNnSc7	dvanáctý
finále	finále	k1gNnSc7	finále
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
turnaji	turnaj	k1gInSc6	turnaj
a	a	k8xC	a
desátým	desátý	k4xOgMnSc7	desátý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgFnSc4d1	rekordní
čtrnáctou	čtrnáctý	k4xOgFnSc4	čtrnáctý
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
na	na	k7c4	na
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
události	událost	k1gFnPc4	událost
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
Finals	Finals	k1gInSc4	Finals
<g/>
,	,	kIx,	,
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
desátý	desátý	k4xOgInSc4	desátý
postup	postup	k1gInSc4	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
od	od	k7c2	od
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osmi	osm	k4xCc2	osm
vzájemných	vzájemný	k2eAgNnPc2d1	vzájemné
utkání	utkání	k1gNnPc2	utkání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
jej	on	k3xPp3gInSc4	on
zdolal	zdolat	k5eAaPmAgMnS	zdolat
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
Turnaje	turnaj	k1gInPc4	turnaj
mistrů	mistr	k1gMnPc2	mistr
přitom	přitom	k6eAd1	přitom
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Srbem	Srb	k1gMnSc7	Srb
přerušil	přerušit	k5eAaPmAgMnS	přerušit
jeho	jeho	k3xOp3gFnSc4	jeho
šňůru	šňůra	k1gFnSc4	šňůra
38	[number]	k4	38
<g/>
zápasové	zápasový	k2eAgFnPc1d1	zápasová
neporazitelnosti	neporazitelnost	k1gFnPc1	neporazitelnost
na	na	k7c6	na
krytých	krytý	k2eAgInPc6d1	krytý
dvorcích	dvorec	k1gInPc6	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
zakončil	zakončit	k5eAaPmAgInS	zakončit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
za	za	k7c7	za
Novakem	Novak	k1gInSc7	Novak
Djokovićem	Djokovićem	k1gInSc1	Djokovićem
a	a	k8xC	a
Andym	Andym	k1gInSc1	Andym
Murraym	Murraymum	k1gNnPc2	Murraymum
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
rozehrál	rozehrát	k5eAaPmAgMnS	rozehrát
jako	jako	k8xS	jako
obhájce	obhájce	k1gMnSc1	obhájce
trofeje	trofej	k1gFnSc2	trofej
a	a	k8xC	a
nejvýše	nejvýše	k6eAd1	nejvýše
nasazený	nasazený	k2eAgMnSc1d1	nasazený
na	na	k7c6	na
lednovém	lednový	k2eAgInSc6d1	lednový
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc5	International
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rerpríze	rerpríz	k1gInSc6	rerpríz
předchozího	předchozí	k2eAgNnSc2d1	předchozí
finále	finále	k1gNnSc2	finále
mu	on	k3xPp3gNnSc3	on
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
Milos	Milosa	k1gFnPc2	Milosa
Raonic	Raonice	k1gFnPc2	Raonice
oplatil	oplatit	k5eAaPmAgInS	oplatit
rok	rok	k1gInSc1	rok
starou	starý	k2eAgFnSc4d1	stará
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gMnSc1	Open
odešel	odejít	k5eAaPmAgMnS	odejít
posedmé	posedmé	k4xO	posedmé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
poražen	porazit	k5eAaPmNgMnS	porazit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
prvního	první	k4xOgMnSc4	první
hráče	hráč	k1gMnSc4	hráč
žebříčku	žebříček	k1gInSc2	žebříček
Novaka	Novak	k1gMnSc4	Novak
Djokoviće	Djoković	k1gMnSc4	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Srb	Srb	k1gMnSc1	Srb
jej	on	k3xPp3gMnSc4	on
zastavil	zastavit	k5eAaPmAgMnS	zastavit
na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
majoru	major	k1gMnSc6	major
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
semifinále	semifinále	k1gNnSc6	semifinále
Australian	Australiana	k1gFnPc2	Australiana
Open	Openo	k1gNnPc2	Openo
si	se	k3xPyFc3	se
při	při	k7c6	při
napouštění	napouštění	k1gNnSc6	napouštění
vany	vana	k1gFnSc2	vana
dětem	dítě	k1gFnPc3	dítě
přivodil	přivodit	k5eAaPmAgMnS	přivodit
poranění	poranění	k1gNnSc4	poranění
kolene	kolen	k1gInSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
února	únor	k1gInSc2	únor
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
artroskopický	artroskopický	k2eAgInSc4d1	artroskopický
operační	operační	k2eAgInSc4d1	operační
výkon	výkon	k1gInSc4	výkon
natrženého	natržený	k2eAgInSc2d1	natržený
menisku	meniskus	k1gInSc2	meniskus
se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
měsíčním	měsíční	k2eAgInSc7d1	měsíční
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
výpadkem	výpadek	k1gInSc7	výpadek
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
programu	program	k1gInSc2	program
dubnový	dubnový	k2eAgInSc4d1	dubnový
Monte-Carlo	Monte-Carlo	k1gNnSc1	Monte-Carlo
Rolex	Rolex	k1gInSc1	Rolex
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
s	s	k7c7	s
Jo-Wilfried	Jo-Wilfried	k1gInSc1	Jo-Wilfried
Tsongou	Tsonga	k1gFnSc7	Tsonga
<g/>
.	.	kIx.	.
</s>
<s>
Odstoupením	odstoupení	k1gNnSc7	odstoupení
z	z	k7c2	z
French	Frencha	k1gFnPc2	Frencha
Open	Opena	k1gFnPc2	Opena
pro	pro	k7c4	pro
bolest	bolest	k1gFnSc4	bolest
zad	zad	k1gInSc1	zad
stanovil	stanovit	k5eAaPmAgInS	stanovit
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
65	[number]	k4	65
účastmi	účast	k1gFnPc7	účast
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
soutěžích	soutěž	k1gFnPc6	soutěž
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
MercedesCupu	MercedesCup	k1gInSc6	MercedesCup
zdolal	zdolat	k5eAaPmAgMnS	zdolat
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
Taylora	Taylora	k1gFnSc1	Taylora
Fritze	Fritze	k1gFnSc1	Fritze
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
posunul	posunout	k5eAaPmAgMnS	posunout
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
statistik	statistika	k1gFnPc2	statistika
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vítězných	vítězný	k2eAgInPc2d1	vítězný
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
1	[number]	k4	1
072	[number]	k4	072
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
utkání	utkání	k1gNnSc4	utkání
překonal	překonat	k5eAaPmAgMnS	překonat
Ivana	Ivan	k1gMnSc4	Ivan
Lendla	Lendla	k1gMnSc4	Lendla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Dominica	Dominicus	k1gMnSc2	Dominicus
Thiema	Thiem	k1gMnSc2	Thiem
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
tak	tak	k6eAd1	tak
prohrál	prohrát	k5eAaPmAgMnS	prohrát
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
něproměnil	něproměnit	k5eAaPmAgInS	něproměnit
mečbol	mečbol	k1gInSc1	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgFnPc1d1	navazující
Gerry	Gerra	k1gFnPc1	Gerra
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gMnSc1	Open
znamenal	znamenat	k5eAaImAgMnS	znamenat
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
od	od	k7c2	od
teenagera	teenager	k1gMnSc2	teenager
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
nezvládnutém	zvládnutý	k2eNgNnSc6d1	nezvládnuté
semifinále	semifinále	k1gNnSc6	semifinále
s	s	k7c7	s
19	[number]	k4	19
<g/>
letým	letý	k2eAgInSc7d1	letý
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Zverevem	Zverev	k1gInSc7	Zverev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
dokázal	dokázat	k5eAaPmAgMnS	dokázat
otočit	otočit	k5eAaPmF	otočit
průběh	průběh	k1gInSc4	průběh
čtvrtfinále	čtvrtfinále	k1gNnPc2	čtvrtfinále
proti	proti	k7c3	proti
Čilićovi	Čilića	k1gMnSc3	Čilića
z	z	k7c2	z
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
na	na	k7c4	na
sety	set	k1gInPc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
307	[number]	k4	307
<g/>
.	.	kIx.	.
vítězným	vítězný	k2eAgInSc7d1	vítězný
zápasem	zápas	k1gInSc7	zápas
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc3	slam
překonal	překonat	k5eAaPmAgMnS	překonat
historický	historický	k2eAgInSc4d1	historický
rekord	rekord	k1gInSc4	rekord
Martiny	Martin	k2eAgFnSc2d1	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
také	také	k9	také
rekordních	rekordní	k2eAgFnPc2d1	rekordní
jedenáct	jedenáct	k4xCc1	jedenáct
účastí	účast	k1gFnPc2	účast
Connorse	Connorse	k1gFnSc2	Connorse
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Premiérově	premiérově	k6eAd1	premiérově
však	však	k9	však
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
majoru	major	k1gMnSc3	major
nalezl	naleznout	k5eAaPmAgInS	naleznout
přemožitele	přemožitel	k1gMnSc4	přemožitel
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
zdolala	zdolat	k5eAaPmAgFnS	zdolat
světová	světový	k2eAgFnSc1d1	světová
sedmička	sedmička	k1gFnSc1	sedmička
Milos	Milosa	k1gFnPc2	Milosa
Raonic	Raonice	k1gFnPc2	Raonice
v	v	k7c6	v
pěti	pět	k4xCc6	pět
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pokračující	pokračující	k2eAgInPc4d1	pokračující
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
levým	levý	k2eAgNnSc7d1	levé
kolenem	koleno	k1gNnSc7	koleno
ukončil	ukončit	k5eAaPmAgInS	ukončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
předčasně	předčasně	k6eAd1	předčasně
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vynechal	vynechat	k5eAaPmAgInS	vynechat
i	i	k8xC	i
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
věnovat	věnovat	k5eAaPmF	věnovat
rekonvalescenci	rekonvalescence	k1gFnSc4	rekonvalescence
a	a	k8xC	a
uzdravit	uzdravit	k5eAaPmF	uzdravit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
působit	působit	k5eAaImF	působit
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Završil	završit	k5eAaPmAgMnS	završit
tím	ten	k3xDgNnSc7	ten
rekordní	rekordní	k2eAgFnSc4d1	rekordní
šňůru	šňůra	k1gFnSc4	šňůra
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
sezónách	sezóna	k1gFnPc6	sezóna
za	za	k7c4	za
sebou	se	k3xPyFc7	se
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tříměsíční	tříměsíční	k2eAgFnSc6d1	tříměsíční
neaktivitě	neaktivita	k1gFnSc6	neaktivita
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
když	když	k8xS	když
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
členem	člen	k1gInSc7	člen
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
turnajem	turnaj	k1gInSc7	turnaj
je	být	k5eAaImIp3nS	být
lednový	lednový	k2eAgInSc4d1	lednový
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
tým	tým	k1gInSc1	tým
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Belindou	Belinda	k1gFnSc7	Belinda
Bencicovou	Bencicová	k1gFnSc7	Bencicová
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Soupeření	soupeření	k1gNnSc2	soupeření
Federera	Federer	k1gMnSc2	Federer
a	a	k8xC	a
Nadala	nadat	k5eAaPmAgFnS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
a	a	k8xC	a
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
střetávají	střetávat	k5eAaImIp3nP	střetávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
vzájemná	vzájemný	k2eAgNnPc1d1	vzájemné
utkání	utkání	k1gNnPc1	utkání
představují	představovat	k5eAaImIp3nP	představovat
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
profesionální	profesionální	k2eAgFnSc2d1	profesionální
kariéry	kariéra	k1gFnSc2	kariéra
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
figurovali	figurovat	k5eAaImAgMnP	figurovat
na	na	k7c4	na
první	první	k4xOgInSc4	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
příčce	příčka	k1gFnSc3	příčka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Nadala	nadat	k5eAaPmAgFnS	nadat
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
pozici	pozice	k1gFnSc6	pozice
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Skot	Skot	k1gMnSc1	Skot
Andy	Anda	k1gFnSc2	Anda
Murray	Murraa	k1gFnSc2	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
a	a	k8xC	a
Španěl	Španěl	k1gMnSc1	Španěl
představují	představovat	k5eAaImIp3nP	představovat
jedinou	jediný	k2eAgFnSc4d1	jediná
dvojici	dvojice	k1gFnSc4	dvojice
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
mužského	mužský	k2eAgInSc2d1	mužský
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zakončila	zakončit	k5eAaPmAgFnS	zakončit
šest	šest	k4xCc4	šest
sezón	sezóna	k1gFnPc2	sezóna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
období	období	k1gNnSc6	období
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
Federer	Federer	k1gInSc4	Federer
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
nepřetržitě	přetržitě	k6eNd1	přetržitě
237	[number]	k4	237
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
rekordní	rekordní	k2eAgNnSc4d1	rekordní
období	období	k1gNnSc4	období
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc3d2	mladší
Nadal	nadat	k5eAaPmAgMnS	nadat
se	se	k3xPyFc4	se
propracoval	propracovat	k5eAaPmAgMnS	propracovat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
a	a	k8xC	a
držel	držet	k5eAaImAgInS	držet
ji	on	k3xPp3gFnSc4	on
160	[number]	k4	160
týdnů	týden	k1gInPc2	týden
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
rekord	rekord	k1gInSc4	rekord
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
basilejského	basilejský	k2eAgMnSc4d1	basilejský
rodáka	rodák	k1gMnSc4	rodák
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
mužského	mužský	k2eAgNnSc2d1	mužské
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS	nadat
má	mít	k5eAaImIp3nS	mít
aktivní	aktivní	k2eAgFnSc4d1	aktivní
bilanci	bilance	k1gFnSc4	bilance
vzájemných	vzájemný	k2eAgNnPc2d1	vzájemné
utkání	utkání	k1gNnPc2	utkání
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Patnáct	patnáct	k4xCc1	patnáct
z	z	k7c2	z
třiceti	třicet	k4xCc2	třicet
čtyř	čtyři	k4xCgNnPc2	čtyři
duelů	duel	k1gInPc2	duel
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
,	,	kIx,	,
nejdominantnějším	dominantní	k2eAgInSc6d3	nejdominantnější
povrchu	povrch	k1gInSc6	povrch
mallorského	mallorský	k2eAgMnSc2d1	mallorský
rodáka	rodák	k1gMnSc2	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
vícekrát	vícekrát	k6eAd1	vícekrát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c4	na
jím	on	k3xPp3gMnSc7	on
preferované	preferovaný	k2eAgFnSc6d1	preferovaná
trávě	tráva	k1gFnSc6	tráva
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
španělský	španělský	k2eAgMnSc1d1	španělský
hráč	hráč	k1gMnSc1	hráč
je	být	k5eAaImIp3nS	být
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
na	na	k7c6	na
otevřených	otevřený	k2eAgInPc6d1	otevřený
dvorcích	dvorec	k1gInPc6	dvorec
s	s	k7c7	s
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
povrchem	povrch	k1gInSc7	povrch
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
antuce	antuka	k1gFnSc6	antuka
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nasazování	nasazování	k1gNnSc1	nasazování
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
žebříčkového	žebříčkový	k2eAgNnSc2d1	žebříčkové
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
utkali	utkat	k5eAaPmAgMnP	utkat
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
až	až	k6eAd1	až
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osmkrát	osmkrát	k6eAd1	osmkrát
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k9	také
rekordní	rekordní	k2eAgInSc4d1	rekordní
zápis	zápis	k1gInSc4	zápis
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2008	[number]	k4	2008
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
finále	finále	k1gNnSc6	finále
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Potkali	potkat	k5eAaPmAgMnP	potkat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
zápasu	zápas	k1gInSc6	zápas
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gNnSc1	Open
2009	[number]	k4	2009
a	a	k8xC	a
French	French	k1gMnSc1	French
Open	Open	k1gInSc4	Open
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS	nadat
získal	získat	k5eAaPmAgMnS	získat
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
šest	šest	k4xCc1	šest
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ztratil	ztratit	k5eAaPmAgMnS	ztratit
pouze	pouze	k6eAd1	pouze
první	první	k4xOgNnSc4	první
dvě	dva	k4xCgNnPc4	dva
wimbledonská	wimbledonský	k2eAgNnPc4d1	wimbledonské
finále	finále	k1gNnPc4	finále
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
osmi	osm	k4xCc2	osm
grandslamových	grandslamový	k2eAgNnPc2d1	grandslamové
klání	klání	k1gNnPc2	klání
o	o	k7c4	o
titul	titul	k1gInSc4	titul
měla	mít	k5eAaImAgFnS	mít
pětisetový	pětisetový	k2eAgInSc4d1	pětisetový
průběh	průběh	k1gInSc4	průběh
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
a	a	k8xC	a
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Wimbledonské	wimbledonský	k2eAgNnSc1d1	wimbledonské
finále	finále	k1gNnSc1	finále
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
bývá	bývat	k5eAaImIp3nS	bývat
řadou	řada	k1gFnSc7	řada
tenisových	tenisový	k2eAgMnPc2d1	tenisový
analytiků	analytik	k1gMnPc2	analytik
označováno	označován	k2eAgNnSc1d1	označováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zápas	zápas	k1gInSc4	zápas
historie	historie	k1gFnSc2	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
střetnutí	střetnutí	k1gNnPc2	střetnutí
dospělo	dochvít	k5eAaPmAgNnS	dochvít
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
také	také	k6eAd1	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
rekordní	rekordní	k2eAgInSc4d1	rekordní
počet	počet	k1gInSc4	počet
deseti	deset	k4xCc2	deset
vzájemných	vzájemný	k2eAgNnPc2d1	vzájemné
finále	finále	k1gNnPc2	finále
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
spolusdílí	spolusdílet	k5eAaPmIp3nS	spolusdílet
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
Nadal	nadat	k5eAaPmAgMnS	nadat
a	a	k8xC	a
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
finálové	finálový	k2eAgInPc4d1	finálový
boje	boj	k1gInPc4	boj
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pětihodinová	pětihodinový	k2eAgFnSc1d1	pětihodinová
bitva	bitva	k1gFnSc1	bitva
na	na	k7c4	na
Rome	Rom	k1gMnSc5	Rom
Masters	Mastersa	k1gFnPc2	Mastersa
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
získal	získat	k5eAaPmAgMnS	získat
Španěl	Španěl	k1gMnSc1	Španěl
až	až	k9	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
pátého	pátý	k4xOgInSc2	pátý
setu	set	k1gInSc2	set
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
předtím	předtím	k6eAd1	předtím
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
dva	dva	k4xCgInPc4	dva
mečboly	mečbol	k1gInPc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Soupeření	soupeření	k1gNnSc2	soupeření
Djokoviće	Djoković	k1gFnSc2	Djoković
a	a	k8xC	a
Federera	Federero	k1gNnSc2	Federero
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
a	a	k8xC	a
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
se	se	k3xPyFc4	se
potkali	potkat	k5eAaPmAgMnP	potkat
ve	v	k7c6	v
45	[number]	k4	45
utkáních	utkání	k1gNnPc6	utkání
okruhu	okruh	k1gInSc2	okruh
(	(	kIx(	(
<g/>
a	a	k8xC	a
finále	finále	k1gNnSc1	finále
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
2014	[number]	k4	2014
nebylo	být	k5eNaImAgNnS	být
odehráno	odehrát	k5eAaPmNgNnS	odehrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srbský	srbský	k2eAgMnSc1d1	srbský
hráč	hráč	k1gMnSc1	hráč
drží	držet	k5eAaImIp3nS	držet
mírně	mírně	k6eAd1	mírně
kladnou	kladný	k2eAgFnSc4d1	kladná
bilanci	bilance	k1gFnSc4	bilance
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Bilance	bilance	k1gFnSc1	bilance
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
je	být	k5eAaImIp3nS	být
také	také	k9	také
paritní	paritní	k2eAgInSc1d1	paritní
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
vede	vést	k5eAaImIp3nS	vést
Srb	Srb	k1gMnSc1	Srb
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
když	když	k8xS	když
první	první	k4xOgNnSc1	první
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
utkání	utkání	k1gNnSc1	utkání
na	na	k7c6	na
zeleném	zelený	k2eAgInSc6d1	zelený
pažitu	pažit	k1gInSc6	pažit
Federer	Federer	k1gMnSc1	Federer
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2012	[number]	k4	2012
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
poražen	poražen	k2eAgMnSc1d1	poražen
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
finále	finále	k1gNnSc2	finále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2014	[number]	k4	2014
i	i	k9	i
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
měření	měření	k1gNnSc1	měření
sil	síla	k1gFnPc2	síla
dospělo	dochvít	k5eAaPmAgNnS	dochvít
až	až	k9	až
do	do	k7c2	do
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
spolu	spolu	k6eAd1	spolu
odehráli	odehrát	k5eAaPmAgMnP	odehrát
čtrnáct	čtrnáct	k4xCc4	čtrnáct
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
mírně	mírně	k6eAd1	mírně
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
pro	pro	k7c4	pro
Djokoviće	Djokoviće	k1gInSc4	Djokoviće
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
Nadala	nadat	k5eAaPmAgFnS	nadat
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
srbský	srbský	k2eAgMnSc1d1	srbský
tenista	tenista	k1gMnSc1	tenista
jediným	jediný	k2eAgMnSc7d1	jediný
hráčem	hráč	k1gMnSc7	hráč
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
švýcarského	švýcarský	k2eAgMnSc4d1	švýcarský
hráče	hráč	k1gMnSc4	hráč
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zdolat	zdolat	k5eAaPmF	zdolat
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
turnajích	turnaj	k1gInPc6	turnaj
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
čtyřky	čtyřka	k1gFnPc4	čtyřka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jej	on	k3xPp3gInSc4	on
přehrál	přehrát	k5eAaPmAgInS	přehrát
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
ho	on	k3xPp3gInSc4	on
také	také	k6eAd1	také
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nadalem	Nadal	k1gInSc7	Nadal
a	a	k8xC	a
Murraym	Murraym	k1gInSc4	Murraym
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
jedinými	jediný	k2eAgInPc7d1	jediný
třemi	tři	k4xCgMnPc7	tři
tenisty	tenista	k1gMnPc7	tenista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
nad	nad	k7c7	nad
Federerem	Federer	k1gInSc7	Federer
dvouciferného	dvouciferný	k2eAgInSc2d1	dvouciferný
počtu	počet	k1gInSc2	počet
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
Djoković	Djoković	k1gFnSc1	Djoković
s	s	k7c7	s
Nadalem	Nadal	k1gMnSc7	Nadal
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
osamoceni	osamocen	k2eAgMnPc1d1	osamocen
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
grandslamových	grandslamový	k2eAgFnPc2d1	grandslamová
výher	výhra	k1gFnPc2	výhra
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
když	když	k8xS	když
Srb	Srb	k1gMnSc1	Srb
stavu	stav	k1gInSc2	stav
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
sety	set	k1gInPc4	set
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gNnSc1	Open
2011	[number]	k4	2011
a	a	k8xC	a
French	French	k1gMnSc1	French
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Španěl	Španěl	k1gMnSc1	Španěl
jej	on	k3xPp3gMnSc4	on
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
dokázal	dokázat	k5eAaPmAgInS	dokázat
zdolat	zdolat	k5eAaPmF	zdolat
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
ukončil	ukončit	k5eAaPmAgInS	ukončit
Djokovićovu	Djokovićův	k2eAgFnSc4d1	Djokovićův
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
Srb	Srb	k1gMnSc1	Srb
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
šňůrou	šňůra	k1gFnSc7	šňůra
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
jedna	jeden	k4xCgFnSc1	jeden
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
2011	[number]	k4	2011
však	však	k8xC	však
švýcarskému	švýcarský	k2eAgMnSc3d1	švýcarský
tenistovi	tenista	k1gMnSc3	tenista
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
po	po	k7c6	po
čtyřsetovém	čtyřsetový	k2eAgInSc6d1	čtyřsetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
porážku	porážka	k1gFnSc4	porážka
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
oplatil	oplatit	k5eAaPmAgInS	oplatit
v	v	k7c6	v
pětisetovém	pětisetový	k2eAgNnSc6d1	Pětisetové
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
dramatu	drama	k1gNnSc6	drama
na	na	k7c6	na
záříjovém	záříjový	k2eAgNnSc6d1	záříjové
US	US	kA	US
Open	Open	k1gInSc4	Open
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
za	za	k7c7	za
sebou	se	k3xPyFc7	se
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
dva	dva	k4xCgInPc4	dva
Federerovy	Federerův	k2eAgInPc4d1	Federerův
mečboly	mečbol	k1gInPc4	mečbol
a	a	k8xC	a
prošel	projít	k5eAaPmAgMnS	projít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
série	série	k1gFnSc1	série
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Švýcar	Švýcar	k1gMnSc1	Švýcar
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
čtyřsetovou	čtyřsetový	k2eAgFnSc4d1	čtyřsetový
porážku	porážka	k1gFnSc4	porážka
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
světové	světový	k2eAgFnSc2d1	světová
jedničce	jednička	k1gFnSc3	jednička
a	a	k8xC	a
obhájci	obhájce	k1gMnPc1	obhájce
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
finále	finále	k1gNnSc7	finále
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
2014	[number]	k4	2014
Švýcar	Švýcar	k1gMnSc1	Švýcar
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
pro	pro	k7c4	pro
zádové	zádový	k2eAgFnPc4d1	zádová
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tak	tak	k9	tak
ve	v	k7c6	v
45	[number]	k4	45
<g/>
leté	letý	k2eAgFnSc6d1	letá
historii	historie	k1gFnSc6	historie
turnaje	turnaj	k1gInSc2	turnaj
finálový	finálový	k2eAgInSc4d1	finálový
duel	duel	k1gInSc4	duel
neodehrál	odehrát	k5eNaPmAgMnS	odehrát
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
expertů	expert	k1gMnPc2	expert
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
soupeření	soupeření	k1gNnSc1	soupeření
obou	dva	k4xCgMnPc2	dva
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Murray	Murraa	k1gFnSc2	Murraa
odehráli	odehrát	k5eAaPmAgMnP	odehrát
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
<g/>
nbsp	nbsp	k1gInSc4	nbsp
<g/>
;	;	kIx,	;
<g/>
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
tenista	tenista	k1gMnSc1	tenista
drží	držet	k5eAaImIp3nS	držet
mírně	mírně	k6eAd1	mírně
aktivní	aktivní	k2eAgFnSc4d1	aktivní
bilanci	bilance	k1gFnSc4	bilance
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
Federer	Federer	k1gMnSc1	Federer
vede	vést	k5eAaImIp3nS	vést
těsně	těsně	k6eAd1	těsně
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
drží	držet	k5eAaImIp3nS	držet
poměr	poměr	k1gInSc1	poměr
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnSc1	dvojice
potkala	potkat	k5eAaPmAgFnS	potkat
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
třech	tři	k4xCgInPc6	tři
duelech	duel	k1gInPc6	duel
vždy	vždy	k6eAd1	vždy
až	až	k9	až
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
když	když	k8xS	když
soupeře	soupeř	k1gMnSc4	soupeř
zdolal	zdolat	k5eAaPmAgMnS	zdolat
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2008	[number]	k4	2008
a	a	k8xC	a
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2012	[number]	k4	2012
mu	on	k3xPp3gMnSc3	on
soupeř	soupeř	k1gMnSc1	soupeř
odebral	odebrat	k5eAaPmAgMnS	odebrat
pouze	pouze	k6eAd1	pouze
úvodní	úvodní	k2eAgFnSc4d1	úvodní
sadu	sada	k1gFnSc4	sada
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc4	série
Skot	skot	k1gInSc1	skot
zastavil	zastavit	k5eAaPmAgInS	zastavit
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
pětisetovém	pětisetový	k2eAgInSc6d1	pětisetový
průběhu	průběh	k1gInSc6	průběh
slavil	slavit	k5eAaImAgInS	slavit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
ročníku	ročník	k1gInSc6	ročník
turnaje	turnaj	k1gInSc2	turnaj
2014	[number]	k4	2014
porážku	porážka	k1gFnSc4	porážka
oplatil	oplatit	k5eAaPmAgInS	oplatit
čtvrtfinálovou	čtvrtfinálový	k2eAgFnSc7d1	čtvrtfinálová
výhrou	výhra	k1gFnSc7	výhra
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
třísetové	třísetový	k2eAgNnSc4d1	třísetové
vítězství	vítězství	k1gNnSc4	vítězství
přidal	přidat	k5eAaPmAgMnS	přidat
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2015	[number]	k4	2015
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
wimbledonském	wimbledonský	k2eAgNnSc6d1	wimbledonské
finále	finále	k1gNnSc6	finále
2012	[number]	k4	2012
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
znovu	znovu	k6eAd1	znovu
střetli	střetnout	k5eAaPmAgMnP	střetnout
na	na	k7c6	na
Centrálním	centrální	k2eAgInSc6d1	centrální
dvorci	dvorec	k1gInSc6	dvorec
All	All	k1gFnSc1	All
England	England	k1gInSc1	England
Clubu	club	k1gInSc2	club
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
boji	boj	k1gInSc6	boj
o	o	k7c4	o
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
když	když	k8xS	když
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
turnaje	turnaj	k1gInSc2	turnaj
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Murray	Murra	k1gMnPc4	Murra
odčinil	odčinit	k5eAaPmAgInS	odčinit
poslední	poslední	k2eAgFnSc4d1	poslední
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
Švýcara	Švýcar	k1gMnSc4	Švýcar
zdolal	zdolat	k5eAaPmAgMnS	zdolat
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mu	on	k3xPp3gMnSc3	on
nedovolil	dovolit	k5eNaPmAgInS	dovolit
získat	získat	k5eAaPmF	získat
kariérní	kariérní	k2eAgInSc1d1	kariérní
Golden	Goldno	k1gNnPc2	Goldno
Slam	sláma	k1gFnPc2	sláma
<g/>
.	.	kIx.	.
</s>
<s>
Murray	Murraa	k1gFnPc1	Murraa
také	také	k9	také
drží	držet	k5eAaImIp3nP	držet
aktivní	aktivní	k2eAgInSc4d1	aktivní
poměr	poměr	k1gInSc4	poměr
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
na	na	k7c6	na
událostech	událost	k1gFnPc6	událost
série	série	k1gFnSc2	série
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
oba	dva	k4xCgMnPc1	dva
tenisté	tenista	k1gMnPc1	tenista
narazili	narazit	k5eAaPmAgMnP	narazit
během	během	k7c2	během
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šanghajském	šanghajský	k2eAgInSc6d1	šanghajský
duelu	duel	k1gInSc6	duel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
nejdříve	dříve	k6eAd3	dříve
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
Skot	Skot	k1gMnSc1	Skot
a	a	k8xC	a
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
tři	tři	k4xCgFnPc4	tři
výhry	výhra	k1gFnPc4	výhra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
a	a	k8xC	a
2012	[number]	k4	2012
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
odvezl	odvézt	k5eAaPmAgMnS	odvézt
Federer	Federer	k1gMnSc1	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
2014	[number]	k4	2014
Švýcar	Švýcary	k1gInPc2	Švýcary
skotského	skotský	k1gInSc2	skotský
rivala	rival	k1gMnSc2	rival
deklasoval	deklasovat	k5eAaBmAgMnS	deklasovat
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
člen	člen	k1gMnSc1	člen
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
set	set	k1gInSc4	set
"	"	kIx"	"
<g/>
kanárem	kanár	k1gMnSc7	kanár
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
zbylým	zbylý	k2eAgMnPc3d1	zbylý
členům	člen	k1gMnPc3	člen
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Djokovićem	Djoković	k1gInSc7	Djoković
a	a	k8xC	a
Nadalem	Nadal	k1gInSc7	Nadal
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
porazili	porazit	k5eAaPmAgMnP	porazit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desetkrát	desetkrát	k6eAd1	desetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Soupeření	soupeření	k1gNnSc2	soupeření
Federera	Federero	k1gNnSc2	Federero
a	a	k8xC	a
Roddicka	Roddicko	k1gNnSc2	Roddicko
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholeté	dlouholetý	k2eAgNnSc1d1	dlouholeté
soupeření	soupeření	k1gNnSc1	soupeření
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
sváděli	svádět	k5eAaImAgMnP	svádět
Federer	Federer	k1gMnSc1	Federer
a	a	k8xC	a
Andy	Anda	k1gFnPc1	Anda
Roddick	Roddick	k1gInSc1	Roddick
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
tenista	tenista	k1gMnSc1	tenista
sesadil	sesadit	k5eAaPmAgMnS	sesadit
Američana	Američan	k1gMnSc4	Američan
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
získal	získat	k5eAaPmAgMnS	získat
premiérový	premiérový	k2eAgInSc4d1	premiérový
titul	titul	k1gInSc4	titul
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
střetli	střetnout	k5eAaPmAgMnP	střetnout
ve	v	k7c6	v
24	[number]	k4	24
zápasech	zápas	k1gInPc6	zápas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
čtyř	čtyři	k4xCgNnPc2	čtyři
finále	finále	k1gNnPc2	finále
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
-	-	kIx~	-
tří	tři	k4xCgInPc2	tři
wimbledonských	wimbledonský	k2eAgInPc2d1	wimbledonský
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
výrazně	výrazně	k6eAd1	výrazně
aktivní	aktivní	k2eAgFnSc2d1	aktivní
bilance	bilance	k1gFnSc2	bilance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
Roddick	Roddick	k1gInSc1	Roddick
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
stal	stát	k5eAaPmAgMnS	stát
hráčem	hráč	k1gMnSc7	hráč
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
porážek	porážka	k1gFnPc2	porážka
od	od	k7c2	od
Federera	Federero	k1gNnSc2	Federero
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
sedmnáct	sedmnáct	k4xCc4	sedmnáct
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
poslední	poslední	k2eAgFnSc1d1	poslední
pátá	pátý	k4xOgFnSc1	pátý
sada	sada	k1gFnSc1	sada
95	[number]	k4	95
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
130	[number]	k4	130
<g/>
leté	letý	k2eAgFnSc6d1	letá
historii	historie	k1gFnSc6	historie
finále	finále	k1gNnSc2	finále
mužského	mužský	k2eAgMnSc4d1	mužský
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
Překonala	překonat	k5eAaPmAgFnS	překonat
tak	tak	k9	tak
počtem	počet	k1gInSc7	počet
her	hra	k1gFnPc2	hra
i	i	k9	i
délkou	délka	k1gFnSc7	délka
pátý	pátý	k4xOgInSc4	pátý
set	set	k1gInSc4	set
finále	finále	k1gNnSc2	finále
na	na	k7c4	na
French	French	k1gInSc4	French
Championships	Championships	k1gInSc4	Championships
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Američan	Američan	k1gMnSc1	Američan
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
pětisetového	pětisetový	k2eAgNnSc2d1	Pětisetové
utkání	utkání	k1gNnSc2	utkání
ztratil	ztratit	k5eAaPmAgMnS	ztratit
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
podání	podání	k1gNnSc4	podání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
v	v	k7c6	v
úplně	úplně	k6eAd1	úplně
posledním	poslední	k2eAgInSc6d1	poslední
gamu	game	k1gInSc6	game
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
setu	set	k1gInSc2	set
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
tímto	tento	k3xDgInSc7	tento
triumfem	triumf	k1gInSc7	triumf
získal	získat	k5eAaPmAgInS	získat
patnáctý	patnáctý	k4xOgInSc1	patnáctý
grandslamový	grandslamový	k2eAgInSc1d1	grandslamový
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
překonal	překonat	k5eAaPmAgMnS	překonat
historicky	historicky	k6eAd1	historicky
rekordní	rekordní	k2eAgInSc4d1	rekordní
zápis	zápis	k1gInSc4	zápis
Peta	Petum	k1gNnSc2	Petum
Samprase	Samprasa	k1gFnSc3	Samprasa
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
a	a	k8xC	a
Marat	Marat	k2eAgInSc4d1	Marat
Safin	Safin	k1gInSc4	Safin
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
ke	k	k7c3	k
dvanácti	dvanáct	k4xCc2	dvanáct
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
střetnutím	střetnutí	k1gNnSc7	střetnutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
měl	mít	k5eAaImAgInS	mít
výrazně	výrazně	k6eAd1	výrazně
aktivní	aktivní	k2eAgFnSc4d1	aktivní
bilanci	bilance	k1gFnSc4	bilance
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
profesionály	profesionál	k1gMnPc7	profesionál
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
jediného	jediný	k2eAgInSc2d1	jediný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Safin	Safin	k1gMnSc1	Safin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ukončil	ukončit	k5eAaPmAgMnS	ukončit
aktivní	aktivní	k2eAgFnSc4d1	aktivní
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
vkročil	vkročit	k5eAaPmAgMnS	vkročit
do	do	k7c2	do
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
a	a	k8xC	a
Federer	Federer	k1gMnSc1	Federer
získal	získat	k5eAaPmAgMnS	získat
status	status	k1gInSc4	status
profesionála	profesionál	k1gMnSc4	profesionál
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
bilanci	bilance	k1gFnSc4	bilance
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ruský	ruský	k2eAgMnSc1d1	ruský
tenista	tenista	k1gMnSc1	tenista
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
lepšího	dobrý	k2eAgInSc2d2	lepší
poměru	poměr	k1gInSc2	poměr
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
na	na	k7c6	na
koberci	koberec	k1gInSc6	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
utkali	utkat	k5eAaPmAgMnP	utkat
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
slavil	slavit	k5eAaImAgMnS	slavit
Švýcar	Švýcar	k1gMnSc1	Švýcar
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
střetnutím	střetnutí	k1gNnSc7	střetnutí
se	se	k3xPyFc4	se
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
finále	finále	k1gNnSc7	finále
na	na	k7c4	na
Hamburg	Hamburg	k1gInSc4	Hamburg
Masters	Masters	k1gInSc1	Masters
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
basilejský	basilejský	k2eAgMnSc1d1	basilejský
rodák	rodák	k1gMnSc1	rodák
připsal	připsat	k5eAaPmAgMnS	připsat
premiérový	premiérový	k2eAgInSc4d1	premiérový
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k1gNnSc7	další
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
těsná	těsný	k2eAgFnSc1d1	těsná
semifinálová	semifinálový	k2eAgFnSc1d1	semifinálová
výhra	výhra	k1gFnSc1	výhra
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
2004	[number]	k4	2004
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
v	v	k7c6	v
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
tiebreaku	tiebreak	k1gInSc6	tiebreak
druhé	druhý	k4xOgFnSc2	druhý
sady	sada	k1gFnSc2	sada
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
proměnil	proměnit	k5eAaPmAgMnS	proměnit
až	až	k6eAd1	až
osmý	osmý	k4xOgInSc4	osmý
mečbol	mečbol	k1gInSc4	mečbol
a	a	k8xC	a
následně	následně	k6eAd1	následně
obhájil	obhájit	k5eAaPmAgMnS	obhájit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gMnSc1	Federer
také	také	k9	také
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gInSc1	Open
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgFnSc4	první
trofej	trofej	k1gFnSc4	trofej
z	z	k7c2	z
melbournském	melbournský	k2eAgMnSc6d1	melbournský
majoru	major	k1gMnSc6	major
<g/>
.	.	kIx.	.
</s>
<s>
Safinovi	Safin	k1gMnSc3	Safin
naopak	naopak	k6eAd1	naopak
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gInSc1	Open
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
hře	hra	k1gFnSc6	hra
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sady	sada	k1gFnSc2	sada
neproměnil	proměnit	k5eNaPmAgInS	proměnit
mečbol	mečbol	k1gInSc4	mečbol
a	a	k8xC	a
Rus	Rus	k1gMnSc1	Rus
poté	poté	k6eAd1	poté
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jeho	jeho	k3xOp3gNnSc4	jeho
24	[number]	k4	24
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
sérii	série	k1gFnSc4	série
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Federerovu	Federerův	k2eAgFnSc4d1	Federerova
všestrannost	všestrannost	k1gFnSc4	všestrannost
shrnula	shrnout	k5eAaPmAgFnS	shrnout
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Jimmy	Jimma	k1gFnSc2	Jimma
Connors	Connors	k1gInSc4	Connors
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
specialistů	specialista	k1gMnPc2	specialista
jste	být	k5eAaImIp2nP	být
buď	buď	k8xC	buď
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
antuku	antuka	k1gFnSc4	antuka
<g/>
,	,	kIx,	,
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
trávu	tráva	k1gFnSc4	tráva
nebo	nebo	k8xC	nebo
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
povrch	povrch	k1gInSc4	povrch
...	...	k?	...
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jste	být	k5eAaImIp2nP	být
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
površích	povrch	k1gInPc6	povrch
komplexním	komplexní	k2eAgInSc7d1	komplexní
hráčem	hráč	k1gMnSc7	hráč
vyčnívajícím	vyčnívající	k2eAgMnSc7d1	vyčnívající
především	především	k6eAd1	především
díky	dík	k1gInPc1	dík
své	své	k1gNnSc1	své
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
proměnlivému	proměnlivý	k2eAgInSc3d1	proměnlivý
stylu	styl	k1gInSc3	styl
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
výjimečným	výjimečný	k2eAgInPc3d1	výjimečný
úderům	úder	k1gInPc3	úder
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgInSc1d1	herní
styl	styl	k1gInSc1	styl
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
úderů	úder	k1gInPc2	úder
praktikovaných	praktikovaný	k2eAgInPc2d1	praktikovaný
převážně	převážně	k6eAd1	převážně
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
čáry	čára	k1gFnSc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Nadto	nadto	k6eAd1	nadto
využívá	využívat	k5eAaPmIp3nS	využívat
přechodu	přechod	k1gInSc2	přechod
a	a	k8xC	a
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
když	když	k8xS	když
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
tenistů	tenista	k1gMnPc2	tenista
s	s	k7c7	s
volejovou	volejový	k2eAgFnSc7d1	volejový
technikou	technika	k1gFnSc7	technika
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Univerzálnost	univerzálnost	k1gFnSc4	univerzálnost
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
prudké	prudký	k2eAgInPc1d1	prudký
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
umístěné	umístěný	k2eAgInPc1d1	umístěný
smeče	smeč	k1gInPc1	smeč
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
precizně	precizně	k6eAd1	precizně
zvládnuté	zvládnutý	k2eAgInPc1d1	zvládnutý
obtížné	obtížný	k2eAgInPc1d1	obtížný
prvky	prvek	k1gInPc1	prvek
moderního	moderní	k2eAgInSc2d1	moderní
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
bekhendová	bekhendový	k2eAgFnSc1d1	bekhendová
smeč	smeč	k1gFnSc1	smeč
<g/>
,	,	kIx,	,
halfvolej	halfvolej	k1gInSc1	halfvolej
a	a	k8xC	a
smeč	smeč	k1gFnSc1	smeč
ve	v	k7c6	v
výskoku	výskok	k1gInSc6	výskok
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Foster	Foster	k1gMnSc1	Foster
Wallace	Wallace	k1gFnSc2	Wallace
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
jeho	jeho	k3xOp3gFnSc4	jeho
hrubou	hrubý	k2eAgFnSc4d1	hrubá
sílu	síla	k1gFnSc4	síla
uplatňovanou	uplatňovaný	k2eAgFnSc4d1	uplatňovaná
při	při	k7c6	při
forhendovém	forhendový	k2eAgInSc6d1	forhendový
pohybu	pohyb	k1gInSc6	pohyb
k	k	k7c3	k
"	"	kIx"	"
<g/>
mocnému	mocný	k2eAgNnSc3d1	mocné
plynulému	plynulý	k2eAgNnSc3d1	plynulé
švihnutí	švihnutí	k1gNnSc3	švihnutí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
John	John	k1gMnSc1	John
McEnroe	McEnroe	k1gInSc1	McEnroe
nazvala	nazvat	k5eAaBmAgFnS	nazvat
tenistův	tenistův	k2eAgInSc4d1	tenistův
forhend	forhend	k1gInSc4	forhend
"	"	kIx"	"
<g/>
největším	veliký	k2eAgInSc7d3	veliký
úderem	úder	k1gInSc7	úder
našeho	náš	k3xOp1gInSc2	náš
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Federer	Federer	k1gInSc1	Federer
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
také	také	k9	také
svým	svůj	k3xOyFgInSc7	svůj
efektivním	efektivní	k2eAgInSc7d1	efektivní
pohybem	pohyb	k1gInSc7	pohyb
po	po	k7c6	po
dvorci	dvorec	k1gInSc6	dvorec
a	a	k8xC	a
výbornou	výborný	k2eAgFnSc7d1	výborná
prací	prací	k2eAgFnSc7d1	prací
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
obíhat	obíhat	k5eAaImF	obíhat
soupeřovy	soupeřův	k2eAgInPc4d1	soupeřův
údery	úder	k1gInPc4	úder
směřující	směřující	k2eAgNnSc1d1	směřující
do	do	k7c2	do
bekhendové	bekhendový	k2eAgFnSc2d1	bekhendová
poloviny	polovina	k1gFnSc2	polovina
kurtu	kurt	k1gInSc2	kurt
a	a	k8xC	a
následně	následně	k6eAd1	následně
odpovídat	odpovídat	k5eAaImF	odpovídat
silným	silný	k2eAgInSc7d1	silný
forhendem	forhend	k1gInSc7	forhend
hraným	hraný	k2eAgInSc7d1	hraný
křižně	křižně	k6eAd1	křižně
nebo	nebo	k8xC	nebo
po	po	k7c6	po
lajně	lajna	k1gFnSc6	lajna
<g/>
.	.	kIx.	.
</s>
<s>
Obíhaný	obíhaný	k2eAgInSc1d1	obíhaný
bekhend	bekhend	k1gInSc1	bekhend
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
Švýcarových	Švýcarův	k2eAgFnPc2d1	Švýcarova
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dostat	dostat	k5eAaPmF	dostat
soupeře	soupeř	k1gMnPc4	soupeř
pod	pod	k7c4	pod
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
rychlým	rychlý	k2eAgInSc7d1	rychlý
dobře	dobře	k6eAd1	dobře
umístěným	umístěný	k2eAgInSc7d1	umístěný
míčem	míč	k1gInSc7	míč
protivníka	protivník	k1gMnSc2	protivník
donutit	donutit	k5eAaPmF	donutit
k	k	k7c3	k
chybě	chyba	k1gFnSc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
používá	používat	k5eAaImIp3nS	používat
jednoručnou	jednoručný	k2eAgFnSc4d1	jednoručný
variantu	varianta	k1gFnSc4	varianta
bekhendu	bekhend	k1gInSc2	bekhend
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
volí	volit	k5eAaImIp3nS	volit
čopovaný	čopovaný	k2eAgInSc4d1	čopovaný
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
protihráče	protihráč	k1gMnSc2	protihráč
přilákat	přilákat	k5eAaPmF	přilákat
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
a	a	k8xC	a
následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
prohodit	prohodit	k5eAaPmF	prohodit
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
urychlení	urychlení	k1gNnSc1	urychlení
hry	hra	k1gFnSc2	hra
topspinem	topspin	k1gInSc7	topspin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
z	z	k7c2	z
bekhendu	bekhend	k1gInSc2	bekhend
znamenat	znamenat	k5eAaImF	znamenat
vítězný	vítězný	k2eAgInSc4d1	vítězný
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
úderu	úder	k1gInSc6	úder
uplatňovaném	uplatňovaný	k2eAgMnSc6d1	uplatňovaný
také	také	k6eAd1	také
při	při	k7c6	při
prohozech	prohoz	k1gInPc6	prohoz
využívá	využívat	k5eAaPmIp3nS	využívat
švihu	švih	k1gInSc3	švih
v	v	k7c6	v
ohebném	ohebný	k2eAgNnSc6d1	ohebné
zápěstí	zápěstí	k1gNnSc6	zápěstí
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
míči	míč	k1gInSc3	míč
udělí	udělit	k5eAaPmIp3nP	udělit
vyšší	vysoký	k2eAgFnSc4d2	vyšší
horní	horní	k2eAgFnSc4d1	horní
rotaci	rotace	k1gFnSc4	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
podání	podání	k1gNnSc1	podání
je	být	k5eAaImIp3nS	být
obtížně	obtížně	k6eAd1	obtížně
čitelné	čitelný	k2eAgNnSc1d1	čitelné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nadhazovat	nadhazovat	k5eAaImF	nadhazovat
míč	míč	k1gInSc1	míč
vždy	vždy	k6eAd1	vždy
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
druh	druh	k1gInSc4	druh
udělené	udělený	k2eAgFnSc2d1	udělená
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
umístění	umístění	k1gNnSc2	umístění
do	do	k7c2	do
dvorce	dvorec	k1gInSc2	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
pohybu	pohyb	k1gInSc2	pohyb
směřují	směřovat	k5eAaImIp3nP	směřovat
záda	záda	k1gNnPc4	záda
vpřed	vpřed	k6eAd1	vpřed
proti	proti	k7c3	proti
soupeři	soupeř	k1gMnSc3	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
získávat	získávat	k5eAaImF	získávat
tzv.	tzv.	kA	tzv.
velké	velký	k2eAgInPc4d1	velký
míče	míč	k1gInPc4	míč
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
často	často	k6eAd1	často
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
utkání	utkání	k1gNnPc1	utkání
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
servisu	servis	k1gInSc2	servis
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
následné	následný	k2eAgFnSc2d1	následná
výměny	výměna	k1gFnSc2	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
prvního	první	k4xOgNnSc2	první
podání	podání	k1gNnSc2	podání
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
míči	míč	k1gInPc7	míč
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
udělit	udělit	k5eAaPmF	udělit
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
220	[number]	k4	220
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rejstříku	rejstřík	k1gInSc6	rejstřík
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
variantu	varianta	k1gFnSc4	varianta
stylu	styl	k1gInSc2	styl
hry	hra	k1gFnSc2	hra
"	"	kIx"	"
<g/>
podání	podání	k1gNnSc1	podání
a	a	k8xC	a
volej	volej	k1gInSc1	volej
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
náběh	náběh	k1gInSc1	náběh
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
po	po	k7c6	po
servisu	servis	k1gInSc6	servis
využíval	využívat	k5eAaPmAgInS	využívat
zejména	zejména	k9	zejména
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
také	také	k9	také
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
překvapivého	překvapivý	k2eAgNnSc2d1	překvapivé
zkrácení	zkrácení	k1gNnSc2	zkrácení
míče	míč	k1gInSc2	míč
se	s	k7c7	s
skrytím	skrytí	k1gNnSc7	skrytí
jeho	on	k3xPp3gInSc2	on
úmyslu	úmysl	k1gInSc2	úmysl
před	před	k7c7	před
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dobíhaní	dobíhaný	k2eAgMnPc1d1	dobíhaný
lobu	lob	k1gInSc3	lob
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
technicky	technicky	k6eAd1	technicky
náročného	náročný	k2eAgInSc2d1	náročný
úderu	úder	k1gInSc2	úder
mezi	mezi	k7c7	mezi
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
odezvu	odezva	k1gFnSc4	odezva
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
případy	případ	k1gInPc4	případ
jeho	jeho	k3xOp3gNnSc2	jeho
zařazení	zařazení	k1gNnSc2	zařazení
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
patří	patřit	k5eAaImIp3nP	patřit
semifinále	semifinále	k1gNnPc1	semifinále
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jím	on	k3xPp3gNnSc7	on
úspěšně	úspěšně	k6eAd1	úspěšně
prohodil	prohodit	k5eAaPmAgMnS	prohodit
Novaka	Novak	k1gMnSc4	Novak
Djokovića	Djokovićus	k1gMnSc4	Djokovićus
a	a	k8xC	a
vítězný	vítězný	k2eAgInSc4d1	vítězný
bod	bod	k1gInSc4	bod
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgMnS	přinést
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
míčem	míč	k1gInSc7	míč
pak	pak	k6eAd1	pak
zápas	zápas	k1gInSc1	zápas
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
setů	set	k1gInPc2	set
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Devítiletého	devítiletý	k2eAgMnSc4d1	devítiletý
Federera	Federer	k1gMnSc4	Federer
začal	začít	k5eAaPmAgMnS	začít
trénovat	trénovat	k5eAaImF	trénovat
Australan	Australan	k1gMnSc1	Australan
Peter	Peter	k1gMnSc1	Peter
Carter	Carter	k1gMnSc1	Carter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
jeho	jeho	k3xOp3gFnSc4	jeho
techniku	technika	k1gFnSc4	technika
hry	hra	k1gFnSc2	hra
i	i	k8xC	i
chování	chování	k1gNnSc2	chování
na	na	k7c6	na
dvorci	dvorec	k1gInSc6	dvorec
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgInS	přivést
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
ukončili	ukončit	k5eAaPmAgMnP	ukončit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
hráčových	hráčův	k2eAgNnPc2d1	hráčovo
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
deseti	deset	k4xCc2	deset
roků	rok	k1gInPc2	rok
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
na	na	k7c6	na
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
týdenních	týdenní	k2eAgInPc6d1	týdenní
trénincích	trénink	k1gInPc6	trénink
v	v	k7c6	v
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc2	boy
Tennis	Tennis	k1gFnPc2	Tennis
Clubu	club	k1gInSc2	club
podílel	podílet	k5eAaImAgInS	podílet
také	také	k9	také
soukromý	soukromý	k2eAgMnSc1d1	soukromý
kouč	kouč	k1gMnSc1	kouč
Adolf	Adolf	k1gMnSc1	Adolf
Kacovsky	Kacovsko	k1gNnPc7	Kacovsko
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
později	pozdě	k6eAd2	pozdě
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Okamžitě	okamžitě	k6eAd1	okamžitě
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tenhle	tenhle	k3xDgMnSc1	tenhle
kluk	kluk	k1gMnSc1	kluk
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
s	s	k7c7	s
raketou	raketa	k1gFnSc7	raketa
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
Federer	Federra	k1gFnPc2	Federra
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
švýcarské	švýcarský	k2eAgNnSc4d1	švýcarské
mistrovství	mistrovství	k1gNnSc4	mistrovství
žactva	žactvo	k1gNnSc2	žactvo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
Švýcarského	švýcarský	k2eAgNnSc2d1	švýcarské
národního	národní	k2eAgNnSc2d1	národní
tenisového	tenisový	k2eAgNnSc2d1	tenisové
centra	centrum	k1gNnSc2	centrum
v	v	k7c4	v
Écublens	Écublens	k1gInSc4	Écublens
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahájil	zahájit	k5eAaPmAgMnS	zahájit
soustavný	soustavný	k2eAgInSc4d1	soustavný
trénink	trénink	k1gInSc4	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Carterem	Carter	k1gInSc7	Carter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
18	[number]	k4	18
<g/>
letého	letý	k2eAgMnSc2d1	letý
Švýcara	Švýcar	k1gMnSc2	Švýcar
stal	stát	k5eAaPmAgMnS	stát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
švédský	švédský	k2eAgMnSc1d1	švédský
tenista	tenista	k1gMnSc1	tenista
Peter	Peter	k1gMnSc1	Peter
Lundgren	Lundgrna	k1gFnPc2	Lundgrna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
u	u	k7c2	u
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
juniory	junior	k1gMnPc7	junior
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
období	období	k1gNnSc6	období
2000	[number]	k4	2000
až	až	k9	až
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
Federer	Federer	k1gInSc1	Federer
bez	bez	k7c2	bez
kouče	kouč	k1gMnSc2	kouč
až	až	k9	až
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
úvodu	úvod	k1gInSc6	úvod
navázal	navázat	k5eAaPmAgMnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c4	na
částečný	částečný	k2eAgInSc4d1	částečný
úvazek	úvazek	k1gInSc4	úvazek
se	s	k7c7	s
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
Australanem	Australan	k1gMnSc7	Australan
Tonym	Tony	k1gMnSc7	Tony
Rochem	Roch	k1gInSc7	Roch
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
gentlemanskou	gentlemanský	k2eAgFnSc4d1	gentlemanská
dohodu	dohoda	k1gFnSc4	dohoda
podáním	podání	k1gNnSc7	podání
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Roche	Roche	k1gInSc1	Roche
byl	být	k5eAaImAgInS	být
vyplácen	vyplácet	k5eAaImNgInS	vyplácet
v	v	k7c6	v
týdenních	týdenní	k2eAgInPc6d1	týdenní
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
jej	on	k3xPp3gMnSc4	on
najal	najmout	k5eAaPmAgMnS	najmout
z	z	k7c2	z
opačných	opačný	k2eAgInPc2d1	opačný
důvodů	důvod	k1gInPc2	důvod
než	než	k8xS	než
kdysi	kdysi	k6eAd1	kdysi
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
zisk	zisk	k1gInSc4	zisk
wimbledonského	wimbledonský	k2eAgInSc2d1	wimbledonský
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Basilejský	basilejský	k2eAgMnSc1d1	basilejský
rodák	rodák	k1gMnSc1	rodák
požadoval	požadovat	k5eAaImAgMnS	požadovat
naopak	naopak	k6eAd1	naopak
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
hru	hra	k1gFnSc4	hra
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
ukončili	ukončit	k5eAaPmAgMnP	ukončit
k	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Rochem	Roch	k1gInSc7	Roch
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2007	[number]	k4	2007
Federer	Federra	k1gFnPc2	Federra
nehrajícího	hrající	k2eNgMnSc2d1	nehrající
kapitána	kapitán	k1gMnSc2	kapitán
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
daviscupového	daviscupový	k2eAgInSc2d1	daviscupový
týmu	tým	k1gInSc2	tým
Severina	Severin	k1gMnSc4	Severin
Lüthiho	Lüthi	k1gMnSc4	Lüthi
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
najal	najmout	k5eAaPmAgMnS	najmout
na	na	k7c4	na
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
koučování	koučování	k1gNnSc1	koučování
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
do	do	k7c2	do
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
vztahu	vztah	k1gInSc2	vztah
a	a	k8xC	a
Lüthi	Lüth	k1gFnSc2	Lüth
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
tenistu	tenista	k1gMnSc4	tenista
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
k	k	k7c3	k
navázání	navázání	k1gNnSc3	navázání
spolupráce	spolupráce	k1gFnSc2	spolupráce
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Před	před	k7c7	před
pěti	pět	k4xCc7	pět
lety	léto	k1gNnPc7	léto
mě	já	k3xPp1nSc4	já
Roger	Roger	k1gMnSc1	Roger
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
bych	by	kYmCp1nS	by
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
nepřijel	přijet	k5eNaPmAgMnS	přijet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
Tonym	Tony	k1gMnSc7	Tony
Rochem	Roch	k1gInSc7	Roch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
spolupráce	spolupráce	k1gFnSc2	spolupráce
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
studovat	studovat	k5eAaImF	studovat
soupeře	soupeř	k1gMnSc4	soupeř
Federera	Federer	k1gMnSc4	Federer
a	a	k8xC	a
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
občasného	občasný	k2eAgMnSc4d1	občasný
sparingpartnera	sparingpartner	k1gMnSc4	sparingpartner
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezón	sezóna	k1gFnPc2	sezóna
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
s	s	k7c7	s
tenistou	tenista	k1gMnSc7	tenista
strávil	strávit	k5eAaPmAgMnS	strávit
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
týdnů	týden	k1gInPc2	týden
cestování	cestování	k1gNnSc2	cestování
po	po	k7c6	po
turnajích	turnaj	k1gInPc6	turnaj
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
světové	světový	k2eAgFnSc3d1	světová
jedničce	jednička	k1gFnSc3	jednička
stále	stále	k6eAd1	stále
unikal	unikat	k5eAaImAgInS	unikat
Pohár	pohár	k1gInSc4	pohár
Mušketýrů	mušketýr	k1gMnPc2	mušketýr
<g/>
,	,	kIx,	,
najala	najmout	k5eAaPmAgFnS	najmout
si	se	k3xPyFc3	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
před	před	k7c7	před
Estoril	Estorila	k1gFnPc2	Estorila
Open	Open	k1gNnSc1	Open
2008	[number]	k4	2008
Španěla	Španěl	k1gMnSc4	Španěl
Josého	Josý	k1gMnSc4	Josý
Higuerase	Higuerasa	k1gFnSc6	Higuerasa
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
antukovou	antukový	k2eAgFnSc4d1	antuková
část	část	k1gFnSc4	část
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Koučování	koučování	k1gNnSc1	koučování
bylo	být	k5eAaImAgNnS	být
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
září	září	k1gNnSc1	září
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
2010	[number]	k4	2010
pak	pak	k9	pak
Švýcara	Švýcar	k1gMnSc4	Švýcar
dále	daleko	k6eAd2	daleko
herně	herna	k1gFnSc6	herna
připravoval	připravovat	k5eAaImAgMnS	připravovat
Lüthi	Lüthe	k1gFnSc4	Lüthe
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2010	[number]	k4	2010
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
Američan	Američan	k1gMnSc1	Američan
Paul	Paul	k1gMnSc1	Paul
Annacone	Annacon	k1gMnSc5	Annacon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měsíční	měsíční	k2eAgFnSc6d1	měsíční
zkušební	zkušební	k2eAgFnSc6d1	zkušební
době	doba	k1gFnSc6	doba
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
Federer	Federer	k1gInSc1	Federer
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
rekordní	rekordní	k2eAgInSc4d1	rekordní
17	[number]	k4	17
<g/>
.	.	kIx.	.
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trénování	trénování	k1gNnSc4	trénování
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
stále	stále	k6eAd1	stále
také	také	k9	také
Lüthi	Lüth	k1gFnSc2	Lüth
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nejhorší	zlý	k2eAgFnSc6d3	nejhorší
Federerově	Federerův	k2eAgFnSc6d1	Federerova
sezóně	sezóna	k1gFnSc6	sezóna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Švýcar	Švýcar	k1gMnSc1	Švýcar
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
grandslam	grandslam	k1gInSc4	grandslam
<g/>
,	,	kIx,	,
ukončil	ukončit	k5eAaPmAgMnS	ukončit
tenista	tenista	k1gMnSc1	tenista
s	s	k7c7	s
Annaconem	Annacon	k1gMnSc7	Annacon
spolupráci	spolupráce	k1gFnSc4	spolupráce
k	k	k7c3	k
říjnu	říjen	k1gInSc3	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
pak	pak	k6eAd1	pak
tenisovým	tenisový	k2eAgMnSc7d1	tenisový
trenérem	trenér	k1gMnSc7	trenér
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Severin	Severin	k1gMnSc1	Severin
Lüthi	Lüth	k1gFnSc2	Lüth
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
stránek	stránka	k1gFnPc2	stránka
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
původně	původně	k6eAd1	původně
jen	jen	k9	jen
na	na	k7c4	na
10	[number]	k4	10
<g/>
týdenní	týdenní	k2eAgFnSc4d1	týdenní
období	období	k1gNnSc3	období
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgMnSc7	druhý
trenérem	trenér	k1gMnSc7	trenér
stala	stát	k5eAaPmAgFnS	stát
bývalá	bývalý	k2eAgFnSc1d1	bývalá
švédská	švédský	k2eAgFnSc1d1	švédská
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Stefan	Stefan	k1gMnSc1	Stefan
Edberg	Edberg	k1gMnSc1	Edberg
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
k	k	k7c3	k
oznámení	oznámení	k1gNnSc3	oznámení
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
velmi	velmi	k6eAd1	velmi
úspěšných	úspěšný	k2eAgInPc6d1	úspěšný
letech	let	k1gInPc6	let
bych	by	kYmCp1nS	by
rád	rád	k2eAgMnSc1d1	rád
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
mému	můj	k1gMnSc3	můj
dětskému	dětský	k2eAgInSc3d1	dětský
idolu	idol	k1gInSc6	idol
Stefanu	Stefan	k1gMnSc3	Stefan
Edbergovi	Edberg	k1gMnSc3	Edberg
za	za	k7c4	za
ochotu	ochota	k1gFnSc4	ochota
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
mému	můj	k3xOp1gInSc3	můj
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
splněný	splněný	k2eAgInSc1d1	splněný
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
koučem	kouč	k1gMnSc7	kouč
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Lüthi	Lüthe	k1gFnSc4	Lüthe
a	a	k8xC	a
Švédovu	Švédův	k2eAgFnSc4d1	Švédova
roli	role	k1gFnSc4	role
převzal	převzít	k5eAaPmAgMnS	převzít
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2016	[number]	k4	2016
bývalý	bývalý	k2eAgMnSc1d1	bývalý
třetí	třetí	k4xOgMnSc1	třetí
hráč	hráč	k1gMnSc1	hráč
světa	svět	k1gInSc2	svět
Ivan	Ivan	k1gMnSc1	Ivan
Ljubičić	Ljubičić	k1gMnSc1	Ljubičić
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
fyzioterapeuta	fyzioterapeut	k1gMnSc2	fyzioterapeut
působí	působit	k5eAaImIp3nS	působit
Daniel	Daniel	k1gMnSc1	Daniel
Troxler	Troxler	k1gMnSc1	Troxler
(	(	kIx(	(
<g/>
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Stephane	Stephan	k1gMnSc5	Stephan
Vivie	Vivius	k1gMnSc5	Vivius
<g/>
)	)	kIx)	)
a	a	k8xC	a
kondiční	kondiční	k2eAgFnSc4d1	kondiční
přípravu	příprava	k1gFnSc4	příprava
vede	vést	k5eAaImIp3nS	vést
Pierre	Pierr	k1gInSc5	Pierr
Paganini	Paganin	k2eAgMnPc5d1	Paganin
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Carter	Carter	k1gMnSc1	Carter
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
Peter	Petra	k1gFnPc2	Petra
Lundgren	Lundgrna	k1gFnPc2	Lundgrna
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
Tony	Tony	k1gMnSc1	Tony
Roche	Roch	k1gInSc2	Roch
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
Severin	Severin	k1gMnSc1	Severin
Lüthi	Lüthe	k1gFnSc4	Lüthe
<g/>
,	,	kIx,	,
od	od	k7c2	od
2007	[number]	k4	2007
José	José	k1gNnPc2	José
Higueras	Higuerasa	k1gFnPc2	Higuerasa
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Paul	Paula	k1gFnPc2	Paula
Annacone	Annacon	k1gInSc5	Annacon
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
Stefan	Stefan	k1gMnSc1	Stefan
Edberg	Edberg	k1gMnSc1	Edberg
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
Ivan	Ivan	k1gMnSc1	Ivan
Ljubičić	Ljubičić	k1gMnSc1	Ljubičić
<g/>
,	,	kIx,	,
od	od	k7c2	od
2016	[number]	k4	2016
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
hrál	hrát	k5eAaImAgMnS	hrát
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
s	s	k7c7	s
uzpůsobenou	uzpůsobený	k2eAgFnSc7d1	uzpůsobená
raketou	raketa	k1gFnSc7	raketa
značky	značka	k1gFnSc2	značka
Wilson	Wilson	k1gInSc1	Wilson
-	-	kIx~	-
Pro	pro	k7c4	pro
Staff	Staff	k1gInSc4	Staff
6.1	[number]	k4	6.1
90	[number]	k4	90
BLX	BLX	kA	BLX
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
menší	malý	k2eAgFnSc7d2	menší
úderovou	úderový	k2eAgFnSc7d1	úderová
plochou	plocha	k1gFnSc7	plocha
hlavy	hlava	k1gFnSc2	hlava
o	o	k7c6	o
povrchu	povrch	k1gInSc6	povrch
581	[number]	k4	581
cm	cm	kA	cm
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
"	"	kIx"	"
<g/>
midsize	midsize	k1gFnSc1	midsize
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
364	[number]	k4	364
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
heavy	heava	k1gFnSc2	heava
<g/>
"	"	kIx"	"
nad	nad	k7c4	nad
320	[number]	k4	320
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
rámu	rám	k1gInSc2	rám
činí	činit	k5eAaImIp3nS	činit
17,5	[number]	k4	17,5
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
držadla	držadlo	k1gNnSc2	držadlo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvodu	obvod	k1gInSc2	obvod
111	[number]	k4	111
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
typ	typ	k1gInSc1	typ
L	L	kA	L
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Podélné	podélný	k2eAgFnPc1d1	podélná
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
vypleteny	vypleten	k2eAgInPc1d1	vypleten
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
závaží	závaží	k1gNnSc2	závaží
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
21,5	[number]	k4	21,5
kg	kg	kA	kg
a	a	k8xC	a
příčné	příčný	k2eAgFnPc1d1	příčná
pak	pak	k6eAd1	pak
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
závaží	závaží	k1gNnSc2	závaží
20	[number]	k4	20
kg	kg	kA	kg
s	s	k7c7	s
přednapnutým	přednapnutý	k2eAgNnSc7d1	přednapnutý
vláknem	vlákno	k1gNnSc7	vlákno
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podélné	podélný	k2eAgFnPc1d1	podélná
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
přírodního	přírodní	k2eAgNnSc2d1	přírodní
střeva	střevo	k1gNnSc2	střevo
Wilson	Wilson	k1gInSc1	Wilson
typu	typ	k1gInSc2	typ
16	[number]	k4	16
a	a	k8xC	a
příčné	příčný	k2eAgFnPc1d1	příčná
pak	pak	k6eAd1	pak
umělé	umělý	k2eAgFnPc1d1	umělá
z	z	k7c2	z
polyesteru	polyester	k1gInSc2	polyester
Luxilon	Luxilon	k1gInSc1	Luxilon
Big	Big	k1gMnSc1	Big
Banger	Banger	k1gMnSc1	Banger
ALU	ala	k1gFnSc4	ala
Power	Power	k1gMnSc1	Power
Rough	Rough	k1gMnSc1	Rough
16	[number]	k4	16
<g/>
L.	L.	kA	L.
K	k	k7c3	k
míře	míra	k1gFnSc3	míra
napnutí	napnutí	k1gNnSc2	napnutí
strun	struna	k1gFnPc2	struna
Federer	Federer	k1gMnSc1	Federer
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
druhem	druh	k1gInSc7	druh
míčů	míč	k1gInPc2	míč
hraji	hrát	k5eAaImIp1nS	hrát
a	a	k8xC	a
proti	proti	k7c3	proti
komu	kdo	k3yQnSc3	kdo
hraji	hrát	k5eAaImIp1nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
vidíte	vidět	k5eAaImIp2nP	vidět
-	-	kIx~	-
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
několika	několik	k4yIc6	několik
okolnostech	okolnost	k1gFnPc6	okolnost
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
;	;	kIx,	;
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
také	také	k9	také
můj	můj	k3xOp1gInSc4	můj
vlastní	vlastní	k2eAgInSc4d1	vlastní
pocit	pocit	k1gInSc4	pocit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tenista	tenista	k1gMnSc1	tenista
má	mít	k5eAaImIp3nS	mít
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
obchodní	obchodní	k2eAgFnSc4d1	obchodní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Nike	Nike	k1gFnSc2	Nike
na	na	k7c4	na
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
obuv	obuv	k1gFnSc4	obuv
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
značku	značka	k1gFnSc4	značka
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2013	[number]	k4	2013
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
v	v	k7c6	v
triku	trik	k1gInSc6	trik
Nike	Nike	k1gFnSc2	Nike
Fall	Fall	k1gMnSc1	Fall
Premier	Premier	k1gMnSc1	Premier
RF	RF	kA	RF
Polo	polo	k6eAd1	polo
a	a	k8xC	a
tenisové	tenisový	k2eAgFnSc2d1	tenisová
obuvi	obuv	k1gFnSc2	obuv
Nike	Nike	k1gFnPc2	Nike
Zoom	Zoom	k1gMnSc1	Zoom
Vapor	Vapor	k1gMnSc1	Vapor
9	[number]	k4	9
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
2006	[number]	k4	2006
mu	on	k3xPp3gNnSc3	on
společnost	společnost	k1gFnSc1	společnost
Nike	Nike	k1gFnSc2	Nike
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
speciálně	speciálně	k6eAd1	speciálně
střižené	střižený	k2eAgNnSc4d1	střižené
bílé	bílý	k2eAgNnSc4d1	bílé
sako	sako	k1gNnSc4	sako
s	s	k7c7	s
erbem	erb	k1gInSc7	erb
tří	tři	k4xCgFnPc2	tři
tenisových	tenisový	k2eAgFnPc2d1	tenisová
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
tři	tři	k4xCgInPc1	tři
wimbledonské	wimbledonský	k2eAgInPc1d1	wimbledonský
tituly	titul	k1gInPc1	titul
získané	získaný	k2eAgInPc1d1	získaný
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
saku	sako	k1gNnSc6	sako
pak	pak	k6eAd1	pak
přicházel	přicházet	k5eAaImAgInS	přicházet
na	na	k7c4	na
dvorec	dvorec	k1gInSc4	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
2007	[number]	k4	2007
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
trofejí	trofej	k1gFnSc7	trofej
přibyla	přibýt	k5eAaPmAgFnS	přibýt
do	do	k7c2	do
znaku	znak	k1gInSc2	znak
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
raketa	raketa	k1gFnSc1	raketa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trendu	trend	k1gInSc6	trend
používání	používání	k1gNnSc2	používání
elegantního	elegantní	k2eAgInSc2d1	elegantní
oděvu	oděv	k1gInSc2	oděv
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Nike	Nike	k1gFnSc1	Nike
připravila	připravit	k5eAaPmAgFnS	připravit
vestu	vesta	k1gFnSc4	vesta
šitou	šitý	k2eAgFnSc4d1	šitá
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vlastní	vlastní	k2eAgNnSc1d1	vlastní
logo	logo	k1gNnSc1	logo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
propojením	propojení	k1gNnSc7	propojení
jeho	jeho	k3xOp3gFnPc2	jeho
inicál	inicála	k1gFnPc2	inicála
"	"	kIx"	"
<g/>
R	R	kA	R
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
F	F	kA	F
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
měl	mít	k5eAaImAgMnS	mít
zlatě	zlatě	k6eAd1	zlatě
vyšité	vyšitý	k2eAgFnPc4d1	vyšitá
na	na	k7c6	na
sportovním	sportovní	k2eAgNnSc6d1	sportovní
saku	sako	k1gNnSc6	sako
ve	v	k7c6	v
vítězném	vítězný	k2eAgInSc6d1	vítězný
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Nike	Nike	k1gFnSc2	Nike
výsadu	výsada	k1gFnSc4	výsada
iniciál	iniciála	k1gFnPc2	iniciála
na	na	k7c4	na
oblečení	oblečení	k1gNnSc4	oblečení
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgMnPc3	dva
sportovcům	sportovec	k1gMnPc3	sportovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Federerovi	Federer	k1gMnSc3	Federer
a	a	k8xC	a
golfistovi	golfista	k1gMnSc3	golfista
Tigeru	Tiger	k1gMnSc3	Tiger
Woodsovi	Woods	k1gMnSc3	Woods
<g/>
.	.	kIx.	.
</s>
<s>
Federer	Federer	k1gInSc1	Federer
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
vydělávajících	vydělávající	k2eAgMnPc2d1	vydělávající
sportovců	sportovec	k1gMnPc2	sportovec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
řadil	řadit	k5eAaImAgMnS	řadit
na	na	k7c6	na
pátém	pátý	k4xOgNnSc6	pátý
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
nejvýdělečnějších	výdělečný	k2eAgMnPc2d3	nejvýdělečnější
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sezóně	sezóna	k1gFnSc3	sezóna
2016	[number]	k4	2016
mu	on	k3xPp3gMnSc3	on
patřila	patřit	k5eAaImAgFnS	patřit
druhá	druhý	k4xOgFnSc1	druhý
příčka	příčka	k1gFnSc1	příčka
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
žebříčku	žebříček	k1gInSc6	žebříček
tenistů	tenista	k1gMnPc2	tenista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
získali	získat	k5eAaPmAgMnP	získat
nejvíce	hodně	k6eAd3	hodně
finančních	finanční	k2eAgFnPc2d1	finanční
odměn	odměna	k1gFnPc2	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
sesadil	sesadit	k5eAaPmAgMnS	sesadit
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
prémií	prémie	k1gFnPc2	prémie
z	z	k7c2	z
profesionálního	profesionální	k2eAgInSc2d1	profesionální
okruhu	okruh	k1gInSc2	okruh
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
několik	několik	k4yIc1	několik
sponzorských	sponzorský	k2eAgFnPc2d1	sponzorská
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
celkový	celkový	k2eAgInSc4d1	celkový
roční	roční	k2eAgInSc4d1	roční
příjem	příjem	k1gInSc4	příjem
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
smluvním	smluvní	k2eAgMnPc3d1	smluvní
partnerům	partner	k1gMnPc3	partner
na	na	k7c6	na
prezentaci	prezentace	k1gFnSc6	prezentace
výrobků	výrobek	k1gInPc2	výrobek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Nike	Nike	k1gFnSc2	Nike
a	a	k8xC	a
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
firmy	firma	k1gFnSc2	firma
Credit	Credit	k1gFnSc1	Credit
Suisse	Suisse	k1gFnSc1	Suisse
<g/>
,	,	kIx,	,
Rolex	Rolex	k1gInSc1	Rolex
<g/>
,	,	kIx,	,
Lindt	Lindt	k1gInSc1	Lindt
a	a	k8xC	a
Jura	jura	k1gFnSc1	jura
Elektroapparate	Elektroapparat	k1gInSc5	Elektroapparat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
automobilka	automobilka	k1gFnSc1	automobilka
Mercedes-Benz	Mercedes-Benz	k1gInSc1	Mercedes-Benz
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
regionální	regionální	k2eAgInSc4d1	regionální
kontrakt	kontrakt	k1gInSc4	kontrakt
mezi	mezi	k7c7	mezi
tenistou	tenista	k1gMnSc7	tenista
a	a	k8xC	a
filiálkou	filiálka	k1gFnSc7	filiálka
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
China	China	k1gFnSc1	China
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
sponzorské	sponzorský	k2eAgFnPc4d1	sponzorská
smlouvy	smlouva	k1gFnPc4	smlouva
Federer	Federer	k1gInSc1	Federer
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
Nationale	Nationale	k1gMnSc2	Nationale
Suisse	Suiss	k1gMnSc2	Suiss
<g/>
,	,	kIx,	,
Gillette	Gillett	k1gMnSc5	Gillett
<g/>
,	,	kIx,	,
Wilson	Wilson	k1gMnSc1	Wilson
a	a	k8xC	a
Moët	Moët	k1gMnSc1	Moët
&	&	k?	&
Chandon	Chandon	k1gMnSc1	Chandon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
propagoval	propagovat	k5eAaImAgMnS	propagovat
produkty	produkt	k1gInPc4	produkt
firem	firma	k1gFnPc2	firma
NetJets	NetJetsa	k1gFnPc2	NetJetsa
a	a	k8xC	a
Maurice	Maurika	k1gFnSc6	Maurika
Lacroix	Lacroix	k1gInSc1	Lacroix
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hráčské	hráčský	k2eAgFnSc2d1	hráčská
statistiky	statistika	k1gFnSc2	statistika
Rogera	Roger	k1gMnSc2	Roger
Federera	Federer	k1gMnSc2	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
osmdesát	osmdesát	k4xCc4	osmdesát
osm	osm	k4xCc4	osm
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
osmačtyřicetkrát	osmačtyřicetkrát	k6eAd1	osmačtyřicetkrát
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k8xC	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
osm	osm	k4xCc4	osm
trofejí	trofej	k1gFnPc2	trofej
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
šest	šest	k4xCc4	šest
finálových	finálový	k2eAgFnPc2d1	finálová
proher	prohra	k1gFnPc2	prohra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
sedmnácti	sedmnáct	k4xCc2	sedmnáct
singlových	singlový	k2eAgInPc2d1	singlový
titulů	titul	k1gInPc2	titul
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
představuje	představovat	k5eAaImIp3nS	představovat
nejúspěšnějšího	úspěšný	k2eAgMnSc4d3	nejúspěšnější
mužského	mužský	k2eAgMnSc4d1	mužský
tenistu	tenista	k1gMnSc4	tenista
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
jako	jako	k8xS	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
ocenění	ocenění	k1gNnSc4	ocenění
Rogera	Rogero	k1gNnSc2	Rogero
Federera	Federero	k1gNnSc2	Federero
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc1	cena
ATP	atp	kA	atp
a	a	k8xC	a
Mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
ITF	ITF	kA	ITF
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
obdržel	obdržet	k5eAaPmAgMnS	obdržet
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
tenisové	tenisový	k2eAgFnSc2d1	tenisová
kariéry	kariéra	k1gFnSc2	kariéra
sportovní	sportovní	k2eAgNnSc4d1	sportovní
i	i	k8xC	i
další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
desetiletí	desetiletí	k1gNnSc2	desetiletí
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
evropský	evropský	k2eAgMnSc1d1	evropský
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
hráč	hráč	k1gMnSc1	hráč
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Cena	cena	k1gFnSc1	cena
Stefana	Stefan	k1gMnSc4	Stefan
Edberga	Edberg	k1gMnSc4	Edberg
za	za	k7c4	za
sportovní	sportovní	k2eAgNnSc4d1	sportovní
chování	chování	k1gNnSc4	chování
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
humanitář	humanitář	k1gMnSc1	humanitář
roku	rok	k1gInSc2	rok
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Arthura	Arthur	k1gMnSc2	Arthur
Ashe	Ash	k1gMnSc2	Ash
za	za	k7c4	za
lidský	lidský	k2eAgInSc4d1	lidský
čin	čin	k1gInSc4	čin
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
ITF	ITF	kA	ITF
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
juniorů	junior	k1gMnPc2	junior
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
Laureus	Laureus	k1gMnSc1	Laureus
World	World	k1gMnSc1	World
Sports	Sportsa	k1gFnPc2	Sportsa
Awards	Awards	k1gInSc4	Awards
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Union	union	k1gInSc1	union
européenne	européennout	k5eAaPmIp3nS	européennout
de	de	k?	de
la	la	k1gNnSc4	la
presse	presse	k1gFnSc2	presse
sportives	sportives	k1gMnSc1	sportives
(	(	kIx(	(
<g/>
UEPS	UEPS	kA	UEPS
<g/>
)	)	kIx)	)
evropský	evropský	k2eAgMnSc1d1	evropský
sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Eurosport	eurosport	k1gInSc1	eurosport
"	"	kIx"	"
<g/>
šampión	šampión	k1gMnSc1	šampión
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
uděleno	udělit	k5eAaPmNgNnS	udělit
2010	[number]	k4	2010
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
Sportovec	sportovec	k1gMnSc1	sportovec
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
-	-	kIx~	-
Crédit	Crédit	k1gMnSc2	Crédit
Suisse	Suiss	k1gMnSc2	Suiss
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mužský	mužský	k2eAgMnSc1d1	mužský
sportovec	sportovec	k1gMnSc1	sportovec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Švýcar	Švýcary	k1gInPc2	Švýcary
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Schweizer	Schweizer	k1gInSc1	Schweizer
des	des	k1gNnSc1	des
Jahres	Jahres	k1gInSc1	Jahres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
ESPY	ESPY	kA	ESPY
Award	Awarda	k1gFnPc2	Awarda
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mužský	mužský	k2eAgMnSc1d1	mužský
sportovec	sportovec	k1gMnSc1	sportovec
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
mužský	mužský	k2eAgMnSc1d1	mužský
tenista	tenista	k1gMnSc1	tenista
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
International	International	k1gFnSc1	International
Tennis	Tennis	k1gFnSc1	Tennis
Writers	Writers	k1gInSc4	Writers
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ITWA	ITWA	kA	ITWA
<g/>
)	)	kIx)	)
hráč	hráč	k1gMnSc1	hráč
roku	rok	k1gInSc2	rok
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Reuters	Reutersa	k1gFnPc2	Reutersa
International	International	k1gMnPc1	International
-	-	kIx~	-
sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Equipe	Equip	k1gInSc5	Equip
"	"	kIx"	"
<g/>
šampión	šampión	k1gMnSc1	šampión
šampiónů	šampión	k1gMnPc2	šampión
<g/>
"	"	kIx"	"
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
BBC	BBC	kA	BBC
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
sportovní	sportovní	k2eAgFnSc1d1	sportovní
osobnost	osobnost	k1gFnSc1	osobnost
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
rekordů	rekord	k1gInPc2	rekord
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
<g/>
.	.	kIx.	.
</s>
<s>
Přehledy	přehled	k1gInPc1	přehled
uvádějí	uvádět	k5eAaImIp3nP	uvádět
vybrané	vybraný	k2eAgInPc1d1	vybraný
herní	herní	k2eAgInPc1d1	herní
rekordy	rekord	k1gInPc1	rekord
Rogera	Rogero	k1gNnSc2	Rogero
Federera	Federero	k1gNnSc2	Federero
dosažené	dosažený	k2eAgFnSc2d1	dosažená
a	a	k8xC	a
primárně	primárně	k6eAd1	primárně
vztažené	vztažený	k2eAgFnSc2d1	vztažená
k	k	k7c3	k
otevřené	otevřený	k2eAgFnSc3d1	otevřená
éře	éra	k1gFnSc3	éra
světového	světový	k2eAgInSc2d1	světový
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
představuje	představovat	k5eAaImIp3nS	představovat
historický	historický	k2eAgInSc4d1	historický
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
nepřekonaný	překonaný	k2eNgInSc4d1	nepřekonaný
ani	ani	k8xC	ani
v	v	k7c6	v
championships	championships	k6eAd1	championships
éře	éra	k1gFnSc6	éra
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
