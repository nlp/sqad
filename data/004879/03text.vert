<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
laryngotracheitida	laryngotracheitida	k1gFnSc1	laryngotracheitida
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgMnSc1d1	akutní
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
hrabavou	hrabavý	k2eAgFnSc4d1	hrabavá
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
herpes	herpes	k1gInSc1	herpes
virus	virus	k1gInSc1	virus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
veterinárního	veterinární	k2eAgInSc2d1	veterinární
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
nákazu	nákaza	k1gFnSc4	nákaza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podléhá	podléhat	k5eAaImIp3nS	podléhat
hlášení	hlášení	k1gNnSc4	hlášení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nákaze	nákaza	k1gFnSc3	nákaza
je	být	k5eAaImIp3nS	být
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
kur	kur	k1gMnSc1	kur
<g/>
,	,	kIx,	,
krůta	krůta	k1gFnSc1	krůta
<g/>
,	,	kIx,	,
bažanti	bažant	k1gMnPc1	bažant
a	a	k8xC	a
perličky	perlička	k1gFnPc1	perlička
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
není	být	k5eNaImIp3nS	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
zvířat	zvíře	k1gNnPc2	zvíře
kromě	kromě	k7c2	kromě
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
hostitele	hostitel	k1gMnPc4	hostitel
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc1	virus
velmi	velmi	k6eAd1	velmi
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
přímé	přímý	k2eAgFnSc2d1	přímá
nákazy	nákaza	k1gFnSc2	nákaza
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
od	od	k7c2	od
již	již	k6eAd1	již
nemocných	nemocný	k2eAgInPc2d1	nemocný
kusů	kus	k1gInPc2	kus
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
možný	možný	k2eAgInSc1d1	možný
(	(	kIx(	(
<g/>
a	a	k8xC	a
častý	častý	k2eAgInSc1d1	častý
<g/>
)	)	kIx)	)
také	také	k9	také
nepřímý	přímý	k2eNgInSc4d1	nepřímý
přenos	přenos	k1gInSc4	přenos
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
původce	původce	k1gMnSc4	původce
do	do	k7c2	do
hejna	hejno	k1gNnSc2	hejno
pronikne	proniknout	k5eAaPmIp3nS	proniknout
pomocí	pomocí	k7c2	pomocí
kontaminovaného	kontaminovaný	k2eAgNnSc2d1	kontaminované
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
pomůcek	pomůcka	k1gFnPc2	pomůcka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
oděvech	oděv	k1gInPc6	oděv
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
ptáků	pták	k1gMnPc2	pták
virus	virus	k1gInSc1	virus
proniká	pronikat	k5eAaImIp3nS	pronikat
přes	přes	k7c4	přes
sliznice	sliznice	k1gFnPc4	sliznice
dýchacího	dýchací	k2eAgMnSc2d1	dýchací
nebo	nebo	k8xC	nebo
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
spojivku	spojivka	k1gFnSc4	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
5-12	[number]	k4	5-12
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
u	u	k7c2	u
akutní	akutní	k2eAgFnSc2d1	akutní
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
dyspnoe	dyspnoe	k1gFnPc1	dyspnoe
<g/>
,	,	kIx,	,
chrapot	chrapot	k1gInSc1	chrapot
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
při	při	k7c6	při
nádechu	nádech	k1gInSc6	nádech
natahují	natahovat	k5eAaImIp3nP	natahovat
krky	krk	k1gInPc4	krk
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
třepat	třepat	k5eAaImF	třepat
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
vykašlávat	vykašlávat	k5eAaImF	vykašlávat
hlen	hlen	k1gInSc4	hlen
a	a	k8xC	a
hnis	hnis	k1gInSc4	hnis
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
sinusitida	sinusitida	k1gFnSc1	sinusitida
a	a	k8xC	a
hnisavý	hnisavý	k2eAgInSc1d1	hnisavý
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
nozder	nozdra	k1gFnPc2	nozdra
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatečném	dostatečný	k2eNgNnSc6d1	nedostatečné
u	u	k7c2	u
okysličování	okysličování	k1gNnSc2	okysličování
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
cyanóza	cyanóza	k1gFnSc1	cyanóza
periferních	periferní	k2eAgFnPc2d1	periferní
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ptácí	Ptácí	k1gMnPc1	Ptácí
jsou	být	k5eAaImIp3nP	být
malátní	malátný	k2eAgMnPc1d1	malátný
<g/>
,	,	kIx,	,
neaktivní	aktivní	k2eNgMnPc1d1	neaktivní
a	a	k8xC	a
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Patognomickým	Patognomický	k2eAgInSc7d1	Patognomický
nálezem	nález	k1gInSc7	nález
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
hlen	hlen	k1gInSc1	hlen
a	a	k8xC	a
sýrovitý	sýrovitý	k2eAgInSc1d1	sýrovitý
zánětlivý	zánětlivý	k2eAgInSc1d1	zánětlivý
výpotek	výpotek	k1gInSc1	výpotek
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
se	se	k3xPyFc4	se
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
postižených	postižený	k2eAgMnPc2d1	postižený
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
histologicky	histologicky	k6eAd1	histologicky
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
nekrotická	nekrotický	k2eAgFnSc1d1	nekrotická
tracheitida	tracheitida	k1gFnSc1	tracheitida
s	s	k7c7	s
deskvamací	deskvamací	k2eAgFnSc7d1	deskvamací
epitelií	epitelie	k1gFnSc7	epitelie
<g/>
,	,	kIx,	,
v	v	k7c6	v
počátečních	počáteční	k2eAgFnPc6d1	počáteční
fázích	fáze	k1gFnPc6	fáze
nemoci	nemoc	k1gFnSc2	nemoc
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
také	také	k9	také
intranukleární	intranukleární	k2eAgFnPc4d1	intranukleární
virové	virový	k2eAgFnPc4d1	virová
inkluze	inkluze	k1gFnPc4	inkluze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
rychle	rychle	k6eAd1	rychle
šíří	šířit	k5eAaImIp3nS	šířit
<g/>
,	,	kIx,	,
morbidita	morbidita	k1gFnSc1	morbidita
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
mortalita	mortalita	k1gFnSc1	mortalita
pak	pak	k6eAd1	pak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nákazy	nákaza	k1gFnSc2	nákaza
virulentním	virulentní	k2eAgInSc7d1	virulentní
kmenem	kmen	k1gInSc7	kmen
viru	vir	k1gInSc2	vir
až	až	k6eAd1	až
50	[number]	k4	50
<g/>
%	%	kIx~	%
</s>
