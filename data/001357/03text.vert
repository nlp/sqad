<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc4	prosinec
1901	[number]	k4	1901
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Žižkov	Žižkov	k1gInSc1	Žižkov
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1974	[number]	k4	1974
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
samostatném	samostatný	k2eAgInSc6d1	samostatný
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
Karla	Karel	k1gMnSc2	Karel
Sladkovského	sladkovský	k2eAgMnSc2d1	sladkovský
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pošťákem	pošťák	k1gMnSc7	pošťák
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Zeměpisném	zeměpisný	k2eAgInSc6d1	zeměpisný
ústavu	ústav	k1gInSc6	ústav
a	a	k8xC	a
na	na	k7c6	na
ředitelství	ředitelství	k1gNnSc6	ředitelství
pošt	pošta	k1gFnPc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
praxe	praxe	k1gFnSc2	praxe
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
Užhorodu	Užhorod	k1gInSc2	Užhorod
na	na	k7c4	na
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
součástí	součást	k1gFnPc2	součást
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
až	až	k9	až
1923	[number]	k4	1923
a	a	k8xC	a
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
ochotnické	ochotnický	k2eAgNnSc4d1	ochotnické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ochotnicky	ochotnicky	k6eAd1	ochotnicky
i	i	k8xC	i
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
žižkovském	žižkovský	k2eAgNnSc6d1	Žižkovské
divadle	divadlo	k1gNnSc6	divadlo
Deklarace	deklarace	k1gFnSc2	deklarace
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
Švandově	Švandův	k2eAgNnSc6d1	Švandovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
práce	práce	k1gFnSc1	práce
výběrčího	výběrčí	k1gMnSc2	výběrčí
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
a	a	k8xC	a
spolužákovi	spolužák	k1gMnSc3	spolužák
Josefu	Josef	k1gMnSc3	Josef
Čapkovi	Čapek	k1gMnSc3	Čapek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stydí	stydět	k5eAaImIp3nP	stydět
vymáhat	vymáhat	k5eAaImF	vymáhat
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
peníze	peníz	k1gInSc2	peníz
<g/>
,	,	kIx,	,
Čapek	Čapek	k1gMnSc1	Čapek
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
k	k	k7c3	k
Burianovi	Burian	k1gMnSc3	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
hostujícím	hostující	k2eAgMnSc7d1	hostující
hercem	herec	k1gMnSc7	herec
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
s	s	k7c7	s
gáží	gáže	k1gFnSc7	gáže
třicet	třicet	k4xCc1	třicet
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
hercem	herec	k1gMnSc7	herec
a	a	k8xC	a
Burianovým	Burianův	k2eAgMnSc7d1	Burianův
dvorním	dvorní	k2eAgMnSc7d1	dvorní
"	"	kIx"	"
<g/>
nahrávačem	nahrávač	k1gMnSc7	nahrávač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
souborů	soubor	k1gInPc2	soubor
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgNnPc2d1	Pražské
a	a	k8xC	a
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgInS	hrát
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
činohry	činohra	k1gFnSc2	činohra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgInS	účinkovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
filmech	film	k1gInPc6	film
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejobsazovanějších	obsazovaný	k2eAgMnPc2d3	nejobsazovanější
českých	český	k2eAgMnPc2d1	český
herců	herec	k1gMnPc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Falešná	falešný	k2eAgFnSc1d1	falešná
kočička	kočička	k1gFnSc1	kočička
aneb	aneb	k?	aneb
Když	když	k8xS	když
si	se	k3xPyFc3	se
žena	žena	k1gFnSc1	žena
umíní	umínit	k5eAaPmIp3nS	umínit
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslední	poslední	k2eAgFnSc4d1	poslední
Noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
Burianových	Burianových	k2eAgInPc6d1	Burianových
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
též	též	k9	též
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Hříšní	hříšný	k2eAgMnPc1d1	hříšný
lidé	člověk	k1gMnPc1	člověk
města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
kriminálního	kriminální	k2eAgMnSc2d1	kriminální
rady	rada	k1gMnSc2	rada
Vacátka	Vacátko	k1gNnSc2	Vacátko
i	i	k9	i
v	v	k7c6	v
navazujících	navazující	k2eAgInPc6d1	navazující
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
1951	[number]	k4	1951
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
1953	[number]	k4	1953
titul	titul	k1gInSc1	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
1971	[number]	k4	1971
titul	titul	k1gInSc1	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
-	-	kIx~	-
purkrabí	purkrabí	k1gMnSc1	purkrabí
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
Havránek	Havránek	k1gMnSc1	Havránek
Smrt	smrt	k1gFnSc1	smrt
černého	černý	k1gMnSc2	černý
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
rada	rada	k1gFnSc1	rada
Vacátko	Vacátko	k1gNnSc1	Vacátko
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Excelsior	Excelsiora	k1gFnPc2	Excelsiora
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
rada	rada	k1gMnSc1	rada
Vacátko	Vacátko	k1gNnSc4	Vacátko
Pěnička	pěnička	k1gFnSc1	pěnička
a	a	k8xC	a
Paraplíčko	Paraplíčko	k?	Paraplíčko
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
rada	rada	k1gFnSc1	rada
Vacátko	Vacátko	k1gNnSc1	Vacátko
Partie	partie	k1gFnSc2	partie
krásného	krásný	k2eAgMnSc2d1	krásný
dragouna	dragoun	k1gMnSc2	dragoun
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
rada	rada	k1gMnSc1	rada
Vacátko	Vacátko	k1gNnSc1	Vacátko
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
smích	smích	k1gInSc4	smích
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
Touha	touha	k1gFnSc1	touha
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Anada	Anada	k1gFnSc1	Anada
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hříšní	hříšný	k2eAgMnPc1d1	hříšný
lidé	člověk	k1gMnPc1	člověk
města	město	k1gNnSc2	město
pražského	pražský	k2eAgInSc2d1	pražský
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgFnSc1d1	policejní
rada	rada	k1gFnSc1	rada
Vacátko	Vacátko	k1gNnSc1	Vacátko
Klec	klec	k1gFnSc4	klec
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
-	-	kIx~	-
Martin	Martin	k2eAgInSc1d1	Martin
Fantom	fantom	k1gInSc1	fantom
Morrisvillu	Morrisvill	k1gInSc2	Morrisvill
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brumpby	Brumpba	k1gFnSc2	Brumpba
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
-	-	kIx~	-
komisař	komisař	k1gMnSc1	komisař
Horký	horký	k2eAgInSc1d1	horký
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mareček	Mareček	k1gMnSc1	Mareček
Čintamani	čintamani	k1gNnSc4	čintamani
a	a	k8xC	a
podvodník	podvodník	k1gMnSc1	podvodník
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
rada	rada	k1gMnSc1	rada
Příběh	příběh	k1gInSc4	příběh
dušičkový	dušičkový	k2eAgInSc4d1	dušičkový
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
-	-	kIx~	-
revident	revident	k1gMnSc1	revident
Koláč	koláč	k1gInSc4	koláč
Táto	táta	k1gMnSc5	táta
<g/>
,	,	kIx,	,
sežeň	sehnat	k5eAaPmRp2nS	sehnat
štěně	štěně	k1gNnSc4	štěně
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Zkáza	zkáza	k1gFnSc1	zkáza
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
farář	farář	k1gMnSc1	farář
Einstein	Einstein	k1gMnSc1	Einstein
kontra	kontra	k1gNnSc7	kontra
Babinský	Babinský	k2eAgMnSc1d1	Babinský
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
Velká	velká	k1gFnSc1	velká
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
generál	generál	k1gMnSc1	generál
Neděle	neděle	k1gFnSc2	neděle
ve	v	k7c4	v
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
-	-	kIx~	-
Válek	válka	k1gFnPc2	válka
Sedmý	sedmý	k4xOgInSc4	sedmý
kontinent	kontinent	k1gInSc4	kontinent
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
-	-	kIx~	-
bagrista	bagrista	k1gMnSc1	bagrista
Rubeš	Rubeš	k1gMnSc1	Rubeš
Chlap	chlap	k1gMnSc1	chlap
jako	jako	k8xC	jako
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
-	-	kIx~	-
Fabián	Fabián	k1gMnSc1	Fabián
O	o	k7c6	o
medvědu	medvěd	k1gMnSc6	medvěd
Ondřejovi	Ondřej	k1gMnSc6	Ondřej
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
král	král	k1gMnSc1	král
Král	Král	k1gMnSc1	Král
Šumavy	Šumava	k1gFnSc2	Šumava
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
respicient	respicient	k1gMnSc1	respicient
finanční	finanční	k2eAgFnSc2d1	finanční
stráže	stráž	k1gFnSc2	stráž
Beran	Beran	k1gMnSc1	Beran
Slečna	slečna	k1gFnSc1	slečna
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pokorný	Pokorný	k1gMnSc1	Pokorný
Pět	pět	k4xCc1	pět
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
milionu	milion	k4xCgInSc2	milion
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
Karpíšek	Karpíšek	k1gMnSc1	Karpíšek
O	o	k7c6	o
věcech	věc	k1gFnPc6	věc
nadpřirozených	nadpřirozený	k2eAgMnPc2d1	nadpřirozený
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tajemství	tajemství	k1gNnSc1	tajemství
písma	písmo	k1gNnSc2	písmo
-	-	kIx~	-
soudce	soudce	k1gMnSc1	soudce
Poslušně	poslušně	k6eAd1	poslušně
hlásím	hlásit	k5eAaImIp1nS	hlásit
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
-	-	kIx~	-
strážmistr	strážmistr	k1gMnSc1	strážmistr
Flanderka	Flanderka	k1gFnSc1	Flanderka
Případ	případ	k1gInSc1	případ
ještě	ještě	k9	ještě
nekončí	končit	k5eNaImIp3nS	končit
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zouplna	Zouplna	k?	Zouplna
Váhavý	váhavý	k2eAgMnSc1d1	váhavý
střelec	střelec	k1gMnSc1	střelec
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jakub	Jakub	k1gMnSc1	Jakub
Něnička	Něnička	k1gMnSc1	Něnička
Kudy	kudy	k6eAd1	kudy
kam	kam	k6eAd1	kam
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
-	-	kIx~	-
Macek	Macek	k1gMnSc1	Macek
Vzorný	vzorný	k2eAgInSc1d1	vzorný
kinematograf	kinematograf	k1gInSc1	kinematograf
Haška	Hašek	k1gMnSc2	Hašek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
Honzátko	Honzátko	k1gNnSc1	Honzátko
Anděl	Anděla	k1gFnPc2	Anděla
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gustav	Gustav	k1gMnSc1	Gustav
Anděl	Anděl	k1gMnSc1	Anděl
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
Řehák	Řehák	k1gMnSc1	Řehák
Cirkus	cirkus	k1gInSc4	cirkus
bude	být	k5eAaImBp3nS	být
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
Liška	liška	k1gFnSc1	liška
Ještě	ještě	k9	ještě
svatba	svatba	k1gFnSc1	svatba
nebyla	být	k5eNaImAgFnS	být
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ambrož	Ambrož	k1gMnSc1	Ambrož
Expres	expres	k6eAd1	expres
z	z	k7c2	z
Norimberka	Norimberk	k1gInSc2	Norimberk
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
-	-	kIx~	-
Čapek	Čapek	k1gMnSc1	Čapek
Jestřáb	jestřáb	k1gMnSc1	jestřáb
kontra	kontra	k2eAgMnSc1d1	kontra
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jestřáb	jestřáb	k1gMnSc1	jestřáb
Plavecký	plavecký	k2eAgInSc4d1	plavecký
mariáš	mariáš	k1gInSc4	mariáš
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Váňa	Váňa	k1gMnSc1	Váňa
Dovolená	dovolená	k1gFnSc1	dovolená
s	s	k7c7	s
Andělem	Anděl	k1gMnSc7	Anděl
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gustav	Gustav	k1gMnSc1	Gustav
Anděl	Anděl	k1gMnSc1	Anděl
Haškovy	Haškův	k2eAgFnSc2d1	Haškova
povídky	povídka	k1gFnSc2	povídka
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
mocnářství	mocnářství	k1gNnSc2	mocnářství
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
rada	rada	k1gFnSc1	rada
Benzet	Benzet	k1gMnPc3	Benzet
Velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vojta	Vojta	k1gMnSc1	Vojta
Náprstek	náprstek	k1gInSc4	náprstek
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
máji	máj	k1gInSc6	máj
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
Šebesta	Šebesta	k1gMnSc1	Šebesta
Veselý	Veselý	k1gMnSc1	Veselý
souboj	souboj	k1gInSc1	souboj
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
Javorin	Javorin	k1gInSc1	Javorin
<g />
.	.	kIx.	.
</s>
<s>
starší	starý	k2eAgInSc4d2	starší
Revoluční	revoluční	k2eAgInSc4d1	revoluční
rok	rok	k1gInSc4	rok
1848	[number]	k4	1848
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Fastr	Fastr	k1gInSc1	Fastr
Němá	němý	k2eAgFnSc1d1	němá
barikáda	barikáda	k1gFnSc1	barikáda
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
strážník	strážník	k1gMnSc1	strážník
Brůček	brůček	k1gInSc1	brůček
Výlet	výlet	k1gInSc1	výlet
pana	pan	k1gMnSc2	pan
Broučka	Brouček	k1gMnSc2	Brouček
do	do	k7c2	do
zlatých	zlatý	k2eAgInPc2d1	zlatý
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Matěj	Matěj	k1gMnSc1	Matěj
Brouček	Brouček	k1gMnSc1	Brouček
Železný	Železný	k1gMnSc1	Železný
dědek	dědek	k1gMnSc1	dědek
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
-	-	kIx~	-
Matys	Matys	k1gMnSc1	Matys
Nikdo	nikdo	k3yNnSc1	nikdo
nic	nic	k3yNnSc1	nic
neví	vědět	k5eNaImIp3nS	vědět
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
Martin	Martin	k1gInSc4	Martin
Plechatý	plechatý	k2eAgInSc4d1	plechatý
Čapkovy	Čapkův	k2eAgFnPc4d1	Čapkova
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bartošek	Bartošek	k1gMnSc1	Bartošek
Poslední	poslední	k2eAgFnSc7d1	poslední
mohykán	mohykán	k1gMnSc1	mohykán
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Kohout	kohout	k1gInSc4	kohout
<g/>
/	/	kIx~	/
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kohout	kohout	k1gInSc4	kohout
Právě	právě	k9	právě
začínáme	začínat	k5eAaImIp1nP	začínat
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vosáhlo	Vosáhlo	k1gMnSc1	Vosáhlo
Nezbedný	nezbedný	k2eAgInSc4d1	nezbedný
bakalář	bakalář	k1gMnSc1	bakalář
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
primas	primas	k1gMnSc1	primas
Rakovníka	Rakovník	k1gInSc2	Rakovník
Lavina	lavina	k1gFnSc1	lavina
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Urban	Urban	k1gMnSc1	Urban
Průlom	průlom	k1gInSc1	průlom
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dudáček	dudáček	k1gInSc4	dudáček
Třináctý	třináctý	k4xOgInSc4	třináctý
revír	revír	k1gInSc4	revír
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspektor	inspektor	k1gMnSc1	inspektor
Čadek	Čadek	k1gMnSc1	Čadek
Řeka	Řek	k1gMnSc2	Řek
čaruje	čarovat	k5eAaImIp3nS	čarovat
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lebeda	Lebeda	k1gMnSc1	Lebeda
Prstýnek	prstýnek	k1gInSc1	prstýnek
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
farář	farář	k1gMnSc1	farář
Paklíč	paklíč	k1gInSc1	paklíč
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vilibald	Vilibald	k1gInSc1	Vilibald
Škarda	škarda	k1gFnSc1	škarda
alias	alias	k9	alias
Čepelka	čepelka	k1gFnSc1	čepelka
Počestné	počestný	k2eAgFnSc2d1	počestná
paní	paní	k1gFnSc2	paní
pardubické	pardubický	k2eAgFnSc2d1	pardubická
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
purkmistr	purkmistr	k1gMnSc1	purkmistr
Neviděli	vidět	k5eNaImAgMnP	vidět
jste	být	k5eAaImIp2nP	být
Bobíka	Bobík	k1gMnSc4	Bobík
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
U	u	k7c2	u
pěti	pět	k4xCc2	pět
veverek	veverka	k1gFnPc2	veverka
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
hostinský	hostinský	k1gMnSc1	hostinský
Pulec	pulec	k1gMnSc1	pulec
Šťastnou	šťastný	k2eAgFnSc4d1	šťastná
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Bláhový	bláhový	k2eAgInSc1d1	bláhový
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
Otakar	Otakar	k1gMnSc1	Otakar
Stárek	Stárek	k1gMnSc1	Stárek
Tanečnice	tanečnice	k1gFnSc1	tanečnice
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
ND	ND	kA	ND
Žíznivé	žíznivý	k2eAgNnSc1d1	žíznivé
mládí	mládí	k1gNnSc1	mládí
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
četnický	četnický	k2eAgMnSc1d1	četnický
strážmistr	strážmistr	k1gMnSc1	strážmistr
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
dno	dno	k1gNnSc4	dno
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Neškudla	Neškudla	k1gMnSc1	Neškudla
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
já	já	k3xPp1nSc1	já
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tonda	Tonda	k1gFnSc1	Tonda
Městečko	městečko	k1gNnSc1	městečko
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zimmerheier	Zimmerheier	k1gMnSc1	Zimmerheier
Ryba	Ryba	k1gMnSc1	Ryba
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
Hořánek	Hořánek	k1gMnSc1	Hořánek
Valentin	Valentin	k1gMnSc1	Valentin
Dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
přednosta	přednosta	k1gMnSc1	přednosta
kanceláře	kancelář	k1gFnSc2	kancelář
Těžký	těžký	k2eAgInSc4d1	těžký
život	život	k1gInSc4	život
dobrodruha	dobrodruh	k1gMnSc2	dobrodruh
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgFnSc2d1	policejní
inspektor	inspektor	k1gMnSc1	inspektor
Tichý	Tichý	k1gMnSc5	Tichý
Nebe	nebe	k1gNnPc4	nebe
a	a	k8xC	a
dudy	dudy	k1gFnPc4	dudy
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bartoš	Bartoš	k1gMnSc1	Bartoš
Noční	noční	k2eAgFnSc2d1	noční
motýl	motýl	k1gMnSc1	motýl
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
Marty	Marta	k1gFnSc2	Marta
Tetička	tetička	k1gFnSc1	tetička
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dusbaba	Dusbab	k1gMnSc2	Dusbab
Provdám	provdat	k5eAaPmIp1nS	provdat
svou	svůj	k3xOyFgFnSc4	svůj
<g />
.	.	kIx.	.
</s>
<s>
ženu	žena	k1gFnSc4	žena
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
gen.	gen.	kA	gen.
ředitel	ředitel	k1gMnSc1	ředitel
Merhaut	Merhaut	k1gMnSc1	Merhaut
Roztomilý	roztomilý	k2eAgMnSc1d1	roztomilý
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Kouřil	Kouřil	k1gMnSc1	Kouřil
Přednosta	přednosta	k1gMnSc1	přednosta
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
inspektor	inspektor	k1gMnSc1	inspektor
Kokrhel	kokrhel	k1gInSc4	kokrhel
Paličova	paličův	k2eAgFnSc1d1	Paličova
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Podleský	podleský	k2eAgInSc1d1	podleský
Střevíčky	střevíček	k1gInPc7	střevíček
slečny	slečna	k1gFnSc2	slečna
Pavlíny	Pavlína	k1gFnSc2	Pavlína
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
důchodní	důchodní	k1gFnSc1	důchodní
<g />
.	.	kIx.	.
</s>
<s>
v.v.	v.v.	k?	v.v.
<g/>
,	,	kIx,	,
nájemník	nájemník	k1gMnSc1	nájemník
Poznej	poznat	k5eAaPmRp2nS	poznat
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
Králíček	Králíček	k1gMnSc1	Králíček
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
štěstí	štěstí	k1gNnSc2	štěstí
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Novák	Novák	k1gMnSc1	Novák
Pelikán	Pelikán	k1gMnSc1	Pelikán
má	mít	k5eAaImIp3nS	mít
alibi	alibi	k1gNnSc4	alibi
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspektor	inspektor	k1gMnSc1	inspektor
Baron	baron	k1gMnSc1	baron
Prášil	Prášil	k1gMnSc1	Prášil
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bedřich	Bedřich	k1gMnSc1	Bedřich
Benda	Benda	k1gMnSc1	Benda
Muzikantská	muzikantský	k2eAgFnSc1d1	muzikantská
Liduška	Liduška	k1gFnSc1	Liduška
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Liška	Liška	k1gMnSc1	Liška
Konečně	konečně	k6eAd1	konečně
sami	sám	k3xTgMnPc1	sám
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Lehovec	Lehovec	k1gMnSc1	Lehovec
Přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
pana	pan	k1gMnSc2	pan
ministra	ministr	k1gMnSc2	ministr
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
Novák	Novák	k1gMnSc1	Novák
Píseň	píseň	k1gFnSc1	píseň
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
primář	primář	k1gMnSc1	primář
v	v	k7c6	v
porodnici	porodnice	k1gFnSc6	porodnice
Katakomby	katakomby	k1gFnPc1	katakomby
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
přednosta	přednosta	k1gMnSc1	přednosta
To	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
muzikant	muzikant	k1gMnSc1	muzikant
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
baron	baron	k1gMnSc1	baron
Hrubý	Hrubý	k1gMnSc1	Hrubý
Srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
celofánu	celofán	k1gInSc6	celofán
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
Herodes	Herodes	k1gMnSc1	Herodes
Paní	paní	k1gFnSc1	paní
Kačka	Kačka	k1gFnSc1	Kačka
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
študákovy	študákův	k2eAgFnSc2d1	študákův
duše	duše	k1gFnSc2	duše
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
prof.	prof.	kA	prof.
Vobořil	Vobořil	k1gMnSc1	Vobořil
U	u	k7c2	u
svatého	svatý	k2eAgMnSc2d1	svatý
Matěje	Matěj	k1gMnSc2	Matěj
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
notář	notář	k1gMnSc1	notář
Svátek	Svátek	k1gMnSc1	Svátek
věřitelů	věřitel	k1gMnPc2	věřitel
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Losman	Losman	k1gMnSc1	Losman
Ulice	ulice	k1gFnSc2	ulice
zpívá	zpívat	k5eAaImIp3nS	zpívat
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Dobře	dobře	k6eAd1	dobře
situovaný	situovaný	k2eAgMnSc1d1	situovaný
pán	pán	k1gMnSc1	pán
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
starožitník	starožitník	k1gMnSc1	starožitník
Samek	Samek	k1gMnSc1	Samek
Kristián	Kristián	k1gMnSc1	Kristián
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
Král	Král	k1gMnSc1	Král
Tulák	tulák	k1gMnSc1	tulák
Macoun	Macoun	k1gMnSc1	Macoun
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
prof.	prof.	kA	prof.
Vlach	Vlachy	k1gInPc2	Vlachy
Bílá	bílý	k2eAgFnSc1d1	bílá
jachta	jachta	k1gFnSc1	jachta
ve	v	k7c6	v
Splitu	Split	k1gInSc6	Split
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
příbuzný	příbuzný	k1gMnSc1	příbuzný
Její	její	k3xOp3gInSc4	její
hřích	hřích	k1gInSc4	hřích
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspektor	inspektor	k1gMnSc1	inspektor
Teď	teď	k6eAd1	teď
zas	zas	k9	zas
my	my	k3xPp1nPc1	my
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
továrník	továrník	k1gMnSc1	továrník
Hložek	Hložek	k1gMnSc1	Hložek
Paní	paní	k1gFnSc1	paní
Morálka	morálka	k1gFnSc1	morálka
kráčí	kráčet	k5eAaImIp3nS	kráčet
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
prof.	prof.	kA	prof.
Černý	Černý	k1gMnSc1	Černý
Venoušek	Venoušek	k1gMnSc1	Venoušek
a	a	k8xC	a
Stázička	Stázička	k1gFnSc1	Stázička
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Huňáček	huňáček	k1gMnSc1	huňáček
Děvče	děvče	k1gNnSc4	děvče
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Květinský	Květinský	k2eAgMnSc1d1	Květinský
U	u	k7c2	u
pokladny	pokladna	k1gFnSc2	pokladna
stál	stát	k5eAaImAgInS	stát
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
MUDr.	MUDr.	kA	MUDr.
Brunner	Brunner	k1gInSc1	Brunner
V	v	k7c6	v
pokušení	pokušení	k1gNnSc6	pokušení
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
továrník	továrník	k1gMnSc1	továrník
Kolár	Kolár	k1gMnSc1	Kolár
Lízino	Lízin	k2eAgNnSc4d1	Lízino
štěstí	štěstí	k1gNnSc4	štěstí
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
Brdička	Brdička	k1gMnSc1	Brdička
Umlčené	umlčený	k2eAgInPc4d1	umlčený
rty	ret	k1gInPc4	ret
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
varhaník	varhaník	k1gMnSc1	varhaník
Urban	Urban	k1gMnSc1	Urban
Studujeme	studovat	k5eAaImIp1nP	studovat
za	za	k7c7	za
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Malí	malý	k2eAgMnPc1d1	malý
velcí	velký	k2eAgMnPc1d1	velký
podvodníci	podvodník	k1gMnPc1	podvodník
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brdička	Brdička	k1gFnSc1	Brdička
Zborov	Zborov	k1gInSc1	Zborov
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovní	plukovní	k2eAgMnSc1d1	plukovní
lékař	lékař	k1gMnSc1	lékař
Boží	boží	k2eAgInSc1d1	boží
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
Soud	soud	k1gInSc1	soud
boží	boží	k2eAgInSc1d1	boží
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
Pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
straší	strašit	k5eAaImIp3nS	strašit
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
notář	notář	k1gMnSc1	notář
Jan	Jan	k1gMnSc1	Jan
Setunský	Setunský	k1gMnSc1	Setunský
Bláhové	Bláhové	k2eAgNnSc4d1	Bláhové
děvče	děvče	k1gNnSc4	děvče
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kosina	Kosina	k1gMnSc1	Kosina
Škola	škola	k1gFnSc1	škola
základ	základ	k1gInSc1	základ
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
prof.	prof.	kA	prof.
Kolísko	kolíska	k1gFnSc5	kolíska
<g/>
,	,	kIx,	,
češtinář	češtinář	k1gMnSc1	češtinář
Manželka	manželka	k1gFnSc1	manželka
něco	něco	k3yInSc4	něco
tuší	tušit	k5eAaImIp3nS	tušit
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Šmejkal	Šmejkal	k1gMnSc1	Šmejkal
Stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
oblaka	oblaka	k1gNnPc4	oblaka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
MUDr.	MUDr.	kA	MUDr.
Moravec	Moravec	k1gMnSc1	Moravec
Vandiny	Vandin	k2eAgFnSc2d1	Vandina
trampoty	trampota	k1gFnSc2	trampota
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
inspektor	inspektor	k1gMnSc1	inspektor
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
nadšenec	nadšenec	k1gMnSc1	nadšenec
Novák	Novák	k1gMnSc1	Novák
Ducháček	Ducháček	k1gMnSc1	Ducháček
to	ten	k3xDgNnSc4	ten
zařídí	zařídit	k5eAaPmIp3nS	zařídit
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kurt	kurt	k1gInSc4	kurt
Bertrand	Bertranda	k1gFnPc2	Bertranda
Cestou	cestou	k7c2	cestou
<g />
.	.	kIx.	.
</s>
<s>
křížovou	křížový	k2eAgFnSc7d1	křížová
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
velkostatku	velkostatek	k1gInSc2	velkostatek
Její	její	k3xOp3gFnPc1	její
pastorkyně	pastorkyně	k1gFnPc1	pastorkyně
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
doktor	doktor	k1gMnSc1	doktor
Milování	milování	k1gNnSc2	milování
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brejcha	Brejcha	k1gMnSc1	Brejcha
Děti	dítě	k1gFnPc4	dítě
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
notář	notář	k1gMnSc1	notář
Pavlík	Pavlík	k1gMnSc1	Pavlík
Boží	božit	k5eAaImIp3nS	božit
mlýny	mlýn	k1gInPc4	mlýn
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Škola	škola	k1gFnSc1	škola
života	život	k1gInSc2	život
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
ordonance	ordonance	k1gFnSc2	ordonance
Hordubalové	Hordubalový	k2eAgFnSc2d1	Hordubalový
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
soudní	soudní	k2eAgMnSc1d1	soudní
znalec	znalec	k1gMnSc1	znalec
Lidé	člověk	k1gMnPc1	člověk
pod	pod	k7c7	pod
horami	hora	k1gFnPc7	hora
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Havrda	Havrda	k1gFnSc1	Havrda
Klatovští	klatovský	k2eAgMnPc1d1	klatovský
dragouni	dragoun	k1gMnPc1	dragoun
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Chytra	Chytr	k1gMnSc2	Chytr
Lízin	Lízin	k2eAgInSc4d1	Lízin
let	let	k1gInSc4	let
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brdlička	Brdlička	k1gFnSc1	Brdlička
Armádní	armádní	k2eAgNnPc1d1	armádní
dvojčata	dvojče	k1gNnPc1	dvojče
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
bankéř	bankéř	k1gMnSc1	bankéř
Novák	Novák	k1gMnSc1	Novák
Batalion	batalion	k1gInSc1	batalion
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
státní	státní	k2eAgInSc1d1	státní
návladní	návladní	k2eAgNnPc4d1	návladní
Tři	tři	k4xCgNnPc4	tři
vejce	vejce	k1gNnPc4	vejce
do	do	k7c2	do
skla	sklo	k1gNnSc2	sklo
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
lékař	lékař	k1gMnSc1	lékař
Poručík	poručík	k1gMnSc1	poručík
Alexander	Alexandra	k1gFnPc2	Alexandra
Rjepkin	Rjepkin	k1gMnSc1	Rjepkin
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovník	plukovník	k1gMnSc1	plukovník
Důvod	důvod	k1gInSc1	důvod
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
zastavárny	zastavárna	k1gFnSc2	zastavárna
Jarčin	Jarčin	k2eAgMnSc1d1	Jarčin
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
baru	bar	k1gInSc2	bar
Švanda	Švanda	k1gMnSc1	Švanda
dudák	dudák	k1gMnSc1	dudák
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Trnka	trnka	k1gFnSc1	trnka
Advokátka	advokátka	k1gFnSc1	advokátka
Věra	Věra	k1gFnSc1	Věra
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
majitel	majitel	k1gMnSc1	majitel
koloniálu	koloniál	k1gInSc2	koloniál
Srdce	srdce	k1gNnSc2	srdce
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
topič	topič	k1gMnSc1	topič
Marek	Marek	k1gMnSc1	Marek
Harmonika	harmonika	k1gFnSc1	harmonika
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
podnikatel	podnikatel	k1gMnSc1	podnikatel
Pač	páčit	k5eAaImRp2nS	páčit
Rozvod	rozvod	k1gInSc1	rozvod
paní	paní	k1gFnSc2	paní
Evy	Eva	k1gFnSc2	Eva
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
MUDr.	MUDr.	kA	MUDr.
Mudroň	Mudroň	k1gFnSc2	Mudroň
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
kře	kra	k1gFnSc6	kra
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
Vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
torpédo	torpédo	k1gNnSc4	torpédo
48	[number]	k4	48
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
gen.	gen.	kA	gen.
Herold	herold	k1gMnSc1	herold
Páter	páter	k1gMnSc1	páter
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Dvorecký	dvorecký	k2eAgMnSc1d1	dvorecký
Švadlenka	švadlenka	k1gFnSc1	švadlenka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
přírodopisu	přírodopis	k1gInSc2	přírodopis
Manželství	manželství	k1gNnSc2	manželství
na	na	k7c4	na
úvěr	úvěr	k1gInSc4	úvěr
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
notář	notář	k1gMnSc1	notář
Lojzička	Lojzička	k1gFnSc1	Lojzička
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
sedlák	sedlák	k1gInSc1	sedlák
Trhani	trhan	k1gMnPc1	trhan
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
pokladník	pokladník	k1gMnSc1	pokladník
Ulička	ulička	k1gFnSc1	ulička
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
magistrátní	magistrátní	k2eAgMnSc1d1	magistrátní
úředník	úředník	k1gMnSc1	úředník
Divoch	divoch	k1gMnSc1	divoch
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
bankéř	bankéř	k1gMnSc1	bankéř
Pechan	Pechan	k1gMnSc1	Pechan
Tři	tři	k4xCgMnPc4	tři
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
Bártových	Bártových	k2eAgInPc2d1	Bártových
závodů	závod	k1gInPc2	závod
Sextánka	sextánek	k1gMnSc2	sextánek
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
Komediantská	komediantský	k2eAgFnSc1d1	komediantská
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
hotelu	hotel	k1gInSc2	hotel
Jánošík	Jánošík	k1gMnSc1	Jánošík
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
pandur	pandur	k1gMnSc1	pandur
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
srbský	srbský	k2eAgMnSc1d1	srbský
major	major	k1gMnSc1	major
Jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
přikázání	přikázání	k1gNnSc1	přikázání
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
První	první	k4xOgInSc4	první
políbení	políbení	k1gNnPc2	políbení
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
prof.	prof.	kA	prof.
Donát	Donát	k1gMnSc1	Donát
Pan	Pan	k1gMnSc1	Pan
otec	otec	k1gMnSc1	otec
Karafiát	Karafiát	k1gMnSc1	Karafiát
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
stárek	stárek	k1gMnSc1	stárek
Hrdina	Hrdina	k1gMnSc1	Hrdina
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
milionu	milion	k4xCgInSc2	milion
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
inspektor	inspektor	k1gMnSc1	inspektor
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
nebožtík	nebožtík	k1gMnSc1	nebožtík
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
Durieux	Durieux	k1gInSc1	Durieux
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
nadlesní	nadlesní	k1gMnSc1	nadlesní
Rys	Rys	k1gMnSc1	Rys
Na	na	k7c6	na
růžích	růž	k1gFnPc6	růž
ustláno	ustlán	k2eAgNnSc1d1	ustláno
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
hotelu	hotel	k1gInSc2	hotel
Grandhotel	grandhotel	k1gInSc1	grandhotel
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
prof.	prof.	kA	prof.
Cogan	Cogan	k1gMnSc1	Cogan
Nezlobte	zlobit	k5eNaImRp2nP	zlobit
dědečka	dědeček	k1gMnSc2	dědeček
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
nervového	nervový	k2eAgNnSc2d1	nervové
sanatoria	sanatorium	k1gNnSc2	sanatorium
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
v	v	k7c6	v
Kocourkově	Kocourkov	k1gInSc6	Kocourkov
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
trestnice	trestnice	k1gFnSc2	trestnice
Pokušení	pokušení	k1gNnSc2	pokušení
paní	paní	k1gFnSc2	paní
Antonie	Antonie	k1gFnSc2	Antonie
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
sekční	sekční	k2eAgMnSc1d1	sekční
šéf	šéf	k1gMnSc1	šéf
Hrdinný	hrdinný	k2eAgMnSc1d1	hrdinný
kapitán	kapitán	k1gMnSc1	kapitán
Korkorán	Korkorán	k2eAgMnSc1d1	Korkorán
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspektor	inspektor	k1gMnSc1	inspektor
Tři	tři	k4xCgInPc4	tři
kroky	krok	k1gInPc4	krok
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
spolku	spolek	k1gInSc2	spolek
věřitelů	věřitel	k1gMnPc2	věřitel
Za	za	k7c7	za
řádovými	řádový	k2eAgFnPc7d1	řádová
dveřmi	dveře	k1gFnPc7	dveře
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovník	plukovník	k1gMnSc1	plukovník
v.v.	v.v.	k?	v.v.
Dubrovnický	dubrovnický	k2eAgMnSc1d1	dubrovnický
Anita	Anita	k1gMnSc1	Anita
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Mazlíček	mazlíček	k1gMnSc1	mazlíček
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
-	-	kIx~	-
dozorce	dozorce	k1gMnSc1	dozorce
Řeka	Řek	k1gMnSc2	Řek
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g />
.	.	kIx.	.
</s>
<s>
hotelu	hotel	k1gInSc2	hotel
Revisor	revisor	k1gMnSc1	revisor
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
hejtman	hejtman	k1gMnSc1	hejtman
S	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Svítání	svítání	k1gNnSc2	svítání
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Okénko	okénko	k1gNnSc1	okénko
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
komisař	komisař	k1gMnSc1	komisař
Záhada	záhada	k1gFnSc1	záhada
modrého	modrý	k2eAgInSc2d1	modrý
pokoje	pokoj	k1gInSc2	pokoj
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
inspektor	inspektor	k1gMnSc1	inspektor
<g />
.	.	kIx.	.
</s>
<s>
Anton	Anton	k1gMnSc1	Anton
Špelec	Špelec	k1gMnSc1	Špelec
<g/>
,	,	kIx,	,
ostrostřelec	ostrostřelec	k1gMnSc1	ostrostřelec
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kačaba	kačaba	k1gMnSc1	kačaba
Písničkář	písničkář	k1gMnSc1	písničkář
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
úředník	úředník	k1gMnSc1	úředník
na	na	k7c6	na
poště	pošta	k1gFnSc6	pošta
Kantor	Kantor	k1gMnSc1	Kantor
Ideál	ideál	k1gInSc1	ideál
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
Suchého	Suchého	k2eAgMnSc1d1	Suchého
spolužák	spolužák	k1gMnSc1	spolužák
Růžové	růžový	k2eAgNnSc4d1	růžové
kombiné	kombiné	k1gNnSc4	kombiné
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
Šenkýřka	šenkýřka	k1gFnSc1	šenkýřka
U	u	k7c2	u
divoké	divoký	k2eAgFnSc2d1	divoká
krásy	krása	k1gFnSc2	krása
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
továrník	továrník	k1gMnSc1	továrník
Konrád	Konrád	k1gMnSc1	Konrád
Malostranští	malostranský	k2eAgMnPc1d1	malostranský
mušketýři	mušketýr	k1gMnPc1	mušketýr
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
divadla	divadlo	k1gNnSc2	divadlo
Funebrák	funebrák	k1gMnSc1	funebrák
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
svedené	svedený	k2eAgFnSc2d1	svedená
dcery	dcera	k1gFnSc2	dcera
Děvčátko	děvčátko	k1gNnSc1	děvčátko
<g/>
,	,	kIx,	,
neříkej	říkat	k5eNaImRp2nS	říkat
ne	ne	k9	ne
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
komisař	komisař	k1gMnSc1	komisař
Pobočník	pobočník	k1gMnSc1	pobočník
Jeho	jeho	k3xOp3gFnPc4	jeho
Výsosti	výsost	k1gFnPc4	výsost
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovník	plukovník	k1gMnSc1	plukovník
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovník	plukovník	k1gMnSc1	plukovník
Kraus	Kraus	k1gMnSc1	Kraus
Třetí	třetí	k4xOgFnSc1	třetí
rota	rota	k1gFnSc1	rota
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
člen	člen	k1gInSc1	člen
výboru	výbor	k1gInSc2	výbor
odboje	odboj	k1gInSc2	odboj
To	to	k9	to
neznáte	neznat	k5eAaImIp2nP	neznat
Hadimršku	hadimrška	k1gFnSc4	hadimrška
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bruckmann	Bruckmann	k1gInSc1	Bruckmann
<g />
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
komisař	komisař	k1gMnSc1	komisař
Psohlavi	Psohlaev	k1gFnSc6	Psohlaev
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
plukovník	plukovník	k1gMnSc1	plukovník
Muži	muž	k1gMnSc3	muž
v	v	k7c4	v
offsidu	offsida	k1gFnSc4	offsida
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgMnSc1d1	policejní
komisař	komisař	k1gMnSc1	komisař
Miláček	miláček	k1gMnSc1	miláček
pluku	pluk	k1gInSc2	pluk
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
kaprál	kaprál	k1gMnSc1	kaprál
Růžička	Růžička	k1gMnSc1	Růžička
Aféra	aféra	k1gFnSc1	aféra
plukovníka	plukovník	k1gMnSc2	plukovník
Rédla	Rédl	k1gMnSc2	Rédl
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšetřující	vyšetřující	k2eAgMnSc1d1	vyšetřující
soudce	soudce	k1gMnSc1	soudce
<g />
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
a	a	k8xC	a
k.	k.	k?	k.
polní	polní	k2eAgMnSc1d1	polní
maršálek	maršálek	k1gMnSc1	maršálek
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
-	-	kIx~	-
sluha	sluha	k1gMnSc1	sluha
pravého	pravý	k2eAgMnSc2d1	pravý
polního	polní	k2eAgMnSc2d1	polní
maršálka	maršálek	k1gMnSc2	maršálek
Když	když	k8xS	když
struny	struna	k1gFnPc1	struna
lkají	lkát	k5eAaImIp3nP	lkát
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
baru	bar	k1gInSc2	bar
Svatý	svatý	k1gMnSc1	svatý
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
slovanský	slovanský	k2eAgMnSc1d1	slovanský
kníže	kníže	k1gMnSc1	kníže
Plukovník	plukovník	k1gMnSc1	plukovník
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
srbský	srbský	k2eAgMnSc1d1	srbský
major	major	k1gMnSc1	major
Blagotič	Blagotič	k1gMnSc1	Blagotič
Páter	páter	k1gMnSc1	páter
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
obce	obec	k1gFnSc2	obec
Knotek	Knotek	k1gMnSc1	Knotek
Modrý	modrý	k2eAgInSc1d1	modrý
démant	démant	k1gInSc1	démant
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
-	-	kIx~	-
číšník	číšník	k1gMnSc1	číšník
Lásky	láska	k1gFnSc2	láska
Kačenky	Kačenka	k1gFnSc2	Kačenka
Strnadové	Strnadová	k1gFnSc2	Strnadová
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
senátor	senátor	k1gMnSc1	senátor
David	David	k1gMnSc1	David
Hradecký	Hradecký	k1gMnSc1	Hradecký
Falešná	falešný	k2eAgFnSc1d1	falešná
kočička	kočička	k1gFnSc1	kočička
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
strážník	strážník	k1gMnSc1	strážník
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
strážník	strážník	k1gMnSc1	strážník
</s>
