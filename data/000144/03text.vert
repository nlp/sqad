<s>
Murmur	Murmura	k1gFnPc2	Murmura
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
dlouhohrající	dlouhohrající	k2eAgNnSc1d1	dlouhohrající
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1983	[number]	k4	1983
a	a	k8xC	a
předcházelo	předcházet	k5eAaImAgNnS	předcházet
mu	on	k3xPp3gMnSc3	on
EP	EP	kA	EP
Chronic	Chronice	k1gFnPc2	Chronice
Town	Towna	k1gFnPc2	Towna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získalo	získat	k5eAaPmAgNnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
jej	on	k3xPp3gMnSc4	on
ohodnotil	ohodnotit	k5eAaPmAgInS	ohodnotit
jako	jako	k9	jako
197	[number]	k4	197
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
televize	televize	k1gFnSc2	televize
VH1	VH1	k1gFnSc1	VH1
jej	on	k3xPp3gNnSc4	on
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
92	[number]	k4	92
<g/>
.	.	kIx.	.
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
albem	album	k1gNnSc7	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
alba	album	k1gNnSc2	album
charakterizoval	charakterizovat	k5eAaBmAgInS	charakterizovat
tichou	tichý	k2eAgFnSc4d1	tichá
a	a	k8xC	a
introvertní	introvertní	k2eAgFnSc4d1	introvertní
stranu	strana	k1gFnSc4	strana
první	první	k4xOgFnSc2	první
vlny	vlna	k1gFnSc2	vlna
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevzdaloval	vzdalovat	k5eNaImAgMnS	vzdalovat
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
konceptu	koncept	k1gInSc2	koncept
tradiční	tradiční	k2eAgFnSc2d1	tradiční
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Kytary	kytara	k1gFnPc1	kytara
mají	mít	k5eAaImIp3nP	mít
jasný	jasný	k2eAgInSc4d1	jasný
zvonící	zvonící	k2eAgInSc4d1	zvonící
zvuk	zvuk	k1gInSc4	zvuk
takřka	takřka	k6eAd1	takřka
jako	jako	k9	jako
zvonkohra	zvonkohra	k1gFnSc1	zvonkohra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
kapele	kapela	k1gFnSc3	kapela
The	The	k1gFnSc2	The
Byrds	Byrdsa	k1gFnPc2	Byrdsa
<g/>
.	.	kIx.	.
</s>
<s>
Baskytarista	baskytarista	k1gMnSc1	baskytarista
Mike	Mik	k1gFnSc2	Mik
Mills	Mills	k1gInSc1	Mills
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejzkušenější	zkušený	k2eAgMnSc1d3	nejzkušenější
hudebník	hudebník	k1gMnSc1	hudebník
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
melodii	melodie	k1gFnSc4	melodie
alba	album	k1gNnSc2	album
a	a	k8xC	a
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
"	"	kIx"	"
<g/>
náladové	náladový	k2eAgFnSc6d1	náladová
<g/>
"	"	kIx"	"
atmosféře	atmosféra	k1gFnSc6	atmosféra
prvních	první	k4xOgNnPc2	první
alb	album	k1gNnPc2	album
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
Výrazným	výrazný	k2eAgInSc7d1	výrazný
příspěvkem	příspěvek	k1gInSc7	příspěvek
ke	k	k7c3	k
zvuku	zvuk	k1gInSc3	zvuk
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
i	i	k9	i
zpěv	zpěv	k1gInSc1	zpěv
Michaela	Michael	k1gMnSc2	Michael
Stipea	Stipeus	k1gMnSc2	Stipeus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
nejasné	jasný	k2eNgInPc1d1	nejasný
a	a	k8xC	a
nezřetelně	zřetelně	k6eNd1	zřetelně
zpívané	zpívaný	k2eAgInPc1d1	zpívaný
texty	text	k1gInPc1	text
přinesly	přinést	k5eAaPmAgInP	přinést
albu	alba	k1gFnSc4	alba
hloubku	hloubka	k1gFnSc4	hloubka
a	a	k8xC	a
mysterióznost	mysterióznost	k1gFnSc4	mysterióznost
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
byl	být	k5eAaImAgInS	být
i	i	k9	i
post-punk	postunk	k1gInSc1	post-punk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
kapelou	kapela	k1gFnSc7	kapela
Gang	Ganga	k1gFnPc2	Ganga
of	of	k?	of
Four	Foura	k1gFnPc2	Foura
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Murmur	Murmura	k1gFnPc2	Murmura
velice	velice	k6eAd1	velice
respektovaným	respektovaný	k2eAgNnSc7d1	respektované
albem	album	k1gNnSc7	album
a	a	k8xC	a
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgNnPc3d1	oblíbené
<g/>
,	,	kIx,	,
na	na	k7c4	na
pozdější	pozdní	k2eAgInSc4d2	pozdější
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
kapely	kapela	k1gFnSc2	kapela
mělo	mít	k5eAaImAgNnS	mít
jen	jen	k9	jen
minimální	minimální	k2eAgInSc4d1	minimální
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
v	v	k7c6	v
Charlotte	Charlott	k1gInSc5	Charlott
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
,	,	kIx,	,
produkovali	produkovat	k5eAaImAgMnP	produkovat
jej	on	k3xPp3gMnSc4	on
Mitch	Mitch	k1gMnSc1	Mitch
Easter	Easter	k1gMnSc1	Easter
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Dixon	Dixon	k1gMnSc1	Dixon
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
jej	on	k3xPp3gInSc4	on
společnost	společnost	k1gFnSc1	společnost
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tehdy	tehdy	k6eAd1	tehdy
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
tehdejších	tehdejší	k2eAgInPc6d1	tehdejší
koncertech	koncert	k1gInPc6	koncert
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
,	,	kIx,	,
vybrali	vybrat	k5eAaPmAgMnP	vybrat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Free	Freus	k1gMnSc5	Freus
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sitting	Sitting	k1gInSc1	Sitting
Still	Still	k1gInSc1	Still
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vydány	vydat	k5eAaPmNgInP	vydat
pod	pod	k7c7	pod
labelem	label	k1gInSc7	label
Hib-Tone	Hib-Ton	k1gMnSc5	Hib-Ton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
Murmur	Murmura	k1gFnPc2	Murmura
byly	být	k5eAaImAgFnP	být
znovu	znovu	k6eAd1	znovu
nahrány	nahrát	k5eAaPmNgFnP	nahrát
v	v	k7c6	v
pomalejším	pomalý	k2eAgNnSc6d2	pomalejší
tempu	tempo	k1gNnSc6	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
opustili	opustit	k5eAaPmAgMnP	opustit
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
punkový	punkový	k2eAgInSc4d1	punkový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
-	-	kIx~	-
"	"	kIx"	"
<g/>
Permanent	permanent	k1gInSc1	permanent
Vacation	Vacation	k1gInSc1	Vacation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ages	Ages	k1gInSc1	Ages
of	of	k?	of
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
the	the	k?	the
Right	Right	k1gInSc1	Right
Friends	Friends	k1gInSc1	Friends
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mystery	Myster	k1gInPc1	Myster
to	ten	k3xDgNnSc1	ten
Me	Me	k1gFnPc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Romance	romance	k1gFnSc1	romance
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
She	She	k1gMnSc5	She
Goes	Goes	k1gInSc4	Goes
Again	Again	k1gInSc4	Again
<g/>
"	"	kIx"	"
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
nahráno	nahrát	k5eAaPmNgNnS	nahrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
album	album	k1gNnSc4	album
nebyly	být	k5eNaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
později	pozdě	k6eAd2	pozdě
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
B-stranách	Btrana	k1gFnPc6	B-strana
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
na	na	k7c6	na
výběrových	výběrový	k2eAgNnPc6d1	výběrové
albech	album	k1gNnPc6	album
Eponymous	Eponymous	k1gInSc4	Eponymous
<g/>
,	,	kIx,	,
Dead	Dead	k1gInSc4	Dead
Letter	Lettra	k1gFnPc2	Lettra
Office	Office	kA	Office
<g/>
,	,	kIx,	,
And	Anda	k1gFnPc2	Anda
I	i	k8xC	i
Feel	Feela	k1gFnPc2	Feela
Fine	Fin	k1gMnSc5	Fin
<g/>
...	...	k?	...
a	a	k8xC	a
na	na	k7c6	na
koncertním	koncertní	k2eAgNnSc6d1	koncertní
DVD	DVD	kA	DVD
Perfect	Perfect	k2eAgInSc4d1	Perfect
Square	square	k1gInSc4	square
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
porostlá	porostlý	k2eAgFnSc1d1	porostlá
rostlinami	rostlina	k1gFnPc7	rostlina
zobrazená	zobrazený	k2eAgNnPc1d1	zobrazené
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
Georgia	Georgius	k1gMnSc2	Georgius
Railroad	Railroad	k1gInSc4	Railroad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
místní	místní	k2eAgFnSc7d1	místní
památkou	památka	k1gFnSc7	památka
přezívanou	přezívaný	k2eAgFnSc7d1	přezívaný
"	"	kIx"	"
<g/>
Murmur	Murmura	k1gFnPc2	Murmura
Trestle	Trestle	k1gFnPc2	Trestle
<g/>
"	"	kIx"	"
a	a	k8xC	a
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
odstranění	odstranění	k1gNnSc4	odstranění
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získalo	získat	k5eAaPmAgNnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
označil	označit	k5eAaPmAgInS	označit
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
Murmur	Murmura	k1gFnPc2	Murmura
za	za	k7c4	za
8	[number]	k4	8
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
jej	on	k3xPp3gMnSc4	on
stejný	stejný	k2eAgInSc1d1	stejný
časopis	časopis	k1gInSc1	časopis
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
197	[number]	k4	197
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
televizí	televize	k1gFnPc2	televize
VH1	VH1	k1gFnPc2	VH1
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
92	[number]	k4	92
<g/>
.	.	kIx.	.
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
albem	album	k1gNnSc7	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pitchfork	Pitchfork	k1gInSc4	Pitchfork
Media	medium	k1gNnSc2	medium
označil	označit	k5eAaPmAgMnS	označit
Murmur	Murmur	k1gMnSc1	Murmur
za	za	k7c4	za
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
ocenil	ocenit	k5eAaPmAgMnS	ocenit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Murmur	Murmura	k1gFnPc2	Murmura
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
album	album	k1gNnSc1	album
porazilo	porazit	k5eAaPmAgNnS	porazit
další	další	k2eAgFnPc4d1	další
slavná	slavný	k2eAgNnPc4d1	slavné
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Thriller	thriller	k1gInSc1	thriller
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
,	,	kIx,	,
Synchronity	Synchronita	k1gFnSc2	Synchronita
od	od	k7c2	od
The	The	k1gFnSc2	The
Police	police	k1gFnSc2	police
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
War	War	k1gMnPc1	War
od	od	k7c2	od
U	U	kA	U
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
zahrála	zahrát	k5eAaPmAgFnS	zahrát
celý	celý	k2eAgInSc4d1	celý
Murmur	Murmura	k1gFnPc2	Murmura
při	při	k7c6	při
posledním	poslední	k2eAgInSc6d1	poslední
koncertu	koncert	k1gInSc6	koncert
svého	svůj	k3xOyFgNnSc2	svůj
turné	turné	k1gNnSc2	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Green	Grena	k1gFnPc2	Grena
v	v	k7c6	v
atlantském	atlantský	k2eAgInSc6d1	atlantský
Fox	fox	k1gInSc1	fox
Theater	Theater	k1gInSc1	Theater
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jíž	jenž	k3xRgFnSc2	jenž
některé	některý	k3yIgFnSc2	některý
písně	píseň	k1gFnSc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
nikdy	nikdy	k6eAd1	nikdy
živě	živě	k6eAd1	živě
nezahráli	zahrát	k5eNaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
všech	všecek	k3xTgFnPc2	všecek
skladeb	skladba	k1gFnPc2	skladba
jsou	být	k5eAaImIp3nP	být
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Buck	Buck	k1gMnSc1	Buck
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Mills	Millsa	k1gFnPc2	Millsa
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gMnSc5	Stip
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
znovuvydala	znovuvydal	k1gMnSc2	znovuvydal
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vlastní	vlastní	k2eAgInSc4d1	vlastní
katalog	katalog	k1gInSc4	katalog
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
Murmur	Murmura	k1gFnPc2	Murmura
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
bonusovými	bonusový	k2eAgFnPc7d1	bonusová
skladbami	skladba	k1gFnPc7	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
She	She	k1gMnSc5	She
Goes	Goes	k1gInSc4	Goes
Again	Again	k1gInSc4	Again
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Live	Live	k1gFnSc1	Live
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Gardening	Gardening	k1gInSc1	Gardening
at	at	k?	at
Night	Night	k1gInSc1	Night
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Live	Live	k1gFnSc1	Live
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Catapult	Catapult	k1gInSc1	Catapult
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Live	Live	k1gFnSc1	Live
<g/>
)	)	kIx)	)
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
She	She	k1gMnSc5	She
Goes	Goes	k1gInSc4	Goes
Again	Again	k1gInSc1	Again
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
na	na	k7c6	na
albu	album	k1gNnSc6	album
Dead	Dead	k1gMnSc1	Dead
Letter	Letter	k1gMnSc1	Letter
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Berry	Berra	k1gFnSc2	Berra
-	-	kIx~	-
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Peter	Peter	k1gMnSc1	Peter
Buck	Buck	k1gMnSc1	Buck
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Mike	Mike	k1gInSc1	Mike
Mills	Mills	k1gInSc1	Mills
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Don	Don	k1gMnSc1	Don
Dixon	Dixon	k1gMnSc1	Dixon
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Perfect	Perfect	k2eAgInSc1d1	Perfect
Circle	Circle	k1gInSc1	Circle
<g/>
"	"	kIx"	"
Murmur	Murmur	k1gMnSc1	Murmur
je	být	k5eAaImIp3nS	být
také	také	k9	také
technika	technika	k1gFnSc1	technika
zpěvu	zpěv	k1gInSc2	zpěv
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
žánrech	žánr	k1gInPc6	žánr
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
black	black	k1gMnSc1	black
nebo	nebo	k8xC	nebo
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Murmur	Murmura	k1gFnPc2	Murmura
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
