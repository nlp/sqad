<s>
Obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
výměra	výměra	k1gFnSc1	výměra
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
charakteristikou	charakteristika	k1gFnSc7	charakteristika
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
dané	daný	k2eAgFnSc6d1	daná
dvourozměrné	dvourozměrný	k2eAgFnSc6d1	dvourozměrná
části	část	k1gFnSc6	část
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
písmenem	písmeno	k1gNnSc7	písmeno
S	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
nesprávně	správně	k6eNd1	správně
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
pro	pro	k7c4	pro
objem	objem	k1gInSc4	objem
prostorových	prostorový	k2eAgNnPc2d1	prostorové
(	(	kIx(	(
<g/>
třírozměrných	třírozměrný	k2eAgNnPc2d1	třírozměrné
<g/>
)	)	kIx)	)
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
písmenem	písmeno	k1gNnSc7	písmeno
V.	V.	kA	V.
Jednotka	jednotka	k1gFnSc1	jednotka
SI	se	k3xPyFc3	se
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
m	m	kA	m
<g/>
2	[number]	k4	2
další	další	k2eAgFnSc2d1	další
používané	používaný	k2eAgFnSc2d1	používaná
jednotky	jednotka	k1gFnSc2	jednotka
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
(	(	kIx(	(
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
=	=	kIx~	=
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
hektar	hektar	k1gInSc4	hektar
(	(	kIx(	(
<g/>
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
ha	ha	kA	ha
=	=	kIx~	=
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
ar	ar	k1gInSc4	ar
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
a	a	k8xC	a
=	=	kIx~	=
100	[number]	k4	100
m	m	kA	m
<g/>
2	[number]	k4	2
decimetr	decimetr	k1gInSc4	decimetr
<g />
.	.	kIx.	.
</s>
<s>
čtvereční	čtvereční	k2eAgFnSc1d1	čtvereční
(	(	kIx(	(
<g/>
dm	dm	kA	dm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
dm	dm	kA	dm
<g/>
2	[number]	k4	2
=	=	kIx~	=
0.01	[number]	k4	0.01
m	m	kA	m
<g/>
2	[number]	k4	2
centimetr	centimetr	k1gInSc4	centimetr
čtvereční	čtvereční	k2eAgFnSc2d1	čtvereční
(	(	kIx(	(
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
=	=	kIx~	=
0.0001	[number]	k4	0.0001
m	m	kA	m
<g/>
2	[number]	k4	2
milimetr	milimetr	k1gInSc4	milimetr
čtvereční	čtvereční	k2eAgFnSc2d1	čtvereční
(	(	kIx(	(
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
=	=	kIx~	=
0.000001	[number]	k4	0.000001
m	m	kA	m
<g/>
2	[number]	k4	2
nestandardní	standardní	k2eNgFnSc2d1	nestandardní
jednotky	jednotka	k1gFnSc2	jednotka
akr	akr	k1gInSc1	akr
měřidla	měřidlo	k1gNnSc2	měřidlo
planimetr	planimetr	k1gInSc1	planimetr
<g/>
,	,	kIx,	,
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
síť	síť	k1gFnSc1	síť
Výpočet	výpočet	k1gInSc1	výpočet
plochy	plocha	k1gFnSc2	plocha
ze	z	k7c2	z
souřadnic	souřadnice	k1gFnPc2	souřadnice
Plocha	plocha	k1gFnSc1	plocha
Povrch	povrch	k6eAd1wR	povrch
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
obsah	obsah	k1gInSc1	obsah
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
obsah	obsah	k1gInSc1	obsah
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
