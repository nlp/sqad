<s>
Vágatunnilin	Vágatunnilin	k1gInSc1
</s>
<s>
Vágatunnilin	Vágatunnilin	k1gInSc1
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Stát	stát	k5eAaPmF,k5eAaImF
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
62	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
24,84	24,84	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
31,08	31,08	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Provozní	provozní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
4	#num#	k4
940	#num#	k4
m	m	kA
Výstavba	výstavba	k1gFnSc1
Otevření	otevřený	k2eAgMnPc1d1
</s>
<s>
2002	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vágatunnilin	Vágatunnilin	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
tunel	tunel	k1gInSc1
Vága	Vág	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
4940	#num#	k4
m	m	kA
dlouhý	dlouhý	k2eAgInSc1d1
podmořský	podmořský	k2eAgInSc1d1
silniční	silniční	k2eAgInSc1d1
tunel	tunel	k1gInSc1
na	na	k7c6
Faerských	Faerský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prochází	procházet	k5eAaImIp3nS
pod	pod	k7c7
úžinou	úžina	k1gFnSc7
Vestmannasund	Vestmannasunda	k1gFnPc2
a	a	k8xC
spojuje	spojovat	k5eAaImIp3nS
ostrovy	ostrov	k1gInPc4
Streymoy	Streymoa	k1gFnSc2
a	a	k8xC
Vágar	Vágara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
je	být	k5eAaImIp3nS
prvním	první	k4xOgInSc7
podmořským	podmořský	k2eAgInSc7d1
tunelem	tunel	k1gInSc7
na	na	k7c6
Faerských	Faerský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
a	a	k8xC
spojuje	spojovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Tórshavn	Tórshavn	k1gInPc1
s	s	k7c7
letištěm	letiště	k1gNnSc7
Vágar	Vágara	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zeměměřické	zeměměřický	k2eAgFnPc1d1
práce	práce	k1gFnPc1
a	a	k8xC
geologický	geologický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
a	a	k8xC
stavba	stavba	k1gFnSc1
tunelu	tunel	k1gInSc2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vůli	vůle	k1gFnSc3
nástupu	nástup	k1gInSc2
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
projekty	projekt	k1gInPc1
rozvoje	rozvoj	k1gInSc2
infrastruktury	infrastruktura	k1gFnSc2
zastaveny	zastavit	k5eAaPmNgInP
<g/>
,	,	kIx,
včetně	včetně	k7c2
práce	práce	k1gFnSc2
na	na	k7c6
tunelu	tunel	k1gInSc6
Vága	Vág	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Pouze	pouze	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
výstavba	výstavba	k1gFnSc1
nového	nový	k2eAgInSc2d1
trajektového	trajektový	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
do	do	k7c2
Sandoy	Sandoa	k1gFnSc2
(	(	kIx(
<g/>
Gamlaræ	Gamlaræ	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovu	znovu	k6eAd1
byly	být	k5eAaImAgFnP
práce	práce	k1gFnPc1
zahájeny	zahájit	k5eAaPmNgFnP
až	až	k6eAd1
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
hrazen	hradit	k5eAaImNgInS
soukromým	soukromý	k2eAgInSc7d1
kapitálem	kapitál	k1gInSc7
<g/>
,	,	kIx,
prvním	první	k4xOgInSc7
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
na	na	k7c6
Faerských	Faerský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investor	investor	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
koncesi	koncese	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
práce	práce	k1gFnPc4
od	od	k7c2
projektu	projekt	k1gInSc2
<g/>
,	,	kIx,
přes	přes	k7c4
výstavbu	výstavba	k1gFnSc4
až	až	k9
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
provozování	provozování	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
otevřen	otevřen	k2eAgMnSc1d1
10	#num#	k4
<g/>
,	,	kIx,
prosince	prosinec	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
projelo	projet	k5eAaPmAgNnS
tunelem	tunel	k1gInSc7
359	#num#	k4
440	#num#	k4
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
už	už	k6eAd1
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
952	#num#	k4
300	#num#	k4
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
2	#num#	k4
609	#num#	k4
aut	auto	k1gNnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uvedením	uvedení	k1gNnSc7
tunelu	tunel	k1gInSc2
do	do	k7c2
provozu	provoz	k1gInSc2
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
lodní	lodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
přes	přes	k7c4
Vestmannasund	Vestmannasund	k1gInSc4
mezi	mezi	k7c4
Vestmanna	Vestmann	k1gMnSc4
a	a	k8xC
Oyrargjógv	Oyrargjógv	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
provozovala	provozovat	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Strandfaraskip	Strandfaraskip	k1gMnSc1
Landsins	Landsins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výstavba	výstavba	k1gFnSc1
</s>
<s>
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
ražen	razit	k5eAaImNgInS
v	v	k7c6
čedičovém	čedičový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
bez	bez	k7c2
identifikovaných	identifikovaný	k2eAgFnPc2d1
poruchových	poruchový	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobné	obdobný	k2eAgNnSc1d1
geologické	geologický	k2eAgNnSc1d1
podloží	podloží	k1gNnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Norsku	Norsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byly	být	k5eAaImAgFnP
využívány	využíván	k2eAgFnPc1d1
norské	norský	k2eAgFnPc1d1
zkušeností	zkušenost	k1gFnSc7
z	z	k7c2
výstavby	výstavba	k1gFnSc2
podmořských	podmořský	k2eAgInPc2d1
tunelů	tunel	k1gInPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgFnP
norské	norský	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
a	a	k8xC
také	také	k9
norské	norský	k2eAgFnSc2d1
normy	norma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výstavbě	výstavba	k1gFnSc6
se	se	k3xPyFc4
kladl	klást	k5eAaImAgInS
důraz	důraz	k1gInSc1
na	na	k7c4
systematické	systematický	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
a	a	k8xC
injektáž	injektáž	k1gFnSc4
prostředí	prostředí	k1gNnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
nepropustné	propustný	k2eNgNnSc4d1
podloží	podloží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Injektáží	injektáž	k1gFnPc2
se	se	k3xPyFc4
vyplňovaly	vyplňovat	k5eAaImAgFnP
trhliny	trhlina	k1gFnPc4
a	a	k8xC
póry	pór	k1gInPc4
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
hydroizolační	hydroizolační	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostění	ostění	k1gNnSc1
tunelu	tunel	k1gInSc2
bylo	být	k5eAaImAgNnS
jednovrstvé	jednovrstvý	k2eAgNnSc1d1
tvořené	tvořený	k2eAgNnSc1d1
pomocí	pomoc	k1gFnSc7
kotev	kotva	k1gFnPc2
a	a	k8xC
injektáže	injektáž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
výstavby	výstavba	k1gFnSc2
byla	být	k5eAaImAgFnS
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
velká	velký	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
přítokům	přítok	k1gInPc3
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Data	datum	k1gNnPc1
</s>
<s>
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
ražen	ražen	k2eAgInSc4d1
jako	jako	k8xC,k8xS
jednotubusový	jednotubusový	k2eAgInSc4d1
se	s	k7c7
dvěma	dva	k4xCgInPc7
jízdnými	jízdný	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
tradiční	tradiční	k2eAgNnSc4d1
metodou	metoda	k1gFnSc7
pomocí	pomocí	k7c2
trhavin	trhavina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tunelu	tunel	k1gInSc6
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc1
jízdní	jízdní	k2eAgInPc1d1
pruhy	pruh	k1gInPc1
s	s	k7c7
nouzovými	nouzový	k2eAgFnPc7d1
odstavnými	odstavný	k2eAgFnPc7d1
plochami	plocha	k1gFnPc7
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
co	co	k9
500	#num#	k4
m	m	kA
pro	pro	k7c4
menší	malý	k2eAgInPc4d2
automobily	automobil	k1gInPc4
a	a	k8xC
tři	tři	k4xCgNnPc4
odstavná	odstavný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
pro	pro	k7c4
velké	velký	k2eAgInPc4d1
automobily	automobil	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
průjezd	průjezd	k1gInSc4
tunelem	tunel	k1gInSc7
se	se	k3xPyFc4
platí	platit	k5eAaImIp3nS
mýtné	mýtné	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
jízdy	jízda	k1gFnSc2
je	být	k5eAaImIp3nS
80	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
délka	délka	k1gFnSc1
<g/>
:	:	kIx,
4950	#num#	k4
km	km	kA
<g/>
,	,	kIx,
</s>
<s>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
nadloží	nadloží	k1gNnSc2
<g/>
:	:	kIx,
30	#num#	k4
m	m	kA
<g/>
,	,	kIx,
</s>
<s>
max	max	kA
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
vody	voda	k1gFnSc2
60	#num#	k4
m	m	kA
<g/>
,	,	kIx,
</s>
<s>
max	max	kA
<g/>
.	.	kIx.
sklon	sklon	k1gInSc1
<g/>
:	:	kIx,
7	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
</s>
<s>
plocha	plocha	k1gFnSc1
výrubu	výrub	k1gInSc2
byl	být	k5eAaImAgMnS
65	#num#	k4
m²	m²	k?
s	s	k7c7
max	max	kA
<g/>
.	.	kIx.
šířkou	šířka	k1gFnSc7
10	#num#	k4
m	m	kA
<g/>
,	,	kIx,
</s>
<s>
šířka	šířka	k1gFnSc1
jízdního	jízdní	k2eAgInSc2d1
pruhu	pruh	k1gInSc2
<g/>
:	:	kIx,
7	#num#	k4
m	m	kA
(	(	kIx(
<g/>
dva	dva	k4xCgInPc4
pruhy	pruh	k1gInPc4
<g/>
,	,	kIx,
nouzové	nouzový	k2eAgFnSc2d1
odstavné	odstavný	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
každých	každý	k3xTgFnPc2
500	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
nejnižší	nízký	k2eAgInSc1d3
bod	bod	k1gInSc1
<g/>
:	:	kIx,
105	#num#	k4
m	m	kA
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
spotřeba	spotřeba	k1gFnSc1
výbušnin	výbušnina	k1gFnPc2
<g/>
:	:	kIx,
850	#num#	k4
t	t	k?
<g/>
,	,	kIx,
</s>
<s>
spotřeba	spotřeba	k1gFnSc1
betonu	beton	k1gInSc2
<g/>
:	:	kIx,
1000	#num#	k4
t	t	k?
<g/>
,	,	kIx,
</s>
<s>
vytěženo	vytěžen	k2eAgNnSc1d1
<g/>
:	:	kIx,
240	#num#	k4
miliónu	milión	k4xCgInSc2
tun	tuna	k1gFnPc2
čediče	čedič	k1gInSc2
(	(	kIx(
<g/>
327	#num#	k4
000	#num#	k4
m³	m³	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
náklady	náklad	k1gInPc1
<g/>
:	:	kIx,
302	#num#	k4
miliónu	milión	k4xCgInSc2
DKK	DKK	kA
(	(	kIx(
<g/>
240	#num#	k4
Faerských	Faerský	k2eAgFnPc2d1
korun	koruna	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Náklady	náklad	k1gInPc1
byly	být	k5eAaImAgInP
splaceny	splacen	k2eAgInPc1d1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
tj.	tj.	kA
po	po	k7c6
14	#num#	k4
letech	léto	k1gNnPc6
od	od	k7c2
otevření	otevření	k1gNnSc2
tunelu	tunel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Silnice	silnice	k1gFnSc1
k	k	k7c3
tunelu	tunel	k1gInSc3
</s>
<s>
Známky	známka	k1gFnPc4
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
tunelu	tunel	k1gInSc2
</s>
<s>
Portál	portál	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vágatunnilin	Vágatunnilina	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
CHABROŇOVÁ	CHABROŇOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
;	;	kIx,
KLEPSATEL	KLEPSATEL	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestné	cestný	k2eAgInPc1d1
tunely	tunel	k1gInPc1
na	na	k7c6
Faerských	Faerský	k2eAgFnPc6d1
ostrovoch	ostrovoch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
137	#num#	k4
<g/>
–	–	k?
<g/>
139	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
–	–	k?
<g/>
139	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
728	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Sø	Sø	k1gFnPc2
um	uma	k1gFnPc2
Vágatunnilin	Vágatunnilin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunnil	Tunnil	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FIROUZ	FIROUZ	kA
<g/>
,	,	kIx,
Gaini	Gain	k1gMnPc1
<g/>
;	;	kIx,
JACOBSEN	JACOBSEN	kA
<g/>
,	,	kIx,
Helgi	Helgi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mynstur	Mynstura	k1gFnPc2
broytast	broytast	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Tórshavn	Tórshavn	k1gInSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Landsverk	Landsverk	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
339	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
99918	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
99918	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
488419805	#num#	k4
↑	↑	k?
Tunnilin	Tunnilina	k1gFnPc2
í	í	k0
tø	tø	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunnil	Tunnil	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dotaz	dotaz	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
tunelů	tunel	k1gInPc2
na	na	k7c6
Faerských	Faerský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vágatunnilin	Vágatunnilina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Doprava	doprava	k1gFnSc1
</s>
