<s>
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
populární	populární	k2eAgMnSc1d1	populární
britský	britský	k2eAgMnSc1d1	britský
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
i	i	k8xC	i
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
?	?	kIx.	?
</s>
