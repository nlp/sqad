<p>
<s>
George	Georg	k1gMnSc2	Georg
Harold	Harold	k1gMnSc1	Harold
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
MBE	MBE	kA	MBE
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
Liverpool	Liverpool	k1gInSc1	Liverpool
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
populární	populární	k2eAgMnSc1d1	populární
britský	britský	k2eAgMnSc1d1	britský
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
i	i	k8xC	i
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
George	George	k1gInSc1	George
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
harmonické	harmonický	k2eAgFnSc6d1	harmonická
rodině	rodina	k1gFnSc6	rodina
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Louisa	Louisa	k?	Louisa
Harrisonová	Harrisonová	k1gFnSc1	Harrisonová
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Harold	Harold	k1gMnSc1	Harold
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Liverpoolu	Liverpool	k1gInSc6	Liverpool
společně	společně	k6eAd1	společně
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
sourozenci	sourozenec	k1gMnSc3	sourozenec
(	(	kIx(	(
<g/>
sestra	sestra	k1gFnSc1	sestra
Louise	Louis	k1gMnSc2	Louis
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
Harold	Harolda	k1gFnPc2	Harolda
ml.	ml.	kA	ml.
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Beatles	beatles	k1gMnSc1	beatles
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
jako	jako	k8xC	jako
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neznali	znát	k5eNaImAgMnP	znát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c4	v
Quarry	Quarra	k1gFnPc4	Quarra
Bank	bank	k1gInSc1	bank
poznal	poznat	k5eAaPmAgInS	poznat
Paula	Paul	k1gMnSc4	Paul
McCartneyho	McCartney	k1gMnSc4	McCartney
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc2	třináct
letech	léto	k1gNnPc6	léto
mu	on	k3xPp3gMnSc3	on
rodiče	rodič	k1gMnPc1	rodič
koupili	koupit	k5eAaPmAgMnP	koupit
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
<g/>
;	;	kIx,	;
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
nezájem	nezájem	k1gInSc4	nezájem
začal	začít	k5eAaPmAgMnS	začít
George	Georg	k1gMnSc4	Georg
pravidelně	pravidelně	k6eAd1	pravidelně
cvičit	cvičit	k5eAaImF	cvičit
a	a	k8xC	a
dělat	dělat	k5eAaImF	dělat
pokroky	pokrok	k1gInPc4	pokrok
<g/>
,	,	kIx,	,
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
nahrávky	nahrávka	k1gFnPc4	nahrávka
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
napodobovat	napodobovat	k5eAaImF	napodobovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
akordy	akord	k1gInPc1	akord
ho	on	k3xPp3gInSc4	on
učil	učit	k5eAaImAgMnS	učit
hrát	hrát	k5eAaImF	hrát
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
matka	matka	k1gFnSc1	matka
Georgovi	Georg	k1gMnSc3	Georg
koupila	koupit	k5eAaPmAgFnS	koupit
pořádnou	pořádný	k2eAgFnSc4d1	pořádná
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc2	on
George	Georg	k1gFnSc2	Georg
splatil	splatit	k5eAaPmAgMnS	splatit
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
na	na	k7c6	na
koupi	koupě	k1gFnSc6	koupě
nástroje	nástroj	k1gInSc2	nástroj
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
poslíček	poslíček	k1gMnSc1	poslíček
u	u	k7c2	u
řezníka	řezník	k1gMnSc2	řezník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
postoupil	postoupit	k5eAaPmAgInS	postoupit
George	George	k1gInSc1	George
na	na	k7c4	na
Liverpool	Liverpool	k1gInSc4	Liverpool
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
po	po	k7c6	po
čase	čas	k1gInSc6	čas
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
elektrikářským	elektrikářský	k2eAgMnSc7d1	elektrikářský
učněm	učeň	k1gMnSc7	učeň
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
Rebels	Rebelsa	k1gFnPc2	Rebelsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
povstalci	povstalec	k1gMnPc1	povstalec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
vzorem	vzor	k1gInSc7	vzor
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
Buddy	Budda	k1gFnSc2	Budda
Holly	Holla	k1gFnSc2	Holla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
začal	začít	k5eAaPmAgInS	začít
George	George	k1gInSc1	George
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
The	The	k1gMnSc2	The
Quarrymen	Quarrymen	k1gInSc4	Quarrymen
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
McCartney	McCartney	k1gInPc7	McCartney
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
třikrát	třikrát	k6eAd1	třikrát
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
<g/>
:	:	kIx,	:
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
na	na	k7c4	na
"	"	kIx"	"
<g/>
Johnny	Johnn	k1gMnPc4	Johnn
and	and	k?	and
the	the	k?	the
Moondogs	Moondogs	k1gInSc1	Moondogs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
na	na	k7c4	na
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Beetles	Beetles	k1gMnSc1	Beetles
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
na	na	k7c4	na
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
slovní	slovní	k2eAgFnSc1d1	slovní
hříčka	hříčka	k1gFnSc1	hříčka
–	–	k?	–
"	"	kIx"	"
<g/>
beetle	beetle	k1gFnSc1	beetle
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
brouk	brouk	k1gMnSc1	brouk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
beat	beat	k1gInSc1	beat
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
tlouci	tlouct	k5eAaImF	tlouct
<g/>
,	,	kIx,	,
bít	bít	k5eAaImF	bít
<g/>
,	,	kIx,	,
tep	tep	k1gInSc1	tep
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
dostala	dostat	k5eAaPmAgFnS	dostat
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gInSc1	George
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgInS	být
plnoletý	plnoletý	k2eAgInSc1d1	plnoletý
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
pracovní	pracovní	k2eAgNnSc4d1	pracovní
povolení	povolení	k1gNnSc4	povolení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
angažmá	angažmá	k1gNnSc7	angažmá
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
původního	původní	k2eAgMnSc2d1	původní
basového	basový	k2eAgMnSc2d1	basový
kytaristy	kytarista	k1gMnSc2	kytarista
Stuarta	Stuart	k1gMnSc2	Stuart
Sutcliffa	Sutcliff	k1gMnSc2	Sutcliff
přebral	přebrat	k5eAaPmAgInS	přebrat
Sutcliffův	Sutcliffův	k2eAgInSc1d1	Sutcliffův
nástroj	nástroj	k1gInSc1	nástroj
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
a	a	k8xC	a
McCartneyovu	McCartneyův	k2eAgFnSc4d1	McCartneyův
sólovou	sólový	k2eAgFnSc4d1	sólová
kytaru	kytara	k1gFnSc4	kytara
mohl	moct	k5eAaImAgMnS	moct
převzít	převzít	k5eAaPmF	převzít
George	Georg	k1gMnSc4	Georg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
byl	být	k5eAaImAgMnS	být
výtečný	výtečný	k2eAgMnSc1d1	výtečný
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Beatles	beatles	k1gMnSc1	beatles
nedostal	dostat	k5eNaPmAgMnS	dostat
mnoho	mnoho	k4c4	mnoho
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
vlastních	vlastní	k2eAgFnPc2d1	vlastní
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
alba	album	k1gNnSc2	album
Revolver	revolver	k1gInSc1	revolver
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
objevovaly	objevovat	k5eAaImAgFnP	objevovat
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
Beatles	Beatles	k1gFnPc2	Beatles
výjimečně	výjimečně	k6eAd1	výjimečně
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
nanejvýš	nanejvýš	k6eAd1	nanejvýš
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
poznal	poznat	k5eAaPmAgMnS	poznat
George	George	k1gFnSc4	George
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Perný	perný	k2eAgInSc4d1	perný
den	den	k1gInSc4	den
modelku	modelka	k1gFnSc4	modelka
Pattie	Pattie	k1gFnSc2	Pattie
Boydovou	Boydová	k1gFnSc7	Boydová
<g/>
.	.	kIx.	.
</s>
<s>
Oddáni	oddán	k2eAgMnPc1d1	oddán
byli	být	k5eAaImAgMnP	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
až	až	k8xS	až
1969	[number]	k4	1969
se	se	k3xPyFc4	se
George	George	k1gInSc1	George
potýkal	potýkat	k5eAaImAgInS	potýkat
s	s	k7c7	s
drogovou	drogový	k2eAgFnSc7d1	drogová
závislostí	závislost	k1gFnSc7	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgMnS	užívat
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
Rubber	Rubbra	k1gFnPc2	Rubbra
Soul	Soul	k1gInSc4	Soul
zaznamenány	zaznamenán	k2eAgInPc4d1	zaznamenán
první	první	k4xOgInPc4	první
Georgeovy	Georgeův	k2eAgInPc4d1	Georgeův
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
indický	indický	k2eAgInSc4d1	indický
strunný	strunný	k2eAgInSc4d1	strunný
nástroj	nástroj	k1gInSc4	nástroj
sitár	sitár	k1gMnSc1	sitár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
indickým	indický	k2eAgMnSc7d1	indický
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
především	především	k6eAd1	především
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
sitáristou	sitárista	k1gMnSc7	sitárista
Ravi	Rav	k1gFnSc2	Rav
Šankarem	Šankar	k1gMnSc7	Šankar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gInSc1	George
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
indickou	indický	k2eAgFnSc4d1	indická
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
meditaci	meditace	k1gFnSc4	meditace
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
význačným	význačný	k2eAgMnSc7d1	význačný
příznivcem	příznivec	k1gMnSc7	příznivec
hnutí	hnutí	k1gNnSc2	hnutí
Hare	Hare	k1gNnSc2	Hare
Krišna	Krišna	k1gMnSc1	Krišna
<g/>
.	.	kIx.	.
</s>
<s>
Odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narušilo	narušit	k5eAaPmAgNnS	narušit
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
Pattie	Pattie	k1gFnSc2	Pattie
Boyd	Boyda	k1gFnPc2	Boyda
po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
odešla	odejít	k5eAaPmAgFnS	odejít
k	k	k7c3	k
Eriku	Erik	k1gMnSc3	Erik
Claptonovi	Clapton	k1gMnSc3	Clapton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Beatles	Beatles	k1gFnSc6	Beatles
vydal	vydat	k5eAaPmAgMnS	vydat
Harrison	Harrison	k1gMnSc1	Harrison
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
–	–	k?	–
jednak	jednak	k8xC	jednak
Wonderwall	Wonderwall	k1gMnSc1	Wonderwall
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
Electronic	Electronice	k1gFnPc2	Electronice
Sound	Sounda	k1gFnPc2	Sounda
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnSc1d1	experimentální
syntezátorová	syntezátorový	k2eAgFnSc1d1	syntezátorová
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
kritiky	kritik	k1gMnPc4	kritik
příliš	příliš	k6eAd1	příliš
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vydal	vydat	k5eAaPmAgInS	vydat
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
All	All	k1gFnPc2	All
Things	Thingsa	k1gFnPc2	Thingsa
Must	Must	k2eAgInSc4d1	Must
Pass	Pass	k1gInSc4	Pass
se	s	k7c7	s
slavnou	slavný	k2eAgFnSc7d1	slavná
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Sweet	Sweet	k1gMnSc1	Sweet
Lord	lord	k1gMnSc1	lord
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
opěvující	opěvující	k2eAgFnSc1d1	opěvující
Krišnu	Krišna	k1gMnSc4	Krišna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
Harrison	Harrison	k1gMnSc1	Harrison
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
podobnosti	podobnost	k1gFnSc3	podobnost
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
He	he	k0	he
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
So	So	kA	So
Fine	Fin	k1gMnSc5	Fin
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
newyorské	newyorský	k2eAgFnSc2d1	newyorská
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
Chiffons	Chiffons	k1gInSc1	Chiffons
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
George	George	k1gInSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
dobročinný	dobročinný	k2eAgInSc4d1	dobročinný
koncert	koncert	k1gInSc4	koncert
pro	pro	k7c4	pro
válkou	válka	k1gFnSc7	válka
zmítaný	zmítaný	k2eAgInSc1d1	zmítaný
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
(	(	kIx(	(
<g/>
Concert	Concert	k1gInSc1	Concert
for	forum	k1gNnPc2	forum
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgMnSc6	který
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
mnoho	mnoho	k4c1	mnoho
slavných	slavný	k2eAgMnPc2d1	slavný
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc4	Bill
Preston	Preston	k1gInSc1	Preston
<g/>
,	,	kIx,	,
Ravi	Ravi	k1gNnSc1	Ravi
Šankar	Šankara	k1gFnPc2	Šankara
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
hudební	hudební	k2eAgNnSc4d1	hudební
album	album	k1gNnSc4	album
a	a	k8xC	a
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
vynikajícím	vynikající	k2eAgNnSc6d1	vynikající
albu	album	k1gNnSc6	album
Living	Living	k1gInSc1	Living
in	in	k?	in
the	the	k?	the
Material	Material	k1gMnSc1	Material
World	World	k1gMnSc1	World
založil	založit	k5eAaPmAgMnS	založit
George	Georg	k1gMnSc4	Georg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vlastní	vlastní	k2eAgFnSc4d1	vlastní
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
firmu	firma	k1gFnSc4	firma
Dark	Dark	k1gMnSc1	Dark
Horse	Horse	k1gFnSc2	Horse
Records	Records	k1gInSc1	Records
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
sólové	sólový	k2eAgNnSc1d1	sólové
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
USA	USA	kA	USA
nebylo	být	k5eNaImAgNnS	být
kritikou	kritika	k1gFnSc7	kritika
přijato	přijmout	k5eAaPmNgNnS	přijmout
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
žil	žít	k5eAaImAgMnS	žít
George	George	k1gFnSc4	George
s	s	k7c7	s
modelkou	modelka	k1gFnSc7	modelka
Kathy	Katha	k1gFnSc2	Katha
Simmons	Simmonsa	k1gFnPc2	Simmonsa
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Roda	Rodus	k1gMnSc2	Rodus
Stewarta	Stewart	k1gMnSc2	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Mexičanky	Mexičanka	k1gFnSc2	Mexičanka
Olivia	Olivia	k1gFnSc1	Olivia
Trinidad	Trinidad	k1gInSc1	Trinidad
Arias	Arias	k1gInSc1	Arias
<g/>
,	,	kIx,	,
sekretářky	sekretářek	k1gInPc1	sekretářek
firmy	firma	k1gFnSc2	firma
A	A	kA	A
<g/>
&	&	k?	&
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
desky	deska	k1gFnPc4	deska
jeho	jeho	k3xOp3gFnSc2	jeho
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Dhani	Dhaeň	k1gFnSc3	Dhaeň
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
skupině	skupina	k1gFnSc6	skupina
Thenewno	Thenewno	k6eAd1	Thenewno
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
vydával	vydávat	k5eAaImAgMnS	vydávat
Harrison	Harrison	k1gMnSc1	Harrison
i	i	k9	i
nadále	nadále	k6eAd1	nadále
sólová	sólový	k2eAgNnPc1d1	sólové
alba	album	k1gNnPc1	album
(	(	kIx(	(
<g/>
33	[number]	k4	33
&	&	k?	&
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
či	či	k8xC	či
George	George	k1gNnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
–	–	k?	–
tu	tu	k6eAd1	tu
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
melodicky	melodicky	k6eAd1	melodicky
silné	silný	k2eAgFnPc4d1	silná
a	a	k8xC	a
textově	textově	k6eAd1	textově
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
počiny	počin	k1gInPc4	počin
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
George	George	k1gFnSc7	George
držel	držet	k5eAaImAgMnS	držet
svého	svůj	k3xOyFgInSc2	svůj
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
jakkoli	jakkoli	k6eAd1	jakkoli
přijímat	přijímat	k5eAaImF	přijímat
stávající	stávající	k2eAgInPc4d1	stávající
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
i	i	k9	i
proto	proto	k8xC	proto
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
poněkud	poněkud	k6eAd1	poněkud
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
před	před	k7c7	před
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
filmové	filmový	k2eAgFnSc3d1	filmová
produkci	produkce	k1gFnSc3	produkce
v	v	k7c6	v
nově	nova	k1gFnSc6	nova
založené	založený	k2eAgFnSc2d1	založená
společnosti	společnost	k1gFnSc2	společnost
HandMade	HandMad	k1gInSc5	HandMad
Films	Films	k1gInSc1	Films
a	a	k8xC	a
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
vydával	vydávat	k5eAaImAgInS	vydávat
sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
album	album	k1gNnSc4	album
Somewhere	Somewher	k1gInSc5	Somewher
in	in	k?	in
England	Englando	k1gNnPc2	Englando
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
písnička	písnička	k1gFnSc1	písnička
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Johnu	John	k1gMnSc3	John
Lennonovi	Lennon	k1gMnSc3	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Those	Those	k1gFnSc2	Those
Years	Years	k1gInSc1	Years
Ago	aga	k1gMnSc5	aga
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
"	"	kIx"	"
<g/>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
jsi	být	k5eAaImIp2nS	být
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
jednali	jednat	k5eAaImAgMnP	jednat
jak	jak	k6eAd1	jak
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vydaným	vydaný	k2eAgNnSc7d1	vydané
albem	album	k1gNnSc7	album
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
"	"	kIx"	"
<g/>
Gone	Gon	k1gInSc2	Gon
Troppo	Troppa	k1gFnSc5	Troppa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příjemné	příjemný	k2eAgNnSc1d1	příjemné
album	album	k1gNnSc1	album
nijak	nijak	k6eAd1	nijak
se	se	k3xPyFc4	se
nevymykající	vymykající	k2eNgFnSc1d1	nevymykající
Georgeově	Georgeův	k2eAgNnSc6d1	Georgeovo
stylu	styl	k1gInSc6	styl
–	–	k?	–
Harrison	Harrison	k1gMnSc1	Harrison
však	však	k9	však
tentokrát	tentokrát	k6eAd1	tentokrát
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
deska	deska	k1gFnSc1	deska
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
koncertní	koncertní	k2eAgNnSc4d1	koncertní
pódium	pódium	k1gNnSc4	pódium
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vzpomínkovým	vzpomínkový	k2eAgInSc7d1	vzpomínkový
pořadem	pořad	k1gInSc7	pořad
Carla	Carl	k1gMnSc2	Carl
Perkinse	Perkins	k1gMnSc2	Perkins
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
i	i	k9	i
na	na	k7c6	na
charitativním	charitativní	k2eAgInSc6d1	charitativní
koncertu	koncert	k1gInSc6	koncert
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
Cloud	Clouda	k1gFnPc2	Clouda
Nine	Nine	k1gNnSc2	Nine
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
žebříčků	žebříček	k1gInPc2	žebříček
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
kvalit	kvalita	k1gFnPc2	kvalita
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	album	k1gNnPc2	album
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
vzešel	vzejít	k5eAaPmAgInS	vzejít
i	i	k9	i
hitový	hitový	k2eAgInSc1d1	hitový
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Got	Got	k1gFnSc1	Got
My	my	k3xPp1nPc1	my
Mind	Mind	k1gInSc1	Mind
Set	set	k1gInSc4	set
on	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
Harrison	Harrison	k1gMnSc1	Harrison
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
hudebníky	hudebník	k1gMnPc7	hudebník
Royem	Roy	k1gMnSc7	Roy
Orbisonem	Orbison	k1gMnSc7	Orbison
<g/>
,	,	kIx,	,
Jeffem	Jeff	k1gMnSc7	Jeff
Lynneem	Lynneus	k1gMnSc7	Lynneus
<g/>
,	,	kIx,	,
Bobem	Bob	k1gMnSc7	Bob
Dylanem	Dylan	k1gMnSc7	Dylan
a	a	k8xC	a
Tomem	Tom	k1gMnSc7	Tom
Pettym	Pettym	k1gInSc1	Pettym
a	a	k8xC	a
pod	pod	k7c7	pod
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Traveling	Traveling	k1gInSc1	Traveling
Wilburys	Wilburys	k1gInSc1	Wilburys
<g/>
"	"	kIx"	"
natočili	natočit	k5eAaBmAgMnP	natočit
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
George	Georg	k1gInSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
rakovině	rakovina	k1gFnSc3	rakovina
plic	plíce	k1gFnPc2	plíce
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
popel	popel	k1gInSc1	popel
nasypán	nasypán	k2eAgInSc1d1	nasypán
do	do	k7c2	do
indické	indický	k2eAgFnSc2d1	indická
posvátné	posvátný	k2eAgFnSc2d1	posvátná
řeky	řeka	k1gFnSc2	řeka
Gangy	Ganga	k1gFnSc2	Ganga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gFnPc2	jeho
instrukcí	instrukce	k1gFnPc2	instrukce
dokončili	dokončit	k5eAaPmAgMnP	dokončit
Jeff	Jeff	k1gInSc4	Jeff
Lynne	Lynn	k1gInSc5	Lynn
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Dhani	Dhaen	k2eAgMnPc1d1	Dhaen
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Jim	on	k3xPp3gMnPc3	on
Keltner	Keltnero	k1gNnPc2	Keltnero
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brainwashed	Brainwashed	k1gInSc1	Brainwashed
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používané	používaný	k2eAgInPc1d1	používaný
nástroje	nástroj	k1gInPc1	nástroj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
Harrison	Harrison	k1gMnSc1	Harrison
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnPc2	The
Quarrymen	Quarrymen	k1gInSc1	Quarrymen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	on	k3xPp3gMnSc4	on
hlavním	hlavní	k2eAgInSc7d1	hlavní
nástrojem	nástroj	k1gInSc7	nástroj
kytara	kytara	k1gFnSc1	kytara
Höfner	Höfner	k1gMnSc1	Höfner
President	president	k1gMnSc1	president
Acoustic	Acoustice	k1gFnPc2	Acoustice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
záhy	záhy	k6eAd1	záhy
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
za	za	k7c4	za
model	model	k1gInSc4	model
Höfner	Höfner	k1gInSc1	Höfner
Club	club	k1gInSc1	club
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
kytarou	kytara	k1gFnSc7	kytara
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
tělem	tělo	k1gNnSc7	tělo
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
nástroj	nástroj	k1gInSc1	nástroj
Jolana	Jolana	k1gFnSc1	Jolana
Futurama	Futurama	k?	Futurama
<g/>
/	/	kIx~	/
<g/>
Grazioso	grazioso	k1gNnSc1	grazioso
<g/>
.	.	kIx.	.
</s>
<s>
Kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
používal	používat	k5eAaImAgMnS	používat
při	při	k7c6	při
prvních	první	k4xOgFnPc6	první
nahrávkách	nahrávka	k1gFnPc6	nahrávka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
převážně	převážně	k6eAd1	převážně
modely	model	k1gInPc1	model
Gretsch	Gretscha	k1gFnPc2	Gretscha
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
hrál	hrát	k5eAaImAgMnS	hrát
přes	přes	k7c4	přes
zesilovač	zesilovač	k1gInSc4	zesilovač
Vox	Vox	k1gFnSc2	Vox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
Gretsch	Gretsch	k1gInSc4	Gretsch
6128	[number]	k4	6128
Duo	duo	k1gNnSc4	duo
Jet	jet	k2eAgInSc1d1	jet
–	–	k?	–
model	model	k1gInSc1	model
měl	mít	k5eAaImAgInS	mít
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
ho	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
od	od	k7c2	od
námořníka	námořník	k1gMnSc2	námořník
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
modelu	model	k1gInSc3	model
měl	mít	k5eAaImAgMnS	mít
záložní	záložní	k2eAgInSc4d1	záložní
model	model	k1gInSc4	model
Gretsch	Gretscha	k1gFnPc2	Gretscha
6131	[number]	k4	6131
Fire	Fire	k1gFnPc2	Fire
Bird	Bird	k1gMnSc1	Bird
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
jiné	jiný	k2eAgInPc4d1	jiný
knoflíky	knoflík	k1gInPc4	knoflík
a	a	k8xC	a
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
<g/>
:	:	kIx,	:
Gibson	Gibson	k1gInSc1	Gibson
EJ-160	EJ-160	k1gFnSc2	EJ-160
–	–	k?	–
model	model	k1gInSc1	model
měl	mít	k5eAaImAgInS	mít
černo-hnědo-žlutou	černonědo-žlutý	k2eAgFnSc4d1	černo-hnědo-žlutý
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Elektroakustická	elektroakustický	k2eAgFnSc1d1	elektroakustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
George	George	k1gInSc1	George
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
Gretsch	Gretsch	k1gInSc1	Gretsch
6122	[number]	k4	6122
Country	country	k2eAgMnSc1d1	country
Gentleman	gentleman	k1gMnSc1	gentleman
–	–	k?	–
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
ořechový	ořechový	k2eAgInSc4d1	ořechový
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
černý	černý	k1gMnSc1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
:	:	kIx,	:
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
posazené	posazený	k2eAgInPc4d1	posazený
knoflíky	knoflík	k1gInPc4	knoflík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
Gretsch	Gretsch	k1gInSc1	Gretsch
6119	[number]	k4	6119
Tennessee	Tennesse	k1gFnSc2	Tennesse
Ann	Ann	k1gFnSc1	Ann
–	–	k?	–
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
červený	červený	k2eAgInSc1d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
Rickenbacker	Rickenbacker	k1gInSc1	Rickenbacker
360	[number]	k4	360
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
–	–	k?	–
měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgInPc4	dva
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
byly	být	k5eAaImAgFnP	být
červeno-žluté	červeno-žlutý	k2eAgMnPc4d1	červeno-žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
hran	hrana	k1gFnPc2	hrana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
byl	být	k5eAaImAgInS	být
hranatý	hranatý	k2eAgMnSc1d1	hranatý
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
zakulacený	zakulacený	k2eAgMnSc1d1	zakulacený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
Epiphone	Epiphon	k1gMnSc5	Epiphon
E230	E230	k1gMnSc5	E230
TDV	TDV	kA	TDV
Casino	Casino	k1gNnSc1	Casino
–	–	k?	–
původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
černo-hnědo-žlutý	černonědo-žlutý	k2eAgInSc1d1	černo-hnědo-žlutý
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnPc1	diskografie
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Beatles	Beatles	k1gFnSc4	Beatles
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Wonderwall	Wonderwall	k1gMnSc1	Wonderwall
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
49	[number]	k4	49
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Electronic	Electronice	k1gFnPc2	Electronice
Sound	Sounda	k1gFnPc2	Sounda
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
191	[number]	k4	191
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
Things	Things	k1gInSc1	Things
Must	Must	k1gMnSc1	Must
Pass	Pass	k1gInSc1	Pass
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Living	Living	k1gInSc1	Living
in	in	k?	in
the	the	k?	the
Material	Material	k1gMnSc1	Material
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Dark	Dark	k1gInSc1	Dark
Horse	Horse	k1gFnSc2	Horse
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Extra	extra	k6eAd1	extra
Texture	Textur	k1gMnSc5	Textur
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Thirty	Thirt	k1gInPc1	Thirt
Three	Thre	k1gFnSc2	Thre
&	&	k?	&
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc4	Georg
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Somewhere	Somewhrat	k5eAaPmIp3nS	Somewhrat
in	in	k?	in
England	England	k1gInSc1	England
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Gone	Gone	k1gFnSc5	Gone
Troppo	Troppa	k1gFnSc5	Troppa
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
108	[number]	k4	108
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Cloud	Cloud	k1gInSc1	Cloud
Nine	Nin	k1gFnSc2	Nin
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Brainwashed	Brainwashed	k1gInSc1	Brainwashed
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
===	===	k?	===
Živé	živý	k2eAgFnPc1d1	živá
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Concert	Concert	k1gInSc4	Concert
For	forum	k1gNnPc2	forum
Bangla	Bangl	k1gMnSc2	Bangl
Desh	Desha	k1gFnPc2	Desha
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Live	Liv	k1gInPc1	Liv
In	In	k1gFnSc1	In
Japan	japan	k1gInSc1	japan
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
126	[number]	k4	126
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
George	Georg	k1gMnSc4	Georg
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
31	[number]	k4	31
</s>
</p>
<p>
<s>
Best	Best	k1gMnSc1	Best
of	of	k?	of
Dark	Dark	k1gInSc1	Dark
Horse	Horse	k1gFnSc1	Horse
1976-1989	[number]	k4	1976-1989
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
132	[number]	k4	132
</s>
</p>
<p>
<s>
The	The	k?	The
Dark	Dark	k1gMnSc1	Dark
Horse	Horse	k1gFnSc2	Horse
Years	Years	k1gInSc1	Years
1976-1992	[number]	k4	1976-1992
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Traveling	Traveling	k1gInSc4	Traveling
Wilburys	Wilburys	k1gInSc4	Wilburys
===	===	k?	===
</s>
</p>
<p>
<s>
Traveling	Traveling	k1gInSc1	Traveling
Wilburys	Wilburysa	k1gFnPc2	Wilburysa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
Traveling	Traveling	k1gInSc1	Traveling
Wilburys	Wilburysa	k1gFnPc2	Wilburysa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
US	US	kA	US
#	#	kIx~	#
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
CLIFFORD	CLIFFORD	kA	CLIFFORD
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnPc2	Mike
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
rocku	rock	k1gInSc2	rock
<g/>
:	:	kIx,	:
vyše	vyše	k6eAd1	vyše
700	[number]	k4	700
hesiel	hesiela	k1gFnPc2	hesiela
<g/>
:	:	kIx,	:
vyše	vyše	k6eAd1	vyše
500	[number]	k4	500
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
obalov	obalovo	k1gNnPc2	obalovo
platní	platný	k2eAgMnPc5d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Mladé	mladá	k1gFnPc1	mladá
letá	letý	k2eAgFnSc1d1	letá
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
394	[number]	k4	394
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERNÁ	Černá	k1gFnSc1	Černá
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
a	a	k8xC	a
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Poplach	poplach	k1gInSc1	poplach
kolem	kolem	k7c2	kolem
Beatles	Beatles	k1gFnSc2	Beatles
–	–	k?	–
liverpoolských	liverpoolský	k2eAgMnPc2d1	liverpoolský
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
notových	notový	k2eAgMnPc2d1	notový
analfabetů	analfabet	k1gMnPc2	analfabet
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
autorů	autor	k1gMnPc2	autor
...	...	k?	...
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panton	Panton	k1gInSc1	Panton
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
Edice	edice	k1gFnSc2	edice
přátel	přítel	k1gMnPc2	přítel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
OCLC	OCLC	kA	OCLC
24413638	[number]	k4	24413638
</s>
</p>
<p>
<s>
MATZNER	MATZNER	kA	MATZNER
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
:	:	kIx,	:
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
výpověď	výpověď	k1gFnSc1	výpověď
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
generaci	generace	k1gFnSc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
286	[number]	k4	286
s.	s.	k?	s.
<g/>
OCLC	OCLC	kA	OCLC
85618421	[number]	k4	85618421
OCLC	OCLC	kA	OCLC
39407088	[number]	k4	39407088
</s>
</p>
<p>
<s>
MATZNER	MATZNER	kA	MATZNER
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
<g/>
:	:	kIx,	:
výpověď	výpověď	k1gFnSc1	výpověď
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
generaci	generace	k1gFnSc6	generace
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dopl	dopl	k1gMnSc1	dopl
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
294	[number]	k4	294
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
s.	s.	k?	s.
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
581	[number]	k4	581
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
SCHMIEDEL	SCHMIEDEL	kA	SCHMIEDEL
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Opus	opus	k1gInSc1	opus
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
126	[number]	k4	126
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
<s>
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
</s>
</p>
<p>
<s>
Pattie	Pattie	k1gFnSc1	Pattie
Boyd	Boyda	k1gFnPc2	Boyda
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Harrison	Harrison	k1gMnSc1	Harrison
</s>
</p>
<p>
<s>
Traveling	Traveling	k1gInSc1	Traveling
Wilburys	Wilburysa	k1gFnPc2	Wilburysa
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
George	Georg	k1gMnSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc1	Commons
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
George	George	k1gFnPc2	George
Harrison	Harrison	k1gMnSc1	Harrison
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
georgeharrison	georgeharrison	k1gInSc1	georgeharrison
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
George	George	k6eAd1	George
Harrison	Harrison	k1gMnSc1	Harrison
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
