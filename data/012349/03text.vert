<p>
<s>
Datlík	datlík	k1gMnSc1	datlík
tříprstý	tříprstý	k2eAgMnSc1d1	tříprstý
(	(	kIx(	(
<g/>
Picoides	Picoides	k1gMnSc1	Picoides
tridactylus	tridactylus	k1gMnSc1	tridactylus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
šplhavce	šplhavec	k1gMnSc2	šplhavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
datlovitých	datlovitý	k2eAgFnPc2d1	datlovitý
(	(	kIx(	(
<g/>
Picidae	Picidae	k1gFnPc2	Picidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
velikosti	velikost	k1gFnSc3	velikost
strakapouda	strakapoud	k1gMnSc2	strakapoud
velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
21,5	[number]	k4	21,5
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
černavý	černavý	k2eAgMnSc1d1	černavý
<g/>
,	,	kIx,	,
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
za	za	k7c7	za
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
bíle	bíle	k6eAd1	bíle
proužkovaným	proužkovaný	k2eAgInSc7d1	proužkovaný
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Spodina	spodina	k1gFnSc1	spodina
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
černě	černě	k6eAd1	černě
proužkovaná	proužkovaný	k2eAgFnSc1d1	proužkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
černý	černý	k2eAgInSc1d1	černý
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
okraji	okraj	k1gInPc7	okraj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
černě	černě	k6eAd1	černě
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
temeno	temeno	k1gNnSc4	temeno
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
samice	samice	k1gFnPc1	samice
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prstů	prst	k1gInPc2	prst
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
datlík	datlík	k1gMnSc1	datlík
tříprstý	tříprstý	k2eAgMnSc1d1	tříprstý
výborný	výborný	k2eAgMnSc1d1	výborný
šplhavec	šplhavec	k1gMnSc1	šplhavec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
severských	severský	k2eAgInPc6d1	severský
lesích	les	k1gInPc6	les
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
až	až	k9	až
po	po	k7c4	po
Kamčatku	Kamčatka	k1gFnSc4	Kamčatka
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
areálu	areál	k1gInSc2	areál
souvislého	souvislý	k2eAgNnSc2d1	souvislé
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stálý	stálý	k2eAgInSc4d1	stálý
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Blanském	blanský	k2eAgInSc6d1	blanský
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
Novohradských	novohradský	k2eAgFnPc6d1	Novohradská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
(	(	kIx(	(
<g/>
novodobě	novodobě	k6eAd1	novodobě
od	od	k7c2	od
r.	r.	kA	r.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
odhad	odhad	k1gInSc1	odhad
početnosti	početnost	k1gFnSc2	početnost
z	z	k7c2	z
r.	r.	kA	r.
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
a	a	k8xC	a
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
hnízdění	hnízdění	k1gNnSc1	hnízdění
i	i	k9	i
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
i	i	k9	i
z	z	k7c2	z
Třemšínských	Třemšínský	k2eAgInPc2d1	Třemšínský
Brd	Brdy	k1gInPc2	Brdy
<g/>
;	;	kIx,	;
poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
lokalita	lokalita	k1gFnSc1	lokalita
Marásek	Marásek	k1gInSc1	Marásek
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
Brdech	Brdy	k1gInPc6	Brdy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vyfotografován	vyfotografován	k2eAgInSc1d1	vyfotografován
(	(	kIx(	(
<g/>
lokalita	lokalita	k1gFnSc1	lokalita
Pod	pod	k7c7	pod
Kamenem	kámen	k1gInSc7	kámen
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Chynín	Chynín	k1gInSc1	Chynín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
se	s	k7c7	s
starými	starý	k2eAgInPc7d1	starý
smrky	smrk	k1gInPc7	smrk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převládá	převládat	k5eAaImIp3nS	převládat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
dřevě	dřevo	k1gNnSc6	dřevo
žijící	žijící	k2eAgMnPc1d1	žijící
brouci	brouk	k1gMnPc1	brouk
(	(	kIx(	(
<g/>
kůrovci	kůrovec	k1gMnPc1	kůrovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
larvy	larva	k1gFnPc4	larva
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
také	také	k9	také
"	"	kIx"	"
<g/>
kroužkuje	kroužkovat	k5eAaImIp3nS	kroužkovat
<g/>
"	"	kIx"	"
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
požívá	požívat	k5eAaImIp3nS	požívat
vytékající	vytékající	k2eAgFnSc4d1	vytékající
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
monogamním	monogamní	k2eAgInSc7d1	monogamní
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
(	(	kIx(	(
<g/>
vletový	vletový	k2eAgInSc1d1	vletový
otvor	otvor	k1gInSc1	otvor
4,5	[number]	k4	4,5
×	×	k?	×
5	[number]	k4	5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
tesají	tesat	k5eAaImIp3nP	tesat
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
dutinu	dutina	k1gFnSc4	dutina
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
bývá	bývat	k5eAaImIp3nS	bývat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
čistě	čistě	k6eAd1	čistě
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
24,4	[number]	k4	24,4
×	×	k?	×
18,5	[number]	k4	18,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
11	[number]	k4	11
dnů	den	k1gInPc2	den
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
také	také	k9	také
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
opouští	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
po	po	k7c4	po
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgNnPc1d1	samostatné
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
až	až	k9	až
2	[number]	k4	2
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
datlík	datlík	k1gMnSc1	datlík
tříprstý	tříprstý	k2eAgMnSc1d1	tříprstý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
datlík	datlík	k1gMnSc1	datlík
tříprstý	tříprstý	k2eAgMnSc1d1	tříprstý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Picoides	Picoides	k1gInSc1	Picoides
tridactylus	tridactylus	k1gInSc1	tridactylus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
