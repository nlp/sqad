<p>
<s>
Vodotrysk	vodotrysk	k1gInSc1	vodotrysk
je	on	k3xPp3gNnPc4	on
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
vytlačována	vytlačován	k2eAgFnSc1d1	vytlačována
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ústí	ústit	k5eAaImIp3nS	ústit
z	z	k7c2	z
trysky	tryska	k1gFnSc2	tryska
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Tryskající	tryskající	k2eAgFnSc1d1	tryskající
voda	voda	k1gFnSc1	voda
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
estetickou	estetický	k2eAgFnSc4d1	estetická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Vodotrysky	vodotrysk	k1gInPc1	vodotrysk
bývají	bývat	k5eAaImIp3nP	bývat
součástí	součást	k1gFnSc7	součást
zejména	zejména	k6eAd1	zejména
fontán	fontána	k1gFnPc2	fontána
<g/>
,	,	kIx,	,
kašen	kašna	k1gFnPc2	kašna
<g/>
,	,	kIx,	,
zahradních	zahradní	k2eAgNnPc2d1	zahradní
a	a	k8xC	a
parkových	parkový	k2eAgNnPc2d1	parkové
jezírek	jezírko	k1gNnPc2	jezírko
i	i	k8xC	i
domácích	domácí	k2eAgInPc2d1	domácí
osvěžovačů	osvěžovač	k1gInPc2	osvěžovač
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
prvkem	prvek	k1gInSc7	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
vodotrysky	vodotrysk	k1gInPc1	vodotrysk
==	==	k?	==
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
přírodním	přírodní	k2eAgInSc7d1	přírodní
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgInPc4d1	přírodní
vodotrysky	vodotrysk	k1gInPc4	vodotrysk
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
přirozených	přirozený	k2eAgNnPc2d1	přirozené
vývěrů	vývěr	k1gInPc2	vývěr
spodních	spodní	k2eAgFnPc2d1	spodní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc4	takovýto
vodotrysky	vodotrysk	k1gInPc4	vodotrysk
obvykle	obvykle	k6eAd1	obvykle
označujeme	označovat	k5eAaImIp1nP	označovat
slovem	slovem	k6eAd1	slovem
gejzír	gejzír	k1gInSc4	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
přírodní	přírodní	k2eAgFnSc4d1	přírodní
podzemní	podzemní	k2eAgFnSc4d1	podzemní
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
vývěrem	vývěr	k1gInSc7	vývěr
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
o	o	k7c4	o
vývěr	vývěr	k1gInSc4	vývěr
termálních	termální	k2eAgInPc2d1	termální
pramenů	pramen	k1gInPc2	pramen
např.	např.	kA	např.
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
===	===	k?	===
</s>
</p>
<p>
<s>
Kiama	Kiama	k1gFnSc1	Kiama
Blow	Blow	k1gFnSc1	Blow
Hole	hole	k1gFnSc1	hole
-	-	kIx~	-
přírodní	přírodní	k2eAgInSc1d1	přírodní
vodotrysk	vodotrysk	k1gInSc1	vodotrysk
u	u	k7c2	u
australského	australský	k2eAgNnSc2d1	Australské
města	město	k1gNnSc2	město
Kiama	Kiamum	k1gNnSc2	Kiamum
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Vodotrysk	vodotrysk	k1gInSc1	vodotrysk
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Rajecká	Rajecká	k1gFnSc1	Rajecká
Lesná	lesný	k2eAgFnSc1d1	Lesná
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Strážovské	Strážovský	k2eAgInPc4d1	Strážovský
vrchy	vrch	k1gInPc4	vrch
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Gejzír	gejzír	k1gInSc1	gejzír
Herľany	Herľana	k1gFnSc2	Herľana
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
asi	asi	k9	asi
28	[number]	k4	28
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Košic	Košice	k1gInPc2	Košice
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
termální	termální	k2eAgInPc1d1	termální
vývěry	vývěr	k1gInPc1	vývěr
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodotrysky	vodotrysk	k1gInPc4	vodotrysk
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vodovodních	vodovodní	k2eAgFnPc2d1	vodovodní
poruch	porucha	k1gFnPc2	porucha
==	==	k?	==
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc1d1	velký
vodotrysky	vodotrysk	k1gInPc1	vodotrysk
a	a	k8xC	a
gejzíry	gejzír	k1gInPc1	gejzír
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
někdy	někdy	k6eAd1	někdy
nechtěně	chtěně	k6eNd1	chtěně
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poruchy	porucha	k1gFnSc2	porucha
či	či	k8xC	či
havárie	havárie	k1gFnSc2	havárie
na	na	k7c6	na
vodovodním	vodovodní	k2eAgNnSc6d1	vodovodní
potrubí	potrubí	k1gNnSc6	potrubí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
užitková	užitkový	k2eAgFnSc1d1	užitková
voda	voda	k1gFnSc1	voda
tryská	tryskat	k5eAaImIp3nS	tryskat
otvorem	otvor	k1gInSc7	otvor
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fontána	fontána	k1gFnSc1	fontána
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
</s>
</p>
<p>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
</s>
</p>
<p>
<s>
Chrlič	chrlič	k1gMnSc1	chrlič
</s>
</p>
