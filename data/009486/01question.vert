<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
vytlačována	vytlačován	k2eAgFnSc1d1	vytlačována
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ústí	ústit	k5eAaImIp3nS	ústit
z	z	k7c2	z
trysky	tryska	k1gFnSc2	tryska
ven	ven	k6eAd1	ven
<g/>
?	?	kIx.	?
</s>
