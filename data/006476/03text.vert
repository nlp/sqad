<s>
Platón	Platón	k1gMnSc1	Platón
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Π	Π	k?	Π
<g/>
,	,	kIx,	,
427	[number]	k4	427
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
347	[number]	k4	347
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Platón	Platón	k1gMnSc1	Platón
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
obecně	obecně	k6eAd1	obecně
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
pseudonym	pseudonym	k1gInSc4	pseudonym
tohoto	tento	k3xDgMnSc2	tento
filosofa	filosof	k1gMnSc2	filosof
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Aristoklés	Aristoklés	k1gInSc4	Aristoklés
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Aristóna	Aristóna	k1gFnSc1	Aristóna
a	a	k8xC	a
Periktiony	Periktion	k1gInPc1	Periktion
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
myslitelů	myslitel	k1gMnPc2	myslitel
vůbec	vůbec	k9	vůbec
<g/>
;	;	kIx,	;
britský	britský	k2eAgMnSc1d1	britský
filosof	filosof	k1gMnSc1	filosof
A.	A.	kA	A.
N.	N.	kA	N.
Whitehead	Whitehead	k1gInSc1	Whitehead
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
západní	západní	k2eAgFnSc1d1	západní
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
Platónovi	Platón	k1gMnSc3	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
založil	založit	k5eAaPmAgMnS	založit
athénskou	athénský	k2eAgFnSc4d1	Athénská
Akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vzorem	vzor	k1gInSc7	vzor
evropským	evropský	k2eAgFnPc3d1	Evropská
univerzitám	univerzita	k1gFnPc3	univerzita
a	a	k8xC	a
vědeckým	vědecký	k2eAgFnPc3d1	vědecká
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
spisy	spis	k1gInPc4	spis
psal	psát	k5eAaImAgInS	psát
většinou	většinou	k6eAd1	většinou
formou	forma	k1gFnSc7	forma
rozprav	rozprava	k1gFnPc2	rozprava
mezi	mezi	k7c7	mezi
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
Sókratem	Sókrat	k1gMnSc7	Sókrat
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
Sókratés	Sókratés	k1gInSc1	Sókratés
svými	svůj	k3xOyFgFnPc7	svůj
otázkami	otázka	k1gFnPc7	otázka
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
jejich	jejich	k3xOp3gInPc4	jejich
předsudečné	předsudečný	k2eAgInPc4d1	předsudečný
a	a	k8xC	a
nezralé	zralý	k2eNgInPc4d1	nezralý
názory	názor	k1gInPc4	názor
a	a	k8xC	a
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
poznání	poznání	k1gNnSc3	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sókratem	Sókrat	k1gMnSc7	Sókrat
obrátili	obrátit	k5eAaPmAgMnP	obrátit
pozornost	pozornost	k1gFnSc4	pozornost
filosofů	filosof	k1gMnPc2	filosof
od	od	k7c2	od
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
a	a	k8xC	a
původu	původ	k1gInSc6	původ
světa	svět	k1gInSc2	svět
k	k	k7c3	k
otázkám	otázka	k1gFnPc3	otázka
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ústředními	ústřední	k2eAgNnPc7d1	ústřední
tématy	téma	k1gNnPc7	téma
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
pravým	pravý	k2eAgNnSc7d1	pravé
poznáním	poznání	k1gNnSc7	poznání
a	a	k8xC	a
pouhým	pouhý	k2eAgNnSc7d1	pouhé
míněním	mínění	k1gNnSc7	mínění
<g/>
;	;	kIx,	;
ctnost	ctnost	k1gFnSc4	ctnost
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
výchovy	výchova	k1gFnSc2	výchova
ke	k	k7c3	k
ctnosti	ctnost	k1gFnSc3	ctnost
<g/>
;	;	kIx,	;
spravedlivé	spravedlivý	k2eAgNnSc4d1	spravedlivé
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
trvalé	trvalý	k2eAgNnSc4d1	trvalé
uspořádání	uspořádání	k1gNnSc4	uspořádání
obce	obec	k1gFnSc2	obec
<g/>
;	;	kIx,	;
dobro	dobro	k1gNnSc1	dobro
jako	jako	k8xS	jako
konečný	konečný	k2eAgInSc1d1	konečný
cíl	cíl	k1gInSc1	cíl
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
měsíční	měsíční	k2eAgInSc1d1	měsíční
kráter	kráter	k1gInSc1	kráter
Plato	plato	k1gNnSc1	plato
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
428	[number]	k4	428
nebo	nebo	k8xC	nebo
427	[number]	k4	427
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
athénských	athénský	k2eAgFnPc2d1	Athénská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Aristón	Aristón	k1gMnSc1	Aristón
prý	prý	k9	prý
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
athénského	athénský	k2eAgMnSc2d1	athénský
krále	král	k1gMnSc2	král
Kodra	Kodra	k1gFnSc1	Kodra
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Periktioné	Periktioná	k1gFnSc2	Periktioná
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
slavného	slavný	k2eAgMnSc4d1	slavný
básníka	básník	k1gMnSc4	básník
a	a	k8xC	a
zákonodárce	zákonodárce	k1gMnSc4	zákonodárce
Solóna	Solón	k1gMnSc4	Solón
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
bratři	bratr	k1gMnPc1	bratr
Charmidés	Charmidésa	k1gFnPc2	Charmidésa
a	a	k8xC	a
Kritiás	Kritiása	k1gFnPc2	Kritiása
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
třiceti	třicet	k4xCc2	třicet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Platónovi	Platónův	k2eAgMnPc1d1	Platónův
bratři	bratr	k1gMnPc1	bratr
Adeimantos	Adeimantos	k1gMnSc1	Adeimantos
a	a	k8xC	a
Glaukón	Glaukón	k1gMnSc1	Glaukón
také	také	k9	také
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dialozích	dialog	k1gInPc6	dialog
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Pótóné	Pótóná	k1gFnSc2	Pótóná
byla	být	k5eAaImAgFnS	být
matka	matka	k1gFnSc1	matka
filosofa	filosof	k1gMnSc2	filosof
Speusippa	Speusipp	k1gMnSc2	Speusipp
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
Platónově	Platónův	k2eAgFnSc6d1	Platónova
smrti	smrt	k1gFnSc6	smrt
vedl	vést	k5eAaImAgMnS	vést
Akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Aristón	Aristón	k1gMnSc1	Aristón
patrně	patrně	k6eAd1	patrně
brzy	brzy	k6eAd1	brzy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Periktioné	Periktioné	k1gNnSc4	Periktioné
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
Pyrilampa	Pyrilamp	k1gMnSc4	Pyrilamp
<g/>
,	,	kIx,	,
diplomata	diplomat	k1gMnSc2	diplomat
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Perikleova	Perikleův	k2eAgMnSc2d1	Perikleův
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Antifóna	Antifón	k1gMnSc4	Antifón
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
o	o	k7c6	o
sobě	se	k3xPyFc3	se
téměř	téměř	k6eAd1	téměř
nepíše	psát	k5eNaImIp3nS	psát
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
všem	všecek	k3xTgMnPc3	všecek
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
sókratovských	sókratovský	k2eAgInPc6d1	sókratovský
dialozích	dialog	k1gInPc6	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Diogena	Diogenes	k1gMnSc2	Diogenes
Laertia	Laertius	k1gMnSc2	Laertius
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Aristoklés	Aristoklés	k1gInSc1	Aristoklés
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
dědovi	děd	k1gMnSc6	děd
<g/>
,	,	kIx,	,
přezdívku	přezdívka	k1gFnSc4	přezdívka
Platón	platón	k1gInSc1	platón
(	(	kIx(	(
<g/>
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
mohutný	mohutný	k2eAgInSc1d1	mohutný
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
dal	dát	k5eAaPmAgInS	dát
jeho	jeho	k3xOp3gMnSc4	jeho
zápasnický	zápasnický	k2eAgMnSc1d1	zápasnický
trenér	trenér	k1gMnSc1	trenér
<g/>
;	;	kIx,	;
jiní	jiný	k1gMnPc1	jiný
ji	on	k3xPp3gFnSc4	on
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
nezvykle	zvykle	k6eNd1	zvykle
širokého	široký	k2eAgNnSc2d1	široké
čela	čelo	k1gNnSc2	čelo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
dostal	dostat	k5eAaPmAgMnS	dostat
vynikající	vynikající	k2eAgMnSc1d1	vynikající
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
gymnastice	gymnastika	k1gFnSc3	gymnastika
a	a	k8xC	a
s	s	k7c7	s
filosofií	filosofie	k1gFnSc7	filosofie
jej	on	k3xPp3gMnSc4	on
seznámil	seznámit	k5eAaPmAgInS	seznámit
Kratylos	Kratylos	k1gInSc1	Kratylos
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Hérakleitův	Hérakleitův	k2eAgMnSc1d1	Hérakleitův
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
Sókratem	Sókrat	k1gMnSc7	Sókrat
a	a	k8xC	a
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
mu	on	k3xPp3gMnSc3	on
naslouchal	naslouchat	k5eAaImAgInS	naslouchat
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
nejvěrnější	věrný	k2eAgMnSc1d3	nejvěrnější
žák	žák	k1gMnSc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
sám	sám	k3xTgMnSc1	sám
to	ten	k3xDgNnSc4	ten
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
nikde	nikde	k6eAd1	nikde
výslovně	výslovně	k6eAd1	výslovně
neříká	říkat	k5eNaImIp3nS	říkat
a	a	k8xC	a
podle	podle	k7c2	podle
dialogu	dialog	k1gInSc2	dialog
"	"	kIx"	"
<g/>
Faidón	Faidón	k1gInSc1	Faidón
<g/>
"	"	kIx"	"
nebyl	být	k5eNaImAgInS	být
při	při	k7c6	při
Sókratově	Sókratův	k2eAgFnSc6d1	Sókratova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
roku	rok	k1gInSc2	rok
399	[number]	k4	399
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
a	a	k8xC	a
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
snad	snad	k9	snad
Platón	Platón	k1gMnSc1	Platón
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
do	do	k7c2	do
Megary	Megara	k1gFnSc2	Megara
a	a	k8xC	a
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
do	do	k7c2	do
Kyrény	Kyréna	k1gFnSc2	Kyréna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
cestách	cesta	k1gFnPc6	cesta
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
tamními	tamní	k2eAgMnPc7d1	tamní
vládci	vládce	k1gMnPc7	vládce
Diónem	Dión	k1gMnSc7	Dión
a	a	k8xC	a
Dionýsem	Dionýsos	k1gMnSc7	Dionýsos
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
Sedmém	sedmý	k4xOgInSc6	sedmý
listu	list	k1gInSc6	list
<g/>
,	,	kIx,	,
jediném	jediný	k2eAgInSc6d1	jediný
zachovaném	zachovaný	k2eAgInSc6d1	zachovaný
autobiografickém	autobiografický	k2eAgInSc6d1	autobiografický
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
40	[number]	k4	40
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
snad	snad	k9	snad
roku	rok	k1gInSc2	rok
387	[number]	k4	387
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
v	v	k7c6	v
"	"	kIx"	"
<g/>
Akadémově	Akadémův	k2eAgInSc6d1	Akadémův
háji	háj	k1gInSc6	háj
<g/>
"	"	kIx"	"
založil	založit	k5eAaPmAgMnS	založit
slavnou	slavný	k2eAgFnSc4d1	slavná
Akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
529	[number]	k4	529
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
zrušil	zrušit	k5eAaPmAgMnS	zrušit
císař	císař	k1gMnSc1	císař
Justinián	Justinián	k1gMnSc1	Justinián
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
žáků	žák	k1gMnPc2	žák
Akademie	akademie	k1gFnSc2	akademie
byl	být	k5eAaImAgInS	být
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
,	,	kIx,	,
po	po	k7c6	po
Platónově	Platónův	k2eAgFnSc6d1	Platónova
smrti	smrt	k1gFnSc6	smrt
však	však	k8xC	však
Akademii	akademie	k1gFnSc6	akademie
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Speusippos	Speusippos	k1gMnSc1	Speusippos
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
82	[number]	k4	82
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
84	[number]	k4	84
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Soudě	soudě	k6eAd1	soudě
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
měl	mít	k5eAaImAgMnS	mít
Platón	Platón	k1gMnSc1	Platón
k	k	k7c3	k
Sókratovi	Sókrat	k1gMnSc3	Sókrat
velice	velice	k6eAd1	velice
blízko	blízko	k6eAd1	blízko
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
o	o	k7c6	o
Sókratovi	Sókrat	k1gMnSc6	Sókrat
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
víme	vědět	k5eAaImIp1nP	vědět
od	od	k7c2	od
Platóna	Platón	k1gMnSc2	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Ironické	ironický	k2eAgNnSc4d1	ironické
líčení	líčení	k1gNnSc4	líčení
Aristofanovo	Aristofanův	k2eAgNnSc4d1	Aristofanův
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k6eAd1	mnoho
nepřidává	přidávat	k5eNaImIp3nS	přidávat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dialozích	dialog	k1gInPc6	dialog
se	se	k3xPyFc4	se
Platón	platón	k1gInSc1	platón
tváří	tvářet	k5eAaImIp3nS	tvářet
jako	jako	k9	jako
pouhý	pouhý	k2eAgMnSc1d1	pouhý
tlumočník	tlumočník	k1gMnSc1	tlumočník
myšlenek	myšlenka	k1gFnPc2	myšlenka
Sókratových	Sókratův	k2eAgFnPc2d1	Sókratova
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc4	nic
nenapsal	napsat	k5eNaPmAgMnS	napsat
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Sedmém	sedmý	k4xOgInSc6	sedmý
listu	list	k1gInSc6	list
dokonce	dokonce	k9	dokonce
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
vlastních	vlastní	k2eAgFnPc6d1	vlastní
myšlenkách	myšlenka	k1gFnPc6	myšlenka
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
nenapsal	napsat	k5eNaPmAgMnS	napsat
a	a	k8xC	a
nenapíše	napsat	k5eNaPmIp3nS	napsat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
filosofie	filosofie	k1gFnSc1	filosofie
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vyučovat	vyučovat	k5eAaImF	vyučovat
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
a	a	k8xC	a
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
"	"	kIx"	"
<g/>
úsilí	úsilí	k1gNnSc6	úsilí
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
Sókratovy	Sókratův	k2eAgInPc1d1	Sókratův
názory	názor	k1gInPc1	názor
a	a	k8xC	a
postoje	postoj	k1gInPc1	postoj
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dialozích	dialog	k1gInPc6	dialog
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
Obrany	obrana	k1gFnSc2	obrana
Platón	platón	k1gInSc1	platón
nikde	nikde	k6eAd1	nikde
netvrdí	tvrdit	k5eNaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
byl	být	k5eAaImAgMnS	být
slyšel	slyšet	k5eAaImAgMnS	slyšet
z	z	k7c2	z
první	první	k4xOgFnSc2	první
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
je	on	k3xPp3gInPc4	on
máme	mít	k5eAaImIp1nP	mít
připisovat	připisovat	k5eAaImF	připisovat
spíše	spíše	k9	spíše
Platónovi	Platónův	k2eAgMnPc1d1	Platónův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dialozích	dialog	k1gInPc6	dialog
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Menón	Menón	k1gInSc1	Menón
<g/>
,	,	kIx,	,
Faidros	Faidrosa	k1gFnPc2	Faidrosa
<g/>
,	,	kIx,	,
Kritón	Kritón	k1gInSc1	Kritón
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
hovoří	hovořit	k5eAaImIp3nS	hovořit
Sókratés	Sókratés	k1gInSc1	Sókratés
v	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
(	(	kIx(	(
<g/>
Lysis	Lysis	k1gInSc1	Lysis
<g/>
,	,	kIx,	,
Charmidés	Charmidés	k1gInSc1	Charmidés
<g/>
,	,	kIx,	,
Ústava	ústava	k1gFnSc1	ústava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Prótagorovi	Prótagor	k1gMnSc6	Prótagor
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
předchozí	předchozí	k2eAgFnSc6d1	předchozí
rozpravě	rozprava	k1gFnSc6	rozprava
s	s	k7c7	s
Prótagorou	Prótagora	k1gFnSc7	Prótagora
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
Faidón	Faidón	k1gInSc4	Faidón
<g/>
,	,	kIx,	,
Symposion	symposion	k1gNnSc4	symposion
a	a	k8xC	a
Theaitétos	Theaitétos	k1gInSc4	Theaitétos
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
Sókratovi	Sókratův	k2eAgMnPc1d1	Sókratův
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
Platón	Platón	k1gMnSc1	Platón
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Theaitéta	Theaitét	k1gInSc2	Theaitét
<g/>
)	)	kIx)	)
nijak	nijak	k6eAd1	nijak
nenaznačuje	naznačovat	k5eNaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
vůbec	vůbec	k9	vůbec
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
biografií	biografie	k1gFnPc2	biografie
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
najevo	najevo	k6eAd1	najevo
značné	značný	k2eAgFnPc4d1	značná
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
v	v	k7c6	v
Prótagorovi	Prótagor	k1gMnSc6	Prótagor
jsou	být	k5eAaImIp3nP	být
Alkibiadés	Alkibiadés	k1gInSc1	Alkibiadés
a	a	k8xC	a
Agathón	Agathón	k1gMnSc1	Agathón
mladíci	mladík	k1gMnPc1	mladík
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Apollodóros	Apollodórosa	k1gFnPc2	Apollodórosa
a	a	k8xC	a
Glaukón	Glaukón	k1gInSc4	Glaukón
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gMnSc3	jejich
otci	otec	k1gMnSc3	otec
<g/>
;	;	kIx,	;
v	v	k7c6	v
Symposiu	symposion	k1gNnSc6	symposion
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nesrovnalosti	nesrovnalost	k1gFnPc1	nesrovnalost
patrně	patrně	k6eAd1	patrně
nejsou	být	k5eNaImIp3nP	být
nahodilé	nahodilý	k2eAgInPc1d1	nahodilý
a	a	k8xC	a
Platón	platón	k1gInSc1	platón
tedy	tedy	k9	tedy
nepíše	psát	k5eNaImIp3nS	psát
jako	jako	k9	jako
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
podobají	podobat	k5eAaImIp3nP	podobat
dobovému	dobový	k2eAgNnSc3d1	dobové
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
nejvýše	nejvýše	k6eAd1	nejvýše
tři	tři	k4xCgFnPc1	tři
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
sbor	sbor	k1gInSc1	sbor
<g/>
"	"	kIx"	"
mlčících	mlčící	k2eAgMnPc2d1	mlčící
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Sókratův	Sókratův	k2eAgInSc1d1	Sókratův
proces	proces	k1gInSc1	proces
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
tématem	téma	k1gNnSc7	téma
tří	tři	k4xCgInPc2	tři
dialogů	dialog	k1gInPc2	dialog
(	(	kIx(	(
<g/>
Obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
Kritón	Kritón	k1gInSc1	Kritón
a	a	k8xC	a
Faidón	Faidón	k1gInSc1	Faidón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
také	také	k9	také
nejčastěji	často	k6eAd3	často
čtou	číst	k5eAaImIp3nP	číst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
dalších	další	k1gNnPc6	další
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
narážky	narážka	k1gFnPc1	narážka
a	a	k8xC	a
varování	varování	k1gNnSc1	varování
<g/>
:	:	kIx,	:
v	v	k7c6	v
Theaitétovi	Theaitét	k1gMnSc6	Theaitét
a	a	k8xC	a
v	v	k7c6	v
Euthyfrónovi	Euthyfrón	k1gMnSc6	Euthyfrón
mluví	mluvit	k5eAaImIp3nS	mluvit
Sókratés	Sókratés	k1gInSc1	Sókratés
o	o	k7c6	o
pomluvách	pomluva	k1gFnPc6	pomluva
a	a	k8xC	a
obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
;	;	kIx,	;
v	v	k7c6	v
Menónovi	Menón	k1gMnSc6	Menón
ho	on	k3xPp3gMnSc4	on
varuje	varovat	k5eAaImIp3nS	varovat
Anytos	Anytos	k1gInSc1	Anytos
<g/>
,	,	kIx,	,
v	v	k7c6	v
Gorgiovi	Gorgius	k1gMnSc6	Gorgius
a	a	k8xC	a
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
musí	muset	k5eAaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
dialogy	dialog	k1gInPc1	dialog
spojují	spojovat	k5eAaImIp3nP	spojovat
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
pokaždé	pokaždé	k6eAd1	pokaždé
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
v	v	k7c6	v
Obraně	obrana	k1gFnSc6	obrana
si	se	k3xPyFc3	se
Sókratés	Sókratés	k1gInSc1	Sókratés
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
Aristofanés	Aristofanés	k1gInSc4	Aristofanés
zostudil	zostudit	k5eAaPmAgMnS	zostudit
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
jeho	jeho	k3xOp3gInSc4	jeho
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
Symposiu	symposion	k1gNnSc6	symposion
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
přáteli	přítel	k1gMnPc7	přítel
hodují	hodovat	k5eAaImIp3nP	hodovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kratylu	Kratyl	k1gInSc6	Kratyl
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
chválí	chválit	k5eAaImIp3nS	chválit
Euthyfrónovu	Euthyfrónův	k2eAgFnSc4d1	Euthyfrónův
moudrost	moudrost	k1gFnSc4	moudrost
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
Euthyfrónovi	Euthyfrón	k1gMnSc6	Euthyfrón
jej	on	k3xPp3gMnSc4	on
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
a	a	k8xC	a
karikuje	karikovat	k5eAaImIp3nS	karikovat
<g/>
.	.	kIx.	.
</s>
<s>
Sókratův	Sókratův	k2eAgInSc1d1	Sókratův
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
o	o	k7c6	o
dobových	dobový	k2eAgFnPc6d1	dobová
Athénách	Athéna	k1gFnPc6	Athéna
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
pochopitelný	pochopitelný	k2eAgInSc1d1	pochopitelný
<g/>
.	.	kIx.	.
</s>
<s>
Bezbožnost	bezbožnost	k1gFnSc1	bezbožnost
a	a	k8xC	a
ateismus	ateismus	k1gInSc1	ateismus
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
století	století	k1gNnSc6	století
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
zločinem	zločin	k1gInSc7	zločin
<g/>
,	,	kIx,	,
Aristofanovy	Aristofanův	k2eAgFnSc2d1	Aristofanova
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
ze	z	k7c2	z
ctitelů	ctitel	k1gMnPc2	ctitel
božstev	božstvo	k1gNnPc2	božstvo
dělá	dělat	k5eAaImIp3nS	dělat
legraci	legrace	k1gFnSc4	legrace
<g/>
,	,	kIx,	,
dostávaly	dostávat	k5eAaImAgFnP	dostávat
ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
někdy	někdy	k6eAd1	někdy
čelil	čelit	k5eAaImAgInS	čelit
nějakému	nějaký	k3yIgNnSc3	nějaký
obvinění	obvinění	k1gNnSc3	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Sókratés	Sókratés	k1gInSc1	Sókratés
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
filosofické	filosofický	k2eAgNnSc4d1	filosofické
poslání	poslání	k1gNnSc4	poslání
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
na	na	k7c4	na
Apollóna	Apollón	k1gMnSc4	Apollón
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
athénských	athénský	k2eAgMnPc2d1	athénský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Platónova	Platónův	k2eAgFnSc1d1	Platónova
nauka	nauka	k1gFnSc1	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Platónova	Platónův	k2eAgNnPc1d1	Platónovo
díla	dílo	k1gNnPc1	dílo
mají	mít	k5eAaImIp3nP	mít
vesměs	vesměs	k6eAd1	vesměs
formu	forma	k1gFnSc4	forma
dialogu	dialog	k1gInSc2	dialog
(	(	kIx(	(
<g/>
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většinou	většinou	k6eAd1	většinou
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Sókratés	Sókratés	k1gInSc1	Sókratés
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
představuje	představovat	k5eAaImIp3nS	představovat
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
soubor	soubor	k1gInSc1	soubor
Platónových	Platónův	k2eAgInPc2d1	Platónův
Listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zejména	zejména	k9	zejména
Sedmý	sedmý	k4xOgInSc1	sedmý
list	list	k1gInSc1	list
vrhá	vrhat	k5eAaImIp3nS	vrhat
na	na	k7c4	na
Platónův	Platónův	k2eAgInSc4d1	Platónův
život	život	k1gInSc4	život
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojetí	pojetí	k1gNnSc4	pojetí
filosofie	filosofie	k1gFnSc1	filosofie
jiné	jiný	k2eAgNnSc4d1	jiné
světlo	světlo	k1gNnSc4	světlo
<g/>
:	:	kIx,	:
filosofii	filosofie	k1gFnSc4	filosofie
nelze	lze	k6eNd1	lze
sepsat	sepsat	k5eAaPmF	sepsat
jako	jako	k9	jako
systém	systém	k1gInSc4	systém
ani	ani	k8xC	ani
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
dobývat	dobývat	k5eAaImF	dobývat
vlastním	vlastní	k2eAgNnSc7d1	vlastní
myšlenkovým	myšlenkový	k2eAgNnSc7d1	myšlenkové
úsilím	úsilí	k1gNnSc7	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pravosti	pravost	k1gFnSc6	pravost
Sedmého	sedmý	k4xOgInSc2	sedmý
listu	list	k1gInSc2	list
mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
pochybovali	pochybovat	k5eAaImAgMnP	pochybovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
bádání	bádání	k1gNnSc6	bádání
se	se	k3xPyFc4	se
však	však	k9	však
stává	stávat	k5eAaImIp3nS	stávat
naopak	naopak	k6eAd1	naopak
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
Platónova	Platónův	k2eAgNnSc2d1	Platónovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Platónova	Platónův	k2eAgNnPc1d1	Platónovo
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
citují	citovat	k5eAaBmIp3nP	citovat
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
stránky	stránka	k1gFnSc2	stránka
a	a	k8xC	a
sloupce	sloupec	k1gInSc2	sloupec
či	či	k8xC	či
úseku	úsek	k1gInSc2	úsek
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
francouzského	francouzský	k2eAgMnSc2d1	francouzský
tiskaře	tiskař	k1gMnSc2	tiskař
H.	H.	kA	H.
Étienne	Étienn	k1gInSc5	Étienn
(	(	kIx(	(
<g/>
Stephanus	Stephanus	k1gInSc4	Stephanus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
blíže	blízce	k6eAd2	blízce
viz	vidět	k5eAaImRp2nS	vidět
Citování	citování	k1gNnSc3	citování
filosofické	filosofický	k2eAgFnSc2d1	filosofická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dochované	dochovaný	k2eAgInPc1d1	dochovaný
Platónovy	Platónův	k2eAgInPc1d1	Platónův
dialogy	dialog	k1gInPc1	dialog
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
čtveřic	čtveřice	k1gFnPc2	čtveřice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tetralogií	tetralogie	k1gFnSc7	tetralogie
<g/>
;	;	kIx,	;
dialogy	dialog	k1gInPc1	dialog
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
autorství	autorství	k1gNnSc6	autorství
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
křížkem	křížek	k1gInSc7	křížek
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc4	název
dialogů	dialog	k1gInPc2	dialog
užíváme	užívat	k5eAaImIp1nP	užívat
podle	podle	k7c2	podle
překladu	překlad	k1gInSc2	překlad
F.	F.	kA	F.
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
:	:	kIx,	:
I.	I.	kA	I.
Euthyfrón	Euthyfrón	k1gInSc4	Euthyfrón
<g/>
,	,	kIx,	,
Obrana	obrana	k1gFnSc1	obrana
Sókrata	Sókrat	k1gMnSc2	Sókrat
(	(	kIx(	(
<g/>
také	také	k9	také
Apologie	apologie	k1gFnSc2	apologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kritón	Kritón	k1gInSc4	Kritón
<g/>
,	,	kIx,	,
Faidón	Faidón	k1gInSc4	Faidón
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kratylos	Kratylos	k1gInSc1	Kratylos
<g/>
,	,	kIx,	,
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
,	,	kIx,	,
Sofistés	Sofistés	k1gInSc1	Sofistés
<g/>
,	,	kIx,	,
Politikos	Politikos	k1gInSc1	Politikos
(	(	kIx(	(
<g/>
také	také	k9	také
Státník	státník	k1gMnSc1	státník
<g/>
)	)	kIx)	)
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
<g/>
,	,	kIx,	,
Filébos	Filébos	k1gInSc1	Filébos
<g/>
,	,	kIx,	,
Symposion	symposion	k1gNnSc1	symposion
(	(	kIx(	(
<g/>
také	také	k9	také
Hostina	hostina	k1gFnSc1	hostina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Faidros	Faidrosa	k1gFnPc2	Faidrosa
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Alkibiadés	Alkibiadés	k1gInSc1	Alkibiadés
I.	I.	kA	I.
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Alkibiadés	Alkibiadés	k1gInSc1	Alkibiadés
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Hipparchos	Hipparchos	k1gInSc1	Hipparchos
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Milovníci	milovník	k1gMnPc1	milovník
V.	V.	kA	V.
+	+	kIx~	+
<g/>
Theagés	Theagés	k1gInSc1	Theagés
<g/>
,	,	kIx,	,
Charmidés	Charmidés	k1gInSc1	Charmidés
<g/>
,	,	kIx,	,
Lachés	Lachés	k1gInSc1	Lachés
<g/>
,	,	kIx,	,
Lysis	Lysis	k1gInSc1	Lysis
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Euthydémos	Euthydémos	k1gInSc1	Euthydémos
<g/>
,	,	kIx,	,
Prótagorás	Prótagorás	k1gInSc1	Prótagorás
<g/>
,	,	kIx,	,
Gorgiás	Gorgiás	k1gInSc1	Gorgiás
<g/>
,	,	kIx,	,
Menón	Menón	k1gInSc1	Menón
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Hippiás	Hippiás	k6eAd1	Hippiás
Větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
Hippiás	Hippiás	k1gInSc1	Hippiás
Menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
Ión	Ión	k1gMnSc1	Ión
<g/>
,	,	kIx,	,
Menexenos	Menexenos	k1gMnSc1	Menexenos
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Kleitofón	Kleitofón	k1gInSc1	Kleitofón
<g/>
,	,	kIx,	,
Ústava	ústava	k1gFnSc1	ústava
(	(	kIx(	(
<g/>
také	také	k9	také
Republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tímaios	Tímaios	k1gInSc1	Tímaios
<g/>
,	,	kIx,	,
Kritiás	Kritiás	k1gInSc1	Kritiás
IX	IX	kA	IX
<g/>
.	.	kIx.	.
+	+	kIx~	+
<g/>
Minós	Minós	k1gInSc1	Minós
<g/>
,	,	kIx,	,
Zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Epinomis	Epinomis	k1gFnPc4	Epinomis
<g/>
,	,	kIx,	,
Listy	list	k1gInPc4	list
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Platónovým	Platónův	k2eAgNnSc7d1	Platónovo
jménem	jméno	k1gNnSc7	jméno
tradovala	tradovat	k5eAaImAgFnS	tradovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
apokryfních	apokryfní	k2eAgInPc2d1	apokryfní
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
autorství	autorství	k1gNnSc6	autorství
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
pochybovalo	pochybovat	k5eAaImAgNnS	pochybovat
už	už	k9	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
tetralogiích	tetralogie	k1gFnPc6	tetralogie
zahrnuty	zahrnut	k2eAgInPc4d1	zahrnut
<g/>
:	:	kIx,	:
+	+	kIx~	+
<g/>
Axiochos	Axiochos	k1gInSc1	Axiochos
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Definice	definice	k1gFnSc1	definice
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Démodokos	Démodokos	k1gInSc1	Démodokos
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Epigramy	epigram	k1gInPc4	epigram
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Eryxiás	Eryxiás	k1gInSc1	Eryxiás
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Halkyón	Halkyón	k1gInSc1	Halkyón
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
O	o	k7c6	o
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
O	o	k7c4	o
ctnosti	ctnost	k1gFnPc4	ctnost
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
<g/>
.	.	kIx.	.
</s>
<s>
Časové	časový	k2eAgNnSc1d1	časové
pořadí	pořadí	k1gNnSc1	pořadí
vzniku	vznik	k1gInSc2	vznik
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dialogů	dialog	k1gInPc2	dialog
neznáme	znát	k5eNaImIp1nP	znát
a	a	k8xC	a
nevíme	vědět	k5eNaImIp1nP	vědět
ani	ani	k8xC	ani
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byly	být	k5eAaImAgFnP	být
dodatečně	dodatečně	k6eAd1	dodatečně
redigovány	redigován	k2eAgFnPc1d1	redigována
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
časové	časový	k2eAgNnSc4d1	časové
uspořádání	uspořádání	k1gNnSc4	uspořádání
udělal	udělat	k5eAaPmAgMnS	udělat
významný	významný	k2eAgMnSc1d1	významný
německý	německý	k2eAgMnSc1d1	německý
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schleiermacher	Schleiermachra	k1gFnPc2	Schleiermachra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
Platónova	Platónův	k2eAgInSc2d1	Platónův
domnělého	domnělý	k2eAgInSc2d1	domnělý
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
bádání	bádání	k1gNnSc1	bádání
zde	zde	k6eAd1	zde
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
exaktních	exaktní	k2eAgFnPc2d1	exaktní
srovnávacích	srovnávací	k2eAgFnPc2d1	srovnávací
metod	metoda	k1gFnPc2	metoda
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
slohu	sloh	k1gInSc2	sloh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgFnPc1	tři
skupiny	skupina	k1gFnPc1	skupina
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
k	k	k7c3	k
sobě	se	k3xPyFc3	se
mají	mít	k5eAaImIp3nP	mít
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jednotlivostech	jednotlivost	k1gFnPc6	jednotlivost
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
<g/>
,	,	kIx,	,
o	o	k7c6	o
zásadním	zásadní	k2eAgNnSc6d1	zásadní
rozlišení	rozlišení	k1gNnSc6	rozlišení
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
však	však	k9	však
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Pokládají	pokládat	k5eAaImIp3nP	pokládat
se	se	k3xPyFc4	se
za	za	k7c4	za
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
historickému	historický	k2eAgMnSc3d1	historický
Sókratovi	Sókrat	k1gMnSc3	Sókrat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obvykle	obvykle	k6eAd1	obvykle
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
otázce	otázka	k1gFnSc6	otázka
(	(	kIx(	(
<g/>
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
zbožnost	zbožnost	k1gFnSc1	zbožnost
<g/>
)	)	kIx)	)
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
odborníka	odborník	k1gMnSc4	odborník
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
odborník	odborník	k1gMnSc1	odborník
věci	věc	k1gFnSc2	věc
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ponecháno	ponechán	k2eAgNnSc1d1	ponecháno
na	na	k7c6	na
čtenáři	čtenář	k1gMnSc6	čtenář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spor	spor	k1gInSc1	spor
rozsoudil	rozsoudit	k5eAaPmAgInS	rozsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
dialogy	dialog	k1gInPc1	dialog
o	o	k7c6	o
Sókratově	Sókratův	k2eAgInSc6d1	Sókratův
soudu	soud	k1gInSc6	soud
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obrana	obrana	k1gFnSc1	obrana
Sókratova	Sókratův	k2eAgFnSc1d1	Sókratova
Kritón	Kritón	k1gInSc4	Kritón
Charmidés	Charmidés	k1gInSc1	Charmidés
Lachés	Lachésa	k1gFnPc2	Lachésa
Lysis	Lysis	k1gFnSc2	Lysis
Euthyfrón	Euthyfrón	k1gInSc1	Euthyfrón
Menexenos	Menexenos	k1gInSc1	Menexenos
Hippiás	Hippiása	k1gFnPc2	Hippiása
Menší	malý	k2eAgMnSc1d2	menší
Ión	Ión	k1gMnSc1	Ión
Za	za	k7c4	za
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
následující	následující	k2eAgFnSc3d1	následující
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
<g/>
:	:	kIx,	:
Gorgiás	Gorgiás	k1gInSc1	Gorgiás
Prótagorás	Prótagorás	k1gInSc1	Prótagorás
Menón	Menón	k1gInSc4	Menón
Už	už	k9	už
v	v	k7c6	v
raných	raný	k2eAgInPc6d1	raný
dialozích	dialog	k1gInPc6	dialog
dává	dávat	k5eAaImIp3nS	dávat
někdy	někdy	k6eAd1	někdy
Sókratés	Sókratés	k1gInSc4	Sókratés
odpovědi	odpověď	k1gFnSc2	odpověď
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
patrně	patrně	k6eAd1	patrně
Platónovy	Platónův	k2eAgInPc4d1	Platónův
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
poznání	poznání	k1gNnSc1	poznání
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
neměnných	měnný	k2eNgFnPc2d1	neměnná
forem	forma	k1gFnPc2	forma
či	či	k8xC	či
idejí	idea	k1gFnPc2	idea
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
o	o	k7c6	o
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
<g/>
,	,	kIx,	,
pravdě	pravda	k1gFnSc6	pravda
a	a	k8xC	a
kráse	krása	k1gFnSc6	krása
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
středního	střední	k2eAgNnSc2d1	střední
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
Symposion	symposion	k1gNnSc1	symposion
a	a	k8xC	a
Ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Euthydémos	Euthydémos	k1gInSc1	Euthydémos
Kratylos	Kratylos	k1gInSc1	Kratylos
Faidón	Faidón	k1gInSc1	Faidón
Faidros	Faidrosa	k1gFnPc2	Faidrosa
Symposion	symposion	k1gNnSc1	symposion
či	či	k8xC	či
Hostina	hostina	k1gFnSc1	hostina
Ústava	ústava	k1gFnSc1	ústava
či	či	k8xC	či
Republika	republika	k1gFnSc1	republika
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
V	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Parmenidés	Parmenidésa	k1gFnPc2	Parmenidésa
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
kritiky	kritika	k1gFnSc2	kritika
myšlenky	myšlenka	k1gFnSc2	myšlenka
forem	forma	k1gFnPc2	forma
či	či	k8xC	či
idejí	idea	k1gFnPc2	idea
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Platón	Platón	k1gMnSc1	Platón
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
v	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
chybí	chybit	k5eAaPmIp3nS	chybit
nebo	nebo	k8xC	nebo
hraje	hrát	k5eAaImIp3nS	hrát
jen	jen	k9	jen
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgInPc1d1	pozdní
dialogy	dialog	k1gInPc1	dialog
jsou	být	k5eAaImIp3nP	být
střízlivější	střízlivý	k2eAgFnPc1d2	střízlivější
a	a	k8xC	a
logičtější	logický	k2eAgFnPc1d2	logičtější
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc1d1	předchozí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slibují	slibovat	k5eAaImIp3nP	slibovat
i	i	k9	i
řešení	řešení	k1gNnSc4	řešení
otázek	otázka	k1gFnPc2	otázka
položených	položený	k2eAgFnPc6d1	položená
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
asi	asi	k9	asi
nejspíš	nejspíš	k9	nejspíš
dá	dát	k5eAaPmIp3nS	dát
hledat	hledat	k5eAaImF	hledat
zralé	zralý	k2eAgNnSc4d1	zralé
Platónovo	Platónův	k2eAgNnSc4d1	Platónovo
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Sofistés	Sofistés	k6eAd1	Sofistés
Politikos	Politikos	k1gMnSc1	Politikos
Filébos	Filébos	k1gMnSc1	Filébos
Tímaios	Tímaios	k1gMnSc1	Tímaios
Krítiás	Krítiás	k1gInSc4	Krítiás
Zákony	zákon	k1gInPc1	zákon
Platónův	Platónův	k2eAgInSc1d1	Platónův
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
západní	západní	k2eAgFnSc4d1	západní
filosofii	filosofie	k1gFnSc4	filosofie
je	být	k5eAaImIp3nS	být
nedozírný	dozírný	k2eNgInSc1d1	nedozírný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navázal	navázat	k5eAaPmAgInS	navázat
střední	střední	k2eAgInSc1d1	střední
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
platonismus	platonismus	k1gInSc1	platonismus
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Augustinovým	Augustinovi	k1gRnPc3	Augustinovi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Prokla	Prokla	k1gFnSc2	Prokla
a	a	k8xC	a
Dionysia	Dionysium	k1gNnSc2	Dionysium
Areopagity	Areopagita	k1gFnSc2	Areopagita
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
renesanční	renesanční	k2eAgNnSc1d1	renesanční
myšlení	myšlení	k1gNnSc1	myšlení
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
zejména	zejména	k9	zejména
Platónem	platón	k1gInSc7	platón
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
pilně	pilně	k6eAd1	pilně
čtou	číst	k5eAaImIp3nP	číst
i	i	k9	i
myslitelé	myslitel	k1gMnPc1	myslitel
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Platónovy	Platónův	k2eAgInPc4d1	Platónův
spisy	spis	k1gInPc4	spis
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
F.	F.	kA	F.
Novotného	Novotný	k1gMnSc2	Novotný
(	(	kIx(	(
<g/>
Oikumené	Oikumený	k2eAgFnSc2d1	Oikumený
<g/>
)	)	kIx)	)
A.	A.	kA	A.
Graeser	Graeser	k1gInSc1	Graeser
<g/>
,	,	kIx,	,
Řecká	řecký	k2eAgFnSc1d1	řecká
filosofie	filosofie	k1gFnSc1	filosofie
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
F.	F.	kA	F.
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
O	o	k7c6	o
Platónovi	Platón	k1gMnSc6	Platón
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
J.	J.	kA	J.
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Negativní	negativní	k2eAgInSc1d1	negativní
platonismus	platonismus	k1gInSc1	platonismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
J.	J.	kA	J.
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Platón	Platón	k1gMnSc1	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
G.	G.	kA	G.
Reale	Real	k1gInSc6	Real
<g/>
,	,	kIx,	,
Platón	Platón	k1gMnSc1	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
interpretaci	interpretace	k1gFnSc4	interpretace
velkých	velký	k2eAgInPc2d1	velký
Platónových	Platónův	k2eAgInPc2d1	Platónův
dialogů	dialog	k1gInPc2	dialog
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
nepsané	psaný	k2eNgFnSc2d1	psaný
nauky	nauka	k1gFnSc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
A.	A.	kA	A.
Rodziewicz	Rodziewicza	k1gFnPc2	Rodziewicza
<g/>
,	,	kIx,	,
IDEA	idea	k1gFnSc1	idea
I	i	k8xC	i
FORMA	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
Ι	Ι	k?	Ι
Κ	Κ	k?	Κ
Ε	Ε	k?	Ε
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
fundamentach	fundamentach	k1gInSc4	fundamentach
filozofii	filozofie	k1gFnSc4	filozofie
Platona	Plato	k1gMnSc2	Plato
i	i	k9	i
presokratyków	presokratyków	k?	presokratyków
(	(	kIx(	(
<g/>
IDEA	idea	k1gFnSc1	idea
AND	Anda	k1gFnPc2	Anda
FORM.	FORM.	k1gFnSc2	FORM.
Ι	Ι	k?	Ι
Κ	Κ	k?	Κ
Ε	Ε	k?	Ε
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Foundations	Foundations	k1gInSc1	Foundations
of	of	k?	of
the	the	k?	the
Philosophy	Philosopha	k1gFnSc2	Philosopha
of	of	k?	of
Plato	Plato	k1gMnSc1	Plato
and	and	k?	and
the	the	k?	the
Presocratics	Presocratics	k1gInSc1	Presocratics
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wroclaw	Wroclaw	k?	Wroclaw
2012	[number]	k4	2012
E.	E.	kA	E.
A.	A.	kA	A.
Wyller	Wyller	k1gMnSc1	Wyller
<g/>
,	,	kIx,	,
Pozdní	pozdní	k2eAgMnSc1d1	pozdní
Platón	Platón	k1gMnSc1	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
Luciano	Luciana	k1gFnSc5	Luciana
De	De	k?	De
Crescenzo	Crescenza	k1gFnSc5	Crescenza
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
řecké	řecký	k2eAgFnSc2d1	řecká
filozofie	filozofie	k1gFnSc2	filozofie
–	–	k?	–
Sokrates	Sokrates	k1gMnSc1	Sokrates
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
druzí	druhý	k4xOgMnPc1	druhý
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Akademie	akademie	k1gFnSc1	akademie
Platonismus	platonismus	k1gInSc4	platonismus
Platónovo	Platónův	k2eAgNnSc4d1	Platónovo
pojetí	pojetí	k1gNnSc4	pojetí
demokracie	demokracie	k1gFnSc1	demokracie
Platónova	Platónův	k2eAgFnSc1d1	Platónova
nauka	nauka	k1gFnSc1	nauka
Platónské	platónský	k2eAgNnSc4d1	platónské
těleso	těleso	k1gNnSc4	těleso
Realismus	realismus	k1gInSc1	realismus
a	a	k8xC	a
nominalismus	nominalismus	k1gInSc4	nominalismus
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Platón	platón	k1gInSc1	platón
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Platón	platón	k1gInSc1	platón
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Platón	platón	k1gInSc1	platón
Texty	text	k1gInPc1	text
<g/>
:	:	kIx,	:
Souborné	souborný	k2eAgNnSc4d1	souborné
Platónovo	Platónův	k2eAgNnSc4d1	Platónovo
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
(	(	kIx(	(
<g/>
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
Ústava	ústava	k1gFnSc1	ústava
-	-	kIx~	-
česky	česky	k6eAd1	česky
Platón	Platón	k1gMnSc1	Platón
–	–	k?	–
en	en	k?	en
Platon	Platon	k1gMnSc1	Platon
-	-	kIx~	-
en	en	k?	en
(	(	kIx(	(
<g/>
Jowett	Jowett	k1gMnSc1	Jowett
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
na	na	k7c6	na
ELPENOR	ELPENOR	kA	ELPENOR
-	-	kIx~	-
en	en	k?	en
Dialogy	dialog	k1gInPc1	dialog
Laches	Laches	k1gInSc1	Laches
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Kriton	Kriton	k1gInSc1	Kriton
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Hesla	heslo	k1gNnSc2	heslo
ve	v	k7c4	v
Stanford	Stanford	k1gInSc4	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Philosophy	Philosopha	k1gMnSc2	Philosopha
<g/>
:	:	kIx,	:
Plato	Plato	k1gMnSc1	Plato
Plato	Plato	k1gMnSc1	Plato
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ethics	Ethics	k1gInSc1	Ethics
Friendship	Friendship	k1gMnSc1	Friendship
and	and	k?	and
Eros	Eros	k1gMnSc1	Eros
Middle	Middle	k1gMnSc1	Middle
Period	perioda	k1gFnPc2	perioda
Metaphysics	Metaphysics	k1gInSc4	Metaphysics
and	and	k?	and
Epistemology	Epistemolog	k1gMnPc4	Epistemolog
Plato	Plato	k1gMnSc1	Plato
on	on	k3xPp3gMnSc1	on
Utopia	Utopius	k1gMnSc2	Utopius
Rhetoric	Rhetorice	k1gFnPc2	Rhetorice
and	and	k?	and
Poetry	Poetr	k1gInPc1	Poetr
Články	článek	k1gInPc1	článek
<g/>
:	:	kIx,	:
W.	W.	kA	W.
K.	K.	kA	K.
C.	C.	kA	C.
Guthrie	Guthrie	k1gFnSc1	Guthrie
<g/>
,	,	kIx,	,
A	a	k8xC	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
Greek	Greek	k1gInSc4	Greek
Philosophy	Philosopha	k1gFnSc2	Philosopha
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Plato	Plato	k1gMnSc1	Plato
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
His	his	k1gNnSc2	his
Dialogues	Dialoguesa	k1gFnPc2	Dialoguesa
<g/>
,	,	kIx,	,
Earlier	Earlira	k1gFnPc2	Earlira
Period	perioda	k1gFnPc2	perioda
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc2	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
8-38	[number]	k4	8-38
Portál	portál	k1gInSc1	portál
Plato	Plato	k1gMnSc1	Plato
and	and	k?	and
His	his	k1gNnPc2	his
Works	Works	kA	Works
<g/>
:	:	kIx,	:
Plato	Plato	k1gMnSc1	Plato
and	and	k?	and
His	his	k1gNnSc2	his
Dialogues	Dialoguesa	k1gFnPc2	Dialoguesa
by	by	k9	by
Bernard	Bernard	k1gMnSc1	Bernard
Suzanne	Suzann	k1gInSc5	Suzann
Are	ar	k1gInSc5	ar
There	Ther	k1gInSc5	Ther
Really	Reall	k1gInPc4	Reall
Platonic	Platonice	k1gFnPc2	Platonice
Forms	Formsa	k1gFnPc2	Formsa
<g/>
?	?	kIx.	?
</s>
<s>
"	"	kIx"	"
<g/>
Plato	Plato	k1gMnSc1	Plato
and	and	k?	and
Totalitarianism	Totalitarianism	k1gMnSc1	Totalitarianism
<g/>
:	:	kIx,	:
A	A	kA	A
Documentary	Documentara	k1gFnSc2	Documentara
Study	stud	k1gInPc1	stud
<g/>
"	"	kIx"	"
Plato	plato	k1gNnSc1	plato
Bibliography	Bibliographa	k1gFnSc2	Bibliographa
at	at	k?	at
PlatoGeek	PlatoGeek	k1gMnSc1	PlatoGeek
</s>
