<s>
Metr	metr	k1gInSc1
krychlový	krychlový	k2eAgInSc1d1
(	(	kIx(
<g/>
také	také	k9
metr	metr	k1gInSc1
kubický	kubický	k2eAgInSc1d1
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
často	často	k6eAd1
kubík	kubík	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
objemu	objem	k1gInSc2
<g/>
,	,	kIx,
značená	značený	k2eAgFnSc1d1
m³	m³	k?
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
jako	jako	k8xC,k8xS
odvozená	odvozený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
.	.	kIx.
</s>