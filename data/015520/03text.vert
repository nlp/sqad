<s>
Metr	metr	k1gInSc1
krychlový	krychlový	k2eAgInSc1d1
</s>
<s>
Betonová	betonový	k2eAgFnSc1d1
krychle	krychle	k1gFnSc1
o	o	k7c6
hraně	hrana	k1gFnSc6
1	#num#	k4
metr	metr	k1gInSc4
</s>
<s>
Metr	metr	k1gInSc1
krychlový	krychlový	k2eAgInSc1d1
(	(	kIx(
<g/>
také	také	k9
metr	metr	k1gInSc1
kubický	kubický	k2eAgInSc1d1
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
často	často	k6eAd1
kubík	kubík	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
objemu	objem	k1gInSc2
<g/>
,	,	kIx,
značená	značený	k2eAgFnSc1d1
m³	m³	k?
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
jako	jako	k8xC,k8xS
odvozená	odvozený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
metr	metr	k1gInSc1
krychlový	krychlový	k2eAgInSc1d1
je	být	k5eAaImIp3nS
objem	objem	k1gInSc1
krychle	krychle	k1gFnSc2
s	s	k7c7
délkou	délka	k1gFnSc7
hrany	hrana	k1gFnSc2
1	#num#	k4
metr	metr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
některých	některý	k3yIgInPc2
řádově	řádově	k6eAd1
blízkých	blízký	k2eAgInPc2d1
objemů	objem	k1gInPc2
</s>
<s>
0,1	0,1	k4
m³	m³	k?
je	být	k5eAaImIp3nS
rovno	roven	k2eAgNnSc1d1
</s>
<s>
3,53	3,53	k4
krychlové	krychlový	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
</s>
<s>
1	#num#	k4
m³	m³	k?
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
</s>
<s>
35,3	35,3	k4
krychlové	krychlový	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
</s>
<s>
objemu	objem	k1gInSc3
koule	koule	k1gFnSc1
o	o	k7c6
poloměru	poloměr	k1gInSc6
0,62	0,62	k4
m	m	kA
</s>
<s>
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Kilometr	kilometr	k1gInSc1
krychlový	krychlový	k2eAgInSc1d1
(	(	kIx(
<g/>
km³	km³	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
objem	objem	k1gInSc1
krychle	krychle	k1gFnSc2
s	s	k7c7
délkou	délka	k1gFnSc7
hrany	hrana	k1gFnSc2
1	#num#	k4
kilometr	kilometr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
je	být	k5eAaImIp3nS
objem	objem	k1gInSc1
krychle	krychle	k1gFnSc2
dán	dát	k5eAaPmNgInS
třetí	třetí	k4xOgFnSc3
mocninou	mocnina	k1gFnSc7
její	její	k3xOp3gFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
</s>
<s>
1	#num#	k4
km³	km³	k?
=	=	kIx~
1	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
m³	m³	k?
<g/>
.	.	kIx.
</s>
