<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
(	(	kIx(	(
<g/>
abchazsky	abchazsky	k6eAd1	abchazsky
<g/>
:	:	kIx,	:
В	В	k?	В
Г	Г	k?	Г
А	А	k?	А
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
А	А	k?	А
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
abchazský	abchazský	k2eAgMnSc1d1	abchazský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
abchazských	abchazský	k2eAgMnPc2d1	abchazský
separatistů	separatista	k1gMnPc2	separatista
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
mezinárodně	mezinárodně	k6eAd1	mezinárodně
neuznané	uznaný	k2eNgFnSc2d1	neuznaná
Abchazské	abchazský	k2eAgFnSc2d1	abchazská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
suchumském	suchumský	k2eAgInSc6d1	suchumský
pedagogickém	pedagogický	k2eAgInSc6d1	pedagogický
institutu	institut	k1gInSc6	institut
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c6	na
Státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
až	až	k9	až
1987	[number]	k4	1987
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
na	na	k7c6	na
institutu	institut	k1gInSc6	institut
orientalistiky	orientalistika	k1gFnSc2	orientalistika
při	při	k7c6	při
Ruské	ruský	k2eAgFnSc6d1	ruská
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
rodné	rodný	k2eAgFnSc2d1	rodná
Abcházie	Abcházie	k1gFnSc2	Abcházie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
abchazském	abchazský	k2eAgInSc6d1	abchazský
institutu	institut	k1gInSc6	institut
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
angažovat	angažovat	k5eAaBmF	angažovat
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
hnutí	hnutí	k1gNnSc6	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Abcházie	Abcházie	k1gFnSc2	Abcházie
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
léta	léto	k1gNnPc4	léto
1989-1991	[number]	k4	1989-1991
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
sovětského	sovětský	k2eAgInSc2d1	sovětský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
byl	být	k5eAaImAgMnS	být
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
komunista	komunista	k1gMnSc1	komunista
<g/>
,	,	kIx,	,
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
frakcím	frakce	k1gFnPc3	frakce
podporujících	podporující	k2eAgFnPc2d1	podporující
nezávislost	nezávislost	k1gFnSc4	nezávislost
malých	malý	k2eAgInPc2d1	malý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
abchazských	abchazský	k2eAgMnPc2d1	abchazský
separatistů	separatista	k1gMnPc2	separatista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Účast	účast	k1gFnSc1	účast
v	v	k7c6	v
gruzínsko-abchazském	gruzínskobchazský	k2eAgInSc6d1	gruzínsko-abchazský
konfliktu	konflikt	k1gInSc6	konflikt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
abchazského	abchazský	k2eAgInSc2d1	abchazský
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
protigruzínské	protigruzínský	k2eAgFnSc3d1	protigruzínská
rétorice	rétorika	k1gFnSc3	rétorika
mezi	mezi	k7c7	mezi
Abchazy	Abchaz	k1gMnPc7	Abchaz
velice	velice	k6eAd1	velice
populární	populární	k2eAgInSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Gruzínci	Gruzínec	k1gMnPc1	Gruzínec
Ardzinbu	Ardzinb	k1gInSc2	Ardzinb
podezřívali	podezřívat	k5eAaImAgMnP	podezřívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
Abchazy	Abchaz	k1gMnPc4	Abchaz
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
v	v	k7c6	v
Suchumi	Suchumi	k1gNnPc6	Suchumi
z	z	k7c2	z
léta	léto	k1gNnSc2	léto
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
rychle	rychle	k6eAd1	rychle
soustředil	soustředit	k5eAaPmAgMnS	soustředit
moc	moc	k6eAd1	moc
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
uvedením	uvedení	k1gNnSc7	uvedení
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
započal	započnout	k5eAaPmAgMnS	započnout
personální	personální	k2eAgFnPc4d1	personální
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
strukturách	struktura	k1gFnPc6	struktura
Abchazské	abchazský	k2eAgFnSc2d1	abchazská
autonomní	autonomní	k2eAgFnSc2d1	autonomní
republiky	republika	k1gFnSc2	republika
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Abchazové	Abchaz	k1gMnPc1	Abchaz
měli	mít	k5eAaImAgMnP	mít
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
než	než	k8xS	než
Gruzínci	Gruzínec	k1gMnPc1	Gruzínec
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
to	ten	k3xDgNnSc1	ten
prováděl	provádět	k5eAaImAgInS	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
jakémukoliv	jakýkoliv	k3yIgInSc3	jakýkoliv
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
gruzínským	gruzínský	k2eAgMnSc7d1	gruzínský
prezidentem	prezident	k1gMnSc7	prezident
Gamsachurdiou	Gamsachurdia	k1gMnSc7	Gamsachurdia
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
přenastavení	přenastavení	k1gNnSc4	přenastavení
etnických	etnický	k2eAgFnPc2d1	etnická
kvót	kvóta	k1gFnPc2	kvóta
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
abchazského	abchazský	k2eAgInSc2d1	abchazský
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
Abchazové	Abchaz	k1gMnPc1	Abchaz
zvýhodněni	zvýhodnit	k5eAaPmNgMnP	zvýhodnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
založil	založit	k5eAaPmAgInS	založit
abchazskou	abchazský	k2eAgFnSc4d1	abchazská
Národní	národní	k2eAgFnSc4d1	národní
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
pouze	pouze	k6eAd1	pouze
Abchazové	Abchaz	k1gMnPc1	Abchaz
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
začal	začít	k5eAaPmAgInS	začít
odvolávat	odvolávat	k5eAaImF	odvolávat
z	z	k7c2	z
důležitých	důležitý	k2eAgFnPc2d1	důležitá
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
abchazské	abchazský	k2eAgFnSc6d1	abchazská
správě	správa	k1gFnSc6	správa
i	i	k8xC	i
zbývající	zbývající	k2eAgFnSc6d1	zbývající
etnické	etnický	k2eAgFnSc6d1	etnická
Gruzínce	Gruzínka	k1gFnSc6	Gruzínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Gruzínci	Gruzínec	k1gMnPc7	Gruzínec
a	a	k8xC	a
Abchazi	Abchaze	k1gFnSc4	Abchaze
stupňovalo	stupňovat	k5eAaImAgNnS	stupňovat
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
Ardzinbova	Ardzinbův	k2eAgFnSc1d1	Ardzinbův
rétorika	rétorika	k1gFnSc1	rétorika
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
radikálnosti	radikálnost	k1gFnSc6	radikálnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1992	[number]	k4	1992
dokonce	dokonce	k9	dokonce
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Abcházie	Abcházie	k1gFnSc1	Abcházie
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
dost	dost	k6eAd1	dost
silná	silný	k2eAgFnSc1d1	silná
na	na	k7c4	na
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
<g/>
"	"	kIx"	"
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
skutečné	skutečný	k2eAgFnSc2d1	skutečná
války	válka	k1gFnSc2	válka
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
mobilizaci	mobilizace	k1gFnSc4	mobilizace
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
nedávno	nedávno	k6eAd1	nedávno
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
Konfederaci	konfederace	k1gFnSc4	konfederace
kavkazských	kavkazský	k2eAgInPc2d1	kavkazský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
řad	řada	k1gFnPc2	řada
naverboval	naverbovat	k5eAaPmAgInS	naverbovat
hlavně	hlavně	k9	hlavně
čečenské	čečenský	k2eAgMnPc4d1	čečenský
militanty	militant	k1gMnPc4	militant
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
i	i	k8xC	i
terorista	terorista	k1gMnSc1	terorista
Šamil	Šamil	k1gMnSc1	Šamil
Basajev	Basajev	k1gMnSc1	Basajev
<g/>
,	,	kIx,	,
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgInS	nařídit
přesun	přesun	k1gInSc4	přesun
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Gudauty	Gudaut	k2eAgInPc1d1	Gudaut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgInPc2	svůj
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
konzervativními	konzervativní	k2eAgMnPc7d1	konzervativní
komunisty	komunista	k1gMnPc7	komunista
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
s	s	k7c7	s
ruskými	ruský	k2eAgMnPc7d1	ruský
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
veliteli	velitel	k1gMnPc7	velitel
a	a	k8xC	a
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
jejich	jejich	k3xOp3gFnSc4	jejich
tichou	tichý	k2eAgFnSc4d1	tichá
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Tbilisi	Tbilisi	k1gNnSc3	Tbilisi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Abcházie	Abcházie	k1gFnSc2	Abcházie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
uklidnění	uklidnění	k1gNnSc4	uklidnění
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
abchazským	abchazský	k2eAgInSc7d1	abchazský
parlamentem	parlament	k1gInSc7	parlament
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
prvním	první	k4xOgMnSc6	první
abchazským	abchazský	k2eAgMnSc7d1	abchazský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
byl	být	k5eAaImAgInS	být
však	však	k9	však
označen	označit	k5eAaPmNgInS	označit
jak	jak	k8xS	jak
centrální	centrální	k2eAgInSc1d1	centrální
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
OSN	OSN	kA	OSN
za	za	k7c4	za
protizákonný	protizákonný	k2eAgInSc4d1	protizákonný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
znovuzvolen	znovuzvolit	k5eAaPmNgInS	znovuzvolit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přímých	přímý	k2eAgFnPc2d1	přímá
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgMnPc4	žádný
protikandidáty	protikandidát	k1gMnPc4	protikandidát
<g/>
.	.	kIx.	.
</s>
<s>
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
zavedl	zavést	k5eAaPmAgMnS	zavést
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
autokratický	autokratický	k2eAgInSc4d1	autokratický
režim	režim	k1gInSc4	režim
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
takřka	takřka	k6eAd1	takřka
politicky	politicky	k6eAd1	politicky
"	"	kIx"	"
<g/>
nedotknutelný	nedotknutelný	k?	nedotknutelný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nezačal	začít	k5eNaPmAgInS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Ševardnadze	Ševardnadze	k1gFnSc1	Ševardnadze
kádrovou	kádrový	k2eAgFnSc4d1	kádrová
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
na	na	k7c4	na
důležité	důležitý	k2eAgInPc4d1	důležitý
posty	post	k1gInPc4	post
dosazoval	dosazovat	k5eAaImAgMnS	dosazovat
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
odmítal	odmítat	k5eAaImAgMnS	odmítat
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gruzínci	Gruzínec	k1gMnPc1	Gruzínec
nezávislost	nezávislost	k1gFnSc4	nezávislost
Abcházie	Abcházie	k1gFnSc2	Abcházie
nikdy	nikdy	k6eAd1	nikdy
neuznají	uznat	k5eNaPmIp3nP	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
těsnější	těsný	k2eAgFnSc4d2	těsnější
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
Abchazové	Abchaz	k1gMnPc1	Abchaz
nejprve	nejprve	k6eAd1	nejprve
sjednotit	sjednotit	k5eAaPmF	sjednotit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
zase	zase	k9	zase
převládla	převládnout	k5eAaPmAgFnS	převládnout
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
úplné	úplný	k2eAgFnSc6d1	úplná
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
gruzínským	gruzínský	k2eAgMnSc7d1	gruzínský
prezidentem	prezident	k1gMnSc7	prezident
Ševardnadzem	Ševardnadz	k1gMnSc7	Ševardnadz
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
bez	bez	k7c2	bez
prostředníků	prostředník	k1gInPc2	prostředník
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
nepoužívání	nepoužívání	k1gNnSc4	nepoužívání
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
gruzínsko-abchazského	gruzínskobchazský	k2eAgInSc2d1	gruzínsko-abchazský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ardzinbův	Ardzinbův	k2eAgInSc1d1	Ardzinbův
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
kritizován	kritizován	k2eAgInSc1d1	kritizován
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
společenstvím	společenství	k1gNnSc7	společenství
za	za	k7c4	za
porušování	porušování	k1gNnSc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
upřel	upřít	k5eAaPmAgInS	upřít
mnohým	mnohý	k2eAgMnPc3d1	mnohý
vyhnaným	vyhnaný	k2eAgMnPc3d1	vyhnaný
Gruzíncům	Gruzínec	k1gMnPc3	Gruzínec
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
domovů	domov	k1gInPc2	domov
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
terčem	terč	k1gInSc7	terč
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Ardzinba	Ardzinba	k1gFnSc1	Ardzinba
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prezidentským	prezidentský	k2eAgInSc7d1	prezidentský
dekretem	dekret	k1gInSc7	dekret
zakázal	zakázat	k5eAaPmAgInS	zakázat
činnost	činnost	k1gFnSc4	činnost
náboženskému	náboženský	k2eAgMnSc3d1	náboženský
hnutí	hnutí	k1gNnPc1	hnutí
Jehovistů	jehovista	k1gMnPc2	jehovista
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
také	také	k9	také
sklidil	sklidit	k5eAaPmAgMnS	sklidit
po	po	k7c6	po
světě	svět	k1gInSc6	svět
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
jeho	jeho	k3xOp3gFnPc4	jeho
vlády	vláda	k1gFnPc1	vláda
jsou	být	k5eAaImIp3nP	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
<g/>
,	,	kIx,	,
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
neaktivitu	neaktivita	k1gFnSc4	neaktivita
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
klesající	klesající	k2eAgFnSc7d1	klesající
popularitou	popularita	k1gFnSc7	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
znovuzvolení	znovuzvolení	k1gNnSc6	znovuzvolení
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
totiž	totiž	k9	totiž
značně	značně	k6eAd1	značně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
a	a	k8xC	a
snad	snad	k9	snad
možná	možná	k9	možná
i	i	k9	i
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
neukazoval	ukazovat	k5eNaImAgMnS	ukazovat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
a	a	k8xC	a
vykonávání	vykonávání	k1gNnSc4	vykonávání
funkce	funkce	k1gFnSc2	funkce
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
přenechával	přenechávat	k5eAaImAgMnS	přenechávat
premiérovi	premiér	k1gMnSc3	premiér
Raulu	Raul	k1gMnSc3	Raul
Chadžimbovi	Chadžimba	k1gMnSc3	Chadžimba
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chatrnému	chatrný	k2eAgNnSc3d1	chatrné
zdraví	zdraví	k1gNnSc3	zdraví
trávil	trávit	k5eAaImAgInS	trávit
totiž	totiž	k9	totiž
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
u	u	k7c2	u
doktorů	doktor	k1gMnPc2	doktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
a	a	k8xC	a
přes	přes	k7c4	přes
sílící	sílící	k2eAgInPc4d1	sílící
hlasy	hlas	k1gInPc4	hlas
opozice	opozice	k1gFnSc2	opozice
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
hnutí	hnutí	k1gNnSc2	hnutí
Amcachara	Amcachar	k1gMnSc2	Amcachar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požadovala	požadovat	k5eAaImAgFnS	požadovat
jeho	jeho	k3xOp3gFnSc4	jeho
rezignaci	rezignace	k1gFnSc4	rezignace
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
volala	volat	k5eAaImAgFnS	volat
po	po	k7c6	po
impeachmentu	impeachment	k1gInSc6	impeachment
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
setrvat	setrvat	k5eAaPmF	setrvat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
až	až	k6eAd1	až
do	do	k7c2	do
řádných	řádný	k2eAgFnPc2d1	řádná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
jeho	on	k3xPp3gMnSc2	on
nástupce	nástupce	k1gMnSc2	nástupce
(	(	kIx(	(
<g/>
Garri	Garri	k1gNnSc7	Garri
Aiba	Aiba	k1gMnSc1	Aiba
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
)	)	kIx)	)
setrval	setrvat	k5eAaPmAgMnS	setrvat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úmrtí	úmrtí	k1gNnPc1	úmrtí
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Vladislavu	Vladislav	k1gMnSc3	Vladislav
Ardzimbovi	Ardzimba	k1gMnSc3	Ardzimba
náhle	náhle	k6eAd1	náhle
začal	začít	k5eAaPmAgMnS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
za	za	k7c4	za
doktory	doktor	k1gMnPc4	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Ardzimba	Ardzimba	k1gMnSc1	Ardzimba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
moskevské	moskevský	k2eAgFnSc6d1	Moskevská
ústřední	ústřední	k2eAgFnSc6d1	ústřední
nemocnici	nemocnice	k1gFnSc6	nemocnice
o	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Příčinu	příčina	k1gFnSc4	příčina
úmrtí	úmrť	k1gFnPc2	úmrť
lékaři	lékař	k1gMnPc1	lékař
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
městech	město	k1gNnPc6	město
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Gagře	Gagra	k1gFnSc6	Gagra
<g/>
,	,	kIx,	,
v	v	k7c6	v
Suchumi	Suchumi	k1gNnSc6	Suchumi
<g/>
)	)	kIx)	)
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
ulice	ulice	k1gFnPc4	ulice
Vl	Vl	k1gFnPc2	Vl
<g/>
.	.	kIx.	.
</s>
<s>
Ardzinby	Ardzinba	k1gFnPc1	Ardzinba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vladislav	Vladislav	k1gMnSc1	Vladislav
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ARDZINBA	ARDZINBA	kA	ARDZINBA
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
Р	Р	k?	Р
и	и	k?	и
м	м	k?	м
д	д	k?	д
А	А	k?	А
<g/>
.	.	kIx.	.
Г	Г	k?	Г
р	р	k?	р
в	в	k?	в
л	л	k?	л
и	и	k?	и
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
apsnyteka	apsnyteka	k1gFnSc1	apsnyteka
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vladislav	Vladislava	k1gFnPc2	Vladislava
Ardzinba	Ardzinba	k1gFnSc1	Ardzinba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
ardzinba	ardzinba	k1gFnSc1	ardzinba
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
www.georgianbiography.com	www.georgianbiography.com	k1gInSc1	www.georgianbiography.com
-	-	kIx~	-
Ardzinba	Ardzinba	k1gMnSc1	Ardzinba
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
www.abkhaziagov.org	www.abkhaziagov.org	k1gInSc1	www.abkhaziagov.org
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
abchazské	abchazský	k2eAgFnSc2d1	abchazská
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
-	-	kIx~	-
В	В	k?	В
А	А	k?	А
</s>
</p>
