<s>
Opistotonus	Opistotonus	k1gMnSc1
</s>
<s>
Opistotonus	Opistotonus	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Opistotonus	Opistotonus	k1gInSc1
je	být	k5eAaImIp3nS
specifická	specifický	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6
leží	ležet	k5eAaImIp3nS
pacient	pacient	k1gMnSc1
na	na	k7c6
zádech	záda	k1gNnPc6
<g/>
,	,	kIx,
opírá	opírat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hlavu	hlava	k1gFnSc4
a	a	k8xC
paty	pata	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
tělo	tělo	k1gNnSc4
prohnuté	prohnutý	k2eAgNnSc4d1
do	do	k7c2
oblouku	oblouk	k1gInSc2
dopředu	dopředu	k6eAd1
jako	jako	k8xS,k8xC
důsledek	důsledek	k1gInSc4
křečovitého	křečovitý	k2eAgInSc2d1
spasmu	spasmus	k1gInSc2
zádového	zádový	k2eAgNnSc2d1
svalstva	svalstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc1
svaly	sval	k1gInPc1
v	v	k7c6
těle	tělo	k1gNnSc6
postiženého	postižený	k1gMnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
těle	tělo	k1gNnSc6
staženy	stažen	k2eAgFnPc1d1
<g/>
,	,	kIx,
horní	horní	k2eAgFnPc1d1
končetiny	končetina	k1gFnPc1
jsou	být	k5eAaImIp3nP
zkroucené	zkroucený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
poloha	poloha	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
poloha	poloha	k1gFnSc1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příčiny	příčina	k1gFnPc4
</s>
<s>
Příčinou	příčina	k1gFnSc7
opistotonu	opistoton	k1gInSc2
je	být	k5eAaImIp3nS
tetanus	tetanus	k1gInSc1
<g/>
,	,	kIx,
eklampsie	eklampsie	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
mozková	mozkový	k2eAgFnSc1d1
obrna	obrna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
může	moct	k5eAaImIp3nS
opistotonus	opistotonus	k1gInSc1
způsobovat	způsobovat	k5eAaImF
hystérie	hystérie	k1gFnPc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
záchvat	záchvat	k1gInSc1
histriónské	histriónský	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
zánět	zánět	k1gInSc4
meningů	mening	k1gInPc2
(	(	kIx(
<g/>
meningitida	meningitida	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úraz	úraz	k1gInSc4
hlavy	hlava	k1gFnSc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k9
decerebrační	decerebrační	k2eAgFnSc1d1
rigidita	rigidita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SR	SR	kA
<g/>
,	,	kIx,
MEFANET	MEFANET	kA
<g/>
,	,	kIx,
síť	síť	k1gFnSc1
lékařských	lékařský	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
ČR	ČR	kA
a.	a.	k?
Opistotonus	Opistotonus	k1gMnSc1
–	–	k?
WikiSkripta	WikiSkripta	k1gMnSc1
<g/>
.	.	kIx.
www.wikiskripta.eu	www.wikiskripta.eu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Křečovité	křečovitý	k2eAgNnSc4d1
propnutí	propnutí	k1gNnSc4
těla	tělo	k1gNnSc2
do	do	k7c2
záklonu	záklon	k1gInSc2
-	-	kIx~
Anamneza	Anamneza	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.anamneza.cz	www.anamneza.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Opistotonus	Opistotonus	k1gInSc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
<g/>
,	,	kIx,
nemoci	nemoc	k1gFnPc4
<g/>
,	,	kIx,
studium	studium	k1gNnSc4
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
LF	LF	kA
UK	UK	kA
<g/>
.	.	kIx.
www.stefajir.cz	www.stefajir.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1
neručí	ručit	k5eNaImIp3nS
za	za	k7c4
správnost	správnost	k1gFnSc4
lékařských	lékařský	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
vyhledejte	vyhledat	k5eAaPmRp2nP
lékaře	lékař	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
prosím	prosit	k5eAaImIp1nS
pokyny	pokyn	k1gInPc4
pro	pro	k7c4
využití	využití	k1gNnSc4
článků	článek	k1gInPc2
o	o	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
