<s>
LPG	LPG	kA
</s>
<s>
LPG	LPG	kA
autobusy	autobus	k1gInPc1
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
</s>
<s>
O	o	k7c6
Landwirtschaftliche	Landwirtschaftliche	k1gFnSc6
Produktionsgenossenschaft	Produktionsgenossenschafta	k1gFnPc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Jednotné	jednotný	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
družstvo	družstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
LPG	LPG	kA
(	(	kIx(
<g/>
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
Liquified	Liquified	k1gInSc1
Petroleum	Petroleum	k1gInSc1
Gas	Gas	k1gFnSc1
<g/>
)	)	kIx)
neboli	neboli	k8xC
zkapalněný	zkapalněný	k2eAgInSc1d1
ropný	ropný	k2eAgInSc1d1
plyn	plyn	k1gInSc1
je	být	k5eAaImIp3nS
směs	směs	k1gFnSc4
uhlovodíkových	uhlovodíkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
používaná	používaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
palivo	palivo	k1gNnSc1
do	do	k7c2
spalovacích	spalovací	k2eAgInPc2d1
spotřebičů	spotřebič	k1gInPc2
a	a	k8xC
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
novodobější	novodobý	k2eAgNnSc4d2
označení	označení	k1gNnSc4
pro	pro	k7c4
směs	směs	k1gFnSc4
topného	topný	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
jako	jako	k8xC,k8xS
propan-butan	propan-butan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
palivo	palivo	k1gNnSc1
pro	pro	k7c4
vaření	vaření	k1gNnSc4
<g/>
,	,	kIx,
vytápění	vytápění	k1gNnSc4
i	i	k8xC
osvětlování	osvětlování	k1gNnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
také	také	k9
jako	jako	k9
palivo	palivo	k1gNnSc4
pro	pro	k7c4
zážehové	zážehový	k2eAgInPc4d1
motory	motor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
plní	plnit	k5eAaImIp3nS
do	do	k7c2
tlakových	tlakový	k2eAgFnPc2d1
lahví	lahev	k1gFnPc2
různých	různý	k2eAgFnPc2d1
velikostí	velikost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
používají	používat	k5eAaImIp3nP
jakožto	jakožto	k8xS
přenosné	přenosný	k2eAgInPc4d1
zásobníky	zásobník	k1gInPc4
topného	topný	k2eAgInSc2d1
plynu	plyn	k1gInSc2
v	v	k7c6
domácnostech	domácnost	k1gFnPc6
a	a	k8xC
při	při	k7c6
pobytu	pobyt	k1gInSc6
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
narušuje	narušovat	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
pryž	pryž	k1gFnSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
všechna	všechen	k3xTgNnPc1
těsnění	těsnění	k1gNnPc1
vyrobena	vyroben	k2eAgFnSc1d1
ze	z	k7c2
syntetických	syntetický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
(	(	kIx(
<g/>
kapalná	kapalný	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
<g/>
)	)	kIx)
zimní	zimní	k2eAgFnSc1d1
směs	směs	k1gFnSc1
540	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
(	(	kIx(
<g/>
Propan	propan	k1gInSc1
510	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
,	,	kIx,
Butan	butan	k1gInSc1
580	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
plynné	plynný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
cca	cca	kA
2,1	2,1	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
(	(	kIx(
<g/>
Z	z	k7c2
1	#num#	k4
litru	litr	k1gInSc2
kapalné	kapalný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
vznikne	vzniknout	k5eAaPmIp3nS
asi	asi	k9
260	#num#	k4
litrů	litr	k1gInPc2
plynu	plyn	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Výhřevnost	výhřevnost	k1gFnSc1
cca	cca	kA
12,9	12,9	k4
kWh	kwh	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
(	(	kIx(
<g/>
46,44	46,44	k4
MJ	mj	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
)	)	kIx)
</s>
<s>
Složení	složení	k1gNnSc1
<g/>
:	:	kIx,
Zimní	zimní	k2eAgFnSc1d1
LPG	LPG	kA
směs	směs	k1gFnSc1
(	(	kIx(
<g/>
60	#num#	k4
<g/>
%	%	kIx~
P	P	kA
a	a	k8xC
40	#num#	k4
<g/>
%	%	kIx~
B	B	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
letní	letní	k2eAgFnSc1d1
LPG	LPG	kA
směs	směs	k1gFnSc1
(	(	kIx(
<g/>
40	#num#	k4
<g/>
%	%	kIx~
P	P	kA
a	a	k8xC
60	#num#	k4
<g/>
%	%	kIx~
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Pohon	pohon	k1gInSc1
automobilů	automobil	k1gInPc2
</s>
<s>
LPG	LPG	kA
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
chybně	chybně	k6eAd1
zaměňovaný	zaměňovaný	k2eAgInSc1d1
za	za	k7c4
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
čili	čili	k8xC
CNG	CNG	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
také	také	k9
s	s	k7c7
úspěchem	úspěch	k1gInSc7
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
jako	jako	k9
alternativní	alternativní	k2eAgInSc4d1
pohon	pohon	k1gInSc4
pro	pro	k7c4
automobily	automobil	k1gInPc4
se	s	k7c7
zážehovými	zážehový	k2eAgInPc7d1
motory	motor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
velkou	velký	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
poloviční	poloviční	k2eAgFnSc1d1
cena	cena	k1gFnSc1
u	u	k7c2
čerpacích	čerpací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
oproti	oproti	k7c3
benzínu	benzín	k1gInSc3
(	(	kIx(
<g/>
LPG	LPG	kA
nebývá	bývat	k5eNaImIp3nS
zatížen	zatížit	k5eAaPmNgInS
vysokou	vysoký	k2eAgFnSc7d1
spotřební	spotřební	k2eAgFnSc7d1
daní	daň	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
šetrnost	šetrnost	k1gFnSc1
k	k	k7c3
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pohon	pohon	k1gInSc1
na	na	k7c6
LPG	LPG	kA
snižuje	snižovat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
o	o	k7c4
5	#num#	k4
-	-	kIx~
10	#num#	k4
<g/>
%	%	kIx~
výkon	výkon	k1gInSc1
motoru	motor	k1gInSc2
(	(	kIx(
<g/>
u	u	k7c2
CNG	CNG	kA
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
až	až	k9
o	o	k7c4
20	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
využití	využití	k1gNnSc2
moderního	moderní	k2eAgInSc2d1
systému	systém	k1gInSc2
sekvenčního	sekvenční	k2eAgNnSc2d1
vstřikování	vstřikování	k1gNnSc2
LPG	LPG	kA
však	však	k8xC
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
odpadá	odpadat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
LPG	LPG	kA
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
tetraethylolovo	tetraethylolovo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
u	u	k7c2
dřívějších	dřívější	k2eAgInPc2d1
olovnatých	olovnatý	k2eAgInPc2d1
benzínů	benzín	k1gInPc2
zvyšovalo	zvyšovat	k5eAaImAgNnS
oktanové	oktanový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
snížení	snížení	k1gNnSc1
náchylnosti	náchylnost	k1gFnSc2
k	k	k7c3
samovznícení	samovznícení	k1gNnSc3
paliva	palivo	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
zároveň	zároveň	k6eAd1
sloužilo	sloužit	k5eAaImAgNnS
k	k	k7c3
mazání	mazání	k1gNnSc3
některých	některý	k3yIgFnPc2
částí	část	k1gFnPc2
ventilových	ventilový	k2eAgInPc2d1
rozvodů	rozvod	k1gInPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
motory	motor	k1gInPc1
s	s	k7c7
pohonem	pohon	k1gInSc7
na	na	k7c6
LPG	LPG	kA
náchylné	náchylný	k2eAgInPc1d1
k	k	k7c3
„	„	k?
<g/>
zaklepání	zaklepání	k1gNnSc2
ventilů	ventil	k1gInPc2
<g/>
“	“	k?
či	či	k8xC
jinému	jiný	k2eAgNnSc3d1
poškození	poškození	k1gNnSc3
ventilů	ventil	k1gInPc2
a	a	k8xC
ventilových	ventilový	k2eAgNnPc2d1
sedel	sedlo	k1gNnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Valve	Valev	k1gFnSc2
Seat	Seat	k2eAgInSc1d1
Recession	Recession	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
koncem	koncem	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
laboratorně	laboratorně	k6eAd1
vyvíjena	vyvíjet	k5eAaImNgFnS
speciální	speciální	k2eAgFnSc7d1
LPG	LPG	kA
aditiva	aditivum	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
palivu	palivo	k1gNnSc6
dodávají	dodávat	k5eAaImIp3nP
mazivost	mazivost	k1gFnSc4
prostřednictvím	prostřednictvím	k7c2
jiných	jiný	k2eAgFnPc2d1
ekologických	ekologický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
již	již	k6eAd1
ale	ale	k9
není	být	k5eNaImIp3nS
u	u	k7c2
drtivé	drtivý	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
nyní	nyní	k6eAd1
přestavovaných	přestavovaný	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
aktuální	aktuální	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
konstrukce	konstrukce	k1gFnSc1
motorů	motor	k1gInPc2
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nebyla	být	k5eNaImAgFnS
na	na	k7c6
přítomnosti	přítomnost	k1gFnSc6
olovnatých	olovnatý	k2eAgNnPc2d1
aditiv	aditivum	k1gNnPc2
v	v	k7c6
benzínu	benzín	k1gInSc6
závislá	závislý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Liquefied	Liquefied	k1gInSc1
petroleum	petroleum	k1gInSc1
gas	gas	k?
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Výhody	výhod	k1gInPc1
pohonu	pohon	k1gInSc2
na	na	k7c4
plyn	plyn	k1gInSc4
LPG	LPG	kA
<g/>
↑	↑	k?
LPG	LPG	kA
(	(	kIx(
<g/>
Liquefied	Liquefied	k1gInSc1
Petroleum	Petroleum	k1gNnSc1
Gas	Gas	k1gFnPc2
<g/>
)	)	kIx)
|	|	kIx~
autolexicon	autolexicon	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
www.autolexicon.net	www.autolexicon.net	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Problémy	problém	k1gInPc1
motorů	motor	k1gInPc2
na	na	k7c6
LPG	LPG	kA
/	/	kIx~
CNG	CNG	kA
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
efektivní	efektivní	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
LPG	LPG	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
LPG	LPG	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Seznam	seznam	k1gInSc1
čerpacích	čerpací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
LPG	LPG	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Fosilní	fosilní	k2eAgNnPc1d1
paliva	palivo	k1gNnPc1
uhlí	uhlí	k1gNnSc2
</s>
<s>
rašelina	rašelina	k1gFnSc1
•	•	k?
gagát	gagát	k1gInSc1
•	•	k?
hnědé	hnědý	k2eAgNnSc4d1
uhlí	uhlí	k1gNnSc4
•	•	k?
černé	černá	k1gFnSc2
uhlí	uhlí	k1gNnSc2
•	•	k?
antracit	antracit	k1gInSc1
</s>
<s>
uhelné	uhelný	k2eAgInPc4d1
produkty	produkt	k1gInPc4
</s>
<s>
briketa	briketa	k1gFnSc1
•	•	k?
saze	saze	k1gFnSc2
•	•	k?
koks	koks	k1gInSc1
•	•	k?
oxid	oxid	k1gInSc1
uhelnatý	uhelnatý	k2eAgInSc1d1
-	-	kIx~
generátorový	generátorový	k2eAgInSc1d1
plyn	plyn	k1gInSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
•	•	k?
koksárenský	koksárenský	k2eAgInSc1d1
plyn	plyn	k1gInSc1
•	•	k?
svítiplyn	svítiplyn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ropa	ropa	k1gFnSc1
</s>
<s>
konvenční	konvenční	k2eAgFnSc1d1
ropa	ropa	k1gFnSc1
</s>
<s>
lehká-sladká	lehká-sladký	k2eAgFnSc1d1
•	•	k?
těžká-kyselá	těžká-kyselý	k2eAgFnSc1d1
nekonvenční	konvenční	k2eNgFnSc1d1
ropa	ropa	k1gFnSc1
</s>
<s>
živice	živice	k1gFnSc1
<g/>
:	:	kIx,
dehtové	dehtový	k2eAgInPc1d1
písky	písek	k1gInPc1
•	•	k?
asfalt	asfalt	k1gInSc1
•	•	k?
kerogen	kerogen	k1gInSc1
<g/>
:	:	kIx,
ropné	ropný	k2eAgFnSc2d1
břidlice	břidlice	k1gFnSc2
ropné	ropný	k2eAgFnSc2d1
produkty	produkt	k1gInPc1
</s>
<s>
minerální	minerální	k2eAgInSc1d1
olej	olej	k1gInSc1
•	•	k?
nafta	nafta	k1gFnSc1
•	•	k?
benzín	benzín	k1gInSc1
•	•	k?
petrolej	petrolej	k1gInSc1
•	•	k?
parafín	parafín	k1gInSc1
•	•	k?
dehet	dehet	k1gInSc1
•	•	k?
LPG	LPG	kA
(	(	kIx(
<g/>
propan	propan	k1gInSc1
•	•	k?
butan	butan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
</s>
<s>
stlačený	stlačený	k2eAgMnSc1d1
(	(	kIx(
<g/>
CNG	CNG	kA
<g/>
)	)	kIx)
•	•	k?
zkapalněný	zkapalněný	k2eAgMnSc1d1
(	(	kIx(
<g/>
LNG	LNG	kA
<g/>
)	)	kIx)
•	•	k?
methan	methan	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgInPc1d1
vývěry	vývěr	k1gInPc1
</s>
<s>
Chiméra	chiméra	k1gFnSc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
•	•	k?
Focul	Focul	k1gInSc1
viu	viu	k?
•	•	k?
Brána	brána	k1gFnSc1
do	do	k7c2
pekla	peklo	k1gNnSc2
•	•	k?
Yanar	Yanar	k1gMnSc1
Dağ	Dağ	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4128636-4	4128636-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85077332	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85077332	#num#	k4
</s>
