<s>
Rabín	rabín	k1gMnSc1	rabín
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
rabi	rab	k1gMnPc1	rab
<g/>
,	,	kIx,	,
z	z	k7c2	z
hebr.	hebr.	k?	hebr.
ר	ר	k?	ר
rav	rav	k?	rav
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
ר	ר	k?	ר
rabi	rabi	k1gMnSc1	rabi
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysokou	vysoký	k2eAgFnSc7d1	vysoká
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
autoritou	autorita	k1gFnSc7	autorita
v	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
učitel	učitel	k1gMnSc1	učitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
rav	rav	k?	rav
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mistr	mistr	k1gMnSc1	mistr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pán	pán	k1gMnSc1	pán
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
zemským	zemský	k2eAgMnSc7d1	zemský
rabínem	rabín	k1gMnSc7	rabín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Karol	Karola	k1gFnPc2	Karola
Sidon	Sidon	k1gMnSc1	Sidon
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
rabi	rabi	k1gMnSc1	rabi
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
používán	používat	k5eAaImNgMnS	používat
zprvu	zprvu	k6eAd1	zprvu
jako	jako	k8xC	jako
titul	titul	k1gInSc4	titul
pro	pro	k7c4	pro
učence	učenec	k1gMnPc4	učenec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
názory	názor	k1gInPc4	názor
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
mišna	mišna	k1gFnSc1	mišna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tana	tanout	k5eAaImSgInS	tanout
<g/>
'	'	kIx"	'
<g/>
im	im	k?	im
<g/>
)	)	kIx)	)
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
ordinaci	ordinace	k1gFnSc4	ordinace
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
nepřerušené	přerušený	k2eNgFnSc6d1	nepřerušená
řadě	řada	k1gFnSc6	řada
na	na	k7c4	na
Mojžíše	Mojžíš	k1gMnPc4	Mojžíš
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
citován	citován	k2eAgInSc1d1	citován
tana	tanout	k5eAaImSgInS	tanout
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nebyl	být	k5eNaImAgInS	být
ordinován	ordinovat	k5eAaImNgInS	ordinovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uváděno	uváděn	k2eAgNnSc1d1	uváděno
jméno	jméno	k1gNnSc1	jméno
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
sanhedrinu	sanhedrin	k1gInSc2	sanhedrin
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
titulována	titulován	k2eAgFnSc1d1	titulována
raban	raban	k1gInSc1	raban
(	(	kIx(	(
<g/>
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Náš	náš	k3xOp1gMnSc1	náš
učitel	učitel	k1gMnSc1	učitel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
mišny	mišna	k1gFnSc2	mišna
a	a	k8xC	a
přesunu	přesun	k1gInSc3	přesun
náboženského	náboženský	k2eAgNnSc2d1	náboženské
střediska	středisko	k1gNnSc2	středisko
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
do	do	k7c2	do
Babylóna	Babylón	k1gInSc2	Babylón
se	se	k3xPyFc4	se
používání	používání	k1gNnSc1	používání
titulu	titul	k1gInSc2	titul
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
–	–	k?	–
titul	titul	k1gInSc1	titul
rav	rav	k?	rav
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
pro	pro	k7c4	pro
babylonské	babylonský	k2eAgMnPc4d1	babylonský
učence	učenec	k1gMnPc4	učenec
a	a	k8xC	a
titul	titul	k1gInSc4	titul
rabi	rab	k1gMnPc1	rab
pro	pro	k7c4	pro
palestinské	palestinský	k2eAgFnPc4d1	palestinská
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
rabín	rabín	k1gMnSc1	rabín
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
významy	význam	k1gInPc7	význam
<g/>
:	:	kIx,	:
odborník	odborník	k1gMnSc1	odborník
na	na	k7c6	na
halachu	halach	k1gInSc6	halach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
delegován	delegován	k2eAgInSc1d1	delegován
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
vydával	vydávat	k5eAaPmAgMnS	vydávat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
duchovní	duchovní	k2eAgMnSc1d1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
slouží	sloužit	k5eAaImIp3nS	sloužit
rabín	rabín	k1gMnSc1	rabín
jako	jako	k8xC	jako
duchovní	duchovní	k1gMnSc1	duchovní
rádce	rádce	k1gMnSc1	rádce
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
rav	rav	k?	rav
vycházejí	vycházet	k5eAaImIp3nP	vycházet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
reb	reb	k?	reb
<g/>
,	,	kIx,	,
rebi	rebi	k1gNnPc1	rebi
<g/>
,	,	kIx,	,
rebe	reb	k1gInPc1	reb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
reb	reb	k?	reb
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
rebe	rebe	k6eAd1	rebe
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
rabín	rabín	k1gMnSc1	rabín
-	-	kIx~	-
často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čestné	čestný	k2eAgNnSc4d1	čestné
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
dotyčnému	dotyčný	k2eAgInSc3d1	dotyčný
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
úcta	úcta	k1gFnSc1	úcta
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
"	"	kIx"	"
<g/>
učený	učený	k2eAgMnSc1d1	učený
<g/>
,	,	kIx,	,
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
židovství	židovství	k1gNnSc2	židovství
a	a	k8xC	a
halachy	halach	k1gInPc7	halach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
