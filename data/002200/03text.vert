<s>
Les	les	k1gInSc1	les
Paul	Paul	k1gMnSc1	Paul
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
modelů	model	k1gInPc2	model
kytar	kytara	k1gFnPc2	kytara
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
společností	společnost	k1gFnPc2	společnost
Gibson	Gibson	k1gMnSc1	Gibson
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
návrhem	návrh	k1gInSc7	návrh
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
přišel	přijít	k5eAaPmAgMnS	přijít
kytarista	kytarista	k1gMnSc1	kytarista
Les	les	k1gInSc4	les
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
zprvu	zprvu	k6eAd1	zprvu
Gibsonem	Gibson	k1gInSc7	Gibson
odmítnut	odmítnut	k2eAgMnSc1d1	odmítnut
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
vzpomenuto	vzpomenut	k2eAgNnSc4d1	vzpomenuto
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
velká	velký	k2eAgFnSc1d1	velká
konkurence	konkurence	k1gFnSc1	konkurence
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Lea	Leo	k1gMnSc2	Leo
Fendera	Fender	k1gMnSc2	Fender
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
elektrické	elektrický	k2eAgFnPc4d1	elektrická
kytary	kytara	k1gFnPc4	kytara
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
tělem	tělo	k1gNnSc7	tělo
-	-	kIx~	-
model	model	k1gInSc1	model
Fender	Fendra	k1gFnPc2	Fendra
Esquire	Esquir	k1gInSc5	Esquir
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
známější	známý	k2eAgMnSc1d2	známější
Fender	Fender	k1gMnSc1	Fender
Telecaster	Telecaster	k1gMnSc1	Telecaster
<g/>
.	.	kIx.	.
</s>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
údajně	údajně	k6eAd1	údajně
zazněla	zaznít	k5eAaPmAgFnS	zaznít
legendární	legendární	k2eAgFnSc1d1	legendární
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Najděte	najít	k5eAaPmRp2nP	najít
toho	ten	k3xDgNnSc2	ten
chlápka	chlápka	k?	chlápka
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
má	mít	k5eAaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
koště	koště	k1gNnSc1	koště
se	se	k3xPyFc4	se
snímačema	snímačema	k?	snímačema
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
tak	tak	k9	tak
totiž	totiž	k9	totiž
bylo	být	k5eAaImAgNnS	být
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
prvnímu	první	k4xOgNnSc3	první
prototypu	prototyp	k1gInSc6	prototyp
jeho	on	k3xPp3gInSc2	on
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Log	log	k1gInSc1	log
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kytary	kytara	k1gFnPc1	kytara
Les	les	k1gInSc1	les
Paul	Paula	k1gFnPc2	Paula
zprvu	zprvu	k6eAd1	zprvu
příliš	příliš	k6eAd1	příliš
populární	populární	k2eAgFnPc1d1	populární
nebyly	být	k5eNaImAgFnP	být
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
prvotní	prvotní	k2eAgFnSc6d1	prvotní
inkarnaci	inkarnace	k1gFnSc6	inkarnace
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
přestaly	přestat	k5eAaPmAgInP	přestat
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
)	)	kIx)	)
-	-	kIx~	-
svou	svůj	k3xOyFgFnSc4	svůj
slávu	sláva	k1gFnSc4	sláva
si	se	k3xPyFc3	se
vydobyly	vydobýt	k5eAaPmAgInP	vydobýt
až	až	k9	až
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
oblíbilo	oblíbit	k5eAaPmAgNnS	oblíbit
mnoho	mnoho	k4c1	mnoho
proslulých	proslulý	k2eAgMnPc2d1	proslulý
kytaristů	kytarista	k1gMnPc2	kytarista
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
britské	britský	k2eAgFnSc2d1	britská
vlny	vlna	k1gFnSc2	vlna
blues	blues	k1gNnSc2	blues
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Peter	Peter	k1gMnSc1	Peter
Green	Green	k2eAgMnSc1d1	Green
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
Mike	Mike	k1gNnSc1	Mike
Bloomfield	Bloomfielda	k1gFnPc2	Bloomfielda
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnSc2	Jimma
Page	Pag	k1gFnSc2	Pag
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
desetiletích	desetiletí	k1gNnPc6	desetiletí
následujících	následující	k2eAgNnPc6d1	následující
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
jako	jako	k9	jako
například	například	k6eAd1	například
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
Ace	Ace	k1gMnSc1	Ace
Frehley	Frehlea	k1gFnSc2	Frehlea
nebo	nebo	k8xC	nebo
Zakk	Zakka	k1gFnPc2	Zakka
Wylde	Wyld	k1gMnSc5	Wyld
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc1d3	nejčastější
kombinace	kombinace	k1gFnSc1	kombinace
snímačů	snímač	k1gInPc2	snímač
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Les	les	k1gInSc4	les
Pauly	Paula	k1gFnSc2	Paula
stala	stát	k5eAaPmAgFnS	stát
naprostým	naprostý	k2eAgInSc7d1	naprostý
standardem	standard	k1gInSc7	standard
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
HH	HH	kA	HH
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
2	[number]	k4	2
<g/>
x	x	k?	x
humbucker	humbucker	k1gInSc1	humbucker
-	-	kIx~	-
dvoucívkový	dvoucívkový	k2eAgInSc1d1	dvoucívkový
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
kytarový	kytarový	k2eAgInSc1d1	kytarový
snímač	snímač	k1gInSc1	snímač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
první	první	k4xOgInPc1	první
modely	model	k1gInPc1	model
byly	být	k5eAaImAgInP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
snímači	snímač	k1gInSc3	snímač
typu	typ	k1gInSc2	typ
P-90	P-90	k1gFnSc2	P-90
(	(	kIx(	(
<g/>
Gibson	Gibson	k1gNnSc1	Gibson
varianta	varianta	k1gFnSc1	varianta
klasického	klasický	k2eAgInSc2d1	klasický
jednocívkového	jednocívkový	k2eAgInSc2d1	jednocívkový
el.	el.	k?	el.
<g/>
mag	mag	k?	mag
<g/>
.	.	kIx.	.
snímače	snímač	k1gInSc2	snímač
<g/>
,	,	kIx,	,
typického	typický	k2eAgMnSc4d1	typický
pro	pro	k7c4	pro
kytary	kytara	k1gFnPc4	kytara
Fender	Fendra	k1gFnPc2	Fendra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snímače	snímač	k1gInPc1	snímač
P90	P90	k1gFnSc2	P90
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
přítomny	přítomen	k2eAgFnPc1d1	přítomna
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
modelech	model	k1gInPc6	model
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Gibson	Gibson	k1gInSc1	Gibson
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Reissue	Reissue	k1gFnPc6	Reissue
modelech	model	k1gInPc6	model
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přísně	přísně	k6eAd1	přísně
imitují	imitovat	k5eAaBmIp3nP	imitovat
produkci	produkce	k1gFnSc4	produkce
Gibsonu	Gibson	k1gInSc2	Gibson
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gibson	Gibson	k1gInSc1	Gibson
Les	les	k1gInSc1	les
Paul	Paula	k1gFnPc2	Paula
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejuniverzálnějších	univerzální	k2eAgFnPc2d3	nejuniverzálnější
elektrických	elektrický	k2eAgFnPc2d1	elektrická
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ji	on	k3xPp3gFnSc4	on
používají	používat	k5eAaImIp3nP	používat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
všude	všude	k6eAd1	všude
napříč	napříč	k7c7	napříč
žánry	žánr	k1gInPc7	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgFnSc7d3	nejtypičtější
vizuální	vizuální	k2eAgFnSc7d1	vizuální
úpravou	úprava	k1gFnSc7	úprava
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
burst	burst	k1gMnSc1	burst
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
typický	typický	k2eAgInSc1d1	typický
přechod	přechod	k1gInSc1	přechod
laku	lak	k1gInSc2	lak
dvou	dva	k4xCgFnPc2	dva
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
formuje	formovat	k5eAaImIp3nS	formovat
tvar	tvar	k1gInSc4	tvar
kapky	kapka	k1gFnSc2	kapka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
její	její	k3xOp3gInSc4	její
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gFnPc7	její
hlavními	hlavní	k2eAgFnPc7d1	hlavní
částmi	část	k1gFnPc7	část
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Pevná	pevný	k2eAgFnSc1d1	pevná
kobylka	kobylka	k1gFnSc1	kobylka
Kytara	kytara	k1gFnSc1	kytara
má	mít	k5eAaImIp3nS	mít
lepený	lepený	k2eAgInSc4d1	lepený
krk	krk	k1gInSc4	krk
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
spoj	spoj	k1gInSc1	spoj
není	být	k5eNaImIp3nS	být
šroubovaný	šroubovaný	k2eAgInSc1d1	šroubovaný
jako	jako	k8xS	jako
např.	např.	kA	např.
u	u	k7c2	u
kytar	kytara	k1gFnPc2	kytara
Fender	Fendra	k1gFnPc2	Fendra
<g/>
)	)	kIx)	)
Nejčastěji	často	k6eAd3	často
klenutá	klenutý	k2eAgFnSc1d1	klenutá
horní	horní	k2eAgFnSc1d1	horní
deska	deska	k1gFnSc1	deska
Trojcestný	trojcestný	k2eAgInSc4d1	trojcestný
přepínač	přepínač	k1gInSc4	přepínač
snímačů	snímač	k1gInPc2	snímač
(	(	kIx(	(
<g/>
Treble	Treble	k1gMnPc2	Treble
-	-	kIx~	-
kobylkový	kobylkový	k2eAgInSc1d1	kobylkový
snímač	snímač	k1gInSc1	snímač
<g/>
,	,	kIx,	,
Rhythm	Rhythm	k1gInSc1	Rhythm
-	-	kIx~	-
krkový	krkový	k2eAgInSc1d1	krkový
snímač	snímač	k1gInSc1	snímač
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
-	-	kIx~	-
oba	dva	k4xCgMnPc1	dva
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Potenciometry	potenciometr	k1gInPc7	potenciometr
-	-	kIx~	-
hlasitost	hlasitost	k1gFnSc1	hlasitost
a	a	k8xC	a
tónová	tónový	k2eAgFnSc1d1	tónová
clona	clona	k1gFnSc1	clona
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
snímač	snímač	k1gInSc1	snímač
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
původních	původní	k2eAgFnPc2d1	původní
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
Reissue	Reissue	k1gFnPc2	Reissue
modelů	model	k1gInPc2	model
vždy	vždy	k6eAd1	vždy
masiv	masiv	k1gInSc4	masiv
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
mahagon	mahagon	k1gInSc1	mahagon
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc1d1	horní
javor	javor	k1gInSc1	javor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
však	však	k9	však
originální	originální	k2eAgInPc1d1	originální
LP	LP	kA	LP
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
dřevin	dřevina	k1gFnPc2	dřevina
jako	jako	k9	jako
např.	např.	kA	např.
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
produkce	produkce	k1gFnSc2	produkce
prováděna	prováděn	k2eAgFnSc1d1	prováděna
redukce	redukce	k1gFnSc1	redukce
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
buďto	buďto	k8xC	buďto
vyvrtávání	vyvrtávání	k1gNnSc1	vyvrtávání
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
swiss	swiss	k1gInSc1	swiss
cheese	cheese	k1gFnSc1	cheese
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
komorování	komorování	k1gNnSc1	komorování
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
chambering	chambering	k1gInSc1	chambering
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zruční	zručný	k2eAgMnPc1d1	zručný
kytaristé	kytarista	k1gMnPc1	kytarista
si	se	k3xPyFc3	se
také	také	k9	také
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
kytary	kytara	k1gFnPc4	kytara
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poněkud	poněkud	k6eAd1	poněkud
dražší	drahý	k2eAgFnSc1d2	dražší
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
kytaristů	kytarista	k1gMnPc2	kytarista
láká	lákat	k5eAaImIp3nS	lákat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kytary	kytara	k1gFnSc2	kytara
originální	originální	k2eAgFnSc1d1	originální
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
přesně	přesně	k6eAd1	přesně
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gFnPc2	jejich
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
kopie	kopie	k1gFnSc1	kopie
Les	les	k1gInSc1	les
Paula	Paula	k1gFnSc1	Paula
<g/>
,	,	kIx,	,
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jolana	Jolana	k1gFnSc1	Jolana
Diamant	diamant	k1gInSc1	diamant
v	v	k7c6	v
Hořovicích	Hořovice	k1gFnPc6	Hořovice
<g/>
.	.	kIx.	.
kytaru	kytara	k1gFnSc4	kytara
Gibson	Gibsona	k1gFnPc2	Gibsona
Les	les	k1gInSc4	les
Paul	Paula	k1gFnPc2	Paula
vlastní	vlastní	k2eAgFnSc2d1	vlastní
mj.	mj.	kA	mj.
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gInSc1	Lister
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
britského	britský	k2eAgNnSc2d1	Britské
sci-fi	scii	k1gNnSc2	sci-fi
sitcomu	sitcom	k1gInSc2	sitcom
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gibson	Gibson	k1gInSc4	Gibson
Les	les	k1gInSc4	les
Paul	Paula	k1gFnPc2	Paula
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Gibson	Gibson	k1gMnSc1	Gibson
<g/>
(	(	kIx(	(
<g/>
výrobce	výrobce	k1gMnSc1	výrobce
Les	les	k1gInSc4	les
Paulů	Paul	k1gMnPc2	Paul
<g/>
,	,	kIx,	,
SG	SG	kA	SG
<g/>
,	,	kIx,	,
Fly	Fly	k1gFnSc1	Fly
V	v	k7c4	v
<g/>
/	/	kIx~	/
<g/>
Explorer	Explorer	k1gInSc4	Explorer
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Epiphone	Epiphon	k1gInSc5	Epiphon
<g/>
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
firmy	firma	k1gFnSc2	firma
Gibson	Gibsona	k1gFnPc2	Gibsona
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnSc2	stránka
kytaristy	kytarista	k1gMnSc2	kytarista
Les	les	k1gInSc4	les
Paula	Paul	k1gMnSc2	Paul
Informace	informace	k1gFnSc2	informace
o	o	k7c6	o
kytarách	kytara	k1gFnPc6	kytara
Gibson	Gibsona	k1gFnPc2	Gibsona
</s>
