<s>
Eugene	Eugen	k1gMnSc5	Eugen
Paul	Paul	k1gMnSc1	Paul
Wigner	Wigner	k1gInSc1	Wigner
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
maďarsky	maďarsky	k6eAd1	maďarsky
Wigner	Wigner	k1gMnSc1	Wigner
Pál	Pál	k1gMnSc1	Pál
Jenő	Jenő	k1gMnSc1	Jenő
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
získal	získat	k5eAaPmAgInS	získat
"	"	kIx"	"
<g/>
za	za	k7c4	za
příspěvky	příspěvek	k1gInPc4	příspěvek
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
objev	objev	k1gInSc4	objev
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
symetrie	symetrie	k1gFnSc2	symetrie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
aplikace	aplikace	k1gFnSc2	aplikace
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
fyziků	fyzik	k1gMnPc2	fyzik
byl	být	k5eAaImAgMnS	být
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xC	jako
tichý	tichý	k2eAgMnSc1d1	tichý
génius	génius	k1gMnSc1	génius
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
ho	on	k3xPp3gMnSc4	on
přirovnávali	přirovnávat	k5eAaImAgMnP	přirovnávat
k	k	k7c3	k
Einsteinovi	Einstein	k1gMnSc3	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gMnSc1	Wigner
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
přetvořili	přetvořit	k5eAaPmAgMnP	přetvořit
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
fyzici	fyzik	k1gMnPc1	fyzik
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
<g/>
:	:	kIx,	:
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
<g/>
,	,	kIx,	,
Erwin	Erwin	k1gMnSc1	Erwin
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc4	Dirac
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
oslnivý	oslnivý	k2eAgInSc1d1	oslnivý
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
otevřel	otevřít	k5eAaPmAgInS	otevřít
mnoho	mnoho	k4c4	mnoho
nových	nový	k2eAgFnPc2d1	nová
základních	základní	k2eAgFnPc2d1	základní
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
je	on	k3xPp3gNnSc4	on
další	další	k2eAgNnSc4d1	další
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
této	tento	k3xDgFnSc2	tento
otázky	otázka	k1gFnSc2	otázka
zodpovídali	zodpovídat	k5eAaImAgMnP	zodpovídat
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
nastolili	nastolit	k5eAaPmAgMnP	nastolit
otázky	otázka	k1gFnPc4	otázka
ještě	ještě	k9	ještě
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gInSc1	Wigner
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
druhé	druhý	k4xOgFnSc6	druhý
skupině	skupina	k1gFnSc6	skupina
těchto	tento	k3xDgMnPc2	tento
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
pojem	pojem	k1gInSc1	pojem
symetrií	symetrie	k1gFnPc2	symetrie
do	do	k7c2	do
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c4	na
atomová	atomový	k2eAgNnPc4d1	atomové
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
až	až	k9	až
1945	[number]	k4	1945
tato	tento	k3xDgFnSc1	tento
generace	generace	k1gFnSc1	generace
pomohla	pomoct	k5eAaPmAgFnS	pomoct
přetvořit	přetvořit	k5eAaPmF	přetvořit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gInSc1	Wigner
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
známých	známý	k2eAgMnPc2d1	známý
maďarsko-židovských	maďarsko-židovský	k2eAgMnPc2d1	maďarsko-židovský
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
matematiků	matematik	k1gMnPc2	matematik
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
sem	sem	k6eAd1	sem
Paul	Paul	k1gMnSc1	Paul
Erdős	Erdősa	k1gFnPc2	Erdősa
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
a	a	k8xC	a
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
američtí	americký	k2eAgMnPc1d1	americký
kolegové	kolega	k1gMnPc1	kolega
je	on	k3xPp3gFnPc4	on
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
nadpozemským	nadpozemský	k2eAgFnPc3d1	nadpozemská
<g/>
"	"	kIx"	"
schopnostem	schopnost	k1gFnPc3	schopnost
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Martians	Martiansa	k1gFnPc2	Martiansa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Marťani	marťan	k1gMnPc1	marťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k1gMnSc1	Szilárd
byl	být	k5eAaImAgMnS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
Wignera	Wigner	k1gMnSc2	Wigner
v	v	k7c4	v
dospělosti	dospělost	k1gFnPc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Neumann	Neumann	k1gMnSc1	Neumann
byl	být	k5eAaImAgMnS	být
Wignerův	Wignerův	k2eAgMnSc1d1	Wignerův
spolužák	spolužák	k1gMnSc1	spolužák
a	a	k8xC	a
rádce	rádce	k1gMnSc1	rádce
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgNnSc6	který
později	pozdě	k6eAd2	pozdě
Wigner	Wigner	k1gInSc1	Wigner
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
nejmoudřejší	moudrý	k2eAgMnSc1d3	nejmoudřejší
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgMnSc4	jaký
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
E.	E.	kA	E.
P.	P.	kA	P.
<g/>
Wigner	Wigner	k1gMnSc1	Wigner
byl	být	k5eAaImAgInS	být
však	však	k9	však
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gMnSc1	Wigner
Jenő	Jenő	k1gMnSc1	Jenő
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
r.	r.	kA	r.
1902	[number]	k4	1902
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
ani	ani	k8xC	ani
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
vědci	vědec	k1gMnPc1	vědec
považovali	považovat	k5eAaImAgMnP	považovat
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
takřka	takřka	k6eAd1	takřka
dokončená	dokončený	k2eAgFnSc1d1	dokončená
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
už	už	k9	už
objeveno	objeven	k2eAgNnSc1d1	objeveno
všechno	všechen	k3xTgNnSc1	všechen
podstatné	podstatný	k2eAgNnSc1d1	podstatné
a	a	k8xC	a
v	v	k7c4	v
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dopracovat	dopracovat	k5eAaPmF	dopracovat
už	už	k6eAd1	už
jen	jen	k9	jen
pár	pár	k4xCyI	pár
drobností	drobnost	k1gFnPc2	drobnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
jedenáct	jedenáct	k4xCc4	jedenáct
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgInS	léčit
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
harmonickém	harmonický	k2eAgNnSc6d1	harmonické
manželství	manželství	k1gNnSc6	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Jenő	Jenő	k?	Jenő
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Fasori	Fasor	k1gFnPc4	Fasor
Evangélikus	Evangélikus	k1gInSc4	Evangélikus
Gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Matematiku	matematika	k1gFnSc4	matematika
ho	on	k3xPp3gInSc2	on
učil	učit	k5eAaImAgMnS	učit
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
Rátz	Rátz	k1gMnSc1	Rátz
László	László	k1gMnSc1	László
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
učil	učit	k5eAaImAgMnS	učit
i	i	k9	i
Neumanna	Neumann	k1gMnSc4	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Fyziku	fyzika	k1gFnSc4	fyzika
ho	on	k3xPp3gInSc2	on
učil	učit	k5eAaImAgInS	učit
Sándor	Sándor	k1gInSc1	Sándor
Mikola	Mikola	k1gFnSc1	Mikola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ukončil	ukončit	k5eAaPmAgMnS	ukončit
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
Polytechniku	polytechnika	k1gFnSc4	polytechnika
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
studiu	studio	k1gNnSc6	studio
už	už	k6eAd1	už
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
odpoledne	odpoledne	k6eAd1	odpoledne
se	se	k3xPyFc4	se
zúčastňoval	zúčastňovat	k5eAaImAgMnS	zúčastňovat
setkání	setkání	k1gNnSc4	setkání
Německé	německý	k2eAgFnSc2d1	německá
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Deutschen	Deutschen	k2eAgInSc1d1	Deutschen
Physikalischen	Physikalischen	k2eAgInSc1d1	Physikalischen
Gesellschaft	Gesellschaft	k1gInSc1	Gesellschaft
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
nechyběli	chybět	k5eNaImAgMnP	chybět
velký	velký	k2eAgInSc1d1	velký
vědci	vědec	k1gMnPc7	vědec
jako	jako	k8xC	jako
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
von	von	k1gInSc4	von
Laue	Laue	k1gNnSc2	Laue
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Ladenburg	Ladenburg	k1gMnSc1	Ladenburg
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
<g/>
,	,	kIx,	,
Walther	Walthra	k1gFnPc2	Walthra
Nernst	Nernst	k1gMnSc1	Nernst
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paul	k1gMnSc3	Paul
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Wigner	Wigner	k1gMnSc1	Wigner
setkal	setkat	k5eAaPmAgMnS	setkat
i	i	k9	i
s	s	k7c7	s
Szilárdem	Szilárd	k1gInSc7	Szilárd
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
ihned	ihned	k6eAd1	ihned
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
dobrým	dobrý	k2eAgMnSc7d1	dobrý
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
osobnost	osobnost	k1gFnSc1	osobnost
Wignera	Wigner	k1gMnSc2	Wigner
mu	on	k3xPp3gMnSc3	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zahalena	zahalen	k2eAgFnSc1d1	zahalena
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnPc1	třetí
významné	významný	k2eAgFnPc1d1	významná
setkaní	setkaný	k2eAgMnPc1d1	setkaný
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c4	v
Kaiser	Kaiser	k1gInSc4	Kaiser
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Wigner	Wigner	k1gMnSc1	Wigner
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Michael	Michael	k1gMnSc1	Michael
Polanyim	Polanyim	k1gMnSc1	Polanyim
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
László	László	k1gMnSc6	László
Rátzovi	Rátz	k1gMnSc6	Rátz
jeho	jeho	k3xOp3gMnSc3	jeho
druhým	druhý	k4xOgNnSc7	druhý
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
otce	otec	k1gMnSc2	otec
jako	jako	k8xS	jako
chemický	chemický	k2eAgMnSc1d1	chemický
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odolat	odolat	k5eAaPmF	odolat
Berlínskému	berlínský	k2eAgInSc3d1	berlínský
výzkumnému	výzkumný	k2eAgInSc3d1	výzkumný
ústavu	ústav	k1gInSc3	ústav
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
do	do	k7c2	do
Heisenbergovy	Heisenbergův	k2eAgFnSc2d1	Heisenbergova
<g/>
,	,	kIx,	,
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
a	a	k8xC	a
Diracovy	Diracův	k2eAgFnSc2d1	Diracova
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
i	i	k9	i
přes	přes	k7c4	přes
mírnému	mírný	k2eAgInSc3d1	mírný
odporu	odpor	k1gInSc3	odpor
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Göttingene	Göttingen	k1gInSc5	Göttingen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
asistentem	asistent	k1gMnSc7	asistent
slavného	slavný	k2eAgMnSc2d1	slavný
matematika	matematik	k1gMnSc2	matematik
Davida	David	k1gMnSc2	David
Hilberta	Hilbert	k1gMnSc2	Hilbert
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
zklamáním	zklamání	k1gNnSc7	zklamání
pro	pro	k7c4	pro
Wignera	Wigner	k1gMnSc4	Wigner
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hilbert	Hilbert	k1gInSc1	Hilbert
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
duševně	duševně	k6eAd1	duševně
aktívní	aktívní	k2eAgInSc1d1	aktívní
a	a	k8xC	a
proto	proto	k8xC	proto
Wigner	Wigner	k1gMnSc1	Wigner
strávil	strávit	k5eAaPmAgMnS	strávit
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
v	v	k7c6	v
Göttingenské	Göttingenský	k2eAgFnSc6d1	Göttingenská
knihovně	knihovna	k1gFnSc6	knihovna
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
teorie	teorie	k1gFnSc2	teorie
symetrií	symetrie	k1gFnPc2	symetrie
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1930	[number]	k4	1930
Wignera	Wigner	k1gMnSc4	Wigner
a	a	k8xC	a
i	i	k9	i
Neumanna	Neumann	k1gMnSc4	Neumann
vzali	vzít	k5eAaPmAgMnP	vzít
na	na	k7c4	na
Princetonskou	Princetonský	k2eAgFnSc4d1	Princetonská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
tu	tu	k6eAd1	tu
našli	najít	k5eAaPmAgMnP	najít
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
ještě	ještě	k9	ještě
půl	půl	k1xP	půl
roka	rok	k1gInSc2	rok
cestovali	cestovat	k5eAaImAgMnP	cestovat
<g/>
,	,	kIx,	,
učili	učit	k5eAaImAgMnP	učit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Těžko	těžko	k6eAd1	těžko
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
mírumilovnějšího	mírumilovný	k2eAgMnSc4d2	mírumilovnější
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
Wigner	Wigner	k1gInSc1	Wigner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
hněvalo	hněvat	k5eAaImAgNnS	hněvat
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
Hitler	Hitler	k1gMnSc1	Hitler
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
lidé	člověk	k1gMnPc1	člověk
děkovali	děkovat	k5eAaImAgMnP	děkovat
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
úsudek	úsudek	k1gInSc4	úsudek
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
ohradil	ohradit	k5eAaPmAgMnS	ohradit
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
hrozby	hrozba	k1gFnSc2	hrozba
nebylo	být	k5eNaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
spíš	spíš	k9	spíš
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
to	ten	k3xDgNnSc4	ten
někdo	někdo	k3yInSc1	někdo
nerozeznal	rozeznat	k5eNaPmAgMnS	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
Wigner	Wigner	k1gMnSc1	Wigner
představil	představit	k5eAaPmAgMnS	představit
svoji	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
Margitu	Margit	k1gMnSc3	Margit
fyzikovi	fyzik	k1gMnSc3	fyzik
Paulovi	Paul	k1gMnSc3	Paul
Diracovi	Diraec	k1gMnSc3	Diraec
<g/>
.	.	kIx.	.
</s>
<s>
Margita	Margita	k1gFnSc1	Margita
a	a	k8xC	a
Dirac	Dirac	k1gInSc1	Dirac
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
a	a	k8xC	a
Wigner	Wigner	k1gInSc1	Wigner
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgInS	sblížit
s	s	k7c7	s
Diracem	Diraec	k1gInSc7	Diraec
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gMnSc1	Wigner
strávil	strávit	k5eAaPmAgMnS	strávit
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
i	i	k9	i
s	s	k7c7	s
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sem	sem	k6eAd1	sem
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Advanced	Advanced	k1gInSc4	Advanced
Study	stud	k1gInPc1	stud
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
i	i	k9	i
na	na	k7c4	na
atomová	atomový	k2eAgNnPc4d1	atomové
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
zde	zde	k6eAd1	zde
jednu	jeden	k4xCgFnSc4	jeden
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
teorií	teorie	k1gFnSc7	teorie
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
brilantním	brilantní	k2eAgMnSc7d1	brilantní
teoretikem	teoretik	k1gMnSc7	teoretik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Wigner	Wigner	k1gInSc1	Wigner
-	-	kIx~	-
Eckartův	Eckartův	k2eAgInSc1d1	Eckartův
teorém	teorém	k1gInSc1	teorém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
experimentálním	experimentální	k2eAgMnSc7d1	experimentální
fyzikem	fyzik	k1gMnSc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1936	[number]	k4	1936
už	už	k6eAd1	už
na	na	k7c6	na
Princetonu	Princeton	k1gInSc6	Princeton
o	o	k7c4	o
Wignera	Wigner	k1gMnSc4	Wigner
neprojevili	projevit	k5eNaPmAgMnP	projevit
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
i	i	k9	i
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
půvabnou	půvabný	k2eAgFnSc7d1	půvabná
studentkou	studentka	k1gFnSc7	studentka
fyziky	fyzika	k1gFnSc2	fyzika
Amelií	Amelie	k1gFnSc7	Amelie
Frankovou	Franková	k1gFnSc7	Franková
<g/>
.	.	kIx.	.
</s>
<s>
Franková	Franková	k1gFnSc1	Franková
však	však	k9	však
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
Wignera	Wigner	k1gMnSc4	Wigner
chce	chtít	k5eAaImIp3nS	chtít
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
Madisonu	Madison	k1gInSc2	Madison
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Princetonská	Princetonský	k2eAgFnSc1d1	Princetonská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
čase	čas	k1gInSc6	čas
hledala	hledat	k5eAaImAgFnS	hledat
vynikajícího	vynikající	k2eAgMnSc4d1	vynikající
mladého	mladý	k2eAgMnSc4d1	mladý
fyzika	fyzik	k1gMnSc4	fyzik
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
právě	právě	k9	právě
Wignera	Wigner	k1gMnSc2	Wigner
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
politického	politický	k2eAgMnSc4d1	politický
laika	laik	k1gMnSc4	laik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1940	[number]	k4	1940
sehrál	sehrát	k5eAaPmAgInS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
i	i	k8xC	i
v	v	k7c6	v
uskutečnění	uskutečnění	k1gNnSc6	uskutečnění
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
byl	být	k5eAaImAgInS	být
zrod	zrod	k1gInSc1	zrod
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zastavit	zastavit	k5eAaPmF	zastavit
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Wignera	Wigner	k1gMnSc4	Wigner
však	však	k9	však
velmi	velmi	k6eAd1	velmi
ranilo	ranit	k5eAaPmAgNnS	ranit
<g/>
,	,	kIx,	,
když	když	k8xS	když
atomové	atomový	k2eAgFnPc4d1	atomová
bomby	bomba	k1gFnPc4	bomba
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
USA	USA	kA	USA
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
nový	nový	k2eAgInSc4d1	nový
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gInSc1	Wigner
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
byla	být	k5eAaImAgFnS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
přijal	přijmout	k5eAaPmAgInS	přijmout
místo	místo	k1gNnSc4	místo
vedoucího	vedoucí	k1gMnSc2	vedoucí
v	v	k7c6	v
Clinton	Clinton	k1gMnSc1	Clinton
Laboratory	Laborator	k1gMnPc7	Laborator
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Oak	Oak	k1gFnSc2	Oak
Ridge	Ridg	k1gFnSc2	Ridg
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
,	,	kIx,	,
Tennessee	Tennesse	k1gInPc1	Tennesse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neměl	mít	k5eNaImAgInS	mít
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
povahu	povaha	k1gFnSc4	povaha
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
Princetonskou	Princetonský	k2eAgFnSc4d1	Princetonská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gMnSc4	on
zarmoutila	zarmoutit	k5eAaPmAgFnS	zarmoutit
smrt	smrt	k1gFnSc1	smrt
E.	E.	kA	E.
Fermiho	Fermi	k1gMnSc2	Fermi
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Einsteina	Einstein	k1gMnSc4	Einstein
a	a	k8xC	a
Neumanna	Neumann	k1gMnSc4	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
ho	on	k3xPp3gMnSc4	on
znepokojilo	znepokojit	k5eAaPmAgNnS	znepokojit
<g/>
,	,	kIx,	,
když	když	k8xS	když
prezident	prezident	k1gMnSc1	prezident
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
odebral	odebrat	k5eAaPmAgMnS	odebrat
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
prověrku	prověrka	k1gFnSc4	prověrka
J.	J.	kA	J.
Robertu	Robert	k1gMnSc3	Robert
Oppenheimerovi	Oppenheimer	k1gMnSc3	Oppenheimer
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Oppenheimerovi	Oppenheimer	k1gMnSc3	Oppenheimer
svědčil	svědčit	k5eAaImAgMnS	svědčit
i	i	k8xC	i
Wignerův	Wignerův	k2eAgMnSc1d1	Wignerův
starý	starý	k2eAgMnSc1d1	starý
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Teller	Teller	k1gInSc1	Teller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
představitelů	představitel	k1gMnPc2	představitel
matematické	matematický	k2eAgFnSc2d1	matematická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
matematiky	matematika	k1gFnSc2	matematika
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nejznámější	známý	k2eAgFnSc6d3	nejznámější
eseji	esej	k1gFnSc6	esej
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
už	už	k6eAd1	už
klasické	klasický	k2eAgFnSc2d1	klasická
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
Unreasonable	Unreasonable	k1gFnSc2	Unreasonable
Effectiveness	Effectiveness	k1gInSc1	Effectiveness
of	of	k?	of
Mathematics	Mathematics	k1gInSc1	Mathematics
in	in	k?	in
the	the	k?	the
Natural	Natural	k?	Natural
Sciences	Sciences	k1gInSc1	Sciences
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
volný	volný	k2eAgInSc1d1	volný
překlad	překlad	k1gInSc1	překlad
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
účinnost	účinnost	k1gFnSc1	účinnost
matematiky	matematika	k1gFnSc2	matematika
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
online	onlinout	k5eAaPmIp3nS	onlinout
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
poznávaní	poznávaný	k2eAgMnPc1d1	poznávaný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
konceptů	koncept	k1gInPc2	koncept
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
šťastná	šťastný	k2eAgFnSc1d1	šťastná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
fyzika	fyzika	k1gFnSc1	fyzika
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
byt	byt	k1gInSc1	byt
"	"	kIx"	"
<g/>
nerozumná	rozumný	k2eNgFnSc1d1	nerozumná
<g/>
"	"	kIx"	"
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
vysvětlitelná	vysvětlitelný	k2eAgFnSc1d1	vysvětlitelná
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
matematik	matematik	k1gMnSc1	matematik
Andrew	Andrew	k1gMnSc1	Andrew
M.	M.	kA	M.
Gleason	Gleason	k1gInSc1	Gleason
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wignerův	Wignerův	k2eAgInSc1d1	Wignerův
pohled	pohled	k1gInSc1	pohled
byl	být	k5eAaImAgInS	být
nerozumný	rozumný	k2eNgMnSc1d1	nerozumný
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mathematics	Mathematics	k1gInSc1	Mathematics
is	is	k?	is
the	the	k?	the
science	science	k1gFnSc2	science
of	of	k?	of
order	order	k1gInSc1	order
and	and	k?	and
patterns	patterns	k1gInSc1	patterns
<g/>
,	,	kIx,	,
so	so	k?	so
why	why	k?	why
should	should	k1gInSc1	should
it	it	k?	it
be	be	k?	be
a	a	k8xC	a
mystery	myster	k1gInPc1	myster
that	that	k2eAgInSc1d1	that
given	given	k2eAgInSc4d1	given
some	some	k1gInSc4	some
order	order	k1gMnSc1	order
in	in	k?	in
the	the	k?	the
universe	universe	k1gFnSc2	universe
we	we	k?	we
should	should	k1gInSc1	should
be	be	k?	be
able	ablat	k5eAaPmIp3nS	ablat
to	ten	k3xDgNnSc4	ten
select	select	k5eAaPmF	select
(	(	kIx(	(
<g/>
or	or	k?	or
construct	construct	k1gInSc1	construct
<g/>
)	)	kIx)	)
a	a	k8xC	a
mathematical	mathematicat	k5eAaPmAgMnS	mathematicat
structure	structur	k1gMnSc5	structur
to	ten	k3xDgNnSc1	ten
fit	fit	k2eAgInSc1d1	fit
the	the	k?	the
facts	facts	k1gInSc1	facts
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
volný	volný	k2eAgInSc1d1	volný
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Matematika	matematika	k1gFnSc1	matematika
je	být	k5eAaImIp3nS	být
vědou	věda	k1gFnSc7	věda
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
zákonitostí	zákonitost	k1gFnPc2	zákonitost
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
byt	byt	k1gInSc4	byt
tajemné	tajemný	k2eAgNnSc1d1	tajemné
dát	dát	k5eAaPmF	dát
vesmíru	vesmír	k1gInSc3	vesmír
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
(	(	kIx(	(
<g/>
anebo	anebo	k8xC	anebo
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
<g/>
)	)	kIx)	)
matematickou	matematický	k2eAgFnSc4d1	matematická
strukturu	struktura	k1gFnSc4	struktura
na	na	k7c4	na
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
Wigner	Wigner	k1gMnSc1	Wigner
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nenapadlo	napadnout	k5eNaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
a	a	k8xC	a
ještě	ještě	k9	ještě
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
nečekal	čekat	k5eNaImAgMnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jeden	k4xCgFnSc7	jeden
bude	být	k5eAaImBp3nS	být
moje	můj	k3xOp1gNnSc1	můj
jméno	jméno	k1gNnSc1	jméno
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
zmiňované	zmiňovaný	k2eAgInPc1d1	zmiňovaný
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
provedl	provést	k5eAaPmAgMnS	provést
nějaké	nějaký	k3yIgNnSc4	nějaký
šibalství	šibalství	k1gNnSc4	šibalství
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vydal	vydat	k5eAaPmAgInS	vydat
svoje	své	k1gNnSc4	své
paměti	paměť	k1gFnSc2	paměť
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Recollections	Recollections	k1gInSc1	Recollections
of	of	k?	of
Eugene	Eugen	k1gInSc5	Eugen
P.	P.	kA	P.
Wigner	Wignra	k1gFnPc2	Wignra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pomohl	pomoct	k5eAaPmAgMnS	pomoct
mu	on	k3xPp3gInSc3	on
Andrew	Andrew	k1gMnSc1	Andrew
Szanton	Szanton	k1gInSc1	Szanton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Wigner	Wigner	k1gMnSc1	Wigner
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
svojí	svůj	k3xOyFgFnSc7	svůj
jemností	jemnost	k1gFnSc7	jemnost
a	a	k8xC	a
zdvořilostí	zdvořilost	k1gFnSc7	zdvořilost
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
na	na	k7c6	na
Göttingenském	Göttingenský	k2eAgNnSc6d1	Göttingenské
koupališti	koupaliště	k1gNnSc6	koupaliště
společně	společně	k6eAd1	společně
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
astronomem	astronom	k1gMnSc7	astronom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Heckman	Heckman	k1gMnSc1	Heckman
<g/>
.	.	kIx.	.
</s>
<s>
Heckman	Heckman	k1gMnSc1	Heckman
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Wignerově	Wignerův	k2eAgFnSc6d1	Wignerova
pravé	pravý	k2eAgFnSc6d1	pravá
noze	noha	k1gFnSc6	noha
lezou	lézt	k5eAaImIp3nP	lézt
mravenci	mravenec	k1gMnPc1	mravenec
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ho	on	k3xPp3gNnSc4	on
štípnul	štípnout	k5eAaPmAgMnS	štípnout
<g/>
.	.	kIx.	.
</s>
<s>
Heckman	Heckman	k1gMnSc1	Heckman
se	se	k3xPyFc4	se
zeptal	zeptat	k5eAaPmAgMnS	zeptat
Wignera	Wigner	k1gMnSc2	Wigner
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
ty	ten	k3xDgMnPc4	ten
mravence	mravenec	k1gMnPc4	mravenec
nezabije	zabít	k5eNaPmIp3nS	zabít
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Protože	protože	k8xS	protože
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mne	já	k3xPp1nSc4	já
kousnul	kousnout	k5eAaPmAgMnS	kousnout
<g/>
"	"	kIx"	"
-	-	kIx~	-
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
mu	on	k3xPp3gMnSc3	on
Wigner	Wigner	k1gInSc4	Wigner
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
schůzích	schůze	k1gFnPc6	schůze
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
formálních	formální	k2eAgInPc2d1	formální
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
neformálních	formální	k2eNgFnPc2d1	neformální
Wigner	Wignra	k1gFnPc2	Wignra
často	často	k6eAd1	často
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nerozumím	rozumět	k5eNaImIp1nS	rozumět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
domýšlivý	domýšlivý	k2eAgInSc1d1	domýšlivý
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nebál	bát	k5eNaImAgMnS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
budou	být	k5eAaImBp3nP	být
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
hlupáka	hlupák	k1gMnSc4	hlupák
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Wigner	Wigner	k1gMnSc1	Wigner
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
pověrčivý	pověrčivý	k2eAgInSc1d1	pověrčivý
<g/>
,	,	kIx,	,
po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
dobré	dobrý	k2eAgFnSc6d1	dobrá
zprávě	zpráva	k1gFnSc6	zpráva
zaklepal	zaklepat	k5eAaPmAgMnS	zaklepat
na	na	k7c4	na
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
myšlení	myšlení	k1gNnSc1	myšlení
stávalo	stávat	k5eAaImAgNnS	stávat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
filosofičtější	filosofický	k2eAgFnSc1d2	filosofičtější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
memoárech	memoáry	k1gInPc6	memoáry
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
plný	plný	k2eAgInSc4d1	plný
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
význam	význam	k1gInSc4	význam
všech	všecek	k3xTgNnPc2	všecek
lidských	lidský	k2eAgNnPc2d1	lidské
přání	přání	k1gNnPc2	přání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
takovou	takový	k3xDgFnSc7	takový
záhadou	záhada	k1gFnSc7	záhada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
překonává	překonávat	k5eAaImIp3nS	překonávat
naše	náš	k3xOp1gMnPc4	náš
chápaní	chápaný	k2eAgMnPc1d1	chápaný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
rozčiloval	rozčilovat	k5eAaImAgMnS	rozčilovat
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
stavem	stav	k1gInSc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
už	už	k6eAd1	už
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
smířil	smířit	k5eAaPmAgMnS	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
ctí	ctít	k5eAaImIp3nS	ctít
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžu	můžu	k?	můžu
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgNnSc2	tento
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
významné	významný	k2eAgFnPc4d1	významná
práce	práce	k1gFnPc4	práce
patří	patřit	k5eAaImIp3nP	patřit
"	"	kIx"	"
<g/>
Vztahy	vztah	k1gInPc1	vztah
disperze	disperze	k1gFnSc2	disperze
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
kauzalitou	kauzalita	k1gFnSc7	kauzalita
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
a	a	k8xC	a
"	"	kIx"	"
<g/>
Symetrie	symetrie	k1gFnSc1	symetrie
a	a	k8xC	a
reflexe	reflexe	k1gFnSc1	reflexe
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
