<s>
Nejchladnější	chladný	k2eAgFnSc1d3	nejchladnější
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c6	na
výzkumné	výzkumný	k2eAgFnSc6d1	výzkumná
stanici	stanice	k1gFnSc6	stanice
Vostok	Vostok	k1gInSc1	Vostok
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
naměřili	naměřit	k5eAaBmAgMnP	naměřit
-89,2	-89,2	k4	-89,2
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
