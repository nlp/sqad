<s>
Mitozom	Mitozom	k1gInSc1	Mitozom
je	být	k5eAaImIp3nS	být
organela	organela	k1gFnSc1	organela
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
vnitrobuněční	vnitrobuněčný	k2eAgMnPc1d1	vnitrobuněčný
paraziti	parazit	k1gMnPc1	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
těmto	tento	k3xDgInPc3	tento
organismům	organismus	k1gInPc3	organismus
prospívá	prospívat	k5eAaImIp3nS	prospívat
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
železosirných	železosirný	k2eAgNnPc2d1	železosirný
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Mitozom	Mitozom	k1gInSc1	Mitozom
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
výhradně	výhradně	k6eAd1	výhradně
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
anaerobních	anaerobní	k2eAgInPc2d1	anaerobní
či	či	k8xC	či
mikroaerofilních	mikroaerofilní	k2eAgInPc2d1	mikroaerofilní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
oxidativní	oxidativní	k2eAgFnPc1d1	oxidativní
fosforylace	fosforylace	k1gFnPc1	fosforylace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nemají	mít	k5eNaImIp3nP	mít
mitochondrie	mitochondrie	k1gFnPc4	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
u	u	k7c2	u
střevního	střevní	k2eAgMnSc2d1	střevní
parazita	parazit	k1gMnSc2	parazit
Entamoeba	Entamoeb	k1gMnSc2	Entamoeb
histolytica	histolyticus	k1gMnSc2	histolyticus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
identifikován	identifikován	k2eAgInSc4d1	identifikován
např.	např.	kA	např.
v	v	k7c6	v
několika	několik	k4yIc6	několik
mikrosporidiích	mikrosporidium	k1gNnPc6	mikrosporidium
<g/>
,	,	kIx,	,
výtrusovci	výtrusovec	k1gMnPc1	výtrusovec
Cryptosporidium	Cryptosporidium	k1gNnSc1	Cryptosporidium
parvum	parvum	k1gNnSc1	parvum
a	a	k8xC	a
v	v	k7c6	v
diplomonádě	diplomonáda	k1gFnSc6	diplomonáda
Giardia	Giardium	k1gNnSc2	Giardium
intestinalis	intestinalis	k1gFnPc2	intestinalis
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mitozomy	mitozom	k1gInPc1	mitozom
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
z	z	k7c2	z
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ony	onen	k3xDgFnPc1	onen
mají	mít	k5eAaImIp3nP	mít
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
membránu	membrána	k1gFnSc4	membrána
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
však	však	k9	však
netvoří	tvořit	k5eNaImIp3nS	tvořit
kristy	krista	k1gFnPc4	krista
<g/>
)	)	kIx)	)
a	a	k8xC	a
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
biochemické	biochemický	k2eAgFnPc4d1	biochemická
podobnosti	podobnost	k1gFnPc4	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
proteiny	protein	k1gInPc1	protein
nalezené	nalezený	k2eAgInPc1d1	nalezený
v	v	k7c6	v
mitozomech	mitozom	k1gInPc6	mitozom
podobné	podobný	k2eAgFnSc2d1	podobná
proteinům	protein	k1gInPc3	protein
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
a	a	k8xC	a
hydrogenozomech	hydrogenozom	k1gInPc6	hydrogenozom
(	(	kIx(	(
<g/>
dalších	další	k2eAgInPc6d1	další
derivátech	derivát	k1gInPc6	derivát
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
mitozomy	mitozom	k1gInPc1	mitozom
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgInPc1	žádný
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
mitozom	mitozom	k1gInSc1	mitozom
je	být	k5eAaImIp3nS	být
syntetizován	syntetizovat	k5eAaImNgInS	syntetizovat
z	z	k7c2	z
genů	gen	k1gInPc2	gen
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc4	některý
dřívější	dřívější	k2eAgInPc4d1	dřívější
výzkumy	výzkum	k1gInPc4	výzkum
přítomnost	přítomnost	k1gFnSc1	přítomnost
DNA	dna	k1gFnSc1	dna
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
mitosome	mitosom	k1gInSc5	mitosom
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
