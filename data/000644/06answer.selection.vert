<s>
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Africa	Afric	k2eAgFnSc1d1	Africa
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Afrique	Afrique	k1gFnSc1	Afrique
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
África	Áfric	k2eAgFnSc1d1	Áfric
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
أ	أ	k?	أ
<g/>
,	,	kIx,	,
Afrī	Afrī	k1gFnSc1	Afrī
<g/>
,	,	kIx,	,
amharsky	amharsky	k6eAd1	amharsky
አ	አ	k?	አ
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Äfə	Äfə	k1gFnSc1	Äfə
<g/>
,	,	kIx,	,
svahilsky	svahilsky	k6eAd1	svahilsky
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
kontinent	kontinent	k1gInSc4	kontinent
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
30500000	[number]	k4	30500000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
20,3	[number]	k4	20,3
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
povrchu	povrch	k1gInSc2	povrch
souše	souš	k1gFnSc2	souš
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
6	[number]	k4	6
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
