<s>
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Africa	Afric	k2eAgFnSc1d1	Africa
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Afrique	Afrique	k1gFnSc1	Afrique
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
África	Áfric	k2eAgFnSc1d1	Áfric
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
أ	أ	k?	أ
<g/>
,	,	kIx,	,
Afrī	Afrī	k1gFnSc1	Afrī
<g/>
,	,	kIx,	,
amharsky	amharsky	k6eAd1	amharsky
አ	አ	k?	አ
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Äfə	Äfə	k1gFnSc1	Äfə
<g/>
,	,	kIx,	,
svahilsky	svahilsky	k6eAd1	svahilsky
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
kontinent	kontinent	k1gInSc4	kontinent
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
30500000	[number]	k4	30500000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
20,3	[number]	k4	20,3
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
povrchu	povrch	k1gInSc2	povrch
souše	souš	k1gFnSc2	souš
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
6	[number]	k4	6
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
jižně	jižně	k6eAd1	jižně
i	i	k9	i
severně	severně	k6eAd1	severně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Indickým	indický	k2eAgInSc7d1	indický
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Afriku	Afrika	k1gFnSc4	Afrika
od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Suezské	suezský	k2eAgFnSc2d1	Suezská
šíje	šíj	k1gFnSc2	šíj
je	být	k5eAaImIp3nS	být
Afrika	Afrika	k1gFnSc1	Afrika
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Euroasijskou	euroasijský	k2eAgFnSc7d1	euroasijská
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Afriky	Afrika	k1gFnSc2	Afrika
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouště	poušť	k1gFnPc1	poušť
(	(	kIx(	(
<g/>
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
,	,	kIx,	,
Kalahari	Kalahari	k1gNnSc1	Kalahari
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polopouště	polopoušť	k1gFnPc4	polopoušť
a	a	k8xC	a
savany	savana	k1gFnPc4	savana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nacházejí	nacházet	k5eAaImIp3nP	nacházet
deštné	deštný	k2eAgInPc4d1	deštný
pralesy	prales	k1gInPc4	prales
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
55	[number]	k4	55
afrických	africký	k2eAgInPc6d1	africký
státech	stát	k1gInPc6	stát
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
obyvatel	obyvatel	k1gMnSc1	obyvatel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
Africe	Afrika	k1gFnSc3	Afrika
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
černý	černý	k2eAgInSc4d1	černý
kontinent	kontinent	k1gInSc4	kontinent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
moderních	moderní	k2eAgInPc2d1	moderní
poznatků	poznatek	k1gInPc2	poznatek
je	být	k5eAaImIp3nS	být
Afrika	Afrika	k1gFnSc1	Afrika
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolébkou	kolébka	k1gFnSc7	kolébka
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vzešel	vzejít	k5eAaPmAgInS	vzejít
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kontinentu	kontinent	k1gInSc2	kontinent
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
starých	starý	k2eAgMnPc2d1	starý
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
odpovídající	odpovídající	k2eAgNnSc1d1	odpovídající
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
Africa	Africus	k1gMnSc2	Africus
terra	terr	k1gMnSc2	terr
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
Aferů	Afer	k1gMnPc2	Afer
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Afer	Afera	k1gFnPc2	Afera
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
arabského	arabský	k2eAgInSc2d1	arabský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
afer	afer	k1gInSc1	afer
<g/>
"	"	kIx"	"
=	=	kIx~	=
prach	prach	k1gInSc1	prach
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
také	také	k9	také
o	o	k7c6	o
pojmenování	pojmenování	k1gNnSc6	pojmenování
kmene	kmen	k1gInSc2	kmen
Afridi	Afrid	k1gMnPc1	Afrid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
okolo	okolo	k7c2	okolo
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnSc2	některý
další	další	k2eAgFnSc2d1	další
teorie	teorie	k1gFnSc2	teorie
názvů	název	k1gInPc2	název
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
aphrike	aphrike	k6eAd1	aphrike
<g/>
"	"	kIx"	"
=	=	kIx~	=
bez	bez	k7c2	bez
chladu	chlad	k1gInSc2	chlad
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
latinský	latinský	k2eAgInSc4d1	latinský
původ	původ	k1gInSc4	původ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
aprica	aprica	k6eAd1	aprica
<g/>
"	"	kIx"	"
=	=	kIx~	=
slunečná	slunečný	k2eAgFnSc1d1	Slunečná
<g/>
)	)	kIx)	)
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
světadíl	světadíl	k1gInSc1	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
i	i	k8xC	i
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
obrys	obrys	k1gInSc1	obrys
Afriky	Afrika	k1gFnSc2	Afrika
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
30	[number]	k4	30
319	[number]	k4	319
069	[number]	k4	069
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
má	mít	k5eAaImIp3nS	mít
málo	málo	k4c4	málo
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
)	)	kIx)	)
a	a	k8xC	a
poloostrovů	poloostrov	k1gInPc2	poloostrov
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
Somálský	somálský	k2eAgInSc1d1	somálský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgInPc1d1	africký
břehy	břeh	k1gInPc1	břeh
omývá	omývat	k5eAaImIp3nS	omývat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
a	a	k8xC	a
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
ji	on	k3xPp3gFnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Středozemní	středozemní	k2eAgNnSc1d1	středozemní
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
nejblíže	blízce	k6eAd3	blízce
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
sobě	se	k3xPyFc3	se
oba	dva	k4xCgInPc1	dva
kontinenty	kontinent	k1gInPc1	kontinent
v	v	k7c6	v
Gibraltarském	gibraltarský	k2eAgInSc6d1	gibraltarský
průlivu	průliv	k1gInSc6	průliv
(	(	kIx(	(
<g/>
14	[number]	k4	14
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Asií	Asie	k1gFnSc7	Asie
je	být	k5eAaImIp3nS	být
Afrika	Afrika	k1gFnSc1	Afrika
spojena	spojit	k5eAaPmNgFnS	spojit
Suezskou	suezský	k2eAgFnSc7d1	Suezská
šíjí	šíj	k1gFnSc7	šíj
(	(	kIx(	(
<g/>
120	[number]	k4	120
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
oba	dva	k4xCgInPc4	dva
kontinenty	kontinent	k1gInPc4	kontinent
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Bílý	bílý	k2eAgInSc1d1	bílý
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
°	°	k?	°
<g/>
21	[number]	k4	21
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Střelkový	střelkový	k2eAgInSc1d1	střelkový
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
Mys	mys	k1gInSc1	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
50	[number]	k4	50
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
°	°	k?	°
<g/>
31	[number]	k4	31
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
mys	mys	k1gInSc1	mys
Hafun	Hafun	k1gNnSc1	Hafun
(	(	kIx(	(
<g/>
51	[number]	k4	51
<g/>
°	°	k?	°
<g/>
25	[number]	k4	25
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
Nejzazší	zadní	k2eAgInPc4d3	nejzazší
body	bod	k1gInPc4	bod
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
tuniské	tuniský	k2eAgNnSc1d1	tuniské
souostroví	souostroví	k1gNnSc1	souostroví
Galite	Galit	k1gInSc5	Galit
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
°	°	k?	°
<g />
.	.	kIx.	.
</s>
<s>
<g/>
34	[number]	k4	34
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Střelkový	střelkový	k2eAgInSc1d1	střelkový
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
50	[number]	k4	50
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
kapverdský	kapverdský	k2eAgInSc1d1	kapverdský
ostrov	ostrov	k1gInSc1	ostrov
Santo	Santo	k1gNnSc1	Santo
Antã	Antã	k1gFnPc2	Antã
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
22	[number]	k4	22
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Île	Île	k1gFnSc1	Île
aux	aux	k?	aux
Cerfs	Cerfsa	k1gFnPc2	Cerfsa
u	u	k7c2	u
Mauricia	Mauricium	k1gNnSc2	Mauricium
(	(	kIx(	(
<g/>
57	[number]	k4	57
<g/>
°	°	k?	°
<g/>
49	[number]	k4	49
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
Afrika	Afrika	k1gFnSc1	Afrika
převážně	převážně	k6eAd1	převážně
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
africké	africký	k2eAgFnSc6d1	africká
litosférické	litosférický	k2eAgFnSc6d1	litosférická
desce	deska	k1gFnSc6	deska
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
na	na	k7c6	na
somálské	somálský	k2eAgFnSc6d1	Somálská
litosférické	litosférický	k2eAgFnSc6d1	litosférická
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
obou	dva	k4xCgFnPc2	dva
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
na	na	k7c6	na
africké	africký	k2eAgFnSc6d1	africká
desce	deska	k1gFnSc6	deska
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
tabule	tabule	k1gFnPc1	tabule
obklopené	obklopený	k2eAgFnPc1d1	obklopená
pánvemi	pánev	k1gFnPc7	pánev
a	a	k8xC	a
na	na	k7c6	na
pohyblivější	pohyblivý	k2eAgFnSc6d2	pohyblivější
somálské	somálský	k2eAgFnSc6d1	Somálská
desce	deska	k1gFnSc6	deska
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
hornatý	hornatý	k2eAgInSc4d1	hornatý
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
sopek	sopka	k1gFnPc2	sopka
(	(	kIx(	(
<g/>
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
horským	horský	k2eAgInSc7d1	horský
masívem	masív	k1gInSc7	masív
sopkou	sopka	k1gFnSc7	sopka
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
deskami	deska	k1gFnPc7	deska
tvoří	tvořit	k5eAaImIp3nP	tvořit
středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
příkopová	příkopový	k2eAgFnSc1d1	příkopová
propadlina	propadlina	k1gFnSc1	propadlina
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jezera	jezero	k1gNnPc1	jezero
Malawi	Malawi	k1gNnSc1	Malawi
a	a	k8xC	a
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
má	mít	k5eAaImIp3nS	mít
bohaté	bohatý	k2eAgFnPc4d1	bohatá
zásoby	zásoba	k1gFnPc4	zásoba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
:	:	kIx,	:
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
fosfátů	fosfát	k1gInPc2	fosfát
<g/>
,	,	kIx,	,
uranových	uranový	k2eAgNnPc2d1	uranové
rud	rudo	k1gNnPc2	rudo
<g/>
,	,	kIx,	,
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
Nalezneme	naleznout	k5eAaPmIp1nP	naleznout
zde	zde	k6eAd1	zde
také	také	k9	také
železné	železný	k2eAgFnPc1d1	železná
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
tuhy	tuha	k1gFnPc1	tuha
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
Třetina	třetina	k1gFnSc1	třetina
povrchu	povrch	k1gInSc2	povrch
Afriky	Afrika	k1gFnSc2	Afrika
jsou	být	k5eAaImIp3nP	být
bezodtokové	bezodtokový	k2eAgFnPc1d1	bezodtoková
pánve	pánev	k1gFnPc1	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
Nil	Nil	k1gInSc4	Nil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvodnatější	vodnatý	k2eAgInSc1d3	nejvodnatější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
velké	velký	k2eAgFnPc1d1	velká
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Niger	Niger	k1gInSc4	Niger
a	a	k8xC	a
Zambezi	Zambezi	k1gNnSc4	Zambezi
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
velká	velká	k1gFnSc1	velká
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
Ukerewe	Ukerew	k1gFnSc2	Ukerew
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
Malawi	Malawi	k1gNnSc4	Malawi
a	a	k8xC	a
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
koryta	koryto	k1gNnPc1	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
vyschlé	vyschlý	k2eAgNnSc1d1	vyschlé
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
se	se	k3xPyFc4	se
zaplňují	zaplňovat	k5eAaImIp3nP	zaplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
korytům	koryto	k1gNnPc3	koryto
říkáme	říkat	k5eAaImIp1nP	říkat
vádí	vádí	k1gNnPc7	vádí
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
prostředek	prostředek	k1gInSc1	prostředek
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
klimatu	klima	k1gNnSc6	klima
mezi	mezi	k7c7	mezi
obratníky	obratník	k1gInPc7	obratník
Raka	rak	k1gMnSc2	rak
a	a	k8xC	a
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemní	vnitrozemní	k2eAgFnSc6d1	vnitrozemní
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
pralesem	prales	k1gInSc7	prales
je	být	k5eAaImIp3nS	být
ekvatoriální	ekvatoriální	k2eAgNnSc1d1	ekvatoriální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
v	v	k7c6	v
přímořské	přímořský	k2eAgFnSc6d1	přímořská
oblasti	oblast	k1gFnSc6	oblast
poté	poté	k6eAd1	poté
tropické	tropický	k2eAgNnSc4d1	tropické
monzunové	monzunový	k2eAgNnSc4d1	monzunové
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Pás	pás	k1gInSc1	pás
Sahelu	Sahel	k1gInSc2	Sahel
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Botswany	Botswana	k1gFnSc2	Botswana
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
semiaridním	semiaridní	k2eAgNnSc6d1	semiaridní
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc4	oblast
pouští	pouštět	k5eAaImIp3nS	pouštět
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
,	,	kIx,	,
Namib	Namib	k1gMnSc1	Namib
či	či	k8xC	či
Danakil	Danakil	k1gMnSc1	Danakil
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
aridním	aridní	k2eAgNnSc6d1	aridní
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
nejsevernější	severní	k2eAgFnPc4d3	nejsevernější
a	a	k8xC	a
nejjižnější	jižní	k2eAgFnPc4d3	nejjižnější
přímořské	přímořský	k2eAgFnPc4d1	přímořská
oblasti	oblast	k1gFnPc4	oblast
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
středozemním	středozemní	k2eAgNnSc6d1	středozemní
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
světadíl	světadíl	k1gInSc4	světadíl
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
naměřen	naměřen	k2eAgInSc4d1	naměřen
teplotní	teplotní	k2eAgInSc4d1	teplotní
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
teplota	teplota	k1gFnSc1	teplota
58	[number]	k4	58
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
zdiskreditován	zdiskreditovat	k5eAaPmNgInS	zdiskreditovat
a	a	k8xC	a
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
tvoří	tvořit	k5eAaImIp3nS	tvořit
60	[number]	k4	60
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
pouště	poušť	k1gFnSc2	poušť
a	a	k8xC	a
suché	suchý	k2eAgFnSc2d1	suchá
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
nastává	nastávat	k5eAaImIp3nS	nastávat
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
období	období	k1gNnSc2	období
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
až	až	k6eAd1	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
se	se	k3xPyFc4	se
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor.	hor.	k?	hor.
Pravidelně	pravidelně	k6eAd1	pravidelně
sněží	sněžit	k5eAaImIp3nS	sněžit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pohoří	pohoří	k1gNnSc6	pohoří
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
je	být	k5eAaImIp3nS	být
Afrika	Afrika	k1gFnSc1	Afrika
zasažena	zasažen	k2eAgFnSc1d1	zasažena
odlesňováním	odlesňování	k1gNnSc7	odlesňování
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
průměr	průměr	k1gInSc1	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
čtyři	čtyři	k4xCgNnPc1	čtyři
miliony	milion	k4xCgInPc1	milion
hektarů	hektar	k1gInPc2	hektar
lesů	les	k1gInPc2	les
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
americké	americký	k2eAgFnSc2d1	americká
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
celkem	celkem	k6eAd1	celkem
65	[number]	k4	65
%	%	kIx~	%
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
31	[number]	k4	31
%	%	kIx~	%
pastvin	pastvina	k1gFnPc2	pastvina
a	a	k8xC	a
19	[number]	k4	19
%	%	kIx~	%
lesů	les	k1gInPc2	les
zasaženo	zasažen	k2eAgNnSc1d1	zasaženo
degradací	degradace	k1gFnSc7	degradace
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
77	[number]	k4	77
%	%	kIx~	%
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
erozí	eroze	k1gFnSc7	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
přišel	přijít	k5eAaPmAgInS	přijít
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
o	o	k7c4	o
90	[number]	k4	90
%	%	kIx~	%
svých	svůj	k3xOyFgInPc2	svůj
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
africkém	africký	k2eAgNnSc6d1	africké
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
240	[number]	k4	240
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
:	:	kIx,	:
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
(	(	kIx(	(
<g/>
Uhuru	Uhur	k1gInSc2	Uhur
<g/>
)	)	kIx)	)
5895	[number]	k4	5895
m	m	kA	m
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
<g/>
:	:	kIx,	:
Nil	Nil	k1gInSc1	Nil
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
Kagera	Kagera	k1gFnSc1	Kagera
<g/>
)	)	kIx)	)
-	-	kIx~	-
6671	[number]	k4	6671
km	km	kA	km
Největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
<g/>
:	:	kIx,	:
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
-	-	kIx~	-
586	[number]	k4	586
690	[number]	k4	690
km2	km2	k4	km2
Největší	veliký	k2eAgInSc4d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Ukerewe	Ukerewe	k1gFnSc1	Ukerewe
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
69	[number]	k4	69
485	[number]	k4	485
km2	km2	k4	km2
Největší	veliký	k2eAgFnSc1d3	veliký
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
<g/>
:	:	kIx,	:
Kamerunská	kamerunský	k2eAgFnSc1d1	kamerunská
hora	hora	k1gFnSc1	hora
4095	[number]	k4	4095
m	m	kA	m
(	(	kIx(	(
<g/>
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
)	)	kIx)	)
Největší	veliký	k2eAgInSc1d3	veliký
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
:	:	kIx,	:
Somálský	somálský	k2eAgInSc1d1	somálský
poloostrov	poloostrov	k1gInSc1	poloostrov
-	-	kIx~	-
850	[number]	k4	850
000	[number]	k4	000
km2	km2	k4	km2
Nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Nigérie	Nigérie	k1gFnSc1	Nigérie
-	-	kIx~	-
126	[number]	k4	126
635	[number]	k4	635
626	[number]	k4	626
obyvatel	obyvatel	k1gMnPc2	obyvatel
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
obydleným	obydlený	k2eAgNnSc7d1	obydlené
územím	území	k1gNnSc7	území
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgInSc1d1	lidský
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
30	[number]	k4	30
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Fajjúm	Fajjúma	k1gFnPc2	Fajjúma
(	(	kIx(	(
<g/>
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
)	)	kIx)	)
předek	předek	k1gMnSc1	předek
člověka	člověk	k1gMnSc2	člověk
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
Aegyptopithecus	Aegyptopithecus	k1gInSc4	Aegyptopithecus
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
6	[number]	k4	6
-	-	kIx~	-
8	[number]	k4	8
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
oddělila	oddělit	k5eAaPmAgFnS	oddělit
vývojová	vývojový	k2eAgFnSc1d1	vývojová
větev	větev	k1gFnSc1	větev
moderních	moderní	k2eAgMnPc2d1	moderní
šimpanzů	šimpanz	k1gMnPc2	šimpanz
(	(	kIx(	(
<g/>
Panina	Panin	k2eAgFnSc1d1	Panin
<g/>
)	)	kIx)	)
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Hominina	Hominin	k2eAgFnSc1d1	Hominin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgMnPc4	první
příslušníky	příslušník	k1gMnPc4	příslušník
samostatné	samostatný	k2eAgFnSc2d1	samostatná
lidské	lidský	k2eAgFnSc2d1	lidská
vývojové	vývojový	k2eAgFnSc2d1	vývojová
linie	linie	k1gFnSc2	linie
po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
předků	předek	k1gInPc2	předek
šimpanzů	šimpanz	k1gMnPc2	šimpanz
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
patřit	patřit	k5eAaImF	patřit
rody	rod	k1gInPc1	rod
Sahelanthropus	Sahelanthropus	k1gInSc1	Sahelanthropus
<g/>
,	,	kIx,	,
Orrorin	Orrorin	k1gInSc1	Orrorin
a	a	k8xC	a
Ardipithecus	Ardipithecus	k1gInSc1	Ardipithecus
<g/>
,	,	kIx,	,
nalézané	nalézaný	k2eAgFnPc1d1	nalézaná
v	v	k7c6	v
Čadu	Čad	k1gInSc6	Čad
<g/>
,	,	kIx,	,
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
4,2	[number]	k4	4,2
-	-	kIx~	-
1,3	[number]	k4	1,3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
pak	pak	k8xC	pak
obývaly	obývat	k5eAaImAgInP	obývat
východní	východní	k2eAgFnSc4d1	východní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc4d1	střední
i	i	k8xC	i
jižní	jižní	k2eAgFnSc4d1	jižní
Afriku	Afrika	k1gFnSc4	Afrika
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
australopitékus	australopitékus	k1gInSc1	australopitékus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
2,5	[number]	k4	2,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
první	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gNnSc1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
typy	typ	k1gInPc4	typ
byly	být	k5eAaImAgFnP	být
prvotní	prvotní	k2eAgFnPc4d1	prvotní
formy	forma	k1gFnPc4	forma
Homo	Homo	k6eAd1	Homo
habilis	habilis	k1gFnPc4	habilis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
zručný	zručný	k2eAgMnSc1d1	zručný
<g/>
)	)	kIx)	)
a	a	k8xC	a
Homo	Homo	k1gMnSc1	Homo
rudolfensis	rudolfensis	k1gFnSc2	rudolfensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
východoafrický	východoafrický	k2eAgMnSc1d1	východoafrický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
předky	předek	k1gMnPc7	předek
druhu	druh	k1gInSc2	druh
Homo	Homo	k1gMnSc1	Homo
ergaster	ergaster	k1gMnSc1	ergaster
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
zručný	zručný	k2eAgMnSc1d1	zručný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
období	období	k1gNnSc6	období
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
již	již	k6eAd1	již
začal	začít	k5eAaPmAgMnS	začít
užívat	užívat	k5eAaImF	užívat
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
pěstní	pěstní	k2eAgInSc4d1	pěstní
klín	klín	k1gInSc4	klín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Keni	Keňa	k1gFnSc2	Keňa
a	a	k8xC	a
Tanzánie	Tanzánie	k1gFnSc2	Tanzánie
se	se	k3xPyFc4	se
nalezly	naleznout	k5eAaPmAgInP	naleznout
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
druhů	druh	k1gMnPc2	druh
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gMnSc1	erectus
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
vzpřímený	vzpřímený	k2eAgMnSc1d1	vzpřímený
<g/>
)	)	kIx)	)
a	a	k8xC	a
Homo	Homo	k1gMnSc1	Homo
heidelbergensis	heidelbergensis	k1gFnSc2	heidelbergensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
heidelberský	heidelberský	k2eAgMnSc1d1	heidelberský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
nalezly	nalézt	k5eAaBmAgInP	nalézt
také	také	k9	také
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
již	již	k6eAd1	již
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
typické	typický	k2eAgInPc1d1	typický
rysy	rys	k1gInPc1	rys
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
typy	typ	k1gInPc4	typ
afrického	africký	k2eAgMnSc4d1	africký
pravěkého	pravěký	k2eAgMnSc4d1	pravěký
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
nalezené	nalezený	k2eAgFnPc1d1	nalezená
africké	africký	k2eAgFnPc1d1	africká
jeskynní	jeskynní	k2eAgFnPc1d1	jeskynní
malby	malba	k1gFnPc1	malba
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
období	období	k1gNnSc2	období
50	[number]	k4	50
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
nástrojů	nástroj	k1gInPc2	nástroj
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
afričtí	africký	k2eAgMnPc1d1	africký
pravěcí	pravěký	k2eAgMnPc1d1	pravěký
lidé	člověk	k1gMnPc1	člověk
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
38	[number]	k4	38
000	[number]	k4	000
-	-	kIx~	-
26	[number]	k4	26
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Neolit	neolit	k1gInSc1	neolit
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
období	období	k1gNnSc6	období
8	[number]	k4	8
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
užívána	užívat	k5eAaImNgFnS	užívat
mikrolitická	mikrolitický	k2eAgFnSc1d1	mikrolitický
industrie	industrie	k1gFnSc1	industrie
(	(	kIx(	(
<g/>
pomongwaská	pomongwaský	k2eAgFnSc1d1	pomongwaský
<g/>
,	,	kIx,	,
nachikufanská	nachikufanský	k2eAgFnSc1d1	nachikufanský
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
kultury	kultura	k1gFnPc1	kultura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
rozkvět	rozkvět	k1gInSc4	rozkvět
měla	mít	k5eAaImAgFnS	mít
industrie	industrie	k1gFnSc1	industrie
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
západní	západní	k2eAgFnSc6d1	západní
Nigérii	Nigérie	k1gFnSc6	Nigérie
a	a	k8xC	a
v	v	k7c6	v
pralesích	prales	k1gInPc6	prales
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kamerunu	Kamerun	k1gInSc2	Kamerun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
severoafrické	severoafrický	k2eAgFnPc1d1	severoafrická
kultury	kultura	k1gFnPc1	kultura
osvojily	osvojit	k5eAaPmAgFnP	osvojit
hrnčířství	hrnčířství	k1gNnSc4	hrnčířství
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gMnSc2	dobytek
a	a	k8xC	a
přešly	přejít	k5eAaPmAgFnP	přejít
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
neolitická	neolitický	k2eAgFnSc1d1	neolitická
revoluce	revoluce	k1gFnSc1	revoluce
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
také	také	k9	také
do	do	k7c2	do
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
plodinami	plodina	k1gFnPc7	plodina
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgInP	být
čirok	čirok	k1gInSc4	čirok
a	a	k8xC	a
proso	proso	k1gNnSc4	proso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
6	[number]	k4	6
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
začít	začít	k5eAaPmF	začít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
odlišných	odlišný	k2eAgInPc6d1	odlišný
lidských	lidský	k2eAgInPc6d1	lidský
typech	typ	k1gInPc6	typ
-	-	kIx~	-
sansko-khoinský	sanskohoinský	k2eAgInSc1d1	sansko-khoinský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
žil	žít	k5eAaImAgInS	žít
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
pygmejský	pygmejský	k2eAgMnSc1d1	pygmejský
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
negroidní	negroidní	k2eAgFnSc3d1	negroidní
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
mediterrání	mediterránit	k5eAaPmIp3nS	mediterránit
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
5	[number]	k4	5
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
chov	chov	k1gInSc4	chov
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgNnPc4	první
stálá	stálý	k2eAgNnPc4d1	stálé
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Fajjúm	Fajjúm	k1gInSc1	Fajjúm
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
přelomem	přelom	k1gInSc7	přelom
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
začali	začít	k5eAaPmAgMnP	začít
afričtí	africký	k2eAgMnPc1d1	africký
lidé	člověk	k1gMnPc1	člověk
užívat	užívat	k5eAaImF	užívat
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
již	již	k6eAd1	již
probíhala	probíhat	k5eAaImAgFnS	probíhat
doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
civilizace	civilizace	k1gFnPc1	civilizace
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
především	především	k6eAd1	především
na	na	k7c6	na
území	území	k1gNnSc6	území
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
výrazně	výrazně	k6eAd1	výrazně
propojeny	propojit	k5eAaPmNgFnP	propojit
se	s	k7c7	s
starověkým	starověký	k2eAgInSc7d1	starověký
Orientem	Orient	k1gInSc7	Orient
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platilo	platit	k5eAaImAgNnS	platit
především	především	k9	především
pro	pro	k7c4	pro
říše	říš	k1gFnPc4	říš
Núbie	Núbie	k1gFnSc2	Núbie
(	(	kIx(	(
<g/>
města	město	k1gNnSc2	město
Meroe	Mero	k1gFnSc2	Mero
<g/>
,	,	kIx,	,
Napata	Napata	k1gFnSc1	Napata
<g/>
,	,	kIx,	,
Kerma	Kerma	k1gFnSc1	Kerma
<g/>
)	)	kIx)	)
a	a	k8xC	a
starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
říší	říš	k1gFnSc7	říš
byl	být	k5eAaImAgInS	být
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
s	s	k7c7	s
proměnlivým	proměnlivý	k2eAgInSc7d1	proměnlivý
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
civilizace	civilizace	k1gFnPc4	civilizace
od	od	k7c2	od
období	období	k1gNnSc2	období
3300	[number]	k4	3300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
343	[number]	k4	343
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Egypt	Egypt	k1gInSc1	Egypt
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
státní	státní	k2eAgFnSc7d1	státní
formou	forma	k1gFnSc7	forma
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
základy	základ	k1gInPc1	základ
byly	být	k5eAaImAgInP	být
položeny	položit	k5eAaPmNgInP	položit
na	na	k7c4	na
území	území	k1gNnSc4	území
Horního	horní	k2eAgInSc2d1	horní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Cenejské	Cenejský	k2eAgNnSc1d1	Cenejský
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dobylo	dobýt	k5eAaPmAgNnS	dobýt
území	území	k1gNnSc4	území
Dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
ucelený	ucelený	k2eAgInSc4d1	ucelený
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
navázal	navázat	k5eAaPmAgMnS	navázat
Egypt	Egypt	k1gInSc4	Egypt
obchodní	obchodní	k2eAgNnSc4d1	obchodní
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
východoafrickou	východoafrický	k2eAgFnSc7d1	východoafrická
zemí	zem	k1gFnSc7	zem
Punt	punt	k1gInSc1	punt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
řecké	řecký	k2eAgFnSc2d1	řecká
kolonizace	kolonizace	k1gFnSc2	kolonizace
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Naukratis	Naukratis	k1gFnSc2	Naukratis
<g/>
,	,	kIx,	,
Kyréna	Kyréno	k1gNnSc2	Kyréno
nebo	nebo	k8xC	nebo
Apollonia	Apollonium	k1gNnSc2	Apollonium
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kolonie	kolonie	k1gFnPc1	kolonie
daly	dát	k5eAaPmAgFnP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
oblasti	oblast	k1gFnPc4	oblast
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Libye	Libye	k1gFnSc2	Libye
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Afriky	Afrika	k1gFnSc2	Afrika
vznikaly	vznikat	k5eAaImAgInP	vznikat
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
Féničanů	Féničan	k1gMnPc2	Féničan
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc7d2	pozdější
civilizací	civilizace	k1gFnSc7	civilizace
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
východoafrické	východoafrický	k2eAgNnSc1d1	Východoafrické
Aksumské	Aksumský	k2eAgNnSc1d1	Aksumské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
přijalo	přijmout	k5eAaPmAgNnS	přijmout
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
africký	africký	k2eAgInSc1d1	africký
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
křesťanství	křesťanství	k1gNnSc2	křesťanství
přijala	přijmout	k5eAaPmAgFnS	přijmout
i	i	k8xC	i
etnika	etnikum	k1gNnPc1	etnikum
žijící	žijící	k2eAgNnPc1d1	žijící
v	v	k7c6	v
Núbii	Núbie	k1gFnSc6	Núbie
(	(	kIx(	(
<g/>
koptové	koptový	k2eAgNnSc1d1	koptové
a	a	k8xC	a
etiopové	etiopový	k2eAgNnSc1d1	etiopový
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
víra	víra	k1gFnSc1	víra
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
koptské	koptský	k2eAgFnSc3d1	koptská
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
etiopské	etiopský	k2eAgFnSc3d1	etiopská
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
území	území	k1gNnSc4	území
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
přímořské	přímořský	k2eAgFnSc2d1	přímořská
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
berberské	berberský	k2eAgNnSc1d1	berberské
království	království	k1gNnSc1	království
Numidie	Numidie	k1gFnSc1	Numidie
<g/>
,	,	kIx,	,
Kartágo	Kartágo	k1gNnSc1	Kartágo
či	či	k8xC	či
území	území	k1gNnSc1	území
kmene	kmen	k1gInSc2	kmen
Libu	Liba	k1gFnSc4	Liba
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
dobyta	dobyt	k2eAgFnSc1d1	dobyta
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
římská	římský	k2eAgFnSc1d1	římská
provincie	provincie	k1gFnSc1	provincie
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
stalo	stát	k5eAaPmAgNnS	stát
provincií	provincie	k1gFnSc7	provincie
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
důsledkem	důsledek	k1gInSc7	důsledek
islámské	islámský	k2eAgFnSc2d1	islámská
expanze	expanze	k1gFnSc2	expanze
provincií	provincie	k1gFnPc2	provincie
Arabské	arabský	k2eAgFnSc2d1	arabská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
Egypt	Egypt	k1gInSc1	Egypt
byl	být	k5eAaImAgInS	být
Araby	Arab	k1gMnPc7	Arab
dobyt	dobyt	k2eAgInSc4d1	dobyt
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
641	[number]	k4	641
<g/>
,	,	kIx,	,
Maghreb	Maghrba	k1gFnPc2	Maghrba
až	až	k8xS	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
690	[number]	k4	690
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Sahelu	Sahel	k1gInSc2	Sahel
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
města	město	k1gNnSc2	město
Tichit	Tichit	k1gInSc4	Tichit
(	(	kIx(	(
<g/>
Dhar	Dhar	k1gMnSc1	Dhar
Tichitt	Tichitt	k1gMnSc1	Tichitt
<g/>
)	)	kIx)	)
a	a	k8xC	a
Walata	Walata	k1gFnSc1	Walata
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zakládající	zakládající	k2eAgNnPc1d1	zakládající
centra	centrum	k1gNnPc1	centrum
obchodní	obchodní	k2eAgFnSc2d1	obchodní
transsaharské	transsaharský	k2eAgFnSc2d1	transsaharský
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Tichit	Tichita	k1gFnPc2	Tichita
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
Ghanské	ghanský	k2eAgFnSc2d1	ghanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
město	město	k1gNnSc1	město
Djenné	Djenný	k2eAgNnSc1d1	Djenné
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
civilizačně	civilizačně	k6eAd1	civilizačně
méně	málo	k6eAd2	málo
rozvinuté	rozvinutý	k2eAgInPc1d1	rozvinutý
a	a	k8xC	a
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
až	až	k9	až
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
Například	například	k6eAd1	například
zpracování	zpracování	k1gNnSc1	zpracování
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
známo	znám	k2eAgNnSc4d1	známo
již	již	k6eAd1	již
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
až	až	k9	až
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
také	také	k6eAd1	také
začala	začít	k5eAaPmAgFnS	začít
migrace	migrace	k1gFnSc1	migrace
protoBantuů	protoBantu	k1gMnPc2	protoBantu
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Bantuů	Bantu	k1gMnPc2	Bantu
na	na	k7c6	na
území	území	k1gNnSc6	území
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
1500	[number]	k4	1500
n.	n.	k?	n.
l.	l.	k?	l.
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
samostatném	samostatný	k2eAgInSc6d1	samostatný
vývoji	vývoj	k1gInSc6	vývoj
Afriky	Afrika	k1gFnSc2	Afrika
či	či	k8xC	či
o	o	k7c6	o
africkém	africký	k2eAgInSc6d1	africký
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
reformovaly	reformovat	k5eAaBmAgFnP	reformovat
staré	starý	k2eAgFnPc1d1	stará
říše	říš	k1gFnPc1	říš
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
například	například	k6eAd1	například
Etiopské	etiopský	k2eAgNnSc1d1	etiopské
císařství	císařství	k1gNnSc1	císařství
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivní	masivní	k2eAgFnSc3d1	masivní
arabské	arabský	k2eAgFnSc3d1	arabská
migraci	migrace	k1gFnSc3	migrace
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
jižně	jižně	k6eAd1	jižně
podél	podél	k7c2	podél
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
islámské	islámský	k2eAgFnSc2d1	islámská
expanze	expanze	k1gFnSc2	expanze
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Núbii	Núbie	k1gFnSc6	Núbie
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
arabizaci	arabizace	k1gFnSc3	arabizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
k	k	k7c3	k
pozvolné	pozvolný	k2eAgFnSc3d1	pozvolná
islamizaci	islamizace	k1gFnSc3	islamizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maghrebu	Maghreb	k1gInSc6	Maghreb
byl	být	k5eAaImAgInS	být
postup	postup	k1gInSc1	postup
Arabů	Arab	k1gMnPc2	Arab
pomalejší	pomalý	k2eAgInPc1d2	pomalejší
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
zde	zde	k6eAd1	zde
naopak	naopak	k6eAd1	naopak
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
islamizaci	islamizace	k1gFnSc3	islamizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomalé	pomalý	k2eAgFnSc6d1	pomalá
arabizaci	arabizace	k1gFnSc6	arabizace
<g/>
.	.	kIx.	.
</s>
<s>
Berbeři	Berber	k1gMnPc1	Berber
dlouho	dlouho	k6eAd1	dlouho
odmítali	odmítat	k5eAaImAgMnP	odmítat
arabskou	arabský	k2eAgFnSc4d1	arabská
nadvládu	nadvláda	k1gFnSc4	nadvláda
a	a	k8xC	a
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dynastie	dynastie	k1gFnSc2	dynastie
Almorávidů	Almorávid	k1gMnPc2	Almorávid
zcela	zcela	k6eAd1	zcela
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Maroko	Maroko	k1gNnSc1	Maroko
a	a	k8xC	a
jih	jih	k1gInSc1	jih
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
nastoupily	nastoupit	k5eAaPmAgFnP	nastoupit
další	další	k2eAgFnPc1d1	další
berberské	berberský	k2eAgFnPc1d1	berberská
dynastie	dynastie	k1gFnPc1	dynastie
Almohadové	Almohadový	k2eAgFnPc1d1	Almohadový
<g/>
,	,	kIx,	,
Marínovci	Marínovec	k1gMnPc1	Marínovec
<g/>
,	,	kIx,	,
Vattásovci	Vattásovec	k1gMnPc1	Vattásovec
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
až	až	k9	až
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
dynastie	dynastie	k1gFnSc2	dynastie
Saadů	Saad	k1gMnPc2	Saad
(	(	kIx(	(
<g/>
po	po	k7c6	po
nich	on	k3xPp3gNnPc6	on
již	již	k6eAd1	již
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
Alaouite	Alaouit	k1gMnSc5	Alaouit
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vládnou	vládnout	k5eAaImIp3nP	vládnout
Maroku	Maroko	k1gNnSc6	Maroko
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc7	jejich
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
bylo	být	k5eAaImAgNnS	být
Mahdia	Mahdium	k1gNnPc4	Mahdium
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInPc4d1	ležící
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
)	)	kIx)	)
rozpínal	rozpínat	k5eAaImAgInS	rozpínat
arabský	arabský	k2eAgInSc1d1	arabský
Fátimovský	fátimovský	k2eAgInSc1d1	fátimovský
chalífát	chalífát	k1gInSc1	chalífát
(	(	kIx(	(
<g/>
vyznávající	vyznávající	k2eAgNnSc1d1	vyznávající
učení	učení	k1gNnSc1	učení
Ismá	Ismá	k1gFnSc1	Ismá
<g/>
'	'	kIx"	'
<g/>
ílíja	ílíja	k1gFnSc1	ílíja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postupně	postupně	k6eAd1	postupně
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
Levantu	Levanta	k1gFnSc4	Levanta
a	a	k8xC	a
Hidžáz	Hidžáz	k1gInSc4	Hidžáz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
severní	severní	k2eAgFnSc7d1	severní
Afrikou	Afrika	k1gFnSc7	Afrika
znovu	znovu	k6eAd1	znovu
chopili	chopit	k5eAaPmAgMnP	chopit
blízkovýchodní	blízkovýchodní	k2eAgMnPc1d1	blízkovýchodní
Arabové	Arab	k1gMnPc1	Arab
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kurdské	kurdský	k2eAgFnSc2d1	kurdská
dynastie	dynastie	k1gFnSc2	dynastie
Ajjúbovců	Ajjúbovec	k1gInPc2	Ajjúbovec
a	a	k8xC	a
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
ustaven	ustaven	k2eAgInSc4d1	ustaven
Mamlúcký	mamlúcký	k2eAgInSc4d1	mamlúcký
sultanát	sultanát	k1gInSc4	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
arabské	arabský	k2eAgFnSc2d1	arabská
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Súdán	Súdán	k1gInSc1	Súdán
a	a	k8xC	a
Čad	Čad	k1gInSc1	Čad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Osmané	Osman	k1gMnPc1	Osman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
historické	historický	k2eAgFnSc6d1	historická
etapě	etapa	k1gFnSc6	etapa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
celá	celý	k2eAgFnSc1d1	celá
řadě	řada	k1gFnSc6	řada
nových	nový	k2eAgInPc2d1	nový
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Epicentra	epicentrum	k1gNnPc1	epicentrum
zrodů	zrod	k1gInPc2	zrod
států	stát	k1gInPc2	stát
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
lze	lze	k6eAd1	lze
seřadit	seřadit	k5eAaPmF	seřadit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
Oblast	oblast	k1gFnSc1	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
,	,	kIx,	,
Sahelu	Sahel	k1gInSc2	Sahel
a	a	k8xC	a
Senegalu	Senegal	k1gInSc2	Senegal
(	(	kIx(	(
<g/>
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
rozmach	rozmach	k1gInSc1	rozmach
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Kanem	Kanem	k?	Kanem
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Tekrúr	Tekrúr	k1gMnSc1	Tekrúr
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Mali	Mali	k1gNnSc2	Mali
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Songhajská	Songhajský	k2eAgFnSc1d1	Songhajský
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Bagírmi	Bagír	k1gFnPc7	Bagír
<g/>
,	,	kIx,	,
Wadaj	Wadaj	k1gFnSc4	Wadaj
a	a	k8xC	a
Wolof	Wolof	k1gInSc4	Wolof
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
14	[number]	k4	14
<g/>
<g />
.	.	kIx.	.
</s>
<s>
stol.	stol.	k?	stol.
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
Oblast	oblast	k1gFnSc1	oblast
meziříčí	meziříčí	k1gNnSc2	meziříčí
řek	řeka	k1gFnPc2	řeka
Limpopo	Limpopa	k1gFnSc5	Limpopa
a	a	k8xC	a
Zambezi	Zambezi	k1gNnSc2	Zambezi
(	(	kIx(	(
<g/>
království	království	k1gNnSc1	království
Mutapa	Mutap	k1gMnSc2	Mutap
či	či	k8xC	či
Monomotapa	Monomotap	k1gMnSc2	Monomotap
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
Konžská	konžský	k2eAgFnSc1d1	Konžská
pánev	pánev	k1gFnSc1	pánev
(	(	kIx(	(
<g/>
Konžské	konžský	k2eAgNnSc1d1	konžské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
státy	stát	k1gInPc1	stát
Lumbů	Lumb	k1gMnPc2	Lumb
a	a	k8xC	a
Lundů	Lunda	k1gMnPc2	Lunda
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
)	)	kIx)	)
Pásmo	pásmo	k1gNnSc1	pásmo
guinejského	guinejský	k2eAgNnSc2d1	guinejské
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
říše	říš	k1gFnSc2	říš
Ojo	Ojo	k1gFnSc2	Ojo
neboli	neboli	k8xC	neboli
říše	říš	k1gFnSc2	říš
Jorubů	Joruba	k1gMnPc2	Joruba
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
a	a	k8xC	a
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
Akanů	Akan	k1gMnPc2	Akan
<g/>
,	,	kIx,	,
Fonů	Fon	k1gMnPc2	Fon
či	či	k8xC	či
Eweů	Ewe	k1gMnPc2	Ewe
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
<g />
.	.	kIx.	.
</s>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
)	)	kIx)	)
Oblast	oblast	k1gFnSc4	oblast
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
království	království	k1gNnSc1	království
Buňoro-Kitara	Buňoro-Kitara	k1gFnSc1	Buňoro-Kitara
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Buganda	Buganda	k1gFnSc1	Buganda
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rwanda	Rwanda	k1gFnSc1	Rwanda
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
království	království	k1gNnSc2	království
Mapungubwe	Mapungubwe	k1gNnSc2	Mapungubwe
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
existovalo	existovat	k5eAaImAgNnS	existovat
mezi	mezi	k7c7	mezi
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
v	v	k7c6	v
regionu	region	k1gInSc6	region
dominovalo	dominovat	k5eAaImAgNnS	dominovat
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kolonizace	kolonizace	k1gFnSc2	kolonizace
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
byla	být	k5eAaImAgFnS	být
Evropanům	Evropan	k1gMnPc3	Evropan
známa	znám	k2eAgNnPc1d1	známo
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
objevování	objevování	k1gNnSc1	objevování
sub-saharské	subaharský	k2eAgFnSc2d1	sub-saharský
Afriky	Afrika	k1gFnSc2	Afrika
začalo	začít	k5eAaPmAgNnS	začít
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Věku	věk	k1gInSc2	věk
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
soustředili	soustředit	k5eAaPmAgMnP	soustředit
především	především	k9	především
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
přístavů	přístav	k1gInPc2	přístav
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
stanic	stanice	k1gFnPc2	stanice
v	v	k7c6	v
přímořských	přímořský	k2eAgFnPc6d1	přímořská
částech	část	k1gFnPc6	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Objevování	objevování	k1gNnSc1	objevování
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ponecháno	ponechán	k2eAgNnSc1d1	ponecháno
arabským	arabský	k2eAgMnPc3d1	arabský
obchodníkům	obchodník	k1gMnPc3	obchodník
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
arabský	arabský	k2eAgInSc1d1	arabský
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
znamenal	znamenat	k5eAaImAgMnS	znamenat
odvlečení	odvlečení	k1gNnSc4	odvlečení
až	až	k6eAd1	až
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
Afričanů	Afričan	k1gMnPc2	Afričan
z	z	k7c2	z
území	území	k1gNnSc2	území
navazujících	navazující	k2eAgInPc2d1	navazující
na	na	k7c4	na
trans-saharskou	transaharský	k2eAgFnSc4d1	trans-saharský
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
pronikli	proniknout	k5eAaPmAgMnP	proniknout
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
hovoří	hovořit	k5eAaImIp3nP	hovořit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Evropané	Evropan	k1gMnPc1	Evropan
zajaly	zajmout	k5eAaPmAgFnP	zajmout
a	a	k8xC	a
přes	přes	k7c4	přes
Atlantik	Atlantik	k1gInSc4	Atlantik
odvezli	odvézt	k5eAaPmAgMnP	odvézt
mezi	mezi	k7c7	mezi
7	[number]	k4	7
a	a	k8xC	a
12	[number]	k4	12
miliony	milion	k4xCgInPc1	milion
Afričanů	Afričan	k1gMnPc2	Afričan
<g/>
.	.	kIx.	.
</s>
<s>
Cílovou	cílový	k2eAgFnSc7d1	cílová
destinací	destinace	k1gFnSc7	destinace
byly	být	k5eAaImAgFnP	být
hlavně	hlavně	k6eAd1	hlavně
Karibik	Karibik	k1gInSc4	Karibik
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
nové	nový	k2eAgMnPc4d1	nový
otroky	otrok	k1gMnPc4	otrok
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
již	již	k6eAd1	již
upadal	upadat	k5eAaImAgInS	upadat
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
přestával	přestávat	k5eAaImAgInS	přestávat
být	být	k5eAaImF	být
výnosný	výnosný	k2eAgInSc1d1	výnosný
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgInSc3	ten
dopomohly	dopomoct	k5eAaPmAgFnP	dopomoct
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
zákony	zákon	k1gInPc4	zákon
zakazující	zakazující	k2eAgNnSc4d1	zakazující
otroctví	otroctví	k1gNnSc4	otroctví
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
r.	r.	kA	r.
1863	[number]	k4	1863
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Evropany	Evropan	k1gMnPc4	Evropan
iniciováno	iniciován	k2eAgNnSc1d1	iniciováno
a	a	k8xC	a
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
padesát	padesát	k4xCc1	padesát
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
lokálními	lokální	k2eAgMnPc7d1	lokální
africkými	africký	k2eAgMnPc7d1	africký
vládci	vládce	k1gMnPc7	vládce
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
stále	stále	k6eAd1	stále
až	až	k9	až
800	[number]	k4	800
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
otroctví	otroctví	k1gNnSc6	otroctví
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1881	[number]	k4	1881
a	a	k8xC	a
1914	[number]	k4	1914
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
tzv.	tzv.	kA	tzv.
Závod	závod	k1gInSc1	závod
o	o	k7c4	o
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
evropské	evropský	k2eAgFnSc2d1	Evropská
dobové	dobový	k2eAgFnSc2d1	dobová
mocnosti	mocnost	k1gFnSc2	mocnost
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
90	[number]	k4	90
%	%	kIx~	%
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
přitom	přitom	k6eAd1	přitom
zabraly	zabrat	k5eAaPmAgInP	zabrat
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
byly	být	k5eAaImAgInP	být
jedinými	jediný	k2eAgNnPc7d1	jediné
dvěma	dva	k4xCgInPc7	dva
nezávislými	závislý	k2eNgInPc7d1	nezávislý
státy	stát	k1gInPc7	stát
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Afriky	Afrika	k1gFnSc2	Afrika
byla	být	k5eAaImAgFnS	být
dohodnuta	dohodnout	k5eAaPmNgFnS	dohodnout
na	na	k7c6	na
Berlínské	berlínský	k2eAgFnSc6d1	Berlínská
konferenci	konference	k1gFnSc6	konference
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
byly	být	k5eAaImAgInP	být
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Britské	britský	k2eAgNnSc1d1	Britské
impérium	impérium	k1gNnSc1	impérium
a	a	k8xC	a
Portugalské	portugalský	k2eAgNnSc1d1	portugalské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
však	však	k9	však
získaly	získat	k5eAaPmAgFnP	získat
i	i	k9	i
Belgické	belgický	k2eAgNnSc4d1	Belgické
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
Italské	italský	k2eAgNnSc1d1	italské
království	království	k1gNnSc1	království
a	a	k8xC	a
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgInPc1d1	africký
státy	stát	k1gInPc1	stát
tím	ten	k3xDgNnSc7	ten
přešly	přejít	k5eAaPmAgInP	přejít
do	do	k7c2	do
plné	plný	k2eAgFnSc2d1	plná
správy	správa	k1gFnSc2	správa
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
území	území	k1gNnSc2	území
nasadily	nasadit	k5eAaPmAgInP	nasadit
vlastní	vlastní	k2eAgMnPc4d1	vlastní
správce	správce	k1gMnPc4	správce
<g/>
,	,	kIx,	,
úředníky	úředník	k1gMnPc4	úředník
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
zaváděly	zavádět	k5eAaImAgFnP	zavádět
své	svůj	k3xOyFgInPc4	svůj
úřední	úřední	k2eAgInSc4d1	úřední
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
měny	měna	k1gFnPc4	měna
<g/>
,	,	kIx,	,
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
práce	práce	k1gFnSc2	práce
mohli	moct	k5eAaImAgMnP	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
jen	jen	k9	jen
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
stali	stát	k5eAaPmAgMnP	stát
vlastníky	vlastník	k1gMnPc7	vlastník
většiny	většina	k1gFnSc2	většina
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomiky	ekonomika	k1gFnPc1	ekonomika
kolonií	kolonie	k1gFnPc2	kolonie
navázaly	navázat	k5eAaPmAgFnP	navázat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dříve	dříve	k6eAd2	dříve
pestře	pestro	k6eAd1	pestro
rozdělených	rozdělený	k2eAgInPc6d1	rozdělený
kmenových	kmenový	k2eAgInPc6d1	kmenový
a	a	k8xC	a
etnických	etnický	k2eAgInPc6d1	etnický
celcích	celek	k1gInPc6	celek
vznikaly	vznikat	k5eAaImAgInP	vznikat
ohromné	ohromný	k2eAgInPc1d1	ohromný
koloniální	koloniální	k2eAgInPc1d1	koloniální
správní	správní	k2eAgInPc1d1	správní
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
belgické	belgický	k2eAgFnSc2d1	belgická
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnSc2d1	britská
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc2d1	francouzská
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
italské	italský	k2eAgFnSc2d1	italská
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgFnSc2d1	španělská
nebo	nebo	k8xC	nebo
portugalské	portugalský	k2eAgFnSc2d1	portugalská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhal	probíhat	k5eAaImAgInS	probíhat
proces	proces	k1gInSc1	proces
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
získala	získat	k5eAaPmAgFnS	získat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
koloniální	koloniální	k2eAgInSc4d1	koloniální
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
jistou	jistý	k2eAgFnSc4d1	jistá
podobu	podoba	k1gFnSc4	podoba
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
však	však	k9	však
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
britským	britský	k2eAgNnSc7d1	Britské
dominiem	dominion	k1gNnSc7	dominion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
JAR	jaro	k1gNnPc2	jaro
<g/>
)	)	kIx)	)
a	a	k8xC	a
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
plná	plný	k2eAgFnSc1d1	plná
nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
v	v	k7c4	v
JAR	jar	k1gFnSc4	jar
platil	platit	k5eAaImAgInS	platit
rasově-segregační	rasověegregační	k2eAgInSc1d1	rasově-segregační
režim	režim	k1gInSc1	režim
apartheidu	apartheid	k1gInSc2	apartheid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
původní	původní	k2eAgInSc1d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
uzavíral	uzavírat	k5eAaImAgInS	uzavírat
ve	v	k7c6	v
vyhrazeném	vyhrazený	k2eAgNnSc6d1	vyhrazené
území	území	k1gNnSc6	území
tzv.	tzv.	kA	tzv.
bantustanů	bantustan	k1gInPc2	bantustan
a	a	k8xC	a
nepřiznával	přiznávat	k5eNaImAgMnS	přiznávat
jim	on	k3xPp3gMnPc3	on
stejná	stejný	k2eAgNnPc4d1	stejné
politická	politický	k2eAgNnPc4d1	politické
práva	právo	k1gNnPc4	právo
jako	jako	k8xC	jako
evropskému	evropský	k2eAgNnSc3d1	Evropské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
padl	padnout	k5eAaImAgInS	padnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
získávání	získávání	k1gNnSc6	získávání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
následovaly	následovat	k5eAaImAgInP	následovat
<g/>
:	:	kIx,	:
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
koloniemi	kolonie	k1gFnPc7	kolonie
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
kolonií	kolonie	k1gFnSc7	kolonie
začalo	začít	k5eAaPmAgNnS	začít
zříkat	zříkat	k5eAaImF	zříkat
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
získaly	získat	k5eAaPmAgInP	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Libyjské	libyjský	k2eAgNnSc1d1	Libyjské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
Guinea	Guinea	k1gFnSc1	Guinea
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
států	stát	k1gInPc2	stát
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
-	-	kIx~	-
Rok	rok	k1gInSc1	rok
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
dalších	další	k2eAgInPc2d1	další
14	[number]	k4	14
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
svých	svůj	k3xOyFgInPc2	svůj
nároků	nárok	k1gInPc2	nárok
musely	muset	k5eAaImAgFnP	muset
vzdát	vzdát	k5eAaPmF	vzdát
i	i	k9	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
koloniální	koloniální	k2eAgFnPc1d1	koloniální
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
kolonie	kolonie	k1gFnSc2	kolonie
drželo	držet	k5eAaImAgNnS	držet
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gFnPc4	on
neochotně	ochotně	k6eNd1	ochotně
pustilo	pustit	k5eAaPmAgNnS	pustit
až	až	k9	až
po	po	k7c6	po
krvavých	krvavý	k2eAgFnPc6d1	krvavá
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
nespokojené	spokojený	k2eNgFnPc1d1	nespokojená
nálady	nálada	k1gFnPc1	nálada
v	v	k7c6	v
podmaněných	podmaněný	k2eAgInPc6d1	podmaněný
státech	stát	k1gInPc6	stát
a	a	k8xC	a
oslabení	oslabení	k1gNnSc1	oslabení
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
obrátily	obrátit	k5eAaPmAgFnP	obrátit
v	v	k7c4	v
řadu	řada	k1gFnSc4	řada
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
do	do	k7c2	do
regulérních	regulérní	k2eAgFnPc2d1	regulérní
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
povstání	povstání	k1gNnSc1	povstání
Mau	Mau	k1gFnSc2	Mau
Mau	Mau	k1gFnSc2	Mau
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
portugalská	portugalský	k2eAgFnSc1d1	portugalská
koloniální	koloniální	k2eAgFnSc1d1	koloniální
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
angolská	angolský	k2eAgFnSc1d1	angolská
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Namibie	Namibie	k1gFnSc2	Namibie
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
obvykle	obvykle	k6eAd1	obvykle
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
k	k	k7c3	k
nedemokratickým	demokratický	k2eNgInPc3d1	nedemokratický
režimům	režim	k1gInPc3	režim
afrického	africký	k2eAgInSc2d1	africký
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
logice	logika	k1gFnSc6	logika
bipolárního	bipolární	k2eAgInSc2d1	bipolární
světa	svět	k1gInSc2	svět
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
režimy	režim	k1gInPc1	režim
musely	muset	k5eAaImAgInP	muset
přimknout	přimknout	k5eAaPmF	přimknout
k	k	k7c3	k
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
velmocí	velmoc	k1gFnPc2	velmoc
USA	USA	kA	USA
x	x	k?	x
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nenávisti	nenávist	k1gFnSc3	nenávist
k	k	k7c3	k
bývalým	bývalý	k2eAgMnPc3d1	bývalý
kolonizátorům	kolonizátor	k1gMnPc3	kolonizátor
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
nové	nový	k2eAgInPc1d1	nový
režimy	režim	k1gInPc1	režim
raději	rád	k6eAd2	rád
volily	volit	k5eAaImAgInP	volit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
podmíněnou	podmíněný	k2eAgFnSc4d1	podmíněná
přechodem	přechod	k1gInSc7	přechod
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tytéž	týž	k3xTgMnPc4	týž
vládce	vládce	k1gMnPc4	vládce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dříve	dříve	k6eAd2	dříve
hlásali	hlásat	k5eAaImAgMnP	hlásat
socialismus	socialismus	k1gInSc4	socialismus
<g/>
,	,	kIx,	,
k	k	k7c3	k
příklonu	příklon	k1gInSc3	příklon
k	k	k7c3	k
západním	západní	k2eAgInPc3d1	západní
státům	stát	k1gInPc3	stát
a	a	k8xC	a
navázání	navázání	k1gNnSc3	navázání
obchodních	obchodní	k2eAgInPc2d1	obchodní
styků	styk	k1gInPc2	styk
se	s	k7c7	s
západními	západní	k2eAgMnPc7d1	západní
investory	investor	k1gMnPc7	investor
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozvráceným	rozvrácený	k2eAgFnPc3d1	rozvrácená
ekonomikám	ekonomika	k1gFnPc3	ekonomika
odstřiženým	odstřižený	k2eAgFnPc3d1	odstřižená
od	od	k7c2	od
evropských	evropský	k2eAgInPc2d1	evropský
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
odlivu	odliv	k1gInSc3	odliv
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
svárům	svár	k1gInPc3	svár
mezi	mezi	k7c7	mezi
různorodými	různorodý	k2eAgNnPc7d1	různorodé
etniky	etnikum	k1gNnPc7	etnikum
uzavřenými	uzavřený	k2eAgFnPc7d1	uzavřená
v	v	k7c6	v
koloniálních	koloniální	k2eAgFnPc6d1	koloniální
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
problémům	problém	k1gInPc3	problém
se	se	k3xPyFc4	se
dekolonizované	dekolonizovaný	k2eAgFnPc1d1	dekolonizovaný
země	zem	k1gFnPc1	zem
často	často	k6eAd1	často
propadaly	propadat	k5eAaPmAgFnP	propadat
do	do	k7c2	do
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
následující	následující	k2eAgInPc4d1	následující
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nezřídka	nezřídka	k6eAd1	nezřídka
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
ihned	ihned	k6eAd1	ihned
<g />
.	.	kIx.	.
</s>
<s>
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
válek	válka	k1gFnPc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
:	:	kIx,	:
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Rhodesii	Rhodesie	k1gFnSc6	Rhodesie
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Čadu	Čad	k1gInSc6	Čad
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Mosambiku	Mosambik	k1gInSc6	Mosambik
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Rwandě	Rwanda	k1gFnSc6	Rwanda
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
genocidu	genocida	k1gFnSc4	genocida
<g />
.	.	kIx.	.
</s>
<s>
etnika	etnikum	k1gNnPc1	etnikum
Tutsiů	Tutsi	k1gInPc2	Tutsi
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
súdánská	súdánský	k2eAgFnSc1d1	súdánská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Pobřeží	pobřeží	k1gNnSc6	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
Dárfúru	Dárfúro	k1gNnSc6	Dárfúro
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Nigeru	Niger	k1gInSc2	Niger
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Nigérie	Nigérie	k1gFnSc1	Nigérie
a	a	k8xC	a
okolní	okolní	k2eAgInPc1d1	okolní
státy	stát	k1gInPc1	stát
potýkají	potýkat	k5eAaImIp3nP	potýkat
s	s	k7c7	s
vlnou	vlna	k1gFnSc7	vlna
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
islamistické	islamistický	k2eAgFnSc2d1	islamistická
skupiny	skupina	k1gFnSc2	skupina
Boko	boka	k1gFnSc5	boka
Haram	Haram	k1gInSc4	Haram
vyznávající	vyznávající	k2eAgInSc1d1	vyznávající
wahhábismus	wahhábismus	k1gInSc1	wahhábismus
a	a	k8xC	a
salafíju	salafíju	k5eAaPmIp1nS	salafíju
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
otřásá	otřásat	k5eAaImIp3nS	otřásat
arabskou	arabský	k2eAgFnSc7d1	arabská
severní	severní	k2eAgFnSc7d1	severní
Afrikou	Afrika	k1gFnSc7	Afrika
vlna	vlna	k1gFnSc1	vlna
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
revolucí	revoluce	k1gFnSc7	revoluce
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Arabské	arabský	k2eAgNnSc4d1	arabské
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgInPc1d1	politický
otřesy	otřes	k1gInPc1	otřes
v	v	k7c6	v
regionu	region	k1gInSc6	region
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
či	či	k8xC	či
svržení	svržení	k1gNnSc3	svržení
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Libyi	Libye	k1gFnSc6	Libye
a	a	k8xC	a
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
vzápětí	vzápětí	k6eAd1	vzápětí
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
režimu	režim	k1gInSc2	režim
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
další	další	k2eAgFnSc1d1	další
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
čtyřmi	čtyři	k4xCgFnPc7	čtyři
stranami	strana	k1gFnPc7	strana
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Africká	africký	k2eAgFnSc1d1	africká
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
z	z	k7c2	z
229	[number]	k4	229
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
na	na	k7c4	na
630	[number]	k4	630
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,1	[number]	k4	1,1
miliardy	miliarda	k4xCgFnPc1	miliarda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
odhady	odhad	k1gInPc1	odhad
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
nárůstu	nárůst	k1gInSc6	nárůst
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2050	[number]	k4	2050
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
2,4	[number]	k4	2,4
miliardy	miliarda	k4xCgFnPc1	miliarda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2100	[number]	k4	2100
až	až	k9	až
4,18	[number]	k4	4,18
miliardy	miliarda	k4xCgFnSc2	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
probíhat	probíhat	k5eAaImF	probíhat
především	především	k9	především
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
současném	současný	k2eAgInSc6d1	současný
trendu	trend	k1gInSc6	trend
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
měla	mít	k5eAaImAgFnS	mít
ze	z	k7c2	z
současných	současný	k2eAgNnPc2d1	současné
59	[number]	k4	59
let	léto	k1gNnPc2	léto
posunout	posunout	k5eAaPmF	posunout
na	na	k7c4	na
70	[number]	k4	70
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2050	[number]	k4	2050
<g/>
.	.	kIx.	.
</s>
<s>
Afriká	Afrikat	k5eAaPmIp3nS	Afrikat
tak	tak	k8xS	tak
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
mladou	mladý	k2eAgFnSc4d1	mladá
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
například	například	k6eAd1	například
každoročně	každoročně	k6eAd1	každoročně
rodí	rodit	k5eAaImIp3nP	rodit
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
mladistvých	mladistvý	k2eAgFnPc2d1	mladistvá
matek	matka	k1gFnPc2	matka
(	(	kIx(	(
<g/>
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
až	až	k9	až
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
obdobnému	obdobný	k2eAgInSc3d1	obdobný
počtu	počet	k1gInSc3	počet
dívek	dívka	k1gFnPc2	dívka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
odepřeno	odepřen	k2eAgNnSc4d1	odepřeno
školní	školní	k2eAgNnSc4d1	školní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
aktuální	aktuální	k2eAgInSc4d1	aktuální
průměr	průměr	k1gInSc4	průměr
počtu	počet	k1gInSc2	počet
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
5,2	[number]	k4	5,2
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
matku	matka	k1gFnSc4	matka
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
1,6	[number]	k4	1,6
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
56	[number]	k4	56
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Afriky	Afrika	k1gFnSc2	Afrika
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
černoši	černoch	k1gMnPc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
a	a	k8xC	a
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
malé	malý	k2eAgInPc4d1	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
významné	významný	k2eAgFnPc1d1	významná
menšiny	menšina	k1gFnPc1	menšina
bělochů	běloch	k1gMnPc2	běloch
a	a	k8xC	a
Asiatů	Asiat	k1gMnPc2	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
mají	mít	k5eAaImIp3nP	mít
arabské	arabský	k2eAgFnSc2d1	arabská
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
celé	celý	k2eAgFnSc2d1	celá
škály	škála	k1gFnSc2	škála
různých	různý	k2eAgFnPc2d1	různá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
islám	islám	k1gInSc1	islám
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
regionálně	regionálně	k6eAd1	regionálně
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
kmenové	kmenový	k2eAgInPc1d1	kmenový
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
obyčeje	obyčej	k1gInPc1	obyčej
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
bantuských	bantuský	k2eAgInPc2d1	bantuský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
Bantuové	Bantu	k1gMnPc1	Bantu
<g/>
)	)	kIx)	)
dominují	dominovat	k5eAaImIp3nP	dominovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc6d1	centrální
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
migrovali	migrovat	k5eAaImAgMnP	migrovat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
domoviny	domovina	k1gFnSc2	domovina
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
rozšítřili	rozšítřit	k5eAaPmAgMnP	rozšítřit
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
části	část	k1gFnSc6	část
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
žijí	žít	k5eAaImIp3nP	žít
mluvčí	mluvčí	k1gMnPc1	mluvčí
nilských	nilský	k2eAgInPc2d1	nilský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Svahilského	svahilský	k2eAgNnSc2d1	svahilské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
mluvčí	mluvčí	k1gMnPc1	mluvčí
svahilštiny	svahilština	k1gFnSc2	svahilština
a	a	k8xC	a
roztroušeně	roztroušeně	k6eAd1	roztroušeně
také	také	k9	také
zbylí	zbylý	k2eAgMnPc1d1	zbylý
domorodí	domorodý	k2eAgMnPc1d1	domorodý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Křováci	Křovák	k1gMnPc1	Křovák
(	(	kIx(	(
<g/>
Khoisan	Khoisan	k1gMnSc1	Khoisan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pygmejové	Pygmej	k1gMnPc1	Pygmej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Africe	Afrika	k1gFnSc6	Afrika
žijí	žít	k5eAaImIp3nP	žít
Khoikhoiové	Khoikhoiový	k2eAgFnPc1d1	Khoikhoiový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
mluvčí	mluvčí	k1gMnPc1	mluvčí
nigerokonžských	nigerokonžský	k2eAgInPc2d1	nigerokonžský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
Jorubové	Jorubový	k2eAgNnSc4d1	Jorubový
<g/>
,	,	kIx,	,
Igbo	Igbo	k1gMnSc1	Igbo
<g/>
,	,	kIx,	,
Fulbové	Fulb	k1gMnPc1	Fulb
<g/>
,	,	kIx,	,
Akani	Akan	k1gMnPc1	Akan
<g/>
,	,	kIx,	,
Wolofové	Wolof	k1gMnPc1	Wolof
a	a	k8xC	a
Mandinkové	Mandinkový	k2eAgFnPc1d1	Mandinkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
i	i	k9	i
mluvčí	mluvčí	k1gFnSc1	mluvčí
nilosaharských	nilosaharský	k2eAgInPc2d1	nilosaharský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
Songhajci	Songhajce	k1gMnPc1	Songhajce
<g/>
,	,	kIx,	,
Kanurijci	Kanurijce	k1gMnPc1	Kanurijce
a	a	k8xC	a
Zarma	Zarma	k1gNnSc1	Zarma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
i	i	k9	i
mluvčí	mluvčí	k1gMnSc1	mluvčí
hauštiny	hauština	k1gFnSc2	hauština
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
domorodých	domorodý	k2eAgFnPc2d1	domorodá
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
Berbeři	Berber	k1gMnPc1	Berber
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
Egypťané	Egypťan	k1gMnPc1	Egypťan
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
3	[number]	k4	3
<g/>
)	)	kIx)	)
mluvčí	mluvčí	k1gFnSc1	mluvčí
nilosaharských	nilosaharský	k2eAgInPc2d1	nilosaharský
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
století	století	k1gNnSc6	století
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
přišli	přijít	k5eAaPmAgMnP	přijít
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
zde	zde	k6eAd1	zde
arabštinu	arabština	k1gFnSc4	arabština
a	a	k8xC	a
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zde	zde	k6eAd1	zde
pobývali	pobývat	k5eAaImAgMnP	pobývat
i	i	k9	i
Féničané	Féničan	k1gMnPc1	Féničan
<g/>
,	,	kIx,	,
Hyksósové	Hyksós	k1gMnPc1	Hyksós
<g/>
,	,	kIx,	,
Alani	Alan	k1gMnPc1	Alan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
.	.	kIx.	.
</s>
<s>
Berbeři	Berber	k1gMnPc1	Berber
dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Kočovný	kočovný	k2eAgInSc1d1	kočovný
kmen	kmen	k1gInSc1	kmen
berberů	berber	k1gMnPc2	berber
Tuaregové	Tuareg	k1gMnPc1	Tuareg
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africkém	africký	k2eAgInSc6d1	africký
rohu	roh	k1gInSc6	roh
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
Amharové	Amhar	k1gMnPc1	Amhar
a	a	k8xC	a
Tigrajové	Tigraj	k1gMnPc1	Tigraj
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mluví	mluvit	k5eAaImIp3nS	mluvit
semitskou	semitský	k2eAgFnSc7d1	semitská
větví	větev	k1gFnSc7	větev
afroasijských	afroasijský	k2eAgInPc2d1	afroasijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Oromové	Orom	k1gMnPc1	Orom
a	a	k8xC	a
Somálci	Somálec	k1gMnPc1	Somálec
hovoří	hovořit	k5eAaImIp3nP	hovořit
kušitskou	kušitský	k2eAgFnSc7d1	kušitský
větví	větev	k1gFnSc7	větev
afroasijských	afroasijský	k2eAgInPc2d1	afroasijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dekolonizací	dekolonizace	k1gFnSc7	dekolonizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
mnoho	mnoho	k6eAd1	mnoho
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
co	co	k9	co
africké	africký	k2eAgFnPc1d1	africká
země	zem	k1gFnPc1	zem
získaly	získat	k5eAaPmAgFnP	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
jich	on	k3xPp3gMnPc2	on
řada	řada	k1gFnSc1	řada
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
100	[number]	k4	100
000	[number]	k4	000
Evropanů	Evropan	k1gMnPc2	Evropan
z	z	k7c2	z
Belgického	belgický	k2eAgNnSc2d1	Belgické
Konga	Kongo	k1gNnSc2	Kongo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
milion	milion	k4xCgInSc1	milion
tzv.	tzv.	kA	tzv.
Černých	Černých	k2eAgFnPc2d1	Černých
noh	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
evropských	evropský	k2eAgMnPc2d1	evropský
obyvatel	obyvatel	k1gMnPc2	obyvatel
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
000	[number]	k4	000
Portugalců	Portugalec	k1gMnPc2	Portugalec
z	z	k7c2	z
Angoly	Angola	k1gFnSc2	Angola
a	a	k8xC	a
Mozambiku	Mozambik	k1gInSc2	Mozambik
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
pět	pět	k4xCc1	pět
milionů	milion	k4xCgInPc2	milion
bílých	bílý	k2eAgMnPc2d1	bílý
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
kořeny	kořen	k1gInPc7	kořen
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4,6	[number]	k4	4,6
milionů	milion	k4xCgInPc2	milion
v	v	k7c4	v
JAR	jar	k1gInSc4	jar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc3d1	bývalá
německé	německý	k2eAgFnSc3d1	německá
kolonii	kolonie	k1gFnSc3	kolonie
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
stále	stále	k6eAd1	stále
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
evropské	evropský	k2eAgFnSc2d1	Evropská
kolonizace	kolonizace	k1gFnSc2	kolonizace
Afriky	Afrika	k1gFnSc2	Afrika
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
především	především	k9	především
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
dovezeni	dovézt	k5eAaPmNgMnP	dovézt
do	do	k7c2	do
britských	britský	k2eAgFnPc2d1	britská
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřesnější	přesný	k2eAgInPc1d3	nejpřesnější
odhady	odhad	k1gInPc1	odhad
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
tisíci	tisíc	k4xCgInPc7	tisíc
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
rodiny	rodina	k1gFnPc1	rodina
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc1	čtyři
<g/>
:	:	kIx,	:
Afroasijské	Afroasijský	k2eAgInPc1d1	Afroasijský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
rodinou	rodina	k1gFnSc7	rodina
asi	asi	k9	asi
240	[number]	k4	240
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
285	[number]	k4	285
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Sahelu	Sahel	k1gInSc2	Sahel
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Nilosaharské	Nilosaharský	k2eAgInPc1d1	Nilosaharský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
rodinou	rodina	k1gFnSc7	rodina
obsahující	obsahující	k2eAgInSc1d1	obsahující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
mluví	mluvit	k5eAaImIp3nP	mluvit
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
oblasti	oblast	k1gFnPc1	oblast
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Nigerokonžské	Nigerokonžský	k2eAgInPc1d1	Nigerokonžský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
rodinou	rodina	k1gFnSc7	rodina
pokrývající	pokrývající	k2eAgFnSc4d1	pokrývající
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
350	[number]	k4	350
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kojsanské	Kojsanský	k2eAgInPc1d1	Kojsanský
jazyky	jazyk	k1gInPc1	jazyk
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
asi	asi	k9	asi
15	[number]	k4	15
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
přibližně	přibližně	k6eAd1	přibližně
360	[number]	k4	360
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
těchto	tento	k3xDgMnPc2	tento
jazyků	jazyk	k1gMnPc2	jazyk
je	být	k5eAaImIp3nS	být
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
Kojkojnů	Kojkojn	k1gMnPc2	Kojkojn
a	a	k8xC	a
Sanů	San	k1gMnPc2	San
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
africké	africký	k2eAgInPc1d1	africký
státy	stát	k1gInPc1	stát
přijaly	přijmout	k5eAaPmAgInP	přijmout
evropské	evropský	k2eAgInPc1d1	evropský
úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
afrikánština	afrikánština	k1gFnSc1	afrikánština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
i	i	k9	i
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Domorodými	domorodý	k2eAgInPc7d1	domorodý
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mluví	mluvit	k5eAaImIp3nS	mluvit
především	především	k9	především
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
stycích	styk	k1gInPc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
některé	některý	k3yIgInPc1	některý
domorodé	domorodý	k2eAgInPc1d1	domorodý
jazyky	jazyk	k1gInPc1	jazyk
si	se	k3xPyFc3	se
udržely	udržet	k5eAaPmAgInP	udržet
status	status	k1gInSc4	status
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
svahilština	svahilština	k1gFnSc1	svahilština
<g/>
,	,	kIx,	,
jorubština	jorubština	k1gFnSc1	jorubština
<g/>
,	,	kIx,	,
igboština	igboština	k1gFnSc1	igboština
nebo	nebo	k8xC	nebo
hauština	hauština	k1gFnSc1	hauština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Křesťanství	křesťanství	k1gNnSc1	křesťanství
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
africká	africký	k2eAgNnPc1d1	africké
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
náboženství	náboženství	k1gNnSc4	náboženství
islám	islám	k1gInSc1	islám
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
nejvíce	hodně	k6eAd3	hodně
Afričanů	Afričan	k1gMnPc2	Afričan
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
15	[number]	k4	15
%	%	kIx~	%
Afričanů	Afričan	k1gMnPc2	Afričan
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
tradiční	tradiční	k2eAgNnPc4d1	tradiční
domorodá	domorodý	k2eAgNnPc4d1	domorodé
náboženství	náboženství	k1gNnPc4	náboženství
a	a	k8xC	a
ostatní	ostatní	k2eAgNnSc4d1	ostatní
náboženství	náboženství	k1gNnSc4	náboženství
(	(	kIx(	(
<g/>
bahá	bahat	k5eAaBmIp3nS	bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
<g/>
,	,	kIx,	,
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
importován	importovat	k5eAaBmNgInS	importovat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dominuje	dominovat	k5eAaImIp3nS	dominovat
především	především	k9	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
proniklo	proniknout	k5eAaPmAgNnS	proniknout
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozmach	rozmach	k1gInSc1	rozmach
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
až	až	k9	až
do	do	k7c2	do
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc3d1	centrální
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
neklesá	klesat	k5eNaImIp3nS	klesat
podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznávajících	vyznávající	k2eAgMnPc2d1	vyznávající
islám	islám	k1gInSc4	islám
pod	pod	k7c4	pod
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
náboženství	náboženství	k1gNnSc1	náboženství
dominuje	dominovat	k5eAaImIp3nS	dominovat
především	především	k9	především
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Mauricius	Mauricius	k1gInSc1	Mauricius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
jako	jako	k8xS	jako
Togo	Togo	k1gNnSc4	Togo
nebo	nebo	k8xC	nebo
Tanzanie	Tanzanie	k1gFnPc4	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Africká	africký	k2eAgFnSc1d1	africká
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Africká	africký	k2eAgFnSc1d1	africká
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
a	a	k8xC	a
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
různorodostí	různorodost	k1gFnSc7	různorodost
etnik	etnikum	k1gNnPc2	etnikum
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
africká	africký	k2eAgFnSc1d1	africká
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
i	i	k8xC	i
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každá	každý	k3xTgFnSc1	každý
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
i	i	k8xC	i
africká	africký	k2eAgFnSc1d1	africká
kultura	kultura	k1gFnSc1	kultura
si	se	k3xPyFc3	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
řadu	řada	k1gFnSc4	řada
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
folklór	folklór	k1gInSc1	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
nejchudší	chudý	k2eAgInSc4d3	nejchudší
obydlený	obydlený	k2eAgInSc4d1	obydlený
kontinent	kontinent	k1gInSc4	kontinent
<g/>
:	:	kIx,	:
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
OSN	OSN	kA	OSN
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
175	[number]	k4	175
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozice	pozice	k1gFnSc1	pozice
od	od	k7c2	od
151	[number]	k4	151
(	(	kIx(	(
<g/>
Gambie	Gambie	k1gFnSc2	Gambie
<g/>
)	)	kIx)	)
do	do	k7c2	do
175	[number]	k4	175
(	(	kIx(	(
<g/>
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
obsazeny	obsazen	k2eAgInPc4d1	obsazen
africkými	africký	k2eAgInPc7d1	africký
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
ubohé	ubohý	k2eAgFnSc2d1	ubohá
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
jsou	být	k5eAaImIp3nP	být
spatřovány	spatřovat	k5eAaImNgFnP	spatřovat
v	v	k7c6	v
kolonialismu	kolonialismus	k1gInSc6	kolonialismus
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
podpořeny	podpořen	k2eAgFnPc1d1	podpořena
korupcí	korupce	k1gFnSc7	korupce
a	a	k8xC	a
despotismem	despotismus	k1gInSc7	despotismus
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
země	země	k1gFnSc1	země
jako	jako	k8xS	jako
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
i	i	k9	i
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
také	také	k9	také
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pozadu	pozadu	k6eAd1	pozadu
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
investic	investice	k1gFnPc2	investice
a	a	k8xC	a
růstu	růst	k1gInSc2	růst
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chudoba	chudoba	k1gFnSc1	chudoba
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k6eAd1	mnoho
následku	následek	k1gInSc2	následek
jako	jako	k8xC	jako
nízká	nízký	k2eAgFnSc1d1	nízká
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc1	násilí
a	a	k8xC	a
nestabilita	nestabilita	k1gFnSc1	nestabilita
-	-	kIx~	-
faktory	faktor	k1gInPc1	faktor
dále	daleko	k6eAd2	daleko
prohlubující	prohlubující	k2eAgFnSc4d1	prohlubující
chudobu	chudoba	k1gFnSc4	chudoba
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
úspěch	úspěch	k1gInSc4	úspěch
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
Botswana	Botswana	k1gFnSc1	Botswana
a	a	k8xC	a
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
vlastní	vlastní	k2eAgInSc4d1	vlastní
kurz	kurz	k1gInSc4	kurz
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
jejich	jejich	k3xOp3gNnSc2	jejich
přírodního	přírodní	k2eAgNnSc2d1	přírodní
bohatství	bohatství	k1gNnSc2	bohatství
v	v	k7c6	v
surovinách	surovina	k1gFnPc6	surovina
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgMnPc1d1	hlavní
producenti	producent	k1gMnPc1	producent
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
diamantů	diamant	k1gInPc2	diamant
–	–	k?	–
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
díky	díky	k7c3	díky
dobře	dobře	k6eAd1	dobře
zavedenému	zavedený	k2eAgInSc3d1	zavedený
právnímu	právní	k2eAgInSc3d1	právní
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
kapitálu	kapitál	k1gInSc3	kapitál
<g/>
,	,	kIx,	,
trhům	trh	k1gInPc3	trh
a	a	k8xC	a
technologiím	technologie	k1gFnPc3	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Nigérie	Nigérie	k1gFnSc1	Nigérie
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
nalezišť	naleziště	k1gNnPc2	naleziště
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
populaci	populace	k1gFnSc4	populace
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
ekonomik	ekonomika	k1gFnPc2	ekonomika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
rapidní	rapidní	k2eAgInSc1d1	rapidní
vzestup	vzestup	k1gInSc1	vzestup
ekonomiky	ekonomika	k1gFnSc2	ekonomika
státu	stát	k1gInSc2	stát
a	a	k8xC	a
opatření	opatření	k1gNnSc4	opatření
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
zařadí	zařadit	k5eAaPmIp3nS	zařadit
brzy	brzy	k6eAd1	brzy
Nigérii	Nigérie	k1gFnSc4	Nigérie
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
ekonomické	ekonomický	k2eAgMnPc4d1	ekonomický
tygry	tygr	k1gMnPc4	tygr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kolonialismus	kolonialismus	k1gInSc1	kolonialismus
měl	mít	k5eAaImAgInS	mít
destabilizační	destabilizační	k2eAgInSc1d1	destabilizační
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
cítit	cítit	k5eAaImF	cítit
v	v	k7c6	v
africké	africký	k2eAgFnSc6d1	africká
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
evropskému	evropský	k2eAgInSc3d1	evropský
vlivu	vliv	k1gInSc3	vliv
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnPc1d1	národní
hranice	hranice	k1gFnPc1	hranice
států	stát	k1gInPc2	stát
nejsou	být	k5eNaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
dohod	dohoda	k1gFnPc2	dohoda
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc4	hranice
ovlivněny	ovlivněn	k2eAgFnPc4d1	ovlivněna
vojenským	vojenský	k2eAgInSc7d1	vojenský
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgInPc1d1	evropský
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
stanovení	stanovení	k1gNnSc4	stanovení
hranic	hranice	k1gFnPc2	hranice
okolo	okolo	k7c2	okolo
jejích	její	k3xOp3gNnPc2	její
teritorií	teritorium	k1gNnPc2	teritorium
znamenaly	znamenat	k5eAaImAgFnP	znamenat
rozdělení	rozdělení	k1gNnSc4	rozdělení
sousedících	sousedící	k2eAgFnPc2d1	sousedící
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
nebo	nebo	k8xC	nebo
násilné	násilný	k2eAgNnSc4d1	násilné
spojení	spojení	k1gNnSc4	spojení
tradičních	tradiční	k2eAgMnPc2d1	tradiční
nepřátel	nepřítel	k1gMnPc2	nepřítel
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
regulace	regulace	k1gFnSc2	regulace
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
řeka	řeka	k1gFnSc1	řeka
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
přirozená	přirozený	k2eAgFnSc1d1	přirozená
hranice	hranice	k1gFnSc1	hranice
má	mít	k5eAaImIp3nS	mít
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sdílí	sdílet	k5eAaImIp3nP	sdílet
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
země	zem	k1gFnSc2	zem
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
izolovalo	izolovat	k5eAaBmAgNnS	izolovat
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
se	s	k7c7	s
vzdálenými	vzdálený	k2eAgInPc7d1	vzdálený
regiony	region	k1gInPc7	region
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
najednou	najednou	k6eAd1	najednou
překračovali	překračovat	k5eAaImAgMnP	překračovat
"	"	kIx"	"
<g/>
hranice	hranice	k1gFnPc4	hranice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
existovaly	existovat	k5eAaImAgFnP	existovat
jen	jen	k9	jen
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
byly	být	k5eAaImAgInP	být
státy	stát	k1gInPc1	stát
často	často	k6eAd1	často
omezovány	omezovat	k5eAaImNgInP	omezovat
nestabilitou	nestabilita	k1gFnSc7	nestabilita
<g/>
,	,	kIx,	,
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
autoritativními	autoritativní	k2eAgInPc7d1	autoritativní
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
afrických	africký	k2eAgInPc2d1	africký
národů	národ	k1gInPc2	národ
jsou	být	k5eAaImIp3nP	být
republiky	republika	k1gFnPc1	republika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
formu	forma	k1gFnSc4	forma
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
systému	systém	k1gInSc2	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pár	pár	k4xCyI	pár
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
vytvořit	vytvořit	k5eAaPmF	vytvořit
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
vládu	vláda	k1gFnSc4	vláda
namísto	namísto	k7c2	namísto
série	série	k1gFnSc2	série
pučů	puč	k1gInPc2	puč
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
diktatur	diktatura	k1gFnPc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
užívalo	užívat	k5eAaImAgNnS	užívat
svoje	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
také	také	k9	také
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rozdmýchali	rozdmýchat	k5eAaPmAgMnP	rozdmýchat
etnické	etnický	k2eAgInPc4d1	etnický
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
během	během	k7c2	během
dob	doba	k1gFnPc2	doba
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
považována	považován	k2eAgFnSc1d1	považována
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
efektivně	efektivně	k6eAd1	efektivně
udržovat	udržovat	k5eAaImF	udržovat
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
70	[number]	k4	70
pučů	puč	k1gInPc2	puč
a	a	k8xC	a
13	[number]	k4	13
vražd	vražda	k1gFnPc2	vražda
představitelů	představitel	k1gMnPc2	představitel
nějakého	nějaký	k3yIgInSc2	nějaký
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
získání	získání	k1gNnSc2	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
trvají	trvat	k5eAaImIp3nP	trvat
různě	různě	k6eAd1	různě
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
regionální	regionální	k2eAgFnSc6d1	regionální
integraci	integrace	k1gFnSc6	integrace
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
organizací	organizace	k1gFnSc7	organizace
je	být	k5eAaImIp3nS	být
Africká	africký	k2eAgFnSc1d1	africká
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
Organizace	organizace	k1gFnSc2	organizace
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
OAU	OAU	kA	OAU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
AU	au	k0	au
aktuálně	aktuálně	k6eAd1	aktuálně
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
54	[number]	k4	54
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Maroka	Maroko	k1gNnPc4	Maroko
a	a	k8xC	a
Západní	západní	k2eAgFnPc4d1	západní
Sahary	Sahara	k1gFnPc4	Sahara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
organizací	organizace	k1gFnSc7	organizace
spadající	spadající	k2eAgFnSc7d1	spadající
pod	pod	k7c7	pod
AU	au	k0	au
je	být	k5eAaImIp3nS	být
i	i	k9	i
Africké	africký	k2eAgNnSc1d1	africké
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
AEC	AEC	kA	AEC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
africké	africký	k2eAgFnPc4d1	africká
regionální	regionální	k2eAgFnPc4d1	regionální
organizace	organizace	k1gFnPc4	organizace
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
ECOWAS	ECOWAS	kA	ECOWAS
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
celní	celní	k2eAgFnSc1d1	celní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
JACU	JACU	kA	JACU
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihoafrické	jihoafrický	k2eAgNnSc4d1	jihoafrické
rozvojové	rozvojový	k2eAgNnSc4d1	rozvojové
společenství	společenství	k1gNnSc4	společenství
(	(	kIx(	(
<g/>
SADC	SADC	kA	SADC
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mezivládní	mezivládní	k2eAgInSc1d1	mezivládní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
IGAD	IGAD	kA	IGAD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
arabského	arabský	k2eAgInSc2d1	arabský
Maghrebu	Maghreb	k1gInSc2	Maghreb
(	(	kIx(	(
<g/>
UMA	uma	k1gFnSc1	uma
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Společný	společný	k2eAgInSc1d1	společný
trh	trh	k1gInSc1	trh
pro	pro	k7c4	pro
Východní	východní	k2eAgFnSc4d1	východní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Afriku	Afrika	k1gFnSc4	Afrika
(	(	kIx(	(
<g/>
COMESA	COMESA	kA	COMESA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Východoafrické	východoafrický	k2eAgNnSc1d1	Východoafrické
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
EAC	EAC	kA	EAC
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
a	a	k8xC	a
území	území	k1gNnSc2	území
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Afrika	Afrika	k1gFnSc1	Afrika
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Střední	střední	k2eAgFnSc1d1	střední
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
Čad	Čad	k1gInSc1	Čad
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
Kongo	Kongo	k1gNnSc4	Kongo
Východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Afrika	Afrika	k1gFnSc1	Afrika
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Západní	západní	k2eAgFnSc1d1	západní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
cestovatelům	cestovatel	k1gMnPc3	cestovatel
Afrikou	Afrika	k1gFnSc7	Afrika
patřili	patřit	k5eAaImAgMnP	patřit
<g/>
:	:	kIx,	:
Abú	abú	k1gMnSc1	abú
Abdallah	Abdallah	k1gMnSc1	Abdallah
Ibn	Ibn	k1gMnSc1	Ibn
Battúta	Battúta	k1gMnSc1	Battúta
<g/>
:	:	kIx,	:
1325	[number]	k4	1325
<g/>
-	-	kIx~	-
<g/>
1353	[number]	k4	1353
-	-	kIx~	-
Battúta	Battúta	k1gMnSc1	Battúta
putoval	putovat	k5eAaImAgMnS	putovat
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
prakticky	prakticky	k6eAd1	prakticky
celým	celý	k2eAgInSc7d1	celý
tehdy	tehdy	k6eAd1	tehdy
známým	známý	k2eAgInSc7d1	známý
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
přes	přes	k7c4	přes
poušť	poušť	k1gFnSc4	poušť
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
Rudému	rudý	k2eAgNnSc3d1	Rudé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
chtěl	chtít	k5eAaImAgMnS	chtít
do	do	k7c2	do
Džiddy	Džidda	k1gFnSc2	Džidda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
válečné	válečný	k2eAgFnPc1d1	válečná
události	událost	k1gFnPc1	událost
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
neumožnily	umožnit	k5eNaPmAgFnP	umožnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1352	[number]	k4	1352
podnikl	podniknout	k5eAaPmAgMnS	podniknout
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
Mali	Mali	k1gNnSc2	Mali
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Sidžilmásy	Sidžilmása	k1gFnSc2	Sidžilmása
se	se	k3xPyFc4	se
s	s	k7c7	s
karavanou	karavana	k1gFnSc7	karavana
vydal	vydat	k5eAaPmAgMnS	vydat
přes	přes	k7c4	přes
Saharu	Sahara	k1gFnSc4	Sahara
do	do	k7c2	do
Timbuktu	Timbukt	k1gInSc2	Timbukt
a	a	k8xC	a
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Niger	Niger	k1gInSc4	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
pohoří	pohoří	k1gNnSc4	pohoří
Ahaggar	Ahaggara	k1gFnPc2	Ahaggara
a	a	k8xC	a
oázu	oáza	k1gFnSc4	oáza
Tuát	Tuáta	k1gFnPc2	Tuáta
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1353	[number]	k4	1353
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Livingstone	Livingston	k1gInSc5	Livingston
<g/>
:	:	kIx,	:
1849	[number]	k4	1849
-	-	kIx~	-
přešel	přejít	k5eAaPmAgInS	přejít
poušť	poušť	k1gFnSc4	poušť
Kalahari	Kalahari	k1gNnSc2	Kalahari
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
jezero	jezero	k1gNnSc4	jezero
Ngami	Nga	k1gFnPc7	Nga
1851	[number]	k4	1851
-	-	kIx~	-
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Chobe	Chob	k1gInSc5	Chob
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
toku	tok	k1gInSc6	tok
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k9	až
k	k	k7c3	k
Zambezi	Zambezi	k1gNnSc3	Zambezi
<g/>
.	.	kIx.	.
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
-	-	kIx~	-
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
na	na	k7c6	na
expedici	expedice	k1gFnSc6	expedice
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
podruhé	podruhé	k6eAd1	podruhé
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
řeky	řeka	k1gFnPc4	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
<g/>
.	.	kIx.	.
</s>
<s>
Prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
tok	tok	k1gInSc1	tok
Zambezi	Zambezi	k1gNnSc2	Zambezi
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
do	do	k7c2	do
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
1855	[number]	k4	1855
-	-	kIx~	-
objevil	objevit	k5eAaPmAgMnS	objevit
velkolepé	velkolepý	k2eAgInPc4d1	velkolepý
vodopády	vodopád	k1gInPc4	vodopád
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Viktoriinymi	Viktoriiny	k1gFnPc7	Viktoriiny
<g/>
.	.	kIx.	.
1859	[number]	k4	1859
-	-	kIx~	-
objevil	objevit	k5eAaPmAgInS	objevit
jezero	jezero	k1gNnSc4	jezero
Malawi	Malawi	k1gNnPc2	Malawi
<g/>
.	.	kIx.	.
1867	[number]	k4	1867
-	-	kIx~	-
objevil	objevit	k5eAaPmAgInS	objevit
jezero	jezero	k1gNnSc4	jezero
Tanganika	Tanganika	k1gFnSc1	Tanganika
a	a	k8xC	a
Mweru	Mwer	k1gMnSc3	Mwer
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
-	-	kIx~	-
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Bangweulu	Bangweul	k1gInSc2	Bangweul
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
vytéká	vytékat	k5eAaImIp3nS	vytékat
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
Konga	Kongo	k1gNnSc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
objasnil	objasnit	k5eAaPmAgMnS	objasnit
otázku	otázka	k1gFnSc4	otázka
pramene	pramen	k1gInSc2	pramen
tohoto	tento	k3xDgInSc2	tento
veletoku	veletok	k1gInSc2	veletok
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
-	-	kIx~	-
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
řeku	řeka	k1gFnSc4	řeka
Lualabu	Lualab	k1gInSc2	Lualab
a	a	k8xC	a
správně	správně	k6eAd1	správně
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Konga	Kongo	k1gNnSc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přešel	přejít	k5eAaPmAgInS	přejít
Afriku	Afrika	k1gFnSc4	Afrika
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
z	z	k7c2	z
Luandy	Luanda	k1gFnSc2	Luanda
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
do	do	k7c2	do
mosambického	mosambický	k2eAgInSc2d1	mosambický
Quelimane	Queliman	k1gMnSc5	Queliman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
Morton	Morton	k1gInSc4	Morton
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
:	:	kIx,	:
1869	[number]	k4	1869
-	-	kIx~	-
vyslali	vyslat	k5eAaPmAgMnP	vyslat
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
tam	tam	k6eAd1	tam
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
zmizelém	zmizelý	k2eAgMnSc6d1	zmizelý
britském	britský	k2eAgMnSc6d1	britský
badateli	badatel	k1gMnSc6	badatel
Davidu	David	k1gMnSc6	David
Livingstoneovi	Livingstoneus	k1gMnSc6	Livingstoneus
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
-	-	kIx~	-
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
ze	z	k7c2	z
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
<g/>
,	,	kIx,	,
ostrova	ostrov	k1gInSc2	ostrov
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hledal	hledat	k5eAaImAgMnS	hledat
Livingstonea	Livingstonea	k1gMnSc1	Livingstonea
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
-	-	kIx~	-
našel	najít	k5eAaPmAgMnS	najít
Livingstonea	Livingstonea	k1gMnSc1	Livingstonea
v	v	k7c6	v
Ujiji	Ujij	k1gFnSc6	Ujij
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
.	.	kIx.	.
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
-	-	kIx~	-
cestoval	cestovat	k5eAaImAgMnS	cestovat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Tanganika	Tanganika	k1gFnSc1	Tanganika
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Lualaba	Lualab	k1gMnSc2	Lualab
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Konga	Kongo	k1gNnSc2	Kongo
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
doplul	doplout	k5eAaPmAgInS	doplout
až	až	k6eAd1	až
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
-	-	kIx~	-
podnikl	podniknout	k5eAaPmAgMnS	podniknout
výpravu	výprava	k1gFnSc4	výprava
pro	pro	k7c4	pro
belgického	belgický	k2eAgMnSc4d1	belgický
krále	král	k1gMnSc4	král
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středoafrické	středoafrický	k2eAgFnSc6d1	Středoafrická
džungli	džungle	k1gFnSc6	džungle
založil	založit	k5eAaPmAgMnS	založit
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
stát	stát	k5eAaImF	stát
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
.	.	kIx.	.
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
-	-	kIx~	-
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
na	na	k7c4	na
poslední	poslední	k2eAgNnSc4d1	poslední
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
súdánského	súdánský	k2eAgMnSc2d1	súdánský
paši	paša	k1gMnSc2	paša
Emina	Emin	k2eAgMnSc2d1	Emin
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
:	:	kIx,	:
1872	[number]	k4	1872
-	-	kIx~	-
odjel	odjet	k5eAaPmAgMnS	odjet
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pracoval	pracovat	k5eAaImAgMnS	pracovat
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
diamantových	diamantový	k2eAgFnPc6d1	Diamantová
polích	pole	k1gFnPc6	pole
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
do	do	k7c2	do
neznámého	známý	k2eNgNnSc2d1	neznámé
afrického	africký	k2eAgNnSc2d1	africké
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
a	a	k8xC	a
nemoc	nemoc	k1gFnSc4	nemoc
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
zabránily	zabránit	k5eAaPmAgInP	zabránit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
cíle	cíl	k1gInPc1	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
etnografický	etnografický	k2eAgInSc4d1	etnografický
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
1883	[number]	k4	1883
-	-	kIx~	-
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Zambezi	Zambezi	k1gNnSc2	Zambezi
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gInSc3	on
však	však	k9	však
ze	z	k7c2	z
spleti	spleť	k1gFnSc2	spleť
příčin	příčina	k1gFnPc2	příčina
zmařili	zmařit	k5eAaPmAgMnP	zmařit
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
příslušníci	příslušník	k1gMnPc1	příslušník
domorodého	domorodý	k2eAgInSc2d1	domorodý
kmene	kmen	k1gInSc2	kmen
Mašukulumbů	Mašukulumb	k1gMnPc2	Mašukulumb
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sházeli	sházet	k5eAaPmAgMnP	sházet
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
vědecké	vědecký	k2eAgInPc4d1	vědecký
přístroje	přístroj	k1gInPc4	přístroj
i	i	k8xC	i
cenné	cenný	k2eAgInPc4d1	cenný
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Zikmundem	Zikmund	k1gMnSc7	Zikmund
<g/>
:	:	kIx,	:
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
-	-	kIx~	-
podnikli	podniknout	k5eAaPmAgMnP	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
bylo	být	k5eAaImAgNnS	být
rozhlasem	rozhlas	k1gInSc7	rozhlas
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
přes	přes	k7c4	přes
700	[number]	k4	700
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
udělali	udělat	k5eAaPmAgMnP	udělat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
natočili	natočit	k5eAaBmAgMnP	natočit
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
filmového	filmový	k2eAgInSc2d1	filmový
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
:	:	kIx,	:
1965	[number]	k4	1965
-	-	kIx~	-
založil	založit	k5eAaPmAgInS	založit
ZOO	zoo	k1gFnSc4	zoo
Safari	safari	k1gNnSc2	safari
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
na	na	k7c4	na
africkou	africký	k2eAgFnSc4d1	africká
zvířenu	zvířena	k1gFnSc4	zvířena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradu	zahrada	k1gFnSc4	zahrada
sám	sám	k3xTgInSc1	sám
zásoboval	zásobovat	k5eAaImAgInS	zásobovat
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
odchytával	odchytávat	k5eAaImAgMnS	odchytávat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
afrických	africký	k2eAgFnPc6d1	africká
expedicích	expedice	k1gFnPc6	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
soustřeďující	soustřeďující	k2eAgFnPc1d1	soustřeďující
se	se	k3xPyFc4	se
na	na	k7c4	na
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
sedmi	sedm	k4xCc2	sedm
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Safari	safari	k1gNnSc2	safari
pod	pod	k7c7	pod
Kilimandžárem	Kilimandžáro	k1gNnSc7	Kilimandžáro
pak	pak	k6eAd1	pak
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
ocenění	ocenění	k1gNnPc2	ocenění
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
