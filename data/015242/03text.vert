<s>
Hans	Hans	k1gMnSc1
Trippel	Trippel	k1gMnSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Trippel	Trippel	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1908	#num#	k4
<g/>
Groß-Umstadt	Groß-Umstadtum	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2001	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
92	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Erbach	Erbach	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
inženýr	inženýr	k1gMnSc1
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mercedes-Benz	Mercedes-Benz	k1gInSc1
300SL	300SL	k4
</s>
<s>
Hans	Hans	k1gMnSc1
Trippel	Trippel	k1gMnSc1
<g/>
,	,	kIx,
také	také	k9
Hanns	Hanns	k1gInSc1
Trippel	Trippela	k1gFnPc2
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1908	#num#	k4
<g/>
,	,	kIx,
Darmstadt	Darmstadt	k1gInSc1
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
Erbach	Erbach	k1gInSc1
<g/>
,	,	kIx,
Hesensko	Hesensko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
designér	designér	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
takzvané	takzvaný	k2eAgFnSc2d1
Gullwing	Gullwing	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
door	doora	k1gFnPc2
(	(	kIx(
<g/>
dveře	dveře	k1gFnPc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
křídel	křídlo	k1gNnPc2
racka	racek	k1gMnSc2
<g/>
)	)	kIx)
pro	pro	k7c4
Mercedes-Benz	Mercedes-Benz	k1gInSc4
300	#num#	k4
SL	SL	kA
<g/>
,	,	kIx,
automobily	automobil	k1gInPc7
Troll	troll	k1gMnSc1
a	a	k8xC
Amphicar	Amphicar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pohledu	pohled	k1gInSc2
automobilové	automobilový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
hlavně	hlavně	k9
jako	jako	k8xS,k8xC
tvůrce	tvůrce	k1gMnSc1
obojživelných	obojživelný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
závodního	závodní	k2eAgInSc2d1
jezdce	jezdec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
známější	známý	k2eAgFnSc1d2
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
posedlost	posedlost	k1gFnSc4
vývojem	vývoj	k1gInSc7
obojživelných	obojživelný	k2eAgNnPc2d1
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
členem	člen	k1gMnSc7
polovojenských	polovojenský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
SA	SA	kA
a	a	k8xC
Schutzstaffel	Schutzstaffel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
odměnu	odměna	k1gFnSc4
za	za	k7c4
aktivní	aktivní	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
nacistického	nacistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
převzal	převzít	k5eAaPmAgInS
Trippel	Trippel	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Německo	Německo	k1gNnSc1
obsadilo	obsadit	k5eAaPmAgNnS
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
závodem	závod	k1gInSc7
Bugatti	Bugatť	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Molsheim	Molsheima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1945	#num#	k4
až	až	k9
1949	#num#	k4
vězněn	věznit	k5eAaImNgInS
francouzskými	francouzský	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hans	Hans	k1gMnSc1
Trippel	Trippel	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Trippel	Trippel	k1gMnSc1
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hessische	Hessische	k1gFnSc1
Biografie	biografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Landesgeschichtliches	Landesgeschichtliches	k1gInSc1
Informationssystem	Informationssyst	k1gInSc7
Hessen	Hessen	k1gInSc1
(	(	kIx(
<g/>
LAGIS	LAGIS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hessisches	Hessisches	k1gMnSc1
Landesamt	Landesamt	k1gMnSc1
für	für	k?
geschichtliche	geschichtlichus	k1gMnSc5
Landeskunde	Landeskund	k1gMnSc5
(	(	kIx(
<g/>
HLGL	HLGL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navštíveno	navštíven	k2eAgNnSc4d1
28	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
↑	↑	k?
GREEN	GREEN	kA
<g/>
,	,	kIx,
By	by	k9
George	George	k1gInSc1
W.	W.	kA
Special	Special	k1gInSc1
Use	usus	k1gInSc5
Vehicles	Vehicles	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Illustrated	Illustrated	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Unconventional	Unconventional	k1gFnSc2
Cars	Carsa	k1gFnPc2
and	and	k?
Trucks	Trucksa	k1gFnPc2
Worldwide	Worldwid	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
McFarland	McFarland	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7864	#num#	k4
<g/>
-	-	kIx~
<g/>
1245	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Vojenský	vojenský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Amphicar	Amphicar	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
15	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1028997183	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
288102445	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
