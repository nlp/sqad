<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Luitpold	Luitpold	k1gMnSc1	Luitpold
Himmler	Himmler	k1gMnSc1	Himmler
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Lüneburg	Lüneburg	k1gMnSc1	Lüneburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
říšský	říšský	k2eAgMnSc1d1	říšský
vůdce	vůdce	k1gMnSc1	vůdce
SS	SS	kA	SS
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
Waffen-SS	Waffen-SS	k1gMnSc1	Waffen-SS
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
hromadného	hromadný	k2eAgNnSc2d1	hromadné
vyvražďování	vyvražďování	k1gNnSc2	vyvražďování
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
holokaustu	holokaust	k1gInSc6	holokaust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
zločiny	zločin	k1gInPc4	zločin
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
souzen	soudit	k5eAaImNgInS	soudit
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
zbožného	zbožný	k2eAgMnSc2d1	zbožný
přísného	přísný	k2eAgMnSc2d1	přísný
římsko-katolického	římskoatolický	k2eAgMnSc2d1	římsko-katolický
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
bavorského	bavorský	k2eAgMnSc2d1	bavorský
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Landshutu	Landshut	k1gInSc6	Landshut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
jako	jako	k9	jako
kadet	kadet	k1gMnSc1	kadet
u	u	k7c2	u
11	[number]	k4	11
<g/>
.	.	kIx.	.
bavorského	bavorský	k2eAgInSc2d1	bavorský
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
demobilizován	demobilizovat	k5eAaBmNgInS	demobilizovat
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
praktikant	praktikant	k1gMnSc1	praktikant
na	na	k7c6	na
statku	statek	k1gInSc6	statek
v	v	k7c6	v
Ingolstadtu	Ingolstadt	k1gInSc6	Ingolstadt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
složil	složit	k5eAaPmAgMnS	složit
maturitní	maturitní	k2eAgFnSc4d1	maturitní
zkoušku	zkouška	k1gFnSc4	zkouška
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
fakultě	fakulta	k1gFnSc6	fakulta
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
úspěšně	úspěšně	k6eAd1	úspěšně
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
s	s	k7c7	s
akademickým	akademický	k2eAgInSc7d1	akademický
titulem	titul	k1gInSc7	titul
Diplomlandwirth	Diplomlandwirtha	k1gFnPc2	Diplomlandwirtha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
ve	v	k7c6	v
Schleissheimu	Schleissheim	k1gInSc6	Schleissheim
u	u	k7c2	u
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
také	také	k9	také
do	do	k7c2	do
Röhmovy	Röhmův	k2eAgFnSc2d1	Röhmův
polovojenské	polovojenský	k2eAgFnSc2d1	polovojenská
organizace	organizace	k1gFnSc2	organizace
Reichsflagge	Reichsflagg	k1gFnSc2	Reichsflagg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1923	[number]	k4	1923
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
neúspěšného	úspěšný	k2eNgInSc2d1	neúspěšný
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
stranický	stranický	k2eAgInSc1d1	stranický
průkaz	průkaz	k1gInSc1	průkaz
měl	mít	k5eAaImAgInS	mít
číslo	číslo	k1gNnSc4	číslo
14	[number]	k4	14
303	[number]	k4	303
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
bez	bez	k7c2	bez
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
sekretář	sekretář	k1gMnSc1	sekretář
Gregora	Gregor	k1gMnSc2	Gregor
Strassera	Strasser	k1gMnSc2	Strasser
v	v	k7c6	v
župní	župní	k2eAgFnSc6d1	župní
služebně	služebna	k1gFnSc6	služebna
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Landshutu	Landshut	k1gInSc6	Landshut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
ústředí	ústředí	k1gNnSc2	ústředí
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Hovořil	hovořit	k5eAaImAgMnS	hovořit
5	[number]	k4	5
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1925	[number]	k4	1925
dal	dát	k5eAaPmAgMnS	dát
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
příkaz	příkaz	k1gInSc4	příkaz
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
své	svůj	k3xOyFgFnSc2	svůj
ochranky	ochranka	k1gFnSc2	ochranka
(	(	kIx(	(
<g/>
Stosstrupp-Hitler	Stosstrupp-Hitler	k1gInSc4	Stosstrupp-Hitler
<g/>
)	)	kIx)	)
Juliu	Julius	k1gMnSc3	Julius
Schreckovi	Schrecek	k1gMnSc3	Schrecek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zřídil	zřídit	k5eAaPmAgMnS	zřídit
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
jednotky	jednotka	k1gFnPc4	jednotka
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
představitelů	představitel	k1gMnPc2	představitel
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
názvu	název	k1gInSc2	název
ochranné	ochranný	k2eAgInPc1d1	ochranný
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Schutzstaffel	Schutzstaffel	k1gInSc1	Schutzstaffel
–	–	k?	–
SS	SS	kA	SS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
SS	SS	kA	SS
stálo	stát	k5eAaImAgNnS	stát
osm	osm	k4xCc1	osm
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
amatérský	amatérský	k2eAgMnSc1d1	amatérský
zápasník	zápasník	k1gMnSc1	zápasník
Ulrich	Ulrich	k1gMnSc1	Ulrich
Graf	graf	k1gInSc1	graf
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
hodinář	hodinář	k1gMnSc1	hodinář
Emil	Emil	k1gMnSc1	Emil
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
,	,	kIx,	,
drogista	drogista	k1gMnSc1	drogista
Julius	Julius	k1gMnSc1	Julius
Schaub	Schaub	k1gMnSc1	Schaub
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
armádní	armádní	k2eAgMnSc1d1	armádní
poručík	poručík	k1gMnSc1	poručík
Erhard	Erhard	k1gMnSc1	Erhard
Heiden	Heidno	k1gNnPc2	Heidno
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
koňský	koňský	k2eAgMnSc1d1	koňský
handlíř	handlíř	k1gMnSc1	handlíř
Weber	Weber	k1gMnSc1	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Schreck	Schreck	k1gMnSc1	Schreck
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
šéfem	šéf	k1gMnSc7	šéf
SS	SS	kA	SS
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1925	[number]	k4	1925
rozeslal	rozeslat	k5eAaPmAgInS	rozeslat
oběžník	oběžník	k1gInSc1	oběžník
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žádá	žádat	k5eAaImIp3nS	žádat
všechny	všechen	k3xTgFnPc4	všechen
místní	místní	k2eAgFnPc4d1	místní
organizace	organizace	k1gFnPc4	organizace
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zřídily	zřídit	k5eAaPmAgInP	zřídit
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
centrály	centrála	k1gFnSc2	centrála
místní	místní	k2eAgFnSc2d1	místní
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oběžníku	oběžník	k1gInSc2	oběžník
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
bojeschopné	bojeschopný	k2eAgFnPc4d1	bojeschopná
elitní	elitní	k2eAgFnPc4d1	elitní
skupiny	skupina	k1gFnPc4	skupina
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
Mnichově	Mnichov	k1gInSc6	Mnichov
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
20	[number]	k4	20
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Požadavek	požadavek	k1gInSc1	požadavek
elitnosti	elitnost	k1gFnSc2	elitnost
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
spíše	spíše	k9	spíše
fyzické	fyzický	k2eAgFnSc3d1	fyzická
zdatnosti	zdatnost	k1gFnSc3	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vypěstování	vypěstování	k1gNnSc6	vypěstování
naprosté	naprostý	k2eAgFnSc2d1	naprostá
oddanosti	oddanost	k1gFnSc2	oddanost
vůdci	vůdce	k1gMnSc3	vůdce
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
postarat	postarat	k5eAaPmF	postarat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
výchovný	výchovný	k2eAgInSc4d1	výchovný
řád	řád	k1gInSc4	řád
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1929	[number]	k4	1929
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
osobní	osobní	k2eAgFnSc2d1	osobní
ochranky	ochranka	k1gFnSc2	ochranka
SS	SS	kA	SS
(	(	kIx(	(
<g/>
Schutz	Schutz	k1gMnSc1	Schutz
Staffel	Staffel	k1gMnSc1	Staffel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
200	[number]	k4	200
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
za	za	k7c4	za
NSDAP	NSDAP	kA	NSDAP
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
Weser-Ems	Weser-Emsa	k1gFnPc2	Weser-Emsa
<g/>
.	.	kIx.	.
</s>
<s>
Himmler	Himmler	k1gMnSc1	Himmler
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
měly	mít	k5eAaImAgInP	mít
SS	SS	kA	SS
již	již	k9	již
52	[number]	k4	52
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
jejich	jejich	k3xOp3gFnSc4	jejich
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
SA	SA	kA	SA
Ernsta	Ernst	k1gMnSc2	Ernst
Röhma	Röhm	k1gMnSc2	Röhm
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
podřízeny	podřízen	k2eAgInPc1d1	podřízen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SS	SS	kA	SS
původně	původně	k6eAd1	původně
ideologickou	ideologický	k2eAgFnSc4d1	ideologická
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
službu	služba	k1gFnSc4	služba
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
službu	služba	k1gFnSc4	služba
SD	SD	kA	SD
(	(	kIx(	(
<g/>
Sicherheitsdienst	Sicherheitsdienst	k1gMnSc1	Sicherheitsdienst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vedením	vedení	k1gNnSc7	vedení
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Reinhardem	Reinhard	k1gMnSc7	Reinhard
Heydrichem	Heydrich	k1gMnSc7	Heydrich
tak	tak	k8xS	tak
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
upevnil	upevnit	k5eAaPmAgInS	upevnit
moc	moc	k1gFnSc4	moc
nacistů	nacista	k1gMnPc2	nacista
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Brunem	Bruno	k1gMnSc7	Bruno
Geschem	Gesch	k1gMnSc7	Gesch
<g/>
,	,	kIx,	,
Obersturmbannführerem-SS	Obersturmbannführerem-SS	k1gMnSc7	Obersturmbannführerem-SS
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1933	[number]	k4	1933
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mnichovským	mnichovský	k2eAgMnSc7d1	mnichovský
policejním	policejní	k2eAgMnSc7d1	policejní
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
velitelem	velitel	k1gMnSc7	velitel
politické	politický	k2eAgFnSc2d1	politická
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1933	[number]	k4	1933
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
všech	všecek	k3xTgFnPc2	všecek
jednotek	jednotka	k1gFnPc2	jednotka
politické	politický	k2eAgFnSc2d1	politická
policie	policie	k1gFnSc2	policie
kromě	kromě	k7c2	kromě
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
podřízen	podřídit	k5eAaPmNgInS	podřídit
Hermannu	Hermann	k1gMnSc3	Hermann
Göringovi	Göring	k1gMnSc3	Göring
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1934	[number]	k4	1934
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfem	šéf	k1gMnSc7	šéf
pruské	pruský	k2eAgFnSc2d1	pruská
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
tajné	tajný	k2eAgFnSc2d1	tajná
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
gestapo	gestapo	k1gNnSc1	gestapo
(	(	kIx(	(
<g/>
Geheime	Geheim	k1gMnSc5	Geheim
Staatspolizei	Staatspolize	k1gMnSc5	Staatspolize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
přelomem	přelom	k1gInSc7	přelom
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
Noc	noc	k1gFnSc4	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
<g/>
"	"	kIx"	"
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
násilně	násilně	k6eAd1	násilně
odstraněno	odstraněn	k2eAgNnSc4d1	odstraněno
celé	celý	k2eAgNnSc4d1	celé
původní	původní	k2eAgNnSc4d1	původní
velení	velení	k1gNnSc4	velení
SA	SA	kA	SA
včetně	včetně	k7c2	včetně
Ernsta	Ernst	k1gMnSc4	Ernst
Röhma	Röhm	k1gMnSc2	Röhm
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
skutečně	skutečně	k6eAd1	skutečně
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
organizace	organizace	k1gFnSc2	organizace
SS	SS	kA	SS
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
plně	plně	k6eAd1	plně
hájila	hájit	k5eAaImAgFnS	hájit
nacionálně	nacionálně	k6eAd1	nacionálně
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
myšlenku	myšlenka	k1gFnSc4	myšlenka
a	a	k8xC	a
převedla	převést	k5eAaPmAgFnS	převést
by	by	kYmCp3nP	by
rasistické	rasistický	k2eAgFnPc1d1	rasistická
myšlenky	myšlenka	k1gFnPc1	myšlenka
režimu	režim	k1gInSc2	režim
v	v	k7c4	v
dynamický	dynamický	k2eAgInSc4d1	dynamický
princip	princip	k1gInSc4	princip
rozhodné	rozhodný	k2eAgFnSc2d1	rozhodná
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1936	[number]	k4	1936
úspěšně	úspěšně	k6eAd1	úspěšně
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
převzetí	převzetí	k1gNnSc4	převzetí
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
šéfem	šéf	k1gMnSc7	šéf
tajné	tajný	k2eAgFnSc2d1	tajná
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
gestapo	gestapo	k1gNnSc4	gestapo
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
postavení	postavení	k1gNnSc3	postavení
Říšského	říšský	k2eAgMnSc2d1	říšský
vůdce	vůdce	k1gMnSc2	vůdce
SS	SS	kA	SS
(	(	kIx(	(
<g/>
SS-Reichsführer	SS-Reichsführer	k1gMnSc1	SS-Reichsführer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
schopným	schopný	k2eAgMnSc7d1	schopný
organizátorem	organizátor	k1gMnSc7	organizátor
a	a	k8xC	a
správcem	správce	k1gMnSc7	správce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pracovitým	pracovitý	k2eAgMnSc7d1	pracovitý
a	a	k8xC	a
výkonným	výkonný	k2eAgMnSc7d1	výkonný
úředníkem	úředník	k1gMnSc7	úředník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
všechny	všechen	k3xTgFnPc4	všechen
metody	metoda	k1gFnPc4	metoda
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
státního	státní	k2eAgInSc2d1	státní
terorismu	terorismus	k1gInSc2	terorismus
proti	proti	k7c3	proti
politickým	politický	k2eAgMnPc3d1	politický
a	a	k8xC	a
ostatním	ostatní	k2eAgMnPc3d1	ostatní
odpůrcům	odpůrce	k1gMnPc3	odpůrce
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
zřídil	zřídit	k5eAaPmAgMnS	zřídit
první	první	k4xOgMnSc1	první
koncentrační	koncentrační	k2eAgMnSc1d1	koncentrační
tábor	tábor	k1gMnSc1	tábor
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
u	u	k7c2	u
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
s	s	k7c7	s
Hitlerovým	Hitlerův	k2eAgInSc7d1	Hitlerův
souhlasem	souhlas	k1gInSc7	souhlas
podstatně	podstatně	k6eAd1	podstatně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
okruh	okruh	k1gInSc1	okruh
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
internovány	internován	k2eAgFnPc1d1	internována
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Vyznával	vyznávat	k5eAaImAgMnS	vyznávat
filozofický	filozofický	k2eAgInSc4d1	filozofický
mysticismus	mysticismus	k1gInSc4	mysticismus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
posedlý	posedlý	k2eAgInSc4d1	posedlý
názory	názor	k1gInPc4	názor
mesmerismu	mesmerismus	k1gInSc2	mesmerismus
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
okultismus	okultismus	k1gInSc4	okultismus
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgNnSc4d1	přírodní
léčitelství	léčitelství	k1gNnSc4	léčitelství
<g/>
,	,	kIx,	,
homeopatii	homeopatie	k1gFnSc4	homeopatie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
fanatickým	fanatický	k2eAgMnSc7d1	fanatický
rasistou	rasista	k1gMnSc7	rasista
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
"	"	kIx"	"
<g/>
árijský	árijský	k2eAgInSc4d1	árijský
<g/>
"	"	kIx"	"
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
nadčlověku	nadčlověk	k1gMnSc6	nadčlověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1937	[number]	k4	1937
Himmler	Himmler	k1gMnSc1	Himmler
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
neexistuje	existovat	k5eNaImIp3nS	existovat
lepší	dobrý	k2eAgInSc4d2	lepší
živý	živý	k2eAgInSc4d1	živý
důkaz	důkaz	k1gInSc4	důkaz
dědičnosti	dědičnost	k1gFnSc2	dědičnost
a	a	k8xC	a
rasových	rasový	k2eAgInPc2d1	rasový
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
než	než	k8xS	než
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hydrocefalici	hydrocefalik	k1gMnPc1	hydrocefalik
<g/>
,	,	kIx,	,
šilhající	šilhající	k2eAgNnPc1d1	šilhající
a	a	k8xC	a
deformovaná	deformovaný	k2eAgNnPc1d1	deformované
individua	individuum	k1gNnPc1	individuum
<g/>
,	,	kIx,	,
položidé	položid	k1gMnPc1	položid
–	–	k?	–
nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
počet	počet	k1gInSc1	počet
podřadných	podřadný	k2eAgMnPc2d1	podřadný
podlidí	podčlověk	k1gMnPc2	podčlověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
německého	německý	k2eAgInSc2d1	německý
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
likvidaci	likvidace	k1gFnSc4	likvidace
těchto	tento	k3xDgMnPc2	tento
podlidí	podčlověk	k1gMnPc2	podčlověk
všude	všude	k6eAd1	všude
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
jádrem	jádro	k1gNnSc7	jádro
nordické	nordický	k2eAgFnSc2d1	nordická
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnPc1	jádro
germánského	germánský	k2eAgInSc2d1	germánský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
nositele	nositel	k1gMnSc4	nositel
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Záhy	záhy	k6eAd1	záhy
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
rasovou	rasový	k2eAgFnSc4d1	rasová
otázku	otázka	k1gFnSc4	otázka
z	z	k7c2	z
"	"	kIx"	"
<g/>
negativního	negativní	k2eAgInSc2d1	negativní
konceptu	koncept	k1gInSc2	koncept
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
antisemitismu	antisemitismus	k1gInSc6	antisemitismus
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
organizační	organizační	k2eAgInSc4d1	organizační
princip	princip	k1gInSc4	princip
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
jednotek	jednotka	k1gFnPc2	jednotka
SS	SS	kA	SS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rasovou	rasový	k2eAgFnSc4d1	rasová
teorii	teorie	k1gFnSc4	teorie
v	v	k7c4	v
rasovou	rasový	k2eAgFnSc4d1	rasová
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrační	koncentrační	k2eAgInPc1d1	koncentrační
tábory	tábor	k1gInPc1	tábor
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
staly	stát	k5eAaPmAgInP	stát
tábory	tábor	k1gInPc4	tábor
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Árijská	árijský	k2eAgFnSc1d1	árijská
<g/>
"	"	kIx"	"
nadvláda	nadvláda	k1gFnSc1	nadvláda
byla	být	k5eAaImAgFnS	být
upevňována	upevňovat	k5eAaImNgFnS	upevňovat
systematickým	systematický	k2eAgNnSc7d1	systematické
a	a	k8xC	a
organizovaným	organizovaný	k2eAgNnSc7d1	organizované
vyhlazováním	vyhlazování	k1gNnSc7	vyhlazování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Slovanů	Slovan	k1gMnPc2	Slovan
a	a	k8xC	a
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
romantický	romantický	k2eAgInSc1d1	romantický
sen	sen	k1gInSc1	sen
o	o	k7c6	o
modrookých	modrooký	k2eAgFnPc6d1	modrooká
blonďatých	blonďatý	k2eAgFnPc6d1	blonďatá
hrdinech	hrdina	k1gMnPc6	hrdina
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
kultivací	kultivace	k1gFnPc2	kultivace
elity	elita	k1gFnSc2	elita
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
zákonů	zákon	k1gInPc2	zákon
výběru	výběr	k1gInSc2	výběr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kritérií	kritérion	k1gNnPc2	kritérion
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
mentálních	mentální	k2eAgInPc2d1	mentální
a	a	k8xC	a
fyzických	fyzický	k2eAgInPc2d1	fyzický
testů	test	k1gInPc2	test
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
aristokratické	aristokratický	k2eAgFnPc1d1	aristokratická
představy	představa	k1gFnPc1	představa
vůdcovství	vůdcovství	k1gNnSc1	vůdcovství
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
úmyslným	úmyslný	k2eAgNnSc7d1	úmyslné
křížením	křížení	k1gNnSc7	křížení
rasově	rasově	k6eAd1	rasově
vybraných	vybraný	k2eAgMnPc2d1	vybraný
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
kombinací	kombinace	k1gFnPc2	kombinace
jejich	jejich	k3xOp3gFnSc2	jejich
charismatické	charismatický	k2eAgFnSc2d1	charismatická
autority	autorita	k1gFnSc2	autorita
s	s	k7c7	s
byrokratickou	byrokratický	k2eAgFnSc7d1	byrokratická
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Příslušník	příslušník	k1gMnSc1	příslušník
SS	SS	kA	SS
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
představitelem	představitel	k1gMnSc7	představitel
nového	nový	k2eAgMnSc4d1	nový
člověka	člověk	k1gMnSc4	člověk
<g/>
:	:	kIx,	:
bojovníka	bojovník	k1gMnSc4	bojovník
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc4	správce
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc4	učitel
a	a	k8xC	a
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
pro	pro	k7c4	pro
kolonizaci	kolonizace	k1gFnSc4	kolonizace
východních	východní	k2eAgNnPc2d1	východní
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
syntetická	syntetický	k2eAgFnSc1d1	syntetická
aristokracie	aristokracie	k1gFnSc1	aristokracie
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
trénována	trénovat	k5eAaImNgFnS	trénovat
v	v	k7c6	v
polouzavřené	polouzavřený	k2eAgFnSc6d1	polouzavřená
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
podporována	podporován	k2eAgNnPc4d1	podporováno
nacistickým	nacistický	k2eAgInSc7d1	nacistický
systémem	systém	k1gInSc7	systém
jako	jako	k8xS	jako
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgMnSc1d1	malý
nenápadný	nápadný	k2eNgMnSc1d1	nenápadný
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
brýlemi	brýle	k1gFnPc7	brýle
vypadající	vypadající	k2eAgFnSc7d1	vypadající
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
bankovní	bankovní	k2eAgMnSc1d1	bankovní
úředník	úředník	k1gMnSc1	úředník
než	než	k8xS	než
jako	jako	k8xC	jako
diktátor	diktátor	k1gMnSc1	diktátor
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
představoval	představovat	k5eAaImAgInS	představovat
kuriózní	kuriózní	k2eAgFnSc7d1	kuriózní
směsí	směs	k1gFnSc7	směs
bizarnosti	bizarnost	k1gFnSc2	bizarnost
<g/>
,	,	kIx,	,
romantické	romantický	k2eAgFnSc2d1	romantická
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
chladné	chladný	k2eAgFnSc2d1	chladná
vědomé	vědomý	k2eAgFnSc2d1	vědomá
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
popisován	popisovat	k5eAaImNgMnS	popisovat
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
bez	bez	k7c2	bez
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
bez	bez	k7c2	bez
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nesnášel	snášet	k5eNaImAgMnS	snášet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
popravě	poprava	k1gFnSc3	poprava
stovky	stovka	k1gFnSc2	stovka
východních	východní	k2eAgInPc2d1	východní
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
ruské	ruský	k2eAgFnSc6d1	ruská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
zkušenosti	zkušenost	k1gFnSc6	zkušenost
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
popravy	poprava	k1gFnPc1	poprava
Židů	Žid	k1gMnPc2	Žid
probíhaly	probíhat	k5eAaImAgFnP	probíhat
"	"	kIx"	"
<g/>
lidštějším	lidský	k2eAgMnSc7d2	lidštější
<g/>
"	"	kIx"	"
způsobem	způsob	k1gInSc7	způsob
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
speciálně	speciálně	k6eAd1	speciálně
upravených	upravený	k2eAgFnPc6d1	upravená
komorách	komora	k1gFnPc6	komora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vypadaly	vypadat	k5eAaImAgFnP	vypadat
jako	jako	k9	jako
sprchovací	sprchovací	k2eAgFnPc1d1	sprchovací
místnosti	místnost	k1gFnPc1	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
maloměstského	maloměstský	k2eAgMnSc4d1	maloměstský
excentrika	excentrik	k1gMnSc4	excentrik
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
přirozené	přirozený	k2eAgNnSc1d1	přirozené
snobství	snobství	k1gNnSc1	snobství
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožnil	umožnit	k5eAaPmAgInS	umožnit
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
SS	SS	kA	SS
také	také	k6eAd1	také
příslušníkům	příslušník	k1gMnPc3	příslušník
starých	starý	k2eAgInPc2d1	starý
aristokratických	aristokratický	k2eAgInPc2d1	aristokratický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Vyznával	vyznávat	k5eAaImAgMnS	vyznávat
primitivní	primitivní	k2eAgNnSc4d1	primitivní
náboženství	náboženství	k1gNnSc4	náboženství
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
kosmologickými	kosmologický	k2eAgNnPc7d1	kosmologické
dogmaty	dogma	k1gNnPc7	dogma
a	a	k8xC	a
spojoval	spojovat	k5eAaImAgMnS	spojovat
příslušníky	příslušník	k1gMnPc4	příslušník
SS	SS	kA	SS
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
vzdálenými	vzdálený	k2eAgMnPc7d1	vzdálený
germánskými	germánský	k2eAgMnPc7d1	germánský
předky	předek	k1gMnPc7	předek
<g/>
.	.	kIx.	.
</s>
<s>
Pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
romantické	romantický	k2eAgFnPc4d1	romantická
představy	představa	k1gFnPc4	představa
"	"	kIx"	"
<g/>
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
půdě	půda	k1gFnSc3	půda
<g/>
"	"	kIx"	"
a	a	k8xC	a
německého	německý	k2eAgNnSc2d1	německé
"	"	kIx"	"
<g/>
branného	branný	k2eAgMnSc2d1	branný
rolníka	rolník	k1gMnSc2	rolník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zušlechťuje	zušlechťovat	k5eAaImIp3nS	zušlechťovat
půdu	půda	k1gFnSc4	půda
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
výkonným	výkonný	k2eAgMnSc7d1	výkonný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
racionalizovaných	racionalizovaný	k2eAgFnPc2d1	racionalizovaná
moderních	moderní	k2eAgFnPc2d1	moderní
metod	metoda	k1gFnPc2	metoda
vyhlazování	vyhlazování	k1gNnSc2	vyhlazování
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
převtělení	převtělení	k1gNnSc4	převtělení
předkřesťanského	předkřesťanský	k2eAgMnSc2d1	předkřesťanský
Sasa	Sas	k1gMnSc2	Sas
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Ptáčníka	Ptáčník	k1gMnSc2	Ptáčník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
Himmler	Himmler	k1gMnSc1	Himmler
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
oslavu	oslava	k1gFnSc4	oslava
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Jindřichovy	Jindřichův	k2eAgFnSc2d1	Jindřichova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
vegetariánem	vegetarián	k1gMnSc7	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Velitelům	velitel	k1gMnPc3	velitel
SS	SS	kA	SS
dokonce	dokonce	k9	dokonce
nařizoval	nařizovat	k5eAaImAgInS	nařizovat
pojídání	pojídání	k1gNnSc4	pojídání
vegetariánské	vegetariánský	k2eAgFnSc2d1	vegetariánská
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Himmlerova	Himmlerův	k2eAgFnSc1d1	Himmlerova
osobnost	osobnost	k1gFnSc1	osobnost
dokonale	dokonale	k6eAd1	dokonale
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
všechny	všechen	k3xTgInPc4	všechen
rozpory	rozpor	k1gInPc4	rozpor
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byly	být	k5eAaImAgFnP	být
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
spojením	spojení	k1gNnSc7	spojení
starého	starý	k2eAgInSc2d1	starý
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
aristokracií	aristokracie	k1gFnSc7	aristokracie
Herrenvolk	Herrenvolka	k1gFnPc2	Herrenvolka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
cti	čest	k1gFnSc2	čest
<g/>
,	,	kIx,	,
poslušnosti	poslušnost	k1gFnSc2	poslušnost
<g/>
,	,	kIx,	,
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Říšský	říšský	k2eAgMnSc1d1	říšský
vůdce	vůdce	k1gMnSc1	vůdce
SS	SS	kA	SS
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
princip	princip	k1gInSc1	princip
rasového	rasový	k2eAgInSc2d1	rasový
výběru	výběr	k1gInSc2	výběr
příslušníků	příslušník	k1gMnPc2	příslušník
SS	SS	kA	SS
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
sňatky	sňatek	k1gInPc4	sňatek
příslušníků	příslušník	k1gMnPc2	příslušník
SS	SS	kA	SS
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
párování	párování	k1gNnSc1	párování
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
"	"	kIx"	"
<g/>
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
státem	stát	k1gInSc7	stát
řízenou	řízený	k2eAgFnSc4d1	řízená
lidskou	lidský	k2eAgFnSc4d1	lidská
farmu	farma	k1gFnSc4	farma
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lebensborn	Lebensborn	k1gInSc1	Lebensborn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
s	s	k7c7	s
dokonalými	dokonalý	k2eAgFnPc7d1	dokonalá
nordickými	nordický	k2eAgFnPc7d1	nordická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mladým	mladý	k2eAgMnPc3d1	mladý
příslušníkům	příslušník	k1gMnPc3	příslušník
SS	SS	kA	SS
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
početí	početí	k1gNnSc2	početí
dokonalého	dokonalý	k2eAgMnSc2d1	dokonalý
nordického	nordický	k2eAgMnSc2d1	nordický
potomka	potomek	k1gMnSc2	potomek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vychováván	vychováván	k2eAgInSc1d1	vychováván
podle	podle	k7c2	podle
národně	národně	k6eAd1	národně
socialistického	socialistický	k2eAgInSc2d1	socialistický
ideálu	ideál	k1gInSc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Himmler	Himmler	k1gInSc1	Himmler
byl	být	k5eAaImAgInS	být
posedlý	posedlý	k2eAgInSc1d1	posedlý
vytvořením	vytvoření	k1gNnSc7	vytvoření
rasy	rasa	k1gFnSc2	rasa
"	"	kIx"	"
<g/>
superlidí	superlidit	k5eAaPmIp3nS	superlidit
<g/>
"	"	kIx"	"
pomocí	pomocí	k7c2	pomocí
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
povinností	povinnost	k1gFnSc7	povinnost
německých	německý	k2eAgFnPc2d1	německá
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
krví	krev	k1gFnSc7	krev
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
mateřství	mateřství	k1gNnSc3	mateřství
s	s	k7c7	s
morální	morální	k2eAgFnSc7d1	morální
vážností	vážnost	k1gFnSc7	vážnost
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
stát	stát	k5eAaPmF	stát
matkami	matka	k1gFnPc7	matka
dětí	dítě	k1gFnPc2	dítě
vojáků	voják	k1gMnPc2	voják
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
bitvy	bitva	k1gFnPc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
váleční	váleční	k2eAgMnPc1d1	váleční
hrdinové	hrdina	k1gMnPc1	hrdina
mohli	moct	k5eAaImAgMnP	moct
uzavřít	uzavřít	k5eAaPmF	uzavřít
druhý	druhý	k4xOgInSc4	druhý
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
SS	SS	kA	SS
brzy	brzy	k6eAd1	brzy
tvořili	tvořit	k5eAaImAgMnP	tvořit
privilegovanou	privilegovaný	k2eAgFnSc4d1	privilegovaná
kastu	kasta	k1gFnSc4	kasta
nacistické	nacistický	k2eAgFnSc2d1	nacistická
imperiální	imperiální	k2eAgFnSc2d1	imperiální
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
jádrem	jádro	k1gNnSc7	jádro
nového	nový	k2eAgInSc2d1	nový
státního	státní	k2eAgInSc2d1	státní
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
brzy	brzy	k6eAd1	brzy
prorostl	prorůst	k5eAaPmAgInS	prorůst
celou	celá	k1gFnSc4	celá
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
život	život	k1gInSc1	život
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
Třetí	třetí	k4xOgFnSc6	třetí
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jednotek	jednotka	k1gFnPc2	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
přijímáni	přijímat	k5eAaImNgMnP	přijímat
i	i	k9	i
"	"	kIx"	"
<g/>
Árijci	Árijec	k1gMnPc1	Árijec
<g/>
"	"	kIx"	"
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
"	"	kIx"	"
<g/>
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
německého	německý	k2eAgInSc2d1	německý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
feudálních	feudální	k2eAgInPc6d1	feudální
principech	princip	k1gInPc6	princip
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
kasty	kasta	k1gFnSc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
myšlenka	myšlenka	k1gFnSc1	myšlenka
Velké	velký	k2eAgFnSc2d1	velká
germánské	germánský	k2eAgFnSc2d1	germánská
říše	říš	k1gFnSc2	říš
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
stala	stát	k5eAaPmAgFnS	stát
reálnější	reální	k2eAgFnSc1d2	reální
<g/>
,	,	kIx,	,
když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
jej	on	k3xPp3gMnSc4	on
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Říšským	říšský	k2eAgMnSc7d1	říšský
komisařem	komisař	k1gMnSc7	komisař
pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
německého	německý	k2eAgNnSc2d1	německé
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
Reichskommissar	Reichskommissar	k1gInSc1	Reichskommissar
für	für	k?	für
die	die	k?	die
Festigung	Festigung	k1gInSc1	Festigung
des	des	k1gNnSc1	des
Deutschen	Deutschna	k1gFnPc2	Deutschna
Volkstums	Volkstumsa	k1gFnPc2	Volkstumsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
tak	tak	k9	tak
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
nově	nově	k6eAd1	nově
anektovaným	anektovaný	k2eAgNnSc7d1	anektované
územím	území	k1gNnSc7	území
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
zodpovídal	zodpovídat	k5eAaImAgMnS	zodpovídat
za	za	k7c4	za
návrat	návrat	k1gInSc4	návrat
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
neněmeckém	německý	k2eNgNnSc6d1	neněmecké
území	území	k1gNnSc6	území
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
německými	německý	k2eAgMnPc7d1	německý
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
Volksdeutche	Volksdeutche	k1gFnSc4	Volksdeutche
<g/>
)	)	kIx)	)
z	z	k7c2	z
baltických	baltický	k2eAgFnPc2d1	baltická
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
vně	vně	k7c2	vně
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milión	milión	k4xCgInSc1	milión
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
odsunuto	odsunut	k2eAgNnSc1d1	odsunuto
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Informoval	informovat	k5eAaBmAgMnS	informovat
příslušníky	příslušník	k1gMnPc4	příslušník
jednotky	jednotka	k1gFnSc2	jednotka
SS-Leibstandarte	SS-Leibstandart	k1gInSc5	SS-Leibstandart
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
snazší	snadný	k2eAgMnSc1d2	snazší
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
jít	jít	k5eAaImF	jít
společně	společně	k6eAd1	společně
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
než	než	k8xS	než
potlačovat	potlačovat	k5eAaImF	potlačovat
překážející	překážející	k2eAgFnSc4d1	překážející
populaci	populace	k1gFnSc4	populace
lidí	člověk	k1gMnPc2	člověk
nižší	nízký	k2eAgFnSc2d2	nižší
kulturní	kulturní	k2eAgFnSc2d1	kulturní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
provádět	provádět	k5eAaImF	provádět
popravy	poprava	k1gFnPc4	poprava
<g/>
,	,	kIx,	,
transportovat	transportovat	k5eAaBmF	transportovat
tyto	tento	k3xDgMnPc4	tento
lidi	člověk	k1gMnPc4	člověk
pryč	pryč	k6eAd1	pryč
nebo	nebo	k8xC	nebo
násilně	násilně	k6eAd1	násilně
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
plačící	plačící	k2eAgFnPc1d1	plačící
a	a	k8xC	a
hysterické	hysterický	k2eAgFnPc1d1	hysterická
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgInS	dokázat
příslušníky	příslušník	k1gMnPc4	příslušník
SS	SS	kA	SS
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
svým	svůj	k3xOyFgInSc7	svůj
apokalyptickým	apokalyptický	k2eAgInSc7d1	apokalyptický
"	"	kIx"	"
<g/>
idealismem	idealismus	k1gInSc7	idealismus
<g/>
"	"	kIx"	"
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
vinu	vina	k1gFnSc4	vina
ani	ani	k8xC	ani
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
masové	masový	k2eAgNnSc4d1	masové
vyvražďování	vyvražďování	k1gNnSc4	vyvražďování
tisíců	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
nevině	nevina	k1gFnSc6	nevina
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
i	i	k8xC	i
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
hovořil	hovořit	k5eAaImAgMnS	hovořit
k	k	k7c3	k
velitelům	velitel	k1gMnPc3	velitel
SS	SS	kA	SS
v	v	k7c6	v
Poznani	Poznaň	k1gFnSc6	Poznaň
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
až	až	k9	až
1941	[number]	k4	1941
formuloval	formulovat	k5eAaImAgInS	formulovat
základy	základ	k1gInPc4	základ
východního	východní	k2eAgInSc2d1	východní
generálního	generální	k2eAgInSc2d1	generální
plánu	plán	k1gInSc2	plán
(	(	kIx(	(
<g/>
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
likvidací	likvidace	k1gFnSc7	likvidace
nebo	nebo	k8xC	nebo
deportací	deportace	k1gFnSc7	deportace
pěti	pět	k4xCc2	pět
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
Hitlerem	Hitler	k1gMnSc7	Hitler
pověřen	pověřit	k5eAaPmNgMnS	pověřit
"	"	kIx"	"
<g/>
konečným	konečný	k2eAgNnSc7d1	konečné
řešením	řešení	k1gNnSc7	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1944	[number]	k4	1944
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
podřízena	podřízen	k2eAgFnSc1d1	podřízena
armádní	armádní	k2eAgFnSc1d1	armádní
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
týlové	týlový	k2eAgFnSc2d1	týlová
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
titulu	titul	k1gInSc2	titul
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
pod	pod	k7c4	pod
něj	on	k3xPp3gMnSc4	on
spadalo	spadat	k5eAaPmAgNnS	spadat
potlačení	potlačení	k1gNnSc1	potlačení
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
osobně	osobně	k6eAd1	osobně
byl	být	k5eAaImAgMnS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
masakry	masakr	k1gInPc4	masakr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
příkaz	příkaz	k1gInSc4	příkaz
nerespektovaly	respektovat	k5eNaImAgFnP	respektovat
Ženevské	ženevský	k2eAgFnPc1d1	Ženevská
konvence	konvence	k1gFnPc1	konvence
a	a	k8xC	a
masově	masově	k6eAd1	masově
vraždily	vraždit	k5eAaImAgFnP	vraždit
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
zajaté	zajatý	k2eAgMnPc4d1	zajatý
bojovníky	bojovník	k1gMnPc4	bojovník
i	i	k8xC	i
ošetřovatelky	ošetřovatelka	k1gFnPc4	ošetřovatelka
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
všechny	všechen	k3xTgMnPc4	všechen
civilní	civilní	k2eAgMnPc4d1	civilní
obyvatele	obyvatel	k1gMnPc4	obyvatel
(	(	kIx(	(
<g/>
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
nemluvňata	nemluvně	k1gNnPc1	nemluvně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
velením	velení	k1gNnSc7	velení
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Horní	horní	k2eAgInSc1d1	horní
Rýn	Rýn	k1gInSc1	Rýn
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
velitelem	velitel	k1gMnSc7	velitel
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Visla	Visla	k1gFnSc1	Visla
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Spojencům	spojenec	k1gMnPc3	spojenec
jednání	jednání	k1gNnSc4	jednání
o	o	k7c4	o
separátní	separátní	k2eAgFnSc4d1	separátní
kapitulaci	kapitulace	k1gFnSc4	kapitulace
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
ho	on	k3xPp3gMnSc4	on
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
zbavil	zbavit	k5eAaPmAgMnS	zbavit
všech	všecek	k3xTgFnPc2	všecek
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
z	z	k7c2	z
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
admirál	admirál	k1gMnSc1	admirál
Karl	Karl	k1gMnSc1	Karl
Dönitz	Dönitz	k1gMnSc1	Dönitz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nástupcem	nástupce	k1gMnSc7	nástupce
po	po	k7c6	po
Hitlerovi	Hitler	k1gMnSc6	Hitler
<g/>
,	,	kIx,	,
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
jeho	jeho	k3xOp3gFnPc2	jeho
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajných	tajný	k2eAgInPc6d1	tajný
dokumentech	dokument	k1gInPc6	dokument
britské	britský	k2eAgFnSc2d1	britská
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
politický	politický	k2eAgInSc4d1	politický
azyl	azyl	k1gInSc4	azyl
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
200	[number]	k4	200
předních	přední	k2eAgMnPc2d1	přední
nacistů	nacista	k1gMnPc2	nacista
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
3500	[number]	k4	3500
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dokumentů	dokument	k1gInPc2	dokument
byli	být	k5eAaImAgMnP	být
vězni	vězeň	k1gMnPc1	vězeň
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
odesláni	odeslat	k5eAaPmNgMnP	odeslat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
nákladních	nákladní	k2eAgInPc6d1	nákladní
vlacích	vlak	k1gInPc6	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
prchal	prchat	k5eAaImAgMnS	prchat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
pobočníky	pobočník	k1gMnPc4	pobočník
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c2	za
rotmistra	rotmistr	k1gMnSc2	rotmistr
vojenské	vojenský	k2eAgFnSc2d1	vojenská
politické	politický	k2eAgFnSc2d1	politická
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Geheime	Geheim	k1gMnSc5	Geheim
Feldspolizei	Feldspolize	k1gMnSc5	Feldspolize
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
padělaným	padělaný	k2eAgInSc7d1	padělaný
dokladem	doklad	k1gInSc7	doklad
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hitzinger	Hitzinger	k1gMnSc1	Hitzinger
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půli	půle	k1gFnSc6	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Brémami	Brémy	k1gFnPc7	Brémy
a	a	k8xC	a
Hamburkem	Hamburk	k1gInSc7	Hamburk
byl	být	k5eAaImAgMnS	být
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
britskými	britský	k2eAgMnPc7d1	britský
vojáky	voják	k1gMnPc4	voják
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nebyl	být	k5eNaImAgInS	být
vůbec	vůbec	k9	vůbec
nikým	nikdo	k3yNnSc7	nikdo
poznán	poznat	k5eAaPmNgInS	poznat
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
bezvýznamného	bezvýznamný	k2eAgMnSc4d1	bezvýznamný
anonymního	anonymní	k2eAgMnSc4d1	anonymní
zajatce	zajatec	k1gMnSc4	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
bez	bez	k7c2	bez
donucení	donucení	k1gNnPc2	donucení
v	v	k7c6	v
internačním	internační	k2eAgInSc6d1	internační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Bertfeldu	Bertfeld	k1gInSc6	Bertfeld
přiznal	přiznat	k5eAaPmAgMnS	přiznat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
pravé	pravý	k2eAgFnSc3d1	pravá
totožnosti	totožnost	k1gFnSc3	totožnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
pravých	pravý	k2eAgInPc6d1	pravý
důvodech	důvod	k1gInPc6	důvod
tohoto	tento	k3xDgInSc2	tento
jeho	jeho	k3xOp3gInSc3	jeho
kroku	krok	k1gInSc3	krok
však	však	k9	však
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
po	po	k7c6	po
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
Lüneburgu	Lüneburg	k1gInSc2	Lüneburg
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
zajetí	zajetí	k1gNnSc6	zajetí
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
kyanidem	kyanid	k1gInSc7	kyanid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
ukryt	ukrýt	k5eAaPmNgInS	ukrýt
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgInPc7	svůj
zuby	zub	k1gInPc7	zub
(	(	kIx(	(
<g/>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
britských	britský	k2eAgMnPc2d1	britský
zpravodajských	zpravodajský	k2eAgMnPc2d1	zpravodajský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
svědkem	svědek	k1gMnSc7	svědek
jeho	jeho	k3xOp3gNnSc2	jeho
zatčení	zatčení	k1gNnSc2	zatčení
<g/>
,	,	kIx,	,
výslechu	výslech	k1gInSc2	výslech
a	a	k8xC	a
internace	internace	k1gFnSc2	internace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
i	i	k9	i
pozdější	pozdní	k2eAgMnSc1d2	pozdější
izraelský	izraelský	k2eAgMnSc1d1	izraelský
prezident	prezident	k1gMnSc1	prezident
Chaim	Chaim	k1gMnSc1	Chaim
Herzog	Herzog	k1gMnSc1	Herzog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1	pohřben
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgMnSc7	jeden
britským	britský	k2eAgMnSc7d1	britský
seržantem	seržant	k1gMnSc7	seržant
zcela	zcela	k6eAd1	zcela
anonymně	anonymně	k6eAd1	anonymně
na	na	k7c6	na
Lüneburském	Lüneburský	k2eAgNnSc6d1	Lüneburské
vřesovišti	vřesoviště	k1gNnSc6	vřesoviště
do	do	k7c2	do
neoznačeného	označený	k2eNgInSc2d1	neoznačený
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
osobních	osobní	k2eAgFnPc2d1	osobní
věcí	věc	k1gFnPc2	věc
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
dochoval	dochovat	k5eAaPmAgInS	dochovat
pouze	pouze	k6eAd1	pouze
monokl	monokl	k1gInSc1	monokl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
dánské	dánský	k2eAgFnSc6d1	dánská
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
<g/>
:	:	kIx,	:
Nacisté	nacista	k1gMnPc1	nacista
Knopp	Knopp	k1gInSc4	Knopp
<g/>
,	,	kIx,	,
Guido	Guido	k1gNnSc4	Guido
<g/>
:	:	kIx,	:
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
pomocníci	pomocník	k1gMnPc1	pomocník
Pool	Pool	k1gMnSc1	Pool
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
:	:	kIx,	:
Kdo	kdo	k3yQnSc1	kdo
financoval	financovat	k5eAaBmAgMnS	financovat
nástup	nástup	k1gInSc4	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Pool	Pool	k1gInSc1	Pool
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
<g/>
:	:	kIx,	:
Tajní	tajný	k2eAgMnPc1d1	tajný
spojenci	spojenec	k1gMnPc1	spojenec
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
Padfield	Padfield	k1gInSc4	Padfield
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
:	:	kIx,	:
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
,	,	kIx,	,
Reichsfuhrer	Reichsfuhrer	k1gMnSc1	Reichsfuhrer
SS	SS	kA	SS
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Votobia	Votobia	k1gFnSc1	Votobia
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7198-283-0	[number]	k4	80-7198-283-0
Gestapo	gestapo	k1gNnSc1	gestapo
Holokaust	holokaust	k1gInSc4	holokaust
SS	SS	kA	SS
Poznaňský	poznaňský	k2eAgInSc4d1	poznaňský
projev	projev	k1gInSc4	projev
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gInSc1	Himmler
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
Osoba	osoba	k1gFnSc1	osoba
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
stránkách	stránka	k1gFnPc6	stránka
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Životopis	životopis	k1gInSc1	životopis
</s>
