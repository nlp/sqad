<s>
Chicago	Chicago	k1gNnSc1	Chicago
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
a	a	k8xC	a
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Chicaga	Chicago	k1gNnSc2	Chicago
má	mít	k5eAaImIp3nS	mít
takřka	takřka	k6eAd1	takřka
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Illinois	Illinois	k1gFnSc2	Illinois
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
přezdívku	přezdívka	k1gFnSc4	přezdívka
Windy	Winda	k1gMnSc2	Winda
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
Větrné	větrný	k2eAgNnSc1d1	větrné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
New	New	k1gMnSc1	New
Yorkem	York	k1gInSc7	York
nebo	nebo	k8xC	nebo
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
je	být	k5eAaImIp3nS	být
Chicago	Chicago	k1gNnSc1	Chicago
mnohem	mnohem	k6eAd1	mnohem
čistší	čistý	k2eAgMnSc1d2	čistší
a	a	k8xC	a
zelenější	zelený	k2eAgMnSc1d2	zelenější
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
parků	park	k1gInPc2	park
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
City	City	k1gFnSc1	City
in	in	k?	in
a	a	k8xC	a
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
městské	městský	k2eAgNnSc1d1	Městské
motto	motto	k1gNnSc1	motto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
budovami	budova	k1gFnPc7	budova
jsou	být	k5eAaImIp3nP	být
Willis	Willis	k1gInSc4	Willis
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
443	[number]	k4	443
m	m	kA	m
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
USA	USA	kA	USA
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
423	[number]	k4	423
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aon	Aon	k1gFnPc4	Aon
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
347	[number]	k4	347
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
344	[number]	k4	344
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1673	[number]	k4	1673
si	se	k3xPyFc3	se
dva	dva	k4xCgMnPc1	dva
francouzští	francouzský	k2eAgMnPc1d1	francouzský
cestovatelé	cestovatel	k1gMnPc1	cestovatel
Louis	Louis	k1gMnSc1	Louis
Jolliet	Jolliet	k1gMnSc1	Jolliet
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
Marquette	Marquett	k1gInSc5	Marquett
zřídili	zřídit	k5eAaPmAgMnP	zřídit
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
základnu	základna	k1gFnSc4	základna
na	na	k7c6	na
močálovitém	močálovitý	k2eAgInSc6d1	močálovitý
břehu	břeh	k1gInSc6	břeh
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nehostinné	hostinný	k2eNgNnSc1d1	nehostinné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
tudy	tudy	k6eAd1	tudy
procházeli	procházet	k5eAaImAgMnP	procházet
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
lovci	lovec	k1gMnPc1	lovec
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
si	se	k3xPyFc3	se
v	v	k7c6	v
bažině	bažina	k1gFnSc6	bažina
postavil	postavit	k5eAaPmAgMnS	postavit
srub	srub	k1gInSc4	srub
Haiťan	Haiťan	k1gMnSc1	Haiťan
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Point	pointa	k1gFnPc2	pointa
du	du	k?	du
Sable	Sable	k1gNnSc4	Sable
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
stálou	stálý	k2eAgFnSc4d1	stálá
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
hranice	hranice	k1gFnSc1	hranice
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
posunovala	posunovat	k5eAaImAgFnS	posunovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
farmáři	farmář	k1gMnPc1	farmář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
na	na	k7c6	na
původně	původně	k6eAd1	původně
indiánském	indiánský	k2eAgNnSc6d1	indiánské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
zde	zde	k6eAd1	zde
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
postavila	postavit	k5eAaPmAgFnS	postavit
pevnost	pevnost	k1gFnSc4	pevnost
Fort	Fort	k?	Fort
Dearborn	Dearborna	k1gFnPc2	Dearborna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Chicago	Chicago	k1gNnSc1	Chicago
oficiálně	oficiálně	k6eAd1	oficiálně
založeno	založit	k5eAaPmNgNnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
strategické	strategický	k2eAgFnSc3d1	strategická
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
rozšiřující	rozšiřující	k2eAgFnSc3d1	rozšiřující
se	se	k3xPyFc4	se
železnici	železnice	k1gFnSc3	železnice
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
stalo	stát	k5eAaPmAgNnS	stát
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
Středozápadu	středozápad	k1gInSc2	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
velká	velký	k2eAgFnSc1d1	velká
menšina	menšina	k1gFnSc1	menšina
českých	český	k2eAgMnPc2d1	český
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
-	-	kIx~	-
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
odhadu	odhad	k1gInSc2	odhad
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Egona	Egon	k1gMnSc2	Egon
Salaby-Vojana	Salaby-Vojan	k1gMnSc2	Salaby-Vojan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
však	však	k8xC	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
až	až	k9	až
152	[number]	k4	152
960	[number]	k4	960
<g/>
)	)	kIx)	)
-	-	kIx~	-
což	což	k3yQnSc1	což
tehdy	tehdy	k6eAd1	tehdy
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
dělalo	dělat	k5eAaImAgNnS	dělat
město	město	k1gNnSc1	město
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
českou	český	k2eAgFnSc7d1	Česká
populací	populace	k1gFnSc7	populace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931-33	[number]	k4	1931-33
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
imigrant	imigrant	k1gMnSc1	imigrant
<g/>
,	,	kIx,	,
Srb	Srb	k1gMnSc1	Srb
Rod	rod	k1gInSc1	rod
Blagojevich	Blagojevich	k1gInSc4	Blagojevich
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
dokonce	dokonce	k9	dokonce
postu	posta	k1gFnSc4	posta
guvernéra	guvernér	k1gMnSc2	guvernér
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
strategické	strategický	k2eAgFnSc3d1	strategická
poloze	poloha	k1gFnSc3	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc4	veškerý
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
produkty	produkt	k1gInPc4	produkt
ze	z	k7c2	z
Středozápadu	středozápad	k1gInSc2	středozápad
a	a	k8xC	a
Velkých	velký	k2eAgFnPc2d1	velká
prérií	prérie	k1gFnPc2	prérie
se	se	k3xPyFc4	se
dovážejí	dovážet	k5eAaImIp3nP	dovážet
právě	právě	k9	právě
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velké	velký	k2eAgInPc1d1	velký
mlýny	mlýn	k1gInPc1	mlýn
a	a	k8xC	a
jatka	jatka	k1gFnSc1	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
daří	dařit	k5eAaImIp3nS	dařit
hlavně	hlavně	k6eAd1	hlavně
výrobnímu	výrobní	k2eAgInSc3d1	výrobní
průmyslu	průmysl	k1gInSc3	průmysl
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
průmysl	průmysl	k1gInSc4	průmysl
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
<g/>
,	,	kIx,	,
hutnický	hutnický	k2eAgInSc1d1	hutnický
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Hi-Tech	Hi-Tech	k1gInSc4	Hi-Tech
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
5	[number]	k4	5
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
výroben	výrobna	k1gFnPc2	výrobna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Společnost	společnost	k1gFnSc1	společnost
Boeing	boeing	k1gInSc1	boeing
ze	z	k7c2	z
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
je	být	k5eAaImIp3nS	být
i	i	k9	i
námořním	námořní	k2eAgInSc7d1	námořní
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velké	velký	k2eAgFnPc1d1	velká
lodě	loď	k1gFnPc1	loď
sem	sem	k6eAd1	sem
mohou	moct	k5eAaImIp3nP	moct
připlout	připlout	k5eAaPmF	připlout
vodní	vodní	k2eAgFnSc7d1	vodní
cestou	cesta	k1gFnSc7	cesta
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
až	až	k9	až
z	z	k7c2	z
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
Chicago	Chicago	k1gNnSc1	Chicago
spojené	spojený	k2eAgNnSc1d1	spojené
i	i	k9	i
s	s	k7c7	s
Mississippi	Mississippi	k1gFnSc7	Mississippi
několika	několik	k4yIc7	několik
průplavy	průplav	k1gInPc7	průplav
<g/>
;	;	kIx,	;
tudy	tudy	k6eAd1	tudy
se	se	k3xPyFc4	se
lodě	loď	k1gFnPc1	loď
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
a	a	k8xC	a
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
27	[number]	k4	27
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
vlaky	vlak	k1gInPc7	vlak
dovážejí	dovážet	k5eAaImIp3nP	dovážet
a	a	k8xC	a
vyvážejí	vyvážet	k5eAaImIp3nP	vyvážet
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
ocel	ocel	k1gFnSc4	ocel
a	a	k8xC	a
chemické	chemický	k2eAgInPc4d1	chemický
a	a	k8xC	a
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
jsou	být	k5eAaImIp3nP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Chicagoans	Chicagoans	k1gInSc4	Chicagoans
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
695	[number]	k4	695
598	[number]	k4	598
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
896	[number]	k4	896
016	[number]	k4	016
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
1	[number]	k4	1
061	[number]	k4	061
928	[number]	k4	928
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
632	[number]	k4	632
909	[number]	k4	909
rodin	rodina	k1gFnPc2	rodina
žijících	žijící	k2eAgFnPc2d1	žijící
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
jednu	jeden	k4xCgFnSc4	jeden
pětinu	pětina	k1gFnSc4	pětina
populace	populace	k1gFnSc2	populace
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc2	Illinois
a	a	k8xC	a
1	[number]	k4	1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
923	[number]	k4	923
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
mělo	mít	k5eAaImAgNnS	mít
Chicago	Chicago	k1gNnSc1	Chicago
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
kdy	kdy	k6eAd1	kdy
mělo	mít	k5eAaImAgNnS	mít
přes	přes	k7c4	přes
3,6	[number]	k4	3,6
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
lidí	člověk	k1gMnPc2	člověk
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
,	,	kIx,	,
až	až	k8xS	až
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
město	město	k1gNnSc1	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
značný	značný	k2eAgInSc4d1	značný
přírůstek	přírůstek	k1gInSc4	přírůstek
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
je	být	k5eAaImIp3nS	být
tavicím	tavicí	k2eAgInSc7d1	tavicí
kotlem	kotel	k1gInSc7	kotel
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
menšina	menšina	k1gFnSc1	menšina
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
Mexičané	Mexičan	k1gMnPc1	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
populaci	populace	k1gFnSc4	populace
Irských	irský	k2eAgMnPc2d1	irský
Američanů	Američan	k1gMnPc2	Američan
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Spoustu	spousta	k1gFnSc4	spousta
městských	městský	k2eAgMnPc2d1	městský
politiků	politik	k1gMnPc2	politik
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
právě	právě	k6eAd1	právě
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
velké	velký	k2eAgFnSc2d1	velká
irské	irský	k2eAgFnSc2d1	irská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
současného	současný	k2eAgMnSc2d1	současný
starosty	starosta	k1gMnSc2	starosta
Richarda	Richard	k1gMnSc2	Richard
M.	M.	kA	M.
Daleyho	Daley	k1gMnSc2	Daley
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
žije	žít	k5eAaImIp3nS	žít
největší	veliký	k2eAgFnSc1d3	veliký
polská	polský	k2eAgFnSc1d1	polská
populace	populace	k1gFnSc1	populace
mimo	mimo	k7c4	mimo
Varšavu	Varšava	k1gFnSc4	Varšava
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
srbské	srbský	k2eAgNnSc4d1	srbské
a	a	k8xC	a
lotyšské	lotyšský	k2eAgNnSc4d1	lotyšské
město	město	k1gNnSc4	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
řecké	řecký	k2eAgNnSc4d1	řecké
město	město	k1gNnSc4	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
velkou	velký	k2eAgFnSc7d1	velká
arabskou	arabský	k2eAgFnSc7d1	arabská
(	(	kIx(	(
<g/>
185	[number]	k4	185
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
populaci	populace	k1gFnSc3	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
žije	žít	k5eAaImIp3nS	žít
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
mexických	mexický	k2eAgMnPc2d1	mexický
Američanů	Američan	k1gMnPc2	Američan
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Chicaga	Chicago	k1gNnSc2	Chicago
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
Indo-Američany	Indo-Američan	k1gMnPc4	Indo-Američan
a	a	k8xC	a
pro	pro	k7c4	pro
Jiho-Asiaty	Jiho-Asiata	k1gFnPc4	Jiho-Asiata
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
jiho-asijskou	jihosijský	k2eAgFnSc4d1	jiho-asijský
populaci	populace	k1gFnSc4	populace
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
San	San	k1gFnSc2	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
21,7	[number]	k4	21,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
(	(	kIx(	(
<g/>
12,2	[number]	k4	12,2
%	%	kIx~	%
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
;	;	kIx,	;
5,0	[number]	k4	5,0
%	%	kIx~	%
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
3,9	[number]	k4	3,9
%	%	kIx~	%
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
45,0	[number]	k4	45,0
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
32,9	[number]	k4	32,9
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
5,5	[number]	k4	5,5
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
13,4	[number]	k4	13,4
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,7	[number]	k4	2,7
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
28,9	[number]	k4	28,9
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kriminalita	kriminalita	k1gFnSc1	kriminalita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
poloviční	poloviční	k2eAgMnSc1d1	poloviční
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
problémy	problém	k1gInPc4	problém
Chicaga	Chicago	k1gNnSc2	Chicago
patří	patřit	k5eAaImIp3nS	patřit
přítomnost	přítomnost	k1gFnSc1	přítomnost
chudinských	chudinský	k2eAgFnPc2d1	chudinská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
blízko	blízko	k7c2	blízko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Downtownu	Downtown	k1gInSc2	Downtown
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
čtvrtích	čtvrt	k1gFnPc6	čtvrt
strhávají	strhávat	k5eAaImIp3nP	strhávat
staré	starý	k2eAgFnPc1d1	stará
budovy	budova	k1gFnPc1	budova
a	a	k8xC	a
staví	stavit	k5eAaBmIp3nS	stavit
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
zejména	zejména	k9	zejména
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Downtownu	Downtown	k1gInSc2	Downtown
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
lety	léto	k1gNnPc7	léto
válčily	válčit	k5eAaImAgInP	válčit
drogové	drogový	k2eAgInPc1d1	drogový
gangy	gang	k1gInPc1	gang
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nP	stát
nové	nový	k2eAgInPc1d1	nový
rodinné	rodinný	k2eAgInPc1d1	rodinný
domy	dům	k1gInPc1	dům
a	a	k8xC	a
kriminalita	kriminalita	k1gFnSc1	kriminalita
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
odešla	odejít	k5eAaPmAgFnS	odejít
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
tradičně	tradičně	k6eAd1	tradičně
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
18.1	[number]	k4	18.1
<g/>
.2012	.2012	k4	.2012
měli	mít	k5eAaImAgMnP	mít
zdejší	zdejší	k2eAgMnPc1d1	zdejší
policisté	policista	k1gMnPc1	policista
konečně	konečně	k6eAd1	konečně
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
oslavě-asi	oslavěs	k1gMnPc1	oslavě-as
po	po	k7c6	po
roce	rok	k1gInSc6	rok
zde	zde	k6eAd1	zde
nebyl	být	k5eNaImAgMnS	být
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
nikdo	nikdo	k3yNnSc1	nikdo
zabit	zabit	k2eAgMnSc1d1	zabit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
letech	léto	k1gNnPc6	léto
<g/>
:	:	kIx,	:
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
927	[number]	k4	927
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
943	[number]	k4	943
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
855	[number]	k4	855
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
931	[number]	k4	931
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
828	[number]	k4	828
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
796	[number]	k4	796
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
761	[number]	k4	761
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
704	[number]	k4	704
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
643	[number]	k4	643
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
633	[number]	k4	633
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
667	[number]	k4	667
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
656	[number]	k4	656
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
601	[number]	k4	601
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
453	[number]	k4	453
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
451	[number]	k4	451
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
471	[number]	k4	471
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
448	[number]	k4	448
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
513	[number]	k4	513
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
459	[number]	k4	459
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
436	[number]	k4	436
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
435	[number]	k4	435
Novým	nový	k2eAgMnSc7d1	nový
starostou	starosta	k1gMnSc7	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
Rahm	Rahm	k1gMnSc1	Rahm
Israel	Israel	k1gMnSc1	Israel
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Michael	Michael	k1gMnSc1	Michael
Daley	Dalea	k1gFnSc2	Dalea
<g/>
,	,	kIx,	,
zastával	zastávat	k5eAaImAgMnS	zastávat
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
celých	celý	k2eAgNnPc2d1	celé
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Daley	Dalea	k1gFnSc2	Dalea
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
bývalého	bývalý	k2eAgMnSc2d1	bývalý
starosty	starosta	k1gMnSc2	starosta
Richarda	Richard	k1gMnSc2	Richard
J.	J.	kA	J.
Daleyho	Daley	k1gMnSc2	Daley
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
William	William	k1gInSc4	William
Daley	Dalea	k1gFnSc2	Dalea
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
prezidenta	prezident	k1gMnSc2	prezident
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
ministrem	ministr	k1gMnSc7	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Daleyových	Daleyová	k1gFnPc2	Daleyová
je	být	k5eAaImIp3nS	být
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvlivnější	vlivný	k2eAgInPc4d3	nejvlivnější
politické	politický	k2eAgInPc4d1	politický
klany	klan	k1gInPc4	klan
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Kennedyové	Kennedyus	k1gMnPc1	Kennedyus
se	se	k3xPyFc4	se
i	i	k9	i
Daleyové	Daleyus	k1gMnPc1	Daleyus
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
angažují	angažovat	k5eAaBmIp3nP	angažovat
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
si	se	k3xPyFc3	se
za	za	k7c2	za
starosty	starosta	k1gMnSc2	starosta
tradičně	tradičně	k6eAd1	tradičně
volí	volit	k5eAaImIp3nS	volit
demokraty	demokrat	k1gMnPc4	demokrat
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
ovládají	ovládat	k5eAaImIp3nP	ovládat
město	město	k1gNnSc4	město
nepřetržitě	přetržitě	k6eNd1	přetržitě
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osmdesát	osmdesát	k4xCc4	osmdesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
bývalý	bývalý	k2eAgMnSc1d1	bývalý
starosta	starosta	k1gMnSc1	starosta
Daley	Dalea	k1gFnSc2	Dalea
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
liberální	liberální	k2eAgMnPc4d1	liberální
politiky	politik	k1gMnPc4	politik
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgNnPc4d1	podporující
práva	právo	k1gNnPc4	právo
žen	žena	k1gFnPc2	žena
na	na	k7c4	na
potrat	potrat	k1gInSc4	potrat
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Daley	Dalea	k1gFnPc1	Dalea
ovšem	ovšem	k9	ovšem
tvrdě	tvrdě	k6eAd1	tvrdě
postupuje	postupovat	k5eAaImIp3nS	postupovat
proti	proti	k7c3	proti
kriminalitě	kriminalita	k1gFnSc3	kriminalita
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
ekologické	ekologický	k2eAgFnSc6d1	ekologická
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
anketě	anketa	k1gFnSc6	anketa
časopisů	časopis	k1gInPc2	časopis
Time	Time	k1gNnSc7	Time
a	a	k8xC	a
USA	USA	kA	USA
Today	Todaa	k1gFnSc2	Todaa
byl	být	k5eAaImAgInS	být
Daley	Daley	k1gInPc4	Daley
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
starostou	starosta	k1gMnSc7	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
starostů	starosta	k1gMnPc2	starosta
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Daley	Dalea	k1gFnSc2	Dalea
utkal	utkat	k5eAaPmAgMnS	utkat
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
soupeři	soupeř	k1gMnPc7	soupeř
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
republikánem	republikán	k1gMnSc7	republikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
období	období	k1gNnSc6	období
výrazného	výrazný	k2eAgNnSc2d1	výrazné
snížení	snížení	k1gNnSc2	snížení
kriminality	kriminalita	k1gFnSc2	kriminalita
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
populačního	populační	k2eAgInSc2d1	populační
přírůstku	přírůstek	k1gInSc2	přírůstek
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Daley	Dale	k1gMnPc4	Dale
ve	v	k7c6	v
výhodné	výhodný	k2eAgFnSc6d1	výhodná
pozici	pozice	k1gFnSc6	pozice
a	a	k8xC	a
komentátoři	komentátor	k1gMnPc1	komentátor
jej	on	k3xPp3gMnSc4	on
prohlašovali	prohlašovat	k5eAaImAgMnP	prohlašovat
za	za	k7c4	za
jistého	jistý	k2eAgMnSc4d1	jistý
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
pak	pak	k6eAd1	pak
Richard	Richard	k1gMnSc1	Richard
Daley	Dalea	k1gFnSc2	Dalea
získal	získat	k5eAaPmAgMnS	získat
mimořádných	mimořádný	k2eAgNnPc2d1	mimořádné
78	[number]	k4	78
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
odevzdány	odevzdat	k5eAaPmNgInP	odevzdat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
600	[number]	k4	600
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Chicago	Chicago	k1gNnSc1	Chicago
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
center	centrum	k1gNnPc2	centrum
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
univerzity	univerzita	k1gFnPc1	univerzita
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
;	;	kIx,	;
např.	např.	kA	např.
University	universita	k1gFnSc2	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Northwestern	Northwestern	k1gNnSc1	Northwestern
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
DePaul	DePaul	k1gInSc4	DePaul
University	universita	k1gFnSc2	universita
nebo	nebo	k8xC	nebo
univerzita	univerzita	k1gFnSc1	univerzita
Loyola	Loyola	k1gFnSc1	Loyola
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
4	[number]	k4	4
hlavní	hlavní	k2eAgInPc4d1	hlavní
regiony	region	k1gInPc4	region
<g/>
:	:	kIx,	:
North	Northa	k1gFnPc2	Northa
Side	Side	k1gInSc1	Side
<g/>
,	,	kIx,	,
South	South	k1gInSc1	South
Side	Side	k1gFnSc1	Side
<g/>
,	,	kIx,	,
Southwest	Southwest	k1gFnSc1	Southwest
Side	Side	k1gFnSc1	Side
a	a	k8xC	a
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Downtown	Downtown	k1gInSc1	Downtown
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c4	mezi
North	North	k1gInSc4	North
Side	Side	k1gFnPc2	Side
a	a	k8xC	a
South	Southa	k1gFnPc2	Southa
Side	Sid	k1gFnSc2	Sid
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Loop	Loop	k1gMnSc1	Loop
<g/>
,	,	kIx,	,
Near	Near	k1gMnSc1	Near
North	Northa	k1gFnPc2	Northa
Side	Side	k1gInSc1	Side
a	a	k8xC	a
Near	Near	k1gInSc1	Near
South	Southa	k1gFnPc2	Southa
Side	Sid	k1gInSc2	Sid
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
The	The	k1gMnSc1	The
Loop	Loop	k1gMnSc1	Loop
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chicago	Chicago	k1gNnSc1	Chicago
Loop	Loop	k1gInSc1	Loop
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgNnSc4d1	komerční
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
downtownu	downtown	k1gInSc2	downtown
<g/>
;	;	kIx,	;
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
budovy	budova	k1gFnSc2	budova
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Loop	Loop	k1gInSc1	Loop
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c4	mezi
Near	Near	k1gInSc4	Near
North	North	k1gInSc4	North
Side	Side	k1gNnPc2	Side
a	a	k8xC	a
Near	Neara	k1gFnPc2	Neara
South	Southa	k1gFnPc2	Southa
Side	Sid	k1gFnSc2	Sid
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
sem	sem	k6eAd1	sem
všechny	všechen	k3xTgFnPc1	všechen
linky	linka	k1gFnPc1	linka
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
velikém	veliký	k2eAgInSc6d1	veliký
nadzemním	nadzemní	k2eAgInSc6d1	nadzemní
okruhu	okruh	k1gInSc6	okruh
(	(	kIx(	(
<g/>
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
Loop	Loop	k1gInSc1	Loop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
North	North	k1gInSc1	North
Side	Sid	k1gFnSc2	Sid
je	být	k5eAaImIp3nS	být
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
částí	část	k1gFnSc7	část
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Chicago	Chicago	k1gNnSc1	Chicago
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
(	(	kIx(	(
<g/>
Near	Near	k1gMnSc1	Near
North	North	k1gMnSc1	North
<g/>
)	)	kIx)	)
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
downtownu	downtown	k1gInSc2	downtown
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgMnSc1d1	hlavní
"	"	kIx"	"
<g/>
tavicí	tavicí	k2eAgInSc1d1	tavicí
kotel	kotel	k1gInSc1	kotel
<g/>
"	"	kIx"	"
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
tu	tu	k6eAd1	tu
spousta	spousta	k1gFnSc1	spousta
německých	německý	k2eAgMnPc2d1	německý
<g/>
,	,	kIx,	,
švédských	švédský	k2eAgMnPc2d1	švédský
a	a	k8xC	a
polských	polský	k2eAgMnPc2d1	polský
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
různorodé	různorodý	k2eAgFnPc4d1	různorodá
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k6eAd1	kolem
Devon	devon	k1gInSc4	devon
Avenue	avenue	k1gFnSc2	avenue
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
Východu	východ	k1gInSc2	východ
a	a	k8xC	a
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
to	ten	k3xDgNnSc1	ten
množství	množství	k1gNnSc1	množství
typických	typický	k2eAgFnPc2d1	typická
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
populární	populární	k2eAgFnPc4d1	populární
oblasti	oblast	k1gFnPc4	oblast
jsou	být	k5eAaImIp3nP	být
Lincoln	Lincoln	k1gMnSc1	Lincoln
Park	park	k1gInSc1	park
a	a	k8xC	a
Lakeview	Lakeview	k1gFnSc1	Lakeview
<g/>
.	.	kIx.	.
</s>
<s>
River	River	k1gMnSc1	River
North	North	k1gMnSc1	North
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Chicago	Chicago	k1gNnSc1	Chicago
a	a	k8xC	a
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
rapidní	rapidní	k2eAgFnSc4d1	rapidní
přeměnu	přeměna	k1gFnSc4	přeměna
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
opuštěného	opuštěný	k2eAgInSc2d1	opuštěný
a	a	k8xC	a
skladištního	skladištní	k2eAgInSc2d1	skladištní
okrsku	okrsek	k1gInSc2	okrsek
<g/>
,	,	kIx,	,
na	na	k7c4	na
komerční	komerční	k2eAgFnSc4d1	komerční
<g/>
,	,	kIx,	,
obytnou	obytný	k2eAgFnSc4d1	obytná
a	a	k8xC	a
zábavní	zábavní	k2eAgFnSc4d1	zábavní
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
zastavěnou	zastavěný	k2eAgFnSc4d1	zastavěná
množstvím	množství	k1gNnSc7	množství
moderních	moderní	k2eAgInPc2d1	moderní
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
South	South	k1gInSc4	South
Side	Sid	k1gMnPc4	Sid
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgNnPc2d1	Evropské
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Irů	Ir	k1gMnPc2	Ir
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
Evropanů	Evropan	k1gMnPc2	Evropan
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
přišli	přijít	k5eAaPmAgMnP	přijít
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
Portoričané	Portoričan	k1gMnPc1	Portoričan
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
přišlo	přijít	k5eAaPmAgNnS	přijít
i	i	k9	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
Hispánců	Hispánec	k1gMnPc2	Hispánec
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
komplexy	komplex	k1gInPc1	komplex
a	a	k8xC	a
továrny	továrna	k1gFnPc1	továrna
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
zavřeny	zavřít	k5eAaPmNgFnP	zavřít
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
známých	známý	k2eAgFnPc2d1	známá
jatek	jatka	k1gFnPc2	jatka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
zaměstnávaly	zaměstnávat	k5eAaImAgFnP	zaměstnávat
přes	přes	k7c4	přes
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
však	však	k9	však
obydleno	obydlet	k5eAaPmNgNnS	obydlet
i	i	k8xC	i
střední	střední	k2eAgFnSc1d1	střední
třídou	třída	k1gFnSc7	třída
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
prosperují	prosperovat	k5eAaImIp3nP	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
z	z	k7c2	z
Chinatownu	Chinatown	k1gInSc2	Chinatown
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
místo	místo	k7c2	místo
východoasijské	východoasijský	k2eAgFnSc2d1	východoasijská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Hyde	Hyde	k6eAd1	Hyde
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
prestižní	prestižní	k2eAgFnSc2d1	prestižní
University	universita	k1gFnSc2	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
částech	část	k1gFnPc6	část
jako	jako	k8xS	jako
Woodlawn	Woodlawn	k1gNnSc4	Woodlawn
<g/>
,	,	kIx,	,
Bronzeville	Bronzeville	k1gNnSc4	Bronzeville
<g/>
,	,	kIx,	,
Bridgeport	Bridgeport	k1gInSc4	Bridgeport
a	a	k8xC	a
McKinley	McKinlea	k1gMnSc2	McKinlea
Park	park	k1gInSc1	park
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
zlepšení	zlepšení	k1gNnSc4	zlepšení
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
demolice	demolice	k1gFnSc1	demolice
starých	starý	k2eAgFnPc2d1	stará
polorozpadlých	polorozpadlý	k2eAgFnPc2d1	polorozpadlá
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nahrazovány	nahrazovat	k5eAaImNgFnP	nahrazovat
novými	nový	k2eAgFnPc7d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
lepší	dobrý	k2eAgNnSc1d2	lepší
prostředí	prostředí	k1gNnSc1	prostředí
předměstského	předměstský	k2eAgInSc2d1	předměstský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
South	South	k1gInSc4	South
Side	Side	k1gNnSc2	Side
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
částí	část	k1gFnPc2	část
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
Pullman	Pullman	k1gMnSc1	Pullman
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Chicaga	Chicago	k1gNnSc2	Chicago
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
převážně	převážně	k6eAd1	převážně
obytné	obytný	k2eAgFnPc4d1	obytná
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
spousta	spousta	k1gFnSc1	spousta
irských	irský	k2eAgMnPc2d1	irský
Američanů	Američan	k1gMnPc2	Američan
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
však	však	k9	však
přibývá	přibývat	k5eAaImIp3nS	přibývat
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
probíhá	probíhat	k5eAaImIp3nS	probíhat
velká	velký	k2eAgFnSc1d1	velká
slavnost	slavnost	k1gFnSc1	slavnost
na	na	k7c4	na
Den	den	k1gInSc4	den
Sv.	sv.	kA	sv.
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
letiště	letiště	k1gNnSc1	letiště
Midway	Midwaa	k1gFnSc2	Midwaa
International	International	k1gFnSc2	International
Airport	Airport	k1gInSc1	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
oblasti	oblast	k1gFnSc3	oblast
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dařilo	dařit	k5eAaImAgNnS	dařit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
velké	velký	k2eAgInPc1d1	velký
parky	park	k1gInPc1	park
<g/>
:	:	kIx,	:
Douglas	Douglas	k1gInSc1	Douglas
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Garfield	Garfield	k1gInSc1	Garfield
Park	park	k1gInSc1	park
a	a	k8xC	a
Humboldt	Humboldt	k2eAgInSc1d1	Humboldt
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
Chicaga	Chicago	k1gNnSc2	Chicago
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
domovem	domov	k1gInSc7	domov
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
hispánského	hispánský	k2eAgNnSc2d1	hispánské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
čtvrť	čtvrť	k1gFnSc1	čtvrť
založená	založený	k2eAgFnSc1d1	založená
českými	český	k2eAgMnPc7d1	český
imigranty	imigrant	k1gMnPc7	imigrant
-	-	kIx~	-
Pilsen	Pilsno	k1gNnPc2	Pilsno
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metro	metro	k1gNnSc4	metro
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
všech	všecek	k3xTgFnPc2	všecek
šest	šest	k4xCc1	šest
železničních	železniční	k2eAgFnPc2d1	železniční
drah	draha	k1gFnPc2	draha
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
Chicago	Chicago	k1gNnSc1	Chicago
Union	union	k1gInSc1	union
Station	station	k1gInSc1	station
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
dálkové	dálkový	k2eAgInPc4d1	dálkový
spoje	spoj	k1gInPc4	spoj
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Seattlu	Seattla	k1gMnSc4	Seattla
<g/>
,	,	kIx,	,
New	New	k1gMnSc4	New
Orleansu	Orleans	k1gInSc2	Orleans
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
kratší	krátký	k2eAgInPc1d2	kratší
spoje	spoj	k1gInPc1	spoj
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Illinois	Illinois	k1gFnSc7	Illinois
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
městu	město	k1gNnSc3	město
Milwaukee	Milwauke	k1gFnSc2	Milwauke
ve	v	k7c4	v
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
většinou	většinou	k6eAd1	většinou
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
společnost	společnost	k1gFnSc1	společnost
Amtrak	Amtrak	k1gInSc1	Amtrak
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgNnSc1d1	podzemní
nádraží	nádraží	k1gNnSc1	nádraží
Millennium	millennium	k1gNnSc4	millennium
Station	station	k1gInSc1	station
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
hlavně	hlavně	k9	hlavně
příměstské	příměstský	k2eAgInPc1d1	příměstský
spoje	spoj	k1gInPc1	spoj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
South	South	k1gInSc1	South
Shore	Shor	k1gInSc5	Shor
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
železnice	železnice	k1gFnSc1	železnice
vedoucí	vedoucí	k1gFnSc1	vedoucí
ze	z	k7c2	z
South	South	k1gInSc4	South
Bendu	Benda	k1gMnSc4	Benda
v	v	k7c6	v
Indianě	Indiana	k1gFnSc6	Indiana
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Michigan	Michigan	k1gInSc4	Michigan
City	City	k1gFnSc2	City
a	a	k8xC	a
Gary	Gara	k1gFnSc2	Gara
až	až	k9	až
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
hlavně	hlavně	k9	hlavně
společnosti	společnost	k1gFnPc1	společnost
Metra	metr	k1gMnSc2	metr
a	a	k8xC	a
NICTD	NICTD	kA	NICTD
(	(	kIx(	(
<g/>
South	South	k1gMnSc1	South
Shore	Shor	k1gInSc5	Shor
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
Chicagem	Chicago	k1gNnSc7	Chicago
vede	vést	k5eAaImIp3nS	vést
spousta	spousta	k1gFnSc1	spousta
několikaproudových	několikaproudový	k2eAgFnPc2d1	několikaproudová
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
bulvárů	bulvár	k1gInPc2	bulvár
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
šířce	šířka	k1gFnSc3	šířka
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
centru	centrum	k1gNnSc3	centrum
města	město	k1gNnSc2	město
dopravní	dopravní	k2eAgFnSc2d1	dopravní
zácpy	zácpa	k1gFnSc2	zácpa
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
tepnou	tepna	k1gFnSc7	tepna
je	být	k5eAaImIp3nS	být
Michigan	Michigan	k1gInSc1	Michigan
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
u	u	k7c2	u
Lincoln	Lincoln	k1gMnSc1	Lincoln
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
jih	jih	k1gInSc4	jih
kolem	kolem	k7c2	kolem
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
parků	park	k1gInPc2	park
Millenium	Millenium	k1gNnSc1	Millenium
a	a	k8xC	a
Grant	grant	k1gInSc1	grant
vede	vést	k5eAaImIp3nS	vést
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
důležitá	důležitý	k2eAgFnSc1d1	důležitá
tepna	tepna	k1gFnSc1	tepna
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Lake	Lake	k1gFnSc1	Lake
Shore	Shor	k1gInSc5	Shor
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
podél	podél	k7c2	podél
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
protíná	protínat	k5eAaImIp3nS	protínat
7	[number]	k4	7
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc1	jméno
známých	známý	k2eAgMnPc2d1	známý
politiků	politik	k1gMnPc2	politik
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
dálnice	dálnice	k1gFnSc1	dálnice
I-90	I-90	k1gMnSc2	I-90
The	The	k1gMnSc2	The
Kennedy	Kenneda	k1gMnSc2	Kenneda
Expressway	Expresswaa	k1gMnSc2	Expresswaa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
v	v	k7c6	v
přilehlých	přilehlý	k2eAgNnPc6d1	přilehlé
předměstích	předměstí	k1gNnPc6	předměstí
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Chicago	Chicago	k1gNnSc1	Chicago
Transit	transit	k1gInSc4	transit
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
CTA	CTA	kA	CTA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
síť	síť	k1gFnSc1	síť
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
systém	systém	k1gInSc4	systém
podzemní	podzemní	k2eAgFnPc1d1	podzemní
<g/>
/	/	kIx~	/
<g/>
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
elevated	elevated	k1gInSc4	elevated
-	-	kIx~	-
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
soupravu	souprava	k1gFnSc4	souprava
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chvíli	chvíle	k1gFnSc4	chvíle
jede	jet	k5eAaImIp3nS	jet
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
říká	říkat	k5eAaImIp3nS	říkat
metro	metro	k1gNnSc1	metro
-	-	kIx~	-
subway	subwaa	k1gFnPc1	subwaa
<g/>
)	)	kIx)	)
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
nadzemka	nadzemka	k1gFnSc1	nadzemka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
je	být	k5eAaImIp3nS	být
však	však	k9	však
chicagské	chicagský	k2eAgNnSc1d1	Chicagské
metro	metro	k1gNnSc1	metro
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
13	[number]	k4	13
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgInSc1d1	nadzemní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1858	[number]	k4	1858
-	-	kIx~	-
1958	[number]	k4	1958
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
síť	síť	k1gFnSc4	síť
(	(	kIx(	(
<g/>
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vrcholu	vrchol	k1gInSc6	vrchol
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1930	[number]	k4	1930
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
trolejbusový	trolejbusový	k2eAgInSc1d1	trolejbusový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
skvělé	skvělý	k2eAgNnSc1d1	skvělé
letecké	letecký	k2eAgNnSc1d1	letecké
spojení	spojení	k1gNnSc1	spojení
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
11	[number]	k4	11
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
přístavů	přístav	k1gInPc2	přístav
-	-	kIx~	-
letiště	letiště	k1gNnSc1	letiště
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Hare	Hare	k1gFnSc6	Hare
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
po	po	k7c6	po
Atlantě	Atlanta	k1gFnSc6	Atlanta
a	a	k8xC	a
Pekingu	Peking	k1gInSc6	Peking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kapela	kapela	k1gFnSc1	kapela
Smashing	Smashing	k1gInSc4	Smashing
Pumpkins	Pumpkins	k1gInSc1	Pumpkins
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
)	)	kIx)	)
-	-	kIx~	-
Chicago	Chicago	k1gNnSc1	Chicago
Bulls	Bullsa	k1gFnPc2	Bullsa
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
NFL	NFL	kA	NFL
<g/>
)	)	kIx)	)
-	-	kIx~	-
Chicago	Chicago	k1gNnSc1	Chicago
Bears	Bearsa	k1gFnPc2	Bearsa
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
-	-	kIx~	-
Chicago	Chicago	k1gNnSc1	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
Baseball	baseball	k1gInSc1	baseball
(	(	kIx(	(
<g/>
MLB	MLB	kA	MLB
<g/>
)	)	kIx)	)
Národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
Chicago	Chicago	k1gNnSc1	Chicago
Cubs	Cubsa	k1gFnPc2	Cubsa
Americká	americký	k2eAgFnSc1d1	americká
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
Chicago	Chicago	k1gNnSc1	Chicago
White	Whit	k1gInSc5	Whit
Sox	Sox	k1gMnSc1	Sox
Fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
MLS	mls	k1gInSc1	mls
<g/>
)	)	kIx)	)
-	-	kIx~	-
Chicago	Chicago	k1gNnSc4	Chicago
Fire	Fir	k1gFnSc2	Fir
Chicagský	chicagský	k2eAgInSc1d1	chicagský
maraton	maraton	k1gInSc1	maraton
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
Grantham	Grantham	k1gInSc1	Grantham
Bain	Baina	k1gFnPc2	Baina
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Rice	k1gFnSc4	Rice
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Lincoln	Lincoln	k1gMnSc1	Lincoln
Ellsworth	Ellsworth	k1gMnSc1	Ellsworth
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
Leonard	Leonard	k1gMnSc1	Leonard
Bloomfield	Bloomfield	k1gMnSc1	Bloomfield
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
Raymond	Raymond	k1gMnSc1	Raymond
Chandler	Chandler	k1gMnSc1	Chandler
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
Ray	Ray	k1gFnSc1	Ray
Kroc	Kroc	k1gFnSc1	Kroc
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
vlastník	vlastník	k1gMnSc1	vlastník
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Hugh	Hugh	k1gMnSc1	Hugh
Hefner	Hefner	k1gMnSc1	Hefner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
časopisu	časopis	k1gInSc2	časopis
Playboy	playboy	k1gMnSc1	playboy
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Markowitz	Markowitza	k1gFnPc2	Markowitza
(	(	kIx(	(
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
V.	V.	kA	V.
Polc	Polc	k1gFnSc1	Polc
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
Anton	Anton	k1gMnSc1	Anton
Szandor	Szandor	k1gMnSc1	Szandor
LaVey	LaVea	k1gFnSc2	LaVea
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satanista	satanista	k1gMnSc1	satanista
James	James	k1gMnSc1	James
Watson	Watson	k1gMnSc1	Watson
Cronin	Cronin	k2eAgMnSc1d1	Cronin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
John	John	k1gMnSc1	John
Jakes	Jakes	k1gMnSc1	Jakes
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Eugene	Eugen	k1gInSc5	Eugen
Cernan	Cernan	k1gInSc4	Cernan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
původu	původ	k1gInSc2	původ
Walter	Walter	k1gMnSc1	Walter
Koenig	Koenig	k1gMnSc1	Koenig
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gFnSc2	Mattingla
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Ray	Ray	k1gMnSc1	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
Herbie	Herbie	k1gFnSc1	Herbie
Hancock	Hancock	k1gMnSc1	Hancock
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Ronald	Ronald	k1gMnSc1	Ronald
Gene	gen	k1gInSc5	gen
Simmons	Simmonsa	k1gFnPc2	Simmonsa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
veterán	veterán	k1gMnSc1	veterán
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
John	John	k1gMnSc1	John
Wayne	Wayn	k1gInSc5	Wayn
Gacy	Gaca	k1gMnSc2	Gaca
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
Paul	Paul	k1gMnSc1	Paul
Butterfield	Butterfield	k1gMnSc1	Butterfield
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Harrison	Harrison	k1gMnSc1	Harrison
Ford	ford	k1gInSc1	ford
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Michael	Michael	k1gMnSc1	Michael
Crichton	Crichton	k1gInSc1	Crichton
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Theodore	Theodor	k1gMnSc5	Theodor
Kaczynski	Kaczynski	k1gNnPc7	Kaczynski
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
kritik	kritik	k1gMnSc1	kritik
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
anarchista	anarchista	k1gMnSc1	anarchista
Patti	Patť	k1gFnSc2	Patť
Smithová	Smithová	k1gFnSc1	Smithová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Hillary	Hillara	k1gFnSc2	Hillara
Clintonová	Clintonová	k1gFnSc1	Clintonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
Timothy	Timotha	k1gFnSc2	Timotha
Zahn	Zahna	k1gFnPc2	Zahna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robin	robin	k2eAgInSc4d1	robin
Williams	Williams	k1gInSc4	Williams
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Rod	rod	k1gInSc4	rod
Blagojevich	Blagojevich	k1gInSc4	Blagojevich
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
Illinois	Illinois	k1gFnSc2	Illinois
Dan	Dan	k1gMnSc1	Dan
Castellaneta	Castellaneta	k1gFnSc1	Castellaneta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dabér	dabér	k1gMnSc1	dabér
John	John	k1gMnSc1	John
Grunsfeld	Grunsfeld	k1gMnSc1	Grunsfeld
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Daryl	Daryl	k1gInSc1	Daryl
Hannah	Hannah	k1gInSc4	Hannah
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Virginia	Virginium	k1gNnSc2	Virginium
Madsen	Madsna	k1gFnPc2	Madsna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Chris	Chris	k1gFnSc2	Chris
Chelios	Cheliosa	k1gFnPc2	Cheliosa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Warren	Warrna	k1gFnPc2	Warrna
DeMartini	DeMartin	k2eAgMnPc1d1	DeMartin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Michelle	Michelle	k1gFnSc2	Michelle
Obamová	Obamová	k1gFnSc1	Obamová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
první	první	k4xOgMnSc1	první
<g />
.	.	kIx.	.
</s>
<s>
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
Lana	lano	k1gNnSc2	lano
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
Andrea	Andrea	k1gFnSc1	Andrea
Jaegerová	Jaegerová	k1gFnSc1	Jaegerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
R.	R.	kA	R.
Kelly	Kella	k1gFnSc2	Kella
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
James	James	k1gMnSc1	James
Iha	iha	k0	iha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g />
.	.	kIx.	.
</s>
<s>
Smashing	Smashing	k1gInSc1	Smashing
Pumpkins	Pumpkinsa	k1gFnPc2	Pumpkinsa
Andy	Anda	k1gFnSc2	Anda
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Kanye	Kany	k1gFnSc2	Kany
West	West	k1gMnSc1	West
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
Philip	Philip	k1gMnSc1	Philip
Jack	Jack	k1gMnSc1	Jack
Brooks	Brooksa	k1gFnPc2	Brooksa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgInSc4d1	profesionální
wrestler	wrestler	k1gInSc4	wrestler
Laura	Laura	k1gFnSc1	Laura
Granvilleová	Granvilleová	k1gFnSc1	Granvilleová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
<g />
.	.	kIx.	.
</s>
<s>
Jennifer	Jennifer	k1gMnSc1	Jennifer
Hudson	Hudson	k1gMnSc1	Hudson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Wallace	Wallace	k1gFnSc2	Wallace
Spearmon	Spearmona	k1gFnPc2	Spearmona
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
Evan	Evana	k1gFnPc2	Evana
Lysacek	Lysacka	k1gFnPc2	Lysacka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Accra	Accra	k1gFnSc1	Accra
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Ammán	Ammán	k1gInSc1	Ammán
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Atény	Atény	k1gFnPc4	Atény
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Birmingham	Birmingham	k1gInSc1	Birmingham
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Bogotá	Bogotá	k1gFnPc2	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Dillí	Dillí	k1gNnSc2	Dillí
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
Durban	Durban	k1gInSc1	Durban
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Galway	Galway	k1gInPc1	Galway
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Göteborg	Göteborg	k1gInSc1	Göteborg
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Láhaur	Láhaur	k1gInSc1	Láhaur
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Lucern	Lucerna	k1gFnPc2	Lucerna
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1998	[number]	k4	1998
Miláno	Milán	k2eAgNnSc1d1	Miláno
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Petach	Petacha	k1gFnPc2	Petacha
Tikva	Tikvo	k1gNnSc2	Tikvo
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Pusan	Pusana	k1gFnPc2	Pusana
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Sã	Sã	k1gFnPc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc5	Brazílie
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Šen-jang	Šenanga	k1gFnPc2	Šen-janga
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
</s>
