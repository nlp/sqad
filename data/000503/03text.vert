<s>
Pluto	plut	k2eAgNnSc4d1	Pluto
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
134340	[number]	k4	134340
<g/>
)	)	kIx)	)
Pluto	Pluto	k1gNnSc4	Pluto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
po	po	k7c6	po
Eris	Eris	k1gFnSc1	Eris
druhou	druhý	k4xOgFnSc7	druhý
nejhmotnější	hmotný	k2eAgFnSc7d3	nejhmotnější
známou	známý	k2eAgFnSc7d1	známá
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
deváté	devátý	k4xOgNnSc4	devátý
největší	veliký	k2eAgNnSc4d3	veliký
a	a	k8xC	a
desáté	desátá	k1gFnSc2	desátá
nejhmotnější	hmotný	k2eAgNnSc4d3	nejhmotnější
známé	známý	k2eAgNnSc4d1	známé
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
přímo	přímo	k6eAd1	přímo
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
ho	on	k3xPp3gNnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Clyde	Clyd	k1gInSc5	Clyd
Tombaugh	Tombaugh	k1gInSc1	Tombaugh
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
kosmické	kosmický	k2eAgNnSc4d1	kosmické
těleso	těleso	k1gNnSc4	těleso
astronomové	astronom	k1gMnPc1	astronom
původně	původně	k6eAd1	původně
řadili	řadit	k5eAaImAgMnP	řadit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
definice	definice	k1gFnSc2	definice
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
26	[number]	k4	26
<g/>
.	.	kIx.	.
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
plutoidy	plutoida	k1gFnPc4	plutoida
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
i	i	k9	i
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc4d1	malé
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
pětinu	pětina	k1gFnSc4	pětina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
třetinu	třetina	k1gFnSc4	třetina
jeho	jeho	k3xOp3gInSc2	jeho
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
vysoce	vysoce	k6eAd1	vysoce
výstřední	výstřední	k2eAgFnSc6d1	výstřední
a	a	k8xC	a
nakloněné	nakloněný	k2eAgFnSc6d1	nakloněná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
30	[number]	k4	30
a	a	k8xC	a
49	[number]	k4	49
astronomickými	astronomický	k2eAgFnPc7d1	astronomická
jednotkami	jednotka	k1gFnPc7	jednotka
(	(	kIx(	(
<g/>
AU	au	k0	au
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
4,4	[number]	k4	4,4
<g/>
-	-	kIx~	-
<g/>
7,4	[number]	k4	7,4
miliard	miliarda	k4xCgFnPc2	miliarda
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitá	ocitat	k5eAaImIp3nS	ocitat
blíže	blízce	k6eAd2	blízce
Slunci	slunce	k1gNnSc3	slunce
než	než	k8xS	než
poslední	poslední	k2eAgFnSc1d1	poslední
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
32	[number]	k4	32
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gNnSc1	Pluto
bylo	být	k5eAaImAgNnS	být
klasifikováno	klasifikovat	k5eAaImNgNnS	klasifikovat
jako	jako	k9	jako
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
objevu	objev	k1gInSc2	objev
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
status	status	k1gInSc1	status
však	však	k9	však
byl	být	k5eAaImAgInS	být
zpochybňován	zpochybňovat	k5eAaImNgInS	zpochybňovat
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
2060	[number]	k4	2060
<g/>
)	)	kIx)	)
Chiron	Chiron	k1gInSc1	Chiron
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
také	také	k9	také
astronomové	astronom	k1gMnPc1	astronom
přesněji	přesně	k6eAd2	přesně
poznali	poznat	k5eAaPmAgMnP	poznat
jeho	jeho	k3xOp3gFnSc4	jeho
malou	malý	k2eAgFnSc4d1	malá
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
mnoho	mnoho	k4c1	mnoho
transneptunických	transneptunický	k2eAgInPc2d1	transneptunický
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
těleso	těleso	k1gNnSc4	těleso
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
27	[number]	k4	27
%	%	kIx~	%
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
astronomickém	astronomický	k2eAgInSc6d1	astronomický
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
přijala	přijmout	k5eAaPmAgFnS	přijmout
novou	nový	k2eAgFnSc4d1	nová
definici	definice	k1gFnSc4	definice
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Pluto	plut	k2eAgNnSc1d1	Pluto
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
trpasličí	trpasličí	k2eAgFnPc1d1	trpasličí
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vedle	vedle	k7c2	vedle
Pluta	Pluto	k1gNnSc2	Pluto
zařadila	zařadit	k5eAaPmAgFnS	zařadit
též	též	k9	též
Eris	Eris	k1gFnSc1	Eris
a	a	k8xC	a
těleso	těleso	k1gNnSc1	těleso
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
Ceres	ceres	k1gInSc1	ceres
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
Pluto	Pluto	k1gNnSc1	Pluto
také	také	k9	také
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
katalogovým	katalogový	k2eAgNnSc7d1	Katalogové
číslem	číslo	k1gNnSc7	číslo
134340	[number]	k4	134340
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
astronomové	astronom	k1gMnPc1	astronom
stále	stále	k6eAd1	stále
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
patřit	patřit	k5eAaImF	patřit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
Charon	Charon	k1gMnSc1	Charon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
Nix	Nix	k1gFnSc1	Nix
a	a	k8xC	a
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
až	až	k9	až
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
malé	malý	k2eAgInPc4d1	malý
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
Kerberos	Kerberos	k1gMnSc1	Kerberos
a	a	k8xC	a
Styx	Styx	k1gInSc1	Styx
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hmotný	hmotný	k2eAgInSc1d1	hmotný
střed	střed	k1gInSc1	střed
soustavy	soustava	k1gFnSc2	soustava
Pluto	Pluto	k1gMnSc1	Pluto
–	–	k?	–
Charon	Charon	k1gMnSc1	Charon
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
tělesy	těleso	k1gNnPc7	těleso
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
někdy	někdy	k6eAd1	někdy
jako	jako	k8xC	jako
o	o	k7c6	o
binárním	binární	k2eAgNnSc6d1	binární
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
IAU	IAU	kA	IAU
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
formální	formální	k2eAgFnSc4d1	formální
definici	definice	k1gFnSc4	definice
binární	binární	k2eAgFnSc2d1	binární
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Charon	Charon	k1gMnSc1	Charon
oficiálně	oficiálně	k6eAd1	oficiálně
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
jako	jako	k8xC	jako
satelit	satelit	k1gMnSc1	satelit
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
Urbain	Urbain	k1gMnSc1	Urbain
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
nepravidelnosti	nepravidelnost	k1gFnSc2	nepravidelnost
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
zákonů	zákon	k1gInPc2	zákon
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
pozici	pozice	k1gFnSc4	pozice
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
neobjevené	objevený	k2eNgFnSc2d1	neobjevená
planety	planeta	k1gFnSc2	planeta
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
astronomové	astronom	k1gMnPc1	astronom
opět	opět	k6eAd1	opět
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
drobné	drobný	k2eAgFnPc4d1	drobná
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
Uranovy	Uranův	k2eAgFnSc2d1	Uranova
vypočítané	vypočítaný	k2eAgFnSc2d1	vypočítaná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znovu	znovu	k6eAd1	znovu
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oběhu	oběh	k1gInSc6	oběh
rušen	rušit	k5eAaImNgInS	rušit
ještě	ještě	k9	ještě
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
neznámou	známý	k2eNgFnSc7d1	neznámá
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Percival	Percivat	k5eAaImAgMnS	Percivat
Lowell	Lowell	k1gMnSc1	Lowell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
bohatý	bohatý	k2eAgMnSc1d1	bohatý
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
založil	založit	k5eAaPmAgMnS	založit
Lowellovu	Lowellův	k2eAgFnSc4d1	Lowellova
observatoř	observatoř	k1gFnSc4	observatoř
ve	v	k7c6	v
Flagstaffu	Flagstaff	k1gInSc6	Flagstaff
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
projektu	projekt	k1gInSc6	projekt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
najít	najít	k5eAaPmF	najít
případnou	případný	k2eAgFnSc4d1	případná
devátou	devátý	k4xOgFnSc4	devátý
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
Planeta	planeta	k1gFnSc1	planeta
X	X	kA	X
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
už	už	k6eAd1	už
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
astronomem	astronom	k1gMnSc7	astronom
Williamem	William	k1gInSc7	William
H.	H.	kA	H.
Pickeringem	Pickering	k1gInSc7	Pickering
vytipováno	vytipován	k2eAgNnSc4d1	vytipováno
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
několikero	několikero	k4xRyIgNnSc4	několikero
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
nacházet	nacházet	k5eAaImF	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
přesto	přesto	k8xC	přesto
nepřinášelo	přinášet	k5eNaImAgNnS	přinášet
žádné	žádný	k3yNgInPc4	žádný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Lowell	Lowell	k1gInSc1	Lowell
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
přesto	přesto	k8xC	přesto
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Lowell	Lowell	k1gMnSc1	Lowell
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
jeho	jeho	k3xOp3gFnSc4	jeho
observatoř	observatoř	k1gFnSc4	observatoř
pořídila	pořídit	k5eAaPmAgFnS	pořídit
dva	dva	k4xCgInPc4	dva
matné	matný	k2eAgInPc4d1	matný
snímky	snímek	k1gInPc4	snímek
s	s	k7c7	s
Plutem	plut	k1gInSc7	plut
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
těleso	těleso	k1gNnSc1	těleso
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nerozpoznáno	rozpoznat	k5eNaPmNgNnS	rozpoznat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Lowell	Lowell	k1gInSc1	Lowell
nebyl	být	k5eNaImAgInS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
takového	takový	k3xDgNnSc2	takový
nevědomky	nevědomky	k6eAd1	nevědomky
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc4d1	známo
16	[number]	k4	16
předobjevových	předobjevův	k2eAgFnPc2d1	předobjevův
fotografií	fotografia	k1gFnPc2	fotografia
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
na	na	k7c6	na
Yerkesově	Yerkesův	k2eAgFnSc6d1	Yerkesův
observatoři	observatoř	k1gFnSc6	observatoř
ve	v	k7c4	v
Williams	Williams	k1gInSc4	Williams
Bay	Bay	k1gFnSc2	Bay
ve	v	k7c4	v
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Lowellově	Lowellův	k2eAgFnSc6d1	Lowellova
smrti	smrt	k1gFnSc6	smrt
následovala	následovat	k5eAaImAgFnS	následovat
desetiletá	desetiletý	k2eAgFnSc1d1	desetiletá
právní	právní	k2eAgFnSc1d1	právní
bitva	bitva	k1gFnSc1	bitva
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
vdovou	vdova	k1gFnSc7	vdova
<g/>
,	,	kIx,	,
Constance	Constanka	k1gFnSc3	Constanka
Lowellovou	Lowellová	k1gFnSc4	Lowellová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
vůli	vůle	k1gFnSc4	vůle
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
observatoři	observatoř	k1gFnSc6	observatoř
odkázal	odkázat	k5eAaPmAgMnS	odkázat
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
této	tento	k3xDgFnSc2	tento
částky	částka	k1gFnSc2	částka
však	však	k9	však
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
soudní	soudní	k2eAgFnPc4d1	soudní
výlohy	výloha	k1gFnPc4	výloha
a	a	k8xC	a
hledání	hledání	k1gNnSc4	hledání
Planety	planeta	k1gFnSc2	planeta
X	X	kA	X
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
znovu	znovu	k6eAd1	znovu
rozběhlo	rozběhnout	k5eAaPmAgNnS	rozběhnout
až	až	k9	až
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
Clyde	Clyd	k1gInSc5	Clyd
Tombaugh	Tombaugh	k1gMnSc1	Tombaugh
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
pozval	pozvat	k5eAaPmAgMnS	pozvat
ředitel	ředitel	k1gMnSc1	ředitel
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Vesto	vesta	k1gFnSc5	vesta
Melvin	Melvina	k1gFnPc2	Melvina
Slipher	Sliphra	k1gFnPc2	Sliphra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc3	jehož
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
Tombaughovy	Tombaughův	k2eAgInPc1d1	Tombaughův
precizní	precizní	k2eAgInPc1d1	precizní
astronomické	astronomický	k2eAgInPc1d1	astronomický
náčrty	náčrt	k1gInPc1	náčrt
<g/>
.	.	kIx.	.
</s>
<s>
Tombaughovým	Tombaughův	k2eAgInSc7d1	Tombaughův
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
systematické	systematický	k2eAgNnSc1d1	systematické
fotografování	fotografování	k1gNnSc1	fotografování
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
snímek	snímek	k1gInSc1	snímek
pořídil	pořídit	k5eAaPmAgInS	pořídit
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
časovém	časový	k2eAgNnSc6d1	časové
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnSc1	dvojice
snímků	snímek	k1gInPc2	snímek
porovnávaly	porovnávat	k5eAaImAgFnP	porovnávat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
nějaký	nějaký	k3yIgInSc1	nějaký
objekt	objekt	k1gInSc1	objekt
nezměnil	změnit	k5eNaPmAgInS	změnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přístroji	přístroj	k1gInSc3	přístroj
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
blink	blink	k0	blink
komparátor	komparátor	k1gInSc1	komparátor
mohl	moct	k5eAaImAgInS	moct
při	při	k7c6	při
prohlížení	prohlížení	k1gNnSc6	prohlížení
fotografických	fotografický	k2eAgFnPc2d1	fotografická
desek	deska	k1gFnPc2	deska
rychle	rychle	k6eAd1	rychle
přecházet	přecházet	k5eAaImF	přecházet
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
u	u	k7c2	u
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
změnily	změnit	k5eAaPmAgFnP	změnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
iluze	iluze	k1gFnSc1	iluze
jejich	jejich	k3xOp3gInSc2	jejich
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
roce	rok	k1gInSc6	rok
hledání	hledání	k1gNnSc2	hledání
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
Tombaugh	Tombaugh	k1gMnSc1	Tombaugh
nalezl	nalézt	k5eAaBmAgMnS	nalézt
pohybující	pohybující	k2eAgNnSc4d1	pohybující
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc4	těleso
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
pořízených	pořízený	k2eAgFnPc2d1	pořízená
23	[number]	k4	23
<g/>
.	.	kIx.	.
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
fotografie	fotografie	k1gFnSc1	fotografie
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
také	také	k6eAd1	také
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
objev	objev	k1gInSc1	objev
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
ještě	ještě	k9	ještě
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
snímcích	snímek	k1gInPc6	snímek
<g/>
,	,	kIx,	,
zaslala	zaslat	k5eAaPmAgFnS	zaslat
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1930	[number]	k4	1930
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
zprávu	zpráva	k1gFnSc4	zpráva
do	do	k7c2	do
Harvard	Harvard	k1gInSc1	Harvard
College	Colleg	k1gMnPc4	Colleg
Observatory	Observator	k1gInPc7	Observator
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
Pluta	Pluto	k1gNnSc2	Pluto
měl	mít	k5eAaImAgInS	mít
obrovský	obrovský	k2eAgInSc1d1	obrovský
ohlas	ohlas	k1gInSc1	ohlas
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lowellova	Lowellův	k2eAgFnSc1d1	Lowellova
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
právo	práv	k2eAgNnSc4d1	právo
těleso	těleso	k1gNnSc4	těleso
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
,	,	kIx,	,
obdržela	obdržet	k5eAaPmAgFnS	obdržet
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
nesčetně	sčetně	k6eNd1	sčetně
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
mj.	mj.	kA	mj.
jména	jméno	k1gNnSc2	jméno
jako	jako	k8xC	jako
Odin	Odina	k1gFnPc2	Odina
<g/>
,	,	kIx,	,
Persephone	Persephon	k1gMnSc5	Persephon
<g/>
,	,	kIx,	,
Erebos	Erebos	k1gInSc1	Erebos
<g/>
,	,	kIx,	,
Atlas	Atlas	k1gInSc1	Atlas
či	či	k8xC	či
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
.	.	kIx.	.
</s>
<s>
Constance	Constance	k1gFnSc1	Constance
Lowellová	Lowellová	k1gFnSc1	Lowellová
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
Zeus	Zeus	k1gInSc4	Zeus
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Percival	Percival	k1gFnSc1	Percival
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
manželovi	manžel	k1gMnSc6	manžel
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
Constance	Constance	k1gFnSc1	Constance
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
zamítnuty	zamítnut	k2eAgInPc4d1	zamítnut
<g/>
,	,	kIx,	,
a	a	k8xC	a
astronomové	astronom	k1gMnPc1	astronom
Lowellovy	Lowellův	k2eAgFnSc2d1	Lowellova
observatoře	observatoř	k1gFnSc2	observatoř
nakonec	nakonec	k6eAd1	nakonec
volili	volit	k5eAaImAgMnP	volit
hlasováním	hlasování	k1gNnSc7	hlasování
z	z	k7c2	z
užšího	úzký	k2eAgInSc2d2	užší
okruhu	okruh	k1gInSc2	okruh
pouhých	pouhý	k2eAgInPc2d1	pouhý
tří	tři	k4xCgInPc2	tři
návrhů	návrh	k1gInPc2	návrh
<g/>
:	:	kIx,	:
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
bohyně	bohyně	k1gFnSc1	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
Cronus	Cronus	k1gInSc4	Cronus
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc4d1	anglický
přepis	přepis	k1gInSc4	přepis
jména	jméno	k1gNnSc2	jméno
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
Titánů	Titán	k1gMnPc2	Titán
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
bůh	bůh	k1gMnSc1	bůh
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
uvedené	uvedený	k2eAgNnSc4d1	uvedené
jméno	jméno	k1gNnSc4	jméno
však	však	k9	však
již	již	k6eAd1	již
patřilo	patřit	k5eAaImAgNnS	patřit
planetce	planetka	k1gFnSc3	planetka
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
)	)	kIx)	)
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
nakonec	nakonec	k6eAd1	nakonec
astronomové	astronom	k1gMnPc1	astronom
jednomyslně	jednomyslně	k6eAd1	jednomyslně
vybrali	vybrat	k5eAaPmAgMnP	vybrat
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
jméno	jméno	k1gNnSc4	jméno
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
jedenáctiletá	jedenáctiletý	k2eAgFnSc1d1	jedenáctiletá
školačka	školačka	k1gFnSc1	školačka
Venetie	Venetie	k1gFnSc1	Venetie
Burneyová	Burneyová	k1gFnSc1	Burneyová
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnPc4	veřejnost
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
oznámen	oznámen	k2eAgInSc1d1	oznámen
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
jména	jméno	k1gNnSc2	jméno
sehrál	sehrát	k5eAaPmAgInS	sehrát
také	také	k9	také
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
počáteční	počáteční	k2eAgInSc1d1	počáteční
dvě	dva	k4xCgNnPc1	dva
písmena	písmeno	k1gNnPc1	písmeno
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
iniciálami	iniciála	k1gFnPc7	iniciála
Percivala	Percivala	k1gFnSc2	Percivala
Lowella	Lowello	k1gNnSc2	Lowello
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
Pluta	Pluto	k1gNnSc2	Pluto
(	(	kIx(	(
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
složením	složení	k1gNnSc7	složení
písmen	písmeno	k1gNnPc2	písmeno
P	P	kA	P
a	a	k8xC	a
L.	L.	kA	L.
Symbol	symbol	k1gInSc4	symbol
užívaný	užívaný	k2eAgInSc1d1	užívaný
astrology	astrolog	k1gMnPc7	astrolog
je	být	k5eAaImIp3nS	být
však	však	k9	však
odlišný	odlišný	k2eAgInSc1d1	odlišný
(	(	kIx(	(
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
symbolu	symbol	k1gInSc2	symbol
Neptunu	Neptun	k1gInSc2	Neptun
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejviditelnějším	viditelný	k2eAgInSc7d3	nejviditelnější
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
kroužek	kroužek	k1gInSc1	kroužek
místo	místo	k7c2	místo
prostředního	prostřední	k2eAgInSc2d1	prostřední
hrotu	hrot	k1gInSc2	hrot
trojzubce	trojzubec	k1gInSc2	trojzubec
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
brzy	brzy	k6eAd1	brzy
vžilo	vžít	k5eAaPmAgNnS	vžít
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
představil	představit	k5eAaPmAgInS	představit
kresleného	kreslený	k2eAgMnSc4d1	kreslený
psa	pes	k1gMnSc4	pes
Pluta	Pluto	k1gMnSc4	Pluto
<g/>
,	,	kIx,	,
společníka	společník	k1gMnSc4	společník
Mickey	Mickea	k1gFnSc2	Mickea
Mouse	Mouse	k1gFnSc2	Mouse
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Glenn	Glenn	k1gMnSc1	Glenn
T.	T.	kA	T.
Seaborg	Seaborg	k1gMnSc1	Seaborg
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc4d1	objevený
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
plutonium	plutonium	k1gNnSc1	plutonium
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
byly	být	k5eAaImAgInP	být
jiné	jiný	k2eAgInPc1d1	jiný
prvky	prvek	k1gInPc1	prvek
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
po	po	k7c6	po
nově	nově	k6eAd1	nově
objevených	objevený	k2eAgFnPc6d1	objevená
planetách	planeta	k1gFnPc6	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
uran	uran	k1gInSc1	uran
a	a	k8xC	a
neptunium	neptunium	k1gNnSc1	neptunium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Pluto	Pluto	k1gMnSc1	Pluto
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
rodě	rod	k1gInSc6	rod
neživotném	životný	k2eNgInSc6d1	neživotný
i	i	k8xC	i
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
středním	střední	k2eAgInSc6d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Varianta	varianta	k1gFnSc1	varianta
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
mužského	mužský	k2eAgNnSc2d1	mužské
jména	jméno	k1gNnSc2	jméno
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
boha	bůh	k1gMnSc2	bůh
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
i	i	k8xC	i
Pravidla	pravidlo	k1gNnSc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
v	v	k7c6	v
pravopisném	pravopisný	k2eAgInSc6d1	pravopisný
slovníku	slovník	k1gInSc6	slovník
uvádějí	uvádět	k5eAaImIp3nP	uvádět
mužský	mužský	k2eAgInSc4d1	mužský
rod	rod	k1gInSc4	rod
a	a	k8xC	a
akademické	akademický	k2eAgNnSc4d1	akademické
vydání	vydání	k1gNnSc4	vydání
Pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
uvádí	uvádět	k5eAaImIp3nS	uvádět
obě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1	internetová
jazyková	jazykový	k2eAgFnSc1d1	jazyková
příručka	příručka	k1gFnSc1	příručka
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
AV	AV	kA	AV
ČR	ČR	kA	ČR
uvádí	uvádět	k5eAaImIp3nS	uvádět
rod	rod	k1gInSc1	rod
mužský	mužský	k2eAgInSc1d1	mužský
a	a	k8xC	a
připouští	připouštět	k5eAaImIp3nS	připouštět
i	i	k9	i
rod	rod	k1gInSc1	rod
střední	střední	k2eAgInSc1d1	střední
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úzu	úzus	k1gInSc6	úzus
častý	častý	k2eAgInSc1d1	častý
zčásti	zčásti	k6eAd1	zčásti
vlivem	vlivem	k7c2	vlivem
koncovky	koncovka	k1gFnSc2	koncovka
"	"	kIx"	"
<g/>
-o	-o	k?	-o
<g/>
"	"	kIx"	"
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
původu	původ	k1gInSc6	původ
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
je	být	k5eAaImIp3nS	být
hledanou	hledaný	k2eAgFnSc7d1	hledaná
planetou	planeta	k1gFnSc7	planeta
X	X	kA	X
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
získávat	získávat	k5eAaImF	získávat
trhliny	trhlina	k1gFnPc4	trhlina
hned	hned	k6eAd1	hned
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
objevu	objev	k1gInSc6	objev
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
měl	mít	k5eAaImAgMnS	mít
totiž	totiž	k9	totiž
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
jasnost	jasnost	k1gFnSc4	jasnost
a	a	k8xC	a
také	také	k9	také
i	i	k9	i
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
velkém	velký	k2eAgInSc6d1	velký
dalekohledu	dalekohled	k1gInSc6	dalekohled
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
jevil	jevit	k5eAaImAgInS	jevit
jen	jen	k9	jen
jako	jako	k9	jako
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
jeho	jeho	k3xOp3gFnSc2	jeho
hmotnosti	hmotnost	k1gFnSc2	hmotnost
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
několikrát	několikrát	k6eAd1	několikrát
významně	významně	k6eAd1	významně
opraveny	opraven	k2eAgInPc1d1	opraven
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
teprve	teprve	k6eAd1	teprve
až	až	k6eAd1	až
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
jeho	jeho	k3xOp3gInPc4	jeho
měsíce	měsíc	k1gInPc4	měsíc
Charona	Charon	k1gMnSc4	Charon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
astronomové	astronom	k1gMnPc1	astronom
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
zákona	zákon	k1gInSc2	zákon
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
hodnotě	hodnota	k1gFnSc3	hodnota
0,2	[number]	k4	0,2
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
hmotnost	hmotnost	k1gFnSc1	hmotnost
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
nízká	nízký	k2eAgFnSc1d1	nízká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
tedy	tedy	k9	tedy
další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
nalézt	nalézt	k5eAaPmF	nalézt
planetu	planeta	k1gFnSc4	planeta
X	X	kA	X
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
též	též	k9	též
"	"	kIx"	"
<g/>
Transpluto	Transplut	k2eAgNnSc1d1	Transpluto
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
mohla	moct	k5eAaImAgFnS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
pak	pak	k6eAd1	pak
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
E.	E.	kA	E.
Myles	Myles	k1gMnSc1	Myles
Standish	Standish	k1gMnSc1	Standish
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
údajů	údaj	k1gInPc2	údaj
získaných	získaný	k2eAgInPc2d1	získaný
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
kolem	kolem	k7c2	kolem
Neptunu	Neptun	k1gInSc2	Neptun
přepočítal	přepočítat	k5eAaPmAgInS	přepočítat
hmotnost	hmotnost	k1gFnSc4	hmotnost
této	tento	k3xDgFnSc2	tento
obří	obří	k2eAgFnSc2d1	obří
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
hodnotu	hodnota	k1gFnSc4	hodnota
o	o	k7c4	o
0,5	[number]	k4	0,5
%	%	kIx~	%
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
udávalo	udávat	k5eAaImAgNnS	udávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
současně	současně	k6eAd1	současně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
uvažovaném	uvažovaný	k2eAgInSc6d1	uvažovaný
gravitačním	gravitační	k2eAgInSc6d1	gravitační
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInPc3d1	nový
výpočtům	výpočet	k1gInPc3	výpočet
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Uran	Uran	k1gInSc1	Uran
již	již	k6eAd1	již
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
nijak	nijak	k6eAd1	nijak
nevymykal	vymykat	k5eNaImAgMnS	vymykat
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gNnSc1	Pluto
tedy	tedy	k9	tedy
sice	sice	k8xC	sice
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k7c2	blízko
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Lowell	Lowell	k1gMnSc1	Lowell
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajných	údajný	k2eAgFnPc2d1	údajná
odchylek	odchylka	k1gFnPc2	odchylka
v	v	k7c6	v
dráze	dráha	k1gFnSc6	dráha
Uranu	Uran	k1gInSc2	Uran
předpověděl	předpovědět	k5eAaPmAgInS	předpovědět
pozici	pozice	k1gFnSc4	pozice
planety	planeta	k1gFnSc2	planeta
X	X	kA	X
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
poznatků	poznatek	k1gInPc2	poznatek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
astronomů	astronom	k1gMnPc2	astronom
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
planeta	planeta	k1gFnSc1	planeta
X	X	kA	X
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
definoval	definovat	k5eAaBmAgInS	definovat
Lowell	Lowell	k1gInSc1	Lowell
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
248	[number]	k4	248
pozemských	pozemský	k2eAgNnPc2d1	pozemské
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristiky	charakteristika	k1gFnPc1	charakteristika
jeho	jeho	k3xOp3gFnSc2	jeho
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
kruhové	kruhový	k2eAgInPc1d1	kruhový
a	a	k8xC	a
neoddalují	oddalovat	k5eNaImIp3nP	oddalovat
se	se	k3xPyFc4	se
od	od	k7c2	od
roviny	rovina	k1gFnSc2	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gNnSc1	Pluto
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
výstřední	výstřední	k2eAgFnSc6d1	výstřední
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nakloněné	nakloněný	k2eAgFnSc6d1	nakloněná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
svírající	svírající	k2eAgFnSc4d1	svírající
s	s	k7c7	s
ekliptikou	ekliptika	k1gFnSc7	ekliptika
úhel	úhel	k1gInSc4	úhel
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
17	[number]	k4	17
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
výstřednosti	výstřednost	k1gFnSc2	výstřednost
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
úseku	úsek	k1gInSc6	úsek
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
nachází	nacházet	k5eAaImIp3nS	nacházet
blíže	blízce	k6eAd2	blízce
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
než	než	k8xS	než
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
perihéliem	perihélium	k1gNnSc7	perihélium
prošel	projít	k5eAaPmAgInS	projít
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Pluta	Pluto	k1gNnSc2	Pluto
chaotická	chaotický	k2eAgFnSc1d1	chaotická
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
relativně	relativně	k6eAd1	relativně
spolehlivě	spolehlivě	k6eAd1	spolehlivě
vypočtena	vypočten	k2eAgFnSc1d1	vypočtena
na	na	k7c4	na
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
dopředu	dopředu	k6eAd1	dopředu
(	(	kIx(	(
<g/>
i	i	k9	i
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
všechny	všechen	k3xTgInPc1	všechen
předpoklady	předpoklad	k1gInPc1	předpoklad
stávají	stávat	k5eAaImIp3nP	stávat
spekulativními	spekulativní	k2eAgNnPc7d1	spekulativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
své	svůj	k3xOyFgFnSc2	svůj
malé	malý	k2eAgFnSc2d1	malá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
Pluto	Pluto	k1gMnSc1	Pluto
citlivý	citlivý	k2eAgMnSc1d1	citlivý
i	i	k9	i
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgInPc4d1	malý
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
jeho	jeho	k3xOp3gFnSc4	jeho
dráhu	dráha	k1gFnSc4	dráha
narušovat	narušovat	k5eAaImF	narušovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nepředvídatelně	předvídatelně	k6eNd1	předvídatelně
měnila	měnit	k5eAaImAgFnS	měnit
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
naopak	naopak	k6eAd1	naopak
především	především	k6eAd1	především
dráhová	dráhový	k2eAgFnSc1d1	dráhová
rezonance	rezonance	k1gFnSc1	rezonance
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
činí	činit	k5eAaImIp3nS	činit
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc4d1	stabilní
a	a	k8xC	a
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
před	před	k7c7	před
srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
či	či	k8xC	či
před	před	k7c7	před
vymrštěním	vymrštění	k1gNnSc7	vymrštění
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
dopředu	dopředu	k6eAd1	dopředu
určit	určit	k5eAaPmF	určit
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Pluta	Pluto	k1gNnSc2	Pluto
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
shora	shora	k6eAd1	shora
zdánlivě	zdánlivě	k6eAd1	zdánlivě
protíná	protínat	k5eAaImIp3nS	protínat
dráhu	dráha	k1gFnSc4	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jejich	jejich	k3xOp3gNnSc2	jejich
uspořádání	uspořádání	k1gNnSc2	uspořádání
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
někdy	někdy	k6eAd1	někdy
srazila	srazit	k5eAaPmAgNnP	srazit
nebo	nebo	k8xC	nebo
i	i	k9	i
jen	jen	k9	jen
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nekříží	křížit	k5eNaImIp3nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
shora	shora	k6eAd1	shora
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Pluto	plut	k2eAgNnSc1d1	Pluto
nejblíže	blízce	k6eAd3	blízce
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
také	také	k9	také
blízko	blízko	k7c2	blízko
dráhy	dráha	k1gFnSc2	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
nejvýše	vysoce	k6eAd3	vysoce
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
Pluto	Pluto	k1gMnSc1	Pluto
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8	[number]	k4	8
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
nad	nad	k7c7	nad
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Vzestupný	vzestupný	k2eAgInSc4d1	vzestupný
a	a	k8xC	a
sestupný	sestupný	k2eAgInSc4d1	sestupný
uzel	uzel	k1gInSc4	uzel
dráhy	dráha	k1gFnSc2	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dráha	dráha	k1gFnSc1	dráha
protíná	protínat	k5eAaImIp3nS	protínat
ekliptiku	ekliptika	k1gFnSc4	ekliptika
<g/>
,	,	kIx,	,
zase	zase	k9	zase
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
svírají	svírat	k5eAaImIp3nP	svírat
úhel	úhel	k1gInSc4	úhel
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
21	[number]	k4	21
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
tato	tento	k3xDgFnSc1	tento
fakta	faktum	k1gNnPc4	faktum
by	by	kYmCp3nP	by
však	však	k9	však
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
Pluta	Pluto	k1gNnSc2	Pluto
nestačila	stačit	k5eNaBmAgFnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Neptun	Neptun	k1gInSc1	Neptun
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
jeho	jeho	k3xOp3gFnPc1	jeho
dráhu	dráha	k1gFnSc4	dráha
narušovat	narušovat	k5eAaImF	narušovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
její	její	k3xOp3gFnPc1	její
charakteristiky	charakteristika	k1gFnPc1	charakteristika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
precese	precese	k1gFnSc2	precese
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
průběhů	průběh	k1gInPc2	průběh
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
změnit	změnit	k5eAaPmF	změnit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nakonec	nakonec	k6eAd1	nakonec
ke	k	k7c3	k
srážce	srážka	k1gFnSc3	srážka
dojít	dojít	k5eAaPmF	dojít
mohlo	moct	k5eAaImAgNnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Fungují	fungovat	k5eAaImIp3nP	fungovat
zde	zde	k6eAd1	zde
však	však	k9	však
ještě	ještě	k9	ještě
i	i	k9	i
další	další	k2eAgInPc4d1	další
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
oběhy	oběh	k1gInPc4	oběh
Neptunu	Neptun	k1gInSc2	Neptun
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
připadnou	připadnout	k5eAaPmIp3nP	připadnout
dva	dva	k4xCgInPc4	dva
oběhy	oběh	k1gInPc4	oběh
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
opět	opět	k6eAd1	opět
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Pluto	Pluto	k1gMnSc1	Pluto
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
svého	svůj	k3xOyFgNnSc2	svůj
perihélia	perihélium	k1gNnSc2	perihélium
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
Neptun	Neptun	k1gMnSc1	Neptun
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
<g/>
°	°	k?	°
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
Pluto	Pluto	k1gMnSc1	Pluto
dokončí	dokončit	k5eAaPmIp3nS	dokončit
další	další	k2eAgInSc4d1	další
oběh	oběh	k1gInSc4	oběh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
už	už	k6eAd1	už
Neptun	Neptun	k1gInSc4	Neptun
za	za	k7c7	za
sebou	se	k3xPyFc7	se
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k6eAd1	půl
svého	svůj	k3xOyFgInSc2	svůj
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
průchodu	průchod	k1gInSc6	průchod
Pluta	Pluto	k1gNnSc2	Pluto
svým	svůj	k3xOyFgNnSc7	svůj
perihéliem	perihélium	k1gNnSc7	perihélium
je	být	k5eAaImIp3nS	být
Neptun	Neptun	k1gInSc4	Neptun
zase	zase	k9	zase
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
napřed	napřed	k6eAd1	napřed
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
a	a	k8xC	a
Neptun	Neptun	k1gMnSc1	Neptun
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
navzájem	navzájem	k6eAd1	navzájem
nemohou	moct	k5eNaImIp3nP	moct
přiblížit	přiblížit	k5eAaPmF	přiblížit
na	na	k7c4	na
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
menší	malý	k2eAgFnPc4d2	menší
než	než	k8xS	než
17	[number]	k4	17
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
občas	občas	k6eAd1	občas
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
více	hodně	k6eAd2	hodně
Uranu	Uran	k1gInSc2	Uran
(	(	kIx(	(
<g/>
11	[number]	k4	11
AU	au	k0	au
<g/>
)	)	kIx)	)
než	než	k8xS	než
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Rezonance	rezonance	k1gFnSc1	rezonance
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
již	již	k6eAd1	již
mnoho	mnoho	k4c1	mnoho
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Pluto	Pluto	k1gNnSc1	Pluto
nemohlo	moct	k5eNaImAgNnS	moct
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
nikdy	nikdy	k6eAd1	nikdy
srazit	srazit	k5eAaPmF	srazit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kdyby	kdyby	kYmCp3nS	kdyby
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
nebyla	být	k5eNaImAgFnS	být
tolik	tolik	k6eAd1	tolik
nakloněná	nakloněný	k2eAgFnSc1d1	nakloněná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rezonance	rezonance	k1gFnSc2	rezonance
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
hrají	hrát	k5eAaImIp3nP	hrát
při	při	k7c6	při
udržování	udržování	k1gNnSc4	udržování
stability	stabilita	k1gFnSc2	stabilita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Pluta	Pluto	k1gNnSc2	Pluto
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ještě	ještě	k6eAd1	ještě
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
vlivy	vliv	k1gInPc1	vliv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
dvou	dva	k4xCgInPc2	dva
dalších	další	k2eAgInPc2d1	další
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
argument	argument	k1gInSc4	argument
šířky	šířka	k1gFnSc2	šířka
perihélia	perihélium	k1gNnSc2	perihélium
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kříží	křížit	k5eAaImIp3nP	křížit
rovinu	rovina	k1gFnSc4	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
,	,	kIx,	,
a	a	k8xC	a
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
Slunci	slunce	k1gNnSc6	slunce
(	(	kIx(	(
<g/>
perihéliem	perihélium	k1gNnSc7	perihélium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Pluto	plut	k2eAgNnSc1d1	Pluto
v	v	k7c6	v
perihéliu	perihélium	k1gNnSc6	perihélium
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
nejvýše	vysoce	k6eAd3	vysoce
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
brání	bránit	k5eAaImIp3nS	bránit
v	v	k7c6	v
setkáních	setkání	k1gNnPc6	setkání
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgInSc7d1	přímý
důsledkem	důsledek	k1gInSc7	důsledek
tzv.	tzv.	kA	tzv.
Kozaiova	Kozaiův	k2eAgInSc2d1	Kozaiův
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
vlivem	vliv	k1gInSc7	vliv
je	být	k5eAaImIp3nS	být
výstřednost	výstřednost	k1gFnSc1	výstřednost
dráhy	dráha	k1gFnSc2	dráha
menšího	malý	k2eAgNnSc2d2	menší
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
Pluta	Pluto	k1gMnSc2	Pluto
<g/>
)	)	kIx)	)
úzce	úzko	k6eAd1	úzko
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
sklonem	sklon	k1gInSc7	sklon
vůči	vůči	k7c3	vůči
dráze	dráha	k1gFnSc3	dráha
většího	veliký	k2eAgNnSc2d2	veliký
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
gravitačně	gravitačně	k6eAd1	gravitačně
působí	působit	k5eAaImIp3nP	působit
(	(	kIx(	(
<g/>
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Pluta	Pluto	k1gNnSc2	Pluto
tento	tento	k3xDgInSc4	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
argument	argument	k1gInSc1	argument
šířky	šířka	k1gFnSc2	šířka
perihélia	perihélium	k1gNnSc2	perihélium
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
±	±	k?	±
<g/>
38	[number]	k4	38
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
perihéliem	perihélium	k1gNnSc7	perihélium
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
52	[number]	k4	52
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největšímu	veliký	k2eAgNnSc3d3	veliký
úhlovému	úhlový	k2eAgNnSc3d1	úhlové
přiblížení	přiblížení	k1gNnSc3	přiblížení
dochází	docházet	k5eAaImIp3nS	docházet
vždy	vždy	k6eAd1	vždy
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tzv.	tzv.	kA	tzv.
délky	délka	k1gFnSc2	délka
vzestupných	vzestupný	k2eAgInPc2d1	vzestupný
uzlů	uzel	k1gInPc2	uzel
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gFnSc1	jejich
dráha	dráha	k1gFnSc1	dráha
protíná	protínat	k5eAaImIp3nS	protínat
rovinu	rovina	k1gFnSc4	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
rezonanci	rezonance	k1gFnSc6	rezonance
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uvedeným	uvedený	k2eAgInSc7d1	uvedený
pohybem	pohyb	k1gInSc7	pohyb
perihélia	perihélium	k1gNnSc2	perihélium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
délky	délka	k1gFnPc1	délka
jsou	být	k5eAaImIp3nP	být
stejné	stejné	k1gNnSc1	stejné
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgInPc4d1	možný
oba	dva	k4xCgInPc4	dva
uzly	uzel	k1gInPc4	uzel
a	a	k8xC	a
Slunce	slunce	k1gNnSc4	slunce
spojit	spojit	k5eAaPmF	spojit
rovnou	rovný	k2eAgFnSc7d1	rovná
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
argument	argument	k1gInSc1	argument
šířky	šířka	k1gFnSc2	šířka
perihélia	perihélium	k1gNnSc2	perihélium
Pluta	plut	k2eAgFnSc1d1	Pluta
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
Pluto	Pluto	k1gNnSc1	Pluto
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
Slunci	slunce	k1gNnSc3	slunce
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
možném	možný	k2eAgInSc6d1	možný
bodě	bod	k1gInSc6	bod
nad	nad	k7c7	nad
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
když	když	k8xS	když
Pluto	Pluto	k1gMnSc1	Pluto
protíná	protínat	k5eAaImIp3nS	protínat
rovinu	rovina	k1gFnSc4	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
za	za	k7c7	za
touto	tento	k3xDgFnSc7	tento
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
superrezonance	superrezonance	k1gFnSc1	superrezonance
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
rezonance	rezonance	k1gFnSc2	rezonance
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
dostat	dostat	k5eAaPmF	dostat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
úzká	úzký	k2eAgFnSc1d1	úzká
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ze	z	k7c2	z
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
vlivů	vliv	k1gInPc2	vliv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
dráhu	dráha	k1gFnSc4	dráha
Pluta	Pluto	k1gNnSc2	Pluto
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
klasický	klasický	k2eAgInSc4d1	klasický
problém	problém	k1gInSc4	problém
tří	tři	k4xCgNnPc2	tři
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
vliv	vliv	k1gInSc1	vliv
dalších	další	k2eAgMnPc2d1	další
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
oscilace	oscilace	k1gFnSc2	oscilace
perihélia	perihélium	k1gNnSc2	perihélium
Pluta	Pluto	k1gMnSc2	Pluto
lze	lze	k6eAd1	lze
nejlépe	dobře	k6eAd3	dobře
pochopit	pochopit	k5eAaPmF	pochopit
při	při	k7c6	při
polárním	polární	k2eAgInSc6d1	polární
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
planety	planeta	k1gFnPc1	planeta
vidíme	vidět	k5eAaImIp1nP	vidět
obíhat	obíhat	k5eAaImF	obíhat
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Pluto	Pluto	k1gNnSc1	Pluto
projde	projít	k5eAaPmIp3nS	projít
svým	svůj	k3xOyFgInSc7	svůj
vzestupným	vzestupný	k2eAgInSc7d1	vzestupný
uzlem	uzel	k1gInSc7	uzel
(	(	kIx(	(
<g/>
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protíná	protínat	k5eAaImIp3nS	protínat
ekliptiku	ekliptika	k1gFnSc4	ekliptika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
se	se	k3xPyFc4	se
zezadu	zezadu	k6eAd1	zezadu
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úkor	úkor	k1gInSc4	úkor
gravitačně	gravitačně	k6eAd1	gravitačně
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Pluto	Pluto	k1gMnSc1	Pluto
nepatrně	patrně	k6eNd1	patrně
postrčí	postrčit	k5eAaPmIp3nS	postrčit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
zákona	zákon	k1gInSc2	zákon
obíhá	obíhat	k5eAaImIp3nS	obíhat
o	o	k7c4	o
trošku	trošku	k6eAd1	trošku
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
postupně	postupně	k6eAd1	postupně
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změnu	změna	k1gFnSc4	změna
jeho	on	k3xPp3gNnSc2	on
perihélia	perihélium	k1gNnSc2	perihélium
a	a	k8xC	a
délek	délka	k1gFnPc2	délka
vzestupných	vzestupný	k2eAgInPc2d1	vzestupný
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
podobné	podobný	k2eAgFnPc1d1	podobná
změny	změna	k1gFnPc1	změna
dějí	dít	k5eAaBmIp3nP	dít
též	též	k9	též
u	u	k7c2	u
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
opakováních	opakování	k1gNnPc6	opakování
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
Pluto	plut	k2eAgNnSc1d1	Pluto
natolik	natolik	k6eAd1	natolik
zpomalen	zpomalen	k2eAgMnSc1d1	zpomalen
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
urychlen	urychlit	k5eAaPmNgInS	urychlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
naopak	naopak	k6eAd1	naopak
Neptun	Neptun	k1gMnSc1	Neptun
začne	začít	k5eAaPmIp3nS	začít
dohánět	dohánět	k5eAaImF	dohánět
Pluto	Pluto	k1gNnSc4	Pluto
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
své	svůj	k3xOyFgFnPc4	svůj
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
opačného	opačný	k2eAgInSc2d1	opačný
uzlu	uzel	k1gInSc2	uzel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
zase	zase	k9	zase
Pluto	Pluto	k1gMnSc1	Pluto
postupně	postupně	k6eAd1	postupně
nabírá	nabírat	k5eAaImIp3nS	nabírat
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
se	se	k3xPyFc4	se
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
původního	původní	k2eAgInSc2d1	původní
uzlu	uzel	k1gInSc2	uzel
role	role	k1gFnSc2	role
znovu	znovu	k6eAd1	znovu
neotočí	otočit	k5eNaPmIp3nS	otočit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
perioda	perioda	k1gFnSc1	perioda
tohoto	tento	k3xDgInSc2	tento
cyklu	cyklus	k1gInSc2	cyklus
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
se	se	k3xPyFc4	se
Pluto	Pluto	k1gNnSc1	Pluto
otočí	otočit	k5eAaPmIp3nS	otočit
vždy	vždy	k6eAd1	vždy
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
153,29	[number]	k4	153,29
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
153,28	[number]	k4	153,28
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeden	jeden	k4xCgInSc4	jeden
tamní	tamní	k2eAgInSc4d1	tamní
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgMnSc1d1	roven
přibližně	přibližně	k6eAd1	přibližně
6,39	[number]	k4	6,39
dne	den	k1gInSc2	den
pozemského	pozemský	k2eAgInSc2d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
Osu	osa	k1gFnSc4	osa
rotace	rotace	k1gFnSc2	rotace
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
planeta	planeta	k1gFnSc1	planeta
Uran	Uran	k1gInSc1	Uran
vůči	vůči	k7c3	vůči
své	svůj	k3xOyFgFnSc3	svůj
dráze	dráha	k1gFnSc3	dráha
extrémně	extrémně	k6eAd1	extrémně
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
asi	asi	k9	asi
120	[number]	k4	120
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
boku	bok	k1gInSc6	bok
<g/>
,	,	kIx,	,
snad	snad	k9	snad
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dávné	dávný	k2eAgFnSc2d1	dávná
srážky	srážka	k1gFnSc2	srážka
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
dokonce	dokonce	k9	dokonce
rotuje	rotovat	k5eAaImIp3nS	rotovat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
retrográdní	retrográdní	k2eAgFnSc1d1	retrográdní
rotace	rotace	k1gFnSc1	rotace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
sklonu	sklon	k1gInSc3	sklon
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
nastávají	nastávat	k5eAaImIp3nP	nastávat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
sezónní	sezónní	k2eAgFnPc4d1	sezónní
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
slunovratů	slunovrat	k1gInPc2	slunovrat
bývá	bývat	k5eAaImIp3nS	bývat
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
vystavena	vystavit	k5eAaPmNgFnS	vystavit
stálému	stálý	k2eAgInSc3d1	stálý
slunečnímu	sluneční	k2eAgInSc3d1	sluneční
svitu	svit	k1gInSc3	svit
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stálém	stálý	k2eAgInSc6d1	stálý
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgInSc1d1	detailní
průzkum	průzkum	k1gInSc1	průzkum
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
velké	velký	k2eAgFnSc3d1	velká
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
mimořádně	mimořádně	k6eAd1	mimořádně
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
charakteristik	charakteristika	k1gFnPc2	charakteristika
proto	proto	k8xC	proto
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
astronomové	astronom	k1gMnPc1	astronom
upřesnit	upřesnit	k5eAaPmF	upřesnit
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
dat	datum	k1gNnPc2	datum
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnSc1	New
Horizons	Horizons	k1gInSc1	Horizons
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
okolo	okolo	k7c2	okolo
Pluta	Pluto	k1gNnSc2	Pluto
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
jasnost	jasnost	k1gFnSc1	jasnost
Pluta	Pluto	k1gNnSc2	Pluto
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
15,1	[number]	k4	15,1
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
perihéliu	perihélium	k1gNnSc6	perihélium
může	moct	k5eAaImIp3nS	moct
zjasnit	zjasnit	k5eAaPmF	zjasnit
až	až	k9	až
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
13,65	[number]	k4	13,65
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
pozorování	pozorování	k1gNnSc3	pozorování
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutný	nutný	k2eAgInSc1d1	nutný
dalekohled	dalekohled	k1gInSc1	dalekohled
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
velkých	velký	k2eAgInPc6d1	velký
dalekohledech	dalekohled	k1gInPc6	dalekohled
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
jeví	jevit	k5eAaImIp3nS	jevit
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
nelze	lze	k6eNd1	lze
tedy	tedy	k9	tedy
rozlišit	rozlišit	k5eAaPmF	rozlišit
jeho	jeho	k3xOp3gInSc4	jeho
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
0,06	[number]	k4	0,06
a	a	k8xC	a
0,11	[number]	k4	0,11
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
zobrazení	zobrazení	k1gNnSc6	zobrazení
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgInPc1d1	provedený
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
pečlivých	pečlivý	k2eAgNnPc6d1	pečlivé
pozorováních	pozorování	k1gNnPc6	pozorování
přechodů	přechod	k1gInPc2	přechod
jeho	on	k3xPp3gInSc2	on
největšího	veliký	k2eAgInSc2d3	veliký
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Charonu	Charon	k1gMnSc3	Charon
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jeho	on	k3xPp3gInSc4	on
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
měřily	měřit	k5eAaImAgFnP	měřit
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
jasnosti	jasnost	k1gFnSc6	jasnost
binárního	binární	k2eAgNnSc2d1	binární
tělesa	těleso	k1gNnSc2	těleso
Pluto-Charon	Pluto-Charon	k1gInSc1	Pluto-Charon
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Charon	Charon	k1gMnSc1	Charon
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
jasnější	jasný	k2eAgFnSc4d2	jasnější
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
změna	změna	k1gFnSc1	změna
jejich	jejich	k3xOp3gFnSc2	jejich
celkové	celkový	k2eAgFnSc2d1	celková
jasnosti	jasnost	k1gFnSc2	jasnost
zřetelnější	zřetelný	k2eAgMnSc1d2	zřetelnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
tmavější	tmavý	k2eAgFnSc4d2	tmavší
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
počítačově	počítačově	k6eAd1	počítačově
zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
mapa	mapa	k1gFnSc1	mapa
jasnějších	jasný	k2eAgFnPc2d2	jasnější
a	a	k8xC	a
tmavších	tmavý	k2eAgFnPc2d2	tmavší
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
změny	změna	k1gFnPc4	změna
jasnosti	jasnost	k1gFnSc2	jasnost
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
mapy	mapa	k1gFnPc1	mapa
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
složením	složení	k1gNnSc7	složení
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
HST	HST	kA	HST
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgFnPc1d3	veliký
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dostupné	dostupný	k2eAgNnSc4d1	dostupné
rozlišení	rozlišení	k1gNnSc4	rozlišení
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
tak	tak	k6eAd1	tak
znatelně	znatelně	k6eAd1	znatelně
více	hodně	k6eAd2	hodně
podrobností	podrobnost	k1gFnPc2	podrobnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
povrchové	povrchový	k2eAgFnPc4d1	povrchová
odlišnosti	odlišnost	k1gFnPc4	odlišnost
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
či	či	k8xC	či
velké	velký	k2eAgFnPc4d1	velká
světlé	světlý	k2eAgFnPc4d1	světlá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
kamer	kamera	k1gFnPc2	kamera
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
HST	HST	kA	HST
využíval	využívat	k5eAaImAgInS	využívat
při	při	k7c6	při
pořizování	pořizování	k1gNnSc6	pořizování
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
tyto	tento	k3xDgFnPc4	tento
mapy	mapa	k1gFnPc4	mapa
nejpodrobnějšími	podrobný	k2eAgFnPc7d3	nejpodrobnější
studiemi	studie	k1gFnPc7	studie
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
proletěla	proletět	k5eAaPmAgFnS	proletět
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
sonda	sonda	k1gFnSc1	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
HST	HST	kA	HST
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pozorovanou	pozorovaný	k2eAgFnSc7d1	pozorovaná
světelnou	světelný	k2eAgFnSc7d1	světelná
křivkou	křivka	k1gFnSc7	křivka
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
také	také	k9	také
periodickými	periodický	k2eAgFnPc7d1	periodická
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
jasnosti	jasnost	k1gFnSc2	jasnost
i	i	k8xC	i
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Pluta	Pluto	k1gNnSc2	Pluto
oplývá	oplývat	k5eAaImIp3nS	oplývat
tolika	tolik	k4yIc7	tolik
kontrasty	kontrast	k1gInPc7	kontrast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
málokteré	málokterý	k3yIgNnSc4	málokterý
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
;	;	kIx,	;
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
měsíc	měsíc	k1gInSc4	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
Iapetus	Iapetus	k1gInSc1	Iapetus
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
přechází	přecházet	k5eAaImIp3nP	přecházet
z	z	k7c2	z
uhlově	uhlově	k6eAd1	uhlově
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
tmavě	tmavě	k6eAd1	tmavě
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
až	až	k6eAd1	až
do	do	k7c2	do
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Marc	Marc	k1gFnSc1	Marc
Buie	Bui	k1gMnSc2	Bui
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
podrobnou	podrobný	k2eAgFnSc7d1	podrobná
analýzou	analýza	k1gFnSc7	analýza
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
červený	červený	k2eAgInSc1d1	červený
než	než	k8xS	než
povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
odstínům	odstín	k1gInPc3	odstín
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gMnSc1	Io
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
příměsí	příměs	k1gFnSc7	příměs
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
porovnání	porovnání	k1gNnSc2	porovnání
složených	složený	k2eAgInPc2d1	složený
snímků	snímek	k1gInPc2	snímek
pořízených	pořízený	k2eAgInPc2d1	pořízený
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Pluta	Pluto	k1gNnSc2	Pluto
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
obdobími	období	k1gNnPc7	období
výrazně	výrazně	k6eAd1	výrazně
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
:	:	kIx,	:
severní	severní	k2eAgFnSc4d1	severní
polární	polární	k2eAgFnSc4d1	polární
oblast	oblast	k1gFnSc4	oblast
zjasnila	zjasnit	k5eAaPmAgFnS	zjasnit
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
ztmavla	ztmavnout	k5eAaPmAgFnS	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2000	[number]	k4	2000
a	a	k8xC	a
2002	[number]	k4	2002
Pluto	Pluto	k1gMnSc1	Pluto
také	také	k9	také
znatelně	znatelně	k6eAd1	znatelně
zčervenal	zčervenat	k5eAaPmAgInS	zčervenat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rychlé	rychlý	k2eAgFnPc1d1	rychlá
změny	změna	k1gFnPc1	změna
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
sezónní	sezónní	k2eAgFnSc7d1	sezónní
sublimací	sublimace	k1gFnSc7	sublimace
některých	některý	k3yIgInPc2	některý
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
posilován	posilovat	k5eAaImNgInS	posilovat
extrémním	extrémní	k2eAgInSc7d1	extrémní
sklonem	sklon	k1gInSc7	sklon
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
výstředností	výstřednost	k1gFnSc7	výstřednost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
analýza	analýza	k1gFnSc1	analýza
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
98	[number]	k4	98
%	%	kIx~	%
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
dusíkového	dusíkový	k2eAgInSc2d1	dusíkový
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
stopy	stopa	k1gFnPc4	stopa
methanu	methan	k1gInSc2	methan
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
k	k	k7c3	k
měsíci	měsíc	k1gInSc3	měsíc
Charonu	Charon	k1gMnSc3	Charon
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
methanového	methanový	k2eAgInSc2d1	methanový
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
část	část	k1gFnSc1	část
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
více	hodně	k6eAd2	hodně
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
Hubblův	Hubblův	k2eAgInSc1d1	Hubblův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
teleskop	teleskop	k1gInSc1	teleskop
pomocí	pomocí	k7c2	pomocí
přístroje	přístroj	k1gInSc2	přístroj
Cosmic	Cosmic	k1gMnSc1	Cosmic
Origins	Originsa	k1gFnPc2	Originsa
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
nacházet	nacházet	k5eAaImF	nacházet
nějaká	nějaký	k3yIgFnSc1	nějaký
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
silně	silně	k6eAd1	silně
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadají	připadat	k5eAaImIp3nP	připadat
především	především	k9	především
složité	složitý	k2eAgFnPc1d1	složitá
molekuly	molekula	k1gFnPc1	molekula
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
či	či	k8xC	či
nitrilů	nitril	k1gInPc2	nitril
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
vznikat	vznikat	k5eAaImF	vznikat
interakcí	interakce	k1gFnSc7	interakce
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
zmrzlým	zmrzlý	k2eAgInSc7d1	zmrzlý
methanem	methan	k1gInSc7	methan
<g/>
,	,	kIx,	,
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
tyto	tento	k3xDgFnPc1	tento
molekuly	molekula	k1gFnPc1	molekula
dávají	dávat	k5eAaImIp3nP	dávat
Plutu	plut	k1gInSc2	plut
jeho	jeho	k3xOp3gNnSc2	jeho
mírně	mírně	k6eAd1	mírně
načervenalý	načervenalý	k2eAgInSc1d1	načervenalý
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
mapování	mapování	k1gNnSc4	mapování
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
sondou	sonda	k1gFnSc7	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgInPc1d1	nový
názvy	název	k1gInPc1	název
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
této	tento	k3xDgFnSc2	tento
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
budou	být	k5eAaImBp3nP	být
čerpány	čerpat	k5eAaImNgFnP	čerpat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
průzkumníků	průzkumník	k1gMnPc2	průzkumník
<g/>
,	,	kIx,	,
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
misí	mise	k1gFnPc2	mise
<g/>
,	,	kIx,	,
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
techniků	technik	k1gMnPc2	technik
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
fiktivních	fiktivní	k2eAgMnPc2d1	fiktivní
průzkumníků	průzkumník	k1gMnPc2	průzkumník
a	a	k8xC	a
cestovatelů	cestovatel	k1gMnPc2	cestovatel
<g/>
,	,	kIx,	,
míst	místo	k1gNnPc2	místo
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
korábů	koráb	k1gInPc2	koráb
<g/>
,	,	kIx,	,
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
lodí	loď	k1gFnPc2	loď
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
podle	podle	k7c2	podle
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
podsvětních	podsvětní	k2eAgFnPc2d1	podsvětní
říší	říš	k1gFnPc2	říš
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
představ	představa	k1gFnPc2	představa
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
světových	světový	k2eAgFnPc6d1	světová
kulturách	kultura	k1gFnPc6	kultura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatelů	cestovatel	k1gMnPc2	cestovatel
podsvětím	podsvětí	k1gNnSc7	podsvětí
a	a	k8xC	a
tvorům	tvor	k1gMnPc3	tvor
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
také	také	k9	také
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
New	New	k1gMnPc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navrhovala	navrhovat	k5eAaImAgNnP	navrhovat
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pojmenování	pojmenování	k1gNnPc1	pojmenování
a	a	k8xC	a
následně	následně	k6eAd1	následně
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
Pluta	Pluto	k1gNnSc2	Pluto
pomocí	pomocí	k7c2	pomocí
HST	HST	kA	HST
astronomové	astronom	k1gMnPc1	astronom
usuzují	usuzovat	k5eAaImIp3nP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
1,8	[number]	k4	1,8
a	a	k8xC	a
2,1	[number]	k4	2,1
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
by	by	kYmCp3nP	by
tělesu	těleso	k1gNnSc3	těleso
mohly	moct	k5eAaImAgFnP	moct
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
procent	procento	k1gNnPc2	procento
dodávat	dodávat	k5eAaImF	dodávat
kamenné	kamenný	k2eAgInPc1d1	kamenný
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
ze	z	k7c2	z
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
procent	procent	k1gInSc1	procent
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlivem	vlivem	k7c2	vlivem
rozpadu	rozpad	k1gInSc2	rozpad
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
materiálech	materiál	k1gInPc6	materiál
tvořících	tvořící	k2eAgFnPc2d1	tvořící
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
led	led	k1gInSc1	led
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
dost	dost	k6eAd1	dost
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
materiálů	materiál	k1gInPc2	materiál
oddělil	oddělit	k5eAaPmAgInS	oddělit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Pluto	Pluto	k1gNnSc1	Pluto
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
má	mít	k5eAaImIp3nS	mít
diferencovanou	diferencovaný	k2eAgFnSc4d1	diferencovaná
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kamenné	kamenný	k2eAgInPc1d1	kamenný
materiály	materiál	k1gInPc1	materiál
shromažďovaly	shromažďovat	k5eAaImAgInP	shromažďovat
v	v	k7c6	v
hutnějším	hutný	k2eAgNnSc6d2	hutnější
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nP	by
obklopoval	obklopovat	k5eAaImAgInS	obklopovat
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
jádra	jádro	k1gNnSc2	jádro
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
kolem	kolem	k7c2	kolem
1700	[number]	k4	1700
km	km	kA	km
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
70	[number]	k4	70
%	%	kIx~	%
průměru	průměr	k1gInSc2	průměr
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahřívání	zahřívání	k1gNnSc1	zahřívání
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k6eAd1	tak
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
pláštěm	plášť	k1gInSc7	plášť
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
z	z	k7c2	z
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
tloušťka	tloušťka	k1gFnSc1	tloušťka
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
činit	činit	k5eAaImF	činit
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pracovníci	pracovník	k1gMnPc1	pracovník
Institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
planet	planeta	k1gFnPc2	planeta
Německého	německý	k2eAgNnSc2d1	německé
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
vypočítali	vypočítat	k5eAaPmAgMnP	vypočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
podobný	podobný	k2eAgInSc4d1	podobný
poměr	poměr	k1gInSc4	poměr
hustoty	hustota	k1gFnSc2	hustota
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgInSc3	svůj
průměru	průměr	k1gInSc3	průměr
jako	jako	k8xS	jako
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
měsíc	měsíc	k1gInSc4	měsíc
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
někde	někde	k6eAd1	někde
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
středně	středně	k6eAd1	středně
velkými	velký	k2eAgInPc7d1	velký
ledovými	ledový	k2eAgInPc7d1	ledový
satelity	satelit	k1gInPc7	satelit
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
kamennými	kamenný	k2eAgInPc7d1	kamenný
satelity	satelit	k1gInPc7	satelit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
Europa	Europ	k1gMnSc2	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
1,3	[number]	k4	1,3
<g/>
×	×	k?	×
<g/>
1022	[number]	k4	1022
kg	kg	kA	kg
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
0,2	[number]	k4	0,2
procenta	procento	k1gNnSc2	procento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
na	na	k7c4	na
2	[number]	k4	2
306	[number]	k4	306
±	±	k?	±
<g/>
20	[number]	k4	20
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
66	[number]	k4	66
%	%	kIx~	%
průměru	průměr	k1gInSc6	průměr
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
sondy	sonda	k1gFnPc4	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
velikost	velikost	k1gFnSc1	velikost
na	na	k7c4	na
2	[number]	k4	2
370	[number]	k4	370
±	±	k?	±
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
momentálně	momentálně	k6eAd1	momentálně
největší	veliký	k2eAgFnSc7d3	veliký
známou	známý	k2eAgFnSc7d1	známá
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
snažili	snažit	k5eAaImAgMnP	snažit
vypočítat	vypočítat	k5eAaPmF	vypočítat
hmotnost	hmotnost	k1gFnSc4	hmotnost
Pluta	Pluto	k1gNnSc2	Pluto
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	on	k3xPp3gInSc2	on
domnělého	domnělý	k2eAgInSc2d1	domnělý
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
hmotný	hmotný	k2eAgInSc4d1	hmotný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
výpočty	výpočet	k1gInPc1	výpočet
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgInPc1d1	provedený
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
tuto	tento	k3xDgFnSc4	tento
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
hmotnost	hmotnost	k1gFnSc4	hmotnost
snížily	snížit	k5eAaPmAgFnP	snížit
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
astronomové	astronom	k1gMnPc1	astronom
Dale	Dale	k1gFnPc2	Dale
Cruikshank	Cruikshanko	k1gNnPc2	Cruikshanko
<g/>
,	,	kIx,	,
Carl	Carla	k1gFnPc2	Carla
Pilcher	Pilchra	k1gFnPc2	Pilchra
a	a	k8xC	a
David	David	k1gMnSc1	David
Morrison	Morrison	k1gMnSc1	Morrison
z	z	k7c2	z
Havajské	havajský	k2eAgFnSc2d1	Havajská
university	universita	k1gFnSc2	universita
vzali	vzít	k5eAaPmAgMnP	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přítomnost	přítomnost	k1gFnSc4	přítomnost
methanového	methanový	k2eAgInSc2d1	methanový
ledu	led	k1gInSc2	led
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
rozměru	rozměr	k1gInSc3	rozměr
velmi	velmi	k6eAd1	velmi
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
tak	tak	k6eAd1	tak
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
jeho	jeho	k3xOp3gNnSc4	jeho
albedo	albedo	k1gNnSc4	albedo
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
Pluta	Pluto	k1gNnSc2	Pluto
nemůže	moct	k5eNaImIp3nS	moct
přesahovat	přesahovat	k5eAaImF	přesahovat
1	[number]	k4	1
procento	procento	k1gNnSc4	procento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Albedo	Albedo	k1gNnSc1	Albedo
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
1,3	[number]	k4	1,3
až	až	k9	až
2	[number]	k4	2
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
albedo	albedo	k1gNnSc4	albedo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
měsíce	měsíc	k1gInSc2	měsíc
Charona	Charon	k1gMnSc2	Charon
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
umožnil	umožnit	k5eAaPmAgInS	umožnit
výpočet	výpočet	k1gInSc1	výpočet
hmotnosti	hmotnost	k1gFnSc2	hmotnost
tohoto	tento	k3xDgInSc2	tento
binárního	binární	k2eAgInSc2d1	binární
systému	systém	k1gInSc2	systém
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
třetího	třetí	k4xOgInSc2	třetí
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
změřena	změřen	k2eAgFnSc1d1	změřena
gravitace	gravitace	k1gFnSc1	gravitace
Charona	Charon	k1gMnSc2	Charon
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
určena	určit	k5eAaPmNgFnS	určit
i	i	k9	i
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hmotnost	hmotnost	k1gFnSc1	hmotnost
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pozorování	pozorování	k1gNnSc3	pozorování
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zákrytů	zákryt	k1gInPc2	zákryt
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
Charona	Charon	k1gMnSc4	Charon
mohli	moct	k5eAaImAgMnP	moct
vědci	vědec	k1gMnPc1	vědec
také	také	k9	také
přesněji	přesně	k6eAd2	přesně
stanovit	stanovit	k5eAaPmF	stanovit
rozměry	rozměr	k1gInPc4	rozměr
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokroky	pokrok	k1gInPc4	pokrok
v	v	k7c6	v
adaptivní	adaptivní	k2eAgFnSc6d1	adaptivní
optice	optika	k1gFnSc6	optika
zase	zase	k9	zase
umožnily	umožnit	k5eAaPmAgFnP	umožnit
lepší	dobrý	k2eAgNnSc4d2	lepší
poznání	poznání	k1gNnSc4	poznání
jeho	on	k3xPp3gInSc2	on
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
dosahující	dosahující	k2eAgFnSc4d1	dosahující
0,2	[number]	k4	0,2
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Měsíce	měsíc	k1gInSc2	měsíc
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
hmotný	hmotný	k2eAgInSc4d1	hmotný
než	než	k8xS	než
terestrické	terestrický	k2eAgFnPc4d1	terestrická
planety	planeta	k1gFnPc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
hmotnost	hmotnost	k1gFnSc4	hmotnost
dokonce	dokonce	k9	dokonce
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
i	i	k9	i
sedm	sedm	k4xCc4	sedm
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
,	,	kIx,	,
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
Io	Io	k1gMnSc5	Io
<g/>
,	,	kIx,	,
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
a	a	k8xC	a
Triton	triton	k1gMnSc1	triton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
možná	možná	k9	možná
také	také	k9	také
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
Pluta	Pluto	k1gNnSc2	Pluto
(	(	kIx(	(
<g/>
odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2300	[number]	k4	2300
km	km	kA	km
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
průměru	průměr	k1gInSc3	průměr
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
oproti	oproti	k7c3	oproti
největšímu	veliký	k2eAgNnSc3d3	veliký
tělesu	těleso	k1gNnSc3	těleso
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
trpasličí	trpasličí	k2eAgFnSc3d1	trpasličí
planetě	planeta	k1gFnSc3	planeta
Ceres	ceres	k1gInSc4	ceres
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
jsou	být	k5eAaImIp3nP	být
odhady	odhad	k1gInPc1	odhad
velikosti	velikost	k1gFnSc2	velikost
zatíženy	zatížit	k5eAaPmNgInP	zatížit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zatím	zatím	k6eAd1	zatím
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
či	či	k8xC	či
menší	malý	k2eAgInSc1d2	menší
průměr	průměr	k1gInSc1	průměr
než	než	k8xS	než
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
určení	určení	k1gNnSc4	určení
jeho	jeho	k3xOp3gFnSc2	jeho
velikosti	velikost	k1gFnSc2	velikost
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
uhlovodíkový	uhlovodíkový	k2eAgInSc1d1	uhlovodíkový
opar	opar	k1gInSc1	opar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
těleso	těleso	k1gNnSc4	těleso
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tenkou	tenký	k2eAgFnSc7d1	tenká
dusíkovou	dusíkový	k2eAgFnSc7d1	dusíková
slupkou	slupka	k1gFnSc7	slupka
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc4d1	obsahující
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
methanu	methan	k1gInSc2	methan
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
ze	z	k7c2	z
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
obal	obal	k1gInSc4	obal
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
km	km	kA	km
silný	silný	k2eAgInSc4d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
se	se	k3xPyFc4	se
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
6,5	[number]	k4	6,5
do	do	k7c2	do
24	[number]	k4	24
mikrobarů	mikrobar	k1gInPc2	mikrobar
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
atmosféru	atmosféra	k1gFnSc4	atmosféra
má	mít	k5eAaImIp3nS	mít
protáhlá	protáhlý	k2eAgFnSc1d1	protáhlá
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
:	:	kIx,	:
čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
plyny	plyn	k1gInPc7	plyn
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
ukládají	ukládat	k5eAaImIp3nP	ukládat
zmrzlé	zmrzlý	k2eAgInPc1d1	zmrzlý
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Pluto	Pluto	k1gMnSc1	Pluto
naopak	naopak	k6eAd1	naopak
Slunci	slunce	k1gNnSc3	slunce
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
pevného	pevný	k2eAgInSc2d1	pevný
povrchu	povrch	k1gInSc2	povrch
mírně	mírně	k6eAd1	mírně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
materiál	materiál	k1gInSc1	materiál
znovu	znovu	k6eAd1	znovu
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sublimace	sublimace	k1gFnSc1	sublimace
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
odpařování	odpařování	k1gNnSc1	odpařování
<g/>
)	)	kIx)	)
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
ochlazující	ochlazující	k2eAgInSc4d1	ochlazující
účinek	účinek	k1gInSc4	účinek
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
naprostém	naprostý	k2eAgInSc6d1	naprostý
opaku	opak	k1gInSc6	opak
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
známe	znát	k5eAaImIp1nP	znát
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
submilimetrového	submilimetrový	k2eAgInSc2d1	submilimetrový
radioteleskopu	radioteleskop	k1gInSc2	radioteleskop
SMA	SMA	kA	SMA
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Mauna	Maun	k1gMnSc2	Maun
Kea	Kea	k1gMnSc2	Kea
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
se	se	k3xPyFc4	se
astronomům	astronom	k1gMnPc3	astronom
podařilo	podařit	k5eAaPmAgNnS	podařit
změřit	změřit	k5eAaPmF	změřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
Pluta	Pluto	k1gNnSc2	Pluto
činí	činit	k5eAaImIp3nS	činit
43	[number]	k4	43
Kelvinů	kelvin	k1gInPc2	kelvin
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
10	[number]	k4	10
Kelvinů	kelvin	k1gInPc2	kelvin
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
činily	činit	k5eAaImAgFnP	činit
předpovědi	předpověď	k1gFnPc1	předpověď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
efektem	efekt	k1gInSc7	efekt
nepočítaly	počítat	k5eNaImAgFnP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
silným	silný	k2eAgInSc7d1	silný
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Pluta	Pluto	k1gNnSc2	Pluto
teplotní	teplotní	k2eAgFnSc4d1	teplotní
inverzi	inverze	k1gFnSc4	inverze
(	(	kIx(	(
<g/>
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
výškou	výška	k1gFnSc7	výška
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
teplota	teplota	k1gFnSc1	teplota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
10	[number]	k4	10
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
o	o	k7c4	o
36	[number]	k4	36
stupňů	stupeň	k1gInPc2	stupeň
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
dokonce	dokonce	k9	dokonce
výšky	výška	k1gFnSc2	výška
kolem	kolem	k7c2	kolem
100	[number]	k4	100
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
173	[number]	k4	173
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
více	hodně	k6eAd2	hodně
methanu	methan	k1gInSc2	methan
než	než	k8xS	než
v	v	k7c6	v
horních	horní	k2eAgInPc6d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
methan	methan	k1gInSc4	methan
po	po	k7c6	po
dusíku	dusík	k1gInSc6	dusík
druhým	druhý	k4xOgInSc7	druhý
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
existence	existence	k1gFnSc2	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c6	na
Plutu	plut	k1gInSc6	plut
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
zachyceny	zachycen	k2eAgInPc1d1	zachycen
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
astronomy	astronom	k1gMnPc4	astronom
z	z	k7c2	z
Wiseovy	Wiseův	k2eAgFnSc2d1	Wiseův
observatoře	observatoř	k1gFnSc2	observatoř
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Pluto	Pluto	k1gMnSc1	Pluto
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
jednu	jeden	k4xCgFnSc4	jeden
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
všimli	všimnout	k5eAaPmAgMnP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
postupně	postupně	k6eAd1	postupně
zhasíná	zhasínat	k5eAaImIp3nS	zhasínat
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
aby	aby	kYmCp3nS	aby
prudce	prudko	k6eAd1	prudko
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
zeslabení	zeslabení	k1gNnSc1	zeslabení
svitu	svit	k1gInSc2	svit
hvězdy	hvězda	k1gFnSc2	hvězda
prosvěcující	prosvěcující	k2eAgFnSc2d1	prosvěcující
atmosféru	atmosféra	k1gFnSc4	atmosféra
Pluta	Pluto	k1gNnSc2	Pluto
určili	určit	k5eAaPmAgMnP	určit
hodnotu	hodnota	k1gFnSc4	hodnota
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
0,15	[number]	k4	0,15
Pascalů	pascal	k1gInPc2	pascal
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
700	[number]	k4	700
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
pozemské	pozemský	k2eAgFnSc2d1	pozemská
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
další	další	k2eAgNnSc4d1	další
pozorování	pozorování	k1gNnSc4	pozorování
podobného	podobný	k2eAgInSc2d1	podobný
zákrytu	zákryt	k1gInSc2	zákryt
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
první	první	k4xOgInSc4	první
průkazný	průkazný	k2eAgInSc4d1	průkazný
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
zákryt	zákryt	k1gInSc1	zákryt
pozorovaly	pozorovat	k5eAaImAgFnP	pozorovat
a	a	k8xC	a
analyzovaly	analyzovat	k5eAaImAgFnP	analyzovat
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
týmy	tým	k1gInPc4	tým
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bruna	Bruno	k1gMnSc2	Bruno
Sicardyho	Sicardy	k1gMnSc2	Sicardy
z	z	k7c2	z
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
L.	L.	kA	L.
Elliota	Elliota	k1gFnSc1	Elliota
z	z	k7c2	z
Massachusettského	massachusettský	k2eAgInSc2d1	massachusettský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
a	a	k8xC	a
Jaye	Jay	k1gMnSc2	Jay
Pasachoffa	Pasachoff	k1gMnSc2	Pasachoff
z	z	k7c2	z
Williams	Williamsa	k1gFnPc2	Williamsa
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Pluto	Pluto	k1gMnSc1	Pluto
byl	být	k5eAaImAgMnS	být
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
než	než	k8xS	než
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
chladnější	chladný	k2eAgInSc1d2	chladnější
a	a	k8xC	a
obklopený	obklopený	k2eAgInSc1d1	obklopený
slabší	slabý	k2eAgFnSc7d2	slabší
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
překvapivému	překvapivý	k2eAgInSc3d1	překvapivý
odhadu	odhad	k1gInSc3	odhad
tlaku	tlak	k1gInSc2	tlak
0,3	[number]	k4	0,3
Pascalům	pascal	k1gInPc3	pascal
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dvojnásobku	dvojnásobek	k1gInSc2	dvojnásobek
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgNnPc3d1	předchozí
pozorováním	pozorování	k1gNnPc3	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
120	[number]	k4	120
letech	léto	k1gNnPc6	léto
dostal	dostat	k5eAaPmAgMnS	dostat
ze	z	k7c2	z
stínu	stín	k1gInSc2	stín
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vysublimoval	vysublimovat	k5eAaPmAgMnS	vysublimovat
dusík	dusík	k1gInSc4	dusík
z	z	k7c2	z
polární	polární	k2eAgFnSc2d1	polární
čepičky	čepička	k1gFnSc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
přebytečný	přebytečný	k2eAgInSc1d1	přebytečný
dusík	dusík	k1gInSc1	dusík
zase	zase	k9	zase
zkondenzuje	zkondenzovat	k5eAaPmIp3nS	zkondenzovat
a	a	k8xC	a
usadí	usadit	k5eAaPmIp3nS	usadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
zmrzlém	zmrzlý	k2eAgInSc6d1	zmrzlý
stavu	stav	k1gInSc6	stav
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
polární	polární	k2eAgFnSc1d1	polární
čepice	čepice	k1gFnSc1	čepice
se	se	k3xPyFc4	se
v	v	k7c6	v
trvalém	trvalý	k2eAgInSc6d1	trvalý
stínu	stín	k1gInSc6	stín
nachází	nacházet	k5eAaImIp3nS	nacházet
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
Naměřené	naměřený	k2eAgFnPc1d1	naměřená
malé	malý	k2eAgFnPc1d1	malá
nepravidelnosti	nepravidelnost	k1gFnPc1	nepravidelnost
v	v	k7c6	v
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Plutu	Pluto	k1gNnSc6	Pluto
vane	vanout	k5eAaImIp3nS	vanout
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
Dale	Dale	k1gNnPc2	Dale
Cruikshank	Cruikshanka	k1gFnPc2	Cruikshanka
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
z	z	k7c2	z
Ames	Amesa	k1gFnPc2	Amesa
Research	Researcha	k1gFnPc2	Researcha
Center	centrum	k1gNnPc2	centrum
americké	americký	k2eAgFnPc1d1	americká
NASA	NASA	kA	NASA
odhalili	odhalit	k5eAaPmAgMnP	odhalit
spektroskopickou	spektroskopický	k2eAgFnSc7d1	spektroskopická
analýzou	analýza	k1gFnSc7	analýza
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Pluta	Pluto	k1gNnSc2	Pluto
ethan	ethan	k1gInSc1	ethan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
fotolýzou	fotolýza	k1gFnSc7	fotolýza
či	či	k8xC	či
radiolýzou	radiolýza	k1gFnSc7	radiolýza
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
chemickou	chemický	k2eAgFnSc7d1	chemická
reakcí	reakce	k1gFnSc7	reakce
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
světla	světlo	k1gNnSc2	světlo
či	či	k8xC	či
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
pomocí	pomocí	k7c2	pomocí
přístrojů	přístroj	k1gInPc2	přístroj
meziplanetární	meziplanetární	k2eAgFnSc2d1	meziplanetární
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnPc2	New
Horizons	Horizons	k1gInSc1	Horizons
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
planetka	planetka	k1gFnSc1	planetka
zažívá	zažívat	k5eAaImIp3nS	zažívat
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
z	z	k7c2	z
astronomického	astronomický	k2eAgNnSc2d1	astronomické
hlediska	hledisko	k1gNnSc2	hledisko
také	také	k6eAd1	také
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
Pluta	Pluto	k1gNnSc2	Pluto
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
známých	známý	k2eAgInPc2d1	známý
přirozených	přirozený	k2eAgInPc2d1	přirozený
satelitů	satelit	k1gInPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Charon	Charon	k1gMnSc1	Charon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
objeven	objevit	k5eAaPmNgMnS	objevit
americkým	americký	k2eAgMnSc7d1	americký
astronomem	astronom	k1gMnSc7	astronom
Jamesem	James	k1gMnSc7	James
Christym	Christym	k1gInSc4	Christym
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
měsíčky	měsíček	k1gInPc4	měsíček
objevil	objevit	k5eAaPmAgMnS	objevit
tzv.	tzv.	kA	tzv.
HST	HST	kA	HST
Pluto	Pluto	k1gNnSc1	Pluto
Companion	Companion	k1gInSc1	Companion
Search	Search	k1gMnSc1	Search
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
skupina	skupina	k1gFnSc1	skupina
americké	americký	k2eAgFnSc2d1	americká
NASA	NASA	kA	NASA
pátrající	pátrající	k2eAgFnSc1d1	pátrající
po	po	k7c6	po
neznámých	známý	k2eNgInPc6d1	neznámý
průvodcích	průvodek	k1gInPc6	průvodek
Pluta	Pluto	k1gNnSc2	Pluto
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
Nix	Nix	k1gMnSc1	Nix
a	a	k8xC	a
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
objeveny	objevit	k5eAaPmNgInP	objevit
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Kerberos	Kerberos	k1gMnSc1	Kerberos
<g/>
,	,	kIx,	,
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
a	a	k8xC	a
Styx	Styx	k1gInSc1	Styx
<g/>
,	,	kIx,	,
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Satelity	satelit	k1gInPc1	satelit
obíhají	obíhat	k5eAaImIp3nP	obíhat
Pluto	Pluto	k1gNnSc4	Pluto
v	v	k7c6	v
neobvyklé	obvyklý	k2eNgFnSc6d1	neobvyklá
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
se	se	k3xPyFc4	se
satelity	satelit	k1gMnPc7	satelit
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
53	[number]	k4	53
%	%	kIx~	%
poloměru	poloměr	k1gInSc6	poloměr
tzv.	tzv.	kA	tzv.
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
(	(	kIx(	(
<g/>
stabilní	stabilní	k2eAgFnSc1d1	stabilní
zóna	zóna	k1gFnSc1	zóna
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
měsíc	měsíc	k1gInSc4	měsíc
Psamathe	Psamathe	k1gNnSc2	Psamathe
obíhá	obíhat	k5eAaImIp3nS	obíhat
svou	svůj	k3xOyFgFnSc4	svůj
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
40	[number]	k4	40
%	%	kIx~	%
poloměru	poloměr	k1gInSc2	poloměr
její	její	k3xOp3gFnSc2	její
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
známé	známý	k2eAgInPc1d1	známý
satelity	satelit	k1gInPc1	satelit
Pluta	Pluto	k1gNnSc2	Pluto
však	však	k9	však
obíhají	obíhat	k5eAaImIp3nP	obíhat
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
do	do	k7c2	do
3	[number]	k4	3
%	%	kIx~	%
této	tento	k3xDgFnSc2	tento
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zatím	zatím	k6eAd1	zatím
ještě	ještě	k9	ještě
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
objevy	objev	k1gInPc4	objev
dalších	další	k2eAgNnPc2d1	další
těles	těleso	k1gNnPc2	těleso
nebo	nebo	k8xC	nebo
i	i	k9	i
malých	malý	k2eAgInPc2d1	malý
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Charon	Charon	k1gMnSc1	Charon
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
Pluto-Charon	Pluto-Charon	k1gInSc1	Pluto-Charon
je	být	k5eAaImIp3nS	být
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc4	její
hmotný	hmotný	k2eAgInSc4d1	hmotný
střed	střed	k1gInSc4	střed
(	(	kIx(	(
<g/>
barycentrum	barycentrum	k1gNnSc4	barycentrum
<g/>
)	)	kIx)	)
neleží	ležet	k5eNaImIp3nS	ležet
uvnitř	uvnitř	k7c2	uvnitř
žádného	žádný	k3yNgInSc2	žádný
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
binární	binární	k2eAgInSc4d1	binární
systém	systém	k1gInSc4	systém
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
po	po	k7c6	po
systému	systém	k1gInSc6	systém
Slunce-Jupiter	Slunce-Jupitra	k1gFnPc2	Slunce-Jupitra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
má	mít	k5eAaImIp3nS	mít
Charon	Charon	k1gMnSc1	Charon
také	také	k9	také
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Plutu	Pluto	k1gNnSc3	Pluto
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc4d1	velký
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
astronomové	astronom	k1gMnPc1	astronom
proto	proto	k8xC	proto
soustavu	soustava	k1gFnSc4	soustava
nazývají	nazývat	k5eAaImIp3nP	nazývat
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
dvojplanetou	dvojplaneta	k1gFnSc7	dvojplaneta
(	(	kIx(	(
<g/>
analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
dvojplanetkám	dvojplanetka	k1gFnPc3	dvojplanetka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
mají	mít	k5eAaImIp3nP	mít
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
oběžné	oběžný	k2eAgFnSc3d1	oběžná
době	doba	k1gFnSc3	doba
kolem	kolem	k7c2	kolem
jejich	jejich	k3xOp3gNnSc2	jejich
barycentra	barycentrum	k1gNnSc2	barycentrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
k	k	k7c3	k
sobě	se	k3xPyFc3	se
neustále	neustále	k6eAd1	neustále
přivrácena	přivrátit	k5eAaPmNgFnS	přivrátit
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgNnSc1	druhý
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
ve	v	k7c6	v
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
(	(	kIx(	(
<g/>
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
není	být	k5eNaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc2	rotace
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
rovině	rovina	k1gFnSc3	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
extrémně	extrémně	k6eAd1	extrémně
nakloněná	nakloněný	k2eAgFnSc1d1	nakloněná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Pluto	Pluto	k1gMnSc1	Pluto
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
pohledu	pohled	k1gInSc2	pohled
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
se	se	k3xPyFc4	se
otáčelo	otáčet	k5eAaImAgNnS	otáčet
"	"	kIx"	"
<g/>
na	na	k7c6	na
boku	bok	k1gInSc6	bok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
nutně	nutně	k6eAd1	nutně
totéž	týž	k3xTgNnSc1	týž
také	také	k9	také
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
binární	binární	k2eAgFnSc4d1	binární
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Charonu	Charon	k1gMnSc6	Charon
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
podrobně	podrobně	k6eAd1	podrobně
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
astronomové	astronom	k1gMnPc1	astronom
z	z	k7c2	z
Gemini	Gemin	k2eAgMnPc1d1	Gemin
Observatory	Observator	k1gInPc1	Observator
odhalili	odhalit	k5eAaPmAgMnP	odhalit
skvrny	skvrna	k1gFnPc1	skvrna
tvořené	tvořený	k2eAgInPc4d1	tvořený
hydráty	hydrát	k1gInPc4	hydrát
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
krystaly	krystal	k1gInPc1	krystal
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
svědčit	svědčit	k5eAaImF	svědčit
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
tzv.	tzv.	kA	tzv.
ledových	ledový	k2eAgInPc2d1	ledový
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
dopravujících	dopravující	k2eAgInPc2d1	dopravující
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
tekutou	tekutý	k2eAgFnSc4d1	tekutá
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
následně	následně	k6eAd1	následně
zamrzala	zamrzat	k5eAaImAgFnS	zamrzat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ledových	ledový	k2eAgInPc2d1	ledový
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hydra	hydra	k1gFnSc1	hydra
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nix	Nix	k1gFnSc4	Nix
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
zachytili	zachytit	k5eAaPmAgMnP	zachytit
astronomové	astronom	k1gMnPc1	astronom
pomocí	pomocí	k7c2	pomocí
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
známým	známý	k2eAgMnSc7d1	známý
tělesem	těleso	k1gNnSc7	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgInSc7	jeden
měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Satelit	satelit	k1gMnSc1	satelit
obíhající	obíhající	k2eAgNnSc4d1	obíhající
Pluto	Pluto	k1gNnSc4	Pluto
po	po	k7c6	po
vzdálenější	vzdálený	k2eAgFnSc6d2	vzdálenější
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
označený	označený	k2eAgMnSc1d1	označený
jako	jako	k9	jako
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
P	P	kA	P
1	[number]	k4	1
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Hydra	hydra	k1gFnSc1	hydra
a	a	k8xC	a
měsíc	měsíc	k1gInSc1	měsíc
nacházející	nacházející	k2eAgInSc1d1	nacházející
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
mateřskému	mateřský	k2eAgNnSc3d1	mateřské
tělesu	těleso	k1gNnSc3	těleso
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
označený	označený	k2eAgMnSc1d1	označený
jako	jako	k9	jako
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
P	P	kA	P
2	[number]	k4	2
<g/>
)	)	kIx)	)
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Nix	Nix	k1gFnSc2	Nix
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
malé	malý	k2eAgInPc1d1	malý
měsíce	měsíc	k1gInPc1	měsíc
obíhají	obíhat	k5eAaImIp3nP	obíhat
Pluto	Pluto	k1gNnSc4	Pluto
ve	v	k7c6	v
dvoj-	dvoj-	k?	dvoj-
až	až	k9	až
trojnásobně	trojnásobně	k6eAd1	trojnásobně
větší	veliký	k2eAgFnPc4d2	veliký
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
než	než	k8xS	než
Charon	Charon	k1gMnSc1	Charon
<g/>
:	:	kIx,	:
dráha	dráha	k1gFnSc1	dráha
Nix	Nix	k1gFnSc2	Nix
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
barycentra	barycentrum	k1gNnSc2	barycentrum
soustavy	soustava	k1gFnSc2	soustava
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
48	[number]	k4	48
700	[number]	k4	700
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
Hydry	Hydrus	k1gInPc1	Hydrus
64	[number]	k4	64
800	[number]	k4	800
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kruhových	kruhový	k2eAgFnPc6d1	kruhová
prográdních	prográdní	k2eAgFnPc6d1	prográdní
dráhách	dráha	k1gFnPc6	dráha
ležících	ležící	k2eAgFnPc6d1	ležící
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Charonu	Charon	k1gMnSc3	Charon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dráhy	dráha	k1gFnPc1	dráha
leží	ležet	k5eAaImIp3nP	ležet
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Nix	Nix	k1gFnSc2	Nix
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Hydry	hydra	k1gFnSc2	hydra
<g/>
)	)	kIx)	)
s	s	k7c7	s
Charonem	Charon	k1gMnSc7	Charon
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
spolehlivě	spolehlivě	k6eAd1	spolehlivě
určeny	určit	k5eAaPmNgFnP	určit
<g/>
.	.	kIx.	.
</s>
<s>
Hydra	hydra	k1gFnSc1	hydra
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Nix	Nix	k1gFnSc1	Nix
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
odrážejí	odrážet	k5eAaImIp3nP	odrážet
více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
odhady	odhad	k1gInPc1	odhad
jejich	jejich	k3xOp3gInPc2	jejich
rozměrů	rozměr	k1gInPc2	rozměr
závisí	záviset	k5eAaImIp3nS	záviset
právě	právě	k9	právě
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
této	tento	k3xDgFnSc2	tento
odrazivosti	odrazivost	k1gFnSc2	odrazivost
(	(	kIx(	(
<g/>
albedu	albedu	k6eAd1	albedu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mají	mít	k5eAaImIp3nP	mít
komety	kometa	k1gFnPc1	kometa
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
Hydra	hydra	k1gFnSc1	hydra
měla	mít	k5eAaImAgFnS	mít
průměr	průměr	k1gInSc4	průměr
167	[number]	k4	167
±	±	k?	±
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
Nix	Nix	k1gFnSc1	Nix
137	[number]	k4	137
±	±	k?	±
11	[number]	k4	11
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
podobné	podobný	k2eAgNnSc1d1	podobné
Charonu	Charon	k1gMnSc6	Charon
(	(	kIx(	(
<g/>
35	[number]	k4	35
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nP	by
tyto	tento	k3xDgInPc1	tento
rozměry	rozměr	k1gInPc1	rozměr
byly	být	k5eAaImAgInP	být
61	[number]	k4	61
±	±	k?	±
4	[number]	k4	4
km	km	kA	km
a	a	k8xC	a
46	[number]	k4	46
±	±	k?	±
4	[number]	k4	4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jejich	jejich	k3xOp3gFnSc6	jejich
hustota	hustota	k1gFnSc1	hustota
podobná	podobný	k2eAgFnSc1d1	podobná
hustotě	hustota	k1gFnSc3	hustota
Pluta	Pluto	k1gNnSc2	Pluto
nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
jejich	jejich	k3xOp3gFnSc3	jejich
individuální	individuální	k2eAgFnSc3d1	individuální
hmotnosti	hmotnost	k1gFnSc3	hmotnost
0,0005	[number]	k4	0,0005
<g/>
násobek	násobek	k1gInSc1	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
celé	celý	k2eAgFnSc2d1	celá
Plutonovy	Plutonův	k2eAgFnSc2d1	Plutonova
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc4	objev
dvou	dva	k4xCgInPc2	dva
malých	malý	k2eAgInPc2d1	malý
měsíců	měsíc	k1gInPc2	měsíc
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Pluto	Pluto	k1gMnSc1	Pluto
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
systém	systém	k1gInSc1	systém
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Dopady	dopad	k1gInPc1	dopad
malých	malý	k2eAgNnPc2d1	malé
těles	těleso	k1gNnPc2	těleso
mohou	moct	k5eAaImIp3nP	moct
vyvrhnout	vyvrhnout	k5eAaPmF	vyvrhnout
dostatek	dostatek	k1gInSc4	dostatek
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
pozorování	pozorování	k1gNnSc1	pozorování
Hubblovým	Hubblův	k2eAgInSc7d1	Hubblův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
nenaznačují	naznačovat	k5eNaImIp3nP	naznačovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
podobně	podobně	k6eAd1	podobně
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prstence	prstenec	k1gInPc1	prstenec
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
velmi	velmi	k6eAd1	velmi
stísněný	stísněný	k2eAgMnSc1d1	stísněný
<g/>
,	,	kIx,	,
nepřesahující	přesahující	k2eNgFnSc4d1	nepřesahující
šířku	šířka	k1gFnSc4	šířka
1000	[number]	k4	1000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zveřejněná	zveřejněný	k2eAgFnSc1d1	zveřejněná
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorování	pozorování	k1gNnSc1	pozorování
Hubblovým	Hubblův	k2eAgInSc7d1	Hubblův
dalekohledem	dalekohled	k1gInSc7	dalekohled
prakticky	prakticky	k6eAd1	prakticky
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ještě	ještě	k9	ještě
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
nějaké	nějaký	k3yIgInPc1	nějaký
měsíce	měsíc	k1gInPc1	měsíc
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
panuje	panovat	k5eAaImIp3nS	panovat
90	[number]	k4	90
<g/>
%	%	kIx~	%
jistota	jistota	k1gFnSc1	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
uvažovaném	uvažovaný	k2eAgMnSc6d1	uvažovaný
albedu	albedu	k6eAd1	albedu
4	[number]	k4	4
%	%	kIx~	%
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přesahující	přesahující	k2eAgFnSc6d1	přesahující
5	[number]	k4	5
úhlových	úhlový	k2eAgFnPc2d1	úhlová
vteřin	vteřina	k1gFnPc2	vteřina
od	od	k7c2	od
Pluta	Pluto	k1gNnSc2	Pluto
již	již	k6eAd1	již
nenachází	nacházet	k5eNaImIp3nS	nacházet
žádné	žádný	k3yNgNnSc1	žádný
těleso	těleso	k1gNnSc1	těleso
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
29	[number]	k4	29
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
albedo	albedo	k1gNnSc1	albedo
38	[number]	k4	38
%	%	kIx~	%
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nP	by
horní	horní	k2eAgFnSc4d1	horní
mez	mez	k1gFnSc4	mez
průměru	průměr	k1gInSc2	průměr
případného	případný	k2eAgInSc2d1	případný
nového	nový	k2eAgInSc2d1	nový
měsíce	měsíc	k1gInSc2	měsíc
dokonce	dokonce	k9	dokonce
jen	jen	k9	jen
9	[number]	k4	9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
rozměrnější	rozměrný	k2eAgFnPc1d2	rozměrnější
tělesa	těleso	k1gNnPc1	těleso
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
ukrývat	ukrývat	k5eAaImF	ukrývat
na	na	k7c6	na
dráhách	dráha	k1gFnPc6	dráha
blíže	blíž	k1gFnSc2	blíž
Plutu	plut	k1gInSc2	plut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pozorování	pozorování	k1gNnSc1	pozorování
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
jeho	jeho	k3xOp3gFnPc4	jeho
záře	zář	k1gFnPc4	zář
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
další	další	k2eAgInPc1d1	další
satelity	satelit	k1gInPc1	satelit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
velikost	velikost	k1gFnSc1	velikost
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
určena	určit	k5eAaPmNgFnS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Kerberos	Kerberos	k1gMnSc1	Kerberos
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
Styx	Styx	k1gInSc4	Styx
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
měsíc	měsíc	k1gInSc4	měsíc
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
Kerberos	Kerberos	k1gMnSc1	Kerberos
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
tým	tým	k1gInSc1	tým
amerických	americký	k2eAgMnPc2d1	americký
astronomů	astronom	k1gMnPc2	astronom
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Showalterem	Showalter	k1gMnSc7	Showalter
z	z	k7c2	z
Institutu	institut	k1gInSc2	institut
SETI	sít	k5eAaImNgMnP	sít
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
satelitu	satelit	k1gInSc2	satelit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nalezli	naleznout	k5eAaPmAgMnP	naleznout
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
,	,	kIx,	,
když	když	k8xS	když
pátrali	pátrat	k5eAaImAgMnP	pátrat
po	po	k7c6	po
případných	případný	k2eAgInPc6d1	případný
prstencích	prstenec	k1gInPc6	prstenec
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
oběžnými	oběžný	k2eAgFnPc7d1	oběžná
dráhami	dráha	k1gFnPc7	dráha
Nix	Nix	k1gFnSc2	Nix
a	a	k8xC	a
Hydry	hydra	k1gFnSc2	hydra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
13	[number]	k4	13
až	až	k9	až
34	[number]	k4	34
km	km	kA	km
a	a	k8xC	a
poloměr	poloměr	k1gInSc4	poloměr
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c4	na
59	[number]	k4	59
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kerberos	Kerberos	k1gMnSc1	Kerberos
je	být	k5eAaImIp3nS	být
desetkrát	desetkrát	k6eAd1	desetkrát
méně	málo	k6eAd2	málo
jasný	jasný	k2eAgInSc4d1	jasný
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
Nix	Nix	k1gFnSc2	Nix
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
zachycení	zachycení	k1gNnSc4	zachycení
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
byla	být	k5eAaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
osmiminutová	osmiminutový	k2eAgFnSc1d1	osmiminutová
expozice	expozice	k1gFnSc1	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
tým	tým	k1gInSc1	tým
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
Plutův	Plutův	k2eAgInSc4d1	Plutův
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
Styx	Styx	k1gInSc4	Styx
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc4	objev
oznámila	oznámit	k5eAaPmAgFnS	oznámit
NASA	NASA	kA	NASA
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
satelit	satelit	k1gInSc1	satelit
je	být	k5eAaImIp3nS	být
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
10	[number]	k4	10
až	až	k8xS	až
25	[number]	k4	25
km	km	kA	km
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
dráhy	dráha	k1gFnSc2	dráha
přibližně	přibližně	k6eAd1	přibližně
46	[number]	k4	46
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
a	a	k8xC	a
Model	model	k1gInSc1	model
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Pluta	Pluto	k1gNnSc2	Pluto
astronomy	astronom	k1gMnPc4	astronom
dlouho	dlouho	k6eAd1	dlouho
mátl	mást	k5eAaImAgInS	mást
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bývalý	bývalý	k2eAgInSc4d1	bývalý
měsíc	měsíc	k1gInSc4	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
kolem	kolem	k7c2	kolem
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
vymrštil	vymrštit	k5eAaPmAgMnS	vymrštit
její	její	k3xOp3gInSc4	její
největší	veliký	k2eAgInSc4d3	veliký
současný	současný	k2eAgInSc4d1	současný
měsíc	měsíc	k1gInSc4	měsíc
Triton	triton	k1gMnSc1	triton
<g/>
.	.	kIx.	.
</s>
<s>
Tahle	tenhle	k3xDgFnSc1	tenhle
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
však	však	k9	však
neujala	ujmout	k5eNaPmAgFnS	ujmout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Pluto	Pluto	k1gMnSc1	Pluto
se	se	k3xPyFc4	se
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
Neptunu	Neptun	k1gInSc2	Neptun
nikdy	nikdy	k6eAd1	nikdy
nepřibližuje	přibližovat	k5eNaImIp3nS	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
místo	místo	k1gNnSc1	místo
Pluta	Pluto	k1gNnSc2	Pluto
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
jasnější	jasný	k2eAgMnSc1d2	jasnější
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
astronomové	astronom	k1gMnPc1	astronom
postupně	postupně	k6eAd1	postupně
nalézali	nalézat	k5eAaImAgMnP	nalézat
další	další	k2eAgNnPc4d1	další
malá	malý	k2eAgNnPc4d1	malé
transneptunická	transneptunický	k2eAgNnPc4d1	transneptunické
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
Plutu	plut	k1gInSc3	plut
podobala	podobat	k5eAaImAgFnS	podobat
nejenom	nejenom	k6eAd1	nejenom
svou	svůj	k3xOyFgFnSc7	svůj
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
proto	proto	k8xC	proto
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
největším	veliký	k2eAgInSc7d3	veliký
členem	člen	k1gInSc7	člen
tzv.	tzv.	kA	tzv.
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
stabilního	stabilní	k2eAgInSc2d1	stabilní
prstence	prstenec	k1gInSc2	prstenec
sestávajícího	sestávající	k2eAgInSc2d1	sestávající
z	z	k7c2	z
těles	těleso	k1gNnPc2	těleso
obíhajících	obíhající	k2eAgNnPc2d1	obíhající
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiná	jiný	k2eAgNnPc1d1	jiné
tělesa	těleso	k1gNnPc1	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
i	i	k8xC	i
Pluto	Pluto	k1gNnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgFnPc4	některý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
společné	společný	k2eAgFnPc4d1	společná
s	s	k7c7	s
kometami	kometa	k1gFnPc7	kometa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
rovněž	rovněž	k9	rovněž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
ležícími	ležící	k2eAgFnPc7d1	ležící
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Pluto	Pluto	k1gMnSc1	Pluto
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xC	jako
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
typický	typický	k2eAgInSc1d1	typický
kometární	kometární	k2eAgInSc1d1	kometární
ohon	ohon	k1gInSc1	ohon
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Pluto	Pluto	k1gNnSc1	Pluto
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
známým	známý	k2eAgNnSc7d1	známé
současným	současný	k2eAgNnSc7d1	současné
tělesem	těleso	k1gNnSc7	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
zřejmě	zřejmě	k6eAd1	zřejmě
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
měsíc	měsíc	k1gInSc4	měsíc
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
natolik	natolik	k6eAd1	natolik
podobné	podobný	k2eAgNnSc4d1	podobné
geologické	geologický	k2eAgNnSc4d1	geologické
i	i	k8xC	i
atmosférické	atmosférický	k2eAgNnSc4d1	atmosférické
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
astronomy	astronom	k1gMnPc7	astronom
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
domněnce	domněnka	k1gFnSc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bývalé	bývalý	k2eAgNnSc4d1	bývalé
těleso	těleso	k1gNnSc4	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
později	pozdě	k6eAd2	pozdě
Neptun	Neptun	k1gMnSc1	Neptun
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
objekt	objekt	k1gInSc4	objekt
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
populace	populace	k1gFnSc2	populace
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
velká	velký	k2eAgNnPc1d1	velké
tělesa	těleso	k1gNnPc1	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
včetně	včetně	k7c2	včetně
Pluta	Pluto	k1gNnSc2	Pluto
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gMnSc1	jejich
největší	veliký	k2eAgMnSc1d3	veliký
představitel	představitel	k1gMnSc1	představitel
jim	on	k3xPp3gMnPc3	on
také	také	k6eAd1	také
dal	dát	k5eAaPmAgInS	dát
název	název	k1gInSc1	název
-	-	kIx~	-
plutina	plutina	k1gFnSc1	plutina
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
i	i	k9	i
ostatní	ostatní	k2eAgNnPc1d1	ostatní
tělesa	těleso	k1gNnPc1	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
ještě	ještě	k6eAd1	ještě
zbyly	zbýt	k5eAaPmAgInP	zbýt
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
soustava	soustava	k1gFnSc1	soustava
teprve	teprve	k6eAd1	teprve
utvářela	utvářet	k5eAaImAgFnS	utvářet
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgFnPc1	který
navzájem	navzájem	k6eAd1	navzájem
nesplynuly	splynout	k5eNaPmAgFnP	splynout
v	v	k7c4	v
plnohodnotné	plnohodnotný	k2eAgFnPc4d1	plnohodnotná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
astronomů	astronom	k1gMnPc2	astronom
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
pozice	pozice	k1gFnSc1	pozice
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
relativně	relativně	k6eAd1	relativně
náhlé	náhlý	k2eAgFnSc2d1	náhlá
migrace	migrace	k1gFnSc2	migrace
Neptunu	Neptun	k1gInSc2	Neptun
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dobách	doba	k1gFnPc6	doba
vývoje	vývoj	k1gInSc2	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
postupu	postup	k1gInSc2	postup
z	z	k7c2	z
vnitřnější	vnitřní	k2eAgFnSc2d2	vnitřnější
oblasti	oblast	k1gFnSc2	oblast
soustavy	soustava	k1gFnSc2	soustava
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
se	se	k3xPyFc4	se
Neptun	Neptun	k1gInSc1	Neptun
dostával	dostávat	k5eAaImAgInS	dostávat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
předchůdci	předchůdce	k1gMnSc6	předchůdce
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zřejmě	zřejmě	k6eAd1	zřejmě
zachytil	zachytit	k5eAaPmAgMnS	zachytit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
Triton	triton	k1gInSc1	triton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiné	k1gNnSc1	jiné
uzamkl	uzamknout	k5eAaPmAgInS	uzamknout
v	v	k7c6	v
dráhových	dráhový	k2eAgFnPc6d1	dráhová
rezonancích	rezonance	k1gFnPc6	rezonance
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
vymetl	vymetnout	k5eAaPmAgInS	vymetnout
na	na	k7c4	na
chaotické	chaotický	k2eAgFnPc4d1	chaotická
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
scénář	scénář	k1gInSc1	scénář
vyplynul	vyplynout	k5eAaPmAgInS	vyplynout
z	z	k7c2	z
počítačového	počítačový	k2eAgInSc2d1	počítačový
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
sestaveného	sestavený	k2eAgInSc2d1	sestavený
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
astronomy	astronom	k1gMnPc7	astronom
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Observatoire	Observatoir	k1gInSc5	Observatoir
de	de	k?	de
la	la	k1gNnPc2	la
Côte	Côte	k1gNnPc2	Côte
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Azur	azur	k1gInSc1	azur
<g/>
,	,	kIx,	,
známém	známý	k2eAgNnSc6d1	známé
jako	jako	k8xC	jako
model	model	k1gInSc4	model
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gMnSc1	Pluto
původně	původně	k6eAd1	původně
obíhal	obíhat	k5eAaImAgMnS	obíhat
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kruhové	kruhový	k2eAgFnSc6d1	kruhová
dráze	dráha	k1gFnSc6	dráha
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
kolem	kolem	k7c2	kolem
33	[number]	k4	33
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
modelu	model	k1gInSc2	model
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
planetesimálním	planetesimální	k2eAgInSc6d1	planetesimální
disku	disk	k1gInSc6	disk
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
kolem	kolem	k7c2	kolem
tisíce	tisíc	k4xCgInSc2	tisíc
těles	těleso	k1gNnPc2	těleso
podobné	podobný	k2eAgFnSc2d1	podobná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Tritonu	triton	k1gInSc2	triton
a	a	k8xC	a
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
také	také	k9	také
lépe	dobře	k6eAd2	dobře
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
vznik	vznik	k1gInSc1	vznik
jeho	jeho	k3xOp3gInSc2	jeho
měsíce	měsíc	k1gInSc2	měsíc
Charonu	Charon	k1gMnSc3	Charon
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
srážka	srážka	k1gFnSc1	srážka
Pluta	Pluto	k1gNnSc2	Pluto
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
velkým	velký	k2eAgNnSc7d1	velké
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tomu	ten	k3xDgMnSc3	ten
zřejmě	zřejmě	k6eAd1	zřejmě
předcházela	předcházet	k5eAaImAgFnS	předcházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
řídkém	řídký	k2eAgInSc6d1	řídký
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
velmi	velmi	k6eAd1	velmi
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střední	střední	k2eAgFnSc6d1	střední
opozici	opozice	k1gFnSc6	opozice
je	být	k5eAaImIp3nS	být
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
Pluta	Pluto	k1gNnSc2	Pluto
jen	jen	k9	jen
13,6	[number]	k4	13,6
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
jasnost	jasnost	k1gFnSc1	jasnost
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
největším	veliký	k2eAgNnSc6d3	veliký
přiblížení	přiblížení	k1gNnSc6	přiblížení
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
o	o	k7c4	o
magnitudu	magnituda	k1gFnSc4	magnituda
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
aféliu	afélium	k1gNnSc6	afélium
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
konjunkci	konjunkce	k1gFnSc6	konjunkce
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
15,9	[number]	k4	15,9
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
pozorování	pozorování	k1gNnSc3	pozorování
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
potřeba	potřeba	k6eAd1	potřeba
větší	veliký	k2eAgInSc4d2	veliký
astronomický	astronomický	k2eAgInSc4d1	astronomický
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
průměr	průměr	k1gInSc1	průměr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
maximálně	maximálně	k6eAd1	maximálně
0,3	[number]	k4	0,3
obloukové	obloukový	k2eAgFnSc2d1	oblouková
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
vidět	vidět	k5eAaImF	vidět
ho	on	k3xPp3gNnSc4	on
jako	jako	k8xS	jako
kotouček	kotouček	k1gInSc1	kotouček
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
Pluta	Pluto	k1gNnSc2	Pluto
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
jsou	být	k5eAaImIp3nP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
detailnější	detailní	k2eAgFnPc1d2	detailnější
hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
sklon	sklon	k1gInSc4	sklon
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
až	až	k9	až
17,15	[number]	k4	17,15
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
Pluto	Pluto	k1gMnSc1	Pluto
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
mnoha	mnoho	k4c7	mnoho
jinými	jiný	k2eAgNnPc7d1	jiné
souhvězdími	souhvězdí	k1gNnPc7	souhvězdí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
pohyb	pohyb	k1gInSc1	pohyb
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
Střelec	Střelec	k1gMnSc1	Střelec
a	a	k8xC	a
setrvá	setrvat	k5eAaPmIp3nS	setrvat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
března	březen	k1gInSc2	březen
2023	[number]	k4	2023
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
let	let	k1gInSc4	let
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
malé	malý	k2eAgFnSc3d1	malá
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
obrovské	obrovský	k2eAgFnSc3d1	obrovská
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
velkou	velký	k2eAgFnSc4d1	velká
výzvu	výzva	k1gFnSc4	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
měla	mít	k5eAaImAgFnS	mít
možnost	možnost	k1gFnSc1	možnost
těleso	těleso	k1gNnSc4	těleso
navštívit	navštívit	k5eAaPmF	navštívit
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
1	[number]	k4	1
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
její	její	k3xOp3gNnSc1	její
řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
dalo	dát	k5eAaPmAgNnS	dát
přednost	přednost	k1gFnSc4	přednost
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
měsíce	měsíc	k1gInSc2	měsíc
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
neslučitelný	slučitelný	k2eNgInSc1d1	neslučitelný
s	s	k7c7	s
trajektorií	trajektorie	k1gFnSc7	trajektorie
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
na	na	k7c4	na
průlet	průlet	k1gInSc4	průlet
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
letu	let	k1gInSc6	let
se	se	k3xPyFc4	se
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
nijak	nijak	k6eAd1	nijak
nepřibližovala	přibližovat	k5eNaImAgFnS	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
seriózní	seriózní	k2eAgFnPc1d1	seriózní
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
Pluta	Pluto	k1gNnSc2	Pluto
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
sondou	sonda	k1gFnSc7	sonda
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
objevily	objevit	k5eAaPmAgFnP	objevit
až	až	k9	až
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
NASA	NASA	kA	NASA
začala	začít	k5eAaPmAgFnS	začít
plánovat	plánovat	k5eAaImF	plánovat
misi	mise	k1gFnSc4	mise
k	k	k7c3	k
Plutu	Pluto	k1gMnSc3	Pluto
a	a	k8xC	a
Charonu	Charon	k1gMnSc3	Charon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Pluto	Pluto	k1gMnSc1	Pluto
Kuiper	Kuipra	k1gFnPc2	Kuipra
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
však	však	k9	však
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
rozpočtových	rozpočtový	k2eAgInPc2d1	rozpočtový
důvodů	důvod	k1gInPc2	důvod
odvolána	odvolán	k2eAgFnSc1d1	odvolána
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
pracovníci	pracovník	k1gMnPc1	pracovník
NASA	NASA	kA	NASA
plánovat	plánovat	k5eAaImF	plánovat
misi	mise	k1gFnSc4	mise
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
podařilo	podařit	k5eAaPmAgNnS	podařit
vyslat	vyslat	k5eAaPmF	vyslat
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
sondu	sonda	k1gFnSc4	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
sonda	sonda	k1gFnSc1	sonda
minula	minout	k5eAaImAgFnS	minout
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využila	využít	k5eAaPmAgFnS	využít
efektu	efekt	k1gInSc3	efekt
gravitačního	gravitační	k2eAgInSc2d1	gravitační
praku	prak	k1gInSc2	prak
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
její	její	k3xOp3gInSc4	její
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
sonda	sonda	k1gFnSc1	sonda
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
GMT	GMT	kA	GMT
<g/>
.	.	kIx.	.
</s>
<s>
Vědecká	vědecký	k2eAgNnPc1d1	vědecké
pozorování	pozorování	k1gNnPc1	pozorování
tělesa	těleso	k1gNnSc2	těleso
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zahájit	zahájit	k5eAaPmF	zahájit
již	již	k9	již
o	o	k7c4	o
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInSc4d1	další
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
plánovaném	plánovaný	k2eAgNnSc6d1	plánované
setkání	setkání	k1gNnSc6	setkání
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
Pluta	Pluto	k1gNnSc2	Pluto
sonda	sonda	k1gFnSc1	sonda
pořídila	pořídit	k5eAaPmAgFnS	pořídit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
testu	test	k1gInSc2	test
dlouhofokální	dlouhofokální	k2eAgFnSc2d1	dlouhofokální
kamery	kamera	k1gFnSc2	kamera
LORRI	LORRI	kA	LORRI
(	(	kIx(	(
<g/>
Long	Long	k1gMnSc1	Long
Range	Rang	k1gFnSc2	Rang
Reconnaisance	Reconnaisance	k1gFnSc1	Reconnaisance
Imager	Imager	k1gMnSc1	Imager
<g/>
)	)	kIx)	)
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
pořízené	pořízený	k2eAgInPc1d1	pořízený
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
4,2	[number]	k4	4,2
miliardy	miliarda	k4xCgFnSc2	miliarda
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sonda	sonda	k1gFnSc1	sonda
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zaměřit	zaměřit	k5eAaPmF	zaměřit
i	i	k8xC	i
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnPc1d1	důležitá
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
směrování	směrování	k1gNnSc4	směrování
k	k	k7c3	k
Plutu	Pluto	k1gNnSc3	Pluto
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
objektům	objekt	k1gInPc3	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Horizons	Horizons	k1gInSc1	Horizons
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
např.	např.	kA	např.
pořizování	pořizování	k1gNnSc1	pořizování
obrazových	obrazový	k2eAgInPc2d1	obrazový
záznamů	záznam	k1gInPc2	záznam
či	či	k8xC	či
provádění	provádění	k1gNnSc2	provádění
spektroskopických	spektroskopický	k2eAgFnPc2d1	spektroskopická
analýz	analýza	k1gFnPc2	analýza
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
geologii	geologie	k1gFnSc4	geologie
a	a	k8xC	a
morfologii	morfologie	k1gFnSc4	morfologie
Pluta	Pluto	k1gNnSc2	Pluto
i	i	k8xC	i
jeho	on	k3xPp3gInSc2	on
měsíce	měsíc	k1gInSc2	měsíc
Charonu	Charon	k1gMnSc3	Charon
<g/>
,	,	kIx,	,
zmapovat	zmapovat	k5eAaPmF	zmapovat
složení	složení	k1gNnSc4	složení
jejich	jejich	k3xOp3gInSc2	jejich
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
analyzovat	analyzovat	k5eAaImF	analyzovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Objevy	objev	k1gInPc1	objev
nových	nový	k2eAgInPc2d1	nový
malých	malý	k2eAgInPc2d1	malý
měsíců	měsíc	k1gInPc2	měsíc
mohou	moct	k5eAaImIp3nP	moct
znamenat	znamenat	k5eAaImF	znamenat
pro	pro	k7c4	pro
sondu	sonda	k1gFnSc4	sonda
nepředvídané	předvídaný	k2eNgFnSc2d1	nepředvídaná
komplikace	komplikace	k1gFnSc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
úvah	úvaha	k1gFnPc2	úvaha
mohl	moct	k5eAaImAgInS	moct
materiál	materiál	k1gInSc1	materiál
vyvržený	vyvržený	k2eAgInSc1d1	vyvržený
při	při	k7c6	při
případných	případný	k2eAgFnPc6d1	případná
kolizích	kolize	k1gFnPc6	kolize
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
a	a	k8xC	a
těmito	tento	k3xDgInPc7	tento
měsíci	měsíc	k1gInPc7	měsíc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
gravitaci	gravitace	k1gFnSc4	gravitace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
únikovou	únikový	k2eAgFnSc4d1	úniková
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
slabý	slabý	k2eAgInSc4d1	slabý
prachový	prachový	k2eAgInSc4d1	prachový
prstenec	prstenec	k1gInSc4	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
zkřížil	zkřížit	k5eAaPmAgInS	zkřížit
sondě	sonda	k1gFnSc3	sonda
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
riziko	riziko	k1gNnSc1	riziko
škod	škoda	k1gFnPc2	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
mikrometeoroidy	mikrometeoroid	k1gInPc4	mikrometeoroid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
sondu	sonda	k1gFnSc4	sonda
mohly	moct	k5eAaImAgInP	moct
vyřadit	vyřadit	k5eAaPmF	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnSc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
plutoidy	plutoida	k1gFnSc2	plutoida
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tělesa	těleso	k1gNnPc4	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
plutina	plutin	k1gMnSc2	plutin
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
objekty	objekt	k1gInPc1	objekt
obíhající	obíhající	k2eAgInPc1d1	obíhající
Slunce	slunce	k1gNnSc4	slunce
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
objevu	objev	k1gInSc6	objev
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
dalších	další	k2eAgNnPc2d1	další
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
jejich	jejich	k3xOp3gMnSc2	jejich
řadového	řadový	k2eAgMnSc2d1	řadový
člena	člen	k1gMnSc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
planetária	planetárium	k1gNnSc2	planetárium
začala	začít	k5eAaPmAgFnS	začít
veřejnosti	veřejnost	k1gFnSc3	veřejnost
představovat	představovat	k5eAaImF	představovat
modely	model	k1gInPc4	model
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
již	již	k6eAd1	již
Pluto	Pluto	k1gNnSc4	Pluto
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
nefiguroval	figurovat	k5eNaImAgMnS	figurovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Quaoar	Quaoar	k1gInSc1	Quaoar
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1280	[number]	k4	1280
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
průměru	průměr	k1gInSc2	průměr
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zase	zase	k9	zase
objevitelé	objevitel	k1gMnPc1	objevitel
Sedny	Sedna	k1gFnSc2	Sedna
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
její	její	k3xOp3gInSc4	její
průměr	průměr	k1gInSc4	průměr
až	až	k9	až
na	na	k7c4	na
1800	[number]	k4	1800
km	km	kA	km
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
hodnotě	hodnota	k1gFnSc3	hodnota
průměru	průměr	k1gInSc2	průměr
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
přesahující	přesahující	k2eAgFnSc4d1	přesahující
2300	[number]	k4	2300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Sílily	sílit	k5eAaImAgFnP	sílit
proto	proto	k8xC	proto
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gMnSc1	Pluto
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
označen	označit	k5eAaPmNgMnS	označit
jen	jen	k9	jen
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
objektů	objekt	k1gInPc2	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
astronomové	astronom	k1gMnPc1	astronom
přestali	přestat	k5eAaPmAgMnP	přestat
za	za	k7c2	za
planety	planeta	k1gFnSc2	planeta
považovat	považovat	k5eAaImF	považovat
Ceres	ceres	k1gInSc1	ceres
<g/>
,	,	kIx,	,
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
Juno	Juno	k1gFnSc4	Juno
a	a	k8xC	a
Vestu	vesta	k1gFnSc4	vesta
<g/>
,	,	kIx,	,
když	když	k8xS	když
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
pás	pás	k1gInSc1	pás
planetek	planetka	k1gFnPc2	planetka
jim	on	k3xPp3gInPc3	on
blízkých	blízký	k2eAgMnPc2d1	blízký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
astronomové	astronom	k1gMnPc1	astronom
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
návrh	návrh	k1gInSc1	návrh
neprošel	projít	k5eNaPmAgInS	projít
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
objev	objev	k1gInSc1	objev
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
podobný	podobný	k2eAgInSc1d1	podobný
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
má	mít	k5eAaImIp3nS	mít
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Objevitelé	objevitel	k1gMnPc1	objevitel
i	i	k8xC	i
tisk	tisk	k1gInSc1	tisk
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
desátou	desátý	k4xOgFnSc4	desátý
planetu	planeta	k1gFnSc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
obecně	obecně	k6eAd1	obecně
mezi	mezi	k7c4	mezi
astronomy	astronom	k1gMnPc4	astronom
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
žádná	žádný	k3yNgFnSc1	žádný
shoda	shoda	k1gFnSc1	shoda
nepanovala	panovat	k5eNaImAgFnS	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
to	ten	k3xDgNnSc4	ten
naopak	naopak	k6eAd1	naopak
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nejsilnější	silný	k2eAgInSc4d3	nejsilnější
argument	argument	k1gInSc4	argument
pro	pro	k7c4	pro
přeřazení	přeřazení	k1gNnSc4	přeřazení
Pluta	Pluto	k1gNnSc2	Pluto
mezi	mezi	k7c4	mezi
planetky	planetka	k1gFnPc4	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Debatu	debata	k1gFnSc4	debata
nakonec	nakonec	k6eAd1	nakonec
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc2d1	konaná
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Pluto	plut	k2eAgNnSc1d1	Pluto
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
že	že	k8xS	že
těleso	těleso	k1gNnSc1	těleso
musí	muset	k5eAaImIp3nS	muset
vládnout	vládnout	k5eAaImF	vládnout
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyčistilo	vyčistit	k5eAaPmAgNnS	vyčistit
okolí	okolí	k1gNnSc4	okolí
své	svůj	k3xOyFgFnSc2	svůj
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
byl	být	k5eAaImAgInS	být
i	i	k9	i
výslovný	výslovný	k2eAgInSc1d1	výslovný
výčet	výčet	k1gInSc1	výčet
uznaných	uznaný	k2eAgFnPc2d1	uznaná
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
Pluto	Pluto	k1gMnSc1	Pluto
nebyl	být	k5eNaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
prototyp	prototyp	k1gInSc4	prototyp
nově	nově	k6eAd1	nově
ustanovené	ustanovený	k2eAgFnSc2d1	ustanovená
kategorie	kategorie	k1gFnSc2	kategorie
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
pak	pak	k8xC	pak
IAU	IAU	kA	IAU
oficiálně	oficiálně	k6eAd1	oficiálně
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
obě	dva	k4xCgNnPc4	dva
největší	veliký	k2eAgNnPc4d3	veliký
transneptunická	transneptunický	k2eAgNnPc4d1	transneptunické
tělesa	těleso	k1gNnPc4	těleso
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
katalogu	katalog	k1gInSc2	katalog
planetek	planetka	k1gFnPc2	planetka
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
(	(	kIx(	(
<g/>
134340	[number]	k4	134340
<g/>
)	)	kIx)	)
Pluto	Pluto	k1gMnSc1	Pluto
a	a	k8xC	a
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
klasifikace	klasifikace	k1gFnSc1	klasifikace
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mezi	mezi	k7c4	mezi
astronomy	astronom	k1gMnPc4	astronom
také	také	k9	také
určitý	určitý	k2eAgInSc4d1	určitý
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
vědeckého	vědecký	k2eAgInSc2d1	vědecký
týmu	tým	k1gInSc2	tým
řídícího	řídící	k2eAgInSc2d1	řídící
let	léto	k1gNnPc2	léto
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnSc1	New
Horizons	Horizons	k1gInSc1	Horizons
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
výslovně	výslovně	k6eAd1	výslovně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
definice	definice	k1gFnSc2	definice
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
planetami	planeta	k1gFnPc7	planeta
ani	ani	k8xC	ani
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
či	či	k8xC	či
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jejich	jejich	k3xOp3gFnPc2	jejich
drah	draha	k1gFnPc2	draha
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
některé	některý	k3yIgFnPc4	některý
planetky	planetka	k1gFnPc4	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ani	ani	k8xC	ani
neodráží	odrážet	k5eNaImIp3nS	odrážet
vůli	vůle	k1gFnSc4	vůle
astronomické	astronomický	k2eAgFnSc2d1	astronomická
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
pět	pět	k4xCc1	pět
procent	procento	k1gNnPc2	procento
astronomů	astronom	k1gMnPc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
např.	např.	kA	např.
Michael	Michael	k1gMnSc1	Michael
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
IAU	IAU	kA	IAU
podpořil	podpořit	k5eAaPmAgMnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
k	k	k7c3	k
protestům	protest	k1gInPc3	protest
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
členů	člen	k1gMnPc2	člen
Kalifornského	kalifornský	k2eAgNnSc2d1	kalifornské
zákonodárného	zákonodárný	k2eAgNnSc2d1	zákonodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
předložilo	předložit	k5eAaPmAgNnS	předložit
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mj.	mj.	kA	mj.
obvinili	obvinit	k5eAaPmAgMnP	obvinit
IAU	IAU	kA	IAU
z	z	k7c2	z
"	"	kIx"	"
<g/>
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
Sněmovny	sněmovna	k1gFnPc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
přijali	přijmout	k5eAaPmAgMnP	přijmout
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vzdali	vzdát	k5eAaPmAgMnP	vzdát
čest	čest	k1gFnSc4	čest
Clydu	Clyd	k1gMnSc3	Clyd
Tombaughovi	Tombaugh	k1gMnSc3	Tombaugh
<g/>
,	,	kIx,	,
objeviteli	objevitel	k1gMnSc3	objevitel
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
výročí	výročí	k1gNnSc2	výročí
objevu	objev	k1gInSc2	objev
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
Den	den	k1gInSc4	den
planety	planeta	k1gFnSc2	planeta
Pluto	Pluto	k1gNnSc4	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
schválil	schválit	k5eAaPmAgInS	schválit
podobnou	podobný	k2eAgFnSc4d1	podobná
rezoluci	rezoluce	k1gFnSc4	rezoluce
illinoiský	illinoiský	k2eAgInSc1d1	illinoiský
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Plutu	Pluto	k1gMnSc3	Pluto
byl	být	k5eAaImAgMnS	být
navrácen	navrácen	k2eAgInSc4d1	navrácen
jeho	jeho	k3xOp3gInSc4	jeho
původní	původní	k2eAgInSc4d1	původní
status	status	k1gInSc4	status
a	a	k8xC	a
současně	současně	k6eAd1	současně
určil	určit	k5eAaPmAgInS	určit
za	za	k7c4	za
den	den	k1gInSc4	den
Pluta	Pluto	k1gNnSc2	Pluto
13	[number]	k4	13
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
statusu	status	k1gInSc6	status
Pluta	Pluto	k1gNnSc2	Pluto
nevyhasly	vyhasnout	k5eNaPmAgFnP	vyhasnout
ani	ani	k9	ani
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
debatovalo	debatovat	k5eAaImAgNnS	debatovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
