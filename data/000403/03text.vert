<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Rothkosteletz	Rothkosteletz	k1gInSc1	Rothkosteletz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Náchod	Náchod	k1gInSc1	Náchod
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Podorlické	podorlický	k2eAgFnSc6d1	Podorlická
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
Olešnici	Olešnice	k1gFnSc4	Olešnice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozlohu	rozloha	k1gFnSc4	rozloha
2	[number]	k4	2
405,59	[number]	k4	405,59
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
osady	osada	k1gFnSc2	osada
"	"	kIx"	"
<g/>
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
"	"	kIx"	"
prvně	prvně	k?	prvně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
konfirmační	konfirmační	k2eAgFnSc1d1	konfirmační
kniha	kniha	k1gFnSc1	kniha
pražské	pražský	k2eAgFnSc2d1	Pražská
diecéze	diecéze	k1gFnSc2	diecéze
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1362	[number]	k4	1362
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
osada	osada	k1gFnSc1	osada
měla	mít	k5eAaImAgFnS	mít
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1831	[number]	k4	1831
přeskočila	přeskočit	k5eAaPmAgFnS	přeskočit
jiskra	jiskra	k1gFnSc1	jiskra
z	z	k7c2	z
neopatrně	opatrně	k6eNd1	opatrně
hlídaného	hlídaný	k2eAgInSc2d1	hlídaný
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
65	[number]	k4	65
domů	dům	k1gInPc2	dům
včetně	včetně	k7c2	včetně
fary	fara	k1gFnSc2	fara
<g/>
,	,	kIx,	,
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
zvonice	zvonice	k1gFnSc2	zvonice
a	a	k8xC	a
kostnice	kostnice	k1gFnSc2	kostnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1837	[number]	k4	1837
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1838	[number]	k4	1838
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
Kostelci	Kostelec	k1gInSc6	Kostelec
žila	žít	k5eAaImAgFnS	žít
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Nastěhovala	nastěhovat	k5eAaPmAgFnS	nastěhovat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
hned	hned	k6eAd1	hned
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
muzeum	muzeum	k1gNnSc1	muzeum
-	-	kIx~	-
domek	domek	k1gInSc1	domek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
žila	žít	k5eAaImAgFnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
muzea	muzeum	k1gNnSc2	muzeum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pomník	pomník	k1gInSc1	pomník
Viktorie	Viktoria	k1gFnSc2	Viktoria
Židové	židová	k1gFnSc2	židová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
osud	osud	k1gInSc1	osud
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k9	jako
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
Viktorky	Viktorka	k1gFnSc2	Viktorka
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
Viktorie	Viktoria	k1gFnSc2	Viktoria
Židové	židová	k1gFnSc2	židová
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
spjatými	spjatý	k2eAgInPc7d1	spjatý
s	s	k7c7	s
Červeným	červený	k2eAgInSc7d1	červený
Kostelcem	Kostelec	k1gInSc7	Kostelec
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
generál	generál	k1gMnSc1	generál
Jan	Jan	k1gMnSc1	Jan
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Martin	Martin	k1gMnSc1	Martin
Růžek	Růžek	k1gMnSc1	Růžek
a	a	k8xC	a
psychotronik	psychotronik	k1gMnSc1	psychotronik
Břetislav	Břetislav	k1gMnSc1	Břetislav
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1926	[number]	k4	1926
navštívil	navštívit	k5eAaPmAgInS	navštívit
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přivítání	přivítání	k1gNnSc6	přivítání
starostou	starosta	k1gMnSc7	starosta
Františkem	František	k1gMnSc7	František
Novákem	Novák	k1gMnSc7	Novák
si	se	k3xPyFc3	se
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
nové	nový	k2eAgNnSc4d1	nové
Městské	městský	k2eAgNnSc4d1	Městské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
hospic	hospic	k1gInSc1	hospic
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
Kostelci	Kostelec	k1gInSc6	Kostelec
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
Krucifix	krucifix	k1gInSc4	krucifix
U	u	k7c2	u
Devíti	devět	k4xCc2	devět
křížů	kříž	k1gInPc2	kříž
<g/>
,	,	kIx,	,
u	u	k7c2	u
lesa	les	k1gInSc2	les
Krákorky	krákorka	k1gFnSc2	krákorka
Pomník	pomník	k1gInSc1	pomník
obětem	oběť	k1gFnPc3	oběť
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Pomník	pomník	k1gInSc4	pomník
vysílačky	vysílačka	k1gFnSc2	vysílačka
Libuše	Libuše	k1gFnPc1	Libuše
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Končinách	končina	k1gFnPc6	končina
Tovární	tovární	k2eAgInSc4d1	tovární
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Textilní	textilní	k2eAgFnSc2d1	textilní
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Küsnacht	Küsnachtum	k1gNnPc2	Küsnachtum
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Warrington	Warrington	k1gInSc1	Warrington
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Ząbkowice	Ząbkowice	k1gFnSc1	Ząbkowice
Śląskie	Śląskie	k1gFnSc1	Śląskie
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
