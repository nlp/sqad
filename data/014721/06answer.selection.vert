<s>
Mendelevium	Mendelevium	k1gNnSc1
či	či	k8xC
mendělevium	mendělevium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Md	Md	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třináctým	třináctý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
devátým	devátý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
)	)	kIx)
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
einsteinia	einsteinium	k1gNnSc2
<g/>
.	.	kIx.
</s>