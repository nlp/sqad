<s>
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kynologií	kynologie	k1gFnSc7	kynologie
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
pobočku	pobočka	k1gFnSc4	pobočka
u	u	k7c2	u
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
kynologické	kynologický	k2eAgFnSc2d1	kynologická
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
