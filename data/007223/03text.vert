<s>
Kynologie	kynologie	k1gFnSc1	kynologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
psů	pes	k1gMnPc2	pes
jako	jako	k8xS	jako
živočišného	živočišný	k2eAgInSc2d1	živočišný
druhu	druh	k1gInSc2	druh
z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
psům	pes	k1gMnPc3	pes
věnují	věnovat	k5eAaPmIp3nP	věnovat
jako	jako	k8xC	jako
domácím	domácí	k2eAgMnPc3d1	domácí
mazlíčkům	mazlíček	k1gMnPc3	mazlíček
a	a	k8xC	a
společníkům	společník	k1gMnPc3	společník
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
seriózního	seriózní	k2eAgInSc2d1	seriózní
vědeckého	vědecký	k2eAgInSc2d1	vědecký
přístupu	přístup	k1gInSc2	přístup
zoologů	zoolog	k1gMnPc2	zoolog
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
k	k	k7c3	k
souhrnnému	souhrnný	k2eAgNnSc3d1	souhrnné
označení	označení	k1gNnSc3	označení
výcvikářů	výcvikář	k1gMnPc2	výcvikář
a	a	k8xC	a
chovatelů	chovatel	k1gMnPc2	chovatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
psům	pes	k1gMnPc3	pes
věnují	věnovat	k5eAaPmIp3nP	věnovat
na	na	k7c6	na
neformální	formální	k2eNgFnSc6d1	neformální
úrovni	úroveň	k1gFnSc6	úroveň
zejména	zejména	k9	zejména
v	v	k7c6	v
psích	psí	k2eAgInPc6d1	psí
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
tak	tak	k9	tak
o	o	k7c4	o
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
větev	větev	k1gFnSc4	větev
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
přípona	přípona	k1gFnSc1	přípona
–	–	k?	–
<g/>
logie	logie	k1gFnSc2	logie
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kynologií	kynologie	k1gFnSc7	kynologie
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
pobočku	pobočka	k1gFnSc4	pobočka
u	u	k7c2	u
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
kynologické	kynologický	k2eAgFnSc2d1	kynologická
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
tedy	tedy	k8xC	tedy
studium	studium	k1gNnSc4	studium
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
všeho	všecek	k3xTgInSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
spojeno	spojen	k2eAgNnSc1d1	spojeno
provádí	provádět	k5eAaImIp3nS	provádět
a	a	k8xC	a
publikují	publikovat	k5eAaBmIp3nP	publikovat
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
obsáhli	obsáhnout	k5eAaPmAgMnP	obsáhnout
relevantní	relevantní	k2eAgFnSc4d1	relevantní
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
formální	formální	k2eAgFnSc4d1	formální
strukturu	struktura	k1gFnSc4	struktura
oboru	obor	k1gInSc2	obor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stanovy	stanova	k1gFnPc4	stanova
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kynologické	kynologický	k2eAgFnSc2d1	kynologická
federace	federace	k1gFnSc2	federace
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc3	zdraví
a	a	k8xC	a
vystavování	vystavování	k1gNnSc3	vystavování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
všichni	všechen	k3xTgMnPc1	všechen
od	od	k7c2	od
biologů	biolog	k1gMnPc2	biolog
<g/>
,	,	kIx,	,
genetiků	genetik	k1gMnPc2	genetik
<g/>
,	,	kIx,	,
zoologů	zoolog	k1gMnPc2	zoolog
a	a	k8xC	a
behavioristů	behaviorista	k1gMnPc2	behaviorista
až	až	k6eAd1	až
po	po	k7c4	po
historiky	historik	k1gMnPc4	historik
<g/>
,	,	kIx,	,
veterináře	veterinář	k1gMnPc4	veterinář
a	a	k8xC	a
odborníky	odborník	k1gMnPc4	odborník
chovů	chov	k1gInPc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Neformálně	formálně	k6eNd1	formálně
se	se	k3xPyFc4	se
psům	pes	k1gMnPc3	pes
věnují	věnovat	k5eAaPmIp3nP	věnovat
i	i	k8xC	i
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
zaměření	zaměření	k1gNnSc2	zaměření
jako	jako	k8xC	jako
novináři	novinář	k1gMnPc1	novinář
<g/>
,	,	kIx,	,
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
chovatelé	chovatel	k1gMnPc1	chovatel
<g/>
,	,	kIx,	,
výcvikáři	výcvikář	k1gMnPc1	výcvikář
<g/>
,	,	kIx,	,
psovodi	psovod	k1gMnPc1	psovod
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
převážně	převážně	k6eAd1	převážně
osobní	osobní	k2eAgFnPc4d1	osobní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgNnSc2	tento
nevědeckého	vědecký	k2eNgNnSc2d1	nevědecké
úsilí	úsilí	k1gNnSc2	úsilí
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
mnoho	mnoho	k4c1	mnoho
praktických	praktický	k2eAgFnPc2d1	praktická
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
videí	video	k1gNnPc2	video
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kynology	kynolog	k1gMnPc7	kynolog
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
tedy	tedy	k9	tedy
formálně	formálně	k6eAd1	formálně
i	i	k9	i
neformálně	formálně	k6eNd1	formálně
zabývat	zabývat	k5eAaImF	zabývat
obory	obor	k1gInPc4	obor
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
veterinární	veterinární	k2eAgNnSc4d1	veterinární
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
,	,	kIx,	,
chovatelství	chovatelství	k1gNnSc4	chovatelství
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
výcvik	výcvik	k1gInSc4	výcvik
psů	pes	k1gMnPc2	pes
nebo	nebo	k8xC	nebo
literaturou	literatura	k1gFnSc7	literatura
a	a	k8xC	a
historií	historie	k1gFnSc7	historie
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Služební	služební	k2eAgFnSc1d1	služební
kynologie	kynologie	k1gFnSc1	kynologie
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
oborem	obor	k1gInSc7	obor
sloužícím	sloužící	k2eAgInSc7d1	sloužící
policii	policie	k1gFnSc3	policie
<g/>
,	,	kIx,	,
vězeňské	vězeňský	k2eAgFnSc3d1	vězeňská
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
celní	celní	k2eAgFnSc3d1	celní
správě	správa	k1gFnSc3	správa
<g/>
,	,	kIx,	,
obranným	obranný	k2eAgFnPc3d1	obranná
složkám	složka	k1gFnPc3	složka
a	a	k8xC	a
strážcům	strážce	k1gMnPc3	strážce
majetku	majetek	k1gInSc2	majetek
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
kynologie	kynologie	k1gFnPc1	kynologie
<g/>
,	,	kIx,	,
či	či	k8xC	či
psí	psí	k2eAgInPc4d1	psí
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
účelnost	účelnost	k1gFnSc4	účelnost
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
budování	budování	k1gNnSc4	budování
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
psovodem	psovod	k1gMnSc7	psovod
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
"	"	kIx"	"
<g/>
importovány	importován	k2eAgFnPc4d1	importována
<g/>
"	"	kIx"	"
z	z	k7c2	z
angloamerické	angloamerický	k2eAgFnSc2d1	angloamerická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
agility	agilit	k2eAgMnPc4d1	agilit
<g/>
,	,	kIx,	,
dog	doga	k1gFnPc2	doga
frisbee	frisbe	k1gInSc2	frisbe
<g/>
,	,	kIx,	,
flyball	flyball	k1gInSc1	flyball
<g/>
,	,	kIx,	,
coursing	coursing	k1gInSc1	coursing
<g/>
,	,	kIx,	,
dog	doga	k1gFnPc2	doga
dancing	dancing	k1gInSc1	dancing
<g/>
,	,	kIx,	,
obedience	obedience	k1gFnSc1	obedience
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
divácky	divácky	k6eAd1	divácky
zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
odvětvím	odvětví	k1gNnSc7	odvětví
kynologie	kynologie	k1gFnSc2	kynologie
jsou	být	k5eAaImIp3nP	být
výstavy	výstava	k1gFnPc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Chovatelstvím	chovatelství	k1gNnSc7	chovatelství
lze	lze	k6eAd1	lze
nazvat	nazvat	k5eAaPmF	nazvat
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
úspěšně	úspěšně	k6eAd1	úspěšně
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
jedince	jedinec	k1gMnPc4	jedinec
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k8xC	tedy
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
původní	původní	k2eAgNnSc4d1	původní
sepětí	sepětí	k1gNnSc4	sepětí
s	s	k7c7	s
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
zejm.	zejm.	k?	zejm.
biologií	biologie	k1gFnSc7	biologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovatelství	chovatelství	k1gNnSc6	chovatelství
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
důležité	důležitý	k2eAgNnSc1d1	důležité
sledovat	sledovat	k5eAaImF	sledovat
zdraví	zdraví	k1gNnSc4	zdraví
všech	všecek	k3xTgMnPc2	všecek
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
potomků	potomek	k1gMnPc2	potomek
před	před	k7c7	před
i	i	k9	i
po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1	zkouška
jsou	být	k5eAaImIp3nP	být
institucionálně	institucionálně	k6eAd1	institucionálně
pořádaná	pořádaný	k2eAgNnPc1d1	pořádané
ověření	ověření	k1gNnPc1	ověření
schopností	schopnost	k1gFnSc7	schopnost
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
dosažení	dosažení	k1gNnSc1	dosažení
určitého	určitý	k2eAgInSc2d1	určitý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
zkušebních	zkušební	k2eAgInPc2d1	zkušební
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
psovod	psovod	k1gMnSc1	psovod
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pes	pes	k1gMnSc1	pes
mohou	moct	k5eAaImIp3nP	moct
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zkušebního	zkušební	k2eAgInSc2d1	zkušební
řádu	řád	k1gInSc2	řád
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInSc4d1	sportovní
výcvik	výcvik	k1gInSc4	výcvik
psů	pes	k1gMnPc2	pes
(	(	kIx(	(
<g/>
NZŘ	NZŘ	kA	NZŘ
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
pes	pes	k1gMnSc1	pes
skládat	skládat	k5eAaImF	skládat
zkoušky	zkouška	k1gFnPc4	zkouška
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
od	od	k7c2	od
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
Zkouška	zkouška	k1gFnSc1	zkouška
základní	základní	k2eAgFnSc2d1	základní
ovladatelnosti	ovladatelnost	k1gFnSc2	ovladatelnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řád	řád	k1gInSc1	řád
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
stupních	stupeň	k1gInPc6	stupeň
také	také	k9	také
zkoušky	zkouška	k1gFnPc4	zkouška
z	z	k7c2	z
všestranného	všestranný	k2eAgInSc2d1	všestranný
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc1	zkouška
psa	pes	k1gMnSc2	pes
stopaře	stopař	k1gMnSc2	stopař
a	a	k8xC	a
zkoušky	zkouška	k1gFnSc2	zkouška
psa	pes	k1gMnSc4	pes
obranáře	obranář	k1gMnSc4	obranář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
také	také	k9	také
lze	lze	k6eAd1	lze
skládat	skládat	k5eAaImF	skládat
zkoušky	zkouška	k1gFnPc4	zkouška
podle	podle	k7c2	podle
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
zkušebního	zkušební	k2eAgInSc2d1	zkušební
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
IPO	IPO	kA	IPO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
složit	složit	k5eAaPmF	složit
například	například	k6eAd1	například
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
zkoušku	zkouška	k1gFnSc4	zkouška
pracovního	pracovní	k2eAgMnSc4d1	pracovní
psa	pes	k1gMnSc4	pes
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stupních	stupeň	k1gInPc6	stupeň
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
zkoušku	zkouška	k1gFnSc4	zkouška
psa	pes	k1gMnSc4	pes
stopaře	stopař	k1gMnSc4	stopař
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tyto	tento	k3xDgInPc4	tento
řády	řád	k1gInPc4	řád
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
Zkušební	zkušební	k2eAgInSc4d1	zkušební
řád	řád	k1gInSc4	řád
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInSc4d1	sportovní
výcvik	výcvik	k1gInSc4	výcvik
psů	pes	k1gMnPc2	pes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
SchH	SchH	k1gFnSc1	SchH
<g/>
/	/	kIx~	/
<g/>
VPG	VPG	kA	VPG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zkoušku	zkouška	k1gFnSc4	zkouška
z	z	k7c2	z
pachových	pachový	k2eAgFnPc2d1	pachová
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
i	i	k8xC	i
Kynologická	kynologický	k2eAgFnSc1d1	kynologická
jednota	jednota	k1gFnSc1	jednota
Brno	Brno	k1gNnSc4	Brno
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
zkušební	zkušební	k2eAgInSc4d1	zkušební
řád	řád	k1gInSc4	řád
poskytující	poskytující	k2eAgFnSc1d1	poskytující
možnost	možnost	k1gFnSc1	možnost
složit	složit	k5eAaPmF	složit
zkoušky	zkouška	k1gFnPc4	zkouška
pasení	pasení	k1gNnSc2	pasení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
psovodů	psovod	k1gMnPc2	psovod
záchranných	záchranný	k2eAgInPc2d1	záchranný
sborů	sbor	k1gInPc2	sbor
máme	mít	k5eAaImIp1nP	mít
Zkušební	zkušební	k2eAgInSc4d1	zkušební
řád	řád	k1gInSc4	řád
Svazu	svaz	k1gInSc2	svaz
záchranných	záchranný	k2eAgFnPc2d1	záchranná
brigád	brigáda	k1gFnPc2	brigáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přezkušující	přezkušující	k2eAgFnSc2d1	přezkušující
schopnosti	schopnost	k1gFnSc2	schopnost
psa	pes	k1gMnSc2	pes
při	při	k7c6	při
záchranných	záchranný	k2eAgFnPc6d1	záchranná
a	a	k8xC	a
lavinových	lavinový	k2eAgFnPc6d1	lavinová
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
kynologický	kynologický	k2eAgInSc1d1	kynologický
svaz	svaz	k1gInSc1	svaz
TART	TART	kA	TART
rovněž	rovněž	k9	rovněž
disponuje	disponovat	k5eAaBmIp3nS	disponovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
zkušebním	zkušební	k2eAgInSc7d1	zkušební
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgFnPc7d1	základní
disciplínami	disciplína	k1gFnPc7	disciplína
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc6d1	uvedená
zkouškách	zkouška	k1gFnPc6	zkouška
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
instituci	instituce	k1gFnSc4	instituce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkušební	zkušební	k2eAgInSc4d1	zkušební
řád	řád	k1gInSc4	řád
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
základní	základní	k2eAgFnSc4d1	základní
poslušnost	poslušnost	k1gFnSc4	poslušnost
<g/>
,	,	kIx,	,
obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
cizí	cizí	k2eAgFnSc1d1	cizí
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInPc4d1	speciální
cviky	cvik	k1gInPc4	cvik
<g/>
,	,	kIx,	,
skupinové	skupinový	k2eAgInPc4d1	skupinový
cviky	cvik	k1gInPc4	cvik
<g/>
,	,	kIx,	,
hlídání	hlídání	k1gNnSc4	hlídání
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
zkušební	zkušební	k2eAgInPc4d1	zkušební
řády	řád	k1gInPc4	řád
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
shody	shoda	k1gFnSc2	shoda
v	v	k7c6	v
disciplínách	disciplína	k1gFnPc6	disciplína
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
cvik	cvik	k1gInSc1	cvik
či	či	k8xC	či
jiné	jiný	k2eAgNnSc1d1	jiné
předvedení	předvedení	k1gNnSc1	předvedení
vypadat	vypadat	k5eAaImF	vypadat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pes	pes	k1gMnSc1	pes
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
u	u	k7c2	u
nohy	noha	k1gFnSc2	noha
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
na	na	k7c4	na
psovoda	psovod	k1gMnSc4	psovod
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
dopředu	dopředu	k6eAd1	dopředu
<g/>
;	;	kIx,	;
přivolání	přivolání	k1gNnSc4	přivolání
s	s	k7c7	s
předsednutím	předsednutí	k1gNnSc7	předsednutí
<g/>
,	,	kIx,	,
přivolání	přivolání	k1gNnPc1	přivolání
bez	bez	k7c2	bez
předsednutí	předsednutí	k1gNnPc2	předsednutí
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
Myslivost	myslivost	k1gFnSc1	myslivost
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
myslivce	myslivec	k1gMnSc4	myslivec
nepostradatelnou	postradatelný	k2eNgFnSc7d1	nepostradatelná
součástí	součást	k1gFnSc7	součást
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
psů	pes	k1gMnPc2	pes
s	s	k7c7	s
loveckou	lovecký	k2eAgFnSc7d1	lovecká
upotřebitelností	upotřebitelnost	k1gFnSc7	upotřebitelnost
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
týkat	týkat	k5eAaImF	týkat
zkoušky	zkouška	k1gFnPc4	zkouška
či	či	k8xC	či
institucionalizované	institucionalizovaný	k2eAgNnSc4d1	institucionalizované
ověření	ověření	k1gNnSc4	ověření
jejich	jejich	k3xOp3gFnPc2	jejich
vloh	vloha	k1gFnPc2	vloha
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
vystavování	vystavování	k1gNnSc6	vystavování
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
uchovňování	uchovňování	k1gNnSc6	uchovňování
využít	využít	k5eAaPmF	využít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pracovní	pracovní	k2eAgFnSc2d1	pracovní
třídy	třída	k1gFnSc2	třída
<g/>
"	"	kIx"	"
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Záchranářství	záchranářství	k1gNnSc1	záchranářství
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jej	on	k3xPp3gMnSc4	on
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
Svaz	svaz	k1gInSc1	svaz
záchranných	záchranný	k2eAgFnPc2d1	záchranná
brigád	brigáda	k1gFnPc2	brigáda
kynologů	kynolog	k1gMnPc2	kynolog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
také	také	k9	také
závody	závod	k1gInPc1	závod
ve	v	k7c6	v
výkonech	výkon	k1gInPc6	výkon
záchranných	záchranný	k2eAgMnPc2d1	záchranný
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
