<s>
Aquadrom	Aquadrom	k1gInSc1
je	být	k5eAaImIp3nS
originální	originální	k2eAgInSc4d1
místní	místní	k2eAgInSc4d1
název	název	k1gInSc4
aquaparku	aquapark	k1gInSc2
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
tří	tři	k4xCgMnPc2
mosteckých	mostecký	k2eAgMnPc2d1
dromů	drom	k1gMnPc2
(	(	kIx(
<g/>
Hipodrom	hipodrom	k1gInSc1
<g/>
,	,	kIx,
Autodrom	autodrom	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
tj.	tj.	kA
tvoří	tvořit	k5eAaImIp3nS
páteřní	páteřní	k2eAgInSc4d1
systém	systém	k1gInSc4
tří	tři	k4xCgInPc2
největších	veliký	k2eAgInPc2d3
sportovních	sportovní	k2eAgInPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgInPc2d1
areálů	areál	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Most	most	k1gInSc1
<g/>
.	.	kIx.
</s>