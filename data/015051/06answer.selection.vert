<s desamb="1">
Celý	celý	k2eAgInSc1d1
areál	areál	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
města	město	k1gNnSc2
(	(	kIx(
<g/>
provozují	provozovat	k5eAaImIp3nP
jej	on	k3xPp3gMnSc4
Technické	technický	k2eAgFnPc1d1
služby	služba	k1gFnPc1
Most	most	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
také	také	k9
financovalo	financovat	k5eAaBmAgNnS
přestavbu	přestavba	k1gFnSc4
bývalého	bývalý	k2eAgNnSc2d1
koupaliště	koupaliště	k1gNnSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
Topolová	Topolová	k1gFnSc1
na	na	k7c4
aquapark	aquapark	k1gInSc4
za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
270	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>