<s>
Aquadrom	Aquadrom	k1gInSc1
</s>
<s>
Vchod	vchod	k1gInSc1
do	do	k7c2
Aquadromu	Aquadrom	k1gInSc2
Most	most	k1gInSc1
</s>
<s>
Aquadrom	Aquadrom	k1gInSc1
je	být	k5eAaImIp3nS
originální	originální	k2eAgInSc4d1
místní	místní	k2eAgInSc4d1
název	název	k1gInSc4
aquaparku	aquapark	k1gInSc2
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
tří	tři	k4xCgMnPc2
mosteckých	mostecký	k2eAgMnPc2d1
dromů	drom	k1gMnPc2
(	(	kIx(
<g/>
Hipodrom	hipodrom	k1gInSc1
<g/>
,	,	kIx,
Autodrom	autodrom	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
tj.	tj.	kA
tvoří	tvořit	k5eAaImIp3nS
páteřní	páteřní	k2eAgInSc4d1
systém	systém	k1gInSc4
tří	tři	k4xCgInPc2
největších	veliký	k2eAgInPc2d3
sportovních	sportovní	k2eAgInPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgInPc2d1
areálů	areál	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Most	most	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
přestavbou	přestavba	k1gFnSc7
dřívějšího	dřívější	k2eAgNnSc2d1
koupaliště	koupaliště	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
otevřeného	otevřený	k2eAgInSc2d1
vyhřívaného	vyhřívaný	k2eAgInSc2d1
bazénu	bazén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
areál	areál	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
města	město	k1gNnSc2
(	(	kIx(
<g/>
provozují	provozovat	k5eAaImIp3nP
jej	on	k3xPp3gMnSc4
Technické	technický	k2eAgFnPc1d1
služby	služba	k1gFnPc1
Most	most	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
také	také	k9
financovalo	financovat	k5eAaBmAgNnS
přestavbu	přestavba	k1gFnSc4
bývalého	bývalý	k2eAgNnSc2d1
koupaliště	koupaliště	k1gNnSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
Topolová	Topolová	k1gFnSc1
na	na	k7c4
aquapark	aquapark	k1gInSc4
za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
270	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Areál	areál	k1gInSc1
kromě	kromě	k7c2
možnosti	možnost	k1gFnSc2
koupání	koupání	k1gNnSc1
v	v	k7c6
několika	několik	k4yIc6
krytých	krytý	k2eAgFnPc6d1
a	a	k8xC
třech	tři	k4xCgInPc6
otevřených	otevřený	k2eAgInPc6d1
bazénech	bazén	k1gInPc6
nabízí	nabízet	k5eAaImIp3nS
také	také	k9
saunu	sauna	k1gFnSc4
<g/>
,	,	kIx,
fitness	fitness	k1gInSc1
<g/>
,	,	kIx,
solárium	solárium	k1gNnSc1
<g/>
,	,	kIx,
minigolf	minigolf	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
sportovních	sportovní	k2eAgNnPc2d1
hřišť	hřiště	k1gNnPc2
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
,	,	kIx,
nohejbal	nohejbal	k1gInSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
kopaná	kopaná	k1gFnSc1
a	a	k8xC
pétanque	pétanque	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkovní	venkovní	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
poskytují	poskytovat	k5eAaImIp3nP
celkem	celkem	k6eAd1
40	#num#	k4
524	#num#	k4
metrů	metr	k1gInPc2
čtverečních	čtvereční	k2eAgFnPc2d1
travnatých	travnatý	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekonstrukci	rekonstrukce	k1gFnSc4
a	a	k8xC
přestavbu	přestavba	k1gFnSc4
zajišťovala	zajišťovat	k5eAaImAgFnS
firma	firma	k1gFnSc1
Metrostav	metrostav	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistika	statistika	k1gFnSc1
</s>
<s>
návštěvnost	návštěvnost	k1gFnSc1
aquadromu	aquadrom	k1gInSc2
<g/>
:	:	kIx,
272	#num#	k4
140	#num#	k4
(	(	kIx(
<g/>
+	+	kIx~
cca	cca	kA
15	#num#	k4
000	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
pravidelných	pravidelný	k2eAgInPc2d1
tréninků	trénink	k1gInPc2
<g/>
,	,	kIx,
plavců	plavec	k1gMnPc2
a	a	k8xC
potápěčů	potápěč	k1gMnPc2
a	a	k8xC
členové	člen	k1gMnPc1
rodinného	rodinný	k2eAgInSc2d1
vstupné	vstupné	k1gNnSc4
<g/>
)	)	kIx)
2007	#num#	k4
</s>
<s>
návštěvnost	návštěvnost	k1gFnSc1
sauny	sauna	k1gFnSc2
<g/>
:	:	kIx,
16	#num#	k4
542	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
5	#num#	k4
881	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
objem	objem	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
kryté	krytý	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
:	:	kIx,
1	#num#	k4
150	#num#	k4
m³	m³	k?
(	(	kIx(
<g/>
bazény	bazén	k1gInPc1
a	a	k8xC
vnitřní	vnitřní	k2eAgFnPc1d1
jímky	jímka	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
travnaté	travnatý	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
<g/>
:	:	kIx,
40	#num#	k4
525	#num#	k4
m²	m²	k?
</s>
<s>
vnitřní	vnitřní	k2eAgInSc1d1
areál	areál	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
bazény	bazén	k1gInPc4
(	(	kIx(
<g/>
1	#num#	k4
X	X	kA
25	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
tobogány	tobogán	k1gInPc4
</s>
<s>
venkovní	venkovní	k2eAgInSc1d1
areál	areál	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
bazény	bazén	k1gInPc1
</s>
<s>
1	#num#	k4
skluzavka	skluzavka	k1gFnSc1
</s>
<s>
Mokré	Mokré	k2eAgNnSc1d1
občerstvení	občerstvení	k1gNnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
345	#num#	k4
prodaných	prodaný	k2eAgInPc2d1
hamburgerů	hamburger	k1gInPc2
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
6	#num#	k4
895	#num#	k4
prodaných	prodaný	k2eAgInPc2d1
langošů	langoš	k1gInPc2
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
10	#num#	k4
465	#num#	k4
prodaných	prodaný	k2eAgInPc2d1
párků	párek	k1gInPc2
v	v	k7c6
rohlíku	rohlík	k1gInSc6
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
9	#num#	k4
284	#num#	k4
prodaných	prodaný	k2eAgFnPc2d1
porcí	porce	k1gFnPc2
hranolek	hranolek	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Metrostav	metrostav	k1gInSc1
<g/>
,	,	kIx,
Aquadrom	Aquadrom	k1gInSc1
<g/>
.	.	kIx.
80.250.10.17	80.250.10.17	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Mostecký	mostecký	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
↑	↑	k?
-	-	kIx~
Vodní	vodní	k2eAgInSc1d1
park	park	k1gInSc1
Aquadrom	Aquadrom	k1gInSc1
Most	most	k1gInSc1
<g/>
↑	↑	k?
-	-	kIx~
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
mostu	most	k1gInSc2
<g/>
.	.	kIx.
www.mumost.cz	www.mumost.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
-	-	kIx~
Aquadrom	Aquadrom	k1gInSc1
Most	most	k1gInSc1
(	(	kIx(
<g/>
turistik	turistika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
pruvodce	pruvodce	k1gMnSc1
<g/>
.	.	kIx.
<g/>
turistik	turistika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
Aquadromu	Aquadrom	k1gInSc2
Most	most	k1gInSc1
</s>
<s>
O	o	k7c6
Aquadromu	Aquadrom	k1gInSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
města	město	k1gNnSc2
Mostu	most	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
|	|	kIx~
Most	most	k1gInSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
