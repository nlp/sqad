<s>
Ivan	Ivan	k1gMnSc1	Ivan
Miloš	Miloš	k1gMnSc1	Miloš
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
teoretická	teoretický	k2eAgNnPc4d1	teoretické
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
společného	společný	k2eAgNnSc2d1	společné
pracoviště	pracoviště	k1gNnSc2	pracoviště
UK	UK	kA	UK
a	a	k8xC	a
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bratrem	bratr	k1gMnSc7	bratr
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c7	mezi
zakladatele	zakladatel	k1gMnSc2	zakladatel
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
obor	obor	k1gInSc4	obor
Automatizace	automatizace	k1gFnSc2	automatizace
a	a	k8xC	a
počítače	počítač	k1gInSc2	počítač
na	na	k7c6	na
Elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
fakultě	fakulta	k1gFnSc6	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgInS	habilitovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Centru	centr	k1gInSc6	centr
pro	pro	k7c4	pro
teoretická	teoretický	k2eAgNnPc4d1	teoretické
studia	studio	k1gNnPc4	studio
při	při	k7c6	při
UK	UK	kA	UK
a	a	k8xC	a
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
mj.	mj.	kA	mj.
kybernetikou	kybernetika	k1gFnSc7	kybernetika
<g/>
,	,	kIx,	,
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
robotikou	robotika	k1gFnSc7	robotika
<g/>
,	,	kIx,	,
kognitivními	kognitivní	k2eAgFnPc7d1	kognitivní
vědami	věda	k1gFnPc7	věda
a	a	k8xC	a
filozofickými	filozofický	k2eAgFnPc7d1	filozofická
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
obory	obor	k1gInPc7	obor
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
vede	vést	k5eAaImIp3nS	vést
dvousemestrální	dvousemestrální	k2eAgInSc1d1	dvousemestrální
transdisciplinární	transdisciplinární	k2eAgInSc1d1	transdisciplinární
seminář	seminář	k1gInSc1	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
M.	M.	kA	M.
Havel	Havel	k1gMnSc1	Havel
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
-	-	kIx~	-
Nadace	nadace	k1gFnSc1	nadace
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Nadace	nadace	k1gFnSc2	nadace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
sepsal	sepsat	k5eAaPmAgMnS	sepsat
krátkou	krátký	k2eAgFnSc4d1	krátká
prózu	próza	k1gFnSc4	próza
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Arsemid	Arsemid	k1gInSc4	Arsemid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
hned	hned	k6eAd1	hned
tří	tři	k4xCgNnPc2	tři
knižních	knižní	k2eAgNnPc2d1	knižní
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Cena	cena	k1gFnSc1	cena
Václava	Václav	k1gMnSc2	Václav
Bendy	Benda	k1gMnSc2	Benda
<g/>
.	.	kIx.	.
</s>
