<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
der	drát	k5eAaImRp2nS	drát
Erste	Erst	k1gMnSc5	Erst
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
první	první	k4xOgInSc4	první
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Nieder-Ingelheim	Nieder-Ingelheim	k1gMnSc1	Nieder-Ingelheim
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Ingelheimu	Ingelheim	k1gInSc6	Ingelheim
chemickou	chemický	k2eAgFnSc4d1	chemická
továrnu	továrna	k1gFnSc4	továrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
farmaceutického	farmaceutický	k2eAgInSc2d1	farmaceutický
koncernu	koncern	k1gInSc2	koncern
Boehringer	Boehringer	k1gMnSc1	Boehringer
Ingelheim	Ingelheim	k1gMnSc1	Ingelheim
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
vlastněn	vlastněn	k2eAgInSc1d1	vlastněn
členy	člen	k1gInPc7	člen
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
byl	být	k5eAaImAgMnS	být
vnukem	vnuk	k1gMnSc7	vnuk
Christiana	Christian	k1gMnSc2	Christian
Boehringera	Boehringer	k1gMnSc2	Boehringer
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Friedrich	Friedrich	k1gMnSc1	Friedrich
Boehringer	Boehringer	k1gMnSc1	Boehringer
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
farmaceutickou	farmaceutický	k2eAgFnSc4d1	farmaceutická
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
později	pozdě	k6eAd2	pozdě
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Mannheimu	Mannheim	k1gInSc2	Mannheim
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
následnická	následnický	k2eAgFnSc1d1	následnická
firma	firma	k1gFnSc1	firma
Boehringer	Boehringer	k1gMnSc1	Boehringer
Mannheim	Mannheim	k1gInSc1	Mannheim
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
převzata	převzít	k5eAaPmNgFnS	převzít
skupinou	skupina	k1gFnSc7	skupina
Hoffmann-La	Hoffmann-Lus	k1gMnSc2	Hoffmann-Lus
Roche	Roch	k1gMnSc2	Roch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boehringer	Boehringer	k1gMnSc1	Boehringer
Ingelheim	Ingelheim	k1gMnSc1	Ingelheim
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
malá	malý	k2eAgFnSc1d1	malá
ingelheimské	ingelheimský	k2eAgFnPc1d1	ingelheimský
továrna	továrna	k1gFnSc1	továrna
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
vinný	vinný	k2eAgInSc4d1	vinný
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
organické	organický	k2eAgFnSc2d1	organická
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
:	:	kIx,	:
vinnou	vinný	k2eAgFnSc7d1	vinná
<g/>
,	,	kIx,	,
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
a	a	k8xC	a
citrónovou	citrónový	k2eAgFnSc7d1	citrónová
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
objevil	objevit	k5eAaPmAgInS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kyselina	kyselina	k1gFnSc1	kyselina
mléčná	mléčný	k2eAgFnSc1d1	mléčná
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
vyrábět	vyrábět	k5eAaImF	vyrábět
biotechnologicky	biotechnologicky	k6eAd1	biotechnologicky
<g/>
,	,	kIx,	,
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
začala	začít	k5eAaPmAgFnS	začít
zabývat	zabývat	k5eAaImF	zabývat
výrobou	výroba	k1gFnSc7	výroba
farmaceutických	farmaceutický	k2eAgFnPc2d1	farmaceutická
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
trvale	trvale	k6eAd1	trvale
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
už	už	k6eAd1	už
1500	[number]	k4	1500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
odpovědnosti	odpovědnost	k1gFnPc4	odpovědnost
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
podnikové	podnikový	k2eAgNnSc4d1	podnikové
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
14	[number]	k4	14
dní	den	k1gInPc2	den
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
,	,	kIx,	,
příspěvek	příspěvek	k1gInSc4	příspěvek
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
podnikové	podnikový	k2eAgNnSc4d1	podnikové
penzijní	penzijní	k2eAgNnSc4d1	penzijní
spoření	spoření	k1gNnSc4	spoření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
Boehringerovy	Boehringerův	k2eAgFnPc4d1	Boehringerův
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
užití	užití	k1gNnSc2	užití
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
padesátých	padesátý	k4xOgFnPc6	padesátý
narozeninách	narozeniny	k1gFnPc6	narozeniny
udělen	udělen	k2eAgInSc1d1	udělen
čestný	čestný	k2eAgInSc1d1	čestný
titul	titul	k1gInSc1	titul
komerční	komerční	k2eAgFnSc1d1	komerční
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
obce	obec	k1gFnSc2	obec
Nieder-Ingelheim	Nieder-Ingelheim	k1gMnSc1	Nieder-Ingelheim
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
matematicko-přírodovědecké	matematickořírodovědecký	k2eAgFnSc2d1	matematicko-přírodovědecký
fakulty	fakulta	k1gFnSc2	fakulta
Freiburské	freiburský	k2eAgFnSc2d1	Freiburská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
jej	on	k3xPp3gMnSc4	on
Ludwig-Maximilians-Universität	Ludwig-Maximilians-Universität	k1gInSc4	Ludwig-Maximilians-Universität
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Corps	corps	k1gInSc4	corps
Franconia	Franconium	k1gNnSc2	Franconium
<g/>
,	,	kIx,	,
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
"	"	kIx"	"
<g/>
občanem	občan	k1gMnSc7	občan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ehrenbürger	Ehrenbürger	k1gInSc1	Ehrenbürger
der	drát	k5eAaImRp2nS	drát
Universität	Universität	k1gInSc1	Universität
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následníci	následník	k1gMnPc5	následník
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
firmy	firma	k1gFnSc2	firma
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
firmy	firma	k1gFnSc2	firma
převzali	převzít	k5eAaPmAgMnP	převzít
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Albert	Alberta	k1gFnPc2	Alberta
(	(	kIx(	(
<g/>
der	drát	k5eAaImRp2nS	drát
Zweite	Zweit	k1gMnSc5	Zweit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc4	druhý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
Boehringer	Boehringer	k1gMnSc1	Boehringer
společně	společně	k6eAd1	společně
se	s	k7c7	s
zetěm	zeť	k1gMnSc7	zeť
Juliem	Julius	k1gMnSc7	Julius
Liebrechtem	Liebrecht	k1gMnSc7	Liebrecht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Margarete	Margarat	k5eAaPmIp2nP	Margarat
Köhler	Köhler	k1gInSc4	Köhler
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hartmut	Hartmut	k1gMnSc1	Hartmut
Geißler	Geißler	k1gMnSc1	Geißler
(	(	kIx(	(
<g/>
Ergänzung	Ergänzung	k1gInSc1	Ergänzung
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Kommerzienrat	Kommerzienrat	k1gInSc1	Kommerzienrat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
Albert	Albert	k1gMnSc1	Albert
Boehringer	Boehringer	k1gMnSc1	Boehringer
ingelheimer-geschichte	ingelheimereschicht	k1gInSc5	ingelheimer-geschicht
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
</s>
</p>
