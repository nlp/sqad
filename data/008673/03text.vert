<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
i	i	k8xC	i
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
transkripce	transkripce	k1gFnSc1	transkripce
ruského	ruský	k2eAgMnSc2d1	ruský
Míša	Míša	k1gFnSc1	Míša
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
domácká	domácký	k2eAgFnSc1d1	domácká
forma	forma	k1gFnSc1	forma
ruského	ruský	k2eAgNnSc2d1	ruské
jména	jméno	k1gNnSc2	jméno
Michail	Michaila	k1gFnPc2	Michaila
nebo	nebo	k8xC	nebo
českého	český	k2eAgInSc2d1	český
Michal	Michal	k1gMnSc1	Michal
(	(	kIx(	(
<g/>
z	z	k7c2	z
původně	původně	k6eAd1	původně
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
Michael	Michael	k1gMnSc1	Michael
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Bůh	bůh	k1gMnSc1	bůh
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bohu	bůh	k1gMnSc3	bůh
podobný	podobný	k2eAgInSc4d1	podobný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
do	do	k7c2	do
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muži	muž	k1gMnPc1	muž
==	==	k?	==
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Alexander	Alexandra	k1gFnPc2	Alexandra
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Auer	Auer	k1gMnSc1	Auer
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Bakaleinikoff	Bakaleinikoff	k1gMnSc1	Bakaleinikoff
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Berlinski	Berlinsk	k1gFnSc2	Berlinsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
prvním	první	k4xOgInSc7	první
a	a	k8xC	a
nejznámějším	známý	k2eAgInSc7d3	nejznámější
románem	román	k1gInSc7	román
Fieldwork	Fieldwork	k1gInSc4	Fieldwork
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
finalistou	finalista	k1gMnSc7	finalista
o	o	k7c4	o
Národní	národní	k2eAgFnSc4d1	národní
knižní	knižní	k2eAgFnSc4d1	knižní
cenu	cena	k1gFnSc4	cena
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Elman	Elman	k1gMnSc1	Elman
(	(	kIx(	(
<g/>
Mikhail	Mikhail	k1gMnSc1	Mikhail
Saulovich	Saulovich	k1gMnSc1	Saulovich
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
židovský	židovský	k2eAgMnSc1d1	židovský
houslista	houslista	k1gMnSc1	houslista
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Hausserman	Hausserman	k1gMnSc1	Hausserman
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1941	[number]	k4	1941
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Keijser	Keijser	k1gMnSc1	Keijser
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
holandský	holandský	k2eAgMnSc1d1	holandský
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
fotožurnalista	fotožurnalista	k1gMnSc1	fotožurnalista
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Maisky	Maiska	k1gFnSc2	Maiska
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
violoncellista	violoncellista	k1gMnSc1	violoncellista
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Mischakoff	Mischakoff	k1gMnSc1	Mischakoff
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgMnSc1d1	koncertní
mistr	mistr	k1gMnSc1	mistr
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Richter	Richter	k1gMnSc1	Richter
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karikaturista	karikaturista	k1gMnSc1	karikaturista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
Strictly	Strictly	k1gMnSc1	Strictly
Richter	Richter	k1gMnSc1	Richter
a	a	k8xC	a
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Zverev	Zverev	k1gFnSc1	Zverev
(	(	kIx(	(
<g/>
Mikhail	Mikhail	k1gInSc1	Mikhail
Zverev	Zverva	k1gFnPc2	Zverva
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenisový	tenisový	k2eAgMnSc1d1	tenisový
hráč	hráč	k1gMnSc1	hráč
</s>
</p>
<p>
<s>
==	==	k?	==
Ženy	žena	k1gFnPc1	žena
==	==	k?	==
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Bartonová	Bartonový	k2eAgFnSc1d1	Bartonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
==	==	k?	==
Fiktivní	fiktivní	k2eAgFnPc4d1	fiktivní
postavy	postava	k1gFnPc4	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
kocour	kocour	k1gMnSc1	kocour
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
The	The	k1gFnSc2	The
Story	story	k1gFnSc2	story
of	of	k?	of
Cinderella	Cinderell	k1gMnSc2	Cinderell
<g/>
,	,	kIx,	,
japonského	japonský	k2eAgMnSc2d1	japonský
anime	anim	k1gInSc5	anim
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
od	od	k7c2	od
Hiroshiho	Hiroshi	k1gMnSc2	Hiroshi
Sasagawu	Sasagawus	k1gInSc2	Sasagawus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gMnSc1	Mischa
Lecter	Lecter	k1gMnSc1	Lecter
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
Hannibala	Hannibal	k1gMnSc2	Hannibal
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
Hannibala	Hannibal	k1gMnSc2	Hannibal
Lectera	Lecter	k1gMnSc2	Lecter
</s>
</p>
<p>
<s>
Mischa	Mischa	k1gFnSc1	Mischa
Vorfren	Vorfrna	k1gFnPc2	Vorfrna
<g/>
,	,	kIx,	,
Anzat	Anzat	k2eAgInSc4d1	Anzat
z	z	k7c2	z
populárního	populární	k2eAgInSc2d1	populární
Star	Star	kA	Star
Wars	Warsa	k1gFnPc2	Warsa
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
</s>
</p>
<p>
<s>
Misha	Misha	k1gFnSc1	Misha
</s>
</p>
