<s>
Ostrov	ostrov	k1gInSc1	ostrov
Mauricius	Mauricius	k1gInSc1	Mauricius
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
korálovými	korálový	k2eAgInPc7d1	korálový
útesy	útes	k1gInPc7	útes
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
malých	malý	k2eAgInPc2d1	malý
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
