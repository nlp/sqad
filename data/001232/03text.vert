<s>
Ronald	Ronald	k1gMnSc1	Ronald
Wilson	Wilson	k1gMnSc1	Wilson
Reagan	Reagan	k1gMnSc1	Reagan
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1911	[number]	k4	1911
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
40	[number]	k4	40
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
-	-	kIx~	-
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
33	[number]	k4	33
<g/>
.	.	kIx.	.
guvernér	guvernér	k1gMnSc1	guvernér
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
-	-	kIx~	-
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
předsedou	předseda	k1gMnSc7	předseda
hereckých	herecký	k2eAgInPc2d1	herecký
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
prezidentství	prezidentství	k1gNnSc1	prezidentství
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xC	jako
počátek	počátek	k1gInSc1	počátek
politické	politický	k2eAgFnSc2d1	politická
dominance	dominance	k1gFnSc2	dominance
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
amerického	americký	k2eAgInSc2d1	americký
konzervatismu	konzervatismus	k1gInSc2	konzervatismus
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
prezidentství	prezidentství	k1gNnSc4	prezidentství
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
nová	nový	k2eAgFnSc1d1	nová
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Reaganomika	Reaganomika	k1gFnSc1	Reaganomika
<g/>
,	,	kIx,	,
a	a	k8xC	a
konfrontační	konfrontační	k2eAgFnSc1d1	konfrontační
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
komunistickému	komunistický	k2eAgNnSc3d1	komunistické
hnutí	hnutí	k1gNnSc3	hnutí
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
drtivým	drtivý	k2eAgInSc7d1	drtivý
poměrem	poměr	k1gInSc7	poměr
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Cartera	Carter	k1gMnSc2	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
získal	získat	k5eAaPmAgMnS	získat
489	[number]	k4	489
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Carter	Carter	k1gInSc1	Carter
jen	jen	k9	jen
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
69	[number]	k4	69
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejstarším	starý	k2eAgMnSc7d3	nejstarší
prezidentem	prezident	k1gMnSc7	prezident
zvoleným	zvolený	k2eAgFnPc3d1	zvolená
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
republikánem	republikán	k1gMnSc7	republikán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
dokázal	dokázat	k5eAaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
demokratického	demokratický	k2eAgMnSc4d1	demokratický
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
porazit	porazit	k5eAaPmF	porazit
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
prezidenta	prezident	k1gMnSc4	prezident
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
stranické	stranický	k2eAgFnPc4d1	stranická
příslušnosti	příslušnost	k1gFnPc4	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
demokratickým	demokratický	k2eAgMnSc7d1	demokratický
kandidátem	kandidát	k1gMnSc7	kandidát
Walterem	Walter	k1gMnSc7	Walter
Mondalem	Mondal	k1gMnSc7	Mondal
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
volebních	volební	k2eAgInPc6d1	volební
obvodech	obvod	k1gInPc6	obvod
kromě	kromě	k7c2	kromě
Minnesoty	Minnesota	k1gFnSc2	Minnesota
a	a	k8xC	a
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
přes	přes	k7c4	přes
60	[number]	k4	60
procent	procent	k1gInSc4	procent
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c4	v
Bel-Air	Bel-Air	k1gInSc4	Bel-Air
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
93	[number]	k4	93
let	léto	k1gNnPc2	léto
na	na	k7c4	na
následky	následek	k1gInPc4	následek
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
trpěl	trpět	k5eAaImAgMnS	trpět
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1911	[number]	k4	1911
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tampico	Tampico	k6eAd1	Tampico
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
synů	syn	k1gMnPc2	syn
Johna	John	k1gMnSc2	John
Edwarda	Edward	k1gMnSc2	Edward
Reagana	Reagan	k1gMnSc2	Reagan
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irsko-amerického	irskomerický	k2eAgMnSc4d1	irsko-americký
katolíka	katolík	k1gMnSc4	katolík
a	a	k8xC	a
Nelle	Nell	k1gMnSc4	Nell
Clyde	Clyd	k1gInSc5	Clyd
Wilson	Wilson	k1gMnSc1	Wilson
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
předky	předek	k1gMnPc4	předek
skotského	skotský	k1gInSc2	skotský
<g/>
,	,	kIx,	,
kanadského	kanadský	k2eAgInSc2d1	kanadský
a	a	k8xC	a
anglického	anglický	k2eAgInSc2d1	anglický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
"	"	kIx"	"
<g/>
Disciples	Disciples	k1gInSc1	Disciples
of	of	k?	of
Christ	Christ	k1gInSc1	Christ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
Neil	Neil	k1gMnSc1	Neil
Reagan	Reagan	k1gMnSc1	Reagan
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
pradědeček	pradědeček	k1gMnSc1	pradědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
Ballyporeenu	Ballyporeen	k1gInSc2	Ballyporeen
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
také	také	k9	také
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc3	jejich
příjmení	příjmení	k1gNnSc1	příjmení
psalo	psát	k5eAaImAgNnS	psát
Regan	Regan	k1gInSc4	Regan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
pradědeček	pradědeček	k1gMnSc1	pradědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
imigroval	imigrovat	k5eAaBmAgMnS	imigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
Paisley	Paislea	k1gFnSc2	Paislea
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
Jane	Jan	k1gMnSc5	Jan
Blue	Bluus	k1gMnSc5	Bluus
<g/>
,	,	kIx,	,
Kanaďanku	Kanaďanka	k1gFnSc4	Kanaďanka
z	z	k7c2	z
Queensu	Queens	k1gInSc2	Queens
v	v	k7c6	v
New	New	k1gFnSc6	New
Brunswicku	Brunswick	k1gInSc2	Brunswick
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc2	Mary
Anne	Ann	k1gFnSc2	Ann
Elsey	Elsea	k1gFnSc2	Elsea
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Epsomu	Epsom	k1gInSc3	Epsom
v	v	k7c4	v
Surrey	Surrea	k1gFnPc4	Surrea
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Reaganův	Reaganův	k2eAgMnSc1d1	Reaganův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
katolík	katolík	k1gMnSc1	katolík
<g/>
,	,	kIx,	,
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
protestantka	protestantka	k1gFnSc1	protestantka
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
podle	podle	k7c2	podle
učení	učení	k1gNnSc2	učení
"	"	kIx"	"
<g/>
Disciples	Disciples	k1gInSc1	Disciples
of	of	k?	of
Christ	Christ	k1gInSc1	Christ
<g/>
"	"	kIx"	"
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
církevní	církevní	k2eAgFnSc4d1	církevní
školu	škola	k1gFnSc4	škola
Eureka	Eureek	k1gMnSc2	Eureek
College	Colleg	k1gMnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
nezaměstnaný	zaměstnaný	k2eNgMnSc1d1	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
díky	díky	k7c3	díky
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
programu	program	k1gInSc3	program
WPA	WPA	kA	WPA
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
New	New	k1gFnPc2	New
Dealu	Deal	k1gInSc2	Deal
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
krátce	krátce	k6eAd1	krátce
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
městech	město	k1gNnPc6	město
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Illinois	Illinois	k1gInSc1	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
distriktu	distrikt	k1gInSc6	distrikt
Mount	Mount	k1gMnSc1	Mount
Lebanon	Lebanon	k1gMnSc1	Lebanon
a	a	k8xC	a
na	na	k7c4	na
Dixon	Dixon	k1gInSc4	Dixon
High	High	k1gInSc1	High
School	School	k1gInSc1	School
v	v	k7c6	v
Dixonu	Dixon	k1gInSc6	Dixon
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svůj	svůj	k3xOyFgInSc4	svůj
vypravěčský	vypravěčský	k2eAgInSc4d1	vypravěčský
a	a	k8xC	a
herecký	herecký	k2eAgInSc4d1	herecký
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nováčků	nováček	k1gMnPc2	nováček
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
projev	projev	k1gInSc4	projev
během	během	k7c2	během
nočního	noční	k2eAgNnSc2d1	noční
setkání	setkání	k1gNnSc2	setkání
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
studentské	studentský	k2eAgFnSc2d1	studentská
stávky	stávka	k1gFnSc2	stávka
na	na	k7c4	na
Eureka	Eureek	k1gMnSc4	Eureek
College	Colleg	k1gMnSc4	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
začal	začít	k5eAaPmAgMnS	začít
Reagan	Reagan	k1gMnSc1	Reagan
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
plavčík	plavčík	k1gMnSc1	plavčík
v	v	k7c4	v
Lowell	Lowell	k1gInSc4	Lowell
Parku	park	k1gInSc2	park
poblíž	poblíž	k7c2	poblíž
Dixonu	Dixon	k1gInSc2	Dixon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisována	připisován	k2eAgFnSc1d1	připisována
záchrana	záchrana	k1gFnSc1	záchrana
77	[number]	k4	77
lidí	člověk	k1gMnPc2	člověk
během	během	k7c2	během
sedmi	sedm	k4xCc2	sedm
letních	letní	k2eAgFnPc2d1	letní
sézón	sézón	k1gInSc1	sézón
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
tam	tam	k6eAd1	tam
pracoval	pracovat	k5eAaImAgInS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
na	na	k7c4	na
Eureka	Eureek	k1gMnSc4	Eureek
College	Colleg	k1gMnSc4	Colleg
(	(	kIx(	(
<g/>
B.A.	B.A.	k1gMnSc4	B.A.
z	z	k7c2	z
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Reagan	Reagan	k1gMnSc1	Reagan
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
WOC	WOC	kA	WOC
v	v	k7c6	v
Davenportu	Davenport	k1gInSc6	Davenport
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c6	v
WHO	WHO	kA	WHO
v	v	k7c6	v
Des	des	k1gNnSc6	des
Moines	Moinesa	k1gFnPc2	Moinesa
jako	jako	k8xS	jako
komentátor	komentátor	k1gMnSc1	komentátor
baseballových	baseballový	k2eAgInPc2d1	baseballový
zápasů	zápas	k1gInPc2	zápas
Chicago	Chicago	k1gNnSc1	Chicago
Cubs	Cubs	k1gInSc4	Cubs
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dostával	dostávat	k5eAaImAgMnS	dostávat
jen	jen	k9	jen
strohé	strohý	k2eAgFnPc4d1	strohá
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
přes	přes	k7c4	přes
telegraf	telegraf	k1gInSc4	telegraf
a	a	k8xC	a
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
své	svůj	k3xOyFgFnSc2	svůj
fantazie	fantazie	k1gFnSc2	fantazie
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
domyslet	domyslet	k5eAaPmF	domyslet
zbytek	zbytek	k1gInSc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
během	během	k7c2	během
deváté	devátý	k4xOgFnSc2	devátý
směny	směna	k1gFnSc2	směna
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
telegrafu	telegraf	k1gInSc2	telegraf
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
rychle	rychle	k6eAd1	rychle
improvizoval	improvizovat	k5eAaImAgMnS	improvizovat
a	a	k8xC	a
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
si	se	k3xPyFc3	se
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
vývoj	vývoj	k1gInSc4	vývoj
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
pálkaři	pálkař	k1gMnPc1	pálkař
obou	dva	k4xCgInPc2	dva
týmů	tým	k1gInPc2	tým
vyautovali	vyautovat	k5eAaPmAgMnP	vyautovat
několik	několik	k4yIc4	několik
nadhazovačů	nadhazovač	k1gMnPc2	nadhazovač
<g/>
)	)	kIx)	)
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
spojení	spojení	k1gNnSc1	spojení
neobnovilo	obnovit	k5eNaPmAgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
když	když	k8xS	když
jel	jet	k5eAaImAgMnS	jet
do	do	k7c2	do
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podával	podávat	k5eAaImAgMnS	podávat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
jarním	jarní	k2eAgInSc6d1	jarní
tréninku	trénink	k1gInSc6	trénink
týmu	tým	k1gInSc2	tým
Chicago	Chicago	k1gNnSc4	Chicago
Cubs	Cubsa	k1gFnPc2	Cubsa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Reagan	Reagan	k1gMnSc1	Reagan
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
kamerové	kamerový	k2eAgFnPc4d1	kamerová
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yRgFnSc3	který
získal	získat	k5eAaPmAgMnS	získat
sedmiletý	sedmiletý	k2eAgInSc4d1	sedmiletý
kontrakt	kontrakt	k1gInSc4	kontrakt
u	u	k7c2	u
Warner	Warnra	k1gFnPc2	Warnra
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
příjemné	příjemný	k2eAgNnSc1d1	příjemné
vystupování	vystupování	k1gNnSc1	vystupování
a	a	k8xC	a
atletická	atletický	k2eAgFnSc1d1	atletická
postava	postava	k1gFnSc1	postava
jej	on	k3xPp3gMnSc4	on
udělaly	udělat	k5eAaPmAgFnP	udělat
populárním	populární	k2eAgInPc3d1	populární
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
filmů	film	k1gInPc2	film
hrál	hrát	k5eAaImAgMnS	hrát
vůdčí	vůdčí	k2eAgInPc4d1	vůdčí
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
"	"	kIx"	"
<g/>
béčkovými	béčkový	k2eAgMnPc7d1	béčkový
<g/>
"	"	kIx"	"
filmy	film	k1gInPc4	film
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
propracoval	propracovat	k5eAaPmAgInS	propracovat
až	až	k9	až
k	k	k7c3	k
"	"	kIx"	"
<g/>
áčkovým	áčkův	k2eAgInPc3d1	áčkův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
roli	role	k1gFnSc4	role
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Love	lov	k1gInSc5	lov
Is	Is	k1gMnSc1	Is
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Air	Air	k1gMnSc3	Air
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
devatenácti	devatenáct	k4xCc2	devatenáct
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
filmem	film	k1gInSc7	film
Sante	Sant	k1gInSc5	Sant
Fe	Fe	k1gFnPc3	Fe
Trail	Trail	k1gInSc4	Trail
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
hrál	hrát	k5eAaImAgMnS	hrát
roli	role	k1gFnSc4	role
George	Georg	k1gMnSc2	Georg
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Gripper	Gripper	k1gMnSc1	Gripper
<g/>
"	"	kIx"	"
Gippa	Gippa	k1gFnSc1	Gippa
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Knute	Knut	k1gMnSc5	Knut
Rockne	Rockne	k1gMnSc5	Rockne
<g/>
,	,	kIx,	,
All	All	k1gMnSc1	All
American	American	k1gMnSc1	American
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
roli	role	k1gFnSc3	role
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
the	the	k?	the
Gripper	Gripper	k1gInSc1	Gripper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
provázela	provázet	k5eAaImAgFnS	provázet
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kings	Kingsa	k1gFnPc2	Kingsa
Row	Row	k1gFnPc2	Row
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
hrál	hrát	k5eAaImAgMnS	hrát
mladíka	mladík	k1gMnSc2	mladík
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
byly	být	k5eAaImAgFnP	být
amputovány	amputován	k2eAgFnPc1d1	amputována
nohy	noha	k1gFnPc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Hlášku	hláška	k1gFnSc4	hláška
z	z	k7c2	z
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Where	Wher	k1gInSc5	Wher
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
rest	rest	k6eAd1	rest
of	of	k?	of
me	me	k?	me
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
název	název	k1gInSc1	název
své	svůj	k3xOyFgFnSc2	svůj
autobiografie	autobiografie	k1gFnSc2	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
Reaganovy	Reaganův	k2eAgInPc4d1	Reaganův
filmy	film	k1gInPc4	film
patří	patřit	k5eAaImIp3nS	patřit
International	International	k1gMnSc1	International
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Partner	partner	k1gMnSc1	partner
<g/>
,	,	kIx,	,
Hellcats	Hellcats	k1gInSc1	Hellcats
of	of	k?	of
the	the	k?	the
Navy	Navy	k?	Navy
<g/>
,	,	kIx,	,
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
the	the	k?	the
Army	Arma	k1gMnSc2	Arma
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
Hasty	Hasta	k1gMnSc2	Hasta
Heart	Heart	k1gInSc4	Heart
<g/>
,	,	kIx,	,
Hong	Hong	k1gInSc4	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Winning	Winning	k1gInSc1	Winning
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
Bedtime	Bedtim	k1gInSc5	Bedtim
for	forum	k1gNnPc2	forum
Bonzo	Bonza	k1gFnSc5	Bonza
<g/>
,	,	kIx,	,
Cattle	Cattle	k1gFnPc1	Cattle
Queen	Queen	k1gInSc1	Queen
of	of	k?	of
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
Storm	Storm	k1gInSc1	Storm
Warning	Warning	k1gInSc1	Warning
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Killers	Killersa	k1gFnPc2	Killersa
a	a	k8xC	a
Prisoner	Prisonra	k1gFnPc2	Prisonra
of	of	k?	of
War	War	k1gFnPc2	War
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
herecké	herecký	k2eAgFnPc4d1	herecká
partnerky	partnerka	k1gFnPc4	partnerka
patřily	patřit	k5eAaImAgInP	patřit
Jane	Jan	k1gMnSc5	Jan
Wymanová	Wymanová	k1gFnSc1	Wymanová
<g/>
,	,	kIx,	,
Priscilla	Priscilla	k1gFnSc1	Priscilla
Laneová	Laneová	k1gFnSc1	Laneová
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
Sheridanová	Sheridanová	k1gFnSc1	Sheridanová
<g/>
,	,	kIx,	,
Viveca	Viveca	k1gFnSc1	Viveca
Lindfors	Lindforsa	k1gFnPc2	Lindforsa
<g/>
,	,	kIx,	,
Patricia	Patricius	k1gMnSc2	Patricius
Nealová	Nealová	k1gFnSc1	Nealová
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
Stanwycková	Stanwycková	k1gFnSc1	Stanwycková
<g/>
,	,	kIx,	,
Rhonda	Rhonda	k1gFnSc1	Rhonda
Flemingová	Flemingový	k2eAgFnSc1d1	Flemingová
<g/>
,	,	kIx,	,
Ginger	Ginger	k1gInSc1	Ginger
Rogersová	Rogersová	k1gFnSc1	Rogersová
<g/>
,	,	kIx,	,
Doris	Doris	k1gFnSc1	Doris
Dayová	Dayová	k1gFnSc1	Dayová
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc1	Nancy
Davisová	Davisový	k2eAgFnSc1d1	Davisová
a	a	k8xC	a
Angie	Angie	k1gFnSc1	Angie
Dickinsonová	Dickinsonová	k1gFnSc1	Dickinsonová
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
hrál	hrát	k5eAaImAgMnS	hrát
Reagan	Reagan	k1gMnSc1	Reagan
ve	v	k7c6	v
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
filmech	film	k1gInPc6	film
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moderoval	moderovat	k5eAaBmAgMnS	moderovat
General	General	k1gMnSc1	General
Electric	Electric	k1gMnSc1	Electric
Theater	Theater	k1gMnSc1	Theater
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
televizních	televizní	k2eAgNnPc6d1	televizní
dramatech	drama	k1gNnPc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
(	(	kIx(	(
<g/>
SAG	SAG	kA	SAG
<g/>
,	,	kIx,	,
hereckých	herecký	k2eAgInPc2d1	herecký
odborů	odbor	k1gInPc2	odbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
moderátora	moderátor	k1gMnSc2	moderátor
a	a	k8xC	a
uměleckého	umělecký	k2eAgMnSc2d1	umělecký
poradce	poradce	k1gMnSc2	poradce
General	General	k1gMnSc2	General
Electric	Electric	k1gMnSc1	Electric
Theater	Theater	k1gMnSc1	Theater
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
producentem	producent	k1gMnSc7	producent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
Reagan	Reagan	k1gMnSc1	Reagan
asi	asi	k9	asi
125	[number]	k4	125
000	[number]	k4	000
$	$	kIx~	$
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
=	=	kIx~	=
přibližně	přibližně	k6eAd1	přibližně
800	[number]	k4	800
000	[number]	k4	000
$	$	kIx~	$
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
role	role	k1gFnSc1	role
jako	jako	k8xS	jako
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
herce	herec	k1gMnSc2	herec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
show	show	k1gFnSc6	show
Death	Deatha	k1gFnPc2	Deatha
Valley	Vallea	k1gFnSc2	Vallea
Days	Daysa	k1gFnPc2	Daysa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
moderoval	moderovat	k5eAaBmAgMnS	moderovat
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
promítal	promítat	k5eAaImAgInS	promítat
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
The	The	k1gMnPc1	The
Killers	Killersa	k1gFnPc2	Killersa
<g/>
,	,	kIx,	,
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
remake	remake	k1gFnSc4	remake
dříve	dříve	k6eAd2	dříve
natočeného	natočený	k2eAgInSc2d1	natočený
filmu	film	k1gInSc2	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Ernesta	Ernest	k1gMnSc2	Ernest
Hemingwaye	Hemingway	k1gMnSc2	Hemingway
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrál	hrát	k5eAaImAgMnS	hrát
vůdce	vůdce	k1gMnSc1	vůdce
gangu	gang	k1gInSc2	gang
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
plánován	plánovat	k5eAaImNgInS	plánovat
jen	jen	k9	jen
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
produkovala	produkovat	k5eAaImAgFnS	produkovat
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gMnSc4	on
zcenzurovala	zcenzurovat	k5eAaPmAgFnS	zcenzurovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Reaganovými	Reaganův	k2eAgMnPc7d1	Reaganův
hereckými	herecký	k2eAgMnPc7d1	herecký
kolegy	kolega	k1gMnPc7	kolega
ve	v	k7c6	v
filmu	film	k1gInSc6	film
byli	být	k5eAaImAgMnP	být
John	John	k1gMnSc1	John
Cassavetes	Cassavetes	k1gMnSc1	Cassavetes
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc3	Lea
Marvin	Marvina	k1gFnPc2	Marvina
a	a	k8xC	a
Angie	Angie	k1gFnSc2	Angie
Dickinson	Dickinsona	k1gFnPc2	Dickinsona
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
celkem	celkem	k6eAd1	celkem
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
88	[number]	k4	88
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
čtrnáctidenních	čtrnáctidenní	k2eAgFnPc2d1	čtrnáctidenní
domácích	domácí	k2eAgFnPc2d1	domácí
vojenských	vojenský	k2eAgFnPc2d1	vojenská
studijí	studít	k5eAaPmIp3nP	studít
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
zařazen	zařazen	k2eAgInSc4d1	zařazen
do	do	k7c2	do
záloh	záloha	k1gFnPc2	záloha
a	a	k8xC	a
pověřen	pověřen	k2eAgMnSc1d1	pověřen
jako	jako	k8xS	jako
poručík	poručík	k1gMnSc1	poručík
v	v	k7c6	v
důstojnických	důstojnický	k2eAgInPc6d1	důstojnický
rezervních	rezervní	k2eAgInPc6d1	rezervní
sborech	sbor	k1gInPc6	sbor
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
poprvé	poprvé	k6eAd1	poprvé
povolán	povolán	k2eAgMnSc1d1	povolán
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
špatnému	špatný	k2eAgInSc3d1	špatný
zraku	zrak	k1gInSc3	zrak
(	(	kIx(	(
<g/>
astigmatismu	astigmatismus	k1gInSc2	astigmatismus
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
omezenou	omezený	k2eAgFnSc4d1	omezená
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbora	k1gFnPc2	Harbora
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
přidělen	přidělit	k5eAaPmNgMnS	přidělit
k	k	k7c3	k
First	First	k1gMnSc1	First
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Unit	Unita	k1gFnPc2	Unita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
dělala	dělat	k5eAaImAgFnS	dělat
tréninkové	tréninkový	k2eAgInPc4d1	tréninkový
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
mohl	moct	k5eAaImAgInS	moct
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
své	svůj	k3xOyFgFnPc4	svůj
filmové	filmový	k2eAgFnPc4d1	filmová
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
si	se	k3xPyFc3	se
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
vzal	vzít	k5eAaPmAgMnS	vzít
herečku	herečka	k1gFnSc4	herečka
Jane	Jan	k1gMnSc5	Jan
Wyman	Wyman	k1gInSc4	Wyman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Maureen	Maureen	k1gInSc1	Maureen
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
adoptovali	adoptovat	k5eAaPmAgMnP	adoptovat
syna	syn	k1gMnSc4	syn
Michaela	Michael	k1gMnSc4	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
Christine	Christin	k1gMnSc5	Christin
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
hned	hned	k6eAd1	hned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1952	[number]	k4	1952
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Nancy	Nancy	k1gFnSc7	Nancy
Davis	Davis	k1gFnSc7	Davis
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Patti	Patť	k1gFnSc2	Patť
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Ron	Ron	k1gMnSc1	Ron
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
demokrat	demokrat	k1gMnSc1	demokrat
<g/>
,	,	kIx,	,
stoupenec	stoupenec	k1gMnSc1	stoupenec
Franklina	Franklina	k1gFnSc1	Franklina
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
a	a	k8xC	a
programu	program	k1gInSc2	program
New	New	k1gMnSc1	New
Deal	Deal	k1gMnSc1	Deal
a	a	k8xC	a
celoživotní	celoživotní	k2eAgMnSc1d1	celoživotní
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
Rooseveltových	Rooseveltových	k2eAgFnPc2d1	Rooseveltových
vůdcovských	vůdcovský	k2eAgFnPc2d1	vůdcovská
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgMnS	být
viditelným	viditelný	k2eAgMnSc7d1	viditelný
stoupencem	stoupenec	k1gMnSc7	stoupenec
Harryho	Harry	k1gMnSc2	Harry
S.	S.	kA	S.
Trumana	Truman	k1gMnSc2	Truman
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jeho	jeho	k3xOp3gFnPc1	jeho
politické	politický	k2eAgFnPc1d1	politická
sympatie	sympatie	k1gFnPc1	sympatie
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
pociťovat	pociťovat	k5eAaImF	pociťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
velkou	velká	k1gFnSc7	velká
politickou	politický	k2eAgFnSc7d1	politická
rolí	role	k1gFnSc7	role
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
Screen	Screna	k1gFnPc2	Screna
Actors	Actorsa	k1gFnPc2	Actorsa
Guild	Guild	k1gInSc1	Guild
<g/>
,	,	kIx,	,
odborů	odbor	k1gInPc2	odbor
reprezentujících	reprezentující	k2eAgInPc2d1	reprezentující
hollywoodské	hollywoodský	k2eAgInPc4d1	hollywoodský
herce	herc	k1gInPc4	herc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
vyjádření	vyjádření	k1gNnSc2	vyjádření
infiltrovány	infiltrován	k2eAgMnPc4d1	infiltrován
komunisty	komunista	k1gMnPc4	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
svědčil	svědčit	k5eAaImAgInS	svědčit
před	před	k7c7	před
Komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
neamerické	americký	k2eNgFnPc4d1	neamerická
aktivity	aktivita	k1gFnPc4	aktivita
o	o	k7c6	o
údajném	údajný	k2eAgInSc6d1	údajný
vlivu	vliv	k1gInSc6	vliv
komunistů	komunista	k1gMnPc2	komunista
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
on	on	k3xPp3gInSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
tajně	tajně	k6eAd1	tajně
sešli	sejít	k5eAaPmAgMnP	sejít
s	s	k7c7	s
agenty	agent	k1gMnPc7	agent
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
udávali	udávat	k5eAaImAgMnP	udávat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
podezřelé	podezřelý	k2eAgInPc1d1	podezřelý
živly	živel	k1gInPc1	živel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
údajně	údajně	k6eAd1	údajně
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Larry	Larra	k1gFnSc2	Larra
Parks	Parks	k1gInSc1	Parks
<g/>
,	,	kIx,	,
Howard	Howard	k1gInSc1	Howard
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Knox	Knox	k1gInSc1	Knox
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
předvoláni	předvolat	k5eAaPmNgMnP	předvolat
před	před	k7c4	před
komisi	komise	k1gFnSc4	komise
a	a	k8xC	a
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
listinu	listina	k1gFnSc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
až	až	k6eAd1	až
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
svobodném	svobodný	k2eAgInSc6d1	svobodný
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
zarytý	zarytý	k2eAgMnSc1d1	zarytý
antikomunista	antikomunista	k1gMnSc1	antikomunista
Reagan	Reagan	k1gMnSc1	Reagan
podporoval	podporovat	k5eAaImAgMnS	podporovat
kandidatury	kandidatura	k1gFnSc2	kandidatura
Dwighta	Dwight	k1gMnSc2	Dwight
Eisenhowera	Eisenhower	k1gMnSc2	Eisenhower
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstával	zůstávat	k5eAaImAgInS	zůstávat
registrovaným	registrovaný	k2eAgMnSc7d1	registrovaný
členem	člen	k1gMnSc7	člen
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
let	léto	k1gNnPc2	léto
studoval	studovat	k5eAaImAgMnS	studovat
Reagan	Reagan	k1gMnSc1	Reagan
pečlivě	pečlivě	k6eAd1	pečlivě
americkou	americký	k2eAgFnSc4d1	americká
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
zakladatele	zakladatel	k1gMnSc2	zakladatel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
ekonomiku	ekonomika	k1gFnSc4	ekonomika
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečtení	přečtení	k1gNnSc6	přečtení
Hayekovy	Hayekův	k2eAgFnSc2d1	Hayekova
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgInS	získat
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
socialismus	socialismus	k1gInSc1	socialismus
byl	být	k5eAaImAgInS	být
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
styl	styl	k1gInSc4	styl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Reagan	Reagan	k1gMnSc1	Reagan
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
socialistu	socialista	k1gMnSc4	socialista
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
republikánem	republikán	k1gMnSc7	republikán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
podporoval	podporovat	k5eAaImAgMnS	podporovat
nominaci	nominace	k1gFnSc4	nominace
Barryho	Barry	k1gMnSc2	Barry
Goldwatera	Goldwater	k1gMnSc2	Goldwater
na	na	k7c4	na
prezidentského	prezidentský	k2eAgMnSc4d1	prezidentský
kandidáta	kandidát	k1gMnSc4	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
odhalil	odhalit	k5eAaPmAgMnS	odhalit
svou	svůj	k3xOyFgFnSc4	svůj
ideologickou	ideologický	k2eAgFnSc4d1	ideologická
motivaci	motivace	k1gFnSc4	motivace
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Otcové	otec	k1gMnPc1	otec
zakladatelé	zakladatel	k1gMnPc1	zakladatel
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
nemůže	moct	k5eNaImIp3nS	moct
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
bez	bez	k7c2	bez
kontroly	kontrola	k1gFnSc2	kontrola
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
vláda	vláda	k1gFnSc1	vláda
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
použít	použít	k5eAaPmF	použít
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
donucení	donucení	k1gNnSc4	donucení
<g/>
.	.	kIx.	.
</s>
<s>
Došli	dojít	k5eAaPmAgMnP	dojít
jsme	být	k5eAaImIp1nP	být
k	k	k7c3	k
okamžiku	okamžik	k1gInSc3	okamžik
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Neopustil	opustit	k5eNaPmAgMnS	opustit
jsem	být	k5eAaImIp1nS	být
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
opustila	opustit	k5eAaPmAgFnS	opustit
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
"	"	kIx"	"
<g/>
An	An	k1gFnSc6	An
American	Americana	k1gFnPc2	Americana
Life	Lif	k1gFnSc2	Lif
<g/>
"	"	kIx"	"
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franklin	Franklin	k1gInSc1	Franklin
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sociální	sociální	k2eAgInPc1d1	sociální
programy	program	k1gInPc1	program
mohou	moct	k5eAaImIp3nP	moct
zničit	zničit	k5eAaPmF	zničit
pracovní	pracovní	k2eAgFnSc4d1	pracovní
etiku	etika	k1gFnSc4	etika
jako	jako	k8xC	jako
narkotika	narkotikon	k1gNnPc4	narkotikon
a	a	k8xC	a
že	že	k8xS	že
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
zrušil	zrušit	k5eAaPmAgMnS	zrušit
dočasné	dočasný	k2eAgInPc4d1	dočasný
sociální	sociální	k2eAgInPc4d1	sociální
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
pomoci	pomoct	k5eAaPmF	pomoct
zemi	zem	k1gFnSc4	zem
při	při	k7c6	při
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
krizi	krize	k1gFnSc6	krize
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byla	být	k5eAaImAgFnS	být
krize	krize	k1gFnSc1	krize
zažehnána	zažehnán	k2eAgFnSc1d1	zažehnána
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
programy	program	k1gInPc1	program
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
Rooseveltově	Rooseveltův	k2eAgFnSc6d1	Rooseveltova
smrti	smrt	k1gFnSc6	smrt
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
zvolen	zvolen	k2eAgMnSc1d1	zvolen
33	[number]	k4	33
<g/>
.	.	kIx.	.
guvernérem	guvernér	k1gMnSc7	guvernér
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgMnS	porazit
Edmunda	Edmund	k1gMnSc4	Edmund
G.	G.	kA	G.
"	"	kIx"	"
<g/>
Pat	pat	k1gInSc1	pat
<g/>
"	"	kIx"	"
Browna	Brown	k1gMnSc2	Brown
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgInS	porazit
Jesse	Jess	k1gMnSc4	Jess
Unruha	Unruh	k1gMnSc4	Unruh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
zisk	zisk	k1gInSc4	zisk
třetího	třetí	k4xOgNnSc2	třetí
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepokusil	pokusit	k5eNaPmAgMnS	pokusit
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
složením	složení	k1gNnSc7	složení
přísahy	přísaha	k1gFnSc2	přísaha
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prvního	první	k4xOgNnSc2	první
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
zmrazil	zmrazit	k5eAaPmAgMnS	zmrazit
přijímání	přijímání	k1gNnSc3	přijímání
nových	nový	k2eAgMnPc2d1	nový
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
schválil	schválit	k5eAaPmAgInS	schválit
zvýšení	zvýšení	k1gNnSc4	zvýšení
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
rychle	rychle	k6eAd1	rychle
umlčel	umlčet	k5eAaPmAgMnS	umlčet
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
protestní	protestní	k2eAgNnSc4d1	protestní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
protestů	protest	k1gInPc2	protest
v	v	k7c6	v
People	People	k1gFnSc6	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
poslal	poslat	k5eAaPmAgMnS	poslat
2200	[number]	k4	2200
členů	člen	k1gInPc2	člen
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
do	do	k7c2	do
kampusu	kampus	k1gInSc2	kampus
University	universita	k1gFnSc2	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
teď	teď	k6eAd1	teď
<g/>
.	.	kIx.	.
</s>
<s>
Appeasement	appeasement	k1gInSc1	appeasement
není	být	k5eNaImIp3nS	být
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
demokratem	demokrat	k1gMnSc7	demokrat
Bobem	Bob	k1gMnSc7	Bob
Morettim	Morettim	k1gMnSc1	Morettim
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
na	na	k7c6	na
reformě	reforma	k1gFnSc6	reforma
sociálního	sociální	k2eAgInSc2d1	sociální
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
také	také	k9	také
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
stavbě	stavba	k1gFnSc3	stavba
velké	velký	k2eAgFnSc2d1	velká
přehrady	přehrada	k1gFnSc2	přehrada
Dos	Dos	k1gFnSc2	Dos
Rios	Riosa	k1gFnPc2	Riosa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zatopit	zatopit	k5eAaPmF	zatopit
údolí	údolí	k1gNnPc1	údolí
s	s	k7c7	s
farmami	farma	k1gFnPc7	farma
amerických	americký	k2eAgMnPc2d1	americký
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Reagan	Reagan	k1gMnSc1	Reagan
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
pohoří	pohoří	k1gNnSc2	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
stavba	stavba	k1gFnSc1	stavba
nové	nový	k2eAgFnSc2d1	nová
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
frustrujících	frustrující	k2eAgInPc2d1	frustrující
problémů	problém	k1gInPc2	problém
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Reagana	Reagan	k1gMnSc4	Reagan
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
úřadování	úřadování	k1gNnSc2	úřadování
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Profiloval	profilovat	k5eAaImAgMnS	profilovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jasný	jasný	k2eAgMnSc1d1	jasný
stoupenec	stoupenec	k1gMnSc1	stoupenec
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
prosadit	prosadit	k5eAaPmF	prosadit
zákony	zákon	k1gInPc4	zákon
zavádějící	zavádějící	k2eAgInPc4d1	zavádějící
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
zmařena	zmařen	k2eAgFnSc1d1	zmařena
<g/>
,	,	kIx,	,
když	když	k8xS	když
kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
případu	případ	k1gInSc6	případ
Lid	Lido	k1gNnPc2	Lido
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
tresty	trest	k1gInPc4	trest
smrti	smrt	k1gFnSc2	smrt
uděleny	udělen	k2eAgInPc4d1	udělen
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1972	[number]	k4	1972
jsou	být	k5eAaImIp3nP	být
neplatné	platný	k2eNgInPc1d1	neplatný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
změněno	změněn	k2eAgNnSc1d1	změněno
díky	díky	k7c3	díky
ústavnímu	ústavní	k2eAgInSc3d1	ústavní
dodatku	dodatek	k1gInSc3	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
podporu	podpora	k1gFnSc4	podpora
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
udělil	udělit	k5eAaPmAgMnS	udělit
Reagan	Reagan	k1gMnSc1	Reagan
dvě	dva	k4xCgFnPc4	dva
milosti	milost	k1gFnPc1	milost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
udělena	udělit	k5eAaPmNgFnS	udělit
už	už	k6eAd1	už
žádná	žádný	k3yNgFnSc1	žádný
milost	milost	k1gFnSc1	milost
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Reaganova	Reaganův	k2eAgNnSc2d1	Reaganovo
úřadování	úřadování	k1gNnSc2	úřadování
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgInSc1d1	popraven
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Aaron	Aaron	k1gNnSc4	Aaron
Mitchell	Mitchell	k1gInSc1	Mitchell
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1967	[number]	k4	1967
v	v	k7c6	v
plynové	plynový	k2eAgFnSc6d1	plynová
komoře	komora	k1gFnSc6	komora
v	v	k7c6	v
San	San	k1gFnSc6	San
Quentinu	Quentin	k1gInSc2	Quentin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
další	další	k2eAgFnSc3d1	další
popravě	poprava	k1gFnSc3	poprava
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Symbionese	Symbionese	k1gFnSc1	Symbionese
Liberation	Liberation	k1gInSc4	Liberation
Army	Arma	k1gFnSc2	Arma
unesla	unést	k5eAaPmAgFnS	unést
v	v	k7c4	v
Berkley	Berkle	k2eAgFnPc4d1	Berkle
Patty	Patta	k1gFnPc4	Patta
Hearst	Hearst	k1gFnSc1	Hearst
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
distribuování	distribuování	k1gNnSc4	distribuování
jídla	jídlo	k1gNnSc2	jídlo
chudým	chudý	k2eAgInSc7d1	chudý
<g/>
,	,	kIx,	,
Reagan	Reagan	k1gMnSc1	Reagan
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
moment	moment	k1gInSc1	moment
pro	pro	k7c4	pro
propuknutí	propuknutí	k1gNnSc4	propuknutí
botulismu	botulismus	k1gInSc2	botulismus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
o	o	k7c6	o
výroku	výrok	k1gInSc6	výrok
informovala	informovat	k5eAaBmAgFnS	informovat
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
omluvil	omluvit	k5eAaPmAgMnS	omluvit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
rušení	rušení	k1gNnSc3	rušení
systému	systém	k1gInSc2	systém
veřejných	veřejný	k2eAgFnPc2d1	veřejná
psychiatrických	psychiatrický	k2eAgFnPc2d1	psychiatrická
léčeben	léčebna	k1gFnPc2	léčebna
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
jejich	jejich	k3xOp3gNnSc4	jejich
nahrazení	nahrazení	k1gNnSc4	nahrazení
ubytováním	ubytování	k1gNnSc7	ubytování
a	a	k8xC	a
léčbou	léčba	k1gFnSc7	léčba
v	v	k7c6	v
komunitách	komunita	k1gFnPc6	komunita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neporušovaly	porušovat	k5eNaImAgFnP	porušovat
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
jako	jako	k8xS	jako
nedobrovolné	dobrovolný	k2eNgFnPc4d1	nedobrovolná
hospitalizace	hospitalizace	k1gFnPc4	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Komunitní	komunitní	k2eAgNnPc1d1	komunitní
střediska	středisko	k1gNnPc1	středisko
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
adekvátně	adekvátně	k6eAd1	adekvátně
financována	financovat	k5eAaBmNgFnS	financovat
ani	ani	k8xC	ani
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnSc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
klasickými	klasický	k2eAgMnPc7d1	klasický
liberály	liberál	k1gMnPc7	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
otázán	otázat	k5eAaPmNgInS	otázat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ekonomové	ekonom	k1gMnPc1	ekonom
jej	on	k3xPp3gMnSc4	on
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bastiat	Bastiat	k1gInSc1	Bastiat
a	a	k8xC	a
von	von	k1gInSc1	von
Mises	Misesa	k1gFnPc2	Misesa
a	a	k8xC	a
Hayek	Hayky	k1gFnPc2	Hayky
a	a	k8xC	a
Hazlitt	Hazlitta	k1gFnPc2	Hazlitta
-	-	kIx~	-
jsem	být	k5eAaImIp1nS	být
pro	pro	k7c4	pro
klasické	klasický	k2eAgMnPc4d1	klasický
ekonomy	ekonom	k1gMnPc4	ekonom
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgFnPc4d1	pracovní
cesty	cesta	k1gFnPc4	cesta
tryskáč	tryskáč	k1gInSc1	tryskáč
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
získala	získat	k5eAaPmAgFnS	získat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
Cessen	Cessna	k1gFnPc2	Cessna
Citation	Citation	k1gInSc1	Citation
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Paynter	Paynter	k1gMnSc1	Paynter
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
své	svůj	k3xOyFgNnSc4	svůj
demokratické	demokratický	k2eAgNnSc4d1	demokratické
smýšlení	smýšlení	k1gNnSc4	smýšlení
v	v	k7c6	v
republikánské	republikánský	k2eAgFnSc2d1	republikánská
během	během	k7c2	během
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poznal	poznat	k5eAaPmAgMnS	poznat
Reagana	Reagan	k1gMnSc4	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Paynter	Paynter	k1gMnSc1	Paynter
často	často	k6eAd1	často
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
byl	být	k5eAaImAgInS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
Reagan	Reagan	k1gMnSc1	Reagan
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dělal	dělat	k5eAaImAgMnS	dělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ptal	ptat	k5eAaImAgMnS	ptat
své	svůj	k3xOyFgFnPc4	svůj
posádky	posádka	k1gFnPc4	posádka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
problém	problém	k1gInSc1	problém
změnit	změnit	k5eAaPmF	změnit
letový	letový	k2eAgInSc4d1	letový
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gMnPc1	jeho
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
byli	být	k5eAaImAgMnP	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
při	při	k7c6	při
rodinných	rodinný	k2eAgFnPc6d1	rodinná
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
do	do	k7c2	do
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
zapojil	zapojit	k5eAaPmAgInS	zapojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
člen	člen	k1gInSc1	člen
hnutí	hnutí	k1gNnSc1	hnutí
"	"	kIx"	"
<g/>
Zastavte	zastavit	k5eAaPmRp2nP	zastavit
Nixona	Nixona	k1gFnSc1	Nixona
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vedl	vést	k5eAaImAgMnS	vést
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
newyorský	newyorský	k2eAgMnSc1d1	newyorský
guvernér	guvernér	k1gMnSc1	guvernér
Nelson	Nelson	k1gMnSc1	Nelson
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
<g/>
.	.	kIx.	.
</s>
<s>
Reaganovi	Reagan	k1gMnSc3	Reagan
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
slib	slib	k1gInSc4	slib
asi	asi	k9	asi
600	[number]	k4	600
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
jej	on	k3xPp3gMnSc4	on
rychle	rychle	k6eAd1	rychle
převálcoval	převálcovat	k5eAaPmAgMnS	převálcovat
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
<g/>
;	;	kIx,	;
Reagan	Reagan	k1gMnSc1	Reagan
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
kongresu	kongres	k1gInSc3	kongres
nominovat	nominovat	k5eAaBmF	nominovat
jednomyslně	jednomyslně	k6eAd1	jednomyslně
Nixona	Nixona	k1gFnSc1	Nixona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Reagan	Reagan	k1gMnSc1	Reagan
prezidenta	prezident	k1gMnSc2	prezident
Geralda	Geraldo	k1gNnSc2	Geraldo
Forda	ford	k1gMnSc2	ford
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
profiloval	profilovat	k5eAaImAgMnS	profilovat
jako	jako	k8xC	jako
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
smýšlející	smýšlející	k2eAgFnSc1d1	smýšlející
organizace	organizace	k1gFnSc1	organizace
jako	jako	k8xC	jako
American	American	k1gInSc1	American
Conservative	Conservativ	k1gInSc5	Conservativ
Union	union	k1gInSc1	union
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
klíčovými	klíčový	k2eAgFnPc7d1	klíčová
komponenty	komponent	k1gInPc4	komponent
jeho	jeho	k3xOp3gFnSc2	jeho
politické	politický	k2eAgFnSc2d1	politická
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
na	na	k7c4	na
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
přišel	přijít	k5eAaPmAgMnS	přijít
manažer	manažer	k1gMnSc1	manažer
jeho	jeho	k3xOp3gFnSc2	jeho
kampaně	kampaň	k1gFnSc2	kampaň
John	John	k1gMnSc1	John
Sears	Sears	k1gInSc1	Sears
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zakládala	zakládat	k5eAaImAgFnS	zakládat
na	na	k7c6	na
rychlých	rychlý	k2eAgFnPc6d1	rychlá
výhrách	výhra	k1gFnPc6	výhra
v	v	k7c6	v
primárkách	primárky	k1gFnPc6	primárky
v	v	k7c6	v
několika	několik	k4yIc6	několik
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vážně	vážně	k6eAd1	vážně
poškozen	poškozen	k2eAgInSc4d1	poškozen
začátek	začátek	k1gInSc4	začátek
Fordovy	Fordův	k2eAgFnSc2d1	Fordova
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
strategie	strategie	k1gFnSc1	strategie
brzy	brzy	k6eAd1	brzy
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Chabý	chabý	k2eAgInSc1d1	chabý
management	management	k1gInSc1	management
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
naplánované	naplánovaný	k2eAgInPc4d1	naplánovaný
projevy	projev	k1gInPc4	projev
slibující	slibující	k2eAgInSc4d1	slibující
přesun	přesun	k1gInSc4	přesun
federálních	federální	k2eAgFnPc2d1	federální
pravomocí	pravomoc	k1gFnPc2	pravomoc
na	na	k7c4	na
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
objasnění	objasnění	k1gNnSc2	objasnění
mechanismu	mechanismus	k1gInSc2	mechanismus
jejich	jejich	k3xOp3gNnSc2	jejich
financování	financování	k1gNnSc2	financování
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
porážkám	porážka	k1gFnPc3	porážka
v	v	k7c6	v
New	New	k1gFnSc6	New
Hampshire	Hampshir	k1gInSc5	Hampshir
a	a	k8xC	a
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
kouta	kout	k1gInSc2	kout
a	a	k8xC	a
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
pozice	pozice	k1gFnSc1	pozice
před	před	k7c7	před
primárkami	primárky	k1gFnPc7	primárky
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
ven	ven	k6eAd1	ven
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Forda	ford	k1gMnSc4	ford
kvůli	kvůli	k7c3	kvůli
Panamskému	panamský	k2eAgInSc3d1	panamský
průplavu	průplav	k1gInSc3	průplav
<g/>
,	,	kIx,	,
détente	détente	k2eAgMnPc1d1	détente
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
autobusové	autobusový	k2eAgFnSc6d1	autobusová
dopravě	doprava	k1gFnSc6	doprava
školáků	školák	k1gMnPc2	školák
a	a	k8xC	a
na	na	k7c4	na
Kissingerovo	Kissingerův	k2eAgNnSc4d1	Kissingerovo
působení	působení	k1gNnSc4	působení
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
53	[number]	k4	53
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
také	také	k9	také
primárky	primárky	k1gFnPc4	primárky
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gNnSc7	Tennessee
<g/>
,	,	kIx,	,
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
Michiganu	Michigan	k1gInSc2	Michigan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
kongres	kongres	k1gInSc1	kongres
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Ford	ford	k1gInSc1	ford
blízko	blízko	k7c2	blízko
vítězství	vítězství	k1gNnSc2	vítězství
díky	díky	k7c3	díky
liberálním	liberální	k2eAgMnPc3d1	liberální
kandidátům	kandidát	k1gMnPc3	kandidát
z	z	k7c2	z
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
Pennsylvánie	Pennsylvánie	k1gFnSc2	Pennsylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
sílu	síla	k1gFnSc4	síla
liberálního	liberální	k2eAgNnSc2d1	liberální
a	a	k8xC	a
umírněného	umírněný	k2eAgNnSc2d1	umírněné
křídla	křídlo	k1gNnSc2	křídlo
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
jako	jako	k9	jako
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
vybral	vybrat	k5eAaPmAgMnS	vybrat
senátora	senátor	k1gMnSc4	senátor
Richarda	Richard	k1gMnSc4	Richard
Schweikera	Schweiker	k1gMnSc4	Schweiker
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Fordův	Fordův	k2eAgInSc1d1	Fordův
náskok	náskok	k1gInSc1	náskok
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Ford	ford	k1gInSc1	ford
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
<g/>
,	,	kIx,	,
když	když	k8xS	když
obdržel	obdržet	k5eAaPmAgMnS	obdržet
1187	[number]	k4	1187
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
1070	[number]	k4	1070
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
jaderné	jaderný	k2eAgFnSc2d1	jaderná
války	válka	k1gFnSc2	válka
a	a	k8xC	a
morální	morální	k2eAgNnSc4d1	morální
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
představoval	představovat	k5eAaImAgInS	představovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Reagan	Reagan	k1gMnSc1	Reagan
nominaci	nominace	k1gFnSc4	nominace
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
,	,	kIx,	,
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
obdržel	obdržet	k5eAaPmAgMnS	obdržet
307	[number]	k4	307
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
388	[number]	k4	388
ve	v	k7c6	v
Wyomingu	Wyoming	k1gInSc6	Wyoming
a	a	k8xC	a
voliči	volič	k1gInSc6	volič
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
získal	získat	k5eAaPmAgMnS	získat
Reagan	Reagan	k1gMnSc1	Reagan
nominaci	nominace	k1gFnSc4	nominace
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
většinu	většina	k1gFnSc4	většina
primárek	primárky	k1gFnPc2	primárky
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
kampaně	kampaň	k1gFnSc2	kampaň
prohrál	prohrát	k5eAaPmAgInS	prohrát
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kongresu	kongres	k1gInSc2	kongres
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Reagan	Reagan	k1gMnSc1	Reagan
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Geralda	Gerald	k1gMnSc4	Gerald
Forda	ford	k1gMnSc4	ford
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
nakonec	nakonec	k6eAd1	nakonec
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
vybral	vybrat	k5eAaPmAgMnS	vybrat
svého	svůj	k3xOyFgMnSc4	svůj
oponenta	oponent	k1gMnSc4	oponent
z	z	k7c2	z
primárek	primárky	k1gFnPc2	primárky
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
značné	značný	k2eAgFnPc4d1	značná
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
pronesl	pronést	k5eAaPmAgMnS	pronést
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
projev	projev	k1gInSc1	projev
poblíž	poblíž	k6eAd1	poblíž
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
v	v	k7c6	v
Mississippi	Mississippi	k1gFnSc6	Mississippi
na	na	k7c6	na
výroční	výroční	k2eAgFnSc6d1	výroční
pouti	pouť	k1gFnSc6	pouť
Neshoba	Neshoba	k1gFnSc1	Neshoba
Country	country	k2eAgFnSc2d1	country
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věřím	věřit	k5eAaImIp1nS	věřit
v	v	k7c4	v
práva	právo	k1gNnPc4	právo
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
zdeformovali	zdeformovat	k5eAaPmAgMnP	zdeformovat
vyvážení	vyvážení	k1gNnSc4	vyvážení
mocí	moc	k1gFnPc2	moc
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
dali	dát	k5eAaPmAgMnP	dát
federální	federální	k2eAgFnSc3d1	federální
vládě	vláda	k1gFnSc3	vláda
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
neměly	mít	k5eNaImAgInP	mít
náležet	náležet	k5eAaImF	náležet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vrátí	vrátit	k5eAaPmIp3nS	vrátit
státům	stát	k1gInPc3	stát
a	a	k8xC	a
místním	místní	k2eAgFnPc3d1	místní
vládám	vláda	k1gFnPc3	vláda
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
náleží	náležet	k5eAaImIp3nP	náležet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
byli	být	k5eAaImAgMnP	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
tři	tři	k4xCgMnPc1	tři
aktivisté	aktivista	k1gMnPc1	aktivista
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
James	James	k1gInSc4	James
Chaney	Chanea	k1gFnSc2	Chanea
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Goodman	Goodman	k1gMnSc1	Goodman
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Schwerner	Schwerner	k1gMnSc1	Schwerner
a	a	k8xC	a
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
kritici	kritik	k1gMnPc1	kritik
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reagan	Reagan	k1gMnSc1	Reagan
vyslal	vyslat	k5eAaPmAgMnS	vyslat
publiku	publikum	k1gNnSc3	publikum
rasistický	rasistický	k2eAgInSc4d1	rasistický
vzkaz	vzkaz	k1gInSc4	vzkaz
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
byla	být	k5eAaImAgFnS	být
pronesena	pronést	k5eAaPmNgFnS	pronést
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
filosofií	filosofie	k1gFnSc7	filosofie
omezené	omezený	k2eAgFnSc2d1	omezená
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kritici	kritik	k1gMnPc1	kritik
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
místo	místo	k1gNnSc4	místo
vybral	vybrat	k5eAaPmAgMnS	vybrat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
bílé	bílý	k2eAgMnPc4d1	bílý
jižanské	jižanský	k2eAgMnPc4d1	jižanský
voliče	volič	k1gMnPc4	volič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biografii	biografie	k1gFnSc6	biografie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
o	o	k7c6	o
Reaganovi	Reagan	k1gMnSc6	Reagan
napsal	napsat	k5eAaPmAgMnS	napsat
Edmund	Edmund	k1gMnSc1	Edmund
Morris	Morris	k1gFnSc2	Morris
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reagan	Reagan	k1gMnSc1	Reagan
pevně	pevně	k6eAd1	pevně
podporoval	podporovat	k5eAaImAgMnS	podporovat
nadvládu	nadvláda	k1gFnSc4	nadvláda
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
občanských	občanský	k2eAgNnPc6d1	občanské
právech	právo	k1gNnPc6	právo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
je	on	k3xPp3gMnPc4	on
zbytečných	zbytečný	k2eAgFnPc2d1	zbytečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
již	již	k6eAd1	již
chrání	chránit	k5eAaImIp3nS	chránit
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
přesto	přesto	k8xC	přesto
zranitelný	zranitelný	k2eAgMnSc1d1	zranitelný
obviněními	obvinění	k1gNnPc7	obvinění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
necitlivostí	necitlivost	k1gFnSc7	necitlivost
týkající	týkající	k2eAgFnSc7d1	týkající
se	se	k3xPyFc4	se
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Running	Running	k1gInSc1	Running
on	on	k3xPp3gMnSc1	on
Race	Race	k1gNnPc1	Race
<g/>
:	:	kIx,	:
Racial	Racial	k1gInSc1	Racial
Politics	Politics	k1gInSc1	Politics
in	in	k?	in
Presidential	Presidential	k1gInSc1	Presidential
Campaigns	Campaignsa	k1gFnPc2	Campaignsa
se	se	k3xPyFc4	se
Carter	Carter	k1gMnSc1	Carter
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obvinit	obvinit	k5eAaPmF	obvinit
Reagana	Reagan	k1gMnSc4	Reagan
z	z	k7c2	z
rasismu	rasismus	k1gInSc2	rasismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
brzy	brzy	k6eAd1	brzy
proti	proti	k7c3	proti
Carterovi	Carter	k1gMnSc3	Carter
samotnému	samotný	k2eAgMnSc3d1	samotný
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
Carterových	Carterův	k2eAgMnPc2d1	Carterův
černých	černý	k2eAgMnPc2d1	černý
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
u	u	k7c2	u
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Young	Young	k1gMnSc1	Young
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vyburcovat	vyburcovat	k5eAaPmF	vyburcovat
černou	černý	k2eAgFnSc4d1	černá
opozici	opozice	k1gFnSc4	opozice
proti	proti	k7c3	proti
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
normální	normální	k2eAgMnSc1d1	normální
zabíjet	zabíjet	k5eAaImF	zabíjet
negry	negr	k1gMnPc4	negr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
ostrý	ostrý	k2eAgInSc4d1	ostrý
výpad	výpad	k1gInSc4	výpad
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Carterovi	Carter	k1gMnSc3	Carter
více	hodně	k6eAd2	hodně
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
než	než	k8xS	než
získal	získat	k5eAaPmAgMnS	získat
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
řídil	řídit	k5eAaImAgInS	řídit
William	William	k1gInSc1	William
J.	J.	kA	J.
Casey	Casea	k1gFnSc2	Casea
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
íránské	íránský	k2eAgFnSc2d1	íránská
krize	krize	k1gFnSc2	krize
s	s	k7c7	s
rukojmími	rukojmí	k1gMnPc7	rukojmí
a	a	k8xC	a
média	médium	k1gNnPc1	médium
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc4	den
přinášela	přinášet	k5eAaImAgFnS	přinášet
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
neúspěšných	úspěšný	k2eNgFnPc6d1	neúspěšná
Carterových	Carterův	k2eAgFnPc6d1	Carterova
snahách	snaha	k1gFnPc6	snaha
osvobodit	osvobodit	k5eAaPmF	osvobodit
rukojmí	rukojmí	k1gMnPc4	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
analytiků	analytik	k1gMnPc2	analytik
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
byla	být	k5eAaImAgFnS	být
oslabena	oslabit	k5eAaPmNgFnS	oslabit
Carterova	Carterův	k2eAgFnSc1d1	Carterova
pozice	pozice	k1gFnSc1	pozice
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
neefektivitu	neefektivita	k1gFnSc4	neefektivita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Carterova	Carterův	k2eAgFnSc1d1	Carterova
neschopnost	neschopnost	k1gFnSc1	neschopnost
si	se	k3xPyFc3	se
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
dvoucifernou	dvouciferný	k2eAgFnSc7d1	dvouciferná
inflací	inflace	k1gFnSc7	inflace
a	a	k8xC	a
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
nevýrazným	výrazný	k2eNgInSc7d1	nevýrazný
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
růstem	růst	k1gInSc7	růst
<g/>
,	,	kIx,	,
nestabilitou	nestabilita	k1gFnSc7	nestabilita
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
frontám	fronta	k1gFnPc3	fronta
před	před	k7c7	před
benzínovými	benzínový	k2eAgFnPc7d1	benzínová
stanicemi	stanice	k1gFnPc7	stanice
a	a	k8xC	a
slabost	slabost	k1gFnSc1	slabost
americké	americký	k2eAgFnSc2d1	americká
obrany	obrana	k1gFnSc2	obrana
možná	možná	k9	možná
měly	mít	k5eAaImAgFnP	mít
na	na	k7c4	na
voliče	volič	k1gMnPc4	volič
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
přišel	přijít	k5eAaPmAgMnS	přijít
Carter	Carter	k1gMnSc1	Carter
s	s	k7c7	s
"	"	kIx"	"
<g/>
indexem	index	k1gInSc7	index
strádání	strádání	k1gNnSc2	strádání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
a	a	k8xC	a
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
index	index	k1gInSc1	index
strádání	strádání	k1gNnSc2	strádání
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
neustále	neustále	k6eAd1	neustále
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
toho	ten	k3xDgMnSc4	ten
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
využil	využít	k5eAaPmAgMnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
respektem	respekt	k1gInSc7	respekt
k	k	k7c3	k
ekonomii	ekonomie	k1gFnSc3	ekonomie
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
mi	já	k3xPp1nSc3	já
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemám	mít	k5eNaImIp1nS	mít
používat	používat	k5eAaImF	používat
slovo	slovo	k1gNnSc4	slovo
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
řeknu	říct	k5eAaPmIp1nS	říct
vám	vy	k3xPp2nPc3	vy
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Recese	recese	k1gFnSc1	recese
je	být	k5eAaImIp3nS	být
když	když	k8xS	když
váš	váš	k3xOp2gMnSc1	váš
soused	soused	k1gMnSc1	soused
ztratí	ztratit	k5eAaPmIp3nS	ztratit
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
vy	vy	k3xPp2nPc1	vy
ztratíte	ztratit	k5eAaPmIp2nP	ztratit
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jimmy	Jimma	k1gFnPc1	Jimma
Carter	Cartra	k1gFnPc2	Cartra
ztratí	ztratit	k5eAaPmIp3nP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reaganovo	Reaganův	k2eAgNnSc4d1	Reaganovo
vystupování	vystupování	k1gNnSc4	vystupování
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
debatách	debata	k1gFnPc6	debata
rozproudilo	rozproudit	k5eAaPmAgNnS	rozproudit
jeho	jeho	k3xOp3gFnSc4	jeho
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Zdál	zdát	k5eAaImAgInS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
v	v	k7c6	v
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
,	,	kIx,	,
kritice	kritika	k1gFnSc6	kritika
z	z	k7c2	z
Carterovy	Carterův	k2eAgFnSc2d1	Carterova
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgInS	bránit
poznámkami	poznámka	k1gFnPc7	poznámka
jako	jako	k9	jako
"	"	kIx"	"
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tu	tu	k6eAd1	tu
zase	zase	k9	zase
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
poznámkou	poznámka	k1gFnSc7	poznámka
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
otázka	otázka	k1gFnSc1	otázka
k	k	k7c3	k
publiku	publikum	k1gNnSc3	publikum
v	v	k7c6	v
době	doba	k1gFnSc6	doba
raketově	raketově	k6eAd1	raketově
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Máte	mít	k5eAaImIp2nP	mít
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
před	před	k7c7	před
čtyřmi	čtyři	k4xCgNnPc7	čtyři
lety	léto	k1gNnPc7	léto
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Stejnou	stejný	k2eAgFnSc4d1	stejná
frázi	fráze	k1gFnSc4	fráze
poté	poté	k6eAd1	poté
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Ronalda	Ronald	k1gMnSc4	Ronald
Reagana	Reagan	k1gMnSc4	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1981	[number]	k4	1981
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
James	James	k1gMnSc1	James
Brady	brada	k1gFnSc2	brada
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
policisté	policista	k1gMnPc1	policista
postřeleni	postřelit	k5eAaPmNgMnP	postřelit
vyšinutým	vyšinutý	k2eAgFnPc3d1	vyšinutá
Johnem	John	k1gMnSc7	John
Hinckleym	Hinckleym	k1gInSc4	Hinckleym
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc4	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
minula	minout	k5eAaImAgFnS	minout
Reaganovo	Reaganův	k2eAgNnSc4d1	Reaganovo
srdce	srdce	k1gNnSc4	srdce
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
palec	palec	k1gInSc4	palec
a	a	k8xC	a
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
plíce	plíce	k1gFnSc2	plíce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
asi	asi	k9	asi
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
z	z	k7c2	z
legrace	legrace	k1gFnSc2	legrace
řekl	říct	k5eAaPmAgMnS	říct
chirurgům	chirurg	k1gMnPc3	chirurg
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
všichni	všechen	k3xTgMnPc1	všechen
republikáni	republikán	k1gMnPc1	republikán
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
nebyli	být	k5eNaImAgMnP	být
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
mu	on	k3xPp3gMnSc3	on
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Joseph	Joseph	k1gMnSc1	Joseph
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnes	dnes	k6eAd1	dnes
jsme	být	k5eAaImIp1nP	být
všichni	všechen	k3xTgMnPc1	všechen
republikáni	republikán	k1gMnPc1	republikán
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
řekl	říct	k5eAaPmAgMnS	říct
slavnou	slavný	k2eAgFnSc4d1	slavná
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Miláčku	miláček	k1gMnSc3	miláček
<g/>
,	,	kIx,	,
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
skrčit	skrčit	k5eAaPmF	skrčit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reagan	Reagan	k1gMnSc1	Reagan
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
měl	mít	k5eAaImAgInS	mít
navštívit	navštívit	k5eAaPmF	navštívit
Philadelphii	Philadelphia	k1gFnSc4	Philadelphia
a	a	k8xC	a
sestře	sestra	k1gFnSc6	sestra
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
bych	by	kYmCp1nS	by
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
začala	začít	k5eAaPmAgFnS	začít
stávka	stávka	k1gFnSc1	stávka
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
odbory	odbor	k1gInPc1	odbor
letových	letový	k2eAgMnPc2d1	letový
dispečerů	dispečer	k1gMnPc2	dispečer
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
porušen	porušen	k2eAgInSc1d1	porušen
zákon	zákon	k1gInSc1	zákon
zakazující	zakazující	k2eAgInSc1d1	zakazující
odborům	odbor	k1gInPc3	odbor
zastupujícím	zastupující	k2eAgFnPc3d1	zastupující
státní	státní	k2eAgMnPc4d1	státní
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
stávkovat	stávkovat	k5eAaImF	stávkovat
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
"	"	kIx"	"
<g/>
Taft	taft	k1gInSc1	taft
Hartley	Hartlea	k1gMnSc2	Hartlea
Act	Act	k1gMnSc2	Act
<g/>
"	"	kIx"	"
nouzový	nouzový	k2eAgInSc1d1	nouzový
stav	stav	k1gInSc1	stav
a	a	k8xC	a
v	v	k7c6	v
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
stávkující	stávkující	k1gMnSc1	stávkující
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
přijdou	přijít	k5eAaPmIp3nP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
nechal	nechat	k5eAaPmAgMnS	nechat
Reagan	Reagan	k1gMnSc1	Reagan
vyhodit	vyhodit	k5eAaPmF	vyhodit
11345	[number]	k4	11345
stávkujících	stávkující	k2eAgMnPc2d1	stávkující
dispečerů	dispečer	k1gMnPc2	dispečer
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neuposlechli	uposlechnout	k5eNaPmAgMnP	uposlechnout
jeho	jeho	k3xOp3gFnPc4	jeho
výzvy	výzva	k1gFnPc4	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Reagan	Reagan	k1gMnSc1	Reagan
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
inflace	inflace	k1gFnSc1	inflace
11,83	[number]	k4	11,83
<g/>
%	%	kIx~	%
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
zavedl	zavést	k5eAaPmAgMnS	zavést
politiky	politika	k1gFnSc2	politika
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
nabídkovou	nabídkový	k2eAgFnSc4d1	nabídková
stranu	strana	k1gFnSc4	strana
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
stimulovat	stimulovat	k5eAaImF	stimulovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
velkými	velký	k2eAgInPc7d1	velký
daňovými	daňový	k2eAgInPc7d1	daňový
škrty	škrt	k1gInPc7	škrt
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
snahu	snaha	k1gFnSc4	snaha
omezit	omezit	k5eAaPmF	omezit
růst	růst	k1gInSc4	růst
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
snížit	snížit	k5eAaPmF	snížit
míru	míra	k1gFnSc4	míra
regulace	regulace	k1gFnSc2	regulace
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
snížit	snížit	k5eAaPmF	snížit
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
o	o	k7c6	o
snížení	snížení	k1gNnSc6	snížení
výdajů	výdaj	k1gInPc2	výdaj
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
posílením	posílení	k1gNnSc7	posílení
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
snižováním	snižování	k1gNnSc7	snižování
daní	daň	k1gFnPc2	daň
se	se	k3xPyFc4	se
Reaganův	Reaganův	k2eAgInSc1d1	Reaganův
přístup	přístup	k1gInSc1	přístup
diametrálně	diametrálně	k6eAd1	diametrálně
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
přístupu	přístup	k1gInSc2	přístup
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
"	"	kIx"	"
<g/>
Reaganomika	Reaganomika	k1gFnSc1	Reaganomika
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gMnPc1	její
stoupenci	stoupenec	k1gMnPc1	stoupenec
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zlepšení	zlepšení	k1gNnSc4	zlepšení
klíčových	klíčový	k2eAgInPc2d1	klíčový
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
ukazatelů	ukazatel	k1gInPc2	ukazatel
<g/>
,	,	kIx,	,
kritici	kritik	k1gMnPc1	kritik
naopak	naopak	k6eAd1	naopak
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
schodek	schodek	k1gInSc4	schodek
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
politika	politika	k1gFnSc1	politika
"	"	kIx"	"
<g/>
mír	mír	k1gInSc1	mír
silou	síla	k1gFnSc7	síla
<g/>
"	"	kIx"	"
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
rekordní	rekordní	k2eAgFnPc4d1	rekordní
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
období	období	k1gNnSc6	období
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
až	až	k9	až
1985	[number]	k4	1985
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Reaganova	Reaganův	k2eAgNnSc2d1	Reaganovo
prezidentství	prezidentství	k1gNnSc2	prezidentství
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
snížení	snížení	k1gNnSc3	snížení
daní	daň	k1gFnPc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
příjmové	příjmový	k2eAgFnSc6d1	příjmová
skupině	skupina	k1gFnSc6	skupina
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
ze	z	k7c2	z
70	[number]	k4	70
<g/>
%	%	kIx~	%
na	na	k7c4	na
28	[number]	k4	28
<g/>
%	%	kIx~	%
během	během	k7c2	během
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
celková	celkový	k2eAgFnSc1d1	celková
suma	suma	k1gFnSc1	suma
vybraných	vybraný	k2eAgInPc2d1	vybraný
peněz	peníze	k1gInPc2	peníze
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
54	[number]	k4	54
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
%	%	kIx~	%
při	při	k7c6	při
započítání	započítání	k1gNnSc6	započítání
inflace	inflace	k1gFnSc2	inflace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
po	po	k7c6	po
propadu	propad	k1gInSc6	propad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
zotavil	zotavit	k5eAaPmAgInS	zotavit
a	a	k8xC	a
rostl	růst	k5eAaImAgInS	růst
za	za	k7c4	za
rok	rok	k1gInSc4	rok
průměrně	průměrně	k6eAd1	průměrně
o	o	k7c4	o
3,4	[number]	k4	3,4
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
než	než	k8xS	než
poválečný	poválečný	k2eAgInSc1d1	poválečný
průměr	průměr	k1gInSc1	průměr
3,6	[number]	k4	3,6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
hodnotou	hodnota	k1gFnSc7	hodnota
9,2	[number]	k4	9,2
<g/>
%	%	kIx~	%
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
Reaganova	Reaganův	k2eAgNnSc2d1	Reaganovo
prezidentství	prezidentství	k1gNnSc2	prezidentství
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
počet	počet	k1gInSc1	počet
zaměstnaných	zaměstnaný	k1gMnPc2	zaměstnaný
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
také	také	k6eAd1	také
inflace	inflace	k1gFnSc1	inflace
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Paula	Paul	k1gMnSc4	Paul
Volckera	Volcker	k1gMnSc4	Volcker
šéfem	šéf	k1gMnSc7	šéf
Fedu	Fedus	k1gInSc2	Fedus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Alan	Alan	k1gMnSc1	Alan
Greenspan	Greenspan	k1gMnSc1	Greenspan
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ekonomové	ekonom	k1gMnPc1	ekonom
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Milton	Milton	k1gInSc4	Milton
Friedman	Friedman	k1gMnSc1	Friedman
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Mundell	Mundell	k1gMnSc1	Mundell
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
daňová	daňový	k2eAgFnSc1d1	daňová
politika	politika	k1gFnSc1	politika
posílila	posílit	k5eAaPmAgFnS	posílit
americkou	americký	k2eAgFnSc4d1	americká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
boomu	boom	k1gInSc3	boom
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
ekonomové	ekonom	k1gMnPc1	ekonom
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Robert	Robert	k1gMnSc1	Robert
Solow	Solow	k1gMnSc1	Solow
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
schodky	schodek	k1gInPc1	schodek
byly	být	k5eAaImAgInP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Reaganův	Reaganův	k2eAgMnSc1d1	Reaganův
nástupce	nástupce	k1gMnSc1	nástupce
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
své	svůj	k3xOyFgInPc4	svůj
volební	volební	k2eAgInPc4d1	volební
sliby	slib	k1gInPc4	slib
a	a	k8xC	a
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přitvrdit	přitvrdit	k5eAaPmF	přitvrdit
boj	boj	k1gInSc4	boj
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
drogy	droga	k1gFnPc4	droga
"	"	kIx"	"
<g/>
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
naši	náš	k3xOp1gFnSc4	náš
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
pracoviště	pracoviště	k1gNnPc4	pracoviště
bez	bez	k7c2	bez
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
protidrogovou	protidrogový	k2eAgFnSc4d1	protidrogová
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
posílí	posílit	k5eAaPmIp3nS	posílit
vynucování	vynucování	k1gNnSc1	vynucování
protidrogových	protidrogový	k2eAgInPc2d1	protidrogový
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
společenské	společenský	k2eAgNnSc4d1	společenské
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
druhého	druhý	k4xOgNnSc2	druhý
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
boj	boj	k1gInSc4	boj
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
přispěl	přispět	k5eAaPmAgMnS	přispět
1,7	[number]	k4	1,7
miliardou	miliarda	k4xCgFnSc7	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
drogové	drogový	k2eAgInPc4d1	drogový
delikty	delikt	k1gInPc4	delikt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
za	za	k7c2	za
držení	držení	k1gNnSc2	držení
jednoho	jeden	k4xCgMnSc2	jeden
kilogramu	kilogram	k1gInSc2	kilogram
heroinu	heroin	k1gInSc2	heroin
byl	být	k5eAaImAgMnS	být
pachatel	pachatel	k1gMnSc1	pachatel
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
nejméně	málo	k6eAd3	málo
na	na	k7c4	na
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
však	však	k9	však
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
údajně	údajně	k6eAd1	údajně
zaváděl	zavádět	k5eAaImAgMnS	zavádět
rasové	rasový	k2eAgInPc4d1	rasový
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
vězni	vězeň	k1gMnPc7	vězeň
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
soudy	soud	k1gInPc1	soud
posuzovaly	posuzovat	k5eAaImAgFnP	posuzovat
crack	crack	k6eAd1	crack
a	a	k8xC	a
kokain	kokain	k1gInSc4	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
také	také	k9	také
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
Reaganovu	Reaganův	k2eAgFnSc4d1	Reaganova
administrativu	administrativa	k1gFnSc4	administrativa
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělala	udělat	k5eAaPmAgFnS	udělat
málo	málo	k6eAd1	málo
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
omezila	omezit	k5eAaPmAgFnS	omezit
dostupnost	dostupnost	k1gFnSc1	dostupnost
drog	droga	k1gFnPc2	droga
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Obhájci	obhájce	k1gMnPc1	obhájce
protidrogové	protidrogový	k2eAgFnSc2d1	protidrogová
politiky	politika	k1gFnSc2	politika
však	však	k9	však
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
adolescenty	adolescent	k1gMnPc7	adolescent
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc1	Nancy
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
určila	určit	k5eAaPmAgFnS	určit
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
drogám	droga	k1gFnPc3	droga
jako	jako	k8xC	jako
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
priorit	priorita	k1gFnPc2	priorita
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
protidrogové	protidrogový	k2eAgNnSc4d1	protidrogové
sdružení	sdružení	k1gNnSc4	sdružení
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Say	Say	k1gFnPc4	Say
No	no	k9	no
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
odrazovaní	odrazovaný	k2eAgMnPc1d1	odrazovaný
mládeže	mládež	k1gFnSc2	mládež
od	od	k7c2	od
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Nancy	Nancy	k1gFnSc1	Nancy
Reagan	Reagan	k1gMnSc1	Reagan
navštívila	navštívit	k5eAaPmAgFnS	navštívit
65	[number]	k4	65
měst	město	k1gNnPc2	město
v	v	k7c6	v
33	[number]	k4	33
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvedla	zvednout	k5eAaPmAgFnS	zvednout
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c4	o
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
do	do	k7c2	do
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
první	první	k4xOgFnSc4	první
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
svého	svůj	k3xOyFgNnSc2	svůj
prezidentství	prezidentství	k1gNnSc2	prezidentství
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
příležitost	příležitost	k1gFnSc1	příležitost
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Potter	Potter	k1gMnSc1	Potter
Stewart	Stewart	k1gMnSc1	Stewart
a	a	k8xC	a
Reaganem	Reagan	k1gMnSc7	Reagan
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
Day	Day	k1gFnSc1	Day
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gInSc4	Connor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Williama	William	k1gMnSc4	William
Rehnquista	Rehnquist	k1gMnSc4	Rehnquist
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
soudce	soudce	k1gMnSc1	soudce
Antonina	Antonin	k2eAgInSc2d1	Antonin
Scaliu	Scalium	k1gNnSc6	Scalium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
však	však	k9	však
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
politickou	politický	k2eAgFnSc4d1	politická
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Senát	senát	k1gInSc1	senát
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
Reaganem	Reagan	k1gMnSc7	Reagan
navrženého	navržený	k2eAgNnSc2d1	navržené
Roberta	Robert	k1gMnSc4	Robert
Borka	borka	k1gFnSc1	borka
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
následně	následně	k6eAd1	následně
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Anthony	Anthona	k1gFnSc2	Anthona
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
Senátem	senát	k1gInSc7	senát
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
peacekeepingové	peacekeepingový	k2eAgFnPc1d1	peacekeepingová
jednotky	jednotka	k1gFnPc1	jednotka
umístěné	umístěný	k2eAgFnPc1d1	umístěná
v	v	k7c6	v
Bejrútu	Bejrút	k1gInSc6	Bejrút
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mnohonárodnostních	mnohonárodnostní	k2eAgFnPc2d1	mnohonárodnostní
sil	síla	k1gFnPc2	síla
během	během	k7c2	během
Libanonské	libanonský	k2eAgFnSc2d1	libanonská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
napadeny	napaden	k2eAgFnPc1d1	napadena
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
útočníci	útočník	k1gMnPc1	útočník
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
241	[number]	k4	241
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
stal	stát	k5eAaPmAgMnS	stát
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnSc4d1	námořní
pěchotu	pěchota	k1gFnSc4	pěchota
nejkrvavějším	krvavý	k2eAgFnPc3d3	nejkrvavější
od	od	k7c2	od
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Iwo	Iwo	k1gFnSc4	Iwo
Jimu	Jimus	k1gInSc2	Jimus
a	a	k8xC	a
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
armádu	armáda	k1gFnSc4	armáda
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
Tet	Teta	k1gFnPc2	Teta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
nazval	nazvat	k5eAaBmAgInS	nazvat
útok	útok	k1gInSc4	útok
zavrženíhodným	zavrženíhodný	k2eAgInSc7d1	zavrženíhodný
a	a	k8xC	a
plánoval	plánovat	k5eAaImAgMnS	plánovat
útok	útok	k1gInSc4	útok
na	na	k7c4	na
kasárna	kasárna	k1gNnPc4	kasárna
v	v	k7c6	v
Baalbeku	Baalbek	k1gInSc6	Baalbek
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
používané	používaný	k2eAgFnSc2d1	používaná
Íránskými	íránský	k2eAgFnPc7d1	íránská
revolučními	revoluční	k2eAgFnPc7d1	revoluční
gardami	garda	k1gFnPc7	garda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
cvičily	cvičit	k5eAaImAgFnP	cvičit
bojovníky	bojovník	k1gMnPc4	bojovník
Hizballáhu	Hizballáha	k1gFnSc4	Hizballáha
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Caspar	Caspar	k1gMnSc1	Caspar
Weinberger	Weinberger	k1gMnSc1	Weinberger
misi	mise	k1gFnSc4	mise
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
arabskými	arabský	k2eAgInPc7d1	arabský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nevýznamného	významný	k2eNgNnSc2d1	nevýznamné
ostřelování	ostřelování	k1gNnSc2	ostřelování
tedy	tedy	k8xC	tedy
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
americké	americký	k2eAgFnSc3d1	americká
odvetě	odveta	k1gFnSc3	odveta
a	a	k8xC	a
mariňáci	mariňáci	k?	mariňáci
byli	být	k5eAaImAgMnP	být
přesunuti	přesunout	k5eAaPmNgMnP	přesunout
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
napadeni	napadnout	k5eAaPmNgMnP	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Reagan	Reagan	k1gMnSc1	Reagan
zahájit	zahájit	k5eAaPmF	zahájit
stahování	stahování	k1gNnSc4	stahování
z	z	k7c2	z
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Stahování	stahování	k1gNnSc1	stahování
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
mnohonárodnostních	mnohonárodnostní	k2eAgFnPc2d1	mnohonárodnostní
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Grenadě	Grenada	k1gFnSc6	Grenada
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
k	k	k7c3	k
puči	puč	k1gInSc3	puč
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
marxisticko-leninistická	marxistickoeninistický	k2eAgFnSc1d1	marxisticko-leninistická
vláda	vláda	k1gFnSc1	vláda
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
s	s	k7c7	s
kubánskou	kubánský	k2eAgFnSc7d1	kubánská
pomocí	pomoc	k1gFnSc7	pomoc
rozvoj	rozvoj	k1gInSc1	rozvoj
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
moci	moct	k5eAaImF	moct
Bernard	Bernard	k1gMnSc1	Bernard
Coard	Coard	k1gMnSc1	Coard
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
rezoluce	rezoluce	k1gFnSc1	rezoluce
OECS	OECS	kA	OECS
(	(	kIx(	(
<g/>
Organisation	Organisation	k1gInSc1	Organisation
of	of	k?	of
Eastern	Eastern	k1gInSc1	Eastern
Caribbean	Caribbean	k1gMnSc1	Caribbean
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Reagan	Reagan	k1gMnSc1	Reagan
také	také	k9	také
jako	jako	k9	jako
další	další	k2eAgInPc4d1	další
důvody	důvod	k1gInPc4	důvod
uvedl	uvést	k5eAaPmAgMnS	uvést
riziko	riziko	k1gNnSc4	riziko
sovětsko-kubánské	sovětskoubánský	k2eAgFnSc2d1	sovětsko-kubánský
vojenské	vojenský	k2eAgFnSc2d1	vojenská
výstavby	výstavba	k1gFnSc2	výstavba
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
údajně	údajně	k6eAd1	údajně
hrozilo	hrozit	k5eAaImAgNnS	hrozit
stovkám	stovka	k1gFnPc3	stovka
amerických	americký	k2eAgMnPc2d1	americký
studentů	student	k1gMnPc2	student
na	na	k7c6	na
St.	st.	kA	st.
George	Georg	k1gMnSc2	Georg
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
invaze	invaze	k1gFnSc1	invaze
na	na	k7c4	na
Grenadu	Grenada	k1gFnSc4	Grenada
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
operace	operace	k1gFnSc1	operace
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
od	od	k7c2	od
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
americkému	americký	k2eAgNnSc3d1	americké
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
19	[number]	k4	19
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
116	[number]	k4	116
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
americké	americký	k2eAgFnPc1d1	americká
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
přijal	přijmout	k5eAaPmAgMnS	přijmout
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
nominaci	nominace	k1gFnSc4	nominace
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
dobré	dobrý	k2eAgFnSc2d1	dobrá
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
zlepšující	zlepšující	k2eAgFnSc4d1	zlepšující
se	se	k3xPyFc4	se
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
úspěchy	úspěch	k1gInPc1	úspěch
amerických	americký	k2eAgMnPc2d1	americký
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zahájil	zahájit	k5eAaPmAgInS	zahájit
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Reaganovy	Reaganův	k2eAgFnPc1d1	Reaganova
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
pronesl	pronést	k5eAaPmAgMnS	pronést
Mondale	Mondala	k1gFnSc3	Mondala
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
vyhrát	vyhrát	k5eAaPmF	vyhrát
prakticky	prakticky	k6eAd1	prakticky
připravil	připravit	k5eAaPmAgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Reagan	Reagan	k1gMnSc1	Reagan
zvedne	zvednout	k5eAaPmIp3nS	zvednout
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
já	já	k3xPp1nSc1	já
udělám	udělat	k5eAaPmIp1nS	udělat
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgFnPc4	samý
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc4	ten
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
teď	teď	k6eAd1	teď
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reagan	Reagan	k1gMnSc1	Reagan
podal	podat	k5eAaPmAgMnS	podat
slabý	slabý	k2eAgInSc4d1	slabý
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
první	první	k4xOgFnSc6	první
debatě	debata	k1gFnSc6	debata
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
začalo	začít	k5eAaPmAgNnS	začít
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ještě	ještě	k6eAd1	ještě
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
druhé	druhý	k4xOgNnSc4	druhý
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgInS	dokázat
se	se	k3xPyFc4	se
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
vzchopit	vzchopit	k5eAaPmF	vzchopit
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
debatě	debata	k1gFnSc6	debata
již	již	k6eAd1	již
dominoval	dominovat	k5eAaImAgMnS	dominovat
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
protikandidátem	protikandidát	k1gMnSc7	protikandidát
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
dominoval	dominovat	k5eAaImAgInS	dominovat
i	i	k9	i
průzkumům	průzkum	k1gInPc3	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
debatě	debata	k1gFnSc6	debata
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
týkající	týkající	k2eAgFnSc4d1	týkající
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc2	jeho
věku	věk	k1gInSc2	věk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebudu	být	k5eNaImBp1nS	být
z	z	k7c2	z
toho	ten	k3xDgMnSc2	ten
dělat	dělat	k5eAaImF	dělat
téma	téma	k1gNnSc4	téma
své	svůj	k3xOyFgFnSc2	svůj
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Nehodlám	hodlat	k5eNaImIp1nS	hodlat
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
poukazovat	poukazovat	k5eAaImF	poukazovat
na	na	k7c4	na
mládí	mládí	k1gNnSc4	mládí
a	a	k8xC	a
nezkušenost	nezkušenost	k1gFnSc4	nezkušenost
svého	svůj	k3xOyFgMnSc2	svůj
protikandidáta	protikandidát	k1gMnSc2	protikandidát
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
a	a	k8xC	a
také	také	k6eAd1	také
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc4	svůj
protikandidáta	protikandidát	k1gMnSc4	protikandidát
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
smích	smích	k1gInSc1	smích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgMnS	být
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgInS	porazit
Waltera	Walter	k1gMnSc4	Walter
Mondalea	Mondaleus	k1gMnSc4	Mondaleus
ve	v	k7c6	v
49	[number]	k4	49
z	z	k7c2	z
50	[number]	k4	50
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Mondale	Mondala	k1gFnSc6	Mondala
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jen	jen	k9	jen
v	v	k7c6	v
Minnesotě	Minnesota	k1gFnSc6	Minnesota
a	a	k8xC	a
v	v	k7c6	v
District	Districtum	k1gNnPc2	Districtum
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
získal	získat	k5eAaPmAgMnS	získat
takřka	takřka	k6eAd1	takřka
60	[number]	k4	60
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Mondale	Mondala	k1gFnSc3	Mondala
jen	jen	k6eAd1	jen
okolo	okolo	k7c2	okolo
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
tak	tak	k9	tak
získal	získat	k5eAaPmAgMnS	získat
hlasy	hlas	k1gInPc4	hlas
rekordních	rekordní	k2eAgMnPc2d1	rekordní
525	[number]	k4	525
volitelů	volitel	k1gMnPc2	volitel
(	(	kIx(	(
<g/>
z	z	k7c2	z
538	[number]	k4	538
možných	možný	k2eAgFnPc2d1	možná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
období	období	k1gNnSc4	období
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1985	[number]	k4	1985
neveřejně	veřejně	k6eNd1	veřejně
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
ceremonie	ceremonie	k1gFnSc1	ceremonie
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Kapitolu	Kapitol	k1gInSc6	Kapitol
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
20	[number]	k4	20
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
připadl	připadnout	k5eAaPmAgInS	připadnout
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
veřejné	veřejný	k2eAgInPc1d1	veřejný
obřady	obřad	k1gInPc1	obřad
nekonají	konat	k5eNaImIp3nP	konat
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejchladnějších	chladný	k2eAgInPc2d3	nejchladnější
dní	den	k1gInPc2	den
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Washingtonu	Washington	k1gInSc2	Washington
D.	D.	kA	D.
C.	C.	kA	C.
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
ceremonie	ceremonie	k1gFnSc1	ceremonie
odehrála	odehrát	k5eAaPmAgFnS	odehrát
uvnitř	uvnitř	k7c2	uvnitř
Kapitolu	Kapitol	k1gInSc2	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
prodělal	prodělat	k5eAaPmAgMnS	prodělat
Reagan	Reagan	k1gMnSc1	Reagan
operaci	operace	k1gFnSc4	operace
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
tračníku	tračník	k1gInSc2	tračník
odstraněny	odstranit	k5eAaPmNgInP	odstranit
polypy	polyp	k1gInPc1	polyp
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
25	[number]	k4	25
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
omezenou	omezený	k2eAgFnSc4d1	omezená
dobu	doba	k1gFnSc4	doba
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1987	[number]	k4	1987
prodělal	prodělat	k5eAaPmAgMnS	prodělat
Reagan	Reagan	k1gMnSc1	Reagan
operaci	operace	k1gFnSc3	operace
kvůli	kvůli	k7c3	kvůli
rakovině	rakovina	k1gFnSc3	rakovina
prostaty	prostata	k1gFnSc2	prostata
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
další	další	k2eAgFnPc4d1	další
obavy	obava	k1gFnPc4	obava
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
76	[number]	k4	76
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
navštívili	navštívit	k5eAaPmAgMnP	navštívit
manželé	manžel	k1gMnPc1	manžel
Reaganovi	Reagan	k1gMnSc3	Reagan
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Bitburgu	Bitburg	k1gInSc6	Bitburg
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
Reagan	Reagan	k1gMnSc1	Reagan
položit	položit	k5eAaPmF	položit
věnec	věnec	k1gInSc4	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
židovští	židovský	k2eAgMnPc1d1	židovský
představitelé	představitel	k1gMnPc1	představitel
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
také	také	k9	také
47	[number]	k4	47
příslušníků	příslušník	k1gMnPc2	příslušník
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Elie	Elie	k6eAd1	Elie
Wiesel	Wiesel	k1gMnSc1	Wiesel
přemlouval	přemlouvat	k5eAaImAgMnS	přemlouvat
Reagana	Reagan	k1gMnSc4	Reagan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hřbitov	hřbitov	k1gInSc1	hřbitov
nenavštívil	navštívit	k5eNaPmAgInS	navštívit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Reagan	Reagan	k1gMnSc1	Reagan
oponoval	oponovat	k5eAaImAgMnS	oponovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
špatné	špatný	k2eAgNnSc1d1	špatné
porušit	porušit	k5eAaPmF	porušit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
Helmutu	Helmut	k1gMnSc3	Helmut
Kohlovi	Kohl	k1gMnSc3	Kohl
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
tedy	tedy	k8xC	tedy
věnec	věnec	k1gInSc4	věnec
položili	položit	k5eAaPmAgMnP	položit
za	za	k7c2	za
Reaganovy	Reaganův	k2eAgFnSc2d1	Reaganova
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dva	dva	k4xCgMnPc4	dva
generálové	generál	k1gMnPc1	generál
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrativa	administrativa	k1gFnSc1	administrativa
byla	být	k5eAaImAgFnS	být
také	také	k9	také
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
pomalý	pomalý	k2eAgInSc4d1	pomalý
postup	postup	k1gInSc4	postup
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
epidemií	epidemie	k1gFnPc2	epidemie
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
slavný	slavný	k2eAgMnSc1d1	slavný
herec	herec	k1gMnSc1	herec
Rock	rock	k1gInSc1	rock
Hudson	Hudson	k1gInSc1	Hudson
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
6	[number]	k4	6
000	[number]	k4	000
jich	on	k3xPp3gMnPc2	on
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Reagan	Reagan	k1gMnSc1	Reagan
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
úmyslně	úmyslně	k6eAd1	úmyslně
zaměstnávat	zaměstnávat	k5eAaImF	zaměstnávat
ilegální	ilegální	k2eAgMnPc4d1	ilegální
imigranty	imigrant	k1gMnPc4	imigrant
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
zaměstnavatelích	zaměstnavatel	k1gMnPc6	zaměstnavatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
potvrdit	potvrdit	k5eAaPmF	potvrdit
právní	právní	k2eAgNnSc4d1	právní
postavení	postavení	k1gNnSc4	postavení
zaměstnávaných	zaměstnávaný	k2eAgMnPc2d1	zaměstnávaný
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
amnestoval	amnestovat	k5eAaBmAgMnS	amnestovat
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
ilegálně	ilegálně	k6eAd1	ilegálně
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
však	však	k9	však
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sankce	sankce	k1gFnPc4	sankce
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
nebudou	být	k5eNaImBp3nP	být
vymahatelné	vymahatelný	k2eAgInPc1d1	vymahatelný
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
nijak	nijak	k6eAd1	nijak
ilegální	ilegální	k2eAgNnSc4d1	ilegální
přistěhovalectví	přistěhovalectví	k1gNnSc4	přistěhovalectví
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
obřadu	obřad	k1gInSc2	obřad
u	u	k7c2	u
nově	nově	k6eAd1	nově
zrestaurované	zrestaurovaný	k2eAgFnSc2d1	zrestaurovaná
Sochy	socha	k1gFnSc2	socha
svobody	svoboda	k1gFnSc2	svoboda
zákon	zákon	k1gInSc1	zákon
podepsán	podepsat	k5eAaPmNgInS	podepsat
<g/>
,	,	kIx,	,
Reagan	Reagan	k1gMnSc1	Reagan
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
značně	značně	k6eAd1	značně
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
životy	život	k1gInPc4	život
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
musí	muset	k5eAaImIp3nS	muset
skrývat	skrývat	k5eAaImF	skrývat
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
výhodám	výhoda	k1gFnPc3	výhoda
otevřené	otevřený	k2eAgFnSc2d1	otevřená
a	a	k8xC	a
svobodné	svobodný	k2eAgFnSc2d1	svobodná
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
bude	být	k5eAaImBp3nS	být
mnoho	mnoho	k6eAd1	mnoho
těchto	tento	k3xDgMnPc2	tento
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
moci	moc	k1gFnSc2	moc
vyjít	vyjít	k5eAaPmF	vyjít
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
Američany	Američan	k1gMnPc4	Američan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aféra	aféra	k1gFnSc1	aféra
Írán-Contras	Írán-Contras	k1gInSc1	Írán-Contras
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrativa	administrativa	k1gFnSc1	administrativa
prodávala	prodávat	k5eAaImAgFnS	prodávat
ilegálně	ilegálně	k6eAd1	ilegálně
zbraně	zbraň	k1gFnPc4	zbraň
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
z	z	k7c2	z
výtěžku	výtěžek	k1gInSc2	výtěžek
financovala	financovat	k5eAaBmAgFnS	financovat
Contras	Contras	k1gInSc4	Contras
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
zemí	zem	k1gFnPc2	zem
podporujících	podporující	k2eAgInPc2d1	podporující
terorismus	terorismus	k1gInSc4	terorismus
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
tam	tam	k6eAd1	tam
exportovat	exportovat	k5eAaBmF	exportovat
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
si	se	k3xPyFc3	se
administrativa	administrativa	k1gFnSc1	administrativa
slibovala	slibovat	k5eAaImAgFnS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
docílí	docílit	k5eAaPmIp3nP	docílit
propuštění	propuštění	k1gNnSc4	propuštění
amerických	americký	k2eAgNnPc2d1	americké
rukojmí	rukojmí	k1gNnPc2	rukojmí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zadržováni	zadržovat	k5eAaImNgMnP	zadržovat
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Aféra	aféra	k1gFnSc1	aféra
Írán-Contras	Írán-Contrasa	k1gFnPc2	Írán-Contrasa
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc7d3	veliký
americkou	americký	k2eAgFnSc7d1	americká
politickou	politický	k2eAgFnSc7d1	politická
aférou	aféra	k1gFnSc7	aféra
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
neví	vědět	k5eNaImIp3nS	vědět
a	a	k8xC	a
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
nezávislém	závislý	k2eNgNnSc6d1	nezávislé
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
porušily	porušit	k5eAaPmAgFnP	porušit
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
úmluvami	úmluva	k1gFnPc7	úmluva
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
zvyklostí	zvyklost	k1gFnSc7	zvyklost
vměšovaly	vměšovat	k5eAaImAgFnP	vměšovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Muskie	Muskie	k1gFnSc2	Muskie
a	a	k8xC	a
Brent	Brent	k?	Brent
Scowcroft	Scowcroft	k1gMnSc1	Scowcroft
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
utvořili	utvořit	k5eAaPmAgMnP	utvořit
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
skandál	skandál	k1gInSc1	skandál
<g/>
.	.	kIx.	.
</s>
<s>
Konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
neměl	mít	k5eNaImAgMnS	mít
kompletní	kompletní	k2eAgFnPc4d1	kompletní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
programu	program	k1gInSc6	program
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nelhal	lhát	k5eNaImAgMnS	lhát
úmyslně	úmyslně	k6eAd1	úmyslně
americkému	americký	k2eAgInSc3d1	americký
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
komisí	komise	k1gFnSc7	komise
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
podřízené	podřízený	k1gMnPc4	podřízený
dostatečně	dostatečně	k6eAd1	dostatečně
nedohlížel	dohlížet	k5eNaImAgMnS	dohlížet
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
ponětí	ponětí	k1gNnSc4	ponětí
o	o	k7c4	o
jejich	jejich	k3xOp3gFnPc4	jejich
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
financování	financování	k1gNnSc4	financování
Contras	Contrasa	k1gFnPc2	Contrasa
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
Kongres	kongres	k1gInSc1	kongres
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
prezident	prezident	k1gMnSc1	prezident
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k9	co
dělali	dělat	k5eAaImAgMnP	dělat
jeho	jeho	k3xOp3gMnPc1	jeho
poradci	poradce	k1gMnPc1	poradce
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
to	ten	k3xDgNnSc1	ten
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Caspar	Caspar	k1gMnSc1	Caspar
Weinberger	Weinberger	k1gMnSc1	Weinberger
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
křivé	křivý	k2eAgFnSc2d1	křivá
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
Bushem	Bush	k1gMnSc7	Bush
st.	st.	kA	st.
omilostněn	omilostnit	k5eAaPmNgInS	omilostnit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Středoameričanů	Středoameričan	k1gMnPc2	Středoameričan
kritizovalo	kritizovat	k5eAaImAgNnS	kritizovat
Reagana	Reagan	k1gMnSc4	Reagan
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
Contras	Contrasa	k1gFnPc2	Contrasa
a	a	k8xC	a
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
fanatický	fanatický	k2eAgMnSc1d1	fanatický
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nehleděl	hledět	k5eNaImAgMnS	hledět
na	na	k7c4	na
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zachránil	zachránit	k5eAaPmAgInS	zachránit
střední	střední	k2eAgFnSc4d1	střední
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Daniel	Daniel	k1gMnSc1	Daniel
Ortega	Ortega	k1gFnSc1	Ortega
<g/>
,	,	kIx,	,
sandinistický	sandinistický	k2eAgMnSc1d1	sandinistický
vládce	vládce	k1gMnSc2	vládce
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
promine	prominout	k5eAaPmIp3nS	prominout
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
špinavou	špinavý	k2eAgFnSc4d1	špinavá
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
Nikaragui	Nikaragua	k1gFnSc3	Nikaragua
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Reagana	Reagan	k1gMnSc2	Reagan
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
eskalaci	eskalace	k1gFnSc3	eskalace
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
a	a	k8xC	a
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
politiky	politika	k1gFnSc2	politika
détente	détente	k2eAgFnSc2d1	détente
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrativa	administrativa	k1gFnSc1	administrativa
zavedla	zavést	k5eAaPmAgFnS	zavést
nové	nový	k2eAgInPc4d1	nový
zbrojní	zbrojní	k2eAgInPc4d1	zbrojní
programy	program	k1gInPc4	program
namířené	namířený	k2eAgInPc4d1	namířený
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
:	:	kIx,	:
obnovení	obnovení	k1gNnSc3	obnovení
vývoje	vývoj	k1gInSc2	vývoj
bombardéru	bombardér	k1gInSc2	bombardér
B-	B-	k1gFnSc2	B-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vývoj	vývoj	k1gInSc4	vývoj
předtím	předtím	k6eAd1	předtím
zastavila	zastavit	k5eAaPmAgFnS	zastavit
Carterova	Carterův	k2eAgFnSc1d1	Carterova
administrativa	administrativa	k1gFnSc1	administrativa
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
raket	raketa	k1gFnPc2	raketa
MX	MX	kA	MX
"	"	kIx"	"
<g/>
Peacekeeper	Peacekeeper	k1gMnSc1	Peacekeeper
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
rozmístění	rozmístění	k1gNnSc6	rozmístění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
střel	střela	k1gFnPc2	střela
SS-20	SS-20	k1gFnSc1	SS-20
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
rozmístěny	rozmístit	k5eAaPmNgInP	rozmístit
rakety	raketa	k1gFnSc2	raketa
Pershing	Pershing	k1gInSc1	Pershing
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejkontroverznějších	kontroverzní	k2eAgInPc2d3	nejkontroverznější
programů	program	k1gInPc2	program
byla	být	k5eAaImAgFnS	být
Strategická	strategický	k2eAgFnSc1d1	strategická
obranná	obranný	k2eAgFnSc1d1	obranná
iniciativa	iniciativa	k1gFnSc1	iniciativa
(	(	kIx(	(
<g/>
Strategic	Strategic	k1gMnSc1	Strategic
Defense	defense	k1gFnSc2	defense
Initiative	Initiativ	k1gInSc5	Initiativ
<g/>
,	,	kIx,	,
SDI	SDI	kA	SDI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
vyvinout	vyvinout	k5eAaPmF	vyvinout
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
ochránil	ochránit	k5eAaPmAgInS	ochránit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
balistickými	balistický	k2eAgFnPc7d1	balistická
raketami	raketa	k1gFnPc7	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
obranný	obranný	k2eAgInSc1d1	obranný
systém	systém	k1gInSc1	systém
učiní	učinit	k5eAaPmIp3nS	učinit
jadernou	jaderný	k2eAgFnSc4d1	jaderná
válku	válka	k1gFnSc4	válka
neuskutečnitelnou	uskutečnitelný	k2eNgFnSc4d1	neuskutečnitelná
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
však	však	k9	však
nevěřili	věřit	k5eNaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc1	projekt
technologicky	technologicky	k6eAd1	technologicky
realizovatelný	realizovatelný	k2eAgInSc1d1	realizovatelný
a	a	k8xC	a
překřtili	překřtít	k5eAaPmAgMnP	překřtít
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
"	"	kIx"	"
<g/>
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
SDI	SDI	kA	SDI
znepokojeni	znepokojen	k2eAgMnPc1d1	znepokojen
a	a	k8xC	a
Jurij	Jurij	k1gMnSc1	Jurij
Andropov	Andropov	k1gInSc4	Andropov
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
rizikem	riziko	k1gNnSc7	riziko
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
SDI	SDI	kA	SDI
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
Národní	národní	k2eAgFnSc2d1	národní
asociace	asociace	k1gFnSc2	asociace
evangelíků	evangelík	k1gMnPc2	evangelík
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1983	[number]	k4	1983
nazval	nazvat	k5eAaBmAgMnS	nazvat
Reagan	Reagan	k1gMnSc1	Reagan
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
"	"	kIx"	"
<g/>
Říší	říš	k1gFnPc2	říš
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1983	[number]	k4	1983
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunismus	komunismus	k1gInSc1	komunismus
padne	padnout	k5eAaImIp3nS	padnout
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
smutnou	smutný	k2eAgFnSc4d1	smutná
a	a	k8xC	a
bizarní	bizarní	k2eAgFnSc4d1	bizarní
kapitolu	kapitola	k1gFnSc4	kapitola
lidských	lidský	k2eAgFnPc2d1	lidská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
poslední	poslední	k2eAgFnPc1d1	poslední
kapitoly	kapitola	k1gFnPc1	kapitola
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
píšou	psát	k5eAaImIp3nP	psát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Sověti	Sovět	k1gMnPc1	Sovět
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1983	[number]	k4	1983
sestřelili	sestřelit	k5eAaPmAgMnP	sestřelit
civilní	civilní	k2eAgInSc4d1	civilní
let	let	k1gInSc4	let
KAL	kal	k1gInSc1	kal
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
Reagan	Reagan	k1gMnSc1	Reagan
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
za	za	k7c4	za
masakr	masakr	k1gInSc4	masakr
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Sověti	Sovět	k1gMnPc1	Sovět
se	se	k3xPyFc4	se
otočili	otočit	k5eAaPmAgMnP	otočit
proti	proti	k7c3	proti
světu	svět	k1gInSc3	svět
a	a	k8xC	a
morálním	morální	k2eAgFnPc3d1	morální
zásadám	zásada	k1gFnPc3	zásada
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
mezilidské	mezilidský	k2eAgInPc1d1	mezilidský
vztahy	vztah	k1gInPc1	vztah
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrativa	administrativa	k1gFnSc1	administrativa
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
zrušením	zrušení	k1gNnSc7	zrušení
všech	všecek	k3xTgInPc2	všecek
civilních	civilní	k2eAgInPc2d1	civilní
leteckých	letecký	k2eAgInPc2d1	letecký
spojů	spoj	k1gInPc2	spoj
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
jednání	jednání	k1gNnSc4	jednání
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
smlouvách	smlouva	k1gFnPc6	smlouva
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
finančně	finančně	k6eAd1	finančně
poškodili	poškodit	k5eAaPmAgMnP	poškodit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
nazývána	nazývat	k5eAaImNgFnS	nazývat
též	též	k9	též
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
doktrína	doktrína	k1gFnSc1	doktrína
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jako	jako	k8xS	jako
agresivní	agresivní	k2eAgFnSc1d1	agresivní
a	a	k8xC	a
imperialistická	imperialistický	k2eAgFnSc1d1	imperialistická
a	a	k8xC	a
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
oponenty	oponent	k1gMnPc4	oponent
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
válečného	válečný	k2eAgMnSc4d1	válečný
štváče	štváč	k1gMnSc4	štváč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
událostem	událost	k1gFnPc3	událost
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačovo	k1gNnPc2	Gorbačovo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zavedl	zavést	k5eAaPmAgMnS	zavést
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
politiky	politika	k1gFnPc4	politika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
větší	veliký	k2eAgFnSc2d2	veliký
otevřenosti	otevřenost	k1gFnSc2	otevřenost
režimu	režim	k1gInSc2	režim
<g/>
:	:	kIx,	:
Glasnosť	glasnosť	k1gFnSc4	glasnosť
a	a	k8xC	a
perestrojku	perestrojka	k1gFnSc4	perestrojka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
stahovat	stahovat	k5eAaImF	stahovat
kvalitativní	kvalitativní	k2eAgInSc4d1	kvalitativní
náskok	náskok	k1gInSc4	náskok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
si	se	k3xPyFc3	se
stihla	stihnout	k5eAaPmAgFnS	stihnout
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
vybudovat	vybudovat	k5eAaPmF	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
však	však	k9	však
sovětský	sovětský	k2eAgInSc1d1	sovětský
rozpočet	rozpočet	k1gInSc1	rozpočet
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
značného	značný	k2eAgInSc2d1	značný
deficitu	deficit	k1gInSc2	deficit
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tak	tak	k6eAd1	tak
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Spojeným	spojený	k2eAgNnSc7d1	spojené
státům	stát	k1gInPc3	stát
ústupky	ústupek	k1gInPc4	ústupek
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
počtu	počet	k1gInSc2	počet
konvenčních	konvenční	k2eAgFnPc2d1	konvenční
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
vůči	vůči	k7c3	vůči
východní	východní	k2eAgFnSc3d1	východní
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
svou	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
Gorbačova	Gorbačův	k2eAgMnSc4d1	Gorbačův
podpořit	podpořit	k5eAaPmF	podpořit
v	v	k7c4	v
provádění	provádění	k1gNnSc4	provádění
dalších	další	k2eAgFnPc2d1	další
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
se	s	k7c7	s
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
Reaganem	Reagan	k1gMnSc7	Reagan
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
summitech	summit	k1gInPc6	summit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rejkjavíku	Rejkjavík	k1gInSc6	Rejkjavík
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Sověty	Sovět	k1gMnPc4	Sovět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
začali	začít	k5eAaPmAgMnP	začít
budovat	budovat	k5eAaImF	budovat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
tržní	tržní	k2eAgFnSc4d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
projevu	projev	k1gInSc6	projev
u	u	k7c2	u
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1987	[number]	k4	1987
řekl	říct	k5eAaPmAgMnS	říct
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Generální	generální	k2eAgMnSc1d1	generální
tajemníku	tajemník	k1gMnSc3	tajemník
Gorbačove	Gorbačov	k1gInSc5	Gorbačov
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
hledáte	hledat	k5eAaImIp2nP	hledat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
hledáte	hledat	k5eAaImIp2nP	hledat
prosperitu	prosperita	k1gFnSc4	prosperita
pro	pro	k7c4	pro
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
hledáte	hledat	k5eAaImIp2nP	hledat
liberalizaci	liberalizace	k1gFnSc4	liberalizace
<g/>
:	:	kIx,	:
Pojďte	jít	k5eAaImRp2nP	jít
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
bráně	brána	k1gFnSc3	brána
<g/>
!	!	kIx.	!
</s>
<s>
Pane	Pan	k1gMnSc5	Pan
Gorbačove	Gorbačov	k1gInSc5	Gorbačov
<g/>
,	,	kIx,	,
otevřete	otevřít	k5eAaPmIp2nP	otevřít
tuto	tento	k3xDgFnSc4	tento
bránu	brána	k1gFnSc4	brána
<g/>
!	!	kIx.	!
</s>
<s>
Pane	Pan	k1gMnSc5	Pan
Gorbačove	Gorbačov	k1gInSc5	Gorbačov
<g/>
,	,	kIx,	,
strhněte	strhnout	k5eAaPmRp2nP	strhnout
tuto	tento	k3xDgFnSc4	tento
zeď	zeď	k1gFnSc4	zeď
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Při	při	k7c6	při
třetím	třetí	k4xOgInSc6	třetí
summitu	summit	k1gInSc6	summit
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.	D.	kA	D.
C.	C.	kA	C.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Reagan	Reagan	k1gMnSc1	Reagan
a	a	k8xC	a
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
Intermediate-Range	Intermediate-Rang	k1gFnSc2	Intermediate-Rang
Nuclear	Nucleara	k1gFnPc2	Nucleara
Forces	Forces	k1gMnSc1	Forces
Treaty	Treata	k1gFnSc2	Treata
(	(	kIx(	(
<g/>
INF	INF	kA	INF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
eliminovala	eliminovat	k5eAaBmAgFnS	eliminovat
všechny	všechen	k3xTgInPc4	všechen
americké	americký	k2eAgInPc4d1	americký
a	a	k8xC	a
sovětské	sovětský	k2eAgFnPc4d1	sovětská
rakety	raketa	k1gFnPc4	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
summit	summit	k1gInSc1	summit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Rusy	Rus	k1gMnPc4	Rus
považován	považován	k2eAgMnSc1d1	považován
takřka	takřka	k6eAd1	takřka
za	za	k7c4	za
celebritu	celebrita	k1gFnSc4	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
novinář	novinář	k1gMnSc1	novinář
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
stále	stále	k6eAd1	stále
považuje	považovat	k5eAaImIp3nS	považovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
za	za	k7c4	za
říši	říše	k1gFnSc4	říše
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
<g/>
,	,	kIx,	,
že	že	k8xS	že
předtím	předtím	k6eAd1	předtím
"	"	kIx"	"
<g/>
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
jiné	jiný	k2eAgFnSc6d1	jiná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
o	o	k7c6	o
jiné	jiný	k2eAgFnSc6d1	jiná
éře	éra	k1gFnSc6	éra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
na	na	k7c4	na
Gorbačovovu	Gorbačovův	k2eAgFnSc4d1	Gorbačovova
žádost	žádost	k1gFnSc4	žádost
Reagan	Reagan	k1gMnSc1	Reagan
vedl	vést	k5eAaImAgMnS	vést
na	na	k7c6	na
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
universitě	universita	k1gFnSc6	universita
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
svobodném	svobodný	k2eAgInSc6d1	svobodný
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Life	Life	k1gInSc4	Life
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Reagan	Reagan	k1gMnSc1	Reagan
optimismus	optimismus	k1gInSc4	optimismus
nad	nad	k7c7	nad
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
sympatie	sympatie	k1gFnPc4	sympatie
vůči	vůči	k7c3	vůči
Gorbačovovi	Gorbačova	k1gMnSc3	Gorbačova
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
Gorbačovovu	Gorbačovův	k2eAgFnSc4d1	Gorbačovova
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obával	obávat	k5eAaImAgMnS	obávat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
silně	silně	k6eAd1	silně
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
může	moct	k5eAaImIp3nS	moct
prosazovat	prosazovat	k5eAaImF	prosazovat
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
riskoval	riskovat	k5eAaBmAgInS	riskovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
padla	padnout	k5eAaImAgFnS	padnout
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
padl	padnout	k5eAaPmAgInS	padnout
i	i	k9	i
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
své	svůj	k3xOyFgInPc4	svůj
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
emoce	emoce	k1gFnSc2	emoce
svým	svůj	k3xOyFgInSc7	svůj
osobitým	osobitý	k2eAgInSc7d1	osobitý
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
příležitostech	příležitost	k1gFnPc6	příležitost
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
komunikátor	komunikátor	k1gInSc1	komunikátor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
schopnosti	schopnost	k1gFnPc4	schopnost
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
i	i	k9	i
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
politické	politický	k2eAgFnSc6d1	politická
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgInS	být
okouzlen	okouzlit	k5eAaPmNgInS	okouzlit
Rooseveltovými	Rooseveltův	k2eAgInPc7d1	Rooseveltův
útoky	útok	k1gInPc7	útok
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
energickou	energický	k2eAgFnSc7d1	energická
obranou	obrana	k1gFnSc7	obrana
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
také	také	k9	také
napodoboval	napodobovat	k5eAaImAgMnS	napodobovat
Rooseveltův	Rooseveltův	k2eAgInSc4d1	Rooseveltův
styl	styl	k1gInSc4	styl
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
svých	svůj	k3xOyFgFnPc2	svůj
rétorických	rétorický	k2eAgFnPc2d1	rétorická
schopností	schopnost	k1gFnPc2	schopnost
využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
legitimitu	legitimita	k1gFnSc4	legitimita
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
říši	říše	k1gFnSc4	říše
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
pošramocené	pošramocený	k2eAgFnSc2d1	pošramocená
americké	americký	k2eAgFnSc2d1	americká
pýchy	pýcha	k1gFnSc2	pýcha
<g/>
.	.	kIx.	.
</s>
<s>
Najal	najmout	k5eAaPmAgMnS	najmout
si	se	k3xPyFc3	se
autory	autor	k1gMnPc4	autor
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uměli	umět	k5eAaImAgMnP	umět
zachytit	zachytit	k5eAaPmF	zachytit
jeho	jeho	k3xOp3gInSc4	jeho
lidový	lidový	k2eAgInSc4d1	lidový
šarm	šarm	k1gInSc4	šarm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
profesor	profesor	k1gMnSc1	profesor
Max	Max	k1gMnSc1	Max
Atkinson	Atkinson	k1gMnSc1	Atkinson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
poradcem	poradce	k1gMnSc7	poradce
britského	britský	k2eAgNnSc2d1	Britské
politika	politikum	k1gNnSc2	politikum
Paddy	Padda	k1gMnSc2	Padda
Ashdowna	Ashdown	k1gMnSc2	Ashdown
<g/>
,	,	kIx,	,
seminář	seminář	k1gInSc1	seminář
o	o	k7c4	o
psaní	psaní	k1gNnSc4	psaní
projevů	projev	k1gInPc2	projev
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Reaganův	Reaganův	k2eAgInSc1d1	Reaganův
rétorický	rétorický	k2eAgInSc1d1	rétorický
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
silný	silný	k2eAgInSc4d1	silný
jazyk	jazyk	k1gInSc4	jazyk
k	k	k7c3	k
odsouzení	odsouzení	k1gNnSc3	odsouzení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
během	během	k7c2	během
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uměl	umět	k5eAaImAgMnS	umět
také	také	k9	také
vyvolat	vyvolat	k5eAaPmF	vyvolat
optimistické	optimistický	k2eAgInPc4d1	optimistický
ideály	ideál	k1gInPc4	ideál
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jako	jako	k8xC	jako
obránce	obránce	k1gMnSc4	obránce
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1964	[number]	k4	1964
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
"	"	kIx"	"
<g/>
A	a	k8xC	a
Time	Time	k1gFnSc3	Time
for	forum	k1gNnPc2	forum
Choosing	Choosing	k1gInSc1	Choosing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Čas	čas	k1gInSc1	čas
pro	pro	k7c4	pro
volbu	volba	k1gFnSc4	volba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
frázi	fráze	k1gFnSc4	fráze
"	"	kIx"	"
<g/>
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
osudem	osud	k1gInSc7	osud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rendezvous	rendezvous	k1gInSc1	rendezvous
with	witha	k1gFnPc2	witha
destiny	destina	k1gFnSc2	destina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
proslavil	proslavit	k5eAaPmAgMnS	proslavit
právě	právě	k9	právě
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
projevech	projev	k1gInPc6	projev
označil	označit	k5eAaPmAgMnS	označit
Ameriku	Amerika	k1gFnSc4	Amerika
za	za	k7c4	za
"	"	kIx"	"
<g/>
zářící	zářící	k2eAgNnSc4d1	zářící
město	město	k1gNnSc4	město
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
šlechetné	šlechetný	k2eAgNnSc4d1	šlechetné
<g/>
,	,	kIx,	,
idealistické	idealistický	k2eAgNnSc4d1	idealistické
<g/>
,	,	kIx,	,
odvážné	odvážný	k2eAgNnSc4d1	odvážné
<g/>
,	,	kIx,	,
slušné	slušný	k2eAgNnSc4d1	slušné
a	a	k8xC	a
férové	férový	k2eAgNnSc4d1	férové
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
občané	občan	k1gMnPc1	občan
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
právo	právo	k1gNnSc4	právo
snít	snít	k5eAaPmF	snít
heroické	heroický	k2eAgInPc4d1	heroický
sny	sen	k1gInPc4	sen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challenger	k1gInSc1	Challenger
odložil	odložit	k5eAaPmAgInS	odložit
poselství	poselství	k1gNnSc4	poselství
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
národu	národ	k1gInSc3	národ
o	o	k7c6	o
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
napsala	napsat	k5eAaPmAgFnS	napsat
Peggy	Pegg	k1gInPc4	Pegg
Noonan	Noonana	k1gFnPc2	Noonana
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nezapomeneme	zapomnět	k5eNaImIp1nP	zapomnět
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c4	na
poslední	poslední	k2eAgInSc4d1	poslední
okamžik	okamžik	k1gInSc4	okamžik
kdy	kdy	k6eAd1	kdy
jsme	být	k5eAaImIp1nP	být
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ráno	ráno	k6eAd1	ráno
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
připravovali	připravovat	k5eAaImAgMnP	připravovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
mávali	mávat	k5eAaImAgMnP	mávat
nám	my	k3xPp1nPc3	my
na	na	k7c4	na
pozdrav	pozdrav	k1gInSc4	pozdrav
a	a	k8xC	a
'	'	kIx"	'
<g/>
vyklouzli	vyklouznout	k5eAaPmAgMnP	vyklouznout
z	z	k7c2	z
pouta	pouto	k1gNnSc2	pouto
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
'	'	kIx"	'
<g/>
dotkli	dotknout	k5eAaPmAgMnP	dotknout
tváře	tvář	k1gFnPc4	tvář
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
výroku	výrok	k1gInSc6	výrok
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
"	"	kIx"	"
<g/>
High	High	k1gMnSc1	High
Flight	Flight	k1gMnSc1	Flight
<g/>
"	"	kIx"	"
Johna	John	k1gMnSc2	John
Gillespie	Gillespie	k1gFnSc2	Gillespie
Mageeho	Magee	k1gMnSc2	Magee
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
zbraní	zbraň	k1gFnSc7	zbraň
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
krátké	krátký	k2eAgInPc4d1	krátký
vtípky	vtípek	k1gInPc4	vtípek
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
své	svůj	k3xOyFgMnPc4	svůj
oponenty	oponent	k1gMnPc4	oponent
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
diváky	divák	k1gMnPc4	divák
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnSc1	diskuze
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
jej	on	k3xPp3gMnSc4	on
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
debatě	debata	k1gFnSc6	debata
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
si	se	k3xPyFc3	se
udělal	udělat	k5eAaPmAgMnS	udělat
legraci	legrace	k1gFnSc4	legrace
ze	z	k7c2	z
svého	svůj	k3xOyFgMnSc2	svůj
protikandidáta	protikandidát	k1gMnSc2	protikandidát
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
byl	být	k5eAaImAgMnS	být
Walter	Walter	k1gMnSc1	Walter
Mondale	Mondala	k1gFnSc3	Mondala
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neudělám	udělat	k5eNaPmIp1nS	udělat
z	z	k7c2	z
věku	věk	k1gInSc2	věk
téma	téma	k1gNnSc2	téma
této	tento	k3xDgFnSc2	tento
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Nehodlám	hodlat	k5eNaImIp1nS	hodlat
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
mládí	mládí	k1gNnSc4	mládí
a	a	k8xC	a
nezkušenost	nezkušenost	k1gFnSc4	nezkušenost
mého	můj	k3xOp1gMnSc2	můj
oponenta	oponent	k1gMnSc2	oponent
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
vtipkoval	vtipkovat	k5eAaImAgMnS	vtipkovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Politika	politika	k1gFnSc1	politika
není	být	k5eNaImIp3nS	být
špatné	špatný	k2eAgNnSc4d1	špatné
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
uspějete	uspět	k5eAaPmIp2nP	uspět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
odměn	odměna	k1gFnPc2	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zdiskreditujete	zdiskreditovat	k5eAaPmIp2nP	zdiskreditovat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
můžete	moct	k5eAaImIp2nP	moct
napsat	napsat	k5eAaPmF	napsat
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
stoupenci	stoupenec	k1gMnPc1	stoupenec
i	i	k8xC	i
oponenti	oponent	k1gMnPc1	oponent
si	se	k3xPyFc3	se
povšimli	povšimnout	k5eAaPmAgMnP	povšimnout
jeho	jeho	k3xOp3gMnPc1	jeho
"	"	kIx"	"
<g/>
zářivého	zářivý	k2eAgInSc2d1	zářivý
optimismu	optimismus	k1gInSc2	optimismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mnozí	mnohý	k2eAgMnPc1d1	mnohý
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
často	často	k6eAd1	často
usměvavým	usměvavý	k2eAgInSc7d1	usměvavý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vážným	vážný	k2eAgMnSc7d1	vážný
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Carterem	Carter	k1gMnSc7	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
jednou	jednou	k6eAd1	jednou
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lekce	lekce	k1gFnPc1	lekce
vůdcovství	vůdcovství	k1gNnSc2	vůdcovství
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
:	:	kIx,	:
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
znalost	znalost	k1gFnSc1	znalost
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
ochota	ochota	k1gFnSc1	ochota
naslouchat	naslouchat	k5eAaImF	naslouchat
a	a	k8xC	a
být	být	k5eAaImF	být
chápající	chápající	k2eAgInSc4d1	chápající
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
povinnost	povinnost	k1gFnSc4	povinnost
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
udělat	udělat	k5eAaPmF	udělat
to	ten	k3xDgNnSc4	ten
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
jménem	jméno	k1gNnSc7	jméno
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
sloužíte	sloužit	k5eAaImIp2nP	sloužit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
prezidenta	prezident	k1gMnSc2	prezident
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
komunikátor	komunikátor	k1gInSc1	komunikátor
<g/>
"	"	kIx"	"
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
nemyslel	myslet	k5eNaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
styl	styl	k1gInSc1	styl
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jsem	být	k5eAaImIp1nS	být
používal	používat	k5eAaImAgInS	používat
byl	být	k5eAaImAgInS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
obsah	obsah	k1gInSc1	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
velký	velký	k2eAgInSc4d1	velký
komunikátor	komunikátor	k1gInSc4	komunikátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komunikoval	komunikovat	k5eAaImAgMnS	komunikovat
jsem	být	k5eAaImIp1nS	být
skvělé	skvělý	k2eAgFnSc2d1	skvělá
věci	věc	k1gFnSc2	věc
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Častým	častý	k2eAgInSc7d1	častý
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
šarmu	šarm	k1gInSc3	šarm
a	a	k8xC	a
rétorickému	rétorický	k2eAgNnSc3d1	rétorické
umění	umění	k1gNnSc3	umění
mohl	moct	k5eAaImAgInS	moct
říci	říct	k5eAaPmF	říct
takřka	takřka	k6eAd1	takřka
cokoliv	cokoliv	k3yInSc1	cokoliv
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
to	ten	k3xDgNnSc1	ten
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
skandálům	skandál	k1gInPc3	skandál
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Teflonový	teflonový	k2eAgMnSc1d1	teflonový
prezident	prezident	k1gMnSc1	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
především	především	k9	především
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
aférou	aféra	k1gFnSc7	aféra
Írán-Contras	Írán-Contrasa	k1gFnPc2	Írán-Contrasa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prodejem	prodej	k1gInSc7	prodej
zbraní	zbraň	k1gFnPc2	zbraň
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
financováním	financování	k1gNnSc7	financování
Nikaragujských	nikaragujský	k2eAgFnPc2d1	Nikaragujská
contras	contrasa	k1gFnPc2	contrasa
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
uznal	uznat	k5eAaPmAgInS	uznat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
vinnými	vinný	k2eAgFnPc7d1	vinná
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
terorismu	terorismus	k1gInSc2	terorismus
v	v	k7c6	v
Nikaraguji	Nikaraguji	k1gFnSc6	Nikaraguji
(	(	kIx(	(
<g/>
případ	případ	k1gInSc1	případ
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
politice	politika	k1gFnSc3	politika
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
často	často	k6eAd1	často
vykreslován	vykreslován	k2eAgMnSc1d1	vykreslován
jako	jako	k8xC	jako
válečný	válečný	k2eAgMnSc1d1	válečný
štváč	štváč	k1gMnSc1	štváč
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jej	on	k3xPp3gInSc4	on
britská	britský	k2eAgFnSc1d1	britská
premiérka	premiérka	k1gFnSc1	premiérka
Margaret	Margareta	k1gFnPc2	Margareta
Thatcher	Thatchra	k1gFnPc2	Thatchra
silně	silně	k6eAd1	silně
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
často	často	k6eAd1	často
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Levice	levice	k1gFnSc1	levice
jej	on	k3xPp3gMnSc4	on
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
za	za	k7c4	za
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
Castrovu	Castrův	k2eAgInSc3d1	Castrův
režimu	režim	k1gInSc3	režim
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
a	a	k8xC	a
za	za	k7c4	za
ignorování	ignorování	k1gNnSc4	ignorování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Reagan	Reagan	k1gMnSc1	Reagan
odmítal	odmítat	k5eAaImAgMnS	odmítat
apartheid	apartheid	k1gInSc4	apartheid
v	v	k7c4	v
JAR	jar	k1gFnSc4	jar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
sankcím	sankce	k1gFnPc3	sankce
<g/>
.	.	kIx.	.
</s>
<s>
Reaganovy	Reaganův	k2eAgFnPc1d1	Reaganova
snahy	snaha	k1gFnPc1	snaha
omezit	omezit	k5eAaPmF	omezit
sociální	sociální	k2eAgFnPc4d1	sociální
podpory	podpora	k1gFnPc4	podpora
a	a	k8xC	a
daně	daň	k1gFnPc4	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
byly	být	k5eAaImAgFnP	být
mnohými	mnohý	k2eAgMnPc7d1	mnohý
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
prospívaly	prospívat	k5eAaImAgFnP	prospívat
jen	jen	k9	jen
bohatým	bohatý	k2eAgMnSc7d1	bohatý
Američanům	Američan	k1gMnPc3	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
pokryly	pokrýt	k5eAaPmAgFnP	pokrýt
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c2	za
Reaganova	Reaganův	k2eAgNnSc2d1	Reaganovo
prezidentství	prezidentství	k1gNnSc2	prezidentství
začal	začít	k5eAaPmAgInS	začít
narůstat	narůstat	k5eAaImF	narůstat
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
si	se	k3xPyFc3	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
půjčovat	půjčovat	k5eAaImF	půjčovat
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
narostl	narůst	k5eAaPmAgInS	narůst
z	z	k7c2	z
26	[number]	k4	26
%	%	kIx~	%
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
na	na	k7c4	na
41	[number]	k4	41
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
míru	míra	k1gFnSc4	míra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
narostl	narůst	k5eAaPmAgInS	narůst
dluh	dluh	k1gInSc4	dluh
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
2,6	[number]	k4	2,6
bilionu	bilion	k4xCgInSc2	bilion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
z	z	k7c2	z
největšího	veliký	k2eAgMnSc2d3	veliký
věřitele	věřitel	k1gMnSc2	věřitel
největším	veliký	k2eAgMnSc7d3	veliký
dlužníkem	dlužník	k1gMnSc7	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Deregulace	deregulace	k1gFnSc1	deregulace
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
bankám	banka	k1gFnPc3	banka
volnější	volný	k2eAgFnSc2d2	volnější
ruce	ruka	k1gFnPc1	ruka
v	v	k7c6	v
investování	investování	k1gNnSc6	investování
peněz	peníze	k1gInPc2	peníze
svých	svůj	k3xOyFgMnPc2	svůj
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pustilo	pustit	k5eAaPmAgNnS	pustit
do	do	k7c2	do
značně	značně	k6eAd1	značně
rizikových	rizikový	k2eAgFnPc2d1	riziková
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Federal	Federal	k1gFnSc1	Federal
Home	Hom	k1gInSc2	Hom
Loan	Loan	k1gInSc4	Loan
Bank	bank	k1gInSc1	bank
Board	Board	k1gInSc1	Board
<g/>
,	,	kIx,	,
federální	federální	k2eAgInSc1d1	federální
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
obor	obor	k1gInSc1	obor
reguloval	regulovat	k5eAaImAgInS	regulovat
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
tento	tento	k3xDgInSc4	tento
trend	trend	k1gInSc4	trend
zarazit	zarazit	k5eAaPmF	zarazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narážel	narážet	k5eAaPmAgMnS	narážet
na	na	k7c4	na
značné	značný	k2eAgInPc4d1	značný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
stála	stát	k5eAaImAgFnS	stát
tato	tento	k3xDgFnSc1	tento
krize	krize	k1gFnSc1	krize
vládu	vláda	k1gFnSc4	vláda
150	[number]	k4	150
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
dnů	den	k1gInPc2	den
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
promluvil	promluvit	k5eAaPmAgMnS	promluvit
Reagan	Reagan	k1gMnSc1	Reagan
z	z	k7c2	z
Oválné	oválný	k2eAgFnSc2d1	oválná
pracovny	pracovna	k1gFnSc2	pracovna
naposledy	naposledy	k6eAd1	naposledy
k	k	k7c3	k
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bushově	Bushův	k2eAgFnSc6d1	Bushova
inauguraci	inaugurace	k1gFnSc6	inaugurace
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
ranč	ranč	k1gInSc4	ranč
poblíž	poblíž	k7c2	poblíž
Santa	Sant	k1gInSc2	Sant
Barbary	Barbara	k1gFnSc2	Barbara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
psal	psát	k5eAaImAgMnS	psát
svou	svůj	k3xOyFgFnSc4	svůj
biografii	biografie	k1gFnSc4	biografie
<g/>
,	,	kIx,	,
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
a	a	k8xC	a
sekal	sekat	k5eAaImAgMnS	sekat
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
domu	dům	k1gInSc2	dům
v	v	k7c4	v
Bel-Air	Bel-Air	k1gInSc4	Bel-Air
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
jej	on	k3xPp3gInSc4	on
najala	najmout	k5eAaPmAgFnS	najmout
japonská	japonský	k2eAgFnSc1d1	japonská
společnost	společnost	k1gFnSc1	společnost
Fujisankei	Fujisanke	k1gFnSc2	Fujisanke
Communications	Communicationsa	k1gFnPc2	Communicationsa
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pronesl	pronést	k5eAaPmAgMnS	pronést
dva	dva	k4xCgInPc4	dva
projevy	projev	k1gInPc4	projev
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
několik	několik	k4yIc4	několik
společenských	společenský	k2eAgFnPc2d1	společenská
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
devět	devět	k4xCc4	devět
dní	den	k1gInPc2	den
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
vydělal	vydělat	k5eAaPmAgMnS	vydělat
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
za	za	k7c4	za
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
příležitostně	příležitostně	k6eAd1	příležitostně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
akcích	akce	k1gFnPc6	akce
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dodatek	dodatek	k1gInSc4	dodatek
k	k	k7c3	k
ústavě	ústava	k1gFnSc3	ústava
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
za	za	k7c4	za
zrušení	zrušení	k1gNnSc4	zrušení
22	[number]	k4	22
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
jen	jen	k6eAd1	jen
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
naposledy	naposledy	k6eAd1	naposledy
promluvil	promluvit	k5eAaPmAgMnS	promluvit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
a	a	k8xC	a
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
naposledy	naposledy	k6eAd1	naposledy
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Národ	národ	k1gInSc1	národ
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
stavu	stav	k1gInSc6	stav
informoval	informovat	k5eAaBmAgMnS	informovat
rukou	ruka	k1gFnPc6	ruka
psaným	psaný	k2eAgInSc7d1	psaný
dopisem	dopis	k1gInSc7	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
typickým	typický	k2eAgInSc7d1	typický
optimismem	optimismus	k1gInSc7	optimismus
napsal	napsat	k5eAaPmAgInS	napsat
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zahájil	zahájit	k5eAaPmAgMnS	zahájit
jsem	být	k5eAaImIp1nS	být
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mě	já	k3xPp1nSc4	já
dovede	dovést	k5eAaPmIp3nS	dovést
až	až	k6eAd1	až
k	k	k7c3	k
soumraku	soumrak	k1gInSc3	soumrak
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ameriku	Amerika	k1gFnSc4	Amerika
budou	být	k5eAaImBp3nP	být
čekat	čekat	k5eAaImF	čekat
jen	jen	k9	jen
jasné	jasný	k2eAgInPc4d1	jasný
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Děkuji	děkovat	k5eAaImIp1nS	děkovat
Vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Vám	vy	k3xPp2nPc3	vy
žehnej	žehnat	k5eAaImRp2nS	žehnat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jak	jak	k8xS	jak
roky	rok	k1gInPc1	rok
postupovaly	postupovat	k5eAaImAgInP	postupovat
<g/>
,	,	kIx,	,
choroba	choroba	k1gFnSc1	choroba
pomalu	pomalu	k6eAd1	pomalu
ničila	ničit	k5eAaImAgFnS	ničit
jeho	jeho	k3xOp3gFnPc4	jeho
mentální	mentální	k2eAgFnPc4d1	mentální
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
donutila	donutit	k5eAaPmAgFnS	donutit
jej	on	k3xPp3gInSc4	on
žít	žít	k5eAaImF	žít
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Reagan	Reagan	k1gMnSc1	Reagan
věku	věk	k1gInSc2	věk
90	[number]	k4	90
let	léto	k1gNnPc2	léto
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
třetím	třetí	k4xOgMnSc7	třetí
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
tohoto	tento	k3xDgInSc2	tento
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
byli	být	k5eAaImAgMnP	být
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
a	a	k8xC	a
Herbert	Herbert	k1gMnSc1	Herbert
Hoover	Hoover	k1gMnSc1	Hoover
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
Gerald	Gerald	k1gInSc1	Gerald
Ford	ford	k1gInSc1	ford
stal	stát	k5eAaPmAgInS	stát
čtvrtým	čtvrtá	k1gFnPc3	čtvrtá
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
tohoto	tento	k3xDgInSc2	tento
věku	věk	k1gInSc2	věk
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
týdny	týden	k1gInPc1	týden
před	před	k7c7	před
svými	svůj	k3xOyFgInPc7	svůj
90	[number]	k4	90
<g/>
.	.	kIx.	.
narozeninami	narozeniny	k1gFnPc7	narozeniny
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
Reagan	Reagan	k1gMnSc1	Reagan
operaci	operace	k1gFnSc4	operace
kyčle	kyčel	k1gFnSc2	kyčel
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
postupující	postupující	k2eAgFnSc3d1	postupující
nemoci	nemoc	k1gFnSc3	nemoc
se	se	k3xPyFc4	se
oslava	oslava	k1gFnSc1	oslava
narozenin	narozeniny	k1gFnPc2	narozeniny
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jen	jen	k9	jen
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
příbuzných	příbuzný	k1gMnPc2	příbuzný
a	a	k8xC	a
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
později	pozdě	k6eAd2	pozdě
řekla	říct	k5eAaPmAgFnS	říct
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Larryho	Larry	k1gMnSc2	Larry
Kinga	King	k1gMnSc2	King
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
mohlo	moct	k5eAaImAgNnS	moct
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
pamatovali	pamatovat	k5eAaImAgMnP	pamatovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgMnSc1	jaký
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Nancy	Nancy	k1gFnSc1	Nancy
Reagan	Reagan	k1gMnSc1	Reagan
dále	daleko	k6eAd2	daleko
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebýt	být	k5eNaImF	být
jeho	jeho	k3xOp3gFnPc4	jeho
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
žít	žít	k5eAaImF	žít
klidně	klidně	k6eAd1	klidně
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
příležitost	příležitost	k1gFnSc4	příležitost
Reagana	Reagan	k1gMnSc2	Reagan
během	během	k7c2	během
nemoci	nemoc	k1gFnSc2	nemoc
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
pokročila	pokročit	k5eAaPmAgFnS	pokročit
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
poznat	poznat	k5eAaPmF	poznat
a	a	k8xC	a
nepamatoval	pamatovat	k5eNaImAgInS	pamatovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
v	v	k7c4	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
PDT	PDT	kA	PDT
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c4	v
Bel-Air	Bel-Air	k1gInSc4	Bel-Air
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pompézním	pompézní	k2eAgInSc6d1	pompézní
státním	státní	k2eAgInSc6d1	státní
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
navštívili	navštívit	k5eAaPmAgMnP	navštívit
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
politici	politik	k1gMnPc1	politik
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c4	v
Ronald	Ronald	k1gInSc4	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Presidential	Presidential	k1gMnSc1	Presidential
Library	Librara	k1gFnSc2	Librara
v	v	k7c6	v
Simi	Simi	k1gNnSc6	Simi
Valley	Vallea	k1gFnSc2	Vallea
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc4	pohřeb
řídil	řídit	k5eAaImAgMnS	řídit
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Reaganovým	Reaganův	k2eAgMnSc7d1	Reaganův
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
autor	autor	k1gMnSc1	autor
biografií	biografie	k1gFnSc7	biografie
několika	několik	k4yIc2	několik
prezidentů	prezident	k1gMnPc2	prezident
(	(	kIx(	(
<g/>
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
Nixona	Nixona	k1gFnSc1	Nixona
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
Reeves	Reeves	k1gMnSc1	Reeves
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
President	president	k1gMnSc1	president
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Triumph	Triumph	k1gMnSc1	Triumph
of	of	k?	of
Imagination	Imagination	k1gInSc1	Imagination
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reagan	Reagan	k1gMnSc1	Reagan
rozuměl	rozumět	k5eAaImAgMnS	rozumět
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xS	jak
být	být	k5eAaImF	být
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
není	být	k5eNaImIp3nS	být
řídit	řídit	k5eAaImF	řídit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vést	vést	k5eAaImF	vést
národ	národ	k1gInSc4	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
věcech	věc	k1gFnPc6	věc
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
i	i	k8xC	i
po	po	k7c6	po
čtvrtstoletí	čtvrtstoletí	k1gNnSc6	čtvrtstoletí
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
řekl	říct	k5eAaPmAgMnS	říct
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
postřelen	postřelen	k2eAgMnSc1d1	postřelen
a	a	k8xC	a
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Budeme	být	k5eAaImBp1nP	být
jednat	jednat	k5eAaImF	jednat
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nS	kdyby
tady	tady	k6eAd1	tady
byl	být	k5eAaImAgMnS	být
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
Je	být	k5eAaImIp3nS	být
hrdinskou	hrdinský	k2eAgFnSc7d1	hrdinská
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Nezničil	zničit	k5eNaPmAgInS	zničit
komunismus	komunismus	k1gInSc1	komunismus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
zničí	zničit	k5eAaPmIp3nS	zničit
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
kolapsu	kolaps	k1gInSc3	kolaps
přispěl	přispět	k5eAaPmAgMnS	přispět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
opovrhoval	opovrhovat	k5eAaImAgMnS	opovrhovat
zavedeným	zavedený	k2eAgFnPc3d1	zavedená
americkým	americký	k2eAgFnPc3d1	americká
politikám	politika	k1gFnPc3	politika
kontroly	kontrola	k1gFnSc2	kontrola
zbrojení	zbrojení	k1gNnSc2	zbrojení
a	a	k8xC	a
détente	détente	k2eAgMnSc7d1	détente
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zeptal	zeptat	k5eAaPmAgMnS	zeptat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
strategii	strategie	k1gFnSc4	strategie
pro	pro	k7c4	pro
studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Vyhrajeme	vyhrát	k5eAaPmIp1nP	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
prohrajou	prohrát	k5eAaPmIp3nP	prohrát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
Jako	jako	k8xC	jako
jeho	jeho	k3xOp3gInSc1	jeho
idol	idol	k1gInSc1	idol
Franklin	Franklina	k1gFnPc2	Franklina
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nezapomenutelným	zapomenutelný	k2eNgMnSc7d1	nezapomenutelný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
průzkumech	průzkum	k1gInPc6	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgInS	být
mnoha	mnoho	k4c7	mnoho
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
intelektuály	intelektuál	k1gMnPc7	intelektuál
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
jistou	jistý	k2eAgFnSc4d1	jistá
podbízivost	podbízivost	k1gFnSc4	podbízivost
a	a	k8xC	a
populismus	populismus	k1gInSc4	populismus
<g/>
;	;	kIx,	;
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc3	jeho
širokým	široký	k2eAgFnPc3d1	široká
masám	masa	k1gFnPc3	masa
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
srozumitelné	srozumitelný	k2eAgInPc1d1	srozumitelný
výroky	výrok	k1gInPc1	výrok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
je	být	k5eAaImIp3nS	být
říší	říš	k1gFnSc7	říš
zla	zlo	k1gNnSc2	zlo
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gallup	Gallup	k1gInSc1	Gallup
Organization	Organization	k1gInSc1	Organization
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
ptala	ptat	k5eAaImAgFnS	ptat
občanů	občan	k1gMnPc2	občan
na	na	k7c4	na
největší	veliký	k2eAgMnPc4d3	veliký
prezidenty	prezident	k1gMnPc4	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
první	první	k4xOgMnSc1	první
s	s	k7c7	s
18	[number]	k4	18
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
průzkumu	průzkum	k1gInSc6	průzkum
stanice	stanice	k1gFnSc2	stanice
ABC	ABC	kA	ABC
pátý	pátý	k4xOgMnSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
Quinnipiac	Quinnipiac	k1gFnSc1	Quinnipiac
university	universita	k1gFnSc2	universita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
Reagan	Reagan	k1gMnSc1	Reagan
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
prezidenta	prezident	k1gMnSc4	prezident
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
stanice	stanice	k1gFnSc2	stanice
C-SPAN	C-SPAN	k1gFnSc2	C-SPAN
se	se	k3xPyFc4	se
Reagan	Reagan	k1gMnSc1	Reagan
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
stanice	stanice	k1gFnSc2	stanice
ABC	ABC	kA	ABC
se	se	k3xPyFc4	se
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
popularita	popularita	k1gFnSc1	popularita
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
the	the	k?	the
Gripper	Gripper	k1gInSc1	Gripper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
odkazu	odkaz	k1gInSc6	odkaz
na	na	k7c4	na
roli	role	k1gFnSc4	role
George	Georg	k1gMnSc2	Georg
Gripa	Grip	k1gMnSc2	Grip
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Knute	Knut	k1gMnSc5	Knut
Rockne	Rockne	k1gMnSc5	Rockne
<g/>
,	,	kIx,	,
All	All	k1gFnPc3	All
American	Americana	k1gFnPc2	Americana
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
frází	fráze	k1gFnSc7	fráze
z	z	k7c2	z
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Vyhrej	vyhrát	k5eAaPmRp2nS	vyhrát
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
Grippera	Gripper	k1gMnSc4	Gripper
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
mu	on	k3xPp3gMnSc3	on
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prezidentství	prezidentství	k1gNnSc2	prezidentství
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
přezdívky	přezdívka	k1gFnSc2	přezdívka
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
komunikátor	komunikátor	k1gInSc1	komunikátor
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Teflonový	teflonový	k2eAgMnSc1d1	teflonový
prezident	prezident	k1gMnSc1	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
Rawhide	Rawhid	k1gMnSc5	Rawhid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
jej	on	k3xPp3gMnSc4	on
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Ronald	Ronald	k1gMnSc1	Ronald
Ray-Gun	Ray-Gun	k1gMnSc1	Ray-Gun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
poprvé	poprvé	k6eAd1	poprvé
použili	použít	k5eAaPmAgMnP	použít
Joan	Joan	k1gMnSc1	Joan
Baez	Baez	k1gMnSc1	Baez
a	a	k8xC	a
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Shurtleff	Shurtleff	k1gInSc1	Shurtleff
před	před	k7c7	před
písní	píseň	k1gFnSc7	píseň
Drug	Druga	k1gFnPc2	Druga
Store	Stor	k1gInSc5	Stor
Truck	truck	k1gInSc1	truck
Drivin	Drivina	k1gFnPc2	Drivina
<g/>
'	'	kIx"	'
Man	mana	k1gFnPc2	mana
ve	v	k7c6	v
Woodstocku	Woodstocko	k1gNnSc6	Woodstocko
'	'	kIx"	'
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
pád	pád	k1gInSc4	pád
komunismu	komunismus	k1gInSc2	komunismus
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
dostal	dostat	k5eAaPmAgMnS	dostat
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
české	český	k2eAgNnSc4d1	české
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
2307	[number]	k4	2307
sběrateli	sběratel	k1gMnPc7	sběratel
mincí	mince	k1gFnPc2	mince
uspořádán	uspořádán	k2eAgInSc4d1	uspořádán
průzkum	průzkum	k1gInSc4	průzkum
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
budoucích	budoucí	k2eAgFnPc6d1	budoucí
amerických	americký	k2eAgFnPc6d1	americká
mincích	mince	k1gFnPc6	mince
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c6	na
deseticentu	deseticent	k1gInSc6	deseticent
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
Washington	Washington	k1gInSc4	Washington
National	National	k1gMnPc2	National
Airport	Airport	k1gInSc1	Airport
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Ronald	Ronald	k1gInSc4	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Washington	Washington	k1gInSc1	Washington
National	National	k1gFnSc7	National
Airport	Airport	k1gInSc4	Airport
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
prezident	prezident	k1gMnSc1	prezident
Clinton	Clinton	k1gMnSc1	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
(	(	kIx(	(
<g/>
CVN-	CVN-	k1gFnSc1	CVN-
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
žijící	žijící	k2eAgFnSc6d1	žijící
osobě	osoba	k1gFnSc6	osoba
a	a	k8xC	a
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
žijícím	žijící	k2eAgMnSc6d1	žijící
prezidentovi	prezident	k1gMnSc6	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ronaldu	Ronald	k1gMnSc3	Ronald
Reaganovi	Reagan	k1gMnSc3	Reagan
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
mnoho	mnoho	k4c1	mnoho
ocenění	ocenění	k1gNnPc2	ocenění
i	i	k9	i
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
jej	on	k3xPp3gNnSc4	on
CNN	CNN	kA	CNN
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
redakcí	redakce	k1gFnSc7	redakce
TIME	TIME	kA	TIME
označila	označit	k5eAaPmAgFnS	označit
"	"	kIx"	"
<g/>
nejvíc	hodně	k6eAd3	hodně
fascinující	fascinující	k2eAgFnSc7d1	fascinující
osobností	osobnost	k1gFnSc7	osobnost
<g/>
"	"	kIx"	"
prvních	první	k4xOgNnPc6	první
25	[number]	k4	25
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
TIME	TIME	kA	TIME
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
lidí	člověk	k1gMnPc2	člověk
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
programu	program	k1gInSc6	program
"	"	kIx"	"
<g/>
Greatest	Greatest	k1gMnSc1	Greatest
American	American	k1gMnSc1	American
<g/>
"	"	kIx"	"
vybrán	vybrán	k2eAgInSc1d1	vybrán
diváky	divák	k1gMnPc7	divák
jako	jako	k8xC	jako
největší	veliký	k2eAgMnSc1d3	veliký
Američan	Američan	k1gMnSc1	Američan
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
schválil	schválit	k5eAaPmAgInS	schválit
Kongres	kongres	k1gInSc1	kongres
vytvoření	vytvoření	k1gNnSc1	vytvoření
Ronald	Ronald	k1gInSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Boyhood	Boyhooda	k1gFnPc2	Boyhooda
National	National	k1gMnSc1	National
Historic	Historic	k1gMnSc1	Historic
Site	Site	k1gFnSc4	Site
v	v	k7c6	v
Dixonu	Dixon	k1gInSc6	Dixon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Reagan	Reagan	k1gMnSc1	Reagan
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
žil	žíla	k1gFnPc2	žíla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
stát	stát	k5eAaImF	stát
Illinois	Illinois	k1gFnSc4	Illinois
dálnici	dálnice	k1gFnSc4	dálnice
East-West	East-West	k1gFnSc4	East-West
Tollway	Tollwaa	k1gFnSc2	Tollwaa
na	na	k7c4	na
Ronald	Ronald	k1gInSc4	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Memorial	Memorial	k1gMnSc1	Memorial
Tollway	Tollwaa	k1gFnSc2	Tollwaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
otevřena	otevřen	k2eAgFnSc1d1	otevřena
nová	nový	k2eAgFnSc1d1	nová
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Ronald	Ronald	k1gMnSc1	Ronald
W.	W.	kA	W.
Reagan	Reagan	k1gMnSc1	Reagan
High	High	k1gMnSc1	High
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Doživotní	doživotní	k2eAgNnSc4d1	doživotní
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
členství	členství	k1gNnSc4	členství
ve	v	k7c4	v
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guilda	k1gFnPc2	Guilda
<g/>
.	.	kIx.	.
</s>
<s>
Sylvanus	Sylvanus	k1gMnSc1	Sylvanus
Thayer	Thayer	k1gMnSc1	Thayer
Award	Award	k1gMnSc1	Award
vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xC	jako
Westpoint	Westpoint	k1gMnSc1	Westpoint
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
získal	získat	k5eAaPmAgInS	získat
čestný	čestný	k2eAgInSc1d1	čestný
britský	britský	k2eAgInSc1d1	britský
rytířský	rytířský	k2eAgInSc1d1	rytířský
titul	titul	k1gInSc1	titul
Knight	Knight	k2eAgMnSc1d1	Knight
Grand	grand	k1gMnSc1	grand
Cross	Crossa	k1gFnPc2	Crossa
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
Bath	Bath	k1gInSc1	Bath
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
za	za	k7c4	za
jméno	jméno	k1gNnSc4	jméno
psát	psát	k5eAaImF	psát
GCB	GCB	kA	GCB
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
sir	sir	k1gMnSc1	sir
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ocenění	ocenění	k1gNnSc1	ocenění
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gNnSc2	on
z	z	k7c2	z
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
získali	získat	k5eAaPmAgMnP	získat
Dwight	Dwight	k2eAgInSc4d1	Dwight
Eisenhower	Eisenhower	k1gInSc4	Eisenhower
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Velkostuha	Velkostuha	k1gFnSc1	Velkostuha
Řádu	řád	k1gInSc2	řád
chryzantémy	chryzantéma	k1gFnSc2	chryzantéma
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Čestný	čestný	k2eAgInSc1d1	čestný
člen	člen	k1gInSc1	člen
Keble	Kebl	k1gMnSc2	Kebl
College	Colleg	k1gMnSc2	Colleg
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
medaile	medaile	k1gFnSc2	medaile
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
přijala	přijmout	k5eAaPmAgFnS	přijmout
Nancy	Nancy	k1gFnSc1	Nancy
Reagan	Reagan	k1gMnSc1	Reagan
jeho	jeho	k3xOp3gFnSc4	jeho
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
jednodolarové	jednodolarový	k2eAgFnSc6d1	jednodolarová
minci	mince	k1gFnSc6	mince
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Reaganovi	Reaganův	k2eAgMnPc1d1	Reaganův
příznivci	příznivec	k1gMnPc1	příznivec
neúspěšně	úspěšně	k6eNd1	úspěšně
lobovali	lobovat	k5eAaBmAgMnP	lobovat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
bankovce	bankovka	k1gFnSc6	bankovka
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
desetidolarovce	desetidolarovka	k1gFnSc6	desetidolarovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Nancy	Nancy	k1gFnSc1	Nancy
Reaganová	Reaganová	k1gFnSc1	Reaganová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
na	na	k7c6	na
deseticentu	deseticent	k1gInSc6	deseticent
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
69	[number]	k4	69
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
zvolený	zvolený	k2eAgMnSc1d1	zvolený
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
70	[number]	k4	70
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
77	[number]	k4	77
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Překonal	překonat	k5eAaPmAgMnS	překonat
tak	tak	k9	tak
Dwighta	Dwight	k1gMnSc4	Dwight
Eisenhowera	Eisenhower	k1gMnSc4	Eisenhower
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
opustil	opustit	k5eAaPmAgMnS	opustit
úřad	úřad	k1gInSc4	úřad
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
nejdéle	dlouho	k6eAd3	dlouho
žijícím	žijící	k2eAgMnSc7d1	žijící
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
překonal	překonat	k5eAaPmAgInS	překonat
Gerald	Gerald	k1gInSc1	Gerald
Ford	ford	k1gInSc1	ford
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
členství	členství	k1gNnSc3	členství
v	v	k7c4	v
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guildo	k1gNnPc2	Guildo
i	i	k8xC	i
jediným	jediný	k2eAgInSc7d1	jediný
členem	člen	k1gInSc7	člen
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
40	[number]	k4	40
<g/>
.	.	kIx.	.
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
35	[number]	k4	35
<g/>
.	.	kIx.	.
dle	dle	k7c2	dle
data	datum	k1gNnSc2	datum
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgInSc4d2	starší
než	než	k8xS	než
čtyři	čtyři	k4xCgMnPc1	čtyři
dříve	dříve	k6eAd2	dříve
zvolení	zvolený	k2eAgMnPc1d1	zvolený
prezidenti	prezident	k1gMnPc1	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
Nixon	Nixon	k1gNnSc1	Nixon
<g/>
,	,	kIx,	,
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
Carter	Carter	k1gInSc1	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
levák	levák	k1gMnSc1	levák
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
většinou	většinou	k6eAd1	většinou
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
jak	jak	k8xS	jak
píše	psát	k5eAaImIp3nS	psát
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
také	také	k9	také
nadhazoval	nadhazovat	k5eAaImAgMnS	nadhazovat
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
nedostal	dostat	k5eNaPmAgMnS	dostat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
nevypadal	vypadat	k5eNaPmAgMnS	vypadat
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
měřil	měřit	k5eAaImAgMnS	měřit
182,5	[number]	k4	182,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Reagana	Reagan	k1gMnSc2	Reagan
časopis	časopis	k1gInSc1	časopis
TIME	TIME	kA	TIME
dvakrát	dvakrát	k6eAd1	dvakrát
zvolil	zvolit	k5eAaPmAgInS	zvolit
osobou	osoba	k1gFnSc7	osoba
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
a	a	k8xC	a
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
vůdcem	vůdce	k1gMnSc7	vůdce
Andropovem	Andropov	k1gInSc7	Andropov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
nejobdivovanějších	obdivovaný	k2eAgMnPc2d3	nejobdivovanější
lidí	člověk	k1gMnPc2	člověk
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Gallupova	Gallupův	k2eAgInSc2d1	Gallupův
ústavu	ústav	k1gInSc2	ústav
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
bonbóny	bonbón	k1gInPc4	bonbón
s	s	k7c7	s
želé	želé	k1gNnSc7	želé
náplní	náplň	k1gFnPc2	náplň
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Jelly	Jella	k1gMnSc2	Jella
Beans	Beansa	k1gFnPc2	Beansa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Killers	Killers	k1gInSc1	Killers
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jack	Jack	k1gInSc1	Jack
Browning	browning	k1gInSc1	browning
Heritage	Heritage	k1gInSc1	Heritage
of	of	k?	of
Splendor	Splendor	k1gInSc1	Splendor
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
The	The	k1gFnSc2	The
Young	Young	k1gMnSc1	Young
Doctors	Doctors	k1gInSc1	Doctors
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Hellcats	Hellcatsa	k1gFnPc2	Hellcatsa
of	of	k?	of
the	the	k?	the
Navy	Navy	k?	Navy
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
-	-	kIx~	-
Commador	Commador	k1gInSc1	Commador
Casey	Casea	k1gFnSc2	Casea
Abbott	Abbott	k2eAgInSc1d1	Abbott
Tennessee	Tennessee	k1gInSc1	Tennessee
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Partner	partner	k1gMnSc1	partner
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
Cowboy	Cowbo	k2eAgFnPc1d1	Cowbo
Cattle	Cattle	k1gFnPc1	Cattle
Queen	Queen	k1gInSc1	Queen
of	of	k?	of
Montana	Montana	k1gFnSc1	Montana
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
Farrell	Farrell	k1gMnSc1	Farrell
Prisoner	Prisoner	k1gMnSc1	Prisoner
of	of	k?	of
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
Webb	Webb	k1gInSc1	Webb
Sloane	Sloan	k1gMnSc5	Sloan
Law	Law	k1gMnSc3	Law
and	and	k?	and
Order	Order	k1gMnSc1	Order
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
-	-	kIx~	-
Frame	Fram	k1gMnSc5	Fram
Johnson	Johnson	k1gMnSc1	Johnson
Tropic	Tropice	k1gFnPc2	Tropice
Zone	Zone	k1gFnPc2	Zone
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
Dan	Dan	k1gMnSc1	Dan
McCloud	McCloud	k1gMnSc1	McCloud
She	She	k1gMnSc1	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Working	Working	k1gInSc1	Working
Her	hra	k1gFnPc2	hra
Way	Way	k1gFnPc2	Way
Through	Through	k1gInSc1	Through
College	Colleg	k1gInSc2	Colleg
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Profesor	profesor	k1gMnSc1	profesor
John	John	k1gMnSc1	John
Palmer	Palmer	k1gMnSc1	Palmer
The	The	k1gFnSc2	The
Winning	Winning	k1gInSc1	Winning
Team	team	k1gInSc1	team
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Grover	Grover	k1gInSc1	Grover
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
Alexander	Alexandra	k1gFnPc2	Alexandra
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jeff	Jeff	k1gInSc1	Jeff
Williams	Williamsa	k1gFnPc2	Williamsa
Bedtime	Bedtim	k1gInSc5	Bedtim
for	forum	k1gNnPc2	forum
Bonzo	Bonza	k1gFnSc5	Bonza
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
Profesor	profesor	k1gMnSc1	profesor
Peter	Peter	k1gMnSc1	Peter
Boyd	Boyd	k1gMnSc1	Boyd
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Outpost	Outpost	k1gFnSc1	Outpost
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kapitán	kapitán	k1gMnSc1	kapitán
Vance	Vance	k1gMnSc1	Vance
Britten	Britten	k2eAgInSc4d1	Britten
Storm	Storm	k1gInSc4	Storm
Warning	Warning	k1gInSc1	Warning
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
Burt	Burt	k1gMnSc1	Burt
Rainey	Rainea	k1gFnSc2	Rainea
The	The	k1gMnSc1	The
Big	Big	k1gMnSc1	Big
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
Host	host	k1gMnSc1	host
<g/>
/	/	kIx~	/
<g/>
Narrator	Narrator	k1gMnSc1	Narrator
Louisa	Louisa	k?	Louisa
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Harold	Harold	k1gMnSc1	Harold
'	'	kIx"	'
<g/>
Hal	hala	k1gFnPc2	hala
<g/>
'	'	kIx"	'
Norton	Norton	k1gInSc1	Norton
The	The	k1gFnSc2	The
Hasty	Hasta	k1gMnSc2	Hasta
Heart	Hearta	k1gFnPc2	Hearta
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Yank	Yank	k1gInSc1	Yank
The	The	k1gFnSc2	The
Girl	girl	k1gFnSc2	girl
from	from	k1gMnSc1	from
Jones	Jones	k1gMnSc1	Jones
Beach	Beach	k1gMnSc1	Beach
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bob	Bob	k1gMnSc1	Bob
Randolph	Randolph	k1gMnSc1	Randolph
Night	Night	k1gMnSc1	Night
Unto	Unto	k1gMnSc1	Unto
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
John	John	k1gMnSc1	John
Galen	Galna	k1gFnPc2	Galna
John	John	k1gMnSc1	John
Loves	Loves	k1gMnSc1	Loves
Mary	Mary	k1gFnSc1	Mary
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lawrence	Lawrence	k1gFnSc2	Lawrence
The	The	k1gMnSc1	The
Voice	Voice	k1gMnSc1	Voice
of	of	k?	of
the	the	k?	the
Turtle	Turtle	k1gFnSc2	Turtle
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sergeant	Sergeant	k1gMnSc1	Sergeant
Bill	Bill	k1gMnSc1	Bill
Page	Page	k1gInSc4	Page
That	That	k2eAgInSc4d1	That
Hagen	Hagen	k1gInSc4	Hagen
Girl	girl	k1gFnSc2	girl
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tom	Tom	k1gMnSc1	Tom
Bates	Batesa	k1gFnPc2	Batesa
Stallion	Stallion	k1gInSc1	Stallion
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
Larry	Larra	k1gFnPc1	Larra
Hanrahan	Hanrahan	k1gInSc4	Hanrahan
Wings	Wings	k1gInSc4	Wings
for	forum	k1gNnPc2	forum
This	Thisa	k1gFnPc2	Thisa
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
The	The	k1gMnSc1	The
Stillwell	Stillwell	k1gMnSc1	Stillwell
<g />
.	.	kIx.	.
</s>
<s>
Road	Road	k1gInSc1	Road
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Jap	Jap	k1gMnSc1	Jap
Zero	Zero	k1gMnSc1	Zero
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
Lieutenant	Lieutenant	k1gInSc1	Lieutenant
Jimmy	Jimma	k1gFnSc2	Jimma
Saunders	Saundersa	k1gFnPc2	Saundersa
This	Thisa	k1gFnPc2	Thisa
Is	Is	k1gMnSc2	Is
the	the	k?	the
Army	Arma	k1gMnSc2	Arma
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
Johnny	Johnna	k1gFnSc2	Johnna
Jones	Jones	k1gMnSc1	Jones
The	The	k1gMnSc1	The
Rear	Rear	k1gMnSc1	Rear
Gunner	Gunner	k1gMnSc1	Gunner
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poručík	poručík	k1gMnSc1	poručík
Ames	Ames	k1gInSc4	Ames
For	forum	k1gNnPc2	forum
God	God	k1gMnSc2	God
and	and	k?	and
Country	country	k2eAgFnPc1d1	country
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
Otec	otec	k1gMnSc1	otec
Michael	Michael	k1gMnSc1	Michael
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
Cadet	Cadet	k1gMnSc1	Cadet
Classification	Classification	k1gInSc1	Classification
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Line	linout	k5eAaImIp3nS	linout
of	of	k?	of
Duty	Duty	k?	Duty
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Desperate	Desperat	k1gInSc5	Desperat
Journey	Journea	k1gMnSc2	Journea
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Letový	letový	k2eAgMnSc1d1	letový
důstojník	důstojník	k1gMnSc1	důstojník
Johnny	Johnna	k1gFnSc2	Johnna
Hammond	Hammond	k1gMnSc1	Hammond
Juke	Juke	k1gFnSc1	Juke
Girl	girl	k1gFnSc1	girl
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Steve	Steve	k1gMnSc1	Steve
Talbot	Talbot	k1gMnSc1	Talbot
Kings	Kingsa	k1gFnPc2	Kingsa
Row	Row	k1gMnSc1	Row
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Drake	Drake	k1gInSc1	Drake
McHugh	McHugh	k1gMnSc1	McHugh
Nine	Nine	k1gFnPc2	Nine
Lives	Lives	k1gMnSc1	Lives
Are	ar	k1gInSc5	ar
Not	nota	k1gFnPc2	nota
Enough	Enough	k1gInSc1	Enough
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Matt	Matt	k1gMnSc1	Matt
Sawyer	Sawyer	k1gMnSc1	Sawyer
International	International	k1gMnSc1	International
Squadron	Squadron	k1gMnSc1	Squadron
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jimmy	Jimma	k1gFnSc2	Jimma
Grant	grant	k1gInSc1	grant
Million	Million	k1gInSc1	Million
Dollar	dollar	k1gInSc4	dollar
Baby	baba	k1gFnSc2	baba
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
Pete	Pet	k1gMnSc4	Pet
<g/>
'	'	kIx"	'
Rowan	Rowan	k1gMnSc1	Rowan
The	The	k1gMnSc1	The
Bad	Bad	k1gMnSc1	Bad
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gilbert	Gilbert	k1gMnSc1	Gilbert
'	'	kIx"	'
<g/>
Gil	Gil	k1gMnSc1	Gil
<g/>
'	'	kIx"	'
Jones	Jones	k1gMnSc1	Jones
Santa	Santa	k1gMnSc1	Santa
Fe	Fe	k1gMnSc1	Fe
Trail	Trail	k1gMnSc1	Trail
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
George	Georg	k1gMnSc2	Georg
Armstrong	Armstrong	k1gMnSc1	Armstrong
Custer	Custer	k1gMnSc1	Custer
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Movieland	Movieland	k1gInSc1	Movieland
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Guest	Guest	k1gFnSc1	Guest
at	at	k?	at
Carlo	Carlo	k1gNnSc1	Carlo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Tugboat	Tugboat	k1gInSc1	Tugboat
Annie	Annie	k1gFnSc2	Annie
Sails	Sailsa	k1gFnPc2	Sailsa
Again	Again	k1gMnSc1	Again
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eddie	Eddie	k1gFnSc1	Eddie
Kent	Kenta	k1gFnPc2	Kenta
Knute	Knut	k1gInSc5	Knut
Rockne	Rockne	k1gFnSc2	Rockne
All	All	k1gMnSc1	All
American	American	k1gMnSc1	American
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
George	Georg	k1gMnSc2	Georg
'	'	kIx"	'
<g/>
The	The	k1gMnSc1	The
Gipper	Gipper	k1gMnSc1	Gipper
<g/>
'	'	kIx"	'
Gipp	Gipp	k1gMnSc1	Gipp
Murder	Murder	k1gMnSc1	Murder
in	in	k?	in
the	the	k?	the
Air	Air	k1gFnSc2	Air
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brass	Brass	k1gInSc1	Brass
Bancroft	Bancroft	k1gInSc1	Bancroft
<g/>
,	,	kIx,	,
aka	aka	k?	aka
Steve	Steve	k1gMnSc1	Steve
Swenko	Swenko	k1gNnSc4	Swenko
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
Coe	Coe	k1gFnSc2	Coe
<g />
.	.	kIx.	.
</s>
<s>
An	An	k?	An
Angel	angel	k1gMnSc1	angel
from	from	k1gMnSc1	from
Texas	Texas	kA	Texas
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Marty	Marta	k1gFnSc2	Marta
Allen	Allen	k1gMnSc1	Allen
Brother	Brother	kA	Brother
Rat	Rat	k1gMnSc1	Rat
and	and	k?	and
a	a	k8xC	a
Baby	baba	k1gFnSc2	baba
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dan	Dan	k1gMnSc1	Dan
Crawford	Crawforda	k1gFnPc2	Crawforda
Smashing	Smashing	k1gInSc4	Smashing
the	the	k?	the
Money	Money	k1gInPc1	Money
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poručík	poručík	k1gMnSc1	poručík
'	'	kIx"	'
<g/>
Brass	Brass	k1gInSc1	Brass
<g/>
'	'	kIx"	'
Bancroft	Bancroft	k2eAgMnSc1d1	Bancroft
The	The	k1gMnSc1	The
Angels	Angelsa	k1gFnPc2	Angelsa
Wash	Wash	k1gMnSc1	Wash
Their	Their	k1gMnSc1	Their
Faces	Faces	k1gMnSc1	Faces
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Deputy	Deput	k1gInPc1	Deput
Dist	Dista	k1gFnPc2	Dista
<g/>
.	.	kIx.	.
</s>
<s>
Atty	Atta	k1gFnPc1	Atta
<g/>
.	.	kIx.	.
</s>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
'	'	kIx"	'
<g/>
Pat	pat	k1gInSc1	pat
<g/>
'	'	kIx"	'
Remson	Remson	k1gMnSc1	Remson
Hell	Hell	k1gMnSc1	Hell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kitchen	Kitchna	k1gFnPc2	Kitchna
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jim	on	k3xPp3gNnPc3	on
Donohue	Donohue	k1gNnPc3	Donohue
Naughty	Naughta	k1gFnSc2	Naughta
But	But	k1gFnSc2	But
Nice	Nice	k1gFnSc2	Nice
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ed	Ed	k1gMnSc1	Ed
'	'	kIx"	'
<g/>
Eddie	Eddie	k1gFnSc1	Eddie
<g/>
'	'	kIx"	'
Clark	Clark	k1gInSc1	Clark
Code	Cod	k1gFnSc2	Cod
of	of	k?	of
the	the	k?	the
Secret	Secret	k1gInSc1	Secret
Service	Service	k1gFnSc1	Service
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poručík	poručík	k1gMnSc1	poručík
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Brass	Brass	k1gInSc1	Brass
<g/>
'	'	kIx"	'
Bancroft	Bancroft	k2eAgInSc1d1	Bancroft
Dark	Dark	k1gInSc1	Dark
Victory	Victor	k1gMnPc7	Victor
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Alec	Alec	k1gInSc1	Alec
Hamm	Hamma	k1gFnPc2	Hamma
Secret	Secreta	k1gFnPc2	Secreta
Service	Service	k1gFnSc2	Service
of	of	k?	of
the	the	k?	the
Air	Air	k1gFnSc2	Air
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poručík	poručík	k1gMnSc1	poručík
'	'	kIx"	'
<g/>
Brass	Brass	k1gInSc1	Brass
<g/>
'	'	kIx"	'
Bancroft	Bancroft	k2eAgMnSc1d1	Bancroft
Going	Going	k1gMnSc1	Going
Places	Places	k1gMnSc1	Places
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jack	Jack	k1gInSc1	Jack
Withering	Withering	k1gInSc1	Withering
Brother	Brother	kA	Brother
Rat	Rat	k1gFnSc2	Rat
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Dan	Dan	k1gMnSc1	Dan
Crawford	Crawford	k1gMnSc1	Crawford
Girls	girl	k1gFnPc2	girl
on	on	k3xPp3gMnSc1	on
Probation	Probation	k1gInSc1	Probation
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Neil	Neil	k1gInSc1	Neil
Dillon	Dillon	k1gInSc1	Dillon
Boy	boa	k1gFnSc2	boa
Meets	Meetsa	k1gFnPc2	Meetsa
Girl	girl	k1gFnSc2	girl
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hlasatel	hlasatel	k1gMnSc1	hlasatel
na	na	k7c6	na
premiéře	premiéra	k1gFnSc6	premiéra
premiere	premirat	k5eAaPmIp3nS	premirat
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Clitterhouse	Clitterhouse	k1gFnSc1	Clitterhouse
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
Hlasatel	hlasatel	k1gMnSc1	hlasatel
Cowboy	Cowboa	k1gFnSc2	Cowboa
from	from	k1gMnSc1	from
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pat	pat	k1gInSc1	pat
Dunn	Dunn	k1gNnSc1	Dunn
Accidents	Accidents	k1gInSc1	Accidents
Will	Will	k1gMnSc1	Will
Happen	Happen	k2eAgMnSc1d1	Happen
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eric	Eric	k1gInSc1	Eric
Gregg	Gregga	k1gFnPc2	Gregga
Swing	swing	k1gInSc1	swing
Your	Your	k1gMnSc1	Your
Lady	lady	k1gFnSc1	lady
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jack	Jack	k1gMnSc1	Jack
Miller	Miller	k1gMnSc1	Miller
Sergeant	Sergeant	k1gMnSc1	Sergeant
Murphy	Murpha	k1gFnSc2	Murpha
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vojín	vojín	k1gMnSc1	vojín
Dennis	Dennis	k1gFnSc2	Dennis
Reilley	Reillea	k1gFnSc2	Reillea
Hollywood	Hollywood	k1gInSc1	Hollywood
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hlasatel	hlasatel	k1gMnSc1	hlasatel
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
Love	lov	k1gInSc5	lov
Is	Is	k1gMnSc1	Is
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Air	Air	k1gMnPc7	Air
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Andy	Anda	k1gFnPc1	Anda
McCaine	McCain	k1gInSc5	McCain
"	"	kIx"	"
<g/>
True	True	k1gNnPc1	True
Grit	Grita	k1gFnPc2	Grita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
(	(	kIx(	(
<g/>
CMT	CMT	kA	CMT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
President	president	k1gMnSc1	president
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Official	Official	k1gMnSc1	Official
Reagan	Reagan	k1gMnSc1	Reagan
Library	Librara	k1gFnSc2	Librara
Tribute	tribut	k1gInSc5	tribut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Great	Great	k2eAgInSc1d1	Great
Speeches	Speeches	k1gInSc1	Speeches
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Stand	Standa	k1gFnPc2	Standa
Up	Up	k1gMnSc1	Up
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
NBC	NBC	kA	NBC
News	News	k1gInSc1	News
Presents	Presents	k1gInSc1	Presents
-	-	kIx~	-
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
ABC	ABC	kA	ABC
News	News	k1gInSc4	News
Presents	Presentsa	k1gFnPc2	Presentsa
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
His	his	k1gNnSc1	his
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
His	his	k1gNnSc1	his
Life	Lif	k1gInSc2	Lif
and	and	k?	and
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
A	A	kA	A
Legacy	Legaca	k1gMnSc2	Legaca
Remembered	Remembered	k1gMnSc1	Remembered
(	(	kIx(	(
<g/>
History	Histor	k1gInPc1	Histor
Channel	Channela	k1gFnPc2	Channela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
The	The	k1gFnSc6	The
Great	Great	k2eAgMnSc1d1	Great
Communicator	Communicator	k1gMnSc1	Communicator
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Salute	salut	k1gInSc5	salut
to	ten	k3xDgNnSc4	ten
Reagan	Reagan	k1gMnSc1	Reagan
-	-	kIx~	-
A	A	kA	A
President	president	k1gMnSc1	president
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Moments	Momentsa	k1gFnPc2	Momentsa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gInSc1	American
Experience	Experience	k1gFnSc2	Experience
-	-	kIx~	-
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Tribute	tribut	k1gInSc5	tribut
to	ten	k3xDgNnSc4	ten
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Reagan	Reagan	k1gMnSc1	Reagan
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Discovery	Discover	k1gInPc4	Discover
Channel	Channel	k1gInSc1	Channel
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
the	the	k?	the
Face	Fac	k1gFnSc2	Fac
of	of	k?	of
Evil	Evil	k1gMnSc1	Evil
<g/>
:	:	kIx,	:
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
War	War	k1gFnSc7	War
in	in	k?	in
Word	Word	kA	Word
and	and	k?	and
Deed	Deed	k1gMnSc1	Deed
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Ronalda	Ronald	k1gMnSc4	Ronald
Reagana	Reagan	k1gMnSc4	Reagan
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
James	James	k1gInSc4	James
Brolin	Brolin	k2eAgInSc4d1	Brolin
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
The	The	k1gFnPc2	The
Reagans	Reagansa	k1gFnPc2	Reagansa
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
s	s	k7c7	s
Judy	judo	k1gNnPc7	judo
Davis	Davis	k1gFnSc2	Davis
jako	jako	k8xC	jako
Nancy	Nancy	k1gFnSc2	Nancy
Richard	Richard	k1gMnSc1	Richard
Crenna	Crenna	k1gFnSc1	Crenna
v	v	k7c6	v
The	The	k1gFnSc6	The
Day	Day	k1gMnSc1	Day
Reagan	Reagan	k1gMnSc1	Reagan
Was	Was	k1gMnSc1	Was
Shot	shot	k1gInSc1	shot
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
s	s	k7c7	s
Holland	Holland	k1gInSc1	Holland
Taylor	Taylor	k1gMnSc1	Taylor
jako	jako	k8xC	jako
Nancy	Nancy	k1gFnSc1	Nancy
Bryan	Bryan	k1gInSc1	Bryan
Clark	Clark	k1gInSc1	Clark
se	se	k3xPyFc4	se
jako	jako	k9	jako
Reagan	Reagan	k1gMnSc1	Reagan
několikrát	několikrát	k6eAd1	několikrát
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
HBO	HBO	kA	HBO
Without	Without	k1gMnSc1	Without
Warning	Warning	k1gInSc1	Warning
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
James	James	k1gMnSc1	James
Brady	brada	k1gFnSc2	brada
Story	story	k1gFnSc2	story
s	s	k7c7	s
Beau	Beaum	k1gNnSc3	Beaum
Bridges	Bridges	k1gInSc4	Bridges
jako	jako	k8xC	jako
Jamesem	James	k1gInSc7	James
Bradym	Bradym	k1gInSc1	Bradym
<g/>
;	;	kIx,	;
a	a	k8xC	a
také	také	k9	také
v	v	k7c4	v
Guts	Guts	k1gInSc4	Guts
and	and	k?	and
Glory	Glora	k1gMnSc2	Glora
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Rise	Ris	k1gMnSc2	Ris
and	and	k?	and
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Oliver	Oliver	k1gMnSc1	Oliver
North	North	k1gMnSc1	North
<g/>
.	.	kIx.	.
</s>
<s>
Johnny	Johnn	k1gInPc1	Johnn
Carson	Carsona	k1gFnPc2	Carsona
v	v	k7c6	v
The	The	k1gFnSc6	The
Tonight	Tonight	k1gMnSc1	Tonight
Show	show	k1gFnSc2	show
Phil	Phil	k1gMnSc1	Phil
Hartman	Hartman	k1gMnSc1	Hartman
a	a	k8xC	a
Randy	rand	k1gInPc4	rand
Quaid	Quaida	k1gFnPc2	Quaida
v	v	k7c4	v
Saturday	Saturday	k1gInPc4	Saturday
Night	Night	k1gMnSc1	Night
Live	Liv	k1gFnSc2	Liv
Rich	Rich	k1gMnSc1	Rich
Little	Little	k1gFnSc1	Little
v	v	k7c6	v
několika	několik	k4yIc6	několik
televizních	televizní	k2eAgFnPc6d1	televizní
vystoupeních	vystoupení	k1gNnPc6	vystoupení
John	John	k1gMnSc1	John
Roarke	Roarke	k1gFnSc4	Roarke
ve	v	k7c6	v
skečích	skeč	k1gInPc6	skeč
komediálního	komediální	k2eAgInSc2d1	komediální
seriálu	seriál	k1gInSc2	seriál
Fridays	Fridaysa	k1gFnPc2	Fridaysa
a	a	k8xC	a
ve	v	k7c6	v
vánoční	vánoční	k2eAgFnSc6d1	vánoční
epizodě	epizoda	k1gFnSc6	epizoda
Fresh	Fresha	k1gFnPc2	Fresha
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Bel-Air	Bel-Air	k1gMnSc1	Bel-Air
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Banksových	Banksův	k2eAgMnPc2d1	Banksův
sousedů	soused	k1gMnPc2	soused
Griff	Griff	k1gInSc4	Griff
Rhys	Rhysa	k1gFnPc2	Rhysa
Jones	Jones	k1gMnSc1	Jones
v	v	k7c6	v
Not	nota	k1gFnPc2	nota
The	The	k1gMnSc2	The
Nine	Nine	k1gFnSc4	Nine
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Clock	Clock	k1gInSc4	Clock
News	Newsa	k1gFnPc2	Newsa
</s>
