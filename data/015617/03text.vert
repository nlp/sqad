<s>
Waters	Waters	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
</s>
<s>
Waters	Waters	k1gInSc1
of	of	k?
EdenInterpretTony	EdenInterpretTona	k1gFnSc2
LevinPruh	LevinPruha	k1gFnPc2
albaStudiové	albaStudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2000	#num#	k4
<g/>
Žánrjazz	Žánrjazz	k1gMnSc1
fusionPélka	fusionPélka	k1gMnSc1
<g/>
54	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
VydavatelstvíNaradaProfesionální	VydavatelstvíNaradaProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tony	Tony	k1gMnSc1
Levin	Levin	k1gMnSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
World	World	k1gInSc1
Diary	Diara	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Waters	Waters	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
<g/>
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pieces	Pieces	k1gInSc1
of	of	k?
the	the	k?
Sun	Sun	kA
<g/>
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Waters	Waters	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
je	být	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc4
sólové	sólový	k2eAgNnSc4d1
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
Tonyho	Tony	k1gMnSc2
Levina	Levin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
kvaret	kvaret	k1gInSc1
pro	pro	k7c4
nahrávání	nahrávání	k1gNnSc4
alba	album	k1gNnSc2
byl	být	k5eAaImAgInS
Levin	Levin	k1gInSc1
<g/>
,	,	kIx,
Larry	Larra	k1gFnPc1
Fast	Fast	k1gInSc1
<g/>
,	,	kIx,
Jerry	Jerr	k1gInPc1
Marotta	Marott	k1gInSc2
a	a	k8xC
Jeff	Jeff	k1gMnSc1
Pevar	Pevar	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
většině	většina	k1gFnSc6
skladeb	skladba	k1gFnPc2
se	se	k3xPyFc4
ale	ale	k9
podíleli	podílet	k5eAaImAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
hudebníci	hudebník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Autorem	autor	k1gMnSc7
všech	všecek	k3xTgFnPc2
skladeb	skladba	k1gFnPc2
je	být	k5eAaImIp3nS
Tony	Tony	k1gMnSc1
Levin	Levin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Bone	bon	k1gInSc5
&	&	k?
Flesh	Flesh	k1gInSc1
<g/>
“	“	k?
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Waters	Waters	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
<g/>
“	“	k?
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Icarus	Icarus	k1gInSc1
<g/>
“	“	k?
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Gecko	Gecko	k1gNnSc1
Walk	Walk	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Belle	bell	k1gInSc5
<g/>
“	“	k?
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Pillar	Pillar	k1gInSc1
of	of	k?
Fire	Fire	k1gInSc1
<g/>
“	“	k?
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Boulevard	Boulevard	k1gInSc1
of	of	k?
Dreams	Dreams	k1gInSc1
<g/>
“	“	k?
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Opal	opal	k1gInSc1
Road	Road	k1gInSc1
<g/>
“	“	k?
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Utopia	Utopia	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
54	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Sestava	sestava	k1gFnSc1
</s>
<s>
Tony	Tony	k1gMnSc1
Levin	Levin	k1gMnSc1
−	−	k?
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
violoncello	violoncello	k1gNnSc1
<g/>
,	,	kIx,
bezpražcová	bezpražcový	k2eAgFnSc1d1
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
kontrabas	kontrabas	k1gInSc1
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgNnSc4d1
violoncello	violoncello	k1gNnSc4
</s>
<s>
Pete	Pete	k6eAd1
Levin	Levin	k2eAgInSc1d1
−	−	k?
syntezátor	syntezátor	k1gInSc1
</s>
<s>
Warren	Warrna	k1gFnPc2
Bernhardt	Bernhardt	k1gMnSc1
−	−	k?
klavír	klavír	k1gInSc1
</s>
<s>
California	Californium	k1gNnPc1
Guitar	Guitar	k1gMnSc1
Trio	trio	k1gNnSc1
−	−	k?
akustické	akustický	k2eAgFnSc2d1
kytary	kytara	k1gFnSc2
</s>
<s>
Larry	Larra	k1gFnPc1
Fast	Fast	k1gInSc1
−	−	k?
syntezátor	syntezátor	k1gInSc1
</s>
<s>
Steve	Steve	k1gMnSc1
Gorn	Gorn	k1gMnSc1
−	−	k?
flétna	flétna	k1gFnSc1
</s>
<s>
Jerry	Jerra	k1gFnPc1
Marotta	Marotto	k1gNnSc2
−	−	k?
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc1
</s>
<s>
Jeff	Jeff	k1gMnSc1
Pevar	Pevar	k1gMnSc1
−	−	k?
elektrická	elektrický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
akustická	akustický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Sancious	Sancious	k1gMnSc1
−	−	k?
syntezátor	syntezátor	k1gInSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Torn	Torn	k1gMnSc1
−	−	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
úd	úd	k1gInSc1
<g/>
,	,	kIx,
programované	programovaný	k2eAgInPc1d1
bicí	bicí	k2eAgInPc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.allmusic.com/album/waters-of-eden-r471445/review	http://www.allmusic.com/album/waters-of-eden-r471445/review	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Tony	Tony	k1gMnSc1
Levin	Levin	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
World	World	k1gMnSc1
Diary	Diara	k1gFnSc2
•	•	k?
Waters	Waters	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
•	•	k?
Pieces	Pieces	k1gInSc1
of	of	k?
the	the	k?
Sun	Sun	kA
•	•	k?
Resonator	Resonator	k1gMnSc1
•	•	k?
Stick	Stick	k1gMnSc1
Man	Man	k1gMnSc1
Koncertní	koncertní	k2eAgNnSc4d1
album	album	k1gNnSc4
</s>
<s>
Double	double	k1gInSc1
Espresso	Espressa	k1gFnSc5
Příbuzná	příbuzný	k2eAgFnSc5d1
témata	téma	k1gNnPc4
</s>
<s>
King	King	k1gMnSc1
Crimson	Crimson	k1gMnSc1
•	•	k?
Liquid	Liquid	k1gInSc1
Tension	Tension	k1gInSc1
Experiment	experiment	k1gInSc1
•	•	k?
Bruford	Bruford	k1gInSc1
Levin	Levin	k1gMnSc1
Upper	Upper	k1gMnSc1
Extremities	Extremities	k1gMnSc1
•	•	k?
Bozzio	Bozzio	k6eAd1
Levin	Levin	k2eAgInSc4d1
Stevens	Stevens	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
