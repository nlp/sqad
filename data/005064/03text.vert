<p>
<s>
Kryptografie	kryptografie	k1gFnSc1	kryptografie
neboli	neboli	k8xC	neboli
šifrování	šifrování	k1gNnSc1	šifrování
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
metodách	metoda	k1gFnPc6	metoda
utajování	utajování	k1gNnSc2	utajování
smyslu	smysl	k1gInSc2	smysl
zpráv	zpráva	k1gFnPc2	zpráva
převodem	převod	k1gInSc7	převod
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
čitelná	čitelný	k2eAgFnSc1d1	čitelná
jen	jen	k9	jen
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
znalostí	znalost	k1gFnSc7	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
kryptografie	kryptografie	k1gFnSc2	kryptografie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
–	–	k?	–
kryptós	kryptós	k1gInSc1	kryptós
je	být	k5eAaImIp3nS	být
skrytý	skrytý	k2eAgInSc1d1	skrytý
a	a	k8xC	a
gráphein	gráphein	k1gInSc1	gráphein
znamená	znamenat	k5eAaImIp3nS	znamenat
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
obecněji	obecně	k6eAd2	obecně
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
čemkoli	cokoli	k3yInSc6	cokoli
spojeném	spojený	k2eAgNnSc6d1	spojené
se	s	k7c7	s
šiframi	šifra	k1gFnPc7	šifra
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
kryptologie	kryptologie	k1gFnSc2	kryptologie
<g/>
.	.	kIx.	.
</s>
<s>
Kryptologie	kryptologie	k1gFnSc1	kryptologie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kryptografii	kryptografie	k1gFnSc4	kryptografie
a	a	k8xC	a
kryptoanalýzu	kryptoanalýza	k1gFnSc4	kryptoanalýza
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
luštění	luštění	k1gNnSc1	luštění
zašifrovaných	zašifrovaný	k2eAgFnPc2d1	zašifrovaná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kryptografie	kryptografie	k1gFnSc1	kryptografie
se	se	k3xPyFc4	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
složitosti	složitost	k1gFnSc3	složitost
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
civilizací	civilizace	k1gFnSc7	civilizace
a	a	k8xC	a
mnohokrát	mnohokrát	k6eAd1	mnohokrát
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
běh	běh	k1gInSc4	běh
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
utajení	utajení	k1gNnSc1	utajení
či	či	k8xC	či
vyzrazení	vyzrazení	k1gNnSc1	vyzrazení
strategických	strategický	k2eAgFnPc2d1	strategická
vojenských	vojenský	k2eAgFnPc2d1	vojenská
informací	informace	k1gFnPc2	informace
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
také	také	k9	také
prozrazení	prozrazení	k1gNnSc1	prozrazení
politických	politický	k2eAgFnPc2d1	politická
intrik	intrika	k1gFnPc2	intrika
<g/>
,	,	kIx,	,
přípravy	příprava	k1gFnSc2	příprava
atentátů	atentát	k1gInPc2	atentát
nebo	nebo	k8xC	nebo
i	i	k9	i
jen	jen	k9	jen
prozrazení	prozrazení	k1gNnSc1	prozrazení
milenců	milenec	k1gMnPc2	milenec
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
může	moct	k5eAaImIp3nS	moct
úzce	úzko	k6eAd1	úzko
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
bezpečném	bezpečný	k2eAgInSc6d1	bezpečný
přenosu	přenos	k1gInSc6	přenos
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
protivníka	protivník	k1gMnSc2	protivník
šifru	šifra	k1gFnSc4	šifra
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
doložení	doložení	k1gNnSc1	doložení
o	o	k7c6	o
zašifrování	zašifrování	k1gNnSc6	zašifrování
zprávy	zpráva	k1gFnSc2	zpráva
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c4	za
období	období	k1gNnSc4	období
Řecko-Perských	řeckoerský	k2eAgFnPc2d1	řecko-perský
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Salamíny	Salamína	k1gFnSc2	Salamína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
kryptografie	kryptografie	k1gFnSc2	kryptografie
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vynalezením	vynalezení	k1gNnSc7	vynalezení
šifry	šifra	k1gFnSc2	šifra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
jako	jako	k8xS	jako
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
šifra	šifra	k1gFnSc1	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
dosti	dosti	k6eAd1	dosti
primitivní	primitivní	k2eAgMnSc1d1	primitivní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
obor	obor	k1gInSc1	obor
šifrování	šifrování	k1gNnSc2	šifrování
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
a	a	k8xC	a
již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc2	novověk
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
pokroků	pokrok	k1gInPc2	pokrok
a	a	k8xC	a
šifry	šifra	k1gFnPc4	šifra
nečekaného	čekaný	k2eNgInSc2d1	nečekaný
stupně	stupeň	k1gInSc2	stupeň
odolnosti	odolnost	k1gFnSc2	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
expertům	expert	k1gMnPc3	expert
španělské	španělský	k2eAgFnPc4d1	španělská
tajné	tajný	k2eAgFnPc4d1	tajná
služby	služba	k1gFnPc4	služba
trvalo	trvat	k5eAaImAgNnS	trvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
i	i	k8xC	i
s	s	k7c7	s
veškerým	veškerý	k3xTgNnSc7	veškerý
moderním	moderní	k2eAgNnSc7d1	moderní
vybavením	vybavení	k1gNnSc7	vybavení
celého	celý	k2eAgInSc2d1	celý
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
prolomili	prolomit	k5eAaPmAgMnP	prolomit
šifrování	šifrování	k1gNnSc4	šifrování
korespondence	korespondence	k1gFnSc2	korespondence
mezi	mezi	k7c7	mezi
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Aragonským	aragonský	k2eAgMnSc7d1	aragonský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
generálem	generál	k1gMnSc7	generál
Gonzalem	Gonzal	k1gMnSc7	Gonzal
Fernándezem	Fernández	k1gInSc7	Fernández
de	de	k?	de
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
tažení	tažení	k1gNnSc4	tažení
španělských	španělský	k2eAgNnPc2d1	španělské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
<g/>
Celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
kryptografie	kryptografie	k1gFnSc2	kryptografie
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
první	první	k4xOgFnSc7	první
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přibližně	přibližně	k6eAd1	přibližně
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
šifrování	šifrování	k1gNnSc3	šifrování
stačila	stačit	k5eAaBmAgFnS	stačit
pouze	pouze	k6eAd1	pouze
tužka	tužka	k1gFnSc1	tužka
a	a	k8xC	a
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiné	jiný	k2eAgFnPc4d1	jiná
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
pomůcky	pomůcka	k1gFnPc4	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
ale	ale	k9	ale
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgInPc4d1	různý
sofistikované	sofistikovaný	k2eAgInPc4d1	sofistikovaný
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
složitější	složitý	k2eAgInSc4d2	složitější
postup	postup	k1gInSc4	postup
při	při	k7c6	při
šifrování	šifrování	k1gNnSc6	šifrování
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
přibližně	přibližně	k6eAd1	přibližně
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
moderní	moderní	k2eAgFnSc1d1	moderní
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
k	k	k7c3	k
šifrování	šifrování	k1gNnSc3	šifrování
zpravidla	zpravidla	k6eAd1	zpravidla
nepoužívají	používat	k5eNaImIp3nP	používat
žádné	žádný	k3yNgInPc4	žádný
zvlášť	zvlášť	k6eAd1	zvlášť
vytvářené	vytvářený	k2eAgInPc4d1	vytvářený
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klasické	klasický	k2eAgInPc4d1	klasický
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Slovem	slovem	k6eAd1	slovem
šifra	šifra	k1gFnSc1	šifra
nebo	nebo	k8xC	nebo
šifrování	šifrování	k1gNnSc1	šifrování
budeme	být	k5eAaImBp1nP	být
označovat	označovat	k5eAaImF	označovat
kryptografický	kryptografický	k2eAgInSc4d1	kryptografický
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
čitelnou	čitelný	k2eAgFnSc4d1	čitelná
zprávu	zpráva	k1gFnSc4	zpráva
neboli	neboli	k8xC	neboli
prostý	prostý	k2eAgInSc4d1	prostý
text	text	k1gInSc4	text
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
nečitelnou	čitelný	k2eNgFnSc4d1	nečitelná
podobu	podoba	k1gFnSc4	podoba
neboli	neboli	k8xC	neboli
šifrový	šifrový	k2eAgInSc4d1	šifrový
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc4d1	opačný
postup	postup	k1gInSc4	postup
nazýváme	nazývat	k5eAaImIp1nP	nazývat
dešifrování	dešifrování	k1gNnSc2	dešifrování
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
algoritmus	algoritmus	k1gInSc1	algoritmus
dešifrování	dešifrování	k1gNnSc2	dešifrování
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
utajovaným	utajovaný	k2eAgInSc7d1	utajovaný
prvkem	prvek	k1gInSc7	prvek
dešifrovací	dešifrovací	k2eAgInSc1d1	dešifrovací
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
parametr	parametr	k1gInSc1	parametr
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
klíč	klíč	k1gInSc1	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
možných	možný	k2eAgInPc2d1	možný
klíčů	klíč	k1gInPc2	klíč
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zprávu	zpráva	k1gFnSc4	zpráva
rozluštit	rozluštit	k5eAaPmF	rozluštit
postupným	postupný	k2eAgNnSc7d1	postupné
zkoušením	zkoušení	k1gNnSc7	zkoušení
všech	všecek	k3xTgInPc2	všecek
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
šifry	šifra	k1gFnPc1	šifra
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
šifrování	šifrování	k1gNnSc4	šifrování
i	i	k8xC	i
dešifrování	dešifrování	k1gNnPc4	dešifrování
stejný	stejný	k2eAgInSc4d1	stejný
klíč	klíč	k1gInSc4	klíč
<g/>
;	;	kIx,	;
takové	takový	k3xDgFnPc1	takový
šifry	šifra	k1gFnPc1	šifra
nazýváme	nazývat	k5eAaImIp1nP	nazývat
symetrické	symetrický	k2eAgFnPc1d1	symetrická
<g/>
.	.	kIx.	.
</s>
<s>
Asymetrické	asymetrický	k2eAgFnPc1d1	asymetrická
šifry	šifra	k1gFnPc1	šifra
používají	používat	k5eAaImIp3nP	používat
dvojice	dvojice	k1gFnPc4	dvojice
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
šifrování	šifrování	k1gNnSc4	šifrování
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
pro	pro	k7c4	pro
dešifrování	dešifrování	k1gNnSc4	dešifrování
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
z	z	k7c2	z
šifrovacího	šifrovací	k2eAgInSc2d1	šifrovací
klíče	klíč	k1gInSc2	klíč
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
dešifrovací	dešifrovací	k2eAgMnSc1d1	dešifrovací
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
možné	možný	k2eAgNnSc1d1	možné
generovat	generovat	k5eAaImF	generovat
dvojice	dvojice	k1gFnPc4	dvojice
šifrovacích	šifrovací	k2eAgInPc2d1	šifrovací
a	a	k8xC	a
dešifrovacích	dešifrovací	k2eAgInPc2d1	dešifrovací
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spočítat	spočítat	k5eAaPmF	spočítat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
druhý	druhý	k4xOgInSc1	druhý
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
<g/>
"	"	kIx"	"
složité	složitý	k2eAgNnSc1d1	složité
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
majitel	majitel	k1gMnSc1	majitel
dvojice	dvojice	k1gFnSc2	dvojice
klíčů	klíč	k1gInPc2	klíč
svůj	svůj	k3xOyFgInSc4	svůj
šifrovací	šifrovací	k2eAgInSc4d1	šifrovací
klíč	klíč	k1gInSc4	klíč
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
–	–	k?	–
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zvaný	zvaný	k2eAgInSc4d1	zvaný
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
klíč	klíč	k1gInSc1	klíč
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
sloužící	sloužící	k1gFnSc2	sloužící
pro	pro	k7c4	pro
dešifrování	dešifrování	k1gNnSc4	dešifrování
je	být	k5eAaImIp3nS	být
soukromý	soukromý	k2eAgInSc1d1	soukromý
klíč	klíč	k1gInSc1	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
šifrování	šifrování	k1gNnSc1	šifrování
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
zprávu	zpráva	k1gFnSc4	zpráva
použije	použít	k5eAaPmIp3nS	použít
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vícenásobné	vícenásobný	k2eAgNnSc4d1	vícenásobné
šifrování	šifrování	k1gNnSc4	šifrování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hašovací	Hašovací	k2eAgFnSc1d1	Hašovací
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
z	z	k7c2	z
libovolně	libovolně	k6eAd1	libovolně
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
textu	text	k1gInSc2	text
vytvořit	vytvořit	k5eAaPmF	vytvořit
krátký	krátký	k2eAgInSc4d1	krátký
řetězec	řetězec	k1gInSc4	řetězec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
text	text	k1gInSc1	text
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
kryptografii	kryptografie	k1gFnSc4	kryptografie
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hašovací	hašovací	k2eAgFnPc4d1	hašovací
funkce	funkce	k1gFnPc4	funkce
speciálně	speciálně	k6eAd1	speciálně
navržené	navržený	k2eAgNnSc1d1	navržené
(	(	kIx(	(
<g/>
kryptografická	kryptografický	k2eAgFnSc1d1	kryptografická
hašovací	hašovací	k2eAgFnSc1d1	hašovací
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
jiný	jiný	k2eAgInSc4d1	jiný
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
hašovací	hašovací	k2eAgFnSc2d1	hašovací
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektronický	elektronický	k2eAgInSc1d1	elektronický
podpis	podpis	k1gInSc1	podpis
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ověření	ověření	k1gNnSc4	ověření
autenticity	autenticita	k1gFnSc2	autenticita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prokázání	prokázání	k1gNnSc4	prokázání
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zprávu	zpráva	k1gFnSc4	zpráva
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
využití	využití	k1gNnSc6	využití
asymetrické	asymetrický	k2eAgFnSc2d1	asymetrická
kryptografie	kryptografie	k1gFnSc2	kryptografie
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
i	i	k9	i
nepopíratelnost	nepopíratelnost	k1gFnSc1	nepopíratelnost
–	–	k?	–
autor	autor	k1gMnSc1	autor
později	pozdě	k6eAd2	pozdě
nemůže	moct	k5eNaImIp3nS	moct
popřít	popřít	k5eAaPmF	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
zprávu	zpráva	k1gFnSc4	zpráva
podepsal	podepsat	k5eAaPmAgMnS	podepsat
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
soukromý	soukromý	k2eAgInSc1d1	soukromý
klíč	klíč	k1gInSc1	klíč
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
nezná	znát	k5eNaImIp3nS	znát
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
tak	tak	k6eAd1	tak
podpis	podpis	k1gInSc4	podpis
podvrhnout	podvrhnout	k5eAaPmF	podvrhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgInSc1d1	digitální
certifikát	certifikát	k1gInSc1	certifikát
je	být	k5eAaImIp3nS	být
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
totožnost	totožnost	k1gFnSc4	totožnost
jeho	jeho	k3xOp3gMnSc2	jeho
majitele	majitel	k1gMnSc2	majitel
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
certifikát	certifikát	k1gInSc1	certifikát
elektronicky	elektronicky	k6eAd1	elektronicky
podepsán	podepsat	k5eAaPmNgInS	podepsat
autoritou	autorita	k1gFnSc7	autorita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
důvěřujeme	důvěřovat	k5eAaImIp1nP	důvěřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
metody	metoda	k1gFnPc1	metoda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Steganografie	Steganografie	k1gFnSc2	Steganografie
===	===	k?	===
</s>
</p>
<p>
<s>
Starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
kryptografie	kryptografie	k1gFnSc2	kryptografie
je	být	k5eAaImIp3nS	být
steganografie	steganografie	k1gFnSc1	steganografie
neboli	neboli	k8xC	neboli
ukrývání	ukrývání	k1gNnSc1	ukrývání
zprávy	zpráva	k1gFnSc2	zpráva
jako	jako	k8xC	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
různé	různý	k2eAgInPc4d1	různý
neviditelné	viditelný	k2eNgInPc4d1	neviditelný
inkousty	inkoust	k1gInPc4	inkoust
<g/>
,	,	kIx,	,
vyrývání	vyrývání	k1gNnSc4	vyrývání
zprávy	zpráva	k1gFnSc2	zpráva
do	do	k7c2	do
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zalije	zalít	k5eAaPmIp3nS	zalít
voskem	vosk	k1gInSc7	vosk
apod.	apod.	kA	apod.
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
lze	lze	k6eAd1	lze
tajné	tajný	k2eAgInPc1d1	tajný
texty	text	k1gInPc1	text
ukrývat	ukrývat	k5eAaImF	ukrývat
například	například	k6eAd1	například
do	do	k7c2	do
souborů	soubor	k1gInPc2	soubor
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
či	či	k8xC	či
obrázky	obrázek	k1gInPc7	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Trithemius	Trithemius	k1gMnSc1	Trithemius
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1462	[number]	k4	1462
v	v	k7c6	v
Trittenheimu	Trittenheim	k1gInSc6	Trittenheim
<g/>
;	;	kIx,	;
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1516	[number]	k4	1516
v	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
a	a	k8xC	a
mnohostranný	mnohostranný	k2eAgMnSc1d1	mnohostranný
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k8xS	jako
teoretik	teoretik	k1gMnSc1	teoretik
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
,	,	kIx,	,
steganografie	steganografie	k1gFnSc1	steganografie
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
tři	tři	k4xCgInPc4	tři
svazky	svazek	k1gInPc4	svazek
díla	dílo	k1gNnSc2	dílo
Steganographia	Steganographia	k1gFnSc1	Steganographia
(	(	kIx(	(
<g/>
napsána	napsán	k2eAgFnSc1d1	napsána
roku	rok	k1gInSc2	rok
1499	[number]	k4	1499
a	a	k8xC	a
vydána	vydán	k2eAgFnSc1d1	vydána
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
na	na	k7c4	na
Index	index	k1gInSc4	index
Librorum	Librorum	k1gInSc1	Librorum
Prohibitorum	Prohibitorum	k1gNnSc1	Prohibitorum
umístěna	umístěn	k2eAgFnSc1d1	umístěna
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Substituční	substituční	k2eAgFnPc4d1	substituční
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
Substituční	substituční	k2eAgFnSc1d1	substituční
šifra	šifra	k1gFnSc1	šifra
obecně	obecně	k6eAd1	obecně
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
nahrazení	nahrazení	k1gNnSc4	nahrazení
každého	každý	k3xTgInSc2	každý
znaku	znak	k1gInSc2	znak
zprávy	zpráva	k1gFnSc2	zpráva
jiným	jiný	k2eAgInSc7d1	jiný
znakem	znak	k1gInSc7	znak
podle	podle	k7c2	podle
nějakého	nějaký	k3yIgNnSc2	nějaký
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Posun	posun	k1gInSc1	posun
písmen	písmeno	k1gNnPc2	písmeno
====	====	k?	====
</s>
</p>
<p>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
Juliu	Julius	k1gMnSc6	Julius
Caesarovi	Caesar	k1gMnSc6	Caesar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
písmeno	písmeno	k1gNnSc1	písmeno
tajné	tajný	k2eAgFnSc2d1	tajná
zprávy	zpráva	k1gFnSc2	zpráva
je	být	k5eAaImIp3nS	být
posunuto	posunut	k2eAgNnSc1d1	posunuto
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
o	o	k7c4	o
pevný	pevný	k2eAgInSc4d1	pevný
počet	počet	k1gInSc4	počet
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
luštitelná	luštitelný	k2eAgFnSc1d1	luštitelná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k4c4	málo
možných	možný	k2eAgInPc2d1	možný
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
představovala	představovat	k5eAaImAgFnS	představovat
nevídanou	vídaný	k2eNgFnSc4d1	nevídaná
metodu	metoda	k1gFnSc4	metoda
a	a	k8xC	a
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulky	tabulka	k1gFnPc1	tabulka
záměn	záměna	k1gFnPc2	záměna
====	====	k?	====
</s>
</p>
<p>
<s>
Šifrování	šifrování	k1gNnSc1	šifrování
pomocí	pomocí	k7c2	pomocí
tabulky	tabulka	k1gFnSc2	tabulka
záměny	záměna	k1gFnSc2	záměna
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
záměně	záměna	k1gFnSc6	záměna
znaku	znak	k1gInSc2	znak
za	za	k7c4	za
jiný	jiný	k2eAgInSc4d1	jiný
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
souvislosti	souvislost	k1gFnSc2	souvislost
či	či	k8xC	či
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalosti	znalost	k1gFnSc2	znalost
klíče	klíč	k1gInSc2	klíč
(	(	kIx(	(
<g/>
hesla	heslo	k1gNnPc4	heslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
tabulky	tabulka	k1gFnSc2	tabulka
záměny	záměna	k1gFnSc2	záměna
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
slova	slovo	k1gNnSc2	slovo
VESLO	veslo	k1gNnSc4	veslo
jako	jako	k8xS	jako
klíče	klíč	k1gInPc4	klíč
<g/>
:	:	kIx,	:
<g/>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
===	===	k?	===
Aditivní	aditivní	k2eAgFnPc4d1	aditivní
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vigenè	Vigenè	k1gFnSc1	Vigenè
šifra	šifra	k1gFnSc1	šifra
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
polyalfabetické	polyalfabetický	k2eAgFnSc2d1	polyalfabetická
šifry	šifra	k1gFnSc2	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Vigenè	Vigenè	k?	Vigenè
šifra	šifra	k1gFnSc1	šifra
používá	používat	k5eAaImIp3nS	používat
heslo	heslo	k1gNnSc1	heslo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
znaky	znak	k1gInPc1	znak
určují	určovat	k5eAaImIp3nP	určovat
posunutí	posunutí	k1gNnSc4	posunutí
otevřeného	otevřený	k2eAgInSc2d1	otevřený
textu	text	k1gInSc2	text
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřený	otevřený	k2eAgInSc1d1	otevřený
text	text	k1gInSc1	text
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
bloky	blok	k1gInPc4	blok
znaků	znak	k1gInPc2	znak
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
heslo	heslo	k1gNnSc1	heslo
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
sečte	sečíst	k5eAaPmIp3nS	sečíst
s	s	k7c7	s
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
znakem	znak	k1gInSc7	znak
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
Vigenè	Vigenè	k1gFnSc2	Vigenè
šifry	šifra	k1gFnSc2	šifra
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
jeden	jeden	k4xCgInSc4	jeden
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Vigenè	Vigenè	k?	Vigenè
šifra	šifra	k1gFnSc1	šifra
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
rozložení	rozložení	k1gNnSc2	rozložení
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
podstatně	podstatně	k6eAd1	podstatně
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
kryptoanalýzu	kryptoanalýza	k1gFnSc4	kryptoanalýza
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
četnosti	četnost	k1gFnSc2	četnost
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Luštění	luštění	k1gNnSc1	luštění
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
bigramových	bigramový	k2eAgFnPc2d1	bigramový
či	či	k8xC	či
trigramových	trigramový	k2eAgFnPc2d1	trigramový
dvojic	dvojice	k1gFnPc2	dvojice
v	v	k7c6	v
šifrovém	šifrový	k2eAgInSc6d1	šifrový
textu	text	k1gInSc6	text
a	a	k8xC	a
určováním	určování	k1gNnSc7	určování
jejich	jejich	k3xOp3gMnSc2	jejich
společného	společný	k2eAgMnSc2d1	společný
dělitele	dělitel	k1gMnSc2	dělitel
vedoucího	vedoucí	k1gMnSc2	vedoucí
k	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
délky	délka	k1gFnSc2	délka
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
slova	slovo	k1gNnSc2	slovo
HESLO	heslo	k1gNnSc4	heslo
jako	jako	k8xC	jako
klíče	klíč	k1gInPc4	klíč
k	k	k7c3	k
zašifrování	zašifrování	k1gNnSc3	zašifrování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
====	====	k?	====
</s>
</p>
<p>
<s>
Vernamova	Vernamův	k2eAgFnSc1d1	Vernamova
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
anglicky	anglicky	k6eAd1	anglicky
často	často	k6eAd1	často
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
one-time	oneimat	k5eAaPmIp3nS	one-timat
pad	padnout	k5eAaPmDgInS	padnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
jednorázová	jednorázový	k2eAgFnSc1d1	jednorázová
tabulková	tabulkový	k2eAgFnSc1d1	tabulková
šifra	šifra	k1gFnSc1	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
známou	známý	k2eAgFnSc4d1	známá
šifru	šifra	k1gFnSc4	šifra
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
exaktně	exaktně	k6eAd1	exaktně
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nerozluštitelná	rozluštitelný	k2eNgFnSc1d1	nerozluštitelná
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Vigenè	Vigenè	k1gFnSc1	Vigenè
šifra	šifra	k1gFnSc1	šifra
i	i	k8xC	i
tato	tento	k3xDgFnSc1	tento
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
sčítání	sčítání	k1gNnSc6	sčítání
písmen	písmeno	k1gNnPc2	písmeno
otevřeného	otevřený	k2eAgInSc2d1	otevřený
textu	text	k1gInSc2	text
a	a	k8xC	a
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
heslo	heslo	k1gNnSc1	heslo
je	být	k5eAaImIp3nS	být
blok	blok	k1gInSc4	blok
náhodně	náhodně	k6eAd1	náhodně
zvolených	zvolený	k2eAgNnPc2d1	zvolené
dat	datum	k1gNnPc2	datum
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc4d1	otevřený
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
splněny	splněn	k2eAgFnPc4d1	splněna
podmínky	podmínka	k1gFnPc4	podmínka
zcela	zcela	k6eAd1	zcela
náhodného	náhodný	k2eAgInSc2d1	náhodný
klíče	klíč	k1gInSc2	klíč
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
délce	délka	k1gFnSc6	délka
jako	jako	k8xS	jako
zpráva	zpráva	k1gFnSc1	zpráva
sama	sám	k3xTgFnSc1	sám
(	(	kIx(	(
<g/>
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
šifrovací	šifrovací	k2eAgFnSc1d1	šifrovací
metoda	metoda	k1gFnSc1	metoda
absolutně	absolutně	k6eAd1	absolutně
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
klíče	klíč	k1gInSc2	klíč
však	však	k9	však
pro	pro	k7c4	pro
běžné	běžný	k2eAgInPc4d1	běžný
účely	účel	k1gInPc4	účel
použití	použití	k1gNnSc2	použití
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
zpravidla	zpravidla	k6eAd1	zpravidla
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
velice	velice	k6eAd1	velice
specializované	specializovaný	k2eAgInPc4d1	specializovaný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
horká	horký	k2eAgFnSc1d1	horká
linka	linka	k1gFnSc1	linka
spojující	spojující	k2eAgFnSc1d1	spojující
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
Moskvu	Moskva	k1gFnSc4	Moskva
a	a	k8xC	a
Washington	Washington	k1gInSc4	Washington
používala	používat	k5eAaImAgFnS	používat
Vernamovu	Vernamův	k2eAgFnSc4d1	Vernamova
šifru	šifra	k1gFnSc4	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
kryptografií	kryptografie	k1gFnSc7	kryptografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Transpoziční	transpoziční	k2eAgFnPc4d1	transpoziční
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
Transpozice	transpozice	k1gFnSc1	transpozice
neboli	neboli	k8xC	neboli
přesmyčka	přesmyčka	k1gFnSc1	přesmyčka
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
pořadí	pořadí	k1gNnSc2	pořadí
znaků	znak	k1gInPc2	znak
podle	podle	k7c2	podle
určitého	určitý	k2eAgNnSc2d1	určité
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřený	otevřený	k2eAgInSc1d1	otevřený
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
po	po	k7c6	po
řádcích	řádek	k1gInPc6	řádek
a	a	k8xC	a
šifrový	šifrový	k2eAgInSc1d1	šifrový
text	text	k1gInSc1	text
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
čtením	čtení	k1gNnSc7	čtení
sloupců	sloupec	k1gInPc2	sloupec
téže	tenže	k3xDgFnSc2	tenže
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skytalé	Skytalý	k2eAgMnPc4d1	Skytalý
====	====	k?	====
</s>
</p>
<p>
<s>
Skytalé	Skytalý	k2eAgNnSc1d1	Skytalý
je	být	k5eAaImIp3nS	být
šifra	šifra	k1gFnSc1	šifra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
válec	válec	k1gInSc4	válec
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
navinutý	navinutý	k2eAgInSc1d1	navinutý
papyrus	papyrus	k1gInSc4	papyrus
či	či	k8xC	či
pergamen	pergamen	k1gInSc4	pergamen
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
napsaný	napsaný	k2eAgInSc4d1	napsaný
vzkaz	vzkaz	k1gInSc4	vzkaz
<g/>
.	.	kIx.	.
</s>
<s>
Skytalé	Skytalý	k2eAgNnSc1d1	Skytalý
používali	používat	k5eAaImAgMnP	používat
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Sparťané	Sparťan	k1gMnPc1	Sparťan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
využívali	využívat	k5eAaPmAgMnP	využívat
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Transpoziční	transpoziční	k2eAgFnSc1d1	transpoziční
mřížka	mřížka	k1gFnSc1	mřížka
====	====	k?	====
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc4	některý
políčka	políčko	k1gNnPc4	políčko
vystřižena	vystřižen	k2eAgNnPc4d1	vystřiženo
a	a	k8xC	a
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
je	být	k5eAaImIp3nS	být
vpisován	vpisován	k2eAgInSc4d1	vpisován
otevřený	otevřený	k2eAgInSc4d1	otevřený
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaplnění	zaplnění	k1gNnSc3	zaplnění
všech	všecek	k3xTgNnPc2	všecek
políček	políčko	k1gNnPc2	políčko
je	být	k5eAaImIp3nS	být
tabulka	tabulka	k1gFnSc1	tabulka
otočena	otočen	k2eAgFnSc1d1	otočena
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
šifru	šifra	k1gFnSc4	šifra
použil	použít	k5eAaPmAgMnS	použít
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Matyáš	Matyáš	k1gMnSc1	Matyáš
Sandorf	Sandorf	k1gMnSc1	Sandorf
(	(	kIx(	(
<g/>
Nový	Nový	k1gMnSc5	Nový
hrabě	hrabě	k1gMnSc5	hrabě
Monte	Mont	k1gMnSc5	Mont
Christo	Christa	k1gMnSc5	Christa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
tvary	tvar	k1gInPc1	tvar
tabulky	tabulka	k1gFnSc2	tabulka
než	než	k8xS	než
čtverec	čtverec	k1gInSc1	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
šestiúhelník	šestiúhelník	k1gInSc4	šestiúhelník
se	s	k7c7	s
šestiúhelníkovými	šestiúhelníkový	k2eAgInPc7d1	šestiúhelníkový
políčky	políček	k1gInPc7	políček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	s	k7c7	s
5	[number]	k4	5
<g/>
×	×	k?	×
otáčí	otáčet	k5eAaImIp3nS	otáčet
o	o	k7c4	o
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Klíčem	klíč	k1gInSc7	klíč
této	tento	k3xDgFnSc2	tento
šifry	šifra	k1gFnSc2	šifra
je	být	k5eAaImIp3nS	být
rozložení	rozložení	k1gNnSc1	rozložení
vystříhaných	vystříhaný	k2eAgNnPc2d1	vystříhané
políček	políčko	k1gNnPc2	políčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
transpoziční	transpoziční	k2eAgFnSc2d1	transpoziční
mřížky	mřížka	k1gFnSc2	mřížka
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
červeně	červeně	k6eAd1	červeně
pro	pro	k7c4	pro
snazší	snadný	k2eAgFnSc4d2	snazší
orientaci	orientace	k1gFnSc4	orientace
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
sloupcová	sloupcový	k2eAgFnSc1d1	sloupcová
transpozice	transpozice	k1gFnSc1	transpozice
====	====	k?	====
</s>
</p>
<p>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
přepsání	přepsání	k1gNnSc2	přepsání
kódovaného	kódovaný	k2eAgInSc2d1	kódovaný
textu	text	k1gInSc2	text
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
počtu	počet	k1gInSc2	počet
písmen	písmeno	k1gNnPc2	písmeno
v	v	k7c6	v
klíčovém	klíčový	k2eAgNnSc6d1	klíčové
slově	slovo	k1gNnSc6	slovo
nebo	nebo	k8xC	nebo
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
přeskupení	přeskupení	k1gNnSc6	přeskupení
sloupců	sloupec	k1gInPc2	sloupec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalosti	znalost	k1gFnSc2	znalost
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kombinované	kombinovaný	k2eAgFnPc4d1	kombinovaná
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
či	či	k8xC	či
slabiny	slabina	k1gFnPc1	slabina
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
šifer	šifra	k1gFnPc2	šifra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
odstranit	odstranit	k5eAaPmF	odstranit
kombinací	kombinace	k1gFnPc2	kombinace
šifer	šifra	k1gFnPc2	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
potřeba	potřeba	k6eAd1	potřeba
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
kombinací	kombinace	k1gFnSc7	kombinace
stejných	stejný	k2eAgInPc2d1	stejný
druhů	druh	k1gInPc2	druh
šifer	šifra	k1gFnPc2	šifra
zesložitění	zesložitění	k1gNnSc4	zesložitění
luštění	luštění	k1gNnSc2	luštění
nedocílíme	docílit	k5eNaPmIp1nP	docílit
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
následné	následný	k2eAgNnSc1d1	následné
použití	použití	k1gNnSc1	použití
dvou	dva	k4xCgFnPc2	dva
tabulek	tabulka	k1gFnPc2	tabulka
záměn	záměna	k1gFnPc2	záměna
k	k	k7c3	k
zašifrování	zašifrování	k1gNnSc3	zašifrování
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
útočníka	útočník	k1gMnSc4	útočník
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
použití	použití	k1gNnSc4	použití
jedné	jeden	k4xCgFnSc2	jeden
tabulky	tabulka	k1gFnSc2	tabulka
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
rozložením	rozložení	k1gNnSc7	rozložení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šifrování	šifrování	k1gNnSc1	šifrování
strojem	stroj	k1gInSc7	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
mechanických	mechanický	k2eAgInPc2d1	mechanický
a	a	k8xC	a
elektronických	elektronický	k2eAgInPc2d1	elektronický
strojů	stroj	k1gInPc2	stroj
přineslo	přinést	k5eAaPmAgNnS	přinést
do	do	k7c2	do
šifrování	šifrování	k1gNnSc2	šifrování
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
možnosti	možnost	k1gFnSc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
opakování	opakování	k1gNnSc4	opakování
určitého	určitý	k2eAgInSc2d1	určitý
úkonu	úkon	k1gInSc2	úkon
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Enigma	enigma	k1gNnSc1	enigma
====	====	k?	====
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
utajování	utajování	k1gNnSc3	utajování
zpráv	zpráva	k1gFnPc2	zpráva
mechanický	mechanický	k2eAgInSc1d1	mechanický
stroj	stroj	k1gInSc1	stroj
Enigma	enigma	k1gFnSc1	enigma
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgFnPc4d1	složitá
operace	operace	k1gFnPc4	operace
se	s	k7c7	s
vstupním	vstupní	k2eAgInSc7d1	vstupní
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
války	válka	k1gFnSc2	válka
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c4	na
prolomení	prolomení	k1gNnSc4	prolomení
šifry	šifra	k1gFnSc2	šifra
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
zjištění	zjištění	k1gNnPc1	zjištění
byla	být	k5eAaImAgNnP	být
později	pozdě	k6eAd2	pozdě
nedocenitelná	docenitelný	k2eNgNnPc1d1	nedocenitelné
pro	pro	k7c4	pro
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
armády	armáda	k1gFnPc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Šifra	šifra	k1gFnSc1	šifra
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
během	během	k7c2	během
války	válka	k1gFnSc2	válka
zlomena	zlomit	k5eAaPmNgFnS	zlomit
a	a	k8xC	a
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
tak	tak	k6eAd1	tak
německé	německý	k2eAgFnSc3d1	německá
straně	strana	k1gFnSc3	strana
pouze	pouze	k6eAd1	pouze
falešný	falešný	k2eAgInSc4d1	falešný
pocit	pocit	k1gInSc4	pocit
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prolomení	prolomení	k1gNnSc1	prolomení
Enigmy	enigma	k1gFnSc2	enigma
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
nevhodným	vhodný	k2eNgInPc3d1	nevhodný
a	a	k8xC	a
nedostatečně	dostatečně	k6eNd1	dostatečně
propracovaným	propracovaný	k2eAgInPc3d1	propracovaný
postupům	postup	k1gInPc3	postup
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
používání	používání	k1gNnSc6	používání
jako	jako	k8xC	jako
byly	být	k5eAaImAgInP	být
např.	např.	kA	např.
nevyužití	nevyužití	k1gNnSc4	nevyužití
maximálního	maximální	k2eAgNnSc2d1	maximální
možného	možný	k2eAgNnSc2d1	možné
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
poskytovaného	poskytovaný	k2eAgInSc2d1	poskytovaný
propojovací	propojovací	k2eAgInSc1d1	propojovací
deskou	deska	k1gFnSc7	deska
(	(	kIx(	(
<g/>
Steckerbrett	Steckerbrett	k2eAgMnSc1d1	Steckerbrett
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
tříznakového	tříznakový	k2eAgInSc2d1	tříznakový
klíče	klíč	k1gInSc2	klíč
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každé	každý	k3xTgFnSc2	každý
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
odhadnutelné	odhadnutelný	k2eAgFnPc1d1	odhadnutelná
fráze	fráze	k1gFnPc1	fráze
v	v	k7c6	v
textu	text	k1gInSc6	text
apod.	apod.	kA	apod.
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Enigma	enigma	k1gFnSc1	enigma
používána	používat	k5eAaImNgFnS	používat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
rozluštění	rozluštění	k1gNnPc4	rozluštění
jejich	jejich	k3xOp3gFnPc2	jejich
zpráv	zpráva	k1gFnPc2	zpráva
triviální	triviálnět	k5eAaImIp3nP	triviálnět
záležitostí	záležitost	k1gFnSc7	záležitost
ani	ani	k8xC	ani
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
dnešní	dnešní	k2eAgFnSc2d1	dnešní
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enigma	enigma	k1gFnSc1	enigma
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
3	[number]	k4	3
rotory	rotor	k1gInPc4	rotor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
otáčely	otáčet	k5eAaImAgInP	otáčet
<g/>
,	,	kIx,	,
podobě	podoba	k1gFnSc6	podoba
jako	jako	k8xC	jako
mechanické	mechanický	k2eAgNnSc4d1	mechanické
počítadlo	počítadlo	k1gNnSc4	počítadlo
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
měl	mít	k5eAaImAgMnS	mít
jinak	jinak	k6eAd1	jinak
propojené	propojený	k2eAgInPc4d1	propojený
vstupní	vstupní	k2eAgInPc4d1	vstupní
a	a	k8xC	a
výstupní	výstupní	k2eAgInPc4d1	výstupní
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgMnS	měnit
průběh	průběh	k1gInSc4	průběh
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
i	i	k9	i
výsledné	výsledný	k2eAgNnSc4d1	výsledné
písmeno	písmeno	k1gNnSc4	písmeno
zašifrovaného	zašifrovaný	k2eAgInSc2d1	zašifrovaný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začalo	začít	k5eAaPmAgNnS	začít
ponorkové	ponorkový	k2eAgNnSc1d1	ponorkové
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
používat	používat	k5eAaImF	používat
čtyřrotorové	čtyřrotorový	k2eAgFnPc1d1	čtyřrotorový
Enigmy	enigma	k1gFnPc1	enigma
a	a	k8xC	a
stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
zdokonalován	zdokonalovat	k5eAaImNgInS	zdokonalovat
i	i	k9	i
zaváděním	zavádění	k1gNnSc7	zavádění
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
Uhr	Uhr	k1gFnPc2	Uhr
Box	box	k1gInSc1	box
a.j.	a.j.	k?	a.j.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgFnPc1d1	moderní
šifry	šifra	k1gFnPc1	šifra
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
šifry	šifra	k1gFnPc4	šifra
zadní	zadní	k2eAgFnPc4d1	zadní
vrátka	vrátka	k1gNnPc4	vrátka
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
šifrování	šifrování	k1gNnSc2	šifrování
závisí	záviset	k5eAaImIp3nS	záviset
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
generátor	generátor	k1gInSc1	generátor
pseudonáhodných	pseudonáhodný	k2eAgNnPc2d1	pseudonáhodné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Symetrické	symetrický	k2eAgFnPc4d1	symetrická
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
standardizovaná	standardizovaný	k2eAgFnSc1d1	standardizovaná
symetrická	symetrický	k2eAgFnSc1d1	symetrická
šifra	šifra	k1gFnSc1	šifra
DES	des	k1gNnSc2	des
(	(	kIx(	(
<g/>
Data	datum	k1gNnSc2	datum
Encryption	Encryption	k1gInSc4	Encryption
Standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
kryptografický	kryptografický	k2eAgInSc1d1	kryptografický
standard	standard	k1gInSc1	standard
AES	AES	kA	AES
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
Encryption	Encryption	k1gInSc1	Encryption
Standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Rijndael	Rijndael	k1gInSc1	Rijndael
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
AES	AES	kA	AES
podporuje	podporovat	k5eAaImIp3nS	podporovat
jen	jen	k9	jen
některé	některý	k3yIgFnPc4	některý
délky	délka	k1gFnPc4	délka
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
klíče	klíč	k1gInSc2	klíč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finalisté	finalista	k1gMnPc1	finalista
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
návrh	návrh	k1gInSc4	návrh
standardu	standard	k1gInSc2	standard
AES	AES	kA	AES
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
Encryption	Encryption	k1gInSc1	Encryption
Standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
-	-	kIx~	-
Twofish	Twofish	k1gMnSc1	Twofish
<g/>
,	,	kIx,	,
RC	RC	kA	RC
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Serpent	Serpent	k1gMnSc1	Serpent
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
a	a	k8xC	a
Rijndael	Rijndael	k1gMnSc1	Rijndael
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
soutěž	soutěž	k1gFnSc4	soutěž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
za	za	k7c4	za
standard	standard	k1gInSc4	standard
AES	AES	kA	AES
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
symetrická	symetrický	k2eAgNnPc1d1	symetrické
kryptografie	kryptografie	k1gFnPc1	kryptografie
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
Šifrování	šifrování	k1gNnSc1	šifrování
dat	datum	k1gNnPc2	datum
</s>
</p>
<p>
<s>
===	===	k?	===
Asymetrické	asymetrický	k2eAgFnPc4d1	asymetrická
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
Asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
kryptografie	kryptografie	k1gFnSc1	kryptografie
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
soukromý	soukromý	k2eAgInSc4d1	soukromý
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
</s>
</p>
<p>
<s>
RSA	RSA	kA	RSA
<g/>
,	,	kIx,	,
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
šifra	šifra	k1gFnSc1	šifra
</s>
</p>
<p>
<s>
známé	známý	k2eAgNnSc1d1	známé
využití	využití	k1gNnSc1	využití
asymetrické	asymetrický	k2eAgFnSc2d1	asymetrická
šifry	šifra	k1gFnSc2	šifra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
PGP	PGP	kA	PGP
(	(	kIx(	(
OpenPGP	OpenPGP	k1gMnSc1	OpenPGP
)	)	kIx)	)
</s>
</p>
<p>
<s>
digitální	digitální	k2eAgInSc4d1	digitální
podpis	podpis	k1gInSc4	podpis
</s>
</p>
<p>
<s>
===	===	k?	===
Hybridní	hybridní	k2eAgFnPc4d1	hybridní
šifry	šifra	k1gFnPc4	šifra
===	===	k?	===
</s>
</p>
<p>
<s>
Současné	současný	k2eAgInPc1d1	současný
asymetrické	asymetrický	k2eAgInPc1d1	asymetrický
šifrovací	šifrovací	k2eAgInPc1d1	šifrovací
systémy	systém	k1gInPc1	systém
používají	používat	k5eAaImIp3nP	používat
hybridní	hybridní	k2eAgNnSc4d1	hybridní
šifrování	šifrování	k1gNnSc4	šifrování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
výhody	výhoda	k1gFnPc4	výhoda
asymetrického	asymetrický	k2eAgNnSc2d1	asymetrické
šifrování	šifrování	k1gNnSc2	šifrování
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
symetrických	symetrický	k2eAgFnPc2d1	symetrická
šifer	šifra	k1gFnPc2	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
samotná	samotný	k2eAgFnSc1d1	samotná
se	se	k3xPyFc4	se
zašifruje	zašifrovat	k5eAaPmIp3nS	zašifrovat
symetrickým	symetrický	k2eAgNnSc7d1	symetrické
šifrováním	šifrování	k1gNnSc7	šifrování
s	s	k7c7	s
náhodně	náhodně	k6eAd1	náhodně
vygenerovaným	vygenerovaný	k2eAgInSc7d1	vygenerovaný
klíčem	klíč	k1gInSc7	klíč
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klíč	klíč	k1gInSc4	klíč
sezení	sezení	k1gNnSc2	sezení
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
session	session	k1gInSc1	session
key	key	k?	key
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g/>
,	,	kIx,	,
zašifrovaný	zašifrovaný	k2eAgInSc1d1	zašifrovaný
pomalou	pomalý	k2eAgFnSc7d1	pomalá
asymetrickou	asymetrický	k2eAgFnSc7d1	asymetrická
šifrou	šifra	k1gFnSc7	šifra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přiloží	přiložit	k5eAaPmIp3nS	přiložit
k	k	k7c3	k
zašifrované	zašifrovaný	k2eAgFnSc3d1	zašifrovaná
zprávě	zpráva	k1gFnSc3	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hašovací	Hašovací	k2eAgFnPc4d1	Hašovací
funkce	funkce	k1gFnPc4	funkce
===	===	k?	===
</s>
</p>
<p>
<s>
Hašovací	Hašovací	k2eAgFnSc1d1	Hašovací
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
jednosměrná	jednosměrný	k2eAgFnSc1d1	jednosměrná
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
kryptografie	kryptografie	k1gFnSc1	kryptografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
OTFE	OTFE	kA	OTFE
</s>
</p>
<p>
<s>
PKI	PKI	kA	PKI
</s>
</p>
<p>
<s>
Šifrování	šifrování	k1gNnSc1	šifrování
dat	datum	k1gNnPc2	datum
</s>
</p>
<p>
<s>
Útoky	útok	k1gInPc7	útok
postranními	postranní	k2eAgInPc7d1	postranní
kanály	kanál	k1gInPc7	kanál
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Singh	Singh	k1gMnSc1	Singh
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Kniha	kniha	k1gFnSc1	kniha
kódů	kód	k1gInPc2	kód
a	a	k8xC	a
šifer	šifra	k1gFnPc2	šifra
<g/>
,	,	kIx,	,
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Kryptografie	kryptografie	k1gFnSc1	kryptografie
Karlín	Karlín	k1gInSc1	Karlín
–	–	k?	–
přednášky	přednáška	k1gFnSc2	přednáška
a	a	k8xC	a
studijní	studijní	k2eAgInPc4d1	studijní
materiály	materiál	k1gInPc4	materiál
z	z	k7c2	z
MFF	MFF	kA	MFF
UK	UK	kA	UK
</s>
</p>
<p>
<s>
Rychlý	Rychlý	k1gMnSc1	Rychlý
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
laický	laický	k2eAgInSc4d1	laický
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
kryptografie	kryptografie	k1gFnSc2	kryptografie
a	a	k8xC	a
zabezpečeného	zabezpečený	k2eAgInSc2d1	zabezpečený
přenosu	přenos	k1gInSc2	přenos
souborů	soubor	k1gInPc2	soubor
–	–	k?	–
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
téma	téma	k1gNnSc4	téma
z	z	k7c2	z
MFF	MFF	kA	MFF
UK	UK	kA	UK
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kryptografie	kryptografie	k1gFnSc2	kryptografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Code	Cod	k1gFnSc2	Cod
Talkers	Talkersa	k1gFnPc2	Talkersa
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc4	seriál
Diskové	diskový	k2eAgNnSc4d1	diskové
šifrování	šifrování	k1gNnSc4	šifrování
pomocí	pomocí	k7c2	pomocí
dm-crypt	dmrypta	k1gFnPc2	dm-crypta
<g/>
/	/	kIx~	/
<g/>
LUKS	LUKS	kA	LUKS
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc4	seznam
schválených	schválený	k2eAgNnPc2d1	schválené
šifrovacích	šifrovací	k2eAgNnPc2d1	šifrovací
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
EU	EU	kA	EU
</s>
</p>
<p>
<s>
Problémy	problém	k1gInPc1	problém
implementace	implementace	k1gFnSc2	implementace
šifrování	šifrování	k1gNnSc2	šifrování
</s>
</p>
