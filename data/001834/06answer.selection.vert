<s>
Oxford	Oxford	k1gInSc1	Oxford
byl	být	k5eAaImAgInS	být
osídlen	osídlen	k2eAgInSc1d1	osídlen
Sasy	Sas	k1gMnPc7	Sas
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Oxenaforda	Oxenaforda	k1gMnSc1	Oxenaforda
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
klášter	klášter	k1gInSc1	klášter
svaté	svatý	k2eAgFnSc2d1	svatá
Frideswidy	Frideswida	k1gFnSc2	Frideswida
a	a	k8xC	a
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
kronice	kronika	k1gFnSc6	kronika
u	u	k7c2	u
roku	rok	k1gInSc2	rok
912	[number]	k4	912
<g/>
.	.	kIx.	.
</s>
