<s>
Antonín	Antonín	k1gMnSc1	Antonín
Pilgram	Pilgram	k1gInSc1	Pilgram
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1460	[number]	k4	1460
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
1516	[number]	k4	1516
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
patrně	patrně	k6eAd1	patrně
roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
dochovala	dochovat	k5eAaPmAgFnS	dochovat
řada	řada	k1gFnSc1	řada
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
nejznámějším	známý	k2eAgInPc3d3	nejznámější
dílům	díl	k1gInPc3	díl
patří	patřit	k5eAaImIp3nS	patřit
portál	portál	k1gInSc1	portál
na	na	k7c6	na
Staré	Staré	k2eAgFnSc6d1	Staré
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
brněnských	brněnský	k2eAgFnPc2d1	brněnská
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
první	první	k4xOgFnSc2	první
samostatné	samostatný	k2eAgFnSc2d1	samostatná
práce	práce	k1gFnSc2	práce
realizoval	realizovat	k5eAaBmAgMnS	realizovat
ve	v	k7c6	v
Švábsku	Švábsko	k1gNnSc6	Švábsko
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
ve	v	k7c6	v
Schwieberdingenu	Schwieberdingen	k1gInSc6	Schwieberdingen
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
v	v	k7c6	v
Rottweilu	Rottweil	k1gInSc6	Rottweil
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1511	[number]	k4	1511
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
participace	participace	k1gFnSc2	participace
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1502	[number]	k4	1502
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
předsíň	předsíň	k1gFnSc1	předsíň
<g/>
,	,	kIx,	,
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
)	)	kIx)	)
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
Židovské	židovský	k2eAgFnSc2d1	židovská
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c4	v
r.	r.	kA	r.
1508	[number]	k4	1508
<g/>
)	)	kIx)	)
portál	portál	k1gInSc1	portál
Staré	Staré	k2eAgFnSc2d1	Staré
radnice	radnice	k1gFnSc2	radnice
(	(	kIx(	(
<g/>
před	před	k7c7	před
r.	r.	kA	r.
1511	[number]	k4	1511
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	let	k1gInPc6	let
1511	[number]	k4	1511
<g/>
–	–	k?	–
<g/>
1515	[number]	k4	1515
vedl	vést	k5eAaImAgMnS	vést
<g />
.	.	kIx.	.
</s>
<s>
kamenickou	kamenický	k2eAgFnSc4d1	kamenická
huť	huť	k1gFnSc4	huť
při	při	k7c6	při
dómu	dóm	k1gInSc6	dóm
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
Dómu	dóm	k1gInSc6	dóm
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
po	po	k7c6	po
r.	r.	kA	r.
1511	[number]	k4	1511
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
často	často	k6eAd1	často
připisuje	připisovat	k5eAaImIp3nS	připisovat
kazatelna	kazatelna	k1gFnSc1	kazatelna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kamenické	kamenický	k2eAgFnSc2d1	kamenická
značky	značka	k1gFnSc2	značka
se	se	k3xPyFc4	se
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
realizaci	realizace	k1gFnSc6	realizace
patrně	patrně	k6eAd1	patrně
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
spíše	spíše	k9	spíše
připisuje	připisovat	k5eAaImIp3nS	připisovat
Niclasi	Niclas	k1gMnPc1	Niclas
Gerhaertu	Gerhaert	k1gInSc2	Gerhaert
van	van	k1gInSc4	van
Leyden	Leydno	k1gNnPc2	Leydno
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
Pilgramův	Pilgramův	k2eAgInSc1d1	Pilgramův
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
dalším	další	k2eAgInSc7d1	další
příspěvkem	příspěvek	k1gInSc7	příspěvek
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
podnoží	podnoží	k1gNnSc4	podnoží
varhan	varhany	k1gFnPc2	varhany
řezbářské	řezbářský	k2eAgFnSc2d1	řezbářská
plastiky	plastika	k1gFnSc2	plastika
světců	světec	k1gMnPc2	světec
(	(	kIx(	(
<g/>
uloženy	uložen	k2eAgFnPc4d1	uložena
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
galerii	galerie	k1gFnSc6	galerie
<g/>
)	)	kIx)	)
plastika	plastika	k1gFnSc1	plastika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
mučedníka	mučedník	k1gMnSc2	mučedník
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
rovněž	rovněž	k9	rovněž
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antonín	Antonín	k1gMnSc1	Antonín
Pilgram	Pilgram	k1gInSc1	Pilgram
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Antonín	Antonín	k1gMnSc1	Antonín
Pilgram	Pilgram	k1gInSc1	Pilgram
na	na	k7c6	na
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Brna	Brno	k1gNnSc2	Brno
Pilgram	Pilgram	k1gInSc1	Pilgram
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Brna	Brno	k1gNnSc2	Brno
</s>
