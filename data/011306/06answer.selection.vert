<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
garantovala	garantovat	k5eAaBmAgFnS	garantovat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
nezávislost	nezávislost	k1gFnSc1	nezávislost
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gNnSc7	jeho
formálním	formální	k2eAgNnSc7d1	formální
začleněním	začlenění	k1gNnSc7	začlenění
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
nabízela	nabízet	k5eAaImAgFnS	nabízet
českým	český	k2eAgMnPc3d1	český
králům	král	k1gMnPc3	král
jako	jako	k8xS	jako
říšským	říšský	k2eAgMnPc3d1	říšský
knížatům	kníže	k1gMnPc3wR	kníže
nové	nový	k2eAgFnSc2d1	nová
možnosti	možnost	k1gFnSc2	možnost
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
