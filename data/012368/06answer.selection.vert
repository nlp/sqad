<s>
Blatnice	Blatnice	k1gFnPc4	Blatnice
klade	klást	k5eAaImIp3nS	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
do	do	k7c2	do
provazců	provazec	k1gInPc2	provazec
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
cm	cm	kA	cm
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
samice	samice	k1gFnSc1	samice
namotává	namotávat	k5eAaImIp3nS	namotávat
na	na	k7c4	na
stonky	stonek	k1gInPc4	stonek
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
kolem	kolem	k7c2	kolem
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
