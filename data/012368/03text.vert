<p>
<s>
Blatnice	Blatnice	k1gFnPc1	Blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
(	(	kIx(	(
<g/>
Pelobates	Pelobates	k1gMnSc1	Pelobates
fuscus	fuscus	k1gMnSc1	fuscus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
blatnice	blatnice	k1gFnSc1	blatnice
česneková	česnekový	k2eAgFnSc1d1	česneková
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
žáby	žába	k1gFnSc2	žába
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
blatnicovití	blatnicovitý	k2eAgMnPc1d1	blatnicovitý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
pásu	pás	k1gInSc6	pás
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
po	po	k7c4	po
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Blatnice	Blatnice	k1gFnPc1	Blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
je	být	k5eAaImIp3nS	být
žába	žába	k1gFnSc1	žába
středních	střední	k2eAgInPc2d1	střední
až	až	k8xS	až
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
(	(	kIx(	(
<g/>
samci	samec	k1gInPc7	samec
4,2	[number]	k4	4,2
<g/>
–	–	k?	–
<g/>
6,6	[number]	k4	6,6
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
5,6	[number]	k4	5,6
<g/>
–	–	k?	–
<g/>
7,2	[number]	k4	7,2
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
na	na	k7c6	na
lehkých	lehký	k2eAgFnPc6d1	lehká
půdách	půda	k1gFnPc6	půda
(	(	kIx(	(
<g/>
spraše	spraš	k1gFnPc1	spraš
<g/>
,	,	kIx,	,
hnědozemě	hnědozem	k1gFnPc1	hnědozem
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
rozmnožovací	rozmnožovací	k2eAgNnSc4d1	rozmnožovací
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
<g/>
Má	mít	k5eAaImIp3nS	mít
vpředu	vpředu	k6eAd1	vpředu
zašpičatělou	zašpičatělý	k2eAgFnSc4d1	zašpičatělá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
vystouplé	vystouplý	k2eAgFnSc6d1	vystouplá
oči	oko	k1gNnPc4	oko
s	s	k7c7	s
hnědožlutou	hnědožlutý	k2eAgFnSc7d1	hnědožlutá
duhovkou	duhovka	k1gFnSc7	duhovka
a	a	k8xC	a
temenním	temenní	k2eAgInSc7d1	temenní
hrbolkem	hrbolek	k1gInSc7	hrbolek
<g/>
.	.	kIx.	.
</s>
<s>
Ušní	ušní	k2eAgInSc1d1	ušní
otvor	otvor	k1gInSc1	otvor
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
patrný	patrný	k2eAgInSc1d1	patrný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
masivní	masivní	k2eAgFnPc4d1	masivní
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
samci	samec	k1gMnPc1	samec
mívají	mívat	k5eAaImIp3nP	mívat
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
typický	typický	k2eAgInSc4d1	typický
pářicí	pářicí	k2eAgInSc4d1	pářicí
mozol	mozol	k1gInSc4	mozol
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
jinými	jiný	k2eAgFnPc7d1	jiná
žábami	žába	k1gFnPc7	žába
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnSc6d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
blatnice	blatnice	k1gFnSc2	blatnice
skvrnité	skvrnitý	k2eAgFnSc2d1	skvrnitá
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
hrubá	hrubý	k2eAgFnSc1d1	hrubá
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bradavičky	bradavička	k1gFnPc1	bradavička
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
sem	sem	k6eAd1	sem
tam	tam	k6eAd1	tam
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
příliš	příliš	k6eAd1	příliš
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
podkladová	podkladový	k2eAgFnSc1d1	podkladová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
šedavá	šedavý	k2eAgFnSc1d1	šedavá
až	až	k6eAd1	až
béžová	béžový	k2eAgFnSc1d1	béžová
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc1	skvrna
jsou	být	k5eAaImIp3nP	být
tmavší	tmavý	k2eAgFnPc1d2	tmavší
a	a	k8xC	a
pískově	pískově	k6eAd1	pískově
či	či	k8xC	či
kaštanově	kaštanově	k6eAd1	kaštanově
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
šedohnědé	šedohnědý	k2eAgFnPc1d1	šedohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
odstínech	odstín	k1gInPc6	odstín
šedivé	šedivý	k2eAgFnSc2d1	šedivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blatnice	Blatnice	k1gFnPc4	Blatnice
klade	klást	k5eAaImIp3nS	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
do	do	k7c2	do
provazců	provazec	k1gInPc2	provazec
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
cm	cm	kA	cm
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
samice	samice	k1gFnSc1	samice
namotává	namotávat	k5eAaImIp3nS	namotávat
na	na	k7c4	na
stonky	stonek	k1gInPc4	stonek
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
kolem	kolem	k7c2	kolem
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nápadní	nápadní	k2eAgMnPc1d1	nápadní
jsou	být	k5eAaImIp3nP	být
pulci	pulec	k1gMnPc1	pulec
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
velikosti	velikost	k1gFnPc1	velikost
8,6	[number]	k4	8,6
až	až	k9	až
18,2	[number]	k4	18,2
cm	cm	kA	cm
(	(	kIx(	(
<g/>
vzácně	vzácně	k6eAd1	vzácně
i	i	k8xC	i
více	hodně	k6eAd2	hodně
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
Čermné	Čermný	k2eAgFnSc2d1	Čermná
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
pozorován	pozorován	k2eAgMnSc1d1	pozorován
jedinec	jedinec	k1gMnSc1	jedinec
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
22,6	[number]	k4	22,6
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyplašena	vyplašit	k5eAaPmNgFnS	vyplašit
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
hlasité	hlasitý	k2eAgInPc4d1	hlasitý
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
odpudivý	odpudivý	k2eAgInSc1d1	odpudivý
výměšek	výměšek	k1gInSc1	výměšek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
páchne	páchnout	k5eAaImIp3nS	páchnout
po	po	k7c6	po
česneku	česnek	k1gInSc6	česnek
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
blatnice	blatnice	k1gFnSc2	blatnice
česneková	česnekový	k2eAgFnSc1d1	česneková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Pelobates	Pelobates	k1gMnSc1	Pelobates
fuscus	fuscus	k1gMnSc1	fuscus
fuscus	fuscus	k1gMnSc1	fuscus
(	(	kIx(	(
<g/>
blatnice	blatnice	k1gFnSc1	blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
)	)	kIx)	)
–	–	k?	–
převažující	převažující	k2eAgInSc4d1	převažující
poddruh	poddruh	k1gInSc4	poddruh
</s>
</p>
<p>
<s>
Pelobates	Pelobates	k1gMnSc1	Pelobates
fuscus	fuscus	k1gMnSc1	fuscus
insubricus	insubricus	k1gMnSc1	insubricus
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
blatnice	blatnice	k1gFnSc1	blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
blatnice	blatnice	k1gFnSc2	blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Pelobates	Pelobates	k1gInSc1	Pelobates
fuscus	fuscus	k1gInSc1	fuscus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Blatnice	Blatnice	k1gFnPc1	Blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
na	na	k7c6	na
Biolib	Biolib	k1gMnSc1	Biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
této	tento	k3xDgFnSc2	tento
žáby	žába	k1gFnSc2	žába
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
webu	web	k1gInSc6	web
Biolib	Bioliba	k1gFnPc2	Bioliba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
