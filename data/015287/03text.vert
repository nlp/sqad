<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblast	oblast	k1gFnSc4
Blanský	blanský	k2eAgInSc1d1
lesIUCN	lesIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
CHKO	CHKO	kA
Blanský	blanský	k2eAgInSc1d1
lesZákladní	lesZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
420	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
212,35	212,35	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
35,23	35,23	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
28,19	28,19	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
31	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.blanskyles.ochranaprirody.cz	www.blanskyles.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zaujímá	zaujímat	k5eAaImIp3nS
plochu	plocha	k1gFnSc4
212,35	212,35	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
území	území	k1gNnSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
okresu	okres	k1gInSc2
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
západě	západ	k1gInSc6
do	do	k7c2
okresů	okres	k1gInPc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
a	a	k8xC
Prachatice	Prachatice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pozoruhodně	pozoruhodně	k6eAd1
zachovalý	zachovalý	k2eAgInSc4d1
krajinný	krajinný	k2eAgInSc4d1
celek	celek	k1gInSc4
v	v	k7c6
Šumavském	šumavský	k2eAgNnSc6d1
podhůří	podhůří	k1gNnSc6
s	s	k7c7
četnými	četný	k2eAgFnPc7d1
cennými	cenný	k2eAgFnPc7d1
lokalitami	lokalita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Horopis	horopis	k1gInSc1
</s>
<s>
Území	území	k1gNnSc1
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
geomorfologického	geomorfologický	k2eAgInSc2d1
celku	celek	k1gInSc2
Šumavské	šumavský	k2eAgNnSc1d1
podhůří	podhůří	k1gNnSc1
<g/>
,	,	kIx,
podcelků	podcelek	k1gInPc2
Prachatická	prachatický	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
(	(	kIx(
<g/>
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bavorovská	Bavorovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
severozápadě	severozápad	k1gInSc6
území	území	k1gNnSc2
patří	patřit	k5eAaImIp3nP
k	k	k7c3
okrsku	okrsek	k1gInSc3
Netolická	netolický	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Českokrumlovská	českokrumlovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
jihu	jih	k1gInSc6
CHKO	CHKO	kA
malý	malý	k2eAgInSc1d1
cíp	cíp	k1gInSc1
okrsku	okrsek	k1gInSc2
Boletická	Boletický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
podcelku	podcelek	k1gInSc2
Prachatická	prachatický	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
zasahují	zasahovat	k5eAaImIp3nP
do	do	k7c2
území	území	k1gNnSc2
okrsky	okrsek	k1gInPc4
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
(	(	kIx(
<g/>
centrální	centrální	k2eAgFnSc4d1
nejvyšší	vysoký	k2eAgFnSc4d3
část	část	k1gFnSc4
území	území	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Křemžská	Křemžský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
(	(	kIx(
<g/>
centrální	centrální	k2eAgFnSc1d1
sníženina	sníženina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Chvalšinská	Chvalšinský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
Lhenická	Lhenický	k2eAgFnSc1d1
brázda	brázda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruh	pruh	k1gInSc1
na	na	k7c6
východě	východ	k1gInSc6
území	území	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
Kaplická	Kaplický	k2eAgFnSc1d1
brázda	brázda	k1gFnSc1
(	(	kIx(
<g/>
okrsky	okrsek	k1gInPc4
Kroclovská	Kroclovský	k2eAgFnSc1d1
a	a	k8xC
Velešínská	Velešínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
již	již	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
celku	celek	k1gInSc3
Novohradské	novohradský	k2eAgNnSc1d1
podhůří	podhůří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1
masiv	masiv	k1gInSc1
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
je	být	k5eAaImIp3nS
plochá	plochý	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInPc1
široké	široký	k2eAgInPc1d1
hřbety	hřbet	k1gInPc1
podkovovitě	podkovovitě	k6eAd1
obklopují	obklopovat	k5eAaImIp3nP
hlubokou	hluboký	k2eAgFnSc4d1
tektonickou	tektonický	k2eAgFnSc4d1
kotlinu	kotlina	k1gFnSc4
Křemžského	Křemžský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
území	území	k1gNnSc4
má	mít	k5eAaImIp3nS
spíše	spíše	k9
charakter	charakter	k1gInSc1
ploché	plochý	k2eAgFnSc2d1
až	až	k8xS
členité	členitý	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východě	východ	k1gInSc6
je	být	k5eAaImIp3nS
masiv	masiv	k1gInSc1
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
ostře	ostro	k6eAd1
ohraničen	ohraničit	k5eAaPmNgInS
hluboce	hluboko	k6eAd1
zaříznutým	zaříznutý	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
Vltavy	Vltava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
kotlin	kotlina	k1gFnPc2
je	být	k5eAaImIp3nS
550	#num#	k4
m	m	kA
<g/>
,	,	kIx,
hřbetů	hřbet	k1gInPc2
750	#num#	k4
m.	m.	k?
</s>
<s>
Významné	významný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Kleť	Kleť	k1gFnSc1
(	(	kIx(
<g/>
1087	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Bulový	Bulový	k2eAgMnSc1d1
(	(	kIx(
<g/>
953	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Buglata	Bugle	k1gNnPc1
(	(	kIx(
<g/>
832	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
Běta	Běta	k1gFnSc1
(	(	kIx(
<g/>
804	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Kluk	kluk	k1gMnSc1
(	(	kIx(
<g/>
741	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Chlumečský	Chlumečský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
610	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
V	v	k7c6
geologickém	geologický	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
zcela	zcela	k6eAd1
převládají	převládat	k5eAaImIp3nP
přeměněné	přeměněný	k2eAgFnPc1d1
horniny	hornina	k1gFnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
granulit	granulit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
vložky	vložka	k1gFnPc1
hadců	hadec	k1gInPc2
a	a	k8xC
krystalických	krystalický	k2eAgInPc2d1
vápenců	vápenec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
podmiňují	podmiňovat	k5eAaImIp3nP
vznik	vznik	k1gInSc4
specifických	specifický	k2eAgFnPc2d1
půd	půda	k1gFnPc2
a	a	k8xC
jim	on	k3xPp3gMnPc3
příslušejících	příslušející	k2eAgNnPc2d1
rostlinných	rostlinný	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
oblasti	oblast	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
úseku	úsek	k1gInSc6
řeka	řeka	k1gFnSc1
Vltava	Vltava	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
kaňon	kaňon	k1gInSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
asi	asi	k9
400	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osu	osa	k1gFnSc4
území	území	k1gNnPc2
tvoří	tvořit	k5eAaImIp3nS
Křemžský	Křemžský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
protékající	protékající	k2eAgInSc1d1
širokou	široký	k2eAgFnSc7d1
kotlinou	kotlina	k1gFnSc7
a	a	k8xC
vlévající	vlévající	k2eAgFnSc1d1
se	se	k3xPyFc4
do	do	k7c2
Vltavy	Vltava	k1gFnSc2
pod	pod	k7c7
zříceninou	zřícenina	k1gFnSc7
hradu	hrad	k1gInSc2
Dívčí	dívčí	k2eAgFnSc1d1
kámen	kámen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Květena	květena	k1gFnSc1
a	a	k8xC
zvířena	zvířena	k1gFnSc1
</s>
<s>
Území	území	k1gNnSc1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
55	#num#	k4
%	%	kIx~
pokryto	pokrýt	k5eAaPmNgNnS
lesními	lesní	k2eAgInPc7d1
porosty	porost	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
jsou	být	k5eAaImIp3nP
svým	svůj	k3xOyFgNnSc7
složením	složení	k1gNnSc7
unikátním	unikátní	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
přirozených	přirozený	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původními	původní	k2eAgInPc7d1
rostlinnými	rostlinný	k2eAgInPc7d1
pokryvy	pokryv	k1gInPc7
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
byly	být	k5eAaImAgFnP
až	až	k9
na	na	k7c4
nepatrné	patrný	k2eNgFnPc4d1,k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
bučiny	bučina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
kolonizace	kolonizace	k1gFnSc2
byly	být	k5eAaImAgInP
částečně	částečně	k6eAd1
vytěženy	vytěžen	k2eAgInPc1d1
a	a	k8xC
v	v	k7c6
uplynulých	uplynulý	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
staletích	staletí	k1gNnPc6
nahrazeny	nahradit	k5eAaPmNgInP
kulturními	kulturní	k2eAgFnPc7d1
porosty	porost	k1gInPc7
smrku	smrk	k1gInSc2
a	a	k8xC
borovice	borovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
značné	značný	k2eAgFnSc6d1
části	část	k1gFnSc6
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
severních	severní	k2eAgInPc6d1
svazích	svah	k1gInPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
bučiny	bučina	k1gFnPc1
v	v	k7c6
původní	původní	k2eAgFnSc6d1
formě	forma	k1gFnSc6
zachovaly	zachovat	k5eAaPmAgInP
dodnes	dodnes	k6eAd1
a	a	k8xC
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
záměrně	záměrně	k6eAd1
pěstovány	pěstován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místy	místo	k1gNnPc7
se	se	k3xPyFc4
v	v	k7c6
bukových	bukový	k2eAgInPc6d1
porostech	porost	k1gInPc6
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
jedle	jedle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologicky	zoologicky	k6eAd1
a	a	k8xC
botanicky	botanicky	k6eAd1
zajímavé	zajímavý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
ostrůvkovitě	ostrůvkovitě	k6eAd1
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgInPc1d1
reliktní	reliktní	k2eAgInPc1d1
bory	bor	k1gInPc1
na	na	k7c6
hadcích	hadec	k1gInPc6
a	a	k8xC
při	při	k7c6
jižním	jižní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
oblasti	oblast	k1gFnSc2
čočky	čočka	k1gFnSc2
krystalických	krystalický	k2eAgInPc2d1
vápenců	vápenec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnPc4d3
lokality	lokalita	k1gFnPc4
těchto	tento	k3xDgNnPc2
vzácných	vzácný	k2eAgNnPc2d1
stanovišť	stanoviště	k1gNnPc2
jsou	být	k5eAaImIp3nP
zvlášť	zvlášť	k6eAd1
chráněny	chránit	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s>
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
bohatá	bohatý	k2eAgFnSc1d1
i	i	k9
na	na	k7c4
řadu	řada	k1gFnSc4
druhů	druh	k1gInPc2
bylin	bylina	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
výskyt	výskyt	k1gInSc1
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
podmíněn	podmínit	k5eAaPmNgInS
přítomností	přítomnost	k1gFnSc7
rozsáhlých	rozsáhlý	k2eAgInPc2d1
bukových	bukový	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejich	jejich	k3xOp3gInSc6
bohatém	bohatý	k2eAgInSc6d1
podrostu	podrost	k1gInSc6
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
mařinku	mařinka	k1gFnSc4
vonnou	vonný	k2eAgFnSc4d1
<g/>
,	,	kIx,
věsenku	věsenka	k1gFnSc4
nachovou	nachový	k2eAgFnSc4d1
<g/>
,	,	kIx,
pitulník	pitulník	k1gInSc4
žlutý	žlutý	k2eAgInSc4d1
<g/>
,	,	kIx,
lýkovec	lýkovec	k1gInSc4
jedovatý	jedovatý	k2eAgInSc4d1
<g/>
,	,	kIx,
vraní	vraní	k2eAgNnSc1d1
oko	oko	k1gNnSc1
čtyřlisté	čtyřlistý	k2eAgNnSc1d1
a	a	k8xC
mnohé	mnohý	k2eAgNnSc1d1
další	další	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Rostlinná	rostlinný	k2eAgNnPc1d1
a	a	k8xC
lesní	lesní	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
jsou	být	k5eAaImIp3nP
provázena	provázet	k5eAaImNgFnS
přirozenou	přirozený	k2eAgFnSc7d1
zvířenou	zvířena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejpozoruhodnější	pozoruhodný	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
skupiny	skupina	k1gFnPc4
hmyzu	hmyz	k1gInSc2
a	a	k8xC
měkkýšů	měkkýš	k1gMnPc2
vázaných	vázaný	k2eAgFnPc2d1
na	na	k7c4
vápencové	vápencový	k2eAgFnPc4d1
a	a	k8xC
hadcové	hadcový	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
lovné	lovný	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
je	být	k5eAaImIp3nS
nejhojnější	hojný	k2eAgFnSc1d3
srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
a	a	k8xC
prase	prase	k1gNnSc1
divoké	divoký	k2eAgNnSc1d1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
sem	sem	k6eAd1
šíří	šířit	k5eAaImIp3nS
i	i	k9
zvěř	zvěř	k1gFnSc1
jelení	jelení	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptáků	pták	k1gMnPc2
je	být	k5eAaImIp3nS
zastoupena	zastoupen	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
populace	populace	k1gFnSc1
holuba	holub	k1gMnSc2
doupňáka	doupňák	k1gMnSc2
<g/>
,	,	kIx,
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
jeřábek	jeřábek	k1gMnSc1
lesní	lesní	k2eAgFnSc2d1
<g/>
,	,	kIx,
lejsek	lejsek	k1gMnSc1
malý	malý	k2eAgMnSc1d1
i	i	k8xC
černohlavý	černohlavý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgFnSc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc6
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
deset	deset	k4xCc1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osm	osm	k4xCc1
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
a	a	k8xC
jedna	jeden	k4xCgFnSc1
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Chrášťanský	Chrášťanský	k2eAgInSc1d1
vrch	vrch	k1gInSc4
</s>
<s>
PR	pr	k0
Jaronínská	Jaronínský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
</s>
<s>
PR	pr	k0
Vysoká	vysoký	k2eAgFnSc1d1
Běta	Běta	k1gFnSc1
</s>
<s>
PR	pr	k0
Dobročkovské	Dobročkovský	k2eAgInPc4d1
hadce	hadec	k1gInSc2
</s>
<s>
PP	PP	kA
Na	na	k7c6
Stráži	stráž	k1gFnSc6
</s>
<s>
PP	PP	kA
Šimečkova	Šimečkův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
PR	pr	k0
Malá	malý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
PR	pr	k0
Ptačí	ptačí	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
</s>
<s>
PP	PP	kA
Provázková	provázkový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PP	PP	kA
Hejdlovský	Hejdlovský	k2eAgInSc4d1
potok	potok	k1gInSc4
</s>
<s>
PP	PP	kA
Mokřad	mokřad	k1gInSc1
u	u	k7c2
Borského	Borského	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
</s>
<s>
PR	pr	k0
Bořinka	bořinka	k1gFnSc1
</s>
<s>
PP	PP	kA
Horní	horní	k2eAgFnSc1d1
luka	luka	k1gNnPc1
</s>
<s>
PR	pr	k0
Kleť	Kleť	k1gFnSc1
</s>
<s>
PR	pr	k0
Holubovské	Holubovské	k2eAgFnSc1d1
hadce	hadec	k1gInSc2
</s>
<s>
PR	pr	k0
Dívčí	dívčí	k2eAgFnSc1d1
kámen	kámen	k1gInSc4
</s>
<s>
PP	PP	kA
Meandry	meandr	k1gInPc4
Chvalšinského	Chvalšinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
PP	PP	kA
Kalamandra	kalamandra	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Vyšenské	Vyšenský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
Správa	správa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
provozuje	provozovat	k5eAaImIp3nS
naučnou	naučný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Vyšenské	Vyšenský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stezka	stezka	k1gFnSc1
symbolicky	symbolicky	k6eAd1
začíná	začínat	k5eAaImIp3nS
před	před	k7c7
budovou	budova	k1gFnSc7
správy	správa	k1gFnSc2
CHKO	CHKO	kA
ve	v	k7c6
Vyšném	vyšný	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc1
větve	větev	k1gFnPc1
-	-	kIx~
jedna	jeden	k4xCgFnSc1
začíná	začínat	k5eAaImIp3nS
a	a	k8xC
končí	končit	k5eAaImIp3nS
ve	v	k7c6
Vyšném	vyšný	k2eAgNnSc6d1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
pak	pak	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
na	na	k7c4
vlakové	vlakový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
pro	pro	k7c4
pěší	pěší	k2eAgMnPc4d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
asi	asi	k9
2,5	2,5	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
14	#num#	k4
zastavení	zastavení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
vede	vést	k5eAaImIp3nS
po	po	k7c6
trase	trasa	k1gFnSc6
Třísov	Třísov	k1gInSc1
-	-	kIx~
Dívčí	dívčí	k2eAgInSc1d1
Kámen	kámen	k1gInSc1
-	-	kIx~
Holubov	Holubovo	k1gNnPc2
územím	území	k1gNnSc7
při	při	k7c6
ústí	ústí	k1gNnSc6
Křemžského	Křemžský	k2eAgInSc2d1
potoka	potok	k1gInSc2
do	do	k7c2
Vltavy	Vltava	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vedle	vedle	k7c2
přírodních	přírodní	k2eAgFnPc2d1
pozoruhodností	pozoruhodnost	k1gFnPc2
soustředily	soustředit	k5eAaPmAgFnP
už	už	k6eAd1
od	od	k7c2
pravěku	pravěk	k1gInSc2
i	i	k8xC
lidské	lidský	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Brložskem	Brložsek	k1gInSc7
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
Brlohu	brloh	k1gInSc6
a	a	k8xC
vede	vést	k5eAaImIp3nS
údolím	údolí	k1gNnSc7
Dobročkovského	Dobročkovský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
přes	přes	k7c4
Kuklov	Kuklov	k1gInSc4
a	a	k8xC
vrch	vrch	k1gInSc4
Stržíšek	Stržíšek	k1gInSc4
zpět	zpět	k6eAd1
do	do	k7c2
výchozího	výchozí	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
7,2	7,2	k4
km	km	kA
a	a	k8xC
má	mít	k5eAaImIp3nS
16	#num#	k4
zastavení	zastavení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Červený	červený	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
je	být	k5eAaImIp3nS
asi	asi	k9
tříkilometrový	tříkilometrový	k2eAgInSc4d1
okruh	okruh	k1gInSc4
zámeckým	zámecký	k2eAgInSc7d1
parkem	park	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
14	#num#	k4
zastavení	zastavení	k1gNnPc2
s	s	k7c7
informacemi	informace	k1gFnPc7
hlavně	hlavně	k9
o	o	k7c6
vzniku	vznik	k1gInSc6
a	a	k8xC
historii	historie	k1gFnSc6
krajinářského	krajinářský	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
kolem	kolem	k6eAd1
Kletě	kletě	k6eAd1
byla	být	k5eAaImAgFnS
jako	jako	k9
poslední	poslední	k2eAgFnSc1d1
nově	nově	k6eAd1
zřízena	zřízen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Kleť	Kleť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měří	měřit	k5eAaImIp3nS
5	#num#	k4
km	km	kA
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
zaměřené	zaměřený	k2eAgFnPc4d1
především	především	k6eAd1
na	na	k7c4
místní	místní	k2eAgFnSc4d1
přírodu	příroda	k1gFnSc4
a	a	k8xC
také	také	k9
historii	historie	k1gFnSc4
staveb	stavba	k1gFnPc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Kletě	kletě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Kromě	kromě	k7c2
přírodních	přírodní	k2eAgFnPc2d1
krás	krása	k1gFnPc2
je	být	k5eAaImIp3nS
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
bohatý	bohatý	k2eAgInSc1d1
i	i	k9
na	na	k7c4
historické	historický	k2eAgFnPc4d1
památky	památka	k1gFnPc4
a	a	k8xC
lidovou	lidový	k2eAgFnSc4d1
architekturu	architektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
patří	patřit	k5eAaImIp3nS
cisterciácký	cisterciácký	k2eAgInSc4d1
klášter	klášter	k1gInSc4
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
s	s	k7c7
kostelem	kostel	k1gInSc7
ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zřícenina	zřícenina	k1gFnSc1
rožmberského	rožmberský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Dívčí	dívčí	k2eAgInSc4d1
kámen	kámen	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
Brložsku	Brložska	k1gFnSc4
zbytky	zbytek	k1gInPc1
nedostavěného	dostavěný	k2eNgInSc2d1
pozdně	pozdně	k6eAd1
gotického	gotický	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
v	v	k7c6
Kuklově	Kuklův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
keltské	keltský	k2eAgNnSc1d1
oppidum	oppidum	k1gNnSc1
u	u	k7c2
Třísova	Třísův	k2eAgInSc2d1
(	(	kIx(
<g/>
Archeologické	archeologický	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
v	v	k7c6
regionu	region	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
památkami	památka	k1gFnPc7
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
vyniká	vynikat	k5eAaImIp3nS
obec	obec	k1gFnSc1
Holašovice	Holašovice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
197	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
zřízení	zřízení	k1gNnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sbírka	sbírka	k1gFnSc1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALBRECHT	Albrecht	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc6
ČR	ČR	kA
VIII	VIII	kA
<g/>
.	.	kIx.
-	-	kIx~
Českobudějovicko	Českobudějovicko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
807	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
473	#num#	k4
<g/>
–	–	k?
<g/>
508	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Blanský	blanský	k2eAgMnSc1d1
les	les	k1gInSc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Mikroregion	mikroregion	k1gInSc1
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
</s>
<s>
CHKO	CHKO	kA
Blanský	blanský	k2eAgInSc4d1
les	les	k1gInSc4
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
Křemežsko	Křemežsko	k6eAd1
a	a	k8xC
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Novohradské	novohradský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Brouskův	Brouskův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Červené	Červené	k2eAgNnSc4d1
blato	blato	k1gNnSc4
•	•	k?
Žofinka	Žofinka	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Hojná	hojný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
•	•	k?
Ruda	ruda	k1gFnSc1
•	•	k?
Terčino	Terčin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Dvořiště	dvořiště	k1gNnSc1
•	•	k?
Horusická	horusický	k2eAgNnPc4d1
blata	blata	k1gNnPc4
•	•	k?
Karvanice	Karvanice	k1gFnPc4
•	•	k?
Libochovka	Libochovka	k1gFnSc1
•	•	k?
Mokřiny	mokřina	k1gFnSc2
u	u	k7c2
Vomáčků	Vomáčka	k1gMnPc2
•	•	k?
Radomilická	Radomilický	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
•	•	k?
Ruda	ruda	k1gFnSc1
u	u	k7c2
Kojákovic	Kojákovice	k1gFnPc2
•	•	k?
V	v	k7c6
Rájích	ráj	k1gInPc6
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Kamýk	Kamýk	k1gInSc1
•	•	k?
Vrbenské	Vrbenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
Běta	Běta	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Baba	baba	k1gFnSc1
•	•	k?
Bedřichovský	Bedřichovský	k2eAgInSc4d1
potok	potok	k1gInSc4
•	•	k?
Blana	Blan	k1gInSc2
•	•	k?
Ďáblík	ďáblík	k1gMnSc1
•	•	k?
Děkanec	Děkanec	k1gMnSc1
•	•	k?
Hliníř	Hliníř	k1gFnSc1
•	•	k?
Hlubocké	hlubocký	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
•	•	k?
Kaliště	kaliště	k1gNnSc1
•	•	k?
Kameník	Kameník	k1gMnSc1
•	•	k?
Lhota	Lhota	k1gMnSc1
u	u	k7c2
Dynína	Dynín	k1gInSc2
•	•	k?
Libnič	Libnič	k1gMnSc1
•	•	k?
Lužnice	Lužnice	k1gFnSc1
•	•	k?
Ohrazení	ohrazení	k1gNnSc2
•	•	k?
Orty	ort	k1gInPc1
•	•	k?
Ostrolovský	Ostrolovský	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Pašínovická	Pašínovický	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Přesličkový	přesličkový	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Sokolí	sokolí	k2eAgNnSc4d1
hnízdo	hnízdo	k1gNnSc4
a	a	k8xC
bažantnice	bažantnice	k1gFnPc4
•	•	k?
Tůně	tůně	k1gFnPc4
u	u	k7c2
Špačků	Špaček	k1gMnPc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Karasín	Karasín	k1gInSc1
•	•	k?
Veveřský	Veveřský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Vltava	Vltava	k1gFnSc1
u	u	k7c2
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Vrbenská	Vrbenský	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
•	•	k?
Zámek	zámek	k1gInSc1
•	•	k?
Žemlička	žemlička	k1gFnSc1
•	•	k?
Židova	Židův	k2eAgFnSc1d1
strouha	strouha	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Národní	národní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Šumava	Šumava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc1d1
parky	park	k1gInPc1
</s>
<s>
Novohradské	novohradský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
•	•	k?
Poluška	Poluška	k1gFnSc1
•	•	k?
Soběnovská	Soběnovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Vyšebrodsko	Vyšebrodsko	k1gNnSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
stěna-Luč	stěna-Lučet	k5eAaPmRp2nS
•	•	k?
Vyšenské	Vyšenský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
•	•	k?
Žofínský	žofínský	k2eAgInSc1d1
prales	prales	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Olšina	olšina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bořinka	bořinka	k1gFnSc1
•	•	k?
Český	český	k2eAgInSc1d1
Jílovec	jílovec	k1gInSc1
•	•	k?
Dívčí	dívčí	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Holubovské	Holubovské	k2eAgInSc2d1
hadce	hadec	k1gInSc2
•	•	k?
Jaronínská	Jaronínský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Ježová	Ježová	k1gFnSc1
•	•	k?
Kleť	Kleť	k1gFnSc1
•	•	k?
Kozí	kozí	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kyselovský	Kyselovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Na	na	k7c6
Mokřinách	mokřina	k1gFnPc6
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
II	II	kA
•	•	k?
Olšov	Olšov	k1gInSc1
•	•	k?
Otov	Otov	k1gInSc1
•	•	k?
Otovský	Otovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Pivonické	Pivonický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Pláničský	Pláničský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Pod	pod	k7c7
Borkovou	Borková	k1gFnSc7
•	•	k?
Ptačí	ptačí	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
•	•	k?
Rapotická	Rapotický	k2eAgFnSc1d1
březina	březina	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Borková	Borková	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Kapličky	kaplička	k1gFnSc2
•	•	k?
Rožnov	Rožnov	k1gInSc1
•	•	k?
Světlá	světlat	k5eAaImIp3nS
•	•	k?
Ševcova	Ševcův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Besednické	besednický	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
•	•	k?
Cvičák	cvičák	k1gInSc1
•	•	k?
Házlův	Házlův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
•	•	k?
Hejdlovský	Hejdlovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
luka	luka	k1gNnPc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Malše	Malše	k1gFnSc1
•	•	k?
Jasánky	jasánek	k1gInPc4
•	•	k?
Kalamandra	kalamandra	k1gFnSc1
•	•	k?
Kotlina	kotlina	k1gFnSc1
pod	pod	k7c7
Pláničským	Pláničský	k2eAgInSc7d1
rybníkem	rybník	k1gInSc7
•	•	k?
Meandry	meandr	k1gInPc4
Chvalšinského	Chvalšinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Medvědí	medvědí	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
u	u	k7c2
Borského	Borského	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
•	•	k?
Muckovské	Muckovský	k2eAgInPc4d1
vápencové	vápencový	k2eAgInPc4d1
lomy	lom	k1gInPc4
•	•	k?
Multerberské	Multerberský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Stráži	stráž	k1gFnSc6
•	•	k?
Olšina	olšina	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Novolhotském	Novolhotský	k2eAgInSc6d1
lese	les	k1gInSc6
•	•	k?
Pestřice	Pestřice	k1gFnSc2
•	•	k?
Pohořské	Pohořský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Prameniště	prameniště	k1gNnSc2
Hamerského	hamerský	k2eAgInSc2d1
potoka	potok	k1gInSc2
u	u	k7c2
Zvonkové	zvonkový	k2eAgFnSc2d1
•	•	k?
Prameniště	prameniště	k1gNnSc4
Pohořského	Pohořský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Provázková	provázkový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Račínská	Račínský	k2eAgNnPc4d1
prameniště	prameniště	k1gNnSc4
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Bobovec	Bobovec	k1gMnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Kyselov	Kyselovo	k1gNnPc2
•	•	k?
Slavkovické	Slavkovický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
chlumek	chlumek	k1gInSc1
•	•	k?
Spáleniště	spáleniště	k1gNnSc2
•	•	k?
Stodůlecký	Stodůlecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kříž	Kříž	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
•	•	k?
Šimečkova	Šimečkův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
U	u	k7c2
tří	tři	k4xCgInPc2
můstků	můstek	k1gInPc2
•	•	k?
Uhlířský	uhlířský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Úval	úval	k1gInSc1
Zvonková	zvonkový	k2eAgFnSc1d1
•	•	k?
Velké	velký	k2eAgNnSc4d1
bahno	bahno	k1gNnSc4
•	•	k?
Vltava	Vltava	k1gFnSc1
u	u	k7c2
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Výří	výří	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Žestov	Žestov	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Prachatice	Prachatice	k1gFnPc1
Národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
</s>
<s>
Šumava	Šumava	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Boubínský	boubínský	k2eAgInSc1d1
prales	prales	k1gInSc1
•	•	k?
Velká	velký	k2eAgFnSc1d1
Niva	niva	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blanice	Blanice	k1gFnSc1
•	•	k?
Prameniště	prameniště	k1gNnSc1
Blanice	Blanice	k1gFnSc2
•	•	k?
U	u	k7c2
Hajnice	hajnice	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Amálino	Amálin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Čertova	čertův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Hadce	hadec	k1gInSc2
u	u	k7c2
Dobročkova	Dobročkův	k2eAgNnSc2d1
•	•	k?
Hliniště	hliniště	k1gNnSc2
•	•	k?
Hornovltavické	Hornovltavický	k2eAgFnSc2d1
pastviny	pastvina	k1gFnSc2
•	•	k?
Kralovické	Kralovický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Libín	Libín	k1gMnSc1
•	•	k?
Lipka	lipka	k1gFnSc1
I.	I.	kA
•	•	k?
Malý	malý	k2eAgInSc1d1
Polec	Polec	k1gInSc1
•	•	k?
Milešický	Milešický	k2eAgInSc1d1
prales	prales	k1gInSc1
•	•	k?
Miletínky	Miletínka	k1gFnSc2
•	•	k?
Mokrý	mokrý	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Na	na	k7c6
soutoku	soutok	k1gInSc6
•	•	k?
Nad	nad	k7c7
Zavírkou	zavírka	k1gFnSc7
•	•	k?
Najmanka	Najmanka	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Kořenského	Kořenský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Opolenec	Opolenec	k1gMnSc1
•	•	k?
Pod	pod	k7c7
Farským	farský	k2eAgInSc7d1
lesem	les	k1gInSc7
•	•	k?
Pod	pod	k7c7
Popelní	popelný	k2eAgMnPc1d1
horou	hora	k1gFnSc7
•	•	k?
Pravětínská	Pravětínský	k2eAgFnSc1d1
Lada	Lada	k1gFnSc1
•	•	k?
Radost	radost	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
u	u	k7c2
Martinala	Martinala	k1gFnSc2
•	•	k?
Saladínská	Saladínský	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
•	•	k?
Zátoňská	Zátoňský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Zátoňská	Zátoňský	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blanice	Blanice	k1gFnSc1
•	•	k?
Buková	bukový	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Čistá	čistý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Dobročkovské	Dobročkovský	k2eAgInPc1d1
hadce	hadec	k1gInPc1
•	•	k?
Háje	háj	k1gInSc2
•	•	k?
Hrádeček	hrádeček	k1gInSc1
•	•	k?
Irův	Irův	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
•	•	k?
Jaroškov	Jaroškov	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Jezerní	jezerní	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Jezerní	jezerní	k2eAgFnSc4d1
slať	slať	k1gFnSc4
•	•	k?
Jilmová	jilmový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Kotlina	kotlina	k1gFnSc1
Valné	valný	k2eAgFnSc2d1
•	•	k?
Koubovský	Koubovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Lipka	lipka	k1gFnSc1
•	•	k?
Mařský	Mařský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Mastnice	mastnice	k1gFnSc2
•	•	k?
Onšovice	Onšovice	k1gFnSc1
–	–	k?
Mlýny	mlýn	k1gInPc1
•	•	k?
Pančice	Pančice	k1gFnSc2
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
řekách	řeka	k1gFnPc6
•	•	k?
Pasecká	Pasecký	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Ostrohem	Ostroh	k1gInSc7
•	•	k?
Pod	pod	k7c7
Ostrou	ostrý	k2eAgFnSc7d1
horou	hora	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Sviňovicemi	Sviňovice	k1gFnPc7
•	•	k?
Pod	pod	k7c7
Vyhlídkou	vyhlídka	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Vyhlídkou	vyhlídka	k1gFnSc7
II	II	kA
•	•	k?
Podhájí	Podhájí	k1gNnSc2
•	•	k?
Polední	polední	k2eAgFnSc1d1
•	•	k?
Polučí	Polučí	k1gFnSc1
•	•	k?
Poušť	poušť	k1gFnSc1
•	•	k?
Skalka	skalka	k1gFnSc1
•	•	k?
Stádla	Stádla	k1gFnPc2
•	•	k?
Štěrbů	Štěrba	k1gMnPc2
louka	louka	k1gFnSc1
•	•	k?
Tisy	Tisa	k1gFnSc2
u	u	k7c2
Chrobol	Chrobola	k1gFnPc2
•	•	k?
U	u	k7c2
Narovců	Narovec	k1gInPc2
•	•	k?
U	u	k7c2
Piláta	Pilát	k1gMnSc2
•	•	k?
U	u	k7c2
poustevníka	poustevník	k1gMnSc2
•	•	k?
Úbislav	Úbislav	k1gMnSc1
•	•	k?
Upolíny	upolín	k1gInPc1
•	•	k?
V	v	k7c6
polích	pole	k1gNnPc6
•	•	k?
Vraniště	Vraniště	k1gNnSc2
•	•	k?
Vyšný	vyšný	k2eAgInSc1d1
–	–	k?
Křišťanov	Křišťanov	k1gInSc1
•	•	k?
Zábrdská	Zábrdský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Žďárecká	žďárecký	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Žižkova	Žižkův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
