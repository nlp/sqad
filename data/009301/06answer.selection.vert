<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
svislé	svislý	k2eAgFnSc2d1	svislá
trikolóry	trikolóra	k1gFnSc2	trikolóra
složené	složený	k2eAgFnSc6d1	složená
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
<g/>
,	,	kIx,	,
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
červeného	červený	k2eAgInSc2d1	červený
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
