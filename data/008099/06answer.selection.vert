<s>
Citoslovce	citoslovce	k1gNnSc1	citoslovce
(	(	kIx(	(
<g/>
interjekce	interjekce	k1gFnSc1	interjekce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neohebný	ohebný	k2eNgInSc1d1	neohebný
slovní	slovní	k2eAgInSc1d1	slovní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
nálady	nálada	k1gFnPc4	nálada
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
vůli	vůle	k1gFnSc4	vůle
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
