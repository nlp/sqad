<s>
Citoslovce	citoslovce	k1gNnSc1	citoslovce
(	(	kIx(	(
<g/>
interjekce	interjekce	k1gFnSc1	interjekce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neohebný	ohebný	k2eNgInSc1d1	neohebný
slovní	slovní	k2eAgInSc1d1	slovní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
nálady	nálada	k1gFnPc4	nálada
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
vůli	vůle	k1gFnSc4	vůle
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
souvislost	souvislost	k1gFnSc4	souvislost
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Citoslovce	citoslovce	k1gNnPc1	citoslovce
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
na	na	k7c4	na
vyplnění	vyplnění	k1gNnSc4	vyplnění
prázdných	prázdný	k2eAgFnPc2d1	prázdná
částí	část	k1gFnPc2	část
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
čárkou	čárka	k1gFnSc7	čárka
nebo	nebo	k8xC	nebo
vykřičníkem	vykřičník	k1gInSc7	vykřičník
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
některá	některý	k3yIgFnSc1	některý
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
označují	označovat	k5eAaImIp3nP	označovat
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
slovesný	slovesný	k2eAgInSc4d1	slovesný
přísudek	přísudek	k1gInSc4	přísudek
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
citoslovce	citoslovce	k1gNnPc1	citoslovce
se	se	k3xPyFc4	se
čárkami	čárka	k1gFnPc7	čárka
neoddělují	oddělovat	k5eNaImIp3nP	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Citoslovcím	citoslovce	k1gNnPc3	citoslovce
jsou	být	k5eAaImIp3nP	být
sémanticky	sémanticky	k6eAd1	sémanticky
i	i	k9	i
syntaktickou	syntaktický	k2eAgFnSc7d1	syntaktická
funkcí	funkce	k1gFnSc7	funkce
blízké	blízký	k2eAgInPc1d1	blízký
ideofony	ideofon	k1gInPc1	ideofon
<g/>
,	,	kIx,	,
slovní	slovní	k2eAgInSc1d1	slovní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
onomatopoicky	onomatopoicky	k6eAd1	onomatopoicky
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
či	či	k8xC	či
způsob	způsob	k1gInSc1	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
"	"	kIx"	"
<g/>
zvukový	zvukový	k2eAgInSc4d1	zvukový
obraz	obraz	k1gInSc4	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
přísudku	přísudek	k1gInSc2	přísudek
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
příslovečného	příslovečný	k2eAgNnSc2d1	příslovečné
určení	určení	k1gNnSc2	určení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ideofony	ideofon	k1gInPc4	ideofon
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
např.	např.	kA	např.
japonština	japonština	k1gFnSc1	japonština
<g/>
,	,	kIx,	,
baskičtina	baskičtina	k1gFnSc1	baskičtina
či	či	k8xC	či
africké	africký	k2eAgInPc1d1	africký
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
slov	slovo	k1gNnPc2	slovo
lze	lze	k6eAd1	lze
jako	jako	k8xC	jako
ideofony	ideofon	k1gInPc1	ideofon
interpretovat	interpretovat	k5eAaBmF	interpretovat
např.	např.	kA	např.
slova	slovo	k1gNnPc4	slovo
cik	cik	k?	cik
cak	cak	k0	cak
<g/>
,	,	kIx,	,
halabala	halabala	k?	halabala
apod.	apod.	kA	apod.
Citoslovce	citoslovce	k1gNnSc1	citoslovce
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Citoslovce	citoslovce	k1gNnPc1	citoslovce
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
<g/>
:	:	kIx,	:
impulzivní	impulzivní	k2eAgMnSc1d1	impulzivní
-	-	kIx~	-
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
projevy	projev	k1gInPc1	projev
citových	citový	k2eAgNnPc2d1	citové
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
:	:	kIx,	:
ach	ach	k0	ach
<g/>
,	,	kIx,	,
ouvej	ouvej	k0	ouvej
<g/>
,	,	kIx,	,
fuj	fuj	k0	fuj
imperativní	imperativní	k2eAgFnPc4d1	imperativní
(	(	kIx(	(
<g/>
rozkazovací	rozkazovací	k2eAgFnPc4d1	rozkazovací
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vůli	vůle	k1gFnSc4	vůle
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
:	:	kIx,	:
haló	haló	k0	haló
<g/>
,	,	kIx,	,
hybaj	hybaj	k1gMnSc1	hybaj
objektivní	objektivní	k2eAgMnSc1d1	objektivní
(	(	kIx(	(
<g/>
onomatopoická	onomatopoický	k2eAgFnSc1d1	onomatopoická
<g/>
,	,	kIx,	,
zvukomalebná	zvukomalebný	k2eAgFnSc1d1	zvukomalebná
<g/>
)	)	kIx)	)
-	-	kIx~	-
převádějí	převádět	k5eAaImIp3nP	převádět
zvuky	zvuk	k1gInPc4	zvuk
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
do	do	k7c2	do
artikulované	artikulovaný	k2eAgFnSc2d1	artikulovaná
podoby	podoba	k1gFnSc2	podoba
-	-	kIx~	-
chacha	chacha	k1gFnSc1	chacha
<g/>
,	,	kIx,	,
haf	haf	k1gInSc1	haf
<g/>
,	,	kIx,	,
buch	buch	k1gInSc1	buch
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgNnPc2	tento
citoslovcí	citoslovce	k1gNnPc2	citoslovce
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
i	i	k9	i
slova	slovo	k1gNnPc4	slovo
jiná	jiný	k2eAgNnPc4d1	jiné
jako	jako	k8xC	jako
chechtat	chechtat	k5eAaImF	chechtat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
břinknout	břinknout	k5eAaPmF	břinknout
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
citoslovce	citoslovce	k1gNnPc1	citoslovce
na	na	k7c4	na
<g/>
:	:	kIx,	:
vlastní	vlastní	k2eAgFnSc1d1	vlastní
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
povahu	povaha	k1gFnSc4	povaha
citoslovcí	citoslovce	k1gNnPc2	citoslovce
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
nevlastní	vlastní	k2eNgFnSc2d1	nevlastní
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ustrnutím	ustrnutí	k1gNnSc7	ustrnutí
tvarů	tvar	k1gInPc2	tvar
jiných	jiný	k2eAgInPc2d1	jiný
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
panečku	panečku	k0	panečku
<g/>
,	,	kIx,	,
holenku	holenka	k1gFnSc4	holenka
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
pád	pád	k1gInSc4	pád
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běda	běda	k1gFnSc1	běda
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc4	pád
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hybaj	hybaj	k1gFnSc1	hybaj
(	(	kIx(	(
<g/>
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
)	)	kIx)	)
Onomatopoeia	Onomatopoeium	k1gNnSc2	Onomatopoeium
Pareidolie	Pareidolie	k1gFnSc2	Pareidolie
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85298	[number]	k4	85298
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
citoslovce	citoslovce	k1gNnSc2	citoslovce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
citoslovce	citoslovce	k1gNnSc2	citoslovce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
