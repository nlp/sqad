<p>
<s>
Strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
arabské	arabský	k2eAgFnSc2d1	arabská
obrody	obroda	k1gFnSc2	obroda
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ح	ح	k?	ح
ا	ا	k?	ا
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
v	v	k7c6	v
Damašku	Damašek	k1gInSc6	Damašek
jako	jako	k9	jako
arabské	arabský	k2eAgNnSc1d1	arabské
sekulární	sekulární	k2eAgNnSc1d1	sekulární
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
hnutí	hnutí	k1gNnSc1	hnutí
bojující	bojující	k2eAgNnSc1d1	bojující
proti	proti	k7c3	proti
koloniálním	koloniální	k2eAgFnPc3d1	koloniální
mocnostem	mocnost	k1gFnPc3	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
založil	založit	k5eAaPmAgMnS	založit
syrský	syrský	k2eAgMnSc1d1	syrský
křesťan	křesťan	k1gMnSc1	křesťan
Michel	Michel	k1gMnSc1	Michel
Aflak	Aflak	k1gMnSc1	Aflak
<g/>
.	.	kIx.	.
</s>
<s>
Baas	Baas	k6eAd1	Baas
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
panarabská	panarabský	k2eAgFnSc1d1	panarabská
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastává	zastávat	k5eAaImIp3nS	zastávat
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
pozici	pozice	k1gFnSc4	pozice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
invazi	invaze	k1gFnSc6	invaze
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
Bahrajnu	Bahrajn	k1gInSc6	Bahrajn
a	a	k8xC	a
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideologicky	ideologicky	k6eAd1	ideologicky
Baas	Baas	k1gInSc1	Baas
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
arabský	arabský	k2eAgInSc1d1	arabský
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
a	a	k8xC	a
panarabismus	panarabismus	k1gInSc1	panarabismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
z	z	k7c2	z
ideologických	ideologický	k2eAgInPc2d1	ideologický
důvodů	důvod	k1gInPc2	důvod
rozštěpila	rozštěpit	k5eAaPmAgFnS	rozštěpit
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
syrská	syrský	k2eAgFnSc1d1	Syrská
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
regionalisté	regionalista	k1gMnPc1	regionalista
neboli	neboli	k8xC	neboli
Qotri	Qotr	k1gMnPc1	Qotr
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
irácká	irácký	k2eAgFnSc1d1	irácká
větev	větev	k1gFnSc1	větev
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nacionalisté	nacionalista	k1gMnPc1	nacionalista
či	či	k8xC	či
Qawmi	Qaw	k1gFnPc7	Qaw
<g/>
,	,	kIx,	,
udržovala	udržovat	k5eAaImAgFnS	udržovat
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
centristické	centristický	k2eAgInPc1d1	centristický
postoje	postoj	k1gInPc1	postoj
a	a	k8xC	a
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
si	se	k3xPyFc3	se
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
a	a	k8xC	a
udržovaly	udržovat	k5eAaImAgFnP	udržovat
paralelní	paralelní	k2eAgFnPc1d1	paralelní
struktury	struktura	k1gFnPc1	struktura
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Baas	Baas	k6eAd1	Baas
používá	používat	k5eAaImIp3nS	používat
motto	motto	k1gNnSc1	motto
"	"	kIx"	"
<g/>
Jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
socialismus	socialismus	k1gInSc1	socialismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
německým	německý	k2eAgInSc7d1	německý
sociálně	sociálně	k6eAd1	sociálně
demokratickým	demokratický	k2eAgInSc7d1	demokratický
sloganem	slogan	k1gInSc7	slogan
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jednota	jednota	k1gFnSc1	jednota
<g/>
"	"	kIx"	"
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
sjednocení	sjednocení	k1gNnSc1	sjednocení
všech	všecek	k3xTgMnPc2	všecek
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
"	"	kIx"	"
<g/>
socialismem	socialismus	k1gInSc7	socialismus
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
myšlena	myšlen	k2eAgFnSc1d1	myšlena
idea	idea	k1gFnSc1	idea
arabského	arabský	k2eAgInSc2d1	arabský
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
marxismu	marxismus	k1gInSc2	marxismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ideologie	ideologie	k1gFnSc1	ideologie
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
filosofie	filosofie	k1gFnSc1	filosofie
===	===	k?	===
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
arabské	arabský	k2eAgNnSc4d1	arabské
nacionalistické	nacionalistický	k2eAgNnSc4d1	nacionalistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
evropskými	evropský	k2eAgMnPc7d1	evropský
mysliteli	myslitel	k1gMnPc7	myslitel
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
romantickým	romantický	k2eAgInSc7d1	romantický
nacionalismem	nacionalismus	k1gInSc7	nacionalismus
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
též	též	k9	též
francouzským	francouzský	k2eAgNnSc7d1	francouzské
levicovým	levicový	k2eAgNnSc7d1	levicové
hnutím	hnutí	k1gNnSc7	hnutí
pozitivistů	pozitivista	k1gMnPc2	pozitivista
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelé	zakladatel	k1gMnPc1	zakladatel
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
ideologií	ideologie	k1gFnSc7	ideologie
setkali	setkat	k5eAaPmAgMnP	setkat
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
studiích	studie	k1gFnPc6	studie
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Klíčový	klíčový	k2eAgInSc4d1	klíčový
vliv	vliv	k1gInSc4	vliv
měl	mít	k5eAaImAgMnS	mít
filosof	filosof	k1gMnSc1	filosof
Johann	Johann	k1gMnSc1	Johann
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Fichte	Ficht	k1gMnSc5	Ficht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
strany	strana	k1gFnSc2	strana
===	===	k?	===
</s>
</p>
<p>
<s>
Baas	Baas	k6eAd1	Baas
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
silně	silně	k6eAd1	silně
hierarchický	hierarchický	k2eAgInSc4d1	hierarchický
systém	systém	k1gInSc4	systém
stranických	stranický	k2eAgFnPc2d1	stranická
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
silných	silný	k2eAgFnPc2d1	silná
vládních	vládní	k2eAgFnPc2d1	vládní
represí	represe	k1gFnPc2	represe
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
divize	divize	k1gFnSc1	divize
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc1d1	regionální
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc1d1	národní
vedení	vedení	k1gNnSc1	vedení
atd.	atd.	kA	atd.
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
vést	vést	k5eAaImF	vést
straně	strana	k1gFnSc3	strana
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
například	například	k6eAd1	například
po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
invazi	invaze	k1gFnSc6	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
podle	podle	k7c2	podle
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sýrie	Sýrie	k1gFnSc1	Sýrie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
a	a	k8xC	a
nastolení	nastolení	k1gNnSc4	nastolení
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
strana	strana	k1gFnSc1	strana
Baas	Baas	k1gInSc4	Baas
populární	populární	k2eAgInSc1d1	populární
masovou	masový	k2eAgFnSc7d1	masová
organizací	organizace	k1gFnSc7	organizace
se	se	k3xPyFc4	se
zastoupením	zastoupení	k1gNnSc7	zastoupení
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Oslovovala	oslovovat	k5eAaImAgFnS	oslovovat
hlavně	hlavně	k9	hlavně
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
maloburžoazii	maloburžoazie	k1gFnSc4	maloburžoazie
a	a	k8xC	a
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
protivníkem	protivník	k1gMnSc7	protivník
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Syrská	syrský	k2eAgFnSc1d1	Syrská
sociálně	sociálně	k6eAd1	sociálně
nacionální	nacionální	k2eAgFnSc1d1	nacionální
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
chápe	chápat	k5eAaImIp3nS	chápat
syrský	syrský	k2eAgInSc4d1	syrský
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
panarabismus	panarabismus	k1gInSc4	panarabismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Syrská	syrský	k2eAgFnSc1d1	Syrská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
třídní	třídní	k2eAgInSc4d1	třídní
boj	boj	k1gInSc4	boj
a	a	k8xC	a
internacionalismus	internacionalismus	k1gInSc4	internacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
strany	strana	k1gFnPc1	strana
provozovaly	provozovat	k5eAaImAgFnP	provozovat
kromě	kromě	k7c2	kromě
parlamentních	parlamentní	k2eAgInPc2d1	parlamentní
soubojů	souboj	k1gInPc2	souboj
i	i	k9	i
násilné	násilný	k2eAgFnPc1d1	násilná
akce	akce	k1gFnPc1	akce
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
potýkání	potýkání	k1gNnSc6	potýkání
se	se	k3xPyFc4	se
s	s	k7c7	s
vnitrostranickými	vnitrostranický	k2eAgFnPc7d1	vnitrostranická
frakcemi	frakce	k1gFnPc7	frakce
nakonec	nakonec	k9	nakonec
Baas	Baas	k1gInSc4	Baas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
podpořila	podpořit	k5eAaPmAgFnS	podpořit
Násirovu	Násirův	k2eAgFnSc4d1	Násirova
vizi	vize	k1gFnSc4	vize
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
arabské	arabský	k2eAgFnSc2d1	arabská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
strana	strana	k1gFnSc1	strana
řešila	řešit	k5eAaImAgFnS	řešit
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Násirovy	Násirův	k2eAgFnSc2d1	Násirova
Arabské	arabský	k2eAgFnSc2d1	arabská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
či	či	k8xC	či
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
osamostatnila	osamostatnit	k5eAaPmAgNnP	osamostatnit
po	po	k7c4	po
vystoupení	vystoupení	k1gNnSc4	vystoupení
Sýrie	Sýrie	k1gFnSc2	Sýrie
ze	z	k7c2	z
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
provedla	provést	k5eAaPmAgFnS	provést
strana	strana	k1gFnSc1	strana
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
nastolila	nastolit	k5eAaPmAgFnS	nastolit
vládu	vláda	k1gFnSc4	vláda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
ostře	ostro	k6eAd1	ostro
na	na	k7c6	na
levici	levice	k1gFnSc6	levice
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
syrské	syrský	k2eAgFnSc2d1	Syrská
a	a	k8xC	a
irácké	irácký	k2eAgFnSc2d1	irácká
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
program	program	k1gInSc1	program
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
více	hodně	k6eAd2	hodně
vyvážil	vyvážit	k5eAaPmAgInS	vyvážit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc4d1	omezený
politický	politický	k2eAgInSc4d1	politický
pluralismus	pluralismus	k1gInSc4	pluralismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Baas	Baas	k1gInSc4	Baas
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
stranou	strana	k1gFnSc7	strana
Národní	národní	k2eAgFnSc2d1	národní
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Irák	Irák	k1gInSc1	Irák
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
straně	strana	k1gFnSc3	strana
Baas	Baasa	k1gFnPc2	Baasa
zpočátku	zpočátku	k6eAd1	zpočátku
nedařilo	dařit	k5eNaImAgNnS	dařit
nabýt	nabýt	k5eAaPmF	nabýt
významnějšího	významný	k2eAgNnSc2d2	významnější
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
dopad	dopad	k1gInSc1	dopad
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nicméně	nicméně	k8xC	nicméně
podařilo	podařit	k5eAaPmAgNnS	podařit
vybudovat	vybudovat	k5eAaPmF	vybudovat
silnou	silný	k2eAgFnSc4d1	silná
stranickou	stranický	k2eAgFnSc4d1	stranická
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
profilovat	profilovat	k5eAaImF	profilovat
silně	silně	k6eAd1	silně
antikomunisticky	antikomunisticky	k6eAd1	antikomunisticky
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
připojení	připojení	k1gNnSc4	připojení
Iráku	Irák	k1gInSc2	Irák
ke	k	k7c3	k
Sjednocené	sjednocený	k2eAgFnSc3d1	sjednocená
arabské	arabský	k2eAgFnSc3d1	arabská
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
plukovník	plukovník	k1gMnSc1	plukovník
Abd	Abd	k1gMnPc2	Abd
al-Karim	al-Karim	k1gMnSc1	al-Karim
Kásim	Kásim	k1gMnSc1	Kásim
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
potlačen	potlačen	k2eAgInSc4d1	potlačen
vliv	vliv	k1gInSc4	vliv
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
strana	strana	k1gFnSc1	strana
Baas	Baas	k1gInSc1	Baas
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
pokusili	pokusit	k5eAaPmAgMnP	pokusit
nacionalisté	nacionalista	k1gMnPc1	nacionalista
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
puč	puč	k1gInSc4	puč
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
armády	armáda	k1gFnSc2	armáda
uspěli	uspět	k5eAaPmAgMnP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
budoval	budovat	k5eAaImAgMnS	budovat
silný	silný	k2eAgInSc4d1	silný
kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
násilím	násilí	k1gNnSc7	násilí
páchaným	páchaný	k2eAgNnSc7d1	páchané
na	na	k7c4	na
příslušnících	příslušník	k1gMnPc6	příslušník
kurdské	kurdský	k2eAgMnPc4d1	kurdský
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původně	původně	k6eAd1	původně
proklamovanému	proklamovaný	k2eAgInSc3d1	proklamovaný
sekularismu	sekularismus	k1gInSc3	sekularismus
přešli	přejít	k5eAaPmAgMnP	přejít
iráčtí	irácký	k2eAgMnPc1d1	irácký
baasisté	baasista	k1gMnPc1	baasista
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
sunnitského	sunnitský	k2eAgInSc2d1	sunnitský
islamismu	islamismus	k1gInSc2	islamismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
Husajnova	Husajnův	k2eAgFnSc1d1	Husajnova
vláda	vláda	k1gFnSc1	vláda
několik	několik	k4yIc4	několik
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
irácko-íránskou	irácko-íránský	k2eAgFnSc4d1	irácko-íránská
válku	válka	k1gFnSc4	válka
nebo	nebo	k8xC	nebo
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
invaze	invaze	k1gFnSc1	invaze
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
konec	konec	k1gInSc4	konec
baasistického	baasistický	k2eAgInSc2d1	baasistický
režimu	režim	k1gInSc2	režim
i	i	k8xC	i
zákaz	zákaz	k1gInSc4	zákaz
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
strana	strana	k1gFnSc1	strana
kooperuje	kooperovat	k5eAaImIp3nS	kooperovat
s	s	k7c7	s
prosyrskými	prosyrský	k2eAgFnPc7d1	prosyrská
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Aliance	aliance	k1gFnSc1	aliance
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
:	:	kIx,	:
dříve	dříve	k6eAd2	dříve
proirácká	proirácký	k2eAgFnSc1d1	proirácký
Arabská	arabský	k2eAgFnSc1d1	arabská
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
fronta	fronta	k1gFnSc1	fronta
a	a	k8xC	a
prosyrská	prosyrský	k2eAgFnSc1d1	prosyrská
Sa	Sa	k1gFnSc1	Sa
<g/>
'	'	kIx"	'
<g/>
ika	ika	k?	ika
<g/>
.	.	kIx.	.
</s>
<s>
Pobočky	pobočka	k1gFnPc1	pobočka
Baas	Baasa	k1gFnPc2	Baasa
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
ideologickým	ideologický	k2eAgNnSc7d1	ideologické
zabarvením	zabarvení	k1gNnSc7	zabarvení
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Baas	Baasa	k1gFnPc2	Baasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
sýrijské	sýrijský	k2eAgFnSc2d1	sýrijský
strany	strana	k1gFnSc2	strana
<g/>
/	/	kIx~	/
<g/>
frakce	frakce	k1gFnSc1	frakce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
Webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
novin	novina	k1gFnPc2	novina
</s>
</p>
