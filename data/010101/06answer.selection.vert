<s>
Strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
arabské	arabský	k2eAgFnSc2d1	arabská
obrody	obroda	k1gFnSc2	obroda
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ح	ح	k?	ح
ا	ا	k?	ا
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
v	v	k7c6	v
Damašku	Damašek	k1gInSc6	Damašek
jako	jako	k9	jako
arabské	arabský	k2eAgNnSc1d1	arabské
sekulární	sekulární	k2eAgNnSc1d1	sekulární
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
hnutí	hnutí	k1gNnSc1	hnutí
bojující	bojující	k2eAgNnSc1d1	bojující
proti	proti	k7c3	proti
koloniálním	koloniální	k2eAgFnPc3d1	koloniální
mocnostem	mocnost	k1gFnPc3	mocnost
<g/>
.	.	kIx.	.
</s>
