<s>
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1974	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Mnichov	Mnichov	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
novinář	novinář	k1gMnSc1
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
člen	člen	k1gMnSc1
zastupitelstva	zastupitelstvo	k1gNnSc2
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c4
Labem-Stará	Labem-Starý	k2eAgNnPc4d1
Boleslav	Boleslav	k1gMnSc1
<g/>
;	;	kIx,
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
1974	#num#	k4
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
internetový	internetový	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
zpravodajského	zpravodajský	k2eAgInSc2d1
serveru	server	k1gInSc2
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
diskusích	diskuse	k1gFnPc6
používá	používat	k5eAaImIp3nS
přezdívku	přezdívka	k1gFnSc4
Tangero	Tangero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Za	za	k7c2
studií	studie	k1gFnPc2
religionistiky	religionistika	k1gFnSc2
se	se	k3xPyFc4
při	při	k7c6
cestách	cesta	k1gFnPc6
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
technologií	technologie	k1gFnSc7
GSM	GSM	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
devadesátých	devadesátý	k4xOgNnPc2
let	rok	k1gInPc2
začínala	začínat	k5eAaImAgFnS
nastupovat	nastupovat	k5eAaImF
i	i	k9
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
svých	svůj	k3xOyFgFnPc6
stránkách	stránka	k1gFnPc6
o	o	k7c6
náboženských	náboženský	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
začal	začít	k5eAaPmAgInS
psát	psát	k5eAaImF
také	také	k9
o	o	k7c6
telekomunikačních	telekomunikační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1996	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Filipem	Filip	k1gMnSc7
Streiblem	Streibl	k1gMnSc7
a	a	k8xC
Petrem	Petr	k1gMnSc7
Mitošinkou	Mitošinka	k1gFnSc7
založil	založit	k5eAaPmAgInS
zpravodajský	zpravodajský	k2eAgInSc1d1
server	server	k1gInSc1
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Server	server	k1gInSc1
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Zandl	Zandl	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
firmu	firma	k1gFnSc4
Medea	Mede	k1gInSc2
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgFnSc1d2
M.	M.	kA
<g/>
I.A.	I.A.	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xC,k8xS
analytik	analytik	k1gMnSc1
informačních	informační	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
ujal	ujmout	k5eAaPmAgMnS
se	se	k3xPyFc4
plně	plně	k6eAd1
postu	post	k1gInSc2
šéfredaktora	šéfredaktor	k1gMnSc2
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2001	#num#	k4
prodal	prodat	k5eAaPmAgInS
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
i	i	k9
s	s	k7c7
přidruženými	přidružený	k2eAgInPc7d1
servery	server	k1gInPc7
Technet	Techneta	k1gFnPc2
(	(	kIx(
<g/>
technické	technický	k2eAgNnSc1d1
zpravodajství	zpravodajství	k1gNnSc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
o	o	k7c6
počítačích	počítač	k1gInPc6
<g/>
)	)	kIx)
a	a	k8xC
BonusWeb	BonusWba	k1gFnPc2
(	(	kIx(
<g/>
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
počítačové	počítačový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
)	)	kIx)
či	či	k8xC
Pandora	Pandora	k1gFnSc1
(	(	kIx(
<g/>
e-mailové	e-mailové	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
)	)	kIx)
vydavatelství	vydavatelství	k1gNnSc1
MaFra	MaFro	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jej	on	k3xPp3gMnSc4
později	pozdě	k6eAd2
začlenilo	začlenit	k5eAaPmAgNnS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
portálu	portál	k1gInSc2
iDNES	iDNES	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zandl	Zandl	k1gMnSc1
firmu	firma	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
listopadu	listopad	k1gInSc2
1998	#num#	k4
stál	stát	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Ondřejem	Ondřej	k1gMnSc7
Neffem	Neff	k1gMnSc7
a	a	k8xC
Ivem	Ivo	k1gMnSc7
Lukačovičem	Lukačovič	k1gMnSc7
u	u	k7c2
zrodu	zrod	k1gInSc2
protestní	protestní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
Internet	Internet	k1gInSc1
proti	proti	k7c3
monopolu	monopol	k1gInSc3
(	(	kIx(
<g/>
známé	známý	k2eAgNnSc1d1
též	též	k9
jako	jako	k8xC,k8xS
Bojkot	bojkot	k1gInSc1
<g/>
)	)	kIx)
proti	proti	k7c3
zdražení	zdražení	k1gNnSc3
vytáčeného	vytáčený	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
k	k	k7c3
Internetu	Internet	k1gInSc3
následně	následně	k6eAd1
ze	z	k7c2
strany	strana	k1gFnSc2
tehdejšího	tehdejší	k2eAgInSc2d1
SPT	SPT	kA
Telecomu	Telecom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
velké	velký	k2eAgFnSc3d1
mediální	mediální	k2eAgFnSc3d1
odezvě	odezva	k1gFnSc3
a	a	k8xC
demonstraci	demonstrace	k1gFnSc6
dvou	dva	k4xCgInPc2
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
před	před	k7c7
sídlem	sídlo	k1gNnSc7
společnosti	společnost	k1gFnSc2
přistoupil	přistoupit	k5eAaPmAgInS
monopolní	monopolní	k2eAgInSc1d1
operátor	operátor	k1gInSc1
na	na	k7c6
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
tarif	tarif	k1gInSc1
Internet	Internet	k1gInSc1
99	#num#	k4
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
jeho	jeho	k3xOp3gFnSc2
modernizace	modernizace	k1gFnSc2
Internet	Internet	k1gInSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Paralelně	paralelně	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
bezdrátovým	bezdrátový	k2eAgNnSc7d1
sítím	sítí	k1gNnSc7
(	(	kIx(
<g/>
WiFi	WiFi	k1gNnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
telekomunikacím	telekomunikace	k1gFnPc3
vůbec	vůbec	k9
–	–	k?
provozuje	provozovat	k5eAaImIp3nS
oborový	oborový	k2eAgMnSc1d1
weblog	weblog	k1gMnSc1
Marigold	Marigold	k1gMnSc1
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
také	také	k9
dvě	dva	k4xCgFnPc4
další	další	k2eAgFnPc4d1
internetové	internetový	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
:	:	kIx,
Cinetik	Cinetik	k1gMnSc1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgMnSc1d1
se	se	k3xPyFc4
půjčováním	půjčování	k1gNnSc7
DVD	DVD	kA
po	po	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
poskytuje	poskytovat	k5eAaImIp3nS
transport	transport	k1gInSc1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
mezi	mezi	k7c7
Internetem	Internet	k1gInSc7
a	a	k8xC
sítěmi	síť	k1gFnPc7
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
také	také	k9
za	za	k7c7
internetovým	internetový	k2eAgInSc7d1
portálem	portál	k1gInSc7
Chronomag	Chronomaga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2006	#num#	k4
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
na	na	k7c6
projektu	projekt	k1gInSc6
internetové	internetový	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Stream	Stream	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
Okolo	okolo	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
na	na	k7c6
Internetu	Internet	k1gInSc6
na	na	k7c4
pokračování	pokračování	k1gNnSc4
zveřejňoval	zveřejňovat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
sci-fi	sci-fi	k1gFnSc4
povídku	povídka	k1gFnSc4
Flotila	flotila	k1gFnSc1
Země	země	k1gFnSc1
<g/>
;	;	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
později	pozdě	k6eAd2
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
knižní	knižní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
jako	jako	k8xC,k8xS
příloha	příloha	k1gFnSc1
časopisu	časopis	k1gInSc2
Pevnost	pevnost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
později	pozdě	k6eAd2
sepsal	sepsat	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
Koncernová	koncernový	k2eAgFnSc1d1
pětiletka	pětiletka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2011	#num#	k4
z	z	k7c2
demonstračních	demonstrační	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
vydána	vydat	k5eAaPmNgFnS
pouze	pouze	k6eAd1
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
2007	#num#	k4
na	na	k7c6
serveru	server	k1gInSc6
Bloguje	Bloguje	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
na	na	k7c4
pokračování	pokračování	k1gNnSc4
psal	psát	k5eAaImAgInS
Příběh	příběh	k1gInSc1
strýčka	strýček	k1gMnSc2
Martina	Martin	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
momentálně	momentálně	k6eAd1
upravuje	upravovat	k5eAaImIp3nS
do	do	k7c2
podoby	podoba	k1gFnSc2
tištěné	tištěný	k2eAgFnSc6d1
a	a	k8xC
elektronicky	elektronicky	k6eAd1
distribuované	distribuovaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
mu	on	k3xPp3gNnSc3
vyšla	vyjít	k5eAaPmAgFnS
kniha	kniha	k1gFnSc1
Apple	Apple	kA
<g/>
:	:	kIx,
cesta	cesta	k1gFnSc1
k	k	k7c3
mobilům	mobil	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
2008	#num#	k4
do	do	k7c2
června	červen	k1gInSc2
2012	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
šéfredaktor	šéfredaktor	k1gMnSc1
internetového	internetový	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
potřetí	potřetí	k4xO
ženatý	ženatý	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
bývalými	bývalý	k2eAgFnPc7d1
manželkami	manželka	k1gFnPc7
má	mít	k5eAaImIp3nS
celkem	celkem	k6eAd1
3	#num#	k4
dcery	dcera	k1gFnSc2
a	a	k8xC
se	s	k7c7
současnou	současný	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
syna	syn	k1gMnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgInS
za	za	k7c4
Piráty	pirát	k1gMnPc4
zvolen	zvolit	k5eAaPmNgInS
zastupitelem	zastupitel	k1gMnSc7
města	město	k1gNnSc2
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
–	–	k?
Stará	Stará	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
Bezdrátové	bezdrátový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
WiFi	WiF	k1gFnSc2
<g/>
:	:	kIx,
praktický	praktický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
204	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-7226-632-2	80-7226-632-2	k4
</s>
<s>
Flotila	flotila	k1gFnSc1
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Wolf	Wolf	k1gMnSc1
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
180	#num#	k4
s.	s.	k?
</s>
<s>
Koncernová	koncernový	k2eAgFnSc1d1
pětiletka	pětiletka	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
pouze	pouze	k6eAd1
elektronicky	elektronicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Apple	Apple	kA
<g/>
:	:	kIx,
cesta	cesta	k1gFnSc1
k	k	k7c3
mobilům	mobil	k1gInPc3
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978-80-204-2641-3	978-80-204-2641-3	k4
</s>
<s>
Husákův	Husákův	k2eAgMnSc1d1
děda	děda	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k6eAd1
2015	#num#	k4
<g/>
.	.	kIx.
168	#num#	k4
s.	s.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
<g/>
:	:	kIx,
Kniha	kniha	k1gFnSc1
Apple	Apple	kA
<g/>
:	:	kIx,
cesta	cesta	k1gFnSc1
k	k	k7c3
mobilům	mobil	k1gInPc3
<g/>
,	,	kIx,
Marigold	Marigold	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
Slížek	Slížka	k1gFnPc2
šéfredaktorem	šéfredaktor	k1gMnSc7
serveru	server	k1gInSc2
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Archivováno	archivován	k2eAgNnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
společnosti	společnost	k1gFnSc2
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
↑	↑	k?
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
<g/>
:	:	kIx,
Dva	dva	k4xCgMnPc1
takové	takový	k3xDgMnPc4
<g/>
…	…	k?
ze	z	k7c2
života	život	k1gInSc2
…	…	k?
<g/>
,	,	kIx,
Marigold	Marigold	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
„	„	k?
<g/>
Dnes	dnes	k6eAd1
dopoledne	dopoledne	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
s	s	k7c7
Markétou	Markéta	k1gFnSc7
na	na	k7c6
Brandýském	brandýský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
vzali	vzít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
,	,	kIx,
osobní	osobní	k2eAgInSc4d1
kanál	kanál	k1gInSc4
na	na	k7c6
Twitteru	Twitter	k1gInSc6
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
↑	↑	k?
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
Marigold	Marigold	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnSc7
0	#num#	k4
<g/>
5.10	5.10	k4
<g/>
.	.	kIx.
-	-	kIx~
0	#num#	k4
<g/>
6.10	6.10	k4
<g/>
.2018	.2018	k4
–	–	k?
zvolení	zvolený	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
dle	dle	k7c2
výsledku	výsledek	k1gInSc2
–	–	k?
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c4
Labem-Stará	Labem-Starý	k2eAgNnPc4d1
Boleslav	Boleslav	k1gMnSc1
na	na	k7c6
oficiálním	oficiální	k2eAgInSc6d1
volebním	volební	k2eAgInSc6d1
serveru	server	k1gInSc6
ČSÚ	ČSÚ	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
</s>
<s>
Marigold	Marigold	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
weblog	weblog	k1gMnSc1
Marigold	Marigold	k1gMnSc1
</s>
<s>
Marigold	Marigold	k1gInSc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
českého	český	k2eAgInSc2d1
internetu	internet	k1gInSc2
–	–	k?
Výběr	výběr	k1gInSc1
článků	článek	k1gInPc2
ze	z	k7c2
Zandlova	Zandlův	k2eAgInSc2d1
weblogu	weblog	k1gInSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
obsahující	obsahující	k2eAgInSc4d1
i	i	k8xC
seriál	seriál	k1gInSc4
článků	článek	k1gInPc2
o	o	k7c6
založení	založení	k1gNnSc6
Mobil	mobil	k1gInSc4
serveru	server	k1gInSc2
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Patrickem	Patricko	k1gNnSc7
Zandlem	Zandlo	k1gNnSc7
<g/>
,	,	kIx,
autorem	autor	k1gMnSc7
knihy	kniha	k1gFnSc2
Flotila	flotila	k1gFnSc1
Země	země	k1gFnSc1
–	–	k?
rozhovor	rozhovor	k1gInSc4
na	na	k7c4
Sardenu	Sardena	k1gFnSc4
</s>
<s>
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
<g/>
:	:	kIx,
nejsem	být	k5eNaImIp1nS
zvyklý	zvyklý	k2eAgMnSc1d1
sedět	sedět	k5eAaImF
doma	doma	k6eAd1
–	–	k?
rozhovor	rozhovor	k1gInSc4
na	na	k7c6
Lupě	lupa	k1gFnSc6
</s>
<s>
Patrick	Patrick	k1gMnSc1
Zandl	Zandl	k1gMnSc1
<g/>
:	:	kIx,
Budoucnost	budoucnost	k1gFnSc1
je	být	k5eAaImIp3nS
složitá	složitý	k2eAgFnSc1d1
–	–	k?
rozhovor	rozhovor	k1gInSc4
na	na	k7c4
Interval	interval	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2003203300	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5778	#num#	k4
7570	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2016028410	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84562012	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2016028410	#num#	k4
</s>
