<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sr	Sr	k1gFnSc2	Sr
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Strontium	Strontium	k1gNnSc1	Strontium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
více	hodně	k6eAd2	hodně
podobá	podobat	k5eAaImIp3nS	podobat
vlastnostem	vlastnost	k1gFnPc3	vlastnost
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
černého	černý	k2eAgInSc2d1	černý
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
lepším	dobrý	k2eAgInPc3d2	lepší
vodičům	vodič	k1gInPc3	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
jako	jako	k8xS	jako
alkalické	alkalický	k2eAgInPc4d1	alkalický
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
reaktivita	reaktivita	k1gFnSc1	reaktivita
natolik	natolik	k6eAd1	natolik
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchováváno	uchovávat	k5eAaImNgNnS	uchovávat
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
petrolej	petrolej	k1gInSc1	petrolej
<g/>
,	,	kIx,	,
nafta	nafta	k1gFnSc1	nafta
<g/>
)	)	kIx)	)
s	s	k7c7	s
nimiž	jenž	k3xRgNnPc7	jenž
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
stroncia	stroncium	k1gNnSc2	stroncium
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
červeně	červeň	k1gFnSc2	červeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pouze	pouze	k6eAd1	pouze
strontnaté	strontnatý	k2eAgFnPc1d1	strontnatý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Sr	Sr	k1gFnSc1	Sr
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
superbáze	superbáza	k1gFnSc6	superbáza
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
stroncium	stroncium	k1gNnSc4	stroncium
stroncidový	stroncidový	k2eAgInSc1d1	stroncidový
anion	anion	k1gInSc1	anion
Sr	Sr	k1gFnSc1	Sr
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
takovéto	takovýto	k3xDgFnPc1	takovýto
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgNnPc4d3	nejsilnější
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
i	i	k8xC	i
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvou	vrstva	k1gFnSc7	vrstva
nažloutlého	nažloutlý	k2eAgInSc2d1	nažloutlý
oxidu	oxid	k1gInSc2	oxid
<g/>
,	,	kIx,	,
práškové	práškový	k2eAgNnSc1d1	práškové
stroncium	stroncium	k1gNnSc1	stroncium
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
schopno	schopen	k2eAgNnSc1d1	schopno
samovolného	samovolný	k2eAgNnSc2d1	samovolné
vznícení	vznícení	k1gNnSc2	vznícení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
Sr	Sr	k1gFnSc7	Sr
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
a	a	k8xC	a
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrH	srha	k1gFnPc2	srha
<g/>
2	[number]	k4	2
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
prvků	prvek	k1gInPc2	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
je	být	k5eAaImIp3nS	být
zásadotvorný	zásadotvorný	k2eAgInSc4d1	zásadotvorný
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c2	za
tvorby	tvorba	k1gFnSc2	tvorba
strontnatých	strontnatý	k2eAgFnPc2d1	strontnatý
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
rudy	ruda	k1gFnSc2	ruda
barya	baryum	k1gNnSc2	baryum
witheritu	witherit	k1gInSc2	witherit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Strontianu	Strontian	k1gInSc2	Strontian
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
olověných	olověný	k2eAgInPc2d1	olověný
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
Adairem	Adairma	k1gFnPc2	Adairma
Crawfordem	Crawford	k1gInSc7	Crawford
minerál	minerál	k1gInSc4	minerál
podobný	podobný	k2eAgInSc4d1	podobný
witheritu	witherit	k1gInSc2	witherit
-	-	kIx~	-
stroncianit	stroncianit	k1gInSc1	stroncianit
<g/>
.	.	kIx.	.
</s>
<s>
Klaproth	Klaproth	k1gInSc1	Klaproth
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neobjevenou	objevený	k2eNgFnSc4d1	neobjevená
zeminu	zemina	k1gFnSc4	zemina
-	-	kIx~	-
strontnatou	strontnatý	k2eAgFnSc4d1	strontnatý
zeminu	zemina	k1gFnSc4	zemina
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc4	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Thomas	Thomas	k1gMnSc1	Thomas
Charles	Charles	k1gMnSc1	Charles
Hope	Hope	k1gInSc4	Hope
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
baryum	baryum	k1gNnSc4	baryum
<g/>
,	,	kIx,	,
stroncium	stroncium	k1gNnSc4	stroncium
a	a	k8xC	a
vápník	vápník	k1gInSc4	vápník
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc4	stroncium
poprvé	poprvé	k6eAd1	poprvé
připravil	připravit	k5eAaPmAgMnS	připravit
sir	sir	k1gMnSc1	sir
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc4	Dav
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
připravil	připravit	k5eAaPmAgInS	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
slabě	slabě	k6eAd1	slabě
zvlhčeného	zvlhčený	k2eAgInSc2d1	zvlhčený
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgMnSc4d1	strontnatý
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
rtuťové	rtuťový	k2eAgFnSc2d1	rtuťová
katody	katoda	k1gFnSc2	katoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
stroncium	stroncium	k1gNnSc4	stroncium
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Sr	Sr	k1gFnSc2	Sr
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
se	se	k3xPyFc4	se
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,03	[number]	k4	0,03
<g/>
–	–	k?	–
<g/>
0,04	[number]	k4	0,04
%	%	kIx~	%
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
procentuální	procentuální	k2eAgInSc1d1	procentuální
obsah	obsah	k1gInSc1	obsah
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
384	[number]	k4	384
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
za	za	k7c4	za
baryum	baryum	k1gNnSc4	baryum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
8	[number]	k4	8
mg	mg	kA	mg
Sr	Sr	k1gFnSc2	Sr
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
stroncia	stroncium	k1gNnSc2	stroncium
přibližně	přibližně	k6eAd1	přibližně
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
minerály	minerál	k1gInPc7	minerál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stroncia	stroncium	k1gNnSc2	stroncium
jsou	být	k5eAaImIp3nP	být
celestin	celestin	k1gMnSc1	celestin
SrSO	SrSO	k1gMnSc1	SrSO
<g/>
4	[number]	k4	4
chemicky	chemicky	k6eAd1	chemicky
síran	síran	k1gInSc1	síran
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
a	a	k8xC	a
stroncianit	stroncianit	k1gInSc1	stroncianit
SrCO	SrCO	k1gFnSc1	SrCO
<g/>
3	[number]	k4	3
chemicky	chemicky	k6eAd1	chemicky
uhličitan	uhličitan	k1gInSc1	uhličitan
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
významným	významný	k2eAgFnPc3d1	významná
rudám	ruda	k1gFnPc3	ruda
stroncia	stroncium	k1gNnSc2	stroncium
patří	patřit	k5eAaImIp3nP	patřit
akuminit	akuminit	k5eAaPmF	akuminit
Sr	Sr	k1gFnSc1	Sr
<g/>
[	[	kIx(	[
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
fluorkapit	fluorkapit	k5eAaPmF	fluorkapit
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Sr	Sr	k1gMnSc1	Sr
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
a	a	k8xC	a
weloganit	weloganit	k1gInSc1	weloganit
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
Sr	Sr	k1gFnSc2	Sr
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Zr	Zr	k1gFnPc2	Zr
<g/>
(	(	kIx(	(
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
·	·	k?	·
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c6	o
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
čtyř	čtyři	k4xCgInPc2	čtyři
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
84	[number]	k4	84
<g/>
Sr	Sr	k1gFnPc2	Sr
(	(	kIx(	(
<g/>
0,56	[number]	k4	0,56
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
86	[number]	k4	86
<g/>
Sr	Sr	k1gFnPc2	Sr
(	(	kIx(	(
<g/>
9,86	[number]	k4	9,86
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
87	[number]	k4	87
<g/>
Sr	Sr	k1gFnPc2	Sr
(	(	kIx(	(
<g/>
7,0	[number]	k4	7,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
88	[number]	k4	88
<g/>
Sr	Sr	k1gFnPc2	Sr
(	(	kIx(	(
<g/>
82,58	[number]	k4	82,58
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
87	[number]	k4	87
<g/>
Sr	Sr	k1gFnPc2	Sr
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
beta	beta	k1gNnSc1	beta
rozpadem	rozpad	k1gInSc7	rozpad
izotopu	izotop	k1gInSc2	izotop
rubidia	rubidium	k1gNnSc2	rubidium
87	[number]	k4	87
<g/>
Rb	Rb	k1gFnPc2	Rb
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
radiogenní	radiogenní	k2eAgFnSc1d1	radiogenní
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
poměrů	poměr	k1gInPc2	poměr
množství	množství	k1gNnSc2	množství
izotopů	izotop	k1gInPc2	izotop
87	[number]	k4	87
<g/>
Sr	Sr	k1gFnPc2	Sr
<g/>
,	,	kIx,	,
86	[number]	k4	86
<g/>
Sr	Sr	k1gMnPc2	Sr
a	a	k8xC	a
87	[number]	k4	87
<g/>
Rb	Rb	k1gFnPc2	Rb
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
odhadnout	odhadnout	k5eAaPmF	odhadnout
i	i	k9	i
stáří	stáří	k1gNnSc4	stáří
Vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
při	při	k7c6	při
jaderných	jaderný	k2eAgInPc6d1	jaderný
rozpadech	rozpad	k1gInPc6	rozpad
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
dalších	další	k2eAgInPc2d1	další
31	[number]	k4	31
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
izotopů	izotop	k1gInPc2	izotop
stroncia	stroncium	k1gNnSc2	stroncium
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
89	[number]	k4	89
<g/>
Sr	Sr	k1gFnPc6	Sr
a	a	k8xC	a
zejména	zejména	k9	zejména
90	[number]	k4	90
<g/>
Sr	Sr	k1gFnPc2	Sr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Stroncium	stroncium	k1gNnSc1	stroncium
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
hliníkem	hliník	k1gInSc7	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
SrO	SrO	k1gFnSc1	SrO
+	+	kIx~	+
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
→	→	k?	→
3	[number]	k4	3
Sr	Sr	k1gFnPc2	Sr
+	+	kIx~	+
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
Kovové	kovový	k2eAgNnSc1d1	kovové
stroncium	stroncium	k1gNnSc1	stroncium
lze	lze	k6eAd1	lze
také	také	k9	také
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
chloridu	chlorid	k1gInSc2	chlorid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
elementární	elementární	k2eAgInSc4d1	elementární
chlor	chlor	k1gInSc4	chlor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ihned	ihned	k6eAd1	ihned
dále	daleko	k6eAd2	daleko
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
elektrolýze	elektrolýza	k1gFnSc3	elektrolýza
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
grafitové	grafitový	k2eAgFnSc2d1	grafitová
anody	anoda	k1gFnSc2	anoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
chlor	chlor	k1gInSc1	chlor
a	a	k8xC	a
železné	železný	k2eAgFnPc1d1	železná
katody	katoda	k1gFnPc1	katoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
stroncium	stroncium	k1gNnSc1	stroncium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
přípravě	příprava	k1gFnSc3	příprava
stroncia	stroncium	k1gNnSc2	stroncium
lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
termický	termický	k2eAgInSc4d1	termický
rozklad	rozklad	k1gInSc4	rozklad
azidu	azid	k1gInSc2	azid
strontnatého	strontnatý	k2eAgMnSc2d1	strontnatý
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
stroncium	stroncium	k1gNnSc4	stroncium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Sloučenin	sloučenina	k1gFnPc2	sloučenina
stroncia	stroncium	k1gNnSc2	stroncium
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pyrotechnických	pyrotechnický	k2eAgInPc2d1	pyrotechnický
produktů	produkt	k1gInPc2	produkt
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
výraznou	výrazný	k2eAgFnSc4d1	výrazná
barevnou	barevný	k2eAgFnSc4d1	barevná
reakci	reakce	k1gFnSc4	reakce
v	v	k7c6	v
plameni	plamen	k1gInSc6	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
uplatnění	uplatnění	k1gNnPc1	uplatnění
mají	mít	k5eAaImIp3nP	mít
sloučeniny	sloučenina	k1gFnPc1	sloučenina
stroncia	stroncium	k1gNnSc2	stroncium
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
aplikacích	aplikace	k1gFnPc6	aplikace
sklářského	sklářský	k2eAgInSc2d1	sklářský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
katodové	katodový	k2eAgFnPc4d1	katodová
trubice	trubice	k1gFnPc4	trubice
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
obrazovek	obrazovka	k1gFnPc2	obrazovka
barevných	barevný	k2eAgInPc2d1	barevný
televizních	televizní	k2eAgInPc2d1	televizní
přijímačů	přijímač	k1gInPc2	přijímač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysokého	vysoký	k2eAgInSc2d1	vysoký
indexu	index	k1gInSc2	index
odrazivosti	odrazivost	k1gFnSc2	odrazivost
titaničitanu	titaničitan	k1gInSc2	titaničitan
strontnatého	strontnatý	k2eAgMnSc2d1	strontnatý
Sr	Sr	k1gMnSc2	Sr
<g/>
2	[number]	k4	2
<g/>
TiO	TiO	k1gFnSc2	TiO
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
optických	optický	k2eAgFnPc6d1	optická
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
měření	měření	k1gNnSc4	měření
barevnosti	barevnost	k1gFnSc2	barevnost
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
analýze	analýza	k1gFnSc3	analýza
spekter	spektrum	k1gNnPc2	spektrum
odražených	odražený	k2eAgInPc2d1	odražený
paprsků	paprsek	k1gInPc2	paprsek
z	z	k7c2	z
barevných	barevný	k2eAgInPc2d1	barevný
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
šperkařský	šperkařský	k2eAgInSc1d1	šperkařský
průmysl	průmysl	k1gInSc1	průmysl
titaničitan	titaničitan	k1gInSc1	titaničitan
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
jako	jako	k8xS	jako
levnější	levný	k2eAgInSc1d2	levnější
náhradu	náhrada	k1gFnSc4	náhrada
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některých	některý	k3yIgFnPc2	některý
strontnatých	strontnatý	k2eAgFnPc2d1	strontnatý
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dusičnanu	dusičnan	k1gInSc6	dusičnan
strontnatého	strontnatý	k2eAgNnSc2d1	strontnatý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
plamene	plamen	k1gInSc2	plamen
na	na	k7c4	na
červeno	červeno	k1gNnSc4	červeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrCO	SrCO	k1gFnSc7	SrCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
využitím	využití	k1gNnSc7	využití
Sr	Sr	k1gFnSc2	Sr
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barevných	barevný	k2eAgFnPc2d1	barevná
televizních	televizní	k2eAgFnPc2d1	televizní
obrazovek	obrazovka	k1gFnPc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodný	vhodný	k2eAgInSc4d1	vhodný
na	na	k7c4	na
odcukerňování	odcukerňování	k1gNnSc4	odcukerňování
melasy	melasa	k1gFnSc2	melasa
v	v	k7c6	v
pivovarech	pivovar	k1gInPc6	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
jiné	jiný	k2eAgFnPc1d1	jiná
strontnaté	strontnatý	k2eAgFnPc1d1	strontnatý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dusičnan	dusičnan	k1gInSc1	dusičnan
strontnatý	strontnatý	k2eAgMnSc1d1	strontnatý
Sr	Sr	k1gMnSc1	Sr
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
umělých	umělý	k2eAgInPc2d1	umělý
radioizotopů	radioizotop	k1gInPc2	radioizotop
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
stroncium-	stroncium-	k?	stroncium-
<g/>
89	[number]	k4	89
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
zejména	zejména	k9	zejména
stroncium-	stroncium-	k?	stroncium-
<g/>
90	[number]	k4	90
jako	jako	k8xS	jako
výkonný	výkonný	k2eAgInSc1d1	výkonný
zářič	zářič	k1gInSc1	zářič
v	v	k7c6	v
radioizotopových	radioizotopový	k2eAgInPc6d1	radioizotopový
termoelektrických	termoelektrický	k2eAgInPc6d1	termoelektrický
generátorech	generátor	k1gInPc6	generátor
(	(	kIx(	(
<g/>
RTG	RTG	kA	RTG
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrH	srha	k1gFnPc2	srha
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silné	silný	k2eAgNnSc1d1	silné
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
hydridu	hydrid	k1gInSc2	hydrid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
hydroxid	hydroxid	k1gInSc1	hydroxid
strontnatý	strontnatý	k2eAgMnSc1d1	strontnatý
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejsnáze	snadno	k6eAd3	snadno
se	se	k3xPyFc4	se
hydrid	hydrid	k1gInSc1	hydrid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
připraví	připravit	k5eAaPmIp3nS	připravit
reakcí	reakce	k1gFnSc7	reakce
zahřátého	zahřátý	k2eAgNnSc2d1	zahřáté
stroncia	stroncium	k1gNnSc2	stroncium
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
často	často	k6eAd1	často
stroncium	stroncium	k1gNnSc1	stroncium
ve	v	k7c6	v
vodíku	vodík	k1gInSc6	vodík
začne	začít	k5eAaPmIp3nS	začít
hořet	hořet	k5eAaImF	hořet
<g/>
.	.	kIx.	.
<g/>
Oxid	oxid	k1gInSc1	oxid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrO	SrO	k1gFnSc4	SrO
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
amorfní	amorfní	k2eAgFnSc1d1	amorfní
<g/>
,	,	kIx,	,
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
strontnatý	strontnatý	k2eAgMnSc1d1	strontnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
hořením	hoření	k1gNnSc7	hoření
stroncia	stroncium	k1gNnSc2	stroncium
v	v	k7c6	v
kyslíkové	kyslíkový	k2eAgFnSc6d1	kyslíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejčastěji	často	k6eAd3	často
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
<g/>
.	.	kIx.	.
<g/>
Hydroxid	hydroxid	k1gInSc1	hydroxid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
Sr	Sr	k1gMnSc7	Sr
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
beztvarý	beztvarý	k2eAgInSc1d1	beztvarý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lépe	dobře	k6eAd2	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
než	než	k8xS	než
hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
(	(	kIx(	(
<g/>
0,7	[number]	k4	0,7
gramu	gram	k1gInSc2	gram
ve	v	k7c4	v
100	[number]	k4	100
ml	ml	kA	ml
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
středně	středně	k6eAd1	středně
silná	silný	k2eAgFnSc1d1	silná
zásada	zásada	k1gFnSc1	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
strontnatého	strontnatý	k2eAgMnSc4d1	strontnatý
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
stroncia	stroncium	k1gNnSc2	stroncium
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
<g/>
Peroxid	peroxid	k1gInSc1	peroxid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrO	SrO	k1gFnSc2	SrO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
strontnatým	strontnatý	k2eAgInSc7d1	strontnatý
<g/>
.	.	kIx.	.
<g/>
Sulfid	sulfid	k1gInSc1	sulfid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrS	SrS	k1gFnSc4	SrS
</s>
</p>
<p>
<s>
====	====	k?	====
Soli	sůl	k1gFnSc6	sůl
====	====	k?	====
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
strontnatých	strontnatý	k2eAgFnPc2d1	strontnatý
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpuští	rozpuštit	k5eAaPmIp3nS	rozpuštit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
hůře	zle	k6eAd2	zle
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
soli	sůl	k1gFnPc1	sůl
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgMnSc1d1	barevný
(	(	kIx(	(
<g/>
manganistany	manganistan	k1gInPc1	manganistan
<g/>
,	,	kIx,	,
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strontnaté	strontnatý	k2eAgFnPc1d1	strontnatý
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
lépe	dobře	k6eAd2	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
než	než	k8xS	než
soli	sůl	k1gFnPc1	sůl
hořečnaté	hořečnatý	k2eAgFnPc1d1	hořečnatá
a	a	k8xC	a
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
<g/>
.	.	kIx.	.
</s>
<s>
Strontnaté	strontnatý	k2eAgFnPc4d1	strontnatý
soli	sůl	k1gFnPc4	sůl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
komplexy	komplex	k1gInPc1	komplex
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
stroncium	stroncium	k1gNnSc4	stroncium
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
typické	typický	k2eAgFnPc1d1	typická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrF	SrF	k1gFnSc7	SrF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
strontnatých	strontnatý	k2eAgMnPc2d1	strontnatý
solí	solit	k5eAaImIp3nP	solit
fluoridovými	fluoridový	k2eAgInPc7d1	fluoridový
anionty	anion	k1gInPc7	anion
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
či	či	k8xC	či
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgMnSc4d1	strontnatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
fluorvodíkovou	fluorvodíková	k1gFnSc7	fluorvodíková
<g/>
.	.	kIx.	.
<g/>
Chlorid	chlorid	k1gInSc1	chlorid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrCl	SrCl	k1gInSc1	SrCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlororvodíkové	chlororvodíková	k1gFnSc2	chlororvodíková
<g/>
.	.	kIx.	.
<g/>
Bromid	bromid	k1gInSc4	bromid
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrBr	SrBr	k1gInSc4	SrBr
<g/>
2	[number]	k4	2
a	a	k8xC	a
jodid	jodid	k1gInSc4	jodid
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrI	SrI	k1gFnSc7	SrI
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
i	i	k8xC	i
bromid	bromid	k1gInSc4	bromid
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
popřípadě	popřípadě	k6eAd1	popřípadě
kyselině	kyselina	k1gFnSc6	kyselina
jodovodíkové	jodovodíkový	k2eAgFnPc1d1	jodovodíková
<g/>
.	.	kIx.	.
<g/>
Dusičnan	dusičnan	k1gInSc1	dusičnan
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
Sr	Sr	k1gMnSc7	Sr
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgMnSc4d1	strontnatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
<g/>
Uhličitan	uhličitan	k1gInSc4	uhličitan
strontnatý	strontnatý	k2eAgInSc4d1	strontnatý
SrCO	SrCO	k1gFnSc7	SrCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
roztok	roztok	k1gInSc1	roztok
reaguje	reagovat	k5eAaBmIp3nS	reagovat
zásaditě	zásaditě	k6eAd1	zásaditě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
stroncianit	stroncianit	k1gInSc1	stroncianit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
srážením	srážení	k1gNnSc7	srážení
strontnatých	strontnatý	k2eAgInPc2d1	strontnatý
iontů	ion	k1gInPc2	ion
uhličitanovými	uhličitanový	k2eAgInPc7d1	uhličitanový
anionty	anion	k1gInPc7	anion
<g/>
,	,	kIx,	,
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
strontnatého	strontnatý	k2eAgInSc2d1	strontnatý
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
obsahujícím	obsahující	k2eAgInSc7d1	obsahující
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
nebo	nebo	k8xC	nebo
pohlcením	pohlcení	k1gNnPc3	pohlcení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
hydroxidem	hydroxid	k1gInSc7	hydroxid
strontnatým	strontnatý	k2eAgInSc7d1	strontnatý
<g/>
.	.	kIx.	.
<g/>
Síran	síran	k1gInSc1	síran
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrSO	SrSO	k1gFnSc2	SrSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
špatně	špatně	k6eAd1	špatně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
se	se	k3xPyFc4	se
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
teplotou	teplota	k1gFnSc7	teplota
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
celestin	celestin	k1gInSc1	celestin
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
strontnatého	strontnatý	k2eAgMnSc4d1	strontnatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
stroncia	stroncium	k1gNnSc2	stroncium
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
strontnaté	strontnatý	k2eAgFnPc1d1	strontnatý
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
strontnaté	strontnatý	k2eAgInPc4d1	strontnatý
alkoholáty	alkoholát	k1gInPc4	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
strontnatým	strontnatý	k2eAgFnPc3d1	strontnatý
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
strontnatých	strontnatý	k2eAgFnPc2d1	strontnatý
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
aspekty	aspekt	k1gInPc1	aspekt
stroncia	stroncium	k1gNnSc2	stroncium
==	==	k?	==
</s>
</p>
<p>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
izotopy	izotop	k1gInPc1	izotop
stroncia	stroncium	k1gNnSc2	stroncium
se	se	k3xPyFc4	se
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
chovají	chovat	k5eAaImIp3nP	chovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
atomy	atom	k1gInPc1	atom
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
naprosto	naprosto	k6eAd1	naprosto
neškodné	škodný	k2eNgNnSc1d1	neškodné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
stronciem	stroncium	k1gNnSc7	stroncium
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
izotopem	izotop	k1gInSc7	izotop
90	[number]	k4	90
<g/>
Sr	Sr	k1gMnPc2	Sr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
radioaktivním	radioaktivní	k2eAgInSc6d1	radioaktivní
rozpadu	rozpad	k1gInSc6	rozpad
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
i	i	k9	i
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
90	[number]	k4	90
<g/>
Sr	Sr	k1gMnPc2	Sr
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
silný	silný	k2eAgInSc4d1	silný
beta	beta	k1gNnSc7	beta
zářič	zářič	k1gInSc4	zářič
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
29,1	[number]	k4	29,1
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
živého	živý	k2eAgInSc2d1	živý
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
zabudovat	zabudovat	k5eAaPmF	zabudovat
do	do	k7c2	do
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
potenciálním	potenciální	k2eAgInSc7d1	potenciální
zdrojem	zdroj	k1gInSc7	zdroj
vzniku	vznik	k1gInSc2	vznik
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
objektivním	objektivní	k2eAgNnSc6d1	objektivní
hodnocení	hodnocení	k1gNnSc6	hodnocení
jeho	jeho	k3xOp3gFnSc2	jeho
skutečné	skutečný	k2eAgFnSc2d1	skutečná
rizikovosti	rizikovost	k1gFnSc2	rizikovost
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
posoudit	posoudit	k5eAaPmF	posoudit
poměr	poměr	k1gInSc4	poměr
výskytu	výskyt	k1gInSc2	výskyt
uvedeného	uvedený	k2eAgInSc2d1	uvedený
izotopu	izotop	k1gInSc2	izotop
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
podobným	podobný	k2eAgInPc3d1	podobný
atomům	atom	k1gInPc3	atom
(	(	kIx(	(
<g/>
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
baryum	baryum	k1gNnSc1	baryum
<g/>
,	,	kIx,	,
neškodné	škodný	k2eNgInPc1d1	neškodný
izotopy	izotop	k1gInPc1	izotop
stroncia	stroncium	k1gNnSc2	stroncium
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vyzáření	vyzáření	k1gNnSc2	vyzáření
beta	beta	k1gNnSc2	beta
částice	částice	k1gFnSc1	částice
(	(	kIx(	(
<g/>
elektron	elektron	k1gInSc1	elektron
<g/>
)	)	kIx)	)
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
spuštěním	spuštění	k1gNnSc7	spuštění
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
právě	právě	k6eAd1	právě
sledovaným	sledovaný	k2eAgInSc7d1	sledovaný
izotopem	izotop	k1gInSc7	izotop
90	[number]	k4	90
<g/>
Sr	Sr	k1gFnPc2	Sr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stroncium	stroncium	k1gNnSc4	stroncium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
stroncium	stroncium	k1gNnSc4	stroncium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
