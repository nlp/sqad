<s>
Stroncium	stroncium	k1gNnSc1	stroncium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sr	Sr	k1gFnSc2	Sr
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Strontium	Strontium	k1gNnSc1	Strontium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
