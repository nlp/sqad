<p>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Dionysus	Dionysus	k1gMnSc1	Dionysus
<g/>
,	,	kIx,	,
Bacchus	Bacchus	k1gMnSc1	Bacchus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
synem	syn	k1gMnSc7	syn
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Semely	semel	k1gInPc4	semel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohem	bůh	k1gMnSc7	bůh
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
nespoutaného	spoutaný	k2eNgNnSc2d1	nespoutané
veselí	veselí	k1gNnSc2	veselí
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
i	i	k9	i
bohem	bůh	k1gMnSc7	bůh
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Thébách	Théby	k1gFnPc6	Théby
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
také	také	k9	také
Naxos	Naxos	k1gInSc1	Naxos
<g/>
,	,	kIx,	,
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
,	,	kIx,	,
Élis	Élis	k1gInSc1	Élis
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
místa	místo	k1gNnPc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
žárlivá	žárlivý	k2eAgFnSc1d1	žárlivá
manželka	manželka	k1gFnSc1	manželka
Héra	Héra	k1gFnSc1	Héra
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Zeus	Zeusa	k1gFnPc2	Zeusa
přivede	přivést	k5eAaPmIp3nS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dítě	dítě	k1gNnSc4	dítě
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
Semelé	Semelý	k2eAgInPc1d1	Semelý
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Thébského	thébský	k2eAgMnSc4d1	thébský
krále	král	k1gMnSc4	král
Kadma	Kadm	k1gMnSc4	Kadm
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
nemůže	moct	k5eNaImIp3nS	moct
nechat	nechat	k5eAaPmF	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Navštívila	navštívit	k5eAaPmAgFnS	navštívit
ve	v	k7c6	v
změněné	změněný	k2eAgFnSc6d1	změněná
podobě	podoba	k1gFnSc6	podoba
Semelu	semlít	k5eAaPmIp1nS	semlít
a	a	k8xC	a
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
ujistit	ujistit	k5eAaPmF	ujistit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
milenec	milenec	k1gMnSc1	milenec
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
ho	on	k3xPp3gMnSc4	on
požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
božské	božský	k2eAgFnSc6d1	božská
nádheře	nádhera	k1gFnSc6	nádhera
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jí	jíst	k5eAaImIp3nS	jíst
Zeus	Zeus	k1gInSc4	Zeus
předem	předem	k6eAd1	předem
slíbil	slíbit	k5eAaPmAgMnS	slíbit
splnit	splnit	k5eAaPmF	splnit
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
opravdu	opravdu	k6eAd1	opravdu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
parádou	paráda	k1gFnSc7	paráda
<g/>
,	,	kIx,	,
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
svými	svůj	k3xOyFgInPc7	svůj
blesky	blesk	k1gInPc7	blesk
a	a	k8xC	a
hromy	hrom	k1gInPc7	hrom
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
byly	být	k5eAaImAgInP	být
hrozné	hrozný	k2eAgInPc1d1	hrozný
<g/>
:	:	kIx,	:
Jeden	jeden	k4xCgInSc1	jeden
blesk	blesk	k1gInSc1	blesk
zapálil	zapálit	k5eAaPmAgInS	zapálit
Kadmův	Kadmův	k2eAgInSc1d1	Kadmův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
plameny	plamen	k1gInPc1	plamen
zasáhly	zasáhnout	k5eAaPmAgInP	zasáhnout
Semelu	semel	k1gInSc6	semel
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
předčasně	předčasně	k6eAd1	předčasně
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
.	.	kIx.	.
</s>
<s>
Pomoci	pomoct	k5eAaPmF	pomoct
jí	on	k3xPp3gFnSc3	on
už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Zeus	Zeus	k1gInSc1	Zeus
zachránil	zachránit	k5eAaPmAgInS	zachránit
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
vyrůst	vyrůst	k5eAaPmF	vyrůst
šlahouny	šlahoun	k1gInPc4	šlahoun
břečťanu	břečťan	k1gInSc2	břečťan
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gNnSc4	on
uchránily	uchránit	k5eAaPmAgFnP	uchránit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc4	dítě
nechal	nechat	k5eAaPmAgMnS	nechat
zašít	zašít	k5eAaPmF	zašít
do	do	k7c2	do
boku	bok	k1gInSc2	bok
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
ho	on	k3xPp3gInSc4	on
donosil	donosit	k5eAaPmAgInS	donosit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pravý	pravý	k2eAgInSc4d1	pravý
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
syn	syn	k1gMnSc1	syn
"	"	kIx"	"
<g/>
podruhé	podruhé	k6eAd1	podruhé
<g/>
"	"	kIx"	"
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posel	posel	k1gMnSc1	posel
bohů	bůh	k1gMnPc2	bůh
Hermés	Hermés	k1gInSc1	Hermés
předal	předat	k5eAaPmAgMnS	předat
chlapce	chlapec	k1gMnSc4	chlapec
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
Semelině	Semelina	k1gFnSc3	Semelina
sestře	sestra	k1gFnSc3	sestra
Ínó	Ínó	k1gFnSc3	Ínó
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
orchomenského	orchomenský	k2eAgMnSc4d1	orchomenský
krále	král	k1gMnSc4	král
Athamanta	Athamant	k1gMnSc4	Athamant
<g/>
.	.	kIx.	.
</s>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
dopátrala	dopátrat	k5eAaPmAgFnS	dopátrat
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
seslala	seslat	k5eAaPmAgFnS	seslat
na	na	k7c4	na
Athamanta	Athamant	k1gMnSc4	Athamant
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zabil	zabít	k5eAaPmAgMnS	zabít
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
Hermés	Hermés	k1gInSc1	Hermés
zachránil	zachránit	k5eAaPmAgInS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
vychovávaly	vychovávat	k5eAaImAgFnP	vychovávat
nymfy	nymfa	k1gFnPc1	nymfa
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
porostlé	porostlý	k2eAgNnSc4d1	porostlé
révou	réva	k1gFnSc7	réva
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
později	pozdě	k6eAd2	pozdě
přinesl	přinést	k5eAaPmAgInS	přinést
lidem	člověk	k1gMnPc3	člověk
první	první	k4xOgFnSc3	první
sazenici	sazenice	k1gFnSc3	sazenice
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
ji	on	k3xPp3gFnSc4	on
Íkariovi	Íkarius	k1gMnSc3	Íkarius
<g/>
,	,	kIx,	,
athénskému	athénský	k2eAgMnSc3d1	athénský
pastýři	pastýř	k1gMnSc3	pastýř
<g/>
,	,	kIx,	,
a	a	k8xC	a
naučil	naučit	k5eAaPmAgMnS	naučit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
vyrábět	vyrábět	k5eAaImF	vyrábět
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povaha	povaha	k1gFnSc1	povaha
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
Silénos	Silénos	k1gMnSc1	Silénos
-	-	kIx~	-
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
Satyrové	satyr	k1gMnPc1	satyr
a	a	k8xC	a
Mainady	mainada	k1gFnPc1	mainada
(	(	kIx(	(
<g/>
známější	známý	k2eAgFnPc1d2	známější
jako	jako	k8xC	jako
Bakchantky	bakchantka	k1gFnPc1	bakchantka
podle	podle	k7c2	podle
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
Bakchus	bakchus	k1gMnSc1	bakchus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
světem	svět	k1gInSc7	svět
-	-	kIx~	-
přes	přes	k7c4	přes
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Frýgii	Frýgie	k1gFnSc4	Frýgie
<g/>
,	,	kIx,	,
Thrákii	Thrákie	k1gFnSc4	Thrákie
<g/>
,	,	kIx,	,
Boiótii	Boiótie	k1gFnSc4	Boiótie
-	-	kIx~	-
tam	tam	k6eAd1	tam
všude	všude	k6eAd1	všude
šířil	šířit	k5eAaImAgMnS	šířit
bujné	bujný	k2eAgNnSc4d1	bujné
veselí	veselí	k1gNnSc4	veselí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgMnPc4	svůj
protivníky	protivník	k1gMnPc4	protivník
nešetřil	šetřit	k5eNaImAgInS	šetřit
<g/>
.	.	kIx.	.
</s>
<s>
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
opojný	opojný	k2eAgInSc4d1	opojný
nápoj	nápoj	k1gInSc4	nápoj
totiž	totiž	k9	totiž
lidé	člověk	k1gMnPc1	člověk
přijímali	přijímat	k5eAaImAgMnP	přijímat
se	s	k7c7	s
smíšenými	smíšený	k2eAgInPc7d1	smíšený
pocity	pocit	k1gInPc7	pocit
i	i	k8xC	i
s	s	k7c7	s
bázní	bázeň	k1gFnSc7	bázeň
<g/>
.	.	kIx.	.
</s>
<s>
Thrácký	thrácký	k2eAgMnSc1d1	thrácký
král	král	k1gMnSc1	král
Lykúrgos	Lykúrgos	k1gMnSc1	Lykúrgos
ho	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
vyhnat	vyhnat	k5eAaPmF	vyhnat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
slepotou	slepota	k1gFnSc7	slepota
<g/>
,	,	kIx,	,
pomatením	pomatení	k1gNnSc7	pomatení
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
thébský	thébský	k2eAgMnSc1d1	thébský
král	král	k1gMnSc1	král
Pentheus	Pentheus	k1gMnSc1	Pentheus
zahynul	zahynout	k5eAaPmAgMnS	zahynout
rozsápán	rozsápat	k5eAaPmNgInS	rozsápat
rozlícenými	rozlícený	k2eAgFnPc7d1	rozlícená
Bakchantkami	bakchantka	k1gFnPc7	bakchantka
<g/>
.	.	kIx.	.
</s>
<s>
Dcery	dcera	k1gFnPc1	dcera
orchomenského	orchomenský	k2eAgMnSc2d1	orchomenský
krále	král	k1gMnSc2	král
zešílely	zešílet	k5eAaPmAgFnP	zešílet
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
proměněny	proměněn	k2eAgFnPc1d1	proměněna
v	v	k7c4	v
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Špatně	špatně	k6eAd1	špatně
dopadli	dopadnout	k5eAaPmAgMnP	dopadnout
také	také	k9	také
piráti	pirát	k1gMnPc1	pirát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
lodi	loď	k1gFnSc6	loď
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
družinu	družina	k1gFnSc4	družina
převáželi	převážet	k5eAaImAgMnP	převážet
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
ho	on	k3xPp3gNnSc4	on
zajmout	zajmout	k5eAaPmF	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
vína	víno	k1gNnSc2	víno
nechal	nechat	k5eAaPmAgMnS	nechat
loď	loď	k1gFnSc4	loď
zarůst	zarůst	k5eAaPmF	zarůst
révou	réva	k1gFnSc7	réva
<g/>
,	,	kIx,	,
vesla	veslo	k1gNnPc4	veslo
proměnil	proměnit	k5eAaPmAgInS	proměnit
v	v	k7c4	v
hady	had	k1gMnPc4	had
a	a	k8xC	a
loď	loď	k1gFnSc4	loď
zahnal	zahnat	k5eAaPmAgInS	zahnat
na	na	k7c4	na
širé	širý	k2eAgNnSc4d1	širé
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
z	z	k7c2	z
pirátů	pirát	k1gMnPc2	pirát
udělal	udělat	k5eAaPmAgMnS	udělat
delfíny	delfín	k1gMnPc4	delfín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mezi	mezi	k7c7	mezi
olympskými	olympský	k2eAgMnPc7d1	olympský
bohy	bůh	k1gMnPc7	bůh
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězném	vítězný	k2eAgNnSc6d1	vítězné
tažení	tažení	k1gNnSc6	tažení
světem	svět	k1gInSc7	svět
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Naxu	Naxos	k1gInSc2	Naxos
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ariadnou	Ariadna	k1gFnSc7	Ariadna
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
krétského	krétský	k2eAgMnSc4d1	krétský
krále	král	k1gMnSc4	král
Mínóa	Mínóus	k1gMnSc4	Mínóus
(	(	kIx(	(
<g/>
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
tam	tam	k6eAd1	tam
zanechal	zanechat	k5eAaPmAgMnS	zanechat
athénský	athénský	k2eAgMnSc1d1	athénský
princ	princ	k1gMnSc1	princ
Théseus	Théseus	k1gMnSc1	Théseus
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
jí	on	k3xPp3gFnSc3	on
předtím	předtím	k6eAd1	předtím
sliboval	slibovat	k5eAaImAgMnS	slibovat
manželství	manželství	k1gNnSc4	manželství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
poté	poté	k6eAd1	poté
usedl	usednout	k5eAaPmAgMnS	usednout
po	po	k7c6	po
pravici	pravice	k1gFnSc6	pravice
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
na	na	k7c6	na
Olympu	Olymp	k1gInSc6	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
Semelé	Semelý	k2eAgFnSc2d1	Semelý
na	na	k7c4	na
Olymp	Olymp	k1gInSc4	Olymp
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jí	jíst	k5eAaImIp3nS	jíst
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
Thyóné	Thyóná	k1gFnSc2	Thyóná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zeus	Zeus	k6eAd1	Zeus
daroval	darovat	k5eAaPmAgMnS	darovat
Dionýsovi	Dionýsos	k1gMnSc3	Dionýsos
palác	palác	k1gInSc4	palác
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
stavem	stav	k1gInSc7	stav
věcí	věc	k1gFnPc2	věc
smířit	smířit	k5eAaPmF	smířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavnosti	slavnost	k1gFnSc3	slavnost
a	a	k8xC	a
uctívání	uctívání	k1gNnSc3	uctívání
==	==	k?	==
</s>
</p>
<p>
<s>
Dionýsova	Dionýsův	k2eAgNnSc2d1	Dionýsovo
tažení	tažení	k1gNnSc2	tažení
světem	svět	k1gInSc7	svět
byla	být	k5eAaImAgFnS	být
provázena	provázet	k5eAaImNgFnS	provázet
hlučným	hlučný	k2eAgNnSc7d1	hlučné
veselím	veselí	k1gNnSc7	veselí
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgFnS	hrát
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
divoká	divoký	k2eAgFnSc1d1	divoká
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Provázeli	provázet	k5eAaImAgMnP	provázet
ho	on	k3xPp3gMnSc4	on
nejen	nejen	k6eAd1	nejen
Siléni	Silén	k1gMnPc1	Silén
<g/>
,	,	kIx,	,
Satyrové	satyr	k1gMnPc1	satyr
a	a	k8xC	a
Bakchantky	bakchantka	k1gFnPc1	bakchantka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Kentauři	kentaur	k1gMnPc1	kentaur
<g/>
,	,	kIx,	,
nymfy	nymfa	k1gFnPc1	nymfa
i	i	k8xC	i
Múzy	Múza	k1gFnPc1	Múza
<g/>
.	.	kIx.	.
</s>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
dostával	dostávat	k5eAaImAgMnS	dostávat
stále	stále	k6eAd1	stále
nová	nový	k2eAgNnPc1d1	nové
jména	jméno	k1gNnPc1	jméno
-	-	kIx~	-
Bakchos	Bakchos	k1gInSc1	Bakchos
-	-	kIx~	-
třeštící	třeštící	k2eAgFnSc1d1	třeštící
<g/>
,	,	kIx,	,
Bromios	Bromios	k1gMnSc1	Bromios
-	-	kIx~	-
bouřící	bouřící	k2eAgMnSc1d1	bouřící
<g/>
,	,	kIx,	,
Lénios	Lénios	k1gMnSc1	Lénios
-	-	kIx~	-
ochránce	ochránce	k1gMnSc1	ochránce
lisu	lis	k1gInSc2	lis
a	a	k8xC	a
mnohá	mnohé	k1gNnPc1	mnohé
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
nebyl	být	k5eNaImAgMnS	být
jenom	jenom	k6eAd1	jenom
bohem	bůh	k1gMnSc7	bůh
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
bohem	bůh	k1gMnSc7	bůh
veškerého	veškerý	k3xTgInSc2	veškerý
růstu	růst	k1gInSc2	růst
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
básnické	básnický	k2eAgNnSc4d1	básnické
a	a	k8xC	a
věštecké	věštecký	k2eAgNnSc4d1	věštecké
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
,	,	kIx,	,
přinášel	přinášet	k5eAaImAgInS	přinášet
i	i	k9	i
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
nemocným	nemocný	k1gMnSc7	nemocný
zjevoval	zjevovat	k5eAaImAgMnS	zjevovat
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
léčebné	léčebný	k2eAgInPc4d1	léčebný
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poctě	pocta	k1gFnSc3	pocta
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
velkolepé	velkolepý	k2eAgFnPc1d1	velkolepá
bouřlivé	bouřlivý	k2eAgFnPc1d1	bouřlivá
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
ho	on	k3xPp3gMnSc4	on
ctili	ctít	k5eAaImAgMnP	ctít
jako	jako	k9	jako
ochránce	ochránce	k1gMnSc1	ochránce
ovocných	ovocný	k2eAgMnPc2d1	ovocný
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
uctívali	uctívat	k5eAaImAgMnP	uctívat
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
původce	původce	k1gMnSc4	původce
píle	píle	k1gFnSc2	píle
<g/>
,	,	kIx,	,
dovedností	dovednost	k1gFnPc2	dovednost
a	a	k8xC	a
pracovitosti	pracovitost	k1gFnSc2	pracovitost
v	v	k7c6	v
pěstování	pěstování	k1gNnSc6	pěstování
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Oceňovali	oceňovat	k5eAaImAgMnP	oceňovat
i	i	k8xC	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přinášel	přinášet	k5eAaImAgMnS	přinášet
radost	radost	k1gFnSc4	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
osvěžoval	osvěžovat	k5eAaImAgMnS	osvěžovat
ducha	duch	k1gMnSc4	duch
i	i	k9	i
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
uvolňoval	uvolňovat	k5eAaImAgMnS	uvolňovat
inspiraci	inspirace	k1gFnSc4	inspirace
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
ale	ale	k9	ale
jenom	jenom	k9	jenom
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
<g/>
-li	i	k?	-li
dodržena	dodržen	k2eAgFnSc1d1	dodržena
zásada	zásada	k1gFnSc1	zásada
"	"	kIx"	"
<g/>
ničeho	nic	k3yNnSc2	nic
příliš	příliš	k6eAd1	příliš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
však	však	k8xC	však
se	se	k3xPyFc4	se
slavnosti	slavnost	k1gFnPc1	slavnost
začaly	začít	k5eAaPmAgFnP	začít
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
bouřlivější	bouřlivý	k2eAgFnSc6d2	bouřlivější
<g/>
,	,	kIx,	,
účastníci	účastník	k1gMnPc1	účastník
upadali	upadat	k5eAaImAgMnP	upadat
mnohdy	mnohdy	k6eAd1	mnohdy
do	do	k7c2	do
extáze	extáze	k1gFnSc2	extáze
<g/>
,	,	kIx,	,
tažení	tažení	k1gNnSc4	tažení
byla	být	k5eAaImAgFnS	být
divoká	divoký	k2eAgFnSc1d1	divoká
a	a	k8xC	a
nespoutaná	spoutaný	k2eNgFnSc1d1	nespoutaná
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
slavnosti	slavnost	k1gFnPc1	slavnost
se	se	k3xPyFc4	se
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
konaly	konat	k5eAaImAgFnP	konat
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc1d1	velká
dionýsie	dionýsie	k1gFnPc1	dionýsie
<g/>
"	"	kIx"	"
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Končily	končit	k5eAaImAgFnP	končit
obvykle	obvykle	k6eAd1	obvykle
velkým	velký	k2eAgNnSc7d1	velké
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
představením	představení	k1gNnSc7	představení
se	s	k7c7	s
sborem	sbor	k1gInSc7	sbor
pěvců	pěvec	k1gMnPc2	pěvec
v	v	k7c6	v
kozlích	kozlí	k2eAgFnPc6d1	kozlí
kůžích	kůže	k1gFnPc6	kůže
a	a	k8xC	a
zpěvy	zpěv	k1gInPc1	zpěv
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ženy	žena	k1gFnPc1	žena
<g/>
/	/	kIx~	/
<g/>
Děti	dítě	k1gFnPc1	dítě
==	==	k?	==
</s>
</p>
<p>
<s>
Afrodíté	Afrodíta	k1gMnPc1	Afrodíta
</s>
</p>
<p>
<s>
Charitky	Charitka	k1gFnSc2	Charitka
(	(	kIx(	(
<g/>
možný	možný	k2eAgMnSc1d1	možný
otec	otec	k1gMnSc1	otec
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Diem	Die	k1gNnSc7	Die
<g/>
,	,	kIx,	,
Uránem	Uráno	k1gNnSc7	Uráno
a	a	k8xC	a
Héliem	hélium	k1gNnSc7	hélium
<g/>
)	)	kIx)	)
<g/>
Aglaia	Aglaium	k1gNnPc1	Aglaium
</s>
</p>
<p>
<s>
Eufrosyné	Eufrosyné	k2eAgFnSc1d1	Eufrosyné
</s>
</p>
<p>
<s>
Thaleia	Thaleia	k1gFnSc1	Thaleia
</s>
</p>
<p>
<s>
Hymén	Hymén	k1gInSc1	Hymén
</s>
</p>
<p>
<s>
Priápos	Priápos	k1gMnSc1	Priápos
</s>
</p>
<p>
<s>
Ariadna	Ariadna	k1gFnSc1	Ariadna
</s>
</p>
<p>
<s>
Oinopión	Oinopión	k1gMnSc1	Oinopión
</s>
</p>
<p>
<s>
Thoás	Thoás	k6eAd1	Thoás
</s>
</p>
<p>
<s>
Stafylos	Stafylos	k1gMnSc1	Stafylos
</s>
</p>
<p>
<s>
Peparethus	Peparethus	k1gMnSc1	Peparethus
</s>
</p>
<p>
<s>
Nyx	Nyx	k?	Nyx
</s>
</p>
<p>
<s>
Zélos	Zélos	k1gInSc1	Zélos
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Zelus	Zelus	k1gInSc1	Zelus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neznámá	známý	k2eNgFnSc1d1	neznámá
matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
Ákis	Ákis	k1gInSc1	Ákis
(	(	kIx(	(
<g/>
Acis	Acis	k1gInSc1	Acis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Althaia	Althaia	k1gFnSc1	Althaia
</s>
</p>
<p>
<s>
Déianeira	Déianeira	k1gMnSc1	Déianeira
(	(	kIx(	(
<g/>
spekulativní	spekulativní	k2eAgMnSc1d1	spekulativní
otec	otec	k1gMnSc1	otec
namísto	namísto	k7c2	namísto
Oinea	Oine	k1gInSc2	Oine
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kirké	Kirká	k1gFnPc1	Kirká
</s>
</p>
<p>
<s>
Komos	Komos	k1gMnSc1	Komos
(	(	kIx(	(
<g/>
Comus	Comus	k1gMnSc1	Comus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Jména	jméno	k1gNnPc4	jméno
vycházející	vycházející	k2eAgNnPc4d1	vycházející
ze	z	k7c2	z
základu	základ	k1gInSc2	základ
Dionysus	Dionysus	k1gInSc1	Dionysus
==	==	k?	==
</s>
</p>
<p>
<s>
Denise	Denisa	k1gFnSc3	Denisa
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnPc1	její
podoby	podoba	k1gFnPc1	podoba
<g/>
:	:	kIx,	:
Denice	denice	k1gFnSc1	denice
<g/>
,	,	kIx,	,
Daniesa	Daniesa	k1gFnSc1	Daniesa
<g/>
,	,	kIx,	,
Denese	Denese	k1gFnSc1	Denese
<g/>
,	,	kIx,	,
a	a	k8xC	a
Denisse	Denisse	k1gFnSc1	Denisse
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Denis	Denisa	k1gFnPc2	Denisa
nebo	nebo	k8xC	nebo
Dennis	Dennis	k1gFnPc2	Dennis
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obměny	obměna	k1gFnPc4	obměna
příjmení	příjmení	k1gNnPc2	příjmení
Denison	Denisona	k1gFnPc2	Denisona
a	a	k8xC	a
Dennison	Dennisona	k1gFnPc2	Dennisona
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Denny	Denna	k1gFnPc1	Denna
</s>
</p>
<p>
<s>
Nis	Nisa	k1gFnPc2	Nisa
(	(	kIx(	(
<g/>
norské	norský	k2eAgNnSc1d1	norské
příjmení	příjmení	k1gNnSc1	příjmení
Nissen	Nissna	k1gFnPc2	Nissna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nils	Nils	k1gInSc1	Nils
(	(	kIx(	(
<g/>
Nicholas	Nicholas	k1gMnSc1	Nicholas
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
základu	základ	k1gInSc2	základ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dion	Dion	k1gMnSc1	Dion
<g/>
,	,	kIx,	,
Deon	Deon	k1gMnSc1	Deon
<g/>
,	,	kIx,	,
Deion	Deion	k1gMnSc1	Deion
</s>
</p>
<p>
<s>
Dénes	Dénes	k1gInSc1	Dénes
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bacchus	Bacchus	k1gInSc1	Bacchus
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dionisio	Dionisio	k1gNnSc1	Dionisio
<g/>
,	,	kIx,	,
Dyonisio	Dyonisio	k1gNnSc1	Dyonisio
<g/>
,	,	kIx,	,
Dionigi	Dionigi	k1gNnSc1	Dionigi
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dionyssios	Dionyssios	k1gInSc1	Dionyssios
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
Δ	Δ	k?	Δ
řecky	řecky	k6eAd1	řecky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Denis	Denisa	k1gFnPc2	Denisa
<g/>
,	,	kIx,	,
Denisa	Denisa	k1gFnSc1	Denisa
<g/>
,	,	kIx,	,
Deniska	Deniska	k1gFnSc1	Deniska
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
slovansky	slovansky	k6eAd1	slovansky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Antičtí	antický	k2eAgMnPc1d1	antický
umělci	umělec	k1gMnPc1	umělec
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
buď	buď	k8xC	buď
jako	jako	k9	jako
vážného	vážné	k1gNnSc2	vážné
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
hustými	hustý	k2eAgInPc7d1	hustý
vlasy	vlas	k1gInPc7	vlas
a	a	k8xC	a
vousy	vous	k1gInPc7	vous
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
mladíka	mladík	k1gMnSc2	mladík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
soch	socha	k1gFnPc2	socha
je	být	k5eAaImIp3nS	být
Praxitelova	Praxitelův	k2eAgFnSc1d1	Praxitelova
socha	socha	k1gFnSc1	socha
Hermés	Hermés	k1gInSc1	Hermés
s	s	k7c7	s
Dionýsem	Dionýsos	k1gMnSc7	Dionýsos
(	(	kIx(	(
<g/>
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
340	[number]	k4	340
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Olympijském	olympijský	k2eAgNnSc6d1	Olympijské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc1	dítě
</s>
</p>
<p>
<s>
Vousatý	vousatý	k2eAgMnSc1d1	vousatý
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
římská	římský	k2eAgFnSc1d1	římská
kopie	kopie	k1gFnSc1	kopie
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Vatikánském	vatikánský	k2eAgNnSc6d1	Vatikánské
muzeu	muzeum	k1gNnSc6	muzeum
</s>
</p>
<p>
<s>
slavné	slavný	k2eAgFnPc1d1	slavná
jsou	být	k5eAaImIp3nP	být
sochy	socha	k1gFnPc1	socha
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
Kapitolský	kapitolský	k2eAgMnSc1d1	kapitolský
<g/>
,	,	kIx,	,
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
Vatikánský	vatikánský	k2eAgMnSc1d1	vatikánský
<g/>
,	,	kIx,	,
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Richelieův	Richelieův	k2eAgMnSc1d1	Richelieův
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Louvru	Louvre	k1gInSc6	Louvre
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
obrazů	obraz	k1gInPc2	obraz
jsou	být	k5eAaImIp3nP	být
slavné	slavný	k2eAgFnPc1d1	slavná
Dionýsova	Dionýsův	k2eAgFnSc1d1	Dionýsova
svatba	svatba	k1gFnSc1	svatba
s	s	k7c7	s
Ariadnou	Ariadna	k1gFnSc7	Ariadna
(	(	kIx(	(
<g/>
vázová	vázový	k2eAgFnSc1d1	vázový
malba	malba	k1gFnSc1	malba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
Kupido	Kupida	k1gFnSc5	Kupida
<g/>
,	,	kIx,	,
Bacchus	Bacchus	k1gMnSc1	Bacchus
a	a	k8xC	a
Pomona	Pomona	k1gFnSc1	Pomona
<g/>
,	,	kIx,	,
Godfried	Godfried	k1gInSc1	Godfried
Schalcken	Schalckna	k1gFnPc2	Schalckna
<g/>
,	,	kIx,	,
1643	[number]	k4	1643
<g/>
,	,	kIx,	,
Haag	Haag	k1gInSc1	Haag
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
Galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michelangelův	Michelangelův	k2eAgInSc1d1	Michelangelův
Bacchus	Bacchus	k1gInSc1	Bacchus
(	(	kIx(	(
<g/>
asi	asi	k9	asi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rubensův	Rubensův	k2eAgInSc1d1	Rubensův
Bacchus	Bacchus	k1gInSc1	Bacchus
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
petrohradské	petrohradský	k2eAgFnSc6d1	Petrohradská
Ermitáži	Ermitáž	k1gFnSc6	Ermitáž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velasquezovi	Velasquezův	k2eAgMnPc1d1	Velasquezův
Pijáci	piják	k1gMnPc1	piják
(	(	kIx(	(
<g/>
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1625	[number]	k4	1625
až	až	k6eAd1	až
1629	[number]	k4	1629
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
madridském	madridský	k2eAgInSc6d1	madridský
Pradu	Prad	k1gInSc6	Prad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
</s>
</p>
<p>
<s>
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Guus	Guus	k1gInSc1	Guus
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnSc2	antika
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dionysios	Dionysios	k1gMnSc1	Dionysios
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
(	(	kIx(	(
<g/>
stránky	stránka	k1gFnPc4	stránka
Antika	antika	k1gFnSc1	antika
<g/>
)	)	kIx)	)
</s>
</p>
