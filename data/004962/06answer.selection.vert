<s>
Ketština	Ketština	k1gFnSc1	Ketština
(	(	kIx(	(
<g/>
к	к	k?	к
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgMnSc1d1	poslední
živý	živý	k2eAgMnSc1d1	živý
představitel	představitel	k1gMnSc1	představitel
jenisejské	jenisejský	k2eAgFnSc2d1	jenisejský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
národem	národ	k1gInSc7	národ
Ketů	Ket	k1gInPc2	Ket
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dříve	dříve	k6eAd2	dříve
podle	podle	k7c2	podle
toponymických	toponymický	k2eAgInPc2d1	toponymický
údajů	údaj	k1gInPc2	údaj
obývali	obývat	k5eAaImAgMnP	obývat
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
oblasti	oblast	k1gFnSc3	oblast
Západní	západní	k2eAgFnSc2d1	západní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
etnikum	etnikum	k1gNnSc4	etnikum
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vymizení	vymizení	k1gNnSc2	vymizení
<g/>
.	.	kIx.	.
</s>
