<s>
Ketština	Ketština	k1gFnSc1	Ketština
(	(	kIx(	(
<g/>
к	к	k?	к
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgMnSc1d1	poslední
živý	živý	k2eAgMnSc1d1	živý
představitel	představitel	k1gMnSc1	představitel
jenisejské	jenisejský	k2eAgFnSc2d1	jenisejský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
národem	národ	k1gInSc7	národ
Ketů	Ket	k1gInPc2	Ket
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dříve	dříve	k6eAd2	dříve
podle	podle	k7c2	podle
toponymických	toponymický	k2eAgInPc2d1	toponymický
údajů	údaj	k1gInPc2	údaj
obývali	obývat	k5eAaImAgMnP	obývat
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
oblasti	oblast	k1gFnSc3	oblast
Západní	západní	k2eAgFnSc2d1	západní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
etnikum	etnikum	k1gNnSc4	etnikum
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vymizení	vymizení	k1gNnSc2	vymizení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
čítal	čítat	k5eAaImAgInS	čítat
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
Kety	Kety	k1gInPc4	Kety
<g/>
,	,	kIx,	,
1494	[number]	k4	1494
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
ketštiny	ketština	k1gFnSc2	ketština
však	však	k9	však
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
150	[number]	k4	150
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Krasnojarském	Krasnojarský	k2eAgInSc6d1	Krasnojarský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řeky	řeka	k1gFnSc2	řeka
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
příslušnost	příslušnost	k1gFnSc1	příslušnost
ketského	ketský	k2eAgInSc2d1	ketský
jazyka	jazyk	k1gInSc2	jazyk
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nS	tvořit
"	"	kIx"	"
<g/>
spojovací	spojovací	k2eAgInSc1d1	spojovací
článek	článek	k1gInSc1	článek
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
sinokavkazskou	sinokavkazský	k2eAgFnSc7d1	sinokavkazský
a	a	k8xC	a
indoevropskou	indoevropský	k2eAgFnSc7d1	indoevropská
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ketština	Ketština	k1gFnSc1	Ketština
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
příbuzná	příbuzná	k1gFnSc1	příbuzná
s	s	k7c7	s
dnes	dnes	k6eAd1	dnes
již	již	k9	již
prakticky	prakticky	k6eAd1	prakticky
vymřelou	vymřelý	k2eAgFnSc7d1	vymřelá
jughštinou	jughština	k1gFnSc7	jughština
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
najít	najít	k5eAaPmF	najít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
izolovaným	izolovaný	k2eAgInSc7d1	izolovaný
jihoasijským	jihoasijský	k2eAgInSc7d1	jihoasijský
jazykem	jazyk	k1gInSc7	jazyk
burušaski	burušask	k1gFnSc2	burušask
nebo	nebo	k8xC	nebo
sinotibetskými	sinotibetský	k2eAgInPc7d1	sinotibetský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
ketština	ketština	k1gFnSc1	ketština
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
navrhované	navrhovaný	k2eAgFnSc2d1	navrhovaná
dené-kavkazské	denéavkazský	k2eAgFnSc2d1	dené-kavkazský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
hypotéz	hypotéza	k1gFnPc2	hypotéza
nebyla	být	k5eNaImAgFnS	být
zatím	zatím	k6eAd1	zatím
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Joseph	Joseph	k1gMnSc1	Joseph
Greenberg	Greenberg	k1gMnSc1	Greenberg
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
spojitost	spojitost	k1gFnSc4	spojitost
mezi	mezi	k7c7	mezi
ketštinou	ketština	k1gFnSc7	ketština
(	(	kIx(	(
<g/>
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
jenisejskými	jenisejský	k2eAgInPc7d1	jenisejský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
severoamerickou	severoamerický	k2eAgFnSc7d1	severoamerická
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
rodinou	rodina	k1gFnSc7	rodina
na-dené	naená	k1gFnSc2	na-dená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
této	tento	k3xDgFnSc2	tento
domněnky	domněnka	k1gFnSc2	domněnka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
lingvista	lingvista	k1gMnSc1	lingvista
Edward	Edward	k1gMnSc1	Edward
Wajda	Wajda	k1gMnSc1	Wajda
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spřízněnost	spřízněnost	k1gFnSc1	spřízněnost
obou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
demonstrovala	demonstrovat	k5eAaBmAgFnS	demonstrovat
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
byl	být	k5eAaImAgInS	být
příznivě	příznivě	k6eAd1	příznivě
přijat	přijmout	k5eAaPmNgInS	přijmout
významnými	významný	k2eAgMnPc7d1	významný
odborníky	odborník	k1gMnPc7	odborník
jak	jak	k8xC	jak
na	na	k7c4	na
jenisejské	jenisejský	k2eAgInPc4d1	jenisejský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
jazyky	jazyk	k1gInPc4	jazyk
na-dené	naená	k1gFnSc2	na-dená
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Krauss	Kraussa	k1gFnPc2	Kraussa
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
Leer	Leer	k1gMnSc1	Leer
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Kari	kari	k1gNnSc2	kari
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Werner	Werner	k1gMnSc1	Werner
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
známými	známý	k2eAgMnPc7d1	známý
lingvisty	lingvista	k1gMnPc7	lingvista
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bernard	Bernard	k1gMnSc1	Bernard
Comrie	Comrie	k1gFnSc2	Comrie
<g/>
,	,	kIx,	,
Johanna	Johanna	k1gFnSc1	Johanna
Nichols	Nichols	k1gInSc1	Nichols
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Golla	Golla	k1gMnSc1	Golla
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Fortescue	Fortescu	k1gFnSc2	Fortescu
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Hamp	Hamp	k1gInSc1	Hamp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vneslo	vnést	k5eAaPmAgNnS	vnést
do	do	k7c2	do
otázky	otázka	k1gFnSc2	otázka
klasifikace	klasifikace	k1gFnSc2	klasifikace
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
na	na	k7c4	na
jenisejské	jenisejský	k2eAgInPc4d1	jenisejský
jazyky	jazyk	k1gInPc4	jazyk
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
však	však	k9	však
nadále	nadále	k6eAd1	nadále
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
nebo	nebo	k8xC	nebo
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
úplně	úplně	k6eAd1	úplně
odmítají	odmítat	k5eAaImIp3nP	odmítat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Stefan	Stefan	k1gMnSc1	Stefan
Georg	Georg	k1gMnSc1	Georg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Normálně	normálně	k6eAd1	normálně
polootevřené	polootevřený	k2eAgNnSc1d1	polootevřené
/	/	kIx~	/
<g/>
ɛ	ɛ	k?	ɛ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
/	/	kIx~	/
<g/>
ɔ	ɔ	k?	ɔ
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
polozavřené	polozavřený	k2eAgNnSc1d1	polozavřené
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
[	[	kIx(	[
<g/>
o	o	k7c4	o
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ɐ	ɐ	k?	ɐ
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
ɑ	ɑ	k?	ɑ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
nosové	nosový	k2eAgFnPc1d1	nosová
souhlásky	souhláska	k1gFnPc1	souhláska
v	v	k7c6	v
ketštině	ketština	k1gFnSc6	ketština
mají	mít	k5eAaImIp3nP	mít
neznělé	znělý	k2eNgInPc1d1	neznělý
alofony	alofon	k1gInPc1	alofon
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jednoslabičného	jednoslabičný	k2eAgNnSc2d1	jednoslabičné
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
glotalizovaným	glotalizovaný	k2eAgInSc7d1	glotalizovaný
nebo	nebo	k8xC	nebo
klesajícím	klesající	k2eAgInSc7d1	klesající
tónem	tón	k1gInSc7	tón
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
ŋ	ŋ	k?	ŋ
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
̥	̥	k?	̥
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
̥	̥	k?	̥
<g/>
,	,	kIx,	,
ŋ	ŋ	k?	ŋ
<g/>
̥	̥	k?	̥
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
situaci	situace	k1gFnSc6	situace
[	[	kIx(	[
<g/>
l	l	kA	l
<g/>
]	]	kIx)	]
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
[	[	kIx(	[
<g/>
ɬ	ɬ	k?	ɬ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
ketština	ketština	k1gFnSc1	ketština
tónickým	tónický	k2eAgMnSc7d1	tónický
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
se	se	k3xPyFc4	se
však	však	k9	však
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
tónických	tónický	k2eAgInPc2d1	tónický
jazyků	jazyk	k1gInPc2	jazyk
není	být	k5eNaImIp3nS	být
tón	tón	k1gInSc1	tón
charakteristikou	charakteristika	k1gFnSc7	charakteristika
každé	každý	k3xTgFnSc2	každý
slabiky	slabika	k1gFnSc2	slabika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
tónů	tón	k1gInPc2	tón
v	v	k7c6	v
ketštině	ketština	k1gFnSc6	ketština
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Ketština	Ketština	k1gFnSc1	Ketština
neměla	mít	k5eNaImAgFnS	mít
písmo	písmo	k1gNnSc4	písmo
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ruský	ruský	k2eAgMnSc1d1	ruský
vědec	vědec	k1gMnSc1	vědec
N.	N.	kA	N.
K.	K.	kA	K.
Karger	Karger	k1gMnSc1	Karger
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
písma	písmo	k1gNnSc2	písmo
pro	pro	k7c4	pro
ketštinu	ketština	k1gFnSc4	ketština
na	na	k7c6	na
základě	základ	k1gInSc6	základ
latinky	latinka	k1gFnSc2	latinka
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
první	první	k4xOgInSc4	první
ketský	ketský	k2eAgInSc4d1	ketský
slabikář	slabikář	k1gInSc4	slabikář
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
nové	nový	k2eAgNnSc1d1	nové
ketské	ketský	k2eAgNnSc1d1	ketský
písmo	písmo	k1gNnSc1	písmo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírně	mírně	k6eAd1	mírně
upravené	upravený	k2eAgFnPc1d1	upravená
azbuky	azbuka	k1gFnPc1	azbuka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Dříve	dříve	k6eAd2	dříve
používaná	používaný	k2eAgFnSc1d1	používaná
latinka	latinka	k1gFnSc1	latinka
<g/>
:	:	kIx,	:
Písmo	písmo	k1gNnSc1	písmo
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
azbuce	azbuka	k1gFnSc6	azbuka
zavedené	zavedený	k2eAgFnSc6d1	zavedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ket	Ket	k1gFnSc2	Ket
language	language	k1gFnPc2	language
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
К	К	k?	К
я	я	k?	я
<g/>
,	,	kIx,	,
ketský	ketský	k2eAgInSc1d1	ketský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
o	o	k7c6	o
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
národů	národ	k1gInPc2	národ
</s>
