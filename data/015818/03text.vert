<s>
Názorové	názorový	k2eAgNnSc1d1
vůdcovství	vůdcovství	k1gNnSc1
</s>
<s>
Názorové	názorový	k2eAgNnSc1d1
vůdcovství	vůdcovství	k1gNnSc1
je	být	k5eAaImIp3nS
sociologický	sociologický	k2eAgInSc1d1
koncept	koncept	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgNnSc2
existuje	existovat	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
část	část	k1gFnSc4
aktivních	aktivní	k2eAgMnPc2d1
a	a	k8xC
informovaných	informovaný	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
ovlivňují	ovlivňovat	k5eAaImIp3nP
názory	názor	k1gInPc4
ostatních	ostatní	k2eAgFnPc2d1
(	(	kIx(
<g/>
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
jedince	jedinko	k6eAd1
nazýváme	nazývat	k5eAaImIp1nP
názorovými	názorový	k2eAgMnPc7d1
vůdci	vůdce	k1gMnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
opinion	opinion	k1gInSc1
leaders	leadersa	k1gFnPc2
nebo	nebo	k8xC
opinion	opinion	k1gInSc1
makers	makersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
je	on	k3xPp3gNnSc4
napříč	napříč	k7c7
sociálními	sociální	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
ve	v	k7c6
všech	všecek	k3xTgFnPc6
skupinách	skupina	k1gFnPc6
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
společenský	společenský	k2eAgInSc4d1
status	status	k1gInSc4
<g/>
,	,	kIx,
vzdělaní	vzdělaný	k2eAgMnPc1d1
a	a	k8xC
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
ostatním	ostatní	k2eAgMnPc3d1
členům	člen	k1gMnPc3
skupiny	skupina	k1gFnSc2
jsou	být	k5eAaImIp3nP
vnímavější	vnímavý	k2eAgMnPc1d2
vůči	vůči	k7c3
jejím	její	k3xOp3gInPc3
zájmům	zájem	k1gInPc3
<g/>
,	,	kIx,
informovanější	informovaný	k2eAgFnSc3d2
co	co	k9
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
skupiny	skupina	k1gFnPc1
dotýkají	dotýkat	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
k	k	k7c3
jejím	její	k3xOp3gInPc3
problémům	problém	k1gInPc3
nejčastěji	často	k6eAd3
vyjadřují	vyjadřovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
dvoustupňový	dvoustupňový	k2eAgInSc4d1
tok	tok	k1gInSc4
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vůdci	vůdce	k1gMnPc1
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP
skupině	skupina	k1gFnSc3
informace	informace	k1gFnSc2
z	z	k7c2
masových	masový	k2eAgInPc2d1
médii	médium	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Teorii	teorie	k1gFnSc4
názorových	názorový	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
dále	daleko	k6eAd2
rozvíjí	rozvíjet	k5eAaImIp3nS
R.	R.	kA
K.	K.	kA
Merton	Merton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišuje	rozlišovat	k5eAaImIp3nS
vůdce	vůdce	k1gMnSc1
lokálního	lokální	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
místní	místní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
vůdce	vůdce	k1gMnSc2
kosmopolitního	kosmopolitní	k2eAgMnSc2d1
<g/>
,	,	kIx,
zajímajícího	zajímající	k2eAgMnSc2d1
se	se	k3xPyFc4
o	o	k7c4
problémy	problém	k1gInPc4
širšího	široký	k2eAgInSc2d2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studie	studie	k1gFnSc1
</s>
<s>
The	The	k?
People	People	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gFnPc4
</s>
<s>
První	první	k4xOgFnSc7
studií	studie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
konceptem	koncept	k1gInSc7
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
práce	práce	k1gFnSc1
kolektivu	kolektiv	k1gInSc2
autorů	autor	k1gMnPc2
Paula	Paul	k1gMnSc2
Lazarsfelda	Lazarsfeld	k1gMnSc2
<g/>
,	,	kIx,
Bernarda	Bernard	k1gMnSc2
Berelsona	Berelson	k1gMnSc2
a	a	k8xC
Hazela	Hazel	k1gMnSc2
Gaudeta	Gaudet	k1gMnSc2
The	The	k1gMnSc2
People	People	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choic	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInSc1
tým	tým	k1gInSc1
zkoumal	zkoumat	k5eAaImAgInS
chování	chování	k1gNnSc4
voličů	volič	k1gMnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
prezidentských	prezidentský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
okrese	okres	k1gInSc6
Erie	Eri	k1gFnSc2
County	Counta	k1gFnSc2
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
od	od	k7c2
května	květen	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
1940	#num#	k4
metodou	metoda	k1gFnSc7
opakovaných	opakovaný	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
stejného	stejný	k2eAgInSc2d1
vzorku	vzorek	k1gInSc2
respondentů	respondent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květno	k1gNnSc6
tazatelé	tazatel	k1gMnPc1
vytipovali	vytipovat	k5eAaPmAgMnP
přibližně	přibližně	k6eAd1
3	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
reprezentovali	reprezentovat	k5eAaImAgMnP
nejblíže	blízce	k6eAd3
složení	složení	k1gNnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
celého	celý	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vybraného	vybraný	k2eAgInSc2d1
vzorku	vzorek	k1gInSc2
byly	být	k5eAaImAgFnP
utvořeny	utvořit	k5eAaPmNgFnP
čtyři	čtyři	k4xCgFnPc1
samostatné	samostatný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
po	po	k7c6
600	#num#	k4
lidech	lid	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgMnPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
sloužily	sloužit	k5eAaImAgFnP
jako	jako	k9
kontrolní	kontrolní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
a	a	k8xC
byly	být	k5eAaImAgFnP
dotazovány	dotazovat	k5eAaImNgInP
jen	jen	k9
jednou	jednou	k6eAd1
za	za	k7c4
celé	celý	k2eAgNnSc4d1
předvolební	předvolební	k2eAgNnSc4d1
období	období	k1gNnSc4
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
A	a	k9
v	v	k7c6
červenci	červenec	k1gInSc6
<g/>
,	,	kIx,
B	B	kA
v	v	k7c6
srpnu	srpen	k1gInSc6
a	a	k8xC
C	C	kA
v	v	k7c6
říjnu	říjen	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgInSc1d1
panel	panel	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dotazována	dotazovat	k5eAaImNgFnS
pravidelně	pravidelně	k6eAd1
každý	každý	k3xTgInSc4
měsíc	měsíc	k1gInSc4
počínaje	počínaje	k7c7
květnem	květen	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
před	před	k7c7
prezidentskými	prezidentský	k2eAgFnPc7d1
primárkami	primárky	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
konče	konče	k7c7
po	po	k7c6
listopadových	listopadový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
se	se	k3xPyFc4
zaměřil	zaměřit	k5eAaPmAgInS
na	na	k7c4
vývoj	vývoj	k1gInSc4
volebních	volební	k2eAgFnPc2d1
preferencích	preference	k1gFnPc6
jednotlivých	jednotlivý	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
a	a	k8xC
sledoval	sledovat	k5eAaImAgMnS
jaké	jaký	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
podněty	podnět	k1gInPc4
stojí	stát	k5eAaImIp3nS
případně	případně	k6eAd1
za	za	k7c7
změnou	změna	k1gFnSc7
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
(	(	kIx(
<g/>
média	médium	k1gNnPc1
<g/>
,	,	kIx,
referenční	referenční	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Autoři	autor	k1gMnPc1
práce	práce	k1gFnSc2
hledali	hledat	k5eAaImAgMnP
názorové	názorový	k2eAgFnPc4d1
vůdce	vůdce	k1gMnSc1
položením	položení	k1gNnSc7
jednoduché	jednoduchý	k2eAgFnSc2d1
dvojice	dvojice	k1gFnSc2
otázek	otázka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
jste	být	k5eAaImIp2nP
se	se	k3xPyFc4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
někoho	někdo	k3yInSc4
přesvědčit	přesvědčit	k5eAaPmF
o	o	k7c6
svých	svůj	k3xOyFgInPc6
politických	politický	k2eAgInPc6d1
názorech	názor	k1gInPc6
<g/>
?	?	kIx.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS
Vás	vy	k3xPp2nPc4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
někdo	někdo	k3yInSc1
o	o	k7c4
radu	rada	k1gFnSc4
v	v	k7c6
politických	politický	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
?	?	kIx.
</s>
<s>
Pokud	pokud	k8xS
respondent	respondent	k1gMnSc1
na	na	k7c4
obě	dva	k4xCgFnPc4
otázky	otázka	k1gFnPc4
odpověděl	odpovědět	k5eAaPmAgMnS
kladně	kladně	k6eAd1
<g/>
,	,	kIx,
21	#num#	k4
%	%	kIx~
respondentů	respondent	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
panelu	panel	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
klasifikován	klasifikovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
názorový	názorový	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
měli	mít	k5eAaImAgMnP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
formování	formování	k1gNnSc4
pohledu	pohled	k1gInSc2
nerozhodnutých	rozhodnutý	k2eNgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
konání	konání	k1gNnSc2
výzkumu	výzkum	k1gInSc2
se	se	k3xPyFc4
o	o	k7c6
10	#num#	k4
%	%	kIx~
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
aktivně	aktivně	k6eAd1
či	či	k8xC
pasivně	pasivně	k6eAd1
<g/>
,	,	kIx,
účastnilo	účastnit	k5eAaImAgNnS
politických	politický	k2eAgFnPc2d1
debat	debata	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
věnovalo	věnovat	k5eAaImAgNnS,k5eAaPmAgNnS
poslechu	poslech	k1gInSc3
rádia	rádio	k1gNnSc2
nebo	nebo	k8xC
čtení	čtení	k1gNnSc2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
periodika	periodikum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
i	i	k9
mezi	mezi	k7c7
voliči	volič	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
během	během	k7c2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
rozhodli	rozhodnout	k5eAaPmAgMnP
volit	volit	k5eAaImF
opačně	opačně	k6eAd1
oproti	oproti	k7c3
původnímu	původní	k2eAgInSc3d1
úmyslu	úmysl	k1gInSc3
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
nerozhodnutí	nerozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
převládal	převládat	k5eAaImAgInS
jako	jako	k9
hlavní	hlavní	k2eAgInSc1d1
důvod	důvod	k1gInSc1
ke	k	k7c3
změně	změna	k1gFnSc3
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
přítelem	přítel	k1gMnSc7
či	či	k8xC
členem	člen	k1gMnSc7
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgFnPc4
čtvrtiny	čtvrtina	k1gFnPc1
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgMnPc4
předpokládali	předpokládat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
volbám	volba	k1gFnPc3
vůbec	vůbec	k9
nepůjdou	jít	k5eNaImIp3nP
<g/>
,	,	kIx,
uvedlo	uvést	k5eAaPmAgNnS
obdobně	obdobně	k6eAd1
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgInSc1d1
důvod	důvod	k1gInSc1
ke	k	k7c3
změně	změna	k1gFnSc3
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
blízkou	blízký	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
názorovým	názorový	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Rovere	rover	k1gMnSc5
Study	stud	k1gInPc7
</s>
<s>
Vzápětí	vzápětí	k6eAd1
po	po	k7c6
skočení	skočení	k1gNnSc6
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
proběhl	proběhnout	k5eAaPmAgMnS
v	v	k7c6
New	New	k1gFnSc6
Jersey	Jersea	k1gFnSc2
obdobný	obdobný	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
však	však	k9
zvolili	zvolit	k5eAaPmAgMnP
opačný	opačný	k2eAgInSc4d1
přístup	přístup	k1gInSc4
nežli	nežli	k8xS
v	v	k7c6
předchozí	předchozí	k2eAgFnSc6d1
studii	studie	k1gFnSc6
a	a	k8xC
respondentů	respondent	k1gMnPc2
se	se	k3xPyFc4
ptali	ptat	k5eAaImAgMnP
<g/>
:	:	kIx,
„	„	k?
<g/>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
Vás	vy	k3xPp2nPc4
ovlivňuje	ovlivňovat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
označeni	označit	k5eAaPmNgMnP
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
4	#num#	k4
<g/>
×	×	k?
za	za	k7c4
vlivné	vlivný	k2eAgInPc4d1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
posléze	posléze	k6eAd1
podrobeni	podroben	k2eAgMnPc1d1
samostatnému	samostatný	k2eAgInSc3d1
rozhovoru	rozhovor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
The	The	k1gFnSc2
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choika	k1gFnSc6
se	se	k3xPyFc4
autoři	autor	k1gMnPc1
zaměřili	zaměřit	k5eAaPmAgMnP
na	na	k7c4
vůdce	vůdce	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
ovlivňovali	ovlivňovat	k5eAaImAgMnP
širší	široký	k2eAgFnPc4d2
skupiny	skupina	k1gFnPc4
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
ovlivňování	ovlivňování	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
pominuto	pominut	k2eAgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
klasifikaci	klasifikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
se	se	k3xPyFc4
nevěnovali	věnovat	k5eNaImAgMnP
prvotnímu	prvotní	k2eAgNnSc3d1
vztahu	vztah	k1gInSc6
mezi	mezi	k7c7
vůdci	vůdce	k1gMnPc7
a	a	k8xC
původními	původní	k2eAgMnPc7d1
respondenty	respondent	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Decatur	Decatura	k1gFnPc2
Study	stud	k1gInPc4
</s>
<s>
The	The	k?
Decatur	Decatura	k1gFnPc2
Study	stud	k1gInPc1
proběhla	proběhnout	k5eAaPmAgNnP
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důraz	důraz	k1gInSc1
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
kladen	klást	k5eAaImNgInS
na	na	k7c4
popis	popis	k1gInSc4
případů	případ	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dopad	dopad	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
rozeznán	rozeznat	k5eAaPmNgInS
a	a	k8xC
vyhodnocen	vyhodnotit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
Rovere	rover	k1gMnSc5
studie	studie	k1gFnSc2
byly	být	k5eAaImAgInP
prováděny	prováděn	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
označeni	označit	k5eAaPmNgMnP
jako	jako	k8xS,k8xC
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
se	se	k3xPyFc4
však	však	k9
výzkum	výzkum	k1gInSc1
zaměřil	zaměřit	k5eAaPmAgInS
i	i	k9
na	na	k7c4
relativní	relativní	k2eAgFnSc4d1
důležitost	důležitost	k1gFnSc4
osobního	osobní	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
a	a	k8xC
na	na	k7c4
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
vůdci	vůdce	k1gMnPc7
a	a	k8xC
ostatními	ostatní	k2eAgMnPc7d1
členy	člen	k1gMnPc7
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tzv.	tzv.	kA
metodě	metoda	k1gFnSc3
sněhové	sněhový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
jednotlivci	jednotlivec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
splňují	splňovat	k5eAaImIp3nP
dané	daný	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
(	(	kIx(
<g/>
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyzvání	vyzvánět	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jmenovali	jmenovat	k5eAaImAgMnP,k5eAaBmAgMnP
další	další	k2eAgMnPc1d1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc1
daná	daný	k2eAgNnPc1d1
kritéria	kritérion	k1gNnPc1
splňuji	splňovat	k5eAaImIp1nS
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
širší	široký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
názorových	názorový	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nominující	nominující	k2eAgFnSc1d1
shodně	shodně	k6eAd1
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
ovlivněni	ovlivněn	k2eAgMnPc1d1
jejich	jejich	k3xOp3gNnPc3
působením	působení	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k9
mluvit	mluvit	k5eAaImF
o	o	k7c6
názorových	názorový	k2eAgMnPc6d1
vůdcích	vůdce	k1gMnPc6
názorových	názorový	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
názorové	názorový	k2eAgNnSc1d1
vůdcovství	vůdcovství	k1gNnSc1
není	být	k5eNaImIp3nS
trvalým	trvalý	k2eAgNnSc7d1
vlastnictvím	vlastnictví	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
s	s	k7c7
referenční	referenční	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
,	,	kIx,
časem	čas	k1gInSc7
i	i	k8xC
prostorem	prostor	k1gInSc7
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
mění	měnit	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Drug	Drug	k1gInSc1
Study	stud	k1gInPc4
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
The	The	k1gMnPc1
Drug	Druga	k1gFnPc2
Study	stud	k1gInPc4
vědci	vědec	k1gMnPc1
zjišťovali	zjišťovat	k5eAaImAgMnP
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
vliv	vliv	k1gInSc4
má	mít	k5eAaImIp3nS
na	na	k7c4
doktory	doktor	k1gMnPc4
integrace	integrace	k1gFnSc2
v	v	k7c6
lékařské	lékařský	k2eAgFnSc6d1
komunitě	komunita	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
způsobem	způsob	k1gInSc7
pak	pak	k6eAd1
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
předepisování	předepisování	k1gNnSc6
nových	nový	k2eAgInPc2d1
léků	lék	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
relativně	relativně	k6eAd1
malému	malý	k2eAgInSc3d1
počtu	počet	k1gInSc3
lékařů	lékař	k1gMnPc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
(	(	kIx(
<g/>
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
na	na	k7c4
1000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
popsat	popsat	k5eAaPmF
roli	role	k1gFnSc4
osobního	osobní	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
provedením	provedení	k1gNnSc7
rozhovorů	rozhovor	k1gInPc2
se	se	k3xPyFc4
všemi	všecek	k3xTgMnPc7
doktory	doktor	k1gMnPc7
ve	v	k7c6
4	#num#	k4
městech	město	k1gNnPc6
na	na	k7c6
americkém	americký	k2eAgInSc6d1
středozápadě	středozápad	k1gInSc6
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
obeznámení	obeznámení	k1gNnSc4
s	s	k7c7
novým	nový	k2eAgInSc7d1
lékem	lék	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
úspěšně	úspěšně	k6eAd1
začal	začít	k5eAaPmAgInS
předepisovat	předepisovat	k5eAaImF
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
před	před	k7c7
začátkem	začátek	k1gInSc7
studie	studie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
sociometrických	sociometrický	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
lékaři	lékař	k1gMnPc1
jmenovali	jmenovat	k5eAaImAgMnP,k5eAaBmAgMnP
3	#num#	k4
kolegy	kolega	k1gMnPc7
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
nejčastěji	často	k6eAd3
konzultují	konzultovat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
případy	případ	k1gInPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vystopovat	vystopovat	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jednotliví	jednotlivý	k2eAgMnPc1d1
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
mezi	mezi	k7c7
doktory	doktor	k1gMnPc7
ovlivňují	ovlivňovat	k5eAaImIp3nP
nasazení	nasazení	k1gNnSc1
nového	nový	k2eAgInSc2d1
léku	lék	k1gInSc2
ve	v	k7c6
skupině	skupina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
integrace	integrace	k1gFnSc1
lékařů	lékař	k1gMnPc2
v	v	k7c6
komunitě	komunita	k1gFnSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
navázanost	navázanost	k1gFnSc1
na	na	k7c4
jednotlivé	jednotlivý	k2eAgMnPc4d1
názorové	názorový	k2eAgMnPc4d1
vůdce	vůdce	k1gMnPc4
ovlivňuje	ovlivňovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
s	s	k7c7
jakou	jaký	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
byl	být	k5eAaImAgMnS
nový	nový	k2eAgInSc4d1
lék	lék	k1gInSc4
jednotlivými	jednotlivý	k2eAgMnPc7d1
lékaři	lékař	k1gMnSc3
předepisován	předepisován	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
dvoustupňového	dvoustupňový	k2eAgInSc2d1
toku	tok	k1gInSc2
komunikace	komunikace	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navrhnout	k5eAaPmNgNnS
vyjmutí	vyjmutí	k1gNnSc1
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
článku	článek	k1gInSc2
a	a	k8xC
její	její	k3xOp3gNnSc4
přesunutí	přesunutí	k1gNnSc4
na	na	k7c4
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
Teorie	teorie	k1gFnSc2
dvoustupňového	dvoustupňový	k2eAgInSc2d1
toku	tok	k1gInSc2
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
k	k	k7c3
návrhu	návrh	k1gInSc3
<g/>
:	:	kIx,
téma	téma	k1gNnSc1
vhodné	vhodný	k2eAgNnSc1d1
pro	pro	k7c4
samotný	samotný	k2eAgInSc4d1
článek	článek	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
anglická	anglický	k2eAgFnSc1d1
wiki	wik	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
mají	mít	k5eAaImIp3nP
největší	veliký	k2eAgInSc4d3
zájem	zájem	k1gInSc4
na	na	k7c6
veřejném	veřejný	k2eAgNnSc6d1
dění	dění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jejich	jejich	k3xOp3gMnSc7
zvýšeným	zvýšený	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
úměrně	úměrně	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
participace	participace	k1gFnPc4
(	(	kIx(
<g/>
debaty	debata	k1gFnPc4
<g/>
,	,	kIx,
demonstrace	demonstrace	k1gFnPc4
<g/>
,	,	kIx,
volby	volba	k1gFnPc4
<g/>
)	)	kIx)
v	v	k7c6
občanském	občanský	k2eAgInSc6d1
životě	život	k1gInSc6
i	i	k9
vystavení	vystavení	k1gNnSc1
masové	masový	k2eAgNnSc1d1
komunikaci	komunikace	k1gFnSc3
(	(	kIx(
<g/>
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
rádio	rádio	k1gNnSc1
<g/>
,	,	kIx,
televize	televize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
zainteresovaní	zainteresovaný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
o	o	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
v	v	k7c6
takové	takový	k3xDgFnSc6
míře	míra	k1gFnSc6
nezajímají	zajímat	k5eNaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jim	on	k3xPp3gMnPc3
jako	jako	k9
náhradní	náhradní	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
slouží	sloužit	k5eAaImIp3nP
právě	právě	k9
názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
lidmi	člověk	k1gMnPc7
artikulují	artikulovat	k5eAaImIp3nP
náhled	náhled	k1gInSc4
na	na	k7c4
problém	problém	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gInSc6
sami	sám	k3xTgMnPc1
dočetli	dočíst	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případně	případně	k6eAd1
upozorňují	upozorňovat	k5eAaImIp3nP
na	na	k7c4
zajímavý	zajímavý	k2eAgInSc4d1
článek	článek	k1gInSc4
či	či	k8xC
pořad	pořad	k1gInSc4
své	svůj	k3xOyFgMnPc4
kolegy	kolega	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názoroví	názorový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
tak	tak	k6eAd1
fungují	fungovat	k5eAaImIp3nP
jako	jako	k9
jakýsi	jakýsi	k3yIgInSc4
most	most	k1gInSc4
mezi	mezi	k7c7
formálními	formální	k2eAgNnPc7d1
médii	médium	k1gNnPc7
a	a	k8xC
řadovým	řadový	k2eAgNnSc7d1
publikem	publikum	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dopady	dopad	k1gInPc1
osobního	osobní	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
</s>
<s>
Vliv	vliv	k1gInSc1
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
masových	masový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
vliv	vliv	k1gInSc1
má	mít	k5eAaImIp3nS
oproti	oproti	k7c3
masovým	masový	k2eAgNnPc3d1
médiím	médium	k1gNnPc3
psychologickou	psychologický	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
masová	masový	k2eAgNnPc1d1
média	médium	k1gNnPc1
působí	působit	k5eAaImIp3nP
na	na	k7c4
jedince	jedinec	k1gMnPc4
takřka	takřka	k6eAd1
neustále	neustále	k6eAd1
<g/>
,	,	kIx,
míří	mířit	k5eAaImIp3nS
na	na	k7c4
celý	celý	k2eAgInSc4d1
terč	terč	k1gInSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musí	muset	k5eAaImIp3nS
zasáhnout	zasáhnout	k5eAaPmF
celé	celý	k2eAgNnSc4d1
své	své	k1gNnSc4
cílové	cílový	k2eAgNnSc1d1
publikum	publikum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobní	osobní	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
cílí	cílit	k5eAaImIp3nS
na	na	k7c4
konkrétního	konkrétní	k2eAgMnSc4d1
jedince	jedinec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpovědi	výpověď	k1gFnSc6
respondentů	respondent	k1gMnPc2
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
změnili	změnit	k5eAaPmAgMnP
názor	názor	k1gInSc4
pod	pod	k7c7
tlakem	tlak	k1gInSc7
osobní	osobní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stejnorodost	stejnorodost	k1gFnSc1
názorů	názor	k1gInPc2
v	v	k7c6
primárních	primární	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
</s>
<s>
Účinnost	účinnost	k1gFnSc1
osobního	osobní	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
se	se	k3xPyFc4
také	také	k9
odrazila	odrazit	k5eAaPmAgFnS
ve	v	k7c6
stejnorodosti	stejnorodost	k1gFnSc6
názorů	názor	k1gInPc2
a	a	k8xC
chování	chování	k1gNnSc2
v	v	k7c6
sociálních	sociální	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředkem	prostředek	k1gInSc7
komunikace	komunikace	k1gFnSc2
v	v	k7c6
nich	on	k3xPp3gFnPc6
je	být	k5eAaImIp3nS
rozhovor	rozhovor	k1gInSc1
z	z	k7c2
očí	oko	k1gNnPc2
do	do	k7c2
očí	oko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názorový	názorový	k2eAgInSc1d1
vůdce	vůdce	k1gMnSc4
se	se	k3xPyFc4
dokáže	dokázat	k5eAaPmIp3nS
přizpůsobit	přizpůsobit	k5eAaPmF
zvyklostem	zvyklost	k1gFnPc3
svého	svůj	k3xOyFgMnSc2
kolegy	kolega	k1gMnSc2
a	a	k8xC
vyvinout	vyvinout	k5eAaPmF
na	na	k7c4
něj	on	k3xPp3gMnSc4
„	„	k?
<g/>
efektivní	efektivní	k2eAgInSc4d1
molekulární	molekulární	k2eAgInSc4d1
tlak	tlak	k1gInSc4
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
názorové	názorový	k2eAgFnSc3d1
homogenitě	homogenita	k1gFnSc3
sociální	sociální	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odměna	odměna	k1gFnSc1
za	za	k7c4
poddajnost	poddajnost	k1gFnSc4
</s>
<s>
Od	od	k7c2
dětství	dětství	k1gNnSc2
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
učí	učit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
nepříjemnostem	nepříjemnost	k1gFnPc3
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
sociální	sociální	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
může	moct	k5eAaImIp3nS
jednoduše	jednoduše	k6eAd1
tím	ten	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
bude	být	k5eAaImBp3nS
dělat	dělat	k5eAaImF
<g/>
,	,	kIx,
co	co	k8xS
mu	on	k3xPp3gMnSc3
ostatní	ostatní	k2eAgMnPc1d1
řeknou	říct	k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
společnosti	společnost	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
uspokojuje	uspokojovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
proti	proti	k7c3
proudu	proud	k1gInSc3
většinového	většinový	k2eAgInSc2d1
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
jde	jít	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
minoritu	minorita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
jedinců	jedinec	k1gMnPc2
raději	rád	k6eAd2
podlehne	podlehnout	k5eAaPmIp3nS
převládajícímu	převládající	k2eAgInSc3d1
pohledu	pohled	k1gInSc3
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nezáměrnost	Nezáměrnost	k1gFnSc1
osobních	osobní	k2eAgInPc2d1
kontaktů	kontakt	k1gInPc2
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
jedinec	jedinec	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
vystavit	vystavit	k5eAaPmF
masovým	masový	k2eAgNnPc3d1
médiím	médium	k1gNnPc3
<g/>
,	,	kIx,
ze	z	k7c2
zkušenosti	zkušenost	k1gFnSc2
se	se	k3xPyFc4
předem	předem	k6eAd1
obrní	obrnit	k5eAaPmIp3nP
svými	svůj	k3xOyFgInPc7
názory	názor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Média	médium	k1gNnPc1
tak	tak	k8xS,k8xC
mají	mít	k5eAaImIp3nP
jen	jen	k9
malou	malý	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
jej	on	k3xPp3gMnSc4
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častěji	často	k6eAd2
navíc	navíc	k6eAd1
jedinec	jedinec	k1gMnSc1
sleduje	sledovat	k5eAaImIp3nS
médium	médium	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
vyhovuje	vyhovovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
pohledu	pohled	k1gInSc2
na	na	k7c4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
osobní	osobní	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
se	se	k3xPyFc4
však	však	k9
takto	takto	k6eAd1
připravit	připravit	k5eAaPmF
nelze	lze	k6eNd1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nevystavuje	vystavovat	k5eNaImIp3nS
dobrovolně	dobrovolně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
pronikavé	pronikavý	k2eAgInPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
k	k	k7c3
člověku	člověk	k1gMnSc3
dostanou	dostat	k5eAaPmIp3nP
nezáměrně	záměrně	k6eNd1
(	(	kIx(
<g/>
i	i	k9
ve	v	k7c6
formě	forma	k1gFnSc6
klepů	klep	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
snadněji	snadno	k6eAd2
ho	on	k3xPp3gMnSc4
ovlivní	ovlivnit	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důvěra	důvěra	k1gFnSc1
ve	v	k7c4
věrohodný	věrohodný	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
</s>
<s>
Pokud	pokud	k8xS
jedinec	jedinec	k1gMnSc1
čte	číst	k5eAaImIp3nS
či	či	k8xC
slyší	slyšet	k5eAaImIp3nS
názor	názor	k1gInSc4
komentátora	komentátor	k1gMnSc2
či	či	k8xC
odborníka	odborník	k1gMnSc2
v	v	k7c6
masových	masový	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
může	moct	k5eAaImIp3nS
si	se	k3xPyFc3
klást	klást	k5eAaImF
otázku	otázka	k1gFnSc4
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
věrohodnosti	věrohodnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zda	zda	k8xS
odborník	odborník	k1gMnSc1
dané	daný	k2eAgNnSc4d1
téma	téma	k1gNnSc4
rozebírá	rozebírat	k5eAaImIp3nS
z	z	k7c2
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
ovlivní	ovlivnit	k5eAaPmIp3nS
právě	právě	k9
jeho	jeho	k3xOp3gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
pro	pro	k7c4
jedince	jedinec	k1gMnPc4
daleko	daleko	k6eAd1
věrohodnější	věrohodný	k2eAgMnSc1d2
a	a	k8xC
přesvědčivější	přesvědčivý	k2eAgInSc1d2
názor	názor	k1gInSc1
jedince	jedinec	k1gMnSc2
(	(	kIx(
<g/>
názorového	názorový	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
<g/>
)	)	kIx)
z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
blízkého	blízký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
důvěřuje	důvěřovat	k5eAaImIp3nS
a	a	k8xC
jemuž	jenž	k3xRgMnSc3
připisuje	připisovat	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
němu	on	k3xPp3gInSc3
má	mít	k5eAaImIp3nS
důvěru	důvěra	k1gFnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c4
problém	problém	k1gInSc4
dívá	dívat	k5eAaImIp3nS
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
jako	jako	k8xC,k8xS
on	on	k3xPp3gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Způsob	způsob	k1gInSc1
měření	měření	k1gNnSc2
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
</s>
<s>
Respondent	respondent	k1gMnSc1
na	na	k7c6
základě	základ	k1gInSc6
dotazníků	dotazník	k1gInPc2
a	a	k8xC
hloubkových	hloubkový	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
hodnotí	hodnotit	k5eAaImIp3nS
míru	míra	k1gFnSc4
svého	svůj	k3xOyFgInSc2
vlivu	vliv	k1gInSc2
na	na	k7c4
blízké	blízký	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
počátcích	počátek	k1gInPc6
fenoménu	fenomén	k1gInSc2
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
se	se	k3xPyFc4
sociologové	sociolog	k1gMnPc1
při	při	k7c6
svých	svůj	k3xOyFgInPc6
výzkumech	výzkum	k1gInPc6
vystačili	vystačit	k5eAaBmAgMnP
pouze	pouze	k6eAd1
s	s	k7c7
určením	určení	k1gNnSc7
<g/>
,	,	kIx,
zdali	zdali	k8xS
někdo	někdo	k3yInSc1
je	být	k5eAaImIp3nS
či	či	k8xC
není	být	k5eNaImIp3nS
názorovým	názorový	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lazarsfeld	Lazarsfelda	k1gFnPc2
<g/>
,	,	kIx,
Berelson	Berelson	k1gNnSc4
a	a	k8xC
Gaudet	Gaudet	k1gInSc4
si	se	k3xPyFc3
vystačili	vystačit	k5eAaBmAgMnP
pouze	pouze	k6eAd1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
otázkami	otázka	k1gFnPc7
–	–	k?
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
dělili	dělit	k5eAaImAgMnP
buď	buď	k8xC
na	na	k7c4
vůdce	vůdce	k1gMnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
následovníky	následovník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přibývajícími	přibývající	k2eAgFnPc7d1
studiemi	studie	k1gFnPc7
ale	ale	k8xC
přicházely	přicházet	k5eAaImAgFnP
snahy	snaha	k1gFnPc1
o	o	k7c4
nahrazení	nahrazení	k1gNnSc4
strohého	strohý	k2eAgNnSc2d1
dělení	dělení	k1gNnSc2
na	na	k7c4
vůdce	vůdce	k1gMnSc4
a	a	k8xC
nevůdce	nevůdec	k1gMnSc4
spojitou	spojitý	k2eAgFnSc7d1
stupnicí	stupnice	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
určovala	určovat	k5eAaImAgFnS
míru	míra	k1gFnSc4
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
případně	případně	k6eAd1
názorové	názorový	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc4
stupnici	stupnice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
z	z	k7c2
použití	použití	k1gNnSc2
šesti	šest	k4xCc2
sebehodnotících	sebehodnotící	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
využila	využít	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
sociologů	sociolog	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
zkoumala	zkoumat	k5eAaImAgFnS
výměny	výměna	k1gFnPc4
názorů	názor	k1gInPc2
mezi	mezi	k7c7
farmáři	farmář	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Problémem	problém	k1gInSc7
těchto	tento	k3xDgNnPc2
šetření	šetření	k1gNnPc2
byla	být	k5eAaImAgFnS
validace	validace	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
ověřování	ověřování	k1gNnSc1
pravdivosti	pravdivost	k1gFnSc2
odpovědí	odpověď	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
prováděla	provádět	k5eAaImAgFnS
jen	jen	k9
velmi	velmi	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
pak	pak	k6eAd1
tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInPc4
dosavadní	dosavadní	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
rozpracovala	rozpracovat	k5eAaPmAgFnS
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
Elisabeth	Elisabeth	k1gInSc4
Noelle-Neumannová	Noelle-Neumannová	k1gFnSc1
se	s	k7c7
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořili	vytvořit	k5eAaPmAgMnP
tzv.	tzv.	kA
Persönlichkeitsstärke	Persönlichkeitsstärke	k1gFnSc4
škálu	škála	k1gFnSc4
(	(	kIx(
<g/>
PS	PS	kA
škálu	škála	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
lze	lze	k6eAd1
vyhodnotit	vyhodnotit	k5eAaPmF
„	„	k?
<g/>
sílu	síla	k1gFnSc4
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Klíčem	klíč	k1gInSc7
k	k	k7c3
určení	určení	k1gNnSc3
míry	míra	k1gFnSc2
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
je	být	k5eAaImIp3nS
10	#num#	k4
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
zodpovídání	zodpovídání	k1gNnSc6
respondent	respondent	k1gMnSc1
předpokládá	předpokládat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
chování	chování	k1gNnSc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
běžných	běžný	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
i	i	k9
druhotně	druhotně	k6eAd1
hodnotí	hodnotit	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
na	na	k7c6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
verze	verze	k1gFnSc1
PS	PS	kA
škály	škála	k1gFnSc2
</s>
<s>
TvrzeníSouhlas	TvrzeníSouhlas	k1gInSc1
(	(	kIx(
<g/>
body	bod	k1gInPc1
<g/>
)	)	kIx)
<g/>
Nesouhlas	nesouhlas	k1gInSc1
(	(	kIx(
<g/>
body	bod	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
počítám	počítat	k5eAaImIp1nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
udělám	udělat	k5eAaPmIp1nS
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
úspěšné	úspěšný	k2eAgNnSc1d1
<g/>
.137	.137	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
zřídka	zřídka	k6eAd1
si	se	k3xPyFc3
nejsem	být	k5eNaImIp1nS
jistý	jistý	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
mám	mít	k5eAaImIp1nS
chovat	chovat	k5eAaImF
<g/>
.147	.147	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rád	rád	k6eAd1
přijímám	přijímat	k5eAaImIp1nS
odpovědnost	odpovědnost	k1gFnSc4
<g/>
.157	.157	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rád	rád	k6eAd1
se	se	k3xPyFc4
ujímám	ujímat	k5eAaImIp1nS
vedení	vedení	k1gNnSc2
při	při	k7c6
společných	společný	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
<g/>
.178	.178	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
chutí	chuť	k1gFnSc7
přesvědčuji	přesvědčovat	k5eAaImIp1nS
druhé	druhý	k4xOgFnSc6
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
mínění	mínění	k1gNnSc6
<g/>
.157	.157	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
zjišťuji	zjišťovat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ostatní	ostatní	k2eAgMnPc1d1
řídí	řídit	k5eAaImIp3nP
podle	podle	k7c2
mého	můj	k3xOp1gInSc2
vzoru	vzor	k1gInSc2
<g/>
.168	.168	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovedu	dovést	k5eAaPmIp1nS
se	se	k3xPyFc4
dobře	dobře	k6eAd1
prosadit	prosadit	k5eAaPmF
<g/>
.147	.147	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsem	být	k5eAaImIp1nS
často	často	k6eAd1
o	o	k7c4
krok	krok	k1gInSc4
před	před	k7c7
ostatními	ostatní	k2eAgMnPc7d1
<g/>
.189	.189	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mám	mít	k5eAaImIp1nS
hodně	hodně	k6eAd1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mi	já	k3xPp1nSc3
ostatní	ostatní	k2eAgMnPc1d1
závidí	závidět	k5eAaImIp3nS
(	(	kIx(
<g/>
Je	být	k5eAaImIp3nS
toho	ten	k3xDgNnSc2
hodně	hodně	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
mi	já	k3xPp1nSc3
ostatní	ostatní	k2eAgMnPc1d1
závidí	závidět	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.159	.159	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
dávám	dávat	k5eAaImIp1nS
rady	rada	k1gFnPc4
a	a	k8xC
doporučení	doporučení	k1gNnPc4
ostatním	ostatní	k2eAgMnPc3d1
<g/>
.126	.126	k4
</s>
<s>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maximální	maximální	k2eAgNnSc1d1
možné	možný	k2eAgNnSc1d1
skóre	skóre	k1gNnSc1
je	být	k5eAaImIp3nS
149	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
minimální	minimální	k2eAgFnSc4d1
75	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součet	součet	k1gInSc1
skóre	skóre	k1gNnSc2
u	u	k7c2
odpovědí	odpověď	k1gFnPc2
zařazuje	zařazovat	k5eAaImIp3nS
odpovídající	odpovídající	k2eAgMnSc1d1
do	do	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
následujících	následující	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
hodnoty	hodnota	k1gFnSc2
do	do	k7c2
99	#num#	k4
bodů	bod	k1gInPc2
označují	označovat	k5eAaImIp3nP
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
někom	někdo	k3yInSc6
názorově	názorově	k6eAd1
závislé	závislý	k2eAgFnPc1d1
<g/>
,	,	kIx,
hodnoty	hodnota	k1gFnPc1
indexu	index	k1gInSc2
mezi	mezi	k7c7
100	#num#	k4
až	až	k8xS
123	#num#	k4
body	bod	k1gInPc7
ukazují	ukazovat	k5eAaImIp3nP
na	na	k7c4
osoby	osoba	k1gFnPc4
průměrné	průměrný	k2eAgFnSc2d1
a	a	k8xC
index	index	k1gInSc1
s	s	k7c7
vysokými	vysoký	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
zpravidla	zpravidla	k6eAd1
od	od	k7c2
124	#num#	k4
bodů	bod	k1gInPc2
označuje	označovat	k5eAaImIp3nS
názorové	názorový	k2eAgMnPc4d1
vůdce	vůdce	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
49	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REVUE	revue	k1gFnPc1
PRO	pro	k7c4
MÉDIA	médium	k1gNnPc4
č.	č.	k?
10	#num#	k4
–	–	k?
Heslář	heslář	k1gInSc1
–	–	k?
Aktivní	aktivní	k2eAgNnSc1d1
publikum	publikum	k1gNnSc1
<g/>
.	.	kIx.
rpm	rpm	k?
<g/>
.	.	kIx.
<g/>
fss	fss	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jeřábek	Jeřábek	k1gMnSc1
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
50.1	50.1	k4
2	#num#	k4
3	#num#	k4
KATZ	KATZ	kA
<g/>
,	,	kIx,
Elihu	Eliha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Two-Step	Two-Step	k1gMnSc1
Flow	Flow	k1gMnSc1
of	of	k?
Communication	Communication	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gMnSc5
Up-To-Date	Up-To-Dat	k1gMnSc5
Report	report	k1gInSc4
on	on	k3xPp3gMnSc1
an	an	k?
Hypothesis	Hypothesis	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Public	publicum	k1gNnPc2
Opinion	Opinion	k1gInSc4
Quarterly	Quarterlo	k1gNnPc7
<g/>
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1957	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
Anniversary	Anniversara	k1gFnPc1
Issue	Issu	k1gInSc2
Devoted	Devoted	k1gInSc1
to	ten	k3xDgNnSc4
Twenty	Twenta	k1gFnPc1
Years	Yearsa	k1gFnPc2
of	of	k?
Pu	Pu	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
266687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://repository.upenn.edu/cgi/viewcontent.cgi?article=1279&	http://repository.upenn.edu/cgi/viewcontent.cgi?article=1279&	k1gInSc1
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
150	#num#	k4
<g/>
–	–	k?
<g/>
151	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
153	#num#	k4
<g/>
–	–	k?
<g/>
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
154	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
155	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
152	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lazarsfeld	Lazarsfeld	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
155	#num#	k4
<g/>
–	–	k?
<g/>
156.1	156.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Jeřábek	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
691	#num#	k4
<g/>
–	–	k?
<g/>
692	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Noelle-Neumann	Noelle-Neumann	k1gInSc1
<g/>
,	,	kIx,
E.	E.	kA
1983	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
Persönlichkeitsstärke	Persönlichkeitsstärke	k1gInSc1
–	–	k?
ein	ein	k?
neues	neues	k1gMnSc1
Kriterium	kriterium	k1gNnSc1
zur	zur	k?
Zielgruppenbeschreibung	Zielgruppenbeschreibung	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPIEGEL	SPIEGEL	kA
–	–	k?
Dokumentation	Dokumentation	k1gInSc1
<g/>
:	:	kIx,
Persönlichkeitsstärke	Persönlichkeitsstärke	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ein	Ein	k1gMnSc1
neuer	neuer	k1gMnSc1
Maßstab	Maßstab	k1gMnSc1
zur	zur	k?
Bestimmung	Bestimmung	k1gInSc1
von	von	k1gInSc1
Zielgruppenpotentialen	Zielgruppenpotentialen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamburg	Hamburg	k1gInSc1
<g/>
,	,	kIx,
Spiegel	Spiegel	k1gInSc1
Verlag	Verlag	k1gInSc1
1983	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Weimann	Weimann	k1gInSc1
<g/>
,	,	kIx,
G.	G.	kA
1991	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
The	The	k1gFnSc1
Influentials	Influentials	k1gInSc1
<g/>
:	:	kIx,
Back	Back	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Concept	Concept	k1gInSc1
of	of	k?
Opinion	Opinion	k1gInSc1
Leaders	Leaders	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Public	publicum	k1gNnPc2
Opinion	Opinion	k1gInSc1
Quarterly	Quarterla	k1gFnSc2
55	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
267	#num#	k4
<g/>
–	–	k?
<g/>
279	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
H.	H.	kA
Jeřábek	jeřábek	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
využitím	využití	k1gNnSc7
německé	německý	k2eAgFnSc2d1
i	i	k8xC
anglické	anglický	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Jeřábek	jeřábek	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
694	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LAZARSFELD	LAZARSFELD	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Felix	Felix	k1gMnSc1
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
BERELSON	BERELSON	kA
a	a	k8xC
Hazel	Hazel	k1gMnSc1
GAUDET	GAUDET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
people	people	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
choice	choika	k1gFnSc6
<g/>
:	:	kIx,
how	how	k?
the	the	k?
voter	voter	k1gMnSc1
makes	makes	k1gMnSc1
up	up	k?
his	his	k1gNnPc2
mind	mindo	k1gNnPc2
in	in	k?
a	a	k8xC
presidential	presidential	k1gMnSc1
campaign	campaign	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
231	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8583	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JEŘÁBEK	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Hynek	Hynek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měření	měření	k1gNnSc1
názorového	názorový	k2eAgNnSc2d1
vůdcovství	vůdcovství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgInPc6d1
sociologických	sociologický	k2eAgInPc6d1
výzkumech	výzkum	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sociologický	sociologický	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Czech	Czech	k1gInSc1
sociological	sociologicat	k5eAaPmAgMnS
review	review	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2336	#num#	k4
<g/>
-	-	kIx~
<g/>
128	#num#	k4
<g/>
x.	x.	k?
Dostupné	dostupný	k2eAgFnSc2d1
z	z	k7c2
<g/>
:	:	kIx,
http://sreview.soc.cas.cz/uploads/9e9ef5d411d618da8e470cd68ff6e38ce043cf25_499_56jerab20.pdf	http://sreview.soc.cas.cz/uploads/9e9ef5d411d618da8e470cd68ff6e38ce043cf25_499_56jerab20.pdf	k1gInSc1
</s>
<s>
KATZ	KATZ	kA
<g/>
,	,	kIx,
Elihu	Eliha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Two-Step	Two-Step	k1gMnSc1
Flow	Flow	k1gMnSc1
of	of	k?
Communication	Communication	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gMnSc5
Up-To-Date	Up-To-Dat	k1gMnSc5
Report	report	k1gInSc4
on	on	k3xPp3gMnSc1
an	an	k?
Hypothesis	Hypothesis	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Public	publicum	k1gNnPc2
Opinion	Opinion	k1gInSc4
Quarterly	Quarterlo	k1gNnPc7
<g/>
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1957	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
Anniversary	Anniversara	k1gFnPc1
Issue	Issu	k1gInSc2
Devoted	Devoted	k1gInSc1
to	ten	k3xDgNnSc4
Twenty	Twenta	k1gFnPc1
Years	Yearsa	k1gFnPc2
of	of	k?
Pu	Pu	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
266687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://repository.upenn.edu/cgi/viewcontent.cgi?article=1279&	http://repository.upenn.edu/cgi/viewcontent.cgi?article=1279&	k6eAd1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4169346-2	4169346-2	k4
</s>
