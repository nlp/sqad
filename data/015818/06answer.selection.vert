<s>
Názorové	názorový	k2eAgNnSc1d1
vůdcovství	vůdcovství	k1gNnSc1
je	být	k5eAaImIp3nS
sociologický	sociologický	k2eAgInSc1d1
koncept	koncept	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgNnSc2
existuje	existovat	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
část	část	k1gFnSc4
aktivních	aktivní	k2eAgMnPc2d1
a	a	k8xC
informovaných	informovaný	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
ovlivňují	ovlivňovat	k5eAaImIp3nP
názory	názor	k1gInPc4
ostatních	ostatní	k2eAgFnPc2d1
(	(	kIx(
<g/>
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>