<s>
Konkláve	konkláve	k1gNnSc1	konkláve
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
cum	cum	k?	cum
clave	claev	k1gFnSc2	claev
-	-	kIx~	-
(	(	kIx(	(
<g/>
uzamčen	uzamčen	k2eAgInSc1d1	uzamčen
<g/>
)	)	kIx)	)
na	na	k7c4	na
klíč	klíč	k1gInSc4	klíč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
kardinálů	kardinál	k1gMnPc2	kardinál
volící	volící	k2eAgMnSc1d1	volící
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
volil	volit	k5eAaImAgInS	volit
zcela	zcela	k6eAd1	zcela
demokratickým	demokratický	k2eAgInSc7d1	demokratický
způsobem	způsob	k1gInSc7	způsob
klérus	klérus	k1gInSc1	klérus
i	i	k8xC	i
všichni	všechen	k3xTgMnPc1	všechen
římští	římský	k2eAgMnPc1d1	římský
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
při	při	k7c6	při
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
samých	samý	k3xTgInPc6	samý
počátcích	počátek	k1gInPc6	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
sice	sice	k8xC	sice
probíhala	probíhat	k5eAaImAgFnS	probíhat
tajně	tajně	k6eAd1	tajně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
nové	nový	k2eAgNnSc1d1	nové
náboženství	náboženství	k1gNnSc1	náboženství
legitimizováno	legitimizován	k2eAgNnSc1d1	legitimizován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
volby	volba	k1gFnSc2	volba
začali	začít	k5eAaPmAgMnP	začít
zasahovat	zasahovat	k5eAaImF	zasahovat
světští	světský	k2eAgMnPc1d1	světský
vládci	vládce	k1gMnPc1	vládce
-	-	kIx~	-
zpočátku	zpočátku	k6eAd1	zpočátku
římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
panovníci	panovník	k1gMnPc1	panovník
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
či	či	k8xC	či
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Papežství	papežství	k1gNnSc1	papežství
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
naprosté	naprostý	k2eAgFnSc2d1	naprostá
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
světské	světský	k2eAgFnSc6d1	světská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Úřadující	úřadující	k2eAgMnSc1d1	úřadující
papež	papež	k1gMnSc1	papež
také	také	k9	také
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
označil	označit	k5eAaPmAgMnS	označit
přímo	přímo	k6eAd1	přímo
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1059	[number]	k4	1059
bulu	bula	k1gFnSc4	bula
In	In	k1gFnPc2	In
nomine	nominout	k5eAaPmIp3nS	nominout
Domini	Domin	k1gMnPc1	Domin
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Páně	páně	k2eAgNnSc6d1	páně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyhradil	vyhradit	k5eAaPmAgMnS	vyhradit
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
kardinály-biskupy	kardinályiskup	k1gMnPc4	kardinály-biskup
<g/>
.	.	kIx.	.
</s>
<s>
Klérus	klérus	k1gInSc4	klérus
i	i	k8xC	i
celé	celý	k2eAgNnSc4d1	celé
křesťanstvo	křesťanstvo	k1gNnSc4	křesťanstvo
včetně	včetně	k7c2	včetně
panovníků	panovník	k1gMnPc2	panovník
pak	pak	k6eAd1	pak
mělo	mít	k5eAaImAgNnS	mít
volbu	volba	k1gFnSc4	volba
jen	jen	k6eAd1	jen
dodatečně	dodatečně	k6eAd1	dodatečně
schválit	schválit	k5eAaPmF	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1179	[number]	k4	1179
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
privilegium	privilegium	k1gNnSc4	privilegium
volby	volba	k1gFnSc2	volba
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
kardinály	kardinál	k1gMnPc4	kardinál
a	a	k8xC	a
také	také	k9	také
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
musí	muset	k5eAaImIp3nS	muset
hlasovat	hlasovat	k5eAaImF	hlasovat
nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
snahy	snaha	k1gFnPc4	snaha
uzavřít	uzavřít	k5eAaPmF	uzavřít
se	se	k3xPyFc4	se
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
před	před	k7c7	před
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
již	již	k6eAd1	již
začátkem	začátkem	k7c2	začátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1216	[number]	k4	1216
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
konkláve	konkláve	k1gNnSc2	konkláve
definitivně	definitivně	k6eAd1	definitivně
zavedl	zavést	k5eAaPmAgInS	zavést
Řehoř	Řehoř	k1gMnSc1	Řehoř
X.	X.	kA	X.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1274	[number]	k4	1274
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgInS	prosadit
volbu	volba	k1gFnSc4	volba
papeže	papež	k1gMnSc2	papež
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
prostoru	prostor	k1gInSc6	prostor
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
pohodlím	pohodlí	k1gNnSc7	pohodlí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
mělo	mít	k5eAaImAgNnS	mít
zamezit	zamezit	k5eAaPmF	zamezit
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
prodlužování	prodlužování	k1gNnSc3	prodlužování
období	období	k1gNnSc2	období
sedisvakance	sedisvakanec	k1gInSc2	sedisvakanec
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
stoletích	století	k1gNnPc6	století
často	často	k6eAd1	často
odehrával	odehrávat	k5eAaImAgInS	odehrávat
celé	celý	k2eAgInPc4d1	celý
týdny	týden	k1gInPc4	týden
nebo	nebo	k8xC	nebo
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
volba	volba	k1gFnSc1	volba
papeže	papež	k1gMnSc2	papež
trvala	trvat	k5eAaImAgFnS	trvat
31	[number]	k4	31
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
1269	[number]	k4	1269
do	do	k7c2	do
září	září	k1gNnSc2	září
1271	[number]	k4	1271
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
právě	právě	k9	právě
Řehoř	Řehoř	k1gMnSc1	Řehoř
X.	X.	kA	X.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
nebyli	být	k5eNaImAgMnP	být
podle	podle	k7c2	podle
nařízení	nařízení	k1gNnSc2	nařízení
Řehoře	Řehoř	k1gMnSc2	Řehoř
X.	X.	kA	X.
omezeni	omezen	k2eAgMnPc1d1	omezen
pouze	pouze	k6eAd1	pouze
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nesměli	smět	k5eNaImAgMnP	smět
opouštět	opouštět	k5eAaImF	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nemohli	moct	k5eNaImAgMnP	moct
dlouho	dlouho	k6eAd1	dlouho
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dohody	dohoda	k1gFnPc4	dohoda
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
snižován	snižovat	k5eAaImNgInS	snižovat
přísun	přísun	k1gInSc1	přísun
jídla	jídlo	k1gNnSc2	jídlo
z	z	k7c2	z
venku	venek	k1gInSc2	venek
-	-	kIx~	-
pokud	pokud	k8xS	pokud
hlasování	hlasování	k1gNnSc1	hlasování
trvalo	trvat	k5eAaImAgNnS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
3	[number]	k4	3
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
kardinálům	kardinál	k1gMnPc3	kardinál
omezeno	omezen	k2eAgNnSc1d1	omezeno
množství	množství	k1gNnSc1	množství
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
5	[number]	k4	5
dnech	den	k1gInPc6	den
byl	být	k5eAaImAgInS	být
podáván	podávat	k5eAaImNgInS	podávat
jen	jen	k9	jen
chléb	chléb	k1gInSc1	chléb
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ale	ale	k9	ale
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
volba	volba	k1gFnSc1	volba
také	také	k9	také
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kardinálům	kardinál	k1gMnPc3	kardinál
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
možným	možný	k2eAgFnPc3d1	možná
snahám	snaha	k1gFnPc3	snaha
volbu	volba	k1gFnSc4	volba
zvenčí	zvenčí	k6eAd1	zvenčí
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Voliči	volič	k1gMnPc1	volič
mohou	moct	k5eAaImIp3nP	moct
snáze	snadno	k6eAd2	snadno
dodržet	dodržet	k5eAaPmF	dodržet
"	"	kIx"	"
<g/>
Solum	Solum	k1gNnSc4	Solum
Deum	Deumo	k1gNnPc2	Deumo
prae	pra	k1gFnSc2	pra
oculis	oculis	k1gFnSc1	oculis
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
pouze	pouze	k6eAd1	pouze
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
nařízení	nařízení	k1gNnPc4	nařízení
při	při	k7c6	při
dalších	další	k2eAgNnPc6d1	další
konkláve	konkláve	k1gNnPc6	konkláve
často	často	k6eAd1	často
porušována	porušovat	k5eAaImNgFnS	porušovat
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgNnPc1d1	aktuální
pravidla	pravidlo	k1gNnPc1	pravidlo
konkláve	konkláve	k1gNnSc2	konkláve
shrnul	shrnout	k5eAaPmAgMnS	shrnout
a	a	k8xC	a
upravil	upravit	k5eAaPmAgMnS	upravit
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
konkláve	konkláve	k1gNnSc7	konkláve
je	být	k5eAaImIp3nS	být
povinností	povinnost	k1gFnSc7	povinnost
všech	všecek	k3xTgMnPc2	všecek
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
odstoupení	odstoupení	k1gNnSc4	odstoupení
papeže	papež	k1gMnSc2	papež
nepřekročili	překročit	k5eNaPmAgMnP	překročit
věk	věk	k1gInSc1	věk
osmdesáti	osmdesát	k4xCc2	osmdesát
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
hranici	hranice	k1gFnSc6	hranice
stanovil	stanovit	k5eAaPmAgMnS	stanovit
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc4d1	maximální
počet	počet	k1gInSc4	počet
volitelů	volitel	k1gMnPc2	volitel
je	být	k5eAaImIp3nS	být
však	však	k9	však
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
<s>
Sejít	sejít	k5eAaPmF	sejít
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
15	[number]	k4	15
-	-	kIx~	-
20	[number]	k4	20
dnů	den	k1gInPc2	den
po	po	k7c6	po
papežově	papežův	k2eAgFnSc6d1	papežova
smrti	smrt	k1gFnSc6	smrt
či	či	k8xC	či
odstoupení	odstoupení	k1gNnSc6	odstoupení
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jakýkoli	jakýkoli	k3yIgMnSc1	jakýkoli
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
katolík	katolík	k1gMnSc1	katolík
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
však	však	k9	však
úřad	úřad	k1gInSc1	úřad
svěřován	svěřován	k2eAgInSc1d1	svěřován
kardinálovi	kardinál	k1gMnSc3	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc4	tři
série	série	k1gFnPc4	série
skrutinií	skrutinium	k1gNnPc2	skrutinium
(	(	kIx(	(
<g/>
33	[number]	k4	33
-	-	kIx~	-
34	[number]	k4	34
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
)	)	kIx)	)
bezvýsledné	bezvýsledný	k2eAgInPc1d1	bezvýsledný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
postačí	postačit	k5eAaPmIp3nS	postačit
pro	pro	k7c4	pro
zvolení	zvolení	k1gNnSc4	zvolení
prostá	prostý	k2eAgFnSc1d1	prostá
většina	většina	k1gFnSc1	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uplatnění	uplatnění	k1gNnSc3	uplatnění
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
nástupce	nástupce	k1gMnSc2	nástupce
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
-	-	kIx~	-
podle	podle	k7c2	podle
nového	nový	k2eAgNnSc2d1	nové
pravidla	pravidlo	k1gNnSc2	pravidlo
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
hlasováních	hlasování	k1gNnPc6	hlasování
automaticky	automaticky	k6eAd1	automaticky
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
o	o	k7c4	o
2	[number]	k4	2
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
kardinálech	kardinál	k1gMnPc6	kardinál
posledního	poslední	k2eAgNnSc2d1	poslední
skrutinia	skrutinium	k1gNnSc2	skrutinium
třetí	třetí	k4xOgFnSc2	třetí
série	série	k1gFnSc2	série
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
získat	získat	k5eAaPmF	získat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Sixtinské	sixtinský	k2eAgFnSc6d1	Sixtinská
kapli	kaple	k1gFnSc6	kaple
a	a	k8xC	a
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
kardinál	kardinál	k1gMnSc1	kardinál
-	-	kIx~	-
komorník	komorník	k1gMnSc1	komorník
(	(	kIx(	(
<g/>
camerlengo	camerlengo	k1gMnSc1	camerlengo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konkláve	konkláve	k1gNnSc1	konkláve
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
se	se	k3xPyFc4	se
kardinálové	kardinál	k1gMnPc1	kardinál
v	v	k7c6	v
procesí	procesí	k1gNnSc6	procesí
odeberou	odebrat	k5eAaPmIp3nP	odebrat
do	do	k7c2	do
Sixtinské	sixtinský	k2eAgFnSc2d1	Sixtinská
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Složí	složit	k5eAaPmIp3nS	složit
zde	zde	k6eAd1	zde
přísahu	přísaha	k1gFnSc4	přísaha
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
a	a	k8xC	a
poté	poté	k6eAd1	poté
ceremoniář	ceremoniář	k1gMnSc1	ceremoniář
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
tzv.	tzv.	kA	tzv.
extra	extra	k2eAgInSc1d1	extra
omnes	omnes	k1gInSc1	omnes
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
všichni	všechen	k3xTgMnPc1	všechen
ven	ven	k6eAd1	ven
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
volby	volba	k1gFnPc1	volba
neúčastní	účastnit	k5eNaImIp3nP	účastnit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
kapli	kaple	k1gFnSc4	kaple
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
a	a	k8xC	a
volba	volba	k1gFnSc1	volba
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
<g/>
,	,	kIx,	,
jedí	jíst	k5eAaImIp3nP	jíst
a	a	k8xC	a
spí	spát	k5eAaImIp3nP	spát
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
prostoru	prostor	k1gInSc6	prostor
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
je	být	k5eAaImIp3nS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
naléhavé	naléhavý	k2eAgFnSc2d1	naléhavá
lékařské	lékařský	k2eAgFnSc2d1	lékařská
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
všechny	všechen	k3xTgFnPc1	všechen
televizní	televizní	k2eAgFnPc1d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgInPc1d1	rozhlasový
přijímače	přijímač	k1gInPc1	přijímač
<g/>
;	;	kIx,	;
dovnitř	dovnitř	k6eAd1	dovnitř
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
dostat	dostat	k5eAaPmF	dostat
žádné	žádný	k3yNgFnPc4	žádný
noviny	novina	k1gFnPc4	novina
nebo	nebo	k8xC	nebo
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
zakázané	zakázaný	k2eAgInPc1d1	zakázaný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Konkláve	konkláve	k1gNnSc1	konkláve
se	se	k3xPyFc4	se
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
i	i	k9	i
pomocný	pomocný	k2eAgInSc4d1	pomocný
personál	personál	k1gInSc4	personál
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
úklid	úklid	k1gInSc4	úklid
<g/>
,	,	kIx,	,
stravování	stravování	k1gNnSc4	stravování
i	i	k8xC	i
případnou	případný	k2eAgFnSc4d1	případná
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
také	také	k9	také
skládají	skládat	k5eAaImIp3nP	skládat
přísahu	přísaha	k1gFnSc4	přísaha
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kardinálové	kardinál	k1gMnPc1	kardinál
ani	ani	k9	ani
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nesmějí	smát	k5eNaImIp3nP	smát
hovořit	hovořit	k5eAaImF	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
volby	volba	k1gFnSc2	volba
přebývali	přebývat	k5eAaImAgMnP	přebývat
kardinálové	kardinál	k1gMnPc1	kardinál
původně	původně	k6eAd1	původně
v	v	k7c6	v
prostých	prostý	k2eAgFnPc6d1	prostá
celách	cela	k1gFnPc6	cela
v	v	k7c6	v
Apoštolském	apoštolský	k2eAgInSc6d1	apoštolský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudován	k2eAgNnSc1d1	zbudováno
poněkud	poněkud	k6eAd1	poněkud
příjemnější	příjemný	k2eAgNnSc1d2	příjemnější
ubytování	ubytování	k1gNnSc1	ubytování
v	v	k7c6	v
Domě	dům	k1gInSc6	dům
svaté	svatý	k2eAgFnSc2d1	svatá
Marty	Marta	k1gFnSc2	Marta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
průběhu	průběh	k1gInSc6	průběh
konkláve	konkláve	k1gNnSc2	konkláve
pro	pro	k7c4	pro
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
nepovolaného	povolaný	k2eNgMnSc4d1	nepovolaný
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
kardinálové	kardinál	k1gMnPc1	kardinál
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
dva	dva	k4xCgInPc4	dva
hlasovací	hlasovací	k2eAgInPc4d1	hlasovací
lístky	lístek	k1gInPc4	lístek
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
Hlasovací	hlasovací	k2eAgInSc1d1	hlasovací
lístek	lístek	k1gInSc1	lístek
je	být	k5eAaImIp3nS	být
obdélný	obdélný	k2eAgMnSc1d1	obdélný
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
jsou	být	k5eAaImIp3nP	být
vytištěna	vytištěn	k2eAgNnPc4d1	vytištěno
slova	slovo	k1gNnPc4	slovo
Eligo	Eligo	k6eAd1	Eligo
in	in	k?	in
Summum	Summum	k?	Summum
Pontificem	Pontific	k1gMnSc7	Pontific
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Volím	volit	k5eAaImIp1nS	volit
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dole	dole	k6eAd1	dole
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
jméno	jméno	k1gNnSc4	jméno
vybrané	vybraný	k2eAgFnSc2d1	vybraná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
musí	muset	k5eAaImIp3nP	muset
jméno	jméno	k1gNnSc4	jméno
kandidáta	kandidát	k1gMnSc2	kandidát
napsat	napsat	k5eAaBmF	napsat
pozměněným	pozměněný	k2eAgNnSc7d1	pozměněné
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
list	list	k1gInSc4	list
přeložit	přeložit	k5eAaPmF	přeložit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
každý	každý	k3xTgMnSc1	každý
kardinál	kardinál	k1gMnSc1	kardinál
zvedne	zvednout	k5eAaPmIp3nS	zvednout
viditelně	viditelně	k6eAd1	viditelně
lístek	lístek	k1gInSc4	lístek
a	a	k8xC	a
odnese	odnést	k5eAaPmIp3nS	odnést
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
urna	urna	k1gFnSc1	urna
přikrytá	přikrytý	k2eAgFnSc1d1	přikrytá
talířem	talíř	k1gInSc7	talíř
<g/>
.	.	kIx.	.
</s>
<s>
Položí	položit	k5eAaPmIp3nP	položit
svůj	svůj	k3xOyFgInSc4	svůj
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
na	na	k7c4	na
talíř	talíř	k1gInSc4	talíř
a	a	k8xC	a
překlopí	překlopit	k5eAaPmIp3nS	překlopit
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
urny	urna	k1gFnSc2	urna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
zbytečně	zbytečně	k6eAd1	zbytečně
složitý	složitý	k2eAgInSc4d1	složitý
způsob	způsob	k1gInSc4	způsob
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kardinál	kardinál	k1gMnSc1	kardinál
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
urny	urna	k1gFnSc2	urna
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
lístek	lístek	k1gInSc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odevzdání	odevzdání	k1gNnSc6	odevzdání
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
lístky	lístek	k1gInPc1	lístek
zamíchají	zamíchat	k5eAaPmIp3nP	zamíchat
<g/>
,	,	kIx,	,
spočítají	spočítat	k5eAaPmIp3nP	spočítat
a	a	k8xC	a
rozloží	rozložit	k5eAaPmIp3nP	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
lístky	lístek	k1gInPc4	lístek
sečteny	sečten	k2eAgInPc4d1	sečten
<g/>
,	,	kIx,	,
zvolá	zvolat	k5eAaPmIp3nS	zvolat
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sčitatelů	sčitatel	k1gMnPc2	sčitatel
jména	jméno	k1gNnSc2	jméno
těch	ten	k3xDgMnPc2	ten
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
získali	získat	k5eAaPmAgMnP	získat
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Jehlicí	jehlice	k1gFnSc7	jehlice
se	se	k3xPyFc4	se
propíchne	propíchnout	k5eAaPmIp3nS	propíchnout
každý	každý	k3xTgInSc4	každý
lístek	lístek	k1gInSc4	lístek
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vytištěno	vytištěn	k2eAgNnSc4d1	vytištěno
slovo	slovo	k1gNnSc4	slovo
Eligo	Eligo	k6eAd1	Eligo
-	-	kIx~	-
a	a	k8xC	a
navléknou	navléknout	k5eAaPmIp3nP	navléknout
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc4	všechen
na	na	k7c4	na
nit	nit	k1gInSc4	nit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překontrolování	překontrolování	k1gNnSc6	překontrolování
jsou	být	k5eAaImIp3nP	být
hlasovací	hlasovací	k2eAgInPc1d1	hlasovací
lístky	lístek	k1gInPc1	lístek
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
součty	součet	k1gInPc4	součet
hlasů	hlas	k1gInPc2	hlas
všech	všecek	k3xTgNnPc2	všecek
volebních	volební	k2eAgNnPc2d1	volební
kol	kolo	k1gNnPc2	kolo
zapisovány	zapisován	k2eAgFnPc1d1	zapisována
<g/>
.	.	kIx.	.
</s>
<s>
Listiny	listina	k1gFnPc1	listina
se	se	k3xPyFc4	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
ve	v	k7c6	v
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
zalepené	zalepený	k2eAgFnSc6d1	zalepená
obálce	obálka	k1gFnSc6	obálka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
jen	jen	k9	jen
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
náznakem	náznak	k1gInSc7	náznak
vývoje	vývoj	k1gInSc2	vývoj
situace	situace	k1gFnSc2	situace
uvnitř	uvnitř	k7c2	uvnitř
Sixtinské	sixtinský	k2eAgFnSc2d1	Sixtinská
kaple	kaple	k1gFnSc2	kaple
je	být	k5eAaImIp3nS	být
dým	dým	k1gInSc1	dým
z	z	k7c2	z
pálených	pálený	k2eAgInPc2d1	pálený
hlasovacích	hlasovací	k2eAgInPc2d1	hlasovací
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
davem	dav	k1gInSc7	dav
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
bedlivě	bedlivě	k6eAd1	bedlivě
sledován	sledovat	k5eAaImNgInS	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
umístěna	umístěn	k2eAgNnPc1d1	umístěno
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
kamínka	kamínka	k1gNnPc1	kamínka
a	a	k8xC	a
komínová	komínový	k2eAgFnSc1d1	komínová
roura	roura	k1gFnSc1	roura
je	být	k5eAaImIp3nS	být
vyvedena	vyvést	k5eAaPmNgFnS	vyvést
nad	nad	k7c4	nad
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
volbě	volba	k1gFnSc6	volba
se	se	k3xPyFc4	se
k	k	k7c3	k
páleným	pálený	k2eAgInPc3d1	pálený
lístkům	lístek	k1gInPc3	lístek
přidávala	přidávat	k5eAaImAgFnS	přidávat
smůla	smůla	k1gFnSc1	smůla
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vznikal	vznikat	k5eAaImAgInS	vznikat
černý	černý	k2eAgInSc4d1	černý
dým	dým	k1gInSc4	dým
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
k	k	k7c3	k
posledním	poslední	k2eAgInPc3d1	poslední
lístkům	lístek	k1gInPc3	lístek
se	se	k3xPyFc4	se
přidávala	přidávat	k5eAaImAgFnS	přidávat
mokrá	mokrý	k2eAgFnSc1d1	mokrá
sláma	sláma	k1gFnSc1	sláma
a	a	k8xC	a
dým	dým	k1gInSc1	dým
měl	mít	k5eAaImAgInS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vyvolat	vyvolat	k5eAaPmF	vyvolat
správnou	správný	k2eAgFnSc4d1	správná
barvu	barva	k1gFnSc4	barva
kouře	kouř	k1gInSc2	kouř
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
výsledek	výsledek	k1gInSc1	výsledek
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
,	,	kIx,	,
a	a	k8xC	a
kouř	kouř	k1gInSc1	kouř
měl	mít	k5eAaImAgInS	mít
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
barvu	barva	k1gFnSc4	barva
kouře	kouř	k1gInSc2	kouř
určena	určen	k2eAgNnPc1d1	určeno
jiná	jiný	k2eAgNnPc1d1	jiné
kamna	kamna	k1gNnPc1	kamna
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
kouře	kouř	k1gInSc2	kouř
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
AFP	AFP	kA	AFP
<g/>
,	,	kIx,	,
řízena	řízen	k2eAgFnSc1d1	řízena
chemickými	chemický	k2eAgFnPc7d1	chemická
přísadami	přísada	k1gFnPc7	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
černého	černý	k2eAgInSc2d1	černý
kouře	kouř	k1gInSc2	kouř
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
směs	směs	k1gFnSc1	směs
chloristanu	chloristan	k1gInSc2	chloristan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
antracenu	antracen	k2eAgFnSc4d1	antracen
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
bílého	bílý	k2eAgInSc2d1	bílý
kouře	kouř	k1gInSc2	kouř
směs	směs	k1gFnSc4	směs
chlorečnanu	chlorečnan	k1gInSc2	chlorečnan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
laktózy	laktóza	k1gFnSc2	laktóza
a	a	k8xC	a
kalafuny	kalafuna	k1gFnSc2	kalafuna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
platnému	platný	k2eAgNnSc3d1	platné
zvolení	zvolení	k1gNnSc3	zvolení
papeže	papež	k1gMnSc2	papež
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1	dvoutřetinová
většina	většina	k1gFnSc1	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálovi	kardinál	k1gMnSc3	kardinál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	on	k3xPp3gFnSc3	on
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
otázka	otázka	k1gFnSc1	otázka
<g/>
:	:	kIx,	:
Acceptasne	Acceptasne	k1gFnSc1	Acceptasne
electionem	election	k1gInSc7	election
de	de	k?	de
te	te	k?	te
canonice	canonice	k1gFnSc1	canonice
factam	factam	k1gInSc1	factam
in	in	k?	in
Summum	Summum	k?	Summum
Pontificem	Pontific	k1gMnSc7	Pontific
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Přijímáš	přijímat	k5eAaImIp2nS	přijímat
své	svůj	k3xOyFgNnSc4	svůj
kanonické	kanonický	k2eAgNnSc4d1	kanonické
zvolení	zvolení	k1gNnSc4	zvolení
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
<g/>
-li	i	k?	-li
kardinál	kardinál	k1gMnSc1	kardinál
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
první	první	k4xOgFnSc7	první
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gInSc3	on
položena	položit	k5eAaPmNgFnS	položit
další	další	k2eAgFnSc1d1	další
<g/>
:	:	kIx,	:
Quo	Quo	k1gFnSc1	Quo
nomine	nominout	k5eAaPmIp3nS	nominout
vis	vis	k1gInSc4	vis
vocari	vocar	k1gFnSc2	vocar
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jakým	jaký	k3yRgNnSc7	jaký
jménem	jméno	k1gNnSc7	jméno
se	se	k3xPyFc4	se
chceš	chtít	k5eAaImIp2nS	chtít
nazývat	nazývat	k5eAaImF	nazývat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Když	když	k8xS	když
si	se	k3xPyFc3	se
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
zvolí	zvolit	k5eAaPmIp3nS	zvolit
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
vzdají	vzdát	k5eAaPmIp3nP	vzdát
mu	on	k3xPp3gMnSc3	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
kardinálové	kardinál	k1gMnPc1	kardinál
hold	hold	k1gInSc4	hold
a	a	k8xC	a
slíbí	slíbit	k5eAaPmIp3nP	slíbit
poslušnost	poslušnost	k1gFnSc4	poslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oblékne	obléknout	k5eAaPmIp3nS	obléknout
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
připraveného	připravený	k2eAgNnSc2d1	připravené
roucha	roucho	k1gNnSc2	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zazní	zaznít	k5eAaPmIp3nS	zaznít
z	z	k7c2	z
balkonu	balkon	k1gInSc2	balkon
baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
tradiční	tradiční	k2eAgNnSc4d1	tradiční
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Annuntio	Annuntio	k1gNnSc1	Annuntio
vobis	vobis	k1gFnSc2	vobis
gaudium	gaudium	k1gNnSc1	gaudium
magnum	magnum	k1gInSc1	magnum
<g/>
.	.	kIx.	.
</s>
<s>
Habemus	Habemus	k1gInSc1	Habemus
Papam	Papam	k1gInSc1	Papam
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Oznamuji	oznamovat	k5eAaImIp1nS	oznamovat
vám	vy	k3xPp2nPc3	vy
radostnou	radostný	k2eAgFnSc4d1	radostná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
papeže	papež	k1gMnSc4	papež
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
zveřejněno	zveřejněn	k2eAgNnSc1d1	zveřejněno
jeho	jeho	k3xOp3gNnSc4	jeho
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc4	příjmení
s	s	k7c7	s
oznámením	oznámení	k1gNnSc7	oznámení
jaké	jaký	k3yQgNnSc4	jaký
papežské	papežský	k2eAgNnSc4d1	papežské
jméno	jméno	k1gNnSc4	jméno
bude	být	k5eAaImBp3nS	být
užívat	užívat	k5eAaImF	užívat
a	a	k8xC	a
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
papež	papež	k1gMnSc1	papež
předstoupí	předstoupit	k5eAaPmIp3nS	předstoupit
před	před	k7c4	před
dav	dav	k1gInSc4	dav
shromážděný	shromážděný	k2eAgInSc4d1	shromážděný
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
projevu	projev	k1gInSc6	projev
udělí	udělit	k5eAaPmIp3nP	udělit
tradiční	tradiční	k2eAgNnPc1d1	tradiční
požehnání	požehnání	k1gNnPc1	požehnání
Urbi	Urb	k1gFnSc2	Urb
et	et	k?	et
Orbi	Orb	k1gFnSc2	Orb
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
nový	nový	k2eAgInSc1d1	nový
pontifikát	pontifikát	k1gInSc1	pontifikát
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
slavena	slaven	k2eAgFnSc1d1	slavena
papežská	papežský	k2eAgFnSc1d1	Papežská
intronizace	intronizace	k1gFnSc1	intronizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
dnech	den	k1gInPc6	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
konkláve	konkláve	k1gNnPc2	konkláve
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Konkláve	konkláve	k1gNnSc2	konkláve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
papežský	papežský	k2eAgInSc1d1	papežský
stolec	stolec	k1gInSc1	stolec
rezignací	rezignace	k1gFnPc2	rezignace
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
datum	datum	k1gNnSc4	datum
začátku	začátek	k1gInSc2	začátek
dalšího	další	k2eAgNnSc2d1	další
konkláve	konkláve	k1gNnSc2	konkláve
stanoven	stanovit	k5eAaPmNgInS	stanovit
12	[number]	k4	12
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
.	.	kIx.	.
</s>
